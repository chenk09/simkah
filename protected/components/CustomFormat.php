<?php 
class CustomFormat extends CFormatter
{
    /**
     * merubah tanggal '12 Apr 2012' jadi '2012-04-12'
     * @param type $date
     * @return type 
     */
    public function formatDateMediumForDB($date)
    {
        $date = trim($date);
        $tanggal = explode(' ', $date);
        if (isset($tanggal[1])){
            $monthName = $tanggal[1];
            $tanggal[1] = $this->getMonthMM($tanggal[1]);
            $result = date ('Y-m-d', strtotime(implode('-', $tanggal)));
        }
        
        if (isset($tanggal[2])){
            if(strlen($tanggal[2]) < 4)
                return $tanggal[0].' '.$monthName.' '.$tanggal[2];
            else
                return $result;
        } else{
            return $date;
        }
        
    }
    
    /**
     * merubah tanggal 'Apr 2012' jadi '2012-04'
     * @param type $date
     * @return type 
     */
    public function formatMonthMediumForDB($month)
    {
        $month = trim($month);
        $tanggal = explode(' ', $month);
        if (isset($tanggal[0])){
            $monthName = $tanggal[0];
            $tanggal[0] = $this->getMonthMM($tanggal[0]);
            $result = $tanggal[1]."-".$tanggal[0];
        }
        return $result;
    }
    
    /**
     * convert d M Y H:i:s to Y-m-d H:i:s
     * @param type $date
     * @return string
     */
    public function formatDateTimeMediumForDB($date)
    {
        $tanggalSaja=trim(substr($date,0,-8)); //Menampilkan Tanggal Tanpa Jam
        $jamSaja=trim(substr($date,-8,8));  // menampilkan Jam Saja
        $tanggalDB = $this->formatDateMediumForDB($tanggalSaja);//Mengubah Tanggal inputan ke tanggal database
        $tanggalJamDB = $tanggalDB.' '.$jamSaja;
        return $tanggalJamDB;
    }
    
    public function formatDateMediumForUser($date)
    {
        $tanggal = explode(' ', $date);
        $monthName = $tanggal[1];
        $tanggal[1] = $this->getMonthUser($tanggal[1]);
        $result = date ('Y-m-d', strtotime(implode('-', $tanggal)));
        
        if(strlen($tanggal[2]) < 4)
            return $tanggal[0].' '.$monthName.' '.$tanggal[2];
        else
            return $result;
    }
    
    public function formatDateMediumForUserDMY($date)
    {
        $tanggal = explode(' ', $date);
        $monthName = $tanggal[1];
        $tanggal[1] = $this->getMonthUser($tanggal[1]);
        $result = $tanggal[0].'-'.$tanggal[1].'-'.$tanggal[2];
        
        if(strlen($tanggal[2]) < 4)
            return $tanggal[0].' '.$monthName.' '.$tanggal[2];
        else
            return $result;
    }
    
    public function formatDateTimeMediumForUser($date)
    {
        $tanggalSaja=trim(substr($date,0,-8)); //Menampilkan Tanggal Tanpa Jam
        $jamSaja=trim(substr($date,-8,8));  // menampilkan Jam Saja
        $tanggalDB = $this->formatDateMediumForUser($tanggalSaja);//Mengubah Tanggal inputan ke tanggal database
        $tanggalJamDB = $tanggalDB.' '.$jamSaja;
        return $tanggalJamDB;
    }
    
    public function getMonthUser($name)
    {
        switch($name){
            case 'Jan' : $result = '01'; break;
            case 'Feb' : $result = '02'; break;
            case 'Mar' : $result = '03'; break;
            case 'Apr' : $result = '04'; break;
            case 'Mei' : $result = '05'; break;
            case 'Jun' : $result = '06'; break;
            case 'Jul' : $result = '07'; break;
            case 'Agus' : $result = '08'; break;
            case 'Sep' : $result = '09'; break;
            case 'Okt' : $result = '10'; break;
            case 'Nop' : $result = '11'; break;
            case 'Des' : $result = '12'; break;
            case 'May' : $result = '05'; break;
            case 'Aug' : $result = '08'; break;
            case 'Agt' : $result = '08'; break;
            case 'Oct' : $result = '10'; break;
            case 'Nov' : $result = '11'; break;
            case 'Dec' : $result = '12'; break;
        }
        return $result;
    }
    
    
    
    public function getMonthMM($name)
    {
        $result = null;
        switch($name){
            case 'Jan' : $result = '01'; break;
            case 'Feb' : $result = '02'; break;
            case 'Mar' : $result = '03'; break;
            case 'Apr' : $result = '04'; break;
            case 'Mei' : $result = '05'; break;
            case 'Jun' : $result = '06'; break;
            case 'Jul' : $result = '07'; break;
            case 'Agus' : $result = '08'; break;
            case 'Sep' : $result = '09'; break;
            case 'Okt' : $result = '10'; break;
            case 'Nop' : $result = '11'; break;
            case 'Des' : $result = '12'; break;
            case 'May' : $result = '05'; break;
            case 'Aug' : $result = '08'; break;
            case 'Agt' : $result = '08'; break;
            case 'Oct' : $result = '10'; break;
            case 'Nov' : $result = '11'; break;
            case 'Dec' : $result = '12'; break;
            case 'Juni': $result = '06'; break;
            case 'Januari' : $result = '01'; break;
            case 'Februari' : $result = '02'; break;
            case 'Maret' : $result = '03'; break;
            case 'April' : $result = '04'; break;
            case 'Juli' : $result = '07'; break;
            case 'Agustus' : $result = '08'; break;
            case 'September' : $result = '09'; break;
            case 'Oktober' : $result = '10'; break;
            case 'Nopember' : $result = '11'; break;
            case 'Desember' : $result = '12'; break;
        }
        return $result;
    }
    /**
     * convert Y-m-d to d M Y (Indonesian)
     * contoh: 15 Januari 1990
     * @param type $date
     * @return string
     */
    public function formatDateINA($date)
    {
        // Date format in (Y-m-d)
        $result = "";
        $my_year = $this->list_month();
        $date_arr = explode("-",$date);
        $result = $date_arr[2] . " " . $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0];
        return $result;
    }
    /**
     * convert Y-m-d to d M Y (Indonesian)
     * contoh: 15 Jan 1990
     * @param type $date
     * @return string
     */
    public function formatDateINAShort($date)
    {
        // Date format in (Y-m-d)
        $result = "";
        $my_year = $this->list_month_short();
        $date_arr = explode("-",$date);
        $result = $date_arr[2] . " " . $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0];
        return $result;
    }
    /**
     * convert Y-m-d to d M Y (Indonesian)
     * contoh: 15 Jan 1990
     * @param type $date
     * @return string
     */
    public function formatMonthINAShort($month)
    {
        // Date format in (Y-m)
        $result = "";
        $my_year = $this->list_month_short();
        $month_arr = explode("-",$month);
        $result = $my_year[(int)$month_arr[1]]["name"]." ".$month_arr[0];
        return $result;
    }
     public function formatDateINAbulantanggal($date)
    {
        // Date format in (Y-m-d)
        $result = "";
        $my_year = $this->list_month();
        $date_arr = explode("-",$date);
        $result =  $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0];
        return $result;
    }

    public function formatDateINAtime($datetime)
    {
        // Date format in (Y-m-d)
        $result = "";
        $datetime_arr = explode(" ",$datetime);
        $my_year = $this->list_month();
        $date_arr = explode("-",$datetime_arr[0]);
        $result = $date_arr[2] . " " . $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0] . " " . $datetime_arr[1];;
        return $result;
    }
    
    public function formatDateINAtimeNew($datetime)
    {
        // Date format in (Y-m-d)
        $result = "";
        $datetime_arr = explode(" ",$datetime);
        $my_year = $this->list_month_short();
        $date_arr = explode("-",$datetime_arr[0]);
        $result = $date_arr[2] . " " . $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0] . " " . $datetime_arr[1];;
        return $result;
    }

    public function formatDMYtoYMD($date)
    {
        $result = "";
        //$my_year = $this->list_month();
        $date_arr = explode("-",$date);
        $result = $date_arr[2] . "-" . $date_arr[1] . "-" . $date_arr[0];
        return $result;
    }


    /* modified by @author Nama Rahman Fad | penambahan data (EHJ-1909) | 22-05-2014 
       hasil formatDMYtoYMD2 d/m/y 
    */

    public function formatDMYtoYMD2($date)
    {
        $result = "";
        //$my_year = $this->list_month();
        $date_arr = explode("-",$date);
        $result = $date_arr[2] . "/" . $date_arr[1] . "/" . $date_arr[0];
        return $result;
    }

    public function formatDMYtoYMDtime($datetime,$usetime=1)
    {
        $result = "";
        //$my_year = $this->list_month();
        $datetime_arr = explode(" ",$datetime);
        $date_arr = explode("-",$datetime_arr[0]);
        if($usetime==1) {
                $result = $date_arr[2] . "-" . $date_arr[1] . "-" . $date_arr[0] . " " . $datetime_arr[1];
        } else {
                $result = $date_arr[2] . "-" . $date_arr[1] . "-" . $date_arr[0] ;
        }
        return $result;
    }

    public function list_month(){
        for($i=1;$i<=12;$i++) {
                $result[$i]['id'] = "$i";
                if($i==1) $result[$i]['name'] = "Januari";
                if($i==2) $result[$i]['name'] = "Februari";
                if($i==3) $result[$i]['name'] = "Maret";
                if($i==4) $result[$i]['name'] = "April";
                if($i==5) $result[$i]['name'] = "Mei";
                if($i==6) $result[$i]['name'] = "Juni";
                if($i==7) $result[$i]['name'] = "Juli";
                if($i==8) $result[$i]['name'] = "Agustus";
                if($i==9) $result[$i]['name'] = "September";
                if($i==10) $result[$i]['name'] = "Oktober";
                if($i==11) $result[$i]['name'] = "November";
                if($i==12) $result[$i]['name'] = "Desember";
        }
        return $result;
    }

    public function list_month_short(){
        for($i=1;$i<=12;$i++) {
                $result[$i]['id'] = "$i";
                if($i==1) $result[$i]['name'] = "Jan";
                if($i==2) $result[$i]['name'] = "Feb";
                if($i==3) $result[$i]['name'] = "Mar";
                if($i==4) $result[$i]['name'] = "Apr";
                if($i==5) $result[$i]['name'] = "Mei";
                if($i==6) $result[$i]['name'] = "Jun";
                if($i==7) $result[$i]['name'] = "Jul";
                if($i==8) $result[$i]['name'] = "Agus";
                if($i==9) $result[$i]['name'] = "Sep";
                if($i==10) $result[$i]['name'] = "Okt";
                if($i==11) $result[$i]['name'] = "Nov";
                if($i==12) $result[$i]['name'] = "Des";
        }
        return $result;
    }
    
    public function list_month_en()
    {
        for($i=1;$i<=12;$i++) {
                $result[$i]['id'] = "$i";
                if($i==1) $result[$i]['name'] = "January";
                if($i==2) $result[$i]['name'] = "February";
                if($i==3) $result[$i]['name'] = "March";
                if($i==4) $result[$i]['name'] = "April";
                if($i==5) $result[$i]['name'] = "May";
                if($i==6) $result[$i]['name'] = "June";
                if($i==7) $result[$i]['name'] = "July";
                if($i==8) $result[$i]['name'] = "August";
                if($i==9) $result[$i]['name'] = "September";
                if($i==10) $result[$i]['name'] = "October";
                if($i==11) $result[$i]['name'] = "November";
                if($i==12) $result[$i]['name'] = "Desember";
        }
        return $result;
    }
    
    /**
     * penambahan dilakukan untuk digunakan di generator pada method (EHOSPITAL JK)
     * 1. public static function noPemesananBarang($instalasi_id)
     * @param type $name 
     * @return string 
     * @author jang wahyu
     */
    public function formatBulanIna($name)
    {
        $result = null;
        switch($name){
            case 'Jan' : $result = 'JAN'; break;
            case 'Feb' : $result = 'FEB'; break;
            case 'Mar' : $result = 'MAR'; break;
            case 'Apr' : $result = 'APR'; break;
            case 'May' : $result = 'MEI'; break;
            case 'Jun' : $result = 'JUN'; break;
            case 'Jul' : $result = 'JUL'; break;
            case 'Aug' : $result = 'AGS'; break;
            case 'Sep' : $result = 'SEP'; break;
            case 'Oct' : $result = 'OKT'; break;
            case 'Nov' : $result = 'NOV'; break;
            case 'Dec' : $result = 'DES'; break;
        }
        return $result;
    }

    public function formatBulanInaLengkap($name)
    {
        $result = null;
        switch($name){
            case 'Jan' : $result = 'Januari'; break;
            case 'Feb' : $result = 'Februari'; break;
            case 'Mar' : $result = 'Maret'; break;
            case 'Apr' : $result = 'April'; break;
            case 'May' : $result = 'Mei'; break;
            case 'Jun' : $result = 'Juni'; break;
            case 'Jul' : $result = 'Juli'; break;
            case 'Aug' : $result = 'Agustus'; break;
            case 'Sep' : $result = 'September'; break;
            case 'Oct' : $result = 'Oktober'; break;
            case 'Nov' : $result = 'November'; break;
            case 'Dec' : $result = 'Desember'; break;
            case '01' : $result = 'Januari'; break;
            case '02' : $result = 'Februari'; break;
            case '03' : $result = 'Maret'; break;
            case '04' : $result = 'April'; break;
            case '05' : $result = 'Mei'; break;
            case '06' : $result = 'Juni'; break;
            case '07' : $result = 'Juli'; break;
            case '08' : $result = 'Agustus'; break;
            case '09' : $result = 'September'; break;
            case '10' : $result = 'Oktober'; break;
            case '11' : $result = 'November'; break;
            case '12' : $result = 'Desember'; break;
        }
        return $result;
    }

    public function formatAngkaBulan($name)
    {
        $result = null;
        switch($name){
            case 'Januari' : $result = '01'; break;
            case 'Februari' : $result = '02'; break;
            case 'Maret' : $result = '03'; break;
            case 'April' : $result = '04'; break;
            case 'Mei' : $result = '05'; break;
            case 'Juni' : $result = '06'; break;
            case 'Juli' : $result = '07'; break;
            case 'Agustus' : $result = '08'; break;
            case 'September' : $result = '09'; break;
            case 'Oktober' : $result = '10'; break;
            case 'November' : $result = '11'; break;
			case 'Nopember' : $result = '11'; break;
            case 'Desember' : $result = '12'; break;
        }
        return $result;
    }

    /**
     * convert Y-m-d to d M Y (Indonesian)
     * contoh: 15 Januari 1990
     * @param type $date
     * @return string
     */
    public function formatDateTimeINA($date)
    {
        // Date format in (Y-m-d)
        $result = "";
        $my_year = $this->list_month();
        $tanggalSaja=trim(substr($date,0,-8)); //Menampilkan Tanggal Tanpa Jam
        $jamSaja=trim(substr($date,-8,8));  // menampilkan Jam Saja
        $date_arr = explode("-",$tanggalSaja);
        $result = $date_arr[2] . " " . $my_year[(int)$date_arr[1]]["name"] . " " . $date_arr[0];
        return $result;
    }
}
?>
