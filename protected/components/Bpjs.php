<?php

class Bpjs{
    var $mode	= 1;
    var $uid    = ""; //ex: 2603
    var $secret = ""; //ex: 1rs2hs3
    var $url  	= "";
    var $port  	= "";

    var $server = array();
    
    function __construct()
    {
        $this->uid = Yii::app()->user->getState('user_bpjs_name');
        $this->secret = Yii::app()->user->getState('secret_bpjs_key');
        $this->url = Yii::app()->user->getState('bpjs_host_tester');
        $this->url_produksi = Yii::app()->user->getState('bpjs_host_production');
        $this->port = Yii::app()->user->getState('bpjs_port');
        $this->servicename_bpjs = Yii::app()->user->getState('bpjs_service_name');
        $this->is_tester = Yii::app()->user->getState('is_tester');
//        $this->bpjs_server_lokal = Yii::app()->user->getState('is_bpjs_server_local');
        if($this->is_tester){
            $urlport = $this->url.":".$this->port."/".$this->servicename_bpjs;
        }else{
            $urlport = $this->url."/".$this->servicename_bpjs;
        }
        
        $this->server = array(
            'search_poli'                   => $urlport.'/referensi/poli',
            'fasilitas_kesehatan'           => $urlport.'/referensi/faskes',
            'search_diagnosa'               => $urlport.'/referensi/diagnosa',
            'search_procedure'              => $urlport.'/referensi/procedure',
            'search_kelas_rawat'            => $urlport.'/referensi/kelasrawat',
            'search_dokter'                 => $urlport.'/referensi/dokter',
            'search_spesialistik'           => $urlport.'/referensi/spesialistik',
            'search_ruangrawat'             => $urlport.'/referensi/ruangrawat',
            'search_carakeluar'             => $urlport.'/referensi/carakeluar',
            'search_pascapulang'            => $urlport.'/referensi/pascapulang',
            'search_kartu'                  => $urlport.'/Peserta/nokartu',
            'search_nik'                    => $urlport.'/Peserta/nik',
            'search_sep'                    => $urlport.'/SEP',
            'search_rujukan_no_rujukan_pcare'   => $urlport.'/Rujukan',
            'search_rujukan_no_rujukan_rs'      => $urlport.'/Rujukan/RS',
            'create_sep_new'                => $urlport.'/SEP/insert',
            'update_sep_new'                => $urlport.'/SEP/Update',
            'update_sep_pulang'                => $urlport.'/SEP/updtglplg',
            'delete_transaksi_sep'                => $urlport.'/SEP/Delete',
            'create_rujukan'                => $urlport.'/Rujukan/insert',
            'update_rujukan'                => $urlport.'/Rujukan/update',
            'delete_rujukan'                => $urlport.'/Rujukan/delete',
            'search_rujukan_norujukan_pcare'        => $urlport.'/Rujukan',
            'search_rujukan_norujukan_rs'           => $urlport.'/Rujukan/RS',
            'search_rujukan_peserta_pcare'        => $urlport.'/Rujukan/Peserta',
            'search_rujukan_peserta_rs'           => $urlport.'/Rujukan/RS/Peserta/',
            'pengajuan_approval'           => $urlport.'/Sep/pengajuanSEP',
            'approval_sep'           => $urlport.'/Sep/aprovalSEP',
            'monitoring_kunjungan'          => $urlport.'/Monitoring/Kunjungan',
            'monitoring_klaim'              => $urlport.'/Monitoring/Klaim',
        );
    }
    
    function output($content)
    {
        echo $content;
    }

    private function HashBPJS($args = '')
    {
        $uid = $this->uid;
        date_default_timezone_set('UTC');
        $timestmp = strval(time()-strtotime('1970-01-01 00:00:00'));
        $str = $uid."&".$timestmp;
        $secret = $this->secret;
        $hasher = base64_encode(hash_hmac('sha256', utf8_encode($str), utf8_encode($secret), TRUE)); //signature;
        return array($uid, $timestmp, $hasher);
    }

    private function request($url, $hashsignature, $uid, $timestmp, $method='', $myvars='')
    {
        $session = curl_init($url);
        $arrheader =  array(
                'X-cons-id: '.$uid,
                'X-timestamp: '.$timestmp,
                'X-signature: '.$hashsignature,
                'Accept: application/json',
                'Content-Type: Application/x-www-form-urlencoded',
                );

        curl_setopt($session, CURLOPT_URL, $url);
        curl_setopt($session, CURLOPT_HTTPHEADER, $arrheader);
        curl_setopt($session, CURLOPT_VERBOSE, true);

        switch($method){
                case 'POST':
                        curl_setopt($session, CURLOPT_POST, true);
                        curl_setopt($session, CURLOPT_POSTFIELDS, $myvars);
                        break;
                case 'PUT':
                        curl_setopt($session, CURLOPT_CUSTOMREQUEST, "PUT");
                        curl_setopt($session, CURLOPT_POSTFIELDS, $myvars);
                        break;
                case 'DELETE':
                        curl_setopt($session, CURLOPT_CUSTOMREQUEST, "DELETE");
                        curl_setopt($session, CURLOPT_POSTFIELDS, $myvars);
                        break;
        }

        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($session);
        curl_close($session);
        return $response;
    }

    function identity_magic()
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
    }

    function help()
    {
        $url = $this->url.'/help';
        $session = curl_init($url);
        curl_setopt($session, CURLOPT_URL, $url);
        curl_setopt($session, CURLOPT_VERBOSE, true);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, TRUE);
        $response = curl_exec($session);
        return $response;
    }
    
    function search_kartu($query)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['search_kartu'].'/'.$query.'/tglSEP/'. date('Y-m-d');
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);
    }

    function search_sep($query)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['search_sep'].'/'.$query;
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);
    }

    function search_nik($query)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['search_nik'].'/'.$query.'/tglSEP/'. date('Y-m-d');
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);	
    }

    function search_rujukan_no_rujukan($query)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['search_rujukan_no_rujukan_pcare']."/".$query;
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);	
    }
    
    function search_rujukan_no_rujukan_rs($query)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['search_rujukan_no_rujukan_rs']."/".$query;
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);	
    }
    
    function search_rujukan_norujukan($query,$jenisfaskes)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            if($jenisfaskes==1){
                $completeUrl = $this->server['search_rujukan_norujukan_pcare']."/".$query;
            }else{
                $completeUrl = $this->server['search_rujukan_norujukan_rs']."/".$query;
            }
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);	
    }
    
    function search_rujukan_peserta($query,$jenisfaskes)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            if($jenisfaskes==1){
                $completeUrl = $this->server['search_rujukan_peserta_pcare']."/".$query;
            }else{
                $completeUrl = $this->server['search_rujukan_peserta_rs']."/".$query;
            }
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);	
    }
    
    function search_monitoring_kunjungan($query1,$query2)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['monitoring_kunjungan']."/Tanggal/".$query1."/JnsPelayanan/".$query2;
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);
    }

    function search_monitoring_klaim($query1,$query2,$query3)
    {
            list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
            $completeUrl = $this->server['monitoring_klaim']."/Tanggal/".$query1."/JnsPelayanan/".$query2."/Status/".$query3;
            return $this->request($completeUrl, $hashsignature, $uid, $timestmp);
    }
    
    function create_sep_new($nokartu,$tglsep,$ppkpelayanan,$jnspelayanan,$klsrawat,$nomr,$asalrujukan,$tglrujukan,$norujukan,$ppkrujukan,$catatan,$diagawal,$politujuan,$eksekutif,$cob,$lakalantas,$penjamin,$lokasilakalantas,$notlp,$user){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noKartu":"'.$nokartu.'",
                    "tglSep":"'.$tglsep.'",
                    "ppkPelayanan":"'.$ppkpelayanan.'",
                    "jnsPelayanan":"'.$jnspelayanan.'",
                    "klsRawat":"'.$klsrawat.'",
                    "noMR":"'.$nomr.'",
                    "rujukan": {
                        "asalRujukan":"'.$asalrujukan.'",
                        "tglRujukan":"'.$tglrujukan.'",
                        "noRujukan":"'.$norujukan.'",
                        "ppkRujukan":"'.$ppkrujukan.'",
                    },
                    "catatan":"'.$catatan.'",
                    "diagAwal":"'.$diagawal.'",
                    "poli": {
                        "tujuan":"'.$politujuan.'",
                        "eksekutif":"'.$eksekutif.'",
                    },
                    "cob": {
                        "cob":"'.$cob.'",
                    },
                    "jaminan": {
                        "lakaLantas":"'.$lakalantas.'",
                        "penjamin":"'.$penjamin.'",
                        "lokasiLaka":"'.$lokasilakalantas.'",
                    },
                    "noTelp":"'.$notlp.'",
                    "user":"'.$user.'",
                }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['create_sep_new'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'POST', $query);
    }
    
    function pengajuan_approval($nokartu,$tglsep,$ppkpelayanan,$jnspelayanan,$klsrawat,$nomr,$asalrujukan,$tglrujukan,$norujukan,$ppkrujukan,$catatan,$diagawal,$politujuan,$eksekutif,$cob,$lakalantas,$penjamin,$lokasilakalantas,$notlp,$user){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noKartu":"'.$nokartu.'",
                    "tglSep":"'.$tglsep.'",
                    "ppkPelayanan":"'.$ppkpelayanan.'",
                    "jnsPelayanan":"'.$jnspelayanan.'",
                    "keterangan":"'.$catatan.'",
                    "user":"'.$user.'",
                }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['pengajuan_approval'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'POST', $query);
    }
    
    function approval_sep($noKartu,$tglSep,$jnsPelayanan,$catatan,$user){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noKartu":"'.$noKartu.'",
                    "tglSep":"'.$tglSep.'",
                    "jnsPelayanan":"'.$jnsPelayanan.'",
                    "keterangan":"'.$catatan.'",
                    "user":"'.$user.'",
                }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['approval_sep'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'POST', $query);
    }
    
    function update_sep_new($noSep,$nokartu,$tglsep,$ppkpelayanan,$jnspelayanan,$klsrawat,$nomr,$asalrujukan,$tglrujukan,$norujukan,$ppkrujukan,$catatan,$diagawal,$politujuan,$eksekutif,$cob,$lakalantas,$penjamin,$lokasilakalantas,$notlp,$user){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noSep":"'.$noSep.'",
                    "klsRawat":"'.$klsrawat.'",
                    "noMR":"'.$nomr.'",
                    "rujukan": {
                        "asalRujukan":"'.$asalrujukan.'",
                        "tglRujukan":"'.$tglrujukan.'",
                        "noRujukan":"'.$norujukan.'",
                        "ppkRujukan":"'.$ppkrujukan.'",
                    },
                    "catatan":"'.$catatan.'",
                    "diagAwal":"'.$diagawal.'",
                    "poli": {
                        "eksekutif":"'.$eksekutif.'",
                    },
                    "cob": {
                        "cob":"'.$cob.'",
                    },
                    "jaminan": {
                        "lakaLantas":"'.$lakalantas.'",
                        "penjamin":"'.$penjamin.'",
                        "lokasiLaka":"'.$lokasilakalantas.'",
                    },
                    "noTelp":"'.$notlp.'",
                    "user":"'.$user.'",
                }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['update_sep_new'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'PUT', $query);
    }
    
    function update_sep_pulang($noSep,$tglPulang,$user){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noSep":"'.$noSep.'",
                    "tglPulang":"'.$tglPulang.'",
                    "user":"'.$user.'",
                }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['update_sep_pulang'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'PUT', $query);
    }
    
    function delete_transaksi_sep($nosep,$nama){
        $query = '{
            "request":
             {
            "t_sep":
                {
                    "noSep":"'.$nosep.'",
                    "user":"'.$nama.'",
                }
            }
        }';
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['delete_transaksi_sep'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'DELETE', $query);		
    }
    
    function insert_rujukan_bpjs($noSep,$tglRujukan,$ppkDirujuk,$jnsPelayanan,$catatan,$diagRujukan,$tipeRujukan,$poliRujukan,$user){
        $query = '{
            "request":
             {
                "t_rujukan": {
                    "noSep": "'.$noSep.'",
                    "tglRujukan": "'.$tglRujukan.'",
                    "ppkDirujuk": "'.$ppkDirujuk.'",
                    "jnsPelayanan": "'.$jnsPelayanan.'",
                    "catatan": "'.$catatan.'",
                    "diagRujukan": "'.$diagRujukan.'",
                    "tipeRujukan": "'.$tipeRujukan.'",
                    "poliRujukan": "'.$poliRujukan.'",
                    "user": "'.$user.'"
                 }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['create_rujukan'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'POST', $query);
    }
    
    function update_rujukan_bpjs($noRujukan,$noSep,$tglRujukan,$ppkDirujuk,$jnsPelayanan,$catatan,$diagRujukan,$tipeRujukan,$poliRujukan,$user){
        $query = '{
            "request":
             {
                "t_rujukan": {
                    "noRujukan": "'.$noRujukan.'",
                    "ppkDirujuk": "'.$ppkDirujuk.'",
                    "tipe": "'.$tipeRujukan.'",
                    "jnsPelayanan": "'.$jnsPelayanan.'",
                    "catatan": "'.$catatan.'",
                    "diagRujukan": "'.$diagRujukan.'",
                    "tipeRujukan": "'.$tipeRujukan.'",
                    "poliRujukan": "'.$poliRujukan.'",
                    "user": "'.$user.'"
                 }
            }
        }';

        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['update_rujukan'];

        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'PUT', $query);
    }
    
    function delete_rujukan($noRujukan,$nama){
        $query = '{
            "request":
             {
            "t_rujukan":
                {
                    "noRujukan":"'.$noRujukan.'",
                    "user":"'.$nama.'",
                }
            }
        }';
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['delete_rujukan'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp, 'DELETE', $query);		
    }
    
    function search_poli($query){
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_poli'].'/'.$query;
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);
    }

    function fasilitas_kesehatan($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['fasilitas_kesehatan'].'/'.$query;
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_doagnosa($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_diagnosa'].'/'.$query;
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_procedure($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_procedure'].'/'.$query;
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_kelas_rawat($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_kelas_rawat'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_dokter($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_dokter'].'/'.$query;
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_spesialistik($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_spesialistik'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_ruangrawat($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_ruangrawat'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_carakeluar($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_carakeluar'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }

    function search_pascapulang($query, $start, $limit)
    {
        list($uid, $timestmp, $hashsignature) = $this->HashBPJS();
        $completeUrl = $this->server['search_pascapulang'];
        return $this->request($completeUrl, $hashsignature, $uid, $timestmp);		
    }
}

