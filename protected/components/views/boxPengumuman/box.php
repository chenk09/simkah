<div align="center" style="font-weight:bold">.:: <a href="javascript:void(0)" onclick="viewPengumuman();"><i class="icon-file"></i> Kotak Informasi</a> ::.</div>
<hr>
<div id="ticker">
<?php
foreach ($this->pengumumans as $i=>$pengumuman)
{
    //echo Yii::app()->createUrl('pengumuman/dialogView',array('id'=>$pengumuman->pengumuman_id));
    echo '<div style="color:#3a87ad;">';
    echo '<b><a href="javascript:void(0);" onclick="viewPengumuman('.$pengumuman->pengumuman_id.');"><i class="icon-tag"></i> '.$pengumuman->judul.'</a></b>';
    echo '<br/><span class="author-pengumuman">'.$pengumuman->userCreate->nama_pemakai.'</span> - <span class="label label-success">'. $pengumuman->create_time .'</span><br/>';
    echo '<br>';
    echo ''.$pengumuman->isi.'';
    echo '</div>';
}
?>
</div>

<script type="text/javascript">
function tick()
{
	var cel_height = $('.dashboard').height() - 100;
	$('#ticker').css('height', cel_height + 'px');
	$('#ticker div').css('height', cel_height + 'px');
	
	$('#ticker div:first').slideUp( function () { $(this).appendTo($('#ticker')).slideDown(); });
}
setInterval(function(){ tick () }, 10000);

//function tick(){
//	$('#ticker li:first').animate({'opacity':0}, 200, function () { $(this).appendTo($('#ticker')).css('opacity', 1); });
//}
//setInterval(function(){ tick () }, 4000); javascript:chat(\''.$user->nama_pemakai.'\');

function viewPengumuman(id)
{
    $('#pengumumandialog').dialog('open');
    if(!id)
        $('#frameinformasi').attr('src', '<?php echo Yii::app()->createUrl('pengumuman/dialogView'); ?>' );
    else
        $('#frameinformasi').attr('src', '<?php echo Yii::app()->createUrl('pengumuman/dialogView'); ?>&id='+id );
}

function clearFrameSrc()
{
    $('#frameinformasi').attr('src', '');
}
</script>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'pengumumandialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=> 'Informasi',
        'autoOpen'=>false,
        'width'=>720,
        'height'=>475,
        'close'=>'js:function(){ clearFrameSrc(); }',
        'modal'=>true,
    ),
));

    echo '<iframe id="frameinformasi" src="" height="100%" width="100%"></iframe> ';

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>