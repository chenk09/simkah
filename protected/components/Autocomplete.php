<?php

Yii::import('MyJuiAutoComplete');
class Autocomplete extends CJuiAutoComplete
{
    /**
    * @var mixed the entries that the autocomplete should choose from. This can be
    * <ul>
    * <li>an Array with local data</li>
    * <li>a String, specifying a URL that returns JSON data as the entries.</li>
    * <li>a javascript callback. Please make sure you prefix the callback name with "js:" in this case.</li>
    * </ul>
    */
    public $source = array();
    /**
     * @var mixed the URL that will return JSON data as the autocomplete items.
     * CHtml::normalizeUrl() will be applied to this property to convert the property
     * into a proper URL. When this property is set, the {@link source} property will be ignored.
     */
    public $sourceUrl;

    /**
     * Run this widget.
     * This method registers necessary javascript and renders the needed HTML code.
     */
    public function run()
    {
            list($name,$id)=$this->resolveNameID();

            if(isset($this->htmlOptions['id']))
                    $id=$this->htmlOptions['id'];
            else
                    $this->htmlOptions['id']=$id;

            if(isset($this->htmlOptions['name']))
                    $name=$this->htmlOptions['name'];

            if($this->hasModel())
                    echo CHtml::activeTextField($this->model,$this->attribute,$this->htmlOptions);
            else
                    echo CHtml::textField($name,$this->value,$this->htmlOptions);

            if($this->sourceUrl!==null)
                    $this->options['source']=CHtml::normalizeUrl($this->sourceUrl);
            else
                    $this->options['source']=$this->source;

//$functionSearch =<<< EOD
//function(event, ui) {
//    evt = (evt) ? evt : event;
//    var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
//    alert('oii');
//    if(charCode == 13) {
//        return true;
//    } else {
//        return false;
//    }
//}
//EOD;
            
// custom minLength
$functionSearch = "
function() {
    var term = extractLast( this.value );
    if ( term.length < 3 ) {
            return false;
    } else {
            return true;
    }
}
";
//"autocompletesearch", function(event, ui) {
//  ...
//}
// don't navigate away from the field on tab when selecting an item
$bindKeydown = <<< BIND
.bind( "keydown", function( event ) {
    if ( event.keyCode === $.ui.keyCode.ENTER ) {
            alert(event.keyCode);return false;
            //$(this).autocomplete("search");return false;
    } 
    if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).data( "autocomplete" ).menu.active ) {
            event.preventDefault();
    }
});
BIND;

$js2 = <<< JS
$(document).ready(function(){
    $('#{$id}').autocomplete("disable");
    $('#{$id}').keypress(function(e){
    if(e.keyCode == 13)
    {
        $('#{$id}').autocomplete("enable");
        $('#{$id}').autocomplete("search" ,  $('#{$id}').val());
    }
    else
    {
        $('#{$id}').autocomplete("disable");
    }
    });
});
JS;
Yii::app()->clientScript->registerScript('register',$js2, CClientScript::POS_HEAD);
//            $functionSearch = str_replace("\\", "", $functionSearch);
//            $functionSearch = str_replace("\n", "", $functionSearch);
//            $functionSearch = str_replace("\r", "", $functionSearch);
//            $functionSearch = str_replace("\t", "", $functionSearch);
//            $this->options['search'] = $functionSearch;
//            
//            $options=CJavaScript::encode($this->options);
//
//            $js = "jQuery('#{$id}')".$bindKeydown;
//            $js .= "jQuery('#{$id}').autocomplete($options);";

            $cs = Yii::app()->getClientScript();
            $cs->registerScript(__CLASS__.'#'.$id, $js);
    }
}
?>
