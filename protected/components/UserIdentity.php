<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
                private $_id;
                public $ruangan;
                const ERROR_RUANGAN_INVALID = 'Kesalahan pada Ruangan';


        public function __construct($username,$password,$ruangan)
	{
		$this->username=$username;
		$this->password=$password;
                $this->ruangan=$ruangan;
	}
        
	public function authenticate()
	{
                $user = LoginpemakaiK::model()->findByAttributes(
                    array(
                        'nama_pemakai' => $this->username,
                        'loginpemakai_aktif' =>TRUE
                    )
                );
                if($user === null)
                {
                    $this->errorCode = self::ERROR_USERNAME_INVALID;
                    
                }else if($user->katakunci_pemakai !== $user->encrypt($this->password))
                {
                    $this->errorCode = self::ERROR_PASSWORD_INVALID;
                    
                }else{
                    $ruangan_user = RuanganpemakaiK::model()->findByAttributes(
                        array(
                            'loginpemakai_id'=>$user->loginpemakai_id,
                            'ruangan_id'=>$this->ruangan
                        )
                    );
                    if($ruangan_user === null)
                    {
                        $this->errorCode=self::ERROR_RUANGAN_INVALID;
                    }else{
                        $this->_id = $user->loginpemakai_id;
                        
                        if($user->lastlogin === null)
                        {
                            $lastLogin =date('Y-m-d H:i:s',  time());
                        } else {
                             $lastLogin = strtotime($user->lastlogin);
                        }
                        $this->setState('lastLoginTime', $lastLogin);
                        $this->setState('nama_pegawai', $user->pegawai->nama_pegawai);
                        $this->setState('pegawai_id', $user->pegawai_id);
                        
                        /*
                        * Daftar user State
                        */ 
                        
                        //session wilayah berdasarkan data profil rumah sakit
                        $profil = ProfilrumahsakitM::model()->findByPk(
                            Params::DEFAULT_PROFIL_RUMAH_SAKIT
                        );
                        $kelasPelayananRuangan = CHtml::listData(
                            KamarruanganM::model()->findAllByAttributes(
                                array(
                                    'ruangan_id'=>$_POST['LoginForm']['ruangan']
                                )
                            ), 
                            'kelaspelayanan_id', 'kelaspelayanan_id'
                        );
                        $idPropinsi = $profil->propinsi_id;    
                        $idKabupaten = $profil->kabupaten_id; 
                        $idKecamatan = $profil->kecamatan_id; 
                        $idKelurahan = $profil->kelurahan_id; 
                        
                        $propinsiNama = $profil->propinsi->propinsi_namalainnya;
                        $kabupatenNama = $profil->kabupaten->kabupaten_namalainnya;
                        $kabupatenNamaKab = $profil->kabupaten->kabupaten_nama;
                        $kecamatanNama = $profil->kecamatan->kecamatan_namalainnya;
                        $kelurahanNama = $profil->kelurahan->kelurahan_namalainnya;
                        
                        //Menentukan nilai untuk session shift_id
                          $now = date('H:i:s');
                        $sql = "SELECT * FROM shift_m where '$now' < shift_jamakhir order by shift_jamakhir";
                        $jalankanSql = Yii::app()->db->createCommand($sql)->queryRow();
                        
                        if(empty($jalankanSql['shift_id']))
                        {
                            $sql = "SELECT * FROM shift_m where '$now' > shift_jamawal order by shift_jamakhir DESC";
                            $jalankanSql = Yii::app()->db->createCommand($sql)->queryRow();
                        }
                        
                        $this->setState('ukuran_kertas', Params::defaultKertas('ukuran'));
                        $this->setState('posisi_kertas', Params::defaultKertas('posisi'));
                        
                        $this->setState('ruangan_id', $_POST['LoginForm']['ruangan']);
                        if (!empty($idKelasPelayanan))
                        $this->setState('kelaspelayanan_id', $idKelaspelayanan);
                        $this->setState('kelaspelayananruangan',$kelasPelayananRuangan);
                        $this->setState('instalasi_id', $_POST['LoginForm']['instalasi']);
                        $this->setState('shift_id', $jalankanSql['shift_id']);
                        $this->setState('propinsi_id', $idPropinsi);
                        $this->setState('kabupaten_id', $idKabupaten);
                        $this->setState('kecamatan_id', $idKecamatan);
                        $this->setState('kelurahan_id', $idKelurahan);
                        
                        $this->setState('propinsi_namalainnya', $propinsiNama);
                        $this->setState('kabupaten_namalainnya', $kabupatenNama);
                        $this->setState('kabupaten_nama', $kabupatenNamaKab);
                        $this->setState('kecamatan_namalainnya', $kecamatanNama);
                        $this->setState('kelurahan_namalainnya', $kelurahanNama);
                        $this->setState('loginpemakai_id',$user->loginpemakai_id);

                        $sql = "SELECT * FROM config_app WHERE endda >= CURRENT_DATE";
                        $confApp = Yii::app()->db->createCommand($sql)->queryAll();
                        foreach ($confApp as $index => $item)
                        {
                            $this->setState($item['config_type'], $item['config_value']);
                        }

                        $this->errorCode = self::ERROR_NONE;
                        $konfig = KonfigsystemK::model()->find();
                        $attributeKonfig = $konfig->getAttributes();
                        foreach($attributeKonfig as $attribute=>$value)
                        {
                            $this->setState($attribute, $value);
                        }

                        $konfigFarmasi = KonfigfarmasiK::model()->find();
                        $attributeKonfigFar = $konfigFarmasi->getAttributes();
                        foreach($attributeKonfigFar as $attribute=>$value)
                        {
                            $this->setState($attribute, $value);
                        }

                        $dataRs = ProfilrumahsakitM::model()->findByPk(
                            Params::DEFAULT_PROFIL_RUMAH_SAKIT
                        );
                        $attributeRs = $dataRs->getAttributes();
                        foreach($attributeRs as $attribute=>$value)
                        {
                            $this->setState($attribute, $value);
                        }

                        $modulUser = ModuluserK::model()->findAllByAttributes(
                            array(
                                'loginpemakai_id'=>$user->loginpemakai_id
                            )
                        );
                        foreach($modulUser as $i=>$modul)
                        {
                            $usersModul[$modul->modul_id] = $modul->modul_id;
                        }
                        $this->setState('usersModul', $usersModul);
                        
                        $attributes = array('isclosing'=>false);
                        $modPeriode = RekperiodM::model()->findAllByAttributes($attributes);
                        
                        $periode = array();
                        foreach($modPeriode as $x=>$value)
                        {
                            $periode[$x] = $value->rekperiod_id;
                        }
                        $this->setState('periodeID', $periode);
                        Yii::app()->session['periodeID'] = $periode;
                        
                        $dataBpjs = KonfigBpjsInacbgK::model()->findByPk(1);
                        if($dataBpjs)
                        {
                            $attributeBpjs = $dataBpjs->getAttributes();
                            foreach($attributeBpjs as $attribute=>$value)
                            {
                                $this->setState($attribute, $value);
                            }
                        }
                    }
                }
                return!$this->errorCode;
	} 
        
        public function getId() {
             return $this->_id;
        }  
        
}