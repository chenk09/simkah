<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class MyDashBoard extends CWidget
{
    public $moduls = array();
    public $kelompokModuls = array();
    public $kategoriModuls = array();
    
    public function init() {
        parent::init();
        $this->kelompokModuls = KelompokmodulK::model()->findAll('kelompokmodul_aktif = true');
        $this->moduls = ModulK::model()->with('kelompokmodul')->findAllByAttributes(array('modul_aktif'=>true),array('order'=>'modul_kategori, t.kelompokmodul_id, modul_urutan'));
        $this->kategoriModuls = KategoriModul::items();
    }
    
    public function run() {
        //parent::run();
        $tabs = array();
        
//        foreach ($this->kategoriModuls as $katValue=>$katName){
//            echo "<div class='dashboard'>";
//            foreach ($this->kelompokModuls as $j=>$kelompokModul) {
//                $tabs[$j]['label'] = $kelompokModul->kelompokmodul_nama;
//                $tabs[$j]['content'] = "<div class='dashboard'>";
//
//                echo "<div class='block'>";
//                echo "<div><h6>".$kelompokModul->kelompokmodul_nama."</h6></div>";
//                foreach ($this->moduls as $i=>$modul){
//                    if($kelompokModul->kelompokmodul_id == $modul->kelompokmodul_id && $katValue == $modul->modul_kategori) {
//                        echo "<a href=".Yii::app()->createUrl($modul->modul_nama)." class='shortcut'>";
//                        echo "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>";
//                        echo "$modul->modul_namalainnya</a>";
//
//                        $content = "<a href=".Yii::app()->createUrl($modul->url_modul,array('modulId'=>$modul->modul_id))." class='shortcut'>".
//                                   "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>".
//                                   "$modul->modul_namalainnya</a>";
//                        $tabs[$j]['content'] .= $content;
//                    }
//                }
//                echo "</div>";
//                $tabs[$j]['content'] .= "</div>";
//            }
//            echo "</div>";
//        }
        //echo "<pre>".print_r($_SESSION,1)."</pre>";
        $userModul = Yii::app()->user->getState('usersModul');
        foreach ($this->kategoriModuls as $katValue=>$katName){
            echo "<div class='dashboard'>";
            echo "<div class='well'>";
			//FRONT OFFICE icon-folder-close icon-list-alt
			if($katValue == "FRONT OFFICE")
			{
				echo "<div><i class='icon-th-list'></i> ". $katValue ."</div>";
			}else{
				echo "<div><i class='icon-folder-close'></i> ". $katValue ."</div>";
			}
            echo "<hr>";
            foreach ($this->kelompokModuls as $j=>$kelompokModul) {
                $tabs[$j]['label'] = $kelompokModul->kelompokmodul_nama;
                $tabs[$j]['content'] = "<div class='dashboard'>";
                
                foreach ($this->moduls as $i=>$modul){
                    if(!empty($userModul[$modul->modul_id])){
                        if($katValue == $modul->modul_kategori && $kelompokModul->kelompokmodul_id == $modul->kelompokmodul_id) {
                            echo "<a href=".Yii::app()->createUrl($modul->url_modul,array('modulId'=>$modul->modul_id))." class='shortcut'>";
                            echo "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>";
                            echo "$modul->modul_namalainnya</a>";

                            $content = "<a href=".Yii::app()->createUrl($modul->url_modul,array('modulId'=>$modul->modul_id))." class='shortcut'>".
                                       "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>".
                                       "$modul->modul_namalainnya</a>";
                            $tabs[$j]['content'] .= $content;
                        }
                    } else {
                        if($katValue == $modul->modul_kategori && $kelompokModul->kelompokmodul_id == $modul->kelompokmodul_id) {
                            echo "<a href='javascript:alert(\"Anda tidak bisa mengakses Modul : ".$modul->modul_namalainnya."\");' class='shortcut disable'>";
                            echo "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>";
                            echo "$modul->modul_namalainnya</a>";

                            $content = "<a href='javascript:alert(\"Anda tidak bisa mengakses Modul : ".$modul->modul_namalainnya."\");' class='shortcut disable'>".
                                       "<img height='48' width='48' alt='' src='".Params::urlIconModulDirectory().$modul->icon_modul."'>".
                                       "$modul->modul_namalainnya</a>";
                            $tabs[$j]['content'] .= $content;
                        }
                    }
                }
                $tabs[$j]['content'] .= "</div>";
            }
            echo "</div>";
            echo "</div>";
        }
      
        /*
        $this->widget('bootstrap.widgets.BootTabbable', array(
            'type'=>'tabs',
            'placement'=>'left', // 'above', 'right', 'below' or 'left'
            'tabs'=>$tabs,
        ));
         */
    }
}
?>
