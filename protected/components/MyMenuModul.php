<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class MyMenuModul 
{
    public static function getMenuModul($menuModuls)
    {
        $menus = array();
        $result = array();
        $tempKelId = '';
        $j = 0;
        if(!empty($menuModuls)) {
            foreach ($menuModuls as $i=>$menuModul) {
                if($tempKelId == $menuModul->kelmenu_id)
                    $j++;
                else 
                    $j = 0;
                //$menus[$menuModul->kelmenu_id]['kelompokmenu_id'] = $menuModul->kelmenu_id;
                //$menus[$menuModul->kelmenu_id]['url'] = $menuModul->modulk->modul_nama;
                $menus[$menuModul->kelmenu_id]['url'] = '';
                $menus[$menuModul->kelmenu_id]['label'] = (isset($menuModul->kelompokmenu->kelmenu_nama) ? $menuModul->kelompokmenu->kelmenu_nama : "") .'<icon class="icon-accordion pull-right"></icon>';
                 //$menus[$menuModul->kelmenu_id]['modul_id'] = $menuModul->modul_id;
                $menus[$menuModul->kelmenu_id][$j]['url'] = array('route'=>$menuModul->menu_url,'params'=>array('modulId'=>$menuModul->modul_id));
                $menus[$menuModul->kelmenu_id][$j]['label'] = '<icon class="icon-min"></icon> '.$menuModul->menu_nama;
                $tempKelId = $menuModul->kelmenu_id;
            }

            foreach ($menus as $kelMenuId=>$key) {
                $result[] = $key;
            }
        } else {
            $result['label'] = 'kosong';
            $result['url'] = '';
        }
        
        return $result;
    }
}
?>
