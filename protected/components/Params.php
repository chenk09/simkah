<?php

Class Params
{
//    const DEFAULT_PROFIL_RUMAH_SAKIT = 1;          
    const DEFAULT_SESSION = 150;                      //id default untuk propinsi
    
    public static function defaultKertas($params = null)
    {
        $kertas = array(
            'ukuran' => 'A4',
            'posisi' => 'P',
        );
        if(is_null($params))
        {
            return $kertas;
        }else{
            return $kertas[$params];
        }
    }
    
    public static function kelasPelayanan($params = null)
    {
        $kelas = array(
            'rawat_jalan' => 4,
            'rawat_darurat' => 4,
            'rawat_inap' => 4,
            'masuk_penunjang' => 2,
            'tanapa_kelas' => 5,
            'bedah_sentral' => 5,
        );
        if(is_null($params))
        {
            return $kelas;
        }else{
            return $kelas[$params];
        }
    }    
    const TOOLTIP_PLACEMENT = 'bottom';                 //nilai konstanta tooltip placement untuk bootstrap tooltip
    const TOOLTIP_SELECTOR = 'a[rel="tooltip"],button[rel="tooltip"],input[rel="tooltip"]';        //nilai konstanta tooltip selector untuk bootstrap tooltip
    
    const DATE_TIME_FORMAT = 'dd M yy';          //format default untuk datetime
    const DATE_FORMAT = 'dd MM yy';                     //format default untuk date
    const DATE_FORMAT_MEDIUM = 'dd M yy';               //format default untuk date medium
    const TIME_FORMAT = 'H:i:s';                        //format default untuk time
    const MONTH_FORMAT_MEDIUM = 'M yy';               //format default untuk month medium

    const DEFAULT_PROFIL_RUMAH_SAKIT = 1;          
    const DEFAULT_BADAN_RUMAH_SAKIT ='BADAN LAYANAN UMUM DAERAH';          //Defaul Untuk Header Print Profil Rumah Sakit
    const DEFAULT_WARGANEGARA = 'INDONESIA';
    const DEFAULT_KOP_LAB = 'LABORATORIUM KLINIK UTAMA';
    
    const DEFAULT_AGAMA = 'ISLAM';
    const DEFAULT_JENIS_IDENTITAS = 'KTP';
	
	const KATEGORI_PEGAWAI_KHUSUS = 'KHUSUS'; // default lookup untuk kategori pegawai khusus
    
    // const DEFAULT_PROPINSI_ID = 30;                      //id default untuk propinsi
    // const DEFAULT_KABUPATEN_ID = 168;                     //id default untuk kabupaten
    // const DEFAULT_KECAMATAN_ID = 595;                     //id default untuk kecamatan
    // const DEFAULT_KELURAHAN_ID = 1;                     //id default untuk kelurahan    
    // const SURAT_KABUPATEN = 'Tasikmalaya';      // default nama tempat untuk penulisan surat/laporan/kwitansi    
    // const DEFAULT_KABUPATEN_RUMAH_SAKIT = 'PEMERINTAH KABUPATEN CIREBON'; //Defaul Untuk Header Print Profil Rumah Sakit
    const DEFAULT_JENIS_PENGELUARAN_PENGGAJIAN_ID = 2;   
    const DEFAULT_NAMA_JENIS_PENGELUARAN_PENGGAJIAN = "Gaji Karyawan";
    
    const DEFAULT_CREATE_LOGIN_PEMAKAI = 1;
    const DEFAULT_CREATA_RUANGAN = 1;
    const DEFAULT_ETIKET = "PUTIH-PENGGUNAAN DALAM";
    
    const INSTALASI_ID_RM = 1;
    const INSTALASI_ID_RJ = 2;
    const INSTALASI_ID_RD = 3;
    const INSTALASI_ID_RI = 4;
    const INSTALASI_ID_LAB = 5;           //id default untuk instalasi lab
    const INSTALASI_ID_RAD = 6;           //id default untuk instalasi rad
    const INSTALASI_ID_REHAB = 8;           //id default untuk instalasi rehab
    const INSTALASI_ID_IBS = 7;           //id default untuk instalasi bedah sentral
    const INSTALASI_ID_GIZI = 10;          //id default untuk instalasi gizi
    const INSTALASI_ID_JZ = 17;          //id default untuk instalasi kamar jenazah
    const INSTALASI_ID_FARMASI = 9;          //id default untuk instalasi farmasi
    const INSTALASI_ID_NON_INSTALASI = 14;          //id default untuk non instalasi 
    const INSTALASI_ID_PEL_KHUSUS = 19;          //id default untuk pelayanan khusus
    const INSTALASI_ID_RAWAT_INTENSIF = 20;          //id default untuk pelayanan khusus
    const INSTALASI_ID_STERILISASI_SENTRAL = 21;          //id default untuk pelayanan khusus
    const INSTALASI_ID_HEMODIALISA = 15;          //id default untuk pelayanan khusus
    const INSTALASI_ID_SANITASI = 24;          //"Sanitasi, Laundry Dan Pulasara Jenazah"
    const INSTALASI_ID_KEAMANAN_TRANSPORTASI = 25;          //"Keamanan Dan Transportasi"
    
    const KOMPONENTARIF_ID_TOTAL = 6;           //id komponentarif_id untuk total tarif
    const KOMPONENTARIF_ID_RS = 1;           //id komponentarif_id untuk Jasa RS
    const KOMPONENTARIF_ID_MEDIS = 5;           //id komponentarif_id untuk Jasa MEdis
    const KOMPONENTARIF_ID_PARAMEDIS = 2;           //id komponentarif_id untuk Paramedis
    const KOMPONENTARIF_ID_BHP = 19;           //id komponentarif_id untuk BHP
    const KOMPONENTARIF_ID_SEWAALAT = 26;           //id komponentarif_id untuk SEWAALAT
    const KOMPONENTARIF_ID_OPERATOR = 21;           //id komponentarif_id untuk OPERATOR
    const KOMPONENTARIF_ID_ALATBAHAN = 19;           //id komponentarif_id untuk OPERATOR
    const KOMPONENTARIF_ID_JASA_RUJPEN = 31;           //id komponentarif_id untuk Rujukan Penunjang
    const KOMPONENTARIF_ID_INSENTIF = 27;           //id komponentarif_id untuk Insentif
    const KOMPONENTARIF_ID_INSENTIF_MEDIS = 39;           //id komponentarif_id untuk Insentif
    const KOMPONENTARIF_ID_INSENTIF_PARAMEDIS = 40;           //id komponentarif_id untuk Insentif
    const KOMPONENTARIF_ID_AHLI_GIZI = 7;           //id komponentarif_id untuk AHLI GIZI
    const KOMPONENTARIF_ID_PERUJUK = 31;           //id komponentarif_id untuk AHLI GIZI
    const KOMPONENTARIF_ID_ANASTESI = 12;           //id komponentarif_id untuk AHLI GIZI
    
    const KELOMPOKTINDAKAN_ID_LAUNDRY = 21;           //id kelompok tindakan untuk laundry
    const KELOMPOKTINDAKAN_ID_JENAZAH = 23;           //id kelompok tindakan untuk kamar jenazah
    const KELOMPOKTINDAKAN_ID_ADM = 17;           //id kelompok tindakan untuk administrasi
    const KELOMPOKTINDAKAN_ID_AMBULANCE= 8;           //id kelompok tindakan untuk ambulance    

    const KATEGORITINDAKAN_ID_ADM = 59;           //id kategori tindakan untuk administrasi
    const KATEGORITINDAKAN_ID_AMBULANCE= 11;           //id kategori tindakan untuk ambulance 
    
    const DEFAULT_PERDA_TARIF = 1;           //id default untuk perda tarif
    const DEFAULT_PERDA_TARIF_NAMA = 'Instalasi Rawat Jalan';           //id default untuk perda tarif nama
    public static function PerdaTarifNama()
    {
        $sql="SELECT perdanama_sk FROM perdatarif_m WHERE perda_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['perdanama_sk'];
    }

    const RUANGAN_ID_PERSALINAN = 23;
    const RUANGAN_ID_LAB = 18;           //id default untuk ruangan lab
    const RUANGAN_ID_RAD = 19;           //id default untuk ruangan rad
    const RUANGAN_ID_REHAB = 26;           //id default untuk ruangan rehab
    const RUANGAN_ID_IRM = 25;           //id default untuk ruangan rehab
    const RUANGAN_ID_IBS = 100;           //id default untuk ruangan bedah sentral
    const RUANGAN_ID_APOTEK_RJ = 20;           //id default untuk ruangan apotek rawat jalan
    const RUANGAN_ID_APOTEK_DEPO = 212;           //id default untuk ruangan apotek rawat jalan
    const RUANGAN_ID_GUDANG_FARMASI = 86;           //id default untuk ruangan gudang farmasi
    const RUANGAN_ID_JZ = 50;           //id default untuk ruangan kamar jenazah
    const RUANGAN_ID_GIZI = 21;           //id default untuk ruangan gizi
    const RUANGAN_ID_OBSGYN = 2;           //id default untuk ruangan obsgyn pagi
    const RUANGAN_ID_KONSULTASI_GIZI = 121; // id default untuk ruangan konsultasi gizi
    const RUANGAN_ID_KASIR_LAB = 124; // id default untuk ruangan kasir laboratorium
    const RUANGAN_ID_KASIR_SENTRAL = 28; // id default untuk ruangan kasir sentral
    const RUANGAN_ID_PERINATOLOGI = 208; // id default untuk ruangan perinatologi
    const RUANGAN_ID_RAWAT_DARURAT = 9; // id default untuk ruangan rawat darurat
    const RUANGAN_ID_HEMODIALISA = 141; // id default untuk ruangan rawat darurat
    const RUANGAN_ID_ESWL = 142; // id default untuk ruangan rawat darurat
    const RUANGAN_ID_ENDOSCOPY = 143; // id default untuk ruangan rawat darurat
    const RUANGAN_ID_LAUNDRY = 49; // id default untuk "Laundry"
    const RUANGAN_ID_JENAZAH = 50; // id default untuk Kamar Jenazah
    const RUANGAN_ID_KEAMANAN_TRANSPORTASI = 135; // id default untuk "Keamanan dan Transportasi"
    
    const RUANGAN_ID_POLY_ALERGY = 117;
    const RUANGAN_ID_POLY_ANAK = 3;
    const RUANGAN_ID_POLY_BEDAH = 106;
    const RUANGAN_ID_POLY_BEDAH_MULUT = 120;
    const RUANGAN_ID_POLY_BEDAH_SYARAF = 112;
    const RUANGAN_ID_POLY_BEDAH_UMUM = 1;
    const RUANGAN_ID_POLY_GIGI = 6;
    const RUANGAN_ID_POLY_JANTUNG = 113;
    const RUANGAN_ID_POLY_KONSERVASI_GIGI = 119;
    const RUANGAN_ID_POLY_ORTHOPEDI = 123;
    const RUANGAN_ID_POLY_PENYAKIT_DALAM = 103;
    const RUANGAN_ID_POLY_PENYAKIT_SYARAF = 4;
    const RUANGAN_ID_POLY_THT = 118;
    const RUANGAN_ID_POLY_UMUM = 7;
    const RUANGAN_ID_POLY_UROLOGI = 111;
    
    const RUANGAN_ID_DIREKTUR = 40;             // id untuk Direktur PT Karsa Abdi Husada dan Direktur RS Jasa Kartini
    const RUANGAN_ID_SUBBAG_AKUNTANSI = 129;        // id untuk Sub Bagian Akuntansi dan Mobilisasi Dana
    const RUANGAN_ID_SUBBAG_BENDAHARA = 128;        // id untuk Sub Bagian pembendaharaan
    const RUANGAN_ID_SEKRETARIS = 39;       // id untuk Sekretaris
    const RUANGAN_ID_STAFF_ME = 39;         // id untuk Staf Medik Fungsional (SMF)
    const RUANGAN_ID_KEPEGAWAIAN = 127;     // id untuk Sub Bagian Kepegawaian
    const RUANGAN_ID_REKAM_MEDIK = 97;      // Sub Bagian Rekam Medik
    const RUANGAN_ID_KEUANGAN = 27;      // Bagian Keuangan
    const RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN = 136;      // Sub Bagian Penyusunan Anggaran
    const RUANGAN_ID_PELAYANAN_KHUSUS = 130;      // Seksi Pelayanan Khusus
    const RUANGAN_ID_PENDAFTARAN = 96;      // Ruangan Pendaftaran
    

    const RUJUKAN = 'RUJUKAN';           //nilai untuk status periksa rujukan
    const KODE_RENCANA_OPERASI = 'RENCANA';           //nilai untuk status periksa rujukan
    const KODE_RUJUKAN = 'DIRUJUK';           //nilai untuk status periksa rujukan
    const KODE_MENINGGAL_1 = 'MENINGGAL < 48 JAM';           //nilai untuk status periksa MENINGGAL < 48 JAM
    const KODE_MENINGGAL_2 = 'MENINGGAL > 48 JAM';           //nilai untuk status periksa MENINGGAL > 48 JAM
    const CARA_KELUAR_RI = 'DIRAWAT INAP';
    const KONDISI_PULANG_RI = 'DIRAWAT INAP';
    const JENIS_KELAMIN_PEREMPUAN = 'PEREMPUAN';    //Parameter text untuk jenis kelamin peremmpuan
    const JENIS_KELAMIN_LAKI_LAKI = 'LAKI-LAKI';
    const KELOMPOKDIAGNOSA_MASUK = 1;   //nilai diagnosa masuk pada kelompok diagnosa
    const KELOMPOKDIAGNOSA_UTAMA = 2;   //nilai diagnosa utama pada kelompok diagnosa
    const KELOMPOKDIAGNOSA_TAMBAHAN = 3;   //nilai diagnosa tambahan pada kelompok diagnosa
    const RI_DIPULANGKAN = 'DIPULANGKAN';
    const KATEGORI_TINDAKAN_GIZI = 101; //ID KATEGORI TINDAKAN = MAKANAN GIZI;
    const ID_PENJAMIN_UMUM = 1;

    const DEFAULT_CARABAYAR = 1;    //id default untuk cara bayar (membayar)
    const CARABAYAR_ASURANSI = 2;
    const CARABAYAR_BPJS = 18;
    const DEFAULT_PENJAMIN = 1;
    const DEFAULT_PASIEN = 0;//id default untuk penjamin pasien (sendiri) NOREKAM: 000000
    const DEFAULT_PASIEN_APOTEK_UNIT = 157323;//id pasien untuk apotek - penjualan unit NOREKAM: AP000001
    const DEFAULT_PASIEN_APOTEK_KARYAWAN = 157324;//id pasien untuk apotek - penjualan karyawan NOREKAM: AP000002
    const DEFAULT_PASIEN_APOTEK_DOKTER = 157325;//id pasien untuk apotek - penjualan dokter NOREKAM: AP000003
    const DEFAULT_PASIEN_APOTEK_UMUM = 157326;//id pasien untuk apotek - penjualan umum NOREKAM: AP000004
    
    const CARAPEMBAYARAN_TUNAI = "TUNAI";//id pasien untuk apotek - penjualan umum NOREKAM: AP000004
    
    const STATUS_REKAMMEDIS_PASIENBARU = 'AKTIF';
    const STATUSSCAN_ID_MASUK = '1'; // status scan dari finger print masuk
    const STATUSSCAN_ID_PULANG = '2'; // status scan dari finger print pulang
    
    const KODE_HASIL_RE = 'HASIL';           //kode untuk no hasil pemeriksaan rehab medis
    const KODE_JADWAL_RE = 'JADWAL';           //kode untuk no jadwal kunjungan rehab medis
    const SATUAN_TINDAKAN_PENDAFTARAN = 'Hari';  //Satuan Tindakan Default untuk tindakan PelayananT Karcis
    const SATUAN_TINDAKAN_VISITE = 'Kali';  //Satuan Tindakan Default untuk tindakan PelayananT Karcis

    const TIPE_TRANSPORTASI = 'transportasi'; //tipe dari tabel lookup_m untuk transportasi
    const TIPE_KEADAAN_MASUK = 'keadaanmasuk'; //tipe dari tabel lookup_m untuk keadaan masuk
    const TIPE_KONDISI_PULANG = 'kondisipulang'; //tipe dari tabel lookup_m untuk kondisi pulang
    const TIPE_CARA_KELUAR = 'carakeluar'; //tipe dari tabel lookup_m untuk cara keluar
    const TIPE_KELOMPOK_PEMERIKSAAN_LAB = 'kelompokpemeriksaanlab'; //tipe dari tabel lookup_m untuk kelompok pemeriksaan lab    
    const TIPE_SATUAN_HASIL_LAB = 'satuanhasillab'; //tipe dari tabel lookup_m untuk satuan hasil lab
    const TIPE_STATUS_PERIKSA_HASIL = 'statusperiksahasil'; // tipe dari tabel lookup_m untuk status periksa hasil
    const SATUANLAMARAWAT_RD = 'Jam'; //satuan lama rawat untuk pasienpulang_t untuk instalasi rawat darurat
    const SATUANLAMARAWAT_RI = 'Hari'; //satuan lama rawat untuk pasienpulang_t untuk instalasi rawat inap
    const HARI_RAWAT = 1; // jika perhitungan kamar menggunakan hari rawat, isi dengan 1 (satu), jika lama rawat isi dengan 0 (nol)
    
    const JENISPESANMENU_PASIEN = 'Pasien'; //tipe dari table lookup_m untuk jenis pesan menu diet Pasien
    const JENISPESANMENU_PEGAWAI = 'Pegawai'; //tipe dari table lookup_m untuk jenis pesan menu diet Pegawai
    
    const DEFAULT_JENISDIET_MAKANAN_HARIAN = 81;
    const DEFAULT_JENISDIET_MAKANAN_CATHERING = 82;
    const DEFAULT_JENISDIET_MAKANAN_EXTRAFOOD = 83;
    const DEFAULT_JENISDIET_MAKANAN_CAIR = 84;
    
    const DEFAULT_OBAT_BEBAS_JURNAL = 10733; // di server bandung
    const DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN = 10734; // di server jk
    const DEFAULT_OBAT_BEBAS_JURNAL_OA = 10735; // di server jk
    const DEFAULT_OBAT_BEBAS_JURNAL_OA2 = 10736; // di server jk
    
    const DEFAULT_BEBAN_BAHAN_AL = 10758; // di server BDG
    const DEFAULT_BEBAN_BAHAN_GM = 10759; // di server BDG

    /** Untuk Penjualan Farmasi */
    const DEFAULT_BIAYA_SERVICE_FARMASI = 10740; // di server bandung
    const DEFAULT_BIAYA_KONSELING_FARMASI = 10741; // di server bandung
    const DEFAULT_BIAYA_ADMINISTRASI_FARMASI = 10742; // di server bandung
 
    /**
     * DEFAULT CONST FOR SRBAC 
     */
    
    const DEFAULT_OPERATING = 'Operating';
    const DEFAULT_UPDATE = 'Update';
    const DEFAULT_CREATE = 'Create';
    const DEFAULT_DELETE = 'Delete';
    const DEFAULT_LAPORAN_RETAIL = 'SalesReport';
    const DEFAULT_PURCHASEORDER = 'Index';
    const DEFAULT_TRANSAKSISTOKOPNAME = 'Stokopname';
    const DEFAULT_GUDANG_RETAIL = 'Stokopname';
    const DEFAULT_PENJUALAN_RETAIL = 'ClosingKasir';
    const DEFAULT_ADMIN = 'Admin';
    const DEFAULT_ALL_OPERATION = 'All Operation';
    const DEFAULT_ALL = 'All';
    const DEFAULT_BATAL_PERIKSA = 'BATAL PERIKSA';
    const DEFAULT_ROLES_SUPER_ADMINISTRATOR = 'Super Administrator';
    const DEFAULT_ROLES_ADMINISTRATOR = 'Administrator';
    const DEFAULT_ROLES_OPERATOR = 'Opertor';
    const DEFAULT_ROLES_SUPERVISOR = 'Supervisor';

    const CARAMASUK_ID_LANGSUNG = 1;    //id untuk cara masuk langsung
    const CARAMASUK_ID_RD = 2;          //id untuk cara masuk melalui rawat darurat
    const CARAMASUK_ID_RJ = 3;          //id untuk cara masuk melalui rawat jalan
    
    const TIPEPAKET_NONPAKET = 1;       //id tipe paket non paket
    const TIPEPAKET_LUARPAKET = 2;       //id tipe paket luar paket
    const TIPEPAKET_BMHP = 20;       //id tipe paket BMHP
    
    const DAFTARTINDAKAN_KONSUL_ID = 6; //id untuk karcis tindakan konsul
    const LAB_PATOLOGI = 'PATOLOGI KLINIK';
    const JUMLAH_PERHALAMAN = 5;
    
    /* konstanta untuk tarif PAKET */
    const PAKET_LAB = 5645;
    const BLOOD_GAS = 11;

    const DEFAULT_RACIKAN_ID = 1;           //id untuk obat racikan di racikan_m
    const DEFAULT_NON_RACIKAN_ID = 2;       //id untuk obat non racikan di racikan_m
    
    //Modul Gudang Farmasi
    const DEFAULT_JENIS_SUPPLIER = 'Farmasi';
    const JENIS_STOKOPNAME_PENYESUAIAN = 'Penyesuaian';

    //Gudang Farmasi
    const ID_INSTALASI_FARMASI = 9;
    const DEFAULT_STATUS_PESAN = 'BIASA'; // Default value modul persalinan
    
    //Modul Gudang Farmasi
    const GIZI_JENIS_SUPPLIER = 'Gizi';
    
    //Konfigurasi finger print
    const IP_FINGER_PRINT = '192.168.1.201';
    const KEY_FINGER_PRINT ='123';
    
    //Konfigurasi Farmasi
    const KONFIG_LIFO = true;
    
    
    const DEFAULT_PASIEN_ID_RETAIL = 1;
    const DEFAULT_ALAMAT_BKM = "TOKO";
    const DEFAULT_DARINAMA_BKM = "PENJUAL";
    const DEFAULT_SEBAGAIPEMBAYARAN_BKM = 'PENJUAL MINIMARKET';
    
    const DEFAULT_IDSATUANKECIL = 18;
    const DEFAULT_IDSATUANBESAR = 32;
    
    const DEFAULT_BIN = ' alias ';
    
    const DEFAULT_DOKTER_LAB = 145; //dr. Dewi Kania Yulianti
//    const DEFAULT_DOKTER_RAD = 576; //dr. Wahyu Rinto Regowo
    const DEFAULT_DOKTER_RAD = 908; //dr. Wahyu Rinto Regowo
    
    const TARIF_ADMINISTRASI_LAB_ID = 2003;//tarif administrasi untuk lab
    const TARIF_ADMINISTRASI_RAD_ID = 2003;
    const KATEGORI_TINDAKAN_ID_ADMINISTRASI = 59;//kategoritindakan_id untuk urin
    const KATEGORI_TINDAKAN_ID_URINE = 32;//kategoritindakan_id untuk urin
    const KATEGORI_TINDAKAN_ID_FAECES = 28;//kategoritindakan_id untuk faeces
    
    const PERIKSA_RONTGEN = "[r] foto x-ray (rontgen)"; //Jenis Pemeriksaan Rad Rontgen
    const PERIKSA_CTSCAN = "[t] ct scan"; //Jenis Pemeriksaan Rad CT Scan
    const PERIKSA_USG = "[s] ultrasonografi (usg)"; //Jenis Pemeriksaan Rad USG
    const PAKET_PEMERIKSAAN = "paket pemeriksaan"; //Jenis Paket Pemeriksaan
    const PAKET_PEMERIKSAAN_MCU_HAJI = "paket pemeriksaan gcu"; //Jenis Paket Pemeriksaan
    
    const PERIKSA_GLUKOSAPUASA_ID = 336; //Pemeriksaan Lab Glukosa Puasa
    const PERIKSA_EKG_ID = 364; //Pemeriksaan Lab E.K.G
    const PERIKSA_TREADMILL_ID = 365; //Pemeriksaan Lab Treadmill Test
    const PERIKSA_ECHOCAR_A_ID = 366; //Pemeriksaan Echo Cardiography A
    const PERIKSA_ECHOCAR_B_ID = 367; //Pemeriksaan Echo Cardiography B
    const PERIKSA_ECHOCAR_C_ID = 368; //Pemeriksaan Echo Cardiography C
    const PERIKSA_SPIROMETRI_ID = 485; //Pemeriksaan Lab Spirometri
    const PERIKSA_AUDIOMETRI_ID = 486 ;//Pemeriksaan Lab Audiometri
    const PAKET_MCU_HAJI_PEREMPUAN = 575 ;
    const PAKET_MCU_HAJI_LAKI_LAKI = 576 ;
    const PAKET_II_HDI = 578 ;//Pemeriksaan Lab II HDI
    const PAKET_IV_HDI = 580 ;//Pemeriksaan Lab IV HDI
    const PAKET_VI_HDI = 582 ;//Pemeriksaan Lab VI HDI
    const PAKET_V_HDI = 581 ;//Pemeriksaan Lab V HDI

    /* PEMERIKSAAN GOLONGAN DARAH*/
    const PERIKSA_GOLONGANDARAH_ID = 328;
    const PERIKSA_RHESUS_ID = 329;
    /* JENIS JURNAL KAS */
    const JURNAL_PENERIMAAN_KAS= 1;
    const JURNAL_PENGELUARAN_KAS = 2;
    const JURNAL_PEMBELIAN = 3;
    const JURNAL_PELAYANAN = 4;
    const JURNAL_PENJUALAN = 5;
    const JURNAL_UMUM = 6;
    
    /* JENIS PENERIMAAN */
    const JENISPENERIMAAN_ID_PELUNASAN_PIUTANG= 7;
    const JENISPENERIMAAN_ID_PIUTANG_MERCHANT= 10;
    const JENISPENERIMAAN_ID_KLAIM_BANK= 8;
    const JENISPENERIMAAN_ID_KLAIM_PIUTANG= 11;

    // Rekening Neraca
    const REKENING_AKTIVA = 1;
    const REKENING_KEWAJIBAN = 2;
    const REKENING_EKUITAS = 3;
    const ID_STRUKTUR_AKTIVA = 1;
    const ID_STRUKTUR_PENDAPATAN_OP = 4;
    const ID_STRUKTUR_KEWAJIBAN = 2;
    const ID_STRUKTUR_EKUITAS = 3;
    const ID_KELOMPOK_AKTIVA_LANCAR = 1;
    const ID_KELOMPOK_PENDAPATAN_OP = 13;
    const ID_KELOMPOK_KEWAJIBAN = 6;
    
	// Akutansi - Arus Kas
	const REKPERIOD2014 = 1;
	const REKPERIOD2015 = 2;
	const REK2_ID_KEWAJIBANLANCAR=5;
	const REK4_ID_BEBANPENYUSUTAN_AMORTISASI=74;
	//end Akutansi
	
    // Jenis Pengeluaran Akuntansi Asset
    const ID_REKENING_PENGHAPUSAN_GEDUNG = 13;
    const ID_REKENING_PENGHAPUSAN_MEDIS = 15;
    const ID_REKENING_PENJUALAN_GEDUNG = 17;
    const ID_REKENING_PENJUALAN_NON_MEDIS = 18;
    const ID_REKENING_PENJUALAN_MEDIS     = 19;
    const ID_REKENING_PENJUALAN_KENDARAAN = 20;
    const ID_REKENING_PENJUALAN_TANAH = 21;
    
    const JENIS_PEMERIKSAAN_MIKROBIOLOGI = "mikrobiologi"; //Jenis Pemeriksaan Mikrobiologi

    //Komponen Gaji & Tunjangan
    const ID_KOMPONEN_GAJI = 9;
    const ID_TUNJANGAN_HARIAN = 10;
    const ID_TUNJANGAN_JABATAN = 11;
    const ID_TUNJANGAN_FUNGSIONAL = 12; 
    
    //Jenis Tarif
    const ID_TARIF_PELAYANAN = 1;
    const ID_TARIF_PERUSAHAAN = 2;
    const ID_TARIF_BPJS = 3;
    const ID_TARIF_JAMKESMAS = 4;    

    const KELASPELAYANAN_ID_VVIP = 8;           //id kelaspelayanan_id untuk Vvip
    const KELASPELAYANAN_ID_VIP_A = 9;           //id kelaspelayanan_id untuk Vip A
    const KELASPELAYANAN_ID_VIP_B = 10;           //id kelaspelayanan_id untuk Vip B
    const KELASPELAYANAN_ID_UTAMA = 18;           //id kelaspelayanan_id untuk Utama
    const KELASPELAYANAN_ID_MADYA_A = 13;           //id kelaspelayanan_id untuk MADYA A
    const KELASPELAYANAN_ID_MADYA_B = 14;           //id kelaspelayanan_id untuk MADYA B
    const KELASPELAYANAN_ID_PRATAMA_A = 15;           //id kelaspelayanan_id untuk PRATAMA A
    const KELASPELAYANAN_ID_PRATAMA_B = 16;           //id kelaspelayanan_id untuk PRATAMA B
    const KELASPELAYANAN_ID_PRATAMA_C = 17;           //id kelaspelayanan_id untuk PRATAMA C
    const KELASPELAYANAN_ID_TANPA_KELAS = 5;           //id kelaspelayanan_id untuk TANPA KELAS
    
    const RUANGAN_ID_2A = 207;
    const RUANGAN_ID_2B = 139;
    const RUANGAN_ID_2C = 206;
    const RUANGAN_ID_3A = 200;
    const RUANGAN_ID_3B = 201;  //belum ada datanya(id)
    const RUANGAN_ID_3C = 209;
    const RUANGAN_ID_4A = 203;
    const RUANGAN_ID_4B = 202;
    const RUANGAN_ID_4C = 204;

    const RUANGAN_ID_ICU = 24;
    const RUANGAN_ID_OBSERVASI = 210;
    const RUANGAN_ID_BANGSAL = 205;

    //Modul Bedah Sentral (IBS)
    const JENIS_ANASTESI_LOKAL_ID = 1; //Lokal
    const TYPEANASTESI_STANDAR_TEXT = "Standar"; //Standar
    const TYPEANASTESI_LOKAL_ID_NAR = 2; //Lokal
    const TYPEANASTESI_LOKAL_ID_SPI = 6; //Lokal
    const TYPEANASTESI_LOKAL_ID_EPI = 10; //Lokal
    const TINDAKAN_RESUSITASI_ID = 10640; //RESUSITASI CAESAR
    const TINDAKAN_RESUSITASI_CYTO_ID = 10641; //RESUSITASI CAESAR CYTO
    const TINDAKAN_BEDAH_MONOR_RALAN_SP = 10701; //BEDAH MINOR RALAN SPESIALIS
    const TINDAKAN_BEDAH_MONOR_RALAN_UM = 10702; //BEDAH MINOR RALAN UMUM
    const TINDAKAN_BEDAH_MONOR_RALAN_SP_WSD = 10703; //BEDAH MINOR RALAN SPESIALIS (WSD)
    const TINDAKAN_BEDAH_MONOR_RALAN_UM_WSD = 10704; //BEDAH MINOR RALAN UMUM (WSD)
    
    const STATUSHADIR = 1;    
    const STATUSSAKIT = 2;    
    const STATUSIJIN = 3;    
    const STATUSALPHA = 5;   
    const STATUSCUTI = 8;     
    //Kasir - Pembayaran Jasa Dokter
    const PEMBAYARAN_JASA_MEDIS = "jasamedis";
    const PEMBAYARAN_JASA_RUJUKAN = "jasarujuk";
    const PEMBAYARAN_JASA_ANASTESI = "jasaanastesi";

    const DEFAULT_INSTALASI_RJ = 1;
    const DEFAULT_PEKERJAAN = 17;

    const GOLONGAN_UMUR_BARU_LAHIR = 6;
    const GOLONGAN_UMUR_BAYI = 7;
    const GOLONGAN_UMUR_BALITA = 8;
    const GOLONGAN_UMUR_ANAK_ANAK = 1;
    const GOLONGAN_UMUR_REMAJA = 2;
    const GOLONGAN_UMUR_DEWASA = 3;
    const GOLONGAN_UMUR_ORANG_TUA = 4;
    const GOLONGAN_UMUR_MANULA = 5;
    const GOLONGAN_UMUR_BATITA = 15;

    const MENINGGAL_LEBIH_48JAM = "Meninggal > 48 JAM";

    const BIAYA_KONSELING = "konseling";
    const BIAYA_SERVICE = "service";
    const BIAYA_ADMINISTRASI = "administrasi";
        
    //Modul Gudang Umum
    const DEFAULT_JENIS_SUPPLIER_GU = 'Gudang Umum';

    const REKJNSPENGELUARAN_RETURPEMBELIAN = 7;

    const PROFIL_RSJK = 1;

    const KUALIFIKASI_D3_TEKNIK_ELEKTROMAKGNETIK = 87;
    const KUALIFIKASI_SARJANA_TEHNIK = 128;
    const KUALIFIKASI_ANALISIS_FARMASI = 55;
    const KUALIFIKASI_SARJANA_BIOLOGI = 122;
    const KUALIFIKASI_S2_SARJANA_KOMPUTER = 118;



    // UNTUK CARA KELUAR - LAPORAN SIRS
    const STATUS_RUJUKAN = 'RUJUKAN';
    const STATUS_NON_RUJUKAN = 'NONRUJUKAN';
    const CARA_KELUAR_DIRUJUK = 'DIRUJUK';
    const CARA_KELUAR_DIPULANGKAN = 'DIPULANGKAN';
    const CARA_KELUAR_PULANG_PAKSA = 'PULANG PAKSA';
    const CARA_KELUAR_MELARIKAN_DIRI = 'MELARIKAN DIRI';
    const CARA_KELUAR_MENINGGAL = 'MENINGGAL';
    const CARA_KELUAR_DIRAWAT_INAP = 'DIRAWAT INAP';

    const NAMA_KLINIK = 'BUDI KARTINI';
    const KOTA_KOPSURAT = 'Tasikmalaya';
	const NO_IZIN_LAB = '440/00157/LAB-KLINIK/BPPT/I/2014';

    public static function jenisPeriksaUrine($urutan) // Urine
    {
        $jenisPemeriksaan = array(1 => "urine lengkap", //Jenis Pemeriksaan Urine Lengkap
                        2 => "urine khusus", //Jenis Pemeriksaan Urine Khusus
                        3 => "test kehamilan", //Jenis Pemeriksaan Test Kehamilan
                        4 => "humadrug", //Jenis Pemeriksaan Humadrug
                        5 => 336,//Pemeriksaan Urine Glukosa Puasa
                        6 => 337, //Pemeriksaan Urine Glukosa 2jampp
                    );
        return $jenisPemeriksaan[$urutan];
    }

    public static function jenisPeaces($urutan) // Peaces
    {
        $jenisPemeriksaan = array(
            1 => "lekosit",
            2 => "urine khusus",
            3 => "test kehamilan",
            4 => "humadrug",
        );
        return $jenisPemeriksaan[$urutan];
    }
    
    public static function jenisPeriksaEdta($urutan)
    {
        $jenisPemeriksaan = array(1 => "hematologi", //Jenis Pemeriksaan Hematologi
                        2 => "karbohidrat", //Jenis Pemeriksaan Karbohidrat
                    );
        return $jenisPemeriksaan[$urutan];
    }    
    public static function jenisPeriksaFaeces($urutan)
    {
        $jenisPemeriksaan = array(1 => "FAECES LENGKAP", //Jenis Pemeriksaan Faeces Khusus
                        2 => "FAECES KHUSUS", //Jenis Pemeriksaan Faeces Lengkap
                    );
        return $jenisPemeriksaan[$urutan];
    }    
    
    public static function tanggalBerlaku()
    {
        $sql="SELECT tglberlaku FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['tglberlaku'];
    }
    
    public static function persenPPN()
    {
        $sql="SELECT persenppn FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['persenppn'];
    }
    
    public static function persenPPH()
    {
        $sql="SELECT persenpph FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['persenpph'];
    }
    
    public static function persenHargaJual()
    {
        $sql="SELECT persehargajual FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['persehargajual'];
    }
    
    public static function totalPersenHargaJual()
    {
        $sql="SELECT totalpersenhargajual FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['totalpersenhargajual'];
    }
    
    public static function bayarLangsung()
    {
        $sql="SELECT bayarlangsung FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['bayarlangsung'];
    }
    
    public static function pesanDistruk()
    {
        $sql="SELECT pesandistruk FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['pesandistruk'];
    }
    
    public static function pesanDifaktur()
    {
        $sql="SELECT pesandifaktur FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['pesandifaktur'];
    }
    
    public static function formulaJasaDokter()
    {
        $sql="SELECT formulajasadokter FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['formulajasadokter'];
    }
    
    public static function formulaJasaParamedis()
    {
        $sql="SELECT formulajasaparamedis FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        return $querSql['formulajasaparamedis'];
    }
    
    /**
     * method untuk mendapatkan value attributes farmasi untuk mendapatkan semua field params dikosongkan
     * @param string $params berupa attributes table konfigfarmasi_k default null
     * @return mixed 
     * 
     */
    public static function konfigFarmasi($params=null)
    {
        $sql="SELECT *FROM konfigfarmasi_k WHERE konfigfarmasi_aktif=TRUE LIMIT 1";
        $querSql = Yii::app()->db->createCommand($sql)->queryRow();
        $result = (!empty($params)) ? $querSql[$params] : $querySql;
        return $result;
    }
    //Akhir Konfigurasi farmasi
    
    public static function singkatanNoPendaftaranGZ()
    {
        $kodeNopendRJ = "GZ";
        return $kodeNopendRJ;
    }
    
    public static function singkatanNoPendaftaranRJ()
    {
        $sql="SELECT nopendaftaran_rj FROM konfigsystem_k LIMIT 1";
        $noNopendRJ = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopendRJ = $noNopendRJ['nopendaftaran_rj'];
        return $kodeNopendRJ;
    }
    
    
    public static function singkatanNoPendaftaranRI()
    {
        $sql="SELECT nopendaftaran_ri FROM konfigsystem_k LIMIT 1";
        $noNopendRI = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopendRI = $noNopendRI['nopendaftaran_ri'];
        return $kodeNopendRI;
    }
    
    public static function singkatanNoPendaftaranRD()
    {
        $sql="SELECT nopendaftaran_gd FROM konfigsystem_k";
        $noNopendRD = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopendRD = $noNopendRD['nopendaftaran_gd'];
        return $kodeNopendRD;
    }
    
    public static function singkatanNoPendaftaranRad()//Radioligi
    {
        $sql="SELECT nopendaftaran_rad FROM konfigsystem_k LIMIT 1";
        $noNopendRad = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopendRad = $noNopendRad['nopendaftaran_rad'];
        return $kodeNopendRad;
    }
    
    public static function singkatanNoPendaftaranLab()//Laboratorium Klinik
    {
        $sql="SELECT nopendaftaran_lab FROM konfigsystem_k";
        $noNopendLab = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopendLab = $noNopendLab['nopendaftaran_lab'];
        return $kodeNopendLab;
    }
    
    public static function singkatanNoPendaftaranIBS()//IBS
    {
        $sql="SELECT nopendaftaran_ibs FROM konfigsystem_k LIMIT 1";
        $noNopend = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopend = $noNopend['nopendaftaran_ibs'];
        return $kodeNopend;
    }
    
    public static function singkatanNoPendaftaranRehabMedis()//Rehab Medis
    {
        $sql="SELECT nopendaftaran_rehabmedis FROM konfigsystem_k";
        $noNopend = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeNopend = $noNopend['nopendaftaran_rehabmedis'];
        return $kodeNopend;
    }

    public static function statusRM($urutan)
    {
        $status = array(1 => 'AKTIF',
                        2 => 'TIDAK AKTIF',
                        );
        return $status[$urutan];
    }
    
    public static function statusPeriksa($urutan)
    {
        $status = array(
            1 => 'ANTRIAN',
            2 => 'SEDANG PERIKSA',
            3 => 'SUDAH DI PERIKSA',
            4 => 'SUDAH PULANG',
            5 => 'BATAL PERIKSA',
            6 => 'SEDANG DIRAWAT INAP',
        );
        return $status[$urutan];
    }
    
    public static function statusPeriksaLK($urutan)
    {
        $statusLK = array(1 => 'BELUM',
                        2 => 'SEDANG',
                        3 => 'SUDAH');
        return $statusLK[$urutan];
    }

    public static function statusPasien($kunjungan)
    {
        $status = array('baru'=>'PENGUNJUNG BARU',
                        'lama'=>'PENGUNJUNG LAMA');
        return $status[$kunjungan];
    }
    
    
    public static function statusKunjungan($kunjungan)
    {
        $status = array('baru'=>'KUNJUNGAN BARU',
                        'lama'=>'KUNJUNGAN LAMA');
        return $status[$kunjungan];
    }
    
    public static function statusMasuk($status)
    {
        $statusMasuk = array('nonrujukan'=>'NON RUJUKAN',
                             'rujukan'=>'RUJUKAN');
        return $statusMasuk[$status];
    }
    
    /**
     * untuk mengambil path direktori gambar untuk slider Antrian
     * @return string path contoh: /var/www/simrs/data/images/slideantrian/
     */
    public static function pathAntrianSliderGambar()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/slideantrian/';
    }
    
    public static function pathAntrianSliderGambarThumbs()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/slideantrian/thumbs/';
    }
    
    public static function urlAntrianSliderGambar()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/slideantrian/';
    }
    
    public static function urlAntrianSliderGambarThumbs()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/slideantrian/thumbs/';
    }

    /**
     * untuk mengambil path direktori icon modul
     * @return string path contoh: /var/www/simrs/images/icon_modul
     */
    public static function pathIconModulDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/icon_modul/';
    }
    
    /**
     * untuk mengambil path direktori thumbnail icon modul
     * @return string path contoh: /var/www/simrs/images/icon_modul
     */
    public static function pathIconModulThumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/icon_modul/thumbs/';
    }
    
    /**
     * untuk mengambil url direktori thumbnail icon modul
     * @return string path contoh: /var/www/simrs/images/icon_modul
     */
    public static function urlIconModulThumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/icon_modul/thumbs/';
    }
    
    /**
     * untuk mengambil path direktori icon menu
     * @return string path contoh: /var/www/simrs/images/icon_menu
     */
    public static function pathIconMenuDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/icon_menu/';
    }
    
    /**
     * untuk mengambil path direktori thumbnail icon menu
     * @return string path contoh: /var/www/simrs/images/icon_menu
     */
    public static function pathIconMenuThumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/icon_menu/thumbs/';
    }
    
    /**
     * untuk mengambil url direktori thumbnail icon modul
     * @return string path contoh: /var/www/simrs/images/icon_modul
     */
    public static function urlIconModulDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/icon_modul/';
    }
    public static function urlIconMenuDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/icon_menu/';
    }
    public static function urlBarangDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/barang/';
    }

    public static function pathProfilRSDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/profil_rs/';
    }
    public static function pathBarangDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/barang/';
    }
    
    public static function pathProfilRSTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/profil_rs/tumbs/';
    }
    public static function pathBarangTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/images/barang/tumbs/';
    }
    
//======================================Path Instalasi==============================================    
    public static function pathRuanganDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/ruangan/';
    }
    
    public static function pathRuanganTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/ruangan/tumbs/';
    }
    
    public static function urlRuanganDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/ruangan/';          //Untuk Menampilkan Gambar Asli
    }
    
    public static function urlRuanganTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/ruangan/tumbs/';    //Untuk Menampilkan Gambar Tumbs
    }  
//====================================Akhir Path dan UrlInstalasi=====================================
    
    //======================================Path Kamr Ruangan==============================================    
    public static function pathKamarRuanganDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/kamarruangan/';
    }
    
    public static function pathKamarRuanganTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/kamarruangan/tumbs/';
    }
    
    public static function urlKamarRuanganDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/kamarruangan/';          //Untuk Menampilkan Gambar Asli
    }
    
    public static function urlKamarRuanganTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/kamarruangan/tumbs/';    //Untuk Menampilkan Gambar Tumbs
    }  
//====================================Akhir Path dan UrlKamarRuangan=====================================
    
//======================================Path Instalasi==============================================    
    public static function pathInstalasiDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/instalasi/';
    }
    
    public static function pathInstalasiTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/instalasi/tumbs/';
    }
    
    public static function urlInstalasiDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/instalasi/';          //Untuk Menampilkan Gambar Asli
    }
    
    public static function urlInstalasiTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/instalasi/tumbs/';    //Untuk Menampilkan Gambar Tumbs
    }  
//====================================Akhir Path dan UrlInstalasi=====================================    
    
    //======================================Path USG==============================================    
    public static function pathUSGDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/fotousg/';
    }
    
    public static function pathUSGTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/fotousg/tumbs/';
    }
    
    public static function urlUSGDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/fotousg/';          //Untuk Menampilkan Gambar Asli
    }
    
    public static function urlUSGTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/fotousg/tumbs/';    //Untuk Menampilkan Gambar Tumbs
    }  
//====================================Akhir Path dan Url USG=====================================     
    
    public static function urlProfilRSDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/profil_rs/';          //Untuk Menampilkan Gambar Asli
    }
    
    public static function urlProfilRSTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/images/profil_rs/tumbs/';    //Untuk Menampilkan Gambar Tumbs
    }  
    
    public static function pathPegawaiDirectory()
    {
            return Yii::getPathOfAlias('webroot').'/data/images/pegawai/';
    }
    
    public static function pathPegawaiTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pegawai/tumbs/';
    }
    
    public static function urlPegawaiDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pegawai/';          //Untuk Menampilkan Gambar Asli Pegawai
    }
    
    public static function urlPegawaiTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pegawai/tumbs/';    //Untuk Menampilkan Gambar Tumbs Pegawai
    }  
    
    public static function urlPhotoPasienDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pasien/';    //Untuk Menampilkan photo pasien
    }
    public static function urlPhotoBarangDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/barang/';    //Untuk Menampilkan photo barang
    }
    
    public static function pathPasienTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pasien/tumbs/';
    }
    
    public static function urlPasienTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pasien/tumbs/';    //Untuk Menampilkan Gambar Tumbs Pasien
    }  
    
    public static function pathPasienDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pasien/';
    }
    
    public static function urlKendaraanDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/kendaraan/';    //Untuk Menampilkan Gambar Kendaraan
    }
    
    public static function pathKendaraanDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/kendaraan/';
    }
    
    public static function urlKendaraanTumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/kendaraan/tumbs/';    //Untuk Menampilkan Gambar Tumbs Kendaraan
    }
    
    public static function pathKendaraanTumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/kendaraan/tumbs/';
    }
    //======== path dan url photo dan file pelamar ========
    public static function pathPelamarThumbsDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pelamar/photos/thumbs/';
    }
    public static function pathPelamarPhotosDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pelamar/photos/';
    }
    public static function pathPelamarFilesDirectory()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pelamar/files/';
    }
    public static function urlPelamarThumbsDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pelamar/photos/thumbs/';
    }
    public static function urlPelamarPhotosDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pelamar/photos/';
    }
    public static function urlPelamarFilesDirectory()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pelamar/files/';
    }
    
    //======== End path dan url photo pelamar ========
    public static function getModules()
    {
        $moduls = Yii::app()->metadata->getModules();
        foreach($moduls as $i=>$modul){
            $result[$modul] = $modul;
        }
        
        return $result;
    }
    
    /**
     *
     * @param type $namaModul 
     */
    public static function getControllers($namaModul)
    {        
        $controllers = Yii::app()->metadata->getControllers($namaModul);
        foreach($controllers as $i=>$controller){
            $controller = str_replace('Controller', '', $controller);
            $result[$controller] = $controller;
        }
        
        return $result;
    }
    
    /**
     *
     * @param type $contorllerId
     * @param type $namaModul 
     */
    public static function getActions($contorllerId, $namaModul)
    {
        $result = array();
        $actions = Yii::app()->metadata->getActions(ucfirst($contorllerId).'Controller', $namaModul);
        foreach($actions as $i=>$action){
            $result[$action] = $action;
        }
        
        return $result;
    }
    
    public static function daftarLookUp()
    {
        $daftar = array(
            'agama'                 =>'Agama',
            'carakeluar'            =>'Cara Keluar',
            'caraPembayaranKlaim'   =>'Cara Pembayaran Klaim',
            'formularium'           =>'Obat Alkes Formularium',
            'gelardepan'            =>'Gelar Depan',
            'golongandarah'         =>'Golongan Darah',
            'gravida'               =>'Gravida',                        //    
            'hubungankeluarga'      =>'Hubungan Keluarga',
            'jenisidentitas'        =>'Jenis Identitas',
            'jeniswaktukerja'       =>'Jenis Waktu Kerja',
            'jeniskb'               =>'Jenis KB',
            'jeniskelamin'          =>'Jenis Kelamin',
            'jenispemeriksaanrad'   =>'Jenis Pemeriksaan Radiologi',
            'jenisrumahsakit'       =>'Jenis Rumah Sakit',
            'kasusdiagnosa'         =>'Kasus Diagnosa',
            'kategorimodul'         =>'Kategori Modul',
            'kategoriasalpegawai'   =>'Kategori Asal Pegawai ',
            'kategoripemakai'       =>'Kategori Pemakai',
            'kategoripegawai'       =>'Kategori Pegawai',
            'keadaanmasuk'          =>'Keadaan Masuk',
            'kelompokjabatan'       =>'Kelompok Jabatan',
            'kelompokpemeriksaanlab'=>'Kelompok Pemeriksaan Lab',
            'kelompokumur'          =>'Kelompok Umur',
            'kondisipulang'         =>'Kondisi Pulang',
            'kunjungan'             =>'Kunjungan',
            'metodebt'              =>'Metode BT',
            'metodekb'              =>'Metode KB',
            'namadepan'             =>'Nama Depan',
            'namahari'              =>'Nama Hari',
            'namakepemilikanrs'     =>'Nama Kepemilikan RS',
            'obatalkes_golongan'    =>'Golongan Obat Alkes',
            'obatalkes_kadarobat'   =>'Obat Alkes Kadar Obat',    
            'obatalkes_kategori'    =>'Kategori Obat Alkes',
            'pengantar'             =>'Pengantar',
            'pentahapanakreditasrs' =>'Pentahapan Akreditas RS',
            'posisijanin'           =>'Posisi Janin',
            'prosedurmasukri'       =>'Prosedur Masuk RI',
            'rhesus'                =>'Rhesus',
            'satuanbarang'          =>'Satuan Barang',
            'satuanhasillab'        =>'Satuan Hasil Lab',
            'satuankekuatan'        =>'Obat Alkes Satukan Kekuatan',
            'satuantindakan'        =>'Satuan Tindakan',
            'sifatsuratizin'        =>'Sifat Surat Izin',
            'sikluskegiatanbt'      =>'Siklus Kegiatab BT',
            'statusbooking'         =>'Status Booking',
            'statusakreditasrs'     =>'Status Akreditas RS',
            'statuskamar'           =>'Status Kamar',
            'statuskepemilikanrs'   =>'Status Kepemilikan RS',
            'statusmasuk'           =>'Status Masuk',
            'statuspasien'          =>'Status Pasien',
            'statusperiksa'         =>'Status Periksa',
            'statusperkawinan'      =>'Status Perkawinan',
            'statusrekammedis'      =>'Status Rekam Medis',
            'statusrsswasta'        =>'Status RS Swasta',
            'statusasuransi'        =>'Status Asuransi',
            'statusMakanan'         =>'Status Makanan',
            'transportasi'          =>'Transportasi',
            'warganegara'           =>'Warga Negara',
            
        );
        asort($daftar);
        return $daftar;
    }
    
    public static function ukuranKertas()
    {
        $daftar = array(
            'A3'=>'A3',
            'A4'=>'A4',
            'A5'=>'A5',
        );
        asort($daftar);
        return $daftar;
    }
    public static function jenisPenjualan()
    {
        $jenispenjualan = array(
            'PENJUALAN RESEP'=>'PENJUALAN RESEP ',
            'PENJUALAN RESEP LUAR'=>'PENJUALAN RESEP LUAR',
            'PENJUALAN BEBAS'=>'PENJUALAN BEBAS',
        );
        asort($jenispenjualan);
        return $jenispenjualan;
    }
    
    public static function statusKonfirmasiBooking()
    {
        $statuskonfirmbooking = array(
            'SUDAH KONFIRMASI'=>'SUDAH KONFIRMASI',
            'BELUM KONFIRMASI'=>'BELUM KONFIRMASI',
            'BATAL BOOKING'=>'BATAL BOOKING',
        );
        asort($statuskonfirmbooking);
        return $statuskonfirmbooking;
    }
    
    public static function statusKonfirmasiAsuransi()
    {
        $statuskonfirmasuransi = array(
            'SUDAH DIKONFIRMASI'=>'SUDAH DIKONFIRMASI',
            'BELUM DIKONFIRMASI'=>'BELUM DIKONFIRMASI',
        );
        asort($statuskonfirmasuransi);
        return $statuskonfirmasuransi;
    }
    
    public static function keteranganPasienPulang()
    {
        $keteranganPasienPulang = array(
            'SUDAH PULANG'=>'SUDAH PULANG',
            'PASIEN MENUNGGU DI RUANGAN'=>'PASIEN MENUNGGU DI RUANGAN',
            'BOOKING'=>'BOOKING',
        );
        asort($keteranganPasienPulang);
        return $keteranganPasienPulang;
    }
    
    public static function namaHari()
    {
        $namaHari = array(
            'SENIN'=>'Senin',
            'SELASA'=>'Selasa',
            'RABU'=>'Rabu',
            'KAMIS'=>'Kamis',
            'JUMAT'=>'Jumat',
            'SABTU'=>'Sabtu',
            'MINGGU'=>'Minggu',
        );
        return $namaHari;
    }

    public static function namaBulan($bulan)
    {
        switch($bulan)
        {
            case 1 : $namaBln = "Januari";
            break;
            case 2 : $namaBln = "Pebruari";
            break;
            case 3 : $namaBln = "Maret";
            break;
            case 4 : $namaBln = "April";
            break;
            case 5 : $namaBln = "Mei";
            break;
            case 6 : $namaBln = "Juni";
            break;
            case 7 : $namaBln = "Juli";
            break;
            case 8 : $namaBln = "Agustus";
            break;
            case 9 : $namaBln = "September";
            break;
            case 10: $namaBln = "Oktober";
            break;
            case 11: $namaBln = "Nopember";
            break;
            case 12: $namaBln = "Desember";
            break;
        }


        return $namaBln;
    }
    
    public static function posisiKertas()
    {
        $daftar = array(
            'L'=>'Landscape',
            'P'=>'Portrait',
        );
        asort($daftar);
        return $daftar;
    }
    
    public static function tahun()
    {
        $dataTahun =array();
        $tahunSekarang=date('Y');
        for($i=1; $i<=10; $i++)
            {
                $dataTahun[$tahunSekarang]=$tahunSekarang;
                $tahunSekarang--;
            }
            asort($dataTahun);
            return $dataTahun;    
    }
    
    public static function listAngka20()
    {
        for ($i = 1; $i <= 20; $i++) {
            $angka[$i] = $i;
        } 
        
        return $angka;
    }
    public static function listTakaran()
    {
        for($i=1;$i<=3;$i++) {
            $result['id'] = "$i";
            if($i==1) $angka[$i] = "1";
            if($i==2) $angka[$i] = "1/2";
            if($i==3) $angka[$i] = "1/3";
        }
        
        return $angka;
    }
    
    public static function textAngka($data = null){
        if ($data == null){
            for($i=1;$i<=10;$i++) {
                    $result[$i]['id'] = "$i";
                    if($i==1) $result[$i]['name'] = "Kesatu";
                    if($i==2) $result[$i]['name'] = "Kedua";
                    if($i==3) $result[$i]['name'] = "Ketiga";
                    if($i==4) $result[$i]['name'] = "Keempat";
                    if($i==5) $result[$i]['name'] = "Kelima";
                    if($i==6) $result[$i]['name'] = "Keenam";
                    if($i==7) $result[$i]['name'] = "Ketujuh";
                    if($i==8) $result[$i]['name'] = "Kedelapan";
                    if($i==9) $result[$i]['name'] = "Kesembilan";
                    if($i==10) $result[$i]['name'] = "Kesepuluh";

            }
        } else {
            if($data==1) $result = "Kesatu";
            if($data==2) $result = "Kedua";
            if($data==3) $result = "Ketiga";
            if($data==4) $result = "Keempat";
            if($data==5) $result = "Kelima";
            if($data==6) $result = "Keenam";
            if($data==7) $result = "Ketujuh";
            if($data==8) $result = "Kedelapan";
            if($data==9) $result = "Kesembilan";
            if($data==10) $result = "Kesepuluh";
        }
        return $result;
    }
    
    public static function pathImagePengumumanUploaded()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pengumuman/';
    }
    
    public static function pathImagePengumumanUploadedThumb()
    {
        return Yii::getPathOfAlias('webroot').'/data/images/pengumuman/thumbs/';
    }
    
    public static function urlImagePengumumanUploaded()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pengumuman/';
    }
    
    public static function urlExcel()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/excel/template/';
    }
    
    public static function urlImagePengumumanUploadedThumb()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/pengumuman/thumbs/';
    }
    
     public static function urliconmenu()
    {
        return Yii::app()->getBaseUrl('webroot').'/css/images/';
    }
    
    public static function pathImageErrorAdmin()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/images/';
    }

    public static function pathVideo()
    {
        return Yii::app()->getBaseUrl('webroot').'/data/video/';
    }
    
    public static function statusBayar(){
        return array(
            'belum'=>'BELUM BAYAR',
            'sudah'=>'SUDAH BAYAR',
        );
    }
    
    public static function daftarTindakan(){
        return array(
            'nama'=>'Nama Tindakan',
            'kode'=>'Kode Tindakan',
        );
    }
    
    /**
     * this params used in gauge meter for range interval
     * last value used in max of gauge meter
     * @return type array
     */
    public static function intervalSpedometerGrafik(){
        return array(
            10,30,40,100
        );
    }
    
    public static function konfigGudangFarmasi($params=null){
        $result =  array(
            'stokminus' => false,
        );
        
        if (isset($params)){
            if (isset($result[$params]))
                $result = $result[$params];
        }
        
        return $result;
    }
    
    /**
     * konfigurasi ip finger dan key
     * @return array
     */
    public static function konfigurasiIPFingerPrint(){
        return AlatfingerM::model()->findAll();
    }
    
    
    /** parameter dengan konfigurasi terbaru **/
    public static function pesanAlert($urutan)
    {
        $status = array(
            1 => 'STOK OBAT HABIS',
            2 => 'TESTING ERROR',
        );
        return $status[$urutan];
    }
    
    public static function komponenTarif($index = null)
    {
        $komponen = array(
            'jasa_rumah_sakit' => 1,
            'jasa_rumah_paramedis' => 2,
            'jasa_keperawatan' => 3,
            'jasa_fisioteraphy' => 4,
            'jasa_medis' => 5,
            'total_tarif' => 6,
            'jasa_bhp' => 19,
            'jasa_operator_bedah' => 21,
            'jasa_asisten_operator' => 23,
            'jasa_dokter_anestesi' => 12,
            'jasa_asisten_anestesi' => 13,
            'jasa_sewa_alat' => 26,
            'jasa_bahan_alat' => 15,
            'remunerasi_dokter_anastesi' => 47,
            'remunerasi_paramedis_2' => 48,
            'remunerasi_operator_bedah' => 49,
            'remunerasi_paramedis' => 44,
            'jasa_perujuk'=>31,
            'jasa_prod_analisrad'=>51,
            'jasa_insentif'=>27,
            'jasa_dokter_lab'=>32,
            'jasa_pembaca'=>33,
        );
        if($index == null)
        {
            return $komponen;
        }else{
            return $komponen[$index];
        }
        
    }
    
    public static function jenisCetakan($jenis)
    {
        $jenisCetakan = array(
            'kwitansi'=>'KWITANSI',
            'rincian_tagihan'=>'RINCIAN TAGIHAN'
        );
        return $jenisCetakan[$jenis];
    }

    public static function monitoringrefresh() {
        $konfigsystem = KonfigsystemK::model()->find();
        $monitoring = $konfigsystem->monitoringrefresh*6;
        $monitoringtime = $monitoring.'0000';
        return $monitoringtime;
    }

    public static function komponenGaji($index = null)
    {
        $komponengaji = array(
            'gaji_pokok' => 9,
            'tunjangan_harian' => 10,
            'tunjangan_jabatan' => 11,
            'tunjangan_fungsional' => 12,
            'tunjangan_khusus' => 13,
            'tunjangan_sosial' => 14,
            'pph_pasal21' => 15,
            'tunjangan_essensial' => 16,
            'tunjangan_pensiun' => 17,
            'tunjangan_kesehatan' => 18,
            'tunjangan_kompetensi' => 19,
            'tunjangan_kemahalan' => 20,
            'insentif' => 21,
            'jasa_lainnya' => 22,
            'lembur' => 23,
            'koperasi' => 24,
            'jamsostek' => 25,
            'potongan_pensiun'=>26,
            'potongan_kesehatan'=>27,
            'bri'=>28,
            'bsj'=>29,
            'btn'=>30,
            'potongan_intern'=>31,
            'apotek'=>32,
            'laboratorium'=>33,
            'tmkrj'=>34,
            'pajak'=>35,
            'lain_lain'=>36,
        );
        if($index == null)
        {
            return $komponengaji;
        }else{
            return $komponengaji[$index];
        }
        
    }

}

?>
