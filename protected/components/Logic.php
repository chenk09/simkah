<?php
class Logic{
    public static function generateKey($params){
        $key = md5(implode(",", $params) . date('YmdHis'));
        return $key;
    }
    public static function getApprovals($params){
        $idx = 1;
		$_criteria = new CDbCriteria;
		$_criteria->addCondition('t.id_transaksi = :id_transaksi AND t.app_id = :app_id');
		$_criteria->join  = 'INNER JOIN (
            SELECT MAX(idx) idx FROM ' . TrxApproval::model()->tableName() . ' app
            WHERE app.id_transaksi = :id_transaksi AND app.app_id = :app_id AND status = \'A\'
        )trx_app ON trx_app.idx = t.idx';
		$_criteria->params[':id_transaksi'] =$params['id_transaksi'];
		$_criteria->params[':app_id'] = $params['app_id'];
        $trxApproval = TrxApproval::model()->find($_criteria);
        if($trxApproval){
            $idx = ($trxApproval->idx + 1);
        }
		$criteria = new CDbCriteria;
		$criteria->select = 'auth.userid AS userid, t.*';
		$criteria->join = ' INNER JOIN ' . AssignmentsK::model()->tableName() . ' auth ON auth.itemname = t.approver';
        $criteria->addCondition('t.idx = :idx AND t.app_id = :app_id');
        $criteria->params[':idx'] = $idx;
        $criteria->params[':app_id'] = $params['app_id'];
		$approval = MapApproval::model()->findAll($criteria);
        $approvals = array();
        foreach($approval as $key => $value){
            $approvals[$value->userid] = array(
                'approver'=>$value->approver,
                'idx'=>$value->idx
            );
        }
        return $approvals;
    }
    public static function setApprovals($params){
        $apps = MapApproval::model()->countByAttributes(array(
            'app_id'=>$params['app_id']
        ));
        if($apps > 0){
            $approvals = self::getApprovals(array(
                'app_id' => $params['app_id'],
                'id_transaksi' => $params['id_transaksi']
            ));
            foreach($approvals as $key => $value){
                $id_approval = self::generateKey(array(
                    'app_id' => $key,
                    'id_transaksi' => $pesan_barang->pesanbarang_id
                ));
                $app = new TrxApproval;
                $app->id_approval = $id_approval;
                $app->id_pegawai = $key;
                $app->approver = $value['approver'];
                $app->status = 'O';
                $app->id_transaksi = $params['id_transaksi'];
                $app->app_id = $params['app_id'];
                $app->idx = $value['idx'];
                if(!$app->save()){
                    $err_pesan = array();
                    foreach($app->getErrors() as $a=>$b){
                        $err_pesan[$a] = implode($b, ',');
                    }
                    throw new Exception(implode($err_pesan, '<br>'), 500);
                }
            }
        }else{
            throw new Exception('mapping petugas approval tidak tersedia, silahkan hubungi admin aplikasi', 500);
        }
    }
    public static function cekGolonganUmur($idGolonganUmur)
    {
        switch ($idGolonganUmur) {
            case 1:return 'umur_0_28hr';
            case 2:return 'umur_28hr_1thn';
            case 3:return 'umur_1_4thn';
            case 4:return 'umur_5_14thn';
            case 5:return 'umur_15_24thn';
            case 6:return 'umur_25_44thn';
            case 7:return 'umur_45_64thn';
            case 8:return 'umur_65';

            default:
                break;
        }

    }
    public static function getKasusDiagnosa($idPasien, $idDiagnosa)
    {
        $modMorbiditas = PasienmorbiditasT::model()->findByAttributes(array('pasien_id'=>$idPasien,'diagnosa_id'=>$idDiagnosa));
        if(!empty($modMorbiditas))
            return 'KASUS LAMA';
        else
            return 'KASUS BARU';
    }

}
?>