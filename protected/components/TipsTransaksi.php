<?php
/**
 * Class untuk mengenerate Tips/Petunjuk transaksi
 */

class TipsTransaksi extends CWidget
{   
    public $type;
    public function run()
    {
         // this method is called by CController::endWidget()
         switch ($this->type) {
             case 'admin' : $this->render('tipsMasterData/tipsMasterDataAdmin'); break;
             case 'create' : $this->render('tipsMasterData/tipsMasterDataCreate'); break;
             case 'update' : $this->render('tipsMasterData/tipsMasterDataUpdate'); break;
             case 'list' : $this->render('tipsMasterData/tipsMasterDataList'); break;
             default : $this->render('tipsMasterData/tipsMasterDataDefault'); break;
         }
         
    }
}
?>
