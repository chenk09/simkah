<?php

class MyFunction
{

    /*
     * menghitung hari berdasarkan periode tanggal
     * $dateFrom tanggal awal
     * $dateTo tanggal akhir
     */

    
    public static function insertDetailJurnal($params)
    {
        /*
         * FORMAT PARAM yang di kirim *
            $params = array(
                'modJurnalRekening' => $modJurnalRekening, 
                'jenis_simpan'=>$_REQUEST['jenis_simpan'], 
                'RekeningakuntansiV'=>$data_parsing['RekeningakuntansiV'],
            );
         * 
         */
        
        $valid = true;
        $modJurnalPosting = null;
        if($params['jenis_simpan'] == 'posting')
        {
            $modJurnalPosting = new AKJurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
            }
        }
        
        
        $modJurnalRekening = $params['modJurnalRekening'];
        $rekeningakuntansi = $params['RekeningakuntansiV'];
        for($i=0;$i<count($rekeningakuntansi);$i++)
        {
            $model[$i] = new AKJurnaldetailT();
            $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $model[$i]->uraiantransaksi = $rekeningakuntansi[$i]['nama_rekening'];
            $model[$i]->saldodebit = $rekeningakuntansi[$i]['saldodebit'];
            $model[$i]->saldokredit = $rekeningakuntansi[$i]['saldokredit'];
            $model[$i]->nourut = $i+1;
            $model[$i]->rekening1_id = $rekeningakuntansi[$i]['struktur_id'];
            $model[$i]->rekening2_id = $rekeningakuntansi[$i]['kelompok_id'];
            $model[$i]->rekening3_id = $rekeningakuntansi[$i]['jenis_id'];
            $model[$i]->rekening4_id = $rekeningakuntansi[$i]['obyek_id'];
            $model[$i]->rekening5_id = $rekeningakuntansi[$i]['rincianobyek_id'];
            $model[$i]->catatan = "";
            if($model[$i]->validate())
            {
                $model[$i]->save();
            }else{
                $this->pesan = $model[$i]->getErrors();
                $valid = false;
                break;
            }                
        }
        return $valid;        
    }
    
    public static function insertNotifikasi($params){
        $is_simpan = false;
        $model = new NofitikasiR;
        $model->attributes = $params;
		
        $criteria = new CDbCriteria;
        $criteria->compare('instalasi_id',$params['instalasi_id']);
        $criteria->compare('modul_id',$params['modul_id']);
        $criteria->compare('LOWER(isinotifikasi)',strtolower($params['isinotifikasi']),true);
        $criteria->compare('create_ruangan',$params['create_ruangan']);
        $criteria->addCondition("DATE(tglnotifikasi) = DATE(NOW()) AND isread = false");
        $is_exist = NofitikasiR::model()->find($criteria);
        if(!$is_exist)
        {
            if($model->save()){
                $is_simpan = true;
            }
        }else{
            $attributes = array(
                'update_time' => date('Y-m-d H:i:s'),
                'update_loginpemakai_id' => Yii::app()->user->id,
            );
            $update = $model::model()->updateByPk($is_exist['nofitikasi_id'], $attributes);
            if($update){
                $is_simpan = true;
            }
        }
        return $is_simpan;
    }
    
    public static function hitungHari($dateFrom,$dateTo=''){
        $dateTo = (!empty($dateTo)) ? strtotime($dateTo) : time(); // or your date as well
        $dateFrom = strtotime($dateFrom);
        $datediff = $dateTo - $dateFrom;
        $hari = floor($datediff/(60*60*24)) + Params::HARI_RAWAT;

        return $hari;
    }
    
    public static function calculate_string( $mathString )    {
        $mathString = trim($mathString);     // trim white spaces
        $mathString = preg_replace('/[^0-9\-\+\/\*]/i', '', $mathString);    // remove any non-numbers chars; exception for math operators
        $mathString = (!empty($mathString)) ? $mathString : true;
        $compute = create_function("", "return (" . $mathString . ");" );
        return 0 + $compute();
    }

    public static function formatNumberForDB($strNumber)
    {
        return str_replace(',', '', $strNumber);
    }
    
    public static function formatNumber($number,$precision=0)
    {
        /*
        $format = new CNumberFormatter('id');
        $result = $format->format('#,##0', $number);
         */
        $result = number_format($number,$precision,'.',',');
        
        return $result;
    }

    function number_unformat($number, $force_number = true, $dec_point = '.', $thousands_sep = ',') {
        if ($force_number) {
            $number = preg_replace('/^[^\d]+/', '', $number);
        } else if (preg_match('/^[^\d]+/', $number)) {
            return false;
        }
        $type = (strpos($number, $dec_point) === false) ? 'int' : 'float';
        $number = str_replace(array($dec_point, $thousands_sep), array('.', ''), $number);
        settype($number, $type);
        
        return $number;
    }
    
    /*fungsi untuk generate filter / criteria pada model untuk grafik
     * $model adalah model yang akan digunakan untuk grafik
     * $type adalah filter akan digunakan sebagai x-axis('data') atau group('tick'), default type sebagai x-axis('data')
     * $addCols variable untuk column tmbahan, typenya mix, diantaranya untuk order dll,
     */
    public static function criteriaGrafik1($model, $type='data', $addCols = array()){
        $criteria = new CDbCriteria;
        $criteria->select = 'count(pendaftaran_id) as jumlah';
        if ($_GET['filter'] == 'carabayar') {
            if (!empty($model->penjamin_id)) {
                $criteria->select .= ', penjamin_nama as '.$type;
                $criteria->group .= 'penjamin_nama';
            } else if (!empty($model->carabayar_id)) {
                $criteria->select .= ', penjamin_nama as '.$type;
                $criteria->group = 'penjamin_nama';
            } else {
                $criteria->select .= ', carabayar_nama as '.$type;
                $criteria->group = 'carabayar_nama';
            }
        } else if ($_GET['filter'] == 'wilayah') {
            if (!empty($model->kelurahan_id)) {
                $criteria->select .= ', kelurahan_nama as '.$type;
                $criteria->group .= 'kelurahan_nama';
            } else if (!empty($model->kecamatan_id)) {
                $criteria->select .= ', kelurahan_nama as '.$type;
                $criteria->group .= 'kelurahan_nama';
            } else if (!empty($model->kabupaten_id)) {
                $criteria->select .= ', kecamatan_nama as '.$type;
                $criteria->group .= 'kecamatan_nama';
            } else if (!empty($model->propinsi_id)) {
                $criteria->select .= ', kabupaten_nama as '.$type;
                $criteria->group .= 'kabupaten_nama';
            } else {
                $criteria->select .= ', propinsi_nama as '.$type;
                $criteria->group .= 'propinsi_nama';
            }
        }
        
        if (!isset($_GET['filter'])){
            $criteria->select .= ', propinsi_nama as '.$type;
            $criteria->group .= 'propinsi_nama';
        }
        
        if (count($addCols) > 0){
            if (is_array($addCols)){
                foreach ($addCols as $i => $v){
                    $criteria->group .= ','.$v;
                    $criteria->select .= ','.$v.' as '.$i;
                }
            }            
        }
        
        return $criteria;
    }
    
    public static function criteriaGrafikJurnal($model, $type='data', $addCols = array()){
        $criteria = new CDbCriteria;
        $criteria->select = 'count(jenisjurnal_id) as jumlah';
        
        
//        if (!isset($_GET['AKLaporanJurnalV'])){
            $criteria->select .= ',saldokredit as '.$type.', saldodebit as '.$type;
            $criteria->group .= 'saldodebit,saldokredit';
//        }
        
        if (count($addCols) > 0){
            if (is_array($addCols)){
                foreach ($addCols as $i => $v){
                    $criteria->group .= ','.$v;
                    $criteria->select .= ','.$v.' as '.$i;
                }
            }            
        }
        
        return $criteria;
    }
    
    public static function criteriaGrafikBukuKas($model, $type='data', $addCols = array()){
        $criteria = new CDbCriteria;
        $criteria->select = 'count(rekening1_id) as jumlah';
        
        
//        if (!isset($_GET['AKLaporanJurnalV'])){
            $criteria->select .= ',saldokredit as '.$type.', saldodebit as '.$type;
            $criteria->group .= 'saldodebit,saldokredit';
//        }
        
        if (count($addCols) > 0){
            if (is_array($addCols)){
                foreach ($addCols as $i => $v){
                    $criteria->group .= ','.$v;
                    $criteria->select .= ','.$v.' as '.$i;
                }
            }            
        }
        
        return $criteria;
    }
    /**
     * Create Message for exception
     * @param object $model
     * @return string 
     */
    public static function exceptionMessage($model){
        $error = $model->getErrors();
        $message = '<ul>';
        foreach ($error as $i=>$v){
            $message .= '<li> ';
            foreach ($v as $x){
                $message .= $x;
            }
            $message .='</li>';
            
        }
        $message .= '</ul>';
        return $message;
    }
    
    /**
     *
     * @param type $model
     * @return string 
     */
    public static function infoMessage($params){
        $message = null;
        if (is_array($params)){
            $message = '<ul>';
            if (count($params)>0){
                foreach ($params as $i=>$v){
                    $message .= '<li>'.$v;
                    $message .='</li>';
                }
            }
            $message .= '</ul>';
        }
        
        return $message;
    }

    public function terbilang($x, $style=4, $strcomma=",")
    {
        if ($x < 0)
        {
            $result = "minus " . trim($this->ctword($x));
        } else {
            $arrnum = explode("$strcomma", $x);
            $arrcount = count($arrnum);
            if ($arrcount == 1) {
                $result = trim($this->ctword($x));
            } else if ($arrcount > 1) {
                $result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
            }
        }
        switch ($style)
        {
            case 1: //1=uppercase  dan
                $result = strtoupper($result);
                break;
            case 2: //2= lowercase
                $result = strtolower($result);
                break;
            case 3: //3= uppercase on first letter for each word
                $result = ucwords($result);
                break;
            default: //4= uppercase on first letter
                $result = ucfirst($result);
                break;
        }
        return $result;
    }

    public function ctword($x)
    {
        $x = abs($x);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($x < 12) {
            $temp = " " . $number[$x];
        } else if ($x < 20) {
            $temp = $this->ctword($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->ctword($x - 100);
        } else if ($x < 1000) {
            $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->ctword($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
        }
        return $temp;
    }
    
    /**
     * menampilkan nama hari berdasarkan tanggal
     * @param type $date ()
     */
    public function getNamaHari($date = null){
        if(empty($date)){
            $date = date('Y-m-d');
            $hari = date('w');
        }else{
            $hari = date('w', strtotime($date));
        }
        switch($hari){     
            case 0 : {
                        $hari='Minggu';
                    }break;
            case 1 : {
                        $hari='Senin';
                    }break;
            case 2 : {
                        $hari='Selasa';
                    }break;
            case 3 : {
                        $hari='Rabu';
                    }break;
            case 4 : {
                        $hari='Kamis';
                    }break;
            case 5 : {
                        $hari="Jum'at";
                    }break;
            case 6 : {
                        $hari='Sabtu';
                    }break;
            default: {
                        $hari='UnKnown';
                    }break;
        }
        return $hari;
    }
    /**
     * menampilkan semua tahun dari $sebelumthn tahun sampai $setelahthn tahun dari tahun sekarang
     * @param type $sebelumthn
     * @param type $setelahthn
     */
    public static function getSemuaTahun($sebelumthn = null, $setelahthn = null){
        $rangeArr = range(2000,date("Y"));
        if(isset($sebelumthn) && empty($setelahthn))
            $rangeArr = range(date("Y", strtotime("-".$sebelumthn." years")),date("Y"));
        else if(empty($sebelumthn) && isset($setelahthn))
            $rangeArr = range(date("Y"),date("Y", strtotime("+".$setelahthn." years")));
        else if(isset($setelahthn) && isset($setelahthn))
            $rangeArr = range(date("Y", strtotime("-".$sebelumthn." years")),date("Y", strtotime("+".$setelahthn." years")));
        
        $tahunArr = array();
        foreach($rangeArr as $value){
            $tahunArr[$value] = $value;
        }
        return $tahunArr;
    }
}
?>
