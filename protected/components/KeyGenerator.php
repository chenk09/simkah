<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class KeyGenerator
{


      /**
     * Fungsi Untuk Mengenerate No HasilPemeriksaanLAB Umum Secara Otomatis
     * @return string
     */
    public static function noMasukKamar($idRuangan)
    {
        $tgl = date('Y-m-d');
        $ruangan = RuanganM::model()->findByPk($idRuangan);
        $namaRuangan = null;
        if ((boolean)count($ruangan))
            $namaRuangan=$ruangan->ruangan_nama;
        $hurufAwalRuangan = substr($namaRuangan,0,1);
        $sql= "SELECT CAST(MAX(SUBSTR(nomasukkamar,9,3)) AS integer) nomasukkamar FROM masukkamar_t
                      WHERE date(tglmasukkamar)='".$tgl."'";
        $noMasukKamar = Yii::app()->db->createCommand($sql)->queryRow();
        $noMasukKamarBaru = $hurufAwalRuangan.date('Ymd').(str_pad($noMasukKamar['nomasukkamar']+1, 3, 0,STR_PAD_LEFT));
        return $noMasukKamarBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Retur Secara Otomatis
     * @return string
     */
    public static function noRetur()
    {
        $tgl = date('Y-m-d');

//        $sql = "SELECT CAST(MAX(SUBSTR(noretur,14,4)) AS integer) noretur FROM returpembelian_t
//                      WHERE date(tglretur)='".$tgl."'";
        $sql = "SELECT CAST(MAX(SUBSTRING(noretur FROM CHAR_LENGTH(TRIM(noretur))-3)) AS integer) noretur
                FROM returpembelian_t
                WHERE date(tglretur)='".$tgl."'";
        $noRetur = Yii::app()->db->createCommand($sql)->queryRow();
        $noReturBaru ='RETUR'.date('Ymd').(str_pad($noRetur['noretur']+1, 4, 0,STR_PAD_LEFT));
        return $noReturBaru;
    }


    /**
     * Fungsi Untuk Mengenerate No Retur Secara Otomatis
     * @return string
     */
    public static function noReturResep()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTRING(noreturresep FROM CHAR_LENGTH(TRIM(noreturresep))-3)) AS integer) noretur
                FROM returresep_t
                WHERE date(tglretur)='".$tgl."'";
        $noRetur = Yii::app()->db->createCommand($sql)->queryRow();
        $noReturBaru ='RETRESEP'.date('Ymd').(str_pad($noRetur['noretur']+1, 4, 0,STR_PAD_LEFT));
        return $noReturBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Perencanaaan Obat Alkes Secara Otomatis
     * @return string
     */
    public static function noFaktur()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nofaktur,15,4)) AS integer) nofaktur FROM fakturpembelian_t
                      WHERE date(tglfaktur)='".$tgl."'";
        $noFaktur = Yii::app()->db->createCommand($sql)->queryRow();
        $noFakturBaru ='FAKTUR'.date('Ymd').(str_pad($noFaktur['nofaktur']+1, 4, 0,STR_PAD_LEFT));
        return $noFakturBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Perencanaaan Obat Alkes Secara Otomatis
     * @return string
     */
    public static function noPerencanaan()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(noperencnaan,16,4)) AS integer) noperencnaan FROM rencanakebfarmasi_t
                      WHERE date(tglperencanaan)='".$tgl."'";
        $noPerencanaan = Yii::app()->db->createCommand($sql)->queryRow();
        $noPerencanaanBaru ='RENCANA'.date('Ymd').(str_pad($noPerencanaan['noperencnaan']+1, 4, 0,STR_PAD_LEFT));
        return $noPerencanaanBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Perencanaaan Obat Alkes Secara Otomatis
     * @return string
     */
    public static function noTerimaBarang()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(noterima,15,4)) AS integer) noterima FROM penerimaanbarang_t
                      WHERE date(tglterima)='".$tgl."'";
        $noTerimaBarang = Yii::app()->db->createCommand($sql)->queryRow();
        $noTerimaBarangBaru ='TERIMA'.date('Ymd').(str_pad($noTerimaBarang['noterima']+1, 4, 0,STR_PAD_LEFT));
        return $noTerimaBarangBaru;
    }


     /**
     * Fungsi Untuk Mengenerate No Perencanaaan Obat Alkes Secara Otomatis
     * @return string
     */
    public static function noPenawaran()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nosuratpenawaran,18,4)) AS integer) nosuratpenawaran FROM permintaanpenawaran_t
                      WHERE date(tglpenawaran)='".$tgl."'";
        $noPenawaran = Yii::app()->db->createCommand($sql)->queryRow();
        $noPenawaranBaru ='PENAWARAN'.date('Ymd').(str_pad($noPenawaran['nosuratpenawaran']+1, 4, 0,STR_PAD_LEFT));
        return $noPenawaranBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Permintaan Obat Alkes Secara Otomatis
     * @return string
     */
    public static function noPembelian()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nopermintaan,18,4)) AS integer) nopermintaan FROM permintaanpembelian_t
                      WHERE date(tglpermintaanpembelian)='".$tgl."'";
        $noPerencanaan = Yii::app()->db->createCommand($sql)->queryRow();
        $noPerencanaanBaru ='PEMBELIAN'.date('Ymd').(str_pad($noPerencanaan['nopermintaan']+1, 4, 0,STR_PAD_LEFT));
        return $noPerencanaanBaru;
    }

    public static function noPermintaan()
    {
        $tgl = date('Y-m-d');
        $sql = "SELECT CAST(MAX(SUBSTR(no_permintaan_beli,18,4)) AS integer) no_permintaan_beli FROM permintaan_beli_barang_t
        WHERE date(tanggal_pengajuan) = '".$tgl."'";
        $noPerencanaan = Yii::app()->db->createCommand($sql)->queryRow();
        $noPerencanaanBaru = 'PERMINTAAN'.date('Ymd').(str_pad($noPerencanaan['no_permintaan_beli']+1, 4, 0,STR_PAD_LEFT));
        return $noPerencanaanBaru;
    }

      /**
     * Fungsi Untuk Mengenerate No HasilPemeriksaanLAB Umum Secara Otomatis
     * @return string
     */
    public static function noSediaanPA()
    {
        $tgl = date('Y-m-d');

        $sqlSediaanPA = "SELECT CAST(MAX(SUBSTR(nosediaanpa,9,3)) AS integer) nosediaanpa FROM hasilpemeriksaanpa_t
                      WHERE date(tglperiksapa)='".$tgl."'";
        $noSediaanPA = Yii::app()->db->createCommand($sqlSediaanPA)->queryRow();
        $kodeSediaanPABaru = date('Ymd').(str_pad($noSediaanPA['nosediaanpa']+1, 3, 0,STR_PAD_LEFT));
        return $kodeSediaanPABaru;
    }
     /**
     * Fungsi Untuk Mengenerate kelompokUmurFromID Secara Otomatis
     * @return string
     */
    public static function kelompokUmurFromID($id)
    {

        $sqlKelompokUmur = "SELECT kelompokumur_nama FROM kelompokumur_m WHERE kelompokumur_id=".$id."";
        $KelompokUmur = Yii::app()->db->createCommand($sqlKelompokUmur)->queryRow();
        return $KelompokUmur['kelompokumur_nama'];
    }
    /**
     * Fungsi Untuk Mengenerate No HasilPemeriksaanLAB Umum Secara Otomatis
     * @return string
     */
    public static function noHasilPemeriksaanLK()
    {
        $tgl = date('Y-m-d');

        $sqlPemeriksaan = "SELECT CAST(MAX(SUBSTR(nohasilperiksalab,9,3)) AS integer) nohasilperiksalab2 FROM hasilpemeriksaanlab_t
                      WHERE date(tglhasilpemeriksaanlab)='".$tgl."'";
        $noPemeriksaan = Yii::app()->db->createCommand($sqlPemeriksaan)->queryRow();
        $kodePemeriksaanBaru = $tgl.(str_pad($noPemeriksaan['nohasilperiksalab2']+1, 3, 0,STR_PAD_LEFT));
        return $kodePemeriksaanBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Antrian Umum Secara Otomatis
     * @return string
     */
    public static function noAntrianUmum()
    {
        $tgl = date('Y-m-d');
        $kodeUmum = 'U';
        $sqlUmum = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) noumum FROM antrian_t
                      WHERE SUBSTR(noantrian,1,1) LIKE 'U' AND date(tglantrian)='".$tgl."'";
        $noUmum = Yii::app()->db->createCommand($sqlUmum)->queryRow();
        $kodeUmumBaru = $kodeUmum.(str_pad($noUmum['noumum']+1, 3, 0,STR_PAD_LEFT));
        return $kodeUmumBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Antrian ASKES Secara Otomatis
     * @return string
     */
    public static function noAntrianAskes()
    {
        $tgl = date('Y-m-d');
        $kodeAskes = 'A';
        $sqlAskes = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) noaskes FROM antrian_t
                      WHERE SUBSTR(noantrian,1,1) LIKE 'S' AND date(tglantrian)='".$tgl."'";
        $noAskes = Yii::app()->db->createCommand($sqlAskes)->queryRow();
        $kodeAskesBaru = $kodeAskes.(str_pad($noAskes['noaskes']+1, 3, 0,STR_PAD_LEFT));
        return $kodeAskesBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Antrian Jamsostek Secara Otomatis
     * @return string
     */
    public static function noAntrianJamsostek()
    {
        $tgl = date('Y-m-d');
        $kodeJamsostek = 'S';
        $sqlJamsostek = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) nojamsostek FROM antrian_t
                      WHERE SUBSTR(noantrian,1,1) LIKE 'S' AND date(tglantrian)='".$tgl."'";
        $noJamsostek = Yii::app()->db->createCommand($sqlJamsostek)->queryRow();
        $kodeJamsostekBaru = $kodeJamsostek.(str_pad($noJamsostek['nojamsostek']+1, 3, 0,STR_PAD_LEFT));
        return $kodeJamsostekBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Antrian Jamkesmas Secara Otomatis
     * @return string
     */
    public static function noAntrianJamkesmas()
    {
        $tgl = date('Y-m-d');
        $kodeJamkesmas = 'K';
        $sqlJamkesmas = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) nojamkesmas FROM antrian_t
                      WHERE SUBSTR(noantrian,1,1) LIKE 'K' AND date(tglantrian)='".$tgl."'";
        $noJamkesmas = Yii::app()->db->createCommand($sqlJamkesmas)->queryRow();
        $kodeJamkesmasBaru = $kodeJamkesmas.(str_pad($noJamkesmas['nojamkesmas']+1, 3, 0,STR_PAD_LEFT));
        return $kodeJamkesmasBaru;
    }

    public static function noRekamMedik($prefix='',$isPasienLuar='FALSE')
    {
        $sqlNoRM = "SELECT CAST(MAX(SUBSTRING(no_rekam_medik FROM CHAR_LENGTH(TRIM(no_rekam_medik))-5)) AS integer)
                    nop FROM pasien_m WHERE ispasienluar=$isPasienLuar";
        $genRM = Yii::app()->db->createCommand($sqlNoRM)->queryRow();
        $noRM = $prefix.str_pad($genRM['nop']+1, 6, 0,STR_PAD_LEFT);
        return trim($noRM);
    }

    public static function noRekamMedikPenunjang($prefix)
    {
        $sqlNoRM = "SELECT CAST(MAX(SUBSTRING(no_rekam_medik FROM CHAR_LENGTH(TRIM(no_rekam_medik))-5)) AS integer)
                    nop FROM pasien_m WHERE ispasienluar=TRUE";
        $genRM = Yii::app()->db->createCommand($sqlNoRM)->queryRow();
        $noRM = $prefix.str_pad($genRM['nop']+1, 6, 0,STR_PAD_LEFT);
        return trim($noRM);
    }

    public static function noRekamMedikGizi($prefix)
    {
        $sqlNoRM = "SELECT CAST(MAX(SUBSTRING(no_rekam_medik FROM CHAR_LENGTH(TRIM(no_rekam_medik))-5)) AS integer)
                    nop FROM pasien_m WHERE ispasienluar=TRUE";
        $genRM = Yii::app()->db->createCommand($sqlNoRM)->queryRow();
        $noRM = $prefix.str_pad($genRM['nop']+1, 6, 0,STR_PAD_LEFT);
        return trim($noRM);
    }

    public static function noPendaftaran($kode_instalasi = 'RJ')
    {
        $tgl = date('ymd');
        $kodeDaftar=$kode_instalasi.$tgl;
        $sqlNoDaftar = "SELECT CAST(MAX(SUBSTR(no_pendaftaran,9,12)) AS integer) nop FROM pendaftaran_t WHERE no_pendaftaran LIKE ('".$kodeDaftar."%')";
        $nopendaftaran = Yii::app()->db->createCommand($sqlNoDaftar)->queryRow();
        $kodeDaftar=$kodeDaftar.(str_pad($nopendaftaran['nop']+1, 4, 0,STR_PAD_LEFT));
        return $kodeDaftar;
    }
    /**
     *digunakan :
     * 1. pendaftaran laboratorium pasienluar
     * @return string
     */
    public static function noPengambilanSample()
    {
        $tgl = date('ymd');
        $kode_sample = 'SAMPLE';
        $kodeFixSample=$kode_sample.$tgl;
        $sqlNoSample = "SELECT CAST(MAX(SUBSTR(no_pengambilansample,13,15)) AS integer) nop FROM pengambilansample_t WHERE no_pengambilansample LIKE ('".$kodeFixSample."%')";
        $nosample = Yii::app()->db->createCommand($sqlNoSample)->queryRow();
        $kodeFixSample=$kodeFixSample.(str_pad($nosample['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kodeFixSample;
    }

    /**
     * Fungsi Untuk Mengenerate No Booking Kamar Secara Otomatis
     * @return string
     */
    public static function noBookingKamar()
    {
        $tgl = date('ymd');
        $kode_book = 'BOOK';
        $kodeFixBook=$kode_book.$tgl;
        $sqlNoBook = "SELECT CAST(MAX(SUBSTR(bookingkamar_no,11,14)) AS integer) nobook FROM bookingkamar_t WHERE bookingkamar_no LIKE ('".$kodeFixBook."%')";
        $noBook = Yii::app()->db->createCommand($sqlNoBook)->queryRow();
        $kodeFixBook = $kodeFixBook.(str_pad($noBook['nobook']+1, 3, 0,STR_PAD_LEFT));
        return $kodeFixBook;
    }

    /**
     *
     * @param string $tglLahir contoh [12 Okt 1998]
     * @return integer id golongan umur
     */
    public static function golonganUmur($tglLahir){
        $format = new CustomFormat;
        $tglLahir = $format->formatDateMediumForDB($tglLahir);
        $dob=$tglLahir; $today=date("Y-m-d");
        list($y,$m,$d)=explode('-',$dob);
        list($ty,$tm,$td)=explode('-',$today);
        if($td-$d<0){
            $day=($td+30)-$d;
            $tm--;
        }
        else{
            $day=$td-$d;
        }
        if($tm-$m<0){
            $month=($tm+12)-$m;
            $ty--;
        }
        else{
            $month=$tm-$m;
        }
        $year=$ty-$y;

        $umur = $year + $month/12 + $day/365;

         $golonganUmur = self::getGolonganUmur($umur);
         return $golonganUmur['golonganumur_id'];
    }

    /**
     *
     * @param string $tglLahir contoh [12 Okt 1998]
     * @return integer id kelompok umur
     */
    public static function kelompokUmur($tglLahir){
        $format = new CustomFormat;
        $tglLahir = $format->formatDateMediumForDB($tglLahir);
        $dob=$tglLahir; $today=date("Y-m-d");
        list($y,$m,$d)=explode('-',$dob);
        list($ty,$tm,$td)=explode('-',$today);
        if($td-$d<0){
            $day=($td+30)-$d;
            $tm--;
        }
        else{
            $day=$td-$d;
        }
        if($tm-$m<0){
            $month=($tm+12)-$m;
            $ty--;
        }
        else{
            $month=$tm-$m;
        }
        $year=$ty-$y;

        $umur = $year + $month/12 + $day/365;

         $kelompokUmur = self::getKelompokUmur($umur);
         return $kelompokUmur['kelompokumur_id'];
    }

    /**
     *
     * @param string $tglLahir contoh [12 Okt 1998]
     * @return integer id kelompok umur
     */
    public static function kelompokUmurNama($tglLahir){
        $format = new CustomFormat;
        $tglLahir = $format->formatDateMediumForDB($tglLahir);
        $dob=$tglLahir; $today=date("Y-m-d");
        list($y,$m,$d)=explode('-',$dob);
        list($ty,$tm,$td)=explode('-',$today);
        if($td-$d<0){
            $day=($td+30)-$d;
            $tm--;
        }
        else{
            $day=$td-$d;
        }
        if($tm-$m<0){
            $month=($tm+12)-$m;
            $ty--;
        }
        else{
            $month=$tm-$m;
        }
        $year=$ty-$y;

        $umur = $year + $month/12 + $day/365;

         $kelompokUmur = self::getKelompokUmur($umur);
         return strtoupper($kelompokUmur['kelompokumur_nama']);
    }

    public static function getKelompokUmur($umur){
         $sql = "select kelompokumur_id, kelompokumur_nama from kelompokumur_m where '".ceil($umur)."' between kelompokumur_minimal AND kelompokumur_maksimal";
         $result = Yii::app()->db->createCommand($sql)->queryRow();
         return $result;
    }

    /**
     *
     * @param type $no_pendaftaran
     * @param type $ruanganid
     * @return type string
     */
    public static function noAntrian($no_pendaftaran,$ruanganid)
    {
        if($ruanganid!=''){
            $tgl = date('Y-m-d');
            $kodeInst = substr($no_pendaftaran, 0,1);
            //$sqlAntrian = "select cast(max(substr(no_pendaftaran,9,12)) as integer) nop from pendaftaran_t where tgl_pendaftaran = '".$tgl."' AND ruangan_id = '".$ruanganid."' AND no_pendaftaran LIKE '".$kodeInst."'";
            //$sqlAntrian = "select max(no_urutantri) nourut from pendaftaran_t where tgl_pendaftaran = '".$tgl."' AND ruangan_id = '".$ruanganid."' AND no_pendaftaran LIKE '".$kodeInst."%'";
            $sqlAntrian = "select max(no_urutantri) nourut from pendaftaran_t where date(tgl_pendaftaran) = '".$tgl."' AND ruangan_id = '".$ruanganid."'";
            $nopendaftaran = Yii::app()->db->createCommand($sqlAntrian)->queryRow();
            $kodeAntrianr=(str_pad($nopendaftaran['nourut']+1, 3, 0,STR_PAD_LEFT));
            return $kodeAntrianr;
        } else {
            return null;
        }
    }

    public static function noMasukPenunjang($kode_instalasi = '')
    {
        $tgl = date('Y-m-d');
        $kodeDaftar=$kode_instalasi;
        $sqlNoDaftar = "SELECT CAST(MAX(SUBSTR(no_masukpenunjang,3,5)) AS integer) nom FROM pasienmasukpenunjang_t WHERE no_masukpenunjang LIKE ('".$kodeDaftar."%') AND DATE(tglmasukpenunjang) = '".$tgl."'";
        $nomasuk = Yii::app()->db->createCommand($sqlNoDaftar)->queryRow();
        $kodeDaftar=$kodeDaftar.(str_pad($nomasuk['nom']+1, 3, 0,STR_PAD_LEFT));
        return $kodeDaftar;
    }

    public static function noMasukPenunjangGZ($kode_instalasi = '')
    {
        $tgl = date('Y-m-d');
        $kodeDaftar=$kode_instalasi;
        $sqlNoDaftar = "SELECT CAST(MAX(SUBSTR(no_masukpenunjang,3,5)) AS integer) nom FROM pasienmasukpenunjang_t WHERE no_masukpenunjang LIKE ('".$kodeDaftar."%') AND DATE(tglmasukpenunjang) = '".$tgl."'";
        $nomasuk = Yii::app()->db->createCommand($sqlNoDaftar)->queryRow();
        $kodeDaftar=$kodeDaftar.(str_pad($nomasuk['nom']+1, 3, 0,STR_PAD_LEFT));
        return $kodeDaftar;
    }

    public static function noPermintaanPenunjang($prefix='')
    {
        $tgl = date('Y-m-d');
        $kode=$prefix;
        $sqlNo = "SELECT CAST(MAX(SUBSTR(noperminatanpenujang,3,6)) AS integer) no_permintaan
                        FROM permintaankepenunjang_t
                        WHERE noperminatanpenujang LIKE ('".$prefix."%') AND DATE(tglpermintaankepenunjang) = '".$tgl."'";
        $no = Yii::app()->db->createCommand($sqlNo)->queryRow();
        $kode=$kode.(str_pad($no['no_permintaan']+1, 4, 0,STR_PAD_LEFT));
        return $kode;
    }

    /**
     *
     * @param type $no_penunjang
     * @param type $ruanganid
     * @return type string
     */
    public static function noAntrianPenunjang($no_penunjang,$ruanganid)
    {
        if($ruanganid!=''){
            $tgl = date('Y-m-d');
            $kodeInst = substr($no_penunjang, 0,1);
            $sqlAntrian = "select max(no_urutperiksa) nourut from pasienmasukpenunjang_t where date(tglmasukpenunjang) = '".$tgl."' AND ruangan_id = '".$ruanganid."'";
            $no_penunjang = Yii::app()->db->createCommand($sqlAntrian)->queryRow();
            $kodeAntrianr=(str_pad($no_penunjang['nourut']+1, 3, 0,STR_PAD_LEFT));
            return $kodeAntrianr;
        } else {
            return null;
        }
    }

    /**
     *
     * @param float $umur
     * @return array hasil query tabel golongan umur ($result['golonganumur_id'],$result['golonganumur_nama'])
     */
    public static function getGolonganUmur($umur){
             $sql = "select golonganumur_id, golonganumur_nama from golonganumur_m where '".ceil($umur)."' between golonganumur_minimal AND golonganumur_maksimal";
             $result = Yii::app()->db->createCommand($sql)->queryRow();
             return $result;
    }

    public static function getStatusPasien($modelPasien)
    {
        $sql = "SELECT pendaftaran_id FROM pendaftaran_t WHERE pasien_id = ".$modelPasien->pasien_id;
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        $status = (!empty($result)) ? Params::statusPasien('lama') : Params::statusPasien('baru');

        return $status;
    }

    public static function getKunjungan($modelPasien, $ruanganId)
    {
        if($ruanganId!=''){
            $sql = "SELECT pendaftaran_id FROM pendaftaran_t WHERE pasien_id = ".$modelPasien->pasien_id.' AND ruangan_id = '.$ruanganId;
            $result = Yii::app()->db->createCommand($sql)->queryRow();
            $status = (!empty($result)) ? Params::statusKunjungan('lama') : Params::statusKunjungan('baru');

            return $status;
        } else {
            return Params::statusKunjungan('baru');
        }
    }

    public static function hitungUmur($tglLahir)
    {
        $format = new CustomFormat;
        $tglLahir = $format->formatDateMediumForDB($tglLahir);
        $dob=$tglLahir; $today=date("Y-m-d");
        list($y,$m,$d)=explode('-',$dob);
        list($ty,$tm,$td)=explode('-',$today);
        if($td-$d<0){
            $day=($td+30)-$d;
            $tm--;
        }
        else{
            $day=$td-$d;
        }
        if($tm-$m<0){
            $month=($tm+12)-$m;
            $ty--;
        }
        else{
            $month=$tm-$m;
        }
        $year=$ty-$y;

        $umur = str_pad($year, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($month, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($day, 2, '0', STR_PAD_LEFT).' Hr';

        return $umur;
    }

    public static function getInstalasiIdFromRuanganId($idRuangan)
    {
        if(!empty($idRuangan)){
            $sql = "SELECT instalasi_id FROM ruangan_m WHERE ruangan_id = '".$idRuangan."'";
            $idInstalasi = Yii::app()->db->createCommand($sql)->queryScalar();
            return $idInstalasi;
        } else {
            return null;
        }
    }

    public static function ambilAntrianPasien($loket)
    {
        $sql = "SELECT * FROM antrian_t WHERE pendaftaran_id IS NOT NULL
                AND tglantrian = DATE(NOW())
                AND loket_antrian = '".$loket."'";
        $antrianPasien = Yii::app()->db->createCommand($sql)->queryAll();
        return $antrianPasien;
    }

    public static function noUrutPasienKirimKeUnitLain($idRuangan)
    {
        if($idRuangan!=''){
            $tgl = date('Y-m-d');
            $sqlAntrian = "select max(nourut) nourut from pasienkirimkeunitlain_t where date(tgl_kirimpasien) = '".$tgl."' AND ruangan_id = '".$idRuangan."'";
            $nourut = Yii::app()->db->createCommand($sqlAntrian)->queryRow();
            if($nourut){
                return $nourut['nourut']+1;
            }else{
                return 1;
            }
        } else {
            return null;
        }
    }

    public static function noRencanaOperasi()
    {
        $kode_rencana = Params::KODE_RENCANA_OPERASI;
        $tgl = date('ymd');
        $kodeRencana = $kode_rencana.$tgl;
        $sqlNoRencana = "SELECT CAST(MAX(SUBSTR(norencanaoperasi,14,16)) AS integer) nop FROM rencanaoperasi_t WHERE norencanaoperasi LIKE ('".$kodeRencana."%')";
        $norencana= Yii::app()->db->createCommand($sqlNoRencana)->queryRow();
        $kodeRencana=$kodeRencana.(str_pad($norencana['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kodeRencana;
    }

    /**
     * Fungsi Untuk Mengenerate No HasilPemeriksaan rehab medis Umum Secara Otomatis
     * @return string
     */
    public static function noHasilPemeriksaanRM()
    {
        $kode_hasil = Params::KODE_HASIL_RE;
        $tgl = date('ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nohasilrm,14,16)) AS integer) nop FROM hasilpemeriksaanrm_t WHERE nohasilrm LIKE ('".$kode_hasil."%')";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Pengajuan Bahan Secara Otomatis
     * @return string
     */
    public static function noPengajuanBahan()
    {
        $kode_hasil = 'PB';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopengajuan,11,13)) AS integer) nop FROM pengajuanbahanmkn_t WHERE date(tglpengajuanbahan) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Pengajuan Bahan Secara Otomatis
     * @return string
     */
    public static function noPenerimaanBahan()
    {
        $kode_hasil = 'TB';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopenerimaanbahan,11,13)) AS integer) nop FROM terimabahanmakan_t WHERE date(tglterimabahan) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Pemesanan Menu Diet Secara Otomatis
     * @return string
     */
    public static function noPesanMenuDiet()
    {
        $kode_hasil = 'PD';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopesanmenu,11,13)) AS integer) nop FROM pesanmenudiet_t WHERE date(tglpesanmenu) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));

        if(count($nohasil)>0){
            $kode_hasil = $kode_hasil.(str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT));
        }else{
            $kode_hasil = $kode_hasil."00001";
        }

        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Pengiriman Menu Diet Secara Otomatis
     * @return string
     */
    public static function noKirimMenuDiet()
    {
        $kode_hasil = 'KD';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nokirimmenu,11,13)) AS integer) nop FROM kirimmenudiet_t WHERE date(tglkirimmenu) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No HasilPemeriksaan rehab medis Umum Secara Otomatis
     * @return string
     */
    public static function noUrutJadwalRencanaRM()
    {
        $kode_jadwal = Params::KODE_JADWAL_RE;
        $tgl = date('ymd');
        $kode_jadwal = $kode_jadwal.$tgl;
        $sqlNoJadwal = "SELECT CAST(MAX(SUBSTR(nojadwal,14,16)) AS integer) nop FROM jadwalkunjunganrm_t WHERE nojadwal LIKE ('".$kode_jadwal."%')";
        $nojadwal= Yii::app()->db->createCommand($sqlNoJadwal)->queryRow();
        $kode_jadwal = $kode_jadwal.(str_pad($nojadwal['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_jadwal;
    }

    /**
     * Fungsi Untuk Mengenerate No Resep Secara Otomatis
     * @return string
     */
//    public static function noResep($prefix='NR')
//    {
//        $singkatanRuangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_singkatan;
//        if(!empty($singkatanRuangan))
//            $prefix = $singkatanRuangan;
//        //$sql = "SELECT CAST(MAX(SUBSTRING(noresep FROM CHAR_LENGTH(TRIM(noresep))-5)) AS integer) noresep FROM reseptur_t WHERE noresep LIKE ('".$prefix."%')";
//        //$sql = "SELECT CAST(MAX(SUBSTR(noresep,3,8)) AS integer) noresep FROM reseptur_t WHERE noresep LIKE ('".$prefix."%')";
//        $sql = "SELECT noresep FROM penjualanresep_t WHERE jenispenjualan ILIKE '%PENJUALAN RESEP%' GROUP BY penjualanresep_id,noresep ORDER BY penjualanresep_id desc";
//        $no= Yii::app()->db->createCommand($sql)->queryRow();
////        $noResep = $prefix.Yii::app()->user->id.(str_pad($no['noresep']+1, 4, 0,STR_PAD_LEFT));
//        $noResep = (str_pad($no['noresep']+1, 3, 0,STR_PAD_LEFT));
////        $noResep = ($no['noresep']+1);
//        return $noResep;
//    }

    public static function noResep($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        //$kode_hasil = "/".$bln."/".$thn."/NORSP/".$instalasi->instalasi_lokasi;
        $kode_hasil = "/".$bln."/".$thn;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noresep,1,5)) AS integer) nop FROM penjualanresep_t WHERE extract(month from tglpenjualan)  = '$blnSql' and extract(year from tglpenjualan)  = '$thn'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        if(count($nohasil)>0){
            $kode_hasil = (str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        }else{
            $kode_hasil = "00001".$kode_hasil;
        }

        return $kode_hasil;
    }
    public static function noResepLuar($prefix='UMUM')
    {
        $singkatanRuangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_singkatan;
        if(!empty($singkatanRuangan))
            $prefix = $singkatanRuangan;
        //$sql = "SELECT CAST(MAX(SUBSTRING(noresep FROM CHAR_LENGTH(TRIM(noresep))-5)) AS integer) noresep FROM reseptur_t WHERE noresep LIKE ('".$prefix."%')";
        //$sql = "SELECT CAST(MAX(SUBSTR(noresep,3,8)) AS integer) noresep FROM reseptur_t WHERE noresep LIKE ('".$prefix."%')";
        $sql = "SELECT count(penjualanresep_id) as noresep FROM penjualanresep_t WHERE jenispenjualan ILIKE '%PENJUALAN RESEP LUAR%'";
        $no= Yii::app()->db->createCommand($sql)->queryRow();
//        $noResep = $prefix.Yii::app()->user->id.(str_pad($no['noresep']+1, 4, 0,STR_PAD_LEFT));
        $noResep = $prefix.(str_pad($no['noresep']+1, 5, 0,STR_PAD_LEFT));
//        $noResep = ($no['noresep']+1);
        return $noResep;
    }
//    public static function noResepLuar($instalasi_id)
//    {
//        $format = new CustomFormat();
//        $bln = $format->formatBulanIna(date('M'));
//        $blnSql = date('m');
//        $thn = date('Y');
//        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
//        $kode_hasil = "/".$bln."/".$thn."/NORSPUM/".$instalasi->instalasi_lokasi;
//
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noresep,1,5)) AS integer) nop FROM penjualanresep_t WHERE extract(month from tglpenjualan)  = '$blnSql' and extract(year from tglpenjualan)  = '$thn'";
//
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
////        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        $kode_hasil = (str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
//        return $kode_hasil;
//    }

    public static function getRuanganPelayananAkhir($idPendaftaran)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('pendaftaran_id', $idPendaftaran);
        $criteria->order = 'create_time DESC';
        $tindakanPelayanan = TindakanpelayananT::model()->find($criteria);

        $ruanganTindakan = $tindakanPelayanan->ruangan_id;

        if(empty($ruanganTindakan)){
            $ruanganTindakan = ObatalkespasienT::model()->find($criteria)->ruangan_id;
        }

        return $ruanganTindakan;
    }

    public static function noUrutKasir($idRuangan)
    {
        $tgl = date('Y-m-d');
        $sql = "SELECT CAST(MAX(nourutkasir) AS integer) nourut FROM tandabuktibayar_t
                WHERE ruangan_id = $idRuangan AND date(tglbuktibayar)='".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $noUrut = $data['nourut']+1;
        return $noUrut;
    }

    public static function noBuktiBayar()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'BKMP';
        //$sql = "SELECT CAST(MAX(SUBSTR(nobuktibayar,14,17)) AS integer) nourut FROM tandabuktibayar_t WHERE date(tglbuktibayar) = '".$tgl."'";
        $sql = "SELECT CAST(MAX (SUBSTRING(nobuktibayar FROM CHAR_LENGTH(TRIM(nobuktibayar))-4)) AS integer) nourut
                FROM tandabuktibayar_t WHERE date(tglbuktibayar) = '".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $noBukti = (str_pad($data['nourut']+1, 5, 0,STR_PAD_LEFT));
        return $kode.$kodeTgl.Yii::app()->user->id.$noBukti;
    }

    public static function noBuktiKeluar()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'BKRET';
        //$sql = "SELECT CAST(MAX(SUBSTR(nobuktibayar,14,17)) AS integer) nourut FROM tandabuktibayar_t WHERE date(tglbuktibayar) = '".$tgl."'";
        $sql = "SELECT CAST(MAX (SUBSTRING(nokaskeluar FROM CHAR_LENGTH(TRIM(nokaskeluar))-4)) AS integer) nourut
                FROM tandabuktikeluar_t WHERE date(tglkaskeluar) = '".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $noBuktiKeluar = (str_pad($data['nourut']+1, 5, 0,STR_PAD_LEFT));
        return $kode.$kodeTgl.Yii::app()->user->id.$noBuktiKeluar;
    }

    /**
     * Fungsi Untuk Mengenerate No Pembayaran PembayaranpelayananT
     * @return string
     */
    public static function noPembayaran()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'NP';
        $sql = "SELECT CAST(MAX(SUBSTRING(nopembayaran FROM CHAR_LENGTH(TRIM(nopembayaran))-4)) AS integer) nobayar FROM pembayaranpelayanan_t WHERE date(tglpembayaran) = '".$tgl."'";
        //$sql = "SELECT CAST(MAX(SUBSTR(nopembayaran,11,15)) AS integer) nobayar FROM pembayaranpelayanan_t WHERE date(tglpembayaran) = '".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $no = (str_pad($data['nobayar']+1, 5, 0,STR_PAD_LEFT));

        return $kode.$kodeTgl.Yii::app()->user->id.$no;
    }

    /**
     * Fungsi Untuk Mengenerate No Retur ReturbayarpelayananT
     * @return string
     */
    public static function noReturBayarPelayanan()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'NR';
//        $sql = "SELECT CAST(MAX(SUBSTR(noreturbayar,11,15)) AS integer) noretur FROM returbayarpelayanan_t WHERE date(tglreturpelayanan) = '".$tgl."'";
        $sql = "SELECT CAST(MAX(SUBSTRING(noreturbayar FROM CHAR_LENGTH(TRIM(noreturbayar))-4)) AS integer) noretur FROM returbayarpelayanan_t WHERE date(tglreturpelayanan) = '".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $no = (str_pad($data['noretur']+1, 5, 0,STR_PAD_LEFT));

        return $kode.$kodeTgl.Yii::app()->user->id.$no;
    }

    /**
     * Fungsi Untuk Mengenerate No Penerimaan  PenerimaanumumT
     * @return string
     */
    public static function noPenerimaanUmum()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'NPU';
        //$sql = "SELECT CAST(MAX(SUBSTR(nopenerimaan,12,16)) AS integer) nopenerimaan FROM penerimaanumum_t WHERE date(tglpenerimaan) = '".$tgl."'";
        //$sql = "SELECT CAST(MAX(SUBSTRING(nopenerimaan FROM CHAR_LENGTH(TRIM(nopenerimaan))-4)) AS integer) nopenerimaan FROM penerimaanumum_t WHERE date(tglpenerimaan) = '".$tgl."'";
        $sql = "
            SELECT CAST(SUBSTRING(nopenerimaan FROM CHAR_LENGTH(TRIM(nopenerimaan))-4) AS integer) AS nopenerimaan
            FROM penerimaanumum_t WHERE date(tglpenerimaan) = '".$tgl."'
            ORDER BY create_time DESC
            LIMIT 1
        ";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $no = (str_pad($data['nopenerimaan']+1, 5, 0,STR_PAD_LEFT));
        return $kode.$kodeTgl.Yii::app()->user->id.$no;
    }

    /**
     * Fungsi Untuk Mengenerate No Pengeluaran  PengeluaranumumT
     * @return string
     */
    public static function noPengeluaranUmum()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'NKU';
//        $sql = "SELECT CAST(MAX(SUBSTR(nopengeluaran,12,16)) AS integer) nopengeluaran
//                FROM pengeluaranumum_t
//                WHERE date(tglpengeluaran) = '".$tgl."'";
        $sql = "
            SELECT CAST(SUBSTRING(nopengeluaran FROM CHAR_LENGTH(TRIM(nopengeluaran))-4) AS integer) AS nopengeluaran
            FROM pengeluaranumum_t WHERE date(tglpengeluaran) = '".$tgl."'
            ORDER BY create_time DESC
            LIMIT 1
        ";
        /*
        $sql = "SELECT CAST(MAX(SUBSTRING(nopengeluaran FROM CHAR_LENGTH(TRIM(nopengeluaran))-4)) AS integer) nopengeluaran
                FROM pengeluaranumum_t
                WHERE date(tglpengeluaran) = '".$tgl."'";
         *
         */
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $no = (str_pad($data['nopengeluaran']+1, 5, 0,STR_PAD_LEFT));

        return $kode.$kodeTgl.Yii::app()->user->id.$no;
    }

    /**
     * Fungsi Untuk Mengenerate No Kas Keluar TandabuktikeluarT
     * @return string
     */
    public static function noKasKeluar()
    {
        $tgl = date('Y-m-d');
        $kodeTgl = date('Ymd');
        $kode = 'KK';
        //$sql = "SELECT CAST(MAX(SUBSTR(nokaskeluar,11,15)) AS integer) nokaskeluar FROM tandabuktikeluar_t WHERE date(tglkaskeluar) = '".$tgl."'";
        $sql = "SELECT CAST(MAX(SUBSTRING(nokaskeluar FROM CHAR_LENGTH(TRIM(nokaskeluar))-4)) AS integer) nokaskeluar
                FROM tandabuktikeluar_t
                WHERE date(tglkaskeluar) = '".$tgl."'";
        $data = Yii::app()->db->createCommand($sql)->queryRow();
        $no = (str_pad($data['nokaskeluar']+1, 5, 0,STR_PAD_LEFT));

        return $kode.$kodeTgl.Yii::app()->user->id.$no;
    }

    /**
     * Fungsi Untuk Mengenerate No Urut Bayi Berdasarkan Ibu Bayi Secara Otomatis
     * @return string
     */
    public static function noUrutBayi($pasien){
        $Idpasien = $pasien;
        $id_persalinan = PersalinanT::model()->findByAttributes(array('pasien_id'=>$Idpasien));
        $sql = "select MAX(nourutbayi) AS nourut from kelahiranbayi_t where persalinan_id= '$id_persalinan->persalinan_id'";
        $command = Yii::app()->db->createCommand($sql);
            $result = $command->queryAll();
            foreach($result as $num)
            {
                if ($num['no_antrian'] <> 0) {
                    $x = $num['no_antrian']+1;
                    return($x);
                } else {
                    return('1');
                }
            }
    }

    /**
     * Fungsi Untuk Mengenerate No Pemesanan PesanObatAlkesT
     * @return string
     */

//    public static function noPemesanan()
//    {
//        $tgl = date('Y-m-d');
//
//        $sql = "SELECT CAST(MAX(SUBSTR(nopemesanan,14,4)) AS integer) nopemesanan FROM pesanobatalkes_t
//                      WHERE date(tglpemesanan)='".$tgl."'";
//        $noPenawaran = Yii::app()->db->createCommand($sql)->queryRow();
//        $noPenawaranBaru ='PR'.date('Ymd').(str_pad($noPenawaran['nopemesanan']+1, 4, 0,STR_PAD_LEFT));
//        return $noPenawaranBaru;
//    }
    public static function noPemesanan($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/PSNOBT/".$instalasi->instalasi_lokasi;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopemesanan,1,5)) AS integer) nop FROM pesanobatalkes_t WHERE extract(month from tglpemesanan)  = '$blnSql' and extract(year from tglpemesanan)  = '$thn'";

        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        if(count($nohasil)>0){
            $kode_hasil = (str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        }else{
            $kode_hasil = "00001".$kode_hasil;
        }

        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Urut Mutasi MutasiOARuanganT
     * @return string
     */
    public static function noMutasi()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nomutasioa,15,4)) AS integer) nomutasi FROM mutasioaruangan_t
                      WHERE date(tglmutasioa)='".$tgl."'";
        $noPenawaran = Yii::app()->db->createCommand($sql)->queryRow();
        $noPenawaranBaru ='MUT'.date('Ymd').(str_pad($noPenawaran['nomutasi']+1, 4, 0,STR_PAD_LEFT));
        return $noPenawaranBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Urut Terima Mutasi TerimamutasiT
     * @return string
     */
    public static function noTerimaMutasi()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(noterimamutasi,16,4)) AS integer) noterimamutasi FROM terimamutasi_t
                      WHERE date(tglterima)='".$tgl."'";
        $noTerimaMutasi = Yii::app()->db->createCommand($sql)->queryRow();
        $noTerimaMutasiBaru ='TMUT'.date('Ymd').(str_pad($noTerimaMutasi['noterimamutasi']+1, 4, 0,STR_PAD_LEFT));
        return $noTerimaMutasiBaru;
    }

    public static function noUrutPinjamRM()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT MAX(CAST(nourut_pinjam AS integer)) nourut_pinjam FROM peminjamanrm_t
                      WHERE date(tglpeminjamanrm)='".$tgl."'";
        $noUrutPinjam = Yii::app()->db->createCommand($sql)->queryRow();
        $noUrutPinjamBaru =(str_pad($noUrutPinjam['nourut_pinjam']+1, 5, 0,STR_PAD_LEFT));
        return $noUrutPinjamBaru;
    }

    public static function noUrutKeluarRM()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT MAX(CAST(nourut_keluar AS integer)) nourut_keluar FROM pengirimanrm_t
                      WHERE date(tglpengirimanrm)='".$tgl."'";
        $noUrutKirim = Yii::app()->db->createCommand($sql)->queryRow();
        $noUrutKirimBaru =(str_pad($noUrutKirim['nourut_keluar']+1, 5, 0,STR_PAD_LEFT));
        return $noUrutKirimBaru;
    }

    public static function noDokumenRM()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT MAX(CAST(SUBSTR(nodokumenrm,9,3) AS integer)) nodokumenrm FROM dokrekammedis_m
                      WHERE date(tglrekammedis)='".$tgl."'";
        $noDokumenRM = Yii::app()->db->createCommand($sql)->queryRow();
        $noDokumenRM =date('Ymd').(str_pad($noDokumenRM['nodokumenrm']+1, 3, 0,STR_PAD_LEFT));
        return $noDokumenRM;
    }

    public static function kodeBarang()
    {
        $sql = "SELECT MAX(barang_kode) AS kodebarang FROM barang_m";
        $barangMax = Yii::app()->db->createCommand($sql)->queryRow();
        $barangMax['kodebarang'] = $barangMax['kodebarang']+1;
        return $barangMax['kodebarang'];
    }

    /**
     * Fungsi Untuk Mengenerate No Pemesanan Barang dari gudang umum Secara Otomatis
     * @return string
     */
//    INI DIGUNAKAN PADA TIAR MEDIKA SIMRS
//    public static function noPemesananBarang()
//    {
//        $kode_hasil = 'PB';
//        $tgl = date('Ymd');
//        $kode_hasil = $kode_hasil.$tgl;
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopemesanan,11,13)) AS integer) nop FROM pesanbarang_t WHERE date(tglpesanbarang) = '".date('Y-m-d')."'";
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        return $kode_hasil;
//    }

//    INI DIGUNAKAN PADA EHOSPITALJK
    public static function noPemesananBarang($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/PMSNBRG/".$instalasi->instalasi_lokasi;

//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopemesanan,11,13)) AS integer) nop FROM pesanbarang_t WHERE date(tglpesanbarang) = '".date('Y-m-d')."'";
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopemesanan,1,5)) AS integer) nop FROM pesanbarang_t WHERE extract(month from tglpesanbarang)  = '$blnSql' and extract(year from tglpesanbarang)  = '$thn'";

        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        $kode_hasil = (str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        return $kode_hasil;
    }
    /**
     * Fungsi Untuk Mengenerate No Mutasi Barang dari gudang umum Secara Otomatis
     * @return string
     */
//    public static function noMutasiBarang()
//    {
//        $kode_hasil = 'MB';
//        $tgl = date('Ymd');
//        $kode_hasil = $kode_hasil.$tgl;
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nomutasibrg,11,13)) AS integer) nop FROM mutasibrg_t WHERE date(tglmutasibrg) = '".date('Y-m-d')."'";
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        return $kode_hasil;
//    }
    public static function noMutasiBarang($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/MUTBRG/".$instalasi->instalasi_lokasi;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nomutasibrg,1,5)) AS integer) nop FROM mutasibrg_t WHERE extract(month from tglmutasibrg)  = '$blnSql' and extract(year from tglmutasibrg)  = '$thn'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=(str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        return $kode_hasil;
    }
    /**
     * Fungsi Untuk Mengenerate No Pembeliaan Barang dari gudang umum Secara Otomatis
     * @return string
     */
//    public static function noPembelianBarang()
//    {
//        $kode_hasil = 'BB';
//        $tgl = date('Ymd');
//        $kode_hasil = $kode_hasil.$tgl;
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopembelian,11,13)) AS integer) nop FROM pembelianbarang_t WHERE date(tglpembelian) = '".date('Y-m-d')."'";
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        return $kode_hasil;
//    }
    public static function noPembelianBarang($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/PBLNBRG/".$instalasi->instalasi_lokasi;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopembelian,1,5)) AS integer) nop FROM pembelianbarang_t WHERE extract(month from tglpembelian)  = '$blnSql' and extract(year from tglpembelian)  = '$thn'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=(str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        return $kode_hasil;
    }
    /**
     * Fungsi Untuk Mengenerate No Penerimaan Persediaan Barang dari gudang umum Secara Otomatis
     * @return string
     */
//    public static function noPenerimaanPersediaan()
//    {
//        $kode_hasil = 'PP';
//        $tgl = date('Ymd');
//        $kode_hasil = $kode_hasil.$tgl;
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopenerimaan,11,13)) AS integer) nop FROM terimapersediaan_t WHERE date(tglterima) = '".date('Y-m-d')."'";
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        return $kode_hasil;
//    }
    public static function noPenerimaanPersediaan($instalasi_id)
    {
        $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/PENPER/".$instalasi->instalasi_lokasi;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopenerimaan,1,5)) AS integer) nop FROM terimapersediaan_t WHERE extract(month from tglterima)  = '$blnSql' and extract(year from tglterima)  = '$thn'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=(str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        return $kode_hasil;
    }
    /**
     * Fungsi Untuk Mengenerate No Retur Penerimaan Persediaan Barang dari gudang umum Secara Otomatis
     * @return string
     */
    public static function noReturTerima()
    {
        $kode_hasil = 'RP';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noreturterima,11,13)) AS integer) nop FROM returpenerimaan_t WHERE date(tglreturterima) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    public static function kodeTerimaPersediaan()
    {
        $kode_hasil = 'TERIMA';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(inventarisasi_kode,15,17)) AS integer) nop FROM inventarisasiruangan_t WHERE date(tgltransaksi) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    //--Modul Gudang Farmasi
    //Generate No Formulir secara Otomatis Transaksi Formulir Stok Obat Opname
    public static function noFormulirOpname()
    {
        $kode_hasil = 'FM';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noformulir,11,13)) AS integer) nop FROM formuliropname_r WHERE date(tglformulir) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    //--Modul Gudang Farmasi
    //Generate No Stok Opnaem secara Otomatis Transaksi No Stok Obat Opname
//    public static function noStokOpname()
//    {
//        $kode_hasil = 'SO';
//        $tgl = date('Ymd');
//        $kode_hasil = $kode_hasil.$tgl;
//        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nostokopname,11,13)) AS integer) nop FROM stokopname_t WHERE date(tglstokopname) = '".date('Y-m-d')."'";
//        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
//        return $kode_hasil;
//    }
    public static function noStokOpname($instalasi_id)
    {
       $format = new CustomFormat();
        $bln = $format->formatBulanIna(date('M'));
        $blnSql = date('m');
        $thn = date('Y');
        $instalasi = InstalasiM::model()->findByPk($instalasi_id);
        $kode_hasil = "/".$bln."/".$thn."/SOPNM/".$instalasi->instalasi_lokasi;

        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nostokopname,1,5)) AS integer) nop FROM stokopname_t WHERE extract(month from tglstokopname)  = '$blnSql' and extract(year from tglstokopname)  = '$thn'";

        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
//        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        $kode_hasil = (str_pad($nohasil['nop']+1, 5, 0,STR_PAD_LEFT)).$kode_hasil;
        return $kode_hasil;
    }

    public static function noPerjanjian()
    {
        $kode_hasil = 'PJ';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(pengangkatantphl_noperjanjian,11,13)) AS integer) nop FROM pengangkatantphl_t WHERE date(pengangkatantphl_tmt) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

        /**
     * --Modul Kepegawaian
     * Generate No Penggajian secara Otomatis untuk transaksi penggajian pegawai
     */
    public static function noPenggajian()
    {
        $kode_hasil = 'NP';
        $tgl = date('Ymd');
        $kode_hasil = $kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(nopenggajian,11,13)) AS integer) nop FROM penggajianpeg_t WHERE date(tglpenggajian) = '".date('Y-m-d')."'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 3, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Resep Secara Otomatis
     * @return string
     */
    public static function noResep2($prefix='NR')
    {
        $tgl = date('Ymd');
        $kode_hasil = $prefix.$kode_hasil.$tgl;
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noresep,12,14)) AS integer) nop FROM penjualanresep_t WHERE date(tglpenjualan) = '".date('Y-m-d')."' and lower(noresep) like '%".strtolower($prefix)."%'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 4, 0,STR_PAD_LEFT));
        return $kode_hasil;
    }

    public static function noResepPenjualanBebas($prefix=null){
        $tgl = date('Ymd');
        $prefix = (isset($prefix)) ? $prefix : 'BEBAS';
//        $kode_hasil = $prefix.$tgl;
        $kode_hasil = $prefix;
        $panjangPrefix = strlen($kode_hasil);
        $sqlNoHasil = "SELECT CAST(MAX(SUBSTR(noresep,".($panjangPrefix+1).",".($panjangPrefix+4).")) AS integer) nop FROM penjualanresep_t WHERE date(tglpenjualan) = '".date('Y-m-d')."' and lower(noresep) like '%".strtolower($prefix)."%'";
        $nohasil= Yii::app()->db->createCommand($sqlNoHasil)->queryRow();
        $kode_hasil=$kode_hasil.(str_pad($nohasil['nop']+1, 4, 0,STR_PAD_LEFT));

        return $kode_hasil;
    }

    /**
     * Fungsi Untuk Mengenerate No Retur Secara Otomatis
     * @return string
     */
    public static function noPembayaranKlaim()
    {
        $tgl = date('Y-m-d');

//        $sql = "SELECT CAST(MAX(SUBSTR(noretur,14,4)) AS integer) noretur FROM returpembelian_t
//                      WHERE date(tglretur)='".$tgl."'";
        $sql = "SELECT count(nopembayaranklaim) As nopembayaranklaim FROM pembayarklaim_t";
        $noRetur = Yii::app()->db->createCommand($sql)->queryRow();
        $noReturBaru ='NPK'.date('Ymd').(str_pad($noRetur['nopembayaranklaim']+1, 4, 0,STR_PAD_LEFT));
        return $noReturBaru;
    }

//    public static function kodeGolongan()
//    {
//        $sql = "SELECT MAX(golongan_kode) AS kodegolongan FROM golongan_m";
//        $golonganMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $golonganMax['kodegolongan'] = $golonganMax['kodegolongan']+1;
//        return $golonganMax['kodegolongan'];
//    }
//    public static function kodeKelompok()
//    {
//        $sql = "SELECT MAX(kelompok_kode) AS kodekelompok FROM kelompok_m";
//        $kelompokMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $kelompokMax['kodekelompok'] = $kelompokMax['kodekelompok']+1;
//        return $kelompokMax['kodekelompok'];
//    }
//    public static function kodeSubKelompok()
//    {
//        $sql = "SELECT MAX(subkelompok_kode) AS kodesubkelompok FROM subkelompok_m";
//        $subkelompokMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $subkelompokMax['kodesubkelompok'] = $subkelompokMax['kodesubkelompok']+1;
//        return $subkelompokMax['kodesubkelompok'];
//    }
//    public static function kodeBidang()
//    {
//        $sql = "SELECT MAX(bidang_kode) AS kodebidang FROM bidang_m";
//        $bidangMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $bidangMax['kodebidang'] = $bidangMax['kodebidang']+1;
//        return $bidangMax['kodebidang'];
//    }
//    public static function kodeLokasiAset()
//    {
//        $sql = "SELECT MAX(lokasiaset_kode) AS kodelokasiaset FROM lokasiaset_m";
//        $lokasiasetMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $lokasiasetMax['kodelokasiaset'] = $lokasiasetMax['kodelokasiaset']+1;
//        return $lokasiasetMax['kodelokasiaset'];
//    }
//    public static function kodePemilikBarang()
//    {
//        $sql = "SELECT MAX(pemilikbarang_kode) AS kodepemilikbarang FROM pemilikbarang_m";
//        $pemilikbarangMax = Yii::app()->db->createCommand($sql)->queryRow();
//        $pemilikbarangMax['kodepemilikbarang'] = $pemilikbarangMax['kodepemilikbarang']+1;
//        return $pemilikbarangMax['kodepemilikbarang'];
//    }


    public static function noBuktiJurnalRek()
    {
        $tgl = date('Y-m-d');
        $sql = "SELECT count(nobuktijurnal) As nobuktijurnal FROM jurnalrekening_t";
        $noBuktiJurnal = Yii::app()->db->createCommand($sql)->queryRow();
        $noBuktiJurnal ='NBJ'.date('Ymd').(str_pad($noBuktiJurnal['nobuktijurnal']+1, 4, 0,STR_PAD_LEFT));
        return $noBuktiJurnal;
    }
    public static function kodeJurnalRek()
    {
        $sql = "SELECT count(kodejurnal) As kode FROM jurnalrekening_t";
        $kodeJurnal = Yii::app()->db->createCommand($sql)->queryRow();
        $kodeJurnal =(str_pad($kodeJurnal['kode']+1, 4, 0,STR_PAD_LEFT));
        return $kodeJurnal;
    }

    /**
     * Fungsi Untuk Mengenerate No Bayar Jasa PembayaranjasaT
     * @return string
     */
    public static function noBayarJasa()
    {
        $tgl = date('Y-m-d');
        $sql = "SELECT count(pembayaranjasa_id) As pembayaranjasa_id FROM pembayaranjasa_t";
        $noBayarJasa = Yii::app()->db->createCommand($sql)->queryRow();
        $noBayarJasa ='BJ'.date('Y').(str_pad($noBayarJasa['pembayaranjasa_id']+1, 4, 0,STR_PAD_LEFT));
        return $noBayarJasa;
    }

    /**
     * Fungsi Untuk Mengenerate No Verifikasi
     * @return string
     */
    public static function noVerifikasi()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(noverifikasi,11,4)) AS integer) noverifikasi FROM verifikasitagihan_t
                      WHERE date(tglverifikasi)='".$tgl."'";
        $noVerifikasi = Yii::app()->db->createCommand($sql)->queryRow();
        $noVerifikasiBaru ='VR'.date('Ymd').(str_pad($noVerifikasi['noverifikasi']+1, 4, 0,STR_PAD_LEFT));
        return $noVerifikasiBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Peminjaman
     * @author : Hardi
     */
    public static function noPeminjaman()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nopinjam,11,4)) AS integer) nopinjam FROM pinjamanpeg_t
                      WHERE date(tglpinjampeg)='".$tgl."'";
        $noPinjam = Yii::app()->db->createCommand($sql)->queryRow();
        $noPinjamBaru ='PNJ'.date('Ymd').(str_pad($noPinjam['nopinjam']+1, 4, 0,STR_PAD_LEFT));
        return $noPinjamBaru;
    }

     /**
     * Fungsi Untuk Mengenerate No Pemakaian Barang Secara Otomatis
     * @return string
     */
    public static function noPemakaianBarang()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nopemakaianbrg,12,4)) AS integer) nopemakaianbrg FROM pemakaianbarang_t
                      WHERE date(tglpemakaianbrg)='".$tgl."'";
        $noPemakaian = Yii::app()->db->createCommand($sql)->queryRow();
        $noPemakaianBaru ='PMK'.date('Ymd').(str_pad($noPemakaian['nopemakaianbrg']+1, 4, 0,STR_PAD_LEFT));
        return $noPemakaianBaru;
    }

    /**
     * Fungsi Untuk Mengenerate No Penjualan Barang Secara Otomatis
     * @return string
     */
    public static function noPenjualanBarang()
    {
        $tgl = date('Y-m-d');

        $sql = "SELECT CAST(MAX(SUBSTR(nopenjualan,12,4)) AS integer) nopenjualan FROM penjualanbarang_t
                      WHERE date(tglpenjualanbrg)='".$tgl."'";
        $noPenjualan = Yii::app()->db->createCommand($sql)->queryRow();
        $noPenjualanBaru ='PNJ'.date('Ymd').(str_pad($noPenjualan['nopenjualan']+1, 4, 0,STR_PAD_LEFT));
        return $noPenjualanBaru;
    }


    /**
     * Fungsi Untuk Mengenerate No Rekam Medis janji poli
     * @return string
     * Tanggal : 12-Mei-2014
     * Issue : EHJ-1773
     * @author Jembar
     */
    public static function noRekamMedikJanjiPoli($prefix='',$isPasienLuar='TRUE')
        {
            $sqlNoRM = "SELECT CAST(MAX(SUBSTR(no_rekam_medik,3,6)) AS integer)
                        nop FROM pasien_m WHERE no_rekam_medik like 'JP%'";
            $genRM = Yii::app()->db->createCommand($sqlNoRM)->queryRow();
            $noRM = $prefix.str_pad($genRM['nop']+1, 6, 0,STR_PAD_LEFT);
            return trim($noRM);
        }
    /**
     * Fungsi Untuk Mengenerate No Rekam Medis Booking Kamar
     * @return string
     * Tanggal : 12-Mei-2014
     * Issue : EHJ-1770
     * @author hardi
     */
    public static function noRekamMedikBK($prefix, $isPasienLuar='FALSE')
    {
        $sqlNoRM = "SELECT CAST(MAX(SUBSTRING(no_rekam_medik FROM CHAR_LENGTH(TRIM(no_rekam_medik))-5)) AS integer)
                    nop FROM pasien_m WHERE ispasienluar=$isPasienLuar";
        $genRM = Yii::app()->db->createCommand($sqlNoRM)->queryRow();
        $noRM = $prefix.str_pad($genRM['nop']+1, 6, 0,STR_PAD_LEFT);
        return trim($noRM);
    }

    public static function noUrutRegisterBarang($idBarang=null,$kodeinventarisasi,$kode=null,$idKomponen=null,$komponen=null)
    {
        if($kodeinventarisasi=='invtanaht'){
            if(isset($idBarang)){
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invtanah_noregister FROM CHAR_LENGTH(TRIM(invtanah_noregister))-5)) AS integer) nop FROM invtanah_t WHERE barang_id=$idBarang";
            }elseif(isset($komponen)) {
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invtanah_noregister FROM CHAR_LENGTH(TRIM(invtanah_noregister))-5)) AS integer) nop FROM invtanah_t WHERE pemilikbarang_id=$idKomponen";
            }else{
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invtanah_noregister FROM CHAR_LENGTH(TRIM(invtanah_noregister))-5)) AS integer) nop FROM invtanah_t";
            }
        }elseif($kodeinventarisasi=='invgedungt'){
            if(isset($idBarang)){
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invgedung_noregister FROM CHAR_LENGTH(TRIM(invgedung_noregister))-5)) AS integer) nop FROM invgedung_t WHERE barang_id=$idBarang";
            }elseif(isset($komponen)) {
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invgedung_noregister FROM CHAR_LENGTH(TRIM(invgedung_noregister))-5)) AS integer) nop FROM invgedung_t WHERE pemilikbarang_id=$idKomponen";
            }else{
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invgedung_noregister FROM CHAR_LENGTH(TRIM(invgedung_noregister))-5)) AS integer) nop FROM invgedung_t";
            }
        }elseif(($kodeinventarisasi=='invasetlaint') || ($kodeinventarisasi=='invjalant') || ($kodeinventarisasi=='invperalatant')){
            if(isset($idBarang)){
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invperalatan_noregister FROM CHAR_LENGTH(TRIM(invperalatan_noregister))-5)) AS integer) nop FROM invperalatan_t WHERE barang_id=$idBarang AND split_part(invperalatan_noregister, '-',3)='$kode' ";
            }elseif(isset($komponen)) {
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invperalatan_noregister FROM CHAR_LENGTH(TRIM(invperalatan_noregister))-5)) AS integer) nop FROM invperalatan_t WHERE pemilikbarang_id=$idKomponen";
            }else{
                $sqlno = "SELECT CAST(MAX(SUBSTRING(invperalatan_noregister FROM CHAR_LENGTH(TRIM(invperalatan_noregister))-5)) AS integer) nop FROM invperalatan_t";
            }
        }
        $genRB = Yii::app()->db->createCommand($sqlno)->queryRow();
        $noRB = $prefix.str_pad($genRB['nop']+1, 6, 0,STR_PAD_LEFT);
        return trim($noRB);
    }

}
?>
