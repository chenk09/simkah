<?php

/*
 * class untuk meng-encode semua nilai attribute dari CGridView
 */
class MyJsonDataGrid 
{
    public static function encode($dataProvider,$dataRow)
    {
        if($dataProvider instanceof CActiveDataProvider)
                $columns=$dataProvider->model->attributeNames();
        else if($dataProvider instanceof IDataProvider)
        {
                // use the keys of the first row of data as the default columns
                $data=$dataProvider->getData();
                if(isset($data[0]) && is_array($data[0]))
                        $columns=array_keys($data[0]);
        }
        $data=$dataProvider->getData();
        
        //echo '<pre>'.print_r($data,1).'</pre>';
        foreach($columns as $i=>$column){
            $array[$column] = $dataRow->$column;
        }
        
        return CJSON::encode($array);
    }
}
?>
