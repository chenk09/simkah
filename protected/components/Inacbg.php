<?php
require_once "protected/extensions/random_compat-1.4.3/lib/random.php";
class Inacbg{
    
    var $keyRS = "";
    var $UrlWS = "";
    
    function __construct()
    {
        $this->keyRS = Yii::app()->user->getState('encryption_key_inacbg');
        $this->UrlWS = Yii::app()->user->getState('host_url_inacbg');
    }
    /*Untuk Get response*/
    private function request($request){
        
        $key = $this->keyRS;

        // data yang akan dikirimkan dengan method POST adalah encrypted:
        $payload = $this->inacbg_encrypt($request,$key);
        // tentukan Content-Type pada http header
        $header = array("Content-Type: application/x-www-form-urlencoded");
        // url server aplikasi E-Klaim,
        // silakan disesuaikan instalasi masing-masing
        $url = $this->UrlWS;
        
        // setup curl
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        
        // request dengan curl
        $response = curl_exec($ch);
        // terlebih dahulu hilangkan "----BEGIN ENCRYPTED DATA----\r\n"
        // dan hilangkan "----END ENCRYPTED DATA----\r\n" dari response
        $first = strpos($response, "\n")+1;
        $last = strrpos($response, "\n")-1;
        $response = substr($response,$first,strlen($response) - $first - $last);
        
        // decrypt dengan fungsi inacbg_decrypt
        $response = $this->inacbg_decrypt($response,$key);
        // hasil decrypt adalah format json, ditranslate kedalam array
        $msg = json_decode($response,true);
        return $response;
    }
    
    // Encryption Function
    function inacbg_encrypt($data, $key) {
        /// make binary representasion of $key
        $key = hex2bin($key);
        /// check key length, must be 256 bit or 32 bytes
        if (mb_strlen($key, "8bit") !== 32) {
        throw new Exception("Needs a 256-bit key!");
        }
        /// create initialization vector
        $iv_size = openssl_cipher_iv_length("aes-256-cbc");
        $iv = random_bytes($iv_size); // dengan catatan dibawah
        /// encrypt
        $encrypted = openssl_encrypt($data,"aes-256-cbc",$key,OPENSSL_RAW_DATA,$iv );
        /// create signature, against padding oracle attacks
        $signature = mb_substr(hash_hmac("sha256",$encrypted,$key,true),0,10,"8bit");
        /// combine all, encode, and format
        $encoded = chunk_split(base64_encode($signature.$iv.$encrypted));
        return $encoded;
    }
    
    function inacbg_decrypt($str, $strkey){
        /// make binary representation of $key
        $key = hex2bin($strkey);
        /// check key length, must be 256 bit or 32 bytes
        if (mb_strlen($key, "8bit") !== 32) {
            throw new Exception("Needs a 256-bit key!");
        }
        /// calculate iv size
        $iv_size = openssl_cipher_iv_length("aes-256-cbc");
        /// breakdown parts
        $decoded = base64_decode($str);
        $signature = mb_substr($decoded,0,10,"8bit");
        $iv = mb_substr($decoded,10,$iv_size,"8bit");
        $encrypted = mb_substr($decoded,$iv_size+10,NULL,"8bit");
        /// check signature, against padding oracle attack
        $calc_signature = mb_substr(hash_hmac("sha256",$encrypted,$key,true),0,10,"8bit");
        if(!$this->inacbg_compare($signature,$calc_signature)) {
            return "SIGNATURE_NOT_MATCH"; /// signature doesn't match
        }
        $decrypted = openssl_decrypt($encrypted,"aes-256-cbc",$key,OPENSSL_RAW_DATA,$iv);
        return $decrypted;
    }
    
    /// Compare Function
    function inacbg_compare($a, $b) {
        /// compare individually to prevent timing attacks
        /// compare length
        if (strlen($a) !== strlen($b)) return false;
        /// compare individual
        $result = 0;
        for($i = 0; $i < strlen($a); $i ++) {
            $result |= ord($a[$i]) ^ ord($b[$i]);
        }
        return $result == 0;
    }
    
    function BuatKlaimBaru($nomor_kartu,$nomor_sep,$nomor_rm,$nama_pasien,$tgl_lahir,$gender){	
        $request ='{
            "metadata":{
                "method":"new_claim"
            },
            "data":{
                "nomor_kartu":"'.$nomor_kartu.'",
                "nomor_sep":"'.$nomor_sep.'",
                "nomor_rm":"'.$nomor_rm.'",
                "nama_pasien":"'.$nama_pasien.'",
                "tgl_lahir":"'.$tgl_lahir.'",
                "gender":"'.$gender.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function UpdateDataPasien($nomor_rmlama,$nomor_kartu,$nomor_rm,$nama_pasien,$tgl_lahir,$gender){	
        $request ='{
            "metadata": {
                "method": "update_patient",
                "nomor_rm": "'.$nomor_rmlama.'"
            },
            "data": {
                "nomor_kartu": "'.$nomor_kartu.'",
                "nomor_rm": "'.$nomor_rm.'",
                "nama_pasien": "'.$nama_pasien.'",
                "tgl_lahir": "'.$tgl_lahir.'",
                "gender": "'.$gender.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function HapusDataPasien($nomor_rm,$coder_nik){	
        $request ='{
            "metadata": {
                "method": "delete_patient"
            },
            "data": {
                "nomor_rm": "'.$nomor_rm.'",
                "coder_nik": "'.$coder_nik.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function UpdateDataKlaim($nomor_sep,$nomor_kartu,$tgl_masuk,$tgl_pulang,$jenis_rawat,$kelas_rawat,$adl_sub_acute,$adl_chronic,$icu_indikator,$icu_los,$ventilator_hour,$upgrade_class_ind,$upgrade_class_class,$upgrade_class_los,$add_payment_pct,$birth_weight,$discharge_status,$diagnosa,$procedure,$prosedur_non_bedah,$prosedur_bedah,$konsultasi,$tenaga_ahli,$keperawatan,$penunjang,$radiologi,$laboratorium,$pelayanan_darah,$rehabilitasi,$kamar,$rawat_intensif,$obat,$alkes,$bmhp,$sewa_alat,$tarif_poli_eks,$nama_dokter,$kode_tarif,$payor_id,$payor_cd,$cob_cd,$coder_nik,$nomor_rm,$nama_pasien,$tgl_lahir,$gender){	
        top:
        $request ='{
            "metadata": {
                "method": "set_claim_data",
                "nomor_sep": "'.$nomor_sep.'"
            },
            "data": {
                "nomor_sep": "'.$nomor_sep.'",
                "nomor_kartu": "'.$nomor_kartu.'",
                "tgl_masuk": "'.$tgl_masuk.'",
                "tgl_pulang": "'.$tgl_pulang.'",
                "jenis_rawat": "'.$jenis_rawat.'",
                "kelas_rawat": "'.$kelas_rawat.'",
                "adl_sub_acute": "'.$adl_sub_acute.'",
                "adl_chronic": "'.$adl_chronic.'",
                "icu_indikator": "'.$icu_indikator.'",
                "icu_los": "'.$icu_los.'",
                "ventilator_hour": "'.$ventilator_hour.'",
                "upgrade_class_ind": "'.$upgrade_class_ind.'",
                "upgrade_class_class": "'.$upgrade_class_class.'",
                "upgrade_class_los": "'.$upgrade_class_los.'",
                "add_payment_pct": "'.$add_payment_pct.'",
                "birth_weight": "'.$birth_weight.'",
                "discharge_status": "'.$discharge_status.'",
                "diagnosa": "'.$diagnosa.'",
                "procedure": "'.$procedure.'",
                "tarif_rs": {
                    "prosedur_non_bedah": "'.$prosedur_non_bedah.'",
                    "prosedur_bedah": "'.$prosedur_bedah.'",
                    "konsultasi": "'.$konsultasi.'",
                    "tenaga_ahli": "'.$tenaga_ahli.'",
                    "keperawatan": "'.$keperawatan.'",
                    "penunjang": "'.$penunjang.'",
                    "radiologi": "'.$radiologi.'",
                    "laboratorium": "'.$laboratorium.'",
                    "pelayanan_darah": "'.$pelayanan_darah.'",
                    "rehabilitasi": "'.$rehabilitasi.'",
                    "kamar": "'.$kamar.'",
                    "rawat_intensif": "'.$rawat_intensif.'",
                    "obat": "'.$obat.'",
                    "alkes": "'.$alkes.'",
                    "bmhp": "'.$bmhp.'",
                    "sewa_alat": "'.$sewa_alat.'"
                },
                "tarif_poli_eks": "'.$tarif_poli_eks.'",
                "nama_dokter": "'.$nama_dokter.'",
                "kode_tarif": "'.$kode_tarif.'",
                "payor_id": "'.$payor_id.'",
                "payor_cd": "'.$payor_cd.'",
                "cob_cd": "'.$cob_cd.'",
                "coder_nik": "'.$coder_nik.'"
            }
        }';
        //echo "Data : ".$request;
        
        $msg = $this->request($request);
        $request = json_decode($msg,true);
        if($request['metadata']['message']=="Ok"){
            $msg = $this->GroupingStage1($nomor_sep,$coder_nik);
            return $msg;
        }else{
            if($request['metadata']['error_no']=='E2004'){ //ini berarti pasien blm terdaftar maka daftarkan otomatis
                $request = json_decode($this->BuatKlaimBaru($nomor_kartu,$nomor_sep,$nomor_rm,$nama_pasien,$tgl_lahir,$gender),true);
                if($request['metadata']['message']=="Ok"){
                    goto top;
                }
            }
            return $msg;
        }
    }
    
    function GroupingStage1($nomor_sep,$coder_nik){	
        $request ='{
            "metadata": {
                "method":"grouper",
                "stage":"1"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);

        return $msg;
    }
    
    function GroupingStage2($nomor_sep,$special_cmg){	
        $request ='{
            "metadata": {
                "method":"grouper",
                "stage":"2"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'",
                "special_cmg": "'.$special_cmg.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function FinalisasiKlaim($nomor_sep,$coder_nik){	
        $request ='{
            "metadata": {
                "method":"claim_final"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'",
                "coder_nik": "'.$coder_nik.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function EditUlangKlaim($nomor_sep){	
        $request ='{
            "metadata": {
                "method":"reedit_claim"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function KirimKlaimPeriode($start_dt,$stop_dt,$jenis_rawat,$date_type){	
        $request ='{
            "metadata": {
                "method":"send_claim"
            },
            "data": {
                "start_dt":"'.$start_dt.'",
                "stop_dt":"'.$stop_dt.'",
                "jenis_rawat":"'.$jenis_rawat.'",
                "date_type":"'.$date_type.'",
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function KirimKlaimIndividual($nomor_sep){	
        $request ='{
            "metadata": {
                "method":"send_claim_individual"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function MenarikDataKlaimPeriode($start_dt,$stop_dt,$jenis_rawat){	
        $request ='{
            "metadata": {
                "method":"pull_claim"
            },
            "data": {
                "start_dt":"'.$start_dt.'",
                "stop_dt":"'.$stop_dt.'",
                "jenis_rawat":"'.$jenis_rawat.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function MengambilDataDetailPerklaim($nomor_sep){	
        $request ='{
            "metadata": {
                "method":"get_claim_data"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function MengambilSetatusPerklaim($nomor_sep){	
        $request ='{
            "metadata": {
                "method":"get_claim_status"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function MenghapusKlaim($nomor_sep,$coder_nik){	
        $request ='{
            "metadata": {
                "method":"delete_claim"
            },
            "data": {
                "nomor_sep":"'.$nomor_sep.'",
                "coder_nik":"'.$coder_nik.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
    function CetakKlaim($nomor_sep){	
        $request ='{
            "metadata": {
                "method": "claim_print"
            },
            "data": {
                "nomor_sep": "'.$nomor_sep.'"
            }
        }';
        $msg = $this->request($request);
        $request = json_decode($msg,true);
        if($request['metadata']['message']=="Ok"){
            return $msg;
        }else{
            return $msg;
        }
    }
    
    function CariDiagnosa($param){	
        $request ='
        {
            "metadata": {
                "method": "search_diagnosis"
            },
            "data": {
                "keyword": "'.$param.'"
            }
        }';
        $msg = $this->request($request);
        return $msg;
    }
    
}