<?php
/**
 * Class untuk mengenerate Tips/Petunjuk pembuatan master data
 */

class TipsMasterData extends CWidget
{   
    public $type;
    public $content;
    public function run()
    {
         // this method is called by CController::endWidget()
         switch ($this->type) {
             case 'admin' : $this->render('tipsMasterData/tipsMasterDataAdmin',array('content'=>$this->content)); break;
             case 'create' : $this->render('tipsMasterData/tipsMasterDataCreate',array('content'=>$this->content)); break;
             case 'update' : $this->render('tipsMasterData/tipsMasterDataUpdate',array('content'=>$this->content)); break;
             case 'list' : $this->render('tipsMasterData/tipsMasterDataList',array('content'=>$this->content)); break;
             case 'transaksi' : $this->render('tipsMasterData/tipsMasterDataTransaksi',array('content'=>$this->content)); break;
             default : $this->render('tipsMasterData/tipsMasterDataDefault',array('content'=>$this->content)); break;
         }
         
    }
}
?>
