
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'supplier_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->supplier_id',
                ),
		'supplier_kode',
		'supplier_nama',
		'supplier_namalain',
		'supplier_alamat',
		'supplier_propinsi',
		/*
		'supplier_kabupaten',
		'supplier_telp',
		'supplier_fax',
		'supplier_kodepos',
		'supplier_npwp',
		'supplier_norekening',
		'supplier_namabank',
		'supplier_rekatasnama',
		'supplier_matauang',
		'supplier_website',
		'supplier_email',
		'supplier_logo',
		'supplier_cp',
		'supplier_cp_hp',
		'supplier_cp_email',
		'supplier_cp2',
		'supplier_cp2_hp',
		'supplier_cp2_email',
		'supplier_jenis',
		'supplier_termin',
		'supplier_aktif',
		*/
 
        ),
    )); 
?>