<?php
$this->breadcrumbs=array(
	'Supplier Ms'=>array('index'),
	$model->supplier_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' SupplierM #'.$model->supplier_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' SupplierM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' SupplierM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' SupplierM', 'icon'=>'pencil','url'=>array('update','id'=>$model->supplier_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' SupplierM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->supplier_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' SupplierM', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'supplier_id',
		'supplier_kode',
		'supplier_nama',
		'supplier_namalain',
		'supplier_alamat',
		'supplier_propinsi',
		'supplier_kabupaten',
		'supplier_telp',
		'supplier_fax',
		'supplier_kodepos',
		'supplier_npwp',
		'supplier_norekening',
		'supplier_namabank',
		'supplier_rekatasnama',
		'supplier_matauang',
		'supplier_website',
		'supplier_email',
		'supplier_logo',
		'supplier_cp',
		'supplier_cp_hp',
		'supplier_cp_email',
		'supplier_cp2',
		'supplier_cp2_hp',
		'supplier_cp2_email',
		'supplier_jenis',
		'supplier_termin',
		'supplier_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>