<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'supplier-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'supplier_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'supplier_kode',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'supplier_nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_namalain',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textAreaRow($model,'supplier_alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'supplier_propinsi',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_kabupaten',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_telp',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_fax',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_kodepos',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_npwp',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_norekening',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_namabank',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_rekatasnama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_matauang',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_website',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_email',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'supplier_logo',array('class'=>'span5','maxlength'=>500)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp_hp',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp_email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp2',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp2_hp',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_cp2_email',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'supplier_jenis',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'supplier_termin',array('class'=>'span5')); ?>

	<?php echo $form->checkBoxRow($model,'supplier_aktif'); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
