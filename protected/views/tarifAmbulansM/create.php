<?php
$this->breadcrumbs=array(
	'Tarif Ambulans Ms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TarifAmbulansM', 'url'=>array('index')),
	array('label'=>'Manage TarifAmbulansM', 'url'=>array('admin')),
);
?>

<h1>Create TarifAmbulansM</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>