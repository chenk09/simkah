<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tarif-ambulans-m-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'daftartindakan_id'); ?>
		<?php echo $form->textField($model,'daftartindakan_id'); ?>
		<?php echo $form->error($model,'daftartindakan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tarifambulans_kode'); ?>
		<?php echo $form->textField($model,'tarifambulans_kode',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'tarifambulans_kode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kepropinsi_nama'); ?>
		<?php echo $form->textField($model,'kepropinsi_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kepropinsi_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kekabupaten_nama'); ?>
		<?php echo $form->textField($model,'kekabupaten_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kekabupaten_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kekecamatan_nama'); ?>
		<?php echo $form->textField($model,'kekecamatan_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kekecamatan_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kekelurahan_nama'); ?>
		<?php echo $form->textField($model,'kekelurahan_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'kekelurahan_nama'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jmlkilometer'); ?>
		<?php echo $form->textField($model,'jmlkilometer'); ?>
		<?php echo $form->error($model,'jmlkilometer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tarifperkm'); ?>
		<?php echo $form->textField($model,'tarifperkm'); ?>
		<?php echo $form->error($model,'tarifperkm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tarifambulans'); ?>
		<?php echo $form->textField($model,'tarifambulans'); ?>
		<?php echo $form->error($model,'tarifambulans'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->