<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'tarifambulans_id'); ?>
		<?php echo $form->textField($model,'tarifambulans_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'daftartindakan_id'); ?>
		<?php echo $form->textField($model,'daftartindakan_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tarifambulans_kode'); ?>
		<?php echo $form->textField($model,'tarifambulans_kode',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kepropinsi_nama'); ?>
		<?php echo $form->textField($model,'kepropinsi_nama',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kekabupaten_nama'); ?>
		<?php echo $form->textField($model,'kekabupaten_nama',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kekecamatan_nama'); ?>
		<?php echo $form->textField($model,'kekecamatan_nama',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kekelurahan_nama'); ?>
		<?php echo $form->textField($model,'kekelurahan_nama',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jmlkilometer'); ?>
		<?php echo $form->textField($model,'jmlkilometer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tarifperkm'); ?>
		<?php echo $form->textField($model,'tarifperkm'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tarifambulans'); ?>
		<?php echo $form->textField($model,'tarifambulans'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->