<?php
$this->breadcrumbs=array(
	'Tarif Ambulans Ms'=>array('index'),
	$model->tarifambulans_id,
);

$this->menu=array(
	array('label'=>'List TarifAmbulansM', 'url'=>array('index')),
	array('label'=>'Create TarifAmbulansM', 'url'=>array('create')),
	array('label'=>'Update TarifAmbulansM', 'url'=>array('update', 'id'=>$model->tarifambulans_id)),
	array('label'=>'Delete TarifAmbulansM', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->tarifambulans_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TarifAmbulansM', 'url'=>array('admin')),
);
?>

<h1>View TarifAmbulansM #<?php echo $model->tarifambulans_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'tarifambulans_id',
		'daftartindakan_id',
		'tarifambulans_kode',
		'kepropinsi_nama',
		'kekabupaten_nama',
		'kekecamatan_nama',
		'kekelurahan_nama',
		'jmlkilometer',
		'tarifperkm',
		'tarifambulans',
	),
)); ?>
