<?php
$this->breadcrumbs=array(
	'Tarif Ambulans Ms'=>array('index'),
	$model->tarifambulans_id=>array('view','id'=>$model->tarifambulans_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TarifAmbulansM', 'url'=>array('index')),
	array('label'=>'Create TarifAmbulansM', 'url'=>array('create')),
	array('label'=>'View TarifAmbulansM', 'url'=>array('view', 'id'=>$model->tarifambulans_id)),
	array('label'=>'Manage TarifAmbulansM', 'url'=>array('admin')),
);
?>

<h1>Update TarifAmbulansM <?php echo $model->tarifambulans_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>