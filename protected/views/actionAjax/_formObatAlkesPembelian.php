<tr>
	<td>
		<?php echo CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE));?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']obatalkes_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']satuankecil_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']sumberdana_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']satuanbesar_id',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']stokakhir',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']maksimalstok',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']minimalstok',array());?>
		<?php echo CHtml::activeHiddenField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']tglkadaluarsa',array());?>
		<?php $total_harga = (($modPermintaanDetail->jmlkemasan * $modObatAlkes->harganetto) * $modPermintaanDetail->jmlpermintaan);?>
	</td>
	<td><?php echo $modObatAlkes->obatalkes_nama;?></td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmlpermintaan',array('class'=>'span1 angka','onkeyup'=>'hitung_total_alkes(this)','readonly'=>false));?>
		<?php echo $modObatAlkes->satuanbesar->satuanbesar_nama?>
	</td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmlkemasan',array('class'=>'span1 angka','onkeyup'=>'hitung_total_alkes(this)','readonly'=>false));?>
		<?php echo $modObatAlkes->satuankecil->satuankecil_nama ."/". $modObatAlkes->satuanbesar->satuanbesar_nama; ?>
		<?php echo CHtml::HiddenField('Permintaandetail['.$modObatAlkes->obatalkes_id . '][jmlkemasan_temp]',$modObatAlkes->kemasanbesar,array(
			'readonly'=>TRUE
		));?>
	</td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargabelibesar', array('class'=>'input-small','readonly'=>true));?>
	</td>
	<td>
		<?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']harganettoper',array('class'=>'input-small angka','onkeyup'=>'hitung_total_alkes(this)','readonly'=>true));?>
	</td>
	<td>
		<?php echo CHtml::textField('PermintaandetailT['. $modObatAlkes->obatalkes_id .'][totalHargaBersih]', $total_harga, array(
			'class'=>'input-small angka',
			'readonly'=>true
		));?>
	</td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']persendiscount',array(
		'class'=>'decimal',
		'size'=>'4',
		'maxlength'=>'4',
		'style'=>'width:30px;',
		'readonly'=>false,
		'onkeyup'=>'hitung_jumlah_discount(this)'
	));?>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']jmldiscount',array(
		'class'=>'input-small angka',
		'readonly'=>false,
		'onkeyup'=>'hitung_discount(this)'
	));?></td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargappnper',array(
		'onkeyup'=>'hitung_ppn(this)',
		'class'=>'input-small ppn',
		'readonly'=>false
	));?></td>
	<td><?php echo CHtml::activetextField($modPermintaanDetail,'['.$modObatAlkes->obatalkes_id.']hargapphper',array(
		'class'=>'input-small pph angka',
		'readonly'=>true
	));?></td>
	<td>
	<?php $subTotal = ($modPermintaanDetail->jmlkemasan * $modPermintaanDetail->hargasatuanper) * $modPermintaanDetail->jmlpermintaan;?>
	<?php echo CHtml::textField('PermintaandetailT['. $modObatAlkes->obatalkes_id .'][subTotal]', $subTotal, array('class'=>'input-small subTotal currency','readonly'=>true));?></td>
	<td><?php echo CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'','onclick'=>'removeRow(this);return false;','style'=>'text-decoration:none;'));?></td>
</tr>
