<tr>   
    <td><?php 
        echo CHtml::activeHiddenField($modDetail, '[]barang_id', array('class'=>'barang')); 
        // echo $modBarang->bidang->subkelompok->kelompok->golongan->golongan_nama; 
        echo $modBarang->barang_kode;
        ?>
    </td>
    <td><?php echo $modBarang->barang_type ?></td>
    <td><?php echo $modBarang->barang_nama ?></td>
    <td><?php echo $modBarang->barang_merk.' / '.$modBarang->barang_noseri ?></td>
    <td><?php echo $modBarang->barang_ukuran.' / '.$modBarang->barang_bahan ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]hargabeli', array('class'=>'span1 numbersOnly beli',)); ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]hargasatuan', array('class'=>'span1 numbersOnly satuan', )); ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]jmlbeli', array('class'=>'span1 numbersOnly qty', )); ?></td>
    <td><?php echo CHtml::activeDropDownList($modDetail, '[]satuanbeli', Satuanbarang::items(), array('empty'=>'-- Pilih --', 'class'=>'span2')); ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]ppn', array('class'=>'span1 numbersOnly ppn', )); ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]persendiskon', array('class'=>'span1 numbersOnly discount', )); ?></td>
    <td><?php echo CHtml::activeTextField($modDetail, '[]jmldlmkemasan', array('class'=>'span1 numbersOnly')); ?></td>
    <td><?php echo Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel')); ?></td>
</tr>        