<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'ubahstatuskamar-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<legend class="rim">Ubah Status Keterangan Kamar</legend>
	<p class="help-block"><?php //echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary($modUbah); ?>
        <table class="table">
            <tr>
                <td>
                     <?php echo CHtml::hiddenField('idKamar',''); ?>
                     <?php echo $form->dropDownListRow($modUbah,'keterangan_kamar', CHtml::listData(LookupM::model()->findAll("lookup_value = any (array['CLEANING','KOSONG','STERILISASI','RUSAK'])"),'lookup_value','lookup_name'),array('empty'=>'-- Pilih --')); ?>
                    
                </td>
            </tr>
        </table>
	<div class="form-actions">
            <?php echo CHtml::htmlButton($modUbah->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createAbsoluteUrl('pendaftaranPenjadwalan/InformasiKamar/index'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'$("#dialogUbahStatusKamar").attr("src",$(this).attr("href")); window.parent.$("#dialogUbahStatusKamar").dialog("close");return false;'));?>
	</div>

<?php $this->endWidget(); ?>
