<legend class="rim2">Daftar Ruangan</legend> 
<table class="table table-bordered table-condensed table-striped">
    <thead>
        <tr>
            <th>Pilih</th>
            <th>Ruangan</th>
            <th>Kamar Ruangan</th>
            <th>No Booking</th>
            <th>Tgl Booking</th>
            <th>Kelas Pelayanan</th>
        </tr>
    </thead>
    <tbody>
        <?php
            if(count($model)>0){
                foreach($model as $key=>$models){
                    
        ?>
        <tr>
            <td><?php echo CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien","onClick"=>"setDataAdmisi($models->ruangan_id,$models->kamarruangan_id,$models->kelaspelayanan_id)"));?></td>
            <td><?php echo $models->ruangan->ruangan_nama; ?></td>
            <td><?php echo "No Kamar :".$models->kamarruangan->kamarruangan_nokamar. " No Bed :".$models->kamarruangan->kamarruangan_nobed."" ?></td>
            <td><?php echo $models->bookingkamar_no ?></td>
            <td><?php echo $models->tglbookingkamar ?></td>
            <td><?php echo $models->kelaspelayanan->kelaspelayanan_nama ?></td>
        </tr>
        <?php
                            
                }
            }
        ?>
    </tbody>
</table>
<script>
    <?php 
        $urlGetKamarKosong = Yii::app()->createUrl('ActionDynamic/GetKamarKosong',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));
    ?>
    function setDataAdmisi(idRuangan,idKamar,idKelasPelayanan){
//        alert(idRuangan+','+idKamar+','+idKelasPelayanan);
        var ruangan_id = idRuangan;
        var kamarruangan_id = idKamar;
        var kelaspelayanan_id = idKelasPelayanan;
        $('#PPPasienAdmisiT_ruangan_id').val(ruangan_id);
        updateKamarRuanganAll(ruangan_id);
        listDokterRuangan(ruangan_id);
        listKasusPenyakitRuangan(ruangan_id);
        listKelasPelayananRI(ruangan_id);  
        if(jQuery.isNumeric(ruangan_id)){
            setTimeout(function(){
                $('#PPPasienAdmisiT_kamarruangan_id').val(kamarruangan_id)
            },1000);
        }   
        $('#dialogAddRuangan').dialog('close');
    }
</script>