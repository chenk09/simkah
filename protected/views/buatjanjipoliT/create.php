<?php
$this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
    <legend class="rim2">Transaksi Buat Janji Poli</legend>

    <?php echo $this->renderPartial('_form', array('modPasien'=>$modPasien,'modPPBuatJanjiPoli'=>$modPPBuatJanjiPoli
)); ?>
</fieldset>
