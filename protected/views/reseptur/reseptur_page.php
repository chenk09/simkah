<?php
$this->breadcrumbs=array(
    'Reseptur Ranap',
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien', array('modPendaftaran' => $modPendaftaran, 'modPasien' => $modPasien, 'modAdmisi'=>$modAdmisi));
$this->renderPartial('/_tabulasi',array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi));
echo '<legend class="rim">RESEPTUR</legend>';
?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>1,
    )
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number0',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjreseptur-t-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>'#RJResepturT_noresep',
    'htmlOptions'=>array(
        'onKeyPress'=>'return disableKeyPress(event)',
    )
));?>
<?php $this->renderPartial('_formInputObat',array('form'=>$form,'modReseptur'=>$modReseptur)); ?>
<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>Nama Obat</th>
        <th style="width: 15px">Sumber Dana</th>
        <th style="width: 15px">Satuan Kecil</th>
        <th style="width: 15px">Qty</th>
        <th style="width: 15px">Harga</th>
        <th style="width: 15px">Sub Total</th>
        <th style="width: 15px">Signa</th>
        <th style="width: 15px">Etiket</th>
        <th style="width: 15px">&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <tr class="no_record">
        <td colspan="7">Data tidak ditemukan</td>
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="5" style="text-align: right;"><b>Total Harga</b></td>
        <td><input style="width: auto;" type="text" readonly name="totalHargaReseptur" id="totalHargaReseptur" class="inputFormTabel currency" /></td>
        <td colspan="3">&nbsp;</td>
    </tr>
    </tfoot>
</table>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php
    $content = $this->renderPartial('../tips/tips',array(),true);
    $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
    ?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    $(document).on('submit', "#rjreseptur-t-form", function (event){
        var formdata = new FormData(this);
        var vUri = $(this).attr('action');

        swal.queue([{
            confirmButtonText: 'Lanjutkan',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: 'Batal',
            text:'Konfirmasi proses dokumen',
            preConfirm: function (){
                return new Promise(function (resolve){
                    $.ajax({
                        type: "POST",
                        data:formdata,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: 'json',
                        url: vUri,
                        success: function(response, statusText, xhr, $form)
                        {
                            swal.close();
                            if(response.status == 'not')
                            {
                                swal({
                                    html: response.msg,
                                    type: 'error'
                                });
                            }
                        },
                        error: function(response, statusText, xhr, $form)
                        {
                            swal.close();
                        }
                    });
                })
            }
        }])
        event.preventDefault();
    });
</script>
