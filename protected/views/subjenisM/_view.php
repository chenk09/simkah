<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->subjenis_id),array('view','id'=>$data->subjenis_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenisobatalkes_id')); ?>:</b>
	<?php echo CHtml::encode($data->jenisobatalkes_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_kode')); ?>:</b>
	<?php echo CHtml::encode($data->subjenis_kode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_nama')); ?>:</b>
	<?php echo CHtml::encode($data->subjenis_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_namalainnya')); ?>:</b>
	<?php echo CHtml::encode($data->subjenis_namalainnya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_farmasi')); ?>:</b>
	<?php echo CHtml::encode($data->subjenis_farmasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjenis_aktif')); ?>:</b>
	<?php echo CHtml::encode($data->subjenis_aktif); ?>
	<br />


</div>