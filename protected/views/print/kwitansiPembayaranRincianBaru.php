<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: left; 
        width:160px;
        color:black;
        padding-right:10px;
        font-size: 12pt;
    }
    table{
        font-size:12px;
    }
');
?>
<style>
    td, div{
        font-size: 12pt;
    }
    td .uang{
        text-align: right;
    }
    td .total{
        border-top: 1px solid #000000;
        text-align: right;
        font-weight: bold;
    }
    td .totalSeluruh{
        border-bottom: 1px solid #000000;
        text-align: right;
        font-weight: bold;
    }
</style>

<table width="88%" style="margin:0px;" cellpadding="0" cellspacing="0">
    <tr>
        <td>
            <table width="100%" cellpadding="0" cellspacing="0">
                    <td width="50%">
                        <label class='control-label'>
                            No. RM/Reg
                        </label>: 
                            <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
                            <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>
                    </td>
                    <td width="5%"></td>
                    <td>
                        <label class='control-label'>
                            Nama PJP
                        </label>: 
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>

                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('nama_pasien')); ?>
                        </label>: 
                        <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?>
                    </td>
                    <td></td>
                    <td>   
                        <label class='control-label'>
                            Alamat PJP
                        </label>: 
                        <?php
                            if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                            {
                                echo CHtml::encode($modPendaftaran->penanggungJawab->alamat_pj);
                            }else{
                                echo CHtml::encode($modPendaftaran->pasien->alamat_pasien);
                            }
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>
                            <?php echo CHtml::encode($modPendaftaran->pasien->getAttributeLabel('jeniskelamin')); ?>
                        </label>: 
                            <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?> /
                            <?php echo CHtml::encode($modPendaftaran->umur); ?>
                    </td>
                    <td></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Unit Pelayanan </label>: 
                            <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
                    </td>
                    <Td></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Dokter Pemeriksa </label>: 
                        <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>                        
                    </td>
                    <Td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>
                        <label class='control-label'>Tgl Perawatan </label>: 
                    </td>
                    <Td></td>
                    <td><label class='control-label'>Perusahaan Penjamin </label>: -</td>
                </tr>
            </table>            
        </td>
    </tr>
    <tr>
        <td>
            <div align="center" style="font-size: 14pt;font-weight: bold;border-bottom: 1px solid #000000;padding: 10px;margin-bottom: 15px;">
                <?php echo $judulPrint; ?>
            </div>
            <?php
                    
                $row = array();
                foreach($modRincian as $i=>$val)
                {
                    $ruangan_id = $val->ruangan_id;
                    $row[$ruangan_id]['nama'] = $val->ruangan_nama;
                    $row[$ruangan_id]['ruangan_id'] = $val->ruangan_id;
                    $row[$ruangan_id]['kategori'][$i]['nama_pegawai'] = $val->nama_pegawai;
                    $row[$ruangan_id]['kategori'][$i]['tindakanpelayanan_id'] = $val->tindakanpelayanan_id;
                    $row[$ruangan_id]['kategori'][$i]['daftartindakan_id'] = $val->daftartindakan_id;
                    $row[$ruangan_id]['kategori'][$i]['daftartindakan_nama'] = $val->daftartindakan_nama;
                    $row[$ruangan_id]['kategori'][$i]['kelas'] = $val->kelaspelayanan_nama;
                    $row[$ruangan_id]['kategori'][$i]['harga'] = (isset($val->tarif_medis) ? ($val->tarif_satuan - $val->tarif_medis) : $val->tarif_satuan);
                    $row[$ruangan_id]['kategori'][$i]['qty'] = $val->qty_tindakan;
                    
                    $row[$ruangan_id]['kategori'][$i]['total'] = ($row[$ruangan_id]['kategori'][$i]['harga'] * $row[$ruangan_id]['kategori'][$i]['qty']);
                    $harga = TindakanpelayananT::model()->findAllByPk($val->tindakanpelayanan_id);
                    $row[$ruangan_id]['kategori'][$i]['harga_dokter'] = (isset($val->tarif_medis) ? $val->tarif_medis : 0);
                    $row[$ruangan_id]['kategori'][$i]['total_dokter'] = (isset($val->tarif_medis) ? ($val->qty_tindakan * $val->tarif_medis) : 0);
                }
//                echo "<pre>";
//                    print_r($row);
//                    exit;
            ?>
            <table width="100%" style='margin-left:auto; margin-right:auto;' class='table table-striped table-bordered table-condensed'>
                <thead>
                    <tr>
                        <th width="8%">&nbsp;</th>
                        <th width="30%">&nbsp;</th>
                        <th>Kelas</th>
                        <th>Harga (Rp.)</th>
                        <th>Banyak</th>
                        <th>Total (Rp.)</th>
                    </tr>
                </thead>
                <?php
                    $cols = '';
                    $total_biaya = 0;
                    
                    foreach($row as $values)
                    {
                        $cols .= '<tr>';
                        $cols .= '<td colspan=6><b>'. strtoupper($values['nama']) .'</b></td>';
                        $cols .= '</tr>';
                        $col = '';
                        $total = 0;
                        foreach($values['kategori'] as $val)
                        {
                            //menentukan harga obat
                            if($values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ){
                               $modObat = ObatalkesM::model()->findByAttributes(array('obatalkes_id'=>$val['daftartindakan_id'])); 
                               $val['harga'] = $modObat->hjaresep;
                               $val['total'] = $val['qty'] * $val['harga'];
                            }
                            
                            $col .= '<tr>';
                            $col .= '<td>&nbsp;</td>';
                            $col .= '<td>'. $val['daftartindakan_nama'] .'</td>';
                            $col .= '<td>'. $val['kelas'] .'</td>';
                            $col .= '<td class="uang">'. number_format($val['harga'],0,'','.') .'</td>';
                            $col .= '<td class="uang">'. $val['qty'] .'</td>';
                            $col .= '<td class="uang">'. number_format($val['total'],0,'','.') .'</td>';
                            $col .= '</tr>';
                            if($values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ){
                                //Pegawai tidak di tulis jika apotek
                            }else{
                                if(strlen($val['nama_pegawai']) > 0)
                                {
                                    $col .= '<tr>';
                                    $col .= '<td>&nbsp;</td>';
                                    $col .= '<td>'. $val['nama_pegawai'] .'</td>';
                                    $col .= '<td>'. $val['kelas'] .'</td>';
                                    $col .= '<td class="uang">'. number_format($val['harga_dokter'],0,'','.') .'</td>';
                                    $col .= '<td class="uang">'. $val['qty'] .'</td>';
                                    $col .= '<td class="uang">'. number_format($val['total_dokter'],0,'','.') .'</td>';
                                    $col .= '</tr>';                                
                                }
                            }
                            $total += $val['total'] + $val['total_dokter'];
                        }
                        $col .= '<tr">';
                        $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                        $col .= '<td class="total">'. number_format($total,0,'','.') .'</td>';
                        $col .= '</tr>';
                        
                        
                        $cols .= $col;
                        $total_biaya += $total;   
                    }
                    echo($cols);
                ?>
            </table>
            <table width="100%" style="margin-top:20px;">
                <tr>
                    <td width="50%" align="center" align="top">
                        <table width="100%">
                            <tr>
                                <td width="50%" align="center">
                                    <div>Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?></div>
                                    <div>Petugas RSJK</div>
                                    <div style="margin-top:60px;"><?php echo $data['nama_pegawai']; ?></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="right" valign="top">
                        <table width="100%" style="text-align: right;">
                            <tr>
                                <td width="50%"><b>Total Biaya</b></td>
                                <td width="3%">:</td>
                                <td class="totalSeluruh"><?php echo number_format($total_biaya,0,'','.'); ?></td>
                            </tr>
                            <tr>
                                <td><b>Deposit</b></td>
                                <td>:</td>
                                <td class="totalSeluruh"><?php echo number_format($data['uang_cicilan'],0,'','.'); ?></td>
                            </tr>
                            <tr>
                                <td><b>Tanggungan Pasien</b></td>
                                <td>:</td>
                                <td class="totalSeluruh">
                                    <?php 
                                        $kembalian = $total_biaya;
                                        if($data['uang_cicilan'] > 0){
                                            if($data['uang_cicilan'] < $total_biaya)
                                            {
                                                $kembalian = $total_biaya - $data['uang_cicilan'];
                                            }                                            
                                        }
                                        echo number_format($kembalian,0,'','.');
                                    ?>
                                </td>
                            </tr>
                        </table>
                        <div id="cetakan_jum" style="margin-top: 40px;font-size: 11px"></div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<script type="text/javascript">

    function insertCetakan()
    {
        var params = {jenis_cetakan:'<?=Params::jenisCetakan($data['jenis_cetakan'])?>',pendaftaran_id:'<?=$modPendaftaran->pendaftaran_id?>'};
        
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/updateJumlahCetakan');?>", {id:params},
            function(data)
            {
                if(data.status == 'not')
                {
                    console.log('insert cetakan data error');
                }else{
                    $('#cetakan_jum').text('Cetakan Ke ' + data.jumlah);
                }
            }, "json"
        );
    }
    insertCetakan();

</script>