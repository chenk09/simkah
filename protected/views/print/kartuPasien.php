<style>
    .content{
        width:325px;
        height:200px;
        border:0px solid;
        margin: 0px 0px 0px 0px;
    }
    .badan{
        margin-top:90px;
        border:0px solid;
        text-align: right;
		border:0px solid #000000;
		width:297px;
    }
    .badan div{
        font-size: 16px;
        text-transform: uppercase;
        z-index: -9;
    }    
</style>
<div class="content">
    <div class="badan">
        <div><b><?=$modPasien->nama_pasien?></b></div>
        <div><b><?=implode(".", str_split($modPasien->no_rekam_medik, 2));?></b></div>
    </div>
	<div style="text-align:right;">
		<?php
			$this->widget('ext.MyBarcode.MyBarcode',array(
					'code'=>$modPasien->no_rekam_medik,
					'imageOptions'=>array(
						'style'=>'height:50px;',
						'alt'=>null
					)
				)
			);
		?>
	</div>
</div>