<style>
    table td{
        font-size:11px;
        text-align:left;
        font-weight:bold;
    }
</style>

<?php 

function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}

 ?>

 <?php if ($modPasien->jeniskelamin=="LAKI-LAKI") {
                $jk =  "(L)";
            } else {
                $jk =  "(P)";
}
?>
<table class="" width="100%"  >
    <td style="width:10px;height:5px;"></td>

    <tr>
        <td style="width:70px;border: 0px solid;padding: 0px;font-weight: bold;font-size: 17pt;padding-left:15px;font-family:calibri;">
            NO RM  
        </td>
        <td style="border: 0px solid;padding: 0px;font-weight: bold;font-size:18pt;padding-left:10px;font-family:calibri;">
             : 
        </td>
        <td style="border: 0px solid;padding: 0px;font-weight: bold;font-size: 20pt;padding-left:10px;font-family:calibri;">
            <?php echo $modPasien->no_rekam_medik; ?>
        </td>
    </tr>
    
    <tr style= "width:10px;height:10px;"> 
        <td valign="top" style="width:100px;height:10px;border: 0px solid;padding: 0px;font-weight: bold;font-size: 17pt;padding-left:15px;font-family:calibri;">
            NAMA <br>
        </td>
        <td valign="top" style="height:10px;width:10px;border: 0px solid;padding: 0px;font-weight: bold;font-size: 17pt;padding-left:10px;font-family:calibri;">
             : <br>
        </td>
        <td valign="top" style="height:20px;border: 0px solid;padding: 0px;font-weight: bold;font-size:17pt;padding-left:10px;font-family:calibri;">
           <?php echo ', '. $modPasien->namadepan . ' , '. custom_echo( $modPasien->nama_pasien , 25 ). ' ' . $jk; ?>
        </td>
    </tr> 
    
  
  <tr>
        <td style="width:70px;border: 0px solid;padding: 0px;font-weight: bold;font-size: 17pt;padding-left:15px;font-family:calibri;">
            TGL LAHIR
        </td>
        <td style="border: 0px solid;padding: 0px;font-weight: bold;font-size:18pt;padding-left:10px;font-family:calibri;">
             : 
        </td>
        <td style="border: 0px solid;padding: 0px;font-weight: bold;font-size: 20pt;padding-left:10px;font-family:calibri;">
            <?php echo $modPasien->tanggal_lahir; ?>
        </td>
    </tr>
</table>



