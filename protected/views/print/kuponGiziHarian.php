<td><!-- Makanan Harian !-->
            <table border="1">
            <tr>
                <td colspan="3">
                    <table>
                        <tr>
                            <td>
                                <?php echo $this->renderPartial('application.views.headerReport.headerDefaultKupon'); ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style="margin-left:10px;font-family:arial black;" width="360px">
                        <tr>
                            <td style="text-align:right;"colspan="3">
                                <b><?php 
                                            $format = new CustomFormat();
                                            $tgl = date('Y-m-d',strtotime($format->formatDateTimeMediumForDB($modKirimMenuDiet->tglkirimmenu)));
                                            $tglKirim = $format->formatDateINAtime($tgl);
                                            $namahari = array("Minggu","Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu"); 
                                            echo $namahari[date("w")].", ".$tglKirim;     
                                    ?></b>
                            </td>
                        </tr>
                        <tr>
                            <td>Ruangan</td>
                            <td>:</td>
                            <td><?php echo //$modKirimMenuPasien->ruangan->instalasi->instalasi_nama." - ".
                                            $modKirimMenuPasien->ruangan->ruangan_nama; ?></td>
                        </tr> 
                        <tr>
                            <td>Nama Pasien</td>
                            <td>:</td>
                            <td><?php echo $modPasien->namadepan.$modPasien->nama_pasien.$modPasien->nama_bin; ?></td>
                        </tr>
                        <tr>
                            <td>Diet</td>
                            <td>:</td>
                            <td>
                                <?php 
                                    $criteria = new CDbCriteria();
                                    $criteria->select = 'menudiet_id,kirimmenudiet_id';
                                    $criteria->group = 'menudiet_id,kirimmenudiet_id';
                                    $criteria->compare('kirimmenudiet_id', $modKirimMenuDiet->kirimmenudiet_id);
                                    $modDetail = KirimmenupasienT::model()->findAll($criteria);
                                    foreach($modDetail as $key=>$detail){
                                        if($detail->menudiet->jenisdiet_id == Params::DEFAULT_JENISDIET_MAKANAN_HARIAN){
                                            $expMenuDiet = explode('|',$detail->menudiet->menudiet_nama);
                                            $menuDiet = $detail->menudiet->menudiet_nama;
//                                            $menuDiet = $expMenuDiet[2];
                                            echo $menuDiet;
                                        }
//                                        else{
//                                            $expMenuDiet = explode('|',$detail->menudiet->menudiet_nama);
//                                            $menuDiet = $detail->menudiet->menudiet_namalain;
////                                            $menuDiet = $expMenuDiet[2];
//                                            echo $menuDiet;
//                                        }
                                    }
                                ?>                            
                            </td>
                        </tr>  
                        <tr>
                            <td>Jam Pemberian</td>
                            <td>:</td>
                            <td><?php echo trim(substr($modKirimMenuDiet->tglkirimmenu,-8,8));  // menampilkan Jam Saja; ?></td>
                        </tr>
                        
                    </table>
                </td>
            </tr>
            <tr>
                <td style="text-align:center;margin-top:-40px;font-weight:bold;font-family:lucida handwriting;font-size:x-small;">
        <!--            <div style="text-align:center;margin-top:-40px;font-weight:bold;font-family:lucida handwriting;font-size:x-small;"><hr style="background:white;">-->
                        <font>
                            <span>
                                <i>Untuk konsultasi gizi & cathering diet</br>
                                   dapat menghubungi Unit Gizi RS Jasa Kartini</i>
                            </span>
                        </font>
        <!--            </div>-->
                </td>
            </tr>
     </table>
        </td><!-- END makanan harian !-->