<style>
    .table th, .table td{
        line-height: 5px;
    }
    </style>

<table width="100%">
    <tr>
        <td colspan="3">
            <?php echo $this->renderPartial('application.views.headerReport.headerDefault'); ?>
        </td>
    </tr>
    <tr>
        <td align="center" valig="middle" colspan="3">
            <TABLE FRAME=VOID ALIGN=LEFT CELLSPACING=0 COLS=11 RULES=NONE BORDER=0 width="100%">
                <TBODY>
                        <TR>
                                <TD COLSPAN=10 ALIGN=CENTER VALIGN=MIDDLE><B>TANDA BUKTI PEMBAYARAN</B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=10 ALIGN=CENTER VALIGN=MIDDLE><B>NOMOR BUKTI <?php echo $model->nobuktibayar;?></B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD  COLSPAN=11 VALIGN=MIDDLE ALIGN=LEFT >Bendahara Penerimaan/Bendahara Penerimaan Pembantu Telah menerima uang sebesar  Rp <?php echo number_format($model->jmlpembayaran,0,'','.');?></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=11 VALIGN=MIDDLE ALIGN=LEFT>Dengan Huruf (<?php echo $this->terbilang($model->jmlpembayaran);?>)</TD>
                                
                        </TR>
                        <TR>
                            <TD VALIGN=MIDDLE ALIGN=LEFT >Dari Nama                   :    <?php echo $model->darinama_bkm;?></TD>
                            <TD COLSPAN=1 VALIGN=MIDDLE ALIGN=LEFT></TD>
                            <TD COLSPAN=9 VALIGN=MIDDLE ALIGN=LEFT>    Alamat                        :    <?php echo $model->alamat_bkm;?></TD>       
                        </TR>
                        <TR>
                            <TD VALIGN=MIDDLE ALIGN=LEFT >No. Rekam Medik / No. Pendaftaran                   :    <?php echo $model->pembayaran->pendaftaran->pasien->no_rekam_medik."&nbsp;/&nbsp;".$model->pembayaran->pendaftaran->no_pendaftaran ;?></TD>      
                        </TR>
                        <TR>
                            <TD VALIGN=MIDDLE ALIGN=LEFT >Kelas Pelayanan / Kasus Penyakit                   :    <?php echo $model->pembayaran->pendaftaran->kelaspelayanan->kelaspelayanan_nama."&nbsp;/&nbsp;".$model->pembayaran->pendaftaran->kasuspenyakit->jeniskasuspenyakit_nama ;?></TD>      
                        </TR>
                        <TR>
                            <TD COLSPAN=11 VALIGN=MIDDLE ALIGN=LEFT>Sebagai Pembayaran   :    <?php echo $model->sebagaipembayaran_bkm;?></TD>
                                
                        </TR>
                        <TR>
                            <TD COLSPAN=11 VALIGN=MIDDLE ALIGN=LEFT>Cara Membayar / Penjamin   :    <?php echo $model->pembayaran->pendaftaran->carabayar->carabayar_nama."&nbsp;/&nbsp;".$model->pembayaran->pendaftaran->penjamin->penjamin_nama ;?></TD>
                                
                        </TR>
<!--                        <TR>
                            <TD COLSPAN=11 VALIGN=MIDDLE ALIGN=LEFT>No. Resep   :    <?php //echo $obatalkespasien->penjualanresep->noresep; ?></TD>
                                
                        </TR>-->

                        

                        <tr>
                            <td colspan="11">
                                <table class="table table-condensed table-striped table-bordered" style='margin-top:5px;'>
                                        <thead>

                                            <tr>
                                                <th>No.</th><th>Uraian</th><th>Harga</th><th>QTY</th><th>Diskon</th><th>JUMLAH</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                             $oaSudahBayar = OasudahbayarT::model()->findAllByAttributes(array('pembayaranpelayanan_id'=>$rincianpembayaran->pembayaranpelayanan_id));
                                            $total = 0;
                                            $kelompoktindakan = '';
                                            $counter = 1;
                                            
                                            $totalSubTotal = 0;
                                            foreach($rincianpembayaran as $i=>$rincian) { 
//                                                    $biayaLainLain += $value['biayaadministrasi']+$value['biayaservice']+$value['biayakonseling'];
//                                                    $value['harga']+=$biayaLainLain;
//                                                    if ($i == 'oa'){
//                                                        $value['harga'] += $model->biayaadministrasi +$model->biayamaterai;
//                                                    }
//                                                    if ($firstKey){
//                                                        $value['harga'] += $model->biayaadministrasi +$model->biayamaterai;
//                                                        $firstKey = false;
//                                                    }
                                                    $subTotal = ($rincian->jmlbiaya_tindakan * $rincian->qty_tindakan);
                                                    $totalSubTotal+= $subTotal;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo ($counter++); ?></td>
                                                        <td><?php echo $rincian->daftartindakan_nama  ?><?php //echo '/'.$rincian->daftartindakan_nama ?></td>
                                                        <td style="text-align: right;"><?php echo MyFunction::formatNumber($rincian->jmlbiaya_tindakan ); ?></td>
                                                        <td style="text-align: right;"><?php echo ($rincian->qty_tindakan); ?></td>
                                                        <td style="text-align: right;"><?php echo MyFunction::formatNumber($rincian->totaldiscount); ?></td>
                                                        <td style="text-align: right;"><?php echo MyFunction::formatNumber($rincian->qty_tindakan * $rincian->jmlbiaya_tindakan); ?></td>
                                                    </tr>
                                            <?php 
                                                
                                            }?>
                                            <tr>
                                                <td style="text-align: right;" colspan="5">TOTAL</td>
                                                <td style="text-align: right;"><?php echo MyFunction::formatNumber($modBayar->totalbayartindakan); //echo MyFunction::formatNumber($pembayaran->totaliurbiaya); ?></td>
                                            </tr>
<!--                                            <tr>
                                                <td style="text-align: right;" colspan="5">BIAYA ADMINISTRASI</td>
                                                <td style="text-align: right;"><?php //echo MyFunction::formatNumber($model->biayaadministrasi); //echo MyFunction::formatNumber($pembayaran->totaliurbiaya); ?></td>
                                            </tr>-->
                                            <tr>
                                                <td style="text-align: right;" colspan="5">SUDAH BAYAR</td>
                                                <td style="text-align: right;"><?php echo MyFunction::formatNumber($modBayar->totalbayartindakan); ?></td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: right;" colspan="5">SISA TAGIHAN</td>
                                                <td style="text-align: right;"><?php echo MyFunction::formatNumber($modBayar->totalsisatagihan); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                        
                        <TR>
                            <TD COLSPAN=11 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Tanggal Diterima Uang   :   <?php echo $model->tglbuktibayar;?></TD>
                                
                        </TR>
                        <TR>
                                <TD ALIGN=LEFT COLSPAN=11><BR></TD>
                             
                        </TR>
                        <TR>
                            <?php $pegawai = LoginpemakaiK::pegawaiLoginPemakai(); ?>
                                <TD COLSPAN=6 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE><B>Mengetahui</B><br/>
                                    <B>Bendahara Penerimaan / Bendahara Penerimaan Pembantu</B>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <b><?php echo $pegawai->nama_pegawai; ?></b>
                                    <br/>
<!--                                    <B>NIP. <?php //echo $pegawai->nomorindukpegawai; ?> </B>-->
                                </TD>
                                <TD colspan=5 ALIGN=center VALIGN=TOP><B>Pembayar / Penyetor</B></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=5 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 COLSPAN=11 ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 COLSPAN=11 ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 COLSPAN=11 ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT COLSPAN=7><BR></TD>
                                <TD ALIGN=LEFT COLSPAN=4></TD>
                                
                        </TR>
                </TBODY>
        </TABLE>
        </td>
    </tr>
</table>