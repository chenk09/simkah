<?php $modPasien = PasienM::model()->findByPK($idPasien); ?>
<div align="right" style="margin-top:5cm;">
    <table style="width:300px;margin-right:1cm;border:1px solid #DDDDDD;border-radius:10px !important;">
        <tr>
            <td>Yth</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>No. Lab</td>
            <td>:</td>
            <td></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><?php echo date('d F Y'); ?></td>
        </tr>
        <tr>
            <td>Nama Pasien</td>
            <td>:</td>
            <td><?php echo $modPasien->nama_pasien; ?></td>
        </tr>
        <tr>
            <td>Umur / Jenis Kelamin</td>
            <td>:</td>
            <td><?php echo $modPasien->jeniskelamin; ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td>:</td>
            <td><?php echo $modPasien->alamat_pasien ?></td>
        </tr>
    </table>
</div>
<div style="margin-bottom:1cm;">
    <center><h3>Hasil Pemeriksaan</h3></center>
</div>
<table width="50%" id="tblFormPemeriksaanLab" class="table table-bordered table-condensed">
    <thead>  
    <tr>
        <th>Detail Pemeriksaan</th>
        <th>Hasil Pemeriksaan</th>
        <th>Nilai Rujukan</th>
        <th>Satuan</th>
        <th>Metode</th>
        <th>Keterangan</th>
    </tr>
    </thead> 
<?php
        foreach($modDetailHasilPemeriksaanLab AS $dataDetailpemeriksaaanLab):
            $pemeriksanaanNama = $dataDetailpemeriksaaanLab->pemeriksaanlab->pemeriksaanlab_nama;
            $jenisPemeriksaanNama = $dataDetailpemeriksaaanLab->pemeriksaanlab->jenispemeriksaan->jenispemeriksaanlab_nama;
            echo "<tr>
                    <td>".CHtml::hiddenField('LKDetailHasilPemeriksaanLabT[detailhasilpemeriksaanlab_id][]',$dataDetailpemeriksaaanLab['detailhasilpemeriksaanlab_id'], array('class'=>'span1','readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")).
                          $jenisPemeriksaanNama."/".$pemeriksanaanNama."</td>
                    <td>".$dataDetailpemeriksaaanLab['hasilpemeriksaan']."</td>
                    <td>".$modNilaiRujukan->nilairujukan_nama."</td>    
                    <td>".$modNilaiRujukan->nilairujukan_satuan."</td>    
                    <td>".$modNilaiRujukan->nilairujukan_metode."</td>    
                    <td>".$modNilaiRujukan->nilairujukan_keterangan."</td>    
                </tr>";
        endforeach;
?>
</table>
<table style="width:100%;">
    <tr>
        <td style="width:50%;">
            <table style="width:100%;">
                <tr>
                    <td style="height:2cm">
                        Catatan Lab : <?php echo $modHasilpemeriksaanLab->catatanlabklinik; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php// echo $data->kabupaten->kabupaten_nama.', '.Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d'), 'yyyy-MM-dd'),'long',null); ?> 
                        Printed By <?php echo LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai; ?>
                        &nbsp;
                        <?php echo date('Y/m/d H:i:s'); ?>
                    </td>
                </tr>
            </table>
    </td>
    <td style="width:50%;" align="center">
        Pemeriksa <?php echo '<br/><br/><br/><br/><br/>'.$modHasilpemeriksaanLab->pendaftaran->pegawai->nama_pegawai; ?>
    </td>
    </tr>
</table>
