<style>
    body{
        padding:0px;
    }
</style>
<div style="margin-bottom: 100px;">&nbsp;</div>
<div>
    <table width="600">
        <tr>
            <td align="center" colspan="3"><h3>KWITANSI</h3></td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <table width="100%" border="1">
                    <tr>
                        <td width="50%">
                            <table width="100%">
                                <tr>
                                    <td width="90">Nama Pasien</td>
                                    <td width="20">:</td>
                                    <td><?=$model->pembayaran->pasien->nama_pasien;?></td>
                                </tr>
                            </table>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="90">Alamat</td>
                                    <td width="20">:</td>
                                    <td><?=$model->pembayaran->pasien->alamat_pasien;?></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="110">No. Lab</td>
                                    <td width="20">:</td>
                                    <td><?=$model->pembayaran->pendaftaran->no_pendaftaran;?></td>
                                </tr>
                            </table>                            
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="90">Umur</td>
                                    <td width="20">:</td>
                                    <td><?=$model->pembayaran->pendaftaran->umur;?></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="110">Tgl. Pembayaran</td>
                                    <td width="20">:</td>
                                    <td><?=$model->tglbuktibayar;?></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>        
        <tr>
            <td width="150">Sudah terima dari</td>
            <td width="10">:</td>
            <td><?php  echo $model->darinama_bkm; ?></td>
        </tr>
        <tr>
            <td>Banyak uang</td>
            <td>:</td>
            <td><?php  echo $terbilang; ?></td>
        </tr>
        <tr>
            <td>Untuk pembayaran</td>
            <td>:</td>
            <td><?php  echo $model->sebagaipembayaran_bkm; ?></td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td valign="top">Jumlah Rp. <?php  echo $model->jmlpembayaran; ?></td>
                        <td align="center">
                            <?php echo $tanggal;?>
                            <div style="margin-bottom: 80px;">&nbsp;</div>
                            ( . . . . . . . . . . . )
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>    
</div>
