<div style="margin-top:10px;margin-left:20px;">
	<table width="100%" border="0">
		<tr>
			<td style="width: 200px;padding-left:15px;font-weight: bold;">NORM / NAMA/ JENIS KELAMIN</td>
			<td style="width: 25;">:</td>
			<td><?php echo $modPasien->no_rekam_medik; ?>
				&nbsp; / &nbsp;
				<?php echo strtoupper($modPasien->namadepan." ".$modPasien->nama_pasien)." ";
				echo (!empty($modPasien->nama_bin)) ? Params::DEFAULT_BIN." ".strtoupper($modPasien->nama_bin) : ""; ?>
				&nbsp; / &nbsp;<?php echo $modPasien->jeniskelamin; ?>
			</td> 
		</tr>
		<tr>
			<td style="padding-left:15px;font-weight: bold;">TGL LAHIR/ UMUR/ AGAMA</td>
			<td>:</td>
			<td><?php echo $modPasien->tanggal_lahir; ?>
				/&nbsp; <?php echo $modPendaftaran->umur; ?>
				/&nbsp; <?php echo $modPasien->agama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:15px;font-weight: bold;">ALAMAT LENGKAP</td>
			<td>:</td>
			<td><?php echo strtoupper($modPasien->alamat_pasien)." "
					."RT.".$modPasien->rt." / RW.".$modPasien->rw.", "
					.strtoupper($modPasien->kelurahan->kelurahan_nama).", "
			?></td>
		</tr>
		<tr>
			<td style="padding-left:15px;font-weight: bold;"></td>
			<td></td>
			<td><?php echo strtoupper($modPasien->kecamatan->kecamatan_nama).", "
					.strtoupper($modPasien->kabupaten->kabupaten_nama);?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:15px;font-weight: bold;">NO TELPONE</td>
			<td>:</td>
			<td><?php echo $modPasien->no_mobile_pasien." - ".$modPasien->no_telepon_pasien; ?>
			</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><?php echo $modPasien->nama_ayah; ?> 
				<?php  if (!empty($modPasien->nama_ibu)) {echo "/Ibu : ";} else echo "";
					echo $modPasien->nama_ibu;  ?></td>
		</tr>
	</table>
</div>