<table>
    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td>
                        <?php echo $this->renderPartial('application.views.headerReport.headerDefault'); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valig="middle" colspan="3">
            <b><?php echo $judulLaporan ?></b>
        </td>
    </tr>
    <tr>
        <td align="center" valig="middle" colspan="3">
             Data Pesan Kamar
        </td>
    </tr>
    <tr>
        <td>Nama Pemesan</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->nama; ?></td>
    </tr>
    <tr>
        <td>No. Telp</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->no_telp; ?></td>
    </tr>   
    <tr>
        <td>Alamat</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->alamat; ?></td>
    </tr>
    <tr>
        <td>No Booking Kamar</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->bookingkamar_no ?></td>
    </tr>
     <tr>
        <td>Status booking</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->statusbooking ?></td>
    </tr>
    <tr>
        <td>Ruangan/No.Kamar-Bed</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->ruangan->ruangan_nama; ?>/<?php echo $modBookingKamar->kamarruangan->kamarruangan_nokamar; ?>-<?php echo $modBookingKamar->kamarruangan->kamarruangan_nobed; ?></td>
    </tr>
     <tr>
        <td>Tanggal Transaksi Booking</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->tgltransaksibooking ?></td>
    </tr>
    <tr>
        <td>Tanggal Booking Kamar</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->tglbookingkamar ?></td>
    </tr>
     <tr>
        <td>Keterangan</td>
        <td>:</td>
        <td><?php echo $modBookingKamar->keteranganbooking ?></td>
    </tr>
    
    
    
</table>