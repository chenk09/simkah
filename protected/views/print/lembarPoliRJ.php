<style>
	table td{
		font-size:11px;
	}
</style>
<table width="100%" border="0" cellspacing="5" cellpadding="5">
    <tr style="border-bottom:1px solid #000000">
        <td colspan="2" align="center" valign="middle">
            <div><b>RS Jasa Kartini</b></div>
            <div>Jl. Otto Iskandar Dinata No. 15 Tasikmalaya</div>
        </td>
		<td width="5" align="center" valign="center">
			<div style="width:100px;font-weight:bold;font-size:36px">
			<?php echo $modPendaftaran->no_urutantri; ?>
			</div>
		</td>
    </tr>
</table>
<table width="100%" border="0" cellspacing="2" cellpadding="2">
	<tr>
        <td colspan="3" align="center" valign="middle" style="padding:15px">
            <?php echo strtoupper($judulLaporan); ?>
        </td>
    </tr>
	<tr>
        <td valign="top" width="80">Nama</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPasien->namadepan.$modPasien->nama_pasien; ?></td>
    </tr>
    <tr>
        <td valign="top">No. RM</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo implode(".", str_split($modPasien->no_rekam_medik, 2));?> / <?php echo $modPendaftaran->no_pendaftaran;?></td>
    </tr>
    <tr>
        <td valign="top">Jenis Kelamin</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPasien->jeniskelamin; ?></td>
    </tr>
    <tr>
        <td valign="top">Alamat</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPasien->alamat_pasien; ?></td>
    </tr>
    <tr>
        <td valign="top">Tangal Lahir</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPasien->tanggal_lahir; ?>/<?php echo $modPendaftaran->umur; ?></td>
    </tr>
    <tr>
        <td valign="top">Cara Bayar</td>
        <td valign="top">:</td>
        <td><?php echo $modPendaftaran->carabayar->carabayar_nama; ?>/<?php echo $modPendaftaran->penjamin->penjamin_nama; ?></td>
    </tr>
    <tr>
        <td valign="top" style="padding:15px" align="center" valig="middle" colspan="3">
            PERAWATAN <?php echo strtoupper($modPendaftaran->instalasi->instalasi_nama); ?>
        </td>
    </tr>
    <tr>
        <td valign="top">Poliklinik</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPendaftaran->ruangan->ruangan_nama; ?></td>
    </tr>
    <tr>
        <td valign="top">Dokter</td>
        <td valign="top">:</td>
        <td valign="top"><?php echo $modPendaftaran->dokter->nama_pegawai; ?></td>
    </tr>
</table>