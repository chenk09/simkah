<table border="0" align="center" width="100%">
    <tr>
        <td>
            <div class="list-view" id="yw1">
                <div class="items">
                    <?php 
                        $criteria = new CDbCriteria();
                        $criteria->select = 'menudiet_id,kirimmenudiet_id';
                        $criteria->group = 'menudiet_id,kirimmenudiet_id';
                        $criteria->compare('kirimmenudiet_id', $modKirimMenuDiet->kirimmenudiet_id);
                        $modDetail = KirimmenupasienT::model()->findAll($criteria);
                        
                        foreach($modDetail as $key=>$detail){
                            if($detail->menudiet->jenisdiet_id == Params::DEFAULT_JENISDIET_MAKANAN_HARIAN){
                    ?>
                            <div class="view" style='width:360px;float:left;border-bottom:2px'>
                                <table>
                                    <tr>
                                       <?php  $this->render('kuponGiziHarian',array(
                                                      'modKirimMenuDiet'=>$modKirimMenuDiet,
                                                      'modPendaftaran'=>$modPendaftaran,
                                                      'modPasien'=>$modPasien,
                                                      'modDetailPesan'=>$modDetailPesan,
                                                      'modMenuDiet'=>$modMenuDiet,
                                                      'modKirimMenuPasien'=>$modKirimMenuPasien,
                                        )); ?>
                                    </tr>
                                </table>
                            </div>
                    <?php } ?>
                    <?php if($detail->menudiet->jenisdiet_id == Params::DEFAULT_JENISDIET_MAKANAN_CAIR){ ?>
                            <div class="view" style='width:350px;float:left;border-bottom:2px'>
                                <table>
                                    <tr>
                                       <?php $this->render('kuponGiziCair',array(
                                                      'modKirimMenuDiet'=>$modKirimMenuDiet,
                                                      'modPendaftaran'=>$modPendaftaran,
                                                      'modPasien'=>$modPasien,
                                                      'modDetailPesan'=>$modDetailPesan,
                                                      'modMenuDiet'=>$modMenuDiet,
                                                      'modKirimMenuPasien'=>$modKirimMenuPasien,
                                        )); ?>
                                    </tr>
                                </table>
                            </div>
                    <?php } ?>
                    <?php if($detail->menudiet->jenisdiet_id == Params::DEFAULT_JENISDIET_MAKANAN_EXTRAFOOD){ ?>
                            <div class="view" style='width:350px;float:left;border-bottom:2px'>
                                <table>
                                    <tr>
                                       <?php $this->render('kuponGiziExtraFooding',array(
                                                      'modKirimMenuDiet'=>$modKirimMenuDiet,
                                                      'modPendaftaran'=>$modPendaftaran,
                                                      'modPasien'=>$modPasien,
                                                      'modDetailPesan'=>$modDetailPesan,
                                                      'modMenuDiet'=>$modMenuDiet,
                                                      'modKirimMenuPasien'=>$modKirimMenuPasien,
                                        )); ?>
                                    </tr>
                                </table>
                            </div>
                    <?php } ?>
                    <?php if($detail->menudiet->jenisdiet_id == Params::DEFAULT_JENISDIET_MAKANAN_CATHERING){ ?>
                            <div class="view" style='width:530px;float:left;border-bottom:2px'>
                                <table>
                                    <tr>
                                       <?php $this->render('kuponGiziCathering',array(
                                                      'modKirimMenuDiet'=>$modKirimMenuDiet,
                                                      'modPendaftaran'=>$modPendaftaran,
                                                      'modPasien'=>$modPasien,
                                                      'modDetailPesan'=>$modDetailPesan,
                                                      'modMenuDiet'=>$modMenuDiet,
                                                      'modKirimMenuPasien'=>$modKirimMenuPasien,
                                        )); ?>
                                    </tr>
                                </table>
                            </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </td>
    </tr>
</table>