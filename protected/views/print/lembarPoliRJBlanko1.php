<style>
	/* @media print  {
	 
	    html, body {
	        width: 200mm;
	 
	        height: 50mm;
	        font-size: 8px;
	        background: #FFF;
	        overflow:visible;
	    }
	    body {
	        padding-top:15mm;
	    }
	} */

	@page {
        size: auto;
        margin: 23mm 0mm 20mm 11mm;    }
    body{
        margin: 0px;
    }
</style>

<!--<style>
	table td{
	font-size:10px;
	} 
</style>

<table class="badan" width="100%" height = "55%"; border="0" cellspacing="2" cellpadding="1">
	<td style="width:40px;height:20px;"></td>
		<td valign="top" width="5" style="font-family:calibri;" ></td>
		<td valign="top"></td>
		<td valign="top" style="font-weight: bold;font-family:calibri;"></td>
    </tr>
	<tr>
		<td valign="top" style="font-weight:bold;font-family:calibri;font-size:11px;" >No. RM</td>
        <td valign="top">:</td>
        <td valign="top" style= ";font-family:calibri;font-size:13px;font-weight:bold"><?php echo implode(".", str_split($modPasien->no_rekam_medik, 2));?></td> 
		
    </tr>
	<tr>
		<td valign="top" width="40" style="font-weight:bold;font-family:calibri;font-size:9px;" >Nama</td>
		<td valign="top">:</td>
		<td valign="top" style="font-family:calibri;font-size:9px;font-weight:bold"><?php echo $modPasien->nama_pasien; ?>, <?php echo $modPasien->namadepan; ?>
		<?php if ($modPasien->jeniskelamin=="LAKI-LAKI") {
				echo "(L)";
			} else {
				echo "(P)";
			}
			?>
		</td>
    </tr>
	<tr>
        <td valign="top" style="font-weight:bold;font-family:calibri;font-size:9px;" >Tgl. Lahir</td>
        <td valign="top">:</td>
        <td valign="top" style= ";font-family:calibri;font-size:10px;"><?php echo $modPasien->tanggal_lahir ?> (<?php echo $modPendaftaran->umur; ?>)</td> 
		
</table>-->

<table class="badan" width="100%" height = "55%"; border="0">
	<!--<td style="width:40px;height:20px;"></td>-->
	<tr>
		<td valign="top" width="0" style="font-family:calibri;" ></td>
		<td valign="top" width="4"></td>
		<td valign="top" style="font-weight: bold;font-family:calibri;"></td>
    </tr>
	<tr style="">
		<td valign="top" style="font-weight:bold;font-family:calibri;font-size:8px;">No. RM</td>
        <td valign="top" style="font-weight:bold;font-family:calibri;font-size:8px;">:</td>
        <td valign="top" style="font-family:calibri;font-size:8px;font-weight:bold;"><?php echo implode(".", str_split($modPasien->no_rekam_medik, 2));?></td> 
		
    </tr>

    <?php 

    function echo_custome( $x, $length ) {
       	if(strlen($x)<=$length) {
    		echo $x;
  		} else {
    		$y=substr($x,0,$length) . '...';
    		echo $y;
  		}
    }
    ?>
	<tr style="">
		<td valign="top" width="40" style="font-weight:bold;font-family:calibri;font-size:8px;" >Nama</td>
		<td valign="top" style="font-weight:bold;font-family:calibri;font-size:8px;">:</td>
		<td valign="top" style="font-family:calibri;font-size:8px;font-weight:bold;"><?php echo_custome( $modPasien->nama_pasien , 15); ?>, <?php echo $modPasien->namadepan; ?>
		<?php if ($modPasien->jeniskelamin=="LAKI-LAKI") {
				echo "(L)";
			} else {
				echo "(P)";
			}
			?>
		</td>
    </tr>
	<tr style="">
        <td valign="top" style="font-weight:bold;font-family:calibri;font-size:8px;" >Tgl. Lahir</td>
        <td valign="top" style="font-weight:bold;font-family:calibri;font-size:8px;">:</td>
        <td valign="top" style= "font-family:calibri; font-size:8px; font-weight: bold;"><?php echo $modPasien->tanggal_lahir ?> (<?php echo $modPendaftaran->umur; ?>)</td> 
		
</table>



 
            
          