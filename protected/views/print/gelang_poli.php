<style>
	table td{
		font-size:11px;
		text-align:right;
		font-weight:bold;
	}
</style>
<div style="margin-top:10px;margin-right:50px;">
	<table width="100%" border="0">
		<tr>
			<td><?php echo implode(".", str_split($modPasien->no_rekam_medik, 2)); ?> / <?php echo $modPendaftaran->no_pendaftaran; ?></td>
		</tr>
		<tr>
			<td><?php echo $modPasien->nama_pasien;?></td>
		</tr>
		<tr>
			<td><?php echo $modPasien->tanggal_lahir; ?> / <?php echo $modPendaftaran->umur; ?></td>
		</tr>
	</table>
</div>