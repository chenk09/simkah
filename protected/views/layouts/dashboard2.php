<?php $this->beginContent('//layouts/main'); ?>
<?php 
$modulMenu = ((!empty($this->module->menu_side)) ? $this->module->menu_side : null); 
?>
<div class="sidebar">
    <?php
        $this->widget('MySideMenu',array(
            'id'=>'mysidemenu',
            'items'=>$this->module->menu_side,
            'htmlOptions'=>array('class'=>'span3'),
            'header'=>array('label'=>"Dashboard", 'url'=>  Yii::app()->createUrl("/".$this->module->id, array('moduleId'=>Yii::app()->session['modulId'])))
            ));
    ?>
</div>
<div class="span-25tmp" style="width:78%">
    <div id="content">
        <?php echo $content; ?>
    </div>
</div>

<?php $this->endContent(); ?>