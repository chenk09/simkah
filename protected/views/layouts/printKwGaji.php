<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
         <?php Yii::app()->clientScript->registerMetaTag('width=device-width, initial-scale=1.0', 'viewport'); ?>
        <style>
		<!-- 
		BODY,DIV,TABLE,THEAD,TBODY,TFOOT,TR,TH,TD,P { font-family:"Liberation Sans"; font-size:x-small }
		 -->
/*                 .container{
                     margin-left:-70px;
                     margin-top:-20px;
                 }*/
	</style>
        
        <script type="text/javascript">
            $(document).ready(function(){
               window.print(); 
            });
        </script>
</head>

<body style="background-color:#ffffff;">

<div class="container" id="kwitansiGaji" >

	<?php echo $content; ?>
    
</div>

</body>
    <script>
    $(document).load(function(){
               window.close(); 
            });
            </script>
</html>
