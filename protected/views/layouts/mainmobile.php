<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<link rel="shortcut icon" href="<?php echo Yii::app()->baseUrl;?>/images/icon/favicon.png"/>
    <style>
        .ui-jqgrid-hbox
        {
            background:#DCEDFA !important;
        }
        .ui-jqgrid-hbox .ui-jqgrid-htable thead .ui-jqgrid-labels th
        {
            background:#DCEDFA !important;
            font-weight:bold;
            border-left:1px solid #DDDDDD !important;
            color:#333333 !important;
            vertical-align:bottom !important;
            height:30px;
        } 
        .ui-jqgrid {border:1px solid #DDDDDD !important;}
        .ui-jqgrid-hbox .ui-jqgrid-htable thead .ui-jqgrid-labels th:hover{border:none !important;}
        .ui-jqgrid-btable {border:none !important;}
        .ui-jqgrid-btable tbody {border:none !important;}
        .ui-jqgrid-btable tbody .jqgrow {height:25px !important;}
        .ui-jqgrid-btable tbody .jqgrow td {border:none !important;border-left:1px solid #DDDDDD !important;vertical-align:bottom !important}
    </style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/mws/mws.style.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/mws/icons/icons.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/treeview/jquery.treeview.css" media="screen" />
        
        
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/jquery-1.3.2.js"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/mws.js"></script>
	<link rel="shortcut icon" href="fav.ico" type="image/x-icon" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <script>
            function set_read_notifikasi(params)
            {
                var id_pesan_kirim = $(params).attr('value');
                //var id_pesan_kirim = params.value;
                $.ajax({
                    url: "index.php?r=site/setReadNotifikasi",
                    cache: false,
                    dataType: "json",
                    data: {id_pesan:id_pesan_kirim},
                    success: function(data){
                        if(data.pesan === "ok")
                        {
                            find_notifikasi();
                        }
                    }
                });
            }
            
            function updateClock ( )
            {
                var currentTime = new Date ( );
                var currentHours = currentTime.getHours ( );
                var currentMinutes = currentTime.getMinutes ( );
                var currentSeconds = currentTime.getSeconds ( );
                
                // Pad the minutes and seconds with leading zeros, if required
                currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
                currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
                
                // Choose either "AM" or "PM" as appropriate
                var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
                
                // Convert the hours component to 12-hour format if needed
                currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
                
                // Convert an hours component of "0" to "12"
                currentHours = ( currentHours == 0 ) ? 12 : currentHours;
                
                // Compose the string for display
                var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
                
                $("#clock").html(currentTimeString);
                
            }
            
            $(document).ready(function()
            {
                setInterval('updateClock()', 1000);
            });
        </script>
</head>

<body style="background-color:#FFF;">
    
    <?php
        include 'header.html';
    ?>
    
		<!--<div class="kepala">
			<div class="kepala_logo_rs"></div>
			<div class="kepala_nama"></div>
			<div class="kepala_nama2"></div>
		</div>-->
<?php       
            $modInstalasi = InstalasiM::model()->findByPk(Yii::app()->user->getState('instalasi_id'));
            $namaInstalasi = (!empty($modInstalasi->instalasi_nama)) ? $modInstalasi->instalasi_nama : '' ;
            $modRuangan = RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'));
            $namaRuangan = (!empty($modRuangan->ruangan_nama)) ? $modRuangan->ruangan_nama : '';
            $Modul = ModulK::model()->findByPk(Yii::app()->session['modulId']);
            $login = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
            $idModul = ((!empty($this->module->id)) ? $this->module->id : null);
            $idUser = ((!empty(Yii::app()->user->id)) ? Yii::app()->user->id : null);
            $modulMenu = ((!empty($this->module->menu)) ? $this->module->menu : null);
            $tglLogin = (!empty($login->lastlogin)) ? strtotime($login->lastlogin) : null;
            $tanggalLogin = date('d M Y', $tglLogin);   
            $jamLogin = date('H:i:s', $tglLogin);
            

            $a = explode("/", $_GET['r']);
            $link_home = Yii::app()->createUrl($a[0]);
            $this->widget('bootstrap.widgets.BootNavbar', array(
            'fixed'=>false,
            //'brand'=>Yii::app()->name,
            'brand'=>'',
            'brandUrl'=>Yii::app()->createUrl(''),
            'collapse'=>false, // requires bootstrap-responsive.css
            'fluid'=>false,
            'items'=>
                array(
                    
                    //'<a href="" class="navbar-link"><img class="navbar-image marginplus" src="http://localhost/simrs/images/icon_modul/1333096830_image.png"></a>',
                    //empty($namaInstalasi)? "":  '<span class="btn btn-info">'.$namaInstalasi.'</span>',
//                        '<form class="navbar-search pull-left" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
                    "<a href='".$link_home."' class='navbar-link' rel='tooltip' data-original-title='".$Modul->modul_namalainnya."'><img class='navbar-image marginplus' src='images/home.png'/></a>",
                    '<span id="clock" class="pull-right navbar-text-baru"></span>',
            ),
        )); ?>
<div class="navbar navbar-static">
    <div class="navbar" style="margin:-2px 0;">
        <div class="container" style="background-image:-moz-linear-gradient(center top , #E8E8E8, #D4D4D4);">
            <div class="span-25" style="padding:0;margin-left:0;">     
                <?php
                $this->widget('application.extensions.menu.SMenu',
                    array(
                    "menu"=> MyMenuModul::getMenuModul($modulMenu),
                    "stylesheet"=>"menu_default.css",
                    "menuID"=>"myMenu",
                    "delay"=>3
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
        
<div class="container" id="page">

<!--	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div> header -->

	<div id="mainmenu">
		<?php 
//                $this->widget('zii.widgets.CMenu',array(
//			'items'=>array(
//				array('label'=>'Home', 'url'=>array('/site/index')),
//				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
//				array('label'=>'Contact', 'url'=>array('/site/contact')),
//				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
//				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
//			),
//		)); ?>
	</div><!-- mainmenu -->
        
<!--        <div class="span-25">
	<?php if(isset($this->breadcrumbs)){
            $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
                        'links'=>$this->breadcrumbs,
                    )); // breadcrumbs 
        }
        ?>
        </div>-->
        <div class="span-25">
        <?php
        if(isset($this->menu)){
            $this->widget('bootstrap.widgets.BootMenu', array(
                'type'=>'pills', // '', 'tabs', 'pills' (or 'list')
                'stacked'=>false, // whether this is a stacked menu
                'items'=>$this->menu,
            ));
        } ?>
        </div>
        
        <?php //echo '<pre>'.print_r($menu,1).'</pre>'; ?>
        <?php //echo '<pre>'.print_r($this->arrMenuModul,1).'</pre>'; ?>
	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Innova-eHospital.
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
<?php

$get_interval = KonfigsystemK::model()->find()->refreshnotifikasi;

//convert jam > milisecond
//$interval = $get_interval * 3600000;
$interval = 5000;

$js = <<< JSCRIPT
function insert_notifikasi(params)
{
    $.post("index.php?r=site/insertNotifikasi",{NofitikasiR:params},
        function(data){
            if(data.status === 'ok')
            {
                $('#pesan_notifikasi').html(data.template);
                $('#count_notif').text(data.count_notif);
            }
            return false;
        },"json"
    );
}

function get_notifikasi()
{
    $.ajax({
        url: "index.php?r=site/getNotifikasi",
        cache: false,
        dataType: "json",
        success: function(data){
            $('#pesan_notifikasi').html(data.template);
            if(data.count_notif == 0)
            {
                $('#count_notif').text("");
                $('#count_notif').removeClass("mws-dropdown-notif");
            }else{
                $('#count_notif').text(data.count_notif);
                $('#count_notif').addClass("mws-dropdown-notif");
            }
            setTimeout('get_notifikasi();', ${interval});
        }
    });
}
get_notifikasi();

function find_notifikasi()
{
    $.ajax({
        url: "index.php?r=site/getNotifikasi",
        cache: false,
        dataType: "json",
        success: function(data){
            $('#pesan_notifikasi').html(data.template);
            if(data.count_notif == 0)
            {
                $('#count_notif').text("");
                $('#count_notif').removeClass("mws-dropdown-notif");
                //$('#combo_content').removeClass("mws-dropdown-box");
            }else{
                $('#count_notif').text(data.count_notif);
                $('#count_notif').addClass("mws-dropdown-notif");
                //$('#combo_content').addClass("mws-dropdown-box");
            }
        }
    });
}

function insert_monitoring(params)
{
    $.post("index.php?r=site/insertHistoryUser",{params:params},
        function(data){
            alert(data.pesan);
            return false;
        },"json"
    );
}

function get_session_user()
{
    $.ajax({
        url: "index.php?r=site/getSessionUser",
        cache: false,
        dataType: "json",
        success: function(data){
            if(data.pesan == 'not'){
                $('#session_end').dialog('open');
                setTimeout(
                    function(){
                        $('#session_end').dialog('close');
                        window.location = 'index.php?r=site/index';
                    }, 10000
                );
            }else{
                setTimeout('get_session_user();', 10000);
            }
        }
    });
}
//get_session_user();

function getDetailNotifikasi(params)
{
    var notifikasi_id = $(params).attr('value');
    $.ajax({
        url: "index.php?r=site/getDetailNotifikasi",
        cache: false,
        data: {notifikasi_id:notifikasi_id},
        success: function(data){
            $("#content_pesan").html(data);
        }
    });
}

    function dialog_kertas()
        {
        $('#ubah_kertas').dialog('open');
        
        }
    function simpan_kertas()
        {
            ukuranKertas = $('#ukuranKertas').val();
            posisiKertas = $('#posisiKertas').val();
            posisiNama = $('#posisiKertas :selected').html();
            $.post("index.php?r=site/setKertas",{ukuranKertas:ukuranKertas,posisiKertas:posisiKertas,posisiNama:posisiNama },function(data){
                alert(data.pesan);
                 $('#ubah_kertas').dialog('close');
                
        return false;
    },"json");
        }
JSCRIPT;

Yii::app()->clientScript->registerScript('jsPendaftaran',$js, CClientScript::POS_HEAD);

$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'ubah_kertas',
    'options'=>array(
        'title'=>'Ubah Ukuran dan Posisi Kertas',
        'autoOpen'=>false,
        'width'=>450,
        'height'=>300,
        'modal'=>'true',
        'hide'=>'explode',
        'resizelable'=>false,
    ),
));

//$form=$this->beginWidget('CActiveForm', array(
//	'id'=>'setting-print-form',
//        'enableAjaxValidation'=>false,
//        'action'=>'/simrs/index.php?r=site/setKertas',
//        'htmlOptions'=>array('class'=>'form-horizontal'),
//));
?>
<div class="control-group ">
    <?php echo CHtml::label('Ukuran Kertas', 'print_ukuranKertas', array('class'=>'control-label')); ?>
    <div class="controls">
        <?php echo CHtml::dropDownList('ukuranKertas', Yii::app()->user->getState('ukuran_kertas'), Params::ukuranKertas(), array('class'=>'span3')); ?>
    </div>
</div>

<div class="control-group ">
    <?php echo CHtml::label('Posisi Kertas', 'print_posisiKertas', array('class'=>'control-label')); ?>
    <div class="controls">
        <?php echo CHtml::dropDownList('posisiKertas', Yii::app()->user->getState('posisi_kertas'), Params::posisiKertas(), array('class'=>'span3')); ?>
    </div>
</div>
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'button', 'name'=>'btn_simpan','onclick'=>'simpan_kertas()')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                        array('class'=>'btn btn-danger', 'type'=>'button', 'name'=>'btn_batal','onclick'=>'$(\'#ubah_kertas\').dialog(\'close\')')); ?>                
         
</div>
<?php //$this->endWidget(); ?>    
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'session_end',
    'options'=>array(
        'title'=>'Waktu Login Habis',
        'autoOpen'=>false,
        'width'=>250,
        'height'=>250,
        'modal'=>'true',
        'hide'=>'explode',
        'resizelable'=>false,
    ),
));
?>
<div align="center">
    <div>Maaf waktu logi anda telah habis, silahkan login kembali !</div>
    <div>&nbsp;</div>
    <a class="search-button btn" href="<?php echo Yii::app()->createUrl('/site/index');?>" onClick="window.location=this.href;">
        Kembali Login
    </a>
</div>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'pop_pesan',
    'options'=>array(
        'title'=>'Notifikasi',
        'autoOpen'=>false,
        'width'=>400,
        'height'=>150,
        'modal'=>'true',
        'resizelable'=>false,
    ),
));
?>
<div id="content_pesan"></div>
<?php $this->endWidget();?>