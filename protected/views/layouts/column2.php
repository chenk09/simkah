<?php $this->beginContent('//layouts/main'); ?>
<div class="row-fluid">
	<div class="span12">
		<div style="margin:5px">
			<div class="row-fluid">
				<div class="span9"><?php echo $content; ?></div>
				<div class="span3">
					<div class="well">
						<?php $this->widget('BoxPengumuman');?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->endContent(); ?>