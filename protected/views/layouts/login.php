<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
	<link rel="shortcut icon" href="fav.ico" type="image/x-icon" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <script>
		
function slideSwitch() {
    var $active = $('#slideshow IMG.active');

    if ( $active.length == 0 ) $active = $('#slideshow IMG:last');

    // use this to pull the images in the order they appear in the markup
    var $next =  $active.next().length ? $active.next()
        : $('#slideshow IMG:first');

    // uncomment the 3 lines below to pull the images in random order
    
    // var $sibs  = $active.siblings();
    // var rndNum = Math.floor(Math.random() * $sibs.length );
    // var $next  = $( $sibs[ rndNum ] );


    $active.addClass('last-active');

    $next.css({opacity: 0.0})
        .addClass('active')
        .animate({opacity: 1.0}, 1000, function() {
            $active.removeClass('active last-active');
        });
}

$(function() {
    setInterval( "slideSwitch()", 5000 );
});
		
		
            function updateClock ( )
            {
                var currentTime = new Date ( );
                var currentHours = currentTime.getHours ( );
                var currentMinutes = currentTime.getMinutes ( );
                var currentSeconds = currentTime.getSeconds ( );
                
                // Pad the minutes and seconds with leading zeros, if required
                currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
                currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;
                
                // Choose either "AM" or "PM" as appropriate
                var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";
                
                // Convert the hours component to 12-hour format if needed
                currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;
                
                // Convert an hours component of "0" to "12"
                currentHours = ( currentHours == 0 ) ? 12 : currentHours;
                
                // Compose the string for display
                var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
                
                $("#clock").html(currentTimeString);
                
            }
            
            $(document).ready(function()
            {
                setInterval('updateClock()', 1000);
            });
        </script>	
</head>

<body>
<div class="container">


</div> 
<div style="float:left">
<div class="container" id="page_login">

<!--	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div> header -->


		<?php 
               // $this->widget('zii.widgets.CMenu',array(
		//	'items'=>array(
			//	array('label'=>'Home', 'url'=>array('/site/index')),
			//	array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				//array('label'=>'Contact', 'url'=>array('/site/contact')),
				//array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				//array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
		//	),
	//	));
//$this->layout = '//layouts/column2';
 ?>

        
	<?php //if(isset($this->breadcrumbs)){
          //  $this->widget('bootstrap.widgets.BootBreadcrumbs', array(
            //         'links'=>$this->breadcrumbs,
            //        )); // breadcrumbs 
       // }
        ?>
    
        <?php
       // if(isset($this->menu)){
       //     $this->widget('bootstrap.widgets.BootMenu', array(
       //         'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
       //         'stacked'=>false, // whether this is a stacked menu
       //         'items'=>$this->menu,
       //     ));
      //  } ?>
   
        <?php //echo '<pre>'.print_r($menu,1).'</pre>'; ?>
        <?php //echo '<pre>'.print_r($this->arrMenuModul,1).'</pre>'; ?>
	<div class="span-10">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
    <div class="">
        <div class="dashboard">
		<div class="flash">
		
        <?php $this->widget('bootstrap.widgets.BootCarousel', array(
				'items'=>array(
					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image1.jpg', 'label'=>''),
					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image2.jpg', 'label'=>''),
					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image3.jpg', 'label'=>''),
					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image4.jpg', 'label'=>''),
					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image5.jpg', 'label'=>''),
//					array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image6.jpg', 'label'=>''),
					//array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image7.jpg', 'label'=>''),
					//array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image8.jpg', 'label'=>''),
					//array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image9.jpg', 'label'=>''),
					//array('image'=>Yii::app()->getBaseUrl('webroot').'/css/image10.jpg', 'label'=>''),
				),
			)); ?> 
        </div>
        </div>
    </div>
	<div class="clear">
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by Innova-eHospital.
		<?php //echo Yii::powered(); ?>
	</div><!-- footer -->
</div></div>
</div><!-- page -->

</body>
</html>

<iframe width=280px height=600px style="float:right" src="<?php echo $this->createUrl('viewTestimoni'); ?>">

</iframe>     
