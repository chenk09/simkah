<?php
//$this->breadcrumbs=array(
//	'Pengumumen'=>array('index'),
//	$model->pengumuman_id,
//);

$arrMenu = array();
//array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pengumuman #'.$model->pengumuman_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pengumuman', 'icon'=>'list', 'url'=>array('index'))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pengumuman', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Pengumuman', 'icon'=>'pencil','url'=>array('update','id'=>$model->pengumuman_id))) :  '' ;
//array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Pengumuman','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->pengumuman_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pengumuman', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;
//
$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'template'=>"{items}\n{pager}",
        'viewData'=>array('inFrame'=>$inFrame),
)); ?>
<?php 
//$this->widget('ext.bootstrap.widgets.BootDetailView',array(
//	'data'=>$model,
//	'attributes'=>array(
//		'pengumuman_id',
//		'judul',
//		'isi',
//		'status_publish',
//		'create_loginpemakai_id',
//		'create_time',
//		'update_loginpemakai_id',
//		'update_time',
//		'publish_loginpemakai_id',
//	),
//)); ?>

<?php //$this->widget('TipsMasterData',array('type'=>'view'));?>