
<?php
//$this->breadcrumbs=array(
//	'Pengumumen'=>array('index'),
//	$model->pengumuman_id=>array('view','id'=>$model->pengumuman_id),
//	'Update',
//);
//
$arrMenu = array();
//array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Pengumuman #'.$model->pengumuman_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pengumuman', 'icon'=>'list', 'url'=>array('index'))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pengumuman', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pengumuman', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->pengumuman_id))) ;
//(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pengumuman', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;
//
$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>
<?php $this->widget('TipsMasterData',array('type'=>'update'));?>