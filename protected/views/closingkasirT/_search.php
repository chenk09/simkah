<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'closingkasir-t-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'closingkasir_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tandabuktibayar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'shift_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglclosingkasir',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'closingdari',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sampaidengan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilaiclosingtrans',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'closingsaldoawal',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmluanglogam',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmluangkertas',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tottransaksi',array('class'=>'span5')); ?>

	<?php echo $form->textAreaRow($model,'keterangan_closing',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update_time',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span5')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
