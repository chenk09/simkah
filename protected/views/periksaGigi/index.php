<?php

$gigi[18] = 'rwwnbH';
$gigi[34] = 'wwwbwV';
$gigi[81] = 'wwwbwS';
$gigi[82] = 'wwwwwE';
$gigi[84] = 'wgrwwA';

$this->widget('Gigi',array('gigis'=>$gigi));

?>

<center class="form-actions">
    <table style="width:615px;">
        <tr>
            <td><?php echo CHtml::button('Belum Erupsi', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("E");')); ?></td>
            <td><?php echo CHtml::button('Tambalan Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("r");')); ?></td>
            <td><?php echo CHtml::button('Sisa Akar', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("A");')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::button('Erupsi Sebagian', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("S");')); ?></td>
            <td><?php echo CHtml::button('Tambalan Non Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("b");')); ?></td>
            <td><?php echo CHtml::button('Gigi Hilang', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("H");')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::button('Anomali Bentuk', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("B");')); ?></td>
            <td><?php echo CHtml::button('Mahkota Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("g");')); ?></td>
            <td><?php echo CHtml::button('Jembatan', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("J");')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::button('Karies', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("K");')); ?></td>
            <td><?php echo CHtml::button('Mahkota Non Logam', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);changeCode("n");')); ?></td>
            <td><?php echo CHtml::button('Gigi Tiruan Lepas', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("L");')); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::button('Non Vital', array('class'=>'btn btn-primary span3','onclick'=>'onKlikTombol(this);addCode("V");')); ?></td>
            <td></td>
            <td></td>
        </tr>
    </table>
</center>
