<style>
	.form-control{
		margin:5px;
	}
	.text_right{
		margin-top:10px;
		font-size:25px;
		font-weight:bold;
		color:#FFFFFF;
		position:absolute
	}
</style>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/login.js"></script>
<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
$bulan = date('m');
$tahun = date('Y');
$tgl = date('d');
$jumlahHari = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
$date = Yii::app()->dateFormatter->formatDateTime(strtotime($tahun.'-'.$bulan.'-'.$tgl),'full',null);
$this->widget('bootstrap.widgets.BootAlert'); ?>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

<div class="text_right" align="right">
	<div id="clock" style="font-size:50px;font-weight:bold;"></div>
	<?php echo $date ?>
</div>

<div class="row-fluid" id="content_login">
	<div class="span4">&nbsp;</div>
	<div class="span4" align="center" style="margin-top:10%;margin-left:35%;position:absolute" >
			<?php $form = $this->beginWidget('CActiveForm', array(
				'id'=>'login-form',
				'enableClientValidation'=>true,
				'focus'=>'#LoginForm_username',
				'htmlOptions'=>array(
					'class'=>'form-vertical'
				),
				'clientOptions'=>array(
					'validateOnSubmit'=>true,
				),
			)); ?>
			<div class="well" style="box-shadow:3px 3px 3px;width:225px">
				<div style="margin:10px"><img src="<?=Yii::app()->request->baseUrl?>/images/logo_jk_basic.png" width="120"></div>
				<hr>
				<div class="form-control">
					<div class="controls">
						<?php echo $form->textField($model,'username',array(
							'placeholder'=>$model->getAttributeLabel('username'),
							'onBlur'=>'cekUsername(this)',
							'style'=>'height:20px'
						)); ?>
						<?php echo $form->error($model,'username'); ?>
						<?php echo CHtml::hiddenField('user_id') ?>
					</div>
				</div>
				<div class="form-control">
					<div class="controls">
						<?php echo $form->passwordField($model,'password',array(
							'placeholder'=>$model->getAttributeLabel('password'),
							'style'=>'height:20px'
						)); ?>
						<?php echo $form->error($model,'password'); ?>
					</div>
				</div>
				
				<div class="form-control">
					<div class="controls">
						<?php echo $form->dropDownList($model, 'instalasi', array(),array(
							'empty'=>'-- Pilih Instalasi --',
							'ajax'=>array(
								'type'=>'POST',
								'url'=>  CController::createUrl('site/dynamicRuangan'),
								'update'=>'#LoginForm_ruangan'
							))
						);?>
						<?php echo $form->error($model,'instalasi'); ?>
					</div>
				</div>
				
				<div class="form-control">
					<div class="controls">
						<?php echo $form->dropDownList($model, 'ruangan', array(),array('empty'=>'-- Pilih Ruangan --'));?>
						<?php echo $form->error($model,'ruangan'); ?>
					</div>
				</div>
				<div>&nbsp;</div>
				<div class="form-control hide">
					<div class="controls">
						<?php echo $form->dropDownList($model, 'modul', array(),array('empty'=>'-- Pilih Modul --'));?>
						<?php echo $form->error($model,'modul'); ?>
					</div>
				</div>
				<?php echo CHtml::htmlButton(Yii::t('Login','{icon} Login',array('{icon}'=>' <i class="icon-user icon-white"></i>')),array('class'=>'btn btn-primary btn-block', 'type'=>'submit', 'style'=>'width:220px')); ?>
			
			</div>
		<?php $this->endWidget(); ?>	
	</div>
	<div class="span4">&nbsp;</div>
</div>
<?php
$url = CController::createUrl('site/AjaxCekUsername');
$js = <<< JSCRIPT

   function cekUsername(obj){
        $.post("${url}", { username: obj.value},
        function(data){
            $('#user_id').val(data.id);
            $('#LoginForm_instalasi').html(data.instalasi)
            $('#LoginForm_ruangan').html(data.ruangan)
            $('#LoginForm_modul').html(data.modul)
        }, "json");
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('hapusPenjualan', $js, CClientScript::POS_HEAD);
?>
