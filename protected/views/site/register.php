<?php
$form=$this->beginWidget('CActiveForm',array(
        'id'=>'registrasi-form',
	'action'=>Yii::app()->controller->createUrl('registrasi'),
        'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),

)); ?>

<table width="300" border="0">
  <tr>
    <td width="73"><?php echo $form->labelEx($modRegistrasi,'namalengkapreg'); ?></td>
    <td width="117"><?php echo $form->textField($modRegistrasi,'namalengkapreg', array('class'=>'span2')); ?>
                    <?php echo $form->error($modRegistrasi,'namalengkapreg',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'alamatlngkpreg'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'alamatlngkpreg', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'alamatlngkpreg',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'propinsi'); ?></td>
    <td><?php $propinsi = PropinsiM::model()->findAll('propinsi_aktif = true order by propinsi_nama'); ?>
        <?php echo $form->dropDownList($modRegistrasi,'propinsi', CHtml::listData($propinsi, 'propinsi_nama', 'propinsi_nama'),array('class'=>'span2',
                                                                                'empty'=>'-- Pilih --',
                                                                                 'ajax'=>array(
                                                                                    'type'=>'POST',
                                                                                    'url'=> CController::createUrl('site/dynamicPropinsi'),
                                                                                    'update'=> '#RegistrasidemoS_kotakabupaten',))); ?>
        <?php echo $form->error($modRegistrasi,'propinsi',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'kotakabupaten'); ?></td>
    <td><?php echo $form->dropDownList($modRegistrasi,'kotakabupaten', array(),array('empty'=>'-- Pilih --','class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'kotakabupaten',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'notelp'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'notelp', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'notelp',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'nomobile'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'nomobile', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'nomobile',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'email'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'email', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'email',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'website'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'website', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'website',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'namainstansi'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'namainstansi', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'namainstansi',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'alamatinstansi'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'alamatinstansi', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'alamatinstansi',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>
  
  <tr>
    <td><?php echo $form->labelEx($modRegistrasi,'telpinstansi'); ?></td>
    <td><?php echo $form->textField($modRegistrasi,'telpinstansi', array('class'=>'span2')); ?>
        <?php echo $form->error($modRegistrasi,'telpinstansi',array('style'=>'font-size:11px;')); ?>
    </td>
  </tr>

<?php if(CCaptcha::checkRequirements()): ?>
  <tr>
    <td>
        <?php $this->widget('CCaptcha', array('imageOptions'=>array('class'=>'span2','id'=>'verifyReg'))); ?>
    </td>
    <td>
        <?php echo $form->textField($modRegistrasi,'verifyCode', array('class'=>'span2')); ?>
        <p style="font-size: 11px;">Masukkan huruf seperti yang terlihat pada gambar di samping.</p>
    </td>
  </tr>
<?php endif; ?> 
  <tr>
      <td colspan="2">
          <div class="form-actions">
            <?php echo CHtml::htmlButton('Simpan',array('class'=>'btn btn-primary pull-right', 'type'=>'submit')); ?>
          </div>
      </td>
  </tr>
</table>

<?php $this->endWidget(); ?>
 