<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/login.js"></script>


<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);

?>

<div class="latar">
<div class="logors"></div>
<ul id="navigation">
    <li class="register">
        <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>'Registrasi',
                'type'=>'inverse',
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
            //        'data-target'=>'#myModal1',
                    'onclick'=>'setContent("registrasi-form")',
                ),
            )); 
            ?>
    </li>
    <li class="testimonial">
        <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>'Testimonial',
                'type'=>'inverse',
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
            //        'data-target'=>'#myModal2',
                    'onclick'=>'setContent("testimonial-form")',
                ),
            )); 
            ?>
    </li>
    <li class="search">
        <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>' Proposal ',
                'type'=>'inverse',
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
                    //'data-target'=>'proposal-form',
                    'onclick'=>'setContent("proposal-form")',
                ),
            )); 
        ?>
    </li>
    <li class="kontak">
        <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>' Kontak',
                'type'=>'inverse',
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
                    //'data-target'=>'#kontak-form',
                    'onClick'=>'setContent("kontak-form")',
                ),
            )); 
            ?>
    </li>
    <li class="mod">
         <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>' Modul',
                'type'=>'inverse',
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
                  //  'data-target'=>'#myModal5',
                    'onClick'=>'setContent("modul-form")',
                ),
            )); 
            ?>
    </li>
    <li class="mod">
         <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>' Booking Kamar',
                'type'=>'inverse',
                'url'=>Yii::app()->createUrl('bookingkamarT/create'),
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
                  //  'data-target'=>'#myModal5',
    //                'onClick'=>'setContent("modul-form")',
                ),
            ));
            ?>
    </li>
    <li class="mod">
         <?php 
            $this->widget('bootstrap.widgets.BootButton', array(
                'label'=>' Buat Janji',
                'type'=>'inverse',
                'url'=>Yii::app()->createUrl('buatjanjipoliT/create'),
                'htmlOptions'=>array(
                    'data-toggle'=>'modal',
                  //  'data-target'=>'#myModal5',
    //                'onClick'=>'setContent("modul-form")',
                ),
            )); 
            ?>
    </li>
 <li class="podcasts"><a title="Podcasts"></a></li>
 <li class="contact"><a title="Contact"></a></li> 
</ul>
    
<div class="span4" align="center" >
    <div class='layer1'>
        <p></p><br/>
        <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
        <h1>Login</h1>

        <p>Silakan isi formulir berikut dengan akun Anda:</p>

        <div class="form">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'login-form',
                'enableClientValidation'=>true,
                'focus'=>'#LoginForm_username',
                'clientOptions'=>array(
                        'validateOnSubmit'=>true,
                ),
        )); ?>
        <p class="note">Kolom ini<span class="required">*</span> harus diisi.	  </p>

                <div class="row">
                        <?php echo $form->labelEx($model,'Nama Pemakai'); ?>
                        <?php echo $form->textField($model,'username',array('onBlur'=>'cekUsername(this)')); ?>
                                        <?php echo CHtml::hiddenField('user_id') ?>
                        <?php echo $form->error($model,'username'); ?>
                </div>

                <div class="row">
                        <?php echo $form->labelEx($model,'Kata Kunci'); ?><img id="capsLockNotice" class="ssdlogo" alt="Caps Lock Is ON" title="Caps Lock Is ON" src="<?php echo Yii::app()->request->baseUrl; ?>/images/icon_modul/capslock-notice.png">
                        <?php echo $form->passwordField($model,'password',array('class'=>'input capLocksCheck')); ?> 
                        <?php echo $form->error($model,'password'); ?>

                </div>
                <div class="row">
                        <?php echo $form->labelEx($model, 'instalasi'); ?>
                        <?php echo $form->dropDownList($model, 'instalasi', array(),array('empty'=>'-- Pilih --',
                                                                                    'ajax'=>array(
                                                                                        'type'=>'POST',
                                                                                        'url'=>  CController::createUrl('site/dynamicRuangan'),
                                                                                        'update'=>'#LoginForm_ruangan',)));  ?>
                        <?php echo $form->error($model,'instalasi'); ?>
                </div>
                <div class="row">
                        <?php echo $form->labelEx($model, 'ruangan'); ?>
                        <?php echo $form->dropDownList($model, 'ruangan', array(),array('empty'=>'-- Pilih --',));  ?>
                                        <?php echo $form->error($model,'ruangan'); ?>
                </div>
                <div class="row">
                        <?php echo $form->labelEx($model, 'modul'); ?>
                        <?php echo $form->dropDownList($model, 'modul', array(),array('empty'=>'-- Pilih --',));  ?>

                </div>
                <div class="row buttons">
                        <?php echo CHtml::htmlButton(Yii::t('Login','{icon} Login',array('{icon}'=>' <i class="icon-user icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
                </div>

        <?php $this->endWidget(); ?>
        </div><!-- form -->
    </div>
    
    <div class='layer2' style='display:none; padding-top:16px;'>
        <!-- Registrasi -->
        <div id="registrasi-form" class="guestform hide">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" onclick="closeContent();">&times;</a>
                <strong>Form Registrasi</strong>
            </div>

            <div class="modal-body" style="background-color: #ffffff;">
               <?php echo $this->renderPartial('register',array('modRegistrasi'=>$modRegistrasi)); ?>                   
            </div>
        </div>

        <!-- Testimonial -->
        <div id="testimonial-form" class="guestform hide">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" onclick="closeContent();">&times;</a>
                <strong>Form Testimonial</strong>
            </div>

            <div class="modal-body">
                <?php echo $this->renderPartial('testimonial',array('modKomen'=>$modKomen)); ?>      
            </div>
        </div>

        <!-- proposal -->
        <div id="proposal-form" class="guestform hide">
            <div class="proposal-header">
                <a class="close" data-dismiss="modal" onclick="closeContent();">&times;</a>
                <strong>Proposal</strong>
            </div>

            <div class="proposal-body">
                <?php echo $this->renderPartial('proposal'); ?>   
            </div>
        </div>

        <!--kontak -->
        <div id="kontak-form" class="guestform hide">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" onclick="closeContent();">&times;</a>
                <strong>Kontak</strong>
            </div>

            <div class="modal-body">
                <?php echo $this->renderPartial('kontak'); ?>
            </div>
        </div>

        <!-- Modules -->
        <div id="modul-form" class="guestform hide">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" onclick="closeContent();">&times;</a>
                <strong>Daftar Moduls</strong>
            </div>

            <div class="modal-body">
               <?php echo $this->renderPartial('modul'); ?>   
            </div>
        </div>
    </div>
</div>
    
<div class="span4" align="center">
    <div class='layer4' style='display:none; padding-left:26px; padding-top:23px; width:362px;' >
    </div>
</div>
 
</div>



<?php
$url = CController::createUrl('site/AjaxCekUsername');
$js = <<< JSCRIPT

   function cekUsernames(obj){
        $.post("${url}", { username: obj.value},
        function(data){
            $('#user_id').val(data.id);
            $('#LoginForm_instalasi').html(data.instalasi)
            console.log(data.ruangan);
            $('#LoginForm_ruangan').html(data.ruangan)
            $('#LoginForm_modul').html(data.modul)
        }, "json");
   }
   
   function setContent(obj){
                $('.guestform').hide();
   		$("#"+obj).show();
		$('.layer1').hide();
		$('.layer2').show();
   }
   
   function closeContent(){
		$('.layer2').hide();
		$('.layer1').show();
   }
   
   function setContent2(content){
		$('.dashboard').hide();
		$('.layer4').html(content).show();
   }
   
   function closeContent2(){
		$('.layer4').hide();
		$('.dashboard').show();
   }
   
    $(function() {
    $('#navigation > li').hover(
            function () {
            $('a',$(this)).stop().animate({'marginLeft':'-2px'},200);
             },
            function () {
            $('a',$(this)).stop().animate({'marginLeft':'-85px'},200);
             }
             );
    });

JSCRIPT;

Yii::app()->clientScript->registerScript('registrationFrom', $js, CClientScript::POS_HEAD);
?>
