
<?php

class PengumumanController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','dialogView','UploadedImages'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','ImageUpload'),
				'roles'=>array('Admin'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'roles'=>array('Admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $criteria = new CDbCriteria;
                $criteria->compare('pengumuman_id', $id);
                $criteria->with = array('userCreate');
                $dataProvider=new CActiveDataProvider('Pengumuman', array(
                        'pagination'=>array(
                                'pageSize'=>10,
                        ),
                        'criteria'=>$criteria,
                ));
                
		$this->render('view',array(
			'dataProvider'=>$dataProvider,
                        'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new Pengumuman;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengumuman']))
		{
			$model->attributes=$_POST['Pengumuman'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pengumuman_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id,$inFrame=false)
	{
                if($inFrame) $this->layout = 'frameDialog';
                
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Pengumuman']))
		{
			$model->attributes=$_POST['Pengumuman'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				($inFrame) ? $this->redirect(array('dialogView','id'=>$model->pengumuman_id)) : $this->redirect(array('view','id'=>$model->pengumuman_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Pengumuman',array('pagination'=>array('PageSize'=>2)));
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new Pengumuman('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Pengumuman']))
			$model->attributes=$_GET['Pengumuman'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Pengumuman::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pengumuman-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new Pengumuman;
            $model->attributes=$_REQUEST['Pengumuman'];
            $judulLaporan='Data Pengumuman';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
	public function actionDialogView($id='')
	{
                $this->layout = 'frameDialog';
		$criteria = new CDbCriteria;
                if(!empty($id))
                    $criteria->compare('pengumuman_id', $id);
                $criteria->with = array('userCreate');
                $dataProvider=new CActiveDataProvider('Pengumuman', array(
                        'pagination'=>array(
                                'pageSize'=>2,
                        ),
                        'criteria'=>$criteria,
                ));
                
		$this->render('view',array(
			'dataProvider'=>$dataProvider,
                        'inFrame'=>true,
                        //'model'=>$this->loadModel($id),
		));
	}
        
        public function actionImageUpload()
        {
            $dir = Params::pathImagePengumumanUploaded();
            $dirThumb = Params::pathImagePengumumanUploadedThumb();
            $_FILES['file']['type'] = strtolower($_FILES['file']['type']);
            if ($_FILES['file']['type'] == 'image/png'
            || $_FILES['file']['type'] == 'image/jpg'
            || $_FILES['file']['type'] == 'image/gif'
            || $_FILES['file']['type'] == 'image/jpeg'
            || $_FILES['file']['type'] == 'image/pjpeg')
            { 
                $file = md5(date('YmdHis')).'.jpg';
                Yii::import("ext.EPhpThumb.EPhpThumb");
                $thumb=new EPhpThumb();
                $thumb->init();
                
                if(copy($_FILES['file']['tmp_name'], $dir.$file))
                {    
                      $thumb->create($_FILES['file']['tmp_name'])
                            ->resize(100,100)
                            ->save($dirThumb.$file);
                  echo CHtml::image(Params::urlImagePengumumanUploaded().$file);
                  Yii::app()->end();
                }
                throw new CHttpException(403,'The server is crying in pain as you try to upload bad stuff');
            }
        }
        
        public function actionUploadedImages()
        {
            $images = array();

            $handler = opendir(Params::pathImagePengumumanUploaded());

            while ($file = readdir($handler))
            {
                if ($file != "." && $file != "..")
                    $images[] = $file;
            }
            closedir($handler);

            $jsonArray=array();

            foreach($images as $image)
                $jsonArray[]=array(
                    'thumb'=>  Params::urlImagePengumumanUploadedThumb ().$image,
                    'image'=>  Params::urlImagePengumumanUploaded().$image,
                );

            header('Content-type: application/json');
            echo CJSON::encode($jsonArray);
        }
}
