<?php
Yii::import('pendaftaranPenjadwalan.models.PPLaporankunjunganrs');
class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
                
                
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
              
                if(Yii::app()->user->isGuest){
                  $this->redirect(array('login'));
                }
                $this->layout = '//layouts/column2';
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
                
		$this->layout = '//layouts/login2';
                $modRegistrasi = new RegistrasidemoS;
                $modKomen = new KomentarS;
                
		$model=new LoginForm;
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
                                                 
			// validate user input and redirect to the previous page if valid
                                  
			if($model->validate() && $model->login()){
                            Yii::app()->session['timeout'] = time();
                            Yii::app()->session['inactive'] = Params::DEFAULT_SESSION * 60;
                            
                            Yii::app()->user->setFlash('success', '<strong>Selamat Datang</strong> '.Yii::app()->user->name);
                            if(isset($_POST['LoginForm']['modul']) && $_POST['LoginForm']['modul'] != "")
                            {
                                $modModulK = ModulK::model()->findByPk($_POST['LoginForm']['modul'],'modul_aktif = TRUE');
                                $url = Yii::app()->createUrl($modModulK->modul_nama,array('modulId'=>$_POST['LoginForm']['modul']));
                            }else{
                                $url = Yii::app()->user->returnUrl;
                            }
                            $this->redirect($url);
                        }

                }
		// display the login form
		$this->render('login2',array('model'=>$model,
                                            'modRegistrasi'=>$modRegistrasi,
                                            'modKomen'=>$modKomen));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{               
                    LoginpemakaiK::model()->updateByPk(Yii::app()->user->id, array('lastlogin'=>date( 'Y-m-d H:i:s', time() ),'statuslogin'=>FALSE));
                    Yii::app()->user->logout();
                    $this->redirect(Yii::app()->homeUrl);
	}
        
        /**
         * ajax untuk mengambil loginpemakai_id berdasarkan username
         * digunakan di site/login handling after blur input username
         */
        public function actionAjaxCekUsername()
        {   
            if (Yii::app()->request->isAjaxRequest){
                $data = array();
                $username = (isset($_POST['username'])) ? $_POST['username'] : null;
                $is_exist = LoginpemakaiK::model()->findByAttributes(array('nama_pemakai'=>$username));
                if($is_exist)
                {
                    $data['id'] = $is_exist->loginpemakai_id;
                    $ruangan = RuanganpemakaiK::model()->findAllByAttributes(array('loginpemakai_id'=>$data['id']));
                }else{
                    $data['ruangan'] = CHtml::dropdownlist('LoginForm_ruangan','ruangan_id',array('empty'=>'-- Pilih Ruangan --'));
                    $data['modul'] = CHtml::dropdownlist('LoginForm_modul','modul_id',array('empty'=>'-- Pilih Modul --'));
                    $data['instalasi'] = CHtml::dropdownlist('LoginForm_instalasi','instalasi_id',array('empty'=>'-- Pilih Instalasi--'));
                    echo json_encode($data);
                    exit;
                }
                
                $arrInstalasi = array();
                $valInstalasi = array();
                $x = 0;
                
                if (count($ruangan) > 0){
                    foreach ($ruangan as $i=>$ruanganNya) {
                        $ruang = RuanganM::model()->findByPk($ruanganNya->ruangan_id,'ruangan_aktif =TRUE');
                        if((boolean)count($ruang)){
                            if(isset ($ruang->instalasi_id))
                            {
                                /**
                                 * untuk    : ehospitaljk/index.php?r=site/login pada saat pemilihan instalasi 
                                 * @author  : Miranitha Fasha
                                 * date     : 06-05-2014 /d-m-y
                                 * issue    : EHJ-1785
                                 * action   : if($ruang->instalasi->instalasi_aktif == TRUE){}
                                 * desc     : Pada saat instalasi di non aktifkan instalasi masih tetap muncul
                                 */
                                if($ruang->instalasi->instalasi_aktif == TRUE){
                                    $arrInstalasi[$ruang->instalasi_id] = $ruang->instalasi->instalasi_nama;
                                    $valInstalasi[$x] = $ruang->instalasi_id;          
                                }
                            }

                        }
                        $x++;
                    }
                }
                
                if(count($arrInstalasi) > 0)
                {
                    // mengurutkan array $arrInstalasi
                    asort($arrInstalasi);
                    if(count($arrInstalasi) == 1)
                    {
                        $data['instalasi'] = CHtml::dropdownlist('LoginForm_instalasi','instalasi_id',$arrInstalasi,
                            array(
                                'ajax'=>array(
                                    'type'=>'POST',
                                    'url'=>  CController::createUrl('site/dynamicRuangan'),
                                    'update'=>'#LoginForm_ruangan',
                                )
                            )
                        );
                    }else{
                        $data['instalasi'] = CHtml::dropdownlist('LoginForm_instalasi','instalasi_id',$arrInstalasi,
                            array(
                                'empty'=>'-- Pilih Instalasi --',
                                'ajax'=>array(
                                    'type'=>'POST',
                                    'url'=>  CController::createUrl('site/dynamicRuangan'),
                                    'update'=>'#LoginForm_ruangan',
                                )
                            )
                        );                        
                    }
                }
                
                $arrRuangan = array();
                /*
                $instalasi = $valInstalasi[0];
                $dataRuangan = RuanganM::model()->findAll('instalasi_id=:instalasi_id AND ruangan_aktif = TRUE order by ruangan_nama', 
                    array(':instalasi_id'=>$instalasi)
                );
                 * 
                 * neli > bb5deb14c5940be904690587fca8e3c5 
                 * 81dc9bdb52d04dc20036dbd8313ed055
                 * 
                 */
                $dataRuangan = $ruangan;
                if(count($dataRuangan) > 0){
                    foreach ($dataRuangan as $i=>$ruanganNya){
                        $arrRuangan[$ruanganNya->ruangan_id] = $ruanganNya->ruangan->ruangan_nama;
                    }
                }
                
                if(count($arrRuangan) > 0)
                {
                    asort($arrRuangan);
                    if(count($arrRuangan) == 1)
                    {
                        $data['ruangan'] = CHtml::dropdownlist('LoginForm_ruangan','ruangan_id',$arrRuangan);
                    }else{
                        $data['ruangan'] = CHtml::dropdownlist('LoginForm_ruangan','ruangan_id',$arrRuangan,array('empty'=>'-- Pilih Ruangan --'));
                    }
                }

                if (!empty($data['id'])){
                    $modul = ModuluserK::model()->findAllByAttributes(array('loginpemakai_id'=>$data['id']));
                }
                
                $arrModul = array();
                if (count($modul) > 0){
                    foreach ($modul as $i=>$modulNya) {
                        $modModulK = ModulK::model()->findByPk($modulNya->modul_id,'modul_aktif = TRUE');
                        if ((boolean)count($modModulK)){
                            $arrModul[$modModulK->modul_id] = $modModulK->modul_nama;
                        }
                    }
                }
                
                if (count($arrModul) > 0){
                    if(count($arrModul) == 1)
                    {
                        $data['modul'] = CHtml::dropdownlist('LoginForm_modul','modul_id',$arrModul);
                    }else{
                        $data['modul'] = CHtml::dropdownlist('LoginForm_modul','modul_id',$arrModul,array('empty'=>'-- Pilih Modul --',));
                    }                    
                }
                
                echo json_encode($data);
                Yii::app()->end();
                
            }
        }

        public function actionDynamicRuangan()
        {
            
            $dataRuangan = array();
            if (isset($_POST['LoginForm']['instalasi'])){
                $instalasi = (int)$_POST['LoginForm']['instalasi'];
                $dataRuangan = RuanganM::model()->findAll('instalasi_id=:instalasi_id AND ruangan_aktif = TRUE order by ruangan_nama', 
                        array(':instalasi_id'=>$instalasi));
            }
            
            $data = CHtml::listData($dataRuangan,'ruangan_id','ruangan_nama');
            
            if(count($data) > 0){
                if(count($data) > 1){
                    echo CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }
                
                foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }

        public function actionDynamicPropinsi()
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('propinsi');
            $criteria->compare('propinsi.propinsi_nama',$_POST['RegistrasidemoS']['propinsi']);
            $criteria->order = 'kabupaten_nama';

            $data= KabupatenM::model()->findAll($criteria);

            $data=CHtml::listData($data,'kabupaten_nama','kabupaten_nama');

            if(empty($data))
            {
                echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Pilih --'),true);
            }
            else
            {
                echo CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }

        public function actionSetKertas()
        {
          if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
               if(isset ($_POST['ukuranKertas']))
                 {
                    $session=new CHttpSession;
                    $session->open();
                    $_SESSION['ukuran_kertas'] = $_POST['ukuranKertas'];
                    $_SESSION['posisi_kertas'] = $_POST['posisiKertas'];
                    $data['pesan']='Ukuran '.$_POST['ukuranKertas'].' dan posisi '.$_POST['posisiNama'].' Berhasil Diset';
                 }
                echo json_encode($data);
                Yii::app()->end();
            }   
        }

        public function actionRegistrasi()
        {
            //echo '<pre>'.print_r($_POST['RegistrasidemoS'],1).'</pre>';
            $this->layout = '//layouts/login';

            $modRegistrasi = new RegistrasidemoS;
            $modKomen = new KomentarS;

            $model=new LoginForm;

            // collect user input data
            if(isset($_POST['RegistrasidemoS']))
            {
                    $modRegistrasi->attributes=$_POST['RegistrasidemoS'];
                    $modRegistrasi->tglregisterdemo = date('Y-m-d');
                    // validate user input and redirect to the previous page if valid

                    if($modRegistrasi->validate()){
                        $modRegistrasi->save();
                        Yii::app()->user->setFlash('info', '<strong>Registrasi</strong> berhasil.');
                        $this->redirect(array('login'));  
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Registrasi</strong> gagal.');
                    }

            }
            // display the login form
            $this->render('login',array('model'=>$model,
                                        'modRegistrasi'=>$modRegistrasi,
                                        'modRegistrasi'=>$modRegistrasi));
        }

        public function actionTestimonial()
        {
            //echo '<pre>'.print_r($_POST['RegistrasidemoS'],1).'</pre>';
            $this->layout = '//layouts/login';

            $modRegistrasi = new RegistrasidemoS;
            $modKomen = new KomentarS;

            $model=new LoginForm;
            if(isset($_POST['ajax']) && $_POST['ajax']==='testimonial-form') {
                    echo CActiveForm::validate($modKomen);
                    Yii::app()->end();
            }
            // collect user input data
            if(isset($_POST['KomentarS']))
            {
                    $modKomen->attributes=$_POST['KomentarS'];
                    $modKomen->tglkomentar = date('Y-m-d');
                    $modKomen->komentar_tampilkan = false;
                    // validate user input and redirect to the previous page if valid

                    if($modKomen->validate()){
                        $modKomen->save();
                        Yii::app()->user->setFlash('info', '<strong>Terimakasih</strong> Anda telah memberi testimoni.');
                        $this->redirect(array('login'));  
                    } else {
                        Yii::app()->user->setFlash('error', '<strong>Testimoni</strong> gagal disimpan.');
                    }

            }
            // display the login form
            $this->render('login',array('model'=>$model,
                                        'modRegistrasi'=>$modRegistrasi,
                                        'modKomen'=>$modKomen));
        }
        
        public function actionViewTestimoni()
        {
            $this->layout = '//framedialog';
            $criteria = new CDbCriteria;
            $criteria->addCondition('komentar_tampilkan = TRUE');
            $criteria->order = 'komentar_id DESC';
            $criteria->limit = 10;
            $modTestimonis = KomentarS::model()->findAll($criteria);
            
            $this->render('viewTestimoni',array('modTestimonis'=>$modTestimonis));
        }
        
        public function actionAjaxViewTestimoni()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->layout = '//framedialog';
                $criteria = new CDbCriteria;
                $criteria->addCondition('komentar_tampilkan = TRUE');
                $criteria->order = 'komentar_id DESC';
                $criteria->limit = 10;
                $modTestimonis = KomentarS::model()->findAll($criteria);

                $data = '';
                foreach ($modTestimonis as $i=>$testimoni) {
                    $data .= '<div class="alert alert-info">';
                    $data .= '<b>'.$testimoni->namakomentar.' - '.$testimoni->instanasi.'</b><br/>';
                    $data .= ''.$testimoni->deskripsikomentat.'';
                    $data .= '<span class="author-pengumuman">'.$testimoni->emailkomentar.' - '.$testimoni->websitekomentar.'</span><br/>';
                    $data .= '</div>';
                }
                echo CJSON::encode($data);
                Yii::app()->end();
            }
        }
                
        public function actionDetailModul()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                if(isset ($_GET['idModul']))
                {
                    $idModul = $_GET['idModul'];
                    $modul = ModulK::model()->findByPk($idModul);
                    $data['modul_nama'] = $modul->modul_namalainnya;
                    $data['modul_fungsi'] = $modul->modul_fungsi;
                    $data['content'] = $this->renderPartial('modul/detail', array('modul'=>$modul), true);
                    
                    echo CJSON::encode($data);
                }
                Yii::app()->end();
            } 
        }
        public function actionDashboard(){
            $model = new PPLaporankunjunganrs('search');
            $model->tglAwal = date('Y-m-d').' 00:00:00';
//            $model->tglAwal = '2012-03-03 00:00:00';
            $model->tglAkhir = date('Y-m-d 23:59:59');
            $model2 = new LaporankunjunganrsV();
            if (isset($_GET['PPLaporankunjunganrs'])) {
                $model->attributes = $_GET['PPLaporankunjunganrs'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
            }

            if (isset($_POST['data']) && ($_POST['data_id']=='garis')){
                $hasil = $model->searchDashboard()->getData(); 
                $result = array();
                $index = array();
                foreach ($hasil as $i=>$v){
                    $index[] = $v['tick'];
                    $result[] = array($i+1,(int)$v['jumlah']);
                }
                $return['result'] = $result;
                $return['index'] = $index;
                echo json_encode($return);
                exit();
            }
            if (isset($_POST['test'])){
                $model->instalasi_id = InstalasiM::model()->findByAttributes(array('instalasi_nama'=>$_POST['ruangan_nama']))->instalasi_id;
                $model->ruangan_id = CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>$model->instalasi_id)), 'ruangan_id','ruangan_id');
                
                $hasil = $model->searchDashboard()->getData(); 
                $result = array();
                $jumlah = 0;
                foreach ($hasil as $i=>$v){
                    $jumlah += (int)$v['jumlah'];
                    $result[] = array($v['data'],(int)$v['jumlah']);
                }
                $data['pie'] = $result;
                $data['spedo'] = $jumlah;
                echo json_encode($data);
                exit();

            }
            if (isset($_POST['data']) && ($_POST['data_id']=='pie')){
                $hasil = $model->searchDashboard()->getData(); 
                $result = array();
                $index = array();
                foreach ($hasil as $i=>$v){
                    $index[] = $v['tick'];
                    $result[] = array($v['tick'],(int)$v['jumlah']);
                }
                $return['result'] = $result;
                $return['index'] = $index;
                echo json_encode($return);
                exit();
            }
           
            if (isset($_POST['data']) && ($_POST['data_id']=='spedo')){
                $jumlah = $model->searchTable()->getTotalItemCount();
                echo $jumlah;
                exit();
            }

            $this->render('test/dashboard', array('model'=>$model, 'model2'=>$model2));
        }
        
        public function actionTest(){
            
            $files = dirname(__FILE__).'/test.xls';

            if (Yii::app()->request->isAjaxRequest){
                if (isset($_FILES['upload'])){
                    $tableName = $_POST['tableName'];
                    $files = CUploadedFile::getInstanceByName('upload');
                    $object = Yii::app()->yexcel->readActiveSheet($files->tempName);
                    $table = Yii::app()->db->getSchema()->getTable($tableName);     
                    echo $this->renderTable($object,$table);
                }
                Yii::app()->end();
            }
            
            if (isset($_POST['tableName'], $_POST['beginValueField'], $_POST['lastValueField'])){
                $tableName = $_POST['tableName'];
                if (isset($_POST['Hasil'])){
                    echo '<pre>';
                    print_r($_POST['Hasil']);
                    echo '<pre>';
                    exit();
                }
                $value = $_POST['Hasil'];
                $beginValueField = $_POST['beginValueField'];
                $lastValueField = $_POST['lastValueField'];
                $files = $files = CUploadedFile::getInstanceByName('upload');
                $object = Yii::app()->yexcel->readActiveSheet($files->tempName);
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $result = $this->saveMassTable($object,$tableName,$value);
                    if ($result){
//                        $transaction->commit();
                        Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data Berhasil disimpan');
                        $this->refresh();
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($exc));
                }
            }
            
            $this->render('test');
        }
        
        protected function renderTable($object,$table){
            $foreignKey = $table->foreignKeys;
            $kolom = $table->columns;
            echo '<table class="table table-bordered table-condensed">
                <thead><th>Pilih<input type="hidden" name="beginValueField" id="beginValueField"><input type="hidden" name="lastValueField" id="lastValueField"></th>';
            foreach($object[1] as $key2=>$value2){
                echo '<th>'.$key2.'</th>';
            }
                echo '</thead><tbody>';
                $findKey = (count($foreignKey) > 0) ? true : false;
                $list = array();
            foreach ($object as $key => $value) {
                echo '<tr valueField = "'.$key.'">';
                if (!$findKey){
                    echo '<td><input type="checkbox" name="Hasil['.$key.'][cek]"></td>';
                }else
                    echo '<td></td>';
                
                foreach ($value as $counter => $value2) {
                    if (isset($list[$counter]) && !$findKey && (empty($value2))){
                        echo '<td columnField="'.$counter.'">
                                <div class="input-append">
                                    <input type="hidden" name="Hasil['.$key.']['.$list[$counter][1].']" class="id"/>
                                    <input type="text" name="Hasil['.$key.']['.$list[$counter][1].'_nama]" id="tableName" style="float:left;">
                                        <span class="add-on"><i class="icon-list-alt"></i></span>
                                </div>
                              </td>';
                    }else{
                        echo '<td columnField="'.$counter.'">'.$value2.'</td>';
                    }
                    
                    if ($findKey){
                        if (isset($foreignKey[$value2]) && count($foreignKey[$value2]) == 2){
                            $list[$counter] = $foreignKey[$value2];
                            unset($foreignKey[$value2]);
                            $findKey = (count($foreignKey) > 0) ? true : false;
                        }
                    }
                }
                echo '</tr>';
            }
            echo '</tbody></table>';
            echo "<script>
                $('#excel .table tbody tr td').bind('click',function(){
                    parent = $(this).parents('tr');
                    valueField = parent.attr('valueField');
                    beginValue = $('#beginValueField').val();
                    lastValue = $('#lastValueField').val();
                    if (beginValue == ''){
                        $('#beginValueField').val(valueField);
                        parent.addClass('yellow_background').find('input[name*=\'[cek]\']').attr('checked','checked');
                    }else {
                        if (valueField == beginValue){
                            if (lastValue == ''){
                                $('#beginValueField').val('');
                                parent.removeClass('yellow_background').find('input[name*=\'[cek]\']').removeAttr('checked');
                            }
                        }else if (valueField == lastValue){
                            $('#lastValueField').val('');
                            parent.prevUntil('#excel .table tbody tr[valueField=\''+(beginValue)+'\']').removeClass('yellow_background').find('input[name*=\'[cek]\']').removeAttr('checked');
                            parent.removeClass('yellow_background').find('input[name*=\'[cek]\']').removeAttr('checked');
                        }else if (lastValue == ''){
                            $('#lastValueField').val(valueField);
                            $('#excel .table tbody tr[valueField=\''+beginValue+'\']').nextUntil('#excel .table tbody tr[valueField=\''+(parseFloat(valueField)+1)+'\']').addClass('yellow_background').find('input[name*=\'[cek]\']').attr('checked','checked');
                        }else if (lastValue != ''){
                            parent.addClass('yellow_background').find('input[name*=\'[cek]\']').attr('checked','checked');
                        }
                    }
                });
             ";
            if (count($list) > 0){
                foreach ($list as $value) {
                    echo '$("input[name*=\'['.$value[1].'_nama]\']").autocomplete({"minLength":"3","source":"/simrs/index.php?r=actionAutoComplete/getValuePrimaryKey&table='.$value[0].'&primaryKey='.$value[1].'","select":function(event,ui){$(this).parents("td").find(".id").val(ui.item.id);}});';
                }
            }
             echo '</script>';
        }
        
        protected function saveMassTable($objects,$tableName,$values){
//            $jumlahObject = count($objects);
            $i=$beginValueField;
            $kolom = array();
            $type = array();
            $table = Yii::app()->db->getSchema()->getTable($tableName);
            $columns = $table->columns;
            $listBoolean = array('Ya'=>'true','Tidak'=>'false');
            
            foreach ($columns as $counter => $column) {
                if (!$column->isPrimaryKey){
                    $kolom[] = $column->name;
                    $type[] = $column->type;
                }
            }
            
            $sql = "insert into {$tableName} (".implode(',', $kolom).") values (";
            $result = true;
            for(;$i<=$lastValueField;$i++){
                $sqlInsert = $sql;
                $valueInsert = array();
                $params = array();
                foreach ($objects[$i] as $key => $value) {
                    $valueInsert[] = ':'.$key;
                    $params[':'.$key] = (!empty($value)) ? ((isset($listBoolean[trim($value)])) ? $listBoolean[trim($value)] : $value ) : null;
                }
                $sqlInsert .= implode(', ', $valueInsert).');';
                $result = Yii::app()->db->createCommand($sqlInsert)->execute($params) && $result;
                
            }
            
            return $result;
        }
        
        public function actionCreateTemplateXcel(){
            if (isset($_GET['tableName'])){
                $this->layout='//layouts/printExcel';
                $tableName = $_GET['tableName'];
                $table = Yii::app()->db->getSchema()->getTable($tableName);     
                $model = null;
                if (!empty($table->name)){
                    $sql = 'select *from '.$table->name;
                    $model = Yii::app()->db->createCommand($sql)->queryAll();
                }
                    
                $judul = "Template Excel $tableName";
                $this->render('_template',array('table'=>$table, 'judul'=>$judul, 'model'=>$model));
            }
        }
        
        public function actionInsertNotifikasi(){
            $model = new NofitikasiR;
            $model->attributes = $_POST['NofitikasiR'];
            $model->tglnotifikasi = date( 'Y-m-d H:i:s');
            $model->create_time = date( 'Y-m-d H:i:s');
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $is_simpan = false;
            $data = array();
            
            $transaction = Yii::app()->db->beginTransaction();
            try{
                if(Yii::app()->getRequest()->getIsAjaxRequest())
                {
                    $criteria = new CDbCriteria;
                    $criteria->compare('instalasi_id',$model->instalasi_id);
                    $criteria->compare('modul_id',$model->modul_id);
                    $criteria->compare('LOWER(isinotifikasi)',strtolower($model->isinotifikasi),true);
                    $criteria->compare('create_ruangan',$model->create_ruangan);
                    $criteria->addCondition("DATE(tglnotifikasi) = DATE(NOW()) AND isread = false");
                    $is_exist = NofitikasiR::model()->find($criteria);
                    
                    if(!$is_exist)
                    {
                        if($model->save()){
                            $is_simpan = true;
                        }
                    }else{
                        $attributes = array(
                            'update_time' => date('Y-m-d H:i:s'),
                            'update_loginpemakai_id' => Yii::app()->user->id,
                        );
                        $update = $model::model()->updateByPk($is_exist['nofitikasi_id'], $attributes);
                        if($update){
                            $is_simpan = true;
                        }
                    }
                    
                    $attributes = array(
                        'instalasi_id'=>Yii::app()->user->getState('instalasi_id'),
                        'modul_id'=>Yii::app()->session['modulId'],
                        'isread'=> false
                    );
                    $data_notif = NofitikasiR::model()->findAllByAttributes($attributes);
                    $isi_notif = "";
                    if(count($data_notif) > 0)
                    {
                        foreach($data_notif as $value)
                        {
                            $isi_notif .= '<li class="read">';
                            $isi_notif .= '<a href="index.php?r=sistemAdministrator/comment/update&id=12">';
                            $isi_notif .= '<span class="sender">'. $value['judulnotifikasi'] .'</span>';
                            $isi_notif .= '<span class="message">'. $value['isinotifikasi'] .'</span>';
                            $isi_notif .= '<span class="time">'. $value['tglnotifikasi'] .'</span>';
                            $isi_notif .= '</a>';
                            $isi_notif .= '</li>';
                        }
                    }
                    $data['template'] = $isi_notif;
                    $data['count_notif'] = count($data_notif);
                    
                    if($is_simpan){
                        $data['pesan'] = 'ok';
                        $transaction->commit();
                    }else{
                        $data['pesan'] = 'gagal';
                        $transaction->rollback();
                    }
                    echo CJSON::encode($data);
                    Yii::app()->end();
                }else{

                }
            }catch(Exception $exc){
                $transaction->rollback();
            }
        }
        
        public function actionGetNotifikasi(){
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                if(!isset(Yii::app()->user->id) || !isset(Yii::app()->session['modulId'])){
                    echo "Session habis!";
                    Yii::app()->end();
                }
				
                $attributes = array(
                    'modul_id'=>Yii::app()->session['modulId'],
                    'isread'=> false
                );
                $data['count_notif'] = count(NofitikasiR::model()->findAllByAttributes($attributes,array('order'=>'create_time desc', 'limit'=>11)));
                echo CJSON::encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionSetReadNotifikasi()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                $id_pesan = $_GET['id_pesan'];
                $attributes = array(
                    'update_time' => date('Y-m-d H:i:s'),
                    'update_loginpemakai_id' => Yii::app()->user->id,
                    'isread' => true
                );
                $update = NofitikasiR::model()->updateByPk($id_pesan, $attributes);
                $data = array(
                    'pesan' => ($update == true ? 'ok' : 'not' )
                );
                echo CJSON::encode($data);
                Yii::app()->end();
            }            
        }

        
        public function actionGetSessionUser()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                $update = true;
                if(isset(Yii::app()->session['timeout']))
                {
                    $session_life = time() - Yii::app()->session['timeout'];
                    if($session_life > Yii::app()->session['inactive'])
                    {
                        LoginpemakaiK::model()->updateByPk(Yii::app()->user->id, array('lastlogin'=>date( 'Y-m-d H:i:s', time() ),'statuslogin'=>FALSE));
                        Yii::app()->user->logout();
                        $update = false;
                    }
                }else{
                    $update = false;
                }
                
                $data = array(
                    'pesan' => ($update == true ? 'ok' : 'not' )
                );
                echo CJSON::encode($data);
                Yii::app()->end();
            }            
        }
        
        public function actionGetDetailNotifikasi()        
        {
            $notifikasi_id = isset($_GET['notifikasi_id']) ? $_GET['notifikasi_id'] : 99999999999;
            $notifikasi = NofitikasiR::model()->findByPk($notifikasi_id);
            
            $data = '<table>';
            $data .= '<tr>';
            $data .= '<td width="90">Judul Pesan</td>';
            $data .= '<td width="5">:</td>';
            $data .= '<td>'. $notifikasi->judulnotifikasi .'</td>';
            $data .= '</tr>';
            $data .= '<tr>';
            $data .= '<td>Isi Pesan</td>';
            $data .= '<td>:</td>';
            $data .= '<td>'. $notifikasi->isinotifikasi .'</td>';
            $data .= '</tr>';
            $data .= '<tr>';
            $data .= '<td>Tgl</td>';
            $data .= '<td>:</td>';
            $data .= '<td>'. date("d-m-Y h:m:s", strtotime($notifikasi->create_time)) .'</td>';
            $data .= '</tr>';
            $data .= '<tr>';
            $data .= '</table>';
            echo($data);
        }
//        public function actionDashboard(){
//            $model = new PPLaporankunjunganrs('search');
//            $model->tglAwal = date('Y-m-d').' 00:00:00';
////            $model->tglAwal = '2012-03-03 00:00:00';
//            $model->tglAkhir = date('Y-m-d H:i:s');
//            
//            if (isset($_GET['PPLaporankunjunganrs'])) {
//                $model->attributes = $_GET['PPLaporankunjunganrs'];
//                $format = new CustomFormat();
//                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
//                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
//            }
//
//            if (isset($_POST['data']) && ($_POST['data_id']=='garis')){
//                echo json_encode(array(array(1,rand(1,4)), array(2,rand(1,4))));
//                exit();
//            }
//
//            if (isset($_POST['data']) && ($_POST['data_id']=='pie')){
//                echo json_encode(array(array(0=>"Rawat Jalan",1=>rand(0,10)),array(0=>"Rawat Inap",1=>1)));
//                exit();
//            }
//            
//            if (isset($_POST['data']) && ($_POST['data_id']=='spedo')){
//                $jumlah = $model->searchGrafik()->getTotalItemCount();
//                echo rand(0,50);
//                exit();
//            }
//
//            $this->render('test/dashboard', array('model'=>$model, 'model2'=>$model2));
//        }
}
