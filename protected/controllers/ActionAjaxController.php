<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Yii::import('rawatJalan.controllers.TindakanController'); //untuk menggunakan fungsi saveJurnalRekening
class ActionAjaxController extends Controller
{
    public function actionPasienRujukRI()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $idPendaftaran=$_POST['idPendaftaran'];
              $idPasien=  PendaftaranT::model()->find('pendaftaran_id='.$idPendaftaran.'')->pasien_id;
                $modPasienPulang = new PasienpulangT;
                $modPasienPulang->pendaftaran_id=$idPendaftaran;
                $modPasienPulang->pasien_id=$idPasien;
                $modPasienPulang->tglpasienpulang=date('Y-m-d H:i:s');
                $modPasienPulang->carakeluar=Params::CARA_KELUAR_RI;
                $modPasienPulang->kondisipulang=Params::KONDISI_PULANG_RI;
                $modPasienPulang->ruanganakhir_id=Yii::app()->user->getState('ruangan_id');
                $modPasienPulang->lamarawat=0;
                $modPasienPulang->satuanlamarawat='lamarawat';
//                echo $modPasienPulang->ruanganakhir_id;exit;
                if($modPasienPulang->save()){
                    PendaftaranT::model()->updateByPk($idPendaftaran, array('pasienpulang_id'=>$modPasienPulang->pasienpulang_id,'statusperiksa'=>'SEDANG DIRAWAT INAP'));
                    $data['pesan']='Berhasil';
                }else{
                    $data['pesan']='Gagal';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
     
    public function actionGetNamaDiagnosa()
    {
          if(Yii::app()->request->isAjaxRequest) {
         
              $data['namaDiagnosa']=DiagnosaM::model()->findByPk($_POST['idDiagnosa'])->diagnosa_nama;
              echo json_encode($data);
         Yii::app()->end();
        }
    }
     
    public function actionGetPenerimaanItems()
    {
        if(Yii::app()->request->isAjaxRequest)
		{
            $isPPN = 0;
            $isPPH = 0;
            $idPenerimaanBarang = $_POST['idPenerimaanBarang'];
            $penerimaanBarang = PenerimaanbarangT::model()->findByPk($idPenerimaanBarang);
            $modPenerimaanDetail = PenerimaandetailT::model()->with('obatalkes','sumberdana','satuankecil','satuanbesar')->findAll('penerimaanbarang_id='.$idPenerimaanBarang.'');
            $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$penerimaanBarang->permintaanpembelian_id));
            if(count($modUangMuka) > 0)
			{
                foreach($modUangMuka as $u=>$uang){
                    $jmlUang += $uang->jumlahuang;
                }
            }else{
                $jmlUang = 0;
            }
//            echo $jmlUang;exit;
            $subTotal = 0;
            $tr = "";
            foreach ($modPenerimaanDetail AS $i=>$tampilDetail):
                $modFakturDetail = new FakturdetailT;
                $subTotal = (($tampilDetail['harganettoper'] + $tampilDetail['hargappnper'] + $tampilDetail['hargapphper'])*$tampilDetail['jmlterima']) - $tampilDetail['jmldiscount'];
				$harga_bruto = (($tampilDetail['harganettoper'] * $tampilDetail['jmlkemasan']) * $tampilDetail['jmlterima']) - $tampilDetail['jmldiscount'];
                
				$tr .="<tr>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']satuankecil_id',array('value'=>$tampilDetail['satuankecil_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']obatalkes_id',array('value'=>$tampilDetail['obatalkes_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']penerimaandetail_id',array('value'=>$tampilDetail['penerimaandetail_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']satuanbesar_id',array('value'=>$tampilDetail['satuanbesar_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']sumberdana_id',array('value'=>$tampilDetail['sumberdana_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']tglkadaluarsa',array('value'=>$tampilDetail['tglkadaluarsa'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']jmlkemasan',array('value'=>$tampilDetail['jmlkemasan'])).
                              CHtml::activeHiddenField($modFakturDetail,'['.$i.']hargasatuan',array('value'=>$tampilDetail['harganettoper'])).
						"</td>
                        <td>".$tampilDetail->sumberdana['sumberdana_nama']."</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_kategori']."/<br/>".$tampilDetail->obatalkes['obatalkes_nama']."</td>
                        <td>".
							CHtml::activeTextField($modFakturDetail,'['.$i.']jmlpermintaan',array('value'=>$tampilDetail['jmlpermintaan'],'readonly'=>TRUE,'class'=>'span1 angka permintaan'))
						."</td>
                        <td>".
							CHtml::activeTextField($modFakturDetail,'['.$i.']jmlterima',array('value'=>$tampilDetail['jmlterima'],'readonly'=>TRUE,'class'=>'span1 angka terima','onkeyup'=>'hitungJumlahDariDiterima(this);')) . " / " .
							CHtml::activeTextField($modFakturDetail,'['.$i.']jmlkemasan',array('value'=>$tampilDetail['jmlkemasan'],'readonly'=>TRUE,'class'=>'span1 angka jml_kemasan','onkeyup'=>'hitungJumlahDariDiterima(this);'))
						."</td>
                        <td>".
							CHtml::activeTextField($modFakturDetail,'['.$i.']harganettofaktur',array('style'=>'text-align:right;','value'=>$tampilDetail['harganettoper'],'readonly'=>TRUE,'class'=>'span2 angka currency netto','onkeyup'=>'hitungSemua();')) .
							CHtml::hiddenField(
								'harga_bruto',
								ceil($harga_bruto),
								array(
									'class'=>'span2 currency harga_bruto'
								)
							)
						."</td>
						<td>".
							CHtml::activeTextField($modFakturDetail,'['.$i.']persendiscount',array('value'=>$tampilDetail['persendiscount'],'maxlength'=>5,'class'=>'span1 float persenDiskon','onkeyup'=>'hitungJumlahDariPersentaseDiskon(this);')) .
							CHtml::hiddenField('diskonLama',$tampilDetail['persendiscount'],array('value'=>$tampilDetail['persendiscount'],'maxlength'=>3,'class'=>'span1 float diskonLama'))
                        ."</td>
						<td>".CHtml::activeTextField($modFakturDetail,'['.$i.']jmldiscount',array('style'=>'text-align:right;','value'=>$tampilDetail['jmldiscount'],'class'=>'span2 angka currency jmlDiskon','readonly'=>true))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'['.$i.']hargappnfaktur',array('style'=>'text-align:right;','value'=>$tampilDetail['hargappnper'],'readonly'=>FALSE,'class'=>'span2 angka currency ppn','onkeyup'=>'hitungSemua();'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'['.$i.']hargapphfaktur',array('style'=>'text-align:right;','value'=>$tampilDetail['hargapphper'],'readonly'=>FALSE,'class'=>'span2 angka currency pph','onkeyup'=>'hitungSemua();'))."</td>
                        <td>".CHtml::textField('subTotal', ceil($harga_bruto),array('style'=>'text-align:right;','readonly'=>true,'class'=>'span2 angka currency subTotal'))."</td>
                    </tr>
				";
				//<td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'javascript:void(0);','onclick'=>'remove_row(this);','style'=>'text-decoration:none;'))."</td>
//            'onkeyup'=>'hitungJumlahDariDiskon(this);'
             if($tampilDetail['hargappnper']>0){
                 $isPPN='1';
             }
             if($tampilDetail['hargapphper']>0){
                 $isPPH='1';
             }
             $idPenerimaanBarang=$tampilDetail['penerimaanbarang_id'];
           endforeach;
           $modPenerimaanBarang=PenerimaanbarangT::model()->findByPk($idPenerimaanBarang);
           $data['tr']=$tr;
           $data['supplier_id']=$modPenerimaanBarang->supplier_id;
           $data['penerimaanbarang_id']=$modPenerimaanBarang->penerimaanbarang_id;
           $data['isPPN']=$isPPN;
           $data['isPPH']=$isPPH;
           $data['uangMuka']=$jmlUang;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
     
    public function actionGetPenerimaanBarang()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $isPPN=0;
            $isPPH=0;
            $idPenerimaanBarang=$_POST['idPenerimaanBarang'];
            $modFakturDetail = new FakturdetailT;
            $modPenerimaanDetail = PenerimaandetailT::model()->with('obatalkes','sumberdana','satuankecil','satuanbesar')->findAll('penerimaanbarang_id='.$idPenerimaanBarang.'');
            foreach ($modPenerimaanDetail AS $tampilDetail):
                $tr .="<tr>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modFakturDetail,'satuankecil_id[]',array('value'=>$tampilDetail['satuankecil_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'obatalkes_id[]',array('value'=>$tampilDetail['obatalkes_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'penerimaandetail_id[]',array('value'=>$tampilDetail['penerimaandetail_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'satuanbesar_id[]',array('value'=>$tampilDetail['satuanbesar_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'sumberdana_id[]',array('value'=>$tampilDetail['sumberdana_id'])).
                              CHtml::activeHiddenField($modFakturDetail,'tglkadaluarsa[]',array('value'=>$tampilDetail['tglkadaluarsa'])).
                              CHtml::activeHiddenField($modFakturDetail,'jmlkemasan[]',array('value'=>$tampilDetail['jmlkemasan'])).
                       "</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_kode']."</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_nama']."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'jmlpermintaan[]',array('value'=>$tampilDetail['jmlpermintaan'],'readonly'=>TRUE,'class'=>'span1 permintaan'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'jmlterima[]',array('value'=>$tampilDetail['jmlterima'],'readonly'=>TRUE,'class'=>'span1 terima','onkeyup'=>'numberOnly(this);hitungJumlahDariDiterima(this);'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'persendiscount[]',array('value'=>$tampilDetail['persendiscount'],'class'=>'span1 persenDiskon','onkeyup'=>'numberOnly(this);hitungJumlahDariPersentaseDiskon(this);'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'jmldiscount[]',array('value'=>$tampilDetail['jmldiscount'],'class'=>'span1 jmlDiskon','onkeyup'=>'numberOnly(this);hitungJumlahDariDiskon(this);'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'harganettofaktur[]',array('value'=>$tampilDetail['harganettoper'],'readonly'=>TRUE,'class'=>'span1 netto'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'hargappnfaktur[]',array('value'=>$tampilDetail['hargappnper'],'readonly'=>TRUE,'class'=>'span1 ppn currency'))."</td>
                        <td>".CHtml::activeTextField($modFakturDetail,'hargapphfaktur[]',array('value'=>$tampilDetail['hargapphper'],'readonly'=>TRUE,'class'=>'span1 pph currency'))."</td>
                    </tr>
                    ";
            
             if($tampilDetail['hargappnper']>0){
                 $isPPN='1';
             }
             if($tampilDetail['hargapphper']>0){
                 $isPPH='1';
             }
             $idPenerimaanBarang=$tampilDetail['penerimaanbarang_id'];
           endforeach;
           $modPenerimaanBarang=PenerimaanbarangT::model()->findByPk($idPenerimaanBarang);
           $data['tr']=$tr;
           $data['supplier_id']=$modPenerimaanBarang->supplier_id;
           $data['penerimaanbarang_id']=$modPenerimaanBarang->penerimaanbarang_id;
           $data['isPPN']=$isPPN;
           $data['isPPH']=$isPPH;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    /**
     * actionGetPermintaanPembelianDariObatAlkes untuk:
     * 1. Transaksi penerimaan items (gudang Farmasi)
     */
    public function actionGetPermintaanPembelianDariObatAlkes()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $tgl_kadaluarsa = $_POST['tgl_kadaluarsa'];
            $idObat=$_POST['idObat'];
            $qty=$_POST['qty'];
            $idSupplier = $_POST['idSupplier'];
            $diskon = $_POST['diskon'];
            if(empty($diskon)){
                $diskon=0;
            }
            $modPenerimaanDetail = new PenerimaandetailT;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            $modObatAlkes->kemasanbesar = ($modObatAlkes->kemasanbesar > 0) ? $modObatAlkes->kemasanbesar : 1;
            $obatSupplier=ObatsupplierM::model()->findByAttributes(array('obatalkes_id'=>$idObat,'supplier_id'=>$idSupplier));
            $hargabelibesar = ($obatSupplier->hargabelibesar > 0) ? $obatSupplier->hargabelibesar : $obatSupplier->hargabelikecil * $modObatAlkes->kemasanbesar;
            $harganetto = number_format($hargabelibesar / ($modObatAlkes->kemasanbesar * $qty),2,'.','');
//            $harganetto = ($obatSupplier->hargabelikecil > 0) ? $obatSupplier->hargabelikecil : 0;
            $subTotal = $hargabelibesar * $qty;
                $tr ="<tr>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)).
                              CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id,"class"=>'obatAlkes')).
                       "</td>
                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                        <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'['.$idObat.']tglkadaluarsa',array('value'=>$tgl_kadaluarsa,'readonly'=>TRUE,'class'=>'span2 tgl isRequired', 'style'=>'width:80px;','onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'['.$idObat.']jmlkemasan',array('value'=>$modObatAlkes->kemasanbesar,'class'=>'span1 numbersOnly','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                        ." ".CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id,'class'=>'span1')).$modObatAlkes->satuankecil->satuankecil_nama
                        ."/".CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']satuanbesar_id',array('value'=>$modObatAlkes->satuanbesar_id,'class'=>'span1')).$modObatAlkes->satuanbesar->satuanbesar_nama."</td>";
                $tr .="<td>".CHtml::activeTextField($modPenerimaanDetail,'['.$idObat.']jmlterima',array('value'=>$qty,'class'=>'span1 numbersOnly','onkeyup'=>'hitungSubtotal(this);','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")) //hitungJumlahDariDiterima(this);
                        ." ".$modObatAlkes->satuanbesar->satuanbesar_nama
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']hargabelibesar',array('value'=>$hargabelibesar,'readonly'=>FALSE,'class'=>'span1 numbersOnly','style'=>'width:70px;','onkeyup'=>'hitungHargaNetto(this);hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']harganettoper',array('value'=>$harganetto,'readonly'=>true,'class'=>'span1','style'=>'width:70px;','onkeyup'=>'hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']persendiscount',array('value'=>$diskon,'class'=>'span1 float','onkeyup'=>'hitungJmlDiskon(this);hitungSubtotal(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"))
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']jmldiscount',array('value'=>0,'class'=>'span1 numbersOnly','style'=>'width:70px;','onkeyup'=>"hitungPersenDiskon(this);hitungSubtotal(this);", 'onkeypress'=>"return $(this).focusNextInputField(event)")) //hitungJumlahDariPersentaseDiskon(this); hitungJumlahDariDiskon(this);
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']hargapphper',array('value'=>0,'readonly'=>FALSE,'class'=>'span1 pph'))
                        .CHtml::activeHiddenField($modPenerimaanDetail,'['.$idObat.']hargappnper',array('value'=>0,'readonly'=>FALSE,'class'=>'span1 ppn numbersOnly'))
                        .CHtml::HiddenField('subTotal',$subTotal, array('readonly'=>TRUE,'class'=>'span1 subTotal isRequired numbersOnly','style'=>'width:70px;', 'onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>
                    </tr>
                    ";
                
           $data['tr']=$tr;
           
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
           public function actionGetTglKadaluarsa()
           {
               if(Yii::app()->request->isAjaxRequest){
                   $tgl_kadaluarsa = $_POST['tgl_kadaluarsa'];
        //           $modShift = ShiftM::model()->findByPk($shiftId);

                    $data['tgl'] = $tgl_kadaluarsa;
        //            $data['jamakhir']= date('Y-m-d')." ".$modShift->shift_jamakhir;

                    echo CJSON::encode($data);
                    Yii::app()->end();
               }
           }

           public function actionGetTakaran()
           {
               if(Yii::app()->request->isAjaxRequest){
                   $takaran = $_POST['takaranPembelian'];
                   $jmltakaran = $_POST['takaran'];
                   $qty = $_POST['qty'];
//
                   if($jmltakaran == 1){
                       $takaranPembelian = 3 * $qty;
                   }else if($jmltakaran == 2){
                       $takaranPembelian = 4;
                   }else if($jmltakaran == 3){
                       $takaranPembelian = 5;
                   }else{
                       $takaranPembelian = $takaran;
                   }
                   $data['qty'] = $takaranPembelian;
                   $data['jml'] = $takaran;
                   $data['test'] = $qty;

                    echo CJSON::encode($data);
                    Yii::app()->end();
               }
           }
           
           public function actionGetPersenDokter(){
               if(Yii::app()->request->isAjaxRequest){
                   $hargaNetto = $_POST['hargaNetto'];
//
                   $jasaResep = JasaresepM::model()->find(' '. $hargaNetto .' between minharga AND maxharga');
                   $data['jasaResep'] = $jasaResep->persenjasa;

                    echo CJSON::encode($data);
                    Yii::app()->end();
               }
           }

        public function actionGetPengOrganisasi()
        {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai = $_POST['pegawai_id'];
            $model = PengorganisasiR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai),array('order'=>'pengorganisasi_tahun'));
            $i=1;
            $tr = '';
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/pengorganisasiR/deletePengOrganisasi',array('id'=>$row->pengorganisasi_id));
                
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->pengorganisasi_nama.'</td>';
                    $tr .= '<td>'.$row->pengorganisasi_kedudukan.'</td>';
                    $tr .= '<td>'.$row->pengorganisasi_tahun.'</td>';
                    $tr .= '<td>'.$row->pengorganisasi_lamanya.'</td>';
                    
                    $tr .= '<td>'.$row->pengorganisasi_tempat.'</td>';
 $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    public function actionGetPendidikanpegawai()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPendidikanpegawai = PendidikanpegawaiR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'tglmasuk'));
            $i=1;
            $tr = '';
            foreach ($modPendidikanpegawai as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePendidikanpegawai',array('pendidikanpegawai_id'=>$row->pendidikanpegawai_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->pendidikan->pendidikan_nama.'</td>';
                    $tr .= '<td>'.$row->namasek_univ.'</td>';
                    $tr .= '<td>'.$row->almtsek_univ.'</td>';
                    $tr .= '<td>'.$row->tglmasuk.'</td>';
                    $tr .= '<td>'.$row->lamapendidikan_bln.' bulan</td>';
                    $tr .= '<td>'.$row->no_ijazah_sert.'</td>';
                    $tr .= '<td>'.$row->tgl_ijazah_sert.'</td>';
                    $tr .= '<td>'.$row->ttd_ijazah_sert.'</td>';
                    $tr .= '<td>'.$row->nilailulus.' / '.$row->gradelulus.'</td>';
                    $tr .= '<td>'.$row->keteranganpend.'</td>';

                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }

    public function actionGetSusunanKeluarga()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai = $_POST['pegawai_id'];
            $model = SusunankelM::model()->findAllByAttributes(array('pegawai_id'=>$pegawai),array('order'=>'susunankel_id'));
            $i=1;
            $tr = '';
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/susunankelM/deleteKeluarga',array('id'=>$row->susunankel_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->nourutkel.'</td>';
                    $tr .= '<td>'.$row->hubkeluarga.'</td>';
                    $tr .= '<td>'.$row->susunankel_nama.'</td>';
                    $tr .= '<td>'.$row->susunankel_jk.'</td>';
                    $tr .= '<td>'.$row->susunankel_tempatlahir.'</td>';
                    $tr .= '<td>'.$row->susunankel_tanggallahir.'</td>';
                    $tr .= '<td>'.$row->pekerjaan_nama.'</td>';
                    $tr .= '<td>'.$row->pendidikan_nama.'</td>';
                    $tr .= '<td>'.$row->susunankel_tanggalpernikahan.'</td>';
                    $tr .= '<td>'.$row->susunankel_tempatpernikahan.'</td>';
                    $tr .= '<td>'.$row->susunankeluarga_nip.'</td>';
                    if($model){
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                    }
                    $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
     public function actionGetPangkat()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai = $_POST['KPKenaikanpangkatT']['pegawai_id'];
            $model = KenaikanpangkatT::model()->findAllByAttributes(array('pegawai_id'=>$pegawai));
            $i=1;
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/kenaikanpangkatT/deletePangkat',array('id'=>$row->kenaikanpangkat_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->jabatan->jabatan_nama.'</td>';
                    $tr .= '<td>'.$row->pangkat->pangkat_nama.'</td>';
                    $tr .= '<td>'.$row->usulan->uskenpangkat_masakerjatahun.'</td>';
                    $tr .= '<td>'.$row->usulan->uskenpangkat_masakerjabulan.'</td>';
                    $tr .= '<td>'.$row->usulan->uskenpangkat_gajipokok.'</td>';
                    $tr .= '<td>'.$row->usulan->uskenpangkat_nosk.'</td>';
                    $tr .= '<td>'.$row->usulan->uskenpangkat_tglsk.'</td>';
                    $tr .= '<td>'.$row->pimpinannama.'</td>';
                    if($model){
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                    }
                    $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPegawaidiklat()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPegawaidiklat = PegawaidiklatT::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'tglditetapkandiklat'));
            $i=1;
            $tr = '';
            foreach ($modPegawaidiklat as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePegawaidiklat',array('pegawaidiklat_id'=>$row->pegawaidiklat_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->jenisdiklat->jenisdiklat_nama.'</td>';
                    $tr .= '<td>'.$row->pegawaidiklat_nama.'</td>';
                    $tr .= '<td>'.$row->pegawaidiklat_tahun.'</td>';
                    $tr .= '<td>'.$row->pegawaidiklat_lamanya.'</td>';
                    $tr .= '<td>'.$row->pegawaidiklat_tempat.'</td>';
                    $tr .= '<td>'.$row->nomorkeputusandiklat.'</td>';
                    $tr .= '<td>'.$row->tglditetapkandiklat.'</td>';
                    $tr .= '<td>'.$row->pejabatygmemdiklat.'</td>';
                    $tr .= '<td>'.$row->pegawaidiklat_keterangan.'</td>';
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPengalamankerja()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPengalamankerja = PengalamankerjaR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'pengalamankerja_nourut'));
            $i=1;
            $tr = '';
            foreach ($modPengalamankerja as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePengalamankerja',array('pengalamankerja_id'=>$row->pengalamankerja_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->namaperusahaan.'</td>';
                    $tr .= '<td>'.$row->bidangperusahaan.'</td>';
                    $tr .= '<td>'.$row->jabatanterahkir.'</td>';
                    $tr .= '<td>'.$row->tglmasuk.'</td>';
                    $tr .= '<td>'.$row->tglkeluar.'</td>';
                    $tr .= '<td>'.$row->lama_tahun.' tahun'.$row->lama_bulan.' bulan'.'</td>';
                    $tr .= '<td>'.$row->alasanberhenti.'</td>';
                    $tr .= '<td>'.$row->keterangan.'</td>';

                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }

    public function actionGetPrestasiKerja()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai = $_POST['pegawai_id'];
            $model = PrestasikerjaR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai),array('order'=>'tglprestasidiperoleh'));
            $i=1;
            $tr = '';
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/prestasikerjaR/deletePrestasi',array('id'=>$row->prestasikerja_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->nourutprestasi.'</td>';
                    $tr .= '<td>'.$row->tglprestasidiperoleh.'</td>';
                    $tr .= '<td>'.$row->instansipemberi.'</td>';
                    $tr .= '<td>'.$row->pejabatpemberi.'</td>';
                    $tr .= '<td>'.$row->namapenghargaan.'</td>';
                    $tr .= '<td>'.$row->keteranganprestasi.'</td>';
 $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPegawaijabatan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPegawaijabatan = PegawaijabatanR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'tglditetapkanjabatan'));
            $i=1;
            $tr = '';
            foreach ($modPegawaijabatan as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePegawaijabatan',array('pegawaijabatan_id'=>$row->pegawaijabatan_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->nomorkeputusanjabatan.'</td>';
                    $tr .= '<td>'.$row->tglditetapkanjabatan.'</td>';
                    $tr .= '<td>'.$row->tmtjabatan.'</td>';
                    $tr .= '<td>'.$row->tglakhirjabatan.'</td>';
                    $tr .= '<td>'.$row->keterangan.'</td>';
                    $tr .= '<td>'.$row->pejabatygmemjabatan.'</td>';
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPegmutasi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPegmutasi = PegmutasiR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'pegmutasi_id'));
            $i=1;
            $tr = '';
            foreach ($modPegmutasi as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePegmutasi',array('pegmutasi_id'=>$row->pegmutasi_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->nomorsurat.'</td>';
                    $tr .= '<td>'.$row->jabatan_nama.'</td>';
//                    $tr .= '<td>'.$row->pangkat_nama.'</td>';
                    $tr .= '<td>'.$row->nosk.'</td>';
                    $tr .= '<td>'. $row->tglsk .'</td>';
                    $tr .= '<td>'. $row->tmtsk .'</td>';
                    $tr .= '<td>'.$row->jabatan_baru.'</td>';
//                    $tr .= '<td>'.$row->pangkat_baru.'</td>';
                    $tr .= '<td>'.$row->mengetahui_nama.'</td>';
                    $tr .= '<td>'.$row->pimpinan_nama.'</td>';

                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }

    public function actionGetPerjalananDinas()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai = $_POST['pegawai_id'];
            $model = PerjalanandinasR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai),array('order'=>'perjalanandinas_id'));
            $i=1;
            $tr = '';
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/perjalanandinasR/deletePerjalanan',array('id'=>$row->perjalanandinas_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->nourutperj.'</td>';
                    $tr .= '<td>'.$row->tujuandinas.'</td>';
                    $tr .= '<td>'.$row->tugasdinas.'</td>';
                    $tr .= '<td>'.$row->descdinas.'</td>';
                    $tr .= '<td>'.$row->alamattujuan.'</td>';
                    $tr .= '<td>'.$row->propinsi_nama.'</td>';
                    $tr .= '<td>'.$row->kotakabupaten_nama.'</td>';
                    $tr .= '<td>'.$row->tglmulaidinas.'</td>';
                    $tr .= '<td>'.$row->sampaidengan.'</td>';
                    $tr .= '<td>'.$row->negaratujuan.'</td>';
 $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPegawaicuti()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modPegawaicuti = PegawaicutiT::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'tglmulaicuti'));
            $i=1;
            $tr = '';
            foreach ($modPegawaicuti as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deletePegawaicuti',array('pegawaicuti_id'=>$row->pegawaicuti_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->jeniscuti_id.'</td>';
                    $tr .= '<td>'.$row->tglmulaicuti.' s/d '.$row->tglakhircuti.'</td>';
                    $tr .= '<td>'.$row->lamacuti.' hari'.'</td>';
                    $tr .= '<td>'.$row->noskcuti.'</td>';
                    $tr .= '<td>'.$row->tglditetapkanskcuti.'</td>';
                    $tr .= '<td>'.$row->keperluancuti.'</td>';
                    $tr .= '<td>'.$row->keterangan.'</td>';
                    $tr .= '<td>'.$row->pejabatmengetahui.'</td>';
                    $tr .= '<td>'.$row->pejabatmenyetujui.'</td>';
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetIjintugasbelajar()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modIzintugasbelajar = IzintugasbelajarR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'izintugasbelajar_id'));
            $i=1;
            $tr = '';
            foreach ($modIzintugasbelajar as $row)
            {
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deleteIzintugasbelajar',array('izintugasbelajar_id'=>$row->izintugasbelajar_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->tglmulaibelajar.'</td>';
                    $tr .= '<td>'.$row->nomorkeputusan.'</td>';
                    $tr .= '<td>'.$row->tglditetapkan .'</td>';
                    $tr .= '<td>'.$row->keteranganizin.'</td>';
                    $tr .= '<td>'.$row->pejabatmemutuskan.'</td>';
                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetHukdisiplin()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modHukdisiplin = HukdisiplinR::model()->findAllByAttributes(array('pegawai_id'=>$pegawai_id),array('order'=>'hukdisiplin_id'));
            $i=1;
            $tr = '';
            foreach ($modHukdisiplin as $row)
            {
                
                $urlDelete = Yii::app()->createUrl('kepegawaian/PegawaiM/deleteHukdisiplin',array('hukdisiplin_id'=>$row->hukdisiplin_id));
                $tr .= '<tr>';
                   
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->jnshukdisiplin->jnshukdisiplin_nama.'</td>';
                    $tr .= '<td>'.$row->jabatan->jabatan_nama.'</td>';
                    $tr .= '<td>'.$row->hukdisiplin_tglhukuman.'</td>';
                    $tr .= '<td>'.$row->hukdisiplin_ruangan.'</td>';
                    $tr .= '<td>'.$row->hukdisiplin_nosk.'</td>';
                    $tr .= '<td>'.$row->hukdisiplin_lamabln.' bulan'.'</td>';
                    $tr .= '<td>'.$row->hukdisiplin_keterangan.'</td>';

                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetPermintaanPembelian()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idPermintaanPembelian=$_POST['idPermintaanPembelian'];
            $modPenerimaanDetail = new PenerimaandetailT;
            $modPermintaanDetail = PermintaandetailT::model()->with('obatalkes','sumberdana','satuankecil','satuanbesar')->findAll('permintaanpembelian_id='.$idPermintaanPembelian.'');
            $tr = '';
            foreach ($modPermintaanDetail AS $tampilDetail):
                $tr .="<tr>
                        <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))."</td>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modPenerimaanDetail,'satuankecil_id[]',array('value'=>$tampilDetail['satuankecil_id'])).
                              CHtml::activeHiddenField($modPenerimaanDetail,'sumberdana_id[]',array('value'=>$tampilDetail['sumberdana_id'])).
                              CHtml::activeHiddenField($modPenerimaanDetail,'obatalkes_id[]',array('value'=>$tampilDetail['obatalkes_id'])).
                              CHtml::activeHiddenField($modPenerimaanDetail,'satuanbesar_id[]',array('value'=>$tampilDetail['satuanbesar_id'])).
                              CHtml::activeHiddenField($modPenerimaanDetail,'jmlkemasan[]',array('value'=>$tampilDetail['jmlkemasan'])).
                       "</td>
                        <td>".$tampilDetail->sumberdana['sumberdana_nama']."</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_kategori']."/<br/>".$tampilDetail->obatalkes['obatalkes_nama']."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'jmlpermintaan[]',array('value'=>$tampilDetail['jmlpermintaan'],'readonly'=>TRUE,'class'=>'span1 permintaan'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'jmlterima[]',array('value'=>$tampilDetail['jmlpermintaan'],'class'=>'span1 terima','onkeyup'=>'numberOnly(this);hitungJumlahDariDiterima(this);'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'persendiscount[]',array('value'=>$tampilDetail['persendiscount'],'class'=>'span1 persenDiskon','onkeyup'=>'numberOnly(this);hitungJumlahDariPersentaseDiskon(this);'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'jmldiscount[]',array('value'=>$tampilDetail['jmldiscount'],'class'=>'span1 jmlDiskon','onkeyup'=>'numberOnly(this);hitungJumlahDariDiskon(this);'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'harganettoper[]',array('value'=>$tampilDetail['harganettoper'],'readonly'=>TRUE,'class'=>'span1 netto'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'hargappnper[]',array('value'=>$tampilDetail['hargappnper'],'readonly'=>TRUE,'class'=>'span1 ppn'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'hargapphper[]',array('value'=>$tampilDetail['hargapphper'],'readonly'=>TRUE,'class'=>'span1 pph'))."</td>
                        <td>".CHtml::activeTextField($modPenerimaanDetail,'tglkadaluarsa[]',array('value'=>$tampilDetail['tglkadaluarsa'],'readonly'=>TRUE,'class'=>'span1 tanggal isRequired'))."</td>
                    </tr>
                    ";
                    $idPermintaanPembelian=$tampilDetail['permintaanpembelian_id'];
           endforeach;
           $modPermintaanPembelian=PermintaanpembelianT::model()->findByPk($idPermintaanPembelian);
           $data['tr']=$tr;
           $data['supplier_id']=$modPermintaanPembelian->supplier_id;
           $data['syaratbayar_id']=$modPermintaanPembelian->syaratbayar_id;
           $data['permintaanpembelian_id']=$modPermintaanPembelian->permintaanpembelian_id;
           if($modPermintaanPembelian->istermasukppn==TRUE){
               $data['isPPN']='1';
           }else{
               $data['isPPN']='0';
           }
           
           if($modPermintaanPembelian->istermasukpph==TRUE){
               $data['isPPH']='1';
           }else{
               $data['isPPH']='0';
           }
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetObatAlkesDariPerencanaanPermintaanPenwaran()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idPerencanaan=$_POST['idPerencanaan'];
            $modPenawaranDetail = new PenawarandetailT;
            $modRencanaKebFarmasiDetail = RencdetailkebT::model()->findAllByAttributes(array('rencanakebfarmasi_id'=>$idPerencanaan));
            $tr = '';
            foreach ($modRencanaKebFarmasiDetail AS $tampilDetail):
                $idObat=$tampilDetail['obatalkes_id'];
                $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
                $modStokObatAlkes=StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$idObat));
                foreach($modStokObatAlkes AS $tampil):
                    $stokAkhir = $stokAkhir + $tampil['qtystok_in'];
                    $maxStok = $maxStok + $tampil['qtystok_out'];
                endforeach;
                
                    $tr .="<tr>
                        <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))."</td>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modPenawaranDetail,'satuankecil_id[]',array('value'=>$modObatAlkes->satuankecil_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'satuanbesar_id[]',array('value'=>$modObatAlkes->satuanbesar_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'sumberdana_id[]',array('value'=>$modObatAlkes->sumberdana_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'obatalkes_id[]',array('value'=>$modObatAlkes->obatalkes_id)).
                       "</td>
                        <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                        <td>".$modObatAlkes->satuankecil->satuankecil_nama."/<br/>".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                        <td>".$stokAkhir."</td>
                        <td>".CHtml::activetextField($modPenawaranDetail,'qty[]',array('value'=>$tampilDetail->jmlpermintaan,'class'=>'span1 numberOnly','onkeyup'=>'numberOnly(this);hitungSubtotal(this)','readonly'=>true))."</td>
                        <td>".CHtml::activetextField($modPenawaranDetail,'harganetto[]',array('value'=>$tampilDetail->harganettorenc,'class'=>'span1 numberOnly netto','readonly'=>TRUE))."</td>
                        <td>".CHtml::textField('subtotal',$tampilDetail->jmlpermintaan * $tampilDetail->harganettorenc,array('class'=>'span2 subTotal','readonly'=>TRUE))."</td>
                      </tr>
                    ";
           endforeach;

           $data['tr'] = $tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    /**
     * actionGetObatAlkesPermintaanPenawaran untuk:
     * 1. Transaksi Permintaan Penawaran (Gudang Farmasi)
     */
    public function actionGetObatAlkesPermintaanPenawaran()
    {
        
       if(Yii::app()->request->isAjaxRequest) {
            $idObat=$_POST['idObat'];
            $qtyObat = $_POST['qtyObat'];
            $idSupplier = $_POST['idSupplier'];
            $modPenawaranDetail = new PenawarandetailT;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            $modStokObatAlkes=StokobatalkesT::model()->findAll('obatalkes_id='.$idObat.'');
            $obatSupplier = new ObatsupplierM;
            if(!empty($idSupplier)){
                $criteria = new CDbCriteria();
                $criteria->compare('obatalkes_id',$idObat);
                $criteria->compare('supplier_id',$idSupplier);
                $obatSupplier = ObatsupplierM::model()->find($criteria);
            }else{
                //Dikosongkan karena input manual
                $obatSupplier->hargabelibesar = 0;
            }
            $stokAkhir = 0;
            $maxStok = 0;
            foreach($modStokObatAlkes AS $tampil):
//                $stokAkhir+=$tampil['qtystok_in'];
                $maxStok+=$tampil['qtystok_out'];
                $stokAkhir+=$tampil['qtystok_current'];
            endforeach;
            $jmlKemasan = (!empty($modObatAlkes->kemasanbesar)) ? $modObatAlkes->kemasanbesar : 1;
            $stokAkhir = $stokAkhir;
//            $hargaJual = ($obatSupplier->hargajual > 0) ? $obatSupplier->hargajual : $modObatAlkes->hargajual;
//            $hargaNetto = ($modObatAlkes->harganetto > 0) ? $modObatAlkes->harganetto : $hargaJual;
//            $harganetto = ($obatSupplier->harganetto > 0) ? $obatSupplier->harganetto : $hargaNetto;
//            $subTotal = $qtyObat * $harganetto;
//            $hargabelikecil = $obatSupplier->hargabelikecil;
            if($obatSupplier->hargabelibesar > 0){
                $hargabelibesar = number_format(($obatSupplier->hargabelibesar > 0 ) ? $obatSupplier->hargabelibesar : $obatSupplier->hargabelikecil * $jmlKemasan,0,'','');
                $hargabelikecil = $hargabelibesar / $jmlKemasan;
            }else{
                $hargabelibesar = 0;
                $hargabelikecil = 0;
            }
            $subTotal = $qtyObat * $hargabelibesar;
                $tr="<tr>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']satuanbesar_id',array('value'=>$modObatAlkes->satuanbesar_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)).
                              CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obatAlkes')).
                       "</td>
                        <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                        <td>".$stokAkhir." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                        <td>".CHtml::activetextField($modPenawaranDetail,'['.$idObat.']jmlkemasan',array('readonly'=>true,'value'=>$jmlKemasan,'class'=>'span1 integer'))
                        ." ".$modObatAlkes->satuankecil->satuankecil_nama."/".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                        <td>".CHtml::activetextField($modPenawaranDetail,'['.$idObat.']qty',array('readonly'=>false,'value'=>$qtyObat,'class'=>'span1 qty integer','onkeyup'=>'hitungSubtotal(this);'))
                        ." ".$modObatAlkes->satuanbesar->satuanbesar_nama
                        .CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']hargabelibesar',array('value'=>$hargabelibesar,'class'=>'span1 integer','readonly'=>false, 'onkeyup'=>'hitungHargaNetto(this); hitungSubtotal(this);'))
                        .CHtml::activeHiddenField($modPenawaranDetail,'['.$idObat.']harganetto',array('value'=>$hargabelikecil,'class'=>'span1 netto','readonly'=>TRUE))
                        .CHtml::hiddenField('subtotal',$subTotal,array('class'=>'span2 subTotal','readonly'=>TRUE))."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove_obat(this);hitungSemuaTotal();return false;','style'=>'text-decoration:none;'))."</td>
                      </tr>
                    ";
            
           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }

    /**
     * ajax url to get obat alkes with stok
     * used in gudangFarmasi/RencanaKebutuhan/rencanaKebutuhan
     */
    public function actionGetObatAlkes()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idObat=$_POST['idObat'];
            $qtyObat = $_POST['qtyObat'];
            $qtyKemasan = $_POST['qtyKemasan'];
            $modRencanaDetailKeb=new RencdetailkebT;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            $modStokObatAlkes=StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$idObat));
            $stokAkhir = 0;
            $maxStok = 0;
            foreach($modStokObatAlkes AS $i => $attr):
                $stokAkhir+=$attr->qtystok_current;
                $maxStok+=$attr->qtystok_out;
            endforeach;
//            $jmlKemasan = ($qtyKemasan > 0) ? $qtyKemasan : 1;
            $jmlKemasan = ($modObatAlkes->kemasanbesar > 0) ? $modObatAlkes->kemasanbesar : 1;;
//            $stokAkhir = $stokAkhir / $jmlKemasan;
            $stokAkhir = $stokAkhir;
                $tr="<tr>
                        <td>". CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obatAlkes')).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']satuanbesar_id',array('value'=>$modObatAlkes->satuanbesar_id)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']harganettorenc',array('value'=>$modObatAlkes->harganetto)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']stokakhir',array('value'=>$stokAkhir)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']maksimalstok',array('value'=>$maxStok)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']minimalstok',array('value'=>$modObatAlkes->minimalstok)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']tglkadaluarsa',array('value'=>$modObatAlkes->tglkadaluarsa)).
                               CHtml::activeHiddenField($modRencanaDetailKeb,'['.$idObat.']jmlkemasan',array('value'=>$jmlKemasan)).
                       "</td>
                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                        <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".$modObatAlkes->tglkadaluarsa."</td>
                        <td>".CHtml::activeTextField($modRencanaDetailKeb,'['.$idObat.']jmlkemasan',array('value'=>$jmlKemasan,'class'=>'span1 numbersOnly','readonly'=>true))
                        ." ".$modObatAlkes->satuankecil->satuankecil_nama."/".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                        <td>".CHtml::activetextField($modRencanaDetailKeb,'['.$idObat.']jmlpermintaan',array('value'=>$qtyObat,'class'=>'span1 numbersOnly permintaan','readonly'=>false))
                        ." ".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                        <td>".$stokAkhir." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                        <td>".$modObatAlkes->minimalstok."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'','onclick'=>'remove_row(this);return false;','style'=>'text-decoration:none;'))."</td>
                     </tr>
                    ";
                //                        <td>".$maxStok."</td>
           $data['tr']=$tr;
           echo json_encode($data);
           Yii::app()->end();
        }
    }
    /**
     * actionGetObatAlkesPembelian untuk:
     * 1. Transaksi Permintaan Pembelian (Gudang Farmasi)
     */
    public function actionGetObatAlkesPembelian()
    {
        
        if(Yii::app()->request->isAjaxRequest)
		{
            $idObat = $_POST['idObat'];
            $qtyObat = $_POST['qtyObat'];
			$idSupplier = $_POST['idSupplier'];
			$format = new CustomFormat();

			$modObatAlkes = ObatalkesM::model()->findByPk($idObat);
            $modPermintaanDetail = new PermintaandetailT;
            $modPermintaanDetail->jmlpermintaan = $qtyObat;
            $modPermintaanDetail->jmlkemasan = $modObatAlkes->kemasanbesar;
            $modPermintaanDetail->harganettoper = $modObatAlkes->harganetto;
            $modPermintaanDetail->obatalkes_id = $modObatAlkes->obatalkes_id;
            $modPermintaanDetail->sumberdana_id = $modObatAlkes->sumberdana_id;
            $modPermintaanDetail->satuanbesar_id = $modObatAlkes->satuanbesar_id;
            $modPermintaanDetail->satuankecil_id = $modObatAlkes->satuankecil_id;
            $modPermintaanDetail->stokakhir = 0;
			
            $maxStok = StokobatalkesT::MaxStokInOut($modObatAlkes->obatalkes_id, Yii::app()->user->getState('ruangan_id'), 'out');
			$modPermintaanDetail->maksimalstok = $maxStok;
            $modPermintaanDetail->minimalstok = $modObatAlkes->minimalstok;
            $modPermintaanDetail->persendiscount = $modObatAlkes->discount;
			$total_harga = (($modPermintaanDetail->jmlkemasan * $modObatAlkes->harganetto) * $modPermintaanDetail->jmlpermintaan);
			
			$jmldiscount = 0;
			$jml_discount_satuan = 0;
			if(!is_null($modObatAlkes->discount) && $modObatAlkes->discount > 0)
			{
				$jmldiscount = $total_harga * ($modObatAlkes->discount / 100);
				$jml_discount_satuan = $modObatAlkes->harganetto * ($modObatAlkes->discount / 100);
			}
            $modPermintaanDetail->jmldiscount = $jmldiscount;
			
			$hargappnper = 0;
			$harga_ppn_satuan = 0;
			if(!is_null($modObatAlkes->ppn_persen) && $modObatAlkes->ppn_persen > 0)
			{
				$hargappnper = ($total_harga - $jmldiscount) * ($modObatAlkes->ppn_persen / 100);
				$harga_ppn_satuan = ($modObatAlkes->harganetto - $jml_discount_satuan) * ($modObatAlkes->ppn_persen / 100);
			}
            $modPermintaanDetail->hargappnper = $hargappnper;
            $modPermintaanDetail->hargapphper = 0;
            $modPermintaanDetail->hargasatuanper = ($modObatAlkes->harganetto - $jml_discount_satuan) + $harga_ppn_satuan;
			
			$modObatAlkes->tglkadaluarsa = $format->formatDateTimeMediumForDB(
				Yii::app()->dateFormatter->formatDateTime($modObatAlkes->tglkadaluarsa)
			);
            $modPermintaanDetail->tglkadaluarsa = $modObatAlkes->tglkadaluarsa;
            $modPermintaanDetail->biaya_lainlain = 0;
            $modPermintaanDetail->hargabelibesar = $modObatAlkes->harga_besar;
			
			$data['id_obat'] = $modObatAlkes->obatalkes_id;
			$data['tr'] = $this->renderPartial('_formObatAlkesPembelian',
				array(
					'modPermintaanDetail'=>$modPermintaanDetail,
					'modObatAlkes'=>$modObatAlkes
				), true
			);
			
			echo json_encode($data);
			Yii::app()->end();
        }
    }
    
	public function actionGetObatAlkesDariPerencanaan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idPerencanaan=$_POST['idPerencanaan'];
            $modPermintaanDetail=new PermintaandetailT;
            $modRencanaKebFarmasiDetail = RencdetailkebT::model()->findAll('rencanakebfarmasi_id='.$idPerencanaan.'');
            foreach ($modRencanaKebFarmasiDetail AS $tampilDetail):
                $idObat=$tampilDetail['obatalkes_id'];
                $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
                $modStokObatAlkes=StokobatalkesT::model()->findAll('obatalkes_id='.$idObat.'');
                $modStokObatAlkes=StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$idObat));
                $stokAkhir = 0;
                $maxStok = 0;
                foreach($modStokObatAlkes AS $i => $attr):
                    $stokAkhir+=$attr->qtystok_current;
                    $maxStok+=$attr->qtystok_out;
                endforeach;
                    $tr .="<tr>
                            <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))."</td>
                            <td> ".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'obatalkes_id[]',array('value'=>$modObatAlkes->obatalkes_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'satuankecil_id[]',array('value'=>$modObatAlkes->satuankecil_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'sumberdana_id[]',array('value'=>$modObatAlkes->sumberdana_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'satuanbesar_id[]',array('value'=>$modObatAlkes->satuanbesar_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'stokakhir[]',array('value'=>$stokAkhir)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'maksimalstok[]',array('value'=>$maxStok)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'minimalstok[]',array('value'=>$modObatAlkes->minimalstok)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'tglkadaluarsa[]',array('value'=>$modObatAlkes->tglkadaluarsa)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'jmlkemasan[]',array('value'=>$modObatAlkes->kemasanbesar)).
                           "</td>
                            <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                            <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'jmlpermintaan[]',array('value'=>1,'class'=>'span1 numberOnly','onkeyup'=>'hitungHargaNetto(this); hitungSubtotal(this);'))."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'harganettoper[]',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 numberOnly netto','readonly'=>TRUE,'onkeyup'=>'numberOnly(this)'))."</td>
                            <td>".$stokAkhir."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'hargappnper[]',array('value'=>0,'class'=>'span1 numberOnly ppn','onkeyup'=>'numberOnlyNol(this);','readonly'=>TRUE))."</td>
                            <td>"./*CHtml::activetextField($modPermintaanDetail,'hargapphper[]',array('value'=>0,'class'=>'span1 numberOnly pph','onkeyup'=>'numberOnlyNol(this);','onchange'=>'onchangePPH();','readonly'=>TRUE)).*/"</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'persendiscount[]',array('value'=>0,'maxlength'=>2,'class'=>'span1 numberOnly persen','onkeyup'=>'hitungJmlDiskon(this); hitungSubtotal(this);'))."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'jmldiscount[]',array('value'=>0,'class'=>'span1 numberOnly jumlah','onkeyup'=>'hitungPersenDiskon(this);'))."</td>
                         </tr>
                        ";
           endforeach;

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetObatAlkesDariPermintaan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idPermintaan=$_POST['idPermintaan'];
            $modPermintaanDetail=new PermintaandetailT;
            $modPenawaranDetail = PenawarandetailT::model()->findAll('permintaanpenawaran_id='.$idPermintaan.'');
            foreach ($modPenawaranDetail AS $tampilDetail):
                $idObat=$tampilDetail['obatalkes_id'];
                $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
                $modStokObatAlkes=StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$idObat));
                $stokAkhir = 0;
                $maxStok = 0;
                foreach($modStokObatAlkes AS $i => $attr):
                    $stokAkhir+=$attr->qtystok_current;
                    $maxStok+=$attr->qtystok_out;
                endforeach;
                    $tr .="<tr>
                            <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))."</td>
                            <td> ".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'obatalkes_id[]',array('value'=>$modObatAlkes->obatalkes_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'satuankecil_id[]',array('value'=>$modObatAlkes->satuankecil_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'sumberdana_id[]',array('value'=>$modObatAlkes->sumberdana_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'satuanbesar_id[]',array('value'=>$modObatAlkes->satuanbesar_id)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'stokakhir[]',array('value'=>$stokAkhir)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'maksimalstok[]',array('value'=>$maxStok)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'minimalstok[]',array('value'=>$modObatAlkes->minimalstok)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'tglkadaluarsa[]',array('value'=>$modObatAlkes->tglkadaluarsa)).
                                   CHtml::activeHiddenField($modPermintaanDetail,'jmlkemasan[]',array('value'=>$modObatAlkes->kemasanbesar)).
                           "</td>
                            <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                            <td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'jmlpermintaan[]',array('value'=>1,'class'=>'span1 numberOnly','onkeyup'=>'numberOnly(this);hitungJumlahDariQty(this);'))."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'harganettoper[]',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 numberOnly netto','readonly'=>FALSE,'onkeyup'=>'numberOnly(this)'))."</td>
                            <td>".$stokAkhir."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'hargappnper[]',array('value'=>0,'class'=>'span1 currency ppn','onkeyup'=>'numberOnlyNol(this)'))."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'persendiscount[]',array('value'=>0,'maxlength'=>2,'class'=>'span1 currency persen','onkeyup'=>'numberOnlyNol(this);hitungJumlahDariDiskon(this);'))."</td>
                            <td>".CHtml::activetextField($modPermintaanDetail,'jmldiscount[]',array('value'=>0,'class'=>'span1 currency jumlah','onkeyup'=>'numberOnlyNol(this);hitungDiskonDariJumlah(this)'))."</td>
                         </tr>
                        ";
           endforeach;

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    

    public $validRujukan = true;
    public $validPulang = false;

    
    public function actionListKarcis(){
        if(Yii::app()->request->isAjaxRequest) {
                $kelasPelayanan=$_POST['kelasPelayanan'];
                $ruangan = $_POST['ruangan'];
                $form='';
                if(!empty($ruangan)){
                    $karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
                    if ($karcis){
//                        echo "c";exit;
//                        $pasienLama = (($_POST['pasienLama'] > 0) ? false : true);
                        $pasienLama = (isset($_POST['pasienLama']) ? (($_POST['pasienLama'] > 0) ? false : true) : false);
                        if($ruangan == Params::RUANGAN_ID_GIZI){
                            $karcisM = CHtml::listData(KarcisM::model()->findAllByAttributes(array("ruangan_id"=>$ruangan, 'pasienbaru_karcis'=>$pasienLama)),'karcis_id','karcis_id');
                        }else{
                            $karcisM = CHtml::listData(KarcisM::model()->findAllByAttributes(array("ruangan_id"=>$ruangan, 'pasienbaru_karcis'=>$pasienLama)),'karcis_id','karcis_id');
                        }
                        sort($karcisM);
                        if($ruangan == Params::RUANGAN_ID_GIZI){
                            $modelKarcis=(count($karcisM) == 1) ? KarcisM::model()->findByAttributes(array('karcis_id'=>$karcisM[0])) : '';
                        }else{
                            $modelKarcis=(count($karcisM) == 1) ? KarcisV::model()->findByAttributes(array('karcis_id'=>$karcisM[0])) : '';
                        }
                    }
                    
                    if($ruangan == Params::RUANGAN_ID_GIZI){
                        $modKarcisV=KarcisV::model()->findAll('kelaspelayanan_id='.$kelasPelayanan.' AND ruangan_id='.$ruangan.' LIMIT 1');
                        if(empty($modKarcisV)){
                            $modKarcisV=TariftindakanM::model()->findAll('kelaspelayanan_id='.$kelasPelayanan.' AND daftartindakan_id='.$modelKarcis->daftartindakan_id.'');
                            $karcis = KarcisM::model()->findAllByAttributes(array('daftartindakan_id'=>$daftartindakan_id));
                        }
                    }else{
                        $modKarcisV=KarcisV::model()->findAll('kelaspelayanan_id='.$kelasPelayanan.' AND ruangan_id='.$ruangan.'');
                    }
                    
                    foreach($modKarcisV AS $tampil){
                        if ($karcis){
                            if($ruangan == Params::RUANGAN_ID_LAB)
                            {
                                $form .='<tr>
                                        <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                            <i class="icon-check"></i>
                                            </a></td>
                                        <td>'.$tampil['karcis_nama'].'</td>
                                        <td>'.CHtml::hiddenField('tarifKarcis', $tampil['harga_tariftindakan']).$tampil['harga_tariftindakan'].'</td>
                                     </tr>';
                            }else if($ruangan == Params::RUANGAN_ID_RAD){
                                $form .='<tr>
                                        <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                            <i class="icon-check"></i>
                                            </a></td>
                                        <td>'.$tampil['karcis_nama'].'</td>
                                        <td>'.CHtml::hiddenField('tarifKarcis', $tampil['harga_tariftindakan']).$tampil['harga_tariftindakan'].'</td>
                                     </tr>';
                            }else if($ruangan == Params::RUANGAN_ID_GIZI){
                                $form .='<tr>
                                        <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                            <i class="icon-check"></i>
                                            </a></td>
                                        <td>'.$tampil['karcis_nama'].'</td>
                                        <td>'.CHtml::hiddenField('tarifKarcis', $tampil['harga_tariftindakan']).$tampil['harga_tariftindakan'].'</td>
                                     </tr>';
                            }else{
                                if (in_array($tampil['karcis_id'], $karcisM))
                                {
                                    $form .='<tr>
                                            <td>'.$tampil['karcis_nama'].'</td>
                                            <td>'.CHtml::hiddenField('tarifKarcis', $tampil['harga_tariftindakan']).$tampil['harga_tariftindakan'].'</td>
                                            <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                                <i class="icon-check"></i>
                                                </a></td>
                                         </tr>';
                                }else{
                                    $form .='<tr>
                                            <td>'.$tampil['karcis_nama'].'</td>
                                            <td>'.CHtml::hiddenField('tarifKarcis', $tampil['harga_tariftindakan']).$tampil['harga_tariftindakan'].'</td>
                                            <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                                <i class="icon-check"></i>
                                                </a></td>
                                         </tr>';
                                }
                            }
                        }else{
                            $form .='<tr>
                                    <td>'.$tampil['karcis_nama'].'</td>
                                    <td>'.$tampil['harga_tariftindakan'].'</td>
                                    <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                        <i class="icon-check"></i>
                                        </a></td>
                                 </tr>';
                        }
                        
                    }
                }
            $data['karcis']=(count($modelKarcis) == 1) ? ((isset($modelKarcis->attributes))? $modelKarcis->attributes : 0 ) : 0;
            $data['form']=$form;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionListKarcisRadLab()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $kelasPelayanan = $_POST['kelasPelayanan'];
            $ruangan = $_POST['ruangan'];
            $form='';
            if(!empty($ruangan)){
                $modKarcisV = KarcisV::model()->findAll(
                    'kelaspelayanan_id='.$kelasPelayanan.' AND ruangan_id='.$ruangan.''
                );
                
                $data_karcis = null;
                foreach($modKarcisV AS $tampil)
                {
                    $form .='<tr>
                            <td><a data-karcis="'.$tampil['karcis_id'].'"id="'. ($ruangan == 18 ? 'rec_karcisLab' : 'rec_karcisRad') .'" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                <i class="icon-check"></i>
                                </a></td>
                            <td>'.$tampil['karcis_nama'].'</td>
                            <td>'.
                                CHtml::textField(($ruangan == 18 ? 'tarifKarcisLab' : 'tarifKarcisRad'), $tampil['harga_tariftindakan'], array('onkeyup'=>'hitungTotalSeluruh()','class'=>'span2 currency')) .
                                CHtml::checkBox(($ruangan == 18 ? 'isAdminLab' : 'isAdminRad'),false, array('style'=>'display:none')).
                                CHtml::hiddenField('[daftartindakan_id]',Params::TARIF_ADMINISTRASI_LAB_ID, array('readonly'=>'true')).
                            '</td>
                         </tr>';
                    $data_karcis = array(
                        "karcis_id" => $tampil['karcis_id'],
                        "daftartindakan_id" => $tampil['daftartindakan_id'],
                        "harga_tariftindakan" => $tampil['harga_tariftindakan']
                    );
                }
            }
            $data['karcis'] = array(
                'panjang' => count($modKarcisV),
                'karcis_attr' => $data_karcis,
            );
            $data['form'] = $form;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionListKarcisRI(){
        if(Yii::app()->request->isAjaxRequest) {
                $kelasPelayanan=$_POST['kelasPelayanan'];
                $ruangan=$_POST['ruangan'];
                $form='';
                if(!empty($ruangan)){
                    $modKarcisV=KarcisV::model()->findAll('kelaspelayanan_id='.$kelasPelayanan.' AND ruangan_id='.$ruangan.'');
                    foreach($modKarcisV AS $tampil){
                        if ($karcis){
                            if (in_array($tampil['karcis_id'],$karcisM)){
                                $form .='<tr>
                                        <td>'.$tampil['karcis_nama'].'</td>
                                        <td>'.$tampil['harga_tariftindakan'].'</td>
                                        <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                            <i class="icon-check"></i>
                                            </a></td>
                                     </tr>';
                            }
                        }else{
                            $form .='<tr>
                                    <td>'.$tampil['karcis_nama'].'</td>
                                    <td>'.$tampil['harga_tariftindakan'].'</td>
                                    <td><a data-karcis="'.$tampil['karcis_id'].'"id="selectPasien" class="btn-small" href="javascript:void(0);" onclick="changeBackground(this,'.$tampil['daftartindakan_id'].','.$tampil['harga_tariftindakan'].','.$tampil['karcis_id'].');return false;">
                                        <i class="icon-check"></i>
                                        </a></td>
                                 </tr>';
                        }
                        
                    }
                }
                $data['karcis']=(count($modelKarcis) == 1) ? ((isset($modelKarcis->attributes))? $modelKarcis->attributes : 0 ) : 0;
                $data['form']=$form;
               echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetKodeObatAlkes() {
       if(Yii::app()->request->isAjaxRequest) {
        $tahun=date('Y');
        $hurufAwalObat=substr($_POST['namaObat'],0,1);
        $sql = "SELECT CAST(MAX(SUBSTR(obatalkes_kode,6,6)) AS integer) nokodeobat FROM obatalkes_m
                WHERE SUBSTR(obatalkes_kode,2,4) LIKE '".$tahun."'";
        $kodeObat = Yii::app()->db->createCommand($sql)->queryRow();
        $data['kodeObatBaru'] =strtoupper($hurufAwalObat).$tahun.(str_pad($kodeObat['nokodeobat']+1, 6, 0,STR_PAD_LEFT));
         echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetMetodeGCS(){
           if(Yii::app()->request->isAjaxRequest) {
         $gcs_eye=$_POST['gcs_eye'];
         $gcs_motorik=$_POST['gcs_motorik'];
         $gcs_verbal=$_POST['gcs_verbal'];
         
         $jumlah = $gcs_eye + $gcs_motorik+$gcs_verbal;
         
         $namaGCS=GcsM::model()->find(''.$jumlah.'>=gcs_nilaimin AND '.$jumlah.'<=gcs_nilaimax AND gcs_aktif=TRUE');
         if(COUNT($namaGCS)>0){//Jika Nilai GCSnya ada
         $data['idGCS']=$namaGCS->gcs_id;
         $data['namaGCS']=$namaGCS->gcs_nama;
         }else{
             $data['pesan']='Nilai GCS Tidak Ditemukan';
         }
         echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetPemeriksaanLAB(){

        if(Yii::app()->request->isAjaxRequest) {
        $idJenisPemeriksaan=$_POST['idJenisPemeriksaan'];
        $periksaLAB=PemeriksaanlabM::model()->findAll('jenispemeriksaanlab_id='.$idJenisPemeriksaan.'
            AND pemeriksaanlab_aktif=TRUE ORDER BY pemeriksaanlab_nama');
        foreach ($periksaLAB as $dataPerisaLAB) {
            $pemeriksaan .=CHtml::checkBox("pemeriksaanLab[]", false, array('value'=>$dataPerisaLAB['pemeriksaanlab_id'],
                                                                                          'onclick' => "inputperiksa(this)"));
            $pemeriksaan .="<span>".$dataPerisaLAB['pemeriksaanlab_nama']."</span><br/>";
        }
         $data['pemeriksaan']=$pemeriksaan;
         echo json_encode($data);
         Yii::app()->end();
        }
 
    }
    
    public function actionGetRiwayatRuangan() {
         if(Yii::app()->request->isAjaxRequest) {
        $instalasi_id=$_POST['instalasi_id'];
        $sql="SELECT
              riwayatruangan_r.tglpenetapanruangan,
              riwayatruangan_r.nopenetapanruangan,
              riwayatruangan_r.tentangpenetapan,
              instalasi_m.instalasi_id,
              instalasi_m.instalasi_nama
            FROM
              public.instalasi_m,
              public.riwayatruangan_r
            WHERE
              instalasi_m.riwayatruangan_id = riwayatruangan_r.riwayatruangan_id
              AND instalasi_m.instalasi_id=".$instalasi_id."";
        $riwayatRuangan=Yii::app()->db->createCommand($sql)->query();
        foreach($riwayatRuangan AS $tampil):
            $data['tglpenetapanruangan']=$tampil['tglpenetapanruangan'];
            $data['nopenetapanruangan']=$tampil['nopenetapanruangan'];
            $data['tentangpenetapan']=$tampil['tentangpenetapan'];
            
        endforeach;

        echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionAjaxSessionPhoto() {
        if(Yii::app()->request->isAjaxRequest) {
        $photo=Yii::app()->session['temporaryFilePhoto'];
        
        $data['photo']=Params::urlPegawaiDirectory().$photo;
        
        echo json_encode($data);
         Yii::app()->end();
        }
    }
    public function actionGetKamarRuangan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $ruangan_id = $_POST['ruangan_id'];
            $ruangan = KamarruanganM::model()->findAll('ruangan_id='.$ruangan_id.'ORDER BY kamarruangan_nokamar');
            $dropDown='<select name="">';
            if(empty($ruangan))
                {
                    $dropDown='<option>';
                }
         echo json_encode($data);
         Yii::app()->end();
        }
        
    }
    
    
    
    
    public function actionRuanganberdasarkanRM()
    {
         if (Yii::app()->getRequest()->getIsAjaxRequest())
         {
// ================================cari Pendaftaran terakhir========================================
             $no_rekam_medik=$_POST['no_rekam_medik'];
             $criteria=new CDbCriteria;
             $criteria->compare('LOWER(pasien.no_rekam_medik)',strtolower($no_rekam_medik),true);
             $criteria->addCondition('tgl_pendaftaran BETWEEN \''.date('Y-m-d').' 00:00:00\' AND \''.date('Y-m-d H:i:s').'\'');
             $criteria->addCondition('t.instalasi_id='.Params::INSTALASI_ID_RJ.' OR t.instalasi_id='.Params::INSTALASI_ID_RD.'');
             $criteria->with=array('pasien','ruangan','instalasi');
             $criteria->order='tgl_pendaftaran DESC';
             $criteria->limit=1;
             
             $pendaftaran=PendaftaranT::model()->find($criteria);
             if(COUNT($pendaftaran)<1)
                {
                
                 $data['data_pesan']='Pasien Belum Mendaftar di Rawat
                                      Jalan ataupun Rawat Darurat Pada hari Ini';
                }
             else
                {
                    $data['pendaftaran_id']=$pendaftaran->pendaftaran_id;
                    $data['data_pesan']='Pasien Terdaftar di '.$pendaftaran->instalasi->instalasi_nama.'<br>
                                         Ruangan :<b>'.$pendaftaran->ruangan->ruangan_nama.' </b>
                                         <br>Tanggal Pendaftran :<b>'.$pendaftaran->tgl_pendaftaran.'</b>';
                }
//==================================Akhir Cari DiPendaftaran==========================================
                
//==================================Cari Di Pasien Admisi=============================================
             $criteria=new CDbCriteria;
             $criteria->compare('LOWER(pasien.no_rekam_medik)',  strtolower($no_rekam_medik),true);
             $criteria->with=array('pasien','ruangan');
             $criteria->addCondition('statuskeluar=FALSE');
             $pasienAdmisi=PasienadmisiT::model()->find($criteria);
             if(COUNT($pasienAdmisi)<1)
                {
                 $data['cek']=null;
                 $data['data_pesan'] .='<br>Pasien Belum terdaftar Dirawat Inap';
                }
             else
                {
                    $data['pasien_id']=$pasienAdmisi->pasien_id;
                    $data['kelaspelayanan_id']=$pasienAdmisi->kelaspelayanan_id;
                    $data['ruangan_id']=$pasienAdmisi->ruangan_id;
                    $data['kamarruangan_id']=$pasienAdmisi->kamarruangan_id;
                    $data['pasienadmisi_id']=$pasienAdmisi->pasienadmisi_id;
                    $data['data_pesan'] .='<br>Pasien sudah Terdaftar di Rawat Inap<br>
                                           Ruangan : <b>'.$pasienAdmisi->ruangan->ruangan_nama.'</b>';
                }
//======================================Akhir Cari Di Pasien Admisi
               echo json_encode($data);
               Yii::app()->end();
         }

    }
    
    /**
     * method untuk pindah ruangan poli klinik
     * digunakan di :
     * 1. Pendaftaran Penjadwalan -> informasi Rawat Jalan -> poliklinik + jenis kasuspenyakit
     * 2. Pendaftaran Penjadwalan -> informasi Rawat Darurat -> ruangan + jenis kasus penyakit
     */
    public function actionSaveRuanganBaru()
    {
            $konfigSys = KonfigsystemK::model()->find();
            $pendaftaran_id = $_POST['pendaftaran_id'];
            $pasien_id = $_POST['pasien_id'];
            $ruangan_id = $_POST['ruangan_id'];
            $jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'];
            $alasan = $_POST['alasan'];
            $ruangan_awal = $_POST['ruangan_awal'];
            $modPasien = PasienM::model()->findByPk($pasien_id);
            $model = PendaftaranT::model()->findByPk($pendaftaran_id);
            $modRiwayat = new UbahruanganR;
            $modRiwayat->ruanganawal_id = $ruangan_awal;
            $modRiwayat->menjadiruangan_id = $ruangan_id;
            $modRiwayat->alasanperubahan = $alasan;
            $modRiwayat->pendaftaran_id = $pendaftaran_id;
            $modRiwayat->tglperubahan = date('Y-m-d');
            $modRiwayat->pasien_id = $pasien_id;
            $data = array();
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $success = false;
                if($modRiwayat->validate()){
                    if(isset($_POST['pasienadmisi_id'])){
                        if(PasienadmisiT::model()->updateByPk ($_POST['pasienadmisi_id'], array('ruangan_id'=>$ruangan_id))){
                            $update = true;
                            $success = true;
                            /* */
//                            $findTindakans = TindakanpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
//                            if(count($findTindakans) > 0){
//                                foreach($findTindakans AS $i => $updateTindakan){
                                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                                    if($konfigSys->isdeljurnaltransaksi == true){
//                                        if(isset($updateTindakan->jurnalrekening_id)){
//                                            $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                                            $updateTindakan->jurnalrekening_id = null;
//                                            $updateTindakan->save();
//                                            // hapus jurnaldetail_t dan jurnalrekening_t
//                                            if($jurnalrekId){
//                                                JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                                JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                            $deleteTindakans =  TindakanpelayananT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id,));
                            if($deleteTindakans)
                            {
                                 $success = $this->saveTindakanPelayanan($modPasien, $model);
                            }
                            /* *
                             */
                        }
                    } else {
                        if(PendaftaranT::model()->updateByPk($pendaftaran_id,array('ruangan_id'=>$ruangan_id,'jeniskasuspenyakit_id'=>$jeniskasuspenyakit_id))){
                            $model->ruangan_id = $ruangan_id;
                            $update = true;
                            $success = true;
                            /* */
//                            $findTindakans = TindakanpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
//                            if(count($findTindakans) > 0){
//                                foreach($findTindakans AS $i => $updateTindakan){
                                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                                    if($konfigSys->isdeljurnaltransaksi == true){
//                                        if(isset($updateTindakan->jurnalrekening_id)){
//                                            $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                                            $updateTindakan->jurnalrekening_id = null;
//                                            $updateTindakan->save();
//                                            // hapus jurnaldetail_t dan jurnalrekening_t
//                                            if($jurnalrekId){
//                                                JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                                JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                                            }
//                                        }
//                                    }
//                                }
//                            }
                            $deleteTindakans = TindakanpelayananT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
                            if($deleteTindakans)
                            {
                                $success = $this->saveTindakanPelayanan($modPasien, $model);
                                $data['status'] = 'OK';
                            }else{
                                $data['status'] = 'Gagal Admisi';
                            }
                             /**
                             */
                        }

                    }
                    
                    if($update && $success){
                        if($modRiwayat->save()){
                            $data['status'] = 'OK';
                        }else{
                            $success = false;
                            $data['status'] = 'Gagal';
                        }
                    } else {
                        $success = false;
                        $data['status'] = 'Gagal';
                    }
                    
                } else {
                    $data['status'] = 'Gagal';
                    echo print_r($modRiwayat->errors,1);
                }
                
                if ($success){
                    $transaction->commit();
                }else{
                    $transaction->rollback();
                }
                
            } catch (Exception $exc) {
//                $data['status'] = 'Gagal';
                $data['status'] = MyExceptionMessage::getMessage($exc,true);
                $transaction->rollback();
            }

            echo CJSON::encode($data);
            Yii::app()->end();
      
    }
    
    public function actionGetRuanganPasien()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
         {
            $pendaftaran_id=$_POST['pendaftaran_id'];
            $ruangan_id=$_POST['ruangan_id'];
            $instalasi_id=$_POST['instalasi_id'];

            if(isset($_POST['jeniskasuspenyakit_id'])){
                $jeniskasuspenyakit_id=$_POST['jeniskasuspenyakit_id'];
                $criteria=new CDbCriteria;
                $criteria->select ='t.ruangan_id, t.jeniskasuspenyakit_id, ruangan_m.ruangan_nama, jeniskasuspenyakit_m.jeniskasuspenyakit_nama,
                                    jeniskasuspenyakit_aktif';
		$criteria->compare('t.ruangan_id',$ruangan_id);
                if(!empty($jeniskasuspenyakit_id)){
                    $criteria->compare('t.jeniskasuspenyakit_id',$jeniskasuspenyakit_id);
                }
                $criteria->addCondition('jeniskasuspenyakit_m.jeniskasuspenyakit_aktif is true');
                $criteria->join = 'LEFT JOIN ruangan_m on t.ruangan_id = ruangan_m.ruangan_id
                                   LEFT JOIN jeniskasuspenyakit_m on t.jeniskasuspenyakit_id = jeniskasuspenyakit_m.jeniskasuspenyakit_id
                                    ';
                $dataJenisPenyakit =KasuspenyakitruanganM::model()->findAll($criteria);
//                $dataJenisPenyakit =KasuspenyakitruanganM::model()->findAll('jeniskasuspenyakit_id='.$jeniskasuspenyakit_id.' AND jeniskasuspenyakit_aktif=TRUE ORDER BY jeniskasuspenyakit_nama');

                  foreach($dataJenisPenyakit AS $jenisPenyakit){
                      if($jenisPenyakit['jeniskasuspenyakit_id']==$jeniskasuspenyakit_id)
                         {
                               $jenisKasusPenyakit .='<option value="'.$jenisPenyakit['jeniskasuspenyakit_id'].'" selected="selected">'.$jenisPenyakit['jeniskasuspenyakit_nama'].'</option>';
                         }
                     else
                          {
                               $jenisKasusPenyakit .='<option value="'.$jenisPenyakit['jeniskasuspenyakit_id'].'">'.$jenisPenyakit['jeniskasuspenyakit_nama'].'</option>';
                          }
                  }
                $data['jenisKasusPenyakit']=$jenisKasusPenyakit;
            }

            $dropDown='';
            $dataRuangan =RuanganM::model()->findAll('instalasi_id='.$instalasi_id.' AND ruangan_aktif=TRUE ORDER BY ruangan_nama');
            foreach ($dataRuangan AS $tampilRuangan)
            {
               if($tampilRuangan['ruangan_id']==$ruangan_id)
                   {
                         $dropDown .='<option value="'.$tampilRuangan['ruangan_id'].'" selected="selected" onchange="getKasusPenyakit('.$ruangan_id.')">'.$tampilRuangan['ruangan_nama'].'</option>';
                   }
               else
                    {
                         $dropDown .='<option value="'.$tampilRuangan['ruangan_id'].'" onchange="return getKasusPenyakit('.$ruangan_id.')">'.$tampilRuangan['ruangan_nama'].'</option>';
                    }

            }
               $data['dropDown']=$dropDown;
               echo json_encode($data);
               Yii::app()->end();
         }
    }
    
    // -- ruangan rawat darurat -- //
    public function actionGetRuanganPasienRD()
    {
        if (Yii::app()->getRequest()->getIsAjaxRequest())
         {
            $pendaftaran_id=$_POST['pendaftaran_id'];
            $ruangan_id=$_POST['ruangan_id'];
            $instalasi_id=$_POST['instalasi_id'];

            if(isset($_POST['jeniskasuspenyakit_id'])){
                $jeniskasuspenyakit_id=$_POST['jeniskasuspenyakit_id'];
                $criteria=new CDbCriteria;
                $criteria->select ='t.ruangan_id, t.jeniskasuspenyakit_id, ruangan_m.ruangan_nama, jeniskasuspenyakit_m.jeniskasuspenyakit_nama,
                                    jeniskasuspenyakit_aktif';
		$criteria->compare('t.ruangan_id',$ruangan_id);
                if(!empty($jeniskasuspenyakit_id)){
                    $criteria->compare('t.jeniskasuspenyakit_id',$jeniskasuspenyakit_id);
                }
                $criteria->addCondition('jeniskasuspenyakit_m.jeniskasuspenyakit_aktif is true');
                $criteria->join = 'LEFT JOIN ruangan_m on t.ruangan_id = ruangan_m.ruangan_id
                                   LEFT JOIN jeniskasuspenyakit_m on t.jeniskasuspenyakit_id = jeniskasuspenyakit_m.jeniskasuspenyakit_id
                                    ';
                $dataJenisPenyakit =KasuspenyakitruanganM::model()->findAll($criteria);
//                $dataJenisPenyakit =KasuspenyakitruanganM::model()->findAll('jeniskasuspenyakit_id='.$jeniskasuspenyakit_id.' AND jeniskasuspenyakit_aktif=TRUE ORDER BY jeniskasuspenyakit_nama');

                  foreach($dataJenisPenyakit AS $jenisPenyakit){
                      if($jenisPenyakit['jeniskasuspenyakit_id']==$jeniskasuspenyakit_id)
                         {
                               $jenisKasusPenyakit .='<option value="'.$jenisPenyakit['jeniskasuspenyakit_id'].'" selected="selected">'.$jenisPenyakit['jeniskasuspenyakit_nama'].'</option>';
                         }
                     else
                          {
                               $jenisKasusPenyakit .='<option value="'.$jenisPenyakit['jeniskasuspenyakit_id'].'">'.$jenisPenyakit['jeniskasuspenyakit_nama'].'</option>';
                          }
                  }
                $data['jenisKasusPenyakit']=$jenisKasusPenyakit;
            }

            $dropDown='';
                $criteria2=new CDbCriteria;
                $criteria2->select ='t.ruangan_id, t.instalasi_id, ruangan_m.ruangan_nama, instalasi_m.instalasi_nama,
                                    ruangan_m.ruangan_aktif';
//		$criteria2->compare('t.instalasi_id',$instalasi_id);
		$criteria2->addCondition('t.instalasi_id in (3,14)');
//                if(!empty($ruangan_id)){
//                    $criteria2->compare('t.ruangan_id',$ruangan_id);
//                }
                $criteria2->addCondition('ruangan_m.ruangan_aktif is true');
                $criteria2->join = 'LEFT JOIN ruangan_m on t.ruangan_id = ruangan_m.ruangan_id
                                   LEFT JOIN instalasi_m on t.instalasi_id = instalasi_m.instalasi_id
                                    ';
            $dataRuangan =InstalasipenddaruratV::model()->findAll($criteria2);
//            $dataRuangan =InstalasipenddaruratV::model()->findAll('instalasi_id='.$instalasi_id.' AND ruangan_aktif=TRUE ORDER BY ruangan_nama');
            foreach ($dataRuangan AS $tampilRuangan)
            {
               if($tampilRuangan['ruangan_id']==$ruangan_id)
                   {
                         $dropDown .='<option value="'.$tampilRuangan['ruangan_id'].'" selected="selected" onchange="getKasusPenyakit('.$ruangan_id.')">'.$tampilRuangan['ruangan_nama'].'</option>';
                   }
               else
                    {
                         $dropDown .='<option value="'.$tampilRuangan['ruangan_id'].'" onchange="return getKasusPenyakit('.$ruangan_id.')">'.$tampilRuangan['ruangan_nama'].'</option>';
                    }

            }
               $data['dropDown']=$dropDown;
               echo json_encode($data);
               Yii::app()->end();
         }
    }
    // -- end ruangan rawat darurat -- //
    
    public function actionGetHari()
    {
          if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                $format = new CustomFormat();
                $tanggalWaktu=$_POST['tanggal'];
                
                $tanggal=trim(substr($tanggalWaktu,0,-8)); //Menampilkan Tanggal Tanpa Jam
                $tanggalDB = $format->formatDateMediumForDB($tanggal);//Mengubah Tanggal inputan ke tanggal database
                $hari=date('l', strtotime($tanggalDB)); //Mendapatkan nilai hari dari tanggal yang dipilih

                 if(strtolower($hari)=='sunday')
                    {
                        $hari='Minggu';
                    }
                 else if(strtolower($hari)=='monday')
                    {
                        $hari='Senin';
                    }
                 else if(strtolower($hari)=='tuesday')
                    {
                        $hari='Selasa';
                    }
                 else if(strtolower($hari)=='wednesday')
                    {
                        $hari='Rabu';
                    }
                 else if(strtolower($hari)=='thursday')
                    {
                        $hari='Kamis';
                    }
                 else if(strtolower($hari)=='friday')
                    {
                        $hari='Jumat';
                    }
                 else if(strtolower($hari)=='saturday')
                    {
                        $hari='Sabtu';
                    }
                $data['hari']=$hari;
                echo json_encode($data);
                Yii::app()->end();
            }
    }
    
    public function actionGetTglLahir()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            // $umur = explode(' ', $_POST['umur']);
            $today = date('Y-m-d');
            if(!empty($_POST['thn'])&&!empty($_POST['bln'])&&!empty($_POST['hr'])){
                // $thn = $umur[0];
                // $bln = $umur[2];
                // $hr = $umur[4];
              
                $thn = $_POST['thn'];
                $bln = $_POST['bln'];
                $hr = $_POST['hr'];

                if($thn=='')$thn=0;if($bln=='')$bln=0;
                    $dateCalculate = strtotime(date("Y-m-d", strtotime($today)) . "-$thn year");
                    $date = date('Y-m-d', $dateCalculate);
                    $dateCalculate = strtotime(date("Y-m-d", strtotime($date)) . "-$bln month");
                    $date = date('Y-m-d', $dateCalculate);
                    $dateCalculate = strtotime(date("Y-m-d", strtotime($date)) . "-$hr day");
                    $tgl = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d', $dateCalculate), 'yyyy-MM-dd'),'medium',null);
                    $data['tglLahir'] = $tgl;
                     // $data['thn'] = $tgl;
                     // $data['bln'] = $tgl;
                     // $data['hr'] = $tgl;
            } else {
                $tgl = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($today, 'yyyy-MM-dd'),'medium',null);
                $data['tglLahir'] = $tgl;
                 // $data['thn'] = $tgl;
                 // $data['bln'] = $tgl;
                 // $data['hr'] = $hr;

            }

            echo json_encode($data);
            //echo json_encode(array("name"=>"John","time"=>"2pm"));
            Yii::app()->end();
        }
    }
    
    public function actionGetUmur()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $format = new CustomFormat;
            $tglLahir = $format->formatDateMediumForDB($_POST['tglLahir']);
            $dob=$tglLahir; $today=date("Y-m-d");
            list($y,$m,$d)=explode('-',$dob);
            list($ty,$tm,$td)=explode('-',$today);
            if($td-$d<0){
                $day=($td+30)-$d;
                $tm--;
            }
            else{
                $day=$td-$d;
            }
            if($tm-$m<0){
                $month=($tm+12)-$m;
                $ty--;
            }
            else{
                $month=$tm-$m;
            }
            $year=$ty-$y;
            
            $data['umur'] = str_pad($year, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($month, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($day, 2, '0', STR_PAD_LEFT).' Hr';
            $data['thn'] = str_pad($year, 2, '0', STR_PAD_LEFT);
            $data['bln'] = str_pad($month, 2, '0', STR_PAD_LEFT);
            $data['hr'] = str_pad($day, 2, '0', STR_PAD_LEFT);

            //$data['umur'] = $dob;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionHitungLamaRawat()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $format = new CustomFormat;
            $tglLahir = $format->formatDateMediumForDB($_POST['tglLahir']);
            $dob=$tglLahir; $today=date("Y-m-d");
            list($y,$m,$d)=explode('-',$dob);
            list($ty,$tm,$td)=explode('-',$today);
            if($td-$d<0){
                $day=($td+30)-$d;
                $tm--;
            }
            else{
                $day=$td-$d;
            }
            if($tm-$m<0){
                $month=($tm+12)-$m;
                $ty--;
            }
            else{
                $month=$tm-$m;
            }
            $year=$ty-$y;
            
            $data['umur'] = str_pad($year, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($month, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($day, 2, '0', STR_PAD_LEFT).' Hr';
            //$data['umur'] = $dob;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    public function actionGetTahun()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (!empty($_POST['tahun'])){
            $format = new CustomFormat;
            $tahun = $format->formatDateMediumForDB($_POST['tahun']);
            $dob=$tahun; $today=date("Y-m-d");
            list($y,$m,$d)=explode('-',$dob);
            list($ty,$tm,$td)=explode('-',$today);
            if($td-$d<0){
                $day=($td+30)-$d;
                $tm--;
            }
            else{
                $day=$td-$d;
            }
            if($tm-$m<0){
                $month=($tm+12)-$m;
                $ty--;
            }
            else{
                $month=$tm-$m;
            }
            $year=$ty-$y;
            
            $data['tahun'] = str_pad($year, 2, '0', STR_PAD_LEFT);
            $data['bulan'] = str_pad($month, 2, '0', STR_PAD_LEFT);
            echo json_encode($data);

            }
                        Yii::app()->end();
        }
    }
    
    public function actionAddPropinsi()
    {
        $modelPropinsi = new PropinsiM;

        if(isset($_POST['PropinsiM']))
        {
            $modelPropinsi->attributes = $_POST['PropinsiM'];
            if($modelPropinsi->save())
            {
                $data=PropinsiM::model()->findAll(array('order'=>'propinsi_nama'));
                $data=CHtml::listData($data,'propinsi_id','propinsi_nama');

                if(empty($data)){
                    $propinsiOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    $propinsiOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($data as $value=>$name)
                    {
                        $propinsiOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Propinsi <b>".$_POST['PropinsiM']['propinsi_nama']."</b> berhasil ditambahkan </div>",
                        'propinsi'=>$propinsiOption,
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddPropinsi', array('modelPropinsi'=>$modelPropinsi), true)));
            exit;
        }
    }
        
    public function actionAddKabupaten()
    {
        $modelKab = new KabupatenM;
        $modProp = PropinsiM::model()->findAll();

        if(isset($_POST['KabupatenM']))
        {
            $modelKab->attributes = $_POST['KabupatenM'];
            $modelKab->kabupaten_aktif = true;
            if($modelKab->save())
            {
                $data= KabupatenM::model()->findAllByAttributes(array('propinsi_id'=>$_POST['KabupatenM']['propinsi_id'],),array('order'=>'kabupaten_nama'));
                $data=CHtml::listData($data,'kabupaten_id','kabupaten_nama');

                if(empty($data)){
                    $kabupatenOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    $kabupatenOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($data as $value=>$name)
                    {
                        $kabupatenOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }

                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Kabupaten <b>".$_POST['KabupatenM']['kabupaten_nama']."</b> berhasil ditambahkan </div>",
                        'kabupaten'=>$kabupatenOption,
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddKabupaten', array('model'=>$modelKab,'modProp'=>$modProp), true)));
            exit;
        }
    }

    public function actionAddKecamatan()
    {
        $modelKec = new KecamatanM;
        //$modProp = PropinsiM::model()->findAll(array('order'=>'propinsi_nama'));

        if(isset($_POST['KecamatanM']))
        {
            $modelKec->attributes = $_POST['KecamatanM'];
            $modelKec->kecamatan_aktif = true;
            if($modelKec->save())
            {
                $data= KecamatanM::model()->findAllByAttributes(array('kabupaten_id'=>$_POST['KecamatanM']['kabupaten_id'],),array('order'=>'kecamatan_nama'));
                $data=CHtml::listData($data,'kecamatan_id','kecamatan_nama');

                if(empty($data)){
                    $kecamatanOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    $kecamatanOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($data as $value=>$name)
                    {
                        $kecamatanOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }

                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Kecamatan <b>".$_POST['KecamatanM']['kecamatan_nama']."</b> berhasil ditambahkan </div>",
                        'kecamatan'=>$kecamatanOption,
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddKecamatan', array('model'=>$modelKec), true)));
            exit;
        }
    }

    public function actionAddKelurahan()
    {
        $modelKel = new KelurahanM;

        if(isset($_POST['KelurahanM']))
        {
            $modelKel->attributes = $_POST['KelurahanM'];
            $modelKel->kelurahan_aktif = true;
            if($modelKel->save())
            {
                $data= KelurahanM::model()->findAllByAttributes(array('kecamatan_id'=>$_POST['KelurahanM']['kecamatan_id']),array('order'=>'kelurahan_nama'));
                $data=CHtml::listData($data,'kelurahan_id','kelurahan_nama');

                if(empty($data)){
                    $kelurahanOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-Pilih-'),true);
                }else{
                    $kelurahanOption = CHtml::tag('option',array('value'=>''),CHtml::encode('-Pilih-'),true);
                    foreach($data as $value=>$name)
                    {
                        $kelurahanOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }

                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Kelurahan <b>".$_POST['KelurahanM']['kelurahan_nama']."</b> berhasil ditambahkan </div>",
                        'kelurahan'=>$kelurahanOption,
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddKelurahan', array('model'=>$modelKel,), true)));
            exit;
        }
    }
        
    public function actionGetListKabupaten()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $data= KabupatenM::model()->findAllByAttributes(array('propinsi_id'=>$_POST['idProp'],'kabupaten_id'=>$_POST['idKab']),array('order'=>'kabupaten_nama'));
            $data=CHtml::listData($data,'kabupaten_id','kabupaten_nama');

            foreach($data as $value=>$name)
            {
                if($value==$_POST['idKab'])
                    $kabupatenOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $kabupatenOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listKabupaten'] = $kabupatenOption;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
        
    public function actionGetListKecamatan()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $data= KecamatanM::model()->findAllByAttributes(array('kabupaten_id'=>$_POST['idKab'],'kecamatan_id'=>$_POST['idKec'],),array('order'=>'kecamatan_nama'));
            $data=CHtml::listData($data,'kecamatan_id','kecamatan_nama');

            foreach($data as $value=>$name)
            {
                if($value==$_POST['idKec'])
                    $kecamatanOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $kecamatanOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listKecamatan'] = $kecamatanOption;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
        
    public function actionGetListDaerahPasien()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $idPropinsi = $_POST['idProp'];
            $idKabupaten = $_POST['idKab'];
            $idKecamatan = $_POST['idKec'];
            $idKelurahan = $_POST['idKel'];
            $idPasien = $_POST['idPasien'];
            
            $propinsis = PropinsiM::model()->findAll('propinsi_aktif = TRUE');
            $propinsis = CHtml::listData($propinsis,'propinsi_id','propinsi_nama');
            $pasien = PasienM::model()->findByPk($idPasien);
            
            
            foreach($propinsis as $value=>$name)
            {
                if($value==$idPropinsi)
                    $propinsiOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $propinsiOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }
            
            $kabupatens = KabupatenM::model()->findAllByAttributes(array('propinsi_id'=>$idPropinsi,'kabupaten_aktif'=>true,));
            $kabupatens = CHtml::listData($kabupatens,'kabupaten_id','kabupaten_nama');
            foreach($kabupatens as $value=>$name)
            {
                if($value==$idKabupaten)
                    $kabupatenOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $kabupatenOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }
            
            $kecamatans = KecamatanM::model()->findAllByAttributes(array('kabupaten_id'=>$idKabupaten,'kecamatan_aktif'=>true,));
            $kecamatans = CHtml::listData($kecamatans,'kecamatan_id','kecamatan_nama');
            foreach($kecamatans as $value=>$name)
            {
                if($value==$idKecamatan)
                    $kecamatanOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $kecamatanOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }
            
            if(!empty($pasien->kelurahan_id)){
//                echo $pasien->kelurahan_id;exit;
                $kelurahans = KelurahanM::model()->findAllByAttributes(array('kecamatan_id'=>$idKecamatan,'kelurahan_aktif'=>true));
                $kelurahans = CHtml::listData($kelurahans,'kelurahan_id','kelurahan_nama');
                $kelurahanOption .= CHtml::tag('option',array('value'=>null),"-- Pilih --",true);
                foreach($kelurahans as $value=>$name)
                {
                    if($value==$pasien->kelurahan_id)
                        $kelurahanOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                    else
                        $kelurahanOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                }
            }else{
                $kelurahanOption .= CHtml::tag('option',array('value'=>null),"-- Pilih --",true);
            }
            

            $dataList['listPropinsi'] = $propinsiOption;
            $dataList['listKabupaten'] = $kabupatenOption;
            $dataList['listKecamatan'] = $kecamatanOption;
            $dataList['listKelurahan'] = $kelurahanOption;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
    
    public function actionGetPasienFromPendaftaran()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $idPendaftaran = $_POST['idPendaftaran'];
            $modPendaftaran = PendaftaranT::model()->with('carabayar','penjamin')->findByPk($idPendaftaran);
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
            $attributePasien = $modPasien->attributeNames();
            $attributePendaftaran = $modPendaftaran->attributeNames();
            
            foreach ($attributePendaftaran as $attribute){
                $data['pendaftaran'][$attribute] = $modPendaftaran->$attribute;
                $data['pendaftaran']['carabayar_nama'] = $modPendaftaran->carabayar->carabayar_nama;
                $data['pendaftaran']['penjamin_nama'] = $modPendaftaran->penjamin->penjamin_nama;
            }
            
            foreach ($attributePasien as $attribute){
                $data['pasien'][$attribute] = $modPasien->$attribute;
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    public function actionGetPendaftaranFromPasien()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $idPasien = $_POST['idPasien'];
            echo $modPendaftaran;
            $modPendaftaran = PendaftaranT::model()->with('carabayar','penjamin')->findByAttributes(array('pasien_id'=>$idPendaftaran));
            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
            $attributePasien = $modPasien->attributeNames();
            $attributePendaftaran = $modPendaftaran->attributeNames();
            
            foreach ($attributePendaftaran as $attribute){
                $data['pendaftaran'][$attribute] = $modPendaftaran->$attribute;
                $data['pendaftaran']['carabayar_nama'] = $modPendaftaran->carabayar->carabayar_nama;
                $data['pendaftaran']['penjamin_nama'] = $modPendaftaran->penjamin->penjamin_nama;
            }
            
            foreach ($attributePasien as $attribute){
                $data['pasien'][$attribute] = $modPasien->$attribute;
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionGetPenanggungJawab()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $data = array();
            if(!empty($_POST['idPenanggungJawab'])) {
                $idPenanggungJawab = $_POST['idPenanggungJawab'];
                $modPenanggungJawab = PenanggungjawabM::model()->findByPk($idPenanggungJawab);
                $attributePJ = $modPenanggungJawab->attributeNames();

                foreach ($attributePJ as $attribute){
                    $data['penanggungjawab'][$attribute] = $modPenanggungJawab->$attribute;
                }
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
        
    public function actionGetListCaraBayar()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $idCaraBayar = $_POST['idCaraBayar'];
            
            $carabayars = CarabayarM::model()->findAllByAttributes(array('carabayar_aktif'=>true),array('order'=>'carabayar_nama'));
            $carabayars = CHtml::listData($carabayars,'carabayar_id','carabayar_nama');
            $Option = "";
            foreach($carabayars as $value=>$name)
            {
                if($value==$idCaraBayar)
                    $Option .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $Option .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listCaraBayar'] = $Option;
            
            $penjamins = PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id'=>$idCaraBayar,'penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
            $penjamins = CHtml::listData($penjamins,'penjamin_id','penjamin_nama');
            $Option = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
            foreach($penjamins as $value=>$name)
            {
                if($value==$idCaraBayar)
                    $Option .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $Option .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listPenjamin'] = $Option;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
        
    public function actionGetListPenjamin()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $idCaraBayar = $_POST['idCaraBayar'];
            $idPenjamin = (isset($_POST['idPenjamin'])) ? $_POST['idPenjamin'] : '';
            
            $penjamins = PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id'=>$idCaraBayar,'penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
            $penjamins = CHtml::listData($penjamins,'penjamin_id','penjamin_nama');
            $Option = "";
            foreach($penjamins as $value=>$name)
            {
                if($value==$idPenjamin)
                    $Option .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $Option .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listPenjamin'] = $Option;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
    
    public function actionUbahCaraBayar()
    {
        $model = new UbahcarabayarR;
        $modPendaftaran = new PendaftaranT;
        
        if(isset($_POST['UbahcarabayarR']))
        {
             $pendaftaran_id = $_POST['pendaftaran_id'];
            $model->attributes = $_POST['UbahcarabayarR'];
            $model->pendaftaran_id = $_POST['pendaftaran_id'];
            $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
            $model->tglubahcarabayar = date('Y-m-d H:i:s');

            $transaction = Yii::app()->db->beginTransaction();
            try {
                
                $modPendaftaran = PendaftaranT::model()->findByPk(
                    $model->pendaftaran_id
                );
                $modPendaftaran->no_asuransi = null;
                $modPendaftaran->namapemilik_asuransi = null;
                $modPendaftaran->nopokokperusahaan = null;
                $modPendaftaran->namaperusahaan = null;
                $modPendaftaran->kelastanggungan_id = null;
                $modPendaftaran->status_konfirmasi = null;
                $modPendaftaran->tgl_konfirmasi = null;
                
                if(isset($_POST['PendaftaranT'])){
                    $modPendaftaran->attributes = $_POST['PendaftaranT'];
                }
              //  $modPendaftaran->penanggungjawab_id = '547';
                $modPendaftaran->carabayar_id = $model->carabayar_id;
                $modPendaftaran->penjamin_id = $model->penjamin_id;
                /*
                $updatePendaftaran = PendaftaranT::model()->updateByPk($model->pendaftaran_id,
                    array(
                        'carabayar_id'=>$model->carabayar_id,
                        'penjamin_id'=>$model->penjamin_id,
                        'update_time'=>date('Y-m-d H:i:s'),
                        'update_loginpemakai_id'=>Yii::app()->user->id
                    )
                );
                */
                
               if($model->save() ){
//                if($model->save() && $modPendaftaran->save()){
                    PendaftaranT::model()->updateByPk($pendaftaran_id,array('carabayar_id'=>$modPendaftaran->  carabayar_id,'penjamin_id'=>$modPendaftaran->penjamin_id));
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah Cara Bayar dan Penjamin Pasien.</div>",
                        )
                    );
                    exit;
                } else {
                    /*
                    echo CJSON::encode(array(
                        'status'=>'not',
                        'div'=>"<div class='flash-error'>Gagal simpan</div>",
                        )
                    );
                     */
                    $transaction->rollback();
                }
            } catch (Exception $exc) {
                $transaction->rollback();
            }
                
//            if($model->save())
//            {
//                if (Yii::app()->request->isAjaxRequest)
//                {
//                    echo CJSON::encode(array(
//                        'status'=>'proses_form',
//                        'div'=>"<div class='flash-success'>Berhasil merubah Cara Bayar dan Penjamin Pasien.</div>",
//                        ));
//                    exit;
//                }
//            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(
                array(
                    'status'=>'create_form',
                    'div'=>$this->renderPartial(
                        '_formUbahCaraBayar',
                        array(
                            'model'=>$model,
                            'modPendaftaran'=>$modPendaftaran
                        ), true
                    )
                )
            );
            exit;
        }
    }
    
    public function actionUbahCaraBayarRI()
    {
        $model = new UbahcarabayarR;
        $modPendaftaran = new PendaftaranT;
        
        if(isset($_POST['UbahcarabayarR']))
        {
            $model->attributes = $_POST['UbahcarabayarR'];
            $model->pendaftaran_id = $_POST['pendaftaran_id'];
            $modPendaftaran = PendaftaranT::model()->findByPk($model->pendaftaran_id);
            $model->tglubahcarabayar = date('Y-m-d H:i:s');

            $transaction = Yii::app()->db->beginTransaction();
            try {
                
                $attributes = array('pendaftaran_id'=>$model->pendaftaran_id);
                $data = PasienadmisiT::model()->findByAttributes($attributes);
                
                $attributes = array(
                    'carabayar_id'=>$model->carabayar_id,
                    'penjamin_id'=>$model->penjamin_id,
                    'update_time'=>date('Y-m-d H:i:s'),
                    'update_loginpemakai_id'=>Yii::app()->user->id
                );
                $updatePendaftaran = PasienadmisiT::model()->updateByPk($data['pasienadmisi_id'], $attributes);
                
                if($model->save() && $updatePendaftaran){
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah Cara Bayar dan Penjamin Pasien.</div>",
                        ));
                    exit;
                } else {
                    $transaction->rollback();
                }
            } catch (Exception $exc) {
                $transaction->rollback();
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial(
                        '_formUbahCaraBayar',
                        array(
                            'model'=>$model,
                            'modPendaftaran'=>$modPendaftaran
                        ), true
                    )
                )
            );
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanLab()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanLab = $_POST['idPemeriksaanlab'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modPeriksaLab = PemeriksaanlabM::model()->with('jenispemeriksaan')->findByPk($idPemeriksaanLab);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanLab', array('modPeriksaLab'=>$modPeriksaLab,
                                                                              //'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'modTarif'=>$modTarif), true)));
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanRad()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRad = $_POST['idPemeriksaanrad'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modPeriksaRad = PemeriksaanradM::model()->findByPk($idPemeriksaanRad);
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id));
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanRad', array('modPeriksaRad'=>$modPeriksaRad,
                                                                              //'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'modTarif'=>$modTarif), true)));
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanLabPendLab()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanLab = $_POST['idPemeriksaanlab'];
            $idKelasPelayan = $_POST['kelasPelayan_id'];
            $modPeriksaLab = PemeriksaanlabM::model()->with('jenispemeriksaan')->findByPk($idPemeriksaanLab);
            $modTindakanRuangan = TariftindakanperdaV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id, 'kelaspelayanan_id'=>$idKelasPelayan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id));
            
            echo CJSON::encode(array(
                'daftartindakan_id'=>$modPeriksaLab->daftartindakan_id,
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanLabPendLab', array('modPeriksaLab'=>$modPeriksaLab,
                                                                              'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'idKelasPelayan'=>$idKelasPelayan  ), true)));
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanLabMasuk()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanLab = $_POST['idPemeriksaanlab'];
//            echo $idPemeriksaanLab ; exit;
//            $idKelasPelayan = $_POST['kelasPelayan_id'];
            $idKelasPelayan = 5; // tanpa kelas
            $modPeriksaLab = PemeriksaanlabM::model()->with('jenispemeriksaan')->findByPk($idPemeriksaanLab);
            $modKelasPelayanan = KelaspelayananM::model()->findByAttributes(array('kelaspelayanan_id'=>$idKelasPelayan));
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id));
            $modTindakanRuangan = TariftindakanperdaruanganV::model()->findByAttributes(
                array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id, 'kelaspelayanan_id'=>$idKelasPelayan)
            );
            //TariftindakanperdaruanganV
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanLabMasuk', array('modPeriksaLab'=>$modPeriksaLab,
                                                                              'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'modKelasPelayanan'=>$modKelasPelayanan,
                                                                              'idKelasPelayan'=>$idKelasPelayan  ), true)));
            exit;
        }
    }
    public function actionLoadFormPemeriksaanLabMasukBaru()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanLab = $_POST['idPemeriksaanlab'];
//            echo $idPemeriksaanLab ; exit;
//            $idKelasPelayan = $_POST['kelasPelayan_id'];
            $idKelasPelayan = 5; // tanpa kelas
            $idPendaftaran = $_POST['idPendaftaran'];
            $modPeriksaLab = PemeriksaanlabM::model()->with('jenispemeriksaan')->findByPk($idPemeriksaanLab);
            $modKelasPelayanan = KelaspelayananM::model()->findByAttributes(array('kelaspelayanan_id'=>$idKelasPelayan));
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id));
            $modTindakanRuangan = TariftindakanperdaruanganV::model()->findByAttributes(
                array('daftartindakan_id'=>$modPeriksaLab->daftartindakan_id, 'kelaspelayanan_id'=>$idKelasPelayan)
            );
            $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,'daftartindakan_id'=>$modPeriksaLab->daftartindakan_id));
            //TariftindakanperdaruanganV
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanLabMasukBaru', array('modPeriksaLab'=>$modPeriksaLab,
                                                                              'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'modKelasPelayanan'=>$modKelasPelayanan,
                                                                              'idKelasPelayan'=>$idKelasPelayan,
                                                                              'modTindakanPelayanan'=>$modTindakanPelayanan), true)));
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanRadPendRad()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRad = $_POST['idPemeriksaanrad'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modPeriksaRad = PemeriksaanradM::model()->findByPk($idPemeriksaanRad);
            $daftartindakan_id = $modPeriksaRad->daftartindakan_id;
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id));
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'daftartindakan_id'=>$daftartindakan_id,
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanRadPendRad',array('modPeriksaRad'=>$modPeriksaRad,
                                                                                    //'modTindakanRuangan'=>$modTindakanRuangan,
                                                                                    'modTarif'=>$modTarif), true)));
            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanRadMasuk()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRad = $_POST['idPemeriksaanrad'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modPeriksaRad = PemeriksaanradM::model()->findByPk($idPemeriksaanRad);
            //$modTindakanRuangan = TindakanruanganV::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id));
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRad->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanRadMasuk',array('modPeriksaRad'=>$modPeriksaRad,
                                                                                    //'modTindakanRuangan'=>$modTindakanRuangan,
                                                                                    'modTarif'=>$modTarif), true)));
            exit;
        }
    }
    
  
    public function actionLoadFormOperasi()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idOperasi = $_POST['idOperasi'];
            $idKelasPelayan = $_POST['kelasPelayan_id'];
            $modOperasi = OperasiM::model()->with('kegiatanoperasi')->findByPk($idOperasi);
            $modTindakanRuangan = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modOperasi->daftartindakan_id,
                                                                                  'kelaspelayanan_id'=>$idKelasPelayan,
                                                                                  'komponentarif_id'=> Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadOperasi', array('modOperasi'=>$modOperasi,
                                                                              'modTindakanRuangan'=>$modTindakanRuangan,
                                                                              'idKelasPelayan'=>$idKelasPelayan  ), true)));
            exit;
        }
    }

    public function actionLoadFormPermintaanOperasi()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idOperasi = $_POST['idOperasi'];
            $idKelasPelayanan = $_POST['kelasPelayan_id'];
            $modOperasi = OperasiM::model()->with('kegiatanoperasi')->findByPk($idOperasi);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modOperasi->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPermintaanOperasi', array('modOperasi'=>$modOperasi,
                                                                                'idKelasPelayan'=>$idKelasPelayanan,
                                                                                'modTarif'=>$modTarif), true)));
            exit;
        }
    }

    public function actionLoadFormPermintaanRehabMedis()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idTindakanRM = $_POST['idTindakanRM'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modTindakanRm = TindakanrmM::model()->with('jenistindakanrm')->findByPk($idTindakanRM);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modTindakanRm->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            
            
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPermintaanRehabMedis', array('modTindakanRm'=>$modTindakanRm,
//                                                                                'idKelasPelayanan'=>$idKelasPelayanan,
                                                                                'modTarif'=>$modTarif), true)));
            exit;
        }
    }
    
    public function actionGetReferensiHasilRad()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRad = $_POST['idPemeriksaanRad'];
            $modReferensi = ReferensihasilradM::model()->findByAttributes(array('pemeriksaanrad_id'=>$idPemeriksaanRad));
            $attributeRef = $modReferensi->attributeNames();

            foreach ($attributeRef as $attribute){
                $data[$attribute] = $modReferensi->$attribute;
            }
            echo CJSON::encode($data);
            Yii::app()->end();
        }
    }
    /**
     * load data referensi berdasarkan dokter (pegawai_id) dan refhasilrad_id
     */
    public function actionGetReferensiHasilRadDokter()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $pegawaiId = $_POST['pegawaiId'];
            $refhasilradId = $_POST['refhasilradId'];
            $modReferensi = ReferensihasilradM::model()->findByAttributes(array('refhasilrad_id'=>$refhasilradId,'pegawai_id'=>$pegawaiId));
            $attributeRef = $modReferensi->attributeNames();

            foreach ($attributeRef as $attribute){
                $data[$attribute] = $modReferensi->$attribute;
            }
            echo CJSON::encode($data);
            Yii::app()->end();
        }
    }
    public function actionLoadFormTindakanGizi()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idRuangan = strlen($_POST['idRuangan'] > 0) ? $_POST['idRuangan'] : "2552599";
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modTindakan = TindakanruanganM::model()->with('daftartindakan')->findAllByAttributes(
                array('ruangan_id'=>$idRuangan)
            );
            
              $modTarif = TariftindakanM::model()->findByAttributes(
                  array(
                      'daftartindakan_id'=>$modTindakan->daftartindakan_id,
                      'kelaspelayanan_id'=>$idKelasPelayanan,
                      'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL
                  )
              );
            
            $form = null;
            if($modTindakan){
                $form = $this->renderPartial('_formLoadTindakanGizi',
                    array(
                        'modTindakan'=>$modTindakan,
                        'idKelasPelayanan'=>$idKelasPelayanan,
                    ), true
                );
            }
            echo CJSON::encode(
                array(
                    'status'=>'create_form',
                    'form'=>$form
                )
            );
            exit;
        }
    }
    public function actionLoadFormTindakanGiziKelas()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idKelas = $_POST['idKelas'];
            $idTindakan = $_POST['idTindakan'];
              $modTarif = TariftindakanM::model()->findByAttributes(
                  array(
                      'daftartindakan_id'=>$idTindakan,
                      'kelaspelayanan_id'=>$idKelas,
                      'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL
                  )
              );
            if(count($modTarif)>0){
                $data['status'] = 'ada';
            }else{
                $data['status'] = 'kosong';
            }
         echo json_encode($data);
         Yii::app()->end();
        }
    }

    public function actionLoadFormOperasiMasuk()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idOperasi = $_POST['idOperasi'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
//            echo $idOperasi;exit;
//            echo $idKelasPelayanan;exit;
            $modOperasi = OperasiM::model()->with('kegiatanoperasi')->findByPk($idOperasi);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modOperasi->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadOperasiMasuk', array('modOperasi'=>$modOperasi,
                                                                              'modTarif'=>$modTarif,
                                                                              'idKelasPelayanan'=>$idKelasPelayanan), true)));

            exit;
        }
    }
    
    public function actionLoadFormPemeriksaanRMPendRM()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRM = $_POST['idPemeriksaanRM'];
            $idKelasPelayanan = $_POST['kelasPelayan_id'];
            $modPeriksaRM = TindakanrmM::model()->with('jenistindakanrm')->findByPk($idPemeriksaanRM);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modPeriksaRM->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadPemeriksaanRMPendRM', array('modPeriksaRM'=>$modPeriksaRM,
                                                                              'modTarif'=>$modTarif,
                                                                              'idKelasPelayanan'=>$idKelasPelayanan  ), true)));
            exit;
        }
    }
    
    public function actionLoadFormRehabMedisMasuk()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPemeriksaanRM = $_POST['idPemeriksaanRM'];
            $idKelasPelayanan = $_POST['kelasPelayanan_id'];
            
            
            $modTindakan = TindakanrmM::model()->with('jenistindakanrm')->findByPk($idPemeriksaanRM);
            $modTarif = TariftindakanM::model()->findByAttributes(array('daftartindakan_id'=>$modTindakan->daftartindakan_id,
                                                                        'kelaspelayanan_id'=>$idKelasPelayanan,
                                                                        'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadRehabMedisMasuk', array('modTindakan'=>$modTindakan,
                                                                              'modTarif'=>$modTarif,
                                                                              'idKelasPelayanan'=>$idKelasPelayanan), true)));
            exit;
        }
    }
    
    public function actionLoadFormDiagnosis()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idDiagnosa = $_POST['idDiagnosa'];
            $idKelDiagnosa = $_POST['idKelDiagnosa'];
            $tglDiagnosa = $_POST['tglDiagnosa'];
            
            $modDiagnosaicdixM = DiagnosaicdixM::model()->findAll();
            $modSebabDiagnosa = SebabdiagnosaM::model()->findAll();
            $modDiagnosa = DiagnosaM::model()->findByPk($idDiagnosa);
            
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadDiagnosis', array('modDiagnosa'=>$modDiagnosa,
                                                                         'idKelDiagnosa'=>$idKelDiagnosa,
                                                                         'modDiagnosaicdixM'=>$modDiagnosaicdixM,
                                                                         'modSebabDiagnosa'=>$modSebabDiagnosa,
                                                                         'tglDiagnosa'=>$tglDiagnosa), true)));
            exit;
        }
    }

    public function actionLoadFormJadwalKunjunganAwal()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPasienMasukPenunjang = $_POST['idPasienMasukPenunjang'];
            $lamaTerapi = $_POST['lamaTerapi'];
            $tindakan = array();
            $idHasil = array();
            
            $sql = "select * from hasilpemeriksaanrm_t where pasienmasukpenunjang_id = $idPasienMasukPenunjang";
            $modHasil = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($modHasil as $i=>$hasilPeriksa) {
                $tindakan[$i] = $hasilPeriksa['tindakanrm_id'];
                $idHasil[$i] = $hasilPeriksa['hasilpemeriksaanrm_id'];
            }
            echo CJSON::encode(array(
                'status'=>'create_form',
                'form'=>$this->renderPartial('_formLoadJadwalKunjunganAwal', array('modHasilPemeriksaan'=>$modHasilPemeriksaan,
                                                                              'idPasienMasukPenunjang'=>$idPasienMasukPenunjang,
                                                                              'lamaTerapi'=>$lamaTerapi,
                                                                              'tindakan'=>$tindakan,
                                                                              'idHasil'=>$idHasil
                    ), true)));
            exit;
        }
    }
    
    public function actionLoadFormTindakanPaket()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idTipePaket = $_POST['idTipePaket'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $idKelompokUmur = $_POST['idKelompokUmur'];
            $idCarabayar = $_POST['idCarabayar'];
            
            $modPaketTindakan = PaketpelayananV::model()->findAllByAttributes(array('tipepaket_id'=>$idTipePaket));
            $modTindakans = array();
            $optionDaftarttindakan = '';
            if($idTipePaket!=Params::TIPEPAKET_LUARPAKET){
                foreach ($modPaketTindakan as $i => $tindakan) {
                    
                    $modTindakans[$i] = new TindakanpelayananT;
                    $modTindakans[$i]->daftartindakan_id = $tindakan->daftartindakan_id;
                    $modTindakans[$i]->daftartindakanNama = $tindakan->daftartindakan_nama;
                    $modTindakans[$i]->kategoriTindakanNama = $tindakan->kategoritindakan_nama;
                    $modTindakans[$i]->qty_tindakan = 1;
                    $modTindakans[$i]->persenCyto = 0;
//                    $modTindakans[$i]->tarif_satuan = $tindakan->tarifpaketpel;
                    $modTindakans[$i]->tarif_satuan = $tindakan->iurbiaya;
                    $modTindakans[$i]->jumlahTarif = $modTindakans[$i]->qty_tindakan * $modTindakans[$i]->tarif_satuan;
                    $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                    $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                    $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                    $modTindakans[$i]->iurbiaya_tindakan = 0;//$tindakan->iurbiaya;


                    //buat option daftartindakanPemakaianBahan
                    $optionDaftarttindakan .= CHtml::tag('option', array('value'=>$modTindakans[$i]->daftartindakan_id), $modTindakans[$i]->daftartindakanNama, true);
                }
            }
            
            // ambil data untuk paket BMHP
            $totHargaBmhp = 0;
            $modPaketBmhp = PaketbmhpM::model()->with('obatalkes','daftartindakan')->findAllByAttributes(array('tipepaket_id'=>$idTipePaket,
                                                                                                               'kelompokumur_id'=>$idKelompokUmur));
            foreach ($modPaketBmhp as $i => $bmhp) {
                $totHargaBmhp = $totHargaBmhp + $bmhp->hargapemakaian;
            }
            // ---------------------------
            
            echo CJSON::encode(array(
                'form'=>$this->renderPartial('_formLoadTindakanPaket', array('modPaketTindakan'=>$modPaketTindakan,
                                                                             'modTindakans'=>$modTindakans,
                    ), true),
                'formPaketBmhp'=>$this->renderPartial('_formLoadPaketBmhp', array('modPaketBmhp'=>$modPaketBmhp,
                    ), true),
                'totHargaBmhp'=>$totHargaBmhp,
                'optionDaftarttindakan'=>$optionDaftarttindakan,
                ));
            exit;
        }
    }

    public function actionGetListPendaftaran()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPasien = $_POST['idPasien'];
            
            $modPendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$idPasien,'ruangan_id'=>Params::RUANGAN_ID_REHAB));
            
            
            echo CJSON::encode(array(
                'status'=>(!empty ($modPendaftaran)) ? 'Ada' : 'Kosong',
                'form'=>$this->renderPartial('_formGetListPendaftaran', array('modPendaftaran'=>$modPendaftaran
                    ), true)));
            exit;
        }
    }
    
    public function actionAddFormPemakaianBahan()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPendaftaran = $_POST['idPendaftaran'];
            $idObatAlkes = $_POST['idObatAlkes'];
            $idDaftartindakan = (isset($_POST['idDaftartindakan']) ? $_POST['idDaftartindakan'] : "");
            $modObatAlkes = ObatalkesM::model()->findByPk($idObatAlkes);
            $modDaftartindakan = DaftartindakanM::model()->findByPk($idDaftartindakan);
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            $persenjual = $this->persenJualRuangan();
            $modObatAlkes->hargajual = floor(($persenjual + 100 ) / 100 * $modObatAlkes->hargajual);
            
            echo CJSON::encode(array(
                'idPasien'=>$modPendaftaran->pasien_id,
                'idPendaftaran'=>$idPendaftaran,
                'namaObat'=>$modObatAlkes->obatalkes_nama,
                'form'=>$this->renderPartial('_formAddPemakaianBahan', array('modObatAlkes'=>$modObatAlkes,'modDaftartindakan'=>$modDaftartindakan,
                    'modPendaftaran'=>$modPendaftaran,
                    ), true),
                ));
            exit;
        }
    }
        
    protected function persenJualRuangan()
    {
        switch(Yii::app()->user->getState('instalasi_id')){
            case Params::INSTALASI_ID_RI : $persen = Yii::app()->user->getState('ri_persjual');
                                            break;
            case Params::INSTALASI_ID_RJ : $persen = Yii::app()->user->getState('rj_persjual');
                                            break;
            case Params::INSTALASI_ID_RD : $persen = Yii::app()->user->getState('rd_persjual');
                                            break;
                                        default : $persen = 0; break;
        }

        return $persen;
    }
    
    public function actionAddFormPemakaianAlat()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idAlat = $_POST['idAlat'];
            $idDaftartindakan = $_POST['idDaftartindakan'];
            $modAlat = AlatmedisM::model()->findByPk($idAlat);
            $modDaftartindakan = DaftartindakanM::model()->findByPk($idDaftartindakan);
            $modObatAlkes = new ObatalkesM;
            echo CJSON::encode(array(
                'namaAlat'=>$modAlat->alatmedis_nama,
                'form'=>$this->renderPartial('_formAddPemakaianAlat', array('modAlat'=>$modAlat,'modDaftartindakan'=>$modDaftartindakan,'modObatAlkes'=>$modObatAlkes
                    ), true),
                ));
            exit;
        }
    }

    public function actionGetListJadwalKunjungan()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idPendaftaran = $_POST['idPendaftaran'];
            
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            
            $modRujukan = RujukanT::model()->findByPk($modPendaftaran->rujukan_id);
            
//            $modJadwalKunjungan = JadwalkunjunganrmT::model()->findAll('pendaftaran_id = '.$idPendaftaran.' order by nourutjadwal');
            
            $sql = "select * from jadwalkunjunganrm_t where pendaftaran_id = $idPendaftaran and tglkunjunganrm is null order by nourutjadwal asc limit 1";
            
            $modJadwalKunjungan = JadwalkunjunganrmT::model()->findBySql($sql);
            
            $sisaTerapi = Yii::app()->db->createCommand('select count(lamaterapikunjungan) from jadwalkunjunganrm_t
                            where pendaftaran_id = '.$idPendaftaran.' and tglkunjunganrm is null ')->queryScalar();
            
            echo CJSON::encode(array(
                'status'=>(!empty ($modJadwalKunjungan)) ? 'Ada' : 'Kosong',
                'jeniskasuspenyakit_id'=> $modPendaftaran->jeniskasuspenyakit_id,
                'pegawai_id'=> $modPendaftaran->pegawai_id,
                'carabayar_id'=> $modPendaftaran->carabayar_id,
                'penjamin_id'=> $modPendaftaran->penjamin_id,
                'asalrujukan_id'=>$modRujukan->asalrujukan_id,
                'no_rujukan'=>$modRujukan->no_rujukan,
                'tanggal_rujukan'=>$modRujukan->tanggal_rujukan,
                'form'=>$this->renderPartial('_formGetListJadwalKunjungan',
                        array('modJadwalKunjungan'=>$modJadwalKunjungan, 'i'=>0,'sisaTerapi'=>$sisaTerapi
                    ), true)));
            exit;
        }
    }

    public function actionAddFormPaketBmhp()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idKelUmur = $_POST['idKelUmur'];
            $idPendaftaran = $_POST['idPendaftaran'];
            $idDaftarTindakan = $_POST['idDaftarTindakan'];
            $modPaketBmhp = PaketbmhpM::model()->with('daftartindakan','obatalkes')->findAllByAttributes(array('daftartindakan_id'=>$idDaftarTindakan,
                                                                        'kelompokumur_id'=>$idKelUmur,));
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            echo CJSON::encode(array(
                'form'=>$this->renderPartial('_formAddPaketBmhp', array('modPaketBmhp'=>$modPaketBmhp,'modPendaftaran'=>$modPendaftaran,
                    ), true),
                ));
            exit;
        }
    }

    public function actionAddPasienPulang()
    {
        $validRujukan = true;
        
        $idPendaftaran = Yii::app()->session['pendaftaran_id'];
        $idPasien = Yii::app()->session['pasien_id'];
        $modelPulang = new PasienpulangT;
        $modRujukanKeluar = new PasiendirujukkeluarT;
        
        $modelPulang->tglpasienpulang = date('Y-m-d H:i:s');
        $modelPulang->pendaftaran_id = $idPendaftaran;
        $modelPulang->pasien_id = $idPasien;
        
        $modRD = InfokunjunganrdV::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasien_id'=>$idPasien));
        
        $modRujukanKeluar->pegawai_id = PendaftaranT::model()->findByPk($idPendaftaran)->pegawai_id;
        $modRujukanKeluar->ruanganasal_id = $modRD->ruangan_id;
        
        $format = new CustomFormat();
        $date1 = $format->formatDateTimeMediumForDB($modRD->tgl_pendaftaran);
        $date2 = date('Y-m-d H:i:s');
        $diff = abs(strtotime($date2) - strtotime($date1));
        $hours   = floor(($diff)/3600);
        
        $modelPulang->lamarawat = $hours;

        if(isset($_POST['PasienpulangT']))
        {
            $modelPulang->attributes = $_POST['PasienpulangT'];
            $modelPulang->satuanlamarawat = Params::SATUANLAMARAWAT;
            $modelPulang->ruanganakhir_id = $modRD->ruangan_id;
            $modelPulang->create_time = date( 'Y-m-d H:i:s');
            $modelPulang->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelPulang->create_loginpemakai_id = Yii::app()->user->id;
            
            if(isset($_POST['pakeRujukan']))
            {
                $modelPulang->pakeRujukan = true;
                $modRujukanKeluar->attributes = $_POST['PasiendirujukkeluarT'];
                $modRujukanKeluar->pendaftaran_id = $modelPulang->pendaftaran_id;
                $modRujukanKeluar->pasien_id = $modelPulang->pasien_id;
                $modRujukanKeluar->create_time = date( 'Y-m-d H:i:s');
                $modRujukanKeluar->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modRujukanKeluar->create_loginpemakai_id = Yii::app()->user->id;
                if($modRujukanKeluar->save())
                {
                    'benar';
                }
                else
                {
                    $validRujukan = false;
                }
            }
            
            if(isset($_POST['isDead']))
            {
                $modPasien = PasienM::model()->findByPk(Yii::app()->session['pasien_id']);
                $modPasien->tgl_meninggal = $_POST['PasienpulangT']['tgl_meninggal'];
                $modPasien->save();
            }
            if($modelPulang->save() && $validRujukan)
            {
                $modPendaftaran = PendaftaranT::model()->findByPk(Yii::app()->session['pendaftaran_id']);
                $modPendaftaran->tglselesaiperiksa = date( 'Y-m-d H:i:s');
                $modPendaftaran->pasienpulang_id = $modelPulang->pasienpulang_id;
                $modPendaftaran->save();
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Data Pasien <b></b> berhasil disimpan </div>",
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formPasienPulang', array('modelPulang'=>$modelPulang,'modRujukanKeluar'=>$modRujukanKeluar,'modRD'=>$modRD), true)));
            exit;
        }
    }
    
    public function actionBuatSessionPendaftaranPasien()
    {
        $idPendaftaran = $_POST['idPendaftaran'];
        $idPasien = $_POST['idPasien'];
        
        Yii::app()->session['pendaftaran_id'] =  $idPendaftaran;
        Yii::app()->session['pasien_id'] = $idPasien;
        
        echo CJSON::encode(array(
                'pendaftaran_id'=>Yii::app()->session['pendaftaran_id'],
                'pasien_id'=>Yii::app()->session['pasien_id']));
        
    }
    //-- persalinan --
    //fungsi ajax modul persalinan
    public function actionGetMenitKe()
    {
        if(Yii::app()->request->isAjaxRequest){
            $menitke = $_POST['menitke'];
            $kelahiranbayi_id = $_POST['kelahiranbayi_id'];

            $hasil = ApgarscoreT::model()->findAllByAttributes(array('menitke'=>$menitke, 'kelahiranbayi_id'=>$kelahiranbayi_id));
            if (count($hasil) > 0){
                $hasil = true;
            } else {
                $hasil = false;
            }

            echo CJSON::encode($hasil);
            exit;
        }
    }
    //-- inventori --//
    //function ajax get untuk tabel pada Pesan Obat Alkes modul inventori
    /**
     * actionGetPesanObatAlkes digunakan pada:
     * 1. Gudang Farmasi - Inventory Pemesanan Obat Alkes
     * 2. Farmasi Apotek - Transaksi Pesan Obat Alkes
     */
    public function actionGetPesanObatAlkes()
    {
       if(Yii::app()->request->isAjaxRequest) {
            $idObat=$_POST['idObat'];
            $qtyObat = $_POST['qtyObat'];
            $idRuangan = $_POST['idRuangan'];
            $satuan = $_POST['satuan'];
            $modPesanOADetail = new PesanoadetailT;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            $modStokObatAlkes=StokobatalkesT::model()->findAll('obatalkes_id='.$idObat.'');
            $StokObat = StokobatalkesT::getStokBarang($idObat, $idRuangan);
//            $subtotal = $qtyObat*$modObatAlkes->hargajual; //Harga Jual diganti dengan HJA Resep
            $subtotal = $qtyObat*$modObatAlkes->hjaresep;
            $modPesanOADetail->stok = $StokObat;
            $modPesanOADetail->subtotal = $subtotal;
            if (!empty($satuan)){
                $modObatAlkes->satuankecil_id = $satuan;
            }
                $tr="<tr>
                        <td>".CHtml::activeHiddenField($modPesanOADetail,'[]satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id)).
                              CHtml::activeHiddenField($modPesanOADetail,'[]sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)).
                              CHtml::activeHiddenField($modPesanOADetail,'[]obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obat')).
                              CHtml::activeHiddenField($modPesanOADetail,'[]stokobat',array('value'=>$StokObat, 'class'=>'stokobat')).
                            $modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                        <td>".$modObatAlkes->satuankecil->satuankecil_nama."/<br/>".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
                        
                        <td>".CHtml::activeTextField($modPesanOADetail,'[]stok', array('class'=>'span1', 'readonly'=>true, 'onkeypress' => "return $(this).focusNextInputField(event)",))."</td>
                        <td>".CHtml::activetextField($modPesanOADetail,'[]jmlpesan',array('value'=>$qtyObat,'class'=>'span1 numbersOnly qty','onblur'=>'hitungSemua();', 'onkeypress' => "return $(this).focusNextInputField(event)",))."</td>
                        <td>".CHtml::activetextField($modPesanOADetail,'[]harganetto',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 numbersOnly netto','readonly'=>TRUE))."</td>
                        <td>".CHtml::activetextField($modPesanOADetail,'[]hargajual',array('value'=>$modObatAlkes->hjaresep,'class'=>'span1 numbersOnly hargaJual','readonly'=>TRUE))."</td>
                        <td>".CHtml::activeTextField($modPesanOADetail,'[]subtotal',array('class'=>'span1 subTotal','readonly'=>TRUE))."</td>
                        <td>".Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel'))."</td>
                      </tr>
                    ";
            
           $data['tr']=$tr;
           $data['stok']=$StokObat;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    //-- inventori --//
    //function ajax get Obat Alkes untuk tabel mutasi pda modul inventori (inventori/MutasiOARuanganT)
    /**
     * actionGetObatAlkesDariMutasi digunakan pada:
     * 1. Apotek Farmasi - Transaksi Mutasi Obat Alkes
     * 2. Gudang Farmasi - Mutasi OA
     */
    public function actionGetObatAlkesDariMutasi()
    {
       if(Yii::app()->request->isAjaxRequest) {
            $idObat=$_POST['idObat'];
            $qtyObat = $_POST['qtyObat'];
            $satuan = $_POST['satuan'];
            $minimalstok = $_POST['minimal'];
            $modMutasiDetail = new MutasioadetailT;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            if (!empty($satuan)){
                $modObatAlkes->satuankecil_id = $satuan;
            }
            $stok = StokobatalkesT::getStokBarang($idObat, Yii::app()->user->getState('ruangan_id'));
            if (Params::konfigGudangFarmasi('stokminus')){
                if ($stok == 0){
                    echo json_encode('Kosong');
                    Yii::app()->end();
                }
                if ($qtyObat > $stok){
                    $jumlahMutasi = $stok;
                }
                else{
                    $jumlahMutasi = $qtyObat;
                }
            }else{
                $jumlahMutasi = $qtyObat;
            }
            if($modObatAlkes->diskonJual > 0)
                $jumlahDiscount = ($modObatAlkes->diskonJual*$modObatAlkes->hpp*$jumlahMutasi)/100;
            else
                $jumlahDiscount = 0;
//            $subTotal = $jumlahMutasi*$modObatAlkes->hargajual;
            $subTotal = $jumlahMutasi*$modObatAlkes->hpp;
            $subTotal = $subTotal - $jumlahDiscount;
            $totalHargaNetto += $modObatAlkes->harganetto;
            $tr="<tr>
                    <td>".$modObatAlkes->sumberdana->sumberdana_nama.
                          CHtml::activeHiddenField($modMutasiDetail,'[]satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id)).
                          CHtml::activeHiddenField($modMutasiDetail,'[]sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)).
                          CHtml::activeHiddenField($modMutasiDetail,'[]obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obat')).
                          CHtml::activeHiddenField($modMutasiDetail,'[]jmlkemasan',array('value'=>$modObatAlkes->kemasanbesar)).
                          CHtml::activeHiddenField($modMutasiDetail,'[]tglkadaluarsa',array('value'=>$modObatAlkes->tglkadaluarsa)).
//                          CHtml::activeHiddenField($modMutasiDetail,'[]persendiscount',array('value'=>$modObatAlkes->discount,'class'=>'span1 persenDiskon')).
                   "<td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                    <td>".CHtml::activeTextField($modMutasiDetail, '[]stok', array('value'=>$stok, 'class'=>'maxStok span1', 'readonly'=>true))."</td>
                    <td>".CHtml::activeTextField($modMutasiDetail, '[]jmlmutasi',array('value'=>$jumlahMutasi,'class'=>'span1 numberOnly qty', 'onkeyup'=>'numberOnly(this);hitungJumlahMutasi(this);', 'onkeypress' => "return $(this).focusNextInputField(event)", 'onblur'=>'hitungSemua();'))
                    ." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                    <td>".CHtml::activeTextField($modMutasiDetail,'[]harganetto',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 hargaNetto','readonly'=>true,'onkeyup'=>'numberOnly(this);'))."</td>
                    <td>".CHtml::activeTextField($modMutasiDetail,'[]hargajualsatuan',array('value'=>$modObatAlkes->hpp,'readonly'=>TRUE,'class'=>'span1 hargaJual'))."</td>
                    <td>".CHtml::activeTextField($modMutasiDetail,'[]persendiscount', array('value'=>$modObatAlkes->diskonJual,'class'=>'span1 float jumlahDiscount','onkeypress' => "return $(this).focusNextInputField(event)", 'onkeyup'=>'hitungSemua();'))."</td>
                        <td>".CHtml::activeTextField($modMutasiDetail,'[]subTotal',array('value'=>$subTotal, 'readonly'=>TRUE,'class'=>'span2 subTotal'))."</td>
                    <td>".Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel'))."</td>
                  </tr>
                ";
           //                    <td>".CHtml::activeTextField($modMutasiDetail,'[]persendiscount', array('value'=>$modObatAlkes->discount,'class'=>'span1 numberOnly jumlahDiscount','onkeypress' => "return $(this).focusNextInputField(event)", 'onblur'=>'hitungSemua();'))."</td>
           $data['tr']=$tr;
           $data['stokobat']=$stok;
           $data['minimalstok']=$minimalstok;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    //-- inventori --//
    //function ajax get data Pemesanan Obat Alkes untuk form Mutasi modul inventori (inventory/MutasiOARuanganT)
    public function actionGetPesanObatAlkesDariMutasi()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idPesanObatAlkes=$_POST['idPesanObatAlkes'];
            $modMutasiObatAlkesDetail = new MutasioadetailT;
            $modDetailPesanObatAlkes = PesanoadetailT::model()->with('obatalkes','sumberdana','satuankecil')->findAll('pesanobatalkes_id='.$idPesanObatAlkes.'');
            foreach ($modDetailPesanObatAlkes AS $tampilDetail):
                $modObatAlkes = ObatalkesM::model()->findByPk($tampilDetail['obatalkes_id']);
                    $modStokObatAlkes=  StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$modObatAlkes->obatalkes_id,'ruangan_id' =>Yii::app()->user->getState('ruangan_id')));
                    $stokAkhir = 0;
                    $maxStok = 0;
                    foreach($modStokObatAlkes AS $tampil):
                        $stokAkhir=$stokAkhir+$tampil['qtystok_in'];
                        $maxStok=$maxStok+$tampil['qtystok_out'];
                    endforeach;
                    $stok = $stokAkhir - $maxStok;
                $jumlahMutasi = $tampilDetail['jmlpesan'];
                if ($jumlahMutasi > $stok){
                    $jumlahMutasi = $stok;
                }
                if($modObatAlkes->diskonJual > 0)
                    $jumlahDiscount = ($modObatAlkes->diskonJual*$modObatAlkes->hjaresep*$jumlahMutasi)/100;
                else
                    $jumlahDiscount = 0;
                $subTotal = ($jumlahMutasi*$modObatAlkes->hjaresep);
                $subTotal = $subTotal - $jumlahDiscount;
                $totalHargaSub += $subTotal;
                $totalHargaNetto += $modObatAlkes->harganetto;
                $tr .="<tr>
                        <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'satuankecil_id[]',array('value'=>$tampilDetail['satuankecil_id'])).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'sumberdana_id[]',array('value'=>$tampilDetail['sumberdana_id'])).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'obatalkes_id[]',array('value'=>$tampilDetail['obatalkes_id'])).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'jmlkemasan[]',array('value'=>$tampilDetail->obatalkes->kemasanbesar)).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'tglkadaluarsa[]',array('value'=>$modObatAlkes->tglkadaluarsa)).
                              CHtml::activeHiddenField($modMutasiObatAlkesDetail,'persendiscount[]',array('value'=>$modObatAlkes->diskonJual,'class'=>'persenDiskon')).
                              CHtml::hiddenField('maxStok[]', $stok, array('class'=>'maxStok')).
                              
                       "</td>
                        <td>".$tampilDetail->sumberdana['sumberdana_nama']."</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_kategori']."/<br/>".$tampilDetail->obatalkes['obatalkes_nama']."</td>
                        <td>".$stok."</td>
                        <td>".CHtml::activeTextField($modMutasiObatAlkesDetail,'jmlpesan[]',array('value'=>$tampilDetail['jmlpesan'],'readonly'=>TRUE,'class'=>'span1 jumlahPesan'))."</td>
                        <td>".CHtml::activeTextField($modMutasiObatAlkesDetail,'jmlmutasi[]',array('value'=>$jumlahMutasi,'class'=>'span1 jumlahMutasi','onkeyup'=>'numberOnly(this);hitungJumlahMutasi(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))
                            ." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                        
                        <td>".CHtml::textField('jumlahDiscount[]',$jumlahDiscount, array('readonly'=>TRUE,'class'=>'span1 jumlahDiscount'))."</td>
                        <td>".CHtml::activeTextField($modMutasiObatAlkesDetail,'harganetto[]',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 hargaNetto','readonly'=>true,'onkeyup'=>'numberOnly(this);'))."</td>
                        <td>".CHtml::activeTextField($modMutasiObatAlkesDetail,'hargajualsatuan[]',array('value'=>$modObatAlkes->hargajual,'readonly'=>TRUE,'class'=>'span1 hargaSatuan'))."</td>
                        <td>".CHtml::textField('subTotal[]',$subTotal, array('readonly'=>TRUE,'class'=>'span1 subTotal'))."</td>
                        
                    </tr>
                    <tr>
                    </tr>
                    ";
                    $idPesanObatAlkes=$tampilDetail['pesanobatalkes_id'];
           endforeach;
                $tr .= "<tr class='totalTable'>
                        <td colspan ='10' style='text-align:right;'>Total Harga</td>
                        <td>".CHtml::textField('totalHargaSub',$totalHargaSub, array('readonly'=>TRUE,'class'=>'span1 subTotal')).
                              CHtml::hiddenField('totalHargaNettoMutasi',$totalHargaNetto).
                        "</td></tr>";
           $modPesanObatAlkes=  PesanobatalkesT::model()->findByPk($idPesanObatAlkes);
           $data['tr']=$tr;
           $data['ruangan_id']=$modPesanObatAlkes->ruanganpemesan_id;
           $data['stok'] = $stok;
           
           
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    //-- inventori --//
    //function ajax get data Pemesanan Obat Alkes untuk form Mutasi modul inventori (inventory/MutasiOARuanganT)
    public function actionGetMutasiObatAlkesDariTerimaMutasi()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idMutasi=$_POST['idMutasi'];
            $modMutasi = MutasioaruanganT::model()->findByPk($idMutasi);
            $modDetailMutasi = MutasioadetailT::model()->with('obatalkes','sumberdana','satuankecil')->findAll('mutasioaruangan_id='.$idMutasi.'');
            $modDetailTerimaMutasi = new TerimamutasidetailT;
            $ruanganTujuan = $modMutasi->ruangantujuan_id;
            
            foreach ($modDetailMutasi AS $tampilDetail):
                $jumlahTerima = $tampilDetail['jmlmutasi'];
                $jumlahDiscount = ($tampilDetail->persendiscount*$tampilDetail->harganetto*$jumlahTerima)/100;
                $subTotal = ($jumlahTerima*$tampilDetail->hargajualsatuan);
                $totalHargaSub += $subTotal;
                $totalHargaNetto += $tampilDetail->harganetto;
                $tr .="<tr>
                        <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'satuankecil_id[]',array('value'=>$tampilDetail['satuankecil_id'])).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'sumberdana_id[]',array('value'=>$tampilDetail['sumberdana_id'])).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'obatalkes_id[]',array('value'=>$tampilDetail['obatalkes_id'])).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'jmlkemasan[]',array('value'=>$tampilDetail->obatalkes->kemasanbesar)).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'tglkadaluarsa[]',array('value'=>$tampilDetail->tglkadaluarsa)).
                              CHtml::activeHiddenField($modDetailTerimaMutasi,'persendiscount[]',array('value'=>$tampilDetail->persendiscount,'class'=>'persenDiskon')).
                       "</td>
                        <td>".$tampilDetail->sumberdana['sumberdana_nama']."</td>
                        <td>".$tampilDetail->obatalkes['obatalkes_kategori']."/<br/>".$tampilDetail->obatalkes['obatalkes_nama']."</td>
                        <td>".CHtml::activeTextField($modDetailTerimaMutasi,'jmlmutasi[]',array('value'=>$tampilDetail['jmlmutasi'],'readonly'=>TRUE,'class'=>'span1 jumlahMutasi'))."</td>
                        <td>".CHtml::activeTextField($modDetailTerimaMutasi,'jmlterima[]',array('value'=>$jumlahTerima,'class'=>'span1 jumlahTerima','onkeyup'=>'numberOnly(this);hitungJumlahTerima(this);'))."</td>
                        
                        <td>".CHtml::textField('jumlahDiscount[]',$jumlahDiscount, array('readonly'=>TRUE,'class'=>'span1 jumlahDiscount'))."</td>
                        <td>".CHtml::activeTextField($modDetailTerimaMutasi,'harganettoterima[]',array('value'=>$tampilDetail->harganetto,'class'=>'span1 hargaNetto','readonly'=>true,'onkeyup'=>'numberOnly(this);'))."</td>
                        <td>".CHtml::activeTextField($modDetailTerimaMutasi,'hargajualterim[]',array('value'=>$tampilDetail->hargajualsatuan,'readonly'=>TRUE,'class'=>'span1 hargaSatuan'))."</td>
                        <td>".CHtml::textField('subTotal[]',$subTotal, array('readonly'=>TRUE,'class'=>'span1 subTotal'))."</td>
                        
                    </tr>
                    <tr>
                    </tr>
                    ";
                    
           endforeach;
                $tr .= "<tr class='totalTable'>
                        <td colspan ='9' style='text-align:right;'>Total Harga</td>
                        <td>".CHtml::textField('totalHargaSub',$totalHargaSub, array('readonly'=>TRUE,'class'=>'span1 subTotal')).
                              CHtml::hiddenField('totalHargaNettoMutasi',$totalHargaNetto).
                        "</td></tr>";
           
           $data['tr']=$tr;
           $data['ruangan_id']=$ruanganTujuan;
           
           
           
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionUpdateCaraKeluar()
    {
        $idPendaftaran = $_POST['idPendaftaran'];
    }
    
    public function actionCekLogin($task='Retur')
    {
        if(Yii::app()->request->isAjaxRequest){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $idRuangan = Yii::app()->user->getState('ruangan_id');
            
            $user = LoginpemakaiK::model()->findByAttributes(array('nama_pemakai' => $username,
                                                                   'loginpemakai_aktif' =>TRUE));
            if ($user === null) {
                $data['error'] = "Login Pemakai salah!";
                $data['cssError'] = 'username';
                $data['status'] = 'Gagal Login';
            } else {
                // cek password
                if ($user->katakunci_pemakai !== $user->encrypt($password)) {
                    $data['error'] = 'password salah!';
                    $data['cssError'] = 'password';
                    $data['status'] = 'Gagal Login';
                } else {
                    // cek ruangan
                    $ruangan_user = RuanganpemakaiK::model()->findByAttributes(array('loginpemakai_id'=>$user->loginpemakai_id,
                                                                                     'ruangan_id'=> $idRuangan));
                    if($ruangan_user===null) {
                        $data['error'] = 'ruangan salah!';
                        $data['status'] = 'Gagal Login';
                    } else {
                        $data['error'] = '';
//                        $cek = Yii::app()->authManager->checkAccess($task,$user->loginpemakai_id);
                        $cek = Yii::app()->authManager->checkAccess("Administrator",$user->loginpemakai_id); // Administrator adalah ROLES
                        if($cek){
                            $data['status'] = 'success';
                            $data['userid'] = $user->loginpemakai_id;
                            $data['username'] = $user->nama_pemakai;
                        } else {
                            $data['status'] = 'Gagal Loginaa';
                        }
                    }
                }
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }

    public function actionCekLoginBatalBayar($task='BatalBayar')
    {
        if(Yii::app()->request->isAjaxRequest){
            $username = $_POST['username'];
            $password = $_POST['password'];
            $idRuangan = Yii::app()->user->getState('ruangan_id');
            
            $user = LoginpemakaiK::model()->findByAttributes(array('nama_pemakai' => $username,
                                                                   'loginpemakai_aktif' =>TRUE));
            if ($user === null) {
                $data['error'] = "Login Pemakai salah!";
                $data['cssError'] = 'username';
                $data['status'] = 'Gagal Login';
            } else {
                // cek password
                if ($user->katakunci_pemakai !== $user->encrypt($password)) {
                    $data['error'] = 'password salah!';
                    $data['cssError'] = 'password';
                    $data['status'] = 'Gagal Login';
                } else {
                    // cek ruangan
                    $ruangan_user = RuanganpemakaiK::model()->findByAttributes(array('loginpemakai_id'=>$user->loginpemakai_id,
                                                                                     'ruangan_id'=> $idRuangan));
                    if($ruangan_user===null) {
                        $data['error'] = 'ruangan salah!';
                        $data['status'] = 'Gagal Login';
                    } else {
                        $data['error'] = '';
//                        $cek = Yii::app()->authManager->checkAccess($task,$user->loginpemakai_id);
                        $cek = Yii::app()->authManager->checkAccess("Administrator",$user->loginpemakai_id); // Administrator adalah ROLES
                        if($cek){
                            $data['status'] = 'success';
                            $data['userid'] = $user->loginpemakai_id;
                            $data['username'] = $user->nama_pemakai;
                        } else {
                            $data['status'] = 'Gagal Login';
                        }
                    }
                }
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionCekLoginPembatalPulang()
    {
        if(Yii::app()->request->isAjaxRequest){
            $username = $_POST['username'];
            $password = md5($_POST['password']);
            $idRuangan = Yii::app()->user->getState('ruangan_id');
            
            $user = LoginpemakaiK::model()->findByAttributes(array('nama_pemakai' => $username,
                                                                   'loginpemakai_aktif' =>TRUE,
                                                                   'katakunci_pemakai'=>$password ));
            if ($user === null) {//Jika Username dan Password Salah
                $data['error'] = "Login Pemakai salah!";
                $data['cssError'] = 'username';
            }else{//Jika Username dan Passwrd Benar
                
                $ruangan_user = RuanganpemakaiK::model()->findByAttributes(array('loginpemakai_id'=>$user->loginpemakai_id,
                                                                                 'ruangan_id'=> $idRuangan));
                if($ruangan_user===null) {//Jika Ruangan Salah
                    $data['error'] = 'ruangan salah!';
                } else { //JIka ruangan Benar
                    $data['error'] = '';
                    $cek = Yii::app()->authManager->checkAccess('Pembatalan Pulang',$user->loginpemakai_id);
                    if($cek){//Jika User Mempunyai Hak Akses
                        $data['status'] = 'success';
                        $data['userid'] = $user->loginpemakai_id;
                        $data['username'] = $user->nama_pemakai;
                    } else {//JIka user Tidak mempunyai Hak Akses
                        $data['status'] = 'Gagal';
                    }
                }
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    //-- SistemAdministrator --//
    //function ajax get data Detail Harga Paket Pelayanan dari sistemAdministrator/paketpelayananM
    /**
     * module sistemAdministrator/paketpelayananM/update&id=6
     * @author      : Miranitha Fasha
     * date         : 9 May 2014
     * issue        : EHS-1182
     */
    public function actionGetPaketPelayanan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $tr = '';
            if (isset($_POST['tipePaket'])){
                $modPaketPelayanan = PaketpelayananM::model()->findAllByAttributes(array('tipepaket_id'=>$_POST['tipePaket']));
                if (count($modPaketPelayanan) > 0){
                    $data['paket'] = 'Ada';
                }
                else {
                    $data['paket'] = 'Tidak';
                }
            }else{
                $idTipePaket=$_POST['idTipePaket'];
                $idDaftarTindakan = $_POST['idDaftarTindakan'];
                $idTarifTindakan = $_POST['idTarifTindakan'];

                $idRuangan = $_POST['idRuangan'];
                $modTipePaket = TipepaketM::model()->findByPk($idTipePaket);
                $modDaftarTindakan = DaftartindakanM::model()->findByPk($idDaftarTindakan);
                $namaRuangan = RuanganM::model()->findByPk($idRuangan)->ruangan_nama;
                $modPaketPelayanan = new PaketpelayananM;
                $modTarifTindakan = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$idDaftarTindakan, 'komponentarif_id'=> Params::KOMPONENTARIF_ID_TOTAL));
                $TarifTindakan = TariftindakanM::model()->findByPk($idTarifTindakan);

                $totaltarif = 0;
                foreach($modTarifTindakan as $row){
                    $totaltarif += $row->harga_tariftindakan;
                }
                $modPaketPelayanan->tipepaket_id  = $idTipePaket;
                $modPaketPelayanan->daftartindakan_id = $idDaftarTindakan;
                $modPaketPelayanan->ruangan_id = $idRuangan;
                $tr .="<tr>
                            <td>".CHtml::TextField('noUrut', '', array('class' => 'span1 noUrut', 'readonly' => TRUE)).
                                    CHtml::activeHiddenField($modPaketPelayanan, '['.$idDaftarTindakan.']tipepaket_id').
                                    CHtml::activeHiddenField($modPaketPelayanan, '['.$idDaftarTindakan.']daftartindakan_id').
                                    //CHtml::activeHiddenField($modPaketPelayanan, 'ruangan_id[]', array('value'=>$idRuangan)).
                        "</td>
                            <td>".$modTipePaket->tipepaket_nama . "</td>
                            <td>".$modDaftarTindakan->daftartindakan_nama . "</td>
                            <td>".CHtml::activeDropDownList($modPaketPelayanan, '['.$idDaftarTindakan.']ruangan_id', CHtml::listData(RuanganM::model()->findAll(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'class' => 'span2 ruangan', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextField($modPaketPelayanan, '['.$idDaftarTindakan.']namatindakan[]', array('value' => $modDaftarTindakan->daftartindakan_nama, 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::TextField('totaltarif[]', number_format($TarifTindakan->harga_tariftindakan, 0, '.',','), array('readonly'=>false, 'class' => 'span2 currency totalTarif numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($modPaketPelayanan, '['.$row->daftartindakan_id.']tarifpaketpel', array('parent'=>'SAPaketpelayananM_tarifpaketpel', 'class' => 'span2 tarifpaket currency', 'onblur' => 'tarifPaket(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($modPaketPelayanan, '['.$row->daftartindakan_id.']subsidiasuransi', array('parent'=>'SAPaketpelayananM_subsidiasuransi', 'class' => 'span2 subisidiAsuransi currency', 'onblur' => 'tarifAsuransi(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td class='cols_hide'>".CHtml::activeTextField($modPaketPelayanan, '['.$row->daftartindakan_id.']subsidipemerintah', array('parent'=>'SAPaketpelayananM_subsidipemerintah', 'class' => 'span1 subisidiPemerintah currency', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($modPaketPelayanan, '['.$row->daftartindakan_id.']subsidirumahsakit', array('parent'=>'SAPaketpelayananM_subsidirumahsakit',  'class' => 'span2 subisidiRS currency', 'onblur' => 'tarifRs(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($modPaketPelayanan, '['.$row->daftartindakan_id.']iurbiaya', array('readonly'=>true,'parent'=>'SAPaketpelayananM_iurbiaya', 'class' => 'span2 iurBiaya currency','onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::link("<i class='icon-remove'></i>", '', array('href'=>'', 'onclick'=>'remove2(this);return false;'))."</td>
                        </tr>
                        ";

                $data['tr'] = $tr;
            }
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    //-- SistemAdministrator --//
    //function ajax get data Detail Paket BMHP dari sistemAdministrator/paketbmhpM
    public function actionGetPaketBMHP()
    {
        if(Yii::app()->request->isAjaxRequest) {
            
            if (isset($_POST['tipePaket'])){
                $modPaketBMHP = PaketbmhpM::model()->findAllByAttributes(array('tipepaket_id'=>$_POST['tipePaket']));
                if (count($modPaketBMHP) > 0){
                    $data['paket'] = 'Ada';
                }
                else{
                    $data['paket'] = 'Tidak';
                }
            }else{
                $idTipePaket=$_POST['idTipePaket'];
                $idDaftarTindakan = $_POST['idDaftarTindakan'];
                $idObatAlkes = $_POST['idObatAlkes'];
                $qtyPemakaian = $_POST['qtyPemakaian'];
                $hargaPemakaian = $_POST['hargaPemakaian'];
                $modTipePaket = TipepaketM::model()->findByPk($idTipePaket);
                $modDaftarTindakan = DaftartindakanM::model()->findByPk($idDaftarTindakan);
                $modObatAlkes = ObatalkesM::model()->with('satuankecil')->findByPk($idObatAlkes);
                $modPaketBMHP = new PaketbmhpM;

                $tr .="<tr>
                            <td>".CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>" . CHtml::TextField('noUrut', '', array('class' => 'span1 noUrut', 'readonly' => TRUE)) .
                                    CHtml::activeHiddenField($modPaketBMHP, 'tipepaket_id[]', array('value' => $modTipePaket->tipepaket_id)) .
                                    CHtml::activeHiddenField($modPaketBMHP, 'daftartindakan_id[]', array('value' => $modDaftarTindakan->daftartindakan_id)) .
                                    CHtml::activeHiddenField($modPaketBMHP, 'obatalkes_id[]', array('value'=>$idObatAlkes)) .
                                    CHtml::activeHiddenField($modPaketBMHP, 'satuankecil_id[]', array('value'=>$modObatAlkes->satuankecil_id)) .
                                    
                        "</td>
                            <td>" . $modTipePaket->tipepaket_nama . "</td>
                            <td>" . $modDaftarTindakan->daftartindakan_nama . "</td>
                            <td>" . $modObatAlkes->satuankecil->satuankecil_nama . "</td>
                            <td>" . $modObatAlkes->obatalkes_nama . "</td>

                            <td>" . CHtml::activeTextField($modPaketBMHP, 'qtypemakaian[]', array('value' => $qtyPemakaian, 'class' => 'span1 qtypemakaian numbersOnly', 'onkeyup'=>'numberOnly(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>

                            <td>" . CHtml::activeTextField($modPaketBMHP, 'hargapemakaian[]', array('value' => $hargaPemakaian, 'class' => 'span1 hargapemakaian numbersOnly', 'onkeyup'=>'numberOnly(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                        </tr>
                        ";

                $data['tr'] = $tr;
            }
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    //-- SistemAdministrator --//
    //function ajax get data Harga Tipe Paket Pelayanan dari sistemAdministrator/paketpelayananM
    public function actionGetTipePaket()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $idTipePaket = $_POST['idTipePaket'];
            $modTipePaket = TipepaketM::model()->findByPk($idTipePaket);
            $data['asuransi'] = $modTipePaket->paketsubsidiasuransi;
            $data['pemerintah'] = $modTipePaket->paketsubsidipemerintah;
            $data['rs'] = $modTipePaket->paketsubsidirs;
            $data['iurbiaya'] = $modTipePaket->paketiurbiaya;
            $data['kelaspelayanan_id'] = $modTipePaket->kelaspelayanan_id;
            $data['tarifpaketpel'] = $modTipePaket->tarifpaket;
            $modPaket = PaketpelayananM::model()->findAll('tipepaket_id = '.$idTipePaket);
            
            $tr = '';
            if (count($modPaket)>0){
                foreach ($modPaket as $i=>$row){
                    $modTarifTindakan = TariftindakanM::model()->findByAttributes(array('daftartindakan_id' => $row->daftartindakan_id, 'komponentarif_id'=>  6, 'kelaspelayanan_id'=>$modTipePaket->kelaspelayanan_id));
                
                    $tr .= "<tr>
                            <td>".CHtml::link("<i class='icon-remove'></i>", '', array('href'=>'','onclick'=>'remove(this);return false;'))."</td>
                            <td>".CHtml::TextField('noUrut', ($i+1), array('class' => 'span1 noUrut', 'readonly' => TRUE)) .
                                CHtml::activeHiddenField($row, '['.$row->daftartindakan_id.']tipepaket_id') .
                                CHtml::activeHiddenField($row, '['.$row->daftartindakan_id.']daftartindakan_id') .
                                CHtml::activeHiddenField($row, '['.$row->daftartindakan_id.']ruangan_id') .
                                CHtml::activeHiddenField($row, '['.$row->daftartindakan_id.']paketpelayanan_id') ."</td>
                            <td>".$row->tipepaket->tipepaket_nama . "</td>
                            <td>".$row->daftartindakan->daftartindakan_nama . "</td>
                            <td>".CHtml::activeDropDownList($row, '['.$row->daftartindakan_id.']ruangan_id', CHtml::listData(RuanganM::model()->findAll(), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'class' => 'span2 ruangan', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']namatindakan', array( 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::TextField('totaltarif[]', $modTarifTindakan->harga_tariftindakan, array('readonly' => true, 'class' => 'span2 totalTarif', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']tarifpaketpel', array('parent'=>'SAPaketpelayananM_tarifpaketpel', 'class' => 'span1 tarifpaket numbersOnly', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']subsidiasuransi', array('parent'=>'SAPaketpelayananM_subsidiasuransi', 'class' => 'span1 subisidiAsuransi numbersOnly', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']subsidipemerintah', array('parent'=>'SAPaketpelayananM_subsidipemerintah', 'class' => 'span1 subisidiPemerintah numbersOnly', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']subsidirumahsakit', array('parent'=>'SAPaketpelayananM_subsidirumahsakit',  'class' => 'span1 subisidiRS numbersOnly', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                            <td>".CHtml::activeTextField($row, '['.$row->daftartindakan_id.']iurbiaya', array('parent'=>'SAPaketpelayananM_iurbiaya', 'class' => 'span1 iurBiaya numbersOnly', 'onblur' => 'sum(this);', 'onkeypress' => "return $(this).focusNextInputField(event)")) . "</td>
                        </tr>";
                }
            }
            $data['tr'] = $tr;

            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    //-- sistemAdministrator --//
    //function ajax get data Penjamin untuk form Master Tanggungan Penjamin
    public function actionGetPenjamin()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idCaraBayar=$_POST['idCaraBayar'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
            $modPenjamin = PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id'=>$idCaraBayar));
//            $TanggunganPenjamin = TanggunganpenjaminM::model()->with('penjamin')->findAllByAttributes(array('carabayar_id'=>$idCaraBayar));
            if (count($modPenjamin) > 0){
                foreach ($modPenjamin as $i=>$row){
                    $tanggungan = TanggunganpenjaminM::model()->findByAttributes(array('carabayar_id'=>$idCaraBayar, 'penjamin_id'=>$row->penjamin_id, 'kelaspelayanan_id'=>$idKelasPelayanan));
                    if (count($tanggungan) == 1){
                        $tampilDetail = $tanggungan;
                    }
                    else{
                        $tampilDetail = new TanggunganpenjaminM;
                        $tampilDetail->kelaspelayanan_id = $idKelasPelayanan;
                        $tampilDetail->carabayar_id = $idCaraBayar;
                        $tampilDetail->penjamin_id = $row->penjamin_id;
                        $tampilDetail->subsidiasuransitind = 0;
                        $tampilDetail->subsidipemerintahtind=0;
                        $tampilDetail->subsidirumahsakittind=0;
                        $tampilDetail->iurbiayatind=0;
                        $tampilDetail->subsidiasuransioa=0;
                        $tampilDetail->subsidipemerintahoa=0;
                        $tampilDetail->subsidirumahsakitoa=0;
                        $tampilDetail->iurbiayaoa=0;
                        $tampilDetail->persentanggcytopel=0;
                        $tampilDetail->makstanggpel=0;
                    }
                    $tr .="<tr>
                                <td>".CHtml::TextField('noUrut',($i+1),array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                      CHtml::activeHiddenField($tampilDetail, '['.$i.']penjamin_id') .
                                      CHtml::activeHiddenField($tampilDetail, '['.$i.']carabayar_id') .
                                      CHtml::activeHiddenField($tampilDetail, '['.$i.']kelaspelayanan_id') .
                                      CHtml::activeHiddenField($tampilDetail, '['.$i.']tanggunganpenjamin_id') .
                               "</td>
                                <td>".$tampilDetail->penjamin->penjamin_nama."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidiasuransitind',array('group'=>'group1','class'=>'span1 asuransitind numbersOnly', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidipemerintahtind',array('group'=>'group1','class'=>'span1 numbersOnly pemerintahtind', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidirumahsakittind',array('group'=>'group1','class'=>'span1 numbersOnly rumahsakittind', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']iurbiayatind',array('group'=>'group1','class'=>'span1 numbersOnly iurbiayatind', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidiasuransioa',array('group'=>'group2','class'=>'span1 numbersOnly asuransioa', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidipemerintahoa',array('group'=>'group2','class'=>'span1 numbersOnly pemerintahoa', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']subsidirumahsakitoa',array('group'=>'group2','class'=>'span1 numbersOnly rumahsakitoa', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']iurbiayaoa',array('group'=>'group2','class'=>'span1 numbersOnly iurbiayaoa', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']persentanggcytopel',array('group'=>'group3','class'=>'span1 numbersOnly persentanggung', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::activeTextField($tampilDetail,'['.$i.']makstanggpel',array('class'=>'span1 numbersOnly makstanggpel', 'onblur'=>'change(this);', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                                <td>".CHtml::link("<i class='icon-remove'></i>", '', array('href'=>'','onclick'=>'remove(this);return false;'))."</td>
                            </tr>
                            ";

                }
            }
           
           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    //-- Rawat Jalan --//
    //function ajax get data Diagnosa Keperawatan untuk form Transaksi Asuhan Keperawatan
    public function actionGetDiagnosaKeperawatan(){
        if(Yii::app()->request->isAjaxRequest) {
            $idDiagnosaKeperawatan = $_POST['idDiagnosaKeperawatan'];
            $modDiagnosaKeperawatan = DiagnosakeperawatanM::model()->findByPk($idDiagnosaKeperawatan);
            $modRencana = RencanakeperawatanM::model()->findAllByAttributes(array('diagnosakeperawatan_id'=>$modDiagnosaKeperawatan->diagnosakeperawatan_id));
            $modImplementasi = ImplementasikeperawatanM::model()->findAllByAttributes(array('diagnosakeperawatan_id'=>$modDiagnosaKeperawatan->diagnosakeperawatan_id));
            $data1 ='';
            $data2 ='';
            $tr = $tr2 = $tr3 = '';
            if (count($modRencana) > 0 ){
                $data1 .= '<ul id="intervensi">';
                foreach($modRencana as $row){
                    if (empty($row->iskolaborasiintervensi)){
                        $row->iskolaborasiintervensi = 0;
                    }
                    $data1 .= '<li>'.CHtml::checkBox('rencana_intervensi[][]', false, array('value'=>$row->rencanakeperawatan_id, 'onclick'=>'submitIntervensi(this);', 'class'=>'intervensi_check', 'textData'=>$row->rencana_intervensi, 'valuedata'=>$row->rencanakeperawatan_id, 'kolaborasi'=>$row->iskolaborasiintervensi, 'value'=>$row->rencanakeperawatan_id)).'<span>'.$row->rencana_intervensi.'</span></li>';
                }
                $data1 .= '</ul>';
            }
            if (count($modImplementasi) > 0 ){
                $data2 .= '<ul id="implementasi">';
                foreach($modImplementasi as $row){
                    $data2 .= '<li>'.CHtml::checkBox('rencana_implementasi[][]', false, array('onclick'=>'warnai(this)', 'class'=>'implementasi_check','textData'=>$row->implementasi_nama, 'valueData'=>$row->implementasikeperawatan_id, 'value'=>$row->implementasikeperawatan_id)).'<span>'.$row->implementasi_nama.'</span></li>';
                }
                $data2 .= '</ul>';
            }
            if (count($modDiagnosaKeperawatan) > 0){
                $model = new AsuhankeperawatanT;
                $tr .="<tr>
                            <td><div class='input-append'>
                                ".CHtml::textField('nama_diagnosa',$modDiagnosaKeperawatan->diagnosa_keperawatan, array('class'=>'span2','readOnly'=>true))."
                            <span class='add-on'><i class='icon-list-alt'></i></span></div>
                                ".CHtml::activeHiddenField($model, 'diagnosakeperawatan_id[]', array('value'=>$idDiagnosaKeperawatan, 'class'=>'span3', 'onkeypress' => "return $(this).focusNextInputField(event)"))."
                                ".CHtml::hiddenField('urutan[]', '',array('class'=>'span2 urutan', 'onkeypress' => "return $(this).focusNextInputField(event)"))."
                            </td>
                            <td>".$data1."</td>
                            <td>".$data2."</td>
                        </tr>
                        ";
                //<td>".CHtml::activeDropDownList($model, 'evaluasi_assesment[]', CHtml::listData($models, $valueField, $textField), $htmlOptions)($model, 'evaluasi_obbjektif', array('cols'=>3, 'rows=>2', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                $tr2 .="<tr>
                            <td width='160px'><div class='input-append'>
                                ".CHtml::textField('nama_diagnosa',$modDiagnosaKeperawatan->diagnosa_keperawatan, array('class'=>'span2','readOnly'=>true))."
                            <span class='add-on'><i class='icon-list-alt'></i></span></div>
                                ".CHtml::activeHiddenField($model, 'diagnosa_id[]',array('value'=>$modDiagnosaKeperawatan->diagnosa_id, 'class'=>'span2 urutan', 'onkeypress' => "return $(this).focusNextInputField(event)")).
                                  CHtml::hiddenField('urutan[]', '',array('class'=>'span2 urutan', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextArea($model, 'evaluasi_subjektif[]', array('cols'=>3, 'rows=>2', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextArea($model, 'evaluasi_objektif[]', array('cols'=>3, 'rows=>2', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeDropDownList($model, 'evaluasi_assesment[]', Evaluasiassesment::items(), array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextArea($model, 'askep_tujuan[]', array('cols'=>3, 'rows=>2', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>".CHtml::activeTextArea($model, 'askep_kriteriahasil[]', array('cols'=>3, 'rows=>2', 'class'=>'span2', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        </tr>";
                $tr3 .= "<tr>
                            
                            <td width='160px'><div class='input-append'>
                                ".CHtml::textField('nama_diagnosa',$modDiagnosaKeperawatan->diagnosa_keperawatan, array('class'=>'span2','readOnly'=>true))."
                            <span class='add-on'><i class='icon-list-alt'></i></span></div>
                                              ".CHtml::hiddenField('urutan[]', '',array('class'=>'span2 urutan', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                            <td>
                            <table class='block'>
                                <tr>
                                    <td>
                                        <div class='boxtindakan'>
                                            <h6>Intervensi</h6>
                                            <div class='isi_inter'>
                                                <ul></ul>
                                            </div>
                                        </div>
                                     </td>
                                    <td>
                                        <div class='boxtindakan'>
                                            <h6>Yang Diambil</h6>
                                            <div class='ambil_inter'>
                                                <ul></ul>
                                            </div>
                                        </div>
                                     </td>
                                     </tr>
                                     <tr>
                                    <td>
                                        <div class='boxtindakan'>
                                            <h6>Kolaborasi</h6>
                                            <div class='isi_kolab'>
                                                <ul></ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class='boxtindakan'>
                                            <h6>Yang Diambil</h6>
                                            <div class='ambil_kolab'>
                                                <ul></ul>
                                            </div>
                                        </div>
                                    </td>
                              </tr>
                          </table>
                          </td>
                        </tr>
                            ";
            }
           $data['tr']=$tr;
           $data['tr2']=$tr2;
           $data['tr3']=$tr3;
//           $data['jam']=$jam;
           $data['id']=$idDiagnosaKeperawatan;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
   
    //-- Rawat Jalan --//
    //function ajax get Text Tekanan Darah untuk form Pemeriksaan Fisik
    public function actionGetTextTekananDarah(){
        if(Yii::app()->request->isAjaxRequest){
            $systolic = $_POST['systolic'];
            $diastolic = $_POST['diastolic'];
            $criteria2 = new CDbCriteria();
            $criteria2->select = 'max(systolic_min) as sys_max';
            $modSys = SysdiaM::model()->find($criteria2);
            $criteria3 = new CDbCriteria();
            $criteria3->select = 'max(diastolic_min) as dias_max';
            $modDia = SysdiaM::model()->find($criteria3);
            
            $criteria = new CDbCriteria();
            
            if ($systolic > $modSys->sys_max){
                $criteria->condition = 'systolic_min <= '.$systolic.' and systolic_max = 0';
            }else{
                $criteria->addCondition($systolic.' >= systolic_min');
                $criteria->addCondition($systolic.' <= systolic_max');
            }
            
            if ($diastolic > $modDia->dias_max){
                $criteria->condition = 'diastolic_min <= '.$diastolic.' and diastolic_max = 0';
            }else{
                $criteria->addCondition($diastolic.' >= diastolic_min');
                $criteria->addCondition($diastolic.' <= diastolic_max');
            }
            
            $modSysDia = SysdiaM::model()->find($criteria);
            $data['text'] = $modSysDia->sysdia_nama;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    //-- Rawat Jalan --//
    //function ajax get Text Tekanan Body Mass Index untuk form Pemeriksaan Fisik
    public function actionGetBMIText(){
        if (Yii::app()->request->isAjaxRequest){
            $bmi = $_POST['bmi'];
            $criteria2 = new CDbCriteria();
            $criteria2->select = 'max(bmi_minimum) as max_bmi';
            $modBMI = BodymassindexM::model()->find($criteria2);
            $criteria = new CDbCriteria();
            //$criteria->addCondition($bmi.' >= bmi_minimum');
            
            if ($bmi > $modBMI->max_bmi){
                 $criteria->condition = 'bmi_minimum <= '.$bmi.' and bmi_maksimum = 0';
                 //$criteria->condition('0 <= bmi_maksimum');
            }else{
                $criteria->addCondition($bmi.' >= bmi_minimum');
                $criteria->addCondition($bmi.' <= bmi_maksimum');
            }
            //echo $bmi;exit();
            //$criteria->order='bmi_minimum ASC';
            
            $data['text'] = BodymassindexM::model()->find($criteria)->bmi_defenisi;
            echo json_encode($data);
            Yii::app()->end();
        }
    }

    public function actionMasterKeluhan()
    {
        if (Yii::app()->request->isAjaxRequest){
            $criteria = new CDbCriteria;
            $criteria->compare('LOWER(keluhananamnesis_nama)', strtolower($_GET['tag']),true);
            $keluhans = KeluhananamnesisM::model()->findAll($criteria);
            foreach ($keluhans as $i => $keluhan) {
                $data[$i] = array('key'=>$keluhan->keluhananamnesis_nama,
                                  'value'=>$keluhan->keluhananamnesis_nama);
            }

            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }

    public function actionMasterKeadaanUmum()
    {
        if (Yii::app()->request->isAjaxRequest){
            $criteria = new CDbCriteria;
            $criteria->compare('LOWER(keadaanumum_nama)', strtolower($_GET['tag']),true);
            $keluhans = KeadaanumumM::model()->findAll($criteria);
            foreach ($keluhans as $i => $keluhan) {
                $data[$i] = array('key'=>$keluhan->keadaanumum_nama,
                                  'value'=>$keluhan->keadaanumum_nama);
            }

            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }

    //-- Rawat Jalan --//
    //function ajax get Value from device untuk form Pemeriksaan Fisik
    public function actionGetfromDevice(){
        if (Yii::app()->request->isAjaxRequest){
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                $file = dirname('c:/OstarP2/x').'/OstarXML.xml';
            } else {
                $file = Yii::app()->getBaseUrl('webroot').'/data/xml/ostar.xml';
            }
            
            $data2 = simplexml_load_file($file);
            $a = $data2->BPMRecord[0]['H'];
            $b = $data2->BPMRecord[0]['L'];
            $c = $data2->BPMRecord[0]['P'];

            $tambah = '';
            if (strlen($a) < 3){
                for($i = strlen($a); $i < 3; $i++){
                    $tambah = $tambah.'0';
                }
                $a = $tambah.$a;
            }
            $tambah = '';
            if (strlen($b) < 3){
                for($i = strlen($b); $i < 3; $i++){
                    $tambah = $tambah.'0';
                }
                $b = $tambah.$b;
            }
            
            $data['sys'] = "$a";
            $data['dias'] = "$b";
            $data['detaknadi'] = "$c";
            $data['tekanandarah'] = $a.' / '.$b;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionGetScoreApacheRI(){
        if (Yii::app()->request->isAjaxRequest){
            $score = $_POST['score'];
            $id = $_POST['id'];
            $id = explode('_',$id);
            if (!empty($id[3])){
                $aktif = true;
            }else{
                $aktif = false;
            }
            $scoreId = $id[2];

            $criteria2 = new CDbCriteria();
            $criteria2->select = 'max(point_minimal) as max_point';
            $criteria2->compare('apachescore_id',$scoreId);
            if (($aktif == true)&&($scoreId == 10)){
                $criteria2->compare('point_arf',true);
            }
            $modApache = ScorepointM::model()->find($criteria2);

            $criteria = new CDbCriteria();
            $criteria->compare('apachescore_id',$scoreId);
            if ($score > $modApache->max_point){
                if (($aktif == true)&&($scoreId == 10)){
                    $criteria->compare('point_arf',true);
                }
                $criteria->addCondition('point_minimal <= '.$score.' and point_maksimal = 0');
            }else{
                $criteria->addCondition($score.' >= point_minimal');
                $criteria->addCondition($score.' <= point_maksimal');
                
            }

            $data['pointscore']= ScorepointM::model()->find($criteria)->point_score;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionCariPasien()
    {
        if (Yii::app()->request->isAjaxRequest){
            $noRM = $_POST['norekammedik'];
            
            $model = PasienM::model()->findByAttributes(array('no_rekam_medik'=>$noRM));
            $attributes = $model->attributeNames();
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            
            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }

    public function actionGetJadwalMakan()
    {
       if(Yii::app()->request->isAjaxRequest) {
           $jeniswaktu = $_POST['jeniswaktu'];
            $jenisdietid=$_POST['jenisdietid'];
            $tipedietid = $_POST['tipedietid'];
            $jeniswaktuid = $_POST['jeniswaktuid'];
            $menudietid = $_POST['menudietid'];
            $modJadwalMakan = new JadwalMakanM;
            $modJenisdiet = JenisdietM::model()->findByPk($jenisdietid);
            $modTipeDiet=TipeDietM::model()->findByPK($tipedietid);
            $modJenisWaktu = JenisWaktuM::model()->findByPK($jeniswaktuid);
            
            $modMenuDiet = MenuDietM::model()->findByPK($menudietid);
            $return = array();
            $tipejeniswaktu = JenisWaktuM::model()->findAll('jeniswaktu_aktif = true ORDER BY jeniswaktu_id');
            
                $tr .="<tr><td>";
                $tr .= CHtml::checkBox('JadwalMakanM[][checkList]',true,array('class'=>'cekList'));
                $tr .= "</td><td>";
                $tr .= $modJenisdiet->jenisdiet_nama;
                $tr .= CHtml::activeHiddenField($modJadwalMakan, '[]jenisdiet_id', array('value'=>$modJenisdiet->jenisdiet_id));
                $tr .= CHtml::activeHiddenField($modJadwalMakan, '[]tipediet_id',array('value'=>$modTipeDiet->tipediet_id));
                $tr .= CHtml::activeHiddenField($modJadwalMakan, '[]jeniswaktu_id',array('value'=>$modJenisWaktu->jeniswaktu_id));
                $tr .= "</td><td>";
                $tr .= $modTipeDiet->tipediet_nama;
                $tr .= "</td>";
                
                foreach ($tipejeniswaktu as $waktu){
                    if (in_array($waktu->jeniswaktu_id, $jeniswaktuid)){
                    $tr .= "<td>";
                    $tr .= CHtml::hiddenField('JadwalMakanM[][menudiet_id]['.$waktu->jeniswaktu_id.']',$modMenuDiet->menudiet_id, array('class'=>'menudiet'));
                    $tr .= '<div class="input-append">';
                    $tr .= CHtml::textField('namaMenuDiet',$modMenuDiet->menudiet_nama, array('class'=>'adamenudiet span2'));
                    $tr .= '<span class="add-on"><a href="javascript:void(0);" onclick="openDialog(this);return false;"><i class="icon-list-alt icon-search"></i><i class="icon-search"></i></a></span>';
                    $tr .= '</div>';
//                    $tr .= $modMenuDiet->menudiet_nama;
                    $tr .="</td>";
                    }
                    else{
                        $tr .= "<td>";
                    $tr .= CHtml::hiddenField('JadwalMakanM[][menudiet_id]['.$waktu->jeniswaktu_id.']', '', array('class'=>'menudiet'));
                    $tr .= '<div class="input-append">';
                    $tr .= CHtml::textField('namaMenuDiet','', array('class'=>'adamenudiet span2'));
                    $tr .= '<span class="add-on"><a href="javascript:void(0);" onclick="openDialog(this);return false;"><i class="icon-list-alt icon-search"></i><i class="icon-search"></i></a></span>';
                    $tr .= '</div>';
//                    $tr .= $modMenuDiet->menudiet_nama;
                    $tr .="</td>";
                    }
                }
                    
                $tr .= "</tr>";
            $return .= $tr;
           $data['return']=$return;
           echo json_encode($data);
         Yii::app()->end();
        }
    }

    public function actionGetBahanMakanan(){
        
        
      if (Yii::app()->request->isAjaxRequest){
            
            
//            $tr = '<tr>
//                    <td>'
//                        .CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))
////                        .CHtml::checkBox($modDetail,'checkList[]', array('value'=>1, 'class'=>'cekList','onclick'=>'hitungSemua()'))
//                        .CHtml::activeHiddenField($modDetail,'golbahanmakanan_id[]', array('value'=>$model->golbahanmakanan_id))
//                        .CHtml::activeHiddenField($modDetail,'bahanmakanan_id[]', array('value'=>$model->bahanmakanan_id))
//                        .CHtml::activeHiddenField($modDetail,'jmlkemasan[]', array('value'=>$model->jmldlmkemasan))
//                        .CHtml::activeHiddenField($modDetail,'harganettobhn[]', array('value'=>$model->harganettobahan))
//                        .CHtml::activeHiddenField($modDetail,'ukuranbahan[]', array('value'=>$ukuran))
//                        .CHtml::activeHiddenField($modDetail,'merkbahan[]', array('value'=>$merk))
//                    .'</td>
//                    <td>'.CHtml::textField('noUrut[]',true,array('class'=>'noUrut span1', 'readonly'=>true)).'</td>
//                    <td>'.$model->golbahanmakanan->golbahanmakanan_nama.'</td>
//                    <td>'.$model->jenisbahanmakanan.'</td>
//                    <td>'.$model->kelbahanmakanan.'</td>
//                    <td>'.$model->namabahanmakanan.'</td>
//                    <td>'.$model->jmlpersediaan.'</td>
//                    <td>'.CHtml::activeDropDownList($modDetail, 'satuanbahan[]', Satuanbahan::items(), array('class'=>'satuanbahan span1')).'</td>
//                    <td>'.$model->harganettobahan.'</td>
//                    <td>'.$model->hargajualbahan.'</td>
//                    <td>'.$model->discount.'</td>
//                    <td>'.$model->tglkadaluarsabahan.'</td>
//
//                    <td>'.CHtml::activeTextField($modDetail, 'qtypengajuan[]', array('value'=>$qty, 'class'=>'span1 numbersOnly qty', 'onkeyup'=>'hitung(this);')).'</td>
//                    <td>'.CHtml::TextField('subNetto[]',$subNetto,array('value'=>$subNetto, 'class'=>'subNetto span2','readonly'=>true)).'</td>
//                    </tr>';
            
            $idBahan = $_POST['id'];
            $qty = $_POST['qty'];
            $ukuran = $_POST['ukuran'];
            $merk = $_POST['merk'];
            $satuanbahan = $_POST['satuanbahan'];
            
            if (!is_numeric($qty)){
                $qty = 0;
            }
            
            $model = BahanmakananM::model()->with('golbahanmakanan')->findByPk($idBahan);
            if ($satuanbahan != $model->satuanbahan){
                $model->satuanbahan = $satuanbahan;
            }
            
            $modDetail = new PengajuanbahandetailT;
            $subNetto = $qty*$model->harganettobahan;
            
            $nourut = 1;
                $tr ="<tr>
                        <td>".CHtml::checkBox('checkList',true,array('class'=>'cekList','onclick'=>'hitungSemua();')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']golbahanmakanan_id',array('value'=>$model->golbahanmakanan_id, 'class'=>'golbahanmakanan_id')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']bahanmakanan_id',array('value'=>$model->bahanmakanan_id, 'class'=>'bahanmakanan_id')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']jmlkemasan',array('value'=>$model->jmldlmkemasan, 'class'=>'jmldlmkemasan')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']harganettobhn',array('value'=>$model->harganettobahan, 'class'=>'harganettobhn')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']ukuranbahan',array('value'=>$ukuran, 'class'=>'ukuranbahan')).
                              CHtml::activeHiddenField($modDetail,'['.$idBahan.']merkbahan',array('value'=>$merk, 'class'=>'merkbahan')).
                       "</td>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE))."</td>
                        <td>".$model->golbahanmakanan->golbahanmakanan_nama."</td>
                        <td>".$model->jenisbahanmakanan."</td>
                        <td>".$model->kelbahanmakanan."</td>
                        <td>".$model->namabahanmakanan."</td>
                        <td>".$model->jmlpersediaan."</td>
                        <td>".CHtml::activeDropDownList($modDetail,'['.$idBahan.']satuanbahan', Satuanbahan::items(), array('class'=>'satuanbahan span1'))."</td>
                        <td>".$model->harganettobahan."</td>
                        <td>".$model->hargajualbahan."</td>
                        <td>".$model->discount."</td>
                        <td>".$model->tglkadaluarsabahan."</td>
                        <td>".CHtml::activetextField($modDetail,'['.$idBahan.']qtypengajuan',array('value'=>$qty,'class'=>'span1 numbersOnly qty','onkeyup'=>'hitung(this);'))."</td>
                        <td>".CHtml::activetextField($modDetail,'['.$idBahan.']subNetto',array('value'=>$subNetto,'class'=>'span1 numbersOnly subNetto','readonly'=>true))."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;', 'class'=>'cancel'))."</td>
                      </tr>";
               $data['tr']=$tr;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    public function actionGetBahanMakananDariPenerimaan(){
        if (Yii::app()->request->isAjaxRequest){
            $idBahan = $_POST['id'];
            $qty = $_POST['qty'];
            $ukuran = $_POST['ukuran'];
            $merk = $_POST['merk'];
            $satuanbahan = $_POST['satuanbahan'];
            if (!is_numeric($qty)){
                $qty = 0;
            }
            $model = BahanmakananM::model()->with('golbahanmakanan')->findByPk($idBahan);
            $modDetail = new TerimabahandetailT();
            $subNetto = $qty*$model->harganettobahan;
//            $modDetail->satuanbahan[] = $satuanbahan;
            $tr = '<tr>
                    <td>'
                        .CHtml::checkBox('checkList[]',true,array('class'=>'cekList','onclick'=>'hitungSemua()'))
                        .CHtml::activeHiddenField($modDetail, 'golbahanmakanan_id[]', array('value'=>$model->golbahanmakanan_id))
                        .CHtml::activeHiddenField($modDetail, 'bahanmakanan_id[]', array('value'=>$model->bahanmakanan_id))
                        .CHtml::activeHiddenField($modDetail, 'harganettobhn[]', array('value'=>$model->harganettobahan))
                        .CHtml::activeHiddenField($modDetail, 'jmlkemasan[]', array('value'=>$model->jmldlmkemasan))
                        .CHtml::activeHiddenField($modDetail, 'hargajualbhn[]', array('value'=>$model->hargajualbahan))
                        .CHtml::activeHiddenField($modDetail, 'ukuran_bahanterima[]', array('value'=>$ukuran))
                        .CHtml::activeHiddenField($modDetail, 'merk_bahanterima[]', array('value'=>$merk))
                    .'</td>
                    <td>'.CHtml::textField('noUrut[]',true,array('class'=>'noUrut span1', 'readonly'=>true)).'</td>
                    <td>'.$model->golbahanmakanan->golbahanmakanan_nama.'</td>
                    <td>'.$model->jenisbahanmakanan.'</td>
                    <td>'.$model->kelbahanmakanan.'</td>
                    <td>'.$model->namabahanmakanan.'</td>
                    <td>'.$model->jmlpersediaan.'</td>
                    <td>'.CHtml::activeDropDownList($modDetail, 'satuanbahan[]', Satuanbahan::items(), array( 'class'=>'span1 satuanbahan')).'</td>
                    <td>'.$model->harganettobahan.'</td>
                    <td>'.$model->hargajualbahan.'</td>
                    <td>'.CHtml::activeTextField($modDetail, 'discount[]', array('value'=>$model->discount, 'class'=>'discount span1 numbersOnly', 'onkeyup'=>'hitungTotalDiscount();')).'</td>
                    <td>'.$model->tglkadaluarsabahan.'</td>
                    
                    <td>'.CHtml::activeTextField($modDetail, 'qty_terima[]', array('value'=>$qty, 'class'=>'span1 numbersOnly qty', 'onkeyup'=>'hitung(this);')).'</td>
                    <td>'.CHtml::TextField('subNetto[]',$subNetto,array('value'=>$subNetto, 'class'=>'subNetto span2','readonly'=>true)).'</td>
                    </tr>';
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMenuDietDetail(){
        if (Yii::app()->request->isAjaxRequest){
            $idPasien = $_POST['idPasien'];
            $idPendaftaran = $_POST['idPendaftaran'];
            $idPasienAdmisi = $_POST['idPasienAdmisi'];
            $idMenuDiet = $_POST['idMenuDiet'];
            $idRuangan = $_POST['idRuangan'];
            $idInstalasi = $_POST['idInstalasi'];
            $urt = $_POST['urt'];
            $jumlah = $_POST['jumlah'];
            $jeniswaktu = $_POST['jeniswaktu'];
            $pendaftaranId = $_POST['pendaftaranId'];
            $pasienAdmisi = $_POST['pasienAdmisi'];
            $modDetail = new PesanmenudetailT();
            $modJenisWaktu = JeniswaktuM::model()->findAll('jeniswaktu_aktif = true');
            $diet = MenuDietM::model()->findByPK($idMenuDiet);
            $jumlahPasien = count($pasienAdmisi);
            if ($jumlahPasien == 0){
                $jumlahPasien = 1;
            }
            for($i = 0; $i < $jumlahPasien; $i++) {
            $modDetail = new PesanmenudetailT();
                if (empty($pasienAdmisi)) {
                    $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $idPendaftaran, 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $idPasienAdmisi));
                } else {
                    $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $pendaftaranId[$i], 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $pasienAdmisi[$i]));
//                    echo print_r($model->attributes);
//                    exit();
                }
                $tr .= '<tr>
                        <td>'
//                            .CHtml::activeHiddenField($modDetail, '[]ruangan_id',array('value'=>$model->ruangan_id))
                            .CHtml::checkBox('PesanmenudetailT[][checkList]',true, array('class'=>'cekList','onclick'=>'hitungSemua()'))
                            .CHtml::activeHiddenField($modDetail, '[]pendaftaran_id', array('value'=>$model->pendaftaran_id))
                            .CHtml::activeHiddenField($modDetail, '[]pasien_id', array('value'=>$model->pasien_id))
                            .CHtml::activeHiddenField($modDetail, '[]pasienadmisi_id', array('value'=>$model->pasienadmisi_id))
                        .'</td>
                        <td>'.RuanganM::model()->with('instalasi')->findByPk($idRuangan)->instalasi->instalasi_nama.'</td>
                        <td>'.$model->ruangan_nama.'/<br/>'.$model->no_pendaftaran.'</td>
                        <td>'.$model->no_rekam_medik.'/<br/>'.$model->nama_pasien.'</td>
                        <td>'.$model->umur.'</td>
                        <td>'.$model->jeniskelamin.'</td>';
                foreach ($modJenisWaktu as $v){
                    if (in_array($v->jeniswaktu_id, $jeniswaktu)){
                        $tr .='<td>'.CHtml::hiddenField('PesanmenudetailT[][jeniswaktu_id]['.$v->jeniswaktu_id.']', $v->jeniswaktu_id )
                       .CHtml::dropDownList('PesanmenudetailT[][menudiet_id]['.$v->jeniswaktu_id.']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty'=>'-- Pilih --', 'class'=>'span2 menudiet', 'options'=>array("$idMenuDiet"=>array("selected"=>"selected")))).'</td>';
                    }else{
                        $tr .='<td>'.CHtml::hiddenField('PesanmenudetailT[][jeniswaktu_id]['.$v->jeniswaktu_id.']', $v->jeniswaktu_id )
                       .CHtml::dropDownList('PesanmenudetailT[][menudiet_id]['.$v->jeniswaktu_id.']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty'=>'-- Pilih --', 'class'=>'span2 menudiet', )).'</td>';
                    }
                }
                 $tr .='<td>'.CHtml::activeTextField($modDetail, '[]jml_pesan_porsi', array('value'=>$jumlah, 'class'=>' span1 numbersOnly')).'</td>
                        <td>'.CHtml::activeDropDownList($modDetail, '[]satuanjml_urt', Ukuranrumahtangga::items(), array('empty'=>'-- Pilih --', 'class'=>'span2 urt', 'options'=>array("$urt"=>array("selected"=>"selected")))).'</td>
                        </tr>';
            }
            
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMenuDietDetailKirim(){
        if (Yii::app()->request->isAjaxRequest){
            $idPasien = $_POST['idPasien'];
            $idPendaftaran = $_POST['idPendaftaran'];
            $idPasienAdmisi = $_POST['idPasienAdmisi'];
            $idMenuDiet = $_POST['idMenuDiet'];
            $idRuangan = $_POST['idRuangan'];
            $idInstalasi = $_POST['idInstalasi'];
            $idDaftarTindakan = $_POST['idDaftarTindakan'];
            $idKelasPelayanan = $_POST['idKelasPelayanan'];
//            $satuanTarif = $_POST['satuanTarif'];
            $urt = $_POST['urt'];
            $jumlah = $_POST['jumlah'];
            $jeniswaktu = $_POST['jeniswaktu'];
            $pendaftaranId = $_POST['pendaftaranId'];
            $pasienAdmisi = $_POST['pasienAdmisi'];
            $modDetail = new PesanmenudetailT();
            $modJenisWaktu = JeniswaktuM::model()->findAll('jeniswaktu_aktif = true');
            $diet = MenuDietM::model()->findByPK($idMenuDiet);
            $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            $tarifTindakan = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$idDaftarTindakan,'kelaspelayanan_id'=>$idKelasPelayanan,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
              foreach($tarifTindakan as $key=>$tarif){
                  if(count($tarif) > 0){
                      $satuanTarif = $tarif->harga_tariftindakan;
                  }else{
                      $tarifTindakan = TariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$idDaftarTindakan,'kelaspelayanan_id'=>Params::KELASPELAYANAN_ID_TANPA_KELAS,'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                      foreach($tarifTindakan as $key=>$tarif){
                          if(count($tarif) > 0){
                              $satuanTarif = $tarif->harga_tariftindakan;
                          }else{
                              $satuanTarif = 0;
                          }
                      }
                  }

              }
              
            $jumlahPasien = count($pasienAdmisi);
            if ($jumlahPasien == 0){
                $jumlahPasien = 1;
            }
            for($i = 0; $i < $jumlahPasien; $i++) {
            $modDetail = new PesanmenudetailT();
//            echo $pasienAdmisi;exit;
                if (!empty($idPasienAdmisi)) {
                    $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $idPendaftaran, 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $idPasienAdmisi));
                    $modPendaftaran = PendaftaranT::model()->findByAttributes(array('pendaftaran_id' => $idPendaftaran, 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $idPasienAdmisi));
                } else {
                    $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $pendaftaranId[$i]));
                    $modPendaftaran = PendaftaranT::model()->findByAttributes(array('pendaftaran_id' => $pendaftaranId[$i]));
                }
                $tr .= '<tr>
                        <td>'  //  .CHtml::activeHiddenField($modDetail, '[]ruangan_id',array('value'=>$model->ruangan_id))
                            .CHtml::checkBox('PesanmenudetailT[][checkList]',true, array('class'=>'cekList','onclick'=>'hitungSemua()'))
                            .CHtml::activeHiddenField($modDetail, '[]pendaftaran_id', array('value'=>$model->pendaftaran_id))
                            .CHtml::activeHiddenField($modDetail, '[]pasien_id', array('value'=>$model->pasien_id))
                            .CHtml::activeHiddenField($modDetail, '[]pasienadmisi_id', array('value'=>$model->pasienadmisi_id))
                        .'</td>
                        <td>'.RuanganM::model()->with('instalasi')->findByPk($idRuangan)->instalasi->instalasi_nama.'</td>
                        <td>'.$model->ruangan_nama.'/<br/>'.$model->no_pendaftaran.'</td>
                        <td>'.$model->no_rekam_medik.'/<br/>'.$model->nama_pasien.'</td>
                        <td>'.$model->umur.'</td>
                        <td>'.$model->jeniskelamin.'</td>';
                foreach ($modJenisWaktu as $v){
                    if (in_array($v->jeniswaktu_id, $jeniswaktu)){
                        $tr .='<td>'.CHtml::hiddenField('PesanmenudetailT[][jeniswaktu_id]['.$v->jeniswaktu_id.']', $v->jeniswaktu_id )
                       .CHtml::hiddenField('PesanmenudetailT[][daftartindakan_id]['.$v->jeniswaktu_id.']', $idDaftarTindakan)
                            .CHtml::hiddenField('PesanmenudetailT[][carabayar_id]['.$v->jeniswaktu_id.']', $model->carabayar_id)
                        .CHtml::hiddenField('PesanmenudetailT[][penjamin_id]['.$v->jeniswaktu_id.']', $model->penjamin_id)
                       .CHtml::hiddenField('PesanmenudetailT[][kelaspelayanan_id]['.$v->jeniswaktu_id.']', $idKelasPelayanan)
                       .CHtml::hiddenField('PesanmenudetailT[][jeniskasuspenyakit_id]['.$v->jeniswaktu_id.']', $model->jeniskasuspenyakit_id)
                       .CHtml::textField('PesanmenudetailT[][satuanTarif]['.$v->jeniswaktu_id.']', $satuanTarif,array('class'=>'span2'))
                       .CHtml::dropDownList('PesanmenudetailT[][menudiet_id]['.$v->jeniswaktu_id.']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty'=>'-- Pilih --', 'class'=>'span2 menudiet', 'options'=>array("$idMenuDiet"=>array("selected"=>"selected")))).'</td>';
                    }else{
                        $tr .='<td>'.CHtml::hiddenField('PesanmenudetailT[][jeniswaktu_id]['.$v->jeniswaktu_id.']', $v->jeniswaktu_id )
                        .CHtml::hiddenField('PesanmenudetailT[][carabayar_id]['.$v->jeniswaktu_id.']', $model->carabayar_id)
                        .CHtml::hiddenField('PesanmenudetailT[][penjamin_id]['.$v->jeniswaktu_id.']', $model->penjamin_id)
                       .CHtml::hiddenField('PesanmenudetailT[][daftartindakan_id]['.$v->jeniswaktu_id.']', $idDaftarTindakan)
                        .CHtml::hiddenField('PesanmenudetailT[][kelaspelayanan_id]['.$v->jeniswaktu_id.']', $idKelasPelayanan)
                        .CHtml::hiddenField('PesanmenudetailT[][jeniskasuspenyakit_id]['.$v->jeniswaktu_id.']', $model->jeniskasuspenyakit_id)
                        .CHtml::textField('PesanmenudetailT[][satuanTarif]['.$v->jeniswaktu_id.']', $satuanTarif,array('class'=>'span2','style'=>'width:60px;'))
                       .CHtml::dropDownList('PesanmenudetailT[][menudiet_id]['.$v->jeniswaktu_id.']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty'=>'-- Pilih --', 'class'=>'span2 menudiet', )).'</td>';
                    }
                }
                 $tr .='<td>'.CHtml::activeTextField($modDetail, '[]jml_kirim', array('value'=>$jumlah, 'class'=>' span1 numbersOnly')).'</td>
                        <td>'.CHtml::activeDropDownList($modDetail, '[]satuanjml_urt', Ukuranrumahtangga::items(), array('empty'=>'-- Pilih --','style'=>'width:80px;',  'class'=>'span2 urt', 'options'=>array("$urt"=>array("selected"=>"selected")))).'</td>
                        <td>'.CHtml::activeDropDownList($modDetail, '[]status_menu', StatusMakanan::items(), array('empty'=>'-- Pilih --','style'=>'width:80px;', 'class'=>'span2 urt', 'options'=>array("SASET"=>array("selected"=>"selected")))).'</td>
                        </tr>';
            }
            
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMenuDietDetailDariKirim(){
        if (Yii::app()->request->isAjaxRequest){
            $idPendaftaran = $_POST['idPendaftaran'];
            $idPasienAdmisi = $_POST['idPasienAdmisi'];
            $idMenuDiet = $_POST['idMenuDiet'];
            $idRuangan = $_POST['idRuangan'];
            $idInstalasi = $_POST['idInstalasi'];
            $urt = $_POST['urt'];
            $jumlah = $_POST['jumlah'];
            $jeniswaktu = $_POST['jeniswaktu'];
            $pendaftaranId = $_POST['pendaftaranId'];
            $pasienAdmisi = $_POST['pasienAdmisi'];
            $butuh = $_POST['butuh'];
            $total = $_POST['total'];
            if (isset($butuh)){
                if(in_array($idMenuDiet, $butuh)){
                    foreach ($butuh as $i=>$dataRow){
                        if($dataRow == $idMenuDiet){
                            $total = $total[$i];
                        }
                    }
                }
                else{
                    $total = 0;
                }
            }
            else{
                $total = 0;
            }
            $modJenisWaktu = JeniswaktuM::model()->findAll('jeniswaktu_aktif = true');
            $jumlahPasien = count($pasienAdmisi);
            if ($jumlahPasien == 0){
                $jumlahPasien = 1;
            }
            $hasil = true;
            if (KonfigsystemK::getKonfigKurangiStokGizi() == true){
                $bahanMenu = BahanMenuDietM::model()->findAllByAttributes(array('menudiet_id'=>$idMenuDiet));
                $kelipatan = count(JeniswaktuM::getJenisWaktu());
                if (count($bahanMenu) > 0){
                    foreach ($bahanMenu as $v){
                        if ($total !=0){
                            $jumlahButuh = $kelipatan*$v->jmlbahan*($jumlah+($total/$kelipatan))*$jumlahPasien;
                        }
                        else{
                            $jumlahButuh = $kelipatan*$v->jmlbahan*$jumlah*$jumlahPasien;
                        }
                        
                        if (StokbahanmakananT::validasiStok($jumlahButuh, $v->bahanmakanan_id) == false){
                            $hasil = false;
                        }
                    }
                }
                else{
                    $hasil = false;
                }
            }
            
            if ($hasil == true){
                for($i = 0; $i < $jumlahPasien; $i++) {
                    $modDetail = new KirimmenupasienT;
                    if (empty($pasienAdmisi)) {
                        $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $idPendaftaran, 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $idPasienAdmisi));
                    } else {
                        $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id' => $pendaftaranId[$i], 'ruangan_id' => $idRuangan, 'pasienadmisi_id' => $pasienAdmisi[$i]));
                    }
                    $tr .= '<tr>
                                <td>'
                            . CHtml::checkBox('KirimmenupasienT[][checkList]', true, array('class' => 'cekList', 'onclick' => 'hitungSemua()'))
                            . CHtml::activeHiddenField($modDetail, '[]ruangan_id', array('value' => $model->ruangan_id))
                            . CHtml::activeHiddenField($modDetail, '[]pendaftaran_id', array('value' => $model->pendaftaran_id))
                            . CHtml::activeHiddenField($modDetail, '[]pasien_id', array('value' => $model->pasien_id))
                            . CHtml::activeHiddenField($modDetail, '[]pasienadmisi_id', array('value' => $model->pasienadmisi_id))
                            . '</td>
                                <td>' . RuanganM::model()->with('instalasi')->findByPk($idRuangan)->instalasi->instalasi_nama . '/
                                <br/>' . $model->ruangan_nama . '</td>
                                <td>' . $model->no_pendaftaran . '/
                                <br/>' . $model->no_rekam_medik . '</td>
                                <td>' . $model->nama_pasien . '</td>
                                <td>' . $model->umur . '</td>
                                <td>' . $model->jeniskelamin . '</td>';
                    foreach ($modJenisWaktu as $v) {
                        if (in_array($v->jeniswaktu_id, $jeniswaktu)) {
                            $tr .='<td>' . CHtml::hiddenField('KirimmenupasienT[][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                    . CHtml::dropDownList('KirimmenupasienT[][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('onchange'=>'cekStokMenu(this)','empty' => '-- Pilih --', 'class' => 'span2 menudiet', 'options' => array("$idMenuDiet" => array("selected" => "selected")))) . '</td>';
                        } else {
                            $tr .='<td>' . CHtml::hiddenField('KirimmenupasienT[][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                    . CHtml::dropDownList('KirimmenupasienT[][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('onchange'=>'cekStokMenu(this)','empty' => '-- Pilih --', 'class' => 'span2 menudiet',)) . '</td>';
                        }
                    }
                    $tr .='<td>' . CHtml::activeTextField($modDetail, '[]jml_kirim', array('value' => $jumlah, 'class' => ' span1 numbersOnly jmlKirim', 'onblur'=>'cekStokMenuInput(this)')) . '</td>
                                <td>' . CHtml::activeDropDownList($modDetail, '[]satuanjml_urt', Ukuranrumahtangga::items(), array('empty' => '-- Pilih --', 'class' => 'span2 urt', 'options' => array("$urt" => array("selected" => "selected")))) . '</td>
                                </tr>';
                }
            }
            
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMenuDietPegawai(){
        if (Yii::app()->request->isAjaxRequest){
            $idPegawai = $_POST['idPegawai'];
            $idMenuDiet = $_POST['idMenuDiet'];
            $idRuangan = $_POST['idRuangan'];
            $idInstalasi = $_POST['idInstalasi'];
            $urt = $_POST['urt'];
            $jumlah = $_POST['jumlah'];
            $jeniswaktu = $_POST['jeniswaktu'];
            $modDetail = new PesanmenupegawaiT();
            $pegawaiId = $_POST['pegawaiId'];
            
            $jumlahPesan = count($pegawaiId);
            if ($jumlahPesan < 1) {
                $pegawaiId = array($idPegawai);
            }

            foreach ($pegawaiId as $idPegawai) {
                $modDetail = new PesanmenupegawaiT();
                $model = PegawaiM::model()->findByPk($idPegawai);
                $nama = $model->nama_pegawai;
                $jeniskelamin = $model->jeniskelamin;
                $tr .= '<tr>
                        <td>'
                        . CHtml::checkBox('PesanmenupegawaiT[][' . $idRuangan . '][checkList]', true, array('class' => 'cekList', 'onclick' => 'hitungSemua()'))
                        . CHtml::activeHiddenField($modDetail, '[][' . $idRuangan . ']pegawai_id', array('value' => $model->pegawai_id))
                        . CHtml::hiddenField('PesanmenupegawaiT[][' . $idRuangan . '][ruangan_id]', $idRuangan)
                        . '</td>
                        <td>' . RuanganM::model()->with('instalasi')->findByPk($idRuangan)->instalasi->instalasi_nama . '/<br/>' . RuanganM::model()->findByPk($idRuangan)->ruangan_nama . '</td>
                        <td>' . CHtml::textField('nama', $nama, array('readonly' => true, 'class' => 'span2 nama')) . '</td>
                        <td>' . $jeniskelamin . '</td>';
                foreach (JeniswaktuM::getJenisWaktu() as $v) {
                    if (in_array($v->jeniswaktu_id, $jeniswaktu)) {
                        $tr .='<td>' . CHtml::hiddenField('PesanmenupegawaiT[][' . $idRuangan . '][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                . CHtml::dropDownList('PesanmenupegawaiT[][' . $idRuangan . '][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty' => '-- Pilih --', 'class' => 'span2 menudiet', 'options' => array("$idMenuDiet" => array("selected" => "selected")))) . '</td>';
                    } else {
                        $tr .='<td>' . CHtml::hiddenField('PesanmenupegawaiT[][' . $idRuangan . '][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                . CHtml::dropDownList('PesanmenupegawaiT[][' . $idRuangan . '][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('empty' => '-- Pilih --', 'class' => 'span2 menudiet',)) . '</td>';
                    }
                }
                $tr .= '<td>' . CHtml::activeTextField($modDetail, '[][' . $idRuangan . ']jml_pesan_porsi', array('value' => $jumlah, 'class' => ' span1 numbersOnly',)) . '</td>
                        <td>' . CHtml::activeDropDownList($modDetail, '[][' . $idRuangan . ']satuanjml_urt', Ukuranrumahtangga::items(), array('empty' => '-- Pilih --', 'class' => 'span2 urt', 'options' => array("$urt" => array("selected" => "selected")))) . '</td>
                        </tr>';
            }
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMenuDietPegawaiDariKirim(){
        if (Yii::app()->request->isAjaxRequest){
            $idPegawai = $_POST['idPegawai'];
            $idMenuDiet = $_POST['idMenuDiet'];
            $idRuangan = $_POST['idRuangan'];
            $idInstalasi = $_POST['idInstalasi'];
            $urt = $_POST['urt'];
            $jumlah = $_POST['jumlah'];
            $jeniswaktu = $_POST['jeniswaktu'];
            $modDetail = new PesanmenupegawaiT();
            $pegawaiId = $_POST['pegawaiId'];
            $butuh = $_POST['butuh'];
            $total = $_POST['total'];
            if (isset($butuh)){
                if(in_array($idMenuDiet, $butuh)){
                    foreach ($butuh as $i=>$dataRow){
                        if($dataRow == $idMenuDiet){
                            $total = $total[$i];
                        }
                    }
                }
                else{
                    $total = 0;
                }
            }
            else{
                $total = 0;
            }
            $hasil = true;
            $jumlahPesan = count($pegawaiId);
            if (KonfigsystemK::getKonfigKurangiStokGizi() == true){
                $bahanMenu = BahanMenuDietM::model()->findAllByAttributes(array('menudiet_id'=>$idMenuDiet));
                $kelipatan = count(JeniswaktuM::getJenisWaktu());
                foreach ($bahanMenu as $v){
                    $jumlahPesanPegawai = $jumlahPesan;
                    if ($jumlahPesan < 0) {
                        $jumlahPesanPegawai = 1;
                    }
                    if ($total !=0){
                        $jumlahButuh = $kelipatan*$v->jmlbahan*($jumlah+($total/$kelipatan))*$jumlahPesanPegawai;
                    }
                    else{
                        $jumlahButuh = $kelipatan*$v->jmlbahan*$jumlah*$jumlahPesanPegawai;
                    }
                    
                    if (StokbahanmakananT::validasiStok($jumlahButuh, $v->bahanmakanan_id) == false){
                        $hasil = false;
                    }
                }
            }
            
            if ($hasil == true){
                if ($jumlahPesan < 1) {
                    $pegawaiId = array($idPegawai);
                }

                foreach ($pegawaiId as $idPegawai) {
                    $modDetail = new KirimmenupegawaiT();
                    $model = PegawaiM::model()->findByPk($idPegawai);
                    $nama = $model->nama_pegawai;
                    $jeniskelamin = $model->jeniskelamin;
                    $tr .= '<tr>
                            <td>'
                            . CHtml::checkBox('KirimmenupegawaiT[][checkList]', true, array('class' => 'cekList', 'onclick' => 'hitungSemua()'))
                            . CHtml::activeHiddenField($modDetail, '[]pegawai_id', array('value' => $model->pegawai_id))
                            . CHtml::hiddenField('KirimmenupegawaiT[][ruangan_id]', $idRuangan)
                            . '</td>
                            <td>' . RuanganM::model()->with('instalasi')->findByPk($idRuangan)->instalasi->instalasi_nama . '/<br/>' . RuanganM::model()->findByPk($idRuangan)->ruangan_nama . '</td>
                            <td>' . CHtml::textField('nama', $nama, array('readonly' => true, 'class' => 'span2 nama')) . '</td>
                            <td>' . $jeniskelamin . '</td>';
                    foreach (JeniswaktuM::getJenisWaktu() as $v) {
                        if (in_array($v->jeniswaktu_id, $jeniswaktu)) {
                            $tr .='<td>' . CHtml::hiddenField('KirimmenupegawaiT[][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                    . CHtml::dropDownList('KirimmenupegawaiT[][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('onchange'=>'cekStokMenu(this)', 'empty' => '-- Pilih --', 'class' => 'span2 menudiet', 'options' => array("$idMenuDiet" => array("selected" => "selected")))) . '</td>';
                        } else {
                            $tr .='<td>' . CHtml::hiddenField('KirimmenupegawaiT[][jeniswaktu_id][' . $v->jeniswaktu_id . ']', $v->jeniswaktu_id)
                                    . CHtml::dropDownList('KirimmenupegawaiT[][menudiet_id][' . $v->jeniswaktu_id . ']', '', Chtml::listData(MenuDietM::model()->findAll(), 'menudiet_id', 'menudiet_nama'), array('onchange'=>'cekStokMenu(this)', 'empty' => '-- Pilih --', 'class' => 'span2 menudiet',)) . '</td>';
                        }
                    }
                    $tr .= '<td>' . CHtml::activeTextField($modDetail, '[]jml_kirim', array('value' => $jumlah, 'class' => ' span1 numbersOnly jmlKirim', 'onblur'=>'cekStokMenuInput(this)')) . '</td>
                            <td>' . CHtml::activeDropDownList($modDetail, '[]satuanjml_urt', Ukuranrumahtangga::items(), array('empty' => '-- Pilih --', 'class' => 'span2 urt', 'options' => array("$urt" => array("selected" => "selected")))) . '</td>
                            </tr>';
                }
            }
            
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetBahanMenuDiet()
    {
       if(Yii::app()->request->isAjaxRequest)
           {
                $menudiet_id=$_POST['menudiet_id'];
                $bahanmakanan_id = $_POST['bahanmakanan_id'];
                $jmlbahan = $_POST['jmlbahan'];
                $satuan = $_POST['satuan'];
                $modBahanMenuDiet = new BahanMenuDietM;
                $modMenuDiet = MenuDietM::model()->findByPk($menudiet_id);
                $modBahanMakanan=BahanmakananM::model()->findByPK($bahanmakanan_id);
                $return = array();
                    $tr .="<tr><td>";
                    $tr .= CHtml::checkBox('checkList[]',true,array('class'=>'cekList', 'onkeypress'=>"return $(this).focusNextInputField(event);"));
                    $tr .= "</td><td>";
                    $tr .= $modMenuDiet->menudiet_nama;
                    $tr .= CHtml::hiddenField('menudiet_id[]',$modMenuDiet->menudiet_id);
                    $tr .= CHtml::hiddenField('bahanmakanan_id[]',$modBahanMakanan->bahanmakanan_id);
                    $tr .= "</td><td>";
                    $tr .= $modBahanMakanan->namabahanmakanan;
                    $tr .= "</td><td>";
                    $tr .= CHtml::textField('jmlbahan[]',$jmlbahan, array('onkeypress'=>"return $(this).focusNextInputField(event);"));
                    $tr .="</td><td>";
                    $tr .= $satuan;
                    $tr .="</td>";
                    $tr .= "</tr>";
                $return .= $tr;
               $data['return']=$return;
               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetStokBahanMakanan(){
        if (Yii::app()->request->isAjaxRequest){
            $value = $_POST['value'];
            $total = $_POST['total'];
            $hasil = true;
            if (KonfigsystemK::getKonfigKurangiStokGizi() == true){
                if (isset($value)){
                    $bahanMenu = BahanMenuDietM::model()->findAllByAttributes(array('menudiet_id'=>$value));
                    $kelipatan = $total;
                    if (count($bahanMenu) > 0){
                        foreach ($bahanMenu as $v){
                            $jumlahButuh = $kelipatan*$v->jmlbahan;
                            if (StokbahanmakananT::validasiStok($jumlahButuh, $v->bahanmakanan_id) == false){
                                $hasil = false;
                            }
                        }
                    }
                    else{
                        $hasil = false;
                    }
                }
            }
            echo $hasil;
            Yii::app()->end();
        }
    }
    
    public function actionGetStokBahanMakananInput(){
        if (Yii::app()->request->isAjaxRequest){
            $total = $_POST['total'];
            $butuh = $_POST['butuh'];
            $hasil = true;
            if (KonfigsystemK::getKonfigKurangiStokGizi() == true){
                if (isset($butuh)){
                    foreach ($butuh as $i=>$dataRow){
                        $total = $total[$i];
                        if (StokbahanmakananT::validasiStokMenu($total, $dataRow) == false){
                            $hasil =false;
                        }
                    }
                }
            }

            echo $hasil;
            Yii::app()->end();
        }
    }

    
    public function actionGetPesanBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new PesanbarangdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanbarang = $satuan;
            $modDetail->qty_pesan = $jumlah;
            
            $tr = $this->renderPartial('_detailPesanBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetMutasiBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            if (KonfigsystemK::getKonfigKurangiStokUmum() == true){
                if (InventarisasiruanganT::validasiStok($jumlah, $idBarang) == false){
                    echo json_encode('kosong');
                    Yii::app()->end();
                }
            }
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new MutasibrgdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanbrg = $satuan;
            $modDetail->qty_mutasi = $jumlah;
            
            $tr = $this->renderPartial('_detailMutasiBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetStokBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['qty'];
            if (KonfigsystemK::getKonfigKurangiStokUmum() == true){
                if (InventarisasiruanganT::validasiStok($jumlah, $idBarang) == false){
                    echo json_encode('kosong');
                    Yii::app()->end();
                }
            }
            echo json_encode($tr);
            Yii::app()->end();
        }
    }

    //-- Modul Gudang Umum --
    //Pencarian Pembelian barang
    public function actionGetPembelianBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new BelibrgdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanbeli = $satuan;
            $modDetail->jmlbeli = $jumlah;
            $modDetail->hargabeli=0;
            $modDetail->hargasatuan = 0;
            $modDetail->jmldlmkemasan = $modBarang->barang_jmldlmkemasan;
            
            $tr = $this->renderPartial('_detailPembelianBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }

    //-- Modul Gudang Umum --
    //Pencarian Pemakaian barang
    public function actionGetPemakaianBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new PemakaianbrgdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanpakai = $satuan;
            $modDetail->jmlpakai = $jumlah;
            $modDetail->harganetto= $modBarang->barang_harganetto;
            $modDetail->hargajual = $modBarang->barang_hargajual;
            $modDetail->ppn = $modBarang->barang_ppn;
            $modDetail->disc = $modBarang->barang_persendiskon;
            $modDetail->hpp = $modBarang->barang_hpp;
            // $modDetail->jmldlmkemasan = $modBarang->barang_jmldlmkemasan;
            
            $tr = $this->renderPartial('_detailPemakaianBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }

    //-- Modul Gudang Umum --
    //Pencarian Pemakaian barang
    public function actionGetPenjualanBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new PenjualanbrgdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanjual = $satuan;
            $modDetail->jmljual = $jumlah;
            $modDetail->harganetto= $modBarang->barang_harganetto;
            $modDetail->hargajual = $modBarang->barang_hargajual;
            $modDetail->ppn = $modBarang->barang_ppn;
            $modDetail->disc = $modBarang->barang_persendiskon;
            $modDetail->hpp = $modBarang->barang_hpp;
            $modDetail->satuanjual = $modBarang->barang_satuan;
            // $modDetail->jmldlmkemasan = $modBarang->barang_jmldlmkemasan;
            
            $tr = $this->renderPartial('_detailPenjualanBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    //-- Modul Gudang Umum --
    //Pencarian Penerimaan Persediaan barang
    public function actionGetPenerimaanPersediaanBarang(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $jumlah = $_POST['jumlah'];
            $satuan = $_POST['satuan'];
            
            $modBarang = BarangM::model()->with('bidang')->findByPk($idBarang);
            $modDetail = new TerimapersdetailT();
            $modDetail->barang_id = $idBarang;
            $modDetail->satuanbeli = $satuan;
            $modDetail->jmlterima = $jumlah;
            $modDetail->hargabeli=0;
            $modDetail->hargasatuan = 0;
            $modDetail->jmldalamkemasan = $modBarang->barang_jmldlmkemasan;
            
            $tr = $this->renderPartial('_detailPenerimaanPersediaanBarang', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }

    public function actionJeniskasuspenyakitruangan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $instalasi_id = $_POST['instalasi_id'];
            $ruanganid = $_POST['ruanganid'];
            $jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'];
            
            $modInstalasi = InstalasiM::model()->findByPK($instalasi_id);
            $modruangan = RuanganM::model()->findByPK($ruanganid);
            
            $modJeniskasuspenyakitruangan = new KasuspenyakitruanganM;
            $modJeniskasuspenyakit = JeniskasuspenyakitM::model()->findByPk($jeniskasuspenyakit_id);
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modInstalasi->instalasi_nama
                            .CHtml::hiddenField('ruangan_id[]',$ruangan_id,array('readonly'=>true))
                            .CHtml::hiddenField('jeniskasuspenyakit_id[]',$jeniskasuspenyakit_id,array('readonly'=>true))
                            ."</td>";
                $tr .= "<td>".$modruangan->ruangan_nama."</td>";
                $tr .= "<td>".$modJeniskasuspenyakit->jeniskasuspenyakit_nama."</td>";
                $tr .= "<td>".$modJeniskasuspenyakit->jeniskasuspenyakit_namalainnya."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this); return false;'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }

    public function actionKasuspenyakitdiagnosaM()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'];
            $diagnosa_id = $_POST['diagnosa_id'];
            
            $modjeniskasuspenyakit = JeniskasuspenyakitM::model()->findByPK($jeniskasuspenyakit_id);
            $moddiagnosa = DiagnosaM::model()->findByPK($diagnosa_id);
            
            $modKasuspenyakitdiagnosa = new KasuspenyakitdiagnosaM;
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modjeniskasuspenyakit->jeniskasuspenyakit_nama
                            .CHtml::activehiddenField($modKasuspenyakitdiagnosa,'[]jeniskasuspenyakit_id',array('readonly'=>true,'value'=>$jeniskasuspenyakit_id,'class'=>'jenispenyakit'))
                            .CHtml::activehiddenField($modKasuspenyakitdiagnosa,'[]diagnosa_id',array('readonly'=>true,'value'=>$diagnosa_id))
                            ."</td>";
                $tr .= "<td>".$moddiagnosa->diagnosa_kode.' - '.$moddiagnosa->diagnosa_nama."</td>";
                $tr .= "<td>".$moddiagnosa->diagnosa_namalainnya."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this);return false;'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionKelasruangan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $instalasi_id = $_POST['instalasi_id'];
            $ruanganid = $_POST['ruanganid'];
            $kelaspelayanan_id = $_POST['kelaspelayanan_id'];
            
            $modinstalasi = InstalasiM::model()->findByPK($instalasi_id);
            $modruangan = RuanganM::model()->findByPK($ruanganid);
            $modkelaspelayanan = KelaspelayananM::model()->findByPK($kelaspelayanan_id);
            
            $modkelasruangan = new KelasruanganM;
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modinstalasi->instalasi_nama
                            .CHtml::hiddenField('ruangan_id['.$kelaspelayanan_id.']',$ruanganid,array('readonly'=>true))
                            .CHtml::hiddenField('kelaspelayanan_id[]',$kelaspelayanan_id,array('readonly'=>true))
                            ."</td>";
                $tr .= "<td>".$modruangan->ruangan_nama."</td>";
                $tr .= "<td>".$modkelaspelayanan->kelaspelayanan_nama."</td>";
                $tr .= "<td>".$modkelaspelayanan->kelaspelayanan_namalainnya."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this); return false;'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    public function actionTindakanruangan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $instalasi_id = $_POST['instalasi_id'];
            $ruanganid = $_POST['ruanganid'];
            $daftartindakan_id = $_POST['daftartindakan_id'];
            
            $modinstalasi = InstalasiM::model()->findByPK($instalasi_id);
            $modruangan = RuanganM::model()->findByPK($ruanganid);
            $moddaftartindakan = DaftartindakanM::model()->findByPK($daftartindakan_id);
            
            $modtindakanruangan = TindakanruanganM::model()->findByAttributes(array('ruangan_id'=>$ruanganid, 'daftartindakan_id' =>$daftartindakan_id));
            if (count($modtindakanruangan) < 1){
            
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modinstalasi->instalasi_nama
                            .CHtml::hiddenField('Tindakanruangan['.$daftartindakan_id.'][ruangan_id]',$ruanganid,array('readonly'=>true))
                            .CHtml::hiddenField('Tindakanruangan['.$daftartindakan_id.'][daftartindakan_id]',$daftartindakan_id,array('readonly'=>true))
                            ."</td>";
                $tr .= "<td>".$modruangan->ruangan_nama."</td>";
                $tr .= "<td>".$moddaftartindakan->daftartindakan_nama."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this);'))."</td>";
                $tr .= "</tr>";
            }
           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionRuanganpegawai()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $instalasi_id = $_POST['instalasi_id'];
            $ruanganid = $_POST['ruanganid'];
            $pegawai_id = $_POST['pegawai_id'];
            
            $modinstalasi = InstalasiM::model()->findByPK($instalasi_id);
            $modruangan = RuanganM::model()->findByPK($ruanganid);
            $modpegawai = PegawaiM::model()->findByPK($pegawai_id);
            
            $modkelasruangan = new KelasruanganM;
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modinstalasi->instalasi_nama
                            .CHtml::hiddenField('ruangan_id[]',$ruangan__id,array('readonly'=>true))
                            .CHtml::hiddenField('pegawai_id[]',$pegawai_id,array('readonly'=>true))
                            ."</td>";
                $tr .= "<td>".$modruangan->ruangan_nama."</td>";
                $tr .= "<td>".$modruangan->ruangan_namalainnya."</td>";
                $tr .= "<td>".$modpegawai->NamaLengkap."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this); return false;'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionKasuspenyakitdiagnosa()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $diagnosa_id = $_POST['diagnosa_id'];
            $obatakes_id = $_POST['obatalkes_id'];
            
            $moddiagnosa = DiagnosaM::model()->findByPK($diagnosa_id);
            $modobatalkes = ObatalkesM::model()->findByPK($obatakes_id);
            $model = new DiagnosaobatM;
                $tr = "<tr>";
                $tr .= "<td>"
                            .$moddiagnosa->diagnosa_kode
                            .CHtml::activehiddenField($model,'[]diagnosa_id',array('readonly'=>true,'value'=>$diagnosa_id,'class'=>'diagnosa'))
                            .CHtml::activehiddenField($model,'[]obatalkes_id',array('readonly'=>true,'value'=>$obatakes_id))
                            ."</td>";
                $tr .= "<td>".$moddiagnosa->diagnosa_nama."</td>";
                $tr .= "<td>".$modobatalkes->obatalkes_nama."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this);'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionKasuspenyakitobatM()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $jeniskasuspenyakit_id = $_POST['jeniskasuspenyakit_id'];
            $obatalkes_id = $_POST['obatalkes_id'];
            
            $modjeniskasuspenyakit = JeniskasuspenyakitM::model()->findByPK($jeniskasuspenyakit_id);
            $modobatalkes = ObatalkesM::model()->findByPK($obatalkes_id);
            
            $modKasuspenyakitobat = new KasuspenyakitobatM;
                $tr = "<tr>";
                $tr .= "<td>"
                            .$modjeniskasuspenyakit->jeniskasuspenyakit_nama
                            .CHtml::activehiddenField($modKasuspenyakitobat,'[]jeniskasuspenyakit_id',array('readonly'=>true,'value'=>$jeniskasuspenyakit_id,'class'=>'jenispenyakit'))
                            .CHtml::activehiddenField($modKasuspenyakitobat,'[]obatalkes_id',array('readonly'=>true,'value'=>$obatalkes_id))
                            ."</td>";
                $tr .= "<td>".$modobatalkes->obatalkes_kode."</td>";
                $tr .= "<td>".$modobatalkes->obatalkes_nama."</td>";
                $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'hapusBaris(this);'))."</td>";
                $tr .= "</tr>";

           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    //INI UNTUK APA ? KARNA HAK AKSES SUDAH DI ATUR DI SRBAC
    public function actionCekHakRetur()
    {
        if(!Yii::app()->user->checkAccess('Retur')){
            //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
            $data['cekAkses'] = false;
        } else {
            //echo 'punya hak akses';
            $data['cekAkses'] = true;
            $data['userid'] = Yii::app()->user->id;
            $data['username'] = Yii::app()->user->name;
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public function actionCekHakBatalBayar()
    {
        if(!Yii::app()->user->checkAccess('BatalBayar')){
            //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
            // echo Yii::app()->user->checkAccess('BatalBayar');
            $data['cekAkses'] = true;
        } else {
            //echo 'punya hak akses';
            $data['cekAkses'] = true;
            $data['userid'] = Yii::app()->user->id;
            $data['username'] = Yii::app()->user->name;
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }
    /*
     * --modul Kepegawaian
     * mendapatkan detail pegawai dari nip yang diinputkan pada transaksi Penggajian
     */
    public function actionGetPegawaiFromNip(){
        if (Yii::app()->request->isAjaxRequest){
            $nip = $_POST['nip'];
            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(nomorindukpegawai)', $nip);
            $criteria->with = array('jabatan','pangkat');
            $criteria->limit = 1;
            $model = PegawaiM::model()->find($criteria);
            $attributes = $model->attributeNames();
            
            foreach($attributes as $j=>$attribute) {
                $data->attributes["$attribute"] = $data->$attribute;
                $data->attributes["jabatan_nama"]=$data->jabatan->jabatan_nama;
                $data->attributes["pangkat_nama"]=$data->pangkat->jabatan_nama;
            }
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
                $returnVal["jabatan_nama"]=$model->jabatan->jabatan_nama;
                $returnVal["pangkat_nama"]=$model->pangkat->jabatan_nama;
            }

            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
    /*
     * --modul Kepegawaian
     * mendapatakan detail pegawai dari nofingerprint yang diinputkan pada transaksi Presensi
     */
    public function actionGetPegawaiFromNoFinger(){
        if (Yii::app()->request->isAjaxRequest){
            $nofinger = $_POST['nofinger'];
            $criteria = new CDbCriteria();
            $criteria->compare('nofingerprint', $nofinger);
            $criteria->with = array('jabatan','pangkat');
            $criteria->limit = 1;
            $model = PegawaiM::model()->find($criteria);
            $attributes = $model->attributeNames();

            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
                $returnVal["jabatan_nama"]=$model->jabatan->jabatan_nama;
                $returnVal["pangkat_nama"]=$model->pangkat->jabatan_nama;
            }

            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
    
    public function actionmonitoringrawatjalanAutoRefresh(){
        if (Yii::app()->request->isAjaxRequest){
            $auto = $_POST['auto'];
            MonitoringrawatjalanV::model()->updateAll();
            Yii::app()->end();
        }
    }
     /*
     * --modul Remunerasi
     * mendapatakan detail Komponen Jasa dari  yang diinputkan pada transaksi Komponen Jasa
     */
     public function actionGetKomponenjasa()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $model = REKomponenjasaM::model()->findAllByAttributes(array('komponenjasa_id'=>$model->komponenjasa_id),array('order'=>'komponenjasa_id'));
            $i=1;
            foreach ($model as $row)
            {
                $urlDelete = Yii::app()->createUrl('Remunerasi/KomponenjasaM/deleteKomponenjasa',array('komponenjasa_id'=>$row->komponenjasa_id));
                $tr .= '<tr>';
                    $tr .= '<td>'.$i.' </td>';
                    $tr .= '<td>'.$row->kelompoktindakan->kelompoktindakan_nama.'</td>';
                    $tr .= '<td>'.$row->ruangan->ruangan_nama.'</td>';
                    $tr .= '<td>'.$row->komponenjasa_kode.'</td>';
                    $tr .= '<td>'.$row->komponenjasa_nama.'</td>';
                    $tr .= '<td>'.$row->komponenjasa_singkatan.' bulan</td>';
                    $tr .= '<td>'.$row->besaranjasa.'</td>';
                    $tr .= '<td>'.$row->potongan.'</td>';
                    $tr .= '<td>'.$row->jasadireksi.'</td>';
                    $tr .= '<td>'.$row->kuebesar.'</td>';
                    $tr .= '<td>'.$row->jasadokter.'</td>';
                    $tr .= '<td>'.$row->jasaparamedis.'</td>';
                    $tr .= '<td>'.$row->jasaunit.'</td>';
                    $tr .= '<td>'.$row->jasabalanceins.'</td>';
                    $tr .= '<td>'.$row->jasaemergency.'</td>';
                    $tr .= '<td>'.$row->biayaumum.'</td>';

                    $tr .= '<td>'.CHtml::link('<i class="icon-trash"></i>',$urlDelete,array('onclick'=>'return (!confirm("Anda yakin akan menghapus item ini ?")) ? false : true')).'</td>';
                $tr .= '</tr>';
                $i++;
            }
                
               $data['tr']=$tr;

               echo json_encode($data);
             Yii::app()->end();
        }
    }
    
    public function actionGetSupplierKode(){
        if (Yii::app()->request->isAjaxRequest){
            $kode = $_POST['supplier_kode'];
            $criteria = new CDbCriteria();
            $criteria->compare('supplier_kode', $kode);
            $criteria->limit = 1;
            $model = SupplierM::model()->find($criteria);
            $attributes = $model->attributeNames();

            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }

            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
    
     public function actionGetKodeBarangSupplier(){
        if (Yii::app()->request->isAjaxRequest){
            $kodebarang = $_POST['obatalkes_kode'];
            $criteria = new CDbCriteria();
            $criteria->compare('obatalkes_kode', $kodebarang);
            $criteria->limit = 1;
            $model = ObatalkesM::model()->find($criteria);
            $attributes = $model->attributeNames();

            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }

            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
    
    public function actionGetBarcodeBarangSupplier(){
        if (Yii::app()->request->isAjaxRequest){
            $barcodebarang = $_POST['obatalkes_barcode'];
            $criteria = new CDbCriteria();
            $criteria->compare('obatalkes_barcode', $barcodebarang);
            $criteria->limit = 1;
            $model = ObatalkesM::model()->find($criteria);
            $attributes = $model->attributeNames();

            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }

            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
     public function actionGetObatSupplier(){
        if (Yii::app()->request->isAjaxRequest){
            $idObat = $_POST['idObat'];
            $idSupplier = $_POST['idSupplier'];
            $modBarang = ObatalkesM::model()->findByPk($idObat);
         
            $modDetail = new ObatsupplierM();
            $modDetail->obatalkes_id = $idObat;
            $modDetail->supplier_id = $idSupplier;
            
            $tr = $this->renderPartial('_detailBarangSupplier', array('modBarang'=>$modBarang, 'modDetail'=>$modDetail), true);
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    

    public function actionGetStockBarangRetail(){
        if (Yii::app()->request->isAjaxRequest){
            $idBarang = $_POST['idBarang'];
            $qty = $_POST['qty'];
            $modBarang = ProdukposV::model()->findByAttributes(array('brg_id'=>$idBarang));
            $modObat = new ObatalkespasienT();
            $criteria = new CDBCriteria();
            $criteria->addCondition('obatalkes_id = '.$idBarang);
            $criteria->order = 'tglstok_in '.((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            $modStok = StokobatalkesT::model()->find($criteria);
            $data["stok"] = StokobatalkesT::getStokBarang($idBarang);
            $data["disc"] = $modBarang->discount;
            $data["sub"] = $modBarang->hargajual;
            $data["netto"] =$modBarang->harganetto;
            $modObat->obatalkes_id = $modBarang->brg_id;
            $modObat->qty_oa = $qty;
            $modObat->hargajual_oa = ((!empty($modBarang->hargajual)) ? $modBarang->hargajual : 0)*$qty;
            $modObat->harganetto_oa = ((!empty($modBarang->harganetto)) ? $modBarang->harganetto : 0)*$qty;
            $modObat->discount = $modBarang->discount*$modBarang->hargajual*$qty/100;
            $modObat->sumberdana_id = $modBarang->sumberdana_id;
            $modObat->biayaadministrasi = $modBarang->ppn_persen*$modBarang->hargajual*$qty/100;
            
            $tr['tr'] = $this->renderPartial('_detailStokBarangRetail', array('modBarang'=>$modBarang, 'modObat'=>$modObat, 'data'=>$data, 'modStok'=>$modStok), true);
            $tr['stok'] = $data["stok"];
            $tr['barang']['harga'] = $modObat->hargajual_oa;
            $tr['barang']['qty'] = $qty;
            $tr['barang']['nama'] = $modBarang->stock_name;
            $tr['barang']['minimal'] = (!empty($modBarang->minimalstok)) ? $modBarang->minimalstok : 0 ;
            echo json_encode($tr);
            Yii::app()->end();
        }
    }
    
    public function actionGetIdBarangStock(){
        if (Yii::app()->request->isAjaxRequest){
            $obj = $_POST['objName'];
            $value = $_POST['objValue'];
            $modBarang = ProdukposV::model()->findByAttributes(array($obj=>$value));
            
            echo json_encode($modBarang->attributes);
            Yii::app()->end();
        }
    }
    
    
    public function actionGetListSubKategori()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $data= SubjenisM::model()->findAllByAttributes(array('jenisobatalkes_id'=>$_POST['idJenis'],'subjenis_id'=>$_POST['idSub']),array('order'=>'subjenis_nama'));
            $data=CHtml::listData($data,'subjenis_id','subjenis_nama');

            foreach($data as $value=>$name)
            {
                if($value==$_POST['idSub'])
                    $SubkategoriOption .= CHtml::tag('option',array('value'=>$value,'selected'=>true),CHtml::encode($name),true);
                else
                    $SubkategoriOption .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
            }

            $dataList['listKabupaten'] = $subkategoriOption;

            echo json_encode($dataList);
            Yii::app()->end();
        }
    }

//    public function actionGetObatalkesKode(){
//        if (Yii::app()->request->isAjaxRequest){
//            $kodeobatalkes = $_POST['obatalkes_kode'];
//            $criteria = new CDbCriteria();
//            $criteria->compare('obatalkes_kode', $kodeobatalkes);
//            $criteria->limit = 1;
//            $model = ObatalkesM::model()->find($criteria);
//            $attributes = $model->attributeNames();
//
//            foreach($attributes as $j=>$attribute) {
//                $returnVal["$attribute"] = $model->$attribute;
//            }
//
//            echo json_encode($returnVal);
//            Yii::app()->end();
//        }
//    }
    
     public function actionPemeriksaanhasil()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $pemeriksaanlab_id = $_POST['pemeriksaanlab_id'];
            $nilairujukan_id = $_POST['nilairujukan_id'];
            // print_r($_POST['nilairujukan_id']);exit();
            $dataNilairujukan = NilairujukanM::model()->findAllByAttributes(array('nilairujukan_id'=>$nilairujukan_id));
            // $dataNilairujukan = NilairujukanM::model()->findAllByAttributes(array('nilairujukan_id'=>$nilairujukan_id);
            $model = new PemeriksaanlabdetM;
            foreach ($dataNilairujukan as $modNilairujukan) {
                $tr = '<tr>';
                    $tr .=
                            '<td>'
                             .CHtml::activeTextField($model,'[]pemeriksaanlabdet_nourut',array('class'=>'span1 nourut'))
                             .CHtml::activeHiddenField($model,'[]pemeriksaanlab_id',array('class'=>'span1','readonly'=>true,'value'=>$pemeriksaanlab_id))
                             .CHtml::activeHiddenField($model,'[]nilairujukan_id',array('class'=>'span1','readonly'=>true,'value'=>$nilairujukan_id))
                             .'</td>';
                    $tr .= '<td>'.$modNilairujukan->kelompokdet.'</td>';
                    $tr .= '<td>'.$modNilairujukan->namapemeriksaandet.'</td>';
                    $tr .= '<td>'.$modNilairujukan->kelompokumur.'</td>';
                    $tr .= '<td>'.$modNilairujukan->nilairujukan_jeniskelamin.'</td>';
                    $tr .= '<td>'.$modNilairujukan->nilairujukan_metode.'</td>';
                    $tr .= '<td>'.CHtml::link('<i class="icon-remove"></i>','',array('id'=>'removebtn','style'=>'cursor:pointer;','class'=>'pemeriksaanlabdet','onclick'=>'hapusBaris(this); return false;')).'</td>';
                $tr .= '</tr>';
            }
            $data['tr'] = $tr;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
        public function actionGetProdukpos()
        {
                $model = REProdukposV;
                $criteria = new CDbCriteria;
                
                $criteria->compare('category_id',$_GET['category_id']);
                $criteria->compare('subcategory_id',$_GET['subcategory_id']);
                $criteria->compare('LOWER(stock_code)',strtolower($_GET['stock_code']),true);
                $criteria->compare('LOWER(stock_name)',strtolower($_GET['stock_name']),true);
                $criteria->compare('LOWER(barang_barcode)',strtolower($_GET['barang_barcode']),true);
                $criteria->compare('LOWER(price_name)',strtolower($_GET['price_name']),true);
                $criteria->order='brg_id asc';
                $criteria->order = $_GET['sidx'].' '.$_GET['sord'];
                
                $searchOn = $_REQUEST['_search'];
                $fld = $_REQUEST['searchField'];
                $fldata = $_REQUEST['searchString'];
                $foper = $_REQUEST['searchOper'];
                $criteria->condition = $wh;
                $dataProvider=new CActiveDataProvider('ProdukposV', array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                        'pageSize'=>$_GET['rows'],
                        'currentPage'=>$_GET['page']-1,
                    ),
                ));
        $responce->page = $_GET['page'];
        $responce->records = $dataProvider->getTotalItemCount();
        $responce->total = ceil($responce->records / $_GET['rows']);
        $rows = $dataProvider->getData();
        $no = 1;
        if ($_GET['page'] > 1) {
            $page = $_GET['page'] - 1;
            $no = $page.'1';
        }
        foreach ($rows as $i=>$row)
        {
                $responce->rows[$i]['cell'] = array(//'',
                    $no,
                    $row->category_name
                    .' / '.$row->subcategory_name
                    .CHtml::hiddenField('REProdukposV['.$row->brg_id.'][brg_id]',$row->brg_id,array('readonly'=>true))
                    .CHtml::checkBox('REProdukposV['.$row->brg_id.'][cekList]',true,array('onclick'=>'setUrutan() getTotal()','class'=>'cekList','style'=>'display:none;')),
                    $row->stock_code.' / '.$row->barang_barcode,
                    $row->stock_name,
                    CHtml::textField('REProdukposV['.$row->brg_id.'][hargajual]',$row->hargajual, array("id"=>"hargajual","class"=>"span1 hargajual numbersOnly margin", "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")),
                    CHtml::textField('REProdukposV['.$row->brg_id.'][discount]',$row->discount, array("id"=>"discount","class"=>"span1 numbersOnly margin", "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")),
                    CHtml::hiddenField('REProdukposV['.$row->brg_id.'][ppn]',$row->ppn_persen, array("id"=>"ppn","class"=>"span1 numbersOnly margin", "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")).''.$row->ppn_persen,
                    CHtml::textField('REProdukposV['.$row->brg_id.'][harganetto]',$row->harganetto, array("id"=>"harganetto","class"=>"span1 numbersOnly margin", "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")),
                    CHtml::hiddenField('REProdukposV['.$row->brg_id.'][ratarata]',$row->movingavarage, array("id"=>"hargaratarata","class"=>"span1 numbersOnly margin", "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")).''.$row->movingavarage,
                    CHtml::textField('stok',StokobatalkesT::getStokBarang($row->brg_id, Yii::app()->user->ruangan_id), array('class'=>'stok span1','readonly'=>true, "onmouseover"=>"getTotal()", "onkeypress"=>"getTotal()")),
                );
                $no++;
        }
        echo json_encode($responce);
        }
         /**
          * for insert to tindakanpelayanan_t
          * digunakan di :
          * 1. ActionAjax/saveRuanganBaru
          * @param object $modPasien PasienM
          * @param object $model PendaftaranT
          *
          */
         private function saveTindakanPelayanan($modPasien, $model) {
            $cekTindakanKomponen = 0;
            $modTindakanPelayan = New TindakanpelayananT;
            $modTindakanPelayan->penjamin_id = $model->penjamin_id;
            $modTindakanPelayan->pasien_id = $modPasien->pasien_id;
            $modTindakanPelayan->kelaspelayanan_id = $model->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $model->pendaftaran_id;
            $modTindakanPelayan->shift_id = Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->carabayar_id = $model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id = $model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan = date('Y-m-d H:i:s');

            $modTindakanPelayan->qty_tindakan = 1;
            
            $modTindakanPelayan->satuantindakan = Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan = 0;
            $modTindakanPelayan->tarifcyto_tindakan = 0;
            $modTindakanPelayan->dokterpemeriksa1_id = $model->pegawai_id;
            $modTindakanPelayan->discount_tindakan = 0;
            $modTindakanPelayan->subsidiasuransi_tindakan = 0;
            $modTindakanPelayan->subsidipemerintah_tindakan = 0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan = 0;
            $modTindakanPelayan->iurbiaya_tindakan = 0;
            $modTindakanPelayan->ruangan_id = $model->ruangan_id;

            $statuspasien = ($model->statuspasien == Params::statusPasien('baru')) ? true : false;
            $karcis = KarcisM::model()->find('ruangan_id = ? and pasienbaru_karcis =?', array($model->ruangan_id, $statuspasien));
            $karcisV = KarcisV::model()->find('karcis_id = ? and kelaspelayanan_id =?', array($karcis->karcis_id, $model->kelaspelayanan_id));
            if (!empty($karcis->karcis_id)) {
                $modTindakanPelayan->karcis_id = $karcis->karcis_id;
                $modTindakanPelayan->tipepaket_id = $this->tipePaketKarcis($model, $karcis);
                $modTindakanPelayan->daftartindakan_id = $karcis->daftartindakan_id;
            }
            if (isset($karcisV)){
                $modTindakanPelayan->tarif_satuan = $karcisV->harga_tariftindakan;
                $modTindakanPelayan->tarif_tindakan = $karcisV->harga_tariftindakan * $modTindakanPelayan->qty_tindakan;
            }

            $tarifTindakan = TariftindakanM::model()->findAll('daftartindakan_id=' . $karcis->daftartindakan_id . ' AND kelaspelayanan_id=' . $modTindakanPelayan->kelaspelayanan_id . '');
            foreach ($tarifTindakan AS $dataTarif):
                if ($dataTarif['komponentarif_id'] == Params::KOMPONENTARIF_ID_RS) {
                    $modTindakanPelayan->tarif_rsakomodasi = $dataTarif['harga_tariftindakan'];
                }
                if ($dataTarif['komponentarif_id'] == Params::KOMPONENTARIF_ID_MEDIS) {
                    $modTindakanPelayan->tarif_medis = $dataTarif['harga_tariftindakan'];
                }
                if ($dataTarif['komponentarif_id'] == Params::KOMPONENTARIF_ID_PARAMEDIS) {
                    $modTindakanPelayan->tarif_paramedis = $dataTarif['harga_tariftindakan'];
                }
                if ($dataTarif['komponentarif_id'] == Params::KOMPONENTARIF_ID_BHP) {
                    $modTindakanPelayan->tarif_bhp = $dataTarif['harga_tariftindakan'];
                }
            endforeach;
            $result = false;
            if ($modTindakanPelayan->save()) {
                $tindakanKomponen = TariftindakanM::model()->findAll('daftartindakan_id=' . $karcis->daftartindakan_id . ' AND kelaspelayanan_id=' . $modTindakanPelayan->kelaspelayanan_id . '');
                $jumlahKomponen = COUNT($tindakanKomponen);
                if ($jumlahKomponen > 0) {
                    foreach ($tindakanKomponen AS $tampilKomponen):
                        $modTindakanKomponen = new TindakankomponenT;
                        $modTindakanKomponen->tindakanpelayanan_id = $modTindakanPelayan->tindakanpelayanan_id;
                        $modTindakanKomponen->komponentarif_id = $tampilKomponen['komponentarif_id'];
                        $modTindakanKomponen->tarif_kompsatuan = $tampilKomponen['harga_tariftindakan'];
                        $modTindakanKomponen->tarif_tindakankomp = $tampilKomponen['harga_tariftindakan'] * $modTindakanPelayan->qty_tindakan;
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                        $modTindakanKomponen->subsidiasuransikomp = $modTindakanPelayan->subsidiasuransi_tindakan;
                        $modTindakanKomponen->subsidipemerintahkomp = $modTindakanPelayan->subsidipemerintah_tindakan;
                        $modTindakanKomponen->subsidirumahsakitkomp = $modTindakanPelayan->subsisidirumahsakit_tindakan;
                        $modTindakanKomponen->iurbiayakomp = $modTindakanPelayan->iurbiaya_tindakan;

                        if ($modTindakanKomponen->save()) {
                            $cekTindakanKomponen++;
                        }
                    endforeach;
                }
                //load $postRekenings dari view
                $jenisPelayanan = 'tm';
                $daftartindakanId = $karcis->daftartindakan_id;
                $criteria = new CDbCriteria;
                $criteria->select = '*, pelayananrek_m.saldonormal AS saldonormal, pelayananrek_m.jnspelayanan AS jnspelayanan';
                $criteria->join = '
                    JOIN pelayananrek_m ON
                        pelayananrek_m.rekening1_id = t.struktur_id AND
                        pelayananrek_m.rekening2_id = t.kelompok_id AND
                        pelayananrek_m.rekening3_id = t.jenis_id AND
                        pelayananrek_m.rekening4_id = t.obyek_id AND
                        pelayananrek_m.rekening5_id = t.rincianobyek_id
                ';
                if(strtolower(trim($jenisPelayanan)) == 'tm'){
                    $criteria->addCondition("pelayananrek_m.daftartindakan_id = ".$daftartindakanId);
                }
                $criteria->addCondition("LOWER(pelayananrek_m.jnspelayanan) = '".strtolower(trim($jenisPelayanan))."'");
                $modRekenings = RekeningakuntansiV::model()->findAll($criteria);
                
                if(isset($modRekenings)){
                    $postRekenings = array();
                    foreach($modRekenings AS $i=>$rekening){
                        $postRekenings[$i]['nama_rekening'] = !empty($rekening->nmrincianobyek) ? $rekening->nmrincianobyek : $rekening->nmobyeklain;
                        $postRekenings[$i]['saldodebit'] = 0; //set TM agar detail di input berdasarkan jenis
                        $postRekenings[$i]['saldokredit'] = $modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
                        $postRekenings[$i]['struktur_id'] = $rekening->struktur_id;
                        $postRekenings[$i]['kelompok_id'] = $rekening->kelompok_id;
                        $postRekenings[$i]['jenis_id'] = $rekening->jenis_id;
                        $postRekenings[$i]['obyek_id'] = $rekening->obyek_id;
                        $postRekenings[$i]['rincianobyek_id'] = $rekening->rincianobyek_id;
                        $postRekenings[$i]['jnspelayanan'] = 'tm'; //set TM agar detail di input berdasarkan jenis
                    }
                    $modJurnalRekening = TindakanController::saveJurnalRekening();
                    //update jurnalrekening_id
                    $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                    $modTindakanPelayan->save();
                    $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                }
                
                if (!(($cekTindakanKomponen != $jumlahKomponen) && ($jumlahKomponen > 0))) {
                    $result = true;
                }
                
            }

            return $result;
        }
        /**
         * method to get type of paket with parameter daftar tindakan, kelaspelayanan,
         * digunakan di :
         * 1. ActionAjaxController/saveTindakanPelayanan
         * @param object $model PendaftaranT
         * @param object $karcis KarcisM
         * @return int
         */
        public function tipePaketKarcis($model,$karcis)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $karcis->daftartindakan_id);
            $criteria->compare('tipepaket.carabayar_id', $model->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $model->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $model->kelaspelayanan_id);
            $result = Params::TIPEPAKET_NONPAKET;
            $paket = PaketpelayananM::model()->find($criteria);
            if(isset($paket->tipepaket_id)) $result = $paket->tipepaket_id;

            return $result;
        }
        
        //-- Rawat Jalan --//
        //-- Function Untuk Pembatalan Rawat Inap --//
        public function actionCekLoginPembatalRawatInap()
    {
        if(Yii::app()->request->isAjaxRequest){
            $username = $_POST['username'];
            $password = md5($_POST['password']);
            $idRuangan = Yii::app()->user->getState('ruangan_id');
            
            $user = LoginpemakaiK::model()->findByAttributes(array('nama_pemakai' => $username,
                                                                   'loginpemakai_aktif' =>TRUE,
                                                                   'katakunci_pemakai'=>$password ));
            if ($user === null) {//Jika Username dan Password Salah
                $data['error'] = "Login Pemakai salah!";
                $data['cssError'] = 'username';
            }else{//Jika Username dan Passwrd Benar
                
                $ruangan_user = RuanganpemakaiK::model()->findByAttributes(array('loginpemakai_id'=>$user->loginpemakai_id,
                                                                                 'ruangan_id'=> $idRuangan));
                if($ruangan_user===null) {//Jika Ruangan Salah
                    $data['error'] = 'ruangan salah!';
                } else { //JIka ruangan Benar
                    $data['error'] = '';
                    $cek = Yii::app()->authManager->checkAccess('Administrator',$user->loginpemakai_id);
                    if($cek){//Jika User Mempunyai Hak Akses
                        $data['status'] = 'success';
                        $data['userid'] = $user->loginpemakai_id;
                        $data['username'] = $user->nama_pemakai;
                    } else {//JIka user Tidak mempunyai Hak Akses
                        $data['status'] = 'Gagal';
                    }
                }
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    // -- Modul Gudang Farmasi -- //
    // Added getObatAlkesSupplier by Miranitha , 22 February 2013
    public function actionGetObatAlkesSupplier(){
        if(Yii::app()->request->isAjaxRequest) {
            $idObat = $_POST['idObatAlkes'];
            $idSupplier = $_POST['idSupplier'];
            
            $modSupplier = SupplierM::model()->findByPk($idSupplier);
            $modObatSupplier = new ObatsupplierM;
            $modObatAlkes=ObatalkesM::model()->findByPk($idObat);
            $nourut = 1;
                $tr="<tr>
                        <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modObatSupplier,'['.$idObat.']obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obatAlkes')).
                              CHtml::activeHiddenField($modObatSupplier,'['.$idObat.']supplier_id',array('value'=>$modSupplier->supplier_id, 'class'=>'supplier')).
                       "</td>
                        <td>".$modSupplier->supplier_nama."</td>
                        <td>".$modObatAlkes->obatalkes_nama."</td>
                        <td>".CHtml::activeDropDownList($modObatSupplier, '['.$idObat.']satuankecil_id', CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'), array('empty'=>'-- Pilih --', 'class' => 'span2 satuankecil', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::activeDropDownList($modObatSupplier, '['.$idObat.']satuanbesar_id', CHtml::listData(SatuanbesarM::model()->findAll(), 'satuanbesar_id', 'satuanbesar_nama'), array('empty'=>'-- Pilih --', 'class' => 'span2 satuanbesar', 'onkeypress' => "return $(this).focusNextInputField(event)"))."</td>
                        <td>".CHtml::activetextField($modObatSupplier,'['.$idObat.']hargabelibesar',array('onkeyup'=>'setHargaJual(this);','value'=>ceil($modObatAlkes->harganetto),'class'=>'span1 numbersOnly netto','readonly'=>FALSE))."</td>
                        <td>".CHtml::activetextField($modObatSupplier,'['.$idObat.']hargabelikecil',array('value'=>ceil($modObatAlkes->harganetto),'class'=>'span1 numbersOnly hargajual','readonly'=>FALSE))."</td>
                        <td>".CHtml::activetextField($modObatSupplier,'['.$idObat.']diskon_persen',array('class'=>'span1 numbersOnly diskon_persen','readonly'=>FALSE,'value'=>0))."</td>
                        <td>".CHtml::activetextField($modObatSupplier,'['.$idObat.']ppn_persen',array('class'=>'span1 numbersOnly ppn_persen','readonly'=>FALSE,'value'=>0))."</td>
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;', 'class'=>'cancel'))."</td>
                      </tr>";
           
           $data['tr']=$tr;
//           $data['obatalkes']=$idSupplier;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    // End getObatAlkesSupplier by Miranitha //

    public function actionGetObatProduksi(){
        if(Yii::app()->request->isAjaxRequest) {
            Yii::import('farmasiApotek.models.FAProduksiobatdetT');
            Yii::import('farmasiApotek.models.FABahanobatM');
            $obatProduksiId = $_POST['obatProduksiId'];
            $obatProduksi = $_POST['obatProduksi'];
            $modDetail= new FAProduksiobatdetT;
            $ObatalkesproduksiM=ObatalkesproduksiM::model()->findByPk($obatProduksiId);
            $modBahanobat=FABahanobatM::model()->findAllByAttributes(array('obatalkesproduksi_id' => $obatProduksiId));
            

            foreach ($modBahanobat as $i => $datas)
            {
                $modObatAlkes=ObatalkesM::model()->findByPk($datas->obatalkes_id);
                $nourut = 1;
                $tr.="<tr>
                  <td>".CHtml::TextField('noUrut','',array('class'=>'span1 noUrut integer','readonly'=>TRUE)).
                        CHtml::activeHiddenField($modDetail,'['.$i.']hargasatuan',array('value'=>$modObatAlkes->hargajual, 'class'=>'hargaSatuan')).
                        CHtml::activeHiddenField($modDetail,'['.$i.']harganetto',array('value'=>$modObatAlkes->harganetto, 'class'=>'hargaSatuan')).
                        CHtml::activeHiddenField($modDetail,'['.$i.']hpp',array('value'=>$modObatAlkes->hpp, 'class'=>'hargaSatuan')).
                 "</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']obatalkes_kode', array('value'=>$modObatAlkes->obatalkes_kode, 'readonly'=>true,'class'=>'span1','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']obatalkes_nama', array('value'=>$modObatAlkes->obatalkes_nama, 'readonly'=>true,'class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event);",)).
                       CHtml::activeHiddenField($modDetail,'['.$i.']obatalkes_id',array('value'=>$datas->obatalkes_id, 'class'=>'obatAlkes')).
                 "</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']dosis', array('value'=>$datas->permintaandosis, 'onblur'=>'hitungQty(this);','class'=>'span1 integer','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']kemasan', array('value'=>$datas->jmlkemasan, 'onblur'=>'hitungQty(this);','class'=>'span1 integer','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']kekuatan', array('value'=>$datas->kekuatan, 'onblur'=>'hitungQty(this);','class'=>'span1 integer','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']qtyproduksi', array('value'=>$datas->qtybahanobt, 'class'=>'span1 float','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::activeTextField($modDetail, '['.$i.']satuankecil_nama', array('value'=>$datas->satuankecil->satuankecil_nama, 'readonly'=>true,'class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event);",))."</td>
                 <td>".CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'remove(this);return false;','style'=>'text-decoration:none;', 'class'=>'cancel'))."</td>
                             
                </tr>";
                // $data['tr']=$tr;
                // CHtml::link("<span class='icon-minus'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;', 'class'=>'cancel'))."</td>
          }
              
           $data['tr']=$tr;
//           $data['obatalkes']=$idSupplier;
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    // Added getSupplier by Miranitha , 25 February 2013
    public function actionGetSupplier(){
        if(Yii::app()->request->isAjaxRequest) {
            $idSupplier = $_POST['idSupplier'];
            $modObatSupplier = ObatsupplierM::model()->findAll('supplier_id='.$idSupplier);
            $data['supplier_id'] = $idSupplier;
            
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    public function actionGetDokter(){
        if(Yii::app()->request->isAjaxRequest) {
            $pegawai_id = $_POST['pegawai_id'];
            $modObatProduksi = ObatalkesproduksiM::model()->findAll('pegawai_id='.$pegawai_id);
             echo print_r($modObatProduksi);exit;
            $data['pegawai_id'] = $pegawai_id;
            
           echo json_encode($data);
         Yii::app()->end();
        }
    }

    public function actionGetSupplierPenerimaan(){
        if(Yii::app()->request->isAjaxRequest) {
            $idSupplier = $_POST['idSupplier'];
            $modObatSupplier = ObatsupplierM::model()->findAll('supplier_id='.$idSupplier);
            $data['supplier_id'] = $idSupplier;
            
           echo json_encode($data);
         Yii::app()->end();
        }
    }
    // End getSupplier by Miranitha //
    

    /**
     * method get list jadwal dokter
     * digunakan di :
     * 1. Pendaftaran -> rawat Jalan
     */
    public function actionGetListJadwalDokter(){
        if (Yii::app()->request->isAjaxRequest){
            $result = '<tr><td colspan=8><i>Data Tidak Ditemukan</i></td></tr>';
            if (isset($_POST['id'])){
                $idInstalasi = $_POST['id'];
                $sql = 'select jadwaldokter_m.pegawai_id, pegawai_m.nama_pegawai, jadwaldokter_m.instalasi_id, jadwaldokter_m.ruangan_id, ruangan_m.ruangan_nama, jadwaldokter_m.jadwaldokter_hari
                                from jadwaldokter_m
                                left join pegawai_m on pegawai_m.pegawai_id = jadwaldokter_m.pegawai_id
                                left join ruangan_m on ruangan_m.ruangan_id = jadwaldokter_m.ruangan_id
                                where jadwaldokter_m.instalasi_id=:instalasi
                                group by jadwaldokter_m.pegawai_id, pegawai_m.nama_pegawai, ruangan_m.ruangan_nama, jadwaldokter_m.instalasi_id, jadwaldokter_m.ruangan_id, jadwaldokter_m.jadwaldokter_hari';
                $modJadwal = Yii::app()->db->createCommand($sql)->queryAll(true, array(":instalasi"=>$idInstalasi));
                if (count($modJadwal)> 0){
                    $result = $this->renderPartial('_getListJadwalDokter', array('modJadwal'=>$modJadwal), true);
                }
            }
            echo json_encode($result);
            Yii::app()->end();
        }
    }

    // Script Ubah Status Periksa //
    
    public function actionUbahStatusPeriksaRJ(){
        if(Yii::app()->request->isAjaxRequest) {
            $idPendaftaran = $_POST['idPendaftaran'];
            $format = new CustomFormat();
            $model = PendaftaranT::model()->findByPk($id);
            $model->tglselesaiperiksa = date('Y-m-d H:i:s');
            if(isset($_POST['PendaftaranT'])){
                $update = PendaftaranT::model()->updateByPk($id,array('statusperiksa'=>$_POST['PendaftaranT']['statusperiksa'],'tglselesaiperiksa'=>($_POST['PendaftaranT']['tglselesaiperiksa'])));
                    if($update){
                         $data['pesan']='Berhasil';
                    }else{
                        $data['pesan']='Gagal';
                    }
                  echo json_encode($data);
             Yii::app()->end();
            }
        }
    }
    
    // End Ubah Status Periksa //
   
    
    public function actionGetListKamarRI(){
//        if(Yii::app()->request->isAjaxRequest){
             $this->layout = '//layouts/polos';
            $idRuangan = $_POST['idRuangan'];
            
            $model =InformasikamarinapV::model()->findAll('kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed');
            $row = $this->renderKamarRuangan($model);
            if (isset($_POST['idRuangan'])){
                $ruangan = $_POST['idRuangan'];
                $model =InformasikamarinapV::model()->findAll(((!empty($ruangan)) ? "ruangan_id =".$ruangan." and " : "").'kamarruangan_aktif = true order by ruangan_id, kelaspelayanan_id, kamarruangan_nokamar, kamarruangan_nobed');
                $row = $this->renderKamarRuangan($model);
                
                echo json_encode($row);
                Yii::app()->end();
            }
            
           if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                    'status'=>'create_form',
                    'div'=>$this->render('_informasiKamarRI',array('model'=>$model,'row'=>$row),true)));
                exit;
            }
//        }
    }
    
    protected function renderKamarRuangan($model){
            $result = '';
//            if (!is_array($model)){
//                $model = array($model);
//            }
                $tempRuangan = '';
                $list1 = array();
                
                foreach ($model as $i=>$row){
                    if ($row->ruangan_id != $tempRuangan){
                        $tempJumlah = 0;
                        $list1[$row->ruangan_id]['name'] = $row->ruangan_nama;
                        $list1[$row->ruangan_id]['ruangan_id'] = $row->ruangan_id;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['name'] = $row->kamarruangan_nokamar;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kelaspelayanan'] = $row->kelaspelayanan_namalainnya;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['jml'] = $row->kamarruangan_jmlbed;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['name'] = $row->kamarruangan_nokamar;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['no'] = $row->kamarruangan_nobed;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['status'] = $row->kamarruangan_status;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['id'] = $row->kamarruangan_id;
                        $list1[$row->ruangan_id]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['keterangan'] = $row->keterangan_kamar;
                        $tempJumlah = $row->kamarruangan_jmlbed;
                        $tempRuangan = $row->ruangan_id;
                    }
                    else{
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['name'] = $row->kamarruangan_nokamar;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kelaspelayanan'] = $row->kelaspelayanan_namalainnya;
                        if ($row->kamarruangan_jmlbed >= $tempJumlah){
                            $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['jml'] = $row->kamarruangan_jmlbed;
                            $tempJumlah = $row->kamarruangan_jmlbed;
                        }
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['name'] = $row->kamarruangan_nokamar;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['no'] = $row->kamarruangan_nobed;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['status'] = $row->kamarruangan_status;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['id'] = $row->kamarruangan_id;
                        $list1[$tempRuangan]['kamar'][$row->kelaspelayanan_id]['kamar'][$row->kamarruangan_nokamar]['bed'][$i]['keterangan'] = $row->keterangan_kamar;
                    }
                }
//                echo '<pre>';
//                echo print_r($list1);
//                exit();
                foreach ($list1 as $i=>$v){
				
                 $result .= '<div class="contentKamar">';
				 			
						$ruangan = RuanganM::model()->findByPk($v['ruangan_id']);
                        $dataRuangan ='';
						
                        if (count($ruangan) == 1){
                            $dataRuangan .='<table width=\'100px\'>';
                            $dataRuangan .='<tr><td rowspan=2><img src=\''.Yii::app()->baseUrl.'/images/'.$ruangan->ruangan_image.'\' class=\'image_ruangan\'></td><td>Fasilitas</td><td>'.((!empty($ruangan->ruangan_fasilitas)) ? $ruangan->ruangan_fasilitas : " - ").'</td></tr>';
                            $dataRuangan .='<tr><td>Lokasi</td><td>'.((!empty($ruangan->ruangan_lokasi)) ? $ruangan->ruangan_lokasi : " - ").'</td></tr>';
                            $dataRuangan .='</table>';
                        }
                        foreach ($v['kamar'] as $j=>$w){
                            $result .='<div class="pintu"></div><h3 class="popover-title"><img src=\''. Yii::app()->baseUrl.'/images/blue-home-icon.png\' style=\'height:30px;\'/>'.$v['name'].' - '.$w['kelaspelayanan'].' - '.$w['jml'].'<a href="" class="pull-right poping" data-content="'.$dataRuangan.'" onclick="return false;"><img src=\''. Yii::app()->baseUrl.'/images/fasilitas.png\' style=\'height:30px;\'/>Detail</a></h3>
                                <ul>';
                            foreach ($w['kamar'] as $x=>$y){
                                $result .='<li class="bed">
                                    <div class="popover-inner">
                                        <h6 class="popover-title">'.$y['name'].'</h3>
                                        <div class="popover-content">';
                                    foreach ($y['bed'] as $a=>$b){
//                                        echo $b['id']."<br>";
                                        $kamar = MasukkamarT::model()->find('kamarruangan_id = '.$b['id'].' order by tglmasukkamar desc');
//                                        echo "<pre>";
//                                        echo print_r($kamar->admisi->pasien->nama_pasien);
//                                        echo "</pre>";
                                      
                                        $dataPasien = '';
                                        if (count($kamar) == 1){
                                            $dataPasien .='<table>';
                                            $dataPasien .='<tr><td>No RM </td><td>: '.$kamar->admisi->pasien->no_rekam_medik.'</td></tr>';
                                            $dataPasien .='<tr><td>Nama </td><td>: '.$kamar->admisi->pasien->nama_pasien.'</td></tr>';
                                            $dataPasien .='<tr><td>Jenis Kelamin </td><td>: '.$kamar->admisi->pasien->jeniskelamin.'</td></tr>';
                                            $dataPasien .= '</table>';
//                                            $dataPasien .='<p><label class=\'control-label\'>Nama :</label> '.$kamar->admisi->pasien->nama_pasien.'</p>';
//                                            $dataPasien .='<p><label class=\'control-label\'>Jenis Kelamin :</label> '.$kamar->admisi->pasien->jeniskelamin.'</p>';
                                        }
					if($b['keterangan'] == "MENUNGGU" || $b['keterangan'] == "PASIEN MENUNGGU DI RUANGAN"){
                                            $result .='<p><a href="" class="btn '.(($b['status']) ? 'btn-danger' : 'btn-primary').'" rel="popover" data-content="'.(($b['status']) ? 'Pasien Masih Menunggu Di Ruangan' : $dataPasien).'" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/'.(($b['status']) ?  'RanjangRumahSakit3' : 'RanjangRumahSakit').'.png\'/>No Bed : '.$b['no'].'</a></p>';
                                        }else{
                                             $result .='<p><a href="" class="btn '.(($b['status']) ? 'btn-danger' : 'btn-primary').'" rel="popover" data-content="'.(($b['status']) ? 'Pasien Kosong' : $dataPasien).'" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/'.(($b['status']) ?  'RanjangRumahSakit2' : 'RanjangRumahSakit').'.png\'/>No Bed : '.$b['no'].'</a></p>';
                                        }
//                                        $result .='<p><a href="" class="btn '.(($b['status']) ? 'btn-danger' : 'btn-primary').'" rel="popover" data-content="'.(($b['status']) ? 'Pasien Kosong' : $dataPasien).'" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/'.(($b['status']) ?  'RanjangRumahSakit2' : 'RanjangRumahSakit').'.png\'/>No Bed : '.$b['no'].'</a></p>';
                                    }
                                    for($d=1;$d<=($w['jml'] - (count($y['bed'])));$d++){
//                                        echo $d;
                                        $result .='<p><a href="" class="btn btn-info" onclick="return false"><img src=\''. Yii::app()->baseUrl.'/images/delete.png\'/>Kosong</a></p>';
                                    }
                                        $result .='</div>
                                    </div>
                                </li>';
                            }
                            $result .='</ul>';
                        }
                       
                    $result .='</div>';
                }
            
//            exit();
            return $result;
        }
        
        
      // Added function BuatSessionUbahStatusKonfirmasiBooking & UbahStatusKonfirmasiBooking, 2 APRIL 2013 //
        
        public function actionBuatSessionUbahStatusKonfirmasiBooking()
        {
            $idBooking = $_POST['idBooking'];
            if(!empty($_POST['idBooking']))
            {
                $idBooking = $_POST['idBooking'];
                Yii::app()->session['bookingkamar_id'] = $idBooking;
            }
            Yii::app()->session['bookingkamar_id'] =  $idBooking;
            echo CJSON::encode(array(
                'bookingkamar_id'=>Yii::app()->session['bookingkamar_id']));

        }
        public function actionGetStatusKamar()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $idKamarruangan = $_POST['idKamarruangan'];
                $model = KamarruanganM::model()->findByPk($idKamarruangan);
                $data['status'] = "<font>".((!empty($model->keterangan_kamar)) ? "Status Kamar : ".$model->keterangan_kamar : "Status Kamar : KOSONG")."</font>";
                $data['kamar'] = !empty($model->keterangan_kamar) ? $model->keterangan_kamar : "KOSONG";
                  echo json_encode($data);
                 Yii::app()->end();
                }
         }

        
         public function actionUbahStatusKonfirmasiBooking()
         {
//            $idBooking = Yii::app()->session['bookingkamar_id'];
             $idbooking = $_POST['idbooking'];
             $status = $_POST['status'];
             $idkamarruangan = $_POST['idkamarruangan'];
            $model = BookingkamarT::model()->findByPk($idbooking);
            if(!empty($_POST['idbooking']))
            {
                if($status == "BELUM KONFIRMASI"){
                    $statusbook = "SUDAH KONFIRMASI";
                }else if($status == "SUDAH KONFIRMASI"){
                    $statusbook = "BATAL BOOKING";
                }
               $update = BookingkamarT::model()->updateByPk($idbooking,array('statuskonfirmasi'=>$statusbook));
               // $kamar = KamarruanganM::model()->updateByPk($idkamarruangan,array('keterangan_kamar'=>"KOSONG"));
               // if($update && $kamar)
               if($update)
                {
                   if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'berhasil',
                            'div'=>"<div class='flash-success'>Data Booking Kamar <b></b> berhasil disimpan </div>",
                            ));
                        exit;
                    }
                }
                else
                {

                    if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'gagal',
                            'div'=>"<div class='flash-error'>Data Booking Kamar <b></b> gagal disimpan </div>",
                            ));
                        exit;
                    }
                }
            }

//            if (Yii::app()->request->isAjaxRequest)
//            {
//                echo CJSON::encode(array(
//                    'status'=>'create_form',
//                    'div'=>$this->renderPartial('_ubahStatusKonfirmasiBooking',array('model'=>$model),true)));
//                exit;
//            }
        }
     // End function BuatSessionUbahStatusKonfirmasiBooking //
        
    public function actionAddAsalRujukan()
    {
        $model = new AsalrujukanM;

        if(isset($_POST['AsalrujukanM']))
        {
            $model->attributes = $_POST['AsalrujukanM'];
            $model->asalrujukan_aktif = true;
            if($model->save())
            {
                $data=AsalrujukanM::model()->findAll(array('order'=>'asalrujukan_nama'));
                $data=CHtml::listData($data,'asalrujukan_id','asalrujukan_nama');

                if(empty($data)){
                    $asalrujukanOptions = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    $asalrujukanOptions = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($data as $value=>$name)
                    {
                        $asalrujukanOptions .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }

                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Asal Rujukan <b>".$_POST['AsalrujukanM']['asalrujukan_nama']."</b> berhasil ditambahkan </div>",
                        'asalrujukan'=>$asalrujukanOptions,
                        'asalrujukan_id'=>$model->asalrujukan_id,
                        ));
                    exit;
                }
            }
            
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddAsalRujukan', array('model'=>$model,), true)));
            exit;
        }
    }
    
    public function actionAddNamaRujukan()
    {
        $model = new RujukandariM;
        $modAsalRujukan = AsalrujukanM::model()->findAll();

        if(isset($_POST['RujukandariM']))
        {
            $model->attributes = $_POST['RujukandariM'];
            if($model->save())
            {
                $data= RujukandariM::model()->findAllByAttributes(array('asalrujukan_id'=>$_POST['RujukandariM']['asalrujukan_id'],),array('order'=>'namaperujuk'));
                $data=CHtml::listData($data,'rujukandari_id','namaperujuk');

                if(empty($data)){
                    $namaRujukanOptions = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    $namaRujukanOptions = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($data as $value=>$name)
                    {
                        $namaRujukanOptions .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                }

                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Nama Perujuk <b>".$_POST['RujukandariM']['namaperujuk']."</b> berhasil ditambahkan </div>",
                        'namarujukan'=>$namaRujukanOptions,
                        'namaperujuk'=>$model->namaperujuk,
                        'rujukandari_id'=>$model->rujukandari_id,
                        'asalrujukan_id'=>$model->asalrujukan_id,
                        ));
                    exit;
                }
            }

        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formAddNamaRujukan', array('model'=>$model,'$modAsalRujukan'=>$modAsalRujukan), true)));
            exit;
        }
    }
 
    // -- GANTI PERIODE LAPORAN -- //
    
    public function actionGantiPeriode()
        {
            if(Yii::app()->request->isAjaxRequest){
                $namaPeriode = $_POST['namaPeriode'];
                $month = date('m');
                $year = date('Y');
                $jumHari = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                
                $bulan =  date ("d M Y", mktime (0,0,0,$month,$jumHari,$year));
                $awalBulan =  date ("d M Y", mktime (0,0,0,$month,1,$year));
                $awalTahun=  date ("d M Y", mktime (0,0,0,1,1,$year));
                $akhirTahun=  date ("d M Y", mktime (0,0,0,12,31,$year));
                
                
                $lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
                $nextyear  = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1);
                
                  if($namaPeriode == "hari"){
                   $awal = date('d M Y 00:00:00');
                   $akhir = date('d M Y 23:59:59');
                }else if($namaPeriode == "bulan"){
                   $awal = ''.$awalBulan.' 00:00:00';
                   $akhir = ''.$bulan.' 23:59:59';

                }else if($namaPeriode == "tahun"){
                     $awal = ''.$awalTahun.' 00:00:00';

                    $akhir = ''.$akhirTahun.' 23:59:59';

                }else{
                    $awal = date('d M Y 00:00:00 ');
                    $akhir = date('d M Y 23:59:59');
                }
                
                 $data['periodeawal']  = $awal;
                 $data['periodeakhir'] = $akhir;
                 $data['namaPeriode'] = $namaPeriode;
                 
                echo CJSON::encode($data);
                    Yii::app()->end();
            }
        }
    // -- END GANTI PERIODE LAPORAN -- //
    
        public function actionGantiPeriode1()
        {
            if(Yii::app()->request->isAjaxRequest){
                $namaPeriode = $_POST['namaPeriode'];
                $month = date('m');
                $year = date('Y');
                $jumHari = cal_days_in_month(CAL_GREGORIAN, $month, $year);
                
                $bulan =  date ("d M Y", mktime (0,0,0,$month,$jumHari,$year));
                $awalBulan =  date ("d M Y", mktime (0,0,0,$month,1,$year));
                $awalTahun=  date ("d M Y", mktime (0,0,0,1,1,$year));
                $akhirTahun=  date ("d M Y", mktime (0,0,0,12,31,$year));
                
                
                $lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
                $nextyear  = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1);
                
                if($namaPeriode == "hari"){
                   $awal = date('d M Y 00:00:00');
                   $akhir = date('d M Y 23:59:59');
                }else if($namaPeriode == "bulan"){
                   $awal = ''.$awalBulan.' 00:00:00';
                   $akhir = ''.$bulan.' 23:59:59';

                }else if($namaPeriode == "tahun"){
                     $awal = ''.$awalTahun.' 00:00:00';

                    $akhir = ''.$akhirTahun.' 23:59:59';

                }else{
                    $awal = date('d M Y 00:00:00 ');
                    $akhir = date('d M Y 23:59:59');
                }
                
                 $data['periodeawal']  = $awal;
                 $data['periodeakhir'] = $akhir;
                 $data['namaPeriode'] = $namaPeriode;
                 
                echo CJSON::encode($data);
                    Yii::app()->end();
            }
        }
    public function actionGetDataPegawai()
    {
        if(Yii::app()->request->isAjaxRequest){
            $data = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$_POST['idPegawai']));
            $post = array(
                'nomorindukpegawai'=>$data->nomorindukpegawai,
                'pegawai_id'=>$data->pegawai_id,
                'nama_pegawai'=>$data->nama_pegawai,
                'tempatlahir_pegawai'=>$data->tempatlahir_pegawai,
                'tgl_lahirpegawai' => $data->tgl_lahirpegawai,
                'jabatan_nama'=> (isset($data->jabatan->jabatan_nama) ? $data->jabatan->jabatan_nama : ''),
                'pangkat_nama'=> (isset($data->pangkat->pangkat_nama) ? $data->pangkat->pangkat_nama : ''),
                'kategoripegawai'=>$data->kategoripegawai,
                'kategoripegawaiasal'=>$data->kategoripegawaiasal,
                'kelompokpegawai_nama'=> (isset($data->kelompokpegawai->kelompokpegawai_nama) ? $data->kelompokpegawai->kelompokpegawai_nama : ''),
                'pendidikan_nama'=> (isset($data->pendidikan->pendidikan_nama) ? $data->pendidikan->pendidikan_nama : ''),
                'jeniskelamin'=>$data->jeniskelamin,
                'statusperkawinan'=>$data->statusperkawinan,
                'alamat_pegawai'=>$data->alamat_pegawai,
                'photopegawai'=>(!is_null($data->photopegawai) ? $data->photopegawai : ''),
            );
            echo CJSON::encode($post);
            Yii::app()->end();
        }
    }
    
    
    public function actionUbahJenisKelamin()
    {
        $model = new PasienM;
        if(isset($_POST['PasienM']))
        {
            $model->attributes = $_POST['PasienM'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $attributes = array('jeniskelamin'=>$_POST['PasienM']['jeniskelamin']);
                $save = PasienM::model()->updateByPk($_POST['PasienM']['pasien_id'], $attributes);
                if($save)
                {
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Jenis Kelamin Pasien.</div>",
                        ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                        ));
                }
                exit;
            }catch(Exception $exc) {
                $transaction->rollback();
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahJenisKelamin', array('model'=>$model), true)));
            exit;
        }
    }

    public function actionGetKasusPenyakit()
    {
        if (Yii::app()->request->isAjaxRequest){
            $ruangan_id = $_POST['id_ruangan'];
            $jenisKasusPenyakit = array();
            if(!empty($ruangan_id)) {
                $sql = "SELECT jeniskasuspenyakit_m.jeniskasuspenyakit_id, jeniskasuspenyakit_m.jeniskasuspenyakit_nama
                        FROM jeniskasuspenyakit_m
                        JOIN kasuspenyakitruangan_m ON jeniskasuspenyakit_m.jeniskasuspenyakit_id = kasuspenyakitruangan_m.jeniskasuspenyakit_id
                        JOIN ruangan_m ON kasuspenyakitruangan_m.ruangan_id = ruangan_m.ruangan_id
                        WHERE ruangan_m.ruangan_id = '$ruangan_id'
                        ORDER BY jeniskasuspenyakit_m.jeniskasuspenyakit_id";
                $modJenKasusPenyakit = JeniskasuspenyakitM::model()->findAllBySql($sql);

                $jenisKasusPenyakit = CHtml::listData($modJenKasusPenyakit,'jeniskasuspenyakit_id','jeniskasuspenyakit_nama');
            }
            if(empty($jenisKasusPenyakit)){
                $option = CHtml::tag('option', array('value'=>''),CHtml::encode('-- Pilih --'),true);
            }else{
                $option = CHtml::tag('option', array('value'=>''),CHtml::encode('-- Pilih --'),true);
                foreach($jenisKasusPenyakit as $value=>$name)
                {
                    $option .= CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
            $dataList['listPenyakit'] = $option;
            echo json_encode($dataList);
            Yii::app()->end();
        }
    }
    
    public function actionGetDataPendaftaranRJ()
    {
        if (Yii::app()->request->isAjaxRequest){
            $id_pendaftaran = $_POST['idPendaftaran'];
            $model = InfokunjunganrjV::model()->findByAttributes(array('pendaftaran_id'=>$id_pendaftaran));
            $attributes = $model->attributeNames();
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }

    public function actionGetDataPendaftaranRD()
    {
        if (Yii::app()->request->isAjaxRequest){
            $id_pendaftaran = $_POST['idPendaftaran'];
            $model = InfokunjunganrdV::model()->findByAttributes(array('pendaftaran_id'=>$id_pendaftaran));
            $attributes = $model->attributeNames();
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }

    public function actionGetDataPendaftaranRI()
    {
        if (Yii::app()->request->isAjaxRequest){
            $id_pendaftaran = $_POST['idPendaftaran'];
            $model = InfokunjunganriV::model()->findByAttributes(array('pendaftaran_id'=>$id_pendaftaran));
            $attributes = $model->attributeNames();
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }
    
    public function actionUbahKelompokPenyakit()
    {
        $model = new PendaftaranT;
        $menu = (isset($_REQUEST['menu']) ? $_REQUEST['menu'] : "");
        if(isset($_POST['PendaftaranT']))
        {
            if($_POST['PendaftaranT']['jeniskasuspenyakit_id'] != "")
            {
                $model->attributes = $_POST['PendaftaranT'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $attributes = array('jeniskasuspenyakit_id'=>$_POST['PendaftaranT']['jeniskasuspenyakit_id']);
                    $save = $model::model()->updateByPk($_POST['PendaftaranT']['pendaftaran_id'], $attributes);
                    if($save)
                    {
                        $transaction->commit();
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Berhasil merubah Kelompok Penyakit.</div>",
                            ));
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                            ));
                    }
                    exit;
                }catch(Exception $exc) {
                    $transaction->rollback();
                }
            }else{
                echo CJSON::encode(
                    array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah Kelompok Penyakit.</div>",
                    )
                );
                exit;
            }
        }
        
        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahKelompokPenyakit', array('model'=>$model, 'menu'=>$menu), true)));
            exit;
        }
    }
    
    public function actionUbahDokterPeriksa()
    {
        $model = new PendaftaranT;
        $menu = (isset($_REQUEST['menu']) ? $_REQUEST['menu'] : "");
        if(isset($_POST['PendaftaranT']))
        {
            if($_POST['PendaftaranT']['pegawai_id'] != "")
            {
                $model->attributes = $_POST['PendaftaranT'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $attributes = array('pegawai_id'=>$_POST['PendaftaranT']['pegawai_id']);
                    $save = $model::model()->updateByPk($_POST['PendaftaranT']['pendaftaran_id'], $attributes);
                    if($save)
                    {
                        $transaction->commit();
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Berhasil merubah Dokter Periksa.</div>",
                            ));
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                            ));
                    }
                    exit;
                }catch(Exception $exc) {
                    $transaction->rollback();
                }
            }else{
                echo CJSON::encode(
                    array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah Dokter Periksa.</div>",
                    )
                );
                exit;
            }
        }
        
        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahDokterPeriksa', array('model'=>$model,'menu'=>$menu), true)));
            exit;
        }
    }
    
    public function actionUbahDokterPeriksaRI()
    {
        $model = new PasienadmisiT;
        $modPendaftaran = new PendaftaranT;
        if(isset($_POST['PasienadmisiT']))
        {
            if($_POST['PasienadmisiT']['pegawai_id'] != "")
            {
                $idPendaftaran = $_POST['PasienadmisiT']['pendaftaran_id'];

                $model->attributes = $_POST['PasienadmisiT'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $attributes = array('pendaftaran_id'=>$idPendaftaran);
                    $data = $model::model()->findByAttributes($attributes);
                    
                    $attributes = array('pegawai_id'=>$_POST['PasienadmisiT']['pegawai_id']);
                    $save = $model::model()->updateByPk($data['pasienadmisi_id'], $attributes);
                    $modPendaftaran->pendaftaran_id = $idPendaftaran;
                    $modPendaftaran->pegawai_id = $_POST['PasienadmisiT']['pegawai_id'];

                    $savePendaftaran = $modPendaftaran::model()->updateByPk($idPendaftaran, $attributes);

                    if($save && $savePendaftaran)
                    {
                        $transaction->commit();
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Berhasil merubah Dokter Periksa.</div>",
                            ));
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                            ));
                    }
                    exit;
                }catch(Exception $exc){
                    $transaction->rollback();
                }
            }else{
                echo CJSON::encode(
                    array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah Dokter Periksa.</div>",
                    )
                );
                exit;
            }
        }
        
        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahDokterPeriksaRI', array('model'=>$model), true)));
            exit;
        }
    }
    
    public function actionUbahKelasPelayanan()
    {
        $model = new PendaftaranT;
        $menu = (isset($_REQUEST['menu']) ? $_REQUEST['menu'] : "");
        if(isset($_POST['PendaftaranT']))
        {
            if($_POST['PendaftaranT']['kelaspelayanan_id'] != "")
            {
                $model->attributes = $_POST['PendaftaranT'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $attributes = array('kelaspelayanan_id'=>$_POST['PendaftaranT']['kelaspelayanan_id']);
                    $save = $model::model()->updateByPk($_POST['PendaftaranT']['pendaftaran_id'], $attributes);
                    if($save)
                    {
                        $transaction->commit();
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Berhasil merubah Kelas Pelayanan.</div>",
                            ));
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                            ));
                    }
                    exit;
                }catch(Exception $exc) {
                    $transaction->rollback();
                }
            }else{
                echo CJSON::encode(
                    array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Kelas Pelayanan.</div>",
                    )
                );
                exit;
            }
        }
        
        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahKelasPelayananRJ', array('model'=>$model,'menu'=>$menu), true)));
            exit;
        }
    }
    
    public function actionUbahKelasPelayananRI()
    {
        $model = new PasienadmisiT;
        $modMasukKamar = new MasukkamarT;
        if(isset($_POST['PasienadmisiT']))
        {
            if($_POST['PasienadmisiT']['kelaspelayanan_id'] != "")
            {
                $model->attributes = $_POST['PasienadmisiT'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $attributes = array('pendaftaran_id'=>$_POST['PasienadmisiT']['pendaftaran_id']);
                    $data = $model::model()->findByAttributes($attributes);
                    
                    $attributes = array('kelaspelayanan_id'=>$_POST['PasienadmisiT']['kelaspelayanan_id']);
                    $save = $model::model()->updateByPk($data['pasienadmisi_id'], $attributes);

                    $dataMK = $modMasukKamar::model()->findByAttributes(array('pasienadmisi_id'=>$data['pasienadmisi_id']));
                    $saveMK = $modMasukKamar::model()->updateByPk($dataMK['masukkamar_id'],$attributes);

                    
                    if($save && $saveMK)
                    {
                        $transaction->commit();
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Berhasil merubah Kelas Pelayanan.</div>",
                            ));
                    }else{
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                            ));
                    }
                    exit;
                }catch(Exception $exc) {
                    $transaction->rollback();
                }
            }else{
                echo CJSON::encode(
                    array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Kelas Pelayanan.</div>",
                    )
                );
                exit;
            }
        }
        
        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahKelasPelayananRI', array('model'=>$model), true)));
            exit;
        }
    }

    public function actionGetLamaKontrak()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest()) {
            $format = new CustomFormat;
            $tglAwal = $format->formatDateMediumForDB($_POST['tglAwal']);
            $tglAkhir = $format->formatDateMediumForDB($_POST['tglAkhir']);
//            $dob=$tglLahir;
//            $today=date("Y-m-d");
            list($y,$m,$d)=explode('-',$tglAwal);
            list($ty,$tm,$td)=explode('-',$tglAkhir);
            
            if($td-$d<0){
                $day=($td+30)-$d;
                $tm--;
            }
            else{
                $day=$td-$d;
            }
            if($tm-$m<0){
                $month=($tm+12)-$m;
                $ty--;
            }
            else{
                $month=$tm-$m;
            }
            $year=$ty-$y;
            
            $data['kontrak'] = str_pad($year, 2, '0', STR_PAD_LEFT).' Thn '. str_pad($month, 2, '0', STR_PAD_LEFT) .' Bln '. str_pad($day, 2, '0', STR_PAD_LEFT).' Hr';
            
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    

     public function actionUbahRekeningDebitKreditPenerimaan()
    {
        $model = new JenispenerimaanM;
        if(isset($_POST['JenispenerimaanM']))
        {
            $model->attributes = $_POST['JenispenerimaanM'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $attributes = array('rekeningdebit_id'=>$_POST['JenispenerimaanM']['rekeningdebit_id'],'rekeningkredit_id'=>$_POST['JenispenerimaanM']['rekeningkredit_id']);
                $save = JenispenerimaanM::model()->updateByPk($_POST['JenispenerimaanM']['jenispenerimaan_id'], $attributes);
                if($save)
                {
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Rekening.</div>",
                        ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                        ));
                }
                exit;
            }catch(Exception $exc) {
                $transaction->rollback();
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_formUbahRekeningDebitKreditPenerimaan', array('model'=>$model), true)));
            exit;
        }
    }

    public function actionGetTreeMenu()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $criteria=new CDbCriteria;
            $rekeningSatu = new Rekening1M;
            $rekeningDua = new Rekening2M;
            $rekeningTiga = new Rekening3M;
            $rekeningEmpat = new Rekening4M;
            $rekeningLima = new Rekening5M;
            
            $params = array('rekening1_aktif' => true);
            $criteria->order = 'rekening1_id';
            $result = $rekeningSatu->findAllByAttributes($params, $criteria);
            $parent_satu = '';
            foreach($result as $val)
            {
                $params_dua = array(
                    'rekening2_aktif' => true,
                    'rekening1_id' => $val->rekening1_id,
                );
                $criteria->order = 'rekening2_id';
                $result_dua = $rekeningDua->findAllByAttributes($params_dua, $criteria);
                $parent_dua = '';
                foreach($result_dua as $val_dua)
                {
                    $params_tiga = array(
                        'rekening3_aktif' => true,
                        'rekening1_id' => $val_dua->rekening1_id,
                        'rekening2_id' => $val_dua->rekening2_id,
                    );
                    $criteria->order = 'rekening3_id';
                    $result_tiga = $rekeningTiga->findAllByAttributes($params_tiga, $criteria);
                    $parent_tiga = '';
                    foreach($result_tiga as $val_tiga)
                    {
                        $params_empat = array(
                            'rekening4_aktif' => true,
                            'rekening1_id' => $val_tiga->rekening1_id,
                            'rekening2_id' => $val_tiga->rekening2_id,
                            'rekening3_id' => $val_tiga->rekening3_id,
                        );
                        $criteria->order = 'rekening4_id';
                        $result_empat = $rekeningEmpat->findAllByAttributes($params_empat, $criteria);
                        $parent_empat = '';
                        foreach($result_empat as $val_empat)
                        {
                            $params_lima = array(
                                'rekening5_aktif' => true,
                                'rekening1_id' => $val_empat->rekening1_id,
                                'rekening2_id' => $val_empat->rekening2_id,
                                'rekening3_id' => $val_empat->rekening3_id,
                                'rekening4_id' => $val_empat->rekening4_id,
                            );
                            $criteria->order = 'rekening5_id';
                            $result_lima = $rekeningLima->findAllByAttributes($params_lima, $criteria);
                            $parent_lima = '';
                            foreach($result_lima as $val_lima)
                            {
                                $parent_lima .= "<li><span class='file'>". $val_lima->nmrekening5 ."<span style='float:right'><a value='". $val_lima->rekening5_id ."' href='#' onclick='editObyekDetailRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Detail Obyek Rekening'><i class='icon-pencil-brown'></i></a></span></span></li>";
                            }

                            $kode_kelompok_lima = $val->kdrekening1 . '_' . $val_dua->kdrekening2 . '_' . $val_tiga->kdrekening3 . '_' . $val_empat->kdrekening4;
                            $id_kelompok_lima = $val_empat->rekening1_id . '_' . $val_empat->rekening2_id . '_' . $val_empat->rekening3_id . '_' . $val_empat->rekening4_id;
                            if(count($result_lima) > 0)
                            {
                                $parent_empat .= "<li><span class='folder'>". $val_empat->nmrekening4 ."<span style='float:right'><a max_kode='". $val_lima->kdrekening5 ."' id_rek='". $id_kelompok_lima ."' kode_rek='". $kode_kelompok_lima ."' href='#' onclick='tambahObyekDetailRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah detail Objek Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_empat->rekening4_id ."' href='#' onclick='editObyekRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Obyek Rekening'><i class='icon-pencil-brown'></i></a></span></span><ul>". $parent_lima ."</ul></li>";
                            }else{
                                $parent_empat .= "<li class='expandable'><span class='folder'>". $val_empat->nmrekening4 ."<span style='float:right'><a max_kode='0' id_rek='". $id_kelompok_lima ."' kode_rek='". $kode_kelompok_lima ."' href='#' onclick='tambahObyekDetailRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah detail Objek Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_empat->rekening4_id ."' href='#' onclick='editObyekRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Obyek Rekening'><i class='icon-pencil-brown'></i></a></span></span></li>";
                            }
                        }
//
                        $kode_kelompok_empat = $val->kdrekening1 . '_' . $val_dua->kdrekening2 . '_' . $val_tiga->kdrekening3;
                        $id_kelompok_empat = $val_tiga->rekening1_id . '_' . $val_tiga->rekening2_id . '_' . $val_tiga->rekening3_id;
                        if(count($result_empat) > 0)
                        {
                            $parent_tiga .= "<li><span class='folder'>". $val_tiga->nmrekening3 ."<span style='float:right'><a max_kode='". $val_empat->kdrekening4 ."' id_rek='". $id_kelompok_empat ."' kode_rek='". $kode_kelompok_empat ."' href='#' onclick='tambahObyekRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Objek Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_tiga->rekening3_id ."' href='#' onclick='editJenisRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Jenis Rekening'><i class='icon-pencil-brown'></i></a></span></span><ul>". $parent_empat ."</ul></li>";
                        }else{
                            $parent_tiga .= "<li class='expandable'><span class='folder'>". $val_tiga->nmrekening3 ."<span style='float:right'><a max_kode='0' id_rek='". $id_kelompok_empat ."' kode_rek='". $kode_kelompok_empat ."' href='#' onclick='tambahObyekRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Objek Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_tiga->rekening3_id ."' href='#' onclick='editJenisRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Jenis Rekening'><i class='icon-pencil-brown'></i></a></span></span></li>";
                        }


                    }

                    $kode_kelompok = $val->kdrekening1 . '_' . $val_dua->kdrekening2;
                    $id_kelompok = $val_dua->rekening1_id . '_' . $val_dua->rekening2_id;
                    if(count($result_tiga) > 0)
                    {
                        $parent_dua .= "<li id='". $id_kelompok ."'><span class='folder'>". $val_dua->nmrekening2 ."<span style='float:right'><a max_kode='". $val_tiga->kdrekening3 ."' id_rek='". $id_kelompok ."' kode_rek='". $kode_kelompok ."' href='#' onclick='tambahJenisRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Jenis Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_dua->rekening2_id ."' href='#' onclick='editKelompokRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Kelompok Rekening'><i class='icon-pencil-brown'></i></a></span></span><ul>". $parent_tiga ."</ul></li>";
                    }else{
                        $parent_dua .= "<li id='". $id_kelompok ."' class='expandable'><span class='folder'>". $val_dua->nmrekening2 ."<span style='float:right'><a max_kode='0' id_rek='". $id_kelompok ."' kode_rek='". $kode_kelompok ."' href='#' onclick='tambahJenisRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Jenis Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val_dua->rekening2_id ."' href='#' onclick='editKelompokRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Kelompok Rekening'><i class='icon-pencil-brown'></i></a></span></span></li>";
                    }

                }

                $value_kode = $val->kdrekening1;
                $value_id = $val->rekening1_id;
                if(count($result_dua) > 0)
                {
                    $parent_satu .= "<li id='". $value_id ."'><span class='folder'>". $val->nmrekening1 ."<span style='float:right'><a max_kode='". $val_dua->kdrekening2 ."' id_rek='". $value_id ."' kode_rek='". $value_kode ."' href='#' onclick='tambahKelompokRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Kelompok Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val->rekening1_id ."' href='#' onclick='editStrukturRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Struktur Rekening'><i class='icon-pencil-brown'></i></a></span></span><ul>". $parent_dua ."</ul></li>";
                }else{
                    $parent_satu .= "<li id='". $value_id ."' class='expandable'><span class='folder'>". $val->nmrekening1 ."<span style='float:right'><a max_kode='0' id_rek='". $value_id ."' kode_rek='". $value_kode ."' href='#' onclick='tambahKelompokRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk menambah Kelompok Rekening'><i class='icon-plus-sign'></i></a></span><span style='float:right'><a value='". $val->rekening1_id ."' href='#' onclick='editStrukturRekening(this);return false;' rel='tooltip' data-original-title='Klik untuk edit Struktur Rekening'><i class='icon-pencil-brown'></i></a></span></span></li>";
                }
            }

            if(count($result) > 0)
            {
                $text = '<span class="folder">Struktur Rekening<span style="float:right"><a max_kode = "'. $val->kdrekening1 .'" href="#" onclick="tambahStrukturRekening(this);return false;" rel="tooltip" data-original-title="Klik untuk menambah Struktur Rekening"><i class="icon-plus-sign"></i></a></span></span>';
                $parent_satu = $text . '<ul>' . $parent_satu . '</ul>';
            }
            echo json_encode($parent_satu);
            Yii::app()->end();
        }
    }

    public function actionGetDataObatAlkes()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $criteria = new CDbCriteria();
            $criteria->compare('obatalkes_nama', $_GET['obatalkes_id']);
            $models = ObatalkesM::model()->findAll($criteria);
            echo CJSON::encode($model->attribute);
            Yii::app()->end();
        }
    }

    public function actionGetPasienLama()
    {

      if(isset($_POST['norm']) && $_POST['norm']!='')
      {
        $criteria = new CDbCriteria();
        $criteria->compare('no_rekam_medik', $_POST['norm']);
        $models = PasienM::model()->find($criteria);

        echo CJSON::encode($models->attributes);
        Yii::app()->end();
        
      }
    }
    
    /** fungsi untuk update cetakan  **/
    public function actionUpdateJumlahCetakan()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $uangmuka = RincianCetakan::model()->findByAttributes($_POST['id']);
            /*
            $uangmuka = RincianCetakan::model()->findByAttributes(
                array(
                    'pendaftaran_id'=>$id,
                    'jenis_cetakan'=>Params::jenisCetakan('rincian_tagihan')
                )
            );
            */
            if($uangmuka)
            {
                
                /** update informasi cetakan **/
                $update = $uangmuka;
                $update->jumlah = ($uangmuka['jumlah']+1);
                
                $is_update = true;
                if(!$update->save())
                {
                    $is_update = false;
                }
                
                $result = array(
                    'status'=>($is_update == false ? 'not': 'ok'),
                    'jumlah'=>$uangmuka['jumlah']
                );
                
            }else{
                
                /** insert informasi cetakan **/
                $attributes = array(
                    'jumlah'=>1
                );
                $insert = new RincianCetakan;
                $insert->attributes = $_POST['id'];
                $insert->jumlah = 1;
                
                $is_insert = true;
                if(!$insert->save())
                {
                    $is_insert = false;
                }
                $result = array(
                    'status'=>($is_insert == false ? 'not': 'ok'),
                    'jumlah'=>1
                );
            }
            
            echo CJSON::encode($result);
            Yii::app()->end();
        }
    }
    
    
    /*
     * ambil data rekening berdasarkan penerimaan Kas
     * modul > akutansi > penerimaan_kas
     */
    public function actionGetDataRekeningByJnsPenerimaan()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $jenispenerimaan_id = $_POST['jenispenerimaan_id'];
            $criteria = new CDbCriteria;
//            $criteria->select = '*, jnspenerimaanrek_m.saldonormal';
//            $criteria->join = '
//                JOIN jnspenerimaanrek_m ON
//                    jnspenerimaanrek_m.rekening1_id = t.struktur_id AND
//                    jnspenerimaanrek_m.rekening2_id = t.kelompok_id AND
//                    jnspenerimaanrek_m.rekening3_id = t.jenis_id AND
//                    jnspenerimaanrek_m.rekening4_id = t.obyek_id AND
//                    jnspenerimaanrek_m.rekening5_id = t.rincianobyek_id
//            ';
            $criteria->condition = 'jenispenerimaan_id = :jenispenerimaan_id';
            $criteria->params = array(':jenispenerimaan_id'=>$jenispenerimaan_id);
//            $model = RekeningakuntansiV::model()->findAll($criteria);
            $model = JenispenerimaanrekeningV::model()->findAll($criteria);
            if($model)
            {
                echo CJSON::encode(
//                    $this->renderPartial('__formKodeRekening', array('model'=>$model), true)
                    $this->renderPartial('akuntansi.views.penerimaanUmum.__formKodeRekening', array('model'=>$model), true)
                );
            }
            Yii::app()->end();
        }
    }
    /*
     * ambil data rekening berdasarkan pengeluaran Kas
     * modul > akutansi > pengeluaran_kas & Penggajian
     */
    public function actionGetDataRekeningByJnsPengeluaran()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $jenispengeluaran_id = $_POST['jenispengeluaran_id'];
            $criteria = new CDbCriteria;
//            $criteria->select = '*, jnspengeluaranrek_m.saldonormal';
//            $criteria->join = '
//                JOIN jnspengeluaranrek_m ON
//                    jnspengeluaranrek_m.rekening1_id = t.struktur_id AND
//                    jnspengeluaranrek_m.rekening2_id = t.kelompok_id AND
//                    jnspengeluaranrek_m.rekening3_id = t.jenis_id AND
//                    jnspengeluaranrek_m.rekening4_id = t.obyek_id AND
//                    jnspengeluaranrek_m.rekening5_id = t.rincianobyek_id
//            ';
            $criteria->condition = 'jenispengeluaran_id = :jenispengeluaran_id';
            $criteria->params = array(':jenispengeluaran_id'=>$jenispengeluaran_id);
            if($jenispengeluaran_id==Params::ID_REKENING_PENJUALAN_KENDARAAN || $jenispengeluaran_id==Params::ID_REKENING_PENJUALAN_NON_MEDIS || $jenispengeluaran_id==Params::ID_REKENING_PENJUALAN_MEDIS){
              $criteria->order ='saldonormal';
            }else{
              $criteria->order ='saldonormal, kdkelompok';
            }
//            $model = RekeningakuntansiV::model()->findAll($criteria);
            $model = JenispengeluaranrekeningV::model()->findAll($criteria);
            if($model)
            {
                echo CJSON::encode(
//                    $this->renderPartial('__formKodeRekening', array('model'=>$model), true)
                    $this->renderPartial('akuntansi.views.penerimaanUmum.__formKodeRekening', array('model'=>$model), true)
                );
            }
            Yii::app()->end();
        }
    }
    
    public function actionGetDataRekeningByJnsPengeluaranGaji()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $jenispengeluaran_id = $_POST['jenispengeluaran_id'];
            $criteria = new CDbCriteria;
//            $criteria->select = '*, jnspengeluaranrek_m.saldonormal';
//            $criteria->join = '
//                JOIN jnspengeluaranrek_m ON
//                    jnspengeluaranrek_m.rekening1_id = t.struktur_id AND
//                    jnspengeluaranrek_m.rekening2_id = t.kelompok_id AND
//                    jnspengeluaranrek_m.rekening3_id = t.jenis_id AND
//                    jnspengeluaranrek_m.rekening4_id = t.obyek_id AND
//                    jnspengeluaranrek_m.rekening5_id = t.rincianobyek_id
//            ';
//            $criteria->condition = 'jnspengeluaranrek_m.jenispengeluaran_id = :jenispengeluaran_id';
//            $criteria->params = array(':jenispengeluaran_id'=>$jenispengeluaran_id);
//            $model = RekeningakuntansiV::model()->findAll($criteria);
            $criteria->order = 'jenispengeluaran_id,saldonormal';
             $model = RekeningpembayarangajiV::model()->findAll($criteria);
            if($model)
            {
                echo CJSON::encode(
//                    $this->renderPartial('__formKodeRekening', array('model'=>$model), true)
                    $this->renderPartial('akuntansi.views.penerimaanUmum.__formKodeRekening', array('model'=>$model), true)
                );
            }
            Yii::app()->end();
        }
    }
    /**
     * digunakan di :
     * rawatJalan/views/tindakan/rekening/_formRekening
     */
    public function actionGetDataRekeningPelayanan()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $daftartindakanId = $_POST['daftartindakan_id'];
            $kelaspelayananId = $_POST['kelaspelayanan_id'];
            $jenisPelayanan = $_POST['jnspelayanan'];
            $qty = ($_POST['qty'] > 0) ? $_POST['qty'] : 1;
            $criteria = new CDbCriteria;
            if(strtolower(trim($jenisPelayanan)) == 'tm'){
                $criteria->addCondition("daftartindakan_id = ".$daftartindakanId);
                $criteria->addCondition("kelaspelayanan_id = ".$kelaspelayananId);
            }
            
            $criteria->addCondition("LOWER(jnspelayanan) = '".strtolower(trim($jenisPelayanan))."'");
            $criteria->order='komponentarif_id';
            $models = PelayananrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                if(strtolower($jenisPelayanan) == 'tm'){
                    $models[$i]['daftartindakan_id'] = $daftartindakanId;
                    $models[$i]['obatalkes_id'] = "";
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($model['tariftindakan'] * $qty);
                    else
                        $models[$i]['saldokredit'] = ($model['tariftindakan'] * $qty);
                    
                }else{
                    $models[$i]['obatalkes_id'] = $daftartindakanId;
                    $models[$i]['daftartindakan_id'] = $model['daftartindakan_id'];
//                    $models[$i]['daftartindakan_id'] = "";
                    $harga = ObatalkesM::model()->findByPk($daftartindakanId)->hjaresep;
                    $hargaBebanObat = ObatalkesM::model()->findByPk($daftartindakanId)->harganetto;
                    if($model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA2 && strtolower($jenisPelayanan) != "tm"){
                        $hargaSaldo = ($hargaBebanObat * $qty);
                    }else{
                        $hargaSaldo = ($harga * $qty);
                    }
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = $hargaSaldo;
                    else
                        $models[$i]['saldokredit'] = $hargaSaldo;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    
    /**
     * untuk menampilkan rows_rekening biaya Administrasi, Konselilng, Service
     */
    public function actionGetDataRekeningBiayaFarmasi()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $daftartindakanId = $_POST['daftartindakan_id'];
            $kelaspelayananId = $_POST['kelaspelayanan_id'];
            $jenisPelayanan = $_POST['jnspelayanan'];
            $jenis_biaya = $_POST['jenis_biaya'];
            $qty = ($_POST['qty'] > 0) ? $_POST['qty'] : 1;
            
            $criteria = new CDbCriteria;
            
            if($jenis_biaya == 'konseling'){
                $criteria->addCondition("daftartindakan_id = ".PARAMS::DEFAULT_BIAYA_KONSELING_FARMASI."");
            }else if($jenis_biaya == 'administrasi'){
                $criteria->addCondition("daftartindakan_id = ".PARAMS::DEFAULT_BIAYA_ADMINISTRASI_FARMASI."");
            }else{
                $criteria->addCondition("daftartindakan_id = ".PARAMS::DEFAULT_BIAYA_SERVICE_FARMASI."");
            }
            $criteria->addCondition("kelaspelayanan_id = ".PARAMS::KELASPELAYANAN_ID_TANPA_KELAS);
            $criteria->addCondition("LOWER(jnspelayanan) = '".strtolower(trim($jenisPelayanan))."'");
            $criteria->order='komponentarif_id';
            $models = PelayananrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    $models[$i]['obatalkes_id'] = $daftartindakanId;
                    $models[$i]['daftartindakan_id'] = $model['daftartindakan_id'];
                    $models[$i]['jenis_biaya'] = $jenis_biaya;
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($model['tariftindakan'] * $qty);
                    else
                        $models[$i]['saldokredit'] = ($model['tariftindakan'] * $qty);
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('farmasiApotek.views.penjualanResep.rekening._rowRekeningBiaya', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * farmasiApotek/views/penjualanResep/rekening/_formRekening
     */
    public function actionGetDataRekeningObatFarmasi()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $daftartindakanId = $_POST['daftartindakan_id'];
            $kelaspelayananId = $_POST['kelaspelayanan_id'];
            $jenisPelayanan = $_POST['jnspelayanan'];
            $jenis_kelompok = $_POST['jenis_kelompok'];
            if(empty($jenis_kelompok)){
                $jenis_kelompok = 'OB';
            }
            $qty = ($_POST['qty'] > 0) ? $_POST['qty'] : 1;
            $criteria = new CDbCriteria;
            if(strtolower(trim($jenisPelayanan)) == 'tm'){
                $criteria->addCondition("daftartindakan_id = ".$daftartindakanId);
                $criteria->addCondition("kelaspelayanan_id = ".$kelaspelayananId);
            }
            
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_ADMINISTRASI_FARMASI);
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_KONSELING_FARMASI);
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_SERVICE_FARMASI);
            
            $criteria->addCondition("LOWER(jnspelayanan) = '".strtolower(trim($jnsPelayanan))."' OR LOWER(jnspelayanan) = '".strtolower(trim($jenis_kelompok))."'");
            $criteria->order='komponentarif_id';
            $models = PelayananrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                if(strtolower($jenisPelayanan) == 'tm'){
                    $models[$i]['daftartindakan_id'] = $daftartindakanId;
                    $models[$i]['obatalkes_id'] = "";
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($model['tariftindakan'] * $qty);
                    else
                        $models[$i]['saldokredit'] = ($model['tariftindakan'] * $qty);
                    
                }else{
                    $models[$i]['obatalkes_id'] = $daftartindakanId;
                    $models[$i]['daftartindakan_id'] = $model['daftartindakan_id'];
//                    $models[$i]['daftartindakan_id'] = "";
                    $harga = ObatalkesM::model()->findByPk($daftartindakanId)->hjaresep;
                    $hargaBebanObat = ObatalkesM::model()->findByPk($daftartindakanId)->harganetto;
                    if($model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA2 && strtolower($jenisPelayanan) != "tm"){
                        $hargaSaldo = ($hargaBebanObat * $qty);
                    }else{
                        $hargaSaldo = ($harga * $qty);
                    }
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = $hargaSaldo;
                    else
                        $models[$i]['saldokredit'] = $hargaSaldo;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('farmasiApotek.views.penjualanResep.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    public function actionGetDataRekeningObatFarmasiNew()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $daftartindakanId = $_POST['daftartindakan_id'];
            $kelaspelayananId = $_POST['kelaspelayanan_id'];
            $jenisPelayanan = $_POST['jnspelayanan'];
            $jenis_kelompok = $_POST['jenis_kelompok'];
            if(empty($jenis_kelompok)){
                $jenis_kelompok = 'OB';
            }
            $qty = ($_POST['qty'] > 0) ? $_POST['qty'] : 1;
            $criteria = new CDbCriteria;
            if(strtolower(trim($jenisPelayanan)) == 'tm'){
                $criteria->addCondition("daftartindakan_id = ".$daftartindakanId);
                $criteria->addCondition("kelaspelayanan_id = ".$kelaspelayananId);
            }
            
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_ADMINISTRASI_FARMASI);
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_KONSELING_FARMASI);
            $criteria->addCondition("daftartindakan_id != ".PARAMS::DEFAULT_BIAYA_SERVICE_FARMASI);
            
            $criteria->addCondition("LOWER(jnspelayanan) = '".strtolower(trim($jnsPelayanan))."' OR LOWER(jnspelayanan) = '".strtolower(trim($jenis_kelompok))."'");
            $criteria->order='komponentarif_id';
            $models = PelayananrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                if(strtolower($jenisPelayanan) == 'tm'){
                    $models[$i]['daftartindakan_id'] = $daftartindakanId;
                    $models[$i]['obatalkes_id'] = "";
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($model['tariftindakan'] * $qty);
                    else
                        $models[$i]['saldokredit'] = ($model['tariftindakan'] * $qty);
                    
                }else{
                    $models[$i]['obatalkes_id'] = $daftartindakanId;
                    $models[$i]['daftartindakan_id'] = $model['daftartindakan_id'];
//                    $models[$i]['daftartindakan_id'] = "";
                    $harga = ObatalkesM::model()->findByPk($daftartindakanId)->hjaresep;
                    $hargaBebanObat = ObatalkesM::model()->findByPk($daftartindakanId)->harganetto;
                    if($model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA2 && strtolower($jenisPelayanan) != "tm"){
                        $hargaSaldo = ($hargaBebanObat * $qty);
                    }else{
                        $hargaSaldo = ($harga * $qty);
                    }
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = $hargaSaldo;
                    else
                        $models[$i]['saldokredit'] = $hargaSaldo;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('farmasiApotek.views.informasiPenjualanResepNew.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
        public function actionAmbilDataRekening()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                $data = array();
                $params = array();
                foreach($_POST['id_rekening'] as $key=>$val)
                {
                    if($key != 'status')
                    {
                        if(strlen(trim($val)) > 0)
                        {
                            $data[] = $key . ' = :' . $key;
                            $params[(string) ':'.$key] = $val;
                        }
                    }
                }
                
                $criteria = new CDbCriteria;
                $criteria->select = '*';
                $criteria->condition = implode($data, ' AND ');
                $criteria->params = $params;
                $model = RekeningakuntansiV::model()->findAll($criteria);
                if($model)
                {
                    echo CJSON::encode(
                        $this->renderPartial('__formKodeRekening', array('model'=>$model, 'status'=>$_POST['id_rekening']['status']), true)
                    );
                }
                Yii::app()->end();
            }
        }
    public function actionListAntrianRuangan(){
        if(Yii::app()->request->isAjaxRequest) {
                $ruangan = $_POST['ruangan'];
                $form='';
            if(!empty($ruangan)){
                        $criteria=new CDbCriteria;
                        $criteria->compare('ruangan_id', $ruangan);
    //                    $criteria->compare('LOWER(hari)', strtolower($hariCari));

                        $modJadwalBukaPoli= JadwalbukapoliM::model()->findAll($criteria);
                        if (count($modJadwalBukaPoli) > 0){
                            foreach($modJadwalBukaPoli as $key=>$antrian){
                                $jadwal = $antrian->maxantiranpoli;
                            }
                             
                        }else{
                            $jadwal = 0;
                        }

                    $data['maxAntrianRuangan']=$jadwal;
                   echo json_encode($data);
             Yii::app()->end();
            }
        }
    }
    public function actionListAntrianDokter(){
        if(Yii::app()->request->isAjaxRequest) {
                $ruangan = $_POST['ruangan'];
                $pegawai = $_POST['pegawai'];
                $form='';
            if(!empty($ruangan)){
                        $criteria=new CDbCriteria;
                        $criteria->compare('ruangan_id', $ruangan);
                        $criteria->compare('pegawai_id', $pegawai);
    //                    $criteria->compare('LOWER(hari)', strtolower($hariCari));

                        $modJadwalDokter= JadwaldokterM::model()->findAll($criteria);
                        if (count($modJadwalDokter) > 0){
                            foreach($modJadwalDokter as $key=>$antrian){
                                $jadwal = $antrian->maximumantrian;
                            }
                             
                        }else{
                            $jadwal = 0;
                        }

                    $data['maxAntrianDokter']=$jadwal;
                   echo json_encode($data);
             Yii::app()->end();
            }
        }
    }
    
    public function actionubahLokasirak()
    {
        $model = new DokrekammedisM;
        if(isset($_POST['DokrekammedisM']))
        {
            $model->attributes = $_POST['DokrekammedisM'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $attributes = array('lokasirak_id'=>$_POST['DokrekammedisM']['lokasirak_id']);
                $save = DokrekammedisM::model()->updateByPk($_POST['DokrekammedisM']['dokrekammedis_id'], $attributes);
                if($save)
                {
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Lokasi Rak.</div>",
                        ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                        ));
                }
                exit;
            }catch(Exception $exc) {
                $transaction->rollback();
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_ubahLokasiRak', array('model'=>$model), true)));
            exit;
        }
    }

    public function actionPasienDokumen()
    {
        if (Yii::app()->request->isAjaxRequest){
            $idDokRM = $_POST['idpasien'];
            $model = DokrekammedisM::model()->findByPk($idDokRM);

            $modPasien = PasienM::model()->findByPk($model->pasien_id);
            $modLokasirak = LokasirakM::model()->findByPk($model->lokasirak_id);
            $modSubrak = SubrakM::model()->findByPk($model->subrak_id);
            $attributes = $model->attributeNames();
            foreach($attributes as $j=>$attribute) {
                $returnVal["$attribute"] = $model->$attribute;
            }
            
            $returnVal["nama_pasien"] = $modPasien->nama_pasien;
            $returnVal["lokasirak_nama"] = $modLokasirak->lokasirak_nama;
            $returnVal["subrak_nama"] = $modSubrak->subrak_nama;
            echo json_encode($returnVal);
            Yii::app()->end();
        }
    }


    public function actionubahSubrak()
    {
        $model = new DokrekammedisM;
        if(isset($_POST['DokrekammedisM']))
        {
            $model->attributes = $_POST['DokrekammedisM'];
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $attributes = array('subrak_id'=>$_POST['DokrekammedisM']['subrak_id']);
                $save = DokrekammedisM::model()->updateByPk($_POST['DokrekammedisM']['dokrekammedis_id'], $attributes);
                if($save)
                {
                    $transaction->commit();
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-success'>Berhasil merubah data Sub Rak.</div>",
                        ));
                }else{
                    echo CJSON::encode(array(
                        'status'=>'proses_form',
                        'div'=>"<div class='flash-error'>Data gagal disimpan.</div>",
                        ));
                }
                exit;
            }catch(Exception $exc) {
                $transaction->rollback();
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'status'=>'create_form',
                'div'=>$this->renderPartial('_ubahSubRak', array('model'=>$model), true)));
            exit;
        }
    }
    
    /*
     * Ubah Status Kamar
     */
         public function actionUbahStatusKamar()
         {
            $idKamar = $_POST['idKamar'];
            $model = KamarruanganM::model()->findByPk($_POST['idKamar']);
            $modUbah = new KamarruanganM();
            $modUbah->keterangan_kamar = $model->keterangan_kamar;
            if(isset($_POST['KamarruanganM']))
            {
               $update = KamarruanganM::model()->updateByPk($idKamar,array('keterangan_kamar'=>$_POST['KamarruanganM']['keterangan_kamar']));
               if($update)
                {
                   $modUbah->isNewRecord = false;
                    if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Data Pasien <b></b> berhasil disimpan </div>",
                            ));
                        exit;
                    }

                }
                else
                {

                    if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data Pasien <b></b> gagal disimpan </div>",
                            ));
                        exit;
                    }
                }
            }
            if (Yii::app()->request->isAjaxRequest)
            {
                if($modUbah->isNewRecord == true){
                    echo CJSON::encode(array(
                        'status'=>'create_form',
                        'keterangan'=>$model->keterangan_kamar,
                        'div'=>$this->renderPartial('_ubahStatusKamar', array('model'=>$model,'modUbah'=>$modUbah),true)));
                    exit;
                }
            }
        }
        function actionUbahStatusKetKamar(){
            $idKamar = $_POST['idKamar'];
            $model = KamarruanganM::model()->findByPk($_POST['idKamar']);
            $modUbah = new KamarruanganM();
            if(isset($_POST['KamarruanganM']))
            {
               $update = KamarruanganM::model()->updateByPk($idKamar,array('keterangan_kamar'=>$_POST['KamarruanganM']['keterangan_kamar']));
               if($update)
                {
                   $modUbah->isNewRecord = false;
                    if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-success'>Data Pasien <b></b> berhasil disimpan </div>",
                            ));
                        exit;
                    }

                }
                else
                {

                    if (Yii::app()->request->isAjaxRequest)
                    {
                        echo CJSON::encode(array(
                            'status'=>'proses_form',
                            'div'=>"<div class='flash-error'>Data Pasien <b></b> gagal disimpan </div>",
                            ));
                        exit;
                    }
                }
            }
        }
    /*
     * end Ubah Status Kamar
     */

    public function actionGetStatusBooking()
    {
          if(Yii::app()->request->isAjaxRequest) {
         
              $idPasien = $_POST['idPasien'];
              $model = BookingkamarT::model()->findAllByAttributes(array('pasien_id'=>$idPasien));
              
              if(count($model) > 0){
                  echo CJSON::encode(array(
                    'status'=>'ada',
                    'jumlah'=>count($model),
                    'div'=>$this->renderPartial('_addRuanganAdmisi', array('model'=>$model),true),
                    ));
                exit;
              }else{
                  $data['status']='tidak';
              }
              
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    /**
     * untuk transaksi farmasiApotek/ProduksiObat
     */
    public function actionAddRowBahanProduksi(){
        if(Yii::app()->request->isAjaxRequest) {
            $idObat = $_POST['idObat'];
            
            echo CJSON::encode(
                $this->renderPartial('farmasiApotek.views.produksiObat._rowDetailProduksi', array('model'=>$model, 'status'=>$_POST['id_rekening']['status']), true)
            );
        }
    }


    public function actionGetJenisKode(){
        if(Yii::app()->request->isAjaxRequest) {
            $jeniskode_id = $_POST['jeniskode_id'];
            
                $modJenis = JeniskodeM::model()->findByPk($jeniskode_id);
                $modObat = ObatalkesM::model()->findAllByAttributes(array('jeniskode_id'=>$jeniskode_id));
            
              $jmlobat = count($modObat);
              $kata = $modJenis->singkatan;
              $kode = $kata."-".($jmlobat + 1);
            // echo $modJenis->singkatan; exit;
            $data['jeniskode']=$kode;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * billingKasir/views/pembayaran/rekening/_formRekening
     */
    public function actionGetDataRekeningKasir()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            parse_str($_POST['datapasien'],$datapasien);
            parse_str($_POST['datapembayaran'],$datapembayaran);
            $tandabuktibayar = $datapembayaran['TandabuktibayarT'];
            $pasien = array_merge((array)$datapasien['BKPendaftaranT'],(array)$datapasien['BKPasienM']);
            $models_penjamin = array();
            $models_carabayar = array();
            $models_piutang = array();
            
            $totalsubsidiasuransi = ($_POST['totalseluruhsubsidiasuransi'] > 0) ? $_POST['totalseluruhsubsidiasuransi'] : 0;
            $totalsubsidirs = ($_POST['totalseluruhsubsidirs'] > 0) ? $_POST['totalseluruhsubsidirs'] : 0;
            if(!empty($pasien['penjamin_id'])){
                $criteria1 = new CDbCriteria;
                $criteria1->addCondition("penjamin_id = ".$pasien['penjamin_id']);
                $criteria1->order='penjamin_id,penjamin_nama,saldonormal';
                $models_penjamin = RekeningpembayarankasirV::model()->findAll($criteria1);
            }
            if($pasien['penjamin_id'] == Params::DEFAULT_CARABAYAR && $tandabuktibayar['carapembayaran'] == Params::CARAPEMBAYARAN_TUNAI){ //jika carabayar umum
                $models_carabayar = array();
            }else{
                $criteria2 = new CDbCriteria;
                $criteria2->compare("LOWER(penjamin_nama)",strtolower($tandabuktibayar['carapembayaran']),true);
                $criteria1->order='penjamin_id,penjamin_nama,saldonormal';
                $models_carabayar = RekeningpembayarankasirV::model()->findAll($criteria2);
            }
            
            if($datapembayaran['pakeKartu'] == 1){
                $criteria3 = new CDbCriteria;
                $criteria3->addCondition("jenispenerimaan_id = ".PARAMS::JENISPENERIMAAN_ID_PIUTANG_MERCHANT);
                $criteria3->order='jenispenerimaan_id,jenispenerimaan_nama,saldonormal';
                $models_piutang = JenispenerimaanrekeningV::model()->findAll($criteria3);
            }
            $models = array_merge((array)$models_penjamin,(array)$models_carabayar,(array)$models_piutang);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                $saldo = 0;
                
                if(empty($model['penjamin_id'])){
                    $saldo = $tandabuktibayar['uangditerima'];
                }else{
                    if($model['penjamin_id'] == Params::DEFAULT_CARABAYAR){
                        $saldo = $tandabuktibayar['uangditerima'];
                    }else{
                        $saldo = $totalsubsidiasuransi + $totalsubsidirs;
                    }
                }
                
                if(!empty($model['jenispenerimaan_id'])){
                    $saldo = $tandabuktibayar['jmlbayardgnkartu'];
                }
                
                if($model['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $saldo;
                }else{
                    $models[$i]['saldokredit'] = $saldo;
                }
                    
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembayaran.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * billingKasir/views/pembayaranUangMuka/rekening/_formRekening
     */
    public function actionGetDataRekeningUangMuka()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            parse_str($_POST['datapembayaran'],$datapembayaran);
            $tandabuktibayar = $datapembayaran['BKTandabuktibayarUangMukaT'];
            $carapembayaran = (isset($_POST['carapembayaran'])&&!empty($_POST['carapembayaran']))? $_POST['carapembayaran'] : "TUNAI";
            $jmlpembayaran = (isset($_POST['jmlpembayaran'])&&!empty($_POST['jmlpembayaran']))? $_POST['jmlpembayaran'] : 0;
            // $models = JenispenerimaanrekeningV::model()->findAll();
            $criteria = new CDbCriteria;
            if($carapembayaran=="TUNAI"){
                $criteria->addCondition("jenispenerimaan_id = 4");
            }else{
                $criteria->addCondition("jenispenerimaan_id = 16");
            }
            $models = RekeningpembayaranuangmukaV::model()->findAll($criteria);
            foreach($models AS $i => $model){
                $saldodebit = 0;
                $saldokredit = 0;
                if($model['saldonormal'] == "D"){
//                    $models[$i]['saldodebit'] = $saldodebit;
                    $models[$i]['saldodebit'] = $jmlpembayaran;
                }else{
//                    $models[$i]['saldokredit'] = $saldokredit;
                    $models[$i]['saldokredit'] = $jmlpembayaran;
                }
                    
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembayaranUangMuka.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * billingKasir/views/pembatalanUangMukaNew/rekening/_formRekening
     */
    public function actionGetDataRekeningBatalUangMuka()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $models = JenispengeluaranrekeningV::model()->findAllByAttributes(array('jenispengeluaran_id'=>48));
            foreach($models AS $i => $model){
                $saldodebit = 0;
                $saldokredit = 0;
                if($model['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $saldodebit;
                }else{
                    $models[$i]['saldokredit'] = $saldokredit;
                }
                    
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembatalanUangMukaNew.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * billingKasir/views/pembayaranKlaimPiutang/rekening/_formRekening
     */
    public function actionGetDataRekeningPembayaranKlaim()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $tandabuktibayar = $datapembayaran['BKTandabuktibayarUangMukaT'];
            $jenispenerimaan_id = PARAMS::JENISPENERIMAAN_ID_PELUNASAN_PIUTANG;
            $criteria = new CDbCriteria;
            $criteria->addCondition("jenispenerimaan_id = ".$jenispenerimaan_id);
            $models = JenispenerimaanrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){
                $saldodebit = 0;
                $saldokredit = 0;
                if($model['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $saldodebit;
                }else{
                    $models[$i]['saldokredit'] = $saldokredit;
                }
                    
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembayaranKlaimPiutang.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * akuntansi/views/pembayaranKlaimMerchant/rekening/_formRekening
     */
    public function actionGetDataRekeningPembayaranKlaimMerchant()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $tandabuktibayar = $datapembayaran['BKTandabuktibayarUangMukaT'];
            $jenispenerimaan_id = PARAMS::JENISPENERIMAAN_ID_KLAIM_BANK;
            $criteria = new CDbCriteria;
            $criteria->addCondition("jenispenerimaan_id = ".$jenispenerimaan_id);
            $models = JenispenerimaanrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){
                $saldodebit = 0;
                $saldokredit = 0;
                if($model['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $saldodebit;
                }else{
                    $models[$i]['saldokredit'] = $saldokredit;
                }
                    
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('akuntansi.views.pembayaranKlaimMerchant.rekening._rowRekening', array('modRekenings'=>$models), true)                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
    /**
     * digunakan di :
     * gudangFarmasi/views/fakturPembelian/rekening/_formRekening
     */
    public function actionGetDataRekeningFaktur()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $supplier_id = $_POST['supplier_id'];
            $total = $_POST['total'];
            $criteria = new CDbCriteria;
            $criteria->addCondition("supplier_id = ".$supplier_id);
            $models = RekeningsupplierV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    $models[$i]['supplier_id'] = $supplier_id;
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($total);
                    else
                        $models[$i]['saldokredit'] = ($total);
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('gudangFarmasi.views.fakturPembelian.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    /**
     * digunakan di : tetapi menunya ada di akuntansi/fakturpembelianT/index
     * gudangFarmasi/views/fakturPembelian/rekening/_formRekening
     */
    
    /**
     * digunakan di :
     * billingKasir/views/pembayaranSupplier/rekening/_formRekening
     */
    public function actionGetDataRekeningBayarKeSupplier()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $supplier_id = $_POST['supplier_id'];
            $total = $_POST['total'];
            $criteria = new CDbCriteria;
            $criteria->order='saldonormal';
            $models = RekeningpembayaransupplierV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($total);
                    else
                        $models[$i]['saldokredit'] = ($total);
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembayaranSupplier.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    /**
     * digunakan di : tetapi menunya ada di billingKasir/pembayaranSupplier/index
     * billingKasir/views/pembayaranSupplier/rekening/_formRekening
     */
    public function actionGetDataRekeningBayarObatLuar()
    {
      if(Yii::app()->getRequest()->getIsAjaxRequest())
      {
        $carapembayaran = $_POST['carapembayaran'];
        $nilai = $_POST['nilai'];
        $criteria = new CDbCriteria;
        $criteria->compare('carapembayaran', $carapembayaran);
        $criteria->order='saldonormal';
        $models = RekeningpembayaranobatluarV::model()->findAll($criteria);
        foreach($models AS $i => $model){
          $saldodebit = $nilai;
          $saldokredit = $nilai;
          if($model['saldonormal'] == "D"){
            $models[$i]['saldokredit'] = 0;
            $models[$i]['saldodebit'] = $saldodebit;
          }else{
            $models[$i]['saldokredit'] = $saldokredit;
            $models[$i]['saldodebit'] = 0;
          }
        }
        if(count($models)>0){
          echo CJSON::encode(
            $this->renderPartial('farmasiApotek.views.pembayaranLangsung.rekening._rowRekening', array('modRekenings'=>$models), true)
          );
        }else{
          echo CJSON::encode();
        }
        Yii::app()->end();
      }
    }


    /**
     * digunakan di : billingKasir/returTagihanPasien/index
     * @Author      : Hardi
     * Issue        : EHJ-1922
     */
    public function actionGetDataRekeningReturTagihan()
    {
      if(Yii::app()->getRequest()->getIsAjaxRequest())
      {
        $model = RekeningreturpelayananV::model()->findAll();
        if($model)
        {
          echo CJSON::encode(
            $this->renderPartial('billingKasir.views.returTagihanPasien.rekening.__formKodeRekening', array('model'=>$model), true)
          );
        }
        Yii::app()->end();
      }
    }

    /**
     * digunakan di : akuntansi/daftarFakturPembelianT/retur
     * @Author      : Hardi
     * Issue        : EHJ-1923
     */
    public function actionGetDataRekeningReturPembelian()
    {
      if(Yii::app()->getRequest()->getIsAjaxRequest())
      {
        // $model = RekeningreturpembelianV::model()->findAll();
        $model = JnspengeluaranrekM::model()->findAllByAttributes(array('jenispengeluaran_id'=> Params::REKJNSPENGELUARAN_RETURPEMBELIAN));
        if($model)
        {
          echo CJSON::encode(
            $this->renderPartial('billingKasir.views.daftarFakturPembelian.rekening.__formKodeRekening', array('model'=>$model), true)
          );
        }
        Yii::app()->end();
      }
    }

    /**
     * digunakan di : gudangUmum/returpenerimaanT/_form
     * @Author      : Hardi
     * Issue        : EHJ-1924
     */
    public function actionGetDataRekeningReturPersediaanBarang()
    {
      if(Yii::app()->getRequest()->getIsAjaxRequest())
      {
        $model = RekeningreturassetV::model()->findAll();
        if($model)
        {
          echo CJSON::encode(
            $this->renderPartial('gudangUmum.views.returpenerimaanT.rekening.__formKodeRekening', array('model'=>$model), true)
          );
        }
        Yii::app()->end();
      }
    }

    /**
     * digunakan di : akuntansi
     * @Author      : jembar
     * Issue        : EHJ-1938
     */
     public function actionGetPenerimaanPersediaan()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $isPPN=0;
            $isPPH=0;
            $idTerimaPers=$_POST['idTerimaPers'];
            $terimaPersediaan = TerimapersediaanT::model()->findByPk($idTerimaPers);
            $modTerimaPersDetail = TerimapersdetailT::model()->with('barang')->findAll('terimapersediaan_id='.$idTerimaPers.'');

            $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$penerimaanBarang->permintaanpembelian_id));
            if(count($modUangMuka) > 0){
                foreach($modUangMuka as $u=>$uang){
                    $jmlUang += $uang->jumlahuang;
                }
            }else{
                $jmlUang = 0;
            }
//            echo $jmlUang;exit;
            $subTotal = 0;
            $tr = "";
            foreach ($modTerimaPersDetail AS $i=>$tampilDetail):
                $modTerimaPersDetail = new TerimapersdetailT;
                $subTotal = ($tampilDetail['hargabeli']) * ($tampilDetail['jmlterima']);
                $modTerimaPersDetail->satuanbeli = $tampilDetail['satuanbeli'];

                $tr .="<tr>
                        <td>".CHtml::TextField('noUrut',$i+1,array('class'=>'span1 noUrut','readonly'=>TRUE)).
                              CHtml::activeHiddenField($modTerimaPersDetail,'['.$i.']barang_id',array('value'=>$tampilDetail['barang_id'])).
                              CHtml::activeHiddenField($modTerimaPersDetail,'['.$i.']terimapersdetail_id',array('value'=>$tampilDetail['terimapersdetail_id'])).
                              CHtml::activeHiddenField($modTerimaPersDetail,'['.$i.']inventarisasi_id',array('value'=>$tampilDetail['inventarisasi_id'])).
                       "</td>

                        <td>".$tampilDetail->barang['bidang']['subkelompok']['kelompok']['golongan']['golongan_nama']."</td>
                        <td>".$tampilDetail->barang['bidang']['subkelompok']['kelompok']['kelompok_nama']."</td>
                        <td>".$tampilDetail->barang['bidang']['subkelompok']['subkelompok_nama']."</td>
                        <td>".$tampilDetail->barang['bidang']['bidang_nama']."</td>
                        <td>".$tampilDetail->barang['barang_kode']." /<br/>".$tampilDetail->barang['barang_nama']."</td>
                        <td>".CHtml::activeTextField($modTerimaPersDetail, '['.$i.']jmlterima', array('value'=>$tampilDetail['jmlterima'],'class'=>'span1 numbersOnly qty','readonly'=>TRUE))."</td>
                        <td>".CHtml::activeDropDownList($modTerimaPersDetail, '['.$i.']satuanbeli', Satuanbarang::items(), array('empty'=>'-- Pilih --', 'class'=>'span2','readonly'=>TRUE))."</td>
                        <td>".CHtml::activeTextField($modTerimaPersDetail, '['.$i.']jmldalamkemasan', array('value'=>$tampilDetail['jmldalamkemasan'],'class'=>'span1 numbersOnly jml','readonly'=>TRUE))."</td>
                        <td>".CHtml::activeTextField($modTerimaPersDetail,'['.$i.']hargabeli',array('style'=>'text-align:right;','value'=>$tampilDetail['hargabeli'],'readonly'=>FALSE,'class'=>'span2 numbersOnly hargaBeli','onkeyup'=>'hitungSemua();'))."</td>
                        <td>".CHtml::activeTextField($modTerimaPersDetail,'['.$i.']hargasatuan',array('style'=>'text-align:right;','value'=>$tampilDetail['hargasatuan'],'readonly'=>TRUE,'class'=>'span2 numbersOnly hargasatuan'))."</td>
                        <td>".CHtml::textField('subTotal',$subTotal,array('style'=>'text-align:right;','readonly'=>true,'class'=>'span2 numbersOnly subTotal'))."</td>
                       
                        <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;'))."</td>
                    </tr>
                    ";
             $idTerimaPers=$tampilDetail['terimapersediaan_id'];
            endforeach;
            $modTerimaPersediaan=TerimapersediaanT::model()->findByPk($idTerimaPers);
            $data['tr']=$tr;
            $data['terimapersediaan_id']=$modTerimaPersediaan->terimapersediaan_id;
            $data['isPPN']=$isPPN;
            $data['isPPH']=$isPPH;
            $data['uangMuka']=$jmlUang;
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    /**
     * digunakan di : akuntansi/FakturPembelianUmum/index
     * @Author      : Hardi
     * Issue        : EHJ-1939
     */
    public function actionGetDataRekeningPembayaranSupplierUmum(){
      if(Yii::app()->getRequest()->getIsAjaxRequest()){
            $supplier_id = $_POST['supplier_id'];
            $total = $_POST['total'];
            $transaksi = $_POST['transaksi'];
            $criteria = new CDbCriteria;
//             $criteria->addCondition("supplier_id = ".$supplier_id);
//            if($transaksi=="informasi"){
//              $criteria->addCondition("jenispengeluaran_id = 26");
//            }else{
//              $criteria->addCondition("jenispengeluaran_id = 3");
//            }
            $criteria->order='saldonormal';
//            $models = SupplierrekM::model()->findAll($criteria);
//            $models = RekeningsupplierV::model()->findAll($criteria);
            $models = RekeningpembayaransupplierV::model()->findAll($criteria); //EHJ-3450
            
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($total);
                    else
                        $models[$i]['saldokredit'] = ($total);
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.pembayaranSupplier.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }
    
     /**
     * digunakan di :
     * akuntansi/views/fakturPembelianGU/rekening/_formRekening
     */
    public function actionGetDataRekeningPembelianGU()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $supplier_id = $_POST['supplier_id'];
            $total = $_POST['total'];
            $criteria = new CDbCriteria;
            $criteria->addCondition("supplier_id = ".$supplier_id);
            $models = RekeningsupplierV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    $models[$i]['supplier_id'] = $supplier_id;
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($total);
                    else
                        $models[$i]['saldokredit'] = ($total);
            }
            if(count($models)>0){
                echo CJSON::encode(
//                    $this->renderPartial('gudangFarmasi.views.fakturPembelian.rekening._rowRekening', array('modRekenings'=>$models), true)
                     $this->renderPartial('akuntansi.views.fakturPembelianGU.rekening._rowRekening', array('modRekenings'=>$models), true)   
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }

    /**
     * digunakan di : RawatInap Asuhan Keperawatan
     * @Author      : Hardi
     * Issue        : EHJ-2126
     */
    public function actionloadAnamnesa(){
      if(Yii::app()->getRequest()->getIsAjaxRequest()){
        $pendaftaran_id = $_POST['pendaftaran_id'];
        $modAnamnesa=AnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
        if(count($modAnamnesa)>0){
          $data['keluhanutama']     = $modAnamnesa->keluhanutama;
          $data['keluhantambahan']  = $modAnamnesa->keluhantambahan;
          $data['riwayatpenyakitterdahulu']   = $modAnamnesa->riwayatpenyakitterdahulu;
          $data['riwayatpenyakitkeluarga']    = $modAnamnesa->riwayatpenyakitkeluarga;
        }

        $modPemeriksaan = PemeriksaanfisikT::model()->findByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
        if(count($modPemeriksaan)>0){
          $data['tekanandarah']     = $modPemeriksaan->tekanandarah;
          $data['detaknadi']        = $modPemeriksaan->detaknadi;
          $data['suhutubuh']        = $modPemeriksaan->suhutubuh;
          $data['beratbadan']       = $modPemeriksaan->beratbadan_kg;
          $data['tinggibadan']      = $modPemeriksaan->tinggibadan_cm;
          $data['pernapasan']       = $modPemeriksaan->pernapasan;
          $data['paramedis']        = $modPemeriksaan->paramedis_nama;
          $data['kelainantubuh']    = $modPemeriksaan->kelainanpadabagtubuh;
        }

        echo json_encode($data);

        Yii::app()->end();
      }
    }
    
    /**
     * actionGetTriasePasien untuk Triase Tabulasi Anamnesa:
     * @autor   :Miranitha Fasha
     * issue    : EHJ-2314
     */
    public function actionGetTriasePasien()
    {
        
        if(Yii::app()->request->isAjaxRequest) {
            $idTriase=$_POST['idTriase'];
            
            $modDetail = new Triase;
            $modTriase=Triase::model()->findByPk($idTriase);
            $warna = Triase::model()->getKodeWarnaId($idTriase);
                  $tr ="<tr>
                            <td> ".CHtml::hiddenField('noUrut','',array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($modDetail,'['.$idTriase.']triase_id',array('value'=>$modTriase->triase_id, 'class'=>'idTriase'))
                                  ."<div class='colorPicker-picker' style='background-color:#".$warna.";'> </div>".
                           "</td>
                            <td>".$modTriase->triase_nama."</td>
                            <td>".$modTriase->keterangan_triase."</td>
                            <td>".CHtml::link("<span class='icon-remove'>&nbsp;</span>",'',
                                    array('href'=>'#','onClick'=>'batal();return false;','style'=>'text-decoration:none;'))."</td>
                         </tr>
                        ";
           $data['tr']=$tr;
           echo json_encode($data);
         Yii::app()->end();
        }
    }

    
    /**
     * digunakan di :
     * bedahSentral/views/daftarPasien/rekening/_formRekening
     * @Author  : Miranitha Fasha
     * Issue    :  EHJ-2362
     */
    public function actionGetDataRekeningPelayananBedahSentral()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $daftartindakanId = $_POST['daftartindakan_id'];
            $kelaspelayananId = $_POST['kelaspelayanan_id'];
            $jenisPelayanan = $_POST['jnspelayanan'];
            $qty = ($_POST['qty'] > 0) ? $_POST['qty'] : 1;
            $criteria = new CDbCriteria;
            if(strtolower(trim($jenisPelayanan)) == 'tm'){
                $criteria->addCondition("daftartindakan_id = ".$daftartindakanId);
                $criteria->addCondition("kelaspelayanan_id = ".$kelaspelayananId);
            }
            
            $criteria->addCondition("LOWER(jnspelayanan) = '".strtolower(trim($jenisPelayanan))."'");
//            $criteria->order='daftartindakan_id,komponentarif_id,saldonormal asc';
            $models = PelayananrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                if(strtolower($jenisPelayanan) == 'tm'){
                    $models[$i]['daftartindakan_id'] = $daftartindakanId;
                    $models[$i]['obatalkes_id'] = "";
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($model['tariftindakan'] * $qty);
                    else
                        $models[$i]['saldokredit'] = ($model['tariftindakan'] * $qty);
                    
                }else{
                    $models[$i]['obatalkes_id'] = $daftartindakanId;
                    $models[$i]['daftartindakan_id'] = $model['daftartindakan_id'];
//                    $models[$i]['daftartindakan_id'] = "";
                    $harga = ObatalkesM::model()->findByPk($daftartindakanId)->hjaresep;
                    $hargaBebanObat = ObatalkesM::model()->findByPk($daftartindakanId)->harganetto;
                    if($model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA || $model->daftartindakan_id == PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA2 && strtolower($jenisPelayanan) != "tm"){
                        $hargaSaldo = ($hargaBebanObat * $qty);
                    }else{
                        $hargaSaldo = ($harga * $qty);
                    }
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = $hargaSaldo;
                    else
                        $models[$i]['saldokredit'] = $hargaSaldo;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('bedahSentral.views.daftarPasien.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }

    /**
     * digunakan di : akuntansi/ClosingKasir/index
     * @Author      : Hardi
     * Issue        : EHJ-2360
     */
    public function actionGetDataClosingKasir(){
      if(Yii::app()->getRequest()->getIsAjaxRequest()){
            $total = $_POST['total'];
            $jenispenerimaan_id = $_POST['jenispenerimaan_id'];
            $criteria = new CDbCriteria;
            $criteria->compare('jenispenerimaan_id', $jenispenerimaan_id);
            $criteria->order='saldonormal';
            $models = JenispenerimaanrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    if($model['saldonormal'] == "D")
                        $models[$i]['saldodebit'] = ($total);
                    else
                        $models[$i]['saldokredit'] = ($total);
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('billingKasir.views.closingKasir.rekening._rowRekening', array('modRekenings'=>$models), true)
                );
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }
    }

    public function actionCekPasien()
    {
      if (Yii::app()->request->isAjaxRequest){
        $pasien_id = $_POST['pasien_id'];
        $returnVal = array();
        $criteria = new CDbCriteria;
        $criteria->compare('pasien_id', $pasien_id);
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.date('Y-m-d').' 00:00:00\' AND \''.date('Y-m-d 23:59:59').'\'');
        $model = PendaftaranT::model()->find($criteria);
        if(isset($model)){
            $returnVal["no_rekam_medik"]  = $model->pasien->no_rekam_medik;
            $returnVal["nama_pasien"]     = $model->pasien->nama_pasien;
            $returnVal["no_pendaftaran"]  = $model->no_pendaftaran;
            $returnVal["statusperiksa"]   = $model->statusperiksa;
            $returnVal["tgl_pendaftaran"] = $model->tgl_pendaftaran;
            $returnVal["ruangan_nama"]    = $model->ruangan->ruangan_namalainnya;
            $returnVal["jumlah"] = count($model);
        }
        echo json_encode($returnVal);
        Yii::app()->end();
      }
    }

    public function actionAmbilPasienId()
    {
      if (Yii::app()->request->isAjaxRequest){
        $no_rekam_medik = $_POST['no_rekam_medik'];

        $model = PasienM::model()->findByAttributes(array('no_rekam_medik'=>$no_rekam_medik));
        $returnVal["pasien_id"]  = $model->pasien_id;

        echo json_encode($returnVal);
        Yii::app()->end();
      }
    }
	
	public function actionAmbilDataKepegawaian($id_pasien = null)
	{
		if(Yii::app()->request->isAjaxRequest)
		{
			$returnVal = array();
			$model = PegawaiM::model()->findByPK($id_pasien);
			foreach($model as $i=>$val)
			{
				$returnVal["$i"] = (strlen($val) > 0 ? $val : "-");
			}
			
			$returnVal["golonganpegawai_id"] = $model->golonganpegawai->golonganpegawai_nama;
			$returnVal["jabatan_id"] = $model->jabatan->jabatan_nama;
			$returnVal["pangkat_id"] = $model->pangkat->pangkat_nama;
			$returnVal['nama_pegawai'] = $model->nama_pegawai;
			$returnVal['jml_tanggungan'] = $model->Jumlahtanggungan;
			
			/** biaya pph 21 tahunan **/
			/*
			$biayajabatan = 0;
			$iuranpensiun = 0;
			if($model->Gapok > 0)
			{
				$biayajabatan = ($model->Gapok*0.05) * 12;
				if($biayajabatan > 6000000)
				{
					$biayajabatan = 6000000;
				}
				
				$iuranpensiun = ($model->Gapok*0.05) * 12;
				if($iuranpensiun > 2400000)
				{
					$iuranpensiun = 2400000;
				}
			}
			$returnVal['biayajabatan'] = $biayajabatan;
			$returnVal['potonganpensiun'] = $iuranpensiun;
			$returnVal['gajipertahun'] = $model->Gapok * 12;
			*/
			
			$get_ptkp = PtkpM::model()->findbyAttributes(array(
				'jmltanggunan'=>$model->Jumlahtanggungan,
				'statusperkawinan'=>$model->statusperkawinan
			));
			$returnVal['ptkppertahun'] = $get_ptkp->wajibpajak_thn;
			$returnVal['kodeptkp'] = $get_ptkp->kodeptkp;
			
			/** biaya pph 21 tahunan **/
			
			
			
			
			/*
			$returnVal['gajipph'] = $model->Ptkp;
			*/
		    $returnVal['LamaKerjaTanpaMasaKerja'] = $model->LamaKerjaTanpaMasaKerja;
			$returnVal['tunjangan'] = array();
			
			$criteria = new CDbCriteria;
			$criteria->compare('jabatan_id',$model->jabatan_id);
			$tunjangan = TunjanganM::model()->findAll($criteria);
			$rec = array();
			
			$modKomponen = KomponengajiM::model()->findAll('komponengaji_aktif = true ORDER BY nourutgaji');
			foreach($modKomponen as $x=>$z)
			{
				$id_elm = 'PenggajiankompT_'. $z->komponengaji_id .'_jumlah';
				$rec[$id_elm] = 0;
			}
			
			foreach($tunjangan as $x=>$z)
			{
				$id_elm = 'PenggajiankompT_'. $z->komponengaji_id .'_jumlah';
				$rec[$id_elm] = $z->nominaltunjangan;
				if ($model->kategoripegawai == Params::KATEGORI_PEGAWAI_KHUSUS){
					$rec[$id_elm] = 0;
				}
			}
			
			$returnVal['tunjanganfungsional'] = $model->Tunjanganfungsional;
			$returnVal['tunjanganjabatan'] = $model->Tunjanganjabatan;
			$returnVal['gapok'] = $model->Gapok;
			
			if ($model->kategoripegawai == Params::KATEGORI_PEGAWAI_KHUSUS){
				$returnVal['tunjanganfungsional'] = 0;
				$returnVal['tunjanganjabatan'] = 0;
				$returnVal['gapok'] = 0;
			}
			
			$penggajianPeriodeSebelumnya = PenggajianpegT::model()->findByAttributes(array('pegawai_id'=>$model->pegawai_id), array('order' => 'periodegaji desc'));
			if ($penggajianPeriodeSebelumnya){
				$komponenPenggajianSebelumnya = PenggajiankompT::model()->findAllByAttributes(array('penggajianpeg_id' => $penggajianPeriodeSebelumnya->penggajianpeg_id));
				foreach ($komponenPenggajianSebelumnya as $value){
					$id_elm = 'PenggajiankompT_'. $value->komponengaji_id .'_jumlah';
					$rec[$id_elm] = $value->jumlah;
				}
			}
			$returnVal['tunjangan'] = $rec;
			
			echo CJSON::encode($returnVal);
		}
		Yii::app()->end();
	}
    
    public function actionDetailBarang()
    {
        if(Yii::app()->request->isAjaxRequest){
            $result = array(
                'status'=>'NOT',
                'record'=>array(),
                'pesan'=>''
            );
            try{
                $map = Yii::app()->request->getParam('map');
                $model = BarangM::model()->findByPk($map);
                if($model){
                    $record = array();
                    foreach($model->attributes as $key => $value){
                        if($key == 'barang_satuan'){
                            $key = 'satuanbarang';
                            $value = strtoupper($value);
                        }
                        $record[$key] = $value;
                    }
                }
                $result['record'] = $record;
                $result['status'] = 'OK';
            }catch(Exception $e){
                $result['pesan'] = $e->getMessage();
            }
            echo json_encode($result);
            Yii::app()->end();
        }
    }    
	
}
?>

