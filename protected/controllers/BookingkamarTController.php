<?php
Yii::import('pendaftaranPenjadwalan.models.*');
class BookingkamarTController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $defaultAction = 'create';
    public $successSave = false;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
//                if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model=new PPBookingKamarT;
                $model->tglbookingkamar = date('d M Y H:i:s');
                $model->bookingkamar_no = Generator::noBookingKamar();
                $modPasien = new PPPasienM;
                $mobile = new MobileDetect();
                $modPasien->tanggal_lahir = date('d M Y');
                

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PPBookingKamarT'],$_POST['PPPasienM'] ))
		{
                    $model->attributes= $_POST['PPBookingKamarT'];
                    $modPasien->attributes=$_POST['PPPasienM'];
//                    echo print_r($_POST['PPPasienM']);
//                    exit();
                    $model->umur = $_POST['PPBookingKamarT']['umur'];
                    $model->tgltransaksibooking=date('Y-m-d H:i:s');
                    $model->create_loginpemakai_id = Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
                    $model->update_loginpemakai_id = Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
                    $model->create_ruangan = Params::DEFAULT_CREATA_RUANGAN;

                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                             

                                
                            
                             if(!isset($_POST['isPasienLama']))
                             {        // Jika Bukan Pasien Lama
                                
                                $modPasien = $this->savePasien($_POST['PPPasienM']);
                                $model->pasien_id=$modPasien->pasien_id;
                             }
                             else{
                                 $modPasien = $this->savePasien($modPasien);
                                 $model->pasien_id=$modPasien->pasien_id;
                             }
                             
                             if($model->save())
                                {
                                  $transaction->commit();
                                  Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                  $this->refresh();
//                                    $transaction->commit();

                                    }
                                else 
                                    {
                                        $transaction->rollback();
                                         Yii::app()->user->setFlash('error', 'Data Gagal disimpan ');

                                    }
                                

                           }
                   catch (Exception $exc) 
                        {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        }
                         
		}

//                if ($mobile->isMobile()){
                    Yii::app()->theme = 'mobile';
                    $this->layout='mainMobile2';
//                }
                
                $this->render('create',array(
			'model'=>$model,
                        'modPasien'=>$modPasien,
                ));
		
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PPBookingKamarT']))
		{
			$model->attributes=$_POST['PPBookingKamarT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->bookingkamar_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PPBookingKamarT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$format = new CustomFormat();
                $model=new PPBookingKamarT;
		$model->unsetAttributes(); // clear any default values
                $model->tglAwal=date('Y-m-d').' 00:00:00';
                $model->tglAkhir=date('Y-m-d H:i:s');
		if(isset($_GET['PPBookingKamarT']))
                {
			$model->attributes=$_GET['PPBookingKamarT'];
                        $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['PPBookingKamarT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPBookingKamarT']['tglAkhir']);
                 }
//                $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//                $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss')); 
        $this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPBookingKamarT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppbooking-kamar-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
    public function savePasien($attrPasien)
    {
        $modPasien = new PPPasienM;
        $modPasien->attributes = $attrPasien;
        $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
        $modPasien->no_rekam_medik = Generator::noRekamMedik();
        $modPasien->tgl_rekam_medik = date('Y-m-d', time());
        $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
        $modPasien->statusrekammedis = 'AKTIF';
//            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modPasien->agama = Params::DEFAULT_AGAMA;
        $modPasien->warga_negara = Params::DEFAULT_WARGANEGARA;
        $modPasien->kabupaten_id = Params::DEFAULT_KABUPATEN_ID;
        $modPasien->kecamatan_id = Params::DEFAULT_KECAMATAN_ID;
        $modPasien->kelurahan_id = Params::DEFAULT_KELURAHAN_ID;
        $modPasien->propinsi_id = Params::DEFAULT_PROPINSI_ID;
        $modPasien->jenisidentitas = Params::DEFAULT_JENIS_IDENTITAS;
        $modPasien->create_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        $modPasien->update_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        $modPasien->create_ruangan= Params::DEFAULT_CREATA_RUANGAN;
        
        if($modPasien->validate()) {
            // form inputs are valid, do something here
            $modPasien->save();
            $this->successSave = true;
        } else {
            // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
            $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                            CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
        }
        return $modPasien;
    }
}
