<?php

class ResepturController extends SBaseController
{
    private $ruangan_id;
    public function init()
    {
        $this->ruangan_id = Yii::app()->user->getState('ruangan_id');
    }

    protected function getRuangan()
    {
        return $this->ruangan_id;
    }

    public function actionIndex()
	{
		$this->render('index');
	}

    protected function postingReseptur($modReseptur, $modMorbiditas, $posting)
    {
        $vRest = array();
        $vRest['status'] = 'ok';
        $transaction = Yii::app()->db->beginTransaction();
        try{
            $vModelName = $modReseptur->getModelName();
            $modReseptur->attributes = $posting[$vModelName];
            $noUrut = ResepturT::model()->findByAttributes(array(
                'noresep'=>$modReseptur['noresep']
            ));
            if($noUrut)
            {
                throw new Exception('No. Resep sudah terdaftar, silakan dicek kembali');
            }
            if(!$modReseptur->save())
            {
                $error = array();
                foreach($modReseptur->getErrors() as $x=>$z)
                {
                    $error[$x] = implode(", ", $z);
                }
                throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
            }

            $post = $posting;

            $vModelMor = $modMorbiditas->getModelName();
            $vDiagnosaId = $post[$vModelMor]['diagnosa_id'];
            foreach ($vDiagnosaId as $i => $item)
            {
                $morb = new PasienmorbiditasT();
                $vAttr = array();
                foreach ($morb->attributes as $index => $attribute)
                {
                    if(isset($post[$vModelMor][$index][$i]))
                    {
                        $vAttr[$index] = $post[$vModelMor][$index][$i];
                    }
                }
                $morb->attributes = $vAttr;
                $morb->kelompokdiagnosa_id = 2;
                $morb->tglmorbiditas = date("Y-m-d H:i:s");
                $morb->kasusdiagnosa = Logic::getKasusDiagnosa($morb->pasien_id, $morb->diagnosa_id);
                if(!$morb->save())
                {
                    $error = array();
                    foreach($morb->getErrors() as $x=>$z)
                    {
                        $error[$x] = implode(", ", $z);
                    }
                    throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
                }
            }

            for ($i = 0; $i < count($post['obat']); $i++)
            {
                $detail = new ResepturdetailT();
                $detail->reseptur_id = $modReseptur->reseptur_id;
                $detail->obatalkes_id = $post['obat'][$i];
                $detail->sumberdana_id = $post['sumberdana'][$i];
                $detail->satuankecil_id = $post['satuankecil'][$i];
                $detail->racikan_id = ($post['isRacikan'][$i]) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                $detail->r = 'R/';
                $detail->rke = ($i + 1);
                $detail->qty_reseptur = $post['qty'][$i];
                $detail->signa_reseptur = $post['signa'][$i];
                $detail->etiket = $post['etiket'][$i];
                $detail->kekuatan_reseptur = $post['kekuatan'][$i];
                $detail->satuankekuatan = $post['satuankekuatan'][$i];
                $detail->hargasatuan_reseptur = $post['hargasatuan'][$i];
                $detail->harganetto_reseptur = $post['harganetto'][$i];
                $detail->hargajual_reseptur = $post['subTotal'][$i];
                $detail->permintaan_reseptur = $post['jmlpermintaan'][$i];
                $detail->jmlkemasan_reseptur = $post['jmlkemasan'][$i];

                if(!$detail->save())
                {
                    $error = array();
                    foreach($detail->getErrors() as $x=>$z)
                    {
                        $error[$x] = implode(", ", $z);
                    }
                    throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
                }
            }
            $transaction->commit();
        }catch (Exception $e)
        {
            $transaction->rollback();
            $vMsg = $e->getMessage();
            $vRest['status'] = 'not';
            $vRest['msg'] = $vMsg;
        }
        return $vRest;
    }

}
