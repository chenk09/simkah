<?php
Yii::import('pendaftaranPenjadwalan.models.*');
class BuatjanjipoliTController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionAdmin()
	{
//      if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modPPBuatJanjiPoli=new PPBuatJanjiPoliT;
        $modPasien=new PPPasienM;
        $mobile = new MobileDetect();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
        $format = new CustomFormat;
		if(isset($_POST['PPBuatJanjiPoliT']))
		{
            $modPasien->attributes = $_POST['PPPasienM'];
            $modPasien->umur = $_POST['PPPasienM']['umur'];
             $transaction = Yii::app()->db->beginTransaction();
               try 
               {    
                    $modPPBuatJanjiPoli->attributes=$_POST['PPBuatJanjiPoliT'];
                    $modPPBuatJanjiPoli->tglbuatjanji=date('Y-m-d H:i:s');
                    $modPPBuatJanjiPoli->tgljadwal=$format->formatDateTimeMediumForDB($_POST['PPBuatJanjiPoliT']['tgljadwal']);
                    $modPPBuatJanjiPoli->create_time=date('Y-m-d H:i:s');
                    $modPPBuatJanjiPoli->update_time=date('Y-m-d H:i:s');
                    $modPPBuatJanjiPoli->no_rekam_medik = 'WOW';
//                                $modPPBuatJanjiPoli->update_loginpemakai_id=Yii::app()->user->id;
                    $modPPBuatJanjiPoli->create_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
                    $modPPBuatJanjiPoli->update_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
                    $modPPBuatJanjiPoli->create_ruangan= Params::DEFAULT_CREATA_RUANGAN;
                   
                    if(isset($_POST['isPasienLama']))
                        {   //Jika Pasiennya Lama
           
                             $modPasien = $this->savePasien($_POST['PPPasienM']);
                             $modPPBuatJanjiPoli->pasien_id=$modPasien->pasien_id;
                           
                           
                        }
                        else{
//                                        echo '<pre>';
//                                        echo print_r($modPasien->attributes);
//                                        exit();
                            $modPasien = $this->savePasien($modPasien);
                             $modPPBuatJanjiPoli->pasien_id=$modPasien->pasien_id;
                        }
                    
//                               echo print_r($modPPBuatJanjiPoli->attributes);
                    if($modPPBuatJanjiPoli->validate())
                        {
                            $modPPBuatJanjiPoli->save();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data Pasien Dan Janji Kunjungan berhasil disimpan.');
                            $transaction->commit();
                            $this->refresh();
                        }
                    else 
                        {
                            $transaction->rollback();
                             Yii::app()->user->setFlash('error', 'Data Gagal disimpan ');

                        }
                    

               }
               catch(Exception $exc)
               {
                   $transaction->rollback();
                   Yii::app()->user->setFlash('error', 'Data Gagal disimpan'.MyExceptionMessage::getMessage($exc,true).'');

               }
		}

//      if ($mobile->isMobile()){
            Yii::app()->theme = 'mobile';
            $this->layout='mainMobile2';    
//      }
            $this->render('admin',array(
                    'modPasien'=>$modPasien,
                    'modPPBuatJanjiPoli'=>$modPPBuatJanjiPoli
            ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPBuatJanjiPoliT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppbuat-janji-poli-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
    public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
    public function savePasien($attrPasien)
    {
//            $modPasien = new PasienM();
        $modPasien = $attrPasien;
        $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
        $modPasien->no_rekam_medik = Generator::noRekamMedik('BJ');
        $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
        $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
        $modPasien->agama = Params::DEFAULT_AGAMA;
        $modPasien->warga_negara = Params::DEFAULT_WARGANEGARA;
        $modPasien->kabupaten_id = Yii::app()->user->getState('kabupaten_id');
        $modPasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
        $modPasien->kelurahan_id = Yii::app()->user->getState('kelurahan_id');
        $modPasien->propinsi_id = Yii::app()->user->getState('propinsi_id');
        $modPasien->jenisidentitas = Params::DEFAULT_JENIS_IDENTITAS;
        $modPasien->create_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        $modPasien->update_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        $modPasien->create_ruangan= Params::DEFAULT_CREATA_RUANGAN;
        $modPasien->statusrekammedis = 'AKTIF';
//            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
        
        if($modPasien->validate()) {

            // form inputs are valid, do something here
            $modPasien->save();
//            echo print_r($modPasien->attributes);
        } else {
//                                exit();
//                echo var_dump($_POST['PPPasienM']);exit;
               // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
            $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                            CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
        }
        return $modPasien;
    }
}
