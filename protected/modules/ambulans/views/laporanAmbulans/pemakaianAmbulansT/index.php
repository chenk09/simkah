<legend class="rim2">Laporan Pemakaian Ambulans</legend>
<?php
$url = Yii::app()->createUrl('ambulans/laporanAmbulans/frameGrafikPemakaianAmbulans&id=1');
Yii::app()->clientScript->registerScript('search', "
$('#laporan-search').submit(function(){
    $.fn.yiiGridView.update('laporan-grid', {
        data: $(this).serialize()
    });
    return false;
});
"); 
?>

<?php $this->renderPartial('pemakaianAmbulansT/_search',array('model'=>$model)); ?>
<fieldset>
    <?php $this->renderPartial('pemakaianAmbulansT/_table',array('model'=>$model)); ?>
    <?php $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0' onload='javascript:resizeIframe(this);'>
    </iframe>
</fieldset>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printPemakaianAmbulans');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
<script type="text/javascript">
function batalPakai(idPemakaian,idPemesanan)
{
    if(confirm('Anda yakin akan membatalkan pemakaian ambulans?')){
        $.post('<?php echo $this->createUrl('batalPakai'); ?>', {idPemakaian:idPemakaian,idPemesanan:idPemesanan}, function(data){
            if(data.status == 'berhasil'){
                $.fn.yiiGridView.update('pemakaianambulans-t-grid', {
                    data: $(this).serialize()
                });
                return false;
            }
        }, 'json');
    }
}
</script>