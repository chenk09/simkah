<style>
    .even td {
        border-left:1px solid #DDDDDD;
    }
    .odd td {
        border-left:1px solid #DDDDDD;
    }
</style>
<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$data = $model->searchLaporan();
$templates = "{pager}{summary}\n{items}";
if (isset($caraPrint)) {
    $data = $model->searchLaporanPrint();
    $templates = "\n{items}";
    if ($caraPrint=='EXCEL') {
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
}
$this->widget($table,array(
    'id'=>'laporan-grid',
    'dataProvider'=>$data,
    //'filter'=>$model,
        'template'=>$templates,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'mergeColumns'=>array(
        'mobil',
    ),
    'columns'=>array(
        array(
            'name'=>'mobil',
            'header'=>'Jenis Kendaraan - No Polisi',
            'value'=>'$data->mobil->jeniskendaraan." - ".$data->mobil->nopolisi',
            'footer'=>'Total'
        ),
        'norekammedis',
        'namapasien',
        'tempattujuan',
        'alamattujuan',
        //'nomobile',
        //'notelepon',
        array(
            'header'=>'No Mobile / Telepon',
            'value'=>'$data->nomobile." / ".$data->notelepon',
        ),
        array(
            'header'=>$model->getAttributeLabel('supir_id'),
            'value'=>'$data->supir->nama_pegawai',
        ),
        array(
            'header'=>'Paramedis',
            'value'=>'$data->paramedis1->nama_pegawai." | ".$data->paramedis2->nama_pegawai',
        ),
        /*
        array(
            'header'=>$model->getAttributeLabel('paramedis2_id'),
            'value'=>'$data->paramedis2->nama_pegawai',
        ),*/
        //'paramedis1.nama_pegawai',
        //'paramedis1',
        //'KMawalKMakhir',
        array(
            'header'=>$model->getAttributeLabel('KMawalKMakhir'),
            'value'=> 'MyFunction::formatNumber($data->kmawal)."/".MyFunction::formatNumber($data->kmakhir)',
            'filter'=>false,
            'footer'=>$model->getTotal('kmawal').'/'.$model->getTotal('kmakhir'),
        ),
        array(
            'name'=>'jumlahkm',
            'value'=>'MyFunction::formatNumber($data->jumlahkm)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            'footer'=>$model->getTotal('jumlahkm'),
        ),
        array(
            'name'=>'tarifperkm',
            'value'=>'MyFunction::formatNumber($data->tarifperkm)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            'footer'=>$model->getTotal('tarifperkm'),
        ),
        array(
            'name'=>'totaltarifambulans',
            'value'=>'MyFunction::formatNumber($data->totaltarifambulans)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            'footer'=>$model->getTotal('totaltarifambulans'),
        ),
        'tglkembaliambulans',
        /*
        'ruangan_id',
        'batalpakaiambulans_id',
        'mobilambulans_id',
        'pasien_id',
        'pesanambulans_t',
        'pendaftaran_id',
        'noidentitas',
        'kelurahan_nama',
        'rt_rw',
        'namapj',
        'hubunganpj',
        'alamatpj',
        'pelaksana_id',
        'jmlbbmliter',
        'untukkeperluan',
        'create_time',
        'update_time',
        'create_loginpemakai_id',
        'update_loginpemakai_id',
        'create_ruangan',
        */
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 