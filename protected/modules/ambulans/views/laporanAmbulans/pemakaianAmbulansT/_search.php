
<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'laporan-search',
        'type'=>'horizontal',
)); ?>
    <table>
        <tr>
            <td>
                <?php //echo $form->textFieldRow($modPemesanan,'tglpemesananambulans',array('class'=>'span3')); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model,'tglpemakaianambulans', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); 
                     ?> </div>
                </div>
            </td>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Sampai dengan','AMPemakaianambulansT_tglAkhir',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
<?php $this->endWidget(); ?>
</div>