<fieldset>
    <legend class="rim2">Informasi Pasien Rawat Inap</legend>
</fieldset>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
//echo Yii::app()->user->getState('ruangan_id');
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchRILagi(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'header'=>'Tgl Admisi / Masuk Kamar',
                        'type'=>'raw',
                        'value'=>'$data->tglAdmisiMasukKamar'
                    ),
//                    'ruangan_nama',
                    array(
                       'name'=>'caramasuk_nama',
                        'type'=>'raw',
                        'value'=>'$data->caramasuk_nama',
                    ),
                    array(
                       'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'name'=>'umur',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                       'name'=>'kamarruangan_nokamar',
                        'type'=>'raw',
                        'value'=>'(!empty($data->kamarruangan_nokamar))? $data->kamarruangan_nokamar : CHtml::link("<i class=icon-home></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Memasukan Pasien Ke kamar","onclick"=>"{buatSessionMasukKamar($data->masukkamar_id,$data->kelaspelayanan_id,$data->pendaftaran_id); addMasukKamar(); $(\'#dialogMasukKamar\').dialog(\'open\');}"))',    
                    ),
                    
//                    array(
//                       'header'=>'Pindah Kamar',
//                       'type'=>'raw',
//                       'value'=>'(!empty($data->pasienpulang_id) ? $data->carakeluar : CHtml::link("<i class=\'icon-share\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/PindahKamarPasienRI",array("idPendaftaran"=>$data->pendaftaran_id)) ,array("title"=>"Klik Untuk Pindah Kamar","target"=>"iframe", "onclick"=>"$(\"#dialogPindahKamar\").dialog(\"open\");", "rel"=>"tooltip"))) ',
//                    ),
//                    array(
//                        'name'=>'Pemeriksaan Pasien',
//                        'type'=>'raw',
//                        'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatInap/anamnesa",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))',
//                        'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//                    ),
                    array(
                        'header'=>'Pemakaian Ambulans',
                        'type'=>'raw',
                        'value'=>'(empty($data->pemakaianambulans_id)) ? CHtml::Link("<i class=\"icon-check\"></i>",
                                               Yii::app()->controller->createUrl("transaksi/pemakaian",array("idPendaftaran"=>$data->pendaftaran_id,
                                                                                                             "modulId"=>Yii::app()->session["modulId"])),
                                               array("class"=>"btn-small")) : ""',
                    )
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr/>

<?php echo $this->renderPartial('_searchPasienRI', array('model'=>$model)); ?>



<?php 
// Dialog untuk pasienpulang_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPasienPulang',
    'options'=>array(
        'title'=>'Pasien Pulang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>

<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogMasukKamar',
    'options'=>array(
        'title'=>'Masuk Kamar Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));




$this->endWidget();
//========= end masukkamar_t dialog =============================
?>

<?php 
// Dialog untuk pindahkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPindahKamar',
    'options'=>array(
        'title'=>'Pindah Kamar Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1000,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));
?>

<iframe src="" name="iframe" width="100%" height="500">
</iframe>

<?php
$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>

<?php 
// Dialog untuk pindahkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogTindakLanjut',
    'options'=>array(
        'title'=>'Pindah Kamar Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1100,
        'minHeight'=>700,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeTindakLanjut" width="100%" height="900">
</iframe>

<?php
$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>
