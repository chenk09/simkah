<legend class="rim">Pencarian</legend> 
<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'pesanambulans-t-search',
        'type'=>'horizontal',
)); ?>
    <table>
        <tr>
            <td>
                <?php //echo $form->textFieldRow($modPemesanan,'tglpemesananambulans',array('class'=>'span3')); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemesanan,'tgl_pemesanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPemesanan,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); 
                  ?> </div></div>
						<div class="control-group ">
                    <label for="namaPasien" class="control-label">
                       Sampai dengan
                      </label>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPemesanan,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); ?>
                    </div>
                </div>
				<div class="control-group "><label for="namaPasien" class="control-label">
                       No. pesan Ambulance </label>        <div class="controls">
                <?php echo $form->textField($modPemesanan,'pesanambulans_no',array('class'=>'span3','maxlength'=>20)); ?>
				</div></div>
            </td>
            <td><div class="control-group "><label for="namaPasien" class="control-label">
                       No. Rekam medis</label>        <div class="controls">
                <?php echo $form->textField($modPemesanan,'norekammedis',array('class'=>'span3','maxlength'=>10)); ?></div></div>
				<div class="control-group "><label for="namaPasien" class="control-label">
                       Nama Pasien</label>        <div class="controls">
                <?php echo $form->textField($modPemesanan,'namapasien',array('class'=>'span3','maxlength'=>100)); ?></div></div>
				<div class="control-group "><label for="namaPasien" class="control-label">
                       Nama ruang</label>        <div class="controls">
                <?php echo $form->textField($modPemesanan,'ruangan_nama',array('class'=>'span3')); ?></div></div>
            </td>
        </tr>
    </table>
    

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php 
            echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); 

        ?>								
<?php  
        $content = $this->renderPartial('../tips/informasi',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));

?>
    </div>

<?php $this->endWidget(); ?>

</div>
<?php 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintInformasiPemesanan');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#pesanambulans-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}   
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);  

?>
    