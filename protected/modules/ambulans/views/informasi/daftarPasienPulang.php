<fieldset>
    <legend class="rim2">Daftar Pasien Pulang</legend>
</fieldset>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchPasienPulang(),
//        'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Instalasi / Ruangan',
                    'value'=>'$data->instalasi_nama." / ".$data->ruangan_nama',
                ),
                array(
                    'header'=>'Cara Pulang / Kondisi Pulang',
                    'value'=>'$data->carakeluar." / ".$data->kondisipulang',
                ),
                array(
                    'header'=>'No RM / No Pendaftaran',
                    'value'=>'$data->no_rekam_medik." / ".$data->no_pendaftaran',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'value'=>'$data->nama_pasien." bin ".$data->nama_bin',
                ),
                array(
                    'header'=>'Umur',
                    'value'=>'$data->umur',
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'value'=>'$data->jeniskelamin',
                ),
                array(
                    'header'=>'Alamat',
                    'value'=>'$data->alamat_pasien',
                ),
                array(
                    'header'=>'Kelurahan',
                    'value'=>'$data->kelurahan_nama',
                ),
                array(
                    'header'=>'Penerima Pasien',
                    'value'=>'',
                ),
                array(
                    'header'=>'Lama Dirawat',
                    'value'=>'$data->lamarawat." ".$data->satuanlamarawat',
                ),
                array(
                    'header'=>'Kasus Penyakit',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
                array(
                    'header'=>'Dirujuk Ke',
                    'type'=>'raw',
                    'value'=>'(!empty($data->rujukankeluar_id)) ? CHtml::link("<i class=\"icon-eye-open\"></i>", "javascript:void(0);", 
                                            array("rel"=>"tooltip",
                                                  "title"=>"Klik untuk melihat detail",
                                                  "onclick"=>"detailRujukan(\'$data->rumahsakitrujukan\',
                                                                            \'$data->alamatrsrujukan\',
                                                                            \'$data->telp_fax\',
                                                                            \'$data->tgldirujuk\',
                                                                            \'$data->ythdokter\',
                                                                            \'$data->dirujukkebagian\',
                                                                            \'$data->alasandirujuk\',
                                                                            \'$data->hasilpemeriksaan_ruj\',
                                                                            \'$data->diagnosasementara_ruj\',
                                                                            \'$data->pengobatan_ruj\',
                                                                            \'$data->lainlain_ruj\',
                                                                            \'$data->catatandokterperujuk\');return false;"))." ".$data->rumahsakitrujukan : " " ',
                ),
                array(
                    'header'=>'Pemakaian Ambulans',
                    'type'=>'raw',
                    'value'=>'(empty($data->pemakaianambulans_id)) ? CHtml::Link("<i class=\"icon-check\"></i>",
                                           Yii::app()->controller->createUrl("transaksi/pemakaian",array("idPendaftaran"=>$data->pendaftaran_id,
                                                                                                         "modulId"=>Yii::app()->session["modulId"])),
                                           array("class"=>"btn-small")) : ""',
                ) 
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr/>

<?php $this->renderPartial('_searchPasienPulang',array('model'=>$model)); ?>

<script type="text/javascript">
function detailRujukan(rumahsakitrujukan,alamatrsrujukan,telp_fax,tgldirujuk,ythdokter,dirujukkebagian,alasandirujuk,hasilpemeriksaan_ruj,diagnosasementara_ruj,pengobatan_ruj,lainlain_ruj,catatandokterperujuk)
{
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'rumahsakitrujukan') ?>').val(rumahsakitrujukan);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'alamatrsrujukan') ?>').val(alamatrsrujukan);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'telp_fax') ?>').val(telp_fax);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'tgldirujuk') ?>').val(tgldirujuk);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'ythdokter') ?>').val(ythdokter);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'dirujukkebagian') ?>').val(dirujukkebagian);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'alasandirujuk') ?>').val(alasandirujuk);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'hasilpemeriksaan_ruj') ?>').val(hasilpemeriksaan_ruj);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'diagnosasementara_ruj') ?>').val(diagnosasementara_ruj);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'pengobatan_ruj') ?>').val(pengobatan_ruj);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'lainlain_ruj') ?>').val(lainlain_ruj);
    $('#dialogDetailRujukan #<?php echo CHtml::activeId($model, 'catatandokterperujuk') ?>').val(catatandokterperujuk);
    $('#dialogDetailRujukan').dialog('open');
}
</script>

<?php 
//========== Dialog untuk detail rujukan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailRujukan',
    'options'=>array(
        'title'=>'Detail Rujukan Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>300,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_detailRujukan',array('model'=>$model));

$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>