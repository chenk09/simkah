<legend class="rim2">Pemakaian Ambulans</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('pemakaianambulans-t-grid', {
        data: $(this).serialize()
    });
    return false;
});
"); 
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pemakaianambulans-t-grid',
    'dataProvider'=>$modPemakaian->searchPemakaian(),
    //'filter'=>$modPemakaian,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        'mobil.nopolisi',
        'mobil.jeniskendaraan',
        'norekammedis',
        'namapasien',
        'tempattujuan',
        'alamattujuan',
        array(
            'header'=>'No Mobile / Telepon',
            'value'=>'$data->nomobile." / ".$data->notelepon',
        ),
        array(
            'header'=>$modPemakaian->getAttributeLabel('supir_id'),
            'value'=>'$data->supir->nama_pegawai',
        ),
        array(
            'header'=>'Paramedis',
            'value'=>'$data->paramedis1->nama_pegawai." | ".$data->paramedis2->nama_pegawai',
        ),
        array(
            'header'=>$modPemakaian->getAttributeLabel('KMawalKMakhir'),
            'value'=> 'MyFunction::formatNumber($data->kmawal)."/".MyFunction::formatNumber($data->kmakhir)',
            'filter'=>false,
        ),
        array(
            'name'=>'jumlahkm',
            'value'=>'MyFunction::formatNumber($data->jumlahkm)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
        ),
        array(
            'name'=>'tarifperkm',
            'value'=>'MyFunction::formatNumber($data->tarifperkm)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
        ),
        array(
            'name'=>'totaltarifambulans',
            'value'=>'MyFunction::formatNumber($data->totaltarifambulans)',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
        ),
        'tglkembaliambulans',
        array(
            'header'=>'Batal Pakai',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0)",
                                   array("onclick"=>"batalPakai(\'$data->pemakaianambulans_id\',\'$data->pesanambulans_t\')",
                                         "class"=>"btn-small"))',
        )
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 

<?php $this->renderPartial('_searchPemakaian',array('modPemakaian'=>$modPemakaian)); ?>

<script type="text/javascript">
function batalPakai(idPemakaian,idPemesanan)
{
    if(confirm('Anda yakin akan membatalkan pemakaian ambulans?')){
        $.post('<?php echo $this->createUrl('batalPakai'); ?>', {idPemakaian:idPemakaian,idPemesanan:idPemesanan}, function(data){
            if(data.status == 'berhasil'){
                $.fn.yiiGridView.update('pemakaianambulans-t-grid', {
                    data: $(this).serialize()
                });
                return false;
            }
        }, 'json');
    }
}
</script>