
<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'pemakaianambulans-t-search',
        'type'=>'horizontal',
)); ?>
    <table>
        <tr>
            <td>
                <?php //echo $form->textFieldRow($modPemesanan,'tglpemesananambulans',array('class'=>'span3')); ?>
                <div class="control-group ">
                    <?php echo CHtml::label('Tgl Pemakaian','tglPemakaian', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPemakaian,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); 
                     ?> </div></div>
						<div class="control-group ">
                    <label for="namaPasien" class="control-label">
                       Sampai dengan
                      </label>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPemakaian,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                            )); ?>
                    </div>
                </div>
                <?php //echo CHtml::activeTextField($modPemakaian->mobil, 'nopolisi', array('readonly'=>true)); ?>
                <?php echo $form->textFieldRow($modPemakaian,'nopolisi',array('class'=>'span3','maxlength'=>20)); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($modPemakaian,'norekammedis',array('class'=>'span3','maxlength'=>10)); ?>
                <?php echo $form->textFieldRow($modPemakaian,'namapasien',array('class'=>'span3','maxlength'=>100)); ?>
                <?php //echo $form->textFieldRow($modPemakaian,'ruangan_nama',array('class'=>'span3')); ?>
            </td>
        </tr>
    </table>
    

    <div class="form-actions">
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
							           <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>								
<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>
</div>