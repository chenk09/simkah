
<legend class="rim2">Informasi Tarif Ambulans</legend>

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'tableTarif',
	'dataProvider'=>$modTarif->search(),
        'template'=>"{pager}{summary}\n{items}",
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Tujuan</center>',
                'start'=>0, //indeks kolom 3
                'end'=>3, //indeks kolom 4
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'name'=>'kepropinsi_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'kekabupaten_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'kekecamatan_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'kekelurahan_nama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'jmlkilometer',
                    'value'=>'MyFunction::formatNumber($data->jmlkilometer)',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'tarifperkm',
                    'value'=>'MyFunction::formatNumber($data->tarifperkm)',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'name'=>'tarifambulans',
                    'value'=>'MyFunction::formatNumber($data->tarifambulans)',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->renderPartial('_searchTarif',array('modTarif'=>$modTarif)) ?>