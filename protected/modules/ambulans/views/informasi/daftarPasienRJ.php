<fieldset>
    <legend class="rim2">Informasi Pasien Rawat Jalan</legend>
</fieldset>


<?php
$urlTindakLanjut = Yii::app()->createUrl('actionAjax/pasienRujukRI');
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchDaftarPasien(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(	
                'no_urutantri',	
                //'pendaftaran.pasienpulang_id',	
                'tgl_pendaftaran',
                array(
                    'name'=>'No_pendaftaran'.'/<br/>'.'No_rekam_medik',
                    'type'=>'raw',
                    'value'=>'"$data->no_pendaftaran"."<br/>"."$data->no_rekam_medik"',
                ),
                array(
                    'name'=>'nama_pasien'.'/<br/>'.'bin',
                    'type'=>'raw',
                    'value'=>'"$data->nama_pasien"."<br/>"."$data->nama_bin"',
                ),
                array(
                    'name'=>'alamat_pasien'.'/<br/>'.'RT RW',
                    'type'=>'raw',
                    'value'=>'"$data->alamat_pasien"."<br/>"."$data->RTRW"',
                ),
                array(
                    'name'=>'Penjamin'.'/<br/>'.'Cara Bayar',
                    'type'=>'raw',
                    'value'=>'"$data->penjamin_nama"."<br/>"."$data->carabayar_nama"',
                ),
		//'no_rekam_medik',
		//'nama_pasien',
		//'nama_bin',
		//'alamat_pasien',
                //'RTRW',
                //'penjamin_nama',
                //'carabayar_id',
                'nama_pegawai',
                //'ruangan_nama',
		'jeniskasuspenyakit_nama',
		'statusperiksa',
                
//                array(
//                    'name'=>'Pemeriksaan Pasien',
//                    'type'=>'raw',
//                    'value'=>'(($data->statusperiksa!=Params::statusPeriksa(5))) ? CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatJalan/anamnesa",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien")):"-"',
//                ),
//                array(
//                    'name'=>'Tindak Lanjut<br/> Rawat Inap',
//                    'type'=>'raw',
//                    'value'=>'(!empty($data->pendaftaran->pasienpulang_id) || ($data->statusperiksa==Params::statusPeriksa(5))) ? "Pasien Rawat Inap" : CHtml::link("<i class=\'icon-user\'></i>", "javascript:tindaklanjutrawatjalan(\'$data->pendaftaran_id\')",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Proses Tindak Lanjut Pasien"))',
//                ),
                array(
                    'header'=>'Pemakaian Ambulans',
                    'type'=>'raw',
                    'value'=>'($data->statusperiksa != Params::statusPeriksa(5)) ? CHtml::Link("<i class=\"icon-check\"></i>",
                                           Yii::app()->controller->createUrl("transaksi/pemakaian",array("idPendaftaran"=>$data->pendaftaran_id,
                                                                                                         "modulId"=>Yii::app()->session["modulId"])),
                                           array("class"=>"btn-small")) : ""',
                )
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->renderPartial('_searchPasienRJ',array('model'=>$model)); ?>
