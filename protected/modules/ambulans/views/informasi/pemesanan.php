
<legend class="rim2">Pemesanan Ambulans</legend>

<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('pesanambulans-t-grid', {
        data: $(this).serialize()
    });
    return false;
});
"); 
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pesanambulans-t-grid',
    'dataProvider'=>$modPemesanan->search(),
    //'filter'=>$modPemesanan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'pesanambulans_t',
        //'pendaftaran_id',
        //'mobilambulans_id',
        //'pemakaianambulans_id',
        //'pasien_id',
        
        'pesanambulans_no',
        'norekammedis',
        'namapasien',
        'tempattujuan',
        'alamattujuan',
        'tglpemakaianambulans',
        'untukkeperluan',
        'ruanganpemesan.ruangan_nama',
        'userpemesan.nama_pemakai',
        /*
        'tglpemesananambulans',
        'kelurahan_nama',
        'rt_rw',
        'nomobile',
        'notelepon',
        'keteranganpesan',
        'create_time',
        'update_time',
        'update_loginpemakai_id',
        'create_ruangan',
        */
        array(
            'header'=>'Pemakaian Ambulans',
            'type'=>'raw',
            'value'=>'(empty($data->pemakaianambulans_id)) ? CHtml::Link("<i class=\"icon-check\"></i>",
                                   Yii::app()->controller->createUrl("transaksi/pemakaian",array("idPemesanan"=>$data->pesanambulans_t,
                                                                                                 "modulId"=>Yii::app()->session["modulId"])),
                                   array("class"=>"btn-small")) : ""',
        )
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 

<?php $this->renderPartial('_searchPemesanan',array('modPemesanan'=>$modPemesanan)) ?>
        <?php
//     $this->widget('bootstrap.widgets.BootButtonGroup', array(
//                'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
//                'buttons'=>array(
//                    array('label'=>'Print', 'icon'=>'icon-print icon-white', 'url'=>'#', 'htmlOptions'=>array('onclick'=>'print(\'PRINT\')')),
//                    array('label'=>'', 'items'=>array(
//                        array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'PDF\')')),
//                        array('label'=>'EXCEL','icon'=>'icon-pdf', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'EXCEL\')')),
//                        array('label'=>'PRINT','icon'=>'icon-print', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'PRINT\')')),
//                    )),       
//                ),
//        //        'htmlOptions'=>array('class'=>'btn')
//            )); 
?>