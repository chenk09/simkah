<fieldset>
    <legend class="rim">Pencarian</legend>
</fieldset>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
)); ?>

                <div class="control-label"> Daftar Tindakan </div>
                <div class="controls">
                    <?php echo $form->hiddenField($modTarif, 'daftartindakan_id',array('id'=>'daftartindakan_id')) ?>
                    <?php $this->widget('MyJuiAutoComplete', array(
                           'name'=>'daftartindakan', 
                            'source'=>'js: function(request, response) {
                                   $.ajax({
                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/Daftartindakan').'",
                                       dataType: "json",
                                       data: {
                                           term: request.term,
                                       },
                                       success: function (data) {
                                               response(data);
                                       }
                                   })
                                }',
                            'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 1,
                                       'focus'=> 'js:function( event, ui )
                                           {
                                            $(this).val(ui.item.daftartindakan_nama);
                                            return false;
                                            }',
                                       'select'=>'js:function( event, ui ) {
                                           $("#daftartindakan_id").val(ui.item.daftartindakan_id);
                                            return false;
                                        }',
                            ),
                            'htmlOptions'=>array(
                                'readonly'=>false,
                                'placeholder'=>'Daftar Tindakan',
                                'size'=>13,
                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogDaftartindakan'),
                    )); ?>
                </div>
		<?php echo $form->textFieldRow($modTarif,'tarifambulans_kode',array('size'=>20,'maxlength'=>20,'class'=>'span2')); ?>
  <?php echo $form->dropDownListRow($modTarif,'kepropinsi_nama', CHtml::listData($modTarif->getPropinsiItems(), 'propinsi_nama', 'propinsi_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
     'url'=>Yii::app()->createUrl('ActionDynamic/GetTarifKabupaten',array('encode'=>false,'namaModel'=>'AMTarifambulansM')),
                                                              'update'=>'#AMTarifambulansM_kekabupaten_nama'))); ?>
        <?php echo $form->dropDownListRow($modTarif,'kekabupaten_nama', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
  'url'=>Yii::app()->createUrl('ActionDynamic/GetTarifKecamatan',array('encode'=>false,'namaModel'=>'AMTarifambulansM')),
                                                              'update'=>'#AMTarifambulansM_kekecamatan_nama'))); ?>
                
                                <?php echo $form->dropDownListRow($modTarif,'kekecamatan_nama', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetTarifKelurahan',array('encode'=>false,'namaModel'=>'AMTarifambulansM')),
                                                              'update'=>'#AMTarifambulansM_kekelurahan_nama'))); ?>
                
                                <?php echo $form->dropDownListRow($modTarif,'kekelurahan_nama', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                               )); ?>
		<?php echo $form->textFieldRow($modTarif,'tarifperkm',array('class'=>'span1')); ?>
		<?php echo $form->textFieldRow($modTarif,'tarifambulans',array('class'=>'span1')); ?>

	<div class="form-actions">
             <?php
                echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); 
                echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
                <?php
                $content = $this->renderPartial('../tips/informasi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
	</div>

<?php $this->endWidget(); ?>
