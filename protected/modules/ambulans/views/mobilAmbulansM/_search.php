<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
)); ?>

                <div class="control-label">Inventaris Aset *</div>
                <div class="controls">
                    <?php echo $form->hiddenField($model, 'inventarisaset_id',array('id'=>'inventarisaset_id')) ?>
                    <?php $this->widget('MyJuiAutoComplete', array(
                                                                       'name'=>'inventarisaset', 
                                                                        'source'=>'js: function(request, response) {
                                                                               $.ajax({
                                                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/Barang').'",
                                                                                   dataType: "json",
                                                                                   data: {
                                                                                       term: request.term,
                                                                                   },
                                                                                   success: function (data) {
                                                                                           response(data);
                                                                                   }
                                                                               })
                                                                            }',
                                                                        'options'=>array(
                                                                                   'showAnim'=>'fold',
                                                                                   'minLength' => 1,
                                                                                   'focus'=> 'js:function( event, ui )
                                                                                       {
                                                                                        $(this).val(ui.item.barang_nama);
                                                                                        return false;
                                                                                        }',
                                                                                   'select'=>'js:function( event, ui ) {
                                                                                       $("#alatmedis_noaset").val(ui.item.barang_id);
                                                                                        return false;
                                                                                    }',
                                                                        ),
                                                                        'htmlOptions'=>array(
                                                                            'readonly'=>false,
                                                                            'placeholder'=>'No Aset',
                                                                            'size'=>13,
                                                                            'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                        ),
                                                                        'tombolDialog'=>array('idDialog'=>'dialogbarang'),
                                                                )); ?>
                </div>
		<?php echo $form->textFieldRow($model,'mobilambulans_kode',array('size'=>20,'maxlength'=>20,'class'=>'span2')); ?>
		<?php echo $form->textFieldRow($model,'nopolisi',array('size'=>20,'maxlength'=>20,'class'=>'span1')); ?>
                                 <?php echo $form->dropDownListRow($model,'jeniskendaraan',
                                CHtml::listData($model->JenisKendaraanItems, 'lookup_name', 'lookup_value'),
                                array('class'=>'inputRequire', 'onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --',)); ?>
		<?php echo $form->textFieldRow($model,'isibbmliter',array('class'=>'span1')); ?>
		<?php echo $form->textFieldRow($model,'kmterakhirkend',array('class'=>'span1')); ?>
		<?php echo $form->textFieldRow($model,'hargabbmliter',array('class'=>'span1')); ?>
		<?php echo $form->checkBoxRow($model,'mobilambulans_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>