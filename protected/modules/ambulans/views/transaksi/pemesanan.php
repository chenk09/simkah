<?php
$this->breadcrumbs=array(
	'Transaksi'=>array('/ambulans/transaksi'),
	'Pemesanan',
);?>
<legend class="rim2">Transaksi Pesan Ambulans</legend>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pesanambulans-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modPemesanan); ?>

            <?php echo CHtml::activeHiddenField($modPemesanan,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo CHtml::activeHiddenField($modPemesanan,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'no_rekam_medis', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $modPemesanan,
                                'attribute' => 'norekammedis',
                                'value' => '',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);

                                            return false;
                                        }',
                                    'select' => 'js:function( event, ui ) {
                                              $("#' . CHtml::activeId($modPemesanan, 'pasien_id') . '").val(ui.item.pasien_id);
                                              $("#' . CHtml::activeId($modPemesanan, 'namapasien') . '").val(ui.item.nama_pasien);
                                              $("#' . CHtml::activeId($modPemesanan, 'pendaftaran_id') . '").val(ui.item.pendaftaran_id);
                                          }',
                                ), 'tombolDialog'=>array('idDialog'=>'dialogPasien'),
                                'htmlOptions'=>array(
                                    'class'=>'span2',
                                    'placeholder'=>'Ketikan No Rekam Medis',
                                    ),
                            ));
                        ?>
                    </div>
                </div>
 	<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'nama pasien<span class="required">*</span>', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'namapasien',array('class'=>'span3', 'onchange'=>'clearDataPasien();' ,'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </div></div>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'tgl pemesanan ambulans', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modPemesanan,
                                                            'attribute'=>'tglpemesananambulans',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'no_pesan_ambulans<span class="required">*</span>', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'pesanambulans_no',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
				</div></div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'tempat tujuan', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'tempattujuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
				</div></div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'kelurahan_nama', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'kelurahan_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
				</div></div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'alamat tujuan', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textArea($modPemesanan,'alamattujuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
				</div></div>
                <?php //echo $form->textFieldRow($modPemesanan,'rt_rw',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemesanan,'rt_rw', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('CMaskedTextField',array(
                                'model'=>$modPemesanan,
                                'attribute'=>'rt_rw',
                                //'name'=>'date',
                                'mask'=>'99/99',
                                'htmlOptions'=>array(
                                    'class'=>'span1',
                                ),
                            ));
                        ?>
                        <?php echo $form->error($modPemesanan, 'rt_rw'); ?>
                    </div>
                </div>
            </td>
            <td>
			<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'no mobile', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'nomobile',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
				</div></div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'no telepon', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textField($modPemesanan,'notelepon',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
				</div></div>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'tgl pemakaian ambulans', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modPemesanan,
                                                            'attribute'=>'tglpemakaianambulans',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>false,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
				   <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'untuk keperluan', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textArea($modPemesanan,'untukkeperluan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
				</div></div>
				<div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'keterangan pesan', array('class' => 'control-label')); ?>
                    <div class="controls">
                <?php echo $form->textArea($modPemesanan,'keteranganpesan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
				</div></div>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemesanan, 'ruangan_id', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('instalasi', '', CHtml::listData($modInstalasi, 'instalasi_id', 'instalasi_nama'),
                                                        array('empty' =>'-- Instalasi --',
                                                              'ajax'=>array('type'=>'POST',
                                                                            'url'=>  CController::createUrl('dynamicRuangan'),
                                                                            'update'=>'#AMPesanambulansT_ruangan_id',),'class'=>'span2')); ?>
                        <?php echo CHtml::activeDropDownList($modPemesanan, 'ruangan_id', array(),array('empty' =>'-- Ruangan --','class'=>'span2')); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
            
    
    <div class="form-actions">
                <?php echo CHtml::htmlButton($modPemesanan->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php /*echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.pesanambulansT.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));*/ ?>
    	           <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'id'=>'resetbtn')); ?>								
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
        
$('#resetbtn').click(function(){
    window.location = '<?php echo Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/Pemesanan'); ?>';
});

function clearDataPasien()
{
    $("#<?php echo CHtml::activeId($modPemesanan, 'pasien_id') ?>").val('');
    $("#<?php echo CHtml::activeId($modPemesanan, 'norekammedis') ?>").val('');
    $("#<?php echo CHtml::activeId($modPemesanan, 'pendaftaran_id') ?>").val('');
}
</script>

<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPasien',
    'options' => array(
        'title' => 'Pencarian Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPasien = new PasienM('search');
$modPasien->unsetAttributes();
if (isset($_GET['PasienM'])) {
    $modPasien->attributes = $_GET['PasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'pasien-m-grid',
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modPasien->search(),
    'filter' => $modPasien,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#AMPesanambulansT_norekammedis\").val(\"$data->no_rekam_medik\");
                                                          $(\"#AMPesanambulansT_namapasien\").val(\"$data->nama_pasien\");
                                                          $(\"#AMPesanambulansT_alamattujuan\").val(\"$data->alamat_pasien\");
                                                          $(\"#AMPesanambulansT_nomobile\").val(\"$data->no_mobile_pasien\");
                                                          $(\"#AMPesanambulansT_notelepon\").val(\"$data->no_telepon_pasien\");
                                                          $(\"#dialogPasien\").dialog(\"close\");    
                                                "))',
        ),
        'no_rekam_medik',
        'nama_pasien',
        'alamat_pasien',
       
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>