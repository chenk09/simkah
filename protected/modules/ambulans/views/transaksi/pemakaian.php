<fieldset>
    <legend class="rim2">Pemakaian Ambulans</legend>
</fieldset>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pemakaianambulans-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modPemakaian); ?>
    
            <?php echo CHtml::activeHiddenField($modPemakaian,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo CHtml::activeHiddenField($modPemakaian,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo CHtml::activeHiddenField($modPemakaian,'pesanambulans_t',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo CHtml::hiddenField('kelaspelayanan_id','',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
    <table>
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemakaian, 'norekammedis', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $modPemakaian,
                                'attribute' => 'norekammedis',
                                'value' => '',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);

                                            return false;
                                        }',
                                    'select' => 'js:function( event, ui ) {
                                              $("#' . CHtml::activeId($modPemakaian, 'pasien_id') . '").val(ui.item.pasien_id);
                                              $("#' . CHtml::activeId($modPemakaian, 'namapasien') . '").val(ui.item.nama_pasien);
                                              $("#' . CHtml::activeId($modPemakaian, 'noidentitas') . '").val(ui.item.no_identitas_pasien);
                                              $("#' . CHtml::activeId($modPemakaian, 'pendaftaran_id') . '").val(ui.item.pendaftaran_id);
                                              $("#' . CHtml::activeId($modPemakaian, 'alamattujuan') . '").val(ui.item.alamat_pasien);
                                              $("#' . CHtml::activeId($modPemakaian, 'rt_rw') . '").val(ui.item.rt_rw);
                                              $("#' . CHtml::activeId($modPemakaian, 'nomobile') . '").val(ui.item.no_mobile);
                                              $("#' . CHtml::activeId($modPemakaian, 'notelepon') . '").val(ui.item.no_telepon);
                                              $("#kelaspelayanan_id").val(ui.item.kelaspelayanan_id);
//                                              setRuanganPemesan(ui.item.instalasiasal_id,ui.item.ruanganasal_id);
                                          }',
                                ), 'tombolDialog'=>array('idDialog'=>'dialogPasien'),
                                'htmlOptions'=>array(
                                    'class'=>'span3',
                                    'placeholder'=>'Ketikan No Rekam Medis',
                                    ),
                            ));
                        ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPemakaian,'noidentitas',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modPemakaian,'namapasien',array('class'=>'span3', 'onchange'=>'clearDataPasien();' ,'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>

                <?php echo $form->textFieldRow($modPemakaian,'tempattujuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo $form->textAreaRow($modPemakaian,'alamattujuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPemakaian,'kelurahan_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php //echo $form->textFieldRow($modPemakaian,'rt_rw',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemakaian,'rt_rw', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('CMaskedTextField',array(
                                'model'=>$modPemakaian,
                                'attribute'=>'rt_rw',
                                //'name'=>'date',
                                'mask'=>'99.99',
                                'htmlOptions'=>array(
                                    'style'=>'width:50px;'
                                ),
                            ));
                        ?>
                        <?php echo $form->error($modPemakaian, 'rt_rw'); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPemakaian,'nomobile',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modPemakaian,'notelepon',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
            <td width="50%">
                <?php //echo $form->textFieldRow($modPemakaian,'supir_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemakaian, 'supir_id', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPemakaian,'supir_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textField($modPemakaian,'supir_nama',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php
                            echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogSupir').dialog('open');",
                                'id' => 'btnAddSupir', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                'rel' => 'tooltip', 'title' => 'Klik untuk mencari ' . $modPemakaian->getAttributeLabel('supir_id')))
                        ?>
                        <?php echo $form->error($modPemakaian, 'supir_id'); ?>
                    </div>
                </div>

                <?php //echo $form->textFieldRow($modPemakaian,'pelaksana_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemakaian, 'pelaksana_id', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPemakaian,'pelaksana_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textField($modPemakaian,'pelaksana_nama',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php
                            echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogPelaksana').dialog('open');",
                                'id' => 'btnAddPelaksana', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                'rel' => 'tooltip', 'title' => 'Klik untuk mencari ' . $modPemakaian->getAttributeLabel('pelaksana_id')))
                        ?>
                        <?php echo $form->error($modPemakaian, 'paramedis2_id'); ?>
                    </div>
                </div>

                <?php //echo $form->textFieldRow($modPemakaian,'paramedis1_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPemakaian, 'paramedis1_id', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPemakaian,'paramedis1_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textField($modPemakaian,'paramedis1_nama',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php
                            echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogParamedis').dialog('open');$('#dialogParamedis #paramedisKe').val(1);",
                                'id' => 'btnAddParamedis1', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                'rel' => 'tooltip', 'title' => 'Klik untuk mencari ' . $modPemakaian->getAttributeLabel('paramedis1_id')))
                        ?>
                        <?php echo $form->error($modPemakaian, 'paramedis2_id'); ?>
                    </div>
                </div>

                <div class="control-group ">
                    <?php echo $form->labelEx($modPemakaian, 'paramedis2_id', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPemakaian,'paramedis2_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textField($modPemakaian,'paramedis2_nama',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php
                            echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogParamedis').dialog('open');$('#dialogParamedis #paramedisKe').val(2);",
                                'id' => 'btnAddParamedis2', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                'rel' => 'tooltip', 'title' => 'Klik untuk mencari ' . $modPemakaian->getAttributeLabel('paramedis2_id')))
                        ?>
                        <?php echo $form->error($modPemakaian, 'paramedis2_id'); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modPemakaian,'tglpemakaianambulans',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemakaian, 'tglpemakaianambulans', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modPemakaian,
                                                            'attribute'=>'tglpemakaianambulans',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                //'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modPemakaian,'tglkembaliambulans',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemakaian, 'tglkembaliambulans', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modPemakaian,
                                                            'attribute'=>'tglkembaliambulans',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>false,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPemakaian,'namapj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->dropDownListRow($modPemakaian,'hubunganpj', HubunganKeluarga::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'empty'=>'-- Pilih --')); ?>
                <?php echo $form->textAreaRow($modPemakaian,'alamatpj',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->textAreaRow($modPemakaian,'untukkeperluan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($modPemakaian, 'ruangan_id', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('instalasi', $instalasi, CHtml::listData($modInstalasi, 'instalasi_id', 'instalasi_nama'),
                                                        array('empty' =>'-- Instalasi --',
                                                              'ajax'=>array('type'=>'POST',
                                                                            'url'=>  CController::createUrl('dynamicRuangan'),
                                                                            'update'=>'#AMPemakaianambulansT_ruangan_id',),'class'=>'span2')); ?>
                        <?php echo CHtml::activeDropDownList($modPemakaian, 'ruangan_id',  CHtml::listData(RuanganM::model()->getRuanganByInstalasi($instalasi),'ruangan_id','ruangan_nama'),array('empty' =>'-- Ruangan --','class'=>'span2')); ?>
                    </div>
                </div>
            </td>
            <td>
                <fieldset>
                    <legend>Kendaraan</legend>
                        <div class="control-group ">
                            <?php echo $form->labelEx($modPemakaian, 'mobilambulans_id', array('class' => 'control-label')) ?>
                            <div class="controls">
                                <?php echo $form->hiddenField($modPemakaian,'mobilambulans_id',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                <?php echo $form->textField($modPemakaian,'mobilambulans_nama',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                <?php
                                    echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogKendaraan').dialog('open');",
                                        'id' => 'btnAddParamedis2', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                        'rel' => 'tooltip', 'title' => 'Klik untuk mencari ' . $modPemakaian->getAttributeLabel('mobilambulans_id')))
                                ?>
                                <?php echo $form->error($modPemakaian, 'mobilambulans_id'); ?>
                            </div>
                        </div>

                        <div class="control-group ">
                            <?php echo $form->labelEx($modPemakaian, 'kmawal', array('class' => 'control-label')) ?>
                            <div class="controls">
                                <?php echo $form->textField($modPemakaian,'kmawal',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                <?php echo $form->error($modPemakaian, 'kmawal'); ?> s/d <span style="font-size:11px;"><?php echo $modPemakaian->getAttributeLabel('kmakhir'); ?></span>
                                <?php echo $form->textField($modPemakaian,'kmakhir',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                <?php echo $form->error($modPemakaian, 'kmakhir'); ?>
                            </div>
                        </div>
                        <?php //echo $form->textFieldRow($modPemakaian,'mobilambulans_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($modPemakaian,'kmawal',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($modPemakaian,'kmakhir',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textFieldRow($modPemakaian,'jmlbbmliter',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>  
                </fieldset>
            </td>
        </tr>
    </table>
            
    <fieldset>
        <legend>Tarif Ambulans
                <?php
                    echo CHtml::htmlButton('<i class="icon-search icon-white"></i>', array('class' => 'btn btn-primary', 'onclick' => "$('#dialogTarif').dialog('open');",
                        'id' => 'btnAddParamedis2', 'onkeypress' => "return $(this).focusNextInputField(event)",
                        'rel' => 'tooltip', 'title' => 'Klik untuk mencari Tarif Ambulans'))
                ?>
        </legend>
            <?php //echo $form->textFieldRow($modPemakaian,'jumlahkm',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($modPemakaian,'tarifperkm',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($modPemakaian,'totaltarifambulans',array('class'=>'span1 number', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <table id="tblTarifAmbulans" class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="4" style='vertical-align:middle;text-align:center;'>Tujuan</th>
                    <th rowspan="2" style='vertical-align:middle;text-align:center;'>Jumlah Km</th>
                    <th rowspan="2" style='vertical-align:middle;text-align:center;'>Tarif / Km</th>
                    <th rowspan="2" style='vertical-align:middle;text-align:center;'>Total Tarif</th>
                </tr>
                <tr>
                    <th>Propinsi</th>
                    <th>Kabupaten</th>
                    <th>Kecamatan</th>
                    <th>Kelurahan</th>
                </tr>
            </thead>
            <tbody>
                <?php for($i=0;$i<count($tarif['tarifAmbulans']);$i++) : ?>
                <?php if(!empty($tarif['tarifAmbulans'][$i])){ ?>
                <tr>
                    <td><input type="text" value="<?php echo $tarif['propinsi'][$i]; ?>" name="tarif[propinsi][]" class="span2" /></td>
                    <td><input type="text" value="<?php echo $tarif['kabupaten'][$i]; ?>" name="tarif[kabupaten][]" class="span2" /></td>
                    <td><input type="text" value="<?php echo $tarif['kecamatan'][$i]; ?>" name="tarif[kecamatan][]" class="span2" /></td>
                    <td><input type="text" value="<?php echo $tarif['kelurahan'][$i]; ?>" name="tarif[kelurahan][]" class="span2" /></td>
                    <td><input type="text" value="<?php echo $tarif['jmlKM'][$i]; ?>" name="tarif[jmlKM][]" class="span1 number" />
                        <input type="hidden" value="<?php echo $tarif['daftartindakanId'][$i]; ?>" name="tarif[daftartindakanId][]" class="span1 number" /></td>
                    <td><input type="text" value="<?php echo $tarif['tarifKM'][$i]; ?>" name="tarif[tarifKM][]" class="span1 currency" /></td>
                    <td><input type="text" value="<?php echo $tarif['tarifAmbulans'][$i]; ?>" name="tarif[tarifAmbulans][]" class="span2 currency" /></td>
                </tr>
                <?php } ?>
                <?php endfor;?>
            </tbody>
        </table>
    </fieldset>
    
    <?php $this->renderPartial('_formPemakaianBahan',array()); ?>
    <br>
    <?php
    //FORM REKENING
        $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
            array(
                'form'=>$form,
                'modRekenings'=>$modRekenings,
            )
        );
    ?>
    <div class="form-actions">
                        <?php echo CHtml::htmlButton($modPemakaian->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.pemakaianambulansT.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
        <?php
            $content = $this->renderPartial('../tips/transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
    </div>

<?php $this->endWidget(); ?>
    
<script type="text/javascript">
//$('.number').each(function(){this.value = formatNumber(this.value)});
//$('.currency').each(function(){this.value = formatNumber(this.value)});
function clearDataPasien()
{

    $("#<?php echo CHtml::activeId($modPemakaian, 'noidentitas') ?>").val('');
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>

<?php 
//========= Dialog buat daftar paramedis  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogParamedis',
    'options'=>array(
        'title'=>'Daftar Paramedis',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_daftarParamedis');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar paramedis =============================
//========= Dialog buat daftar supir  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogSupir',
    'options'=>array(
        'title'=>'Daftar Supir',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_daftarSupir');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar supir =============================
//========= Dialog buat daftar pelaksana  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPelaksana',
    'options'=>array(
        'title'=>'Daftar Pelaksana',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_daftarPelaksana');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar pelaksana =============================
//========= Dialog buat daftar ambulans  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogKendaraan',
    'options'=>array(
        'title'=>'Daftar Kendaraan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_daftarKendaraan');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar ambulans =============================
//========= Dialog buat daftar tarif ambulans  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogTarif',
    'options'=>array(
        'title'=>'Daftar Tarif Ambulans',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>500,
        'resizable'=>false,
    ),
));
    $this->renderPartial('_daftarTarifAmbulans');

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end daftar tarif ambulans =============================
?> 

<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPasien',
    'options' => array(
        'title' => 'Pencarian Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPasien = new PasienM('search');
$modPasien->unsetAttributes();
if (isset($_GET['PasienM'])) {
    $modPasien->attributes = $_GET['PasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'pasien-m-grid',
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modPasien->search(),
    'filter' => $modPasien,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#AMPemakaianambulansT_norekammedis\").val(\"$data->no_rekam_medik\");
                                                          $(\"#AMPemakaianambulansT_namapasien\").val(\"$data->nama_pasien\");
                                                          $(\"#AMPemakaianambulansT_pasien_id\").val(\"$data->pasien_id\");
                                                          $(\"#AMPemakaianambulansT_alamattujuan\").val(\"$data->alamat_pasien\");
                                                          $(\"#AMPemakaianambulansT_nomobile\").val(\"$data->no_mobile_pasien\");
                                                          $(\"#AMPemakaianambulansT_notelepon\").val(\"$data->no_telepon_pasien\");
                                                          $(\"#kelaspelayanan_id\").val(\"$data->kelas\");
                                                          $(\"#dialogPasien\").dialog(\"close\");    
                                                "))',
        ),
        'no_rekam_medik',
        'nama_pasien',
        'alamat_pasien',
       
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>