<?php

class AMPemakaianambulansT extends PemakaianambulansT
{
    public $supir_nama;
    public $pelaksana_nama;
    public $paramedis1_nama;
    public $paramedis2_nama;
    public $mobilambulans_nama;
    public $ruangan_nama;
    public $tglAwal;
    public $tglAkhir;
    public $tick, $data, $jumlah;
    public $KMawalKMakhir;
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
        
    public function searchPemakaian()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('pemakaianambulans_id',$this->pemakaianambulans_id);
            $criteria->compare('batalpakaiambulans_id',$this->batalpakaiambulans_id);
            $criteria->compare('mobilambulans_id',$this->mobilambulans_id);
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('pesanambulans_t',$this->pesanambulans_t);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('LOWER(tglpemakaianambulans)',strtolower($this->tglpemakaianambulans),true);
            $criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
            $criteria->compare('LOWER(norekammedis)',strtolower($this->norekammedis),true);
            $criteria->compare('LOWER(namapasien)',strtolower($this->namapasien),true);
            $criteria->compare('LOWER(tempattujuan)',strtolower($this->tempattujuan),true);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('LOWER(alamattujuan)',strtolower($this->alamattujuan),true);
            $criteria->compare('LOWER(rt_rw)',strtolower($this->rt_rw),true);
            $criteria->compare('LOWER(nomobile)',strtolower($this->nomobile),true);
            $criteria->compare('LOWER(notelepon)',strtolower($this->notelepon),true);
            $criteria->compare('LOWER(namapj)',strtolower($this->namapj),true);
            $criteria->compare('LOWER(hubunganpj)',strtolower($this->hubunganpj),true);
            $criteria->compare('LOWER(alamatpj)',strtolower($this->alamatpj),true);
            $criteria->compare('LOWER(mobil.nopolisi)',strtolower($this->nopolisi),true);
            $criteria->compare('supir_id',$this->supir_id);
            $criteria->compare('pelaksana_id',$this->pelaksana_id);
            $criteria->compare('LOWER(paramedis1_id)',strtolower($this->paramedis1_id),true);
            $criteria->compare('LOWER(paramedis2_id)',strtolower($this->paramedis2_id),true);
            $criteria->compare('kmawal',$this->kmawal);
            $criteria->compare('kmakhir',$this->kmakhir);
            $criteria->compare('jmlbbmliter',$this->jmlbbmliter);
            $criteria->compare('jumlahkm',$this->jumlahkm);
            $criteria->compare('tarifperkm',$this->tarifperkm);
            $criteria->compare('totaltarifambulans',$this->totaltarifambulans);
            $criteria->compare('LOWER(tglkembaliambulans)',strtolower($this->tglkembaliambulans),true);
            $criteria->compare('LOWER(untukkeperluan)',strtolower($this->untukkeperluan),true);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

            $criteria->with=array('mobil','supir');
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
        
    public function criteriaLaporan()
    {
    $criteria=new CDbCriteria;

//                $criteria->with = array('supir','paramedis1','ruanganpemesan','mobil');
//                $criteria->order = 'mobil.jeniskendaraan, mobil.nopolisi';

    $criteria->addBetweenCondition('t.tglpemakaianambulans', $this->tglAwal, $this->tglAkhir);

            return $criteria;
    }
        
    public function searchLaporan()
    {
        return new CActiveDataProvider($this, array(
                'criteria'=>$this->criteriaLaporan(),
        ));
    }        
                
    public function searchLaporanPrint()
    {
        return new CActiveDataProvider($this, array(
                'criteria'=>$this->criteriaLaporan(),
                                        'pagination'=>false,
        ));
    }
                
    public function getTotal($kolom)
    {
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglpemakaianambulans', $this->tglAwal, $this->tglAkhir);
        $criteria->select = "SUM($kolom)";
        $total = $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        return MyFunction::formatNumber($total);
    }
                
    public function searchGrafik()
    {
        $criteria=new CDbCriteria;

                        $criteria->join = 'JOIN mobilambulans_m ON t.mobilambulans_id=mobilambulans_m.mobilambulans_id';
                        $criteria->select = 'COUNT(norekammedis) AS jumlah, mobilambulans_m.nopolisi AS data';
                        $criteria->group = 't.mobilambulans_id, mobilambulans_m.nopolisi';
    //                                $criteria->group = 'mobil.mobilambulans_id, t.pemakaianambulans_id, mobil.inventarisaset_id, mobil.mobilambulans_kode, mobil.nopolisi, mobil.jeniskendaraan, mobil.isibbmliter, mobil.kmterakhirkend, mobil.photokendaraan, mobil.hargabbmliter, mobil.formulajasars, mobil.formulajasaba, mobil.formulajasapel, mobil.mobilambulans_aktif';

        $criteria->addBetweenCondition('tglpemakaianambulans', $this->tglAwal, $this->tglAkhir);

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                                        'pagination'=>false,
        ));
    }
}
?>
