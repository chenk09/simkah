<?php

/**
 * This is the model class for table "obatalkes_m".
 *
 * The followings are the available columns in table 'obatalkes_m':
 * @property integer $obatalkes_id
 * @property integer $lokasigudang_id
 * @property integer $therapiobat_id
 * @property integer $pbf_id
 * @property integer $generik_id
 * @property integer $satuanbesar_id
 * @property integer $sumberdana_id
 * @property integer $satuankecil_id
 * @property integer $jenisobatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $kemasanbesar
 * @property double $harganetto
 * @property double $hargajual
 * @property double $discount
 * @property string $tglkadaluarsa
 * @property integer $minimalstok
 * @property string $formularium
 * @property boolean $discountinue
 * @property boolean $obatalkes_aktif
 */
class AMObatAlkesM extends ObatalkesM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}