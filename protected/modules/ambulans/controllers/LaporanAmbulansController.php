<?php

class LaporanAmbulansController extends SBaseController
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
                public function actionPemakaianambulans()
	{
                    $model = new AMPemakaianambulansT('search');
                    $model->tglAwal  = date('Y-m-d 00:00:00');
                    $model->tglAkhir  = date('Y-m-d 23:59:59');
                    if(isset($_GET['AMPemakaianambulansT'])){
                        $format = new CustomFormat();
                        $model->attributes = $_GET['AMPemakaianambulansT'];
                        $model->tglAwal  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAwal']);
                        $model->tglAkhir  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAkhir']);
                    }
                    $this->render('pemakaianAmbulansT/index',array('model'=>$model));
	}
        
                public function actionPrintPemakaianAmbulans()
                {
                    $model = new AMPemakaianambulansT('search');
                    $judulLaporan = 'Laporan Pemakaian Ambulans';

                    //Data Grafik
                    $data['title'] = 'Grafik Pemakaian Ambulans';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['AMPemakaianambulansT'])) {
                        $model->attributes = $_REQUEST['AMPemakaianambulansT'];
                        $format = new CustomFormat();
                        $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['AMPemakaianambulansT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['AMPemakaianambulansT']['tglAkhir']);
                    }

                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'pemakaianAmbulansT/Print';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);            
                }
                
                public function actionFrameGrafikPemakaianAmbulans() 
                {
                    $this->layout = '//layouts/frameDialog';

                    $model = new AMPemakaianambulansT('search');
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Presensi';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['AMPemakaianambulansT'])) {
                        $format = new CustomFormat();
                        $model->attributes = $_GET['AMPemakaianambulansT'];
                        $model->tglAwal  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAwal']);
                        $model->tglAkhir  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAkhir']);
                    }
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                    ));
                }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
/* =================================== Function Laporan ======================================= */                
                protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
                    $format = new CustomFormat();
                    $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

                    if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
                        $this->layout = '//layouts/printWindows';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($caraPrint == 'EXCEL') {
                        $this->layout = '//layouts/printExcel';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($_REQUEST['caraPrint'] == 'PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('', $ukuranKertasPDF);
                        $mpdf->useOddEven = 2;
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
                        $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
                        $mpdf->Output();
                    }
                }

                protected function parserTanggal($tgl){
                    $tgl = explode(' ', $tgl);
                    $result = array();
                    foreach ($tgl as $row){
                        if (!empty($row)){
                            $result[] = $row;
                        }
                    }
                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

                }
}