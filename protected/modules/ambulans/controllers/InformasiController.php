<?php

class InformasiController extends SBaseController
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionPemakaian()
	{
            $modPemakaian = new AMPemakaianambulansT('search');
            $modPemakaian->tglAwal  = date('d M Y 00:00:00');
            $modPemakaian->tglAkhir  = date('d M Y 23:59:59');
            if(isset($_GET['AMPemakaianambulansT'])){
                $modPemakaian->unsetAttributes();
                $format = new CustomFormat();
                $modPemakaian->attributes = $_GET['AMPemakaianambulansT'];
                $modPemakaian->tglAwal  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAwal']);
                $modPemakaian->tglAkhir  = $format->formatDateTimeMediumForDB($_GET['AMPemakaianambulansT']['tglAkhir']);
            }
            $this->render('pemakaian',array('modPemakaian'=>$modPemakaian));
	}

	public function actionPemesanan()
	{
            $modPemesanan = new AMPesanambulansT('search');
            $modPemesanan->tglAwal  = date('d M Y 00:00:00');
            $modPemesanan->tglAkhir  = date('d M Y 23:59:59');
            if(isset($_GET['AMPesanambulansT'])){
                $modPemesanan->unsetAttributes();
                $format = new CustomFormat();
                $modPemesanan->attributes = $_GET['AMPesanambulansT'];
                $modPemesanan->tglAwal  = $format->formatDateTimeMediumForDB($_GET['AMPesanambulansT']['tglAwal']);
                $modPemesanan->tglAkhir  = $format->formatDateTimeMediumForDB($_GET['AMPesanambulansT']['tglAkhir']);
            }
            $this->render('pemesanan',array('modPemesanan'=>$modPemesanan));
	}

	public function actionTarif()
	{
            $modTarif = new AMTarifambulansM('search');
            $this->render('tarif',array('modTarif'=>$modTarif));
	}
        
        public function actionDaftarPasienRI()
        {
            $this->pageTitle = Yii::app()->name." - Pasien Rawat Inap";
            $format = new CustomFormat();
            $model = new AMPasienrawatinapV;
            $model->tglAwal = date("Y-m-d").' 00:00:00';
            $model->tglAkhir = date('Y-m-d h:i:s');

            if(isset ($_REQUEST['AMPasienrawatinapV'])){
                $model->attributes=$_REQUEST['AMPasienrawatinapV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AMPasienrawatinapV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AMPasienrawatinapV']['tglAkhir']);
                $model->ceklis = $_REQUEST['AMPasienrawatinapV']['ceklis'];
           }
            
           $this->render('daftarPasienRI',array('model'=>$model));
        }
        
        public function actionDaftarPasienRD()
        {
            $this->pageTitle = Yii::app()->name." - Daftar Pasien";
            $format = new CustomFormat();
            $model = new AMInfoKunjunganRDV;
            $model->tglAwal = date("Y-m-d").' 00:00:00';
            $model->tglAkhir = date('Y-m-d h:i:s');
            if(isset ($_REQUEST['AMInfoKunjunganRDV'])){
                $model->attributes=$_REQUEST['AMInfoKunjunganRDV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AMInfoKunjunganRDV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AMInfoKunjunganRDV']['tglAkhir']);
                $model->ceklis = $_REQUEST['AMInfoKunjunganRDV']['ceklis'];
            }

            $this->render('daftarPasienRD',array('model'=>$model));
        }
        
        public function actionDaftarPasienRJ()
        {
            $model = new AMInfokunjunganrjV('searchDaftarPasien');
            $model->unsetAttributes();
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
            //$model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if(isset($_GET['AMInfokunjunganrjV'])){
                $model->attributes = $_GET['AMInfokunjunganrjV'];
                $format = new CustomFormat();
                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['AMInfokunjunganrjV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AMInfokunjunganrjV']['tglAkhir']);
                $model->ceklis = $_GET['AMInfokunjunganrjV']['ceklis'];
            }
            
            $this->render('daftarPasienRJ',array('model'=>$model));
        }
        
        public function actionDaftarPasienPulang()
        {
            $this->pageTitle = Yii::app()->name." - Daftar Pasien Pulang";
            $model = new AMPasienpulangrddanriV;
            $model->unsetAttributes();
            $model->tglAwal = date("Y-m-d").' 00:00:00';
            $model->tglAkhir = date('Y-m-d h:i:s');
            if(isset ($_GET['AMPasienpulangrddanriV'])){
                $model->attributes=$_GET['AMPasienpulangrddanriV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AMPasienpulangrddanriV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AMPasienpulangrddanriV']['tglAkhir']);
                $model->ceklis = $_GET['AMPasienpulangrddanriV']['ceklis'];
            }
            $this->render('daftarPasienPulang',array('model'=>$model));
        }

        public function actionBatalPakai()
        {
            if(Yii::app()->request->isAjaxRequest){
                $result=array();
                $idPemakaian = $_POST['idPemakaian'];
                $idPemesanan = $_POST['idPemesanan'];
                if(AMPemakaianambulansT::model()->deleteByPk($idPemakaian)){
//                    $newIdPakai = AMPemakaianambulansT::model()->findByAttributes(array('pesanambulans_t'=>$idPemesanan))->pemakaianambulans_id;
                    //$newIdPakai = (!empty($newIdPakai)) ? $newIdPakai : null;
                    AMPesanambulansT::model()->updateByPk($idPemesanan, array('pemakaianambulans_id'=>$idPemakaian));
                    $result['status'] = 'berhasil';
                } else {
                    $result['status'] = 'gagal';
                }
                echo CJSON::encode($result);
            }
            Yii::app()->end();
        }
        
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}