<?php

class TransaksiController extends SBaseController
{
    protected $successSavePemakaianBahan = true;
    protected $successSaveTindakanPelayanan = true;
    
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionPemakaian($idPemesanan='',$idPendaftaran='')
	{
            $modPemakaian = new AMPemakaianambulansT;
            $modPemakaian->tglpemakaianambulans = date('d M Y H:i:s');
            $modPemakaian->tglkembaliambulans = date('d M Y H:i:s');
            $modPasien = new PasienM;
            $modInstalasi = InstalasiM::model()->findAllByAttributes(array('instalasi_aktif'=>true),array('order'=>'instalasi_nama'));
            $instalasi = '';
            $tarif = array();
            $tarif['tarifAmbulans'][] = null;
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
		
            if(!empty($idPemesanan)){
                $modPemakaian = $this->setDataPemakaianFromPemesanan($idPemesanan);
                $instalasi = RuanganM::model()->findByPk($modPemakaian->ruangan_id)->instalasi_id;
            }
            
            if(!empty($idPendaftaran)){
                $modPemakaian = $this->setDataPemakaianFromPendaftaran($idPendaftaran);
                $instalasi = RuanganM::model()->findByPk($modPemakaian->ruangan_id)->instalasi_id;
            }
            
            if(isset($_POST['AMPemakaianambulansT'])){
                if(isset($_POST['tarif'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        foreach($_POST['tarif']['tarifAmbulans'] as $i=>$tarifAmbulans){
                            $tarif['tarifAmbulans'][$i] = $tarifAmbulans;
                            $tarif['tarifKM'][$i] = $_POST['tarif']['tarifKM'][$i];
                            $tarif['jmlKM'][$i] = $_POST['tarif']['jmlKM'][$i];
                            $tarif['kelurahan'][$i] = $_POST['tarif']['kelurahan'][$i];
                            $tarif['kecamatan'][$i] = $_POST['tarif']['kecamatan'][$i];
                            $tarif['kabupaten'][$i] = $_POST['tarif']['kabupaten'][$i];
                            $tarif['propinsi'][$i] = $_POST['tarif']['propinsi'][$i];
                            $tarif['daftartindakanId'][$i] = $_POST['tarif']['daftartindakanId'][$i];

                            //=== set attribute pemakaian ambulans ===//
                            $save = true;
                            $modPemakaian = new AMPemakaianambulansT;
                            $modPemakaian->attributes = $_POST['AMPemakaianambulansT'];
                            $modPemakaian->tarifperkm = $tarif['tarifKM'][$i];
                            $modPemakaian->jumlahkm = $tarif['jmlKM'][$i];
                            $modPemakaian->totaltarifambulans = $tarif['tarifAmbulans'][$i];
//                            $modPemakaian->daftartindakanId = $tarif['daftartindakanId'][$i];
                            $instalasi = $_POST['instalasi'];
                            $format = new CustomFormat();
                            $modPemakaian->tglpemakaianambulans = $format->formatDateTimeMediumForDB($_POST['AMPemakaianambulansT']['tglpemakaianambulans']);
                            if(empty($_POST['AMPemakaianambulansT']['tglkembaliambulans'])){
                                $modPemakaian->tglkembaliambulans = null;
                            }else{
                                $modPemakaian->tglkembaliambulans = $format->formatDateTimeMediumForDB($_POST['AMPemakaianambulansT']['tglkembaliambulans']);
                            }
                            
                            //=== save pemakaian ambulans ===//
                            if($modPemakaian->validate()){
//                                echo "<pre>";
//                                echo print_r($modPemakaian->getAttributes());exit;
                                $save = $save && $modPemakaian->save();
                                if(!empty($modPemakaian->pendaftaran_id) && $save){
                                    $modPendaftaran = PendaftaranT::model()->findByPk($modPemakaian->pendaftaran_id);
                                    $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                                    $tindakanPel = $this->saveTindakanPelayanan($modPasien, $modPendaftaran, $modPemakaian,$_POST['RekeningakuntansiV']);
                                }
                                if(!empty($idPemesanan)){
                                    AMPesanambulansT::model()->updateByPk($idPemesanan, array('pemakaianambulans_id'=>$modPemakaian->pemakaianambulans_id));
                                }
                            } else {
                                $save = false;
                            }
                        }
                        //=== simpan pemakaian obat alkes ===//
                        if(!empty($modPemakaian->pendaftaran_id)){
                            $modPendaftaran = PendaftaranT::model()->findByPk($modPemakaian->pendaftaran_id);
                            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                            if(isset($_POST['pemakaianBahan']))
                                $pemakaianObat = $this->savePemakaianBahan($modPendaftaran, $_POST['pemakaianBahan']);
                        }
                        //=== commit or rollback ===//
                        if($save && $this->successSavePemakaianBahan && $this->successSaveTindakanPelayanan){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                            //=== mengosongkan nilai attribute pemakaian ambulans ===//
//                            $modPemakaian = new AMPemakaianambulansT;
//                            $modPemakaian->tglpemakaianambulans = date('d M Y H:i:s');
//                            $tarif = array();
//                            $tarif['tarifAmbulans'][] = null;
                            $this->redirect(array('Pemakaian'));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                
            }
            
            $this->render('pemakaian',array('modPemakaian'=>$modPemakaian,
                                            'modPasien'=>$modPasien,
                                            'modInstalasi'=>$modInstalasi,
                                            'instalasi'=>$instalasi,
                                            'tarif'=>$tarif));
	}
        
        protected function setDataPemakaianFromPendaftaran($idPendaftaran)
        {
            $modPemakaian = new AMPemakaianambulansT;
            $modPendaftaran = PendaftaranT::model()->with('pasien')->findByPk($idPendaftaran);
            $modPemakaian->tglpemakaianambulans = date('d M Y H:i:s');
            $modPemakaian->pasien_id = $modPendaftaran->pasien_id;
            $modPemakaian->namapasien = $modPendaftaran->pasien->nama_pasien;
            $modPemakaian->nomobile = $modPendaftaran->pasien->no_mobile_pasien;
            $modPemakaian->notelepon = $modPendaftaran->pasien->no_telepon_pasien;
            $modPemakaian->norekammedis = $modPendaftaran->pasien->no_rekam_medik;
            $modPemakaian->noidentitas = $modPendaftaran->pasien->no_identitas_pasien;
            $modPemakaian->tempattujuan = '';
            $modPemakaian->alamattujuan = $modPendaftaran->pasien->alamat_pasien;
            $modPemakaian->kelurahan_nama = $modPendaftaran->pasien->kelurahan->kelurahan_nama;
            $modPemakaian->rt_rw = $modPendaftaran->pasien->rt.'/'.$modPendaftaran->pasien->rw;
            $modPemakaian->tglpemakaianambulans = null;
            $modPemakaian->pesanambulans_t = null;
            $modPemakaian->pendaftaran_id = $idPendaftaran;
            $modPemakaian->ruangan_id = $modPendaftaran->ruangan_id;
            
            return $modPemakaian;
        }
        
        protected function setDataPemakaianFromPemesanan($idPemesanan)
        {
            $modPemakaian = new AMPemakaianambulansT;
            $modPemesanan = AMPesanambulansT::model()->findByPk($idPemesanan);
            $modPemakaian->pasien_id = $modPemesanan->pasien_id;
            $modPemakaian->namapasien = $modPemesanan->namapasien;
            $modPemakaian->nomobile = $modPemesanan->nomobile;
            $modPemakaian->notelepon = $modPemesanan->notelepon;
            $modPemakaian->norekammedis = $modPemesanan->norekammedis;
            $modPemakaian->noidentitas = PasienM::model()->findByPk($modPemesanan->pasien_id)->no_identitas_pasien;
            $modPemakaian->tempattujuan = $modPemesanan->tempattujuan;
            $modPemakaian->alamattujuan = $modPemesanan->alamattujuan;
            $modPemakaian->kelurahan_nama = $modPemesanan->kelurahan_nama;
            $modPemakaian->rt_rw = $modPemesanan->rt_rw;
            $modPemakaian->tglpemakaianambulans = $modPemesanan->tglpemakaianambulans;
            $modPemakaian->pesanambulans_t = $idPemesanan;
            $modPemakaian->pendaftaran_id = $modPemesanan->pendaftaran_id;
            $modPemakaian->ruangan_id = $modPemesanan->ruangan_id;
            
            return $modPemakaian;
        }

	public function actionPemesanan()
	{
            $modPemesanan = new AMPesanambulansT;
            $modPemesanan->tglpemesananambulans = date('d M Y H:i:s');
            $modPasien = new PasienM;
            $modInstalasi = InstalasiM::model()->findAllByAttributes(array('instalasi_aktif'=>true),array('order'=>'instalasi_nama'));
            $modPemesanan->create_loginpemakai_id = Yii::app()->session['loginpemakai_id'];
            if(isset($_POST['AMPesanambulansT'])){
                $modPemesanan->attributes = $_POST['AMPesanambulansT'];
                $format = new CustomFormat();
                $modPemesanan->tglpemesananambulans  = $format->formatDateTimeMediumForDB($_POST['AMPesanambulansT']['tglpemesananambulans']);
                if ($_POST['AMPesanambulansT']['tglpemakaianambulans'] < 1) {
                    $modPemesanan->tglpemakaianambulans = null;
                } else {
                    $modPemesanan->tglpemakaianambulans  = $format->formatDateTimeMediumForDB($_POST['AMPesanambulansT']['tglpemakaianambulans']);
                }
                $modPemesanan->create_time = date('Ymd H:i:s');
                $modPemesanan->create_ruangan = Params::RUANGAN_ID_JZ;
                 $modPemesanan->create_loginpemakai_id = 8;
                if($modPemesanan->validate()){
                    if($modPemesanan->save()){
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $modPemesanan = new AMPesanambulansT;
                        $modPemesanan->tglpemesananambulans = date('d M Y H:i:s');
                    } else {
                        Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                    }
                }
            }
            
            $this->render('pemesanan',array('modPemesanan'=>$modPemesanan,
                                            'modPasien'=>$modPasien,
                                            'modInstalasi'=>$modInstalasi));
	}
        
        protected function savePemakaianBahan($modPendaftaran,$pemakaianBahan)
        {
            $valid = true;
            foreach ($pemakaianBahan as $i => $bmhp) {
                $modPakaiBahan[$i] = new AMObatalkesPasienT;
                $modPakaiBahan[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modPakaiBahan[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                $modPakaiBahan[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                $modPakaiBahan[$i]->daftartindakan_id = $bmhp['daftartindakan_id'];
                $modPakaiBahan[$i]->sumberdana_id = $bmhp['sumberdana_id'];
                $modPakaiBahan[$i]->pasien_id = $modPendaftaran->pasien_id;
                $modPakaiBahan[$i]->satuankecil_id = $bmhp['satuankecil_id'];
                $modPakaiBahan[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modPakaiBahan[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                $modPakaiBahan[$i]->obatalkes_id = $bmhp['obatalkes_id'];
                $modPakaiBahan[$i]->pegawai_id = $modPendaftaran->pegawai_id;
                $modPakaiBahan[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                $modPakaiBahan[$i]->shift_id = Yii::app()->user->getState('shift_id');
                $modPakaiBahan[$i]->tglpelayanan = date('Y-m-d H:i:s');
                $modPakaiBahan[$i]->qty_oa = $bmhp['qty'];
                $modPakaiBahan[$i]->hargajual_oa = $bmhp['subtotal'];
                $modPakaiBahan[$i]->harganetto_oa = $bmhp['harganetto'];
                $modPakaiBahan[$i]->hargasatuan_oa = $bmhp['hargasatuan'];

                $valid = $modPakaiBahan[$i]->validate() && $valid;
                if($valid) {
                    $modPakaiBahan[$i]->save();
                    $this->kurangiStok($modPakaiBahan[$i]->qty_oa, $modPakaiBahan[$i]->obatalkes_id);
                    $this->successSavePemakaianBahan = true;
                } else {
                    $this->successSavePemakaianBahan = false;
                }
            }
            
            return $modPakaiBahan;
        }
        
        protected function kurangiStok($qty,$idobatAlkes)
        {
            $sql = "SELECT stokobatalkes_id,qtystok_in,qtystok_out,qtystok_current FROM stokobatalkes_t WHERE obatalkes_id = $idobatAlkes ORDER BY tglstok_in";
            $stoks = Yii::app()->db->createCommand($sql)->queryAll();
            $selesai = false;
                foreach ($stoks as $i => $stok) {
                    if($qty <= $stok['qtystok_current']) {
                        $stok_current = $stok['qtystok_current'] - $qty;
                        $stok_out = $stok['qtystok_out'] + $qty;
                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('qtystok_current'=>$stok_current,'qtystok_out'=>$stok_out));
                        $selesai = true;
                        break;
                    } else {
                        $qty = $qty - $stok['qtystok_current'];
                        $stok_current = 0;
                        $stok_out = $stok['qtystok_out'] + $stok['qtystok_current'];
                        StokobatalkesT::model()->updateByPk($stok['stokobatalkes_id'], array('stok_current'=>$stok_current,'qtystok_out'=>$stok_out));
                    }
                }
        }
        
        protected function kembalikanStok($obatAlkesT)
        {
            foreach ($obatAlkesT as $i => $obatAlkes) {
                $stok = new StokObatalkesT;
                $stok->obatalkes_id = $obatAlkes->obatalkes_id;
                $stok->sumberdana_id = $obatAlkes->sumberdana_id;
                $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $stok->tglstok_in = date('Y-m-d H:i:s');
                $stok->tglstok_out = date('Y-m-d H:i:s');
                $stok->qtystok_in = $obatAlkes->qty_oa;
                $stok->qtystok_out = 0;
                $stok->harganetto_oa = $obatAlkes->harganetto_oa;
                $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
                $stok->discount = $obatAlkes->discount;
                $stok->satuankecil_id = $obatAlkes->satuankecil_id;
                $stok->save();
            }
        }
        
        protected function saveTindakanPelayanan($modPasien,$modPendaftaran,$modPemakaian,$postRekenings = array())
        {
            $modTindakan = new TindakanpelayananT;
            $modTindakan->shift_id = Yii::app()->user->getState('shift_id');
            $modTindakan->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modTindakan->pasien_id = $modPasien->pasien_id;
            $modTindakan->instalasi_id = $modPendaftaran->instalasi_id;
            $modTindakan->daftartindakan_id = $modPemakaian->daftartindakanId;
            $modTindakan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakan->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modTindakan->instalasi_id =  Yii::app()->user->getState('instalasi_id');
            $modTindakan->ruangan_id =  Yii::app()->user->getState('ruangan_id');
            $modTindakan->penjamin_id = $modPendaftaran->penjamin_id;
            $modTindakan->tgl_tindakan = date('Y-m-d H:i:s');
            
            $modTindakan->tarif_tindakan = $modPemakaian->totaltarifambulans;
            $modTindakan->satuantindakan = 'Km';
            $modTindakan->qty_tindakan = $modPemakaian->jumlahkm;
            $modTindakan->tarif_satuan = $modPemakaian->tarifperkm;
                    
            $modTindakan->cyto_tindakan = 0;
            $modTindakan->tarifcyto_tindakan = 0;
            $modTindakan->discount_tindakan = 0;
            $modTindakan->subsidiasuransi_tindakan = 0;
            $modTindakan->subsidipemerintah_tindakan = 0;
            $modTindakan->subsisidirumahsakit_tindakan = 0;
            $modTindakan->iurbiaya_tindakan = 0;
            
            if($modTindakan->save()){
                if(isset($postRekenings)){
                    $modJurnalRekening = TindakanController::saveJurnalRekening();
                    //update jurnalrekening_id
                    $modTindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                    $modTindakan->save();
                    $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakan, 'tm');
                }
                $this->successSaveTindakanPelayanan = true;
            } else {
                $this->successSaveTindakanPelayanan = false;
                Yii::app()->user->setFlash('info','<pre>'.print_r($modTindakan->getErrors(),1).'</pre>');
            }
            
            return $this->successSaveTindakanPelayanan;
        }
                
        public function actionDynamicRuangan()
        {
            $data = RuanganM::model()->findAll('instalasi_id=:instalasi_id AND ruangan_aktif = TRUE order by ruangan_nama', 
                  array(':instalasi_id'=>(int) $_POST['instalasi']));

            $data=CHtml::listData($data,'ruangan_id','ruangan_nama');

            if(empty($data))
            {
                echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Ruangan --'),true);
            }
            else
            {
                echo CHtml::tag('option',array('value'=>''),CHtml::encode('-- Ruangan --'),true);
                foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}