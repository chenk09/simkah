<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'jurnalpengeluarankas-search',
        'type'=>'horizontal',
        'htmlOptions'=>array(
                'onKeyPress'=>'return disableKeyPress(event)'
        ),
        'focus'=>'#',
)); ?>

<table>
    <tr>
        <td width="50%">
            <div class="control-group ">
                <?php echo $form->labelEx($model,'tglAwal',array('class'=>'control-label'));?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAwal',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'readonly'=>true
                            ),
                        ));
                    ?>

                </div>
            </div>
            <div class="control-group ">
                <label class="control-label" for="AKRincianpengeluarankasrekeningV_tglAkhir">Sampai Dengan</label>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAkhir',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'readonly'=>true
                            ),
                        ));
                    ?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'nokaskeluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model,'jenis_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->hiddenField($model,'jenis_id') ?>
                    <?php 
                        $this->widget('MyJuiAutoComplete', array(
                                'model'=>$model,
                                'attribute'=>'jenisNama',
                                'source'=>'js: function(request, response) {
                                               $.ajax({
                                                   url: "'.Yii::app()->createUrl('akuntansi/ActionAutoComplete/jenisPengeluaran').'",
                                                   dataType: "json",
                                                   data: {
                                                       term: request.term,
                                                   },
                                                   success: function (data) {
                                                           response(data);
                                                   }
                                               })
                                            }',
                                 'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 2,
                                       'focus'=> 'js:function( event, ui ) {
                                           $(this).val(ui.item.value);
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) {
                                            $("#AKRincianpengeluarankasrekeningV_jenis_id").val(ui.item.jenispengeluaran_id);
                                            $("#AKRincianpengeluarankasrekeningV_jenisNama").val(ui.item.jenispengeluaran_nama);
                                            return false;
                                        }',
                                ),
                                'htmlOptions'=>array('placeholder'=>'ketik Nama Jenis Pengeluaran','class'=>'reqForm'),
                                'tombolDialog' => array('idDialog' => 'dialogJenisPengeluaran',),
                        )); 
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('id'=>'btn_submit','class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>'addDetail()')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('id'=>'btn_resset','class'=>'btn btn-danger', 'type'=>'reset')); ?>
    
</div>
<?php $this->endWidget();?>  
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogJenisPengeluaran',
    'options'=>array(
        'title'=>'Daftar Jenis Pengeluaran',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modJenisPengeluaran = new JenispengeluaranM('search');
$modJenisPengeluaran->unsetAttributes();
if(isset($_GET['JenispengeluaranM'])) {
    $modJenisPengeluaran->attributes = $_GET['JenispengeluaranM'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'jenispengeluaran-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modJenisPengeluaran->searchJnsPengeluaranInRek(),
	'filter'=>$modJenisPengeluaran,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Jenis Pengeluaran',
                    'name'=>'jenispengeluaran_nama',
                    'value'=>'$data->jenispengeluaran_nama',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'jenispengeluaran_namalain',
                    'value'=>'$data->jenispengeluaran_namalain',
                ),
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                        $(\"#AKRincianpengeluarankasrekeningV_jenis_id\").val(\"$data->jenispengeluaran_id\");
                                        $(\"#AKRincianpengeluarankasrekeningV_jenisNama\").val(\"$data->jenispengeluaran_nama\");
                                        $(\"#dialogJenisPengeluaran\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>