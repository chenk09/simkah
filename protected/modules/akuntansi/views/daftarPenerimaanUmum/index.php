<?php
$this->breadcrumbs=array(
	'Daftar Penerimaan Kas',
);

Yii::app()->clientScript->registerScript('search', "
$('#penerimaan-t-search').submit(function()
{
    $.fn.yiiGridView.update('daftarpenerimaan-m-grid', {
        data: $(this).serialize()
    });
    return false;
});
$('#btn_resset').click(function()
{
    setTimeout(function(){
        $.fn.yiiGridView.update('daftarpenerimaan-m-grid', {
            data: $('#penerimaan-t-search').serialize()
        });    
    }, 1000);
});
");

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'action'=>Yii::app()->createUrl($this->route),
        'method'=>'get',
        'id'=>'penerimaan-t-search',
        'type'=>'horizontal',
    )
);
?>

<legend class="rim2">Informasi Penerimaan Kas</legend>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'daftarpenerimaan-m-grid',
	'dataProvider'=>$modPenerimaan->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'No',
                'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
            ),
            array(
                'header'=>'Tgl Penerimaan',
                'type'=>'raw',
                'value'=>'$data->tglpenerimaan',
            ),
            array(
                'header'=>'No Penerimaan',
                'type'=>'raw',
                'value'=>'$data->nopenerimaan',
            ),
            array(
                'header'=>'Kelompok Transaksi',
                'type'=>'raw',
                'value'=>'$data->kelompoktransaksi',
            ),
            array(
                'header'=>'Jenis Penerimaan',
                'type'=>'raw',
                'value'=>'$data->jenispenerimaan->jenispenerimaan_nama',
                'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-style:italic;'),
                'footer'=>'Jumlah Total',
            ),
            'volume',
//            'satuanvol',
            array(
                'header'=>'Harga',
                'name'=>'totalHarga',
                'value'=>'MyFunction::formatNumber($data->hargasatuan+$data->buktibayar->biayaadministrasi+$data->buktibayar->biayamaterai)',
                'htmlOptions'=>array('style'=>'width:100px;text-align:right'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'sum(totalHarga)',
            ),
            array(
                'header'=>'Total Harga',
                'name'=>'total',
                'value'=>'MyFunction::formatNumber($data->totalharga+$data->buktibayar->biayaadministrasi+$data->buktibayar->biayamaterai)',
                'htmlOptions'=>array('style'=>'width:100px;text-align:right'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'sum(total)',
            ),
            array(
                'header'=>'Keterangan',
                'type'=>'raw',
                'value'=>'$data->keterangan_penerimaan',
                'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                'footer'=>'-',
            ),
            array( 
                'header'=>'Retur / Batal',
                'type'=>'raw',
                'htmlOptions' => array(
                    'style' => 'width: 100px; text-align: center;',
                ),
                'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
                'footer'=>'-',
                'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("returPenerimaanKas/index",array("frame"=>1,"idPenerimaan"=>$data->penerimaanumum_id)) ,array("title"=>"Klik Untuk Meretur Penerimaan Umum","target"=>"iframeRetur", "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");", "rel"=>"tooltip"))',
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>


<legend class="rim">Pencarian</legend>
<table class="table-condensed">
    <tr>
        <td>
            <div class="control-group ">
                <?php $modPenerimaan->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenerimaan->tglAwal, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Tgl Penerimaan Kas','tglPenerimaanKas', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenerimaan,
                                        'attribute'=>'tglAwal',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php $modPenerimaan->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenerimaan->tglAkhir, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenerimaan,
                                        'attribute'=>'tglAkhir',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'minDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenerimaan,'nopenerimaan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <div class="control-group ">
                <?php echo CHtml::label('Jenis Pengeluaran','jenisPengeluaran', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                          echo  $form->dropDownList($modPenerimaan,'jenispenerimaan_id',CHtml::listData(JenispenerimaanM::model()->findAll(),
                                            'jenispenerimaan_id','jenispenerimaan_nama'),array('class'=>'span2','style'=>"width:140px;",'empty'=>'--Pilih--'));
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('id'=>'btn_resset','class'=>'btn btn-danger', 'type'=>'reset')); ?>
                <?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>


<?php $this->endWidget(); ?>

<?php
// ===========================Dialog Retur=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRetur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Retur Penerimaan Umum',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframeRetur" width="100%" height="550">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Retur================================
