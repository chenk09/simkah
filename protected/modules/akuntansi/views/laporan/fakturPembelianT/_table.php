<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = true;
    if (isset($caraPrint)){
//        $data = $model->searchLaporanPrint();
        $data = $model->searchLapFakturPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
//        $data = $model->searchLaporan();
        $data = $model->searchLapFaktur();
         $template = "{pager}{summary}\n{items}";
    }
?>
<div id="div_rekap">
    <legend class="rim"> Table Faktur Pembelian - Rekap</legend>
    <?php $this->widget($table,array(
            'id'=>'rekapLaporanFakturPembelian',
            'dataProvider'=>$data,
            'template'=>$template,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns'=>array('supplier_id'),
            'extraRowColumns'=> array('supplier_id'),
            'columns'=>array(
                array(
                    'header'=>'Nama Supplier',
                    'name'=>'supplier_id',
                    'value'=>'$data->supplier->supplier_nama',
                    'footer'=>'Total',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'No Faktur',
                    'name'=>'nofaktur',
                    'type'=>'raw',
                    'value'=>'$data->nofaktur',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'Tgl Faktur',
                    'name'=>'tglfaktur',
                    'type'=>'raw',
                    'value'=>'$data->tglfaktur',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'Tgl Jatuh Tempo',
                    'name'=>'tgljatuhtempo',
                    'type'=>'raw',
                    'value'=>'$data->tgljatuhtempo',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'footerHtmlOptions'=>array(
                            'colspan'=>3,
                            'style'=>'text-align:right;font-style:italic;'
                    ),
                    'footer'=>'Total',
                ),  
				array(
                    'header'=>'Bruto',
                    'name'=>'totalhargabruto',
                    'value'=>'MyFunction::formatNumber($data->totalhargabruto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totalhargabruto)',
                ),
                array(
                    'header'=>'Discount',
                    'name'=>'jmldiscount',
                    'value'=>'MyFunction::formatNumber($data->jmldiscount)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(jmldiscount)',
                ),
                array(
                    'header'=>'Ppn',
                    'name'=>'totalpajakppn',
                    'value'=>'MyFunction::formatNumber($data->totalpajakppn)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totalpajakppn)',
                ),
                array(
                    'header'=>'Materai',
                    'name'=>'biayamaterai',
                    'value'=>'MyFunction::formatNumber($data->biayamaterai)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(biayamaterai)',
                ),
                array(
                    'header'=>'Netto',
                    'name'=>'totharganetto',
                    'value'=>'MyFunction::formatNumber($data->totharganetto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totharganetto)',
                ),
                array(
                    'header'=>'Total Tagihan',
                    'name'=>'total_tagihan',
                    'value'=>'MyFunction::formatNumber($data->totalhargabruto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_tagihan)',
                ),
                array(
                    'header'=>'Bayar',
                    'name'=>'total_bayar',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->getTotalBayar())',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_bayar)',
                ),
                array(
                    'header'=>'Sisa',
                    'name'=>'total_sisa',
                    'value'=>'MyFunction::formatNumber($data->total_sisa)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_sisa)',
                ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</div>
<div id="div_detail">
    <legend class="rim"> Table Faktur Pembelian - Detail</legend>
     <?php $this->widget($table,array(
            'id'=>'rincianLaporanFakturPembelian',
            'dataProvider'=>$data,
            'template'=>$template,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns'=>array('supplier_id'),
            'extraRowColumns'=> array('supplier_id'),
            'columns'=>array(
                array(
                    'header'=>'Nama Supplier',
                    'name'=>'supplier_id',
                    'value'=>'$data->supplier->supplier_nama',
                    'footer'=>'Total',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'No Faktur',
                    'name'=>'nofaktur',
                    'type'=>'raw',
                    'value'=>'$data->nofaktur',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'Tgl Faktur',
                    'name'=>'tglfaktur',
                    'type'=>'raw',
                    'value'=>'$data->tglfaktur',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                ),
                array(
                    'header'=>'Tgl Jatuh Tempo',
                    'name'=>'tgljatuhtempo',
                    'type'=>'raw',
                    'value'=>'$data->tgljatuhtempo',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'footerHtmlOptions'=>array(
                            'colspan'=>3,
                            'style'=>'text-align:right;font-style:italic;'
                    ),
                    'footer'=>'Total',
                ),  
                array(
                    'header'=>'Bruto',
                    'name'=>'totalhargabruto',
                    'value'=>'MyFunction::formatNumber($data->totalhargabruto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totalhargabruto)',
                ),
                array(
                    'header'=>'Discount',
                    'name'=>'jmldiscount',
                    'value'=>'MyFunction::formatNumber($data->jmldiscount)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(jmldiscount)',
                ),
                array(
                    'header'=>'Ppn',
                    'name'=>'totalpajakppn',
                    'value'=>'MyFunction::formatNumber($data->totalpajakppn)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totalpajakppn)',
                ),
                array(
                    'header'=>'Materai',
                    'name'=>'biayamaterai',
                    'value'=>'MyFunction::formatNumber($data->biayamaterai)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(biayamaterai)',
                ),
                array(
                    'header'=>'Netto',
                    'name'=>'totharganetto',
                    'value'=>'MyFunction::formatNumber($data->totharganetto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(totharganetto)',
                ),
                array(
                    'header'=>'Total Tagihan',
                    'name'=>'total_tagihan',
                    'value'=>'MyFunction::formatNumber($data->totalhargabruto)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_tagihan)',
                ),
                array(
                    'header'=>'Bayar',
                    'name'=>'total_bayar',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->getTotalBayar())',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_bayar)',
                ),
                array(
                    'header'=>'Sisa',
                    'name'=>'total_sisa',
                    'value'=>'MyFunction::formatNumber($data->total_sisa)',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'sum(total_sisa)',
                ),
                array(
                    'header'=>'Print Detail',
                    'name'=>'fakturpembelian_id',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\"icon-print\"></i>", "javascript:printDetail(\'$data->fakturpembelian_id\');", array("rel"=>"tooltip","title"=>"Klik untuk mencetak Detail Laporan Faktur Pembelian"))',
                    'footer'=>'Total',
                    'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:center'),
                    'footerHtmlOptions'=>array(
                        'style'=>'text-align:right;color:white;',
                        'class'=>'currency'
                    ),                            
                    'footer'=>'-',
                ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); ?>
</div>
<?php 
    
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintDetailFakturPembelian');
    $js = <<< JSCRIPT

function printDetail(idFaktur)
   {    
               window.open("${url}/"+$('#search-laporan').serialize()+"&idFaktur="+idFaktur,"",'location=_new, width=900px, scrollbars=yes');
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('jsprintdetailfakturpembelian',$js, CClientScript::POS_HEAD);
?>