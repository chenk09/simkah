<?php
    echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));  
?>
<div style='border:1px solid #cccccc; border-radius:2px;padding:10px; width: 15%;float:right;margin-right:30px;margin-top:-80px;text-align:center;'>
                <font style='font-size:9pt;text-align:center;'><B>PURCHASE RECEIVED REPORT</B></font><br></div>
  
<div id="div_detail">
<?php
    $criteria2 = new CDbCriteria;
    $criteria2->compare('t.fakturpembelian_id',$idFaktur);
    
    $model = GFFakturpembelianT::model()->find($criteria2);
    $supplier_temp = "";
         if($model->supplier_id){
                $supplier = $model->supplier->supplier_nama;
                $alamat = $model->supplier->supplier_alamat;
                $nopermintaan = $model->penerimaan->permintaanpembelian->nopermintaan;
                $nofaktur = $model->nofaktur;
                $tglfaktur = $model->tglfaktur;
                $tgljatuhtempo = $model->tgljatuhtempo;
                $noterima = $model->penerimaan->noterima;
                $tglterima = $model->penerimaan->tglterima;
            } else{
                $supplier = '';
            }           
            if($supplier_temp != $supplier)
            {
            echo "                
                <table width='100%' border='0' style='font-size:small;'>
                    <tr>
                        <td>&nbsp;Receiving : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Purchase Order : </td>
                    </tr>
                    <tr>
                        <td>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".$noterima."</B></font><br>
                            </div>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".$tglterima."</B></font><br>
                            </div>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".(!empty($nopermintaan) ? $nopermintaan : "-")."</B></font><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;Faktur / Surat : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Due Date: </td>
                    </tr>
                    <tr>
                        <td>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".$nofaktur."</B></font><br>
                            </div>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".$tglfaktur."</B></font><br>
                            </div>
                            <div style='border:1px solid #cccccc; border-radius:2px;padding:5px; width: 12%;float:left;margin-left:3px;'>
                                <font style='font-size:9pt'><B>".$tgljatuhtempo."</B></font><br>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan=7 style='font-size:9pt;'><b> Nama Supplier: $supplier </b></td>
                    </tr>
                    <tr>
                        <td colspan=7 style='font-size:9pt;'><b> Alamat Supplier: $alamat </b></td>
                    </tr>
                    <tr><td>";
                echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;background-color:#C0C0C0'>
                                <td align='center'>Kode</td>
                                <td align='center'>Nama</td>
                                <td align='center'>Satuan</td>
                                <td align='center'>Qty</td>
                                <td align='center'>Harga</td>
                                <td align='center'>Netto</td>
                                <td align='center'>Disc %</td>
                                <td align='center'>Ppn %</td>
                                <td align='center'>Bruto</td>
                                <td align='center'>Keterangan</td>
                            </tr>";
				//EHJ-3637
            $format = new CustomFormat();
            $term = $supplier;
            $totHarga = 0;
            $totBruto = 0;
            $totDiscount = 0;
            $totPpn = 0;
            $totNetto = 0;
            $totBayar = 0;
            $totSisa = 0;
            $detail = FakturdetailT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFaktur));
            foreach($detail as $key=>$details){
                    $harga = $details->harganettofaktur;
                    $netto = $harga * $details->jmlterima;
                    $discount = ($netto * $details->persendiscount / 100);
                    $ppn = $details->hargappnfaktur * $details->jmlterima;
                    $bruto = $netto + $ppn;
                    
                    $totHarga += $harga;
                    $totBruto += $netto;
                    $totDiscount += $discount;
                    $totPpn += $ppn;
                    $totNetto += $bruto;
                    
                    echo "<tr>
                              <td width='150px;'>".$details->obatalkes->obatalkes_kode."</td>
                              <td width='280px;'>".$details->obatalkes->obatalkes_nama."</td>
                              <td width='220px;'>".$details->obatalkes->satuanbesar->satuanbesar_nama."</td>
                              <td width='70px;' style='text-align:center'>".$details->jmlterima."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($harga)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($netto)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($discount)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($ppn)."</td>
                              <td width='70px;' style='text-align:right'>".MyFunction::formatNumber($bruto)."</td>
                              <td width='70px;' style='text-align:right'>-</td>
                          </tr>";
            }
			$totBayar = $model->bayarkesupplier->jmldibayarkan;
			$totSisa = $totNetto - $totBayar;
            
                    echo "<tfoot style='background-color:#ffffff;'>
                            <div>
                                <table align=right>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Total : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBruto)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Discount : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totDiscount)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Ppn : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totPpn)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Total Transaksi : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totNetto)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Bayar : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totBayar)."</td>
                                    </tr>
                                    <tr>
                                        <td colspan=7 style='text-align:right'>Sisa : </td>
                                        <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totSisa)."</td>
                                    </tr>
                                </table>
                            </div>
                            <div style='border:0px solid #cccccc;padding:10px; width: 10%;float:right;margin-top:5px;margin-right:60px;'>
                                    <font style='font-size:9pt'><B><CENTER>Purchasing<CENTER></B><br><br><br/>
                                    <font style='font-size:9pt'><B><CENTER>MELI<CENTER></B><hr style='height:3px;background:#000000;margin-top:-2px;' />
</div>
                            </div>
                          </tfoot>";
                     echo "</table><br/>";
            }
            
            $supplier_temp = $supplier;
      echo "</td></tr></table>";
?>
</div>    