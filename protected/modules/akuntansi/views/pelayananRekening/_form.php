<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pelayananrek-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<?php 
//        if (isset($modDetails)){ echo $form->errorSummary($modDetails); }
        ?>
	<?php echo $form->errorSummary($model); ?>
        <table>
            <tr>
                <td>
                    <div class="control-group">
                        <?php echo $form->labelEx($model, 'jnspelayanan', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php
                                echo $form->dropDownList($model, 'jnspelayanan', Jenispelayanan::items(), array('class'=>'span2','empty'=>'-- Pilih --'));
                            ?>
                        </div>
                    </div>
                    <div class='control-group'>
                                      <?php echo CHtml::label('Rekening Debit','rekeningdebit_id', array('class'=>'control-label')) ?>
                                 <div class="controls">
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][saldonormal]','D',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][rekening_id1]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][rekening_id2]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][rekening_id3]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][rekening_id4]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][1][rekening_id5]','',array('readonly'=>true)); ?>
                                     
                                        <?php
                                            $this->widget('MyJuiAutoComplete', array(
                                                'model' => $model,
                                                'attribute' => 'rekDebit',
                                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/RekeningAkuntansiDebit'),
                                                'options' => array(
                                                    'showAnim' => 'fold',
                                                    'minLength' => 2,
                                                    'focus' => 'js:function( event, ui ) {
                                                            $(this).val(ui.item.nmstruktur);
                                                            return false;
                                                        }',
                                                    'select' => 'js:function( event, ui ) {
                                                                    $(this).val(ui.item.nmrincianobyek);
                                                                    $("#AKPelayananrekM_rekening_1_rekening_id1").val(ui.item.struktur_id);
                                                                    $("#AKPelayananrekM_rekening_1_rekening_id2").val(ui.item.kelompok_id);
                                                                    $("#AKPelayananrekM_rekening_1_rekening_id3").val(ui.item.jenis_id);
                                                                    $("#AKPelayananrekM_rekening_1_rekening_id4").val(ui.item.obyek_id);
                                                                    $("#AKPelayananrekM_rekening_1_rekening_id5").val(ui.item.rincianobyek_id);
                                                                        return false;
                                                                  }'
                                                ),
                                                'htmlOptions' => array(
                                                    'onkeypress' => "return $(this).focusNextInputField(event)",
                                                    'placeholder'=>'Ketikan Nama Rekening',
                                                    'class'=>'span3',
                                                    'style'=>'width:150px;',
                                                ),
                                                'tombolDialog' => array('idDialog' => 'dialogRekDebit',),
                                            ));
                                        ?>
                                 </div>
                       </div>

                       <div class='control-group'>
                                      <?php echo CHtml::label('Rekening Kredit','rekeningkredit_id', array('class'=>'control-label')) ?>
                                 <div class="controls">
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][saldonormal]','K',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][rekening_id1]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][rekening_id2]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][rekening_id3]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][rekening_id4]','',array('readonly'=>true)); ?>
                                     <?php echo CHtml::hiddenField('AKPelayananrekM[rekening][2][rekening_id5]','',array('readonly'=>true)); ?>
                                        <?php
                                            $this->widget('MyJuiAutoComplete', array(
                                                'model' => $model,
                                                'attribute' => 'rekKredit',
                                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/RekeningAkuntansiKredit'),
                                                'options' => array(
                                                    'showAnim' => 'fold',
                                                    'minLength' => 2,
                                                    'focus' => 'js:function( event, ui ) {
                                                            $(this).val(ui.item.nmstruktur);
                                                            return false;
                                                        }',
                                                    'select' => 'js:function( event, ui ) {
                                                                    $(this).val(ui.item.nmrincianobyek);
                                                                    $("#AKPelayananrekM_rekening_2_rekening_id1").val(ui.item.struktur_id);
                                                                    $("#AKPelayananrekM_rekening_2_rekening_id2").val(ui.item.kelompok_id);
                                                                    $("#AKPelayananrekM_rekening_2_rekening_id3").val(ui.item.jenis_id);
                                                                    $("#AKPelayananrekM_rekening_2_rekening_id4").val(ui.item.obyek_id);
                                                                    $("#AKPelayananrekM_rekening_2_rekening_id5").val(ui.item.rincianobyek_id);
                                                                        return false;
                                                                  }'
                                                ),
                                                'htmlOptions' => array(
                                                    'onkeypress' => "return $(this).focusNextInputField(event)",
                                                    'placeholder'=>'Ketikan Nama Rekening',
                                                    'class'=>'span3',
                                                    'style'=>'width:150px;',
                                                ),
                                                'tombolDialog' => array('idDialog' => 'dialogRekKredit',),
                                            ));
                                        ?>
                                 </div>
                       </div>
                </td>
            </tr>
        </table>
    
    <?php 
        $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'daftartindakankomponen-v-grid',
            'dataProvider'=>$modTindakanKomponen->searchTable(),
            'filter'=>$modTindakanKomponen,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                    'header'=>'Pilih'.'<br>'.CHtml::checkBox("AKDaftartindakankomponenV[cekAll]","",array('onclick'=>'checkAllKomponen(this);')),
                    'type'=>'raw',
                    'value'=>'CHtml::checkBox("AKDaftartindakankomponenV[komponen][".$data->daftartindakan_id."][".$data->komponentarif_id."][pilihKomponen]","",array("onclick"=>"setAll();","class"=>"cekList"))',
                ),
                array(
                    'header'=>'Kategori Tindakan',
                    'name'=>'kategoritindakan_id',
                    'filter'=> CHtml::listData(KategoritindakanM::model()->findAll(array('order'=>'kategoritindakan_nama'),"kategoritindakan_aktif IS TRUE"),'kategoritindakan_id','kategoritindakan_nama'),
                    'value'=>'$data->kategoritindakan_nama',
                    'type'=>'raw',
                ),
                array(
                    'header'=>'Kelompok Tindakan',
                    'name'=>'kelompoktindakan_id',
                    'filter'=> CHtml::listData(KelompoktindakanM::model()->findAll(array('order'=>'kelompoktindakan_nama'),"kelompoktindakan_aktif IS TRUE"),'kelompoktindakan_id','kelompoktindakan_nama'),
                    'value'=>'$data->kelompoktindakan_nama',
                    'type'=>'raw',
                ),
                array(
                    'header'=>'Tindakan',
                    'name'=>'daftartindakan_nama',
//                    'filter'=> CHtml::listData(DaftartindakanM::model()->findAll("daftartindakan_aktif IS TRUE"),'daftartindakan_id','daftartindakan_nama'),
                    'value'=>'$data->daftartindakan_nama',
                    'type'=>'raw',
                ),
                array(
                    'header'=>'Komponen Tarif',
                    'name'=>'komponentarif_nama',
//                    'filter'=> CHtml::listData(KomponentarifM::model()->findAll("komponentarif_aktif IS TRUE"),'komponentarif_id','komponentarif_nama'),
                    'value'=>'$data->komponentarif_nama',
                    'type'=>'raw',
                ),
            ),
            'afterAjaxUpdate'=>'
                function(id, data){
                    jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            }',
        ));
                
    ?>

    <div class="form-actions">
                            <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.$this->id.'/create'), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>


<?php $this->endWidget(); ?>

     
<?php 
//========= Dialog buat cari data Rek Debit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekDebit',
    'options'=>array(
        'title'=>'Daftar Rekening Debit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>700,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekDebit = new RekeningakuntansiV('search');
$modRekDebit->unsetAttributes();
$account = "";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekDebit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'rekdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekDebit->searchAccounts($account),
	'filter'=>$modRekDebit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	   'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'name'=>'nourutrek',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Kode 1',
                    'name'=>'kdstruktur',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Kode 2',
                    'name'=>'kdkelompok',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Kode 3',
                    'name'=>'kdjenis',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Kode 4',
                    'name'=>'kdobyek',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Kode 5',
                    'name'=>'kdrincianobyek',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrincianobyek',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrincianobyeklain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rincianobyek_nb',
                    'value'=>'($data->rincianobyek_nb == "D") ? "Debit" : "Kredit"',
                     'filter'=>array('D'=>'Debit','K'=>'Kredit'),
                                   ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKPelayananrekM_rekening_1_rekening_id1\").val(\"$data->struktur_id\");
                                                $(\"#AKPelayananrekM_rekening_1_rekening_id2\").val(\"$data->kelompok_id\");
                                                $(\"#AKPelayananrekM_rekening_1_rekening_id3\").val(\"$data->jenis_id\");
                                                $(\"#AKPelayananrekM_rekening_1_rekening_id4\").val(\"$data->obyek_id\");
                                                $(\"#AKPelayananrekM_rekening_1_rekening_id5\").val(\"$data->rincianobyek_id\");
                                                $(\"#AKPelayananrekM_rekDebit\").val(\"$data->nmrincianobyek\");                                                
                                                $(\"#dialogRekDebit\").dialog(\"close\");    
                                                return false;
                                                 
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Debit dialog =============================
?>
        
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekKredit',
    'options'=>array(
        'title'=>'Daftar Rekening Kredit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekKredit = new RekeningakuntansiV('search');
$modRekKredit->unsetAttributes();
$account = "";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'rekkredit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts(),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'name'=>'nourutrek',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Kode 1',
                    'name'=>'kdstruktur',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'name'=>'kdkelompok',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Kode 3',
                    'name'=>'kdjenis',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Kode 4',
                    'name'=>'kdobyek',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Kode 5',
                    'name'=>'kdrincianobyek',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrincianobyek',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrincianobyeklain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rincianobyek_nb',
                    'value'=>'($data->rincianobyek_nb == "K" ) ? "Kredit" : "Debit"',
                      'filter'=>array('D'=>'Debit','K'=>'Kredit'),
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKPelayananrekM_rekening_2_rekening_id1\").val(\"$data->struktur_id\");
                                                $(\"#AKPelayananrekM_rekening_2_rekening_id2\").val(\"$data->kelompok_id\");
                                                $(\"#AKPelayananrekM_rekening_2_rekening_id3\").val(\"$data->jenis_id\");
                                                $(\"#AKPelayananrekM_rekening_2_rekening_id4\").val(\"$data->obyek_id\");
                                                $(\"#AKPelayananrekM_rekening_2_rekening_id5\").val(\"$data->rincianobyek_id\");
                                                
                                                $(\"#AKPelayananrekM_rekKredit\").val(\"$data->nmrincianobyek\");
                                                $(\"#dialogRekKredit\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>

