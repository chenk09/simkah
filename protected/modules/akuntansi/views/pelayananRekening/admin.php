<?php
$this->breadcrumbs=array(
	'Jenis Penjamin Alkes Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pelayanan Rekening ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Pelayanan Rekening ', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
        $('#pelayananrek-m-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('pelayananrek-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pelayananrek-m-grid',
	'dataProvider'=>$model->searchPelayanan(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',  
                ),                
                array(
                    'header'=>'Jenis Pelayanan',
                    'name'=>'jnspelayanan',
                    'value'=>'$data->jnspelayanan',  
                ),
                array(
                    'header'=>'Nama Pelayanan',
                    'name'=>'daftartindakan_nama',
                    'value'=>'$data->daftartindakan->daftartindakan_nama',  
                ),
                array(
                    'header'=>'Komponen Tarif',
                    'name'=>'komponentarif_nama',
                    'value'=>'$data->komponentarif->komponentarif_nama',  
                ),
                array(
                    'header'=>'Rekening Debit',
                    'type'=>'raw',
                    'name'=>'rekDebit',
                    'value'=>'$this->grid->owner->renderPartial("_rekPelayanan",array(saldonormal=>D,daftartindakan_id=>$data->daftartindakan_id,komponentarif_id=>$data->komponentarif_id),true)',
                ),   
                 array(
                    'header'=>'Rekening Kredit',
                    'type'=>'raw',
                    'name'=>'rekKredit',
                    'value'=>'$this->grid->owner->renderPartial("_rekPelayanan",array(saldonormal=>K,daftartindakan_id=>$data->daftartindakan_id,komponentarif_id=>$data->komponentarif_id),true)',
                ), 

		       array(
                    'header'=>Yii::t('zii','View'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{view}',
                    'buttons'=>array(
                            'view' => array (
                                    'label'=>"<i class='icon-view'></i>",
                                    'options'=>array('title'=>Yii::t('mds','View')),
                                    'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/view",array("daftartindakan_id"=>"$data->daftartindakan_id","komponentarif_id"=>"$data->komponentarif_id"))',
                                                
                            ),
                    ),
                ),

		        array(
                    'header'=>Yii::t('zii','Delete'),
                    'class'=>'bootstrap.widgets.BootButtonColumn',
                    'template'=>'{delete}',
                    'buttons'=>array(                                        
                             'delete' => array (
                                'label'=>"<i class='icon-delete'></i>",
                                'options'=>array('title'=>Yii::t('mds','Delete')),
                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/delete",array("daftartindakan_id"=>"$data->daftartindakan_id","komponentarif_id"=>"$data->komponentarif_id"))',           
                            ),
                    )
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
?>
<?php
        $content = $this->renderPartial('../tips/master2',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        
        
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#pelayananrek-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?>

<?php 
// Dialog buat lihat penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahRekeningDebitKredit',
    'options'=>array(
        'title'=>'Ubah Data Rekening',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>400,
        'resizable'=>false,
        'close'=>'js:function(){$.fn.yiiGridView.update(\'pelayananrek-m-grid\',{})}'
    ),
));
?>
<iframe src="" name="iframeEditRekeningDebitKredit" width="100%" height="300" >
</iframe>
<?php $this->endWidget(); ?>