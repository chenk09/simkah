<?php
$this->breadcrumbs=array(
	'Jurnal Rekening Penjamin'=>array('index'),
	$model->daftartindakan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jurnal Rekening Pelayanan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jurnal Rekening Pelayanan', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            array(
                'label'=>'Jenis Pelayanan',
                'type'=>'raw',
                'value'=>$model->jnspelayanan,
            ),
            array(
                'label'=>'Tindakan',
                'type'=>'raw',
                'value'=>$model->daftartindakan->daftartindakan_nama,
            ),
            array(
                'label'=>'Komponen',
                'type'=>'raw',
                'value'=>$model->komponentarif->komponentarif_nama,
            ),
            array(
                    'label'=>'Rekening Debit',
                    'type'=>'raw',
                    'value'=>$this->renderPartial('_viewRekening',array('daftartindakan_id'=>$model->daftartindakan_id,'komponentarif_id'=>$model->komponentarif_id,'saldonormal'=>"D"),true),
             ),
            array(
                    'label'=>'Rekening Kredit',
                    'type'=>'raw',
                    'value'=>$this->renderPartial('_viewRekening',array('daftartindakan_id'=>$model->daftartindakan_id,'komponentarif_id'=>$model->komponentarif_id,'saldonormal'=>"K"),true),
             ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>