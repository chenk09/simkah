
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
  ?>  

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pelayananrek-m-grid',
	'dataProvider'=>$model->searchPelayanan(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',  
                ),                
                array(
                    'header'=>'Jenis Pelayanan',
                    'value'=>'$data->jnspelayanan',  
                ),
                array(
                    'header'=>'Nama Pelayanan',
                    'value'=>'$data->daftartindakan->daftartindakan_nama',  
                ),
                array(
                    'header'=>'Komponen Tarif',
                    'name'=>'komponentarif_nama',
                    'value'=>'$data->komponentarif->komponentarif_nama',  
                ),
                array(
                    'header'=>'Rekening Debit',
                    'type'=>'raw',
                    'name'=>'rekDebit',
                    'value'=>'$this->grid->owner->renderPartial("_rekPelayanan",array(saldonormal=>D,daftartindakan_id=>$data->daftartindakan_id,komponentarif_id=>$data->komponentarif_id),true)',
                ),   
                 array(
                    'header'=>'Rekening Kredit',
                    'type'=>'raw',
                    'name'=>'rekKredit',
                    'value'=>'$this->grid->owner->renderPartial("_rekPelayanan",array(saldonormal=>K,daftartindakan_id=>$data->daftartindakan_id,komponentarif_id=>$data->komponentarif_id),true)',
                ), 
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>