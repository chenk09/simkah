<fieldset id ="input-penjualanaset"> 
<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp. ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    ));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'akpenjualanaset-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class="rim2">Transaksi Penjualan Aset Gedung</legend>
<table>
    <tr>
        <td width="33%" align="right">
            <?php echo $form->labelEx($modAset, 'tgl penjualan', array('class' => 'control-label')); ?>  
            <div class="controls">
                <?php $this->widget('MyDateTimePicker',array(
                    'model'=>$modAset,
                    'attribute'=>'[0]tglpenghapusan',
                    'mode'=>'date',
                    'options'=> array(
                        'dateFormat'=> 'MM yy',
                        'changeYear' => true,
                        'changeMonth' => true,
                        'changeDate' => false,
                        'showSecond' => false,
                        'showDate' => false,
                        'showMonth' => false,
                        // 'timeFormat' => 'hh:mm:ss',
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                        'onChange'=>'ambilDataPenghapusan()',
                    ),
                )); ?> 
            </div>
        </td>
    </tr>
</table>
<div id="div_tblInputUraian">
    <table id="tblInputUraian" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th rowspan="2">Pilih<br><?php 
                    echo CHtml::checkBox('checkAllAset',true, array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked')) ?>
                </th>
                <th>Kode Inventaris</th>
                <th>No. Register</th>
                <th>Nama Gedung</th>
                <th>Harga</th>
                <th>Harga Jual Aktiva</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $this->renderPartial('akuntansi.views.penjualanAset._rowUraian',array('form'=>$form,'modAset'=>$modAset)); ?>
        </tbody>
    </table>
</div>
<?php
    $this->renderPartial('akuntansi.views.penjualanAset.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
<div class="form-actions">
    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/matauangM/admin'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    <?php
        $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
    ?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    setJurnal(<?php echo Params::ID_REKENING_PENJUALAN_GEDUNG; ?>);
	function maskMoneyAll()
    {
        $('#tblInputRekening tbody tr').each(function(){
            $(this).find("input[class*='currency']").maskMoney(
                {"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
            );            
        });
    }
    
    function unMaskMoneyAll()
    {
        $('#tblInputRekening tbody tr').each(function(){
            $(this).find("input[class*='currency']").unmaskMoney(
                {"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0}
            );            
        });
    } 
    function setJurnal(jenispengeluaran_id){     
        var aktivatetap = 0;
        var akumsusut   = 0;
        var kerugian    = 0;  

        setTimeout(function(){
            getDataManajemenAset(jenispengeluaran_id,akumsusut,kerugian,aktivatetap);
        },500);
    }

    function ambilDataPenghapusan()
    {
        $('#JenispengeluaranrekeningV_1_saldodebit').val(0);
        $('#JenispengeluaranrekeningV_2_saldokredit').val(0);

        var periode     = $('#InvgedungT_0_tglpenghapusan').val();
        var jenisInven  = "gedung";
        $.post('<?php echo Yii::app()->createUrl('akuntansi/actionAjax/ambilDataPenghapusan');?>', {periode:periode, jenis:jenisInven}, function(data){
                $("#tblInputUraian > tbody").empty();
                $("#tblInputUraian > tbody").append(data.replace());
                hitungRekening();

                maskMoneyAll();
        }, 'json');
    }

    function checkAll()
    {
        if ($("#checkAllAset").is(":checked"))
        {
            var jumlah = 0;
            $('#tblInputUraian input[name*="is_checked"]').each(
                function(){
                    $(this).attr('checked',true);
                    $(this).val(1);
                }
            );
            hitungRekening();
        } else {
            $('#tblInputUraian input[name*="is_checked"]').each(
                function(){
                    $(this).removeAttr('checked');
                    $(this).val(0);
                }
            );
            $('#JenispengeluaranrekeningV_0_saldodebit').val(0);
            $('#JenispengeluaranrekeningV_0_saldokredit').val(0);
            $('#JenispengeluaranrekeningV_1_saldodebit').val(0);
            $('#JenispengeluaranrekeningV_1_saldokredit').val(0);
            $('#JenispengeluaranrekeningV_2_saldodebit').val(0);
            $('#JenispengeluaranrekeningV_2_saldokredit').val(0);
            $('#JenispengeluaranrekeningV_3_saldodebit').val(0);
            $('#JenispengeluaranrekeningV_3_saldokredit').val(0);
            $('#JenispengeluaranrekeningV_4_saldodebit').val(0);
            $('#JenispengeluaranrekeningV_4_saldokredit').val(0);
        }
    }

    function checkRekening(obj, key)
    {
        var index = $(obj).parents("tr").find("input[name$='[invgedung_id]']").val();  
        if($(obj).is(":checked"))
        {    
            $('#tblInputUraian').find('input[name$="[invgedung_id]"][value="'+ index +'"]').each(
                function(){
                    $(obj).parents("tr").find("input[name$='[is_checked]']").attr('checked',true);
                    $(obj).parents("tr").find("input[name$='[is_checked]']").val(1);
                }
            );
        }else{
            $('#tblInputUraian').find('input[name$="[invgedung_id]"][value="'+ index +'"]').each(
                function(){
                    $(obj).parents("tr").find("input[name$='[is_checked]']").attr('checked',false);
                    $(obj).parents("tr").find("input[name$='[is_checked]']").val(0);
                }
            );            
        }
        hitungRekening();

    }

    function hitungRekening()
    {
        var jumlah = 0;
        var hargabeli = 0;
        var untung = 0;
        var rugi = 0;
        var penyusutan = 0;
        $('#tblInputUraian input[name*="is_checked"]').each(
            function(){

                var ceklis = $(this).parents("tr").find("input[name$='[is_checked]']").val();

                if(ceklis=="1"){
                    var a = $(this).parents("tr").find("input[name$='[hargajualaktiva]']").val();
                    jumlah = parseFloat(jumlah) + parseFloat(a);

                    var hb = $(this).parents("tr").find("input[name$='[invgedung_harga]']").val();
                    hargabeli = parseFloat(hargabeli) + parseFloat(hb);

                    var kerugian = $(this).parents("tr").find("input[name$='[kerugian]']").val();
                    rugi = parseFloat(rugi) + (parseFloat(kerugian)*-1);

                    var keuntungan = $(this).parents("tr").find("input[name$='[keuntungan]']").val();
                    untung = parseFloat(untung) + parseFloat(keuntungan);

                    var susut = $(this).parents("tr").find("input[name$='[invgedung_akumsusut]']").val();
                    penyusutan = parseFloat(penyusutan) + parseFloat(susut);
                }
            }
        );
        var laba = parseFloat(jumlah) + parseFloat(penyusutan) - parseFloat(hargabeli);
        if(laba>=0){
            untung = laba;
            rugi = 0;
            $('#JenispengeluaranrekeningV_4_saldokredit').val(untung);
            $('#JenispengeluaranrekeningV_2_saldodebit').val(rugi);
        }else{
            untung = 0;
            rugi = laba*-1;
            $('#JenispengeluaranrekeningV_4_saldokredit').val(untung);
            $('#JenispengeluaranrekeningV_2_saldodebit').val(rugi);
        }
        $('#JenispengeluaranrekeningV_0_saldodebit').val(jumlah);
        $('#JenispengeluaranrekeningV_1_saldodebit').val(penyusutan);
        $('#JenispengeluaranrekeningV_3_saldokredit').val(hargabeli);
    }
</script>