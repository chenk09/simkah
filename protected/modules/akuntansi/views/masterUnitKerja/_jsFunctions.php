<?php $baseUrl = Yii::app()->createUrl("/");?>
<script type='text/javascript'>
function setTab(obj){
    $(obj).parents("ul").find("li").each(function(){
        $(this).removeClass("active");
        $(this).attr("onclick","setTab(this);");
    });
    $(obj).addClass("active");
    $(obj).removeAttr("onclick","setTab(this);");
    var tab = $(obj).attr("tab");
    $("#frame").attr("src","<?php echo $baseUrl;?>?r="+tab+"&frame=1");
    resizeIframe(document.getElementById("frame"));
    return false;
}
function resizeIframe(obj) {
    $("#frame").parent().addClass("srbacLoading");
    $(obj).load(function(){
        obj.style.height = (obj.contentWindow.document.body.scrollHeight+25) + 'px';
        $("#frame").parent().removeClass("srbacLoading");
    });
}
</script>
<?php
Yii::app()->clientScript->registerScript('onLoadJs','
    setTab($("#tab-default"));
    resizeIframe(document.getElementById("frame"));    
', CClientScript::POS_READY);

?>