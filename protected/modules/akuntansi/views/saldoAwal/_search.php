<legend class="rim">Pencarian</legend>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'aksaldoawal-t-search',
        'type'=>'horizontal',
)); ?>

<?php echo $form->textFieldRow($model,'kdrekening5',array('class'=>'span3')); ?>
<?php echo $form->textFieldRow($model,'nmrekening5',array('class'=>'span3')); ?>
<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
        Yii::app()->createUrl($this->module->id.'/SaldoAwal/informasi'), 
        array('class'=>'btn btn-danger',
           	  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
  	<?php
		$content = $this->renderPartial('../tips/informasi',array(),true);
		$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
	?>
</div>
<?php $this->endWidget(); ?>