<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<fieldset>
<legend class="rim2">Daftar Saldo Rekening</legend>
    <div style="max-width:1300px;overflow:auto;">
        <?php
            $this->widget('ext.bootstrap.widgets.BootGridView',
                array(
                    'id'=>'grid-saldo-rekening',
                    'dataProvider'=>$model->searchByFilter(),
                    'template'=>"{pager}{summary}\n{items}",
                    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'columns'=>array(
                        array(
                          'header'=>'No',
                          'type'=>'raw',
                          'value'=>'$row+1',
                          'htmlOptions'=>array('style'=>'width:20px')
                        ),
                        array(
                           'name'=>'kdstruktur',
                           'type'=>'raw',
                           'value'=>'$data->kdstruktur',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px;')
                        ),
                        array(
                           'name'=>'kdkelompok',
                           'type'=>'raw',
                           'value'=>'$data->kdkelompok',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdjenis',
                           'type'=>'raw',
                           'value'=>'($data->kdjenis == null ? "-" : $data->kdjenis)',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdobyek',
                           'type'=>'raw',
                           'value'=>'($data->kdobyek == null ? "-" : $data->kdobyek)',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdrincianobyek',
                           'type'=>'raw',
                            'value'=>'($data->kdrincianobyek == null ? "-" : $data->kdrincianobyek)',
                           'htmlOptions'=>array('style'=>'text-align: center; width:80px')
                        ),
                        array(
                           'header'=>'Nama Rekening',
                           'type'=>'raw',
                           'value'=>'(isset($data->nmrincianobyek) ? $data->nmrincianobyek : (isset($data->nmobyek) ? $data->nmobyek : (isset($data->nmjenis) ? $data->nmjenis : (isset($data->nmkelompok) ? $data->nmkelompok : (isset($data->nmstruktur) ? $data->nmstruktur : "-")))))',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),                        
                        /*
                        array(
                           'name'=>'nmstruktur',
                           'type'=>'raw',
                           'value'=>'isset($data->nmstruktur) ? $data->nmstruktur : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmkelompok',
                           'type'=>'raw',
                           'value'=>'isset($data->nmkelompok) ? $data->nmkelompok : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmjenis',
                           'type'=>'raw',
                           'value'=>'isset($data->nmjenis) ? $data->nmjenis : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmobyek',
                           'type'=>'raw',
                           'value'=>'isset($data->nmobyek) ? $data->nmobyek : "-"',
                        ),
                        array(
                           'name'=>'nmrincianobyek',
                           'type'=>'raw',
                           'value'=>'isset($data->nmrincianobyek) ? $data->nmrincianobyek : "-"',
                        ),
                        array(
                           'name'=>'rincianobyek_nb',
                           'type'=>'raw',
                            'value'=>'($data->rincianobyek_nb == null ? "-" : ($data->rincianobyek_nb == "D" ? "Debit" : "Kredit"))',
                        ),
                        array(
                           'name'=>'kelompokrek',
                           'type'=>'raw',
                            'value'=>'($data->kelompokrek == null ? "-" : $data->kelompokrek)',
                        ),
                         * 
                         */
                        'matauang',
                        'jmlanggaran',
                        'jmlsaldoawald',
                        'jmlsaldoawalk',
                        'jmlmutasid',
                        'jmlmutasik',
                        'jmlsaldoakhird',
                        'jmlsaldoakhirk',
                        array(
                           'header'=>'&nbsp;',
                           'type'=>'raw',
                           'value'=>'CHtml::Link("<i class=\'icon-pencil-brown\'></i>", Yii::app()->controller->createUrl("SaldoAwal/editSaldoRekening",array("id"=>$data->saldoawal_id)),array("value"=>$data->saldoawal_id, "onclick"=>"editSaldoJenisRek(this);return false;","rel"=>"tooltip", "title"=>"Klik Untuk Edit<br>Saldo Rekening",))',
                        )
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                )
            );
        ?>
    </div>
</fieldset>
<?php 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
?>
<?php
        $content = $this->renderPartial('../tips/master',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        
        
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#penjaminpasien-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditSaldoRekening',
        'options' => array(
            'title' => 'Edit Saldo Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 300,
            'resizable' => false,
            'close'=>'js:function(){$.fn.yiiGridView.update(\'grid-saldo-rekening\', {});}'
        ),
    )
);
?>
<div id="pop_up_content"></div>
<?php
    $this->endWidget();
?>

<script type="text/javascript">
    
</script>