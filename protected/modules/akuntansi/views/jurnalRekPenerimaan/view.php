<?php
$this->breadcrumbs=array(
	'Jenispenerimaan Ms'=>array('index'),
	$model->jenispenerimaan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jurnal Rek Penerimaan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jurnal Rek Penerimaan ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php if(count($model)>0){ ?>
<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
                array(
                    'label'=>'Jenis Penerimaan',
                    'type'=>'raw',
                    'value'=>$model->jenispenerimaan->jenispenerimaan_nama,
                ),
                array(
                    'label'=>'Nama Lain',
                    'type'=>'raw',
                    'value'=>$model->jenispenerimaan->jenispenerimaan_namalain,
                ),
                
		array(
                    'label'=>'Kode',
                    'type'=>'raw',
                    'value'=>$model->jenispenerimaan->jenispenerimaan_kode,
                ),
                 array(
                     'label'=>'Rekening Debit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewDebit',array('jenispenerimaan_id'=>$model->jenispenerimaan_id,'saldonormal'=>D),true),
                 ),
                 array(
                     'label'=>'Rekening Kredit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewKredit',array('jenispenerimaan_id'=>$model->jenispenerimaan_id,'saldonormal'=>K),true),
                 ),
		array(            
                    'label'=>'Aktif',
                    'type'=>'raw',
                    'value'=>(($model->jenispenerimaan->jenispenerimaan_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>
<?php }else{ ?>
<div class="alert alert-block alert-error">
        <a class="close" data-dismiss="alert"></a>
        Data Rekening Belum Diset.
    </div>
<?php } ?>
<?php $this->widget('TipsMasterData',array('type'=>'view'));?>