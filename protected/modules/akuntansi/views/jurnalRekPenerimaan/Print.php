
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'jenispenerimaan-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchJnsPenerimaanRekPrint(),
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
		array(
                    'header'=>'Kode',
                    'value'=>'$data->jenispenerimaan_kode',
                ),
		array(
                    'header'=>'Jenis Penerimaan',
                    'value'=>'$data->jenispenerimaan_nama',
                ),
		array(
                    'header'=>'Nama Lain',
                    'value'=>'$data->jenispenerimaan_namalain',
                ),
		array(
                    'header'=>'Rekening Debit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekPenerimaanD",array(saldonormal=>D,jenispenerimaan_id=>$data->jenispenerimaan_id),true)',
                ),
		array(
                    'header'=>'Rekening Kredit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekPenerimaanK",array(saldonormal=>K,jenispenerimaan_id=>$data->jenispenerimaan_id),true)',
                ),
		array(
                    'header'=>'Status',
                    'value'=>'($data->jenispenerimaan_aktif = 1 ) ? "Aktif" : "Tidak Aktif" ',
                ),
        ),
    )); 
?>