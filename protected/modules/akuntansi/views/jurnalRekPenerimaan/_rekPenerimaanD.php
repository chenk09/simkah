<?php
    $modRekPenerimaan = JnspenerimaanrekM::model()->findAllByAttributes(array('jenispenerimaan_id'=>$jenispenerimaan_id,'saldonormal'=>$saldonormal));
    
    if(COUNT($modRekPenerimaan)>0)
    {   
        foreach($modRekPenerimaan as $i=>$data)
        {
            
            if(isset($_GET['caraPrint'])){
                
                echo "<pre>";
                echo $data->rekeningdebit->nmrekening5;
                echo "</pre>";
            }else{
                echo "<pre>";
                echo $data->rekeningdebit->nmrekening5.CHtml::Link("<i class=\"icon-pencil\"></i>",
                                Yii::app()->controller->createUrl("rekeningPenerimaan/ubahRekeningDebit",array("id"=>$data->jnspenerimaanrek_id)),
                                array("class"=>"", 
                                      "target"=>"iframeEditRekeningDebitKredit",
                                      "onclick"=>"$(\"#dialogUbahRekeningDebitKredit\").dialog(\"open\");",
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk ubah Rekening Debit",
                                ));
                echo "</pre>";
            }
        }
    }
    else
    {
        echo Yii::t('zii','Belum Diset'); 
    }   
?>