<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'jenispengeluaran-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#AKJenispengeluaranM_jenispengeluaran_kode',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary($model); ?>
        
    <table>
            <tr>
                <td>
                    <div class='control-group'>
                                  <?php echo $form->labelEx($model,'jenispengeluaran_kode', array('class'=>'control-label')) ?>
                             <div class="controls">
                                  <?php echo $form->textField($model,'jenispengeluaran_kode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'maxlength'=>50)); ?>
                             </div>
                   </div>

                   <div class='control-group'>
                                  <?php echo $form->labelEx($model,'jenispengeluaran_nama', array('class'=>'control-label')) ?>
                             <div class="controls">
                                  <?php echo $form->textField($model,'jenispengeluaran_nama',array('class'=>'span3', 'onkeyup'=>"namaLain(this)", 'onkeypress'=>"return $(this).focusNextInputField(event);",'maxlength'=>50)); ?>
                             </div>
                   </div>

                   <div class='control-group'>
                                  <?php echo $form->labelEx($model,'jenispengeluaran_namalain', array('class'=>'control-label')) ?>
                             <div class="controls">
                                  <?php echo $form->textField($model,'jenispengeluaran_namalain',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'maxlength'=>50)); ?>
                             </div>
                   </div>
                    
                   <div class='control-group'>
                                    <?php //echo $form->checkBoxRow($model,'jenispengeluaran_aktif',array('checked'=>'checked')); ?>
                   </div>
                </td>
            </tr>
        </table>
        
	<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.jurnalRekPengeluaran.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
	</div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('AKJenispengeluaranM_jenispengeluaran_namalain').value = nama.value.toUpperCase();
    }
</script>