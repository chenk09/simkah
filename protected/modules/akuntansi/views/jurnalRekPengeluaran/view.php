<?php
$this->breadcrumbs=array(
	'Jenispengeluaran Ms'=>array('index'),
	$model->jenispengeluaran_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jurnal Rek Pengeluaran ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jurnal Rek Pengeluaran ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php if(count($model)>0){ ?>
<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'label'=>'Jenis Pengeluaran',
                    'type'=>'raw',
                    'value'=>$model->jenispengeluaran->jenispengeluaran_nama,
                ),
                array(
                    'label'=>'Nama Lain',
                    'type'=>'raw',
                    'value'=>$model->jenispengeluaran->jenispengeluaran_namalain,
                ),
                
		array(
                    'label'=>'Kode',
                    'type'=>'raw',
                    'value'=>$model->jenispengeluaran->jenispengeluaran_kode,
                ),
                array(
                     'label'=>'Rekening Debit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewDebit',array('jenispengeluaran_id'=>$model->jenispengeluaran_id,'saldonormal'=>D),true),
                 ),
                 array(
                     'label'=>'Rekening Kredit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewKredit',array('jenispengeluaran_id'=>$model->jenispengeluaran_id,'saldonormal'=>K),true),
                 ),
		array(            
                    'label'=>'Status',
                    'type'=>'raw',
                    'value'=>(($model->jenispengeluaran->jenispengeluaran_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>
<?php }else{ ?>
<div class="alert alert-block alert-error">
        <a class="close" data-dismiss="alert"></a>
        Data Rekening Belum Diset.
    </div>
<?php } ?>
<?php $this->widget('TipsMasterData',array('type'=>'view'));?>