<?php
$this->breadcrumbs=array(
	'Akunitkerja Ms'=>array('index'),
	$model->unitkerja_id=>array('view','id'=>$model->unitkerja_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Unit Kerja', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//               array_push($arrMenu,array('label'=>Yii::t('mds','List').' AKUnitkerjaM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess('Create')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' AKUnitkerjaM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' AKUnitkerjaM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->unitkerja_id))) ;
                (Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Unit Kerja', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'update'));?>