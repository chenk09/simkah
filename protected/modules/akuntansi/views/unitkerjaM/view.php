<?php
$this->breadcrumbs=array(
	'Akunitkerja Ms'=>array('index'),
	$model->unitkerja_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Unit Kerja ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Unit Kerja', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess('Create')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Unit Kerja', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess('Update')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Unit Kerja', 'icon'=>'pencil','url'=>array('update','id'=>$model->unitkerja_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Unit Kerja','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->unitkerja_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Unit Kerja', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'unitkerja_id',
		'kodeunitkerja',
		'namaunitkerja',
		'namalain',
		array(               // related city displayed as a link
                    'name'=>'unitkerja_aktif',
                    'type'=>'raw',
                    'value'=>(($model->unitkerja_aktif==1)? Yii::t('mds','Yes') : Yii::t('mds','No')),
                ),
	),
)); ?>
