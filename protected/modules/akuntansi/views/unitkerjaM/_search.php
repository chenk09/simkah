<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'akunitkerja-m-search',
        'type'=>'horizontal',
)); ?>

	<?php echo $form->textFieldRow($model,'unitkerja_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'kodeunitkerja',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'namaunitkerja',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'namalain',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->checkBoxRow($model,'unitkerja_aktif',array('checked'=>'unitkerja_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
