<?php
$this->breadcrumbs=array(
	'Bank Ms'=>array('index'),
	$model->bank_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Bank', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Bank', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
                    'label'=>'Nama Bank',
                    'value'=>$model->bank->namabank,
                ),
                array(
                    'label'=>'No Rekening',
                    'value'=>$model->bank->norekening,
                ),
                array(
                    'label'=>'Mata Uang',
                    'value'=>$model->bank->matauang->matauang,
                ),
                array(
                    'label'=>'Propinsi',
                    'value'=>$model->bank->propinsi->propinsi_nama,
                ),
                array(
                    'label'=>'Kabupaten',
                    'value'=>$model->bank->kabupaten->kabupaten_nama,
                ),
                array(
                    'label'=>'Alamat Bank',
                    'value'=>$model->bank->alamatbank,
                ),
                array(
                    'label'=>'Telp Bank 1 / 2',
                    'value'=>$model->bank->telpbank1." / ". $data->bank->telpbank2,
                ),
                array(
                    'label'=>'Fax Bank / <br/> Kode Pos',
                    'value'=>$model->bank->faxbank ." / ". $data->bank->kodepos,
                ),
                array(
                    'label'=>'Email / <br/> Website',
                    'value'=>$model->bank->emailbank ." / ". $data->bank->website,
                ),
                array(
                    'label'=>'Cabang dari / <br/> Negara',
                    'value'=>$model->bank->cabangdari ." / ".$data->bank->negara,
                ),
                array(
                     'label'=>'Rekening Debit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewDebit',array('bank_id'=>$model->bank_id,'saldonormal'=>D),true),
                 ),
                 array(
                     'label'=>'Rekening Kredit',
                     'type'=>'raw',
                     'value'=>$this->renderPartial('_viewKredit',array('bank_id'=>$model->bank_id,'saldonormal'=>K),true),
                 ),
		array(            
                    'label'=>'Aktif',
                    'type'=>'raw',
                    'value'=>(($model->bank->bank_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>