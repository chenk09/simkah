<?php
$this->breadcrumbs=array(
	'Bank Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Bank ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Bank', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
    $('#AKBankRekM_propinsi_id').focus();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bank-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<!-- <div style='max-width:970;overflow-x:scroll'> -->
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'bank-m-grid',
	'dataProvider'=>$model->searchBank(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Nama Bank',
                    'name'=>'namabank',
                    'value'=>'$data->bank->namabank',
                ),
                array(
                    'header'=>'No Rekening',
                    'name'=>'norekening',
                    'value'=>'$data->bank->norekening',
                ),
                array(
                    'header'=>'Mata Uang',
                    'name'=>'matauang',
                    'value'=>'$data->bank->matauang->matauang',
                ),
                array(
                    'header'=>'Propinsi',
                    'name'=>'propinsi_nama',
                    'value'=>'$data->bank->propinsi->propinsi_nama',
                ),
                array(
                    'header'=>'Kabupaten',
                    'name'=>'kabupaten_nama',
                    'value'=>'$data->bank->kabupaten->kabupaten_nama',
                ),
                array(
                    'header'=>'Alamat Bank',
                    'name'=>'alamatbank',
                    'value'=>'$data->bank->alamatbank',
                ),
                // array(
                //     'header'=>'Telp Bank 1 / 2',
                //     'name'=>'telpbank1',
                //     'value'=>'$data->bank->telpbank1." / ". $data->bank->telpbank2',
                // ),
                // array(
                //     'header'=>'Fax Bank / <br/> Kode Pos',
                //     'name'=>'faxbank',
                //     'value'=>'$data->bank->faxbank ." / ". $data->bank->kodepos',
                // ),
                // array(
                //     'header'=>'Email / <br/> Website',
                //     'name'=>'emailbank',
                //     'value'=>'$data->bank->emailbank ." / ". $data->bank->website',
                // ),
                array(
                    'header'=>'Cabang dari / <br/> Negara',
                    'name'=>'cabangdari',
                    'value'=>'$data->bank->cabangdari ." / ".$data->bank->negara',
                ),
               array(
                    'header'=>'Rekening Debit',
                    'name'=>'rekening_debit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekBankD",array(saldonormal=>D,bank_id=>$data->bank_id),true)',
                ),
		array(
                    'header'=>'Rekening Kredit',
                    'name'=>'rekeningKredit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekBankK",array(saldonormal=>K,bank_id=>$data->bank_id),true)',
                ),
                array(
                    'header'=>'Aktif',
                    'value'=>'($data->bank_aktif == 1) ? "Aktif" : "Tidak Aktif" ',
                ),
		array(
                        'header'=>Yii::t('zii','View'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                                'view' => array (
                                        'label'=>"<i class='icon-view'></i>",
                                        'options'=>array('title'=>Yii::t('mds','View')),
                                        'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/view",array("id"=>"$data->bank_id"))',
                                                 
                                ),
                        ),
		),
		array(
                        'header'=>Yii::t('zii','Delete'),
            'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{remove} {delete}',
                        'buttons'=>array(
                                        'remove' => array (
                                                'label'=>"<i class='icon-remove'></i>",
                                                'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
                                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->bank_id"))',
                                                'visible'=>'($data->bank_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
                                                'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
                                        ),
                                        'delete'=> array(
                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
                                        ),
                        )
        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<!-- </div></br> -->
<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
        $content = $this->renderPartial('akuntansi.views.tips.master2',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#bank-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php 
// Dialog buat lihat penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahRekeningDebitKredit',
    'options'=>array(
        'title'=>'Ubah Data Rekening',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>400,
        'resizable'=>false,
        'close'=>'js:function(){$.fn.yiiGridView.update(\'bank-m-grid\', {})}'
    ),
));
?>
<iframe src="" name="iframeEditRekeningDebitKredit" width="100%" height="300" >
</iframe>
<?php $this->endWidget(); ?>