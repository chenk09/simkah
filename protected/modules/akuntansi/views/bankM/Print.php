
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Nama Bank',
                    'name'=>'namabank',
                    'value'=>'$data->bank->namabank',
                ),
                array(
                    'header'=>'No Rekening',
                    'name'=>'norekening',
                    'value'=>'$data->bank->norekening',
                ),
                array(
                    'header'=>'Mata Uang',
                    'name'=>'matauang_id',
                    'value'=>'$data->bank->matauang->matauang',
                ),
                array(
                    'header'=>'Propinsi',
                    'name'=>'propinsi_id',
                    'value'=>'$data->bank->propinsi->propinsi_nama',
                ),
                array(
                    'header'=>'Kabupaten',
                    'name'=>'kabupaten_id',
                    'value'=>'$data->bank->kabupaten->kabupaten_nama',
                ),
                array(
                    'header'=>'Alamat Bank',
                    'name'=>'alamatbank',
                    'value'=>'$data->bank->alamatbank',
                ),
                array(
                    'header'=>'Telp Bank 1 / 2',
                    'name'=>'telpbank1',
                    'value'=>'$data->bank->telpbank1." / ". $data->bank->telpbank2',
                ),
                array(
                    'header'=>'Fax Bank / <br/> Kode Pos',
                    'name'=>'faxbank',
                    'value'=>'$data->bank->faxbank ." / ". $data->bank->kodepos',
                ),
                array(
                    'header'=>'Email / <br/> Website',
                    'name'=>'emailbank',
                    'value'=>'$data->bank->emailbank ." / ". $data->bank->website',
                ),
                array(
                    'header'=>'Cabang dari / <br/> Negara',
                    'name'=>'cabangdari',
                    'value'=>'$data->bank->cabangdari ." / ".$data->bank->negara',
                ),
               array(
                    'header'=>'Rekening Debit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekBankD",array(saldonormal=>D,bank_id=>$data->bank_id),true)',
                ),
		array(
                    'header'=>'Rekening Kredit',
                    'type'=>'raw',
                    'value'=>'$this->grid->owner->renderPartial("_rekBankK",array(saldonormal=>K,bank_id=>$data->bank_id),true)',
                ),
                array(
                    'header'=>'Aktif',
                    'value'=>'($data->bank->bank_aktif == 1) ? "Aktif" : "Tidak Aktif" ',
                ),
	),
    )); 
?>