
<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
if($caraPrint=='EXCEL')
{
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>3));      
?>
<?php 
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'akunitkerja-m-grid',
	'dataProvider'=>$model->searchPrint(),
	'filter'=>$model,
	'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                        'name'=>'unitkerja_id',
                        'value'=>'$data->unitkerja_id',
                        'filter'=>false,
                ),
            array(
                    'header'=>'Nama Unit Kerja',
                    'value'=>'$data->unitkerja->namaunitkerja',
            ),
            array(
                    'name'=>'ruangan',
                    'type'=>'raw',
                    'value'=>'$this->grid->getOwner()->renderPartial(\'_ruanganUnitKerja\',array(\'unitkerja_id\'=>$data->unitkerja_id),true)',
                    'filter'=>false
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>