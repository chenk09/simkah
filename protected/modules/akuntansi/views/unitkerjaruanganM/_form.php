

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'akunitkerja-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#AKUnitkerjaM_namaunitkerja',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
<table>

			<div class='control-group'>
        <label class ='control-label'>Unit Kerja</label>
        <?php echo CHtml::hiddenField('unitkerja_id'); ?>
        <div class="controls">
        <?php
        $this->widget('MyJuiAutoComplete', array(
            'name' => 'namaunitkerja',
            'source' => 'js: function(request, response) {
                               $.ajax({
                                   url: "' . Yii::app()->createUrl('ActionAutoComplete/UnitKerja') . '",
                                   dataType: "json",
                                   data: {
                                       term: request.term,
                                   },
                                   success: function (data) {
                                           response(data);
                                   }
                               })
                            }',
            'options' => array(
                'showAnim' => 'fold',
                'minLength' => 2,
                'focus' => 'js:function( event, ui ) {
                                    $(this).val( ui.item.label);
                                    return false;
                                }',
                'select' => 'js:function( event, ui ) {
                                    $("#unitkerja_id").val(ui.item.unitkerja_id); 
                                    return false;
                                }',
            ),
            'htmlOptions' => array(
                'onkeypress' => "return $(this).focusNextInputField(event)",
                'class' => 'span2',
            ),
            'tombolDialog' => array('idDialog' => 'dialogKerja'),
        ));
        ?>
   </div>
    </div>

    <div class="control-group">
                        <?php echo Chtml::label('Ruangan','ruangan_id',array('class'=>'control-label'));?>
                        <div class="controls">
                           <?php 
                               $this->widget('application.extensions.emultiselect.EMultiSelect',
                                             array('sortable'=>true, 'searchable'=>true)
                                        );
                                echo CHtml::dropDownList(
                                'ruangan_id[]',
                                '',
                                CHtml::listData(RuanganM::model()->findAll(array('order'=>'ruangan_nama')), 'ruangan_id', 'ruangan_nama'),
                                array('multiple'=>'multiple','key'=>'ruangan_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                        );
                          ?>
                        </div>
                    </div>      
</table>
            
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.unitkerjaM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
        </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    // function namaLain(nama)
    // {
    //     document.getElementById('AKUnitkerjaM_namalain').value = nama.value.toUpperCase();
    // }
</script>
<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogKerja',
    'options' => array(
        'title' => 'Daftar Unit Kerja',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000,
        'height' => 500,
        'resizable' => false,
    ),
));

$modKerja = new AKUnitkerjaM('search');
$modKerja->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['AKUnitkerjaM']))
    $modKerja->attributes = $_GET['AKUnitkerjaM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'barang-m-grid',
    'dataProvider'=>$modKerja->search(),
    'filter'=>$modKerja,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'barang_id',
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectUnitkerja",
                                    "onClick" => "
                                        
                                        $(\'#unitkerja_id\').val(\'$data->unitkerja_id\');
                                        $(\'#namaunitkerja\').val(\'$data->namaunitkerja\');
                                        $(\'#kodeunitkerja\').val(\'$data->kodeunitkerja\');
                                        $(\'#dialogKerja\').dialog(\'close\');
                                        return false;"))',
        ),
        array(
                        'name'=>'unitkerja_id',
                        'value'=>'$data->unitkerja_id',
                        'filter'=>false,
                ),

     'kodeunitkerja', 'namaunitkerja', 'namalain',

    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>