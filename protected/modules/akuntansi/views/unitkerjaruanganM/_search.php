<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'akunitkerja-m-search',
        'type'=>'horizontal',
)); ?>

	<?php echo $form->textFieldRow($model,'unitkerja_id',array('class'=>'span1')); ?>
	<?php echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span1')); ?>


	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
