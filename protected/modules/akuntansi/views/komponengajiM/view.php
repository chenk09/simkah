<?php
    $this->breadcrumbs=array(
            'komponengaji Ms'=>array('index'),
            $model->komponengaji_id,
    );

    $arrMenu = array();
                    array_push($arrMenu,array('label'=>Yii::t('mds','View').' Komponen Gaji ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                    (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Komponen Gaji', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

    $this->menu=$arrMenu;

    $this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
        'komponengaji_id',
        'nourutgaji',
        'komponengaji_kode',
        'komponengaji_nama',
        'komponengaji_singkt',
        'ispotongan',
        'kenapajak',
		array(            
                    'label'=>'Aktif',
                    'type'=>'raw',
                    'value'=>(($model->komponengaji_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>