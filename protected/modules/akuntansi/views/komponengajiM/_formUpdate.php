
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'komponengaji-m-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#AKKomponengajiM_komponengaji_nama',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($model); ?>

            <?php echo $form->textFieldRow($model,'nourutgaji',array('class'=>'span1')); ?>
            <?php echo $form->textFieldRow($model,'komponengaji_kode',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
            <?php echo $form->textFieldRow($model,'komponengaji_nama',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'komponengaji_singkt',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>5)); ?>
            <?php echo $form->textFieldRow($model,'ispotongan',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'kenapajak',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'rekening5_id',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
        
             <table>
            <tr>
                <td>
                   <div class='control-group'>
                                  <?php echo $form->labelEx($model,'rekening5_id', array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo $form->hiddenField($model,'rekening5_id',array('class'=>'span7','maxlength'=>100));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'rekening5_nama',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/komponenGaji'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.rekening5_id);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.rekening5_id);
                                                                $("#' . CHtml::activeId($model, 'rekening5_id') . '").val(ui.item.rekening5_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Rekening',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogRek',),
                                        ));
                                    ?>
                             </div>
                         </div>

                </td>
            </tr>
        </table>

        <?php echo $form->checkBoxRow($model,'komponengaji_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.KomponengajiM.'/admin'),
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
    </div>

<?php $this->endWidget(); ?>

<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRek',
    'options'=>array(
        'title'=>'Daftar Jenis Penerimaan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRek = new Rekening5M('search');
$modRek->unsetAttributes();
if(isset($_GET['Rekening5M'])) {
    $modRek->attributes = $_GET['Rekening5M'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
    'id'=>'jenispenerimaan-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider'=>$modRek->search(),
    'filter'=>$modRek,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(

                array(
                    'header'=>'No Urut',
                    'name'=>'nourutrek',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Kode 1',
                    'name'=>'rekening1_id',
                    'value'=>'$data->rekening1_id',
                ),
                array(
                    'header'=>'Kode 2',
                    'name'=>'rekening2_id',
                    'value'=>'$data->rekening2_id',
                ),
                array(
                    'header'=>'Kode 3',
                    'name'=>'rekening3_id',
                    'value'=>'$data->rekening3_id',
                ),
                array(
                    'header'=>'Kode 4',
                    'name'=>'rekening4_id',
                    'value'=>'$data->rekening4_id',
                ),
                array(
                    'header'=>'Kode 5',
                    'name'=>'rekening5_id',
                    'value'=>'$data->rekening5_id',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrekening5',
                    'value'=>'$data->nmrekening5',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrekeninglain5',
                    'value'=>'$data->nmrekeninglain5',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rekening5_nb',
                    'value'=>'$data->rekening5_nb',
                ),
                
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRek",
                                    "onClick" =>"
                                                $(\"#AKKomponengajiM_rekening5_id\").val(\"$data->rekening5_id\");
                                                $(\"#AKKomponengajiM_rekening5_nama\").val(\"$data->nmrekening5\");
                                                $(\"#dialogRek\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>
