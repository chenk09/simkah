<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'reharga-jual-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#AKPenjaminRekM_rekDebit',
)); ?>

	<?php 
        if (isset($modDetails)){ echo $form->errorSummary($modDetails); }
        ?>
	<?php echo $form->errorSummary($model); ?>
        <table>
            <tr>
                <td>
                    <div class='control-group'>
                                  <?php echo CHtml::label('Rekening Debit','rekening debit',array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][saldonormal]','D', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][rekening5_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][rekening4_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][rekening3_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][rekening2_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][1][rekening1_id]','', array('readonly'=>true));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'rekDebit',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/RekeningAkuntansiDebit'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.nmrincianobyek);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.nmrincianobyek);
                                                                $("#AKPenjaminRekM_rekening_1_rekening5_id").val(ui.item.rincianobyek_id);
                                                                $("#AKPenjaminRekM_rekening_1_rekening4_id").val(ui.item.obyek_id);
                                                                $("#AKPenjaminRekM_rekening_1_rekening3_id").val(ui.item.jenis_id);
                                                                $("#AKPenjaminRekM_rekening_1_rekening2_id").val(ui.item.kelompok_id);
                                                                $("#AKPenjaminRekM_rekening_1_rekening1_id").val(ui.item.struktur_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Rekening',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogRekDebit',),
                                        ));
                                    ?>
                             </div>
                   </div>
                    
                   <div class='control-group'>
                                  <?php echo CHtml::label('Rekening Kredit','rekening kredit',array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][saldonormal]','K', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][rekening5_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][rekening4_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][rekening3_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][rekening2_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKPenjaminRekM[rekening][2][rekening1_id]','', array('readonly'=>true));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'rekKredit',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/RekeningAkuntansiKredit'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.nmrincianobyek);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.nmrincianobyek);
                                                                 $("#AKPenjaminRekM_rekening_2_rekening5_id").val(ui.item.rincianobyek_id);
                                                                 $("#AKPenjaminRekM_rekening_2_rekening4_id").val(ui.item.obyek_id);
                                                                 $("#AKPenjaminRekM_rekening_2_rekening3_id").val(ui.item.jenis_id);
                                                                 $("#AKPenjaminRekM_rekening_2_rekening2_id").val(ui.item.kelompok_id);
                                                                 $("#AKPenjaminRekM_rekening_2_rekening1_id").val(ui.item.struktur_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Rekening',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogRekKredit',),
                                        ));
                                    ?>
                             </div>
                   </div>
                </td>
            </tr>
        </table>
    
<div style='max-height:400px;max-width:100%;overflow-y: scroll;align:center;margin-left:30px;'>
    <?php 
            $this->widget('ext.bootstrap.widgets.BootGridView',array(
                'id'=>'penjaminpasien-m-grid',
                'dataProvider'=>$modPenjamin->searchPenjamin(),
                'filter'=>$modPenjamin,
                'template'=>"{pager}\n{items}",
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                'columns'=>array(
                        array(
                            'header'=>'<center>Pilih</center>'.'<br/><center>'.CHtml::checkBox("cekAll","",array('onclick'=>'checkAllPenjamin();')).'</center>',
                            'type'=>'raw',
                            'value'=>'CHtml::checkBox("AKPenjaminRekM[penjamin][$data->penjamin_id][pilihPenjamin]","",array("onclick"=>"setAll();","class"=>"cekList"))',
                            'htmlOptions'=>array(
                                'style'=>'text-align:center',
                            ),
                        ),
//                        array(
//                            'header' => '<center>No</center>',
//                            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
//                            'htmlOptions'=>array(
//                                'style'=>'text-align:center',
//                            ),
//                        ),
                        array(
                            'header'=>'<center>Cara Bayar</center>',
                            'name'=>'carabayar_nama',
//                            'filter'=>CHtml::listData(CarabayarM::model()->findAll(),'carabayar_id','carabayar_nama'),
                            'value'=>'CHtml::hiddenField("AKPenjaminRekM[$data->penjamin_id][carabayar_id]", $data->carabayar_id, array("id"=>"carabayar_id","onkeypress"=>"return $(this).focusNextInputField(event)"))."".$data->carabayar->carabayar_nama',
                            'type'=>'raw',
                        ),
                        array(
                            'header'=>'<center>Penjamin</center>',
                            'name'=>'penjamin_nama',
//                            'filter'=>CHtml::listData(PenjaminpasienM::model()->findAll(),'penjamin_id','penjamin_nama'),
                            'value'=>'CHtml::hiddenField("AKPenjaminRekM[$data->penjamin_id][penjamin_id]", $data->penjamin_id, array("id"=>"penjamin_id","onkeypress"=>"return $(this).focusNextInputField(event)"))."".$data->penjamin_nama',
                            'type'=>'raw',
                        ),
                        array(
                            'header'=>'<center>Penjamin Nama Lain</center>',
                            'name'=>'penjamin_namalainnya',
//                            'filter'=>CHtml::listData(PenjaminpasienM::model()->findAll(),'penjamin_id','penjamin_nama'),
                            'value'=>'$data->penjamin_namalainnya',
                            'type'=>'raw',
                        ),

                ),
                'afterAjaxUpdate'=>'
                    function(id, data){
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                }',
        )); 
    ?>
</div>
            
	<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.$this->id.'/create'), 
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit3a',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
	</div>


<?php $this->endWidget(); ?>

     
<?php 
//========= Dialog buat cari data Rek Debit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekDebit',
    'options'=>array(
        'title'=>'Daftar Rekening Debit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekDebit = new RekeningakuntansiV('search');
$modRekDebit->unsetAttributes();
$account = "";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekDebit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'rekdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekDebit->searchAccounts($account),
	'filter'=>$modRekDebit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'name'=>'nourutrek',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Kode 1',
                    'name'=>'kdstruktur',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Kode 2',
                    'name'=>'kdkelompok',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Kode 3',
                    'name'=>'kdjenis',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Kode 4',
                    'name'=>'kdobyek',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Kode 5',
                    'name'=>'kdrincianobyek',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrincianobyek',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrincianobyeklain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rincianobyek_nb',
                    'value'=>'($data->rincianobyek_nb == "D") ? "Debit" : "Kredit"',
                      'filter'=>array('D'=>'Debit','K'=>'Kredit'),
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKPenjaminRekM_rekening_1_rekening1_id\").val(\"$data->struktur_id\");
                                                $(\"#AKPenjaminRekM_rekening_1_rekening2_id\").val(\"$data->kelompok_id\");
                                                $(\"#AKPenjaminRekM_rekening_1_rekening3_id\").val(\"$data->jenis_id\");
                                                $(\"#AKPenjaminRekM_rekening_1_rekening4_id\").val(\"$data->obyek_id\");
                                                $(\"#AKPenjaminRekM_rekening_1_rekening5_id\").val(\"$data->rincianobyek_id\");
                                                $(\"#AKPenjaminRekM_rekDebit\").val(\"$data->nmrincianobyek\");                                                
                                                $(\"#dialogRekDebit\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Debit dialog =============================
?>
        
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekKredit',
    'options'=>array(
        'title'=>'Daftar Rekening Kredit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekKredit = new RekeningakuntansiV('search');
$modRekKredit->unsetAttributes();
$account = "";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'rekkredit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts($account),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'name'=>'nourutrek',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Kode 1',
                    'name'=>'kdstruktur',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Kode 2',
                    'name'=>'kdkelompok',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Kode 3',
                    'name'=>'kdjenis',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Kode 4',
                    'name'=>'kdobyek',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Kode 5',
                    'name'=>'kdrincianobyek',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'name'=>'nmrincianobyek',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'nmrincianobyeklain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'name'=>'rincianobyek_nb',
                    'value'=>'($data->rincianobyek_nb == "D") ? "Debit" : "Kredit"',
                      'filter'=>array('D'=>'Debit','K'=>'Kredit'),
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKPenjaminRekM_rekening_2_rekening1_id\").val(\"$data->struktur_id\");
                                                $(\"#AKPenjaminRekM_rekening_2_rekening2_id\").val(\"$data->kelompok_id\");
                                                $(\"#AKPenjaminRekM_rekening_2_rekening3_id\").val(\"$data->jenis_id\");
                                                $(\"#AKPenjaminRekM_rekening_2_rekening4_id\").val(\"$data->obyek_id\");
                                                $(\"#AKPenjaminRekM_rekening_2_rekening5_id\").val(\"$data->rincianobyek_id\");
                                                $(\"#AKPenjaminRekM_rekKredit\").val(\"$data->nmrincianobyek\");
                                                $(\"#dialogRekKredit\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>

<script>
        function checkAllPenjamin() {
            if ($("#cekAll").is(":checked")) {
                $('#penjaminpasien-m-grid input[name*="pilihPenjamin"]').each(function(){
                   $(this).attr('checked',true);
                })
            } else {
               $('#penjaminpasien-m-grid input[name*="pilihPenjamin"]').each(function(){
                   $(this).removeAttr('checked');
                })
            }
            setAll();
        }
        
        function setAll(obj){
            $('.cekList').each(function(){
               if ($(this).is(':checked')){

                    $(this).parents('tr').find('.cekList').val(1);
                    }else{
                        $(this).parents('tr').find('.cekList').val(0);
                    }
            });
        }
</script>