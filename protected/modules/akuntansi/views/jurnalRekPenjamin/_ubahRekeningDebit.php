<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'jenispengeluaran-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onSubmit'=>'verifikasi();'),
        'focus'=>'#',
)); ?>

<div class='divForForm'>
</div>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary($model); ?>

       <table>
            <tr>
                <td>
                   <div class="control-group">
                         <?php echo CHtml::label('Cara Bayar','', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                                echo $form->dropDownList($modPenjamin,'carabayar_id', CHtml::listData($modPenjamin->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,
                                        array('empty'=>'-- Pilih --','disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'AKPenjaminpasienM')), 
                                                    'update'=>'#'.CHtml::activeId($modPenjamin,'penjamin_id').''  //selector to update
                                                ),
                                    )); 
                            ?>
                        </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Penjamin','', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                                echo $form->dropDownList($modPenjamin,'penjamin_id', CHtml::listData($modPenjamin->getPenjaminItems($modPenjamin->carabayar_id), 'penjamin_id', 'penjamin_nama') ,
                                        array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'disabled'=>true)); 
                            ?>
                        </div>
                    </div>
                    
                     <div class='control-group'>
                                  <?php echo CHtml::label('Rekening Debit','',array('class'=>'control-label')) ?>
                             <div class="controls">
                                  <?php echo CHtml::textField('debit',$model->rekeningdebit->nmrekening5,array()); ?>
                                  <?php echo CHtml::hiddenField('rekening5_id',$model->rekening5_id,array()); ?>
                                  <?php echo CHtml::hiddenField('rekening4_id',$model->rekening4_id,array()); ?>
                                  <?php echo CHtml::hiddenField('rekening3_id',$model->rekening3_id,array()); ?>
                                  <?php echo CHtml::hiddenField('rekening2_id',$model->rekening2_id,array()); ?>
                                  <?php echo CHtml::hiddenField('rekening1_id',$model->rekening1_id,array()); ?>
                                  <?php echo CHtml::hiddenField('penjamin_id',$model->penjamin_id,array()); ?>
                                  <?php echo CHtml::hiddenField('penjaminrek_id',$model->penjaminrek_id,array()); ?>
                                  <?php echo CHtml::hiddenField('saldonormal',$model->saldonormal,array()); ?>
                             </div>
                   </div>
                </td>
            </tr>
        </table>
<?php $this->endWidget(); ?>

<legend class="rim">Checklist Untuk Ubah Rekening Debit</legend>
<div style="max-width:500px;">
    <?php 
        $modRekDebit = new RekeningakuntansiV('search');
        $modRekDebit->unsetAttributes();
        $account = "D";
        if(isset($_GET['RekeningakuntansiV'])) {
            $modRekDebit->attributes = $_GET['RekeningakuntansiV'];
        }
        $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
                'id'=>'rekdebit-m-grid',
                //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
                'dataProvider'=>$modRekDebit->searchAccounts($account),
                'filter'=>$modRekDebit,
                'template'=>"{pager}{summary}\n{items}",
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                'mergeHeaders'=>array(
                    array(
                        'name'=>'<center>Kode Rekening</center>',
                        'start'=>1, //indeks kolom 3
                        'end'=>5, //indeks kolom 4
                    ),
                ),
                'columns'=>array(
                        array(
                            'header'=>'No Urut',
                            'name'=>'nourutrek',
                            'value'=>'$data->nourutrek',
                        ),
                        array(
                            'header'=>'Kode 1',
                            'name'=>'kdstruktur',
                            'value'=>'$data->kdstruktur',
                        ),
                        array(
                            'header'=>'Kode 2',
                            'name'=>'kdkelompok',
                            'value'=>'$data->kdkelompok',
                        ),
                        array(
                            'header'=>'Kode 3',
                            'name'=>'kdjenis',
                            'value'=>'$data->kdjenis',
                        ),
                        array(
                            'header'=>'Kode 4',
                            'name'=>'kdobyek',
                            'value'=>'$data->kdobyek',
                        ),
                        array(
                            'header'=>'Kode 5',
                            'name'=>'kdrincianobyek',
                            'value'=>'$data->kdrincianobyek',
                        ),
                        array(
                            'header'=>'Nama Rekening',
                            'type'=>'raw',
                            'name'=>'nmrincianobyek',
                            'value'=>'($data->nmrincianobyek == "" ? $data->nmobyek : ($data->nmobyek == "" ? $data->nmjenis : ($data->nmjenis == "" ? $data->nmkelompok : ($data->nmkelompok == "" ? $data->nmstruktur : ($data->nmstruktur == "" ? "-" : $data->nmrincianobyek)))))',
                        ),  
                        array(
                            'header'=>'Nama Lain',
                            'name'=>'nmrincianobyeklain',
                            'value'=>'($data->nmrincianobyeklain == "" ? $data->nmobyeklain : ($data->nmobyeklain == "" ? $data->nmjenislain : ($data->nmjenislain == "" ? $data->nmkelompoklain : ($data->nmkelompoklain == "" ? $data->nmstrukturlain : ($data->nmstrukturlain == "" ? "-" : $data->nmrincianobyeklain)))))',
                        ),
                        array(
                            'header'=>'Saldo Normal',
                            'name'=>'rincianobyek_nb',
                            'value'=>'($data->rincianobyek_nb == "D") ? "Debit" : "Kredit"',
                        ),

                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectRekDebit",
                                            "onClick" =>"
                                                        $(\"#rekening5_id\").val(\"$data->rincianobyek_id\");
                                                        $(\"#rekening4_id\").val(\"$data->obyek_id\");
                                                        $(\"#rekening3_id\").val(\"$data->jenis_id\");
                                                        $(\"#rekening2_id\").val(\"$data->kelompok_id\");
                                                        $(\"#rekening1_id\").val(\"$data->struktur_id\");
                                                        $(\"#debit\").val(\"$data->namaRekening\");  
                                                        saveDebit();
                                                        return false;
                                    "))',
                        ),
                ),
                'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        ));
    ?>
</div>
  
<script>
    function verifikasi(){
        if(confirm('<?php echo Yii::t('mds','Yakin Anda akan Ubah Data Rekening?') ?>'))
        {
            $('#dialogUbahRekeningDebitKredit').dialog('close');
        }
        else
        {   
            $('#submit').submit();
            return false;
        }
    }
    
</script>
<?php
$urlEditDebit = Yii::app()->createUrl('akuntansi/actionAjax/getRekeningEditDebitPenjamin');//MAsukan Dengan memilih Rekening
$mds = Yii::t('mds','Anda yakin akan ubah data rekening kredit ?');
$jscript = <<< JS

function saveDebit()
{
    rekening1_id = $('#rekening1_id').val();
    rekening2_id = $('#rekening2_id').val();
    rekening3_id = $('#rekening3_id').val();
    rekening4_id = $('#rekening4_id').val();
    rekening5_id = $('#rekening5_id').val();
    saldonormal = $('#saldonormal').val();
    penjamin_id = $('#penjamin_id').val();
    penjaminrek_id = $('#penjaminrek_id').val();


    if(confirm("${mds}"))
    {
        $.post("${urlEditDebit}", {rekening1_id:rekening1_id, rekening2_id:rekening2_id, rekening3_id:rekening3_id, rekening4_id:rekening4_id, rekening5_id:rekening5_id, penjamin_id:penjamin_id, saldonormal:saldonormal,penjaminrek_id:penjaminrek_id},
            function(data){
                $('.divForForm').html(data.pesan);
                setTimeout(function(){
                    $("#iframeEditRekeningDebitKredit").attr("src",$(this).attr("href"));
                    window.parent.$("#dialogUbahRekeningDebitKredit").dialog("close");
                    setInterval("location.refresh(true);",1000);
                    return true;
                },500);
        }, "json");
    }
}
    
JS;
Yii::app()->clientScript->registerScript('penjaminRek',$jscript, CClientScript::POS_HEAD);
?>