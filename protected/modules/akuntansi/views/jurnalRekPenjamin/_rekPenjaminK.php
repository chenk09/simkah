<?php
    $modRekPenjamin = PenjaminrekM::model()->findAllByAttributes(array('penjamin_id'=>$penjamin_id,'saldonormal'=>$saldonormal));
    
    if(COUNT($modRekPenjamin)>0)
    {   
        foreach($modRekPenjamin as $i=>$data)
        {
            if(isset($_GET['caraPrint'])){
                echo "<pre>";
                echo $data->rekeningdebit->nmrekening5;
                echo "</pre>";
            }else{                
                echo "<pre>";
                echo $data->rekeningdebit->nmrekening5.CHtml::Link("<i class=\"icon-pencil\"></i>",
                                Yii::app()->controller->createUrl("jurnalRekPenjamin/ubahRekeningKredit",array("id"=>$data->penjaminrek_id)),
                                array("class"=>"", 
                                      "target"=>"iframeEditRekeningDebitKredit",
                                      "onclick"=>"$(\"#dialogUbahRekeningDebitKredit\").dialog(\"open\");",
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk ubah Rekening Debit",
                                ));
                echo "</pre>";
            }
        }
    }
    else
    {
        echo Yii::t('zii','Not set'); 
    }   
?>