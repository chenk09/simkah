<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'jurnalpenerimaankas-search',
        'type'=>'horizontal',
        'htmlOptions'=>array(
                'onKeyPress'=>'return disableKeyPress(event)'
        ),
        'focus'=>'#',
)); ?>

<table>
    <tr>
        <td width="50%">
            <div class="control-group ">
                <?php echo $form->labelEx($model,'tglAwal',array('class'=>'control-label'));?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAwal',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'readonly'=>true
                            ),
                        ));
                    ?>

                </div>
            </div>
            <div class="control-group ">
                <label class="control-label" for="AKRincianpenerimaankasrekeningV_tglAkhir">Sampai Dengan</label>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAkhir',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_TIME_FORMAT,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
                                'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'readonly'=>true
                            ),
                        ));
                    ?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'nobuktibayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <div class="control-group ">
            <?php echo $form->labelEx($model,'jenis_id', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php echo $form->hiddenField($model,'jenis_id') ?>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$model,
                                    'attribute'=>'jenisNama',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('akuntansi/ActionAutoComplete/jenisPenerimaan').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                               $(this).val("");
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#AKRincianpenerimaankasrekeningV_jenisNama").val(ui.item.value);
                                                $("#AKRincianpenerimaankasrekeningV_jenis_id").val(ui.item.jenispenerimaan_id);
                                                return false;
                                            }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogJenisPenerimaan','idTombol'=>'tombolJenisPenerimaanDialog'),
                                    'htmlOptions'=>array('placeholder'=>'ketik Kode/Nama Jenis Penerimaan'),
                                )); 
                ?>
            </div>
        </div>
        </td>
    </tr>
</table>
<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('id'=>'btn_submit','class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>'addDetail()')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('id'=>'btn_resset','class'=>'btn btn-danger', 'type'=>'reset')); ?>    
</div>
<?php $this->endWidget();?>   
<?php 
//========= Dialog buat cari data Jenis Penerimaan =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogJenisPenerimaan',
    'options'=>array(
        'title'=>'Pencarian Jenis Penerimaan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
$modJenispenerimaan = new JenispenerimaanM('searchJenisPenerimaan');
$modJenispenerimaan->unsetAttributes();
if(isset($_GET['JenispenerimaanM'])) {
    $modJenispenerimaan->attributes = $_GET['JenispenerimaanM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-t-grid',
	'dataProvider'=>$modJenispenerimaan->searchJenisPenerimaan(),
	'filter'=>$modJenispenerimaan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPendaftaran",
                                    "onClick" => "
                                        $(\"#dialogJenisPenerimaan\").dialog(\"close\");
                                        $(\"#AKRincianpenerimaankasrekeningV_jenis_id\").val(\"$data->jenispenerimaan_id\");
                                        $(\"#AKRincianpenerimaankasrekeningV_jenisNama\").val(\"$data->jenispenerimaan_kode - $data->jenispenerimaan_nama\");
                                    "))',
                ),
                'jenispenerimaan_kode',
                'jenispenerimaan_nama',
                'jenispenerimaan_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>