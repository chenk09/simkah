<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<fieldset>
<legend class="rim2">Daftar Rekening</legend>
    <div style="max-width:1300px;overflow:auto;">
        <?php
            $this->widget('ext.bootstrap.widgets.BootGridView',
                array(
                    'id'=>'AKRekeningakuntansi-v',
                    'dataProvider'=>$model->searchByFilter(),
                    'filter'=>$model,
                    'template'=>"{pager}{summary}\n{items}",
                    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'columns'=>array(
                        array(
                          'header'=>'No',
                          'type'=>'raw',
                          'value'=>'$row+1',
                          'htmlOptions'=>array('style'=>'width:20px')
                        ),
                        array(
                           'name'=>'kdstruktur',
                           'type'=>'raw',
                           'value'=>'$data->kdstruktur',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px;')
                        ),
                        array(
                           'name'=>'kdkelompok',
                           'type'=>'raw',
                           'value'=>'$data->kdkelompok',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdjenis',
                           'type'=>'raw',
                           'value'=>'$data->kdjenis',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdobyek',
                           'type'=>'raw',
                           'value'=>'($data->kdobyek == null ? "-" : $data->kdobyek)',
                           'htmlOptions'=>array('style'=>'text-align: center; width:50px')
                        ),
                        array(
                           'name'=>'kdrincianobyek',
                           'type'=>'raw',
                            'value'=>'($data->kdrincianobyek == null ? "-" : $data->kdrincianobyek)',
                           'htmlOptions'=>array('style'=>'text-align: center; width:80px')
                        ),
                        array(
                           'name'=>'nmstruktur',
                           'type'=>'raw',
                           'value'=>'isset($data->nmstruktur) ? CHtml::Link($data->nmstruktur, Yii::app()->controller->createUrl("KodeRekening/editStrukturRekening",array("id"=>$data->struktur_id)),array("style"=>"color:blue","target"=>"frameEditStruktur", "onclick"=>"$(\"#dialogEditStruktur\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik Untuk Ubah<br>Struktur Rekening",)) : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmkelompok',
                           'type'=>'raw',
                           'value'=>'isset($data->nmkelompok) ? CHtml::Link($data->nmkelompok, Yii::app()->controller->createUrl("KodeRekening/editKelompokRekening",array("id"=>$data->kelompok_id)),array("style"=>"color:blue","target"=>"frameEditKelompokRek", "onclick"=>"$(\"#dialogEditKelompokRek\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik Untuk<br>Ubah Kelompok Rekening",)) : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmjenis',
                           'type'=>'raw',
                           'value'=>'isset($data->nmjenis) ? CHtml::Link($data->nmjenis, Yii::app()->controller->createUrl("KodeRekening/editJenisRekening",array("id"=>$data->jenis_id)),array("style"=>"color:blue","target"=>"frameEditKelompokRek", "onclick"=>"$(\"#dialogEditKelompokRek\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik Untuk Ubah<br>Jenis Rekening",)) : "-"',
                           'htmlOptions'=>array('style'=>'width:80px')
                        ),
                        array(
                           'name'=>'nmobyek',
                           'type'=>'raw',
                           'value'=>'isset($data->nmobyek) ? CHtml::Link($data->nmobyek, Yii::app()->controller->createUrl("KodeRekening/editObyekRekening",array("id"=>$data->obyek_id)),array("style"=>"color:blue","target"=>"frameEditObyekRek", "onclick"=>"$(\"#dialogEditObyekRek\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik Untuk Ubah<br>Obyek Rekening",)) : "-"',
                        ),
                        array(
                           'name'=>'nmrincianobyek',
                           'type'=>'raw',
                           'value'=>'isset($data->nmrincianobyek) ? CHtml::Link($data->nmrincianobyek, Yii::app()->controller->createUrl("KodeRekening/editRincianObyekRek",array("id"=>$data->rincianobyek_id)),array("style"=>"color:blue","target"=>"frameEditRincianObyekRek", "onclick"=>"$(\"#dialogEditRincianObyekRek\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik Untuk Ubah<br>Rincian Obyek Rekening",)) : "-"',
                        ),
                        array(
                           'header'=>'Saldo Normal',
                           'name'=>'rincianobyek_nb',
                           'type'=>'raw',
                            'value'=>'($data->rincianobyek_nb == null ? "-" : ($data->rincianobyek_nb == "D" ? "Debit" : "Kredit"))',
                        ),
//                        array(
//                           'name'=>'kelompokrek',
//                           'type'=>'raw',
//                            'value'=>'($data->kelompokrek == null ? "-" : $data->kelompokrek)',
//                        ),
                        array(
                           'name'=>'keterangan',
                           'type'=>'raw',
                            'value'=>'($data->keterangan == null ? "-" : $data->keterangan)',
                        ),
                        array(
                           'header'=>'Status',
                           'type'=>'raw',
                           'value'=>'($data->rincianobyek_aktif == null ? "-" : ($data->rincianobyek_aktif == true ? "Aktif" : "Non Aktif"))',
                           'htmlOptions'=>array('style'=>'text-align: center; width:80px')
                        ),
                       array(
                          'header'=>Yii::t('zii','Delete'),
                          'class'=>'bootstrap.widgets.BootButtonColumn',
                          'template'=>'{trash}',
                          'buttons'=>array(                                        
                                   'trash' => array (
                                      'label'=>"<i class='icon-trash'></i>",
                                      'options'=>array('title'=>Yii::t('mds','Delete')),
                                      'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/delete",array("rekening5_id"=>"$data->rincianobyek_id"))',           
                                  ),
                    )
                ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                )
            );
        ?>
    </div>
</fieldset>
<?php 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
?>
<?php
        $content = $this->renderPartial('../tips/master',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        
        
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#penjaminpasien-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);    
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditStruktur',
        'options' => array(
            'title' => 'Ubah Struktur Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 300,
            'resizable' => false,
            'close'=>'js:function(){getTreeMenu();$.fn.yiiGridView.update(\'AKRekeningakuntansi-v\', {});}'
        ),
    )
);
?>
<iframe name='frameEditStruktur' width="100%" height="100%"></iframe>
<?php
    $this->endWidget();
?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditKelompokRek',
        'options' => array(
            'title' => 'Ubah Kelompok Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 300,
            'resizable' => false,
            'close'=>'js:function(){getTreeMenu();$.fn.yiiGridView.update(\'AKRekeningakuntansi-v\', {});}'
        ),
    )
);
?>
<iframe name='frameEditKelompokRek' width="100%" height="100%"></iframe>
<?php
    $this->endWidget();
?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditJenisRek',
        'options' => array(
            'title' => 'Ubah Jenis Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 300,
            'resizable' => false,
            'close'=>'js:function(){getTreeMenu();$.fn.yiiGridView.update(\'AKRekeningakuntansi-v\', {});}'
        ),
    )
);
?>
<iframe name='frameEditJenisRek' width="100%" height="100%"></iframe>
<?php
    $this->endWidget();
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditObyekRek',
        'options' => array(
            'title' => 'Ubah Obyek Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 300,
            'resizable' => false,
            'close'=>'js:function(){getTreeMenu();$.fn.yiiGridView.update(\'AKRekeningakuntansi-v\', {});}'
        ),
    )
);
?>
<iframe name='frameEditObyekRek' width="100%" height="100%"></iframe>
<?php
    $this->endWidget();
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', 
    array(
        'id' => 'dialogEditRincianObyekRek',
        'options' => array(
            'title' => 'Ubah Rincian Obyek Rekening',
            'autoOpen' => false,
            'modal' => true,
            'width' => 550,
            'height' => 450,
            'resizable' => false,
            'close'=>'js:function(){getTreeMenu();$.fn.yiiGridView.update(\'AKRekeningakuntansi-v\', {});}'
        ),
    )
);
?>
<iframe name='frameEditRincianObyekRek' width="100%" height="100%"></iframe>
<?php
    $this->endWidget();
?>


<script type="text/javascript">
function deleteRecord(id){
        var id = id;
        var url = '<?php echo $url."/delete"; ?>';
        var answer = confirm('Yakin Akan Menghapus Data ini ?');
            if (answer){
                 $.post(url, {id: id},
                     function(data){
                        if(data.status == 'proses_form'){
                                $.fn.yiiGridView.update('lookup-m-grid');
                            }else{
                                alert('Data Gagal di Hapus')
                            }
                },"json");
           }
    }
    
</script>