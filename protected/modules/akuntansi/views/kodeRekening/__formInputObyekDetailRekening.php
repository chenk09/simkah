<fieldset id='fieldsetDetailObyekRekening'>
    <?php if(isset($_GET['id'])){ ?>    
    <?php }else{ ?>
    <legend class="rim">Tambah Detail Obyek Rekening</legend>
    <?php } ?>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'id'=>'form-detail-obyek-rekening',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)'
        ),
        'focus'=>'#',
    )
);
$this->widget('bootstrap.widgets.BootAlert');
?>
<?php echo $form->hiddenField($model,'rekening1_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening2_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening3_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening4_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening5_id', array('class'=>'span1')); ?>
<div class="control-group ">
    <label class="control-label required" for="AKRekening5M_kdrekening5">Kode Rekening&nbsp;<span class="required">*</span></label>
    <div class="controls">
        <?php echo $form->textField($model,'kdrekening1',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening2',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening3',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening4',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening5',array('class'=>'span1 reqForm', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>false)); ?>
    </div>
</div>
<?php echo $form->textFieldRow($model,'nourutrek',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>false)); ?>
<?php echo $form->textFieldRow($model,'nmrekening5',array('class'=>'span4 reqForm', 'onkeyup'=>'autoInput();','onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
<?php echo $form->textFieldRow($model,'nmrekeninglain5',array('class'=>'span4 reqForm', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
<?php echo $form->dropDownListRow($model,'rekening5_nb', JenisRekening::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
<?php echo $form->textAreaRow($model,'keterangan',array('class'=>'span4', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>false)); ?>
<?php echo $form->dropDownListRow($model,'tiperekening_id', TiperekeningM::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
<?php echo $form->dropDownListRow($model,'kelompokrek', KelompokRekening::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
<?php if(!empty($_GET['id'])){
        echo $form->radioButtonListInlineRow($model, 'rekening5_aktif', array('Tidak', 'Aktif'), array('onkeypress'=>"return $(this).focusNextInputField(event)")); 
      }
?>
<div class="form-actions">
    <?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
        echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), array('style'=>'display:none','id' => 'reseter', 'class'=>'btn btn-danger', 'type'=>'reset'));
    ?>
</div>
    
<?php
    $urlPostData = Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/SimpanRekening');
?>
    
<script type="text/javascript">    
    $('#form-detail-obyek-rekening').submit(function(){
        var kosong = "";
        var jumlahKosong = $("#fieldsetDetailObyekRekening").find(".reqForm[value="+ kosong +"]");
        if(jumlahKosong.length > 0){
            alert('Inputan bertanda bintang harap di isi !!');
        }else{
            
            $.post("<?php echo $urlPostData;?>", {data:$(this).serialize()},
                function(data){
                    if(data.pesan == 'exist'){
                        alert('Kode Rekening telah terdaftar');
                    }
                    
                    if(data.status == 'ok'){
                        alert('Rekening berhasil disimpan');
                        if(data.pesan == 'insert'){
                            $("#reseter").click();
                            $('#fieldsetDetailObyekRekening').find("input[name$='[kdrekening1]']").val(data.id_parent.kdrekening1);
                            $('#fieldsetDetailObyekRekening').find("input[name$='[kdrekening2]']").val(data.id_parent.kdrekening2);
                            $('#fieldsetDetailObyekRekening').find("input[name$='[kdrekening3]']").val(data.id_parent.kdrekening3);
                            $('#fieldsetDetailObyekRekening').find("input[name$='[kdrekening4]']").val(data.id_parent.kdrekening4);
                            $('#fieldsetDetailObyekRekening').find("input[name$='[kdrekening5]']").val(data.id_parent.kdrekening5);                            
                        }
                        getTreeMenu();
                        $.fn.yiiGridView.update('AKRekeningakuntansi-v', {});
                        
                    }
                }, "json"
            );
        }
        return false;
    });
    function autoInput(){
        var namaRekening = $('#AKRekening5M_nmrekening5').val();
        
        $('#AKRekening5M_nmrekeninglain5').val(namaRekening);
    }
</script>

<?php $this->endWidget();?>