<fieldset id='fieldsetObyekRekening'>
    <?php if(isset($_GET['id'])){ ?>    
    <?php }else{ ?>
    <legend class="rim">Tambah Obyek Rekening</legend>
    <?php } ?>
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'id'=>'form-obyek-rekening',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)'
        ),
        'focus'=>'#',
    )
);
$this->widget('bootstrap.widgets.BootAlert');
?>
<?php echo $form->hiddenField($model,'rekening1_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening2_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening3_id', array('class'=>'span1')); ?>
<?php echo $form->hiddenField($model,'rekening4_id', array('class'=>'span1')); ?>
<div class="control-group ">
    <label class="control-label required" for="AKRekening4M_kdrekening4">Kode Rekening&nbsp;<span class="required">*</span></label>
    <div class="controls">
        <?php echo $form->textField($model,'kdrekening1',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening2',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening3',array('class'=>'span1', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>true)); ?>
        <?php echo $form->textField($model,'kdrekening4',array('class'=>'span1 reqForm', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>false)); ?>
    </div>
</div>
<?php echo $form->textFieldRow($model,'nmrekening4',array('class'=>'span3 reqForm', 'onkeyup'=>'autoInput();', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
<?php echo $form->textFieldRow($model,'nmrekeninglain4',array('class'=>'span3 reqForm', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
<?php echo $form->dropDownListRow($model,'rekening4_nb', JenisRekening::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
<?php if(!empty($_GET['id'])){
        echo $form->radioButtonListInlineRow($model, 'rekening4_aktif', array('Tidak', 'Aktif'), array('onkeypress'=>"return $(this).focusNextInputField(event)"));
      }
?>
<div class="form-actions">
    <?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
        echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), array('style'=>'display:none','id' => 'reseter', 'class'=>'btn btn-danger', 'type'=>'reset'));
    ?>
</div>
    
<?php
    $urlPostData = Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/SimpanRekening');
?>
    
<script type="text/javascript">    
    $('#form-obyek-rekening').submit(function(){
        var kosong = "";
        var jumlahKosong = $("#fieldsetJenisRekening").find(".reqForm[value="+ kosong +"]");
        if(jumlahKosong.length > 0){
            alert('Inputan bertanda bintang harap di isi !!');
        }else{
            
            $.post("<?php echo $urlPostData;?>", {data:$(this).serialize()},
                function(data){
                    if(data.pesan == 'exist'){
                        alert('Kode Rekening telah terdaftar');
                    }
                    
                    if(data.status == 'ok'){
                        alert('Rekening berhasil disimpan');
                        if(data.pesan == 'insert'){
                            $("#reseter").click();
                            $('#fieldsetObyekRekening').find("input[name$='[kdrekening1]']").val(data.id_parent.kdrekening1);
                            $('#fieldsetObyekRekening').find("input[name$='[kdrekening2]']").val(data.id_parent.kdrekening2);
                            $('#fieldsetObyekRekening').find("input[name$='[kdrekening3]']").val(data.id_parent.kdrekening3);
                            $('#fieldsetObyekRekening').find("input[name$='[kdrekening4]']").val(data.id_parent.kdrekening4);
                        }
                        
                        if(typeof getTreeMenu == 'function')
                        {
                            getTreeMenu();
                            $.fn.yiiGridView.update('AKRekeningakuntansi-v', {});
                        }
                    }
                }, "json"
            );
        }
        return false;
    });
    
    function autoInput(){
        var namaRekening = $('#AKRekening4M_nmrekening4').val();
        
        $('#AKRekening4M_nmrekeninglain4').val(namaRekening);
    }
</script>

<?php $this->endWidget();?>