
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchByFilterPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchByFilter();
         $template = "{pager}{summary}\n{items}";
    }
  ?>  

<?php
$this->widget($table,
    array(
        'id'=>'AKRekeningakuntansi-v',
        'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
              'header'=>'No',
              'type'=>'raw',
              'value'=>'$row+1',
              'htmlOptions'=>array('style'=>'width:20px'),
              'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kode Struktur',
               'type'=>'raw',
               'value'=>'$data->kdstruktur',
               'htmlOptions'=>array('style'=>'text-align: center; width:50px;'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kode Kelompok',
               'type'=>'raw',
               'value'=>'$data->kdkelompok',
               'htmlOptions'=>array('style'=>'text-align: center; width:50px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kode Jenis',
               'type'=>'raw',
               'value'=>'$data->kdjenis',
               'htmlOptions'=>array('style'=>'text-align: center; width:50px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kode Obyek',
               'type'=>'raw',
               'value'=>'($data->kdobyek == null ? "-" : $data->kdobyek)',
               'htmlOptions'=>array('style'=>'text-align: center; width:50px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kode Rincian Obyek',
               'type'=>'raw',
                'value'=>'($data->kdrincianobyek == null ? "-" : $data->kdrincianobyek)',
               'htmlOptions'=>array('style'=>'text-align: center; width:80px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),

            ),
            array(
               'header'=>'Nama Struktur',
               'type'=>'raw',
               'value'=>'isset($data->nmstruktur) ? $data->nmstruktur: "-"',
               'htmlOptions'=>array('style'=>'width:80px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Nama Kelompok',
               'type'=>'raw',
               'value'=>'isset($data->nmkelompok) ? $data->nmkelompok: "-"',
               'htmlOptions'=>array('style'=>'width:80px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Nama Jenis',
               'type'=>'raw',
               'value'=>'isset($data->nmjenis) ? $data->nmjenis: "-"',
               'htmlOptions'=>array('style'=>'width:80px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Nama Obyek',
               'type'=>'raw',
               'value'=>'isset($data->nmobyek) ? $data->nmobyek : "-"',
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Nama Rincian Obyek',
               'type'=>'raw',
               'value'=>'isset($data->nmrincianobyek) ? $data->nmrincianobyek: "-"',
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Rincian Obyek NB',
               'type'=>'raw',
               'value'=>'($data->rincianobyek_nb == null ? "-" : ($data->rincianobyek_nb == "D" ? "Debit" : "Kredit"))',
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Kelompok Rekening',
               'type'=>'raw',
               'value'=>'($data->kelompokrek == null ? "-" : $data->kelompokrek)',
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'keterangan',
               'type'=>'raw',
               'value'=>'($data->keterangan == null ? "-" : $data->keterangan)',
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
               'header'=>'Status',
               'type'=>'raw',
               'value'=>'($data->rincianobyek_aktif == null ? "-" : ($data->rincianobyek_aktif == true ? "Aktif" : "Non Aktif"))',
               'htmlOptions'=>array('style'=>'text-align: center; width:80px'),
               'headerHtmlOptions'=>array('style'=>'text-align:center;'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )
);
?>