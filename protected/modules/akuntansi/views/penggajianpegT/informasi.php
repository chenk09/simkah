<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$url = Yii::app()->createUrl('akuntansi/penggajianpegT/frameGrafikLaporanBukuBesar&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<fieldset>
    <legend class="rim2">Laporan Penggajian Karyawan</legend>
</fieldset>



<?php
$this->breadcrumbs=array(
	'Kppenggajianpeg Ts'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kppenggajianpeg-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'kppenggajianpeg-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                   // 'header'=>'NIP',
                    'name'=>'nomorindukpegawai',
                    'value'=>'$data->nomorindukpegawai',
                ),
                array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'nama_pegawai',
                    'value'=>'$data->nama_pegawai',
                ),
              array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'periodegaji',
                    'value'=>'$data->periodegaji',
                ),
              
                 array(
                    'header'=>'Jabatan',
                    'value'=>'(isset($data->pegawai->jabatan->jabatan_nama) ? $data->pegawai->jabatan->jabatan_nama : "-")',
                ),
                 //array(
                    //'header'=>'No Rekening',
                    //'value'=>'$data->pegawai->norekening',
                //),
                array(
                    'header'=>'NPWP',
                    'value'=>'$data->pegawai->npwp',
                ),
		'tglpenggajian',
		'nopenggajian',
		'keterangan',
		'mengetahui',
            
                 array(
                    'header'=>'Status Pembayaran',
                    'value'=>'$data->statusbayar',
                ),
            
//            array(
//                        'header'=>'Rincian Gaji',
//                        'type'=>'raw',
//                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                        'value'=>'CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("printrincian",array("id"=>$data->penggajianpeg_id,"frame"=>true)),
//                                    array("class"=>"", 
//                                          "target"=>"iframeRincianGaji",
//                                          "onclick"=>"$(\"#dialogRincianGaji\").dialog(\"open\");",
//                                          "rel"=>"tooltip",
//                                          "title"=>"Klik untuk melihat Rincian Gaji",
//                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
//                    ),
//                
	
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->renderPartial('_search',array('model'=>$model)); ?>

<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//        $this->widget('TipsMasterData',array('type'=>'admin'));
//        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
//        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
//
//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//    window.open("${urlPrint}/"+$('#kppenggajianpeg-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianGaji',
    'options'=>array(
        'title'=>'Rincian Gaji',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianGaji" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>