<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->search();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php 

    $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        
	'columns'=>array(
           array(
                   // 'header'=>'NIP',
                    'name'=>'nomorindukpegawai',
                    'value'=>'$data->nomorindukpegawai',
                ),
                array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'nama_pegawai',
                    'value'=>'$data->nama_pegawai',
                ),
              array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'periodegaji',
                    'value'=>'$data->periodegaji',
                ),
              
                 array(
                    'header'=>'Jabatan',
                    'value'=>'(isset($data->pegawai->jabatan->jabatan_nama) ? $data->pegawai->jabatan->jabatan_nama : "-")',
                ),
                 //array(
                    //'header'=>'No Rekening',
                    //'value'=>'$data->pegawai->norekening',
                //),
                array(
                    'header'=>'NPWP',
                    'value'=>'$data->pegawai->npwp',
                ),
		'tglpenggajian',
		'nopenggajian',
		'keterangan',
		'mengetahui',
            
                 array(
                    'header'=>'Status Pembayaran',
                    'value'=>'$data->statusbayar',
                ),
            
//            array(
//                        'header'=>'Rincian Gaji',
//                        'type'=>'raw',
//                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                        'value'=>'CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("printrincian",array("id"=>$data->penggajianpeg_id,"frame"=>true)),
//                                    array("class"=>"", 
//                                          "target"=>"iframeRincianGaji",
//                                          "onclick"=>"$(\"#dialogRincianGaji\").dialog(\"open\");",
//                                          "rel"=>"tooltip",
//                                          "title"=>"Klik untuk melihat Rincian Gaji",
//                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
//                    ),
//                
	
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 
?>


