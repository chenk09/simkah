<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'GET',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'
            ),
        )
    );
?>
<div>
<fieldset>
    <table>
        <tr>   
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Tgl Bukti Bayar', 'tblbuktibayar',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'name' => 'Filter[tglAwal]',
                                                'model'=>$modTandabukti,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
            //                                    'onchange'=>'ajaxGetList()',
                                                ),
                                )); 
                            ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Bank', 'bankkartu',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php
                                echo CHtml::activeTextField($modTandabukti, 'bankkartu',array('class'=>'span3','placeholder'=>'Ketik Nama Bank'));
                           ?>
                    </div>
                </div>
            </td>           
        </tr>
        <tr>
             <td>
                <div class="control-group">
                    <?php echo CHtml::label('Sampai Dengan', 'sampai dengan',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'name' => 'Filter[tglAkhir]',
                                                'model'=>$modTandabukti,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
            //                                    'onchange'=>'ajaxGetList()',
                                                ),
                                )); 
                            ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset> 
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onClick'=>'ajaxGetList();')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onClick'=>'onReset()')); ?>
</div>
    
<?php
    $this->endWidget();
?>