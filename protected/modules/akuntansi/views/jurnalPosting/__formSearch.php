<fieldset id="frmSearchJurnalRek">
    <legend class="rim2">Pencarian Posting</legend>
    <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
            array(
                'id'=>'form-search-jurnal-rek',
                'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'htmlOptions'=>array(
                    'onKeyPress'=>'return disableKeyPress(event)'
                ),
                'focus'=>'#',
            )
        );
    ?>
    <table width="50">
        <tr>
            <td>
                <div class="control-group ">
                    <label class="control-label" for="AKJurnalrekeningblmpostingV_tgl_akhir">Tgl Bukti Jurnal</label>
                    <div class="controls">
                        <?php   
                        // $model->tgl_awal = $format->formatDateINAShort($model->tgl_awal);
                            $this->widget('MyDateTimePicker',array(
                                'model'=>$model,
                                'attribute'=>'tgl_awal',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions'=>array(
                                    'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                ),
                            ));
                        
                        ?>

                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="AKJurnalrekeningblmpostingV_tgl_akhir">Sampai Dengan</label>
                    <div class="controls">
                        <?php   
                        // $model->tgl_akhir = $format->formatDateINAShort($model->tgl_akhir);

                            $this->widget('MyDateTimePicker',array(
                                'model'=>$model,
                                'attribute'=>'tgl_akhir',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions'=>array(
                                    'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                ),
                            ));
                            
                        ?>
                    </div>
                </div>
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'nobuktijurnal',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
                <?php echo $form->textFieldRow($model,'kodejurnal',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false)); ?>
                <?php echo $form->dropDownListRow($model,'jenisjurnal_id', CHtml::listData(JenisjurnalM::model()->findAll('jenisjurnal_aktif = true'), 'jenisjurnal_id', 'jenisjurnal_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('id'=>'btn_submit','class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('id'=>'btn_resset','class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
    <?php $this->endWidget();?>   
    <div id="loading">Proses... 
        <img align="left" alt="Gambar1" src="images/ajax-loader.gif" />  
    </div> 
</fieldset>

