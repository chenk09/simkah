<?php
	$this->breadcrumbs=array(
		'Jurnal Posting',
	);

//	Yii::app()->clientScript->registerScript('search', "
//	$('#form-search-jurnal-rek').submit(function()
//	{
//	    $.fn.yiiGridView.update('jurnalposting-m-grid', {
//	        data: $(this).serialize()
//	    });
//	    return false;
//	});
//	$('#btn_resset').click(function()
//	{
//	    setTimeout(function(){
//	        $.fn.yiiGridView.update('jurnalposting-m-grid', {
//	            data: $('#form-search-jurnal-rek').serialize()
//	        });    
//	    }, 1000);
//	});
//	");

	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
	$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
	    array(
	        'action'=>Yii::app()->createUrl($this->route),
	        'method'=>'get',
	        'id'=>'form-search-jurnal-rek',
	        'type'=>'horizontal',
	    )
	);
?>
<legend class="rim">Pencarian</legend>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td>
            <div class="control-group ">
                <?php // $model->tgl_awal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tgl_awal, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Tanggal Bukti Jurnal','tglbuktijurnal', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tgl_awal',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php // $model->tgl_akhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tgl_akhir, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tgl_akhir',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                            'minDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                        )); 
                    ?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'nobuktijurnal',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false));
             echo $form->textFieldRow($model,'kodejurnal',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>32,'readonly'=>false));
            echo $form->dropDownListRow($model,'jenisjurnal_id', CHtml::listData(JenisjurnalM::model()->findAll('jenisjurnal_aktif = true'), 'jenisjurnal_id', 'jenisjurnal_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
</div>
<?php $this->endWidget(); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'jurnalposting-m-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'jurnalposting-m-grid',
	'dataProvider'=>$model->searchWithJoin(),
                'template'=>"{pager}{summary}\n{items}",
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
            'header'=> 'Pilih',
            'type'=>'raw',
            'value'=>'
                CHtml::activeHiddenField($data, \'[\'.$data->jurnaldetail_id.\']jurnaldetail_id\').
                CHtml::checkBox(\'AKJurnalrekeningblmpostingV[\'.$data->jurnaldetail_id.\'][cekList]\', true, array(\'onclick\'=>\'setUrutan(this)\', \'class\'=>\'cekList;\'));
                ',
        ),
        array(
            'header'=>'Tgl Jurnal',
            'type'=>'raw',
            'value'=>'$data->tglbuktijurnal',
        ),
        'nobuktijurnal',
        'kodejurnal',
        array(
            'header'=>'Uraian Jurnal',
            'type'=>'raw',
            'value'=>'CHtml::activeHiddenField($data, \'[\'.$data->urianjurnal.\']urianjurnal\'). $data->urianjurnal',
        ),
        'KodeRekening',
        array(
            'header'=>'Nama Rekening',
            'type'=>'raw',
            'value'=>'$data->namarekening',
            'footerHtmlOptions'=>array('colspan'=>7,'style'=>'text-align:right;font-style:italic;'),
                'footer'=>'Saldo',
        ),
        array(
            'header'=>'Debit',
            'name'=>'saldodebit',
            'value'=>'MyFunction::formatNumber($data->saldodebit)',
            'htmlOptions'=>array('style'=>'width:100px;text-align:right'),
            'footerHtmlOptions'=>array('style'=>'text-align:right;'),
            'footer'=>'sum(saldodebit)',
        ),
        array(
            'header'=>'Kredit',
            'name'=>'saldokredit',
            'value'=>'MyFunction::formatNumber($data->saldokredit)',
            'htmlOptions'=>array('style'=>'width:100px;text-align:right'),
            'footerHtmlOptions'=>array('style'=>'text-align:right;'),
            'footer'=>'sum(saldokredit)',
        ),
        'catatan',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>


<div class="form-actions">
    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/matauangM/admin'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    <?php
        $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
    ?>
</div>

<script type="text/javascript">
    function setUrutan(obj)
    {
        var nilai = $(obj).val();
        if(nilai){
            $(obj).val('');
        }else{
            $(obj).val(1);
        }
    }
</script>

<?php $this->endWidget(); ?>