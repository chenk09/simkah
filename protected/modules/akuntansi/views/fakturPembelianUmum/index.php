<?php
$this->breadcrumbs=array(
	'Informasi Faktur Pembelian Umum',
);

Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('fakturpembelianumum-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Faktur Pembelian Umum</legend>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'fakturpembelianumum-t-search',
        'type'=>'horizontal',
)); ?>
    <?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'fakturpembelianumum-m-grid',
	'dataProvider'=>$modFaktur->search(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tglterima',
                'nopenerimaan',
                'supplier_nama',
                'nofaktur',
                'tgljatuhtempo',
                array(
                    'header'=>'Keterangan Persediaan',
                    'type'=>'raw',
                    'value'=>'$data->keterangan_persediaan',
                    'footer'=>'Total Hutang :',
                    'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Total Harga',
                    'name'=>'totalhargabruto',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalhargabruto)',
                    'footer'=>'sum(totalhargabruto)',
                    'footerHtmlOptions'=>array('style'=>'text-align:left;'),
                ),
                array(
                    'header'=>'Jumlah Discount',
                    'name'=>'jmldiscount',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->jmldiscount)',   
                    'footer'=>'-',
                    'footerHtmlOptions'=>array('style'=>'text-align:left;color:white;'),
                ),
                array(
                    'header'=>'Total Pajak PPH',
                    'name'=>'totalpajakpph',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalpajakpph)',
                    'footer'=>'-',
                    'footerHtmlOptions'=>array('style'=>'text-align:left;color:white;'),
                ),
                array(
                    'header'=>'Total Pajak',
                    'name'=>'totalpajakppn',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->totalpajakppn)',
                    'footer'=>'-',
                    'footerHtmlOptions'=>array('style'=>'text-align:left;color:white;'),
                ),            
                // array(//Details ini langsung terhubung ke details Faktur d peneriaam Items supaya mudah memaintenance karena 1 view dan action 
                //    'header'=>'Details',
                //    'type'=>'raw',
                //    'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->createUrl("billingKasir/DaftarFakturPembelian/detailsFaktur",array("idFakturPembelian"=>$data->fakturpembelian_id)) ,array("title"=>"Klik Untuk Melihat Detail Faktur","target"=>"iframe", "onclick"=>"$(\"#dialogDetailsFaktur\").dialog(\"open\");", "rel"=>"tooltip"))',
                //     ),
               array( 
                   'header'=>'Bayar ke Supplier',
                   'type'=>'raw',
                   'value'=>'((empty($data->bayarkesupplier_id)) ? CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->createUrl("billingKasir/pembayaranSupplier/indexGudangUmum",array("frame"=>1,"idFakturPembelian"=>$data->terimapersediaan_id,"fakturId"=>$data->fakturpembelian_id)) ,array("title"=>"Klik Untuk Membayar ke Supplier","target"=>"iframeRetur", "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");", "rel"=>"tooltip")) : "Lunas")',
                   'footer'=>'-',
                    'footerHtmlOptions'=>array('style'=>'text-align:left;color:white;'),
                    ),  
            // array( 
            //        'header'=>'Retur',
            //        'type'=>'raw',
            //        'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/retur",array("idFakturPembelian"=>$data->fakturpembelian_id)) ,array("title"=>"Klik Untuk Melihat Detail Faktur","target"=>"iframeRetur", "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");", "rel"=>"tooltip"))',
            //     ),    
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php //$modFaktur->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modFaktur->tglAwal, 'yyyy-MM-dd H:i:s'),'long',null); ?>
                <?php echo CHtml::label('Tgl Terima','InformasifakturgudangumumV_tglAwal', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modFaktur,
                                        'attribute'=>'tglAwal',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php //$modFaktur->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modFaktur->tglAkhir, 'yyyy-MM-dd H:i:s'),'long',null); ?>
                <?php echo CHtml::label('Sampai Dengan','InformasifakturgudangumumV_tglAkhir', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modFaktur,
                                        'attribute'=>'tglAkhir',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                </div>
            </div> 
            
                
            <div class="control-group ">
                <?php //$modFaktur->tglAwalJatuhTempo = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modFaktur->tglAwalJatuhTempo, 'yyyy-MM-dd'),'medium',null); ?>
                <label class="control-label">
                    <?php echo CHtml::checkBox('berdasarkanJatuhTempo','',array('uncheckValue'=>0)); ?>
                    Tgl Jatuh Tempo
                </label>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modFaktur,
                                        'attribute'=>'tglAwalJatuhTempo',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php //$modFaktur->tglAkhirJatuhTempo = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modFaktur->tglAkhirJatuhTempo, 'yyyy-MM-dd'),'medium',null); ?>
                <?php echo CHtml::label('Sampai Dengan','InformasifakturgudangumumV_tglAkhirJatuhTempo', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$modFaktur,
                                            'attribute'=>'tglAkhirJatuhTempo',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT,
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                        )); ?>
                    </div>
            </div> 
        </td>
        <td>
              <?php echo $form->textFieldRow($modFaktur,'nopenerimaan',array('class'=>'numberOnly')); ?>
              <?php echo $form->dropDownListRow($modFaktur,'supplier_id',
                                                               CHtml::listData(SupplierM::model()->findAll(array('order'=>'supplier_nama asc')), 'supplier_id', 'supplier_nama'),
                                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                               'empty'=>'-- Pilih --',)); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
	 			<?php  
$content = $this->renderPartial('billingKasir.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
    
</div>
</fieldset>
<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenFaktur',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/fakturPembelian/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php
// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetailsFaktur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Faktur Pembelian',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframe" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRetur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Pembayaran Supplier',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                            "beforeClose"=>'js:function(){$("#divSearch-form form").submit();}'
                         ),
                    ));
?>
<iframe src="" name="iframeRetur" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

$js = <<< JSCRIPT
function formFaktur(idPenerimaan,noPenerimaan,tglPenerimaan)
{
    $('#idPenerimaanForm').val(idPenerimaan);
    $('#noPenerimaanForm').val(noPenerimaan);
    $('#tglPenerimaanForm').val(tglPenerimaan);
    $('#form_hiddenFaktur').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);