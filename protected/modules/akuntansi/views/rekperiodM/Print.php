<?php 

    $table = 'ext.bootstrap.widgets.BootGridView';
    $template = "{pager}{summary}\n{items}";
    if (isset($caraPrint)){
        $template = "{items}";
    }
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');   
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
    echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
		'perideawal',
		'sampaidgn',
		'deskripsi',
		array(
                    'header'=>'<center>Status Closing<center>',
                    'filter'=>false,
                    'name'=>'isclosing',
                    'value'=>'($data->isclosing == 1) ? "Aktif" : "Tidak Aktif"',
                    'htmlOptions'=>array('style'=>'text-align:center'),
                ),
 
        ),
    )); 
?>