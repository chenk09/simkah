<?php
$this->breadcrumbs=array(
	'Pembayaran',
);?>
<?php
//$this->widget('application.extensions.moneymask.MMask',array(
//    'element'=>'.currency',
//    'currency'=>'PHP',
//    'config'=>array(
//        'symbol'=>'Rp ',
////        'showSymbol'=>true,
////        'symbolStay'=>true,
//        'defaultZero'=>true,
//        'allowZero'=>true,
//        'precision'=>0,
//    )
//));
?>

    <legend class="rim2">Transaksi Piutang Asuransi (NEW)</legend>
    <legend class="rim"> Pencarian </legend>
        <?php 
            $this->renderPartial('_search',
                    array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modPembayaranKlaim'=>$modPembayaranKlaim,
                            'modPembayaranKlaimDetail'=>$modPembayaranKlaimDetail));
        ?>
<?php 
        Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#searchLaporan').submit(function(){
        getDataRekeningKlaim();
        updateRekening();
	return false;
});
");
        $this->widget('bootstrap.widgets.BootAlert'); 
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onSubmit'=>'return cekInputTindakan(); '),
));?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>

	
	<?php echo $form->errorSummary(array($modPembayaranKlaim,$modPembayaranKlaimDetail)); ?>
        
<fieldset>
    <legend class="rim">Tabel Pembayaran</legend>
    <!--<div style='max-height:300px;max-width:960px;overflow-y: scroll;'>-->
    <div id="tableList">
        <table id="tableList" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Rekam Medik <br/> No Pendaftaran</th>
                    <th>Nama Pasien</th>
                    <th>Alamat</th>
                    <th>Penanggung Jawab <br/> Pasien</th>
                    <th>Penjamin <br/> Pasien</th>
                    <th>No Transaksi</th>
                    <th>Jml Tagihan</th>
                    <th>Jml Telah Bayar</th>
                    <th>Jml Piutang</th>

                    <th>Jml Bayar</th>
                    <th>Sisa Tagihan</th>
                    <th>Pilih<br>
                        <?php 
                            echo CHtml::checkBox('checkPembayaran',true, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                            'class'=>'checkbox-column','onClick'=>'checkAllPembayaran()','checked'=>'checked')) 
                        ?>
                    </th>
                </tr>
            </thead>
            <tbody>  
                    <?php
                        if(isset($tr)){
                            echo $tr;
                        }
                    ?>
            </tbody>
            <tfoot>
               <tr class="trfooter">
                    <td colspan="7">Total</td>
                    <td>
                        <?php echo CHtml::textField("tottagihan", $totaltransaksi, array('readonly'=>false,'class'=>'inputFormTabel currency lebar3','style'=>'width:70px;',)); ?>
                    </td>
                    <td>
                        <?php echo CHtml::textField("tottelahbayar", $tottelahbayar, array('readonly'=>false,'class'=>'inputFormTabel currency lebar3','style'=>'width:70px;',)); ?>
                    </td>
                    <td>
                        <?php echo CHtml::textField("totpiutang", $totpiutang, array('readonly'=>false,'class'=>'inputFormTabel currency lebar3','style'=>'width:70px;',)); ?>
                    </td>
                    <td>
                        <?php echo CHtml::textField("totbayar", $totbayar, array('readonly'=>false,'class'=>'inputFormTabel currency lebar3','style'=>'width:70px;',)); ?>
                    </td>
                    <td>
                        <?php echo CHtml::textField("totsisatagihan", $totsisatagihan, array('readonly'=>false,'class'=>'inputFormTabel currency lebar3','style'=>'width:70px;',)); ?>
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
    </div>
</fieldset>
    <br><br>
<legend class="rim">Data Pembayaran</legend>
<fieldset>
    <div class="row-fluid box">
            <div class = "span4">
                <div class="control-group ">
                    <?php $modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo CHtml::label('Tgl Pembayaran','tglpembayaran', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPembayaranKlaim,
                                                'attribute'=>'tglpembayaranklaim',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('No Pembayaran <font color="red">*</font>','noPembayaran', array('class'=>'control-label inline')) ?> 
                    <div class="controls">
                        <?php echo $form->textField($modPembayaranKlaim,'nopembayaranklaim',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Cara Membayar <font color="red">*</font> ','caraMembayar', array('class'=>'control-label inline')) ?> 
                    <div class="controls">
                        <?php echo $form->dropDownList($modPembayaranKlaim,'pembayaranmelalui', CaraPembayaranKlaim::items(),array('onchange'=>'enableInputPembayaran()','empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'style'=>'width:120px;')); ?>
                    </div>
                </div>
                <div id="divDenganTransfer" class="hide">
                    <?php echo $form->textFieldRow($modPembayaranKlaim,'nobuktisetor',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modPembayaranKlaim,'alamatpenyetor',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modPembayaranKlaim,'namabank',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modPembayaranKlaim,'norekbank',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>
            </div>
            <div class = "span4">
                <div class="control-group ">
                    <?php echo CHtml::label('Total Piutang','totalPiutang', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPembayaranKlaim,'totalpiutang',array('readonly'=>false,'class'=>'inputFormTabel currency span3', 'style'=>'width:110px;','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Telah Bayar','totalTelahBayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPembayaranKlaim,'telahbayar',array('readonly'=>false,'class'=>'inputFormTabel currency span3', 'style'=>'width:110px;','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Bayar','totalBayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPembayaranKlaim,'totalbayar',array('onkeyup'=>'hitungTotalBayar();','readonly'=>false,'class'=>'inputFormTabel currency span3', 'style'=>'width:110px;')) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Sisa Piutang','totalSisaPiutang', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPembayaranKlaim,'totalsisapiutang',array('readonly'=>false,'class'=>'inputFormTabel currency span3','style'=>'width:110px;', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>    
            </div>
    </div>
</fieldset>
<br>
    <div>
        <?php
            //FORM REKENING
            $this->renderPartial('akuntansi.views.pembayaranPiutangAsuransi.rekening._formRekening',
                array(
                    'form'=>$form,
                    'modRekenings'=>$modRekenings,
                )
            );
        ?>
    </div>
            
    <div class="form-actions">
             <?php 
                if($modPembayaranKlaim->isNewRecord)
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); 
                else{
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)','disabled'=>true)); 
                echo "&nbsp;&nbsp;";
                }
                $reffUrl = ((isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? array('modulId'=>Yii::app()->session['modulId'], 'frame'=>$_GET['frame'], 'idPendaftaran'=>$_GET['idPendaftaran']) : array('modulId'=>Yii::app()->session['modulId']));
                
                    //BELUM ADA FUNGSINYA >>> echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"return false",'disabled'=>true)); 
            ?>
            <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('index',$reffUrl), array('class'=>'btn btn-danger')); ?>
            <?php  
            $content = $this->renderPartial('billingKasir.views.tips.transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
    </div>
<?php $this->endWidget(); ?>

<?php
$pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;
//    $pembulatan = ($pembulatanHarga > 0) ? $totTagihan % $pembulatanHarga : 0 ;
?>
        
<script type="text/javascript">
$('.currency').each(function(){this.value = formatUang(this.value)});

function enableInputPembayaran()
{
    if($('#AKPembayaranklaimT_pembayaranmelalui').val() == "CASH")
        $('#divDenganTransfer').show();
    else if($('#AKPembayaranklaimT_pembayaranmelalui').val() == "TRANSFER")
        $('#divDenganTransfer').show();
    else 
        $('#divDenganTransfer').hide();
    if($('#AKPembayaranklaimT_pembayaranmelalui').val() == 'TRANSFER'){
        //alert('isi');
        $('#AKPembayaranklaimT_nobuktisetor').removeAttr('readonly');
        $('#AKPembayaranklaimT_alamatpenyetor').removeAttr('readonly');
        $('#AKPembayaranklaimT_namabank').removeAttr('readonly');
        $('#AKPembayaranklaimT_norekbank').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#AKPembayaranklaimT_nobuktisetor').attr('readonly','readonly');
        $('#AKPembayaranklaimT_alamatpenyetor').attr('readonly','readonly');
        $('#AKPembayaranklaimT_namabank').attr('readonly','readonly');
        $('#AKPembayaranklaimT_norekbank').attr('readonly','readonly');
        
        $('#AKPembayaranklaimT_nobuktisetor').val('');
        $('#AKPembayaranklaimT_alamatpenyetor').val('');
        $('#AKPembayaranklaimT_namabank').val('');
        $('#AKPembayaranklaimT_norekbank').val('');
    }
}

function hitungPembayaran()
{
    $('#tableList').find('input[name$="[pembayaranpelayanan_id]"]').each(function(){
        hitungTotalPembayaran(this);
    });
    hitungJmlPembayaran();
}

function hitungTotalPembayaran(obj)
{
    var pembayaranpelayanan_id = unformatNumber(obj.value);
    var jmlTagihan = unformatNumber($(obj).parents('tr').find('input[name$="[jmltagihan]"]').val());
    var jmlPiutang = unformatNumber($(obj).parents('tr').find('input[name$="[jmlpiutang]"]').val());
    var jmlTelahBayar = unformatNumber($(obj).parents('tr').find('input[name$="[jmltelahbayar]"]').val());
    var jmlBayar = unformatNumber($(obj).parents('tr').find('input[name$="[jmlBayar]"]').val());
    var JmlSisaTagihan = unformatNumber($(obj).parents('tr').find('input[name$="[jmlsisatagihan]"]').val());
    

    
    var totTagihan = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[jmltagihan]"]').each(function(){
            totTagihan = totTagihan + unformatNumber(this.value);
        });
    });
    $('#tottagihan').val(formatUang(totTagihan));
    
    var totPiutang = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[jmlpiutang]"]').each(function(){
            totPiutang = totPiutang + unformatNumber(this.value);
        });
    });
    $('#totpiutang').val(formatUang(totPiutang));
    $('#AKPembayaranklaimT_totalpiutang').val(formatUang(totPiutang));
    
    var totTelahBayar = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[jmltelahbayar]"]').each(function(){
            totTelahBayar = totTelahBayar + unformatNumber(this.value);
        });
    });
    $('#tottelahbayar').val(formatUang(totTelahBayar));
    
    var totBayar = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[jmlbayar]"]').each(function(){
            totBayar = totBayar + unformatNumber(this.value);
        });
    });
    $('#totbayar').val(formatUang(totBayar));
    
    var totSisaTagihan = 0;
    $(obj).parents('table').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[jmlsisatagihan]"]').each(function(){
            totSisaTagihan = totSisaTagihan + unformatNumber(this.value);
        });
    });
    $('#totsisatagihan').val(formatUang(totSisaTagihan));
}

function hitungJmlPembayaran()
{
    var totalPiutang = unformatNumber($('#totpiutang').val());
    var totalTelahBayar = unformatNumber($('#AKPembayaranklaimT_telahbayar').val());
    var totalBayar = unformatNumber($('#AKPembayaranklaimT_totalbayar').val());
    var totalSisaPiutang = unformatNumber($('#AKPembayaranklaimT_totalsisapiutang').val());
    
    
    totpiutang = totalPiutang;
    $('#AKPembayaranklaimT_totalpiutang').val(totpiutang);
}

function cekInputTindakan()
{
    jumlahPilihan = $(".cek:checked").length;
    if (jumlahPilihan < 1){
        alert('Tidak Ada Transaksi Pembayaran');
        return false;
    }
        
    if($('#tottagihan').val() <=0 ){
        alert('Tidak ada Tagihan');
        return false;
    } else {
        $('.currency').each(function(){this.value = unformatNumber(this.value)});
        $('.number').each(function(){this.value = unformatNumber(this.value)});
        return true;
    }
    return false;
}

function hitungTotalBayar(obj){
    totalBayar = 0;
    totJmlBayar = 0;

    totalBayar = unformatNumber($('#AKPembayaranklaimT_totalbayar').val());
    var totaltagihan = $('#tottagihan').val();
    var sisatagihan =  unformatNumber(totaltagihan) - totalBayar;
    $('#totsisatagihan').val(sisatagihan);

     $('#tableList tbody .cek').each(function(){
        var totTagihan = unformatNumber($('#tottagihan').val());
        var totPiutang = unformatNumber($('#totpiutang').val());
        var totTelahBayar = unformatNumber($('#tottelahbayar').val());
        var totBayar = unformatNumber($('#totbayar').val());
        var totSisaTagihan = unformatNumber($('#totsisatagihan').val());

        totJmlBayar = 0;
        totJmlSisaTagihan = 0;
        $('.cek').each(function(){

                var jmlTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmltagihan]"]').val()));
                var jmlPiutang = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlpiutang]"]').val()));
                var jmlTelahBayar = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmltelahbayar]"]').val()));
                var jmlBayar = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlbayar]"]').val()));
                var jmlSisaTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlsisatagihan]"]').val()));

                var totalJumlahBayar = unformatNumber($(this).parents('tr').find('.jmlbayar').val());
                var totalJumlahSisaTagihan = unformatNumber($(this).parents('tr').find('.jmlsisatagihan').val());

                // var nilaijmlbayar = jmlTagihan / totTagihan * totBayar;
                // alert(nilaijmlbayar);

                jmlSisaTagihan = 0;

                jumlahBayar = Math.ceil((((jmlTagihan - jmlTelahBayar) / (totTagihan)) * (totalBayar)).toFixed(2));

                pembulatan = (jumlahBayar) % <?php echo $pembulatanHarga; ?>;

                jmlPembulatan = <?php echo $pembulatanHarga; ?> - pembulatan;
                if (jQuery.isNumeric(jmlPembulatan)){  
                    if (jmlPembulatan >= <?php echo $pembulatanHarga; ?>){
                        jmlPembulatan = 0;
                    }
                }

                if (jQuery.isNumeric(totalJumlahBayar)){
                    totJmlBayar += parseFloat(unformatNumber(totalJumlahBayar));
                }
                if (jQuery.isNumeric(totalJumlahSisaTagihan)){
                    totJmlSisaTagihan += parseFloat(unformatNumber(totalJumlahSisaTagihan));
                }

                if(jmlBayar > jmlPiutang){
                    if(jmlSisaTagihan > 0 ){
                        jumlahSisaTagihan = 0;
                    }else{
                        jumlahSisaTagihan = (jmlBayar - (jmlPiutang - jmlTelahBayar));
                    }
                }else{
                    jmlSisaTagihan = ((jmlPiutang - jmlTelahBayar) - jmlBayar);
                }

            jmlSisaTagihan = Math.ceil((((jmlTagihan - jmlTelahBayar) / (totTagihan)) * (totSisaTagihan)).toFixed(2));

//            $(this).parents("tr").find('input[name$="[jmlbayar]"]').val(formatUang(jumlahBayar));
//            $(this).parents("tr").find('input[name$="[jmlsisatagihan]"]').val(formatUang(jmlSisaTagihan));
//            $('#totbayar').val(formatUang(totJmlBayar));
            // $('#totsisatagihan').val(formatUang(totJmlSisaTagihan));
        });
    });
    
    $('#totbayar').val(totalBayar);
}
    
function checkAllPembayaran() {
    if ($("#checkPembayaran").is(":checked")) {
        $('#tableList input[name*="cekList"]').each(function(){
           $(this).attr('checked',true);
        })
    } else {
       $('#tableList input[name*="cekList"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    
    $("#tableList > tbody > tr:last .cek").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
    setAll();
}

function updateRekening(){
    setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya        
        var saldo = unformatNumber($('#AKPembayaranklaimT_totalbayar').val());
        updateRekeningPembayaranKlaim(formatDesimal(saldo));
    },1500);
}
 
</script>

<?php 
    $url =Yii::app()->createUrl($this->route);    
    $jmltotpiutang = CHtml::activeId($modPembayaranKlaim, 'totalpiutang');
    $jmltotbayar = CHtml::activeId($modPembayaranKlaim, 'totalbayar');
    $jmltottelahbayar = CHtml::activeId($modPembayaranKlaim, 'telahbayar');
    $jmltotsisapiutang = CHtml::activeId($modPembayaranKlaim, 'totalsisapiutang');

    $js = <<< JS
    function setAll(obj){
    
        jmltagihan = 0;
        jmlpiutang = 0;
        jmltelahbayar = 0;
        jmlbayar = 0;
        jmlsisatagihan = 0;
    
        jmltotpiutang = 0;
        jmltotbayar = 0;
        jmltottelahbayar = 0;
        jmltotsisapiutang = 0;
    
        totaltransaksi = 0;
    
        $('.cek').each(function(){
           if ($(this).is(':checked')){
                temptagihan = unformatNumber($(this).parents('tr').find('.jmltagihan').val());
                temppiutang = unformatNumber($(this).parents('tr').find('.jmlpiutang').val());
                temptelahbayar = unformatNumber($(this).parents('tr').find('.jmltelahbayar').val());
                tempbayar = unformatNumber($(this).parents('tr').find('.jmlbayar').val());
                tempsisatagihan = unformatNumber($(this).parents('tr').find('.jmlsisatagihan').val());
    
                temptotaltransaksi = $(this).parents('tr').find('.totaltransaksi').val();
                totalbayar = parseFloat(unformatNumber($('#REClosingkasirT_nilaiclosingtrans').val()));
                saldoawal = parseFloat($('#REClosingkasirT_closingsaldoawal').val());
                totalnilaiclosing =$('#totalnilaiclosing').val();

                jmlnilaiclosing = totalbayar + saldoawal;
        
                if (jQuery.isNumeric(temptagihan)){
                    jmltagihan += parseFloat(unformatNumber(temptagihan));
                }
                if (jQuery.isNumeric(temppiutang)){
                    jmlpiutang += parseFloat(unformatNumber(temppiutang));
                }
                if (jQuery.isNumeric(temptelahbayar)){
                    jmltelahbayar += parseFloat(unformatNumber(temptelahbayar));
                }
                if (jQuery.isNumeric(tempbayar)){
                    jmlbayar += parseFloat(unformatNumber(tempbayar));
                }
                if (jQuery.isNumeric(tempsisatagihan)){
                    jmlsisatagihan += parseFloat(unformatNumber(tempsisatagihan));
                }
                 $(this).parents('tr').find('.cek').val(1);
            }else{
                $(this).parents('tr').find('.cek').val(0);
            }
        });
    
        $('#AKPembayaranklaimT_totalpiutang').val(formatUang(jmlpiutang));
        $('#AKPembayaranklaimT_telahbayar').val(formatUang(jmltelahbayar));
        $('#AKPembayaranklaimT_totalbayar').val(formatUang(jmlbayar));
        $('#AKPembayaranklaimT_totalsisapiutang').val(formatUang(jmlsisatagihan));
        
        $('#tottagihan').val(formatUang(jmltagihan));
        $('#totpiutang').val(formatUang(jmlpiutang));
        $('#tottelahbayar').val(formatUang(jmltelahbayar));
        $('#totbayar').val(formatUang(jmlbayar));
        $('#totsisatagihan').val(formatUang(jmlsisatagihan));
    
    }
           
    function hitungSisaTagihan(obj){
         $('#tableList tbody .cek').each(function(){
        
            totTagihan = unformatNumber($('#tottagihan').val());
            totPiutang = unformatNumber($('#totpiutang').val());
            totTelahBayar = unformatNumber($('#tottelahbayar').val());
            totBayar = unformatNumber($('#totbayar').val());
            totSisaTagihan = unformatNumber($('#totsisatagihan').val());
        
            jmlTagihan = parseFloat(unformatNumber($(obj).parents('tr').find('input[name$="[jmltagihan]"]').val()));
            jmlPiutang = parseFloat(unformatNumber($(obj).parents('tr').find('input[name$="[jmlpiutang]"]').val()));
            jmlTelahBayar = parseFloat(unformatNumber($(obj).parents('tr').find('input[name$="[jmltelahbayar]"]').val()));
            jmlBayar = parseFloat(unformatNumber($(obj).parents('tr').find('input[name$="[jmlbayar]"]').val()));
            jmlSisaTagihan = parseFloat(unformatNumber($(obj).parents('tr').find('input[name$="[jmlsisatagihan]"]').val()));

            jumlahSisaTagihan = 0;
            if(jmlBayar > jmlPiutang){
                jumlahSisaTagihan = 0;
            }else{
                jumlahSisaTagihan = jmlPiutang-(jmlBayar+jmlTelahBayar);
            }
            totalJumlahSisaTagihan = 0;
            $('.cek').each(function(){
                    totalJumlahSisaTagihan = unformatNumber($(this).parents('tr').find('input[name$="[jmlsisatagihan]"]').val());

                    if (jQuery.isNumeric(totalJumlahSisaTagihan)){
                        totalJumlahSisaTagihan += parseFloat(unformatNumber(totalJumlahSisaTagihan));
                    }
            });            
            console.log(jmlBayar+' jmlBayar');
            console.log(jmlPiutang+' jmlPiutang');
            console.log(jumlahSisaTagihan+' jumlahSisaTagihan');
            $(obj).parents("tr").find('input[name$="[jmlsisatagihan]"]').val(formatUang(jumlahSisaTagihan));
            $('#totalsisatagihan').val(formatUang(totalJumlahSisaTagihan));
        });
        setAll(this);
        updateRekening();
    }
    
    function hitungJumlahPiutang(obj){
         $('#tableList tbody .cek').each(function(){
        
            totTagihan = unformatNumber($('#tottagihan').val());
            totPiutang = unformatNumber($('#totpiutang').val());
            totTelahBayar = unformatNumber($('#tottelahbayar').val());
            totBayar = unformatNumber($('#totbayar').val());
            totSisaTagihan = unformatNumber($('#totsisatagihan').val());
        
            jmlTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmltagihan]"]').val()));
            jmlPiutang = (obj.value);
            jmlTelahBayar = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmltelahbayar]"]').val()));
            jmlBayar = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlbayar]"]').val()));
            jmlSisaTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlsisatagihan]"]').val()));
    
            $('.cek').each(function(){
    
                    jumlahSisaTagihan = 0; 
                    if(jmlBayar > jmlPiutang){
                          jumlahSisaTagihan = 0;
                    }else{
                        jumlahSisaTagihan = ((jmlPiutang - jmlTelahBayar) - jmlBayar);
                    }

                    totalJumlahSisaTagihan = 0;
                    if (jQuery.isNumeric(totalJumlahSisaTagihan)){
                        totalJumlahSisaTagihan += parseFloat(unformatNumber(jmlSisaTagihan));
                    }
            });

            $(obj).parents("tr").find('input[name$="[jmlsisatagihan]"]').val(jumlahSisaTagihan);
            $(this).parents("tr").find('input[name$="[jmlpiutang2]"]').val(jmlPiutang);
            $('#totalsisatagihan').val(formatUang(totalJumlahSisaTagihan));
        });
        setAll(this);
    }
    
    function hitungJumlahTelahBayar(obj){
        $('#tableList tbody .cek').each(function(){
    
            totTagihan = unformatNumber($('#tottagihan').val());
            totPiutang = unformatNumber($('#totpiutang').val());
            totTelahBayar = unformatNumber($('#tottelahbayar').val());
            totBayar = unformatNumber($('#totbayar').val());
            totSisaTagihan = unformatNumber($('#totsisatagihan').val());
        
            jmlTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmltagihan]"]').val()));
            jmlPiutang = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlpiutang]"]').val()));
            jmlPiutang2 = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlpiutang2]"]').val()));
            jmlTelahBayar = (obj.value);
            jmlBayar = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlbayar]"]').val()));
            jmlSisaTagihan = parseFloat(unformatNumber($(this).parents('tr').find('input[name$="[jmlsisatagihan]"]').val()));

            jumlahSisaTagihan = 0; 
            if(jmlTelahBayar > jmlPiutang){
    //                jumlahSisaTagihan = (jmlBayar - (jmlPiutang2 - jmlTelahBayar));
                     // jumlahPiutang = (jmlPiutang2 - jmlTelahBayar);
                  jumlahSisaTagihan = 0;
                  jumlahPiutang = 0;
            }else{
                jumlahSisaTagihan2 = ((jmlPiutang2 - jmlTelahBayar) - jmlBayar);
                jumlahSisaTagihan = (jumlahSisaTagihan2 < 0 ? 0 : jumlahSisaTagihan2);
                jumlahPiutang = (jmlPiutang2 - jmlTelahBayar);
            }
        
            $('.cek').each(function(){
                    totalJumlahSisaTagihan = unformatNumber($(this).parents('tr').find('.jmlsisatagihan').val());
                    totalJumlahPiutang = unformatNumber($(this).parents('tr').find('.jmlpiutang').val());

                    totalJumlahSisaTagihan = 0;
                    if (jQuery.isNumeric(totalJumlahSisaTagihan)){
                        totalJumlahSisaTagihan += parseFloat(unformatNumber(totalJumlahSisaTagihan));
                    }
            });
                $(this).parents("tr").find('input[name$="[jmlsisatagihan]"]').val(jumlahSisaTagihan);
                $('#totalsisatagihan').val(formatUang(totalJumlahSisaTagihan));

        });
        setAll(this);
    }
      
        
    function ajaxGetList(){
        tglAwal = $('#Filter_tglAwal').val();
        tglAkhir = $('#Filter_tglAkhir').val();
        pasien_id = $('#Filter_pasien_id').val();
        carabayar_id = $('#AKPendaftaranT_carabayar_id').val();
        penjamin_id = $('#AKPendaftaranT_penjamin_id').val();
        noPendaftaran = $('#noPendaftaran').val();
    
        if(carabayar_id == '' || penjamin_id == ''){
            alert('Isi Data Cara Bayar dan Penjamin');
        }else{
                if(pasien_id == ''){
                    alert('Isi Data Pasien');
                }else{
                    $('#tableList').addClass('srbacLoading');
                    $.get('${url}', {tglAwal:tglAwal, tglAkhir:tglAkhir, carabayar_id:carabayar_id, penjamin_id:penjamin_id, noPendaftaran:noPendaftaran},function(data){
                        $('#tableList tbody').html(data);
                    });                    
                    setTimeout(function(){checkAllPembayaran();}, 1000);
                }$('#tableList').removeClass('srbacLoading');
        }
    }
    
    function hitungSemuaTransaksi(obj){
        $('.cek').each(function(){
                    jmltagihan = unformatNumber($(obj).parents('tr').find('.jmltagihan').val());
                    jmlpiutang = unformatNumber($(obj).parents('tr').find('.jmlpiutang').val());
                    jmltelahbayar = unformatNumber($(obj).parents('tr').find('.jmltelahbayar').val());
                    jmlbayar = unformatNumber($(obj).parents('tr').find('.jmlbayar').val());
                    jmlsisatagihan = unformatNumber($(obj).parents('tr').find('.jmlsisatagihan').val());

                    if(jmltagihan == ''){
                        jmltagihan = 0;
                    }
                    if(jmlpiutang == ''){
                        jmlpiutang = 0;
                    }
                    if(jmltelahbayar == ''){
                        jmltelahbayar = 0;
                    }
                    if(jmlbayar == ''){
                        jmlbayar = 0;
                    }
                    if(jmlsisatagihan == ''){
                        jmlsisatagihan = 0;
                    }
                    
                    $(obj).parents('tr').find('.jmltagihan').val(jmltagihan);
                    $(obj).parents('tr').find('.jmlpiutang').val(jmlpiutang);
                    $(obj).parents('tr').find('.jmltelahbayar').val(jmltelahbayar);
                    $(obj).parents('tr').find('.jmlbayar').val(jmlbayar);
                    $(obj).parents('tr').find('.jmlsisatagihan').val(jmlsisatagihan);
        });
    }
    
    function cekInput()
    {
        $(".currency").each(function(){this.value = unformatNumber(this.value)});
        return true;
    }
    
JS;
Yii::app()->clientScript->registerScript('onheadDialog', $js, CClientScript::POS_HEAD);
?>

<?php
$this->widget('application.extensions.moneymask.MMask', array(
     'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>