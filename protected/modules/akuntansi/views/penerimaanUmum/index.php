<fieldset id="input-penerimaan-kas">
<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp. ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    ));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'akpenerimaan-umum-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=> array(
            'onKeyPress'=>'return disableKeyPress(event)',
        ),
)); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<legend class="rim2">Transaksi Penerimaan Kas</legend>
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
    <table border="1">
        <tr>
            <td width="50%" align="right">
                <div class="control-group ">
                    <?php $modPenUmum->tglpenerimaan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenUmum->tglpenerimaan, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modPenUmum,'tglpenerimaan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',
                                array(
                                        'model'=>$modPenUmum,
                                        'attribute'=>'tglpenerimaan',
                                        'mode'=>'datetime',
                                        'options'=>array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array(
                                            'class'=>'dtPicker2-5 reqForm',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                                )
                            ); 
                        ?>

                    </div>
                </div>
                <?php echo $form->textFieldRow($modPenUmum,'nopenerimaan',array('class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                <?php echo $form->dropDownListRow($modPenUmum,'kelompoktransaksi',KelompokTransaksi::items(),array('class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo $form->hiddenField($modPenUmum,'jenispenerimaan_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->hiddenField($modPenUmum,'is_posting',array('readonly'=>true)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenUmum,'jenispenerimaan_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPenUmum,
                                    'attribute'=>'jenisKodeNama',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/jenisPenerimaan').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                               $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#AKPenerimaanUmumT_jenispenerimaan_id").val(ui.item.jenispenerimaan_id);
                                                getDataRekening(ui.item.jenispenerimaan_id);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('placeholder'=>'ketik Kode/Nama Jenis Penerimaan','class'=>'span3 reqForm'),
                                    'tombolDialog' => array('idDialog' => 'dialogJenisPenerimaan',),
                            )); 
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenUmum,'volume', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPenUmum,'volume',array('onblur'=>'hitungTotalHarga()','class'=>'span1 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->dropDownList($modPenUmum,'satuanvol', SatuanUmum::items(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    </div>
                </div>
                  <?php echo $form->textFieldRow($modTandaBukti,'biayaadministrasi',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPenUmum,'hargasatuan',array('onblur'=>'hitungTotalHarga()','class'=>'inputFormTabel currency reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPenUmum,'totalharga',array('readonly'=>true,'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                
            </td>
            <td align="left">
                <?php echo $form->textAreaRow($modPenUmum,'keterangan_penerimaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPenUmum,'namapenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modPenUmum,'nippenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modPenUmum,'jabatanpenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>  
                 <div class="control-group ">
                <?php echo $form->labelEx($modPenUmum, 'ruangan_id', array('class' => 'control-label')); ?>
                      <div class="controls">
                        <?php
                        echo $form->dropDownList($modPenUmum, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                          'ajax' => array('type' => 'POST',
                              'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $modPenUmum->getNamaModel() . '')),
                              'update' => '#' . CHtml::activeId($modPenUmum, 'ruangan_id') . ''),));
                        ?>
                        
                
                        <?php echo $form->dropDownList($modPenUmum, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                        <?php echo $form->error($modPenUmum, 'ruangan_id'); ?>
                </div>
                </div>
                   
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
<!--    <legend class="accord1" style="width:460px;">
        <?php // echo CHtml::checkBox('pakeAsuransi', true, array('onchange'=>'bukaUraian(this)','onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        Pilih Jika Transaksi Ada Uraiannya
    </legend>
    <div id="div_tblInputUraian">
        <table id="tblInputUraian" class="table table-bordered table-condensed" style="width: 100%; overflow-x: scroll;">
            <thead>
                <tr>
                    <th>Uraian</th>
                    <th>Volume</th>
                    <th>Satuan</th>
                    <th>Harga</th>
                    <th>Total</th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php // $this->renderPartial('_rowUraian',array('form'=>$form, 'modUraian'=>$modUraian)); ?>
            </tbody>
        </table>
   </div>-->
    
    <fieldset>
        <legend class="rim">Jurnal Rekening</legend>
        <table>
            <tr>
                <td width="70%">
                    <div>
                        <?php
                            $this->renderPartial('_rowListRekening',
                                array(
                                    'form'=>$form,
                                )
                            );
                        ?>
                    </div>
                </td>
                <td width="20%">
                    <div class="control-group ">
                        <?php echo CHtml::label('Total Tagihan','totTagihan', array('class'=>'control-label inline')) ?>
                        <div class="controls">
                            <?php echo CHtml::textField('totTagihan',$totTagihan,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                        </div>
                    </div>
                    <?php echo $form->hiddenField($modTandaBukti,'jmlpembulatan',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                  
                    <?php echo $form->hiddenField($modTandaBukti,'biayamaterai',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'jmlpembayaran',array('onkeyup'=>'hitungKembalian();','readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->hiddenField($modTandaBukti,'uangditerima',array('onkeyup'=>'hitungKembalian();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->hiddenField($modTandaBukti,'uangkembalian',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                </td>
                <td width="20%">
                    <div class="control-group ">
                        <?php $modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                        <?php echo $form->labelEx($modTandaBukti,'tglbuktibayar', array('class'=>'control-label inline')) ?>
                        <div class="controls">
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modTandaBukti,
                                                'attribute'=>'tglbuktibayar',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                                )); 
                            ?>

                        </div>
                    </div>
                    <?php echo $form->dropDownListRow($modTandaBukti,'carapembayaran',  CaraPembayaran::items(),array('onchange'=>'ubahCaraPembayaran(this)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <div class="control-group ">
                        <?php echo CHtml::label('Menggunakan Kartu','pakeKartu', array('class'=>'control-label inline')) ?>
                        <div class="controls">
                            <?php echo CHtml::checkBox('pakeKartu',false,array('onchange'=>"enableInputKartu();", 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?> <i class="icon-chevron-down"></i>
                        </div>
                    </div>
                    <div id="divDenganKartu" class="hide">
                        <?php echo $form->dropDownListRow($modTandaBukti,'dengankartu',  DenganKartu::items(),array('onchange'=>'enableInputKartu()','empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        <?php echo $form->textFieldRow($modTandaBukti,'bankkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                        <?php echo $form->textFieldRow($modTandaBukti,'nokartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                        <?php echo $form->textFieldRow($modTandaBukti,'nostrukkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    </div>

                    <?php echo $form->hiddenField($modTandaBukti,'darinama_bkm',array('value'=>'-', 'class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->hiddenField($modTandaBukti,'alamat_bkm',array('value'=>'-', 'class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->hiddenField($modTandaBukti,'sebagaipembayaran_bkm',array('value'=>'-', 'class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </td>
            </tr>
        </table>
    </fieldset>
      
    <div class="form-actions">
        <div style="float:left;margin-right:6px;">
            <?php
                $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
                $urlSave=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/index');
                $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'=>'primary',
                    'buttons'=>array(
                        array(
                            'label'=>'Simpan',
                            'icon'=>'icon-ok icon-white',
                            'url'=>"#",
                            'htmlOptions'=>
                                array(
                                    'onclick'=>'simpanPenerimaan(\'jurnal\');return false;',
                                )
                       ),
                        array(
                            'label'=>'',
                            'items'=>array(
                                array(
                                    'label'=>'Posting',
                                    'icon'=>'icon-ok',
                                    'url'=>"#",
                                    'itemOptions' => array(
                                        'onclick'=>'simpanPenerimaan(\'posting\');return false;'
                                    )
                                ),
                            )
                        ),
                    ),
                ));
                echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), array('style'=>'display:none','id' => 'reseter', 'class'=>'btn btn-danger', 'type'=>'reset'));
             ?>
        </div>
           <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKasir($('#FAPendaftaranT_pendaftaran_id').val());return false",'disabled'=>false)); ?>
           <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                    Yii::app()->createUrl($this->module->id.'/'.penerimaanUmum.'/index'), 
                    array('class'=>'btn btn-danger',
                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            <?php
//                $content = $this->renderPartial('../tips/transaksi',array(),true);
//                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
    </div>
<?php $this->endWidget(); ?>
</fieldset>

<script type="text/javascript">
var trUraian=new String(<?php echo CJSON::encode($this->renderPartial('_rowUraian',array('form'=>$form,'modUraian'=>array(0=>$modUraian[0]),'removeButton'=>true),true));?>);
$('.currency').each(function(){this.value = formatNumber(this.value)});

function simpanPenerimaan(params)
{
    if(params=='posting'){
        $('#AKPenerimaanUmumT_is_posting').val(true);
    }else{
        $('#AKPenerimaanUmumT_is_posting').val(false);
    }

    jenis_simpan = params;
    var kosong = "" ;
    var dataKosong = $("#input-penerimaan-kas").find(".reqForm[value="+ kosong +"]");
    if(dataKosong.length > 0){
        alert('Bagian dengan tanda * harus diisi ');
    }else{
            
          var detail = 1;
//        var detail = 0;
//        $('#tblInputUraian tbody tr').each(
//            function(){
//                var total_hgr = $(this).find('input[name$="[totalharga]"]');
//                if(total_hgr.length > 0){
//                    detail++;
//                }
//            }
//        );
        
        if(detail > 0){
            $('.currency').each(
                function(){
                    this.value = unformatNumber(this.value)
                }
            );
            $('#akpenerimaan-umum-t-form').submit();
            /*MENGGUNAKAN METHOD POST PHP    
            $.post('<?php // echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/SimpanPenerimaan');?>', {jenis_simpan:jenis_simpan, data:$('#akpenerimaan-umum-t-form').serialize()},
                function(data){
                    if(data.status == 'ok')
                    {
                        if(data.action == 'insert')
                        {
                            alert("Simpan data berhasil");
                            $("#tblInputUraian").find('tr[class$="child"]').detach();
                            $("#reseter").click();
                            $("#input-penerimaan-kas").find("input[name$='[nopenerimaan]']").val(data.pesan.nopenerimaan);
                            $("#tblInputRekening > tbody").find('tr').detach();
                        }else{
                            alert("Update data berhasil");
                        }
                    }else{
                        alert("Data gagal disimpan");
                    }
            }, "json");
                */
        }else{
            alert('Detail uraian masih kosong');
        }
         
    }
     return false;
}

function cekInput()
{
    var harga = 0; var totharga = 0;
    if($('#AKPenerimaanUmumT_isuraintransaksi').is(':checked')){    
        $('#tblInputUraian').find('input[name$="[hargasatuan]"]').each(function(){
            harga = harga + unformatNumber(this.value);
        });
        $('#tblInputUraian').find('input[name$="[totalharga]"]').each(function(){
            totharga = totharga + unformatNumber(this.value);
        });
        
        //if(harga != unformatNumber($('#AKPenerimaanUmumT_hargasatuan').val())){
        //    alert('Harga tidak sesuai');return false;
        //}
        if(totharga != unformatNumber($('#AKPenerimaanUmumT_totalharga').val())){
            alert('Harga Uraian tidak sesuai');return false;
        }
    }
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    
    return true;
}

function hitungTotalUraian(obj)
{
    var volume = unformatNumber($(obj).parents('tr').find('input[name$="[volume]"]').val());
    var hargasatuan = unformatNumber($(obj).parents('tr').find('input[name$="[hargasatuan]"]').val());
    $(obj).parents('tr').find('input[name$="[totalharga]"]').val(formatNumber(volume*hargasatuan));
    totalTagihan();
}

function hitungTotalHarga()
{
    var biayaAdministrasi = unformatNumber($('#AKTandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#AKTandabuktibayarT_biayamaterai').val());
    var vol = unformatNumber($('#AKPenerimaanUmumT_volume').val());
    var harga = unformatNumber($('#AKPenerimaanUmumT_hargasatuan').val());
    
    var jmlpembayaran = (vol*harga)+biayaAdministrasi+biayaMaterai;
    
    $('#AKPenerimaanUmumT_totalharga').val(formatNumber(vol*harga));
    $('#AKTandabuktibayarT_jmlpembayaran').val(formatNumber(jmlpembayaran));
    $('#AKTandabuktibayarT_uangditerima').val(formatNumber(jmlpembayaran));
    $('#totTagihan').val(formatNumber(jmlpembayaran));
     $('#RekeningakuntansiV_0_saldodebit').val(formatNumber(jmlpembayaran));
//            
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber((vol*harga)+biayaAdministrasi+biayaMaterai));
    
    //alert(<?//php count($modUraian); ?>);
    
    <?php foreach ($modUraian as $i => $uraian) { ?>
//            if ($('#RekeningakuntansiV_$i_saldonormal') == 'D') 
//            {
               // var i = <?php echo $i ?>;
                  $('#RekeningakuntansiV_0_saldodebit').val(formatNumber((vol*harga)+biayaAdministrasi+biayaMaterai));
//              }
//              elseif ($('#RekeningakuntansiV_$i_saldonormal') == 'K') 
//            {
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber((vol*harga)+biayaAdministrasi+biayaMaterai));
//              }
   
              
    <?php } ?>
}

function bukaUraian(obj)
{
    if($(obj).is(':checked')){
        $('#div_tblInputUraian').slideDown();
    } else {
        $('#div_tblInputUraian').slideUp();
    }
}

function addRowUraian(obj)
{
    $(obj).parents('table').children('tbody').append(trUraian.replace());
        
    renameInput('AKUraianpenumumT','uraiantransaksi');
    renameInput('AKUraianpenumumT','volume');
    renameInput('AKUraianpenumumT','satuanvol');
    renameInput('AKUraianpenumumT','hargasatuan');
    renameInput('AKUraianpenumumT','totalharga');
    jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
    maskMoneyInput($('#tblInputUraian > tbody > tr:last'));
}

function totalTagihan()
{
    var total = 0;
    $("#tblInputUraian").find('input[name$="[totalharga]"]').each(
        function(){
            total += unformatNumber($(this).val());
        }
    );
    $("#totTagihan").val(formatNumber(total));
    $("#AKTandabuktibayarT_jmlpembayaran").val(formatNumber(total));
     $('#RekeningakuntansiV_0_saldodebit').val(formatNumber(total));
//            
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber(total));
}

function perhitunganUang()
{
    var biayaadministrasi = unformatNumber($("#AKTandabuktibayarT_biayaadministrasi").val());
    var biayamaterai = unformatNumber($("#AKTandabuktibayarT_biayamaterai").val());
    var uangditerima = unformatNumber($("#AKTandabuktibayarT_uangditerima").val());
    $("#AKTandabuktibayarT_jmlpembayaran").val(biayaadministrasi+biayamaterai+uangditerima);
}

function totaltagihankeseluruhan(obj)
{
    var totaltagihan = 0;
    var totalharga = 0;
    var totalbaris = 0;
    $(obj).each(function()
    {
        totalbaris = $(obj).parents("tr").children(".totalharga").val();
        totalharga = unformatNumber(totalbaris);
        totaltagihan += totalharga;
    });
//    $('#totTagihan').hide();
    $('#totTagihan').val(totaltagihan);
}
 
function batalUraian(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan Uraian?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
        renameInput('AKUraianpenumumT','uraiantransaksi');
        renameInput('AKUraianpenumumT','volume');
        renameInput('AKUraianpenumumT','satuanvol');
        renameInput('AKUraianpenumumT','hargasatuan');
        renameInput('AKUraianpenumumT','totalharga');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblInputUraian tr').length;
    var i = -1;
    $('#tblInputUraian tr').each(function(){
        if($(this).has('input[name$="[uraiantransaksi]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function enableInputKartu()
{
    if($('#pakeKartu').is(':checked'))
        $('#divDenganKartu').show();
    else 
        $('#divDenganKartu').hide();
    if($('#AKTandabuktibayarT_dengankartu').val() != ''){
        //alert('isi');
        $('#AKTandabuktibayarT_bankkartu').removeAttr('readonly');
        $('#AKTandabuktibayarT_nokartu').removeAttr('readonly');
        $('#AKTandabuktibayarT_nostrukkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#AKTandabuktibayarT_bankkartu').attr('readonly','readonly');
        $('#AKTandabuktibayarT_nokartu').attr('readonly','readonly');
        $('#AKTandabuktibayarT_nostrukkartu').attr('readonly','readonly');
        
        $('#AKTandabuktibayarT_bankkartu').val('');
        $('#AKTandabuktibayarT_nokartu').val('');
        $('#AKTandabuktibayarT_nostrukkartu').val('');
    }
}

function ubahCaraPembayaran(obj)
{
    if(obj.value == 'CICILAN'){
        $('#AKTandabuktibayarT_jmlpembayaran').removeAttr('readonly');
    } else {
        $('#AKTandabuktibayarT_jmlpembayaran').attr('readonly', true);
        hitungJmlBayar();
    }
    
    if(obj.value == 'TUNAI'){
        hitungJmlBayar();
    } 
}

function hitungJmlBayar()
{
    var biayaAdministrasi = unformatNumber($('#AKTandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#AKTandabuktibayarT_biayamaterai').val());
    var totTagihan = unformatNumber($('#totTagihan').val());
    var jmlPembulatan = unformatNumber($('#AKTandabuktibayarT_jmlpembulatan').val());
    var hargasatuan = unformatNumber($('#AKPenerimaanUmumT_hargasatuan').val());
    
    totBayar = totTagihan + hargasatuan + jmlPembulatan + biayaAdministrasi + biayaMaterai;
    $('#AKTandabuktibayarT_jmlpembayaran').val(formatNumber(totBayar));
    $('#RekeningakuntansiV_0_saldodebit').val(formatNumber(totBayar));
//            
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber(totBayar));
    hitungKembalian();
}

function hitungKembalian()
{
    var jmlBayar = unformatNumber($('#AKTandabuktibayarT_jmlpembayaran').val());
    var uangDiterima = unformatNumber($('#AKTandabuktibayarT_uangditerima').val());
    var uangKembalian = uangDiterima - jmlBayar;
    if(uangKembalian < 0)
    {
       uangKembalian = 0; 
    }
    $('#AKTandabuktibayarT_uangkembalian').val(formatNumber(uangKembalian));
    
}

function unMaskMoneyInput(tr)
{
    $(tr).find('input.currency:text').unmaskMoney();
}

function maskMoneyInput(tr)
{
    $(tr).find('input.currency:text').maskMoney(
        {
            "symbol":"Rp. ",
            "defaultZero":true,
            "allowZero":true,
            "decimal":".",
            "thousands":",",
            "precision":0
        }
    );
}

</script>
    
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogJenisPenerimaan',
    'options'=>array(
        'title'=>'Daftar Jenis Penerimaan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modJenisPenerimaan = new JenispenerimaanM();
$modJenisPenerimaan->unsetAttributes();
if(isset($_GET['JenispenerimaanM'])) {
    $modJenisPenerimaan->attributes = $_GET['JenispenerimaanM'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'jenispenerimaan-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modJenisPenerimaan->searchJenisPenerimaanRek(),
	'filter'=>$modJenisPenerimaan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Jenis Penerimaan',
                    'name'=>'jenispenerimaan_nama',
                    'value'=>'$data->jenispenerimaan_nama',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'jenispenerimaan_namalain',
                    'value'=>'$data->jenispenerimaan_namalain',
                ),
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                        getDataRekening($data->jenispenerimaan_id);
                                        $(\"#AKPenerimaanUmumT_jenispenerimaan_id\").val(\"$data->jenispenerimaan_id\");
                                        $(\"#AKPenerimaanUmumT_jenisKodeNama\").val(\"$data->jenispenerimaan_nama\");
                                        $(\"#dialogJenisPenerimaan\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>