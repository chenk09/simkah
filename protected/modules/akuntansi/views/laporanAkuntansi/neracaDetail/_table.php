<div id="tableLaporan" class="grid-view" style="">
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th>
            <center>Nama Rekening</center>
        </th>
        <th>
            <center>Total Saldo</center>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php
        $tab1 = '&nbsp; &nbsp; &nbsp; ';
        $tab2 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        $tab3 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        $tab4 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        $tab5 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        
        if(COUNT($model) > 0){
           
            foreach ($model as $i => $neraca) {
                echo '<tr><td colspan="2">'.$tab1.' - '.strtoupper($neraca->nmrekening1).'</td></tr>';
               
                //start rekening ke 2
                $criteria2 = new CDbCriteria;
                $criteria2->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
                    $criteria2->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
                }
                $criteria2->addCondition('rekening1_id='.$neraca->rekening1_id);
                $criteria2->group = 'rekening2_id, nmrekening2, rekperiod_id';
                $criteria2->select = $criteria2->group;
                $model2 = AKLaporanneracadetailV::model()->findAll($criteria2);
                if(COUNT($model2) > 0){
                    $total_rekening1 = 0;
                    foreach ($model2 as $i2 => $neraca2) {
                        echo '<tr><td colspan="2">'.$tab2.' - '.strtoupper($neraca2->nmrekening2).'</td></tr>'; 
                        
                        //start rekening ke 3
                        $criteria3 = new CDbCriteria;
                        $criteria3->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                        if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
                            $criteria3->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
                        }
                        $criteria3->addCondition('rekening1_id='.$neraca->rekening1_id);
                        $criteria3->addCondition('rekening2_id='.$neraca2->rekening2_id);
                        $criteria3->select = 'rekening3_id, nmrekening3';
                        $criteria3->group = $criteria3->select;
                        $criteria3->order = 'rekening3_id ASC';
                        $model3 = AKLaporanneracadetailV::model()->findAll($criteria3);
                        if(COUNT($model3) > 0){
                            foreach ($model3 as $i3 => $neraca3) {
                               echo '<tr><td colspan="2">'.$tab3.' - '.strtoupper($neraca3->nmrekening3).'</td></tr>';
                               
                               //start rekening ke 4
                                $criteria4 = new CDbCriteria;
                                $criteria4->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                                if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
                                    $criteria4->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
                                }
                                $criteria4->addCondition('rekening1_id='.$neraca->rekening1_id);
                                $criteria4->addCondition('rekening2_id='.$neraca2->rekening2_id);
                                $criteria4->addCondition('rekening3_id='.$neraca3->rekening3_id);
                                $criteria4->select = 'rekening4_id, nmrekening4';
                                $criteria4->group = $criteria4->select;
                                $model4 = AKLaporanneracadetailV::model()->findAll($criteria4);
                                if(COUNT($model4) > 0){
                                    $total_saldo = 0;
                                    foreach ($model4 as $i4 => $neraca4) {
                                        echo '<tr><td colspan="2">'.$tab4.' - '.strtoupper($neraca4->nmrekening4).'</td></tr>';
                                        
                                        //start rekening ke 5
                                        $criteria5 = new CDbCriteria;
                                        $criteria5->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                                        if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
                                            $criteria5->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
                                        }
                                        $criteria5->addCondition('rekening1_id='.$neraca->rekening1_id);
                                        $criteria5->addCondition('rekening2_id='.$neraca2->rekening2_id);
                                        $criteria5->addCondition('rekening3_id='.$neraca3->rekening3_id);
                                        $criteria5->addCondition('rekening4_id='.$neraca4->rekening4_id);
                                        $criteria5->select = 'rekening5_id, nmrekening5, sum(saldo) AS saldo';
                                        $criteria5->group = 'rekening5_id, nmrekening5';
                                        $model5 = AKLaporanneracadetailV::model()->findAll($criteria5);
                                        if(COUNT($model5) > 0){
                                            foreach ($model5 as $i5 => $neraca5) {
                                                echo '<tr><td>'.$tab5.' - '.$neraca5->nmrekening5.'</td>
                                                        <td style="text-align:right;">'.number_format($neraca5->saldo,2).'</td>
                                                       </tr>';
                                                $total_saldo += $neraca5->saldo;
                                            }
                                        }
                                        //end rekening ke 5
                                        
                                    }
                                }
                                //end rekening ke 4
                                echo '<tr><td style="text-align:right; font-weight:bold;"> TOTAL '.strtoupper($neraca3->nmrekening3).'</td>
                                                <td style="text-align:right; font-weight:bold;">'.number_format($total_saldo,2).'</td></tr>'; 
                                $total_rekening1 += $total_saldo;
                           } 
                        }
                        //end rekening ke 3
                       echo '<tr><td style="text-align:right; font-weight:bold;"> TOTAL '.strtoupper($neraca2->nmrekening2).'</td>
                                                <td style="text-align:right; font-weight:bold;">'.number_format($total_rekening1,2).'</td></tr>'; 
                   }
                }
                //end rekening ke 2
                
            }
            
        }
        else{
            echo '<tr><td colspan="2"><span class="empty">Tidak ditemukan hasil.</span></td></tr>';
        }
      ?>
    </tbody>
  </table>
</div>