<?php
$url = '';
Yii::app()->clientScript->registerScript('search', "
$('.search-form').submit(function(){
    $('#tableLaporan').addClass('srbacLoading');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Posisi Keuangan / Neraca Detail</legend>
<div class="search-form">
<?php $this->renderPartial('akuntansi.views.laporanAkuntansi.neracaDetail/_search',array('modelLaporan'=>$modelLaporan)); 
?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Neraca Detail</legend>
    <?php $this->renderPartial('akuntansi.views.laporanAkuntansi.neracaDetail/_table', array('model'=>$model,'modelLaporan'=>$modelLaporan)); ?>
    <?php //$this->renderPartial('_tab'); ?>
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanNeracaDetail');
    $this->renderPartial('akuntansi.views.laporanAkuntansi._footerNoGraph', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>