<div class="search-form" style="">
<legend class="rim">Pencarian</legend>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
    ));
?>
<table>
    <tr>
        <td>
        <div class="control-group ">
            <label class="control-label">Tanggal Posting</label>
            <div class="controls">
                <?php 
                $format = new CustomFormat;
                $this->widget('MyDateTimePicker',array(
                    'model'=>$modelLaporan,
                    'attribute'=>'tglAwal',
                    'mode'=>'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                    ),
                )); ?> 
            </div>
        </div>
        <div class="control-group ">
            <label class="control-label">Sampai Dengan</label>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker',array(
                    'model'=>$modelLaporan,
                    'attribute'=>'tglAkhir',
                    'mode'=>'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                    ),
                )); ?> 
            </div>
        </div>
        </td>
        <td>
        <div class="control-group ">
             <label class="control-label">Periode Akutansi</label>
                <div class="controls">
                   <?php
                      echo $form->dropDownList($modelLaporan,'rekperiod_id',CHtml::listData(RekperiodM::model()->findAll(),
                                    'rekperiod_id','deskripsi'),array('class'=>'span2','style'=>'width:140px','empty'=>'-- Pilih --')); 
                 ?>
                </div>
        </div>
        </td>
    </tr>
</table>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            
    </div>
<?php
$this->endWidget();
?>
</div>