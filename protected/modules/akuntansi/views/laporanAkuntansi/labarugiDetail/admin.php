<?php
$url = '';
Yii::app()->clientScript->registerScript('search', "
$('.search-form').submit(function(){
    $('#tableLaporan').addClass('srbacLoading');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Laba Rugi Detail</legend>
<div class="search-form">
<?php $this->renderPartial('akuntansi.views.laporanAkuntansi.labarugiDetail/_search',array('modelLaporan'=>$modelLaporan)); 
?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Laba Rugi Detail</legend>
    <?php $this->renderPartial('akuntansi.views.laporanAkuntansi.labarugiDetail/_table', array('model'=>$model,'modelLaporan'=>$modelLaporan)); ?>
    <?php //$this->renderPartial('_tab'); ?>
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanLabaRugiDetail');
    $this->renderPartial('akuntansi.views.laporanAkuntansi._footerNoGraph', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>