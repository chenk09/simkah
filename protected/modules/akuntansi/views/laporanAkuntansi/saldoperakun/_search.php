<legend class="rim">Pencarian</legend>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
    ));
?>
<table>
    <tr>
        <td>
        <div class="control-group ">
            <?php echo $form->labelEx($modelLaporan,'Periode Awal', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php 
                $format = new CustomFormat;
//                $modelLaporan->tglAwal = $format->formatDateTimeMediumForUser($modelLaporan->tglAwal);
                $this->widget('MyDateTimePicker',array(
                    'model'=>$modelLaporan,
                    'attribute'=>'tglAwal',
                    'mode'=>'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                    ),
                )); ?> 
            </div>
        </div>
        
        </td>
        <td>
        <div class="control-group ">
            <?php echo $form->labelEx($modelLaporan,'sampai', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php
//                $modelLaporan->tglAkhir = $format->formatDateTimeMediumForUser($modelLaporan->tglAkhir);
                $this->widget('MyDateTimePicker',array(
                    'model'=>$modelLaporan,
                    'attribute'=>'tglAkhir',
                    'mode'=>'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                    ),
                )); ?> 
            </div>
        </div>
        </td>
    </tr>
</table>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    </div>
<?php
$this->endWidget();
?>