<?php
$url = Yii::app()->createUrl('akuntansi/laporanAkuntansi/actionFrameGrafikLaporanSaldoPerAkun&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Saldo Per Akun</legend>
<div class="search-form">
<?php $this->renderPartial('akuntansi.views.laporanAkuntansi.saldoperakun/_search',array('modelLaporan'=>$model)); ?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Saldo</legend>
    <?php $this->renderPartial('akuntansi.views.laporanAkuntansi.saldoperakun/_table', array('model'=>$model)); ?>
    <?php // $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanSaldoPerAkun');
    $this->renderPartial('akuntansi.views.laporanAkuntansi._footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>