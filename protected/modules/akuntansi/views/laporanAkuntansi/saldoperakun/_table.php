<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $modelLaporan->searchTable();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>
<?php
  
?>

<div id="tableLaporan" class="grid-view" >
  <table class="table table-striped table-bordered table-condensed">
    <thead>
        <tr>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>No. Rekening</center>
              </th>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Nama Rekening</center>
              </th>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Saldo Awal</center>
              </th>
              <th id="tableLaporan_c0" colspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Mutasi</center>
              </th>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Saldo Akhir</center>
              </th>
        </tr>
        <tr>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Debit</center>
              </th>
              <th id="tableLaporan_c0" rowspan="2" style="height: 20px; vertical-align: middle;">
                  <center>Kredit</center>
              </th>
        </tr>
    </thead>
    <tbody>
        <?php
            $mnorek4 = $model->getNorek4();
            foreach ($mnorek4 as $i=>$vnorek4){
		$saldoawal=0;
                $subtotsaldodebit=0;
                $subtotsaldokredit=0;
                $saldoakhir=0;
                $subtotalsaldoakhir=0;
		$jml=0;
		
        ?>
            <tr>
                <td>
                    <b><?php echo $vnorek4->kdrekening1."-".$vnorek4->kdrekening2."-".$vnorek4->kdrekening3."-".$vnorek4->kdrekening4;?></b>
                </td>
                <td colspan="5">
                    <b><i><?php echo $vnorek4->nmrekening4;?></i></b>
                </td>
            </tr>
                <?php
                    $mnorek = $model->getNorek($vnorek4->kdrekening1,$vnorek4->kdrekening2,$vnorek4->kdrekening3,$vnorek4->kdrekening4);
			foreach ($mnorek as $i=>$vnorek){$jml+=count($jml);}
			foreach ($mnorek as $i=>$vnorek){
			$saldoawal=$model->getSaldoAwal($vnorek->rekening5_id);
                        $subtotsaldodebit += $vnorek->saldodebit;
                        $subtotsaldokredit += $vnorek->saldokredit;
                        $saldoakhir = $saldoawal+$vnorek->saldoakhirberjalan;
                        $subtotalsaldoakhir += $saldoakhir;
			
                ?>
                <tr>
                    <td><?php echo $vnorek->kdrekening1."-".$vnorek->kdrekening2."-".$vnorek->kdrekening3."-".$vnorek->kdrekening4."-".$vnorek->kdrekening5;?></td>
                    <td><?php echo $vnorek->nmrekening5; ?></td>
                    <td><?php echo number_format($saldoawal); ?></td>
			<?php
//				echo "<td rowspan='".$jml."'>".number_format($saldoawal)."</td>";
			?>
                    <td><?php echo number_format($vnorek->saldodebit);?></td>
                    <td><?php echo number_format($vnorek->saldokredit);?></td>
                    <td><?php echo number_format($saldoakhir);?></td>
                </tr>
                <?php
                    }
                ?>
            <tr>
                <td colspan="3" style="text-align: right; padding-right: 3em"><i><b>Total <?php echo $vnorek4->nmrekening4;?></b></i></td>
                <td><b><?php echo number_format($subtotsaldodebit);?></b></td>
                <td><b><?php echo number_format($subtotsaldokredit);?></b></td>
                <td><b><?php echo number_format($subtotalsaldoakhir);?></b></td>
            </tr>
            <thead><tr><td style="height: 15px;" colspan="6" bgcolor="e0e0e9"></td></tr></thead>
        <?php
            }
        ?>
    </tbody>
  </table>
</div>