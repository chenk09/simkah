<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $modelLaporan->searchNeraca();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        // $data = $model->searchNeraca();
        //  $template = "{pager}{summary}\n{items}";
    }
?>
<?php
  $j=0;
  foreach ($jmlRekening as $key => $value) {
    $j += $value['urutan'];
    $urutan[$key] = $j;
  }

  $a=0;
  foreach ($jmlStruktur as $key => $jmls) {
    $a += $jmls['urutan'];
    $struktur[$key] = $a;
  }
  $modRuangans = $modelLaporan->getRuangan();
?>

<div id="tableLaporan" class="grid-view" style="max-width:1250px;overflow-x:scroll;">
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th id="tableLaporan_c0" rowspan="2">
            <center>Nama Rekening</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
            <center>Jumlah Nominal</center>
        </th>
        <?php
        $colspan = 1;
        foreach ($modRuangans as $i=>$instalasi){
            $instalasi_nama[$i] =  $instalasi->instalasi_nama;
            if ($i != 0 && $instalasi_nama[$i-1] == $instalasi_nama[$i]){
                $colspan++;
                 $tr[$i-1] = "<th id='tableLaporan_c0' colspan=".$colspan."><center>".$instalasi->instalasi_nama."</center></th>";
            } else {
                $colspan = 1;
                $tr[$i] = "<th id='tableLaporan_c0'><center>".$instalasi->instalasi_nama."</center></th>";
            }
        }
        foreach ($tr as $th){
            echo $th;
        }
        ?>
      </tr>
      <tr>
        <?php
        foreach ($modRuangans as $ruangan) {
            echo "<th id='tableLaporan_c0'><center>".$ruangan->ruangan_nama."</center></th>";
        }
        ?>
      </tr>
    </thead>
    <tbody>
        <?php
        $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        if (count($model)>0){
        ?>

        <?php
        foreach ($modRuangans as $ruangan){
            $jmlruang+=count($jmlruang);
        } ?>
        
        <!----------------- BEGIN PER STRUKTUR ---------------------------------------------->
        <tr><td style="height: 30px; vertical-align: middle;" colspan="<?php echo $jmlruang+3; ?>"><b><i>AKTIVA</i></b></td></tr>
        <?php
            $jml_aktiva = 0;
            $count_aktiva = 0;
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_AKTIVA){
                    echo "<tr>";
                        echo "<td>".$spasi.$neraca->nmstruktur."</td>";
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                        echo "<td>".number_format($nilai_nominal,0)."</td>";
                        $jml_aktiva +=$nilai_nominal;
                        $count_aktiva++;
                        foreach ($modRuangans as $ruangan){
                            $nilai_per_ruangan = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                            echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
                        }
                    echo "<tr>";
                }
            }
            
        ?>
        <tr>
            <td style="text-align: right; padding-right: 1em"><i><b>TOTAL AKTIVA &nbsp; &nbsp;</b></i></td>
            <?php
            echo "<td><b><i>".number_format($jml_aktiva,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                if ($count_aktiva > 0){
                    $nilai_per_ruangan = $modelLaporan->getSaldoTotal(Params::ID_STRUKTUR_AKTIVA, '', '', $ruangan->ruangan_id);
                    echo "<td><b><i>".number_format($nilai_per_ruangan,0)."</b></i></td>";
                } else {
                    echo "<td><b><i>0</b></i></td>";
                }
            }
            ?>
        </tr>
        <thead><tr><th style="height: 20px; vertical-align: middle;" colspan="<?php echo $jmlruang+3; ?>"></th></tr></thead>
        <!----------------- END PER STRUKTUR ---------------------------------------------->
        
        
        <!----------------- BEGIN PER STRUKTUR ---------------------------------------------->
        <tr><td style="height: 30px; vertical-align: middle;" colspan="<?php echo $jmlruang+3; ?>"><b><i>PASSIVA</i></b></td></tr>
        <?php
            $jml_wajib = 0;
            $count_wajib = 0;
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_KEWAJIBAN){
                    echo "<tr>";
                        echo "<td>".$spasi.$neraca->nmstruktur."</td>";
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                        echo "<td>".number_format($nilai_nominal,0)."</td>";
                        $jml_wajib +=$nilai_nominal;
                        $count_wajib++;
                        foreach ($modRuangans as $ruangan){
                            $nilai_per_ruangan = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                            echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
                        }
                    echo "<tr>";
                }
            }
        ?>
        <?php
            $jml_ekuitas = 0;
            $count_ekuitas = 0;
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_EKUITAS){
                    echo "<tr>";
                        echo "<td>".$spasi.$neraca->nmstruktur."</td>";
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                        echo "<td>".number_format($nilai_nominal,0)."</td>";
                        $jml_ekuitas +=$nilai_nominal;
                        $count_ekuitas++;
                        foreach ($modRuangans as $ruangan){
                            $nilai_per_ruangan = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                            echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
                        }
                    echo "<tr>";
                }
            }
        ?>
        
        <tr>
            <td style="text-align: right; padding-right: 1em"><i><b>TOTAL PASSIVA &nbsp; &nbsp;</b></i></td>
            <?php
            $jml_wajib_ekuitas = 0;
            $nilai_per_ruangan = array();
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_KEWAJIBAN || $neraca->struktur_id == Params::ID_STRUKTUR_EKUITAS){
                    $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                    $jml_wajib_ekuitas +=$nilai_nominal;
                }
            }
            echo "<td><b><i>".number_format($jml_wajib_ekuitas,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                $jml_wajib_ekuitas_per_ruangan = 0;
                foreach ($model as $i=>$neraca){
                    if ($neraca->struktur_id == Params::ID_STRUKTUR_KEWAJIBAN || $neraca->struktur_id == Params::ID_STRUKTUR_EKUITAS){
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                        $jml_wajib_ekuitas_per_ruangan +=$nilai_nominal;
                    }
                }
                echo "<td><b><i>".number_format($jml_wajib_ekuitas_per_ruangan,0)."</b></i></td>";
            }
        ?>
        </tr>
        <thead><tr><th style="height: 20px; vertical-align: middle;" colspan="<?php echo $jmlruang+3; ?>"></th></tr></thead>
        <!----------------- END PER STRUKTUR ---------------------------------------------->
        
      <?php
      }
      ?>
    </tbody>
  </table>
</div>