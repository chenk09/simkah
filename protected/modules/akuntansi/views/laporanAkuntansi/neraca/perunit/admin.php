<?php
$url = Yii::app()->createUrl('akuntansi/laporanAkuntansi/frameGrafikLaporanJurnal&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Posisi Keuangan / Neraca</legend>
<div class="search-form">
<?php $this->renderPartial('akuntansi.views.laporanAkuntansi.neraca/_search',array('model'=>$model,'modelLaporan'=>$modelLaporan)); 
?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Neraca</legend>
    <?php $this->renderPartial('akuntansi.views.laporanAkuntansi.neraca.perunit/_tableDetail', array('model'=>$model,'modelLaporan'=>$modelLaporan, 'jmlRekening'=>$jmlRekening,'jmlStruktur'=>$jmlStruktur, 'modTotalNominal'=>$modTotalNominal)); ?>
    <?php //$this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanNeracaPerUnit');
    $this->renderPartial('akuntansi.views.laporanAkuntansi._footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>