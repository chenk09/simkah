<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchNeraca();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        // $data = $model->searchNeraca();
        //  $template = "{pager}{summary}\n{items}";
    }
?>
<?php
  $j=0;
  foreach ($jmlRekening as $key => $value) {
    $j += $value['urutan'];
    $urutan[$key] = $j;
  }

  $a=0;
  foreach ($jmlStruktur as $key => $jmls) {
    $a += $jmls['urutan'];
    $struktur[$key] = $a;
  }
  $modRuangans = $modelLaporan->getRuangan();
?>

<div id="tableLaporan" class="grid-view" style="max-width:1250px;overflow-x:scroll;">
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th id="tableLaporan_c0" rowspan="2">
            <center>Rincian</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
            <center>Jumlah Nominal</center>
        </th>
        <?php
        $colspan = 1;
        foreach ($modRuangans as $i=>$instalasi){
            $instalasi_nama[$i] =  $instalasi->instalasi_nama;
            if ($i != 0 && $instalasi_nama[$i-1] == $instalasi_nama[$i]){
                $colspan++;
                 $tr[$i-1] = "<th id='tableLaporan_c0' colspan=".$colspan."><center>".$instalasi->instalasi_nama."</center></th>";
            } else {
                $colspan = 1;
                $tr[$i] = "<th id='tableLaporan_c0'><center>".$instalasi->instalasi_nama."</center></th>";
            }
        }
        foreach ($tr as $th){
            echo $th;
        }
        ?>
      </tr>
      <tr>
        <?php
        foreach ($modRuangans as $ruangan) {
            echo "<th id='tableLaporan_c0'><center>".$ruangan->ruangan_nama."</center></th>";
        }
        ?>
      </tr>
    </thead>
    <tbody>
        <?php
        $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        if (count($model)>0){
        ?>
        <tr>
            <td>
                <b>Aktiva</b>
            </td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aktiva Lancar</b>
            </td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <?php
            $jml_aktiva = 0;
            $count_aktiva = 0;
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_AKTIVA && $neraca->kelompok_id == Params::ID_KELOMPOK_AKTIVA_LANCAR){
                    echo "<tr>";
                        echo "<td>".$spasi.$neraca->nmjenis."</td>";
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                        echo "<td>".number_format($nilai_nominal,0)."</td>";
                        $jml_aktiva +=$nilai_nominal;
                        $count_aktiva++;
                        foreach ($modRuangans as $ruangan){
                            $nilai_per_ruangan = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                            echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
                        }
                    echo "<tr>";
                }
            }
        ?>
        <tr>
            <td>
                <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Aktiva Lancar</b>
            </td>
            <?php
            echo "<td><b><i>".number_format($jml_aktiva,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                if ($count_aktiva > 0){
                    $nilai_per_ruangan = $modelLaporan->getSaldoTotal(Params::ID_STRUKTUR_AKTIVA, Params::ID_KELOMPOK_AKTIVA_LANCAR, '', $ruangan->ruangan_id);
                    echo "<td><b><i>".number_format($nilai_per_ruangan,0)."</b></i></td>";
                } else {
                    echo "<td><b><i>0</b></i></td>";
                }
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>Jumlah Aktiva</b>
            </td>
            <?php
            echo "<td><b><i>".number_format($jml_aktiva,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                if ($count_aktiva > 0){
                    $nilai_per_ruangan = $modelLaporan->getSaldoTotal(Params::ID_STRUKTUR_AKTIVA, '', '', $ruangan->ruangan_id);
                    echo "<td><b><i>".number_format($nilai_per_ruangan,0)."</b></i></td>";
                } else {
                    echo "<td><b><i>0</b></i></td>";
                }
            }
            ?>
        </tr>
        <tr bgcolor="B8C9E0">
            <td>&nbsp;</td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>Pendapatan Operasional</b>
            </td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pendapatan Operasional</b>
            </td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <?php
            $jml_pendapatan = 0;
            $count_pend = 0;
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_PENDAPATAN_OP && $neraca->kelompok_id == Params::ID_KELOMPOK_PENDAPATAN_OP){
                    echo "<tr>";
                        echo "<td>".$spasi.$neraca->nmjenis."</td>";
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                        echo "<td>".number_format($nilai_nominal,0)."</td>";
                        $jml_pendapatan +=$nilai_nominal;
                        $count_pend++;
                        foreach ($modRuangans as $ruangan){
                            $nilai_per_ruangan = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                            echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
                        }
                    echo "<tr>";
                }
            }
        ?>
        <tr>
            <td>
                <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Pendapatan Operasional</b>
            </td>
            <?php
            echo "<td><b><i>".number_format($jml_pendapatan,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                if ($count_pend > 0){
                    $nilai_per_ruangan = $modelLaporan->getSaldoTotal(Params::ID_STRUKTUR_PENDAPATAN_OP, Params::ID_KELOMPOK_PENDAPATAN_OP, '', $ruangan->ruangan_id);
                    echo "<td><b><i>".number_format($nilai_per_ruangan,0)."</b></i></td>";
                } else {
                    echo "<td><b><i>0</b></i></td>";
                }
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>Jumlah Pendapatan Operasional</b>
            </td>
            <?php
            echo "<td><b><i>".number_format($jml_pendapatan,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                if ($count_pend > 0){
                    $nilai_per_ruangan = $modelLaporan->getSaldoTotal(Params::ID_STRUKTUR_PENDAPATAN_OP, '', '', $ruangan->ruangan_id);
                    echo "<td><b><i>".number_format($nilai_per_ruangan,0)."</b></i></td>";
                } else {
                    echo "<td><b><i>0</b></i></td>";
                }
            }
            ?>
        </tr>
        <tr bgcolor="B8C9E0">
            <td>&nbsp;</td>
            <td></td>
            <?php
            foreach ($modRuangans as $ruangan){
                echo "<td></td>";
            }
            ?>
        </tr>
        <tr>
            <td>
                <b>Jumlah Kewajiban & Ekuitas</b>
            </td>
            <?php
            $jml_wajib_ekuitas = 0;
            $nilai_per_ruangan = array();
            foreach ($model as $i=>$neraca){
                if ($neraca->struktur_id == Params::ID_STRUKTUR_KEWAJIBAN || $neraca->struktur_id == Params::ID_STRUKTUR_EKUITAS){
                    $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id);
                    $jml_wajib_ekuitas +=$nilai_nominal;
                }
            }
            echo "<td><b><i>".number_format($jml_wajib_ekuitas,0)."</b></i></td>";
            foreach ($modRuangans as $ruangan){
                $jml_wajib_ekuitas_per_ruangan = 0;
                foreach ($model as $i=>$neraca){
                    if ($neraca->struktur_id == Params::ID_STRUKTUR_KEWAJIBAN || $neraca->struktur_id == Params::ID_STRUKTUR_EKUITAS){
                        $nilai_nominal = $modelLaporan->getSaldoTotal($neraca->struktur_id, $neraca->kelompok_id, $neraca->jenis_id, $ruangan->ruangan_id);
                        $jml_wajib_ekuitas_per_ruangan +=$nilai_nominal;
                    }
                }
                echo "<td><b><i>".number_format($jml_wajib_ekuitas_per_ruangan,0)."</b></i></td>";
            }
        }
        ?>
        </tr>
      <?php
//        foreach ($model as $key => $value) {
//          $nmstruktur[$key] = $value['nmstruktur'];
//          if($nmstruktur[$key-1]!=$nmstruktur[$key])
//          {
//            $rincian = $value['nmstruktur'];
//            $tab = "";
//            $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
//            echo '<tr>';
//              echo '<td>'.$tab.''.$rincian.'</td><td></td>';
//              foreach ($modRuangans as $ruangan) {
//                echo "<td></td>";
//              }
//            echo '</tr>';
//            $nmkelompok1[$key] = $value['nmkelompok'];
//            $tab1 = $spasi;
//            echo '<tr>';
//              echo '<td>'.$tab1.''.$nmkelompok1[$key].'</td><td></td>';
//              foreach ($modRuangans as $ruangan) {
//                echo "<td></td>";
//              }
//            echo '</tr>';
//            $nmjenis1[$key] = $value['nmjenis'];
//            $tab2 = $tab1.$spasi;
//            echo '<tr>';
//              echo '<td>'.$tab2.''.$nmjenis1[$key].'</td>';
//              echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'])->nominal).'</td>'; 
//              foreach ($modRuangans as $ruangan) {
//                echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'],$ruangan->ruangan_id)->nominal).'</td>'; 
//              }
//            echo '</tr>';
//          }else{
//            $nmkelompok[$key] = $value['nmkelompok'];
//            if($nmkelompok1[$key-1]!=$nmkelompok[$key])
//            {
//              $rincian2 = $value['nmkelompok'];
//              $tab = $spasi;
//              echo '<tr>';
//                echo '<td>'.$tab.''.$rincian2.'</td><td></td>';
//                foreach ($modRuangans as $ruangan) {
//                    echo "<td></td>";
//                }
//              echo '</tr>';
//              $nmjenis1[$key] = $value['nmjenis'];
//              $tab2 = $tab.$spasi;
//              echo '<tr>';
//                echo '<td>'.$tab2.''.$nmjenis1[$key].'</td><td></td>';
//                foreach ($modRuangans as $ruangan) {
//                    echo "<td></td>";
//                }
//                echo '<td>'.number_format($value['nominal'],0).'</td>';                
//              echo '</tr>';
//            }else{
//              $nmjenis[$key] = $value['nmjenis'];
//              if($nmjenis1[$key-1]!=$nmjenis[$key])
//              {
//                $rincian3 = $value['nmjenis'];
//                $tab2 = $tab1.$spasi;
//                echo '<tr>';
//                  echo '<td>'.$tab2.''.$rincian3.'</td>';
//                  echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'])->nominal).'</td>';
//                  foreach ($modRuangans as $ruangan) {
//                    echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'],$ruangan->ruangan_id)->nominal).'</td>'; 
//                  }
//                echo '</tr>';
//              }else{
//              }
//            }
//          } 
//          $i++;
//          $total_nilai += $value['nominal'];
//          
//          if(in_array($i, $urutan))
//          {
//            echo '<tr>';      
//              echo '<td>'.$tab1.'<i><b>Total '.$value['nmkelompok'].'</b><i></td>';
//              echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'])->nominal).'</td>';
////              $total_per_ruangan = array();
////              foreach ($modRuangans as $i=>$ruangan) {
////                $total_per_ruangan[] = $modTotalNominal->getSaldoTotal($value['kdstruktur'],$value['kdkelompok'],'',$ruangan->ruangan_id);
////                echo '<td>'.number_format($total_per_ruangan[$i]).'</td>'; 
////              }
//            echo '</tr>';
//            $total_nilai = 0;
//          }  
//
//          $total_struktur += $value['nominal'];
//          
//          if(in_array($i, $struktur))
//          {
//            echo '<tr>';      
//              echo '<td><i><b>Total abc '.$value['nmstruktur'].'</b><i></td>';
//              echo '<td><i><b>'.number_format($total_struktur,0).'</b><i></td>';
//            echo '</tr>';
//            if($value['nmstruktur']=='Kewajiban' || $value['nmstruktur']=='Pendapatan Operasional' || $value['nmstruktur']=='Beban operasional'){
//              $total += $total_struktur;
//            }
//            $total_struktur = 0;
//          }
//
//          if($i == count($model)){
//            echo '<tr>';      
//              echo '<td><i><b>Total Kewajiban & Ekuitas</b><i></td>';
//              echo '<td><i><b>'.number_format($total,0).'</b><i></td>';
//            echo '</tr>';
//          }
//        }
      ?>
    </tbody>
  </table>
</div>