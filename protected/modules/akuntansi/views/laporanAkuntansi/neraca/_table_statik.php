<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchNeraca();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        // $data = $model->searchNeraca();
        //  $template = "{pager}{summary}\n{items}";
    }
?>
<?php
  $j=0;
  foreach ($jmlRekening as $key => $value) {
    $j += $value['urutan'];
    $urutan[$key] = $j;
  }

  $a=0;
  foreach ($jmlStruktur as $key => $jmls) {
    $a += $jmls['urutan'];
    $struktur[$key] = $a;
  }
  
    
?>

<div id="tableLaporan" class="grid-view" style="max-width:1250px;overflow-x:scroll;">
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Rincian</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Total Nominal</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Direktur</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Satuan Pengawas Internal (SPI)</center>
        </th>
        <th id="tableLaporan_c0" colspan="7">
        <center>Komite Independen</center>
        </th>
        <th id="tableLaporan_c0" colspan="15">
        <center>Wakil Direktur Umum dan Keuangan</center>
        </th>
        <th id="tableLaporan_c0" colspan="9">
        <center>Wakil Direktur Pelayanan</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Pelayanan Khusus</center>
        </th>
        <th id="tableLaporan_c0" colspan="13">
        <center>Instalasi Rawat Inap</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Gawat Darurat</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Bedah Sentral</center>
        </th>
        <th id="tableLaporan_c0" colspan="12">
        <center>Instalasi Rawat Jalan</center>
        </th>
        <th id="tableLaporan_c0" colspan="4">
        <center>Instalasi Rawat Intensif</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Sterilisasi Sentral</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Haemodialisa</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi ESWL</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Endoscopy</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Gizi</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Farmasi</center>
        </th>
        <th id="tableLaporan_c0" colspan="2">
        <center>Instalasi Radiologi dan Laboratorium</center>
        </th>
        <th id="tableLaporan_c0" colspan="1">
        <center>Instalasi Pemeliharaan Sarana Rumah Sakit</center>
        </th>
        <th id="tableLaporan_c0" colspan="3">
        <center>Instalasi Sanitasi Laundry & Pulasara Jenazah</center>
        </th>
        <th id="tableLaporan_c0" rowspan="2">
        <center>Instalasi Keamanan & Transportasi</center>
        </th>
      </tr>
      <tr>
        <th id="tableLaporan_c0">
        <center>Komite Medik</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Komite Keperawatan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Komite Etik dan Hukum</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Panitia Mutu</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Panitia KPRS</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Panitia K3</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Staf Medik Fungsional (SMF)</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bagian Sekretariat</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Tata Usaha</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Kepegawaian</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Rumah Tangga dan Perlengkapan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Diklat</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Humas dan pemasaran</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bagian Perencanaan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Pengendalian P3</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Penyusunan Program dan Laporan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Hukum</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Rekam Medik</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bagian Keuangan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Penyusunan Anggaran</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Pembendaharaan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sub Bagian Akuntansi dan Mobilisasi Dana</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bidang Pelayanan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi Pelayanan Medis</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi Rujukan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bidang Penunjang Pelayanan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi Penunjang Medis</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Bidang Keperawatan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi Asuhan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi SDM Keperawatan</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Seksi Logistik</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Observasi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas IA</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas I B</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas I C</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas Madya</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas II</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas III</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas Utama</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas VIP</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas VIP B</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Kelas VVIP</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Partus A</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Ruangan Perinatologi A</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Umum</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Anak</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Bedah</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Bedah Syaraf</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Gigi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Konservasi Gigi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Obgyn</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Dalam</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Syaraf</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly THT</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Urologi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Poly Kemoterafi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>ICU</center>
        </th>
        <th id="tableLaporan_c0">
        <center>PICU</center>
        </th>
        <th id="tableLaporan_c0">
        <center>NICU</center>
        </th>
        <th id="tableLaporan_c0">
        <center>PIU</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Radiologi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Laboratorium</center>
        </th>
        <th id="tableLaporan_c0">
        <center>UPRS</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Sanitasi</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Laundry</center>
        </th>
        <th id="tableLaporan_c0">
        <center>Pulasara Jenasah</center>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php
        $nmstruktur[-1] = '';
        $nmkelompok[-1] = '';
        $nmjenis[-1]    = '';
        $i = 0;
        $total_nilai = 0;
        $total_struktur = 0;
        $total = 0;
        
        $total_direktur = 0;
        $total_spi = 0;
        $total_k_medik = 0;
        $total_k_keperawatan = 0;
        $total_k_etik = 0;
        $total_p_mutu = 0;
        $total_p_kprs = 0;
        $total_p_k3 = 0;
        $total_smf = 0;
        $total_bg_sekre = 0;
        $total_sb_tatausaha = 0;
        $total_sb_kepegawaian = 0;
        $total_sb_rumahtangga = 0;
        $total_sb_diklat = 0;
        $total_sb_humas = 0;
        $total_bg_perencanaan = 0;
        $total_sb_kendalip3 = 0;
        $total_sb_program = 0;
        $total_sb_hukum = 0;
        $total_sb_rm = 0;
        $total_bg_keuangan = 0;
        $total_sb_anggaran = 0;
        $total_sb_bendahara = 0;
        $total_sb_akuntansi = 0;
        $total_bd_pelayanan = 0;
        $total_sk_pel_medis = 0;
        $total_sk_rujukan = 0;
        $total_bd_penunjang_pel = 0;
        $total_sk_penunjang_med = 0;
        $total_bd_perawat = 0;
        $total_sk_asuhan = 0;
        $total_sk_sdm_perawat = 0;
        $total_sk_logistik = 0;
        $total_in_pel_khusus = 0;
        $total_r_observasi = 0;
        $total_r_1a = 0;
        $total_r_1b = 0;
        $total_r_1c = 0;
        $total_r_madya = 0;
        $total_r_2 = 0;
        $total_r_3 = 0;
        $total_r_utama = 0;
        $total_r_vip = 0;
        $total_r_vip_b = 0;
        $total_r_vvip = 0;
        $total_r_partus_a = 0;
        $total_r_perinatologi_a = 0;
        $total_igd = 0;
        $total_ibd = 0;
        $total_poli_umum = 0;
        $total_poli_anak = 0;
        $total_poli_bedah = 0;
        $total_poli_bedahsyaraf = 0;
        $total_poli_gigi = 0;
        $total_poli_konv_gigi = 0;
        $total_poli_obgyn = 0;
        $total_poli_dalam = 0;
        $total_poli_syaraf = 0;
        $total_poli_tht = 0;
        $total_poli_urologi = 0;
        $total_poli_kemoterafi = 0;
        $total_icu = 0;
        $total_picu = 0;
        $total_nicu = 0;
        $total_piu = 0;
        $total_in_steril= 0;
        $total_in_hemodialisa= 0;
        $total_in_eswl= 0;
        $total_in_endoscopy= 0;
        $total_in_gizi= 0;
        $total_in_farmasi= 0;
        $total_rad= 0;
        $total_lab= 0;
        $total_uprs= 0;
        $total_sanitasi= 0;
        $total_laundry= 0;
        $total_jenazah= 0;
        $total_keamanan= 0;
        
        $total_struktur_direktur = 0;
        $total_struktur_spi = 0;
        $total_struktur_k_medik = 0;
        $total_struktur_k_keperawatan = 0;
        $total_struktur_k_etik = 0;
        $total_struktur_p_mutu = 0;
        $total_struktur_p_kprs = 0;
        $total_struktur_p_k3 = 0;
        $total_struktur_smf = 0;
        $total_struktur_bg_sekre = 0;
        $total_struktur_sb_tatausaha = 0;
        $total_struktur_sb_kepegawaian = 0;
        $total_struktur_sb_rumahtangga = 0;
        $total_struktur_sb_diklat = 0;
        $total_struktur_sb_humas = 0;
        $total_struktur_bg_perencanaan = 0;
        $total_struktur_sb_kendalip3 = 0;
        $total_struktur_sb_program = 0;
        $total_struktur_sb_hukum = 0;
        $total_struktur_sb_rm = 0;
        $total_struktur_bg_keuangan = 0;
        $total_struktur_sb_anggaran = 0;
        $total_struktur_sb_bendahara = 0;
        $total_struktur_sb_akuntansi = 0;
        $total_struktur_bd_pelayanan = 0;
        $total_struktur_sk_pel_medis = 0;
        $total_struktur_sk_rujukan = 0;
        $total_struktur_bd_penunjang_pel = 0;
        $total_struktur_sk_penunjang_med = 0;
        $total_struktur_bd_perawat = 0;
        $total_struktur_sk_asuhan = 0;
        $total_struktur_sk_sdm_perawat = 0;
        $total_struktur_sk_logistik = 0;
        $total_struktur_in_pel_khusus = 0;
        $total_struktur_r_observasi = 0;
        $total_struktur_r_1a = 0;
        $total_struktur_r_1b = 0;
        $total_struktur_r_1c = 0;
        $total_struktur_r_madya = 0;
        $total_struktur_r_2 = 0;
        $total_struktur_r_3 = 0;
        $total_struktur_r_utama = 0;
        $total_struktur_r_vip = 0;
        $total_struktur_r_vip_b = 0;
        $total_struktur_r_vvip = 0;
        $total_struktur_r_partus_a = 0;
        $total_struktur_r_perinatologi_a = 0;
        $total_struktur_igd = 0;
        $total_struktur_ibd = 0;
        $total_struktur_poli_umum = 0;
        $total_struktur_poli_anak = 0;
        $total_struktur_poli_bedah = 0;
        $total_struktur_poli_bedahsyaraf = 0;
        $total_struktur_poli_gigi = 0;
        $total_struktur_poli_konv_gigi = 0;
        $total_struktur_poli_obgyn = 0;
        $total_struktur_poli_dalam = 0;
        $total_struktur_poli_syaraf = 0;
        $total_struktur_poli_tht = 0;
        $total_struktur_poli_urologi = 0;
        $total_struktur_poli_kemoterafi = 0;
        $total_struktur_icu = 0;
        $total_struktur_picu = 0;
        $total_struktur_nicu = 0;
        $total_struktur_piu = 0;
        $total_struktur_in_steril= 0;
        $total_struktur_in_hemodialisa= 0;
        $total_struktur_in_eswl= 0;
        $total_struktur_in_endoscopy= 0;
        $total_struktur_in_gizi= 0;
        $total_struktur_in_farmasi= 0;
        $total_struktur_rad= 0;
        $total_struktur_lab= 0;
        $total_struktur_uprs= 0;
        $total_struktur_sanitasi= 0;
        $total_struktur_laundry= 0;
        $total_struktur_jenazah= 0;
        $total_struktur_keamanan= 0;
        
        $total_semua_direktur = 0;
        $total_semua_spi = 0;
        $total_semua_k_medik = 0;
        $total_semua_k_keperawatan = 0;
        $total_semua_k_etik = 0;
        $total_semua_p_mutu = 0;
        $total_semua_p_kprs = 0;
        $total_semua_p_k3 = 0;
        $total_semua_smf = 0;
        $total_semua_bg_sekre = 0;
        $total_semua_sb_tatausaha = 0;
        $total_semua_sb_kepegawaian = 0;
        $total_semua_sb_rumahtangga = 0;
        $total_semua_sb_diklat = 0;
        $total_semua_sb_humas = 0;
        $total_semua_bg_perencanaan = 0;
        $total_semua_sb_kendalip3 = 0;
        $total_semua_sb_program = 0;
        $total_semua_sb_hukum = 0;
        $total_semua_sb_rm = 0;
        $total_semua_bg_keuangan = 0;
        $total_semua_sb_anggaran = 0;
        $total_semua_sb_bendahara = 0;
        $total_semua_sb_akuntansi = 0;
        $total_semua_bd_pelayanan = 0;
        $total_semua_sk_pel_medis = 0;
        $total_semua_sk_rujukan = 0;
        $total_semua_bd_penunjang_pel = 0;
        $total_semua_sk_penunjang_med = 0;
        $total_semua_bd_perawat = 0;
        $total_semua_sk_asuhan = 0;
        $total_semua_sk_sdm_perawat = 0;
        $total_semua_sk_logistik = 0;
        $total_semua_in_pel_khusus = 0;
        $total_semua_r_observasi = 0;
        $total_semua_r_1a = 0;
        $total_semua_r_1b = 0;
        $total_semua_r_1c = 0;
        $total_semua_r_madya = 0;
        $total_semua_r_2 = 0;
        $total_semua_r_3 = 0;
        $total_semua_r_utama = 0;
        $total_semua_r_vip = 0;
        $total_semua_r_vip_b = 0;
        $total_semua_r_vvip = 0;
        $total_semua_r_partus_a = 0;
        $total_semua_r_perinatologi_a = 0;
        $total_semua_igd = 0;
        $total_semua_ibd = 0;
        $total_semua_poli_umum = 0;
        $total_semua_poli_anak = 0;
        $total_semua_poli_bedah = 0;
        $total_semua_poli_bedahsyaraf = 0;
        $total_semua_poli_gigi = 0;
        $total_semua_poli_konv_gigi = 0;
        $total_semua_poli_obgyn = 0;
        $total_semua_poli_dalam = 0;
        $total_semua_poli_syaraf = 0;
        $total_semua_poli_tht = 0;
        $total_semua_poli_urologi = 0;
        $total_semua_poli_kemoterafi = 0;
        $total_semua_icu = 0;
        $total_semua_picu = 0;
        $total_semua_nicu = 0;
        $total_semua_piu = 0;
        $total_semua_in_steril= 0;
        $total_semua_in_hemodialisa= 0;
        $total_semua_in_eswl= 0;
        $total_semua_in_endoscopy= 0;
        $total_semua_in_gizi= 0;
        $total_semua_in_farmasi= 0;
        $total_semua_rad= 0;
        $total_semua_lab= 0;
        $total_semua_uprs= 0;
        $total_semua_sanitasi= 0;
        $total_semua_laundry= 0;
        $total_semua_jenazah= 0;
        $total_semua_keamanan= 0;
        foreach ($model as $key => $value) {
          $nmstruktur[$key] = $value['nmstruktur'];
          if($nmstruktur[$key-1]!=$nmstruktur[$key])
          {
            $rincian = $value['nmstruktur'];
            $tab = "";
            echo '<tr>';
              echo '<td>'.$tab.''.$rincian.'</td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>';
            echo '</tr>';
            $nmkelompok1[$key] = $value['nmkelompok'];
            $tab1 = $spasi;
            echo '<tr>';
              echo '<td>'.$tab1.''.$nmkelompok1[$key].'</td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>';
            echo '</tr>';
            $nmjenis1[$key] = $value['nmjenis'];
            $tab2 = $tab1.$spasi;
            echo '<tr>';
              echo '<td>'.$tab2.''.$nmjenis1[$key].'</td>';
//              $a = $modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis']);
              echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'])->nominal).'</td>';
//              Direktur PT Karsa Abdi Husada dan Direktur RS Jasa Kartini
              $nilai_direktur = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_DIREKTUR,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_direktur,0).'</td>';
/////////////////////////////////////////////////////////////
//              Satuan Pengawas Internal (SPI)
              $nilai_spi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_spi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Komite Medik
              $nilai_k_medik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_medik,0).'</td>';
//              Komite Keperawatan
              $nilai_k_keperawatan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_keperawatan,0).'</td>';
//              Komite Etik dan Hukum
              $nilai_k_etik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_etik,0).'</td>';
//              Panitia Mutu
              $nilai_p_mutu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_mutu,0).'</td>';
//              Panitia KPRS
              $nilai_p_kprs = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_kprs,0).'</td>';
//              Panitia K3
              $nilai_p_k3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_k3,0).'</td>';
//              Staf Medik Fungsional (SMF)
              $nilai_smf = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_smf,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bagian Sekretariat
              $nilai_bg_sekre = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SEKRETARIS,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_sekre,0).'</td>';
//              Sub Bagian Tata Usaha
              $nilai_sb_tatausaha = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_tatausaha,0).'</td>';
//              Sub Bagian Kepegawaian
              $nilai_sb_kepegawaian = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEPEGAWAIAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kepegawaian,0).'</td>';
//              Sub Bagian Rumah Tangga dan Perlengkapan
              $nilai_sb_rumahtangga = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_rumahtangga,0).'</td>';
//              Sub Bagian Diklat
              $nilai_sb_diklat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_diklat,0).'</td>';
//              Sub Bagian Humas dan pemasaran
              $nilai_sb_humas = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_humas,0).'</td>';
//              Bagian Perencanaan
              $nilai_bg_perencanaan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_perencanaan,0).'</td>';
//              Sub Bagian Pengendalian P3
              $nilai_sb_kendalip3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kendalip3,0).'</td>';
//              Sub Bagian Penyusunan Program dan Laporan
              $nilai_sb_program = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_program,0).'</td>';
//              Sub Bagian Hukum
              $nilai_sb_hukum = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_hukum,0).'</td>';
//              Sub Bagian Rekam Medik
              $nilai_sb_rm = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PENDAFTARAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_sb_rm,0).'</td>';
//              Bagian Keuangan
              $nilai_bg_keuangan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEUANGAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_bg_keuangan,0).'</td>';
//              Sub Bagian Penyusunan Anggaran
              $nilai_sb_anggaran = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_anggaran,0).'</td>';
//              Sub Bagian Pembendaharaan
              $nilai_sb_bendahara = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_BENDAHARA,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_bendahara,0).'</td>';
//              Sub Bagian Akuntansi dan Mobilisasi Dana
              $nilai_sb_akuntansi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_AKUNTANSI,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_akuntansi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bidang Pelayanan
              $nilai_bd_pelayanan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_pelayanan,0).'</td>';
//              Seksi Pelayanan Medis
              $nilai_sk_pel_medis = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_pel_medis,0).'</td>';
//              Seksi Rujukan
              $nilai_sk_rujukan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_rujukan,0).'</td>';
//              Bidang Penunjang Pelayanan
              $nilai_bd_penunjang_pel = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_penunjang_pel,0).'</td>';
//              Seksi Penunjang Medis
              $nilai_sk_penunjang_med = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_penunjang_med,0).'</td>';
//              Bidang Keperawatan
              $nilai_bd_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_perawat,0).'</td>';
//              Seksi Asuhan
              $nilai_sk_asuhan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_asuhan,0).'</td>';
//              Seksi SDM Keperawatan
              $nilai_sk_sdm_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_sdm_perawat,0).'</td>';
//              Seksi Logistik
              $nilai_sk_logistik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_logistik,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Pelayanan Khusus
              $nilai_in_pel_khusus = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PELAYANAN_KHUSUS,Params::INSTALASI_ID_PEL_KHUSUS);
              echo '<td>'.number_format($nilai_in_pel_khusus,0).'</td>';
/////////////////////////////////////////////////////////////
//              Ruangan Observasi
              $nilai_r_observasi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSERVASI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_observasi,0).'</td>';
//              Ruangan Kelas IA
              $nilai_r_1a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1a,0).'</td>';
//              Ruangan Kelas IB
              $nilai_r_1b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1b,0).'</td>';
//              Ruangan Kelas I C
              $nilai_r_1c = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1c,0).'</td>';
//              Ruangan Kelas Madya
              $nilai_r_madya = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_madya,0).'</td>';
//              Ruangan Kelas II
              $nilai_r_2 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_2A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_2,0).'</td>';
//              Ruangan Kelas III
              $nilai_r_3 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_3A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_3,0).'</td>';
//              Ruangan Kelas Utama
              $nilai_r_utama = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_utama,0).'</td>';
//              Ruangan Kelas VIP
              $nilai_r_vip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip,0).'</td>';
//              Ruangan Kelas VIP B
              $nilai_r_vip_b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip_b,0).'</td>';
//              Ruangan Kelas VVIP
              $nilai_r_vvip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vvip,0).'</td>';
//              Ruangan Partus A
              $nilai_r_partus_a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_partus_a,0).'</td>';
//              Ruangan Perinatologi A
              $nilai_r_perinatologi_a = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PERINATOLOGI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_perinatologi_a,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Gawat Darurat
              $nilai_igd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAWAT_DARURAT,Params::INSTALASI_ID_RD);
              echo '<td>'.number_format($nilai_igd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Bedah Sentral
              $nilai_ibd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_IBS,Params::INSTALASI_ID_IBS);
              echo '<td>'.number_format($nilai_ibd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Poly Umum
              $nilai_poli_umum = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UMUM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_umum,0).'</td>';
//              Poly Anak
              $nilai_poli_anak = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_ANAK,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_anak,0).'</td>';
//              Poly Bedah
              $nilai_poli_bedah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedah,0).'</td>';
//              Poly Bedah Syaraf
              $nilai_poli_bedahsyaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedahsyaraf,0).'</td>';
//              Poly Gigi
              $nilai_poli_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_gigi,0).'</td>';
//              Poly Konservasi Gigi
              $nilai_poli_konv_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_KONSERVASI_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_konv_gigi,0).'</td>';
//              Poly Obgyn
              $nilai_poli_obgyn = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSGYN,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_obgyn,0).'</td>';
//              Poly Dalam
              $nilai_poli_dalam = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_DALAM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_dalam,0).'</td>';
//              Poly Syaraf
              $nilai_poli_syaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_syaraf,0).'</td>';
//              Poly THT
              $nilai_poli_tht = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_THT,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_tht,0).'</td>';
//              Poly Urologi
              $nilai_poli_urologi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UROLOGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_urologi,0).'</td>';
//              Poly Kemoterafi 
              $nilai_poli_kemoterafi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_kemoterafi,0).'</td>';
/////////////////////////////////////////////////////////////
//              ICU
              $nilai_icu = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ICU,Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_icu,0).'</td>';
//              PICU
              $nilai_picu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_picu,0).'</td>';
//              NICU
              $nilai_nicu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_nicu,0).'</td>';
//              PIU
              $nilai_piu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_piu,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Sterilisasi Sentral
              $nilai_in_steril = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_steril,0).'</td>';     
//              Instalasi Hemodialisa
              $nilai_in_hemodialisa = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_HEMODIALISA,Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_hemodialisa,0).'</td>';     
//              Instalasi ESWL
              $nilai_in_eswl = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ESWL,'bebas');
              echo '<td>'.number_format($nilai_in_eswl,0).'</td>';     
//              Instalasi Endoscopy
              $nilai_in_endoscopy = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ENDOSCOPY,'bebas');
              echo '<td>'.number_format($nilai_in_endoscopy,0).'</td>';     
//              Instalasi Gizi
              $nilai_in_gizi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_GIZI,Params::INSTALASI_ID_GIZI);
              echo '<td>'.number_format($nilai_in_gizi,0).'</td>';     
//              Instalasi Farmasi
              $nilai_in_farmasi = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_FARMASI);
              echo '<td>'.number_format($nilai_in_farmasi,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Radiologi
              $nilai_rad = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAD,Params::INSTALASI_ID_RAD);
              echo '<td>'.number_format($nilai_rad,0).'</td>';     
//              Laboratorium
              $nilai_lab = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAB,Params::INSTALASI_ID_LAB);
              echo '<td>'.number_format($nilai_lab,0).'</td>';     
/////////////////////////////////////////////////////////////
//              UPRS
              $nilai_uprs = $modelLaporan->getSumRuangan($value['nmjenis'],'','');
              echo '<td>'.number_format($nilai_uprs,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Sanitasi
              $nilai_sanitasi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_sanitasi,0).'</td>';     
//              Laundry
              $nilai_laundry = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAUNDRY,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_laundry,0).'</td>';     
//              Pulasara Jenasah
              $nilai_jenazah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_JENAZAH,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_jenazah,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Instalasi Keamanan dan Tranportasi
              $nilai_keamanan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEAMANAN_TRANSPORTASI,Params::INSTALASI_ID_KEAMANAN_TRANSPORTASI);
              echo '<td>'.number_format($nilai_keamanan,0).'</td>';     
            echo '</tr>';
          }else{
            $nmkelompok[$key] = $value['nmkelompok'];
            if($nmkelompok1[$key-1]!=$nmkelompok[$key])
            {
              $rincian2 = $value['nmkelompok'];
              $tab = $spasi;
              echo '<tr>';
                echo '<td>'.$tab.''.$rincian2.'</td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td>'
                      . '<td></td><td></td><td></td><td></td>';
              echo '</tr>';
              $nmjenis1[$key] = $value['nmjenis'];
              $tab2 = $tab.$spasi;
              echo '<tr>';
                echo '<td>'.$tab2.''.$nmjenis1[$key].'</td>';
                echo '<td>'.number_format($value['nominal'],0).'</td>';
                //              Direktur PT Karsa Abdi Husada dan Direktur RS Jasa Kartini
              $nilai_direktur = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_DIREKTUR,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_direktur,0).'</td>';
/////////////////////////////////////////////////////////////
//              Satuan Pengawas Internal (SPI)
              $nilai_spi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_spi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Komite Medik
              $nilai_k_medik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_medik,0).'</td>';
//              Komite Keperawatan
              $nilai_k_keperawatan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_keperawatan,0).'</td>';
//              Komite Etik dan Hukum
              $nilai_k_etik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_etik,0).'</td>';
//              Panitia Mutu
              $nilai_p_mutu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_mutu,0).'</td>';
//              Panitia KPRS
              $nilai_p_kprs = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_kprs,0).'</td>';
//              Panitia K3
              $nilai_p_k3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_k3,0).'</td>';
//              Staf Medik Fungsional (SMF)
              $nilai_smf = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_smf,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bagian Sekretariat
              $nilai_bg_sekre = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SEKRETARIS,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_sekre,0).'</td>';
//              Sub Bagian Tata Usaha
              $nilai_sb_tatausaha = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_tatausaha,0).'</td>';
//              Sub Bagian Kepegawaian
              $nilai_sb_kepegawaian = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEPEGAWAIAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kepegawaian,0).'</td>';
//              Sub Bagian Rumah Tangga dan Perlengkapan
              $nilai_sb_rumahtangga = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_rumahtangga,0).'</td>';
//              Sub Bagian Diklat
              $nilai_sb_diklat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_diklat,0).'</td>';
//              Sub Bagian Humas dan pemasaran
              $nilai_sb_humas = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_humas,0).'</td>';
//              Bagian Perencanaan
              $nilai_bg_perencanaan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_perencanaan,0).'</td>';
//              Sub Bagian Pengendalian P3
              $nilai_sb_kendalip3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kendalip3,0).'</td>';
//              Sub Bagian Penyusunan Program dan Laporan
              $nilai_sb_program = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_program,0).'</td>';
//              Sub Bagian Hukum
              $nilai_sb_hukum = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_hukum,0).'</td>';
//              Sub Bagian Rekam Medik
              $nilai_sb_rm = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PENDAFTARAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_sb_rm,0).'</td>';
//              Bagian Keuangan
              $nilai_bg_keuangan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEUANGAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_bg_keuangan,0).'</td>';
//              Sub Bagian Penyusunan Anggaran
              $nilai_sb_anggaran = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_anggaran,0).'</td>';
//              Sub Bagian Pembendaharaan
              $nilai_sb_bendahara = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_BENDAHARA,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_bendahara,0).'</td>';
//              Sub Bagian Akuntansi dan Mobilisasi Dana
              $nilai_sb_akuntansi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_AKUNTANSI,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_akuntansi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bidang Pelayanan
              $nilai_bd_pelayanan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_pelayanan,0).'</td>';
//              Seksi Pelayanan Medis
              $nilai_sk_pel_medis = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_pel_medis,0).'</td>';
//              Seksi Rujukan
              $nilai_sk_rujukan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_rujukan,0).'</td>';
//              Bidang Penunjang Pelayanan
              $nilai_bd_penunjang_pel = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_penunjang_pel,0).'</td>';
//              Seksi Penunjang Medis
              $nilai_sk_penunjang_med = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_penunjang_med,0).'</td>';
//              Bidang Keperawatan
              $nilai_bd_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_perawat,0).'</td>';
//              Seksi Asuhan
              $nilai_sk_asuhan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_asuhan,0).'</td>';
//              Seksi SDM Keperawatan
              $nilai_sk_sdm_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_sdm_perawat,0).'</td>';
//              Seksi Logistik
              $nilai_sk_logistik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_logistik,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Pelayanan Khusus
              $nilai_in_pel_khusus = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PELAYANAN_KHUSUS,Params::INSTALASI_ID_PEL_KHUSUS);
              echo '<td>'.number_format($nilai_in_pel_khusus,0).'</td>';
/////////////////////////////////////////////////////////////
//              Ruangan Observasi
              $nilai_r_observasi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSERVASI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_observasi,0).'</td>';
//              Ruangan Kelas IA
              $nilai_r_1a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1a,0).'</td>';
//              Ruangan Kelas IB
              $nilai_r_1b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1b,0).'</td>';
//              Ruangan Kelas I C
              $nilai_r_1c = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1c,0).'</td>';
//              Ruangan Kelas Madya
              $nilai_r_madya = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_madya,0).'</td>';
//              Ruangan Kelas II
              $nilai_r_2 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_2A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_2,0).'</td>';
//              Ruangan Kelas III
              $nilai_r_3 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_3A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_3,0).'</td>';
//              Ruangan Kelas Utama
              $nilai_r_utama = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_utama,0).'</td>';
//              Ruangan Kelas VIP
              $nilai_r_vip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip,0).'</td>';
//              Ruangan Kelas VIP B
              $nilai_r_vip_b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip_b,0).'</td>';
//              Ruangan Kelas VVIP
              $nilai_r_vvip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vvip,0).'</td>';
//              Ruangan Partus A
              $nilai_r_partus_a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_partus_a,0).'</td>';
//              Ruangan Perinatologi A
              $nilai_r_perinatologi_a = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PERINATOLOGI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_perinatologi_a,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Gawat Darurat
              $nilai_igd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAWAT_DARURAT,Params::INSTALASI_ID_RD);
              echo '<td>'.number_format($nilai_igd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Bedah Sentral
              $nilai_ibd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_IBS,Params::INSTALASI_ID_IBS);
              echo '<td>'.number_format($nilai_ibd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Poly Umum
              $nilai_poli_umum = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UMUM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_umum,0).'</td>';
//              Poly Anak
              $nilai_poli_anak = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_ANAK,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_anak,0).'</td>';
//              Poly Bedah
              $nilai_poli_bedah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedah,0).'</td>';
//              Poly Bedah Syaraf
              $nilai_poli_bedahsyaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedahsyaraf,0).'</td>';
//              Poly Gigi
              $nilai_poli_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_gigi,0).'</td>';
//              Poly Konservasi Gigi
              $nilai_poli_konv_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_KONSERVASI_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_konv_gigi,0).'</td>';
//              Poly Obgyn
              $nilai_poli_obgyn = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSGYN,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_obgyn,0).'</td>';
//              Poly Dalam
              $nilai_poli_dalam = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_DALAM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_dalam,0).'</td>';
//              Poly Syaraf
              $nilai_poli_syaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_syaraf,0).'</td>';
//              Poly THT
              $nilai_poli_tht = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_THT,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_tht,0).'</td>';
//              Poly Urologi
              $nilai_poli_urologi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UROLOGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_urologi,0).'</td>';
//              Poly Kemoterafi 
              $nilai_poli_kemoterafi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_kemoterafi,0).'</td>';
/////////////////////////////////////////////////////////////
//              ICU
              $nilai_icu = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ICU,Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_icu,0).'</td>';
//              PICU
              $nilai_picu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_picu,0).'</td>';
//              NICU
              $nilai_nicu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_nicu,0).'</td>';
//              PIU
              $nilai_piu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_piu,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Sterilisasi Sentral
              $nilai_in_steril = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_steril,0).'</td>';     
//              Instalasi Hemodialisa
              $nilai_in_hemodialisa = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_HEMODIALISA,Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_hemodialisa,0).'</td>';     
//              Instalasi ESWL
              $nilai_in_eswl = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ESWL,'bebas');
              echo '<td>'.number_format($nilai_in_eswl,0).'</td>';     
//              Instalasi Endoscopy
              $nilai_in_endoscopy = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ENDOSCOPY,'bebas');
              echo '<td>'.number_format($nilai_in_endoscopy,0).'</td>';     
//              Instalasi Gizi
              $nilai_in_gizi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_GIZI,Params::INSTALASI_ID_GIZI);
              echo '<td>'.number_format($nilai_in_gizi,0).'</td>';     
//              Instalasi Farmasi
              $nilai_in_farmasi = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_FARMASI);
              echo '<td>'.number_format($nilai_in_farmasi,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Radiologi
              $nilai_rad = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAD,Params::INSTALASI_ID_RAD);
              echo '<td>'.number_format($nilai_rad,0).'</td>';     
//              Laboratorium
              $nilai_lab = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAB,Params::INSTALASI_ID_LAB);
              echo '<td>'.number_format($nilai_lab,0).'</td>';     
/////////////////////////////////////////////////////////////
//              UPRS
              $nilai_uprs = $modelLaporan->getSumRuangan($value['nmjenis'],'','');
              echo '<td>'.number_format($nilai_uprs,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Sanitasi
              $nilai_sanitasi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_sanitasi,0).'</td>';     
//              Laundry
              $nilai_laundry = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAUNDRY,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_laundry,0).'</td>';     
//              Pulasara Jenasah
              $nilai_jenazah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_JENAZAH,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_jenazah,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Instalasi Keamanan dan Tranportasi
              $nilai_keamanan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEAMANAN_TRANSPORTASI,Params::INSTALASI_ID_KEAMANAN_TRANSPORTASI);
              echo '<td>'.number_format($nilai_keamanan,0).'</td>';
              echo '</tr>';
            }else{
              $nmjenis[$key] = $value['nmjenis'];
              if($nmjenis1[$key-1]!=$nmjenis[$key])
              {
                $rincian3 = $value['nmjenis'];
                $tab2 = $tab1.$spasi;
                echo '<tr>';
                  echo '<td>'.$tab2.''.$rincian3.'</td>';
                  echo '<td>'.number_format($modTotalNominal->getTotalNominal($value['kdstruktur'],$value['kdkelompok'],$value['kdjenis'])->nominal).'</td>';
                  //              Direktur PT Karsa Abdi Husada dan Direktur RS Jasa Kartini
              $nilai_direktur = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_DIREKTUR,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_direktur,0).'</td>';
/////////////////////////////////////////////////////////////
//              Satuan Pengawas Internal (SPI)
              $nilai_spi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_spi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Komite Medik
              $nilai_k_medik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_medik,0).'</td>';
//              Komite Keperawatan
              $nilai_k_keperawatan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_keperawatan,0).'</td>';
//              Komite Etik dan Hukum
              $nilai_k_etik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_k_etik,0).'</td>';
//              Panitia Mutu
              $nilai_p_mutu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_mutu,0).'</td>';
//              Panitia KPRS
              $nilai_p_kprs = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_kprs,0).'</td>';
//              Panitia K3
              $nilai_p_k3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_p_k3,0).'</td>';
//              Staf Medik Fungsional (SMF)
              $nilai_smf = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_smf,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bagian Sekretariat
              $nilai_bg_sekre = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SEKRETARIS,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_sekre,0).'</td>';
//              Sub Bagian Tata Usaha
              $nilai_sb_tatausaha = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_tatausaha,0).'</td>';
//              Sub Bagian Kepegawaian
              $nilai_sb_kepegawaian = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEPEGAWAIAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kepegawaian,0).'</td>';
//              Sub Bagian Rumah Tangga dan Perlengkapan
              $nilai_sb_rumahtangga = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_rumahtangga,0).'</td>';
//              Sub Bagian Diklat
              $nilai_sb_diklat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_diklat,0).'</td>';
//              Sub Bagian Humas dan pemasaran
              $nilai_sb_humas = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_humas,0).'</td>';
//              Bagian Perencanaan
              $nilai_bg_perencanaan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bg_perencanaan,0).'</td>';
//              Sub Bagian Pengendalian P3
              $nilai_sb_kendalip3 = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_kendalip3,0).'</td>';
//              Sub Bagian Penyusunan Program dan Laporan
              $nilai_sb_program = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_program,0).'</td>';
//              Sub Bagian Hukum
              $nilai_sb_hukum = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_hukum,0).'</td>';
//              Sub Bagian Rekam Medik
              $nilai_sb_rm = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PENDAFTARAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_sb_rm,0).'</td>';
//              Bagian Keuangan
              $nilai_bg_keuangan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEUANGAN,Params::INSTALASI_ID_RM);
              echo '<td>'.number_format($nilai_bg_keuangan,0).'</td>';
//              Sub Bagian Penyusunan Anggaran
              $nilai_sb_anggaran = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_SUSUN_ANGGARAN,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_anggaran,0).'</td>';
//              Sub Bagian Pembendaharaan
              $nilai_sb_bendahara = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_BENDAHARA,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_bendahara,0).'</td>';
//              Sub Bagian Akuntansi dan Mobilisasi Dana
              $nilai_sb_akuntansi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_SUBBAG_AKUNTANSI,Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sb_akuntansi,0).'</td>';
/////////////////////////////////////////////////////////////
//              Bidang Pelayanan
              $nilai_bd_pelayanan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_pelayanan,0).'</td>';
//              Seksi Pelayanan Medis
              $nilai_sk_pel_medis = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_pel_medis,0).'</td>';
//              Seksi Rujukan
              $nilai_sk_rujukan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_rujukan,0).'</td>';
//              Bidang Penunjang Pelayanan
              $nilai_bd_penunjang_pel = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_penunjang_pel,0).'</td>';
//              Seksi Penunjang Medis
              $nilai_sk_penunjang_med = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_penunjang_med,0).'</td>';
//              Bidang Keperawatan
              $nilai_bd_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_bd_perawat,0).'</td>';
//              Seksi Asuhan
              $nilai_sk_asuhan = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_asuhan,0).'</td>';
//              Seksi SDM Keperawatan
              $nilai_sk_sdm_perawat = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_sdm_perawat,0).'</td>';
//              Seksi Logistik
              $nilai_sk_logistik = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_NON_INSTALASI);
              echo '<td>'.number_format($nilai_sk_logistik,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Pelayanan Khusus
              $nilai_in_pel_khusus = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PELAYANAN_KHUSUS,Params::INSTALASI_ID_PEL_KHUSUS);
              echo '<td>'.number_format($nilai_in_pel_khusus,0).'</td>';
/////////////////////////////////////////////////////////////
//              Ruangan Observasi
              $nilai_r_observasi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSERVASI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_observasi,0).'</td>';
//              Ruangan Kelas IA
              $nilai_r_1a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1a,0).'</td>';
//              Ruangan Kelas IB
              $nilai_r_1b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1b,0).'</td>';
//              Ruangan Kelas I C
              $nilai_r_1c = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_1c,0).'</td>';
//              Ruangan Kelas Madya
              $nilai_r_madya = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_madya,0).'</td>';
//              Ruangan Kelas II
              $nilai_r_2 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_2A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_2,0).'</td>';
//              Ruangan Kelas III
              $nilai_r_3 = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_3A,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_3,0).'</td>';
//              Ruangan Kelas Utama
              $nilai_r_utama = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_utama,0).'</td>';
//              Ruangan Kelas VIP
              $nilai_r_vip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip,0).'</td>';
//              Ruangan Kelas VIP B
              $nilai_r_vip_b = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vip_b,0).'</td>';
//              Ruangan Kelas VVIP
              $nilai_r_vvip = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_vvip,0).'</td>';
//              Ruangan Partus A
              $nilai_r_partus_a = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_partus_a,0).'</td>';
//              Ruangan Perinatologi A
              $nilai_r_perinatologi_a = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_PERINATOLOGI,Params::INSTALASI_ID_RI);
              echo '<td>'.number_format($nilai_r_perinatologi_a,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Gawat Darurat
              $nilai_igd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAWAT_DARURAT,Params::INSTALASI_ID_RD);
              echo '<td>'.number_format($nilai_igd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Bedah Sentral
              $nilai_ibd = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_IBS,Params::INSTALASI_ID_IBS);
              echo '<td>'.number_format($nilai_ibd,0).'</td>';
/////////////////////////////////////////////////////////////
//              Poly Umum
              $nilai_poli_umum = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UMUM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_umum,0).'</td>';
//              Poly Anak
              $nilai_poli_anak = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_ANAK,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_anak,0).'</td>';
//              Poly Bedah
              $nilai_poli_bedah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedah,0).'</td>';
//              Poly Bedah Syaraf
              $nilai_poli_bedahsyaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_BEDAH_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_bedahsyaraf,0).'</td>';
//              Poly Gigi
              $nilai_poli_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_gigi,0).'</td>';
//              Poly Konservasi Gigi
              $nilai_poli_konv_gigi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_KONSERVASI_GIGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_konv_gigi,0).'</td>';
//              Poly Obgyn
              $nilai_poli_obgyn = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_OBSGYN,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_obgyn,0).'</td>';
//              Poly Dalam
              $nilai_poli_dalam = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_DALAM,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_dalam,0).'</td>';
//              Poly Syaraf
              $nilai_poli_syaraf = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_PENYAKIT_SYARAF,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_syaraf,0).'</td>';
//              Poly THT
              $nilai_poli_tht = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_THT,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_tht,0).'</td>';
//              Poly Urologi
              $nilai_poli_urologi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_POLY_UROLOGI,Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_urologi,0).'</td>';
//              Poly Kemoterafi 
              $nilai_poli_kemoterafi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RJ);
              echo '<td>'.number_format($nilai_poli_kemoterafi,0).'</td>';
/////////////////////////////////////////////////////////////
//              ICU
              $nilai_icu = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ICU,Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_icu,0).'</td>';
//              PICU
              $nilai_picu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_picu,0).'</td>';
//              NICU
              $nilai_nicu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_nicu,0).'</td>';
//              PIU
              $nilai_piu = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_RAWAT_INTENSIF);
              echo '<td>'.number_format($nilai_piu,0).'</td>';
/////////////////////////////////////////////////////////////
//              Instalasi Sterilisasi Sentral
              $nilai_in_steril = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_steril,0).'</td>';     
//              Instalasi Hemodialisa
              $nilai_in_hemodialisa = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_HEMODIALISA,Params::INSTALASI_ID_HEMODIALISA);
              echo '<td>'.number_format($nilai_in_hemodialisa,0).'</td>';     
//              Instalasi ESWL
              $nilai_in_eswl = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ESWL,'bebas');
              echo '<td>'.number_format($nilai_in_eswl,0).'</td>';     
//              Instalasi Endoscopy
              $nilai_in_endoscopy = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_ENDOSCOPY,'bebas');
              echo '<td>'.number_format($nilai_in_endoscopy,0).'</td>';     
//              Instalasi Gizi
              $nilai_in_gizi = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_GIZI,Params::INSTALASI_ID_GIZI);
              echo '<td>'.number_format($nilai_in_gizi,0).'</td>';     
//              Instalasi Farmasi
              $nilai_in_farmasi = $modelLaporan->getSumRuangan($value['nmjenis'],'bebas',Params::INSTALASI_ID_FARMASI);
              echo '<td>'.number_format($nilai_in_farmasi,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Radiologi
              $nilai_rad = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_RAD,Params::INSTALASI_ID_RAD);
              echo '<td>'.number_format($nilai_rad,0).'</td>';     
//              Laboratorium
              $nilai_lab = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAB,Params::INSTALASI_ID_LAB);
              echo '<td>'.number_format($nilai_lab,0).'</td>';     
/////////////////////////////////////////////////////////////
//              UPRS
              $nilai_uprs = $modelLaporan->getSumRuangan($value['nmjenis'],'','');
              echo '<td>'.number_format($nilai_uprs,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Sanitasi
              $nilai_sanitasi = $modelLaporan->getSumRuangan($value['nmjenis'],'',Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_sanitasi,0).'</td>';     
//              Laundry
              $nilai_laundry = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_LAUNDRY,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_laundry,0).'</td>';     
//              Pulasara Jenasah
              $nilai_jenazah = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_JENAZAH,Params::INSTALASI_ID_SANITASI);
              echo '<td>'.number_format($nilai_jenazah,0).'</td>';     
/////////////////////////////////////////////////////////////
//              Instalasi Keamanan dan Tranportasi
              $nilai_keamanan = $modelLaporan->getSumRuangan($value['nmjenis'],Params::RUANGAN_ID_KEAMANAN_TRANSPORTASI,Params::INSTALASI_ID_KEAMANAN_TRANSPORTASI);
              echo '<td>'.number_format($nilai_keamanan,0).'</td>';
                echo '</tr>';
              }else{
              }
            }
          } 
          $i++;
          $total_nilai += $value['nominal'];
            $total_direktur += $nilai_direktur;
            $total_spi += $nilai_spi;
            $total_k_medik += $nilai_k_medik;
            $total_k_keperawatan += $nilai_k_keperawatan;
            $total_k_etik += $nilai_k_etik;
            $total_p_mutu += $nilai_p_mutu;
            $total_p_kprs += $nilai_p_kprs;
            $total_p_k3 += $nilai_p_k3;
            $total_smf += $nilai_smf;
            $total_bg_sekre += $nilai_bg_sekre;
            $total_sb_tatausaha += $nilai_sb_tatausaha;
            $total_sb_kepegawaian += $nilai_sb_kepegawaian;
            $total_sb_rumahtangga += $nilai_sb_rumahtangga;
            $total_sb_diklat += $nilai_sb_diklat;
            $total_sb_humas += $nilai_sb_humas;
            $total_bg_perencanaan += $nilai_bg_perencanaan;
            $total_sb_kendalip3 += $nilai_sb_kendalip3;
            $total_sb_program += $nilai_sb_program;
            $total_sb_hukum += $nilai_sb_hukum;
            $total_sb_rm += $nilai_sb_rm;
            $total_bg_keuangan += $nilai_bg_keuangan;
            $total_sb_anggaran += $nilai_sb_anggaran;
            $total_sb_bendahara += $nilai_sb_bendahara;
            $total_sb_akuntansi += $nilai_sb_akuntansi;
            $total_bd_pelayanan += $nilai_bd_pelayanan;
            $total_sk_pel_medis += $nilai_sk_pel_medis;
            $total_sk_rujukan += $nilai_sk_rujukan;
            $total_bd_penunjang_pel += $nilai_bd_penunjang_pel;
            $total_sk_penunjang_med += $nilai_sk_penunjang_med;
            $total_bd_perawat += $nilai_bd_perawat;
            $total_sk_asuhan += $nilai_sk_asuhan;
            $total_sk_sdm_perawat += $nilai_sk_sdm_perawat;
            $total_sk_logistik += $nilai_sk_logistik;
            $total_in_pel_khusus += $nilai_in_pel_khusus;
            $total_r_observasi += $nilai_r_observasi;
            $total_r_1a += $nilai_r_1a;
            $total_r_1b += $nilai_r_1b;
            $total_r_1c += $nilai_r_1c;
            $total_r_madya += $nilai_r_madya;
            $total_r_2 += $nilai_r_2;
            $total_r_3 += $nilai_r_3;
            $total_r_utama += $nilai_r_utama;
            $total_r_vip += $nilai_r_vip;
            $total_r_vip_b += $nilai_r_vip_b;
            $total_r_vvip += $nilai_r_vvip;
            $total_r_partus_a += $nilai_r_partus_a;
            $total_r_perinatologi_a += $nilai_r_perinatologi_a;
            $total_igd += $nilai_igd;
            $total_ibd += $nilai_ibd;
            $total_poli_umum += $nilai_poli_umum;
            $total_poli_anak += $nilai_poli_anak;
            $total_poli_bedah += $nilai_poli_bedah;
            $total_poli_bedahsyaraf += $nilai_poli_bedahsyaraf;
            $total_poli_gigi += $nilai_poli_gigi;
            $total_poli_konv_gigi += $nilai_poli_konv_gigi;
            $total_poli_obgyn += $nilai_poli_obgyn;
            $total_poli_dalam += $nilai_poli_dalam;
            $total_poli_syaraf += $nilai_poli_syaraf;
            $total_poli_tht += $nilai_poli_tht;
            $total_poli_urologi += $nilai_poli_urologi;
            $total_poli_kemoterafi += $nilai_poli_kemoterafi;
            $total_icu += $nilai_icu;
            $total_picu += $nilai_picu;
            $total_nicu += $nilai_nicu;
            $total_piu += $nilai_piu;
            $total_in_steril += $nilai_in_steril;
            $total_in_hemodialisa += $nilai_in_hemodialisa;
            $total_in_eswl += $nilai_in_eswl;
            $total_in_endoscopy += $nilai_in_endoscopy;
            $total_in_gizi += $nilai_in_gizi;
            $total_in_farmasi += $nilai_in_farmasi;
            $total_rad += $nilai_rad;
            $total_lab += $nilai_lab;
            $total_uprs += $nilai_uprs;
            $total_sanitasi += $nilai_sanitasi;
            $total_laundry += $nilai_laundry;
            $total_jenazah += $nilai_jenazah;
            $total_keamanan += $nilai_keamanan;
          if(in_array($i, $urutan))
          {
            echo '<tr>';      
              echo '<td>'.$tab2.'<i><b>Total '.$value['nmkelompok'].'</b><i></td>';
              echo '<td><i><b>'.number_format($total_nilai,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_direktur,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_spi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_k_medik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_k_keperawatan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_k_etik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_p_mutu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_p_kprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_p_k3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_smf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bg_sekre,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_tatausaha,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_kepegawaian,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_rumahtangga,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_diklat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_humas,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bg_perencanaan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_kendalip3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_program,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_hukum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_rm,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bg_keuangan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_anggaran,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_bendahara,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sb_akuntansi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bd_pelayanan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_pel_medis,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_rujukan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bd_penunjang_pel,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_penunjang_med,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_bd_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_asuhan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_sdm_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sk_logistik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_pel_khusus,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_observasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_1a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_1b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_1c,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_madya,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_2,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_utama,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_vip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_vip_b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_vvip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_partus_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_r_perinatologi_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_igd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_ibd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_umum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_anak,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_bedah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_bedahsyaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_konv_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_obgyn,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_dalam,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_syaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_tht,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_urologi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_poli_kemoterafi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_icu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_picu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_nicu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_piu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_steril,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_hemodialisa,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_eswl,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_endoscopy,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_gizi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_in_farmasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_rad,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_lab,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_uprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_sanitasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_laundry,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_jenazah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_keamanan,0).'</b><i></td>';
            echo '</tr>';
            $total_nilai = 0;
            $total_spi = 0;
            $total_k_medik = 0;
            $total_k_keperawatan = 0;
            $total_k_etik = 0;
            $total_p_mutu = 0;
            $total_p_kprs = 0;
            $total_p_k3 = 0;
            $total_smf = 0;
            $total_bg_sekre = 0;
            $total_sb_tatausaha = 0;
            $total_sb_kepegawaian = 0;
            $total_sb_rumahtangga = 0;
            $total_sb_diklat = 0;
            $total_sb_humas = 0;
            $total_bg_perencanaan = 0;
            $total_sb_kendalip3 = 0;
            $total_sb_program = 0;
            $total_sb_hukum = 0;
            $total_sb_rm = 0;
            $total_bg_keuangan = 0;
            $total_sb_anggaran = 0;
            $total_sb_bendahara = 0;
            $total_sb_akuntansi = 0;
            $total_bd_pelayanan = 0;
            $total_sk_pel_medis = 0;
            $total_sk_rujukan = 0;
            $total_bd_penunjang_pel = 0;
            $total_sk_penunjang_med = 0;
            $total_bd_perawat = 0;
            $total_sk_asuhan = 0;
            $total_sk_sdm_perawat = 0;
            $total_sk_logistik = 0;
            $total_in_pel_khusus = 0;
            $total_r_observasi = 0;
            $total_r_1a = 0;
            $total_r_1b = 0;
            $total_r_1c = 0;
            $total_r_madya = 0;
            $total_r_2 = 0;
            $total_r_3 = 0;
            $total_r_utama = 0;
            $total_r_vip = 0;
            $total_r_vip_b = 0;
            $total_r_vvip = 0;
            $total_r_partus_a = 0;
            $total_r_perinatologi_a = 0;
            $total_igd = 0;
            $total_ibd = 0;
            $total_poli_umum = 0;
            $total_poli_anak = 0;
            $total_poli_bedah = 0;
            $total_poli_bedahsyaraf = 0;
            $total_poli_gigi = 0;
            $total_poli_konv_gigi = 0;
            $total_poli_obgyn = 0;
            $total_poli_dalam = 0;
            $total_poli_syaraf = 0;
            $total_poli_tht = 0;
            $total_poli_urologi = 0;
            $total_poli_kemoterafi = 0;
            $total_icu = 0;
            $total_picu = 0;
            $total_nicu = 0;
            $total_piu = 0;
            $total_in_steril= 0;
            $total_in_hemodialisa= 0;
            $total_in_eswl= 0;
            $total_in_endoscopy= 0;
            $total_in_gizi= 0;
            $total_in_farmasi= 0;
            $total_rad= 0;
            $total_lab= 0;
            $total_uprs= 0;
            $total_sanitasi= 0;
            $total_laundry= 0;
            $total_jenazah= 0;
            $total_keamanan= 0;
          }  

          $total_struktur += $value['nominal'];
          $total_struktur_direktur += $nilai_direktur;
            $total_struktur_spi += $nilai_spi;
            $total_struktur_k_medik += $nilai_k_medik;
            $total_struktur_k_keperawatan += $nilai_k_keperawatan;
            $total_struktur_k_etik += $nilai_k_etik;
            $total_struktur_p_mutu += $nilai_p_mutu;
            $total_struktur_p_kprs += $nilai_p_kprs;
            $total_struktur_p_k3 += $nilai_p_k3;
            $total_struktur_smf += $nilai_smf;
            $total_struktur_bg_sekre += $nilai_bg_sekre;
            $total_struktur_sb_tatausaha += $nilai_sb_tatausaha;
            $total_struktur_sb_kepegawaian += $nilai_sb_kepegawaian;
            $total_struktur_sb_rumahtangga += $nilai_sb_rumahtangga;
            $total_struktur_sb_diklat += $nilai_sb_diklat;
            $total_struktur_sb_humas += $nilai_sb_humas;
            $total_struktur_bg_perencanaan += $nilai_bg_perencanaan;
            $total_struktur_sb_kendalip3 += $nilai_sb_kendalip3;
            $total_struktur_sb_program += $nilai_sb_program;
            $total_struktur_sb_hukum += $nilai_sb_hukum;
            $total_struktur_sb_rm += $nilai_sb_rm;
            $total_struktur_bg_keuangan += $nilai_bg_keuangan;
            $total_struktur_sb_anggaran += $nilai_sb_anggaran;
            $total_struktur_sb_bendahara += $nilai_sb_bendahara;
            $total_struktur_sb_akuntansi += $nilai_sb_akuntansi;
            $total_struktur_bd_pelayanan += $nilai_bd_pelayanan;
            $total_struktur_sk_pel_medis += $nilai_sk_pel_medis;
            $total_struktur_sk_rujukan += $nilai_sk_rujukan;
            $total_struktur_bd_penunjang_pel += $nilai_bd_penunjang_pel;
            $total_struktur_sk_penunjang_med += $nilai_sk_penunjang_med;
            $total_struktur_bd_perawat += $nilai_bd_perawat;
            $total_struktur_sk_asuhan += $nilai_sk_asuhan;
            $total_struktur_sk_sdm_perawat += $nilai_sk_sdm_perawat;
            $total_struktur_sk_logistik += $nilai_sk_logistik;
            $total_struktur_in_pel_khusus += $nilai_in_pel_khusus;
            $total_struktur_r_observasi += $nilai_r_observasi;
            $total_struktur_r_1a += $nilai_r_1a;
            $total_struktur_r_1b += $nilai_r_1b;
            $total_struktur_r_1c += $nilai_r_1c;
            $total_struktur_r_madya += $nilai_r_madya;
            $total_struktur_r_2 += $nilai_r_2;
            $total_struktur_r_3 += $nilai_r_3;
            $total_struktur_r_utama += $nilai_r_utama;
            $total_struktur_r_vip += $nilai_r_vip;
            $total_struktur_r_vip_b += $nilai_r_vip_b;
            $total_struktur_r_vvip += $nilai_r_vvip;
            $total_struktur_r_partus_a += $nilai_r_partus_a;
            $total_struktur_r_perinatologi_a += $nilai_r_perinatologi_a;
            $total_struktur_igd += $nilai_igd;
            $total_struktur_ibd += $nilai_ibd;
            $total_struktur_poli_umum += $nilai_poli_umum;
            $total_struktur_poli_anak += $nilai_poli_anak;
            $total_struktur_poli_bedah += $nilai_poli_bedah;
            $total_struktur_poli_bedahsyaraf += $nilai_poli_bedahsyaraf;
            $total_struktur_poli_gigi += $nilai_poli_gigi;
            $total_struktur_poli_konv_gigi += $nilai_poli_konv_gigi;
            $total_struktur_poli_obgyn += $nilai_poli_obgyn;
            $total_struktur_poli_dalam += $nilai_poli_dalam;
            $total_struktur_poli_syaraf += $nilai_poli_syaraf;
            $total_struktur_poli_tht += $nilai_poli_tht;
            $total_struktur_poli_urologi += $nilai_poli_urologi;
            $total_struktur_poli_kemoterafi += $nilai_poli_kemoterafi;
            $total_struktur_icu += $nilai_icu;
            $total_struktur_picu += $nilai_picu;
            $total_struktur_nicu += $nilai_nicu;
            $total_struktur_piu += $nilai_piu;
            $total_struktur_in_steril += $nilai_in_steril;
            $total_struktur_in_hemodialisa += $nilai_in_hemodialisa;
            $total_struktur_in_eswl += $nilai_in_eswl;
            $total_struktur_in_endoscopy += $nilai_in_endoscopy;
            $total_struktur_in_gizi += $nilai_in_gizi;
            $total_struktur_in_farmasi += $nilai_in_farmasi;
            $total_struktur_rad += $nilai_rad;
            $total_struktur_lab += $nilai_lab;
            $total_struktur_uprs += $nilai_uprs;
            $total_struktur_sanitasi += $nilai_sanitasi;
            $total_struktur_laundry += $nilai_laundry;
            $total_struktur_jenazah += $nilai_jenazah;
            $total_struktur_keamanan += $nilai_keamanan;
          if(in_array($i, $struktur))
          {
            echo '<tr>';      
              echo '<td><i><b>Total '.$value['nmstruktur'].'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_direktur,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_spi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_k_medik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_k_keperawatan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_k_etik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_p_mutu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_p_kprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_p_k3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_smf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bg_sekre,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_tatausaha,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_kepegawaian,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_rumahtangga,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_diklat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_humas,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bg_perencanaan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_kendalip3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_program,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_hukum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_rm,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bg_keuangan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_anggaran,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_bendahara,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sb_akuntansi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bd_pelayanan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_pel_medis,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_rujukan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bd_penunjang_pel,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_penunjang_med,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_bd_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_asuhan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_sdm_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sk_logistik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_pel_khusus,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_observasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_1a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_1b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_1c,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_madya,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_2,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_utama,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_vip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_vip_b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_vvip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_partus_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_r_perinatologi_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_igd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_ibd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_umum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_anak,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_bedah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_bedahsyaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_konv_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_obgyn,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_dalam,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_syaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_tht,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_urologi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_poli_kemoterafi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_icu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_picu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_nicu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_piu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_steril,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_hemodialisa,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_eswl,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_endoscopy,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_gizi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_in_farmasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_rad,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_lab,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_uprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_sanitasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_laundry,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_jenazah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_struktur_keamanan,0).'</b><i></td>';
            echo '</tr>';
            if($value['nmstruktur']=='Kewajiban' || $value['nmstruktur']=='Pendapatan Operasional' || $value['nmstruktur']=='Beban operasional'){
              $total += $total_struktur;
              $total_semua_direktur += $nilai_direktur;
                $total_semua_spi += $nilai_spi;
                $total_semua_k_medik += $nilai_k_medik;
                $total_semua_k_keperawatan += $nilai_k_keperawatan;
                $total_semua_k_etik += $nilai_k_etik;
                $total_semua_p_mutu += $nilai_p_mutu;
                $total_semua_p_kprs += $nilai_p_kprs;
                $total_semua_p_k3 += $nilai_p_k3;
                $total_semua_smf += $nilai_smf;
                $total_semua_bg_sekre += $nilai_bg_sekre;
                $total_semua_sb_tatausaha += $nilai_sb_tatausaha;
                $total_semua_sb_kepegawaian += $nilai_sb_kepegawaian;
                $total_semua_sb_rumahtangga += $nilai_sb_rumahtangga;
                $total_semua_sb_diklat += $nilai_sb_diklat;
                $total_semua_sb_humas += $nilai_sb_humas;
                $total_semua_bg_perencanaan += $nilai_bg_perencanaan;
                $total_semua_sb_kendalip3 += $nilai_sb_kendalip3;
                $total_semua_sb_program += $nilai_sb_program;
                $total_semua_sb_hukum += $nilai_sb_hukum;
                $total_semua_sb_rm += $nilai_sb_rm;
                $total_semua_bg_keuangan += $nilai_bg_keuangan;
                $total_semua_sb_anggaran += $nilai_sb_anggaran;
                $total_semua_sb_bendahara += $nilai_sb_bendahara;
                $total_semua_sb_akuntansi += $nilai_sb_akuntansi;
                $total_semua_bd_pelayanan += $nilai_bd_pelayanan;
                $total_semua_sk_pel_medis += $nilai_sk_pel_medis;
                $total_semua_sk_rujukan += $nilai_sk_rujukan;
                $total_semua_bd_penunjang_pel += $nilai_bd_penunjang_pel;
                $total_semua_sk_penunjang_med += $nilai_sk_penunjang_med;
                $total_semua_bd_perawat += $nilai_bd_perawat;
                $total_semua_sk_asuhan += $nilai_sk_asuhan;
                $total_semua_sk_sdm_perawat += $nilai_sk_sdm_perawat;
                $total_semua_sk_logistik += $nilai_sk_logistik;
                $total_semua_in_pel_khusus += $nilai_in_pel_khusus;
                $total_semua_r_observasi += $nilai_r_observasi;
                $total_semua_r_1a += $nilai_r_1a;
                $total_semua_r_1b += $nilai_r_1b;
                $total_semua_r_1c += $nilai_r_1c;
                $total_semua_r_madya += $nilai_r_madya;
                $total_semua_r_2 += $nilai_r_2;
                $total_semua_r_3 += $nilai_r_3;
                $total_semua_r_utama += $nilai_r_utama;
                $total_semua_r_vip += $nilai_r_vip;
                $total_semua_r_vip_b += $nilai_r_vip_b;
                $total_semua_r_vvip += $nilai_r_vvip;
                $total_semua_r_partus_a += $nilai_r_partus_a;
                $total_semua_r_perinatologi_a += $nilai_r_perinatologi_a;
                $total_semua_igd += $nilai_igd;
                $total_semua_ibd += $nilai_ibd;
                $total_semua_poli_umum += $nilai_poli_umum;
                $total_semua_poli_anak += $nilai_poli_anak;
                $total_semua_poli_bedah += $nilai_poli_bedah;
                $total_semua_poli_bedahsyaraf += $nilai_poli_bedahsyaraf;
                $total_semua_poli_gigi += $nilai_poli_gigi;
                $total_semua_poli_konv_gigi += $nilai_poli_konv_gigi;
                $total_semua_poli_obgyn += $nilai_poli_obgyn;
                $total_semua_poli_dalam += $nilai_poli_dalam;
                $total_semua_poli_syaraf += $nilai_poli_syaraf;
                $total_semua_poli_tht += $nilai_poli_tht;
                $total_semua_poli_urologi += $nilai_poli_urologi;
                $total_semua_poli_kemoterafi += $nilai_poli_kemoterafi;
                $total_semua_icu += $nilai_icu;
                $total_semua_picu += $nilai_picu;
                $total_semua_nicu += $nilai_nicu;
                $total_semua_piu += $nilai_piu;
                $total_semua_in_steril += $nilai_in_steril;
                $total_semua_in_hemodialisa += $nilai_in_hemodialisa;
                $total_semua_in_eswl += $nilai_in_eswl;
                $total_semua_in_endoscopy += $nilai_in_endoscopy;
                $total_semua_in_gizi += $nilai_in_gizi;
                $total_semua_in_farmasi += $nilai_in_farmasi;
                $total_semua_rad += $nilai_rad;
                $total_semua_lab += $nilai_lab;
                $total_semua_uprs += $nilai_uprs;
                $total_semua_sanitasi += $nilai_sanitasi;
                $total_semua_laundry += $nilai_laundry;
                $total_semua_jenazah += $nilai_jenazah;
                $total_semua_keamanan += $nilai_keamanan;
            }
            $total_struktur = 0;
            $total_struktur_direktur = 0;
            $total_struktur_spi = 0;
            $total_struktur_k_medik = 0;
            $total_struktur_k_keperawatan = 0;
            $total_struktur_k_etik = 0;
            $total_struktur_p_mutu = 0;
            $total_struktur_p_kprs = 0;
            $total_struktur_p_k3 = 0;
            $total_struktur_smf = 0;
            $total_struktur_bg_sekre = 0;
            $total_struktur_sb_tatausaha = 0;
            $total_struktur_sb_kepegawaian = 0;
            $total_struktur_sb_rumahtangga = 0;
            $total_struktur_sb_diklat = 0;
            $total_struktur_sb_humas = 0;
            $total_struktur_bg_perencanaan = 0;
            $total_struktur_sb_kendalip3 = 0;
            $total_struktur_sb_program = 0;
            $total_struktur_sb_hukum = 0;
            $total_struktur_sb_rm = 0;
            $total_struktur_bg_keuangan = 0;
            $total_struktur_sb_anggaran = 0;
            $total_struktur_sb_bendahara = 0;
            $total_struktur_sb_akuntansi = 0;
            $total_struktur_bd_pelayanan = 0;
            $total_struktur_sk_pel_medis = 0;
            $total_struktur_sk_rujukan = 0;
            $total_struktur_bd_penunjang_pel = 0;
            $total_struktur_sk_penunjang_med = 0;
            $total_struktur_bd_perawat = 0;
            $total_struktur_sk_asuhan = 0;
            $total_struktur_sk_sdm_perawat = 0;
            $total_struktur_sk_logistik = 0;
            $total_struktur_in_pel_khusus = 0;
            $total_struktur_r_observasi = 0;
            $total_struktur_r_1a = 0;
            $total_struktur_r_1b = 0;
            $total_struktur_r_1c = 0;
            $total_struktur_r_madya = 0;
            $total_struktur_r_2 = 0;
            $total_struktur_r_3 = 0;
            $total_struktur_r_utama = 0;
            $total_struktur_r_vip = 0;
            $total_struktur_r_vip_b = 0;
            $total_struktur_r_vvip = 0;
            $total_struktur_r_partus_a = 0;
            $total_struktur_r_perinatologi_a = 0;
            $total_struktur_igd = 0;
            $total_struktur_ibd = 0;
            $total_struktur_poli_umum = 0;
            $total_struktur_poli_anak = 0;
            $total_struktur_poli_bedah = 0;
            $total_struktur_poli_bedahsyaraf = 0;
            $total_struktur_poli_gigi = 0;
            $total_struktur_poli_konv_gigi = 0;
            $total_struktur_poli_obgyn = 0;
            $total_struktur_poli_dalam = 0;
            $total_struktur_poli_syaraf = 0;
            $total_struktur_poli_tht = 0;
            $total_struktur_poli_urologi = 0;
            $total_struktur_poli_kemoterafi = 0;
            $total_struktur_icu = 0;
            $total_struktur_picu = 0;
            $total_struktur_nicu = 0;
            $total_struktur_piu = 0;
            $total_struktur_in_steril= 0;
            $total_struktur_in_hemodialisa= 0;
            $total_struktur_in_eswl= 0;
            $total_struktur_in_endoscopy= 0;
            $total_struktur_in_gizi= 0;
            $total_struktur_in_farmasi= 0;
            $total_struktur_rad= 0;
            $total_struktur_lab= 0;
            $total_struktur_uprs= 0;
            $total_struktur_sanitasi= 0;
            $total_struktur_laundry= 0;
            $total_struktur_jenazah= 0;
            $total_struktur_keamanan= 0;
          }

          if($i == count($model)){
            echo '<tr>';      
              echo '<td><i><b>Total Kewajiban & Ekuitas</b><i></td>';
              echo '<td><i><b>'.number_format($total,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_direktur,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_spi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_k_medik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_k_keperawatan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_k_etik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_p_mutu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_p_kprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_p_k3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_smf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bg_sekre,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_tatausaha,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_kepegawaian,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_rumahtangga,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_diklat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_humas,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bg_perencanaan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_kendalip3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_program,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_hukum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_rm,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bg_keuangan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_anggaran,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_bendahara,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sb_akuntansi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bd_pelayanan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_pel_medis,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_rujukan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bd_penunjang_pel,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_penunjang_med,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_bd_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_asuhan,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_sdm_perawat,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sk_logistik,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_pel_khusus,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_observasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_1a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_1b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_1c,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_madya,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_2,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_3,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_utama,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_vip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_vip_b,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_vvip,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_partus_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_r_perinatologi_a,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_igd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_ibd,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_umum,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_anak,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_bedah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_bedahsyaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_konv_gigi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_obgyn,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_dalam,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_syaraf,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_tht,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_urologi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_poli_kemoterafi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_icu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_picu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_nicu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_piu,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_steril,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_hemodialisa,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_eswl,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_endoscopy,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_gizi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_in_farmasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_rad,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_lab,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_uprs,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_sanitasi,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_laundry,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_jenazah,0).'</b><i></td>';
              echo '<td><i><b>'.number_format($total_semua_keamanan,0).'</b><i></td>';
            echo '</tr>';
          }
        }
      ?>
    </tbody>
  </table>
</div>