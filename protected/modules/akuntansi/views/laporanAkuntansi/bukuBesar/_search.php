<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<div class="search-form" style="">
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
    ));
?>
<style>

label.checkbox{
        width:150px;
        display:inline-block;
}
</style>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php 
                    if(isset($_GET['AKLaporanBukuBesarV'])){
                        $modelLaporan->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modelLaporan->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
                    }
                ?>
                <?php echo $form->labelEx($modelLaporan,'tglAwal', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',
                            array(
                                    'model'=>$modelLaporan,
                                    'attribute'=>'tglAwal',
                                    'mode'=>'datetime',
                                    'options'=>array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'dtPicker2-5',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
                            )
                        ); 
                    ?>

                </div>
            </div>
            
            <div class="control-group ">
                <?php $modelLaporan->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modelLaporan->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php echo $form->labelEx($modelLaporan,'tglAkhir', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',
                            array(
                                    'model'=>$modelLaporan,
                                    'attribute'=>'tglAkhir',
                                    'mode'=>'datetime',
                                    'options'=>array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'dtPicker2-5',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
                            )
                        ); 
                    ?>

                </div>
            </div>
            <div style="margin-top:4px;"><?php echo CHtml::label('Unit Kerja', 'Unit Kerja', array('class' => 'control-label')) ?>
                </div>
            <div class="controls">
                <?php
                    echo $form->dropDownList($modelLaporan,'ruangan_id',CHtml::listData(RuanganM::model()->findAll(),
                            'ruangan_id','ruangan_nama'),array('class'=>'span2','style'=>'width:140px','empty'=>'-- Pilih --')); 
                ?>
            </div>
        </td>
        <td>            
            <div class='control-group'>
                  <?php echo CHtml::label('Kode Rekening','kodeRekening', array('class'=>'control-label')) ?>
                
                 <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $modelLaporan,
                                'attribute' => 'kodeRekening',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningKodeAkuntansi'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                        if(ui.item.kdrincianobyek ){
                                            $(this).val(ui.item.kdrincianobyek);
                                        }else{
                                            if(ui.item.kdobyek){
                                                $(this).val(ui.item.kdobyek);
                                            }else if(ui.item.kdjenis){
                                                $(this).val(ui.item.kdjenis)
                                            }
                                        }
                                        return false;
                                    }',
                                    'select' => 'js:function( event, ui ) {
                                           $(this).val(ui.item.value);                                            
                                           return false;
                                    }'
                                ),
                                'htmlOptions' => array(
//                                    'onkeypress' => "return $(this).focusNextInputField(event)",
                                    'placeholder'=>'Ketikan Kode Rekening',
                                    'class'=>'span3',
                                    'style'=>'width:150px;',
                                ),
                            ));
                        ?>
                 </div>
            </div>
            
            <div class='control-group'>
                  <?php echo CHtml::label('Nama Rekening','namaRekening', array('class'=>'control-label')) ?>
                 <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $modelLaporan,
                                'attribute' => 'namaRekening',
                                'name'=>'namaRekening',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                         if(ui.item.nmrincianobyek){
                                            $(this).val(ui.item.nmrincianobyek);
                                         }else{
                                            if(ui.item.nmobyek){
                                                $(this).val(ui.item.nmobyek);
                                            }else if(ui.item.nmjenis){
                                                $(this).val(ui.item.nmjenis)
                                            }
                                         }
                                         return false;
                                    }',
                                    'select' => 'js:function( event, ui ) {
                                        $(this).val(ui.item.value);
                                          return false;
                                    }'
                                ),
                                'htmlOptions' => array(
                                    'onkeypress' => "return $(this).focusNextInputField(event)",
                                    'placeholder'=>'Ketikan Nama Rekening',
                                    'class'=>'span3',
                                    'style'=>'width:150px;',
                                ),
                            ));
                        ?>
                 </div>
            </div>
        </td>
    </tr>
</table>
    <div class="form-actions">
        <?php
                echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
                    array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));?>
        
        <?php
                echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
</div>  

<?php
    $this->endWidget();
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>
<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#content4").find("input[type=\'checkbox\']").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
