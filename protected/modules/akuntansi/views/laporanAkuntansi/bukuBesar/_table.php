<style type="text/css">
  .table th{
    text-align: center;
  }
</style>
<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        // $data = $model->searchTable();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        // $data = $model->searchNeraca();
        //  $template = "{pager}{summary}\n{items}";
    }

  $a=0;
  foreach ($jmlrekening as $key => $jmls) {
    $a += $jmls['urutan'];
    $struktur[$key] = $a;
  }
?>


<?php 
    if(isset($caraPrint)){
       // print_r(count($model));
        $nmRekening_temp = "";
        foreach($model as $i=>$data){
            if($data->nmrekening5){
                $nmRekening = $data->nmrekening5;
            }else{
                if($data->nmrekening4){
                    $nmRekening = $data->nmrekening4;
                }else{
                    $nmRekening = $data->nmrekening3;
                }
            }
            
            if($nmRekening_temp != $nmRekening)
            {
            
                if($data->kdrekening5){
                    $kdRekening = $data->kdrekening5;
                    $kdRekening_text = $data->kdrekening1.'-'.$data->kdrekening2.'-'.$data->kdrekening3.'-'.$data->kdrekening4.'-'.$data->kdrekening5;
                }else{
                    if($data->kdrekening4){
                        $kdRekening = $data->kdrekening4;
                        $kdRekening_text = $data->kdrekening1.'-'.$data->kdrekening2.'-'.$data->kdrekening3.'-'.$data->kdrekening4;
                    }else{
                        $kdRekening = $data->kdrekening3;
                        $kdRekening_text = $data->kdrekening1.'-'.$data->kdrekening2.'-'.$data->kdrekening3;
                    }
                }
            echo "<table >
                    <tr>
                        <td>Nama Rekening</td>
                        <td>:</td>
                        <td>".$nmRekening."</td>
                    </tr>
                    <tr>
                        <td>Kode Rekening</td>
                        <td>:</td>
                        <td>".$kdRekening_text."</td>
                    </tr>";
            echo "</table>";
            
            echo "<table width='100%' border='1'>
                            <tr>
                                <td rowspan='2' align='center'>Tgl Posting</td>
                                <td rowspan='2' align='center'>Uraian Transaksi</td>
                                <td rowspan='2' align='center'>No. Ref</td>
                                <td colspan='2' align='center'>Saldo</td>
                                <td rowspan='2' align='center'>Saldo</td>
                            </tr>
                            <tr>
                                <td align='center'>Debit</td>
                                <td align='center'>Kredit</td>
                            </tr>";
               
            $criteria = new CDbCriteria;
            $term = $nmRekening;
            $termKode = $kdRekening;
            $criteria->compare('ruangan_id',$modelLaporan->ruangan_id);
            // $criteria->compare('kdrekening5',$termKode);
            $criteria->addBetweenCondition('tglbukubesar',$modelLaporan->tglAwal, $modelLaporan->tglAkhir);
            $condition  = "nmrekening5 ILIKE '%".$term."%' OR nmrekening4 ILIKE '%". $term ."%' OR nmrekening3 ILIKE '%". $term ."%'";
            $conditionKode  = "kdrekening5 ILIKE '%".$termKode."%' OR kdrekening4 ILIKE '%". $termKode ."%' OR kdrekening3 ILIKE '%". $termKode ."%'";
            $criteria->addCondition($condition);
            $criteria->addCondition($conditionKode);
            $criteria->limit = -1;
            $criteria->order = 'tglbukubesar ASC, nourut ASC';
            
            $totDebit = 0;
            $totKredit = 0;
            $totSaldo = 0;
            $detail = AKLaporanBukuBesarV::model()->findAll($criteria);
            foreach($detail as $key=>$details){
                    if($details->saldodebit >0 && $details->saldokredit >0){
                        $saldo = $details->saldodebit - $details->saldokredit;
                    }else if($details->saldodebit > 0){
                        $saldo = $details->saldodebit - $details->saldokredit;
                    }else if($details->saldodebit <=0 && $details->saldokredit >0){
                        $saldo = $details->saldodebit - $details->saldokredit;
                    }

                    if($details->saldonormal=='D'){
                      $saldo = $details->saldodebit - $details->saldokredit;
                    }else{
                      $saldo = $details->saldokredit;
                    }
                    
                    $totDebit += $details->saldodebit;
                    $totKredit += $details->saldokredit;
                    
                    if($details->saldonormal=='D'){
                      $totSaldo = $totDebit - $totKredit;
                    }elseif($details->saldonormal=='K') {
                      $totSaldo = $totKredit - $totDebit ;
                    }  
                    // if($totSaldo < 0){
                    //     $totSaldo = 0;
                    // }
                    echo "<tr>
                              <td width='150px;'>".$details->tglbukubesar."</td>
                              <td width='200px;'>".$details->uraiantransaksi."</td>
                              <td width='40px;' style='text-align:center'>".$details->noreferensi."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($details->saldodebit)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($details->saldokredit)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totSaldo)."</td>
                          </tr>";
            }
            
                    echo "<tfoot>
                            <tr>
                              <td colspan=3 style='text-align:right'>Total : </td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totDebit)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totKredit)."</td>
                              <td width='150px;' style='text-align:right'>".MyFunction::formatNumber($totSaldo)."</td>
                            </tr>
                          </tfoot>";
            echo "</table><br/><br/>";
            
            }
            $nmRekening_temp = $nmRekening;
            
        }
    
?>

<?php }else{  ?>
  
  <div id="tableLaporan" class="grid-view">
    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th id="tableLaporan_c0" rowspan="2">
            Tgl. Posting
          </th>
          <th id="tableLaporan_c0" rowspan="2">
            Uraian Transaksi
          </th>      
          <th id="tableLaporan_c0" rowspan="2">
            No. Ref
          </th>
          <th id="tableLaporan_c0" colspan="2" align="center">
            Saldo
          </th>
          <th id="tableLaporan_c0" rowspan="2">
            Saldo
          </th>
        </tr>
        <tr>
          <th id="tableLaporan_c0">
            Debit
          </th>
          <th id="tableLaporan_c0">
            Kredit
          </th>
        </tr>
      </thead>   
      <tbody>
        <?php
          $nmrekening5[-1]    = '';
          $format = new CustomFormat();
          $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
          $i = 0;
          $total = count($model);
          $totaldebit   = 0;
          $totalkredit  = 0;
          foreach ($model as $key => $value) 
          {
            $nmrekening5[$key]    = $value['nmrekening5'];
            $tglbukubesar[$key]  = $format->formatDateINAtime($value['tglbukubesar']);
            if($nmrekening5[$key-1]!=$nmrekening5[$key])
            {
              $rekening = $value['nmrekening5'];   
              $totaldebit   += $value['saldodebit'];
              $totalkredit  += $value['saldokredit'];
              echo "<tr>
                      <td colspan=6><b><i>".$rekening."</i></b></td>
                    </tr>";
            }else{
              $totaldebit += $value['saldodebit'];
              $totalkredit  += $value['saldokredit'];
            }
            if($value['saldonormal']=='D'){
              $saldo = $totaldebit - $totalkredit;  
            }else if($value['saldonormal']=='K'){
              $saldo = $totalkredit - $totaldebit;  
            }
            
            echo "<tr>
                    <td>".$spasi.$tglbukubesar[$key]."</td>
                    <td>".$value['uraiantransaksi']."</td>
                    <td>".$value['noreferensi']."</td>
                    <td>".number_format($value['saldodebit'])."</td>
                    <td>".number_format($value['saldokredit'])."</td>
                    <td>".number_format($saldo)."</td>
                  </tr>";
  
            
            $i++;      
            if(in_array($i, $struktur))
            {
              echo '<tr>';      
              echo '<td colspan="3"><i><b>Total '.$rekening.'</b><i></td>';
              echo '<td>'.number_format($totaldebit).'</td>';
              echo '<td>'.number_format($totalkredit).'</td>';
              echo '<td>'.number_format($saldo).'</td>';
              echo '</tr>';  

              $totaldebit   = 0;    
              $totalkredit  = 0;     
              $saldo  = 0;       
            }
          }
        ?>
      </tbody>
    </table>   
  </div>

<?php
  // $this->widget($table,array(
  // 'id'=>'tableLaporan',
  // 'dataProvider'=>$data,
  //       'template'=>$template,
  //       'enableSorting'=>$sort,
  //       'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  //       'mergeHeaders'=>array(
  //           array(
  //               'name'=>'<center>Saldo</center>',
  //               'start'=>6, //indeks kolom 3
  //               'end'=>7, //indeks kolom 4
  //           ),
  //       ),
  // 'columns'=>array(
  //          array(
  //              'header'=>'No',
  //              'type'=>'raw',
  //              'value'=>'$row+1',
  //              'htmlOptions'=>array(
  //                  'style'=>'text-align:center',
  //              )
  //          ),
  //          array(
  //             'header'=>'Kode Rekening',
  //             'type'=>'raw',
  //             'value'=>'$data->getKodeRekening()',
  //          ),
  //          array(
  //             'header'=>'Nama Rekening',
  //             'type'=>'raw',
  //             'value'=>'$data->getNamaRekening()',
  //             'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-style:italic;'),
  //             'footer'=>'Jumlah Total',
  //          ),
  //          array(
  //             'header'=>'Tgl Posting',
  //             'type'=>'raw',
  //             'value'=>'$data->tgljurnalpost',
  //          ),
  //          array(
  //             'header'=>'Uraian Transaksi',
  //             'type'=>'raw',
  //             'value'=>'$data->uraiantransaksi',
  //          ),
  //          array(
  //             'header'=>'No Referensi',
  //             'type'=>'raw',
  //             'value'=>'$data->noreferensi',
  //          ),
  //          array(
  //             'header'=>'<center>Debit</center>',
  //             'name'=>'saldodebit',
  //             'value'=>'MyFunction::formatNumber($data->saldodebit)',
  //             'htmlOptions'=>array('style'=>'width:100px;text-align:right', 'class'=>'currency'),
  //             'footerHtmlOptions'=>array('style'=>'text-align:right;'),
  //             'footer'=>'sum(saldodebit)',
  //          ),
  //          array(
  //             'header'=>'<center>Kredit</center>',
  //             'name'=>'saldokredit',
  //             'value'=>'MyFunction::formatNumber($data->saldokredit)',
  //             'htmlOptions'=>array('style'=>'width:100px;text-align:right', 'class'=>'currency'),
  //             'footerHtmlOptions'=>array('style'=>'text-align:right;'),
  //             'footer'=>'sum(saldokredit)',
  //          ), 
  //          array(
  //             'header'=>'<center>Saldo</center>',
  //             'name'=>'getTotSaldo',
  //             'value'=>'MyFunction::formatNumber($data->getSaldoAkhir())',
  //             'htmlOptions'=>array('style'=>'width:100px;text-align:right', 'class'=>'currency'),
  //             'footerHtmlOptions'=>array('style'=>'text-align:right;'),
  //             'footer'=>MyFunction::formatNumber($model->getTotSaldo()),
  //          ), 
  // ),
  //       'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
  //   )); 

  } 
?>
