<div id="tableLaporan" class="grid-view" style="">
  <table class="table table-striped table-bordered table-condensed">
    <thead>
      <tr>
        <th>
            <center>Nama Rekening</center>
        </th>
        <th>
            <center>Total Saldo</center>
        </th>
      </tr>
    </thead>
    <tbody>
      <?php
        $tab1 = '&nbsp; &nbsp; &nbsp; ';
        $tab2 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        $tab3 = '&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ';
        if(COUNT($model) > 0){
            
            foreach ($model as $i => $neraca) {
                echo '<tr><td colspan="2">'.$tab1.' - '.strtoupper($neraca->nmrekening1).'</td></tr>';
               
                //start rekening ke 2
                $criteria2 = new CDbCriteria;
                $criteria2->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                if(!empty($_GET['AKLaporanneracanewV']['rekperiod_id'])){
                    $criteria2->addCondition('rekperiod_id='.$_GET['AKLaporanneracanewV']['rekperiod_id']);
                }
                $criteria2->addCondition('rekening1_id='.$neraca->rekening1_id);
                $criteria2->group = 'rekening2_id, nmrekening2, rekperiod_id';
                $criteria2->select = $criteria2->group;
                $model2 = AKLaporanneracanewV::model()->findAll($criteria2);
                if(COUNT($model2) > 0){
                    $total_rekning1 = 0;
                    foreach ($model2 as $i2 => $neraca2) {
                        echo '<tr><td colspan="2">'.$tab2.' - '.strtoupper($neraca2->nmrekening2).'</td></tr>'; 
                        
                        //start rekening ke 3
                        $criteria3 = new CDbCriteria;
                        $criteria3->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
                        if(!empty($_GET['AKLaporanneracanewV']['rekperiod_id'])){
                            $criteria3->addCondition('rekperiod_id='.$_GET['AKLaporanneracanewV']['rekperiod_id']);
                        }
                        $criteria3->addCondition('rekening1_id='.$neraca->rekening1_id);
                        $criteria3->addCondition('rekening2_id='.$neraca2->rekening2_id);
                        $criteria3->select = 'rekening3_id, nmrekening3, sum(saldo) AS saldo';
                        $criteria3->group = 'rekening3_id, nmrekening3';
                        $criteria3->order = 'rekening3_id ASC';
                        $model3 = AKLaporanneracanewV::model()->findAll($criteria3);
                        if(COUNT($model3) > 0){
                           $total_saldo = 0;
                           foreach ($model3 as $i3 => $neraca3) {
                               echo '<tr><td>'.$tab3.' - '.$neraca3->nmrekening3.'</td>
                                        <td style="text-align:right;">'.number_format($neraca3->saldo,2).'</td>
                                       </tr>';
                               $total_saldo += $neraca3->saldo;
                           } 
                        }
                        $total_rekning1 += $total_saldo;
                        //end rekening ke 3
                        
                        echo '<tr><td style="text-align:right; font-weight:bold;"> TOTAL '.strtoupper($neraca2->nmrekening2).'</td>
                        <td style="text-align:right; font-weight:bold;">'.number_format($total_saldo,2).'</td></tr>'; 
                   }
                }
                //end rekening ke 2
                echo '<tr><td style="text-align:right; font-weight:bold;"> TOTAL '.strtoupper($neraca->nmrekening1).'</td>
                        <td style="text-align:right; font-weight:bold;">'.number_format($total_rekning1,2).'</td></tr>'; 
            }
            
        }
        else{
            echo '<tr><td colspan="2"><span class="empty">Tidak ditemukan hasil.</span></td></tr>';
        }
      ?>
    </tbody>
  </table>
</div>