<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$url = Yii::app()->createUrl('akuntansi/laporanAkuntansi/frameGrafikLaporanPerubahanModal&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('tableLaporan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Perubahan Modal</legend>
<div class="search-form">
<?php $this->renderPartial('akuntansi.views.laporanAkuntansi.perubahanmodal/_search',array('model'=>$model, 'filter'=>$filter)); 
?>
</div><!-- search-form --> 
<fieldset> 
    <legend class="rim">Tabel Perubahan Modal</legend>
    <?php $this->renderPartial('akuntansi.views.laporanAkuntansi.perubahanmodal/_table', array('model'=>$model, 'periode'=>$periode,'format'=>$format)); ?>
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanPerubahanModal');
    $this->renderPartial('akuntansi.views.laporanAkuntansi._footerNoGraph', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>