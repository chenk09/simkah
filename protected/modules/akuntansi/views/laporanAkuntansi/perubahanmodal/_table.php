<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>
<?php

  $criteria=new CDbCriteria;
  $criteria->select ='sum(saldo) as saldo, issaldoawal, rekening1_id';
  $criteria->compare('LOWER(perideawal)',strtolower($periode['tglawal']),true);
  $criteria->compare('LOWER(sampaidgn)',strtolower($periode['tglakhir']),true);
  $criteria->compare('ruangan_id',$model->ruangan_id);
  $condition = "nmrekening3 not like '%%Pembagian Deviden%%'";
  $criteria->addCondition($condition);
  $criteria->group = 'rekening1_id, issaldoawal'; 

  $data = LaporanperubahanmodalV::model()->findAll($criteria);
  $saldo_a = 0; //$saldo_a = 0;
  $saldo_b = 0; //$saldo_b = 0;
  $saldo_d = 0; //$saldo_d = 0;
  $saldo_e = 0; //$saldo_e = 0;
  $saldo_g = 0; //$saldo_g = 0;
  foreach ($data as $key => $value) 
  {
    $rekening[$key]     = $value['rekening1_id'];
    $saldo[$key]   = $value['saldo'];
    $issaldoawal        = $value['issaldoawal'];

    if($issaldoawal=='true'){
      $saldo_a     += $saldo[$key];
    }else{
      if($rekening[$key]==10){
        $saldo_b     += $saldo[$key];
      }elseif($rekening[$key]==3){
        $saldo_d     += $saldo[$key];
      }elseif($rekening[$key]==4){
        $saldo_e     += $saldo[$key];
      }elseif($rekening[$key]==11){
        $saldo_g     += $saldo[$key];
      }
    }
  }

//   ===== Rekening Pembagian Deviden =====
  $criteria=new CDbCriteria;
  $criteria->select ='sum(saldo) as saldo, issaldoawal, rekening1_id';
  $criteria->compare('LOWER(perideawal)',strtolower($periode['tglawal']),true);
  $criteria->compare('LOWER(sampaidgn)',strtolower($periode['tglakhir']),true);
  $criteria->compare('ruangan_id',$model->ruangan_id);
  $condition = "nmrekening3 like '%%Pembagian Deviden%%'";
  $criteria->addCondition($condition);
  $criteria->group = 'rekening1_id, issaldoawal'; 

  $data2 = LaporanperubahanmodalV::model()->findAll($criteria);
  $saldo_f = 0; $saldo_f = 0;
  foreach ($data2 as $key => $value2) {
    $rekening[$key]     = $value2['rekening1_id'];
    $saldo[$key]   = $value2['saldo'];
    $issaldoawal        = $value2['issaldoawal'];
    if($issaldoawal==false){
      $saldo_f     += $saldo[$key];
    }
  }

?>
<div id="tableLaporan" class="grid-view">
<table class="table table-striped table-bordered table-condensed">
        <thead>
            <tr>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Perkiraan
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Modal Disetor
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                            Saham yang <br> Diperoleh Kembali
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Tambahan Modal Disetor
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Hibah
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Cadangan Umum
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Cadangan Tujuan
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                            Selisih Penilaian <br> Aktiva tetap
                        </th>
                        <th id="tableLaporan_c0" style="text-align:center; height: 30px; vertical-align: middle;">
                                Laba (Rugi)
                        </th>
                </tr>
        </thead>
        <tbody>
                <tr>
                    <td><b>
                            <?php if(isset($_GET['AKLaporanperubahanmodalV'])){
                            echo substr("Saldo Per ".$_GET['AKLaporanperubahanmodalV']['tglAwal'], 0, -8);
                            }?>
                        </b></td>
                    <td><b><?php echo number_format($model->getModal(true,18)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,19)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,20)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,21)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,22)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,23)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,24)->saldo);  ?></b></td>
                    <td><b><?php echo number_format($model->getModal(true,0)->saldo);  ?></b></td>
                </tr>
                <tr>
                        <td>Modal Disetor</td>
                        <td><?php echo number_format($model->getModal(false,18)->saldo);  ?></td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Saham yang Diperoleh Kembali</td>
                        <td>0</td>
                        <td><?php echo number_format($model->getModal(false,19)->saldo);  ?></td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Tambahan Modal</td>
                        <td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,20)->saldo);  ?></td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Hibah</td>
                        <td>0</td><td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,21)->saldo);  ?></td>
                        <td>0</td><td>0</td><td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Cadangan Umum</td>
                        <td>0</td><td>0</td><td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,22)->saldo);  ?></td>
                        <td>0</td><td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Cadangan Tujuan</td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,23)->saldo);  ?></td>
                        <td>0</td><td>0</td>
                </tr>
                <tr>
                        <td>Selisih Penilaian Aktiva Tetap</td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,24)->saldo);  ?></td>
                        <td>0</td>
                </tr>
                <tr>
                        <td>Laba (Rugi)</td>
                        <td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td>
                        <td><?php echo number_format($model->getModal(false,0)->saldo);  ?></td>
                </tr>
                <tr>
                    <td><b>
                        <?php if(isset($_GET['AKLaporanperubahanmodalV'])){
                            echo substr("Saldo Per ".$_GET['AKLaporanperubahanmodalV']['tglAkhir'], 0, -8);
                            }?>
                        </b></td>
                        <td><b><?php echo number_format($model->getTotal(18)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(19)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(20)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(21)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(22)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(23)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(24)->saldo);  ?></b></td>
                        <td><b><?php echo number_format($model->getTotal(0)->saldo);  ?></b></td>
                </tr>
                
                <!---------------------------------------------------------------------------------------------------------------------------------------->
                
        </tbody>
</table>
</div>