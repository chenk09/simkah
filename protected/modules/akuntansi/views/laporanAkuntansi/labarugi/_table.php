<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->search();
//        $modelLaporan = LaporanlabarugiV::model()->findAll(array('order'=>'rekening1_id, rekening2_id, rekening3_id, rekening4_id'));
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
//        $modelLaporan = LaporanlabarugiV::model()->findAll(array('order'=>'rekening1_id, rekening2_id, rekening3_id, rekening4_id'));
    }
//    $modRuangans = $model->getRuangan();
    $criteria = new CDbCriteria;
    $criteria->group = 'rekening1_id, nmrekening1, rekening2_id, nmrekening2, rekening3_id, nmrekening3, rekening4_id, nmrekening4';
    $criteria->select = $criteria->group;
    $criteria->order = 'rekening1_id, rekening2_id, rekening3_id, rekening4_id';
    $modelLaporan = LaporanlabarugiV::model()->findAll($criteria);
?>

<div id="tableLaporan" class="grid-view">
  	<table class="table table-striped table-bordered table-condensed">
	    <thead>
	      <tr>
	        <th id="tableLaporan_c0">
	          Rincian
	        </th>
	        <th id="tableLaporan_c0">
	          Total Nominal
	        </th>
                <?php
//                foreach ($modRuangans as $ruangan) {
//                    echo "<th id='tableLaporan_c0'>".$ruangan->ruangan_nama."</th>";
//                }
                ?>
	      </tr>
	    </thead>
	    <tbody> 
                <?php
                $spasi = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                ?>
                <tr>
                    <td>
                        <b>PENDAPATAN OPERASIONAL</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pendapatan Operasional Bruto</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_pend_bruto = 0;
                    foreach ($modelLaporan as $i=>$labarugi){
                        if ($labarugi->nmrekening1 == 'Pendapatan Operasional' && $labarugi->nmrekening3 == 'Pendapatan Operasional Bruto'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_pend_bruto += $nilai_nominal;
                            
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Pendapatan Operasional Bruto</b>
                    </td>
                    <?php
                    echo "<td>".number_format($jml_pend_bruto,0)."</td>";
//                    foreach ($modRuangans as $ruangan){
//                        $jml_pend_bruto_ruangan = $model->getSaldoRuangan('Pendapatan Operasional Bruto',$ruangan->ruangan_id,'rekening3');
//                        echo "<td>".number_format($jml_pend_bruto_ruangan,0)."</td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengurangan Pendapatan Operasional</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_kurang_pend_op = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Pendapatan Operasional' && $labarugi->nmrekening3 == 'Pengurang pendapatan operasional'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_kurang_pend_op += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Pengurangan Pendapatan Operasional</b>
                    </td>
                    <?php
                    echo "<td>".number_format($jml_kurang_pend_op,0)."</td>";
//                    foreach ($modRuangans as $ruangan){
//                        $jml_kurang_pend_ruangan = $model->getSaldoRuangan('Pengurang pendapatan operasional',$ruangan->ruangan_id,'rekening3');
//                        echo "<td>".number_format($jml_kurang_pend_ruangan,0)."</td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right:2em">
                        <b><i>Jumlah Pendapatan Operasional</i></b>
                    </td>
                    <?php 
                    $jml_pend_op = $jml_pend_bruto + $jml_kurang_pend_op;
                    echo "<td><b><i>".number_format($jml_pend_op)."</i></b></td>";
                    $jml_pend_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $jml_pend_op_ruangan[] = $model->getSaldoRuangan('Pendapatan Operasional',$ruangan->ruangan_id,'rekening1');
//                        echo "<td><b><i>".number_format($jml_pend_op_ruangan[$i],0)."</i></b></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>PENDAPATAN NON OPERASIONAL</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_pend_non_op = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Pendapatan non operasional' && $labarugi->nmrekening3 == 'Pendapatan non operasional'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_pend_non_op += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td style="text-align: right; padding-right:2em">
                        <b><i>Jumlah Pendapatan Non Operasional</i></b>
                    </td>
                    <?php
                    echo "<td><b><i>".number_format($jml_pend_non_op,0)."</i></b></td>";
                    $jml_pend_non_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $jml_pend_non_op_ruangan[] = $model->getSaldoRuangan('Pendapatan non operasional',$ruangan->ruangan_id,'rekening3');
//                        echo "<td><b><i>".number_format($jml_pend_non_op_ruangan[$i],0)."</i></b></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right:2em; height: 20px;"><b>JUMLAH PENDAPATAN</b></td>
                    <?php
                        $jml_pend = $jml_pend_op+$jml_pend_non_op;
                    echo "<td><b>".  number_format($jml_pend)."</b></td>";
//                    $jml_pend_non_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        
//                        echo "<td><b>".number_format($jml_pend_op_ruangan[$i]+$jml_pend_non_op_ruangan[$i],0)."</b></td>";
//                    }
                    ?>
                </tr>
            <thead>
                <tr>
                    <th style="height: 20px;"></th>
                    <th></th>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<th></th>";
//                    }
                    ?>
                </tr>
            </thead>
                <tr>
                    <td>
                        <b>BEBAN OPERASIONAL</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beban Pelayanan</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_beban_pel = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Beban operasional' && $labarugi->nmrekening3 == 'Beban pelayanan'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_beban_pel += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Beban Pelayanan</b>
                    </td>
                    <?php
                    echo "<td>".number_format($jml_beban_pel,0)."</td>";
//                    foreach ($modRuangans as $ruangan){
//                        $jml_beban_pel_ruangan = $model->getSaldoRuangan('Beban pelayanan',$ruangan->ruangan_id,'rekening3');
//                        echo "<td>".number_format($jml_beban_pel_ruangan,0)."</td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Beban Umum dan Administrasi</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_beban_umum = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Beban operasional' && $labarugi->nmrekening3 == 'Beban umum dan administrasi'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_beban_umum += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jumlah Beban Umum dan Administrasi</b>
                    </td>
                    <?php
                    echo "<td>".number_format($jml_beban_umum,0)."</td>";
//                    foreach ($modRuangans as $ruangan){
//                        $jml_beban_umum_ruangan = $model->getSaldoRuangan('Beban umum dan administrasi',$ruangan->ruangan_id,'rekening3');
//                        echo "<td>".number_format($jml_beban_umum_ruangan,0)."</td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right:2em">
                        <b><i>Jumlah Beban Operasional</i></b>
                    </td>
                    <?php 
                    $jml_beban_op = $jml_beban_pel + $jml_beban_umum;
                    echo "<td><b><i>".number_format($jml_beban_op)."</i></b></td>";
//                    $jml_beb_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $jml_beb_op_ruangan[] = $model->getSaldoRuangan('Beban operasional',$ruangan->ruangan_id,'rekening1');
//                        echo "<td><b><i>".number_format($jml_beb_op_ruangan[$i],0)."</i></b></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>BEBAN NON OPERASIONAL</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_beban_non_op = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Beban non operasional' && $labarugi->nmrekening3 == 'Beban non operasional'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_beban_non_op += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <td style="text-align: right; padding-right:2em">
                        <b><i>Jumlah Beban Non Operasional</i></b>
                    </td>
                    <?php
                    echo "<td><b><i>".number_format($jml_beban_non_op,0)."</i></b></td>";
//                    $jml_beban_non_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $jml_beban_non_op_ruangan[] = $model->getSaldoRuangan('Beban non operasional',$ruangan->ruangan_id,'rekening3');
//                        echo "<td><b><i>".number_format($jml_beban_non_op_ruangan[$i],0)."</i></b></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td style="text-align: right; padding-right:2em; height: 20px;"><b>JUMLAH BEBAN</b></td>
                    <?php
                        $jml_beban = $jml_beban_op+$jml_beban_non_op;
                    echo "<td><b>".  number_format($jml_beban)."</b></td>";
//                    $jml_pend_non_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        
//                        echo "<td><b>".number_format($jml_beb_op_ruangan[$i]+$jml_beban_non_op_ruangan[$i],0)."</b></td>";
//                    }
                    ?>
                </tr>
                <thead>
                <tr>
                    <th style="height: 20px;"></th>
                    <th></th>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<th></th>";
//                    }
                    ?>
                </tr>
                </thead>
<!--                <tr>
                    <?php
//                    $laba_rugi_op = $jml_pend_op - $jml_beban_op;
//                    if ($laba_rugi_op < 0){
//                        $status = "RUGI";
//                    } else {
//                        $status = "LABA";
//                    }
                    ?>
                    <td>
                        <b><?php // echo $status; ?> OPERASIONAL</b>
                    </td>
                    <?php 
//                    echo "<td><b>".number_format($laba_rugi_op)."</b></td>";
//                    $laba_rugi_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
////                        $jml_pend_op_ruangan = $model->getSaldoRuangan('Pendapatan Operasional',$ruangan->ruangan_id,'rekening1');
////                        $jml_beb_op_ruangan = $model->getSaldoRuangan('Beban operasional',$ruangan->ruangan_id,'rekening1');
//                        $laba_rugi_op_ruangan[] = $jml_pend_op_ruangan[$i] - $jml_beb_op_ruangan[$i];
//                        echo "<td><b>".number_format($laba_rugi_op_ruangan[$i],0)."</b></td>";
//                    }
                    ?>
                </tr>-->
                
<!--                <tr>
                    <?php
//                    $laba_rugi_non_op = $jml_pend_non_op - $jml_beban_non_op;
//                    if ($laba_rugi_non_op < 0){
//                        $status = "RUGI";
//                    } else {
//                        $status = "LABA";
//                    }
                    ?>
                    <td>
                        <b><?php // echo $status; ?> NON OPERASIONAL</b>
                    </td>
                    <?php 
//                    echo "<td><b>".number_format($laba_rugi_non_op)."</b></td>";
//                    $laba_rugi_non_op_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $laba_rugi_non_op_ruangan[] = $jml_pend_non_op_ruangan[$i] - $jml_beban_non_op_ruangan[$i];
//                        echo "<td><b>".number_format($laba_rugi_non_op_ruangan[$i],0)."</b></td>";
//                    }
                    ?>
                </tr>-->
                <tr>
                    <?php
//                    $laba_rugi_no_pajak = $laba_rugi_op + $laba_rugi_non_op;
                    if ($jml_pend-$jml_beban < 0){
                        $status = "RUGI";
                    } else {
                        $status = "LABA";
                    }
                    ?>
                    <td>
                        <b><?php echo $status; ?> SEBELUM PAJAK</b>
                    </td>
                    <?php 
                    echo "<td><b>".number_format($jml_pend-$jml_beban)."</b></td>";
//                    $laba_rugi_no_pajak_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $beban_ruang = $jml_beb_op_ruangan[$i]+$jml_beban_non_op_ruangan[$i];
//                        $pend_ruang = $jml_pend_op_ruangan[$i]+$jml_pend_non_op_ruangan[$i];
//                        echo "<td><b>".number_format($pend_ruang-$beban_ruang,0)."</b></td>";
//                    }
                    ?>
                </tr>
                <tr>
                    <td>
                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pajak Penghasilan</b>
                    </td>
                    <td></td>
                    <?php
//                    foreach ($modRuangans as $ruangan){
//                        echo "<td></td>";
//                    }
                    ?>
                </tr>
                    <?php
                    $jml_pajak = 0;
                    foreach ($modelLaporan as $labarugi){
                        if ($labarugi->nmrekening1 == 'Pajak penghasilan'){
                            echo "<tr>";
                            echo "<td>".$spasi.$labarugi->nmrekening4."</td>";
                            
                            $nilai_nominal = $model->getSaldoRuangan($labarugi->nmrekening4,'','rekening4');
                            echo "<td>".number_format($nilai_nominal,0)."</td>";
                            $jml_pajak += $nilai_nominal;
                            
//                            foreach ($modRuangans as $ruangan){
//                                $nilai_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
//                                echo "<td>".number_format($nilai_per_ruangan,0)."</td>";
//                            }
                            echo "</tr>";
                        }
                    }
                    ?>
                <tr>
                    <?php
                    $laba_rugi_pajak = ($jml_pend-$jml_beban) - $jml_pajak;
                    if ($laba_rugi_pajak < 0){
                        $status = "RUGI";
                    } else {
                        $status = "LABA";
                    }
                    ?>
                    <td>
                        <b><?php echo $status; ?> SETELAH PAJAK</b>
                    </td>
                    <?php 
                    echo "<td><b>".number_format($laba_rugi_pajak)."</b></td>";
//                    $laba_rugi_pajak_ruangan = array();
//                    foreach ($modRuangans as $i=>$ruangan){
//                        $pajak_per_ruangan = $model->getSaldoRuangan($labarugi->nmrekening4,$ruangan->ruangan_id,'rekening4');
////                        $laba_rugi_pajak_ruangan[] = $laba_rugi_no_pajak_ruangan[$i] - $pajak_per_ruangan;
//                        $beban_ruang = $jml_beb_op_ruangan[$i]+$jml_beban_non_op_ruangan[$i];
//                        $pend_ruang = $jml_pend_op_ruangan[$i]+$jml_pend_non_op_ruangan[$i];
//                        echo "<td><b>".number_format(($pend_ruang-$beban_ruang)-$pajak_per_ruangan,0)."</b></td>";
//                    }
                    ?>
                </tr>
	    </tbody>
    </table>
</div>