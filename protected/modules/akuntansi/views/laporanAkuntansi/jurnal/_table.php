<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Saldo</center>',
                'start'=>6, 
                'end'=>7,
            ),
        ),
	'columns'=>array(
           array(
               'header'=>'No',
               'type'=>'raw',
               'value'=>'$row+1',
               'htmlOptions'=>array(
                   'style'=>'text-align:center',
               )
           ),
           array(
              'header'=>'Tgl Posting ',
              'type'=>'raw',
              'value'=>'$data->tgljurnalpost ',
           ),
              array(
              'header'=>' Tgl Jurnal',
              'type'=>'raw',
              'value'=>'$data->tglbuktijurnal',
           ),
           array(
              'header'=>'Uraian Transaksi',
              'type'=>'raw',
              'value'=>'$data->uraiantransaksi',
           ),
           array(
              'header'=>'Kode Rekening',
              'type'=>'raw',
              'value'=>'$data->kdrekening1."-".$data->kdrekening2."-".$data->kdrekening3."-".$data->kdrekening4."-".$data->kdrekening5',
           ),
           array(
              'header'=>'Nama Rekening',
              'type'=>'raw',
              'value'=>'($data->getNamaRekeningDebit() == "-" ?  $data->getNamaRekeningKredit() : $data->getNamaRekeningDebit())',
              'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-style:italic;'),
              'footer'=>'Jumlah Total',
           ),
           array(
              'header'=>'<center>Debit</center>',
              'name'=>'saldodebit',
              'value'=>'MyFunction::formatNumber($data->saldodebit)',
              'htmlOptions'=>array('style'=>'width:100px;text-align:right', 'class'=>'currency'),
              'footerHtmlOptions'=>array('style'=>'text-align:right;'),
              'footer'=>'sum(saldodebit)',
           ),
           array(
              'header'=>'<center>Kredit</center>',
              'name'=>'saldokredit',
              'value'=>'MyFunction::formatNumber($data->saldokredit)',
              'htmlOptions'=>array('style'=>'width:100px;text-align:right', 'class'=>'currency'),
              'footerHtmlOptions'=>array('style'=>'text-align:right;'),
              'footer'=>'sum(saldokredit)',
           ), 
           array(
              'header'=>'Catatan',
              'type'=>'raw',
              'value'=>'$data->catatan',
              'footerHtmlOptions'=>array('style'=>'text-align:right;color:white'),
              'footer'=>'-',
           ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>