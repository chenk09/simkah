<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#search-form').submit(function(){
                $('#tableLaporan').addClass('srbacLoading');
            $.fn.yiiGridView.update('tableLaporan', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>
<?php

    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
         $data = $model->searchArusKas();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
//         $data = $model->searchArusKas();
//          $template = "{pager}{summary}\n{items}";
    }

	$criteria=new CDbCriteria;
	$criteria->select ='nmrekening5, sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit, sum(saldo) as saldo, issaldoawal, rekening1_id';
	$criteria->compare('DATE(perideawal)',strtolower($periode['tglawal']),true);
	$criteria->compare('DATE(sampaidgn)',strtolower($periode['tglakhir']),true);
                $criteria->compare('ruangan_id',$model->ruangan_id);
	$criteria->group = 'rekening1_id, issaldoawal, nmrekening5'; 
	$data = LaporanaruskasV::model()->findAll($criteria);
                              
	$saldodebit_x = 0; $saldokredit_x = 0; $saldo_x = 0;
	$saldodebit_y = 0; $saldokredit_y = 0; $saldo_y = 0;
	$saldodebit_z = 0; $saldokredit_z = 0; $saldo_z = 0;
	$saldodebit_c = 0; $saldokredit_c = 0; $saldo_c = 0;
	foreach ($data as $key => $value) {
		$rekening[$key] 	= $value['rekening1_id'];
		$saldodebit[$key] 	= $value['saldodebit'];
		$saldokredit[$key] 	= $value['saldokredit'];
                                $saldo[$key]            = $value['saldo'];
                                $issaldoawal            = $value['issaldoawal'];

                        if($issaldoawal==false || empty($issaldoawal)){
                                if($rekening[$key]==4 || $rekening[$key]==5)
                                {
                                        $saldodebit_x 	+= $saldodebit[$key];
                                        $saldokredit_x  += $saldokredit[$key];
                                        $saldo_x  += $saldo[$key];
                                }
                                if($rekening[$key]==6 || $rekening[$key]==7)
//                                if($rekening[$key]==1)
                                {
                                        $saldodebit_y 	+= $saldodebit[$key];
                                        $saldokredit_y  += $saldokredit[$key];
                                        $saldo_y  += $saldo[$key];
                                }

                                if($rekening[$key]==3)
                                {
                                        $saldodebit_z 	+= $saldodebit[$key];
                                        $saldokredit_z  += $saldokredit[$key];
                                        $saldo_z  += $saldo[$key];
                                }
                        }else{
                                $saldodebit_c += $saldodebit[$key];
                                $saldokredit_c  += $saldokredit[$key];
                                $saldo_c  += $saldo[$key];
                        }
	}
?>

<div id="tableLaporan" class="grid-view">
    <?php
    
    $criteriaInstalasi=new CDbCriteria;
    $criteriaInstalasi->group = 'ruangan_id, ruangan_nama'; 
    $criteriaInstalasi->select = $criteriaInstalasi->group;
    $criteriaInstalasi->order = 'ruangan_id';
    $dataInstalasi = AKLaporanaruskasV::model()->findAll($criteriaInstalasi);
?>
    <div style="max-width:1500px;overflow-x:scroll;">
    <table class="table table-striped table-bordered table-condensed">
      	<thead>
        <tr>
            <th id="tableLaporan_c0" rowspan="2" style="text-align:center;">
                            Uraian Transaksi
            </th>
            <th id="tableLaporan_c0" rowspan="2" style="text-align:center;">
                            Nominal
            </th>
            <?php 
                $jmlruang=0;
                $datatableheader = $model->getTableHeader();
                    foreach($datatableheader as $vth){
                       echo "<th><center>".$vth->instalasi_nama."</center></th>";
                       $jmlruang+=count($jmlruang);
                    }
            ?>
        </tr>
        <tr>
            <?php
                foreach($datatableheader as $vth){
                   echo "<th><center>".$vth->ruangan_nama."</center></th>";
                }
            ?>
        </tr>
        </thead>
        <tbody>
            <tr><td style="height: 30px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"><b><i>ARUS KAS DARI AKTIFITAS OPERASI</i></b></td></tr>
                <?php $dataKat = $model->getKategori(4,5);
                foreach($dataKat as $keyTable){ ?>
                    <tr>
                        <td style="padding-left: 3em;"><?php
                         echo $keyTable->nmrekening5; ?></td>
                        <td><?php 
                                if(($keyTable->issaldoawal)==FALSE){
                                    echo number_format($keyTable->saldo);
                                }else{
                                    echo 0;
                                }
                                ?>
                        </td>
                        <?php
                                foreach($datatableheader as $vth){
                                        echo"<td>".number_format($model->getSaldoPerRek($vth->ruangan_id,$keyTable->rekening5_id))."</td>";
                                }
                        $totalperrek1_op += $keyTable->saldo;        
                        ?>            
                    </tr>
                <?php } ?>
            <tr>
                <td style="text-align: right; padding-right: 1em"><i><b>Kas Bersih Dari Aktifitas Operasi </b></i></td>
                <td><i><b><?php echo number_format($totalperrek1_op); ?></b></i></td>
                <?php
                        foreach($datatableheader as $vth){
                                        echo"<td><i><b>".number_format($model->getKategori(4,5,$vth->ruangan_id))."</b></i></td>";
                                }
                ?>
            </tr>
        </tbody>
        <thead><tr><th style="height: 20px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"></th></tr></thead>
        
        <tbody>
            <tr><td style="height: 30px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"><b><i>ARUS KAS DARI AKTIFITAS INVESTASI</i></b></td></tr>
                <?php $dataKat = $model->getKategori(6,7);
                foreach($dataKat as $keyTable){ ?>
                    <tr>
                        <td style="padding-left: 3em;"><?php
                         echo $keyTable->nmrekening5; ?></td>
                        <td><?php 
                                if(($keyTable->issaldoawal)==FALSE){
                                    echo number_format($keyTable->saldo);
                                }else{
                                    echo 0;
                                }
                                ?>
                        </td>
                        <?php
                                foreach($datatableheader as $vth){
                                        echo"<td>".number_format($model->getSaldoPerRek($vth->ruangan_id,$keyTable->rekening5_id))."</td>";
                                }
                        $totalperrek1_iv += $keyTable->saldo;        
                        ?>            
                    </tr>
                <?php } ?>
            <tr>
                <td style="text-align: right; padding-right: 1em"><i><b>Kas Bersih Dari Aktifitas Investasi </b></i></td>
                <td><i><b><?php echo number_format($totalperrek1_iv); ?></b></i></td>
                <?php
                        foreach($datatableheader as $vth){
                                        echo"<td><i><b>".number_format($model->getKategori(6,7,$vth->ruangan_id))."</b></i></td>";
                                }
                ?>
            </tr>
        </tbody>
        <thead><tr><th style="height: 20px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"></th></tr></thead>
        
        <tbody>
            <tr><td style="height: 30px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"><b><i>ARUS KAS DARI AKTIFITAS PENDANAAN</i></b></td></tr>
                <?php $dataKat = $model->getKategori(3);
                foreach($dataKat as $keyTable){ ?>
                    <tr>
                        <td style="padding-left: 3em;"><?php
                         echo $keyTable->nmrekening5; ?></td>
                        <td><?php 
                                if(($keyTable->issaldoawal)==FALSE){
                                    echo number_format($keyTable->saldo);
                                }else{
                                    echo 0;
                                }
                                ?>
                        </td>
                        <?php
                                foreach($datatableheader as $vth){
                                        echo"<td>".number_format($model->getSaldoPerRek($vth->ruangan_id,$keyTable->rekening5_id))."</td>";
                                }
                        $totalperrek1_dn += $keyTable->saldo;        
                        ?>            
                    </tr>
                <?php } ?>
            <tr>
                <td style="text-align: right; padding-right: 1em"><i><b>Kas Bersih Dari Aktifitas Pendanaan </b></i></td>
                <td><i><b><?php echo number_format($totalperrek1_dn); ?></b></i></td>
                <?php
                        foreach($datatableheader as $vth){
                                        echo"<td><i><b>".number_format($model->getKategori(3,null,$vth->ruangan_id))."</b></i></td>";
                                }
                ?>
            </tr>
        </tbody>
        <thead><tr><th style="height: 20px; vertical-align: middle;" colspan="<?php echo $jmlruang+2; ?>"></th></tr></thead>
        
        
        <tr>
                <td><b>KENAIKAN ( PENURUNAN ) KAS PERIODE INI</b></td>
                <td><b><i><?php echo number_format($total_a = $saldo_x + $saldo_y+$saldo_z );  ?></i></b></td>
                    <?php 
                            foreach($datatableheader as $vth){
                                    echo"<td><b><i>".number_format($model->getTotKas($vth->ruangan_id))."</td>";
                            }
                    ?>
            </tr>
            <tr>
                <td><b>SALDO KAS AWAL PERIODE</b></td>
                <td><b><i><?php echo number_format($saldo_c);  ?></i></b></td>
            </tr>
            <tr>
                    <td><b>SALDO KAS AKHIR PERIODE</b></td>
                    <td><b><i><?php echo number_format($total_c = $total_a + $saldo_c );  ?></i></b></td>
            </tr>
    </table>
    </div>
</div>