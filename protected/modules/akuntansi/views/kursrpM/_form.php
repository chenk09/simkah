<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'kursrp-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#AKKursrpM_matauang_id',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary($model); ?>

        <table>
            <tr>
                <td>
                      <div class="control-group">
                         <?php echo $form->labelEx($model,'matauang_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                                echo $form->dropDownList($model,'matauang_id', CHtml::listData($model->MataUangItems, 'matauang_id', 'matauang') ,
                                        array('style'=>'width:150px;','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); 
                            ?>
                        </div>
                    </div>
                    
                     <div class='control-group'>
                            <?php echo $form->labelEx($model,'tglkursrp', array('class'=>'control-label')) ?>
                            <div class="controls">
                                 <?php //$minDate = (Yii::app()->user->getState('tglpemakai')) ? '' : 'd'; ?>
                                 <?php 
                                     $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglkursrp',
                                                            'mode'=>'date',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
        //                                                        'minDate' => 'd',
                                                                'maxDate'=>$minDate,
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                      )); 
                                 ?>
                            </div>
                    </div> 
                    
                    <div class='control-group'>
                            <?php echo $form->labelEx($model,'nilai', array('class'=>'control-label')) ?>
                            <div class="controls">
                                    <?php echo $form->textField($model,'nilai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            </div>
                     </div> 
                    
                     <div class='control-group'>
                            <?php echo $form->labelEx($model,'rupiah', array('class'=>'control-label')) ?>
                            <div class="controls">
                                     <?php echo $form->textField($model,'rupiah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            </div>
                      </div>
                    
<!--                      <div class='control-group'>
                            <?php //echo $form->labelEx($model,'kursrp_aktif', array('class'=>'control-label')) ?>
                            <div class="controls">
                                    <?php //echo $form->checkBox($model,'kursrp_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            </div>
                       </div>                     -->
                </td>
            </tr>
        </table>
        
	<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/kursrpM/admin'),
                                    array('class'=>'btn btn-danger',
                                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('akuntansi.views.tips.tipsaddedit4b',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
	</div>

<?php $this->endWidget(); ?>
