<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'bank-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary($model); ?>

        <table>
            <tr>
                <td>
                   <div class='control-group'>
                                  <?php echo $form->labelEx($model,'bank_id', array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo $form->hiddenField($model,'bank_id',array('class'=>'span3','maxlength'=>50));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'namaBank',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/bank'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.namabank);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.namabank);
                                                                $("#' . CHtml::activeId($model, 'bank_id') . '").val(ui.item.bank_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Bank',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogBank',),
                                        ));
                                    ?>
                             </div>
                   </div>
                    
                   <div class='control-group'>
                                  <?php echo CHtml::label('Rekening Debit','rekening debit',array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][saldonormal]','D', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][rekening5_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][rekening4_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][rekening3_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][rekening2_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][1][rekening1_id]','', array('readonly'=>true));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'rekDebit',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.nmrincianobyek);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.nmrincianobyek);
                                                                $("#AKBankRekM_rekening_1_rekening5_id").val(ui.item.rincianobyek_id);
                                                                $("#AKBankRekM_rekening_1_rekening4_id").val(ui.item.obyek_id);
                                                                $("#AKBankRekM_rekening_1_rekening3_id").val(ui.item.jenis_id);
                                                                $("#AKBankRekM_rekening_1_rekening2_id").val(ui.item.kelompok_id);
                                                                $("#AKBankRekM_rekening_1_rekening1_id").val(ui.item.struktur_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Rekening',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogRekDebit',),
                                        ));
                                    ?>
                             </div>
                   </div>
                    
                   <div class='control-group'>
                                  <?php echo CHtml::label('Rekening Kredit','rekening kredit',array('class'=>'control-label')) ?>
                             <div class="controls">
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][saldonormal]','K', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][rekening5_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][rekening4_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][rekening3_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][rekening2_id]','', array('readonly'=>true));  ?>
                                 <?php echo CHtml::hiddenField('AKBankRekM[rekening][2][rekening1_id]','', array('readonly'=>true));  ?>
                                    <?php
                                        $this->widget('MyJuiAutoComplete', array(
                                            'model' => $model,
                                            'attribute' => 'rekKredit',
                                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi'),
                                            'options' => array(
                                                'showAnim' => 'fold',
                                                'minLength' => 2,
                                                'focus' => 'js:function( event, ui ) {
                                                        $(this).val(ui.item.nmrincianobyek);
                                                        return false;
                                                    }',
                                                'select' => 'js:function( event, ui ) {
                                                                $(this).val(ui.item.nmrincianobyek);
                                                                 $("#AKBankRekM_rekening_2_rekening5_id").val(ui.item.rincianobyek_id);
                                                                 $("#AKBankRekM_rekening_2_rekening4_id").val(ui.item.obyek_id);
                                                                 $("#AKBankRekM_rekening_2_rekening3_id").val(ui.item.jenis_id);
                                                                 $("#AKBankRekM_rekening_2_rekening2_id").val(ui.item.kelompok_id);
                                                                 $("#AKBankRekM_rekening_2_rekening1_id").val(ui.item.struktur_id);
                                                                    return false;
                                                              }'
                                            ),
                                            'htmlOptions' => array(
                                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'placeholder'=>'Ketikan Nama Rekening',
                                                'class'=>'span3',
                                                'style'=>'width:150px;',
                                            ),
                                            'tombolDialog' => array('idDialog' => 'dialogRekKredit',),
                                        ));
                                    ?>
                             </div>
                   </div>
                </td>
            </tr>
        </table>
        
	<div class="form-actions">
                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                            Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); 
                ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.jurnalRekPenerimaan.'/admin'), 
                            array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
                ?>       
                <?php $this->widget('TipsMasterData',array('type'=>'update'));?>
	</div>

<?php $this->endWidget(); ?>
<?php 
//========= Dialog buat cari data Rek Debit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekDebit',
    'options'=>array(
        'title'=>'Daftar Rekening Debit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>700,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekDebit = new RekeningakuntansiV('search');
$modRekDebit->unsetAttributes();
$account = "D";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekDebit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekDebit->searchAccounts($account),
	'filter'=>$modRekDebit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Saldo Normal</center>',
                'start'=>8, //indeks kolom 3
                'end'=>9, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Rek. 1',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Rek. 3',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Rek. 4',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Rek. 5',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'value'=>'$data->rincianobyek_nb',
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKBankRekM_rekening_1_rekening5_id\").val(\"$data->rincianobyek_id\");
                                                $(\"#AKBankRekM_rekening_1_rekening4_id\").val(\"$data->obyek_id\");
                                                $(\"#AKBankRekM_rekening_1_rekening3_id\").val(\"$data->jenis_id\");
                                                $(\"#AKBankRekM_rekening_1_rekening2_id\").val(\"$data->kelompok_id\");
                                                $(\"#AKBankRekM_rekening_1_rekening1_id\").val(\"$data->struktur_id\");
                                                $(\"#AKBankRekM_rekDebit\").val(\"$data->nmrincianobyek\");                                                
                                                $(\"#dialogRekDebit\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Debit dialog =============================
?>
        
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRekKredit',
    'options'=>array(
        'title'=>'Daftar Rekening Kredit',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modRekKredit = new RekeningakuntansiV('search');
$modRekKredit->unsetAttributes();
$account = "K";
if(isset($_GET['RekeningakuntansiV'])) {
    $modRekKredit->attributes = $_GET['RekeningakuntansiV'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modRekKredit->searchAccounts($account),
	'filter'=>$modRekKredit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kode Rekening</center>',
                'start'=>1, //indeks kolom 3
                'end'=>5, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$data->nourutrek',
                ),
                array(
                    'header'=>'Rek. 1',
                    'value'=>'$data->kdstruktur',
                ),
                array(
                    'header'=>'Rek. 2',
                    'value'=>'$data->kdkelompok',
                ),
                array(
                    'header'=>'Rek. 3',
                    'value'=>'$data->kdjenis',
                ),
                array(
                    'header'=>'Rek. 4',
                    'value'=>'$data->kdobyek',
                ),
                array(
                    'header'=>'Rek. 5',
                    'value'=>'$data->kdrincianobyek',
                ),
                array(
                    'header'=>'Nama Rekening',
                    'value'=>'$data->nmrincianobyek',
                ),
                array(
                    'header'=>'Nama Lain',
                    'value'=>'$data->nmrincianobyeklain',
                ),
                array(
                    'header'=>'Saldo Normal',
                    'value'=>'$data->rincianobyek_nb',
                ),
            
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                                $(\"#AKBankRekM_rekening_2_rekening5_id\").val(\"$data->rincianobyek_id\");
                                                $(\"#AKBankRekM_rekening_2_rekening4_id\").val(\"$data->obyek_id\");
                                                $(\"#AKBankRekM_rekening_2_rekening3_id\").val(\"$data->jenis_id\");
                                                $(\"#AKBankRekM_rekening_2_rekening2_id\").val(\"$data->kelompok_id\");
                                                $(\"#AKBankRekM_rekening_2_rekening1_id\").val(\"$data->struktur_id\");
                                                $(\"#AKBankRekM_rekKredit\").val(\"$data->nmrincianobyek\");
                                                $(\"#dialogRekKredit\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>

        <?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogBank',
    'options'=>array(
        'title'=>'Daftar Bank',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modBank = new BankM('search');
$modBank->unsetAttributes();
if(isset($_GET['BankM'])) {
    $modBank->attributes = $_GET['BankM'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'rekdebit-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modBank->searchBank(),
	'filter'=>$modBank,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'No Urut',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Nama Bank ',
                    'value'=>'$data->namabank',
                ),
                array(
                    'header'=>'Mata Uang',
                    'value'=>'$data->matauang->matauang',
                ),
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBank",
                                    "onClick" =>"
                                                $(\"#AKBankRekM_bank_id\").val(\"$data->bank_id\");
                                                $(\"#AKBankRekM_namaBank\").val(\"$data->namabank\");
                                                $(\"#dialogBank\").dialog(\"close\");    
                                                return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>
