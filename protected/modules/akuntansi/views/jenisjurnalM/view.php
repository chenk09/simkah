<?php
    $this->breadcrumbs=array(
            'Jenisjurnal Ms'=>array('index'),
            $model->jenisjurnal_id,
    );

    $arrMenu = array();
                    array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jenis Jurnal ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                    (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jenis Jurnal', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

    $this->menu=$arrMenu;

    $this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'jenisjurnal_id',
		'jenisjurnal_nama',
		'jenisjurnal_namalain',
		array(            
                    'label'=>'Aktif',
                    'type'=>'raw',
                    'value'=>(($model->jenisjurnal_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>