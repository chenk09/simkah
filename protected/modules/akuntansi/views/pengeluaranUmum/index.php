<fieldset id ="input-pengeluaran">
<?php
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp. ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    ));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'akpengeluaran-umum-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <legend class="rim2">Transaksi Pengeluaran Kas</legend>
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php //echo $form->errorSummary(array($modPengUmum,$modBuktiKeluar)); ?>
    <table>
        <tr>
            <td width="40%" align="right">
                <div class="control-group ">
                    <?php $modPengUmum->tglpengeluaran = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPengUmum->tglpengeluaran, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modPengUmum,'tglpengeluaran', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                            'model'=>$modPengUmum,
                                            'attribute'=>'tglpengeluaran',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('class'=>'dtPicker2-5 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                                )); 
                        ?>

                    </div>
                </div>
                <?php echo $form->textFieldRow($modPengUmum,'nopengeluaran',array('class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                <?php echo $form->dropDownListRow($modPengUmum,'kelompoktransaksi',KelompokTransaksi::items(),array('class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo $form->hiddenField($modPengUmum,'jenispengeluaran_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->hiddenField($modPengUmum,'is_posting',array('readonly'=>true)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPengUmum,'jenispengeluaran_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPengUmum,
                                    'attribute'=>'jenisKodeNama',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/jenisPengeluaran').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                               $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#AKPengeluaranumumT_jenispengeluaran_id").val(ui.item.jenispengeluaran_id);
                                                getDataRekeningPengeluaran(ui.item.jenispenerimaan_id);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('placeholder'=>'ketik Nama Jenis Pengeluaran','class'=>'reqForm'),
                                    'tombolDialog' => array('idDialog' => 'dialogJenisPengeluaran',),
                            )); 
                        ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modPengUmum,'volume',array('onblur'=>'hitungTotalHarga()','class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->dropDownListRow($modPengUmum,'satuanvol', SatuanUmum::items(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
<!--                <div class="control-group ">
                    <?php // echo $form->labelEx($modPengUmum,'volume', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php // echo $form->textField($modPengUmum,'volume',array('onblur'=>'hitungTotalHarga()','class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php // echo $form->dropDownList($modPengUmum,'satuanvol', SatuanUmum::items(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    </div>
                </div>-->
                <?php echo $form->hiddenField($modPengUmum,'volume',array('value'=>'1', 'readonly'=>true)); ?>
                <?php echo $form->hiddenField($modPengUmum,'satuanvol',array('value'=>'KALI', 'readonly'=>true)); ?>
                    
                    <?php echo $form->hiddenField($modBuktiKeluar,'biayaadministrasi',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPengUmum,'hargasatuan',array('onblur'=>'hitungTotalHarga()','class'=>'inputFormTabel currency reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modPengUmum,'totalharga',array('readonly'=>true,'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                
            </td>
            <td width="40%" align="right">
                <?php echo $form->textAreaRow($modPengUmum,'keterangankeluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                 <div class="control-group ">
                <?php echo $form->labelEx($modPengUmum, 'ruangan_id', array('class' => 'control-label')); ?>
                      <div class="controls">
                        <?php
                        echo $form->dropDownList($modPengUmum, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                          'ajax' => array('type' => 'POST',
                              'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $modPengUmum->getNamaModel() . '')),
                              'update' => '#' . CHtml::activeId($modPengUmum, 'ruangan_id') . ''),));
                        ?>
                        
                
                        <?php echo $form->dropDownList($modPengUmum, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                        <?php echo $form->error($modPengUmum, 'ruangan_id'); ?>
                </div>
                </div>
                   
                <?php //echo $form->dropDownListRow($modPengUmum,'ruangan_id',CHtml::listData(RuanganpegawaiM::model()->findAll(), 'ruangan_id', 'ruangan.ruangan_nama'),array('class'=>'span3','maxlength'=>50, 'empty'=>'-- Pilih --')); ?>
                <?php //echo $form->textFieldRow($modPengUmum,'namapenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php //echo $form->textFieldRow($modPengUmum,'nippenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php //echo $form->textFieldRow($modPengUmum,'jabatanpenandatangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php //echo $form->dropDownListRow($modPengUmum,'penjamin_id', CHtml::listData($modPengUmum->getPenjaminItems(1), 'penjamin_id', 'penjamin_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    
<!--  <legend class="accord1" style="width:460px;">
        <?php // echo $form->checkBox($modPengUmum, 'isurainkeluarumum', array('checked'=>true,'onchange'=>'bukaUraian(this)','onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        Pilih Jika Transaksi Ada Uraiannya
</legend>
    <div id="div_tblInputUraian">
    <table id="tblInputUraian" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>
                    Uraian
                </th>
                <th>Volume</th>
                <th>Satuan</th>
                <th>Harga</th>
                <th>Total</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php // $this->renderPartial('_rowUraian',array('form'=>$form,'modUraian'=>$modUraian)); ?>
        </tbody>
    </table>
    </div>-->

<fieldset>
    <legend class="rim">Jurnal Rekening</legend>
    <table>
        <tr>
            <td width="70%">
                <div>
                    <?php
                        $this->renderPartial('akuntansi.views.penerimaanUmum._rowListRekening',
                            array(
                                'form'=>$form,
                            )
                        );
                    ?>
                </div>                
            </td>
            <td width="15%">
                <div class="control-group ">
                    <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modBuktiKeluar,'tglkaskeluar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modBuktiKeluar,
                                                'attribute'=>'tglkaskeluar',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>

                    </div>
                </div>
                <?php // echo $form->dropDownListRow($modBuktiKeluar,'tahun', Params::tahun(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
                <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php echo $form->textFieldRow($modBuktiKeluar,'nokaskeluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                <?php echo $form->textFieldRow($modBuktiKeluar,'jmlkaskeluar',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modBuktiKeluar,'carabayarkeluar', CaraBayarKeluar::items(),array('onchange'=>'formCarabayar(this.value)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <div id="divCaraBayarTransfer" class="hide">
                    <?php echo $form->textFieldRow($modBuktiKeluar,'melalubank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modBuktiKeluar,'denganrekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modBuktiKeluar,'atasnamarekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>
                
            </td>
            <td width="15%">
                &nbsp;
                <?php echo $form->hiddenField($modBuktiKeluar,'namapenerima',array('value'=>'-', 'class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->hiddenField($modBuktiKeluar,'alamatpenerima',array('value'=>'-','class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->hiddenField($modBuktiKeluar,'untukpembayaran',array('value'=>'-', 'class'=>'span3 reqForm', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
        </tr>
    </table>
</fieldset>      
    <div class="form-actions">
        <div style="float:left;margin-right:6px;">
            <?php
                $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
                $urlSave=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/index');
                
                $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'=>'primary',
                    'buttons'=>array(
                        array(
                            'label'=>'Simpan',
                            'icon'=>'icon-ok icon-white',
                            'url'=>"#",
                            'htmlOptions'=>
                                array(
                                    'onclick'=>'simpanPengeluaran(\'jurnal\');return false;',
                                )
                       ),
                        array(
                            'label'=>'',
                            'items'=>array(
                                array(
                                    'label'=>'Posting',
                                    'icon'=>'icon-ok',
                                    'url'=>"#",
                                    'itemOptions' => array(
                                        'onclick'=>'simpanPengeluaran(\'posting\');return false;'
                                    )
                                ),
                            )
                        ),
                    ),
                ));
                echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), array('style'=>'display:none','id' => 'reseter', 'class'=>'btn btn-danger', 'type'=>'reset'));
             ?>
        </div>
            <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKasir($('#FAPendaftaranT_pendaftaran_id').val());return false",'disabled'=>false)); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                    Yii::app()->createUrl($this->module->id.'/'.pengeluaranUmum.'/index'), 
                    array('class'=>'btn btn-danger',
                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
<?php  
    $content = $this->renderPartial('../tips/transaksi',array(),true);
//    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>
<?php $this->endWidget(); ?>
</fieldset>

<script type="text/javascript">
var trUraian=new String(<?php echo CJSON::encode($this->renderPartial('_rowUraian',array('form'=>$form,'modUraian'=>array(0=>$modUraian[0]),'removeButton'=>true),true));?>);

function simpanPengeluaran(params)
{
    if(params=='posting'){
        $('#AKPengeluaranumumT_is_posting').val(true);
    }else{
        $('#AKPengeluaranumumT_is_posting').val(false);
    }
    
    jenis_simpan = params;
    var kosong = "" ;
    var dataKosong = $("#input-pengeluaran").find(".reqForm[value="+ kosong +"]");
//    if(dataKosong.length > 0){
//        alert('Bagian dengan tanda * harus diisi ');
//    }else{
//        var detail = 0;
//        $('#tblInputUraian tbody tr').each(
//            function(){
//                detail++;
//            }
//        );
        var detail = 1;
        if(detail > 0){
            $('.currency').each(
                function(){
                    this.value = unformatNumber(this.value)
                }
            );
            $('#akpengeluaran-umum-t-form').submit();
            /*MENGGUNAKAN METHOD POST PHP
            $.post('<?php // echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/SimpanPengeluaran');?>', {jenis_simpan:jenis_simpan, data:$('#akpengeluaran-umum-t-form').serialize()},
                function(data){
                    if(data.status == 'ok')
                    {
                        if(data.action == 'insert')
                        {
                            alert("Simpan data berhasil");
                            $("#tblInputUraian").find('tr[class$="child"]').detach();
                            $("#reseter").click();
                            $("#input-pengeluaran").find("input[name$='[nopengeluaran]']").val(data.pesan.nopengeluaran);
                            $("#input-pengeluaran").find("input[name$='[nokaskeluar]']").val(data.pesan.nokaskeluar);
                            $("#tblInputRekening > tbody").find('tr').detach();
                        }else{
                            alert("Update data berhasil");
                        }
                    }
            }, "json");
                */
        }else{
            alert('Detail uraian masih kosong');
        }
         
//    }
    
       
}

function cekInput()
{
    var harga = 0; var totharga = 0;
    if($('#AKPengeluaranumumT_isuraintransaksi').is(':checked')){    
        $('#tblInputUraian').find('input[name$="[hargasatuan]"]').each(function(){
            harga = harga + unformatNumber(this.value);
        });
        $('#tblInputUraian').find('input[name$="[totalharga]"]').each(function(){
            totharga = totharga + unformatNumber(this.value);
        });
        
        //if(harga != unformatNumber($('#AKPengeluaranumumT_hargasatuan').val())){
        //    alert('Harga tidak sesuai');return false;
        //}
        if(totharga != unformatNumber($('#AKPengeluaranumumT_totalharga').val())){
            alert('Harga Uraian tidak sesuai');return false;
        }
    }
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    
    return true;
}

function hitungTotalUraian(obj)
{
    var volume = unformatNumber($(obj).parents('tr').find('input[name$="[volume]"]').val());
    var hargasatuan = unformatNumber($(obj).parents('tr').find('input[name$="[hargasatuan]"]').val());
    
    $(obj).parents('tr').find('input[name$="[totalharga]"]').val(formatNumber(volume*hargasatuan));
}

function hitungTotalHarga()
{
    var biayaAdministrasi = unformatNumber($('#AKTandabuktikeluarT_biayaadministrasi').val());
    var vol = unformatNumber($('#AKPengeluaranumumT_volume').val());
    var harga = unformatNumber($('#AKPengeluaranumumT_hargasatuan').val());
    
    $('#AKPengeluaranumumT_totalharga').val(formatNumber(vol*harga));
    $('#AKTandabuktikeluarT_jmlkaskeluar').val(formatNumber(vol*harga+biayaAdministrasi));
    
    <?php foreach ($modUraian as $i => $uraian) { ?>
//            if ($('#RekeningakuntansiV_$i_saldonormal') == 'D') 
//            {
                  $('#RekeningakuntansiV_0_saldodebit').val(formatNumber(vol*harga+biayaAdministrasi));
//              }
//              elseif ($('#RekeningakuntansiV_$i_saldonormal') == 'K') 
//            {
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber(vol*harga+biayaAdministrasi));
//              }
   
   
    <?php } ?>
}

function hitungJmlBayar()
{
    var biayaAdministrasi = unformatNumber($('#AKTandabuktikeluarT_biayaadministrasi').val());
    var totBayar = 0;
    var totHarga = unformatNumber($('#AKPengeluaranumumT_totalharga').val());
    
    totBayar = totHarga + biayaAdministrasi;
    
    $('#AKTandabuktikeluarT_jmlkaskeluar').val(formatNumber(totBayar));
     $('#RekeningakuntansiV_0_saldodebit').val(formatNumber(totBayar));
//            
                 $('#RekeningakuntansiV_1_saldokredit').val(formatNumber(totBayar));
}

function bukaUraian(obj)
{
    if($(obj).is(':checked')){
        $('#div_tblInputUraian').slideDown();
    } else {
        $('#div_tblInputUraian').slideUp();
    }
}
/*
function bukaUraian(obj)
{
    if($(obj).is(':checked')){
        $('#tblInputUraian').children('tbody').slideDown();
    } else {
        $('#tblInputUraian').children('tbody').slideUp();
    }
}
*/
function addRowUraian(obj)
{
    $(obj).parents('table').children('tbody').append(trUraian.replace());
        
    renameInput('AKUraiankeluarumumT','uraiantransaksi');
    renameInput('AKUraiankeluarumumT','volume');
    renameInput('AKUraiankeluarumumT','satuanvol');
    renameInput('AKUraiankeluarumumT','hargasatuan');
    renameInput('AKUraiankeluarumumT','totalharga');
    jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
    maskMoneyInput($('#tblInputUraian > tbody > tr:last'));
}
 
function batalUraian(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan Uraian?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
        renameInput('AKUraiankeluarumumT','uraiantransaksi');
        renameInput('AKUraiankeluarumumT','volume');
        renameInput('AKUraiankeluarumumT','satuanvol');
        renameInput('AKUraiankeluarumumT','hargasatuan');
        renameInput('AKUraiankeluarumumT','totalharga');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblInputUraian tr').length;
    var i = -1;
    $('#tblInputUraian tr').each(function(){
        if($(this).has('input[name$="[uraiantransaksi]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function formCarabayar(carabayar)
{
    //alert(carabayar);
    if(carabayar == 'TRANSFER'){
        $('#divCaraBayarTransfer').slideDown();
    } else {
        $('#divCaraBayarTransfer').slideUp();
    }
}

function unMaskMoneyInput(tr)
{
    $(tr).find('input.currency:text').unmaskMoney();
}

function maskMoneyInput(tr)
{
    $(tr).find('input.currency:text').maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
}
</script>
      
<?php 
//========= Dialog buat cari data Rek Kredit =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogJenisPengeluaran',
    'options'=>array(
        'title'=>'Daftar Jenis Pengeluaran',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>400,
        'resizable'=>false,
    ),
));

$modJenisPengeluaran = new JenispengeluaranM('search');
$modJenisPengeluaran->unsetAttributes();
if(isset($_GET['JenispengeluaranM'])) {
    $modJenisPengeluaran->attributes = $_GET['JenispengeluaranM'];
}
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'jenispengeluaran-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modJenisPengeluaran->searchJnsPengeluaranInRek(),
	'filter'=>$modJenisPengeluaran,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'No',
                    'value'=>'$this->grid->dataProvider->Pagination->CurrentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                ),
                array(
                    'header'=>'Jenis Pengeluaran',
                    'name'=>'jenispengeluaran_nama',
                    'value'=>'$data->jenispengeluaran_nama',
                ),
                array(
                    'header'=>'Nama Lain',
                    'name'=>'jenispengeluaran_namalain',
                    'value'=>'$data->jenispengeluaran_namalain',
                ),
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectRekDebit",
                                    "onClick" =>"
                                        getDataRekeningPengeluaran($data->jenispengeluaran_id);
                                        $(\"#AKPengeluaranumumT_jenispengeluaran_id\").val(\"$data->jenispengeluaran_id\");
                                        $(\"#AKPengeluaranumumT_jenisKodeNama\").val(\"$data->jenispengeluaran_nama\");
                                        $(\"#dialogJenisPengeluaran\").dialog(\"close\");    
                                        return false;
                            "))',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Rek Kredit dialog =============================
?>