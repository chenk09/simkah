<?php
class AKRincianpengeluarankasrekeningV extends RincianpengeluarankasrekeningV
{
        public $tglAwal,$tglAkhir;
        public $saldodebit,$saldokredit;
        public $jenisNama;
	
        public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         * digunakan di:
         * - akuntansi/actionAjax/GetRekeningPengeluaranKas
         */
        public function criteriaFunction(){
            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglkaskeluar',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(nokaskeluar)',strtolower($this->nokaskeluar),true);
            $criteria->compare('jenis_id',$this->jenis_id);

            $criteria->limit = 500; //batas maksimal data
            return $criteria;
        }
        
        public function searchInformasi()
	{
            $criteria=new $this->criteriaFunction();

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
	}
}
?>