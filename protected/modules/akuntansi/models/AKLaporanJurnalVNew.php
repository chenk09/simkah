<?php
class AKLaporanJurnalVNew extends LaporanjurnalV
{
    public $data;
    public $jumlah;
    public $tick;
    
     public static function model($className = __CLASS__) {
        parent::model($className);
    }
    
    public function searchTable()
    {   
                $criteria=new CDbCriteria;
                $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kode_rekening)',strtolower($this->kode_rekening),true);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
//		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
                if(!empty($this->jenisjurnal_id)){
			$criteria->addCondition("jenisjurnal_id = ".$this->jenisjurnal_id);			
		}
                if(!empty($this->ruangan_id)){
			$criteria->addCondition("ruangan_id = ".$this->ruangan_id);			
		}
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
//		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
                
                $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);

                 return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                    ));
    }
    
    public function searchPrint()
    {
                $criteria=new CDbCriteria;
                $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kode_rekening)',strtolower($this->kode_rekening),true);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
//		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
                if(!empty($this->jenisjurnal_id)){
			$criteria->addCondition("jenisjurnal_id = ".$this->jenisjurnal_id);			
		}
                if(!empty($this->ruangan_id)){
			$criteria->addCondition("ruangan_id = ".$this->ruangan_id);			
		}
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
//		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
                
                $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);

                 return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                            'pagination'=>false,
                    ));
    }
    
    public function searchGrafik(){
                $criteria = MyFunction::criteriaGrafikJurnal($this,'data', array('tick'=>'jenisjurnal_nama'));
                
                $criteria->order = 'jenisjurnal_nama';
                $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
		$criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
		$criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
		$criteria->compare('LOWER(kode_rekening)',strtolower($this->kode_rekening),true);
		$criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
		$criteria->compare('saldodebit',$this->saldodebit);
		$criteria->compare('saldokredit',$this->saldokredit);
		$criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
//		$criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
                if(!empty($this->jenisjurnal_id)){
			$criteria->addCondition("jenisjurnal_id = ".$this->jenisjurnal_id);			
		}
                if(!empty($this->ruangan_id)){
			$criteria->addCondition("ruangan_id = ".$this->ruangan_id);			
		}
		$criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
//		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
                
                $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);
                
                return new CActiveDataProvider($this, array(
                           'criteria'=>$criteria,
                   ));
    }
    
}