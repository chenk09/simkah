<?php
class AKPelayananrekM extends PelayananrekM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PelayananrekM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         * digunakan di:
         * - protected/modules/akuntansi/views/pelayananRekening/admin.php
         * - protected/modules/akuntansi/views/pelayananRekening/Print.php
         * @return \CActiveDataProvider
         */
        public function searchPelayanan()
        {

            $criteria=new CDbCriteria;
            $criteria->select = 't.daftartindakan_id, t.jnspelayanan, t.komponentarif_id';
            $criteria->join = "
                LEFT JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = t.daftartindakan_id
                LEFT JOIN komponentarif_m ON komponentarif_m.komponentarif_id = t.komponentarif_id
            ";
//            $criteria->compare('pelayananrek_id',$this->pelayananrek_id);
//            $criteria->compare('rekening3_id',$this->rekening3_id);
//            $criteria->compare('rekening4_id',$this->rekening4_id);
//            $criteria->compare('rekening2_id',$this->rekening2_id);
//            $criteria->compare('rekening5_id',$this->rekening5_id);
//            $criteria->compare('rekening1_id',$this->rekening1_id);
            $criteria->compare('LOWER(daftartindakan_m.daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
            $criteria->compare('LOWER(saldonormal)',strtolower($this->saldonormal),true);
            $criteria->compare('LOWER(jnspelayanan)',strtolower($this->jnspelayanan),true);
            $criteria->compare('LOWER(komponentarif_m.komponentarif_nama)',strtolower($this->komponentarif_nama),true);
            $criteria->group = 't.daftartindakan_id, jnspelayanan, t.komponentarif_id'; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
}