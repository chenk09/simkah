<?php

class AKRekeningakuntansiV extends RekeningakuntansiV
{
    /**
    * Returns the static model of the specified AR class.
    * @param string $className active record class name.
    * @return AnamnesaT the static model class
    */
    public $rekening1_id;
    public $id_temp_rek;
    public $pilihRekening;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getInfoRekening()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('rincianobyek_id',$this->rincianobyek_id);
        $criteria->compare('obyek_id',$this->obyek_id);
        $criteria->compare('jenis_id',$this->jenis_id);
        $criteria->order = 'struktur_id, kelompok_id, jenis_id, obyek_id, rincianobyek_id, nourutrek';
        $result = AKRekeningakuntansiV::model()->find($criteria);
        return $result;
    }
    
    public function cariRekening()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
        $criteria->compare('LOWER(nmrincianobyeklain)',strtolower($this->nmrincianobyeklain),true);
//        $criteria->addCondition('kdrincianobyek IS NOT NULL');
        $criteria->order = 'struktur_id, kelompok_id, jenis_id, obyek_id, rincianobyek_id, nourutrek';
        return new CActiveDataProvider($this,
            array(
                'criteria'=>$criteria,
            )
        );
    }
    
    public function getKodeRekening()
    {
        if(isset($this->rincianobyek_id))
        {
            $kode_rekening = $this->kdstruktur . "-" . $this->kdkelompok . "-" . $this->kdjenis . "-" . $this->kdobyek . "-" . $this->kdrincianobyek;
        }else{
            if(isset($this->obyek_id))
            {
                $kode_rekening = $this->kdstruktur . "-" . $this->kdkelompok . "-" . $this->kdjenis . "-" . $this->kdobyek;
            }else{
                $kode_rekening = $this->kdstruktur . "-" . $this->kdkelompok . "-" . $this->kdjenis;
            }
        }
        
        return $kode_rekening;
    }
    
    public function getNamaRekening()
    {
        if(isset($this->rincianobyek_id))
        {
            $kode_rekening = $this->nmrincianobyek;
        }else{
            if(isset($this->obyek_id))
            {
                $kode_rekening = $this->nmobyek;
            }else{
                $kode_rekening = $this->nmjenis;
            }
        }
        
        return $kode_rekening;
    }
    
    public function getIdRekening()
    {
        if(isset($this->rincianobyek_id))
        {
            $kode_rekening = $this->rincianobyek_id;
        }else{
            if(isset($this->obyek_id))
            {
                $kode_rekening = $this->obyek_id;
            }else{
                $kode_rekening = $this->jenis_id;
            }
        }
        return $kode_rekening;
    }
    
    public function searchByFilter()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        
        $criteria->compare('struktur_id',$this->struktur_id);
        $criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur));
        $criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
        $criteria->compare('LOWER(nmstrukturlain)',strtolower($this->nmstrukturlain),true);
        $criteria->compare('LOWER(struktur_nb)',strtolower($this->struktur_nb),true);
        $criteria->compare('struktur_aktif',$this->struktur_aktif);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
        $criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
        $criteria->compare('LOWER(nmkelompoklain)',strtolower($this->nmkelompoklain),true);
        $criteria->compare('LOWER(kelompok_nb)',strtolower($this->kelompok_nb),true);
        $criteria->compare('kelompok_aktif',$this->kelompok_aktif);
        $criteria->compare('jenis_id',$this->jenis_id);
        $criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
        $criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
        $criteria->compare('LOWER(nmjenislain)',strtolower($this->nmjenislain),true);
        $criteria->compare('LOWER(jenis_nb)',strtolower($this->jenis_nb),true);
        $criteria->compare('jenis_aktif',$this->jenis_aktif);
        $criteria->compare('obyek_id',$this->obyek_id);
        $criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
        $criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
        $criteria->compare('LOWER(nmobyeklain)',strtolower($this->nmobyeklain),true);
        $criteria->compare('LOWER(obyek_nb)',strtolower($this->obyek_nb),true);
        $criteria->compare('obyek_aktif',$this->obyek_aktif);
        $criteria->compare('rincianobyek_id',$this->rincianobyek_id);
        $criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
        $criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
        $criteria->compare('LOWER(nmrincianobyeklain)',strtolower($this->nmrincianobyeklain),true);
        $criteria->compare('LOWER(rincianobyek_nb)',strtolower($this->rincianobyek_nb),true);
        $criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
        $criteria->compare('nourutrek',$this->nourutrek);
        $criteria->compare('rincianobyek_aktif',$this->rincianobyek_aktif);
        $criteria->compare('LOWER(kelompokrek)',strtolower($this->kelompokrek),true);
        $criteria->compare('sak',$this->sak);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
//        $condition = 'rincianobyek_id IS NOT NULL';
//        $criteria->addCondition($condition);
        $criteria->order = 'struktur_id, kelompok_id, jenis_id, obyek_id, nourutrek';
        
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }    
    public function searchByFilterPrint()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        
        $criteria->compare('struktur_id',$this->struktur_id);
        $criteria->compare('LOWER(kdstruktur)',strtolower($this->kdstruktur),true);
        $criteria->compare('LOWER(nmstruktur)',strtolower($this->nmstruktur),true);
        $criteria->compare('LOWER(nmstrukturlain)',strtolower($this->nmstrukturlain),true);
        $criteria->compare('LOWER(struktur_nb)',strtolower($this->struktur_nb),true);
        $criteria->compare('struktur_aktif',$this->struktur_aktif);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('LOWER(kdkelompok)',strtolower($this->kdkelompok),true);
        $criteria->compare('LOWER(nmkelompok)',strtolower($this->nmkelompok),true);
        $criteria->compare('LOWER(nmkelompoklain)',strtolower($this->nmkelompoklain),true);
        $criteria->compare('LOWER(kelompok_nb)',strtolower($this->kelompok_nb),true);
        $criteria->compare('kelompok_aktif',$this->kelompok_aktif);
        $criteria->compare('jenis_id',$this->jenis_id);
        $criteria->compare('LOWER(kdjenis)',strtolower($this->kdjenis),true);
        $criteria->compare('LOWER(nmjenis)',strtolower($this->nmjenis),true);
        $criteria->compare('LOWER(nmjenislain)',strtolower($this->nmjenislain),true);
        $criteria->compare('LOWER(jenis_nb)',strtolower($this->jenis_nb),true);
        $criteria->compare('jenis_aktif',$this->jenis_aktif);
        $criteria->compare('obyek_id',$this->obyek_id);
        $criteria->compare('LOWER(kdobyek)',strtolower($this->kdobyek),true);
        $criteria->compare('LOWER(nmobyek)',strtolower($this->nmobyek),true);
        $criteria->compare('LOWER(nmobyeklain)',strtolower($this->nmobyeklain),true);
        $criteria->compare('LOWER(obyek_nb)',strtolower($this->obyek_nb),true);
        $criteria->compare('obyek_aktif',$this->obyek_aktif);
        $criteria->compare('rincianobyek_id',$this->rincianobyek_id);
        $criteria->compare('LOWER(kdrincianobyek)',strtolower($this->kdrincianobyek),true);
        $criteria->compare('LOWER(nmrincianobyek)',strtolower($this->nmrincianobyek),true);
        $criteria->compare('LOWER(nmrincianobyeklain)',strtolower($this->nmrincianobyeklain),true);
        $criteria->compare('LOWER(rincianobyek_nb)',strtolower($this->rincianobyek_nb),true);
        $criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
        $criteria->compare('nourutrek',$this->nourutrek);
        $criteria->compare('rincianobyek_aktif',$this->rincianobyek_aktif);
        $criteria->compare('LOWER(kelompokrek)',strtolower($this->kelompokrek),true);
        $criteria->compare('sak',$this->sak);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
//        $condition = 'rincianobyek_id IS NOT NULL';
//        $criteria->addCondition($condition);
        $criteria->limit = -1;
        $criteria->order = 'struktur_id, kelompok_id, jenis_id, obyek_id, nourutrek';
        
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }    

    protected function afterFind(){
        if(isset($this->rincianobyek_id))
        {
            $this->id_temp_rek = 'rincianobyek_id' . 'x' . $this->rincianobyek_id;
        }else{
            if(isset($this->obyek_id))
            {
                $this->id_temp_rek = 'obyek_id' . 'x' . $this->obyek_id;
            }else{
                $this->id_temp_rek = 'jenis_id' . 'x' . $this->jenis_id;
            }
        }
        return true;
    }
    
}
?>
