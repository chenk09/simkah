<?php

/**
 * This is the model class for table "laporanperubahanmodal_v".
 *
 * The followings are the available columns in table 'laporanperubahanmodal_v':
 * @property integer $rekperiod_id
 * @property string $perideawal
 * @property string $sampaidgn
 * @property string $deskripsi
 * @property integer $rekening1_id
 * @property string $kdrekening1
 * @property string $nmrekening1
 * @property integer $rekening2_id
 * @property string $kdrekening2
 * @property string $nmrekening3
 * @property double $saldo
 * @property double $saldokredit
 * @property boolean $issaldoawal
 */
class AKLaporanperubahanmodalV extends LaporanperubahanmodalV
{
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return LaporanperubahanmodalV the static model class
         */
        public $tglAwal, $tglAkhir;

        public static function model($className=__CLASS__)
        {
                return parent::model($className);
        }
        
        public function getPerRuang($ruangId=null,$modalAwal=FALSE){
            
                if($modalAwal===TRUE){
                        $criteriaPerRuang=new CDbCriteria;
                        $criteriaPerRuang->group = "ruangan_id, rekening1_id, issaldoawal";
                        $criteriaPerRuang->select = "ruangan_id, sum(saldo) as saldo, issaldoawal, rekening1_id";
                        $criteriaPerRuang->compare('ruangan_id',$ruangId);
                        $conditionPerRuang = "nmrekening3 not like '%%Pembagian Deviden%%'";
                        $criteriaPerRuang->addCondition($conditionPerRuang);
                        $criteriaPerRuang->compare('LOWER(perideawal)',strtolower($periode['tglawal']),true);
                        $criteriaPerRuang->compare('LOWER(sampaidgn)',strtolower($periode['tglakhir']),true);
                        $dataPerRuang = LaporanperubahanmodalV::model()->findAll($criteriaPerRuang);
                        $saldo_a = 0; $saldokredit_a = 0;
                        $saldo_b = 0; $saldokredit_b = 0;
                        $saldo_d = 0; $saldokredit_d = 0;
                        $saldo_e = 0; $saldokredit_e = 0;
                        $saldo_g = 0; $saldokredit_g = 0;
                        
                        foreach ($dataPerRuang as $key => $value) 
                        { 
                          $rekening[$key]     = $value['rekening1_id'];
                          $saldo[$key]   = $value['saldo'];
                          $issaldoawal          = $value['issaldoawal'];

                          if($issaldoawal==TRUE){ 
                            $saldo_a     += $saldo[$key];
                          }else{ 
                            if($rekening[$key]==10){
                              $saldo_b     += $saldo[$key];
                            }elseif($rekening[$key]==3){
                              $saldo_d     += $saldo[$key];
                            }elseif($rekening[$key]==4){
                              $saldo_e     += $saldo[$key];
                            }elseif($rekening[$key]==11){
                              $saldo_g     += $saldo[$key];
                            }
                          }
                        }
                        $saldo = array();
                        $saldo[0] = $saldo_a;
                        $saldo[1] = $saldo_b;
                        $saldo[2] = $saldo_d;
                        $saldo[3] = $saldo_e;
                        $saldo[4] = $saldokredit_g;
                        return $saldo;
                        
                }else{
                        $criteriaPerRuang=new CDbCriteria;
                        $criteriaPerRuang->group = 'ruangan_id, ruangan_nama';
                        $criteriaPerRuang->select = $criteriaPerRuang->group;
                        $dataPerRuang = LaporanperubahanmodalV::model()->findAll($criteriaPerRuang);
                        return $dataPerRuang;
                }
        }
        
        public function getPemDev($ruangId=null){
            
                $criteriaPerRuang=new CDbCriteria;
                $criteriaPerRuang->group = "ruangan_id, rekening1_id, issaldoawal";
                $criteriaPerRuang->select = "ruangan_id, sum(saldo) as saldo, issaldoawal, rekening1_id";
                $criteriaPerRuang->compare('ruangan_id',$ruangId);
                $conditionPerRuang = "nmrekening3 like '%%Pembagian Deviden%%'";
                $criteriaPerRuang->addCondition($conditionPerRuang);
                $dataPerRuang = LaporanperubahanmodalV::model()->findAll($criteriaPerRuang);
                $saldo_f = 0; //$saldokredit_f = 0;
                        foreach ($dataPerRuang as $key => $value2) {
                        $saldo[$key]   = $value2['saldo'];
                        $issaldoawal        = $value2['issaldoawal'];
                                if($issaldoawal==false){
                                  $saldo_f     += $saldo[$key];
                                }
                        $saldo = $saldo_f;// - $saldokredit_f;
                        return $saldo;
                        }
        }
        
        public function getModal($issaldoawal,$rekening3_id){
                $criteria=new CDbCriteria;
                $criteria->group = "issaldoawal, rekening3_id,  nmrekening3";
                $criteria->select = "issaldoawal, rekening3_id, nmrekening3, sum(saldo) as saldo";
                $criteria->compare('issaldoawal',$issaldoawal);
                $criteria->compare('rekening3_id',$rekening3_id);
                $data = LaporanperubahanmodalV::model()->find($criteria);
                return $data;
        }
        
        public function getTotal($rekening3_id){
                $criteria=new CDbCriteria;
                $criteria->select = "sum(saldo) as saldo";
                $criteria->compare('rekening3_id',$rekening3_id);
                $data = LaporanperubahanmodalV::model()->find($criteria);
                return $data;
        }
        
}