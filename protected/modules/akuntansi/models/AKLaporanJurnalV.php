<?php
class AKLaporanJurnalV extends LaporanjurnalV
{
    public $data;
    public $jumlah;
    public $tick;
    
    public static function model($className = __CLASS__) {
        parent::model($className);
    }
    
    public function searchTable()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);
            $criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
            $criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
            $criteria->compare('rekperiod_id',$this->rekperiod_id);
            $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
            $criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
            $criteria->compare('LOWER(kodejurnal)',strtolower($this->kodejurnal),true);
            $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
            $criteria->compare('LOWER(tglreferensi)',strtolower($this->tglreferensi),true);
            $criteria->compare('nobku',$this->nobku);
            $criteria->compare('LOWER(urianjurnal)',strtolower($this->urianjurnal),true);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
            $criteria->compare('jurnalposting_id',$this->jurnalposting_id);
            $criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
            $criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
            $criteria->compare('rekening1_id',$this->rekening1_id);
            $criteria->compare('LOWER(kdrekening1)',strtolower($this->kdrekening1),true);
            $criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
            $criteria->compare('rekening2_id',$this->rekening2_id);
            $criteria->compare('LOWER(kdrekening2)',strtolower($this->kdrekening2),true);
            $criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
            $criteria->compare('rekening3_id',$this->rekening3_id);
            $criteria->compare('LOWER(kdrekening3)',strtolower($this->kdrekening3),true);
            $criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
            $criteria->compare('rekening4_id',$this->rekening4_id);
            $criteria->compare('LOWER(kdrekening4)',strtolower($this->kdrekening4),true);
            $criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
            $criteria->compare('rekening5_id',$this->rekening5_id);
            $criteria->compare('LOWER(kdrekening5)',strtolower($this->kdrekening5),true);
            $criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
            $criteria->compare('tiperekening_id',$this->tiperekening_id);
            $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
            $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
            $criteria->compare('saldodebit',$this->saldodebit);
            $criteria->compare('saldokredit',$this->saldokredit);
            $criteria->compare('koreksi',$this->koreksi);
            $criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
            $criteria->compare('ruangan_id',$this->ruangan_id);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    
     public function searchPrint()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);
            $criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
            $criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
            $criteria->compare('rekperiod_id',$this->rekperiod_id);
            $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
            $criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
            $criteria->compare('LOWER(kodejurnal)',strtolower($this->kodejurnal),true);
            $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
            $criteria->compare('LOWER(tglreferensi)',strtolower($this->tglreferensi),true);
            $criteria->compare('nobku',$this->nobku);
            $criteria->compare('LOWER(urianjurnal)',strtolower($this->urianjurnal),true);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
            $criteria->compare('jurnalposting_id',$this->jurnalposting_id);
            $criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
            $criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
            $criteria->compare('rekening1_id',$this->rekening1_id);
            $criteria->compare('LOWER(kdrekening1)',strtolower($this->kdrekening1),true);
            $criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
            $criteria->compare('rekening2_id',$this->rekening2_id);
            $criteria->compare('LOWER(kdrekening2)',strtolower($this->kdrekening2),true);
            $criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
            $criteria->compare('rekening3_id',$this->rekening3_id);
            $criteria->compare('LOWER(kdrekening3)',strtolower($this->kdrekening3),true);
            $criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
            $criteria->compare('rekening4_id',$this->rekening4_id);
            $criteria->compare('LOWER(kdrekening4)',strtolower($this->kdrekening4),true);
            $criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
            $criteria->compare('rekening5_id',$this->rekening5_id);
            $criteria->compare('LOWER(kdrekening5)',strtolower($this->kdrekening5),true);
            $criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
            $criteria->compare('tiperekening_id',$this->tiperekening_id);
            $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
            $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
            $criteria->compare('saldodebit',$this->saldodebit);
            $criteria->compare('saldokredit',$this->saldokredit);
            $criteria->compare('koreksi',$this->koreksi);
            $criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
            $criteria->compare('ruangan_id',$this->ruangan_id);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                  'pagination'=>false,
            ));
    }
    
    public function searchGrafik(){
               
                $criteria = MyFunction::criteriaGrafikJurnal($this,'data', array('tick'=>'jenisjurnal_nama'));
                
                $criteria->order = 'jenisjurnal_nama';
                
                $criteria->addBetweenCondition('tgljurnalpost', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('jenisjurnal_id',$this->jenisjurnal_id);
                $criteria->compare('LOWER(jenisjurnal_nama)',strtolower($this->jenisjurnal_nama),true);
                $criteria->compare('rekperiod_id',$this->rekperiod_id);
                $criteria->compare('LOWER(tglbuktijurnal)',strtolower($this->tglbuktijurnal),true);
                $criteria->compare('LOWER(nobuktijurnal)',strtolower($this->nobuktijurnal),true);
                $criteria->compare('LOWER(kodejurnal)',strtolower($this->kodejurnal),true);
                $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
                $criteria->compare('LOWER(tglreferensi)',strtolower($this->tglreferensi),true);
                $criteria->compare('nobku',$this->nobku);
                $criteria->compare('LOWER(urianjurnal)',strtolower($this->urianjurnal),true);
                $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
                $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
                $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
                $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
                $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('jurnalposting_id',$this->jurnalposting_id);
                $criteria->compare('LOWER(tgljurnalpost)',strtolower($this->tgljurnalpost),true);
                $criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
                $criteria->compare('rekening1_id',$this->rekening1_id);
                $criteria->compare('LOWER(kdrekening1)',strtolower($this->kdrekening1),true);
                $criteria->compare('LOWER(nmrekening1)',strtolower($this->nmrekening1),true);
                $criteria->compare('rekening2_id',$this->rekening2_id);
                $criteria->compare('LOWER(kdrekening2)',strtolower($this->kdrekening2),true);
                $criteria->compare('LOWER(nmrekening2)',strtolower($this->nmrekening2),true);
                $criteria->compare('rekening3_id',$this->rekening3_id);
                $criteria->compare('LOWER(kdrekening3)',strtolower($this->kdrekening3),true);
                $criteria->compare('LOWER(nmrekening3)',strtolower($this->nmrekening3),true);
                $criteria->compare('rekening4_id',$this->rekening4_id);
                $criteria->compare('LOWER(kdrekening4)',strtolower($this->kdrekening4),true);
                $criteria->compare('LOWER(nmrekening4)',strtolower($this->nmrekening4),true);
                $criteria->compare('rekening5_id',$this->rekening5_id);
                $criteria->compare('LOWER(kdrekening5)',strtolower($this->kdrekening5),true);
                $criteria->compare('LOWER(nmrekening5)',strtolower($this->nmrekening5),true);
                $criteria->compare('tiperekening_id',$this->tiperekening_id);
                $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
                $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
                $criteria->compare('saldodebit',$this->saldodebit);
                $criteria->compare('saldokredit',$this->saldokredit);
                $criteria->compare('koreksi',$this->koreksi);
                $criteria->compare('LOWER(catatan)',strtolower($this->catatan),true);
                $criteria->compare('ruangan_id',$this->ruangan_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
    
    public function getKodeRekDebit()
    {
            $criteria = new CDbCriteria;
            $criteria->compare('struktur_id',$this->rekening1_id);
            $criteria->compare('kelompok_id',$this->rekening2_id);
            $criteria->compare('jenis_id',$this->rekening3_id);
            $criteria->compare('obyek_id',$this->rekening4_id);
            $criteria->compare('rincianobyek_id',$this->rekening5_id);
            $result = AKRekeningakuntansiV::model()->find($criteria);
            
            if(isset($result['rincianobyek_id']))
            {
                $kode_rekening = $result['nmrincianobyek'];
            }else{
                if(isset($result['obyek_id']))
                {
                    $kode_rekening = $result['nmobyek'];
                }else{
                    $kode_rekening = $result['nmjenis'];
                }
            }
            return ($this->saldokredit == 0 ? $kode_rekening : "-") ;
    }
    
    public function getNamaRekeningDebit()
    {
            $criteria = new CDbCriteria;
            $criteria->compare('struktur_id',$this->rekening1_id);
            $criteria->compare('kelompok_id',$this->rekening2_id);
            $criteria->compare('jenis_id',$this->rekening3_id);
            $criteria->compare('obyek_id',$this->rekening4_id);
            $criteria->compare('rincianobyek_id',$this->rekening5_id);
            $result = AKRekeningakuntansiV::model()->find($criteria);
            
            if(isset($result['rincianobyek_id']))
            {
                $kode_rekening = $result['nmrincianobyek'];
            }else{
                if(isset($result['obyek_id']))
                {
                    $kode_rekening = $result['nmobyek'];
                }else{
                    $kode_rekening = $result['nmjenis'];
                }
            }
            return ($this->saldodebit == 0 ? $kode_rekening : "-") ;
    } 
    
    public function getNamaRekeningKredit()
    {
            $criteria = new CDbCriteria;
            $criteria->compare('struktur_id',$this->rekening1_id);
            $criteria->compare('kelompok_id',$this->rekening2_id);
            $criteria->compare('jenis_id',$this->rekening3_id);
            $criteria->compare('obyek_id',$this->rekening4_id);
            $criteria->compare('rincianobyek_id',$this->rekening5_id);
            $result = AKRekeningakuntansiV::model()->find($criteria);
            
            if(isset($result['rincianobyek_id']))
            {
                $kode_rekening = $result['nmrincianobyek'];
            }else{
                if(isset($result['obyek_id']))
                {
                    $kode_rekening = $result['nmobyek'];
                }else{
                    $kode_rekening = $result['nmjenis'];
                }
            }
            return ($this->saldokredit == 0 ? $kode_rekening : "-") ;
    }   
	
}

?>