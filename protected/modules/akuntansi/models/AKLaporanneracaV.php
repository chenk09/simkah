<?php

/**
 * This is the model class for table "laporanneraca_v".
 *
 * The followings are the available columns in table 'laporanneraca_v':
 * @property integer $struktur_id
 * @property string $kdstruktur
 * @property string $nmstruktur
 * @property integer $kelompok_id
 * @property string $kdkelompok
 * @property string $nmkelompok
 * @property integer $jenis_id
 * @property string $kdjenis
 * @property string $nmjenis
 * @property string $tglbuktijurnal
 * @property integer $rekperiod_id
 * @property double $saldonominal
 * @property string $kelompokrek
 */
class AKLaporanneracaV extends LaporanneracaV
{
        /**
         * Returns the static model of the specified AR class.
         * @param string $className active record class name.
         * @return LaporanneracaV the static model class
         */
        public $tglAwal, $tglAkhir, $nominal,$nmrekening4;
        public static function model($className=__CLASS__)
        {
                return parent::model($className);
        }
        
//        public function getBerdasarkanStruktur($strukturid)
//        {
//            $criteria=new CDbCriteria;
//            $criteria->select = 'struktur_id,nmstruktur,sum(saldonominal) as saldo';
//            $criteria->group = "struktur_id,nmstruktur";
//            $criteria->compare('struktur_id', ''.$strukturid.'');
//            $res = LaporanneracaV::model()->findAll($criteria);
//            return $res;
//        }
        
        public function getTotalNominal($kdstruktur,$kdkelompok,$kdjenis=null,$ruangan_id=null)
        {
                $criteriaTotal=new CDbCriteria;
//                $criteriaTotal->addBetweenCondition('tglbuktijurnal', $this->tglAwal, $this->tglAkhir);
                $criteriaTotal->select =  'kdstruktur, kdkelompok, kdjenis, sum(saldonominal) as nominal';
                $criteriaTotal->group = 'nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
                $criteriaTotal->order = 'kdstruktur, kdkelompok, kdjenis';
                $criteriaTotal->compare('kdstruktur', ''.$kdstruktur.'');
                $criteriaTotal->compare('kdkelompok', ''.$kdkelompok.'');
                $criteriaTotal->compare('kdjenis', ''.$kdjenis.'');
                $criteriaTotal->compare('ruangan_id', ''.$ruangan_id.'');
                $res = LaporanneracaV::model()->find($criteriaTotal);
                return $res;
        }
        
        public function getSaldoTotal($struktur_id=null,$kelompok_id=null,$jenis_id=null,$ruangan_id=null){
            $format = new CustomFormat();
            $criteria= new CDbCriteria;
            $criteria->group = 'nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
            $criteria->select = $criteria->group.', sum(saldonominal) as saldonominal';
            $criteria->compare('struktur_id',$struktur_id);
            $criteria->compare('kelompok_id',$kelompok_id);
            $criteria->compare('jenis_id',$jenis_id);
            $criteria->compare('ruangan_id',$ruangan_id);
            
            $modRuangans = LaporanneracaV::model()->findAll($criteria);
            
            $jml = 0;

            foreach($modRuangans as $ruangan){
                $jml += $ruangan->saldonominal;
            }
            
            return $jml;
        }
        
}