<?php

    class AKJenispenerimaanM extends JenispenerimaanM
    {
            /**
             * Returns the static model of the specified AR class.
             * @param string $className active record class name.
             * @return JenispenerimaanM the static model class
             */
            public $rekening_debit,$rekeningKredit;
            public static function model($className=__CLASS__)
            {
                    return parent::model($className);
            }
            
            public function searchJnsPenerimaanRek()
            {
                    // Warning: Please modify the following code to remove attributes that
                    // should not be searched.

                    $criteria=new CDbCriteria;

                    $criteria->select ='t.jenispenerimaan_id,t.jenispenerimaan_nama,t.jenispenerimaan_kode,t.jenispenerimaan_namalain,
                                        t.jenispenerimaan_aktif';
                    $criteria->compare('LOWER(jenispenerimaan_kode)',strtolower($this->jenispenerimaan_kode),true);
                    $criteria->compare('LOWER(jenispenerimaan_nama)',strtolower($this->jenispenerimaan_nama),true);
                    $criteria->compare('LOWER(jenispenerimaan_namalain)',strtolower($this->jenispenerimaan_namalain),true);
                    $criteria->compare('jenispenerimaan_aktif',$this->jenispenerimaan_aktif);
                    $criteria->group = 't.jenispenerimaan_id,t.jenispenerimaan_nama,t.jenispenerimaan_kode,t.jenispenerimaan_namalain,
                                        t.jenispenerimaan_aktif';   
                    $criteria->order='t.jenispenerimaan_id ASC';
                    if(isset($this->rekening_debit)){
                        $debit = "D";
                        $criteria_satu = new CDbCriteria;
                        $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekening_debit),true);
                        $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                        $record = JnspenerimaanrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                        var_dump($record->attributes);
                        $data = array();
                        foreach($record as $value)
                        {
                            $data[] = $value->jenispenerimaan_id;
                        }
                        if(count($data)>0){
                               $condition = 't.jenispenerimaan_id IN ('. implode(',', $data) .')';
                               $criteria->addCondition($condition);
                        }
                    }

                    if(isset($this->rekeningKredit)){
                            $debit = "K";
                            $criteria_satu = new CDbCriteria;
                            $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekeningKredit),true);
                            $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                            $record = JnspenerimaanrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                            var_dump($record->attributes);
                            $data = array();
                            foreach($record as $value)
                            {
                                $data[] = $value->jenispenerimaan_id;
                            }
                            if(count($data)>0){
                                   $condition = 't.jenispenerimaan_id IN ('. implode(',', $data) .')';
                                   $criteria->addCondition($condition);
                            }
                    }
                    $criteria->join ='LEFT JOIN jnspenerimaanrek_m on t.jenispenerimaan_id = jnspenerimaanrek_m.jenispenerimaan_id';

                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                    ));
            }
            
            public function searchJnsPenerimaanRekPrint()
            {
                    // Warning: Please modify the following code to remove attributes that
                    // should not be searched.

                    $criteria=new CDbCriteria;

                    $criteria->select ='t.jenispenerimaan_id,t.jenispenerimaan_nama,t.jenispenerimaan_kode,t.jenispenerimaan_namalain,
                                        t.jenispenerimaan_aktif';
                    $criteria->compare('LOWER(jenispenerimaan_kode)',strtolower($this->jenispenerimaan_kode),true);
                    $criteria->compare('LOWER(jenispenerimaan_nama)',strtolower($this->jenispenerimaan_nama),true);
                    $criteria->compare('LOWER(jenispenerimaan_namalain)',strtolower($this->jenispenerimaan_namalain),true);
                    $criteria->compare('jenispenerimaan_aktif',$this->jenispenerimaan_aktif);
                    $criteria->group = 't.jenispenerimaan_id,t.jenispenerimaan_nama,t.jenispenerimaan_kode,t.jenispenerimaan_namalain,
                                        t.jenispenerimaan_aktif';   
                    $criteria->order='t.jenispenerimaan_id ASC';
                    if(isset($this->rekening_debit)){
                        $debit = "D";
                        $criteria_satu = new CDbCriteria;
                        $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekening_debit),true);
                        $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                        $record = JnspenerimaanrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                        var_dump($record->attributes);
                        $data = array();
                        foreach($record as $value)
                        {
                            $data[] = $value->jenispenerimaan_id;
                        }
                        if(count($data)>0){
                               $condition = 't.jenispenerimaan_id IN ('. implode(',', $data) .')';
                               $criteria->addCondition($condition);
                        }
                    }

                    if(isset($this->rekeningKredit)){
                            $debit = "K";
                            $criteria_satu = new CDbCriteria;
                            $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekeningKredit),true);
                            $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                            $record = JnspenerimaanrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                            var_dump($record->attributes);
                            $data = array();
                            foreach($record as $value)
                            {
                                $data[] = $value->jenispenerimaan_id;
                            }
                            if(count($data)>0){
                                   $condition = 't.jenispenerimaan_id IN ('. implode(',', $data) .')';
                                   $criteria->addCondition($condition);
                            }
                    }
                    $criteria->join ='LEFT JOIN jnspenerimaanrek_m on t.jenispenerimaan_id = jnspenerimaanrek_m.jenispenerimaan_id';
                    $criteria->limit = -1;

                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                            'pagination'=>false,
                    ));
            }


    }

?>