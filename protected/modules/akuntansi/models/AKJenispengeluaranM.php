<?php

    class AKJenispengeluaranM extends JenispengeluaranM
    {
            public static function model($className=__CLASS__)
            {
                    return parent::model($className);
            }
            
            public $rekening_debit,$rekeningKredit;
            public function searchJnsPengeluaranRek()
            {
                    $criteria=new CDbCriteria;
                    $criteria->select ='t.jenispengeluaran_id,t.jenispengeluaran_nama,t.jenispengeluaran_kode,t.jenispengeluaran_namalain,
                                        t.jenispengeluaran_aktif';
                    $criteria->compare('LOWER(t.jenispengeluaran_nama)',strtolower($this->jenispengeluaran_nama),true);
                    $criteria->compare('LOWER(t.jenispengeluaran_kode)',strtolower($this->jenispengeluaran_kode),true);
                    $criteria->compare('LOWER(t.jenispengeluaran_namalain)',strtolower($this->jenispengeluaran_namalain),true);
                    $criteria->compare('t.jenispengeluaran_aktif',$this->jenispengeluaran_aktif);
                    $criteria->group = 't.jenispengeluaran_id,t.jenispengeluaran_nama,t.jenispengeluaran_kode,t.jenispengeluaran_namalain,
                                        t.jenispengeluaran_aktif';   
                    $criteria->order='t.jenispengeluaran_id ASC';
                    if(isset($this->rekening_debit)){
                        $debit = "D";
                        $criteria_satu = new CDbCriteria;
                        $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekening_debit),true);
                        $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                        $record = JnspengeluaranrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                        var_dump($record->attributes);
                        $data = array();
                        foreach($record as $value)
                        {
                            $data[] = $value->jenispengeluaran_id;
                        }
                        if(count($data)>0){
                               $condition = 't.jenispengeluaran_id IN ('. implode(',', $data) .')';
                               $criteria->addCondition($condition);
                        }
                    }

                    if(isset($this->rekeningKredit)){
                            $debit = "K";
                            $criteria_satu = new CDbCriteria;
                            $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekeningKredit),true);
                            $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                            $record = JnspengeluaranrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                            var_dump($record->attributes);
                            $data = array();
                            foreach($record as $value)
                            {
                                $data[] = $value->jenispengeluaran_id;
                            }
                            if(count($data)>0){
                                   $condition = 't.jenispengeluaran_id IN ('. implode(',', $data) .')';
                                   $criteria->addCondition($condition);
                            }
                    }
                    $criteria->join ='LEFT JOIN jnspengeluaranrek_m on t.jenispengeluaran_id = jnspengeluaranrek_m.jenispengeluaran_id';
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                    ));
            }
            
            public function searchJnsPengeluaranRekPrint()
            {
                    $criteria=new CDbCriteria;
                    $criteria->select ='t.jenispengeluaran_id,t.jenispengeluaran_nama,t.jenispengeluaran_kode,t.jenispengeluaran_namalain,
                                        t.jenispengeluaran_aktif';
                    $criteria->compare('t.jenispengeluaran_id',$this->jenispengeluaran_id);
                    $criteria->compare('LOWER(t.jenispengeluaran_nama)',strtolower($this->jenispengeluaran_nama),true);
                    $criteria->compare('LOWER(t.jenispengeluaran_kode)',strtolower($this->jenispengeluaran_kode),true);
                    $criteria->compare('LOWER(t.jenispengeluaran_namalain)',strtolower($this->jenispengeluaran_namalain),true);
                    $criteria->compare('t.jenispengeluaran_aktif',$this->jenispengeluaran_aktif);
                    $criteria->group = 't.jenispengeluaran_id,t.jenispengeluaran_nama,t.jenispengeluaran_kode,t.jenispengeluaran_namalain,
                                        t.jenispengeluaran_aktif';   
                    $criteria->order='t.jenispengeluaran_id ASC';
                    if(isset($this->rekening_debit)){
                        $debit = "D";
                        $criteria_satu = new CDbCriteria;
                        $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekening_debit),true);
                        $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                        $record = JnspengeluaranrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                        var_dump($record->attributes);
                        $data = array();
                        foreach($record as $value)
                        {
                            $data[] = $value->jenispengeluaran_id;
                        }
                        if(count($data)>0){
                               $condition = 't.jenispengeluaran_id IN ('. implode(',', $data) .')';
                               $criteria->addCondition($condition);
                        }
                    }

                    if(isset($this->rekeningKredit)){
                            $debit = "K";
                            $criteria_satu = new CDbCriteria;
                            $criteria_satu->compare('LOWER(rekeningdebit.nmrekening5)', strtolower($this->rekeningKredit),true);
                            $criteria_satu->compare('LOWER(saldonormal)',strtolower($debit),true);

                            $record = JnspengeluaranrekM::model()->with("rekeningdebit","rekeningkredit")->findAll($criteria_satu);
                            var_dump($record->attributes);
                            $data = array();
                            foreach($record as $value)
                            {
                                $data[] = $value->jenispengeluaran_id;
                            }
                            if(count($data)>0){
                                   $condition = 't.jenispengeluaran_id IN ('. implode(',', $data) .')';
                                   $criteria->addCondition($condition);
                            }
                    }
                    $criteria->join ='LEFT JOIN jnspengeluaranrek_m on t.jenispengeluaran_id = jnspengeluaranrek_m.jenispengeluaran_id';
                    $criteria->limit = -1;
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$criteria,
                            'pagination'=>false,
                    ));
            }


    }

?>