<?php
class AKLaporanBukuBesarV extends LaporanbukubesarV
{
    public $data;
    public $jumlah;
    public $tick;
    
    public $getSaldoAkhir,$totSaldo,$getTotalDebit,$getTotalKredit,$getTotSaldo;
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchTable()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglbukubesar',$this->tglAwal, $this->tglAkhir); 
            
            $criteria->compare('rekening1_id',$this->rekening1_id);            
            $criteria->compare('rekening2_id',$this->rekening2_id);            
            $criteria->compare('rekening3_id',$this->rekening3_id);            
            $criteria->compare('rekening4_id',$this->rekening4_id);            
            $criteria->compare('rekening5_id',$this->rekening5_id);
            $term = $_GET['AKLaporanBukuBesarV']['namaRekening'];
            $termKode = $_GET['AKLaporanBukuBesarV']['kodeRekening'];
//            var_dump($termKode);
//            exit;
            $condition  = "LOWER(nmrekening5) ILIKE '%". $term ."%' OR LOWER(nmrekening4) ILIKE '%". $term ."%' OR LOWER(nmrekening3) ILIKE '%". $term ."%'";
            $condition2 ="LOWER(kdrekening5) ILIKE '%".$termKode."%' OR LOWER(kdrekening4) ILIKE '%".$termKode."%' OR LOWER(kdrekening3) ILIKE '%".$termKode."%'";
            $criteria->addCondition($condition);
            $criteria->addCondition($condition2);
            
            $criteria->compare('tiperekening_id',$this->tiperekening_id);
            $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
            $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
            
            $criteria->compare('saldodebit',$this->saldodebit);
            $criteria->compare('saldokredit',$this->saldokredit);
            $criteria->compare('bukubesar_id',$this->bukubesar_id);
            $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
            
            $criteria->order = 'bukubesar_id ASC , nourut ASC';

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
     public function searchTablePrint()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglbukubesar',$this->tglAwal, $this->tglAkhir); 
            
            $criteria->compare('rekening1_id',$this->rekening1_id);            
            $criteria->compare('rekening2_id',$this->rekening2_id);            
            $criteria->compare('rekening3_id',$this->rekening3_id);            
            $criteria->compare('rekening4_id',$this->rekening4_id);            
            $criteria->compare('rekening5_id',$this->rekening5_id);
            $term = $_GET['AKLaporanBukuBesarV']['namaRekening'];
            $termKode = $_GET['AKLaporanBukuBesarV']['kodeRekening'];
//            var_dump($termKode);
//            exit;
            $condition  = "LOWER(nmrekening5) ILIKE '%". $term ."%' OR LOWER(nmrekening4) ILIKE '%". $term ."%' OR LOWER(nmrekening3) ILIKE '%". $term ."%'";
            $condition2 ="LOWER(kdrekening5) ILIKE '%".$termKode."%' OR LOWER(kdrekening4) ILIKE '%".$termKode."%' OR LOWER(kdrekening3) ILIKE '%".$termKode."%'";
            $criteria->addCondition($condition);
            $criteria->addCondition($condition2);
            
            $criteria->compare('tiperekening_id',$this->tiperekening_id);
            $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
            $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
            
            $criteria->compare('saldodebit',$this->saldodebit);
            $criteria->compare('saldokredit',$this->saldokredit);
            
            $criteria->compare('bukubesar_id',$this->bukubesar_id);
            $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);
            
            $criteria->order = 'bukubesar_id ASC , nourut ASC';
            $criteria->limit = -1;
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    
    public function searchGrafik(){
               
        if($this->rekening5_id){
            $criteria = MyFunction::criteriaGrafikBukuKas($this,'data', array('tick'=>'nmrekening5'));
            $criteria->order = 'nmrekening5';
        }else{
            if($this->rekening4_id){
                $criteria = MyFunction::criteriaGrafikBukuKas($this,'data', array('tick'=>'nmrekening4'));
                $criteria->order = 'nmrekening4';
            }else{
                $criteria = MyFunction::criteriaGrafikBukuKas($this,'data', array('tick'=>'nmrekening3'));
                $criteria->order = 'nmrekening3';
            }
        }
        
        $criteria->addBetweenCondition('tglbukubesar',$this->tglAwal, $this->tglAkhir); 

        $criteria->compare('rekening1_id',$this->rekening1_id);            
        $criteria->compare('rekening2_id',$this->rekening2_id);            
        $criteria->compare('rekening3_id',$this->rekening3_id);            
        $criteria->compare('rekening4_id',$this->rekening4_id);            
        $criteria->compare('rekening5_id',$this->rekening5_id);
        $term = $_GET['AKLaporanBukuBesarV']['namaRekening'];
        $termKode = $_GET['AKLaporanBukuBesarV']['kodeRekening'];
    //            var_dump($termKode);
    //            exit;
        $condition  = "LOWER(nmrekening5) ILIKE '%". $term ."%' OR LOWER(nmrekening4) ILIKE '%". $term ."%' OR LOWER(nmrekening3) ILIKE '%". $term ."%'";
        $condition2 ="LOWER(kdrekening5) ILIKE '%".$termKode."%' OR LOWER(kdrekening4) ILIKE '%".$termKode."%' OR LOWER(kdrekening3) ILIKE '%".$termKode."%'";
        $criteria->addCondition($condition);
        $criteria->addCondition($condition2);

        $criteria->compare('tiperekening_id',$this->tiperekening_id);
        $criteria->compare('LOWER(nourut)',strtolower($this->nourut),true);
        $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);

        $criteria->compare('saldodebit',$this->saldodebit);
        $criteria->compare('saldokredit',$this->saldokredit);

        $criteria->compare('bukubesar_id',$this->bukubesar_id);
        $criteria->compare('LOWER(noreferensi)',strtolower($this->noreferensi),true);


            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public function getKodeRekDebit()
    {
            $criteria = new CDbCriteria;
            $criteria->compare('struktur_id',$this->rekening1_id);
            $criteria->compare('kelompok_id',$this->rekening2_id);
            $criteria->compare('jenis_id',$this->rekening3_id);
            $criteria->compare('obyek_id',$this->rekening4_id);
            $criteria->compare('rincianobyek_id',$this->rekening5_id);
            $result = AKRekeningakuntansiV::model()->find($criteria);
            
            if(isset($result['rincianobyek_id']))
            {
                $kode_rekening = $result['nmrincianobyek'];
            }else{
                if(isset($result['obyek_id']))
                {
                    $kode_rekening = $result['nmobyek'];
                }else{
                    $kode_rekening = $result['nmjenis'];
                }
            }
            return ($this->saldokredit == 0 ? $kode_rekening : "-") ;
    }
    
       
    public function getKodeRekening(){
        if(isset($this->rekening5_id)){
            $kodeRekening = $this->kdrekening1." - ".$this->kdrekening2." - ".$this->kdrekening3." - ".$this->kdrekening4." - ".$this->kdrekening5;
        }else{
            if(isset($this->rekening4_id)){
                $kodeRekening = $this->kdrekening1." - ".$this->kdrekening2." - ".$this->kdrekening3." - ".$this->kdrekening4;
            }else if($this->rekening3_id){
                $kodeRekening = $this->kdrekening1." - ".$this->kdrekening2." - ".$this->kdrekening3;
            }
        }
        return $kodeRekening;
    }
    
    public function getNamaRekening(){
        if(isset($this->nmrekening5)){
            $namaRekening = $this->nmrekening5;
        }else{
            if(isset($this->nmrekening4)){
                $namaRekening = $this->nmrekening4;
            }else if(isset($this->nmrekening3)){
                $namaRekening = $this->nmrekening3;
            }
        }
        return $namaRekening;
    }

    public function getSaldoAkhir()
    {   
            $totDebit += $this->saldodebit;
            $totKredit += $this->saldokredit;

            $totSaldo = $totDebit - $totKredit;

            if($totSaldo < 0){
                $totSaldo = 0;
            }
            
        return $totSaldo;
    }
    
    public function getTotSaldo(){
         $criteria=$this->criteriaTable();
         $criteria->select = 'SUM(saldodebit - saldokredit)';
         return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
    }
    
    public function criteriaTable()
    {
        $criteria=new CDbCriteria;

        $criteria->select ='SUM(saldodebit) AS saldodebit,
                SUM(saldokredit) AS saldokredit';
        return $criteria;
    }
	
}

?>