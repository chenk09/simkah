<?php

/**
 * This is the model class for table "unitkerja_m".
 *
 * The followings are the available columns in table 'unitkerja_m':
 * @property integer $unitkerja_id
 * @property integer $ruangan_id
 * @property string $kodeunitkerja
 * @property string $namaunitkerja
 * @property string $namalain
 * @property boolean $unitkerja_aktif
 *
 * The followings are the available model relations:
 * @property RuanganM $ruangan
 */
class AKUnitKerjaRuanganM extends UnitkerjaruanganM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UnitkerjaM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchTable()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = 'unitkerja_id';
                $criteria->group = 'unitkerja_id';
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('unitkerja_id',$this->unitkerja_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = 'unitkerja_id';
                $criteria->group = 'unitkerja_id';
                
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('unitkerja_id',$this->unitkerja_id);
                $criteria->limit = -1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}

}
