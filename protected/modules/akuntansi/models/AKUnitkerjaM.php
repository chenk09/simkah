<?php

/**
 * This is the model class for table "unitkerja_m".
 *
 * The followings are the available columns in table 'unitkerja_m':
 * @property integer $unitkerja_id
 * @property integer $ruangan_id
 * @property string $kodeunitkerja
 * @property string $namaunitkerja
 * @property string $namalain
 * @property boolean $unitkerja_aktif
 *
 * The followings are the available model relations:
 * @property RuanganM $ruangan
 */
class AKUnitkerjaM extends UnitkerjaM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UnitkerjaM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// protected function beforeValidate ()
 //    {
 //        return parent::beforeSave();
 //    }
 //    public function beforeSave() 
 //    {
 //        return parent::beforeSave();
 //    }
}
