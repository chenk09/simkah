<?php
class AKRincianpenerimaankasrekeningV extends RincianpenerimaankasrekeningV
{
        public $tglAwal,$tglAkhir;
        public $saldodebit,$saldokredit;
        public $jenisNama;
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
         * digunakan di:
         * - akuntansi/actionAjax/GetRekeningPenerimaanKas
         */
        public function criteriaFunction(){
            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglbuktibayar',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(nobuktibayar)',strtolower($this->nobuktibayar),true);
            $criteria->compare('jenis_id',$this->jenis_id);

            $criteria->limit = 500; //batas maksimal data
            return $criteria;
        }
        
        public function searchInformasi()
	{
            $criteria=new $this->criteriaFunction();

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
	}
}
?>