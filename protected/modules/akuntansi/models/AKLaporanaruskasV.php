<?php

/**
 * This is the model class for table "laporanaruskas_v".
 *
 * The followings are the available columns in table 'laporanaruskas_v':
 * @property integer $rekperiod_id
 * @property string $perideawal
 * @property string $sampaidgn
 * @property string $deskripsi
 * @property integer $rekening1_id
 * @property string $kdrekening1
 * @property string $nmrekening1
 * @property integer $rekening2_id
 * @property string $kdrekening2
 * @property string $nmrekening2
 * @property integer $rekening3_id
 * @property string $kdrekening3
 * @property string $nmrekening3
 * @property integer $rekening5_id
 * @property string $kdrekening5
 * @property string $nmrekening5
 * @property double $saldodebit
 * @property double $saldokredit
 * @property double $saldo
 * @property boolean $issaldoawal
 */
class AKLaporanaruskasV extends LaporanaruskasV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanaruskasV the static model class
	 */
	public $tglAwal, $tglAkhir,$ruangan_id,$saldoinstalasi,$saldoPerRek,$nmrekening5;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	

    /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchArusKas()
	{
                        // Warning: Please modify the following code to remove attributes that
                        // should not be searched.

                        $criteria=new CDbCriteria;

                        $criteria->select ='sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit, sum(saldo) as saldo, issaldoawal, rekening1_id';
                        $criteria->compare('DATE(perideawal)',strtolower($periode['tglawal']),true);
                        $criteria->compare('DATE(sampaidgn)',strtolower($periode['tglakhir']),true);
                        $criteria->compare('ruangan_id',$model->ruangan_id);
                        $criteria->group = 'rekening1_id, issaldoawal';

                        return new CActiveDataProvider($this, array(
                                'criteria'=>$criteria,
                        ));
	}
                
                public function getTableHeader(){
                    $criteriaTableHeader=new CDbCriteria;
                    $criteriaTableHeader->group = 'instalasi_id, instalasi_nama, ruangan_id, ruangan_nama'; 
                    $criteriaTableHeader->select = 'instalasi_id, instalasi_nama, ruangan_id, ruangan_nama';
                    $dataTableHeader = AKLaporanaruskasV::model()->findAll($criteriaTableHeader);
                    return $dataTableHeader;
                }
                
		public function getLabaRugi(){
			$criteria=new CDbCriteria;
			$criteria->group = "rekperiod_id, jenissaldo";
			$criteria->select = $criteria->group.", sum(laba) as laba, sum(rugi) as rugi, sum(pajak) as pajak";
			$criteria->compare('rekperiod_id',PARAMS::REKPERIOD2015);
			$data = AKSumlabarugiV::model()->findAll($criteria);
			
			$laba=0;$rugi=0;$pajak=0;$labarugi=0;
			foreach($data as $value){
				$laba+=$value->laba;
				$rugi+=$value->rugi;
				$pajak+=$value->pajak;
			}
			$labarugi=$laba-$rugi-$pajak;
			return $labarugi;
		}	
		public function getSelisihKewajiban($rekperiod_id=null){
			$criteria=new CDbCriteria;
			$criteria->group = "kelompok_id, rekperiod_id";
			$criteria->select = $criteria->group.", sum(saldonominal) as saldonominal";
			isset($rekperiod_id)?$criteria->addCondition("rekperiod_id= ".$rekperiod_id):"";
			$criteria->compare('kelompok_id',PARAMS::REK2_ID_KEWAJIBANLANCAR); //EHJ-3286
			$data = AKLaporanneracaV::model()->findAll($criteria);
			
			$saldonominal = 0;
			foreach($data as $value){
				$saldonominal += $value->saldonominal;
			}
			
			return $saldonominal;
		}	
		public function getSelisihAktiva($rekperiod_id=null){
			$criteria=new CDbCriteria;
			$criteria->group = "jenis_id, rekperiod_id";
			$criteria->select = $criteria->group.", sum(saldonominal) as saldonominal";
			isset($rekperiod_id)?$criteria->addCondition("rekperiod_id= ".$rekperiod_id):"";
			$condition = "jenis_id=2 OR jenis_id=3 OR jenis_id=4 OR jenis_id=5 OR jenis_id=6"; //EHJ-3286
			$criteria->addCondition($condition);
			$data = AKLaporanneracaV::model()->findAll($criteria);
			
			$saldonominal = 0;
			foreach($data as $value){
				$saldonominal += $value->saldonominal;
			}
			
			return $saldonominal;
		}	
		public function getPenyusutanAmortisasi($rekperiod_id=null){
			$criteria=new CDbCriteria;
			$criteria->group = "rekening4_id, rekperiod_id";
			$criteria->select = $criteria->group.", sum(saldonominal) as saldonominal";
			isset($rekperiod_id)?$criteria->addCondition("rekperiod_id= ".$rekperiod_id):"";
			$criteria->compare('rekening4_id',PARAMS::REK4_ID_BEBANPENYUSUTAN_AMORTISASI); //EHJ-3286
			$data = AKLaporanneracaV::model()->findAll($criteria);
			
			$saldonominal = 0;
			foreach($data as $value){
				$saldonominal += $value->saldonominal;
			}
			
			return $saldonominal;
		}
		public function getTotalAktifasiOperasi(){
			$labarugi				= $this->getLabaRugi();
			$selisihkewajiban		= ($this->getSelisihKewajiban(PARAMS::REKPERIOD2015))-($this->getSelisihKewajiban(PARAMS::REKPERIOD2014));
			$selisihaktiva			= ($this->getSelisihAktiva(PARAMS::REKPERIOD2015))-($this->getSelisihAktiva(PARAMS::REKPERIOD2014));
			$penyusutanamortisasi	= ($this->getPenyusutanAmortisasi(PARAMS::REKPERIOD2015))-($this->getPenyusutanAmortisasi(PARAMS::REKPERIOD2014));
			$total = $labarugi+$selisihkewajiban+$selisihaktiva+$penyusutanamortisasi;
			return $total;
		}
		
                public function getKategori($rek1,$rek1_2=null,$ruangan_id=null){
                    $criteriaTable=new CDbCriteria;
                    if($rek1_2<>null){ 
                        $conditionTable = '(rekening1_id='.$rek1.' OR rekening1_id='.$rek1_2.')';
                    }else{
                        $conditionTable = 'rekening1_id='.$rek1.'';
                    }
                    if($ruangan_id<>null){
                        $criteriaTable->group = 'issaldoawal'; 
                        $criteriaTable->select = 'issaldoawal,  sum(saldo) as saldo';
                        $conditionTable .= ' AND ruangan_id='.$ruangan_id.'';
                    }else{
                        $criteriaTable->group = 'rekening1_id,rekening5_id,issaldoawal, nmrekening5'; 
                        $criteriaTable->select = 'rekening1_id, rekening5_id,issaldoawal, nmrekening5, sum(saldo) as saldo';
                        $criteriaTable->order = 'nmrekening5';
                    }
                    
                    $criteriaTable->addCondition($conditionTable);
                    
                    if($ruangan_id<>null){
                        $dataTable = AKLaporanaruskasV::model()->find($criteriaTable);
                        $dataTable = $dataTable->saldo;
                    }else{
                        $dataTable = AKLaporanaruskasV::model()->findAll($criteriaTable);
                    }
                    
                    return $dataTable;
                }

		
                public function getSaldoPerRek($a,$b){
                        $criteriaPerRek=new CDbCriteria;
                        $criteriaPerRek->group = 'issaldoawal, ruangan_id, ruangan_nama'; 
                        $criteriaPerRek->select = 'issaldoawal, ruangan_id, ruangan_nama, sum(saldo) as saldo';
                        $criteriaPerRek->compare('ruangan_id',$a);
                        $criteriaPerRek->compare('rekening5_id',$b);
                        $dataPerRek = AKLaporanaruskasV::model()->find($criteriaPerRek);
                        
                        if(isset($dataPerRek->saldo)){
                            if(($dataPerRek->issaldoawal)==FALSE){
                                return $dataPerRek->saldo;
                            }else{
                                $dataPerRek->saldo = 0;         // saldo di beri nilai 0 jika issaldoawal = TRUE
                                return $dataPerRek->saldo;
                            }
                        }else{
                            $dataPerRek->saldo = 0;             // saldo di beri nilai 0 jika nilai yg dibawa parameter ternyata kosong
                            return $dataPerRek->saldo;
                        }
                }
                
                public function getTotKas($ruangId){
                        $criteria=new CDbCriteria;
                        $criteria->select ='nmrekening5, sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit, sum(saldo) as saldo, issaldoawal, rekening1_id';
//                        $criteria->compare('DATE(perideawal)',strtolower($periode['tglawal']),true);
//                        $criteria->compare('DATE(sampaidgn)',strtolower($periode['tglakhir']),true);
                        $criteria->compare('ruangan_id',$ruangId);
                        $criteria->group = 'rekening1_id, issaldoawal, nmrekening5'; 
                        $data = LaporanaruskasV::model()->findAll($criteria);

                        $saldodebit_x = 0; $saldokredit_x = 0; $saldo_x = 0;
                        $saldodebit_y = 0; $saldokredit_y = 0; $saldo_y = 0;
                        $saldodebit_z = 0; $saldokredit_z = 0; $saldo_z = 0;
                        $saldodebit_c = 0; $saldokredit_c = 0; $saldo_c_peruang = 0;
                        foreach ($data as $key => $value) {
                                $rekening[$key] 	= $value['rekening1_id'];
                                $saldodebit[$key] 	= $value['saldodebit'];
                                $saldokredit[$key] 	= $value['saldokredit'];
                                                $saldo[$key]            = $value['saldo'];
                                                $issaldoawal            = $value['issaldoawal'];

                                        if($issaldoawal==false || empty($issaldoawal)){
                                                if($rekening[$key]==4 || $rekening[$key]==5)
                                                {
                                                        $saldodebit_x 	+= $saldodebit[$key];
                                                        $saldokredit_x  += $saldokredit[$key];
                                                        $saldo_x  += $saldo[$key];
                                                }

                                                 if($rekening[$key]==6 || $rekening[$key]==7)
                                                {
                                                        $saldodebit_y 	+= $saldodebit[$key];
                                                        $saldokredit_y  += $saldokredit[$key];
                                                        $saldo_y  += $saldo[$key];
                                                }

                                                if($rekening[$key]==3)
                                                {
                                                        $saldodebit_z 	+= $saldodebit[$key];
                                                        $saldokredit_z  += $saldokredit[$key];
                                                                                        $saldo_z  += $saldo[$key];
                                                }
                                        }else{
                                                $saldodebit_c += $saldodebit[$key];
                                                $saldokredit_c  += $saldokredit[$key];
                                                $saldo_c_peruang  += $saldo[$key];
                                        }
                        }
                        return $total_a_peruang = $saldo_x +$saldo_y+$saldo_z;
                }
                
                public function getTotKasNaik($ruangId){
                        $criteria=new CDbCriteria;
                        $criteria->select ='nmrekening5, sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit, sum(saldo) as saldo, issaldoawal, rekening1_id';
//                        $criteria->compare('DATE(perideawal)',strtolower($periode['tglawal']),true);
//                        $criteria->compare('DATE(sampaidgn)',strtolower($periode['tglakhir']),true);
                        $criteria->compare('ruangan_id',$ruangId);
                        $criteria->group = 'rekening1_id, issaldoawal, nmrekening5'; 
                        $data = LaporanaruskasV::model()->findAll($criteria);

                        $saldodebit_x = 0; $saldokredit_x = 0; $saldo_x = 0;
                        $saldodebit_y = 0; $saldokredit_y = 0; $saldo_y = 0;
                        $saldodebit_z = 0; $saldokredit_z = 0; $saldo_z = 0;
                        $saldodebit_c = 0; $saldokredit_c = 0; $saldo_c_peruang = 0;
                        foreach ($data as $key => $value) {
                                $rekening[$key] 	= $value['rekening1_id'];
                                $saldodebit[$key] 	= $value['saldodebit'];
                                $saldokredit[$key] 	= $value['saldokredit'];
                                                $saldo[$key]            = $value['saldo'];
                                                $issaldoawal            = $value['issaldoawal'];

                                        if($issaldoawal==false || empty($issaldoawal)){
                                                if($rekening[$key]==4 || $rekening[$key]==5)
                                                {
                                                        $saldodebit_x 	+= $saldodebit[$key];
                                                        $saldokredit_x  += $saldokredit[$key];
                                                        $saldo_x  += $saldo[$key];
                                                }

                                                if($rekening[$key]==1)
                                                {
                                                        $saldodebit_y 	+= $saldodebit[$key];
                                                        $saldokredit_y  += $saldokredit[$key];
                                                        $saldo_y  += $saldo[$key];
                                                }

                                                if($rekening[$key]==3)
                                                {
                                                        $saldodebit_z 	+= $saldodebit[$key];
                                                        $saldokredit_z  += $saldokredit[$key];
                                                                                        $saldo_z  += $saldo[$key];
                                                }
                                        }else{
                                                $saldodebit_c += $saldodebit[$key];
                                                $saldokredit_c  += $saldokredit[$key];
                                                $saldo_c_peruang  += $saldo[$key];
                                        }
                        }
                        return $saldo_c_peruang;
                }
                
                public function getTotKasAkhir($ruangId){
                        $criteria=new CDbCriteria;
                        $criteria->select ='nmrekening5, sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit, sum(saldo) as saldo, issaldoawal, rekening1_id';
//                        $criteria->compare('DATE(perideawal)',strtolower($periode['tglawal']),true);
//                        $criteria->compare('DATE(sampaidgn)',strtolower($periode['tglakhir']),true);
                        $criteria->compare('ruangan_id',$ruangId);
                        $criteria->group = 'rekening1_id, issaldoawal, nmrekening5'; 
                        $data = LaporanaruskasV::model()->findAll($criteria);

                        $saldodebit_x = 0; $saldokredit_x = 0; $saldo_x = 0;
                        $saldodebit_y = 0; $saldokredit_y = 0; $saldo_y = 0;
                        $saldodebit_z = 0; $saldokredit_z = 0; $saldo_z = 0;
                        $saldodebit_c = 0; $saldokredit_c = 0; $saldo_c_peruang = 0;
                        foreach ($data as $key => $value) {
                                $rekening[$key] 	= $value['rekening1_id'];
                                $saldodebit[$key] 	= $value['saldodebit'];
                                $saldokredit[$key] 	= $value['saldokredit'];
                                                $saldo[$key]            = $value['saldo'];
                                                $issaldoawal            = $value['issaldoawal'];

                                        if($issaldoawal==false || empty($issaldoawal)){
                                                if($rekening[$key]==4 || $rekening[$key]==5)
                                                {
                                                        $saldodebit_x 	+= $saldodebit[$key];
                                                        $saldokredit_x  += $saldokredit[$key];
                                                        $saldo_x  += $saldo[$key];
                                                }

                                                if($rekening[$key]==1)
                                                {
                                                        $saldodebit_y 	+= $saldodebit[$key];
                                                        $saldokredit_y  += $saldokredit[$key];
                                                        $saldo_y  += $saldo[$key];
                                                }

                                                if($rekening[$key]==3)
                                                {
                                                        $saldodebit_z 	+= $saldodebit[$key];
                                                        $saldokredit_z  += $saldokredit[$key];
                                                                                        $saldo_z  += $saldo[$key];
                                                }
                                        }else{
                                                $saldodebit_c += $saldodebit[$key];
                                                $saldokredit_c  += $saldokredit[$key];
                                                $saldo_c_peruang  += $saldo[$key];
                                        }
                        }
                        return $saldo_c_peruang = $total_a + $saldo_c;
                }
                
                
}