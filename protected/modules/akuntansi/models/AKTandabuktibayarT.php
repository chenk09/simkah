<?php

class AKTandabuktibayarT extends TandabuktibayarT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TandabuktibayarT the static model class
	 */
        public $tglAwal;
        public $tglAkhir;
        public function attributeLabels()
	{
		return array(
			'tandabuktibayar_id' => 'Tandabuktibayar',
			'closingkasir_id' => 'Closingkasir',
			'ruangan_id' => 'Ruangan',
			'shift_id' => 'Shift',
			'bayaruangmuka_id' => 'Bayaruangmuka',
			'pembayaranpelayanan_id' => 'Pembayaranpelayanan',
			'nourutkasir' => 'No Urut Kasir',
			'nobuktibayar' => 'No Bukti Bayar',
			'tglbuktibayar' => 'Tgl Bukti Bayar',
			'carapembayaran' => 'Cara Pembayaran',
			'dengankartu' => 'Dengan Kartu',
			'bankkartu' => 'Bank Kartu',
			'nokartu' => 'No Kartu',
			'nostrukkartu' => 'No Struk Kartu',
			'darinama_bkm' => 'Dari Nama',
			'alamat_bkm' => 'Alamat',
			'sebagaipembayaran_bkm' => 'Keterangan',
			'jmlpembulatan' => 'Jml Pembulatan',
			'jmlpembayaran' => 'Jml Penerimaan',
			'biayaadministrasi' => 'Biaya Administrasi',
			'biayamaterai' => 'Biaya Materai',
			'uangditerima' => 'Uang Diterima',
			'uangkembalian' => 'Uang Kembalian',
			'keterangan_pembayaran' => 'Keterangan Pembayaran',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Login Pemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function searchTable()
	{
		$criteria=new CDbCriteria;
                $criteria->compare('shift_id',$this->shift_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->addBetweenCondition('DATE(tglbuktibayar)',$this->tglAwal, $this->tglAkhir);
                $criteria->addCondition('closingkasir_id IS NULL ');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}        
}