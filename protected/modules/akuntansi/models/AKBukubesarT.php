<?php

/**
 * This is the model class for table "bukubesar_t".
 *
 * The followings are the available columns in table 'bukubesar_t':
 * @property integer $bukubesar_id
 * @property integer $rekening3_id
 * @property integer $rekening4_id
 * @property integer $rekening2_id
 * @property integer $rekening5_id
 * @property integer $rekening1_id
 * @property string $tglbukubesar
 * @property string $uraiantransaksi
 * @property double $saldodebit
 * @property double $saldokredit
 * @property double $saldoakhirberjalan
 * @property string $create_time
 * @property string $update_time
 * @property integer $create_loginpemakai_id
 * @property integer $update_loginpemakai_id
 * @property integer $create_ruangan
 */
class AKBukubesarT extends BukubesarT
{
        public $tglAwal,$tglAkhir,$jenisjurnal_id,$ruangan_id,$tgljurnalpost,$tglbuktijurnal,$kdrekening1,$kdrekening2,$kdrekening3;
        public $kdrekening4,$kdrekening5,$catatan,$nmrekening5,$saldoawal,$saldoakhir,$nmrekening4;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BukubesarT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchTable()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;

                $criteria->compare('bukubesar_id',$this->bukubesar_id);
                $criteria->compare('rekening3_id',$this->rekening3_id);
                $criteria->compare('rekening4_id',$this->rekening4_id);
                $criteria->compare('rekening2_id',$this->rekening2_id);
                $criteria->compare('rekening5_id',$this->rekening5_id);
                $criteria->compare('rekening1_id',$this->rekening1_id);
                $criteria->compare('LOWER(tglbukubesar)',strtolower($this->tglbukubesar),true);
                $criteria->compare('LOWER(uraiantransaksi)',strtolower($this->uraiantransaksi),true);
                $criteria->compare('saldodebit',$this->saldodebit);
                $criteria->compare('saldokredit',$this->saldokredit);
                $criteria->compare('saldoakhirberjalan',$this->saldoakhirberjalan);
                $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
                $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
                $criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
                $criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
                $criteria->compare('create_ruangan',$this->create_ruangan);

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        public function getNorek4(){
            $criteria4=new CDbCriteria;
            $criteria4->addBetweenCondition('tglbukubesar', $this->tglAwal, $this->tglAkhir);
            $criteria4->group = 't.rekening1_id, rekening1_m.kdrekening1, rekening2_m.kdrekening2, rekening3_m.kdrekening3, t.rekening4_id, rekening4_m.kdrekening4, rekening4_m.nmrekening4';
            $criteria4->select = $criteria4->group;
            $criteria4->join = 'JOIN rekening1_m ON t.rekening1_id=rekening1_m.rekening1_id '
                                    . 'JOIN rekening2_m ON t.rekening2_id=rekening2_m.rekening2_id '
                                    . 'JOIN rekening3_m ON t.rekening3_id=rekening3_m.rekening3_id '
                                    . 'JOIN rekening4_m ON t.rekening4_id=rekening4_m.rekening4_id ';
            $criteria4->order = 'rekening1_m.kdrekening1, rekening2_m.kdrekening2, rekening3_m.kdrekening3, rekening4_m.kdrekening4';
            $data4 = AKBukubesarT::model()->findAll($criteria4);
            return $data4;
        }
        public function getNorek($rek1,$rek2,$rek3,$rek4){
		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('tglbukubesar', $this->tglAwal, $this->tglAkhir);
		$criteria->group = 't.rekening1_id, rekening1_m.kdrekening1, rekening2_m.kdrekening2, rekening3_m.kdrekening3, rekening4_m.kdrekening4, rekening5_m.kdrekening5, t.rekening5_id, rekening5_m.nmrekening5';
		$criteria->select = $criteria->group.",sum(saldoakhirberjalan) as saldoakhirberjalan, sum(saldodebit) as saldodebit, sum(saldokredit) as saldokredit";
		$criteria->join = 'JOIN rekening1_m ON t.rekening1_id=rekening1_m.rekening1_id '
								. 'JOIN rekening2_m ON t.rekening2_id=rekening2_m.rekening2_id '
								. 'JOIN rekening3_m ON t.rekening3_id=rekening3_m.rekening3_id '
								. 'JOIN rekening4_m ON t.rekening4_id=rekening4_m.rekening4_id '
								. 'JOIN rekening5_m ON t.rekening5_id=rekening5_m.rekening5_id ';
//		$criteria->order = 'rekening1_m.kdrekening1, rekening2_m.kdrekening2, rekening3_m.kdrekening3, rekening4_m.kdrekening4, rekening5_m.kdrekening5';
		$criteria->compare('rekening1_m.kdrekening1',$rek1);
		$criteria->compare('rekening2_m.kdrekening2',$rek2);
		$criteria->compare('rekening3_m.kdrekening3',$rek3);
		$criteria->compare('rekening4_m.kdrekening4',$rek4);
		$data = AKBukubesarT::model()->findAll($criteria);
		return $data;
        }
	
	public function getSaldoAwal($rek5){
		$criteria=new CDbCriteria;
		$criteria->group = 'jmlanggaran';
		$criteria->select = $criteria->group.",sum(jmlanggaran) as saldoawal";
		$criteria->compare('rekening5_id', $rek5);
		$data = AKSaldoawalT::model()->find($criteria);
		isset($data->saldoawal)?$res = $data->saldoawal : $res = 0;
		return $res;
	}

        
}