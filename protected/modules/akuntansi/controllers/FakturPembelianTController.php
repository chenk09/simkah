<?php
Yii::import('gudangFarmasi.controllers.FakturPembelianController');
Yii::import('gudangFarmasi.models.*');
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class FakturPembelianTController extends FakturPembelianController
{
       public $pathView = 'gudangFarmasi.views.fakturPembelian.';
        
        public function actionRetur($idFakturPembelian)
        {
            $this->layout='//layouts/frameDialog';
            $modFaktur = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
            $modFakturDetail = BKFakturDetailT::model()->findAll('fakturpembelian_id='.$idFakturPembelian.'');
            $modRetur = new BKReturPembelianT;
            $modRetur->fakturpembelian_id=$modFaktur->fakturpembelian_id;
            $modRetur->noretur=  Generator::noRetur();
            $modRetur->totalretur=0;
            $modRetur->tglretur=date('Y-m-d H:i:s');
            $modRetur->supplier_id=$modFaktur->supplier_id;
            $modRetur->create_loginpemakai_id = Yii::app()->user->id;
            $modRetur->update_loginpemakai_id = Yii::app()->user->id;
            $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modRetur->create_time = date('Y-m-d H:i:s');
            $modRetur->update_time = date('Y-m-d H:i:s');
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modReturDetails = new BKReturDetailT;
            $tersimpan=false;
            $modRekenings = array();
            
            if(isset($_POST['BKReturPembelianT'])){
//                
                $modRetur->attributes = $_POST['BKReturPembelianT'];
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;

            $transaction = Yii::app()->db->beginTransaction();
            try {     
//                                 
                $jumlahCekList=0;
                $jumlahSave=0;
                $modRetur = new BKReturPembelianT;
              
                $modRetur->attributes=$_POST['BKReturPembelianT'];
                $modRetur->ruangan_id=Yii::app()->user->getState('ruangan_id');
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;
                $modRetur->create_loginpemakai_id = Yii::app()->user->id;
                $modRetur->update_loginpemakai_id = Yii::app()->user->id;
                $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modRetur->create_time = date('Y-m-d H:i:s');
                $modRetur->update_time = date('Y-m-d H:i:s');
                if($modRetur->save()){
                $jumlahObat=COUNT($_POST['BKReturDetailT']['obatalkes_id']);
                    for($i=0; $i<=$jumlahObat; $i++):
                       if($_POST['checkList'][$i]=='1'){
                            $jumlahCekList++;
                            $modReturDetails = new BKReturDetailT;
                            $modReturDetails->penerimaandetail_id=$_POST['BKReturDetailT']['penerimaandetail_id'][$i];
                            $modReturDetails->obatalkes_id=$_POST['BKReturDetailT']['obatalkes_id'][$i];
                            $modReturDetails->satuanbesar_id=$_POST['BKReturDetailT']['satuanbesar_id'][$i];
                            $modReturDetails->fakturdetail_id=$_POST['BKReturDetailT']['fakturdetail_id'][$i];
                            $modReturDetails->sumberdana_id=$_POST['BKReturDetailT']['sumberdana_id'][$i];
                            $modReturDetails->returpembelian_id=$modRetur->returpembelian_id;
                            $modReturDetails->satuankecil_id=$_POST['BKReturDetailT']['satuankecil_id'][$i];
                            $modReturDetails->jmlretur=$_POST['BKReturDetailT']['jmlretur'][$i];                       
                            $modReturDetails->harganettoretur=$_POST['BKReturDetailT']['harganettoretur'][$i];
                            $modReturDetails->hargappnretur=$_POST['BKReturDetailT']['hargappnretur'][$i];
                            $modReturDetails->hargapphretur=$_POST['BKReturDetailT']['hargapphretur'][$i];
                            $modReturDetails->jmldiscount=$_POST['BKReturDetailT']['jmldiscount'][$i];
                            $modReturDetails->hargasatuanretur=$_POST['BKReturDetailT']['hargasatuanretur'][$i];
                            
                            //ini digunakan untuk mendapatkan jumalah terima dari tabel faktur detail
                            $fd = FakturdetailT::model()->findByPk($modReturDetails->fakturdetail_id);
                            $idfd = $fd->fakturdetail_id;
                            $jum1 = $fd->jmlterima;
                            $jum2 = $modReturDetails->jmlretur;
                            $jumupdate = $jum1-$jum2;
                           
                            
                            if($modReturDetails->save()){
                                $jumlahSave++;
                                BKPenerimaanDetailT::model()->updateByPk($modReturDetails->penerimaandetail_id,
                                                                        array('returdetail_id'=>$modReturDetails->returdetail_id));
                                
                                //ini digunakan untuk mengupdata tabel faktur detail dan penerimaan detail ketika terjadi retur
                                FakturdetailT::model()->updateByPk($idfd, array('jmlterima'=>$jumupdate));
                                PenerimaandetailT::model()->updateByPk($modReturDetails->penerimaandetail_id, array('jmlterima'=>$jumupdate));
                                //========================================================
                                
                                $idStokObatAlkes=BKPenerimaanDetailT::model()->findByPk($modReturDetails->penerimaandetail_id)->stokobatalkes_id;
                                $stokObatAlkesIN=BKStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_in;
                                $stokCurrent=BKStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_current;
                                $stokINBaru=$stokObatAlkesIN - $modReturDetails->jmlretur;
                                $stokCurrentBaru=$stokCurrent - $modReturDetails->jmlretur;
                                BKStokObatAlkesT::model()->updateByPk($idStokObatAlkes,array('qtystok_in'=>$stokINBaru,
                                                                                             'qtystok_current'=>$stokCurrentBaru));
                            }
                    }
                    endfor;
                    
                 }

                 if(($jumlahCekList==$jumlahSave) and ($jumlahCekList>0)){
                     $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                        $tersimpan=true;
                     
                 }else{
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $transaction->rollback();
                 }
             }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }   
                    
            }
            
            $this->render($this->pathView.'retur',array('modFaktur'=>$modFaktur,
                            'modFakturDetail'=>$modFakturDetail,
                            'modRetur'=>$modRetur,
                            'modReturDetails'=>$modReturDetails,
                            'tersimpan'=>$tersimpan,
                            'modRekenings'=>$modRekenings
                        ));
        }
}
?>
