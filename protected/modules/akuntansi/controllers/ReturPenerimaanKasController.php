<?php

class ReturPenerimaanKasController extends SBaseController
{
        protected $successSave=true;
        
        public function actionIndex()
	{
            if(!empty($_GET['frame']) && !empty($_GET['idPenerimaan'])){
                $this->layout = 'frameDialog';
                $idPenerimaan = $_GET['idPenerimaan'];
                $modPenerimaan = AKPenerimaanUmumT::model()->findByPk($idPenerimaan);
                $modBuktiBayar = AKTandabuktibayarT::model()->findByPk($modPenerimaan->tandabuktibayar_id);
                
                $modBuktiKeluar = new AKTandabuktikeluarT;
                $modBuktiKeluar->tahun = date('Y');
                $modBuktiKeluar->namapenerima = $modBuktiBayar->darinama_bkm;
                $modBuktiKeluar->alamatpenerima = $modBuktiBayar->alamat_bkm;
                $modBuktiKeluar->untukpembayaran = 'Retur Tagihan Pasien';
                $modBuktiKeluar->nokaskeluar = Generator::noKasKeluar();
                $modBuktiKeluar->jmlkaskeluar = $modPenerimaan->totalharga;
                $modRetur = new AKReturPenerimaanUmumT;
                $modRetur->penerimaanumum_id = $modPenerimaan->penerimaanumum_id;
                $modRetur->tandabuktibayar_id = $modPenerimaan->tandabuktibayar_id;
            } else {
                $modPenerimaan = new AKPenerimaanUmumT;
                $modBuktiBayar = new AKTandabuktibayarT;
            }
            
            if(isset($_POST['AKReturPenerimaanUmumT'])){
                $idPenerimaan = $_POST['AKReturPenerimaanUmumT']['penerimaanumum_id'];
                $modPenerimaan = AKPenerimaanUmumT::model()->findByPk($idPenerimaan);
                $modBuktiBayar = AKTandabuktibayarT::model()->findByPk($modPenerimaan->tandabuktibayar_id);
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modRetur = $this->saveReturPenerimaan($_POST['AKReturPenerimaanUmumT']);
                    $modBuktiKeluar = $this->saveBuktiKeluar($_POST['AKTandabuktikeluarT'],$modRetur);
                    $this->updateTandaBuktiBayar($modPenerimaan->tandabuktibayar_id,$modRetur);
                    
                    $successSave = $this->successSave;
                    if($successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
		
            $this->render('index',array('modPenerimaan'=>$modPenerimaan,
                                        'modBuktiBayar'=>$modBuktiBayar,
                                        'modBuktiKeluar'=>$modBuktiKeluar,
                                        'modRetur'=>$modRetur));
	}
        
        protected function saveReturPenerimaan($postRetur)
        {
            $modRetur = new AKReturPenerimaanUmumT;
            $modRetur->attributes = $postRetur;
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if($modRetur->validate()){
                $modRetur->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modRetur;
        }
        
        protected function saveBuktiKeluar($postBuktiKeluar,$modRetur)
        {
            $modBuktiKeluar = new AKTandabuktikeluarT;
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->tglkaskeluar = $modRetur->tglreturumum;
            if($modBuktiKeluar->validate()){
                $modBuktiKeluar->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBuktiKeluar;
        }
        
        protected function updateTandaBuktiBayar($idBuktiBayar,$modRetur)
        {
            AKTandabuktibayarT::model()->updateByPk($idBuktiBayar, array('returpenerimaanumum_id'=>$modRetur->returpenerimaanumum_id));
        }

}