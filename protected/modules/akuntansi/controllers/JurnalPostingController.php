<?php

class JurnalPostingController extends SBaseController {

    protected $pathView = 'akuntansi.views.jurnalPosting.';
    public $success = true;
    public $is_action = 'insert';
    public $pesan = 'succes';

    public function actionIndexPosting() {
        $model = new AKJurnalrekeningblmpostingV('searchWithJoin');
        $format = new CustomFormat();
        $model->tgl_awal = date('Y-m-d H:i:s');
        $model->tgl_akhir = date('Y-m-d H:i:s');
        $model->jenisjurnal_id = 0;

        if (isset($_GET['AKJurnalrekeningblmpostingV'])) {
            $model->attributes = $_GET['AKJurnalrekeningblmpostingV'];
            $model->tgl_awal = $format->formatDateTimeMediumForDB($_GET['AKJurnalrekeningblmpostingV']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateTimeMediumForDB($_GET['AKJurnalrekeningblmpostingV']['tgl_akhir']);
        }

        if (isset($_POST['AKJurnalrekeningblmpostingV'])) {
            $data = array();
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $success = true;
                foreach ($_POST['AKJurnalrekeningblmpostingV'] as $key => $value) {
                    if ($value['cekList']) {
                        // echo $key."-".$value['cekList']."<br>";
                        $format = new CustomFormat();
                        $data[] = $value['jurnaldetail_id'];

                        $modPosting = new AKJurnalpostingT();
                        $modPosting->tgljurnalpost = date("Y-m-d H:i:s");
                        $modPosting->keterangan = $_POST['AKJurnalrekeningblmpostingV']['urianjurnal'];
                        $modPosting->create_time = date("Y-m-d H:i:s");
                        $modPosting->create_loginpemekai_id = Yii::app()->user->id;
                        $modPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                        if ($success = $modPosting->validate() && $success) {
                            $success = $modPosting->save() && $success;
                        } else {
                            $this->pesan = $modPosting->getErrors();
                        }

                        $parameter = array(
                            'jurnalposting_id' => $modPosting->jurnalposting_id,
                            'koreksi' => true,
                        );
                        $id = $key;
                        $updateDetail = true;
                        // $index = $val['jurnalrekening_id'];
                        if ($updateDetail == true && $this->success) {
                            $success = JurnaldetailT::model()->updateByPk($key, $parameter) && $success;
                        } else {
                            $this->pesan = "Jurnal Detail gagal di update";
                        }
                    }
                }

                $imp_jurnaldetail = implode(",", $data);
                if ($success) {

                    $dataBukuBesar = "SELECT jurnaldetail_t.rekening1_id, jurnaldetail_t.rekening2_id, jurnaldetail_t.rekening3_id, jurnaldetail_t.rekening4_id, rekening5_m.rekening5_id, rekening5_m.rekening5_nb, uraiantransaksi, sum(saldodebit) as debit, sum(saldokredit) as kredit, jurnalrekening_id, jurnaldetail_id from jurnaldetail_t join rekening5_m on jurnaldetail_t.rekening5_id = rekening5_m.rekening5_id where jurnaldetail_t.jurnalposting_id is not null and jurnaldetail_t.jurnaldetail_id in($imp_jurnaldetail) group by jurnaldetail_t.rekening1_id, jurnaldetail_t.rekening2_id, jurnaldetail_t.rekening3_id, jurnaldetail_t.rekening4_id, rekening5_m.rekening5_id, rekening5_m.rekening5_nb, uraiantransaksi, jurnalrekening_id, jurnaldetail_id  order by jurnaldetail_t.rekening1_id, jurnaldetail_t.rekening2_id, jurnaldetail_t.rekening3_id, jurnaldetail_t.rekening4_id, rekening5_m.rekening5_id, uraiantransaksi asc";
                    $bukubesar = Yii::app()->db->createCommand($dataBukuBesar)->queryAll();
                    foreach ($bukubesar as $key => $value) {
                        $modBukubesar = new BukubesarT();
                        $modBukubesar->rekening1_id = $value['rekening1_id'];
                        $modBukubesar->rekening2_id = $value['rekening2_id'];
                        $modBukubesar->rekening3_id = $value['rekening3_id'];
                        $modBukubesar->rekening4_id = $value['rekening4_id'];
                        $modBukubesar->rekening5_id = $value['rekening5_id'];
                        $modBukubesar->tglbukubesar = $modPosting->tgljurnalpost;
                        $modBukubesar->uraiantransaksi = $value['uraiantransaksi'];
                        $modBukubesar->saldodebit = $value['debit'];
                        $modBukubesar->saldokredit = $value['kredit'];
                        if ($value['rekening5_nb'] == "D")
                            $modBukubesar->saldoakhirberjalan = $value['debit'] - $value['kredit'];
                        else
                            $modBukubesar->saldoakhirberjalan = $value['kredit'] - $value['debit'];
                        $modBukubesar->create_time = date("Y-m-d H:i:s");
                        $modBukubesar->create_loginpemakai_id = Yii::app()->user->id;
                        $modBukubesar->create_ruangan = 111;
                        $jurnaldetail_id = $value['jurnaldetail_id'];
                        $modJurnalD = JurnaldetailT::model()->findByPk($jurnaldetail_id);
                        $posting_id = $modJurnalD->jurnalposting_id;
                        if ($success = $modBukubesar->validate() && $success) {
                            $success = $modBukubesar->save() && $success;
                            $success = JurnalpostingT::model()->updateByPk($posting_id, array('bukubesar_id' => $modBukubesar->bukubesar_id)) && $success;
                        }
                    }
                }

                if ($success) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('danger', '<strong>Gagal!</strong> Data gagal disimpan.');
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('danger', $exc->getMessage());
                $this->pesan = $exc;
                $this->success = false;
            }
        }

        $this->render($this->pathView . 'indexPosting', array('model' => $model, 'pathView' => $this->pathView, 'format' => $format));
    }

    public function actionIndex() {
        $model = new AKJurnalrekeningblmpostingV();
        $format = new CustomFormat();
        $model->tgl_awal = date('Y-m-d') . " 00:00:00";
        $model->tgl_akhir = date('Y-m-d H:i:s');
//        print_r(Yii::app()->controller->id);
//        $modJurnalRekening = new JurnalrekeningT;
//        $modRekenings = array();
//        if(isset($_POST['AKJurnalrekeningblmpostingV'])){
//            $transaction = Yii::app()->db->beginTransaction();
//            try {
//                $noUrut = 1;
//                foreach($_POST['AKJurnalrekeningblmpostingV'] AS $i => $post){
//                    if(isset($post['pilihRekening'])){
//                        if(strtolower($post['tm']) == 'tm'){
//                            $cekTindakanObat = TindakanpelayananT::model()->findByPk($post['tindakanpelayanan_id']);
//                            if(isset($cekTindakanObat)){
//                                if(empty($cekTindakanObat->jurnalrekening_id)){
//                                    $modJurnalRekening = $this->saveJurnalRekening();
//                                }else{
//                                    $modJurnalRekening = JurnalrekeningT::model()->findByPk($cekTindakanObat->jurnalrekening_id);
//                                }
//                                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, true);
//                                $cekTindakanObat->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                                $cekTindakanObat->update();
//                                $noUrut ++;
//                            }
//                        }else{
//                            $cekTindakanObat = ObatalkespasienT::model()->findByPk($post['tindakanpelayanan_id']);
//                            if(isset($cekTindakanObat)){
//                                if(empty($cekTindakanObat->jurnalrekening_id)){
//                                    $modJurnalRekening = $this->saveJurnalRekening();
//                                }else{
//                                    $modJurnalRekening = JurnalrekeningT::model()->findByPk($cekTindakanObat->jurnalrekening_id);
//                                }
//                                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, true);
//                                $cekTindakanObat->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                                $cekTindakanObat->update();
//                                $noUrut ++;
//                            }
//                        }
//                        if($modJurnalRekening && $modJurnalDetail && $cekTindakanObat){
//                            $this->success = $this->success && true;
//                        }else{
//                            $this->success = false;
//                        }
//                        //kembalikan nilai jika gagal disimpan
//                        $modRekenings[$i] = new AKJurnalrekeningblmpostingV;
//                        $modRekenings[$i]->attributes = $post;
//                        $modRekenings[$i]->nmrincianobyek = $post['nama_rekening'];
//                        $modPasien = PasienM::model()->findByPk($modRekenings[$i]->pasien_id);
//                        $modRekenings[$i]->namadepan = $modPasien->namadepan;
//                        $modRekenings[$i]->nama_pasien = $modPasien->nama_pasien;
//                      //  $modRekenings[$i]->no_rekam_medik = $modPasien->no_rekam_medik;
//                        $modPendaftaran = PendaftaranT::model()->findByPk($modRekenings[$i]->pendaftaran_id);
//                        $modRekenings[$i]->no_pendaftaran = $modPendaftaran->no_pendaftaran;
//                        $modRekenings[$i]->carabayar_nama = $modPendaftaran->carabayar->carabayar_nama;
//                        $modRekenings[$i]->penjamin_nama = $modPendaftaran->penjamin->penjamin_nama;
//                    }
//                }
//                if($this->success){
//                    $transaction->commit();
//                    Yii::app()->user->setFlash('success',"Posting Jurnal Berhasil");
//                    $this->refresh();
//                }else{
//                    $transaction->rollback();
//                }
//                    
//            }catch (Exception $exc) {
//                $transaction->rollback();
//                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
//            }
//            Yii::app()->user->setFlash('error',"Data Gagal disimpan. Silahkan pilih rekening dengan benar !");
//        }  

        $this->render($this->pathView . 'index', array('model' => $model, 'pathView' => $this->pathView, 'format' => $format));
    }

    public function actionGetDaftarRekening() {
        if (Yii::app()->request->isAjaxRequest) {
            parse_str($_REQUEST['data'], $data_parsing);
            $format = new CustomFormat;
            $model = new AKJurnalrekeningblmpostingV();
            $model->attributes = $data_parsing['AKJurnalrekeningblmpostingV'];
            //  $model->is_posting = 1;

            $model->tgl_awal = $format->formatDateTimeMediumForDB($data_parsing['AKJurnalrekeningblmpostingV']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateTimeMediumForDB($data_parsing['AKJurnalrekeningblmpostingV']['tgl_akhir']);

            // echo"<pre>";
            // print_r($model->tgl_akhir);
            // exit();
            // $jenis_rekeing = Params::JURNAL_PENERIMAAN_KAS;
            /*
              switch (Yii::app()->controller->id)
              {
              case "jurnalPengeluaranKas":
              $jenis_rekeing = Params::JURNAL_PENGELUARAN_KAS;
              break;
              case "jurnalPembelian":
              $jenis_rekeing = Params::JURNAL_PEMBELIAN;
              break;
              case "jurnalPelayanan":
              $jenis_rekeing = Params::JURNAL_PELAYANAN;
              break;
              case "jurnalPenjualan":
              $jenis_rekeing = Params::JURNAL_PENJUALAN;
              break;
              }
             * 
             */
            // $model->jenisjurnal_id = $jenis_rekeing;
            // $record = $model->searchByFilter();
            $criteria = new CDbCriteria;
            if (isset($model->jenisjurnal_id))
                $criteria->compare('jenisjurnal_id', $model->jenisjurnal_id);
            if (isset($model->nobuktijurnal))
                $criteria->compare('LOWER(nobuktijurnal)', strtolower($model->nobuktijurnal), true);
            if (isset($model->kodejurnal))
                $criteria->compare('LOWER(kodejurnal)', strtolower($model->kodejurnal), true);
            $criteria->addBetweenCondition('tglbuktijurnal', $model->tgl_awal, $model->tgl_akhir);
            // $criteria->addCondition("kdstruktur like '%%0%%' OR kdstruktur like '%%1%%' OR kdstruktur like '%%2%%' OR kdstruktur like '%%3%%' OR kdstruktur like '%%4%%' OR kdstruktur like '%%5%%' OR kdstruktur like '%%6%%' OR kdstruktur like '%%7%%' OR kdstruktur like '%%8%%' OR kdstruktur like '%%9%%'");
            $criteria->order = 'jurnaldetail_id';
            $dataPosting = AKJurnalrekeningblmpostingV::model()->findAll($criteria);
            // echo $model->tgl_awal."/".$model->tgl_akhir."/";
            // echo count($record->getData());
            // exit();

            $result = array();
            foreach ($dataPosting as $key => $val) {
                $attributes = $val->attributes;
                $attributes['tglbuktijurnal'] = date("d-m-Y", strtotime($val->tglbuktijurnal));
                $attributes['nobuktijurnal'] = $val->nobuktijurnal;
                $attributes['kodejurnal'] = $val->kodejurnal;
//                $attributes['urianjurnal'] = $val->urianjurnal;
                $attributes['urianjurnal'] = $val->uraiantransaksi;

                //        $criteria = new CDbCriteria;
                //        $criteria->compare('kdstruktur',$val->rekening1_id);
                //        $criteria->compare('kdkelompok',$val->rekening2_id);
                //        $criteria->compare('kdjenis',$val->rekening3_id);
                //        $criteria->compare('kdobyek',$val->rekening4_id);
                //        $criteria->compare('kdrincianobyek',$val->rekening5_id);
                //    //     $criteria->compare('jurnalrekening_id',$val->rekening5_id);
                // //      $rec_nama = AKRekeningakuntansiV::model()->find($criteria);
                //        $rec_nama = AKJurnalrekeningblmpostingV::model()->find($criteria);
                if (isset($val->kdrincianobyek)) {
                    $nama_rekening = $val->nmrincianobyek;
                    $kode_rekening = $val->kdstruktur . "-" . $val->kdkelompok . "-" . $val->kdjenis . "-" . $val->kdobyek . "-" . $val->kdrincianobyek;
                    $status_rekening = $val->nmrincianobyek;
                } else {
                    if (isset($val->kdobyek)) {
                        $nama_rekening = $val->nmobyek;
                        $kode_rekening = $val->kdstruktur . "-" . $val->kdkelompok . "-" . $val->kdjenis . "-" . $val->kdobyek;
                        $status_rekening = $val->nmobyek;
                    } else {
                        $nama_rekening = $val->nmjenis;
                        $kode_rekening = $val->kdstruktur . "-" . $val->kdkelompok . "-" . $val->kdjenis;
                        $status_rekening = $val->nmjenis;
                    }
                }


                $attributes['nama_rekening'] = $nama_rekening;
                // $attributes['kode_rekening'] = $kode_rekening;
                $attributes['kode_rekening'] = $val->kdstruktur . '-' . $val->kdkelompok . '-' . $val->kdjenis . '-' . $val->kdobyek . '-' . $val->kdrincianobyek;
                $attributes['saldo_normal'] = ($status_rekening == "D" ? "Debit" : "Kredit");
                $attributes['rekening1_id'] = $val->struktur_id;
                $attributes['rekening2_id'] = $val->kelompok_id;
                $attributes['rekening3_id'] = $val->jenis_id;
                $attributes['rekening4_id'] = $val->obyek_id;
                $attributes['rekening5_id'] = $val->rincianobyek_id;
//                $attributes['saldodebit'] = MyFunction::formatNumber($attributes['saldodebit']);
//                $attributes['saldokredit'] = MyFunction::formatNumber($attributes['saldokredit']);
//                $result[] = $val->attributes;
                $result[] = $attributes;
            }

            echo json_encode($result);
        }
        Yii::app()->end();
    }

    public function actionSimpanJurnalPosting() {
        if (Yii::app()->request->isAjaxRequest) {
            $format = new CustomFormat();
            parse_str($_REQUEST['data'], $data_parsing);
            $transaction = Yii::app()->db->beginTransaction();

            try {
                $record = $this->validasiTabular($data_parsing['AKJurnalrekeningblmpostingV']);

                if (count($record) > 0) {
                    $index = "";
                    foreach ($record as $key => $val) {
                        if ($index != $val['jurnalrekening_id']) {
                            $model = new AKJurnalpostingT();
                            $model->tgljurnalpost = date("Y-m-d H:i:s");
                            $model->keterangan = $val['urianjurnal'];
                            $model->create_time = date("Y-m-d H:i:s");
                            $model->create_loginpemekai_id = Yii::app()->user->id;
                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                            if ($model->validate()) {
                                $model->save();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            } else {
                                $this->pesan = $model->getErrors();
                                $this->success = false;
                            }
                        }
                        if (empty($val['uraiantransaksi'])) {
                            $uraian = "-";
                        } else {
                            $uraian = $val['uraiantransaksi'];
                        }
                        $parameter = array(
                            'uraiantransaksi' => $uraian,
                            // 'jurnalrekening_id' => $val['jurnalrekening_id'],
                            'jurnalposting_id' => $model->jurnalposting_id,
                            'koreksi' => true,
                            'rekening1_id' => $val['rekening1_id'],
                            'rekening2_id' => $val['rekening2_id'],
                            'rekening3_id' => $val['rekening3_id'],
                            'rekening4_id' => $val['rekening4_id'],
                            'rekening5_id' => $val['rekening5_id'],
                            'saldodebit' => $val['saldodebit'],
                            'nourut' => $val['nourut'],
                            'saldokredit' => $val['saldokredit']
                        );
                        $id = $val['jurnaldetail_id'];
                        $updateDetail = true;
                        $index = $val['jurnalrekening_id'];
                        if ($updateDetail == true) {
                            //                        echo "a";
                            //                        echo $id;$model->jurnalposting_id;exit;
                            $update = JurnaldetailT::model()->updateByPk($val['jurnaldetail_id'], $parameter);
                            //$update = $this->updateJurnalDetail($model,$parameter);
                        } else {
                            echo "Jurnal Detail gagal di update";
                        }
                    }
                }

                if ($this->success && $update) {
                    $transaction->commit();
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                echo $exc;
                $this->pesan = $exc;
                $this->success = false;
            }

            $result = array(
                'action' => $this->is_action,
                'pesan' => $this->pesan,
                'status' => ($this->success == true) ? 'ok' : 'not',
            );
            echo json_encode($result);
        }
        Yii::app()->end();
    }

    private function validasiTabular($params) {
        $result = array();
        $index = null;
        $i = 1;
        foreach ($params as $key => $val) {
            if ($val['is_checked'] == 1) {
                $result[] = $val;
            }
        }
        return $result;
    }

    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */

    public function saveJurnalRekening() {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "";
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if ($modJurnalRekening->validate()) {
            $modJurnalRekening->save();
        } else {
            $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
        }
        return $modJurnalRekening;
    }

    /**
     * simpan jurnaldetail_t dan jurnalposting_t digunakan di:
     * - akuntansi/JurnalPiutangPasien
     */
    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut = 0, $isPosting = false) {
        $modJurnalPosting = null;
        if ($isPosting == true) {
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if ($modJurnalPosting->validate()) {
                $modJurnalPosting->save();
            }
        }

        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->jurnaldetail_id = $post['jurnaldetail_id'];
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $post['nama_rekening'];
        $modJurnalDetail->saldodebit = $post['saldodebit'];
        $modJurnalDetail->saldokredit = $post['saldokredit'];
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";
        if ($modJurnalDetail->validate()) {
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;
    }

}
