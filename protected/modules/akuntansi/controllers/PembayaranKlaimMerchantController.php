<?php
class PembayaranKlaimMerchantController extends SBaseController
{
    public $layout='//layouts/column1';
    public $defaultAction = 'Index';
    public $pathView = 'akuntansi.views.pembayaranKlaimMerchant.';
        
    public function actionIndex()
    {
        $this->pageTitle = Yii::app()->name.' - '.'Transaksi Pembayaran Klaim Merchant';

        $modPembayaranKlaim=new AKPembayaranklaimT;
        $modPembayaranKlaimDetail = new AKPembayarklaimdetailT;
        $modTandabukti = new TandabuktibayarT;
        $modPembayaranPelayanan = new PembayaranpelayananT;
        $modPendaftaran = new AKPendaftaranT;
        $modPasien = new AKPasienM;
        $format = new CustomFormat();
        $modRekenings = array();

        $modTandabukti->tglAwal = date('Y-m-d 00:00:00');
        $modTandabukti->tglAkhir = date('Y-m-d 23:59:59');

        $modPembayaranKlaim->tglpembayaranklaim = date('Y-m-d H:i:s');
        $modPembayaranKlaim->nopembayaranklaim = Generator::noPembayaranKlaim();

        if (isset($_POST['AKPembayaranklaimT'])) {
            $modPembayaranKlaim->attributes = $_POST['AKPembayaranklaimT'];
            $modPembayaranKlaim->carabayar_id = 1;
            $modPembayaranKlaim->penjamin_id= 117;
            if (count($_POST['AKPembayarklaimdetailT']) > 0) {
                $pembayaranpelayanan_id = $this->sortPilih($_POST['AKPembayarklaimdetailT']);
                $modDetails = $this->validasiTabular($modPembayaranKlaim, $_POST['AKPembayarklaimdetailT']);

            }
                if ($modPembayaranKlaim->validate()) {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $success = true;
                        if ($modPembayaranKlaim->save()) {
                                $modDetails = $this->validasiTabular($modPembayaranKlaim, $_POST['AKPembayarklaimdetailT']);
//                                $nourut =1;
//                                foreach($_POST['RekeningpembayarankasirV'] AS $i => $post){
                                    $postRekenings = $_POST['RekeningpembayarankasirV'];
                                    if(isset($postRekenings)){
                                        $modJurnalRekening = $this->saveJurnalRekening();
                                        $saveDetailJurnal = $this->saveJurnalDetail($modJurnalRekening, $postRekenings, $nourut, false);
                                        $nourut++;
                                    }
//                                }

                                foreach ($modDetails as $i => $data) {
    //                                    echo '<pre>';
    //                                    echo print_r($data->jmlsisapiutang);
    //                                    echo exit();
                                    if ($data->tandabuktibayar_id > 0) {
                                        if ($data->save()) {
//                                                 PembayaranpelayananT::model()->updateByPk($data->pembayaranpelayanan_id, array('pembklaimdetal_id'=>$data->pembklaimdetal_id));
                                                 TandabuktibayarT::model()->updateByPk($data->tandabuktibayar_id, array('pembklaimdetail_id'=>$data->pembklaimdetal_id));

                                        } else {
                                            $success = false;
                                        }
                                        echo '<pre>';
                                        echo print_r($data->getErrors());
                                        echo '</pre>';
    //                                          exit();
                                    }
                                }
                        }
                        if ($success == true) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index', 'id' => $modPembayaranKlaim->pembayarklaim_id));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                        }
                    } catch (Exception $ex) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', "Data gagal disimpan ".$ex->getMessage());
                    }
            } else {
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
        }

        if ((isset($_GET['tglAwal'])) && (isset($_GET['tglAkhir']))) {
            if (Yii::app()->request->isAjaxRequest) {
                $tglAwal  = $format->formatDateTimeMediumForDB($_GET['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['tglAkhir']);
                $pembklaimdetal_id = $_GET['pembklaimdetal_id'];
                $bankkartu = $_GET['bankkartu'];

                $tr = $this->createList($tglAwal, $tglAkhir, $bankkartu, true);
                echo $tr;
                Yii::app()->end();
            }
        }
                
        $this->render($this->pathView.'index',array(
                'modPembayaranKlaim'=>$modPembayaranKlaim,
                'modPembayaranKlaimDetail'=>$modPembayaranKlaimDetail,
                'modPendaftaran'=>$modPendaftaran,
                'modPasien'=>$modPasien,
                'modPembayaranPelayanan'=>$modPembayaranPelayanan,
                'tr' => $tr,
                'modDetails'=>$modDetails,
                'modRekenings'=>$modRekenings,
                'modTandabukti'=>$modTandabukti,
//                    'pembayaran'=>$pembayaran,
        ));
    }

    protected function rowPengeluaran($pengeluaran, $totaltransaksi, $tr, $text=null) {
        if (count($pengeluaran) > 0) {
            foreach ($pengeluaran as $i => $row) {
               $i++;
               $totaltransaksi = count($pengeluaran);
               $jumlahPiutang = 0;
                if($row->pembayaran->pendaftaran->carabayar->issubsidiasuransi == true || $row->pembayaran->pendaftaran->carabayar->issubsidipemerintah == true || $row->pembayaran->pendaftaran->carabayar->issubsidirs == true){
                    $jumlahPiutang += $row->pembayaran->totalsubsidiasuransi + $row->pembayaran->totalsubsidipemerintah + $row->pembayaran->totalsubsidirs;
                }else if($row->pembayaran->pendaftaran->carabayar->issubsidiasuransi == true){
                    $jumlahPiutang += $row->pembayaran->totalsubsidiasuransi;
                }else if($row->pembayaran->pendaftaran->carabayar->issubsidipemerintah == true){
                    $jumlahPiutang += $row->pembayaran->totalsubsidipemerintah ;
                }else if($row->pembayaran->pendaftaran->carabayar->issubsidirs == true){
                    $jumlahPiutang += $row->pembayaran->totalsubsidirs;
                }
               
                $biayapelayanan += $row->pembayaran->totalbiayapelayanan;
                $jmltagihan = $row->jmlpembayaran;
                $jmlpiutang = $row->jmlbayardgnkartu;
                $jmltelahbayar = $row->uangditerima;
                $jmlbayar = $row->jmlbayardgnkartu;
                $sisatagihan = $jmlpiutang - $jmlbayar;
                
                $tr .= '<tr >';
                $tr .= '<td>'.$i.'</td>';
                $tr .= '<td>' . $row->pembayaran->pasien->no_rekam_medik."<br/>".$row->pembayaran->pendaftaran->no_pendaftaran . '</td>';
                $tr .= '<td>' . $row->pembayaran->pasien->nama_pasien . '</td>';
                $tr .= '<td>' . $row->pembayaran->pasien->alamat_pasien . '</td>';
                $tr .= '<td>' . $row->pembayaran->pendaftaran->penanggungJawab->nama_pj."-".$row->pembayaran->pendaftaran->penanggungJawab->pengantar . '</td>';
                $tr .= '<td>' . $row->pembayaran->nopembayaran . '</td>';
                if ($text == true){
                    $tr .= '<td>'.MyFunction::formatNumber($jmltagihan).'</td>';
                    $tr .= '<td>'.MyFunction::formatNumber($jmlpiutang).'</td>';
                    $tr .= '<td>'.MyFunction::formatNumber($jmltelahbayar).'</td>';
                    $tr .= '<td>'.MyFunction::formatNumber($jmlbayar).'</td>';
                    $tr .= '<td>'.MyFunction::formatNumber($sisatagihan).'</td>';
                }else{
                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltagihan]', MyFunction::formatNumber($jmltagihan), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltagihan currency ', 'readonly' => false,'onkeyup'=>'hitungSemuaTransaksi()','onblur'=>'hitungSemuaTransaksi()')) . '</td>';
//                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlpiutang]', (empty($row->pembklaimdetal_id) ? MyFunction::formatNumber($jumlahPiutang) : MyFunction::formatNumber($row->pembayaran->detailklaim->jmlpiutang)), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang currency ', 'onkeyup' => 'hitungJumlahPiutang(this);')) . 
                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlpiutang]', MyFunction::formatNumber($jmlpiutang), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang currency ', 'onkeyup' => 'hitungJumlahPiutang(this);','onblur' => 'hitungJumlahPiutang(this);')) . 
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][jmlpiutang2]', MyFunction::formatNumber($jmlpiutang), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang2 currency')) .
                           '</td>';
                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltelahbayar]', MyFunction::formatNumber($jmltelahbayar), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltelahbayar currency ', 'onkeyup' => 'hitungJumlahTelahBayar(this);')) . '</td>';
//                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltelahbayar]', (empty($row->pembklaimdetal_id) ? (empty($row->pembayaran->detailklaim->telahbayar) ? "0" : MyFunction::formatNumber($row->jmlpembayaran)) : MyFunction::formatNumber($row->detailklaim->jmltelahbayar)), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltelahbayar currency ', 'onkeyup' => 'hitungJumlahTelahBayar(this);')) . '</td>';
                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlbayar]',MyFunction::formatNumber($jmlbayar), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlbayar currency ', 'onkeyup' => 'hitungSisaTagihan(this);')) . '</td>';
//                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlbayar]', (empty($row->pembklaimdetal_id) ? MyFunction::formatNumber($row->jmlpembayaran) : MyFunction::formatNumber($row->pembayaran->detailklaim->jmlpiutang - $row->pembayaran->detailklaim->jmltelahbayar) ), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlbayar currency ', 'onkeyup' => 'hitungSisaTagihan(this);')) . '</td>';
                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlsisatagihan]',MyFunction::formatNumber($sisatagihan), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlsisatagihan currency ','onkeyup' => 'setAll(this);')). '</td>';
//                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlsisatagihan]',MyFunction::formatNumber($sisatagihan), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlsisatagihan currency ', 'onkeyup' => 'hitungSemuaTransaksi();')). '</td>';
//                    $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlsisatagihan]',(empty($row->pembklaimdetal_id) ? (empty($row->pembayaran->detailklaim->jmlsisapiutang) ? "0" : MyFunction::formatNumber($row->pembayaran->totalbiayapelayanan - $row->jmlpembayaran)) : MyFunction::formatNumber($row->pembayaran->detailklaim->jmlpiutang - ($row->pembayaran->detailklaim->jmltelahbayar + ($row->pembayaran->detailklaim->jmlpiutang - $row->pembayaran->detailklaim->jmltelahbayar)))) , array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlsisatagihan currency ', 'onkeyup' => 'hitungSemuaTransaksi();')). '</td>';

                    $tr .= '<td>' . CHtml::checkBox('AKPembayarklaimdetailT['.$i.'][cekList]', true, array('value'=>$row->pembayaranpelayanan_id,'class' => 'cek', 'onClick' => 'setAll();')) .
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pendaftaran_id]', $row->pembayaran->pendaftaran_id, array('style'=>'width:70px;','class' => 'inputFormTabel currency span3 jmlsisatagihan',)).
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pasien_id]', $row->pembayaran->pasien_id, array('style'=>'width:70px;','class' => 'inputFormTabel currency span3 jmlsisatagihan',)).
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pembayaranpelayanan_id]', $row->pembayaranpelayanan_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3 ')).
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][tandabuktibayar_id]', $row->tandabuktibayar_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][carabayar_id]', $row->pembayaran->carabayar_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).
                                    CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][penjamin_id]', $row->pembayaran->penjamin_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).

                           '</td>';

                }
                $tr .= '</tr>';
            }
        }
        return $tr;
    }
    
    protected function createList($tglAwal, $tglAkhir, $bankkartu,$status=null) {            
        $criteria = new CDbCriteria();

        $criteria->addBetweenCondition('t.tglbuktibayar', $tglAwal, $tglAkhir);
        $criteria->compare('LOWER(t.bankkartu)',strtolower($bankkartu)); 
        $criteria->addCondition('t.jmlbayardgnkartu > 0'); 
        $criteria->addCondition('t.pembklaimdetail_id is null'); 

        $pengeluaran = TandabuktibayarT::model()->findAll($criteria);

        $tr = $this->rowPengeluaran($pengeluaran, $data['totaltransaksi'], $data['tr']);

        return $tr;
    }

    protected function sortPilih($data){
        $result = array();
        foreach ($data as $i=>$row){
            if ($row['cekList'] == 1){
                $result[] = $row['tandabuktibayar_id'];
            }
        }

        return $result;
    }
        
    
    protected function validasiTabular($modPembayaranKlaim, $data) {
        foreach ($data as $i => $row) {
            if($row['cekList'] == 1){
                
                $modDetails[$i] = new AKPembayarklaimdetailT();
                $modDetails[$i]->attributes = $row;                
                $modDetails[$i]->pendaftaran_id = $row['pendaftaran_id'];
                $modDetails[$i]->pasien_id = $row['pasien_id'];
                $modDetails[$i]->pembayarklaim_id = $modPembayaranKlaim->pembayarklaim_id;
                $modDetails[$i]->pembayaranpelayanan_id = $row['pembayaranpelayanan_id'];
                $modDetails[$i]->tandabuktibayar_id = $row['tandabuktibayar_id'];
                $modDetails[$i]->jmlpiutang = $row['jmlpiutang']-$row['jmlbayar'];
                $modDetails[$i]->jumlahbayar = $row['jmlbayar'];
                $modDetails[$i]->jmltelahbayar = $row['jmlbayar'];
                $modDetails[$i]->jmlsisapiutang = $row['jmlsisatagihan'];
                $modDetails[$i]->validate();
            }
            
//            echo '<pre>';
//            echo print_r($modDetails[$i]->getErrors());
//            echo '</pre>';
        }

        return $modDetails;
    }
    
    /**
     * simpan jurnaldetail_t dan jurnalposting_t digunakan di:
     * - akuntansi/pembayaranKlaimMerchant
     */
    public function saveJurnalDetail($modJurnalRekening, $postRekenings, $noUrut=0, $isPosting = false){
        $modJurnalPosting = null;
        if($isPosting == true){
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
            }
        }
        
        foreach($postRekenings AS $i => $post){
            $modJurnalDetail[$i] = new JurnaldetailT();
            $modJurnalDetail[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $modJurnalDetail[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $modJurnalDetail[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $modJurnalDetail[$i]->uraiantransaksi = $post['nama_rekening'];
            $modJurnalDetail[$i]->saldodebit = $post['saldodebit'];
            $modJurnalDetail[$i]->saldokredit = $post['saldokredit'];
            $modJurnalDetail[$i]->nourut = $i+1;
            $modJurnalDetail[$i]->rekening1_id = $post['struktur_id'];
            $modJurnalDetail[$i]->rekening2_id = $post['kelompok_id'];
            $modJurnalDetail[$i]->rekening3_id = $post['jenis_id'];
            $modJurnalDetail[$i]->rekening4_id = $post['obyek_id'];
            $modJurnalDetail[$i]->rekening5_id = $post['rincianobyek_id'];
            $modJurnalDetail[$i]->catatan = "";
            if($modJurnalDetail[$i]->validate()){
                $modJurnalDetail[$i]->save();
            }
        }
        return $modJurnalDetail;   
                   
    }
    
     /**
    * simpan jurnalrekening_t
    * @return \JurnalrekeningT
    */
    public function saveJurnalRekening()
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "";
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
        } else {
            $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
        }
        return $modJurnalRekening;
    }
}
?>
