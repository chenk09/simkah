<?php

class JurnalPiutangPasienController extends SBaseController
{
    public $success = true; //true karena di looping
    
    public function actionIndex(){
        $model = new AKRincianpiutangrekeningpasienV();
        $format = new CustomFormat();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $modJurnalRekening = new JurnalrekeningT;
        $modRekenings = array();
        if(isset($_POST['AKRincianpiutangrekeningpasienV'])){
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $noUrut = 1;
                foreach($_POST['AKRincianpiutangrekeningpasienV'] AS $i => $post){
                    if(isset($post['pilihRekening'])){
                        if(strtolower($post['tm']) == 'tm'){
                            $cekTindakanObat = TindakanpelayananT::model()->findByPk($post['tindakanpelayanan_id']);
                            if(isset($cekTindakanObat)){
                                if(empty($cekTindakanObat->jurnalrekening_id)){
                                    $modJurnalRekening = $this->saveJurnalRekening();
                                }else{
                                    $modJurnalRekening = JurnalrekeningT::model()->findByPk($cekTindakanObat->jurnalrekening_id);
                                }
                                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, true);
                                $cekTindakanObat->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                $cekTindakanObat->update();
                                $noUrut ++;
                            }
                        }else{
                            $cekTindakanObat = ObatalkespasienT::model()->findByPk($post['tindakanpelayanan_id']);
                            if(isset($cekTindakanObat)){
                                if(empty($cekTindakanObat->jurnalrekening_id)){
                                    $modJurnalRekening = $this->saveJurnalRekening();
                                }else{
                                    $modJurnalRekening = JurnalrekeningT::model()->findByPk($cekTindakanObat->jurnalrekening_id);
                                }
                                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, true);
                                $cekTindakanObat->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                $cekTindakanObat->update();
                                $noUrut ++;
                            }
                        }
                        if($modJurnalRekening && $modJurnalDetail && $cekTindakanObat){
                            $this->success = $this->success && true;
                        }else{
                            $this->success = false;
                        }
                        //kembalikan nilai jika gagal disimpan
                        $modRekenings[$i] = new AKRincianpiutangrekeningpasienV;
                        $modRekenings[$i]->attributes = $post;
                        $modRekenings[$i]->nmrincianobyek = $post['nama_rekening'];
                        $modPasien = PasienM::model()->findByPk($modRekenings[$i]->pasien_id);
                        $modRekenings[$i]->namadepan = $modPasien->namadepan;
                        $modRekenings[$i]->nama_pasien = $modPasien->nama_pasien;
                        $modRekenings[$i]->no_rekam_medik = $modPasien->no_rekam_medik;
                        $modPendaftaran = PendaftaranT::model()->findByPk($modRekenings[$i]->pendaftaran_id);
                        $modRekenings[$i]->no_pendaftaran = $modPendaftaran->no_pendaftaran;
                        $modRekenings[$i]->carabayar_nama = $modPendaftaran->carabayar->carabayar_nama;
                        $modRekenings[$i]->penjamin_nama = $modPendaftaran->penjamin->penjamin_nama;
                    }
                }
                if($this->success){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Posting Jurnal Berhasil");
                    $this->refresh();
                }else{
                    $transaction->rollback();
                }
                    
            }catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
            }
            Yii::app()->user->setFlash('error',"Data Gagal disimpan. Silahkan pilih rekening dengan benar !");
        }       
        
        $this->render('index', array('model'=>$model, 'modRekenings'=>$modRekenings));
    }
    
    /**
    * simpan jurnalrekening_t
    * @return \JurnalrekeningT
    */
    public function saveJurnalRekening()
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "";
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
        } else {
            $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
        }
        return $modJurnalRekening;
    }
    /**
     * simpan jurnaldetail_t dan jurnalposting_t digunakan di:
     * - akuntansi/JurnalPiutangPasien
     */
    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $isPosting = false){
        $modJurnalPosting = null;
        if($isPosting == true){
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
            }
        }
        
        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $post['nama_rekening'];
        $modJurnalDetail->saldodebit = $post['saldodebit'];
        $modJurnalDetail->saldokredit = $post['saldokredit'];
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";
        if($modJurnalDetail->validate()){
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;        
    }
}