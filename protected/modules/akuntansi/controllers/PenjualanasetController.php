<?php

class PenjualanasetController extends SBaseController
{
        protected $succesSave = true;
        protected $successSave = true;
        protected $pesan = "succes";
        protected $is_action = "insert";
        public $pathView = 'akuntansi.views.penjualanAset.';
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	public function actionIndex($id=null)
	{
		$modAset = new InvperalatanT;

		if(isset($_POST['InvperalatanT'])){
	        $transaction = Yii::app()->db->beginTransaction();
	        try {

	        	foreach ($_POST['InvperalatanT'] as $key => $value) {
	        		if($value['is_checked']){
	        			$id 	= $value['invperalatan_id'];
	        			$model 	= InvperalatanT::model()->findByPk($id);;
	        			$model->tipepenghapusan = "penjualan telah dijurnal";
	        			$model->save();
	        		}
	        	}
                if(isset($_POST['JenispengeluaranrekeningV'])){
                    
                    //=========== Save Jurnal Rekening =================
                    $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['InvperalatanT']);
                    // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                    // {
                    //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    // }else{
                    //     $modJurnalPosting = null;
                    // }
                    $noUrut = 0;
                    foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
                        $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                        $noUrut ++;
                    }
                    //==================================================
                    
                }

                if($this->succesSave){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->redirect(array('index'));
                    $model->isNewRecord = false;
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }

	        } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
	    }

		$this->render($this->pathView.'index',array('modAset'=>$modAset));
	}

    public function actionIndexTanah($id=null)
    {
        $modAset = new InvtanahT;

        if(isset($_POST['InvtanahT'])){
            $transaction = Yii::app()->db->beginTransaction();
            try {

                foreach ($_POST['InvtanahT'] as $key => $value) {
                    if($value['is_checked']){
                        $id     = $value['invtanah_id'];
                        $model  = InvtanahT::model()->findByPk($id);;
                        $model->tipepenghapusan = "penjualan telah dijurnal";
                        $model->save();
                    }
                }
                if(isset($_POST['JenispengeluaranrekeningV'])){
                    
                    //=========== Save Jurnal Rekening =================
                    $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['InvtanahT']);
                    // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                    // {
                    //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    // }else{
                    //     $modJurnalPosting = null;
                    // }
                    $noUrut = 0;
                    foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
                        $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                        $noUrut ++;
                    }
                    //==================================================
                }

                if($this->succesSave){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->redirect(array('indexTanah'));
                    $model->isNewRecord = false;
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }

            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
        }

        $this->render($this->pathView.'indexTanah',array('modAset'=>$modAset));
    }
    
    public function actionGetDataRekeningByJnsPengeluaran()
    {
        if(Yii::app()->getRequest()->getIsAjaxRequest())
        {
            $jenispengeluaran_id = $_POST['jenispengeluaran_id'];
            $criteria = new CDbCriteria;
//            $criteria->select = '*, jnspengeluaranrek_m.saldonormal';
//            $criteria->join = '
//                JOIN jnspengeluaranrek_m ON 
//                    jnspengeluaranrek_m.rekening1_id = t.struktur_id AND
//                    jnspengeluaranrek_m.rekening2_id = t.kelompok_id AND
//                    jnspengeluaranrek_m.rekening3_id = t.jenis_id AND
//                    jnspengeluaranrek_m.rekening4_id = t.obyek_id AND
//                    jnspengeluaranrek_m.rekening5_id = t.rincianobyek_id                
//            ';
            $criteria->condition = 'jenispengeluaran_id = :jenispengeluaran_id';
            $criteria->order ='jenispengeluaran_id,saldonormal';
            $criteria->params = array(':jenispengeluaran_id'=>$jenispengeluaran_id);
//            $model = RekeningakuntansiV::model()->findAll($criteria);
             $model = JenispengeluaranrekeningV::model()->findAll($criteria);
            if($model)
            {
                echo CJSON::encode(
//                    $this->renderPartial('__formKodeRekening', array('model'=>$model), true)
                    $this->renderPartial('akuntansi.views.penjualanAset._formKodeRekening', array('model'=>$model), true)
                );                
            }
            Yii::app()->end();
        }        
    }

    public function actionIndexGedung($id=null)
    {
        $modAset = new InvgedungT;

        if(isset($_POST['InvgedungT'])){
            $transaction = Yii::app()->db->beginTransaction();
            try {

                foreach ($_POST['InvgedungT'] as $key => $value) {
                    if($value['is_checked']){
                        $id     = $value['invgedung_id'];
                        $model  = InvgedungT::model()->findByPk($id);;
                        $model->tipepenghapusan = "penjualan telah dijurnal";
                        $model->save();
                    }
                }
                if(isset($_POST['JenispengeluaranrekeningV'])){
                    
                    //=========== Save Jurnal Rekening =================
                    $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['InvgedungT']);
                    // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                    // {
                    //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    // }else{
                    //     $modJurnalPosting = null;
                    // }
                    $noUrut = 0;
                    foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
                        $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                        $noUrut ++;
                    }
                    //==================================================
                }

                if($this->succesSave){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->redirect(array('indexGedung'));
                    $model->isNewRecord = false;
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }

            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
        }

        $this->render($this->pathView.'indexGedung',array('modAset'=>$modAset));
    }
    
    public function actionIndexPeralatanNonMedis($id=null)
	{
		$modAset = new InvperalatanT;

		if(isset($_POST['InvperalatanT'])){
	        $transaction = Yii::app()->db->beginTransaction();
	        try {

	        	foreach ($_POST['InvperalatanT'] as $key => $value) {
	        		if($value['is_checked']){
	        			$id 	= $value['invperalatan_id'];
	        			$model 	= InvperalatanT::model()->findByPk($id);;
	        			$model->tipepenghapusan = "penjualan telah dijurnal";
	        			$model->save();
	        		}
	        	}
                if(isset($_POST['JenispengeluaranrekeningV'])){
                    
                    //=========== Save Jurnal Rekening =================
                    $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['InvperalatanT']);
                    // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                    // {
                    //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    // }else{
                    //     $modJurnalPosting = null;
                    // }
                    $noUrut = 0;
                    foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
                        $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                        $noUrut ++;
                    }
                    //==================================================
                    
                }

                if($this->succesSave){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->redirect(array('indexPeralatanNonMedis'));
                    $model->isNewRecord = false;
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }

	        } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
	    }

		$this->render($this->pathView.'indexPeralatanNonMedis',array('modAset'=>$modAset));
	}

    public function actionIndexKendaraan($id=null)
    {
        $modAset = new InvperalatanT;

        if(isset($_POST['InvperalatanT'])){
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST['InvperalatanT'] as $key => $value) {
                    if($value['is_checked']){
                        $id     = $value['invperalatan_id'];
                        $model  = InvperalatanT::model()->findByPk($id);;
                        $model->tipepenghapusan = "penjualan telah dijurnal";
                        $model->save();
                    }
                }
                if(isset($_POST['JenispengeluaranrekeningV'])){
                    //=========== Save Jurnal Rekening =================
                    $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['InvperalatanT']);
                    // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                    // {
                    //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    // }else{
                    //     $modJurnalPosting = null;
                    // }
                    $noUrut = 0;
                    foreach($_POST['JenispengeluaranrekeningV'] AS $i => $post){
                        $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                        $noUrut ++;
                    }
                    //==================================================
                }

                if($this->succesSave){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $this->redirect(array('indexKendaraan'));
                    $model->isNewRecord = false;
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                }

            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
        }
        $this->render($this->pathView.'indexKendaraan',array('modAset'=>$modAset));
    }

	protected function saveJurnalRekening($model, $postPenUmum)
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');//$model->tglpenghapusan;
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');//$model->tglpenghapusan;
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "Penjualan Aset ".date('Y-m-d H:i:s');//$model->tglpenghapusan;
        
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');//$model->tglpenghapusan;
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modJurnalRekening->ruangan_id = Yii::app()->user->getState('ruangan_id');
        
        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalRekening->getErrors();
        }

        return $modJurnalRekening;
    }

    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
        $modJurnalDetail->saldodebit = $post['saldodebit'];
        $modJurnalDetail->saldokredit = $post['saldokredit'];
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";

        if($modJurnalDetail->validate()){
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;        
    }

    protected function saveJurnalPosting($arrJurnalPosting)
    {
        $modJurnalPosting = new JurnalpostingT;
        $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
        $modJurnalPosting->keterangan = "Posting automatis";
        $modJurnalPosting->create_time = date('Y-m-d H:i:s');
        $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
        $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
        if($modJurnalPosting->validate()){
            $modJurnalPosting->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalPosting->getErrors();
        }
        return $modJurnalPosting;
    } 
}