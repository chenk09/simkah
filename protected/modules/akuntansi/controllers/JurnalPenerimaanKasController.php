<?php

class JurnalPenerimaanKasController extends SBaseController
{
    protected $pathView = 'akuntansi.views.jurnalPenerimaanKas.';
    
    public $success = true;
    public $is_action = 'insert';
    public $pesan = 'succes';
    
    
    public function actionIndex()
    {
        $format = new CustomFormat();
        $model = new AKJurnalrekeningT();        
        $model->tgl_awal = date('d M Y');
        $model->tgl_akhir = date('d M Y');
//        print_r(Yii::app()->controller->id);        
        if(isset($_GET['AKJurnalrekeningT'])){
            $model->attributes=$_GET['AKJurnalrekeningT'];
            $model->tgl_awal = $format->formatDateMediumForDB($_GET['AKJurnalrekeningT']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateMediumForDB($_GET['AKJurnalrekeningT']['tgl_akhir']);                      
        }        
        
        $this->render($this->pathView.'index', array('model'=>$model, 'pathView'=>$this->pathView));
    }
    
    public function actionGetDaftarRekening()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            parse_str($_REQUEST['data'], $data_parsing);
            
            $format = new CustomFormat();
            $model = new AKJurnaldetailT();            
            $model->attributes = $data_parsing['AKJurnalrekeningT'];
            $model->is_posting = 1;
            $model->tgl_awal = $format->formatDateMediumForDB($data_parsing['AKJurnalrekeningT']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateMediumForDB($data_parsing['AKJurnalrekeningT']['tgl_akhir']);
            
            $jenis_rekeing = Params::JURNAL_PENERIMAAN_KAS;
            /*
            switch (Yii::app()->controller->id)
            {
                case "jurnalPengeluaranKas": 
                    $jenis_rekeing = Params::JURNAL_PENGELUARAN_KAS;
                    break;
                case "jurnalPembelian":
                    $jenis_rekeing = Params::JURNAL_PEMBELIAN;
                    break;
                case "jurnalPelayanan":
                    $jenis_rekeing = Params::JURNAL_PELAYANAN;
                    break;
                case "jurnalPenjualan":
                    $jenis_rekeing = Params::JURNAL_PENJUALAN;
                    break;
            }
             * 
             */
            $model->jenisjurnal_id = $jenis_rekeing;
            
            $record = $model->searchWithJoin();
            $result = array();
            foreach($record->getData() as $key=>$val)
            {
                $attributes = $val->attributes;
                $attributes['tglbuktijurnal'] = date("d-m-Y", strtotime($val->jurnalRekening->tglbuktijurnal));
                $attributes['nobuktijurnal'] = $val->jurnalRekening->nobuktijurnal;
                $attributes['kodejurnal'] = $val->jurnalRekening->kodejurnal;
                $attributes['urianjurnal'] = $val->jurnalRekening->urianjurnal;
                
                $criteria = new CDbCriteria;
                $criteria->compare('struktur_id',$val->rekening1_id);
                $criteria->compare('kelompok_id',$val->rekening2_id);
                $criteria->compare('jenis_id',$val->rekening3_id);
                $criteria->compare('obyek_id',$val->rekening4_id);
                $criteria->compare('rincianobyek_id',$val->rekening5_id);
                $rec_nama = AKRekeningakuntansiV::model()->find($criteria);
                
                if(isset($rec_nama['rincianobyek_id']))
                {
                    $nama_rekening = $rec_nama['nmrincianobyek'];
                    $kode_rekening = $rec_nama['kdstruktur'] . "-" . $rec_nama['kdkelompok'] . "-" . $rec_nama['kdjenis'] . "-" . $rec_nama['kdobyek'] . "-" . $rec_nama['kdrincianobyek'];
                    $status_rekening = $rec_nama['rincianobyek_nb'];
                }else{
                    if(isset($rec_nama['obyek_id']))
                    {
                        $nama_rekening = $rec_nama['nmobyek'];
                        $kode_rekening = $rec_nama['kdstruktur'] . "-" . $rec_nama['kdkelompok'] . "-" . $rec_nama['kdjenis'] . "-" . $rec_nama['kdobyek'];
                        $status_rekening = $rec_nama['obyek_nb'];
                    }else{
                        $nama_rekening = $rec_nama['nmjenis'];
                        $kode_rekening = $rec_nama['kdstruktur'] . "-" . $rec_nama['kdkelompok'] . "-" . $rec_nama['kdjenis'];
                        $status_rekening = $rec_nama['jenis_nb'];
                    }
                }
                $attributes['nama_rekening'] = $nama_rekening;
                $attributes['kode_rekening'] = $kode_rekening;
                $attributes['saldo_normal'] = ($status_rekening == "D" ? "Debit" : "Kredit");
//                $attributes['saldodebit'] = MyFunction::formatNumber($attributes['saldodebit']);
//                $attributes['saldokredit'] = MyFunction::formatNumber($attributes['saldokredit']);
                
//                $result[] = $val->attributes;
                $result[] = $attributes;
            }
            echo json_encode($result);
        }
        Yii::app()->end();
    }
    
    public function actionSimpanJurnalPosting()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $format = new CustomFormat();
            parse_str($_REQUEST['data'], $data_parsing);
            $transaction = Yii::app()->db->beginTransaction();
            
            try{
                $record = $this->validasiTabular($data_parsing['AKJurnalrekeningT']);
                if(count($record) > 0)
                {
                    $index = "";
                    foreach($record as $key=>$val)
                    {
                        if($index != $val['jurnalrekening_id'])
                        {
                            $model = new AKJurnalpostingT();
                            $model->tgljurnalpost = date("Y-m-d H:i:s");
                            $model->keterangan = $val['urianjurnal'];
                            $model->create_time = date("Y-m-d H:i:s");
                            $model->create_loginpemekai_id = Yii::app()->user->id;
                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                            if($model->validate())
                            {
                                $model->save();
                            }else{
                                $this->pesan = $model->getErrors();
                                $this->success = false;
                            }
                        }
                        $parameter = array(
                            'jurnalposting_id' => $model->jurnalposting_id,
                            'koreksi' => true,
                            'rekening1_id' => $val['rekening1_id'],
                            'rekening2_id' => $val['rekening2_id'],
                            'rekening3_id' => $val['rekening3_id'],
                            'rekening4_id' => $val['rekening4_id'],
                            'rekening5_id' => $val['rekening5_id'],
                            'saldodebit' => $val['saldodebit'],
                            'saldokredit' => $val['saldokredit']
                        );
                        $update = AKJurnaldetailT::model()->updateByPk($val['jurnaldetail_id'], $parameter);
                        $index = $val['jurnalrekening_id'];
                    }
                }
                
                if($this->success)
                {
                    $transaction->commit();
                }
                
            }catch(Exception $exc){
                $transaction->rollback();
                print_r($exc);
                $this->pesan = $exc;
                $this->success = false;
            }
            
            $result = array(
                'action' => $this->is_action,
                'pesan' => $this->pesan,
                'status' => ($this->success == true) ? 'ok' : 'not',
            );
            echo json_encode($result);            
            
        }
        Yii::app()->end();
    }
    
    private function validasiTabular($params)
    {
        $result = array();
        $index = null;
        $i=1;
        foreach ($params as $key=>$val)
        {
            if($val['is_checked'] == 1)
            {
                $result[] = $val;
            }
        }
        return $result;
    }
    
    
    

    // Uncomment the following methods and override them if needed
    /*
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
                'inlineFilterName',
                array(
                        'class'=>'path.to.FilterClass',
                        'propertyName'=>'propertyValue',
                ),
        );
    }

    public function actions()
    {
        // return external action classes, e.g.:
        return array(
                'action1'=>'path.to.ActionClass',
                'action2'=>array(
                        'class'=>'path.to.AnotherActionClass',
                        'propertyName'=>'propertyValue',
                ),
        );
    }
    */
}