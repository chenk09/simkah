
<?php

class RekeningPenerimaanController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathJenisPenerimaan= 'akuntansi.views.jurnalRekPenerimaan.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','ubahRekeningKredit','ubahRekeningDebit','penerimaan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new AKJnsPenerimaanRekM();

		if(isset($_POST['AKJnsPenerimaanRekM']))
		{
                        if (count($_POST['AKJnsPenerimaanRekM']) > 0) {
                            $modDetails = $this->validasiTabular($_POST['AKJnsPenerimaanRekM']);
                        }
                            $transaction = Yii::app()->db->beginTransaction();
                            try{
                                 $success = true;
                                 $modDetails = $this->validasiTabular($_POST['AKJnsPenerimaanRekM']);
                                    foreach ($modDetails as $i => $data) {
                                        if ($data->jnspenerimaanrek_id > 0) {
                                            if ($data->update()) {
                                                $success = true;
                                                
                                            } else {
                                                $success = false;
                                            }
                                        }else{
                                            $data->save();
                                        }
                                    }
                                if ($success == true) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                    $this->redirect(array('admin','id'=>'1'));
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                                }
                                
                            }
                            catch(Exception $ex){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
                            }

		}

		$this->render('create',array(
			'model'=>$model, 'modDetails'=>$modDetails,
		));
	}
        
        protected function validasiTabular($data){
            sort($data['rekening']);
            foreach ($data['rekening'] as $i=>$row){
                  if($row['rekening1_id'] > 0)
                {
                    $modDetails[$i] = new AKJnsPenerimaanRekM();
                    $modDetails[$i]->attributes = $row;                
                    $modDetails[$i]->rekening1_id = $row['rekening1_id'];
                    $modDetails[$i]->rekening2_id = $row['rekening2_id'];
                    $modDetails[$i]->rekening3_id = $row['rekening3_id'];
                    $modDetails[$i]->rekening4_id = $row['rekening4_id'];
                    $modDetails[$i]->rekening5_id = $row['rekening5_id'];
                    $modDetails[$i]->saldonormal = $row['saldonormal'];
                    $modDetails[$i]->jenispenerimaan_id = $_POST['AKJnsPenerimaanRekM']['jenispenerimaan_id'];
                    $modDetails[$i]->validate();
                }
            }
            return $modDetails;
        }
                
        public function actionAdmin(){
           
          $model = AKJenispenerimaanM;
          
          $this->redirect(Yii::app()->createUrl('akuntansi/jurnalRekPenerimaan/admin')); 
       }
       
       public function actionUbahRekeningDebit($id)
       {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		 $this->layout = 'frameDialog';
                $model= AKJnsPenerimaanRekM::model()->findByPk($id);
                $modPenerimaan = AKJenispenerimaanM::model()->findByPk($model->jenispenerimaan_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKJnsPenerimaanRekM']))
		{
                        $model->attributes=$_POST['AKJnsPenerimaanRekM'];
                        $view = 'UbahRekeningDebit';
                           
                        $update = AKJnsPenerimaanRekM::model()->updateByPk($id,array('rekening5_id'=>$_POST['AKJnsPenerimaanRekM']['rekening5_id'],
                                                                                'rekening4_id'=>$_POST['AKJnsPenerimaanRekM']['rekening4_id'],
                                                                                'rekening3_id'=>$_POST['AKJnsPenerimaanRekM']['rekening3_id'],
                                                                                'rekening2_id'=>$_POST['AKJnsPenerimaanRekM']['rekening2_id'],
                                                                                'rekening1_id'=>$_POST['AKJnsPenerimaanRekM']['rekening1_id']));
			if($update){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				if (isset($_GET['frame']) && !empty($_GET['idPenerimaan'])){
                                    $this->redirect(array(((isset($view)) ? $view : 'UbahRekeningDebit'),'id'=>$model->jnspenerimaanrek_id, 'frame'=>$_GET['frame'], 'idPenerimaan'=>$_GET['idPenerimaan']));
                                }else{
                                    $this->redirect(array(((isset($view)) ? $view : 'admin'),'id'=>$model->jenispenerimaan_id));
                                }
                        }
                }

		$this->render(((isset($view)) ? $view : '_ubahRekeningDebit'),array(
			'model'=>$model,
                        'modPenerimaan'=>$modPenerimaan
		));
	}
        public function actionUbahRekeningKredit($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		 $this->layout = 'frameDialog';
                $model= AKJnsPenerimaanRekM::model()->findByPk($id);
                $modPenerimaan = AKJenispenerimaanM::model()->findByPk($model->jenispenerimaan_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKJnsPenerimaanRekM']))
		{
                        $model->attributes=$_POST['AKJnsPenerimaanRekM'];
                        $view = 'UbahRekeningKredit';
                           
                        $update = AKJnsPenerimaanRekM::model()->updateByPk($id,array('rekening5_id'=>$_POST['AKJnsPenerimaanRekM']['rekening5_id'],
                                                                                'rekening4_id'=>$_POST['AKJnsPenerimaanRekM']['rekening4_id'],
                                                                                'rekening3_id'=>$_POST['AKJnsPenerimaanRekM']['rekening3_id'],
                                                                                'rekening2_id'=>$_POST['AKJnsPenerimaanRekM']['rekening2_id'],
                                                                                'rekening1_id'=>$_POST['AKJnsPenerimaanRekM']['rekening1_id']));
			if($update){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				if (isset($_GET['frame']) && !empty($_GET['idPenerimaan'])){
                                    $this->redirect(array(((isset($view)) ? $view : 'UbahRekeningKredit'),'id'=>$model->jnspenerimaanrek_id, 'frame'=>$_GET['frame'], 'idPenerimaan'=>$_GET['idPenerimaan']));
                                }else{
                                    $this->redirect(array(((isset($view)) ? $view : 'admin'),'id'=>$model->jenispenerimaan_id));
                                }
                        }
                }

		$this->render(((isset($view)) ? $view : '_ubahRekeningKredit'),array(
			'model'=>$model,
                        'modPenerimaan'=>$modPenerimaan
		));
	}
      
}
