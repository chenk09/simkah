<?php
Yii::import('billingKasir.models.*');
class FakturPembelianUmumController extends SBaseController
{
	public function actionIndex()
	{
        $modFaktur = new InformasifakturgudangumumV;
        $format = new CustomFormat();
        $modFaktur->tglAwal=date('Y-m-d')." 00:00:00";
        $modFaktur->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['InformasifakturgudangumumV'])){
            $modFaktur->attributes=$_GET['InformasifakturgudangumumV'];
            $modFaktur->tglAwal = $format->formatDateTimeMediumForDB($_GET['InformasifakturgudangumumV']['tglAwal']);
            $modFaktur->tglAkhir = $format->formatDateTimeMediumForDB($_GET['InformasifakturgudangumumV']['tglAkhir']);
            
            if($_GET['berdasarkanJatuhTempo']>0){
                $modFaktur->tglAwalJatuhTempo = $format->formatDateTimeMediumForDB($_GET['InformasifakturgudangumumV']['tglAwalJatuhTempo']);
                $modFaktur->tglAkhirJatuhTempo = $format->formatDateTimeMediumForDB($_GET['InformasifakturgudangumumV']['tglAkhirJatuhTempo']);
            } else {
                $modFaktur->tglAwalJatuhTempo = null;
                $modFaktur->tglAkhirJatuhTempo = null;
            }

        }
        
        $this->render('index', array('modFaktur'=>$modFaktur));
	}

}