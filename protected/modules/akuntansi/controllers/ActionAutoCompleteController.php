<?php

class ActionAutoCompleteController extends Controller
{
    public function actionJenisPenerimaan()
    {
        if(Yii::app()->request->isAjaxRequest) {
                $returnVal = array();
                $criteria = new CDbCriteria();
//                $criteria->compare('LOWER(jenispenerimaan_nama)', strtolower($_GET['term']), true);
                $criteria->addCondition('LOWER(jenispenerimaan_kode) || \' - \' || LOWER(jenispenerimaan_nama) LIKE \'%'.strtolower($_GET['term']).'%\'');
                $models = JenispenerimaanM::model()->findAll($criteria);
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $returnVal[$i]['label'] = $model->jenispenerimaan_kode.' - '.$model->jenispenerimaan_nama;
                    $returnVal[$i]['value'] = $model->jenispenerimaan_kode.' - '.$model->jenispenerimaan_nama;
                }

                echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
    public function actionJenisPengeluaran()
    {
        if(Yii::app()->request->isAjaxRequest) {
                $returnVal = array();
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(jenispengeluaran_nama)', strtolower($_GET['term']), true);
//                $criteria->addCondition('LOWER(jenispenerimaan_kode) || \' - \' || LOWER(jenispenerimaan_nama) LIKE \'%'.strtolower($_GET['term']).'%\'');
                $models = JenispengeluaranM::model()->findAll($criteria);
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {                        
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $returnVal[$i]['label'] = $model->jenispengeluaran_nama;
                    $returnVal[$i]['value'] = $model->jenispengeluaran_nama;
                }

                echo CJSON::encode($returnVal);
        }
        Yii::app()->end();
    }
    
}
?>
