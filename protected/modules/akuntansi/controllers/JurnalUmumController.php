<?php
class JurnalUmumController extends SBaseController
{
        public $success = false;
        protected $pesan = "succes";
        protected $is_action = "insert";
        //FILTER USER MENGGUNAKAN SRBAC
//    public function filters()
//    {
//        return array(
//            'accessControl',
//        );
//    }
    
//    public function accessRules()
//    {
//        return array(
//            array(
//                'allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array('index','view'),
//                'users'=>array('@'),
//            ),
//            array(
//                'allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('create','update','print','getDataRekening','SimpanJurnalUmum','RincianJurnal', 'EditJurnal'),
//                'users'=>array('@'),
//            ),
//            array(
//                'allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('admin','delete','RemoveTemporary'),
//                'users'=>array('@'),
//            ),
//            array(
//                'deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }
    
    public function loadModel($id)
    {
        $model = AKJurnalrekeningT::model()->findByPk($id);
        if($model === null) throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }
    
    public function actionAdmin()
    {
        $model = new AKJurnaldetailT();
        $model->tgl_awal = date('d M Y');
        $model->tgl_akhir = date('d M Y');
        if(isset($_REQUEST['AKJurnaldetailT']))
        {
            $format = new CustomFormat();
            $model->attributes = $_REQUEST['AKJurnaldetailT'];
            
            $model->tgl_awal = $format->formatDateMediumForDB($_REQUEST['AKJurnaldetailT']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateMediumForDB($_REQUEST['AKJurnaldetailT']['tgl_akhir']);
            $model->is_posting = $_REQUEST['AKJurnaldetailT']['is_posting'];
            $model->jenisjurnal_id = $_REQUEST['AKJurnaldetailT']['jenisjurnal_id'];
            $model->nobuktijurnal = $_REQUEST['AKJurnaldetailT']['nobuktijurnal'];
            $model->noreferensi = $_REQUEST['AKJurnaldetailT']['noreferensi'];
            $model->kodejurnal = $_REQUEST['AKJurnaldetailT']['kodejurnal'];
            
        }
        $this->render('gridJurnalUmum',array(
                'model'=>$model
            )
        );        
    }
    
    public function actionIndex()
    {
        $model = new AKJurnalrekeningT();
        $modelJurDetail = new AKJurnaldetailT();
        $modelJurPosting = new AKJurnalpostingT();
        $rekeningakuntansiV = new AKRekeningakuntansiV;
        $periodeID = Yii::app()->session['periodeID'];
        $model->rekperiod_id = $periodeID[0];
        $model->nobuktijurnal = Generator::noBuktiJurnalRek();
        $model->kodejurnal = Generator::kodeJurnalRek();
        $urlRedirect = Yii::app()->createUrl(Yii::app()->controller->module->id . '/RekperiodM');
        
        $model->tglbuktijurnal = date('Y-m-d H:m:s');
        $model->tglreferensi = date('Y-m-d H:m:s');
        
        $format = new CustomFormat();
        $is_action = 'insert';
        $pesan = 'succes';
        parse_str($_REQUEST['form-jurnal-umum'], $data_parsing);
        
        if(isset($_POST['AKJurnalrekeningT'])){
            $transaction = Yii::app()->db->beginTransaction();
            try{                
                $model->tglbuktijurnal = $format->formatDateTimeMediumForDB($_POST['AKJurnalrekeningT']['tglbuktijurnal']);
                $model->tglreferensi = $format->formatDateTimeMediumForDB($_POST['AKJurnalrekeningT']['tglreferensi']);
                $jurnal_rek = $this->simpanJurnal($_POST['AKJurnalrekeningT']);
                $jurPosting = null;
                if($jurnal_rek->success)
                {   
                    $modelJurPosting = array();
                    $modelJurPosting['tgljurnalpost'] = $_POST['AKJurnalrekeningT']['tglbuktijurnal'];
                    $modelJurPosting['keterangan'] = "Posting Otomatis";
                    $modelJurPosting['create_time']= date('Y-m-d');
                    $modelJurPosting['update_time']= date('Y-m-d');
                    $modelJurPosting['create_loginpemekai_id'] = Yii::app()->user->id;
                    $modelJurPosting['update_loginpemakai_id'] = Yii::app()->user->id;
                    $modelJurPosting['create_ruangan'] = Yii::app()->user->getState('ruangan_id');
                    $jurPosting = $this->simpanJurnalPosting($modelJurPosting);
//                }
                
                    if($jurnal_rek->save() && $jurPosting->save() && isset($_POST['JurnaldetailT']))
                    {
                        $jurnal_det =  $this->simpanDetailJurnal($_POST['JurnaldetailT'], $jurnal_rek, $jurPosting);
                        if($this->success){
                            $transaction->commit();
                            // $this->refresh();
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan, kolom Uraian rek harus diisi!");
                        }
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan, Detail Jurnal Belum Diisi! ");
                    }
                }else{
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan, kolom bertanda bintang harus diisi!");
                }
            }catch(Exception $exc){
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                $this->success = false;
            }
        }
            
        $this->render('index',array(
                'model'=>$model,
                'modelJurDetail'=>$modelJurDetail,
                'modelJurPosting'=>$modelJurPosting,
                'rekeningakuntansiV'=>$rekeningakuntansiV,
                'redirect' => array($periodeID, $urlRedirect)
            )
        );
    }
    
    public function actionGetDataRekening()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $model = new AKRekeningakuntansiV();
            $get_id = explode("x", $_POST['id']);
            $model->$get_id[0] = $get_id[1];
//            $model->rincianobyek_id = $_POST['id'];
            $result = $model->getInfoRekening();
            $rec = $result->attributes;
            if(isset($result->rincianobyek_id))
            {
                $rec['nmrincianobyek'] = $result->nmrincianobyek;
            }else{
                if(isset($this->obyek_id))
                {
                    $rec['nmrincianobyek'] = $result->nmobyek;
                }else{
                    $rec['nmrincianobyek'] = $result->nmjenis;
                }
            }
            echo json_encode($rec);
        }
        Yii::app()->end();
    }
    
    /*public function actionSimpanJurnalUmum()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $format = new CustomFormat();
            $is_action = 'insert';
            $pesan = 'succes';
            parse_str($_REQUEST['data'], $data_parsing);
            $data_parsing['AKJurnalrekeningT']['tglbuktijurnal'] = $format->formatDateTimeMediumForDB($data_parsing['AKJurnalrekeningT']['tglbuktijurnal']);
            $data_parsing['AKJurnalrekeningT']['tglreferensi'] = $format->formatDateTimeMediumForDB($data_parsing['AKJurnalrekeningT']['tglreferensi']);
            $transaction = Yii::app()->db->beginTransaction();
            
            try{
                
                $jurnal_rek = $this->simpanJurnal($data_parsing['AKJurnalrekeningT']);
                $jurPosting = null;
                if($_REQUEST['jenis_simpan'] == 'posting')
                {
                    $params = array();
                    $params['tgljurnalpost'] = $data_parsing['AKJurnalrekeningT']['tglbuktijurnal'];
                    $params['keterangan'] = "Posting Otomatis";
                    $params['create_time'] = date('Y-m-d');
                    $params['update_time'] = date('Y-m-d');
                    $params['create_loginpemekai_id'] = Yii::app()->user->id;
                    $params['update_loginpemakai_id'] = Yii::app()->user->id;
                    $params['create_ruangan'] = Yii::app()->user->getState('ruangan_id');
                    $jurPosting = $this->simpanJurnalPosting($params);
                }
                $this->simpanDetailJurnal($data_parsing, $jurnal_rek, $jurPosting);
                
                if($this->success)
                {
                    $transaction->commit();
                    
                    $periodeID = Yii::app()->session['periodeID'];
                    $pesan = array(
                        'nobuktijurnal' => Generator::noBuktiJurnalRek(),
                        'kodejurnal' => Generator::kodeJurnalRek(),
                        'rekperiod_id' => $periodeID[0]
                    );
                }
                
            }catch(Exception $exc){
                $transaction->rollback();
                $pesan = $exc;
                $this->success = false;
            }
            
            $result = array(
                'action' => $is_action,
                'pesan' => $pesan,
                'status' => ($this->success == true) ? 'ok' : 'not',
            );
            echo json_encode($result);
        }
        Yii::app()->end();        
    }
     * 
     */
    
    private function simpanJurnal($params)
    {
        $model = new AKJurnalrekeningT();
        $model->attributes = $params;
        $model->jenisjurnal_id = $params['jenisjurnal_id'];
        $model->noreferensi = $params['noreferensi'];
        $model->create_time = date('Y-m-d');
        $model->update_time = date('Y-m-d');
        $model->create_loginpemakai_id = Yii::app()->user->id;
        $model->update_loginpemakai_id = Yii::app()->user->id;
        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
        // $model->validate();
        if($model->validate()){
           $model->success = true;
        }else{
           $model->success = false;
        }

        return $model;
    }
    
    private function simpanJurnalPosting($params)
    {
        $modelJurPosting = new AKJurnalpostingT();
        $modelJurPosting->attributes = $params;
        $modelJurPosting->validate();
//        if($modelJurPosting->validate()){
//            if($modelJurPosting->save()){
//                $this->success = true;
//            }else{
//                $this->success = false;
////                print_r($modelJurPosting->getErrors());
//            }
//        }else{
//            $this->success = false;
////            print_r($modelJurPosting->getErrors());
//        }
        return $modelJurPosting;
    }    
    
    private function simpanDetailJurnal($params, $jurnal_rek, $jurPosting=null)
    {
        $modDetail = $this->validasiTabular($params);
        foreach ($modDetail as $i => $data)
        {
            $data->rekperiod_id = $jurnal_rek->rekperiod_id;
            $data->jurnalrekening_id = $jurnal_rek->jurnalrekening_id;
            if(isset($jurPosting->jurnalposting_id))
            {
                $data->jurnalposting_id = $jurPosting->jurnalposting_id;
            }
            if ($data->jurnaldetail_id > 0) {
                if ($data->update()){
                    $this->success = true;
                }else{
                    $this->success = false;
                }
                    
            }else{
                if($data->save()){
                    $this->success = true;
                }else{
                    $this->success = false;
                    // print_r($data->getErrors());exit;
                }
                    
            }
        }  
        return $this->success;
    }    
    
    private function validasiTabular($params)
    {
        $modDetails = array();
        sort($params);
        foreach ($params as $i=>$row){
            $modDetails[$i] = new JurnaldetailT();
            $modDetails[$i]->attributes = $row;
            $modDetails[$i]->saldokredit = (empty($row['saldokredit']) ? 0 : $row['saldokredit']);
            $modDetails[$i]->validate();
        }
        return $modDetails;        
    }
    
    public  function actionRincianJurnal()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new AKJurnaldetailT();
        $model->jurnalrekening_id = $_GET['id'];
        $this->render('__gridRincianJurnal',array(
                'model'=>$model
            )
        );
    }

    public  function actionEditJurnal()
    {
        $model = $this->loadModel($_GET['id']);
        $this->render('editJurnal',array(
                'model'=>$model
            )
        );
    }
}
