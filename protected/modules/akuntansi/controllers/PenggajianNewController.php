<?php

class PenggajianNewController extends SBaseController
{
        protected $succesSave = true;
        protected $pesan = "succes";
        protected $is_action = "insert";

	public function actionIndex($id=null)
	{
            $modPengUmum = new AKPengeluaranumumT;
            $modPengUmum->volume = 1;
            $modPengUmum->hargasatuan = 0;
            $modPengUmum->totalharga = 0;
            $modPengUmum->nopengeluaran = KeyGenerator::noPengeluaranUmum();
            $modUraian[0] = new AKUraiankeluarumumT;
            $modUraian[0]->volume = 1;
            $modUraian[0]->hargasatuan = 0;
            $modUraian[0]->totalharga = 0;
            $modBuktiKeluar = new AKTandabuktikeluarT;
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->nokaskeluar = KeyGenerator::noKasKeluar();
            $modBuktiKeluar->biayaadministrasi = 0;
            $modBuktiKeluar->jmlkaskeluar = 0;
            $modJurnalRekening = new AKJurnalrekeningT;
            $modJurnalDetail = new AKJurnaldetailT;
            $modJurnalPosting = new AKJurnalpostingT;
            
		
            if(isset($_POST['AKPengeluaranumumT'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    
                    $modBuktiKeluar = $this->saveTandaBuktiKeluar($_POST['AKTandabuktikeluarT']);
                    $modPengUmum = $this->savePengeluaranUmum($_POST['AKPengeluaranumumT'], $modBuktiKeluar, $_POST['AKUraiankeluarumumT']);
                    $this->updateTandaBuktiKeluar($modBuktiKeluar, $modPengUmum);

                    if($modPengUmum->isurainkeluarumum && isset($_POST['AKUraiankeluarumumT'])){
                        $modUraian = $this->saveUraian($_POST['AKUraiankeluarumumT'], $modPengUmum);
                    }
                    if($this->succesSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect(array('index','id'=>$modPengUmum->pengeluaranumum_id));
                        $model->isNewRecord = false;
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',array('modPengUmum'=>$modPengUmum,
                                        'modUraian'=>$modUraian,
                                        'modBuktiKeluar'=>$modBuktiKeluar,
                                        'modJurnalRekening'=>$modJurnalRekening,
                                        'modJurnalDetail'=>$modJurnalDetail,
                                        'modJurnalPosting'=>$modJurnalPosting));
	}
        
        protected function updateTandaBuktiKeluar($modBuktiKeluar,$modPengUmum)
        {
            AKTandabuktikeluarT::model()->updateByPk($modBuktiKeluar->tandabuktikeluar_id, array('pengeluaranumum_id'=>$modPengUmum->pengeluaranumum_id));
        }
        
        public function actionSimpanPengeluaran()
	{
            if(Yii::app()->request->isAjaxRequest)
            {
                parse_str($_REQUEST['data'],$data_parsing);
                $format = new CustomFormat();
                if(isset($data_parsing['AKPengeluaranumumT'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        
                        $modBuktiKeluar = $this->saveTandaBuktiKeluar($data_parsing['AKTandabuktikeluarT']);
                        $data_parsing['AKPengeluaranumumT']['tglpengeluaran'] = $format->formatDateTimeMediumForDB($data_parsing['AKPengeluaranumumT']['tglpengeluaran']);
                        $modPengUmum = $this->savePengeluaranUmum($data_parsing['AKPengeluaranumumT'], $modBuktiKeluar);
                        if(isset($data_parsing['AKPengeluaranumumT']['isurainkeluarumum'])){
                            $modUraian = $this->saveUraian($data_parsing['AKUraiankeluarumumT'], $modPengUmum);
                        }
                        
                        $modJurnalRekening = $this->saveJurnalRekening($modPengUmum, $data_parsing['AKPengeluaranumumT']);
                        
                        $params = array(
                            'modJurnalRekening' => $modJurnalRekening, 
                            'jenis_simpan'=>$_REQUEST['jenis_simpan'], 
                            'RekeningakuntansiV'=>$data_parsing['RekeningakuntansiV'],
                        );
                        $insertDetailJurnal = MyFunction::insertDetailJurnal($params);
                        $this->succesSave = $insertDetailJurnal;
                        
                        /*
                        if($_REQUEST['jenis_simpan'] == 'posting')
                        {
                            $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                        }
                        $modJurnalDetail = $this->saveJurnalDetail(
                            $data_parsing['AKPenerimaanUmumT'],
                            $modJurnalRekening,
                            $modJurnalPosting,
                            $data_parsing['RekeningakuntansiV']
                        );
                        */
                        if($this->succesSave){
                            $transaction->commit();
                            $this->pesan = array(
                                'nopengeluaran'=>KeyGenerator::noPengeluaranUmum(),
                                'nokaskeluar'=>KeyGenerator::noKasKeluar()
                            );
                        }else{
                            $transaction->rollback();
                        }
                    } catch (Exception $exc) {
                        print_r($exc);
                        $this->pesan = $exc;
                        $this->succesSave = false;
                        $transaction->rollback();
                    }
                }
                $result = array(
                    'action'=>$this->is_action,
                    'pesan'=>$this->pesan,
                    'status' => ($this->succesSave == true ? 'ok' : 'not'),
                );
                echo json_encode($result);
                Yii::app()->end();
            }
	}
        
        protected function saveTandaBuktiKeluar($postBuktiKeluar)
        {
            $modBuktiKeluar = new AKTandabuktikeluarT;
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->nokaskeluar = KeyGenerator::noKasKeluar();
            $modBuktiKeluar->biayaadministrasi = $postBuktiKeluar['biayaadministrasi'];
            // $modBuktiKeluar->jmlkaskeluar = 0;
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->tahun = date('Y');
            $this->succesSave = false;
            if($modBuktiKeluar->validate()){
                $modBuktiKeluar->save();
                $this->succesSave = true;
            } else {
                $this->succesSave = false;
                $this->pesan = $modBuktiKeluar->getErrors();
            }
            
            return $modBuktiKeluar;
        }
        
        protected function savePengeluaranUmum($postPengeluaran, $modBuktiKeluar, $postUraian)
        {
            
            $totalharga = 0;
            foreach ($postUraian as $i => $value) {
                $modUraian[$i]->totalharga = $value['totalharga'] ;
                $modUraian[$i]->hargasatuan = $value['hargasatuan'] ;
                $totalharga += $modUraian[$i]->totalharga; 
            }
            $format = new CustomFormat();
            $modPengUmum = new AKPengeluaranumumT;
            $modPengUmum->attributes = $postPengeluaran;
            $modPengUmum->nopengeluaran = KeyGenerator::noPengeluaranUmum();            
            $modPengUmum->biayaadministrasi = $modBuktiKeluar->biayaadministrasi;
            $modPengUmum->totalharga = $totalharga;
            $modPengUmum->hargasatuan = $totalharga;
            $modPengUmum->tglpengeluaran = $format->formatDateTimeMediumForDB($postPengeluaran['tglpengeluaran']);

            foreach ($postPengeluaran as $key => $value) {
//                echo $value; 
            }
            
            if($modPengUmum->validate()){
                $postRekenings = $_POST['RekeningakuntansiV'];
                if(isset($postRekenings)){//simpan jurnal rekening
                    if(count($postRekenings) > 0){

                     $modJurnalRekening = PenggajianController::saveJurnalRekening($modPengUmum, $data_parsing['AKPengeluaranumumT']);
    //                      ADA DI BAWAH >> $modPengUmum->save();
                     $saveDetailJurnal = PenggajianController::saveJurnalDetail($modJurnalRekening, $postRekenings, null);
                    }
                }

                $modPengUmum->save();
                $this->succesSave = true;
                $attributes = array(
                    'pengeluaranumum_id' => $modPengUmum->pengeluaranumum_id,
                    'jurnalrekening_id' => $modJurnalRekening->jurnalrekening_id
                );
                AKTandabuktikeluarT::model()->updateByPk($modBuktiKeluar->tandabuktikeluar_id, $attributes);
            } else {
                $this->succesSave = false;
                $this->pesan = $modPengUmum->getErrors();
            }
            
            return $modPengUmum;
        }

        protected function saveUraian($arrPostUraian,$modPengUmum)
        {
            $valid = false;
            $modUraian = array();
            for($i=0;$i<count($arrPostUraian);$i++){
                if(strlen($arrPostUraian[$i]['uraiantransaksi']) > 0)
                {
                    $modUraian[$i] = new AKUraiankeluarumumT;
                    $modUraian[$i]->attributes = $arrPostUraian[$i];
                    $penggajianpeg_id = $arrPostUraian[$i]['penggajianpeg_id'];
                    $modUraian[$i]->pengeluaranumum_id = $modPengUmum->pengeluaranumum_id;
                    if($modUraian[$i]->validate())
                    {
                        $modUraian[$i]->save();
                        $valid = true;

                        $attributes = array(
                            'pengeluaranumum_id' => $modPengUmum->pengeluaranumum_id
                        );
                        PenggajianpegT::model()->updateByPk($penggajianpeg_id, $attributes);
                    }else{
                        $this->pesan = $modUraian[$i]->getErrors();
                    }
                }
            }
            $this->succesSave = $valid;
            return $modUraian;
        }
        
        protected function saveJurnalRekening($modPenUmum, $postPenUmum)
        {
            $format = new CustomFormat();
            $modJurnalRekening = new AKJurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = $format->formatDateTimeMediumForDB($modPenUmum->tglpengeluaran);
            $modJurnalRekening->nobuktijurnal = KeyGenerator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = KeyGenerator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = $format->formatDateTimeMediumForDB($modPenUmum->tglpengeluaran);
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = $postPenUmum['jenisKodeNama'];

            
            /*
            $attributes = array(
                'jenisjurnal_aktif' => true
            );
            $jenisjurnal_id = JenisjurnalM::model()->findByAttributes($attributes);
            $modJurnalRekening->jenisjurnal_id = $jenisjurnal_id->jenisjurnal_id;
             * 
             */
            
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = $format->formatDateTimeMediumForDB($modPenUmum->tglpengeluaran);
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            // echo"<pre>";

            if($modJurnalRekening->validate()){
                // echo"<pre>"; print_r($modJurnalRekening->attributes) exit();

                // echo $modJurnalRekening->nobuktijurnal; exit();

                $modJurnalRekening->save();
                $this->succesSave = true;
            } else {
                $this->succesSave = false;
                $this->pesan = $modJurnalRekening->getErrors();
            }
            return $modJurnalRekening;
        }

        public function saveJurnalDetail($modJurnalRekening, $postRekenings, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                $model[$i] = new JurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                $model[$i]->uraiantransaksi = $rekening['nama_rekening'];
                $model[$i]->saldodebit = $rekening['saldodebit'];
                $model[$i]->saldokredit = $rekening['saldokredit'];
                $model[$i]->nourut = $i+1;
                $model[$i]->rekening1_id = $rekening['struktur_id'];
                $model[$i]->rekening2_id = $rekening['kelompok_id'];
                $model[$i]->rekening3_id = $rekening['jenis_id'];
                $model[$i]->rekening4_id = $rekening['obyek_id'];
                $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                $model[$i]->catatan = "";
                if($model[$i]->validate()){
                    $model[$i]->save();
                }else{
    //                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                    $valid = false;
                    break;
                }
            }
            return $valid;        
        }        
        /*
        protected function saveJurnalDetail($arrJurnal, $modJurnalRekening, $modJurnalPosting = null)
        {
            $valid = true;
            for($i=0;$i<2;$i++)
            {
                $model[$i] = new AKJurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                $model[$i]->uraiantransaksi = $arrJurnal['jenisKodeNama'];
                $model[$i]->saldodebit = 0;
                $model[$i]->saldokredit = 0;
                $model[$i]->nourut = $i+1;
                if($i == 0)
                {
                    $jenisPnrm = JnspengeluaranrekM::model()->findByAttributes(
                        array(
                            'jenispengeluaran_id'=>$arrJurnal['jenispengeluaran_id'],
                            'saldonormal' => 'D'
                        )
                    );
                    $model[$i]->saldodebit = $arrJurnal['totalharga'];
                }else{
                    $jenisPnrm = JnspengeluaranrekM::model()->findByAttributes(
                        array(
                            'jenispengeluaran_id'=>$arrJurnal['jenispengeluaran_id'],
                            'saldonormal' => 'K'
                        )
                    );
                    $model[$i]->saldokredit = $arrJurnal['totalharga'];
                }
                $model[$i]->rekening1_id = $jenisPnrm['rekening1_id'];
                $model[$i]->rekening2_id = $jenisPnrm['rekening2_id'];
                $model[$i]->rekening3_id = $jenisPnrm['rekening3_id'];
                $model[$i]->rekening4_id = $jenisPnrm['rekening4_id'];
                $model[$i]->rekening5_id = $jenisPnrm['rekening5_id'];
                $model[$i]->catatan = "";
                if($model[$i]->validate())
                {
                    $model[$i]->save();
                }else{
                    $this->pesan = $model[$i]->getErrors();
                    $valid = false;
                    break;
                }
            }
            $this->succesSave = $valid;
        }
        */
        protected function saveJurnalPosting($arrJurnalPosting)
        {
                $modJurnalPosting = new AKJurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                    $this->succesSave = true;
                } else {
                    $this->succesSave = false;
                    $this->pesan = $modJurnalPosting->getErrors();
                }
                return $modJurnalPosting;
        }        
        
        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}