<?php

class DaftarPengeluaranUmumController extends SBaseController
{
	public function actionIndex()
	{
            $modPengeluaran = new AKPengeluaranumumT();
            $format = new CustomFormat();
            $modPengeluaran->tglAwal=date('Y-m-d 00:00:00');
            $modPengeluaran->tglAkhir=date('Y-m-d H:i:s');
		
            if(isset($_GET['AKPengeluaranumumT'])){
                $modPengeluaran->attributes=$_GET['AKPengeluaranumumT'];
                $modPengeluaran->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKPengeluaranumumT']['tglAwal']);
                $modPengeluaran->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKPengeluaranumumT']['tglAkhir']);
            }
            
            $this->render('index', array('modPengeluaran'=>$modPengeluaran));
	}
        
	public function actionReturPengeluaranUmum()
	{
//            $this->render('index', array('modPengeluaran'=>$modPengeluaran));
	}        

}