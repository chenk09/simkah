<?php

class PembayaranPiutangAsuransiController extends SBaseController
{
    public $layout='//layouts/column1';
    public $defaultAction = 'Index';
    public $detailKlaim;
    public function actionIndex()
    {
        
        $this->pageTitle = Yii::app()->name.' - '.'Transaksi Pembayaran Piutang Asuransi';
        
        $modPembayaranKlaim=new AKPembayaranklaimT;
            $modPembayaranKlaimDetail = new AKPembayarklaimdetailT;
            $modTandabukti = new TandabuktibayarT;
            $modPembayaranPelayanan = new PembayaranpelayananT;
            $modPendaftaran = new AKPendaftaranT;
            $modPasien = new AKPasienM;
            $format = new CustomFormat();
            $modRekenings = array();
            
            $modPendaftaran->tglAwal = date('Y-m-d 00:00:00');
            $modPendaftaran->tglAkhir = date('Y-m-d 23:59:59');
            
            $modPembayaranKlaim->tglpembayaranklaim = date('Y-m-d H:i:s');
            $modPembayaranKlaim->nopembayaranklaim = Generator::noPembayaranKlaim();
            
            if (isset($_POST['AKPembayaranklaimT'])) {
                $modPembayaranKlaim->attributes = $_POST['AKPembayaranklaimT'];
                $modPembayaranKlaim->tglpembayaranklaim = $format->formatDateTimeMediumForDB($modPembayaranKlaim->tglpembayaranklaim);
                if (count($_POST['AKPembayarklaimdetailT']) > 0) {
                    $pembayaranpelayanan_id = $this->sortPilih($_POST['AKPembayarklaimdetailT']);
                    $modDetails = $this->validasiTabular($modPembayaranKlaim, $_POST['AKPembayarklaimdetailT']);
                    $modPembayaranKlaim->carabayar_id=$_POST['AKPembayarklaimdetailT'][1]['carabayar_id'];
                    $modPembayaranKlaim->penjamin_id=$_POST['AKPembayarklaimdetailT'][1]['penjamin_id'];
                }
                if ($modPembayaranKlaim->validate()) { 
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $success = true;
                        if ($modPembayaranKlaim->save()) {
                                $modDetails = $this->validasiTabular($modPembayaranKlaim, $_POST['AKPembayarklaimdetailT']);
//                                    $nourut =1;
//                                    foreach($_POST['RekeningpembayarankasirV'] AS $i => $post){
                                    $postRekenings = $_POST['RekeningpembayarankasirV'];
                                    if(isset($postRekenings)){
                                        $modJurnalRekening = $this->saveJurnalRekening();
                                        $saveDetailJurnal = $this->saveJurnalDetail($modJurnalRekening, $postRekenings, $nourut, false);
//                                            $nourut++;
                                    }
//                                    }
//                                    
                                foreach ($modDetails as $i => $data) {

                                    if ($data->pembayaranpelayanan_id > 0) {
                                        if ($data->save()) {
                                                 PembayaranpelayananT::model()->updateByPk($data->pembayaranpelayanan_id, array('pembklaimdetal_id'=>$data->pembklaimdetal_id)); 
                                        } else {
                                            $success = false;
                                        }
                                    }
                                }
                        }
                        if ($success == true) {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index', 'id' => $modPembayaranKlaim->pembayarklaim_id));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                        }
                    } catch (Exception $ex) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', "Data gagal disimpan ".$ex->getMessage());
                    }
            } else {
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
            }
                
             if ((isset($_GET['tglAwal'])) && (isset($_GET['tglAkhir'])) && (isset($_GET['carabayar_id'])) && (isset($_GET['penjamin_id']))) {
                if (Yii::app()->request->isAjaxRequest) {
                    $tglAwal  = $format->formatDateTimeMediumForDB($_GET['tglAwal']);
                    $tglAkhir = $format->formatDateTimeMediumForDB($_GET['tglAkhir']);
                    $pasien_id = $_GET['pasien_id'];
                    $carabayar_id = $_GET['carabayar_id'];
                    $penjamin_id = $_GET['penjamin_id'];
                    $pembklaimdetal_id = $_GET['pembklaimdetal_id'];
                    $noPendaftaran = $_GET['noPendaftaran'];

                    $tr = $this->createList($tglAwal, $tglAkhir, $noPendaftaran, $carabayar_id, $penjamin_id, true);
                    echo $tr;exit;
                    Yii::app()->end();
                }
            }
        
        $this->render('index',array(
                    'modPembayaranKlaim'=>$modPembayaranKlaim,
                    'modPembayaranKlaimDetail'=>$modPembayaranKlaimDetail,
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modPembayaranPelayanan'=>$modPembayaranPelayanan,
                    'tr' => $tr,
                    'modDetails'=>$modDetails,
                    'modRekenings'=>$modRekenings,
//                    'pembayaran'=>$pembayaran,
            ));
        
    }
    
    protected function rowPengeluaran($pengeluaran, $totaltransaksi, $tr, $text=null) {
            foreach ($pengeluaran as $i=>$row){
//                echo"<pre>";
//                echo $row->pembayarklaim->penjamin->penjamin_nama;
//                exit;
                if (isset($row->pembayarklaim_id)){
                    $i++;
                    $totaltransaksi = count($penjamins);
                     $tr .= '<tr >';
                     $tr .= '<td>'.$i.'</td>';
                     $tr .= '<td>' . $row->pasien->no_rekam_medik."<br/>".$row->pendaftaran->no_pendaftaran . '</td>';
                     $tr .= '<td>' . $row->pasien->nama_pasien . '</td>';
                     $tr .= '<td>' . $row->pasien->alamat_pasien . '</td>';
                     $tr .= '<td>' . $row->pendaftaran->penanggungJawab->nama_pj."-".$row->pendaftaran->penanggungJawab->pengantar . '</td>';
                     $tr .= '<td>' . $row->pembayarklaim->penjamin->penjamin_nama . '</td>';
                     $tr .= '<td>' . $row->pelayanan->nopembayaran . '</td>';
                     if ($text == true){
                         $tr .= '<td>'.MyFunction::formatNumber($row->pelayanan->totalbiayapelayanan).'</td>';
                         $tr .= '<td>'.MyFunction::formatNumber($row->pelayanan->totalbayartindakan).'</td>';
                         $tr .= '<td>'.MyFunction::formatNumber($row->pelayanan->totalsisatagihan).'</td>';
                         $tr .= '<td>'.MyFunction::formatNumber($row->pelayanan->totalbayartindakan).'</td>';
                         $tr .= '<td>'.MyFunction::formatNumber($row->pelayanan->totalsisatagihan).'</td>';
                     }else{
                         $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltagihan]', MyFunction::formatNumber($row->pelayanan->totalbiayapelayanan), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltagihan currency ', 'readonly' => true,'onblur'=>'hitungSemuaTransaksi(this)')) . '</td>';
 //                        $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltelahbayar]', (empty($row->pembklaimdetal_id) ? (empty($row->detailklaim->telahbayar) ? "0" : MyFunction::formatNumber($row->tandabukti->jmlpembayaran)) : MyFunction::formatNumber($row->detailklaim->jmltelahbayar)), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltelahbayar currency ', 'onblur' => 'hitungJumlahTelahBayar(this);')) . '</td>';
                         $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmltelahbayar]', MyFunction::formatNumber(isset($row->jmltelahbayar)?$row->jmltelahbayar:0), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmltelahbayar currency ', 'readOnly'=>true)) . '</td>';
 //                        $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlpiutang]', (empty($row->pembklaimdetal_id) ? MyFunction::formatNumber($jumlahPiutang) : MyFunction::formatNumber($row->detailklaim->jmlpiutang)), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang currency ', 'onblur' => 'hitungJumlahPiutang(this);')) . 
 //                        $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlpiutang]', (empty($row->pembklaimdetal_id) ? MyFunction::formatNumber($row->totalsubsidiasuransi): MyFunction::formatNumber($row->detailklaim->jmlsisapiutang)), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang currency ','readonly' => true,'onblur' => 'hitungJumlahPiutang(this);')) . 
                         $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlpiutang]', MyFunction::formatNumber($row->jmlpiutang), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang currency ','readonly' => true)) . 
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][jmlpiutang2]', MyFunction::formatNumber($row->jmlpiutang), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlpiutang2 currency')) .
                                '</td>';
                         $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlbayar]', '', array('style'=>'width:70px;','placeHolder'=>'0','class' => 'inputFormTabel span3 jmlbayar currency ','onblur' => 'hitungSisaTagihan(this);')) . '</td>';

 //                        $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlsisatagihan]',(empty($row->pembklaimdetal_id) ? (empty($row->detailklaim->jmlsisapiutang) ? 0 : 0) : 0), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlsisatagihan currency ', 'readOnly'=>true)). '</td>';

                         if(empty($row->pelayanan->pembklaimdetal_id)){
                             $terbyr=$row->pelayanan->totalsubsidiasuransi;
                         }else {
                             $terbyr=$row->pelayanan->detailklaim->jmlsisapiutang;
                         }

                         $tr .= '<td>' . CHtml::textField('AKPembayarklaimdetailT['.$i.'][jmlsisatagihan]',MyFunction::formatNumber($terbyr), array('style'=>'width:70px;','class' => 'inputFormTabel span3 jmlsisatagihan currency ', 'readOnly'=>true)). '</td>';

                         $tr .= '<td>' . CHtml::checkBox('AKPembayarklaimdetailT['.$i.'][cekList]', true, array('value'=>$row->pendaftaran->piutangasuransi->piutangasuransi_id,'class' => 'cek', 'onClick' => 'setAll();')) .
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pendaftaran_id]', $row->pendaftaran_id, array('style'=>'width:70px;','class' => 'inputFormTabel currency span3 jmlsisatagihan',)).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pasien_id]', $row->pasien_id, array('style'=>'width:70px;','class' => 'inputFormTabel currency span3 jmlsisatagihan',)).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][piutangasuransi_id]', $row->pendaftaran->piutangasuransi->piutangasuransi_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3 ')).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][pembayaranpelayanan_id]', $row->pembayaranpelayanan_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3 ')).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][tandabuktibayar_id]', $row->pelayanan->tandabuktibayar_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][carabayar_id]', $row->pembayarklaim->carabayar_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).
                                         CHtml::hiddenField('AKPembayarklaimdetailT['.$i.'][penjamin_id]', $row->pembayarklaim->penjamin_id, array('style'=>'width:70px;','class' => 'inputFormTabel  span3')).

                                '</td>';
                     }
                     $tr .= '</tr>';
                } 
            }


            return $tr;
        }
        
        protected function pembklaimdetal($pendaftaran_id) {            
            $criteriaPemb = new CDbCriteria();
            $criteriaPemb->compare('pendaftaran_id',$pendaftaran_id);
            $pemb = AKPembayarklaimdetailT::model()->find($criteriaPemb);
            return $pemb; //return array
        }
        
        protected function createList($tglAwal, $tglAkhir, $noPendaftaran, $carabayar_id, $penjamin_id,$status=null) {            
            $criteria = new CDbCriteria();
            $penjamins = "";
            $criteria->with = array('pembayarklaim','pelayanan');
            $criteria->addCondition('pembayarklaim.penjamin_id = '.$penjamin_id);
            $criteria->join = 'JOIN pembayaranpelayanan_t ON t.pembklaimdetal_id = pembayaranpelayanan_t.pembklaimdetal_id'; 
            
            $criteria->addCondition('t.jmlsisapiutang != 0');
            if(empty($noPendaftaran)){
                $criteria->addBetweenCondition('pelayanan.tglpembayaran', $tglAwal, $tglAkhir);
            }else{
                $criteria->compare('pendaftaran_t.no_pendaftaran',$noPendaftaran);
            }
            $penjamins = AKPembayarklaimdetailT::model()->findAll($criteria);

            $tr = $this->rowPengeluaran($penjamins, $data['totaltransaksi'], $data['tr']);
            
            return $tr;
        }
        
        protected function sortPilih($data){
            $result = array();
            foreach ($data as $i=>$row){
                if ($row['cekList'] == 1){
                    $result[] = $row['pembayaranpelayanan_id'];
                }
            }
            
            return $result;
        }
        
    
    protected function validasiTabular($modPembayaranKlaim, $data) {
        foreach ($data as $i => $row) {
            if($row['cekList'] == 1){
                
                $modDetails[$i] = new AKPembayarklaimdetailT();
                
                $res = $this->pembklaimdetal($row['pendaftaran_id']);
                $res = $row['jmlbayar']+$res->jmltelahbayar;
                
                $modDetails[$i]->attributes = $row;                
                $modDetails[$i]->pendaftaran_id = $row['pendaftaran_id'];
                $modDetails[$i]->pasien_id = $row['pasien_id'];
                $modDetails[$i]->pembayarklaim_id = $modPembayaranKlaim->pembayarklaim_id;                 
                $modDetails[$i]->pembayaranpelayanan_id = $row['pembayaranpelayanan_id'];
                $modDetails[$i]->tandabuktibayar_id = $row['tandabuktibayar_id'];
                $modDetails[$i]->jmlpiutang = $row['jmltagihan'];
                $modDetails[$i]->jumlahbayar = $row['jmlbayar'];
                $modDetails[$i]->jmltelahbayar = $res;
                $modDetails[$i]->jmlsisapiutang = $row['jmlsisatagihan'];
                $modDetails[$i]->validate();
            }
            
//            echo '<pre>';
//            echo print_r($modDetails[$i]->getErrors());
//            echo '</pre>';
        }

        return $modDetails;
    }
    
    /**
     * simpan jurnaldetail_t dan jurnalposting_t digunakan di:
     * - akuntansi/pembayaranKlaimPiutangAK
     * - billingKasir/pembayaranKlaimPiutang
     */
    public function saveJurnalDetail($modJurnalRekening, $postRekenings, $noUrut=0, $isPosting = false){
        $modJurnalPosting = null;
        if($isPosting == true){
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
            }
        }
        
        foreach($postRekenings AS $i => $post){
            $modJurnalDetail[$i] = new JurnaldetailT();
            $modJurnalDetail[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $modJurnalDetail[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $modJurnalDetail[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $modJurnalDetail[$i]->uraiantransaksi = $post['nama_rekening'];
            $modJurnalDetail[$i]->saldodebit = $post['saldodebit'];
            $modJurnalDetail[$i]->saldokredit = $post['saldokredit'];
            $modJurnalDetail[$i]->nourut = $i+1;
            $modJurnalDetail[$i]->rekening1_id = $post['struktur_id'];
            $modJurnalDetail[$i]->rekening2_id = $post['kelompok_id'];
            $modJurnalDetail[$i]->rekening3_id = $post['jenis_id'];
            $modJurnalDetail[$i]->rekening4_id = $post['obyek_id'];
            $modJurnalDetail[$i]->rekening5_id = $post['rincianobyek_id'];
            $modJurnalDetail[$i]->catatan = "";
            if($modJurnalDetail[$i]->validate()){
                $modJurnalDetail[$i]->save();
            }
        }
        return $modJurnalDetail;       
    }
    
     /**
    * simpan jurnalrekening_t
    * @return \JurnalrekeningT
    */
    public function saveJurnalRekening()
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "";
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = date('Y-m-d H:i:s');
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
        } else {
            $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
        }
        return $modJurnalRekening;
    }
    
}