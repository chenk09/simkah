<?php
class PelayananRekeningController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
//	FILTER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print','ubahRekeningDebit','ubahRekeningKredit'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($daftartindakan_id,$komponentarif_id)
	{
            $model= AKPelayananrekM::model()->findByAttributes(array('daftartindakan_id'=>$daftartindakan_id, 'komponentarif_id'=>$komponentarif_id));
                
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AKPelayananrekM();
                
                //== untuk pencarian / filter grid tindakankomponen
                $modTindakanKomponen = new AKDaftartindakankomponenV('searchTable');
                if(isset($_GET['AKDaftartindakankomponenV'])){
                    $modTindakanKomponen->attributes = $_GET['AKDaftartindakankomponenV'];
                }
                //==
                if(isset($_POST['AKPelayananrekM']) && isset($_POST['AKDaftartindakankomponenV'])){
                    $rekening = $_POST['AKPelayananrekM'];
                    $tindkomps = $_POST['AKDaftartindakankomponenV'];

                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $berhasil = $this->simpanRekenings($rekening, $tindkomps);
                        if($berhasil == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin'));
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
                    }
                }
                

		$this->render('create',array(
			'model'=>$model, 'modTindakanKomponen'=>$modTindakanKomponen
		));
	}
        /**
         * menyimpan pelayananrek_m
         * @param type $rekening
         * @param type $tindkomps
         * @return boolean
         */
        protected function simpanRekenings($rekening, $tindkomps){
            $x = 0;
            $sukses = true;
            foreach ($rekening['rekening'] AS $i => $rek) {
                foreach($tindkomps['komponen'] AS $tindId=>$komps){
                    foreach($komps AS $kompId=>$pilih){
                        if($pilih == true){
                            $modRekeningPel[$x] = new AKPelayananrekM;                    
                            $modRekeningPel[$x]->attributes = $rek; 
                            $modRekeningPel[$x]->daftartindakan_id = $tindId;               
                            $modRekeningPel[$x]->komponentarif_id = $kompId;               
                            $modRekeningPel[$x]->rekening1_id 		= $rek['rekening_id1'];
                            $modRekeningPel[$x]->rekening2_id 		= $rek['rekening_id2'];
                            $modRekeningPel[$x]->rekening3_id 		= $rek['rekening_id3'];
                            $modRekeningPel[$x]->rekening4_id 		= $rek['rekening_id4'];
                            $modRekeningPel[$x]->rekening5_id 		= $rek['rekening_id5'];
                            $modRekeningPel[$x]->saldonormal 		= $rek['saldonormal'];   
                            $modRekeningPel[$x]->jnspelayanan 		= $rekening['jnspelayanan'];
                            if($modRekeningPel[$x]->validate()){
                                $modRekeningPel[$x]->save();
                                $sukses = $sukses && true;
                            }else{
                                $sukses = false;
                            }
                            $x++;
                        }
                    }
                }
            }
//            return $modRekeningPel;
            return $sukses;
        }

	/**
	 * Hapus rekening komponentarif
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($daftartindakan_id, $komponentarif_id)
	{
            if(Yii::app()->request->isAjaxRequest)
            {
                    // we only allow deletion via POST request
                    $model=AKPelayananrekM::model()->deleteAllByAttributes(array('daftartindakan_id'=>$daftartindakan_id, 'komponentarif_id'=>$komponentarif_id));

                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }
            else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect($this->createUrl('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new AKPelayananrekM();
		$model->unsetAttributes(); 
		if(isset($_GET['AKPelayananrekM'])){
			$model->attributes=$_GET['AKPelayananrekM'];                       
			$model->daftartindakan_nama = $_GET['AKPelayananrekM']['daftartindakan_nama'];
			$model->komponentarif_nama = $_GET['AKPelayananrekM']['komponentarif_nama'];
			$model->rekDebit = $_GET['AKPelayananrekM']['rekDebit'];
			$model->rekKredit = $_GET['AKPelayananrekM']['rekKredit'];
		}

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        /**
         * untuk menampilkan update rekening debit / kredit
         * @param type $id
         */
        public function actionUbahRekening($id,$saldonormal = null)
	{
            $this->layout = 'frameDialog';

            $model= AKPelayananrekM::model()->findByPk($id);
            $criteria = new CdbCriteria();
            $criteria->compare("struktur_id",$model->rekening1_id);
            $criteria->compare("kelompok_id",$model->rekening2_id);
            $criteria->compare("jenis_id",$model->rekening3_id);
            $criteria->compare("obyek_id",$model->rekening4_id);
            $criteria->compare("rincianobyek_id",$model->rekening5_id);
            $criteria->compare("rincianobyek_nb",$saldonormal);
            $rekDebit = RekeningakuntansiV::model()->find($criteria);
            if(!empty($model->rekening5_id)){ 
                    $model->rekeningnama = $rekDebit->nmrincianobyek;
            } elseif(!empty($model->rekening4_id)) { 
                    $model->rekeningnama = $rekDebit->nmobyek;
            } elseif(!empty($model->rekening3_id)){
                    $model->rekeningnama = $rekDebit->nmjenis;
            } elseif(!empty($model->rekening2_id)){
                    $model->rekeningnama = $rekDebit->nmkelompok;
            } else{
                    $model->rekeningnama = $rekDebit->nmstruktur;
            }

            $this->render('_ubahRekening',array(
                    'model'=>$model
            ));
	}
       
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
            $model=AKPenjaminpasienM::model()->findByPk($id);
            if($model===null)
                    throw new CHttpException(404,'The requested page does not exist.');
            return $model;
	}
        
	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='penjaminpasien-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                AKPenjaminpasienM::model()->updateByPk($id, array('penjamin_aktif '=>false));
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new AKPelayananrekM();
            $model->attributes=$_REQUEST['AKPelayananrekM'];
            $judulLaporan='Data Pelayanan Rekening ';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
}
