<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ActionAjaxController extends Controller
{
     
    /* jurnal rek penjamin */
    public function actionGetRekeningEditDebitPenjamin()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $penjamin_id =$_POST['penjamin_id'];
              $penjaminrek_id =$_POST['penjaminrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKPenjaminRekM::model()->updateByPk($penjaminrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    
    public function actionGetRekeningEditKreditPenjamin()

    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];

              $penjamin_id =$_POST['penjamin_id'];
              $penjaminrek_id =$_POST['penjaminrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKPenjaminRekM::model()->updateByPk($penjaminrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }


    /*end jurnal rek penjamin */
    
     public function actionGetRekeningEditDebitPelayananRek(){
           if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];

              $pelayananrek_id = $_POST['pelayananrek_id'];
              
            $update = PelayananrekM::model()->updateByPk($pelayananrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
            if($update){
                $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
            }else{
                $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
            }

            echo json_encode($data);
         Yii::app()->end();
        }

     }

    
    /* jurnal rek penerimaan */
    public function actionGetRekeningEditKreditPenerimaan()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $jenispenerimaan_id =$_POST['jenispenerimaan_id'];
              $jnspenerimaanrek_id =$_POST['jnspenerimaanrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKJnsPenerimaanRekM::model()->updateByPk($jnspenerimaanrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetRekeningEditDebitPenerimaan()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $jenispenerimaan_id =$_POST['jenispenerimaan_id'];
              $jnspenerimaanrek_id =$_POST['jnspenerimaanrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKJnsPenerimaanRekM::model()->updateByPk($jnspenerimaanrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    /*end jurnal rek penerimaan */



    //getRekeningEditDebitKreditSupplier
    public function actionGetRekeningEditDebitKreditSupplier()
    {
      if(Yii::app()->request->isAjaxRequest) {
          $rekening1_id   = $_POST['rekening1_id'];
          $rekening2_id   = $_POST['rekening2_id'];
          $rekening3_id   = $_POST['rekening3_id'];
          $rekening4_id   = $_POST['rekening4_id'];
          $rekening5_id   = $_POST['rekening5_id'];
          $supplier_id    = $_POST['supplier_id'];
          $supplierrek_id = $_POST['supplierrek_id'];
          $saldonormal    = $_POST['saldonormal'];
          
          $update = AKSupplierRekM::model()->updateByPk($supplierrek_id, array('rekening1_id'=>$rekening1_id,
                                                                               'rekening2_id'=>$rekening2_id,
                                                                               'rekening3_id'=>$rekening3_id,
                                                                               'rekening4_id'=>$rekening4_id,
                                                                               'rekening5_id'=>$rekening5_id));
            if($update){
                $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
            }else{
                $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
            }
          echo json_encode($data);
      Yii::app()->end();
      }
    }

    //getRekeningEditDebitKreditSumberdana
    public function actionGetRekeningEditDebitKreditSumberdana()
    {
      if(Yii::app()->request->isAjaxRequest) {
          $rekening1_id     = $_POST['rekening1_id'];
          $rekening2_id     = $_POST['rekening2_id'];
          $rekening3_id     = $_POST['rekening3_id'];
          $rekening4_id     = $_POST['rekening4_id'];
          $rekening5_id     = $_POST['rekening5_id'];
          $sumberdana_id    = $_POST['sumberdana_id'];
          $sumberdanarek_id = $_POST['sumberdanarek_id'];
          $saldonormal      = $_POST['saldonormal'];
          
          $update = AKSumberdanaRekM::model()->updateByPk($sumberdanarek_id, array('rekening1_id'=>$rekening1_id,
                                                                               'rekening2_id'=>$rekening2_id,
                                                                               'rekening3_id'=>$rekening3_id,
                                                                               'rekening4_id'=>$rekening4_id,
                                                                               'rekening5_id'=>$rekening5_id));
          if($update){
              $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
          }else{
              $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
          }
          echo json_encode($data);
      Yii::app()->end();
      }
    }

    //getRekeningEditDebitKreditCarabayar
    public function actionGetRekeningEditDebitKreditCarabayar()
    {
      if(Yii::app()->request->isAjaxRequest) {
          $rekening1_id     = $_POST['rekening1_id'];
          $rekening2_id     = $_POST['rekening2_id'];
          $rekening3_id     = $_POST['rekening3_id'];
          $rekening4_id     = $_POST['rekening4_id'];
          $rekening5_id     = $_POST['rekening5_id'];
          $carabayar_id     = $_POST['carabayar_id'];
          $carapembrek_id   = $_POST['carapembrek_id'];
          $saldonormal      = $_POST['saldonormal'];
          
          $update = AKCarapembayarRekM::model()->updateByPk($carapembrek_id, array('rekening1_id'=>$rekening1_id,
                                                                               'rekening2_id'=>$rekening2_id,
                                                                               'rekening3_id'=>$rekening3_id,
                                                                               'rekening4_id'=>$rekening4_id,
                                                                               'rekening5_id'=>$rekening5_id));
          if($update){
              $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
          }else{
              $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
          }
          echo json_encode($data);
      Yii::app()->end();
      }
    }

    /* Jurnal Pengeluaran */
    public function actionGetRekeningEditKreditPengeluaran()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $jenispengeluaran_id =$_POST['jenispengeluaran_id'];
              $jnspengeluaranrek_id =$_POST['jnspengeluaranrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKJnsPengeluaranRekM::model()->updateByPk($jnspengeluaranrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetRekeningEditDebitPengeluaran()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $jenispengeluaran_id =$_POST['jenispengeluaran_id'];
              $jnspengeluaranrek_id =$_POST['jnspengeluaranrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKJnsPengeluaranRekM::model()->updateByPk($jnspengeluaranrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    /*end jurnal pengeluaran */
    
    /* rekening bank */
    public function actionGetRekeningEditKreditBank()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $bank_id =$_POST['bank_id'];
              $bankrek_id =$_POST['bankrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKBankRekM::model()->updateByPk($bankrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetRekeningEditDebitBank()
    {
          if(Yii::app()->request->isAjaxRequest) {
              $rekening1_id =$_POST['rekening1_id'];
              $rekening2_id =$_POST['rekening2_id'];
              $rekening3_id =$_POST['rekening3_id'];
              $rekening4_id =$_POST['rekening4_id'];
              $rekening5_id =$_POST['rekening5_id'];
              $bank_id =$_POST['bank_id'];
              $bankrek_id =$_POST['bankrek_id'];
              $saldonormal =$_POST['saldonormal'];
              
               $update =  AKBankRekM::model()->updateByPk($bankrek_id, array('rekening1_id'=>$rekening1_id,
                                                                                     'rekening2_id'=>$rekening2_id,
                                                                                     'rekening3_id'=>$rekening3_id,
                                                                                     'rekening4_id'=>$rekening4_id,
                                                                                     'rekening5_id'=>$rekening5_id));
                if($update){
                    $data['pesan']='<div class="flash-success">Ubah Data Rekening <b></b> Berhasil  Disimpan </div>';
                }else{
                    $data['pesan']='<div class="flash-error">Ubah Data Rekening <b></b> Gagal  Disimpan </div>';
                }
              echo json_encode($data);
         Yii::app()->end();
        }
    }
    /*end rekening bank */
    /**
     * actionGetRekeningPiutangPasien digunakan di :
     * akuntansi/views/jurnalPiutangPasien/_jsFunctions
     */
    public function actionGetRekeningPiutangPasien()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $model = new AKRincianpiutangrekeningpasienV;
            $format = new CustomFormat();
            if(isset($_POST['AKRincianpiutangrekeningpasienV'])){
                $model->attributes = $_POST['AKRincianpiutangrekeningpasienV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_POST['AKRincianpiutangrekeningpasienV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_POST['AKRincianpiutangrekeningpasienV']['tglAkhir']);
            }
//            echo "<pre>";
//            print_r($model->attributes);
//            exit;
            $criteria = new CDbCriteria;
            $criteria = $model->criteriaFunction();
            $models = AKRincianpiutangrekeningpasienV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                $models[$i]['saldodebit'] = 0;    
                $models[$i]['saldokredit'] = 0;    
                if($models[$i]['rincianobyek_nb'] == "D"){
                    $models[$i]['saldodebit'] = $model->saldotarif;
                }else{
                    $models[$i]['saldokredit'] = $model->saldotarif;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('akuntansi.views.jurnalPiutangPasien._rowRekening', array('modRekenings'=>$models), true)
                );                
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }        
    }
    
    /**
     * actionGetRekeningPiutangSupplier digunakan di :
     * akuntansi/views/jurnalPiutangSupplier/_jsFunctions
     */
    public function actionGetRekeningPiutangSupplier()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $model = new AKRincianfakturhutangsupplierV;
            $format = new CustomFormat();
            if(isset($_POST['AKRincianfakturhutangsupplierV'])){
                $model->attributes = $_POST['AKRincianfakturhutangsupplierV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_POST['AKRincianfakturhutangsupplierV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_POST['AKRincianfakturhutangsupplierV']['tglAkhir']);
            }
            $criteria = new CDbCriteria;
            $criteria = $model->criteriaFunction();
            $models = AKRincianfakturhutangsupplierV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                $models[$i]['saldodebit'] = 0;    
                $models[$i]['saldokredit'] = 0;    
                if($models[$i]['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $model->saldotransaksi;
                }else{
                    $models[$i]['saldokredit'] = $model->saldotransaksi;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('akuntansi.views.jurnalPiutangSupplier._rowRekening', array('modRekenings'=>$models), true)
                );                
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }        
    }
    
    /**
     * actionGetRekeningPenerimaanKas digunakan di :
     * akuntansi/views/jurnalPenerimaan/_jsFunctions
     */
    public function actionGetRekeningPenerimaanKas()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $model = new AKRincianpenerimaankasrekeningV;
            $format = new CustomFormat();
            if(isset($_POST['AKRincianpenerimaankasrekeningV'])){
                $model->attributes = $_POST['AKRincianpenerimaankasrekeningV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_POST['AKRincianpenerimaankasrekeningV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_POST['AKRincianpenerimaankasrekeningV']['tglAkhir']);
            }
            $criteria = new CDbCriteria;
            $criteria = $model->criteriaFunction();
            $models = AKRincianpenerimaankasrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                $models[$i]['saldodebit'] = 0;    
                $models[$i]['saldokredit'] = 0;    
                if($models[$i]['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $model->jmlpembayaran;
                }else{
                    $models[$i]['saldokredit'] = $model->jmlpembayaran;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('akuntansi.views.jurnalPenerimaan._rowRekening', array('modRekenings'=>$models), true)
                );                
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }        
    }
    /**
     * actionGetRekeningPengeluaranKas digunakan di :
     * akuntansi/views/jurnalPengeluaran/_jsFunctions
     */
    public function actionGetRekeningPengeluaranKas()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $model = new AKRincianpengeluarankasrekeningV;
            $format = new CustomFormat();
            if(isset($_POST['AKRincianpengeluarankasrekeningV'])){
                $model->attributes = $_POST['AKRincianpengeluarankasrekeningV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_POST['AKRincianpengeluarankasrekeningV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_POST['AKRincianpengeluarankasrekeningV']['tglAkhir']);
            }
            $criteria = new CDbCriteria;
            $criteria = $model->criteriaFunction();
            $models = AKRincianpengeluarankasrekeningV::model()->findAll($criteria);
            foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                $models[$i]['saldodebit'] = 0;    
                $models[$i]['saldokredit'] = 0;    
                if($models[$i]['saldonormal'] == "D"){
                    $models[$i]['saldodebit'] = $model->jmlkaskeluar;
                }else{
                    $models[$i]['saldokredit'] = $model->jmlkaskeluar;
                }
            }
            if(count($models)>0){
                echo CJSON::encode(
                    $this->renderPartial('akuntansi.views.jurnalPengeluaran._rowRekening', array('modRekenings'=>$models), true)
                );                
            }else{
                echo CJSON::encode();
            }
            Yii::app()->end();
        }        
    }

  public function actionambilDataGaji()
  {
    if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    {
      $periode        = explode(' ', $_POST['periode']);
      $periode_bulan  = $periode[0];
      $periode_tahun  = $periode[1];
      $format         = new CustomFormat;
      $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
      $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

      // $modPenggajian  = PenggajianpegawaiV::model()->findAllByAttributes(array(''));

      $conditions = "periodegaji = '".$periode_gaji."' ";
      $criteria       = new CDbCriteria;
      $criteria->addCondition($conditions);
      $modPenggajian  = PenggajianpegawaiV::model()->findAll($criteria);
      if(count($modPenggajian)>0){
        foreach ($modPenggajian as $i => $model) {
          $models[$i]['uraian']   = $model->nama_pegawai.' - '.$model->nopenggajian;
          $models[$i]['pegawai_id']     = $model->pegawai_id;
          $models[$i]['periodegaji']    = $model->periodegaji;
          $models[$i]['penggajianpeg_id']   = $model->penggajianpeg_id;
          $models[$i]['penerimaanbersih']   = $model->penerimaanbersih;
          $models[$i]['tglpenggajian']  = $model->tglpenggajian;
          $models[$i]['volume']       = 1;
          $models[$i]['satuanvol']    = 'BULAN';
          $models[$i]['totalharga']   = $models[$i]['volume'] * $models[$i]['penerimaanbersih'];
        }
        echo CJSON::encode(
          $this->renderPartial('akuntansi.views.penggajian._rinciangaji', array('modRinciangaji'=>$models), true)
        ); 
      }else{
        echo CJSON::encode();
      }
      Yii::app()->end();
    }
  }

  public function actiontampilRekening()
  {
    if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    {
      $periode        = explode(' ', $_POST['periode']);
      $periode_bulan  = $periode[0];
      $periode_tahun  = $periode[1];
      $format         = new CustomFormat;
      $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
      $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

      $conditions = "periodegaji = '".$periode_gaji."' ";
      $criteria       = new CDbCriteria;
      $criteria->select ='SUM(penerimaanbersih) AS penerimaanbersih';
      $criteria->addCondition($conditions);
      $modPenggajian  = PenggajianpegawaiV::model()->find($criteria);
      $modKas = $modPenggajian->penerimaanbersih;

      $criteria   = new CDbCriteria;
      $criteria->addCondition('rekening5_id IS NOT NULL');
      $criteria->order = 'nourutgaji';
      $modKomponenGaji = KomponengajiM::model()->findAll($criteria);      

      if(count($modKomponenGaji)>0){
        $ind = 1;
        foreach ($modKomponenGaji as $i => $model) {          
          $modRekening[$ind]['kdstruktur']      = $model->rekening->rekening1->kdrekening1;
          $modRekening[$ind]['struktur_id']     = $model->rekening->rekening1_id;
          $modRekening[$ind]['kdkelompok']      = $model->rekening->rekening2->kdrekening2;
          $modRekening[$ind]['kelompok_id']     = $model->rekening->rekening2_id;
          $modRekening[$ind]['kdjenis']         = $model->rekening->rekening3->kdrekening3;
          $modRekening[$ind]['jenis_id']        = $model->rekening->rekening3_id;
          $modRekening[$ind]['kdobyek']         = $model->rekening->rekening4->kdrekening4;
          $modRekening[$ind]['obyek_id']        = $model->rekening->rekening4_id; 
          $modRekening[$ind]['kdrincianobyek']  = $model->rekening->kdrekening5;
          $modRekening[$ind]['rincianobyek_id'] = $model->rekening->rekening5_id; 
          $modRekening[$ind]['nama_rekening']   = $model->rekening->nmrekening5; 
          $modRekening[$ind]['rekDebitKredit']  = $model->rekening->nmrekening5;

          $conditions = "periodegaji = '".$periode_gaji."' AND pengeluaranumum_id is null AND komponengaji_id=".$model->komponengaji_id."";
          $criteria       = new CDbCriteria;
          $criteria->select ='SUM(jumlah) AS jumlah';
          $criteria->addCondition($conditions);
          $modNilai   = RekapgajipegawaiV::model()->find($criteria);         
          
          if ($model->ispotongan == TRUE)
          {
            $modRekening[$ind]['saldodebit']      = 0;
            $modRekening[$ind]['saldokredit']     = isset($modNilai->jumlah) ? $modNilai->jumlah : 0;
          } else {
            $modRekening[$ind]['saldodebit']      = isset($modNilai->jumlah) ? $modNilai->jumlah : 0;
            $modRekening[$ind]['saldokredit']     = 0;
          }
          
          $ind++;
        }

        echo CJSON::encode(
          $this->renderPartial('akuntansi.views.penggajian._listRekening', array('modRekening'=>$modRekening, 'modKas'=>$modKas), true)
        ); 
      }else{
        echo CJSON::encode();
      }
      Yii::app()->end();
    }
  }

  public function actionambilDataPenghapusan()
  {
    if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    {
      $periode        = explode(' ', $_POST['periode']);
      $periode_bulan  = $periode[0];
      $periode_tahun  = $periode[1];
      $format         = new CustomFormat;
      $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
      $periode_hapus   = $periode_tahun.'-'.$bulan_angka;

      $jenis_inventori = $_POST['jenis'];

      $conditions = "to_char(tglpenghapusan,'yyyy-mm') = '".$periode_hapus."' AND tipepenghapusan='penjualan'";
      $criteria       = new CDbCriteria;
      $criteria->addCondition($conditions);
      if($jenis_inventori=="peralatan"){
        $criteria->addCondition("split_part(invperalatan_noregister, '-',3)='04' ");
        $modPenggajian  = InvperalatanT::model()->findAll($criteria);
        if(count($modPenggajian)>0){
          foreach ($modPenggajian as $i => $model) {
            $models[$i]['invperalatan_kode']  = $model->invperalatan_kode;
            $models[$i]['invperalatan_noregister']  = $model->invperalatan_noregister;
            $models[$i]['invperalatan_namabrg']    = $model->invperalatan_namabrg;
            $models[$i]['invperalatan_harga']    = $model->invperalatan_harga;
            $models[$i]['invperalatan_id']   = $model->invperalatan_id;
            $models[$i]['hargajualaktiva']   = $model->hargajualaktiva;
            $models[$i]['keuntungan']        = $model->keuntungan;
            $models[$i]['kerugian']          = $model->kerugian;
            $models[$i]['invperalatan_akumsusut']   = $model->invperalatan_akumsusut;
          }
          echo CJSON::encode(
            $this->renderPartial('akuntansi.views.penjualanAset._rincian', array('modRinciangaji'=>$models), true)
          ); 
        }else{
          echo CJSON::encode(
			$this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array(), false)
			);
        }
      }elseif($jenis_inventori=="peralatan_non_medis"){
        $criteria->addCondition("split_part(invperalatan_noregister, '-',3)='05' ");
        $modPenggajian  = InvperalatanT::model()->findAll($criteria);
        if(count($modPenggajian)>0){
          foreach ($modPenggajian as $i => $model) {
            $models[$i]['invperalatan_kode']  = $model->invperalatan_kode;
            $models[$i]['invperalatan_noregister']  = $model->invperalatan_noregister;
            $models[$i]['invperalatan_namabrg']    = $model->invperalatan_namabrg;
            $models[$i]['invperalatan_harga']    = $model->invperalatan_harga;
            $models[$i]['invperalatan_id']   = $model->invperalatan_id;
            $models[$i]['hargajualaktiva']   = $model->hargajualaktiva;
            $models[$i]['keuntungan']        = $model->keuntungan;
            $models[$i]['kerugian']          = $model->kerugian;
            $models[$i]['invperalatan_akumsusut']   = $model->invperalatan_akumsusut;
          }
          echo CJSON::encode(
            $this->renderPartial('akuntansi.views.penjualanAset._rincian', array('modRinciangaji'=>$models), true)
          ); 
        }else{
          echo CJSON::encode(
			$this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array(), false)
			);
        }
      }elseif($jenis_inventori=="tanah"){
        $modPenggajian  = InvtanahT::model()->findAll($criteria);
        if(count($modPenggajian)>0){
          foreach ($modPenggajian as $i => $model) {
            $models[$i]['invtanah_kode']  = $model->invtanah_kode;
            $models[$i]['invtanah_noregister']  = $model->invtanah_noregister;
            $models[$i]['invtanah_namabrg']     = $model->invtanah_namabrg;
            $models[$i]['invtanah_harga']   = $model->invtanah_harga;
            $models[$i]['invtanah_id']      = $model->invtanah_id;
            $models[$i]['hargajualaktiva']  = $model->hargajualaktiva;
            $models[$i]['keuntungan']       = $model->keuntungan;
            $models[$i]['kerugian']         = $model->kerugian;
            $models[$i]['invtanah_alamat']  = $model->invtanah_alamat;
          }
          echo CJSON::encode(
            $this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array('modRinciangaji'=>$models), true)
          ); 
        }else{
          echo CJSON::encode(
			$this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array(), false)
			);
        }
      }elseif($jenis_inventori=="gedung"){
        $modPenggajian  = InvgedungT::model()->findAll($criteria);
        if(count($modPenggajian)>0){
          foreach ($modPenggajian as $i => $model) {
            $models[$i]['invgedung_kode']  = $model->invgedung_kode;
            $models[$i]['invgedung_noregister']  = $model->invgedung_noregister;
            $models[$i]['invgedung_namabrg']     = $model->invgedung_namabrg;
            $models[$i]['invgedung_harga']   = $model->invgedung_harga;
            $models[$i]['invgedung_id']      = $model->invgedung_id;
            $models[$i]['hargajualaktiva']  = $model->hargajualaktiva;
            $models[$i]['keuntungan']       = $model->keuntungan;
            $models[$i]['kerugian']         = $model->kerugian;
            $models[$i]['invgedung_akumsusut']   = $model->invgedung_akumsusut;
          }
          echo CJSON::encode(
            $this->renderPartial('akuntansi.views.penjualanAset._rincianGedung', array('modRinciangaji'=>$models), true)
          ); 
        }else{
          echo CJSON::encode(
			$this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array(), false)
			);
        }
      }elseif($jenis_inventori=="kendaraan"){
        $criteria->addCondition("split_part(invperalatan_noregister, '-',3)='03' ");
        $modPenggajian  = InvperalatanT::model()->findAll($criteria);
        if(count($modPenggajian)>0){
          foreach ($modPenggajian as $i => $model) {
            $models[$i]['invperalatan_kode']  = $model->invperalatan_kode;
            $models[$i]['invperalatan_noregister']  = $model->invperalatan_noregister;
            $models[$i]['invperalatan_namabrg']    = $model->invperalatan_namabrg;
            $models[$i]['invperalatan_harga']    = $model->invperalatan_harga;
            $models[$i]['invperalatan_id']   = $model->invperalatan_id;
            $models[$i]['hargajualaktiva']   = $model->hargajualaktiva;
            $models[$i]['keuntungan']        = $model->keuntungan;
            $models[$i]['kerugian']          = $model->kerugian;
            $models[$i]['invperalatan_akumsusut']   = $model->invperalatan_akumsusut;
          }
          echo CJSON::encode(
            $this->renderPartial('akuntansi.views.penjualanAset._rincian', array('modRinciangaji'=>$models), true)
          ); 
        }else{
          echo CJSON::encode(
			$this->renderPartial('akuntansi.views.penjualanAset._rincianTanah', array(), false)
			);
        }
      }
      Yii::app()->end();
    }
  }
    
}
?>
