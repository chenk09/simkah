
<?php

class RekeningPengeluaranController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'create';
        public $pathJenisPengeluaran = 'akuntansi.views.jurnalRekPengeluaran.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
        
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','print','ubahRekeningDebit','ubahRekeningKredit','penerimaan'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new AKJnsPengeluaranRekM();

		if(isset($_POST['AKJnsPengeluaranRekM']))
		{
                        if (count($_POST['AKJnsPengeluaranRekM']) > 0) {
                            $modDetails = $this->validasiTabular($_POST['AKJnsPengeluaranRekM']);
                        }
                            $transaction = Yii::app()->db->beginTransaction();
                            try{
                                 $success = true;
                                 $modDetails = $this->validasiTabular($_POST['AKJnsPengeluaranRekM']);
                                    foreach ($modDetails as $i => $data) {
                                        if ($data->jnspengeluaranrek_id > 0) {
                                            if ($data->update()) {
                                                $success = true;
                                                
                                            } else {
                                                $success = false;
                                            }
                                        }else{
                                            $data->save();
                                        }
                                    }
                                if ($success == true) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                    $this->redirect(array('admin','id'=>'1'));
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error', "Data gagal disimpan ");
                                }
                                
                            }
                            catch(Exception $ex){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
                            }

		}

		$this->render('create',array(
			'model'=>$model, 'modDetails'=>$modDetails,
		));
	}
        
        protected function validasiTabular($data){
            sort($data['rekening']);
            foreach ($data['rekening'] as $i=>$row){
                if($row['rekening1_id'] > 0)
                {
                    $modDetails[$i] = new AKJnsPengeluaranRekM();
                    $modDetails[$i]->attributes = $row;                
                    $modDetails[$i]->rekening1_id = $row['rekening1_id'];
                    $modDetails[$i]->rekening2_id = $row['rekening2_id'];
                    $modDetails[$i]->rekening3_id = $row['rekening3_id'];
                    $modDetails[$i]->rekening4_id = $row['rekening4_id'];
                    $modDetails[$i]->rekening5_id = $row['rekening5_id'];
                    $modDetails[$i]->saldonormal = $row['saldonormal'];
                    $modDetails[$i]->jenispengeluaran_id = $_POST['AKJnsPengeluaranRekM']['jenispengeluaran_id'];
                    $modDetails[$i]->validate();                    
                }
            }
            return $modDetails;
        }
        
       public function actionAdmin(){
           
          $model = AKJenispengeluaranM;
          
          $this->redirect(Yii::app()->createUrl('akuntansi/jurnalRekPengeluaran/admin')); 
       }
       
       public function actionUbahRekeningDebit($id)
       {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		 $this->layout = 'frameDialog';
                $model= AKJnsPengeluaranRekM::model()->findByPk($id);
                $modPengeluaran = AKJenispengeluaranM::model()->findByPk($model->jenispengeluaran_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKJnsPenerimaanRekM']))
		{
                        $model->attributes=$_POST['AKJnsPenerimaanRekM'];
                        $view = 'UbahRekeningKredit';
                           
                        $update = AKJnsPengeluaranRekM::model()->updateByPk($id,array('rekening5_id'=>$_POST['AKJnsPengeluaranRekM']['rekening5_id'],
                                                                                'rekening4_id'=>$_POST['AKJnsPengeluaranRekM']['rekening4_id'],
                                                                                'rekening3_id'=>$_POST['AKJnsPengeluaranRekM']['rekening3_id'],
                                                                                'rekening2_id'=>$_POST['AKJnsPengeluaranRekM']['rekening2_id'],
                                                                                'rekening1_id'=>$_POST['AKJnsPengeluaranRekM']['rekening1_id']));
			if($update){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				if (isset($_GET['frame']) && !empty($_GET['idPengeluaran'])){
                                    $this->redirect(array(((isset($view)) ? $view : 'UbahRekeningKredit'),'id'=>$model->jnspengeluaranrek_id, 'frame'=>$_GET['frame'], 'idPengeluaran'=>$_GET['idPengeluaran']));
                                }else{
                                    $this->redirect(array(((isset($view)) ? $view : 'admin'),'id'=>$model->jenispengeluaranrek_id));
                                }
                        }
                }

		$this->render(((isset($view)) ? $view : '_ubahRekeningDebit'),array(
			'model'=>$model,
                        'modPengeluaran'=>$modPengeluaran
		));
	}
        public function actionUbahRekeningKredit($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		 $this->layout = 'frameDialog';
                $model= AKJnsPengeluaranRekM::model()->findByPk($id);
                $modPengeluaran = AKJenispengeluaranM::model()->findByPk($model->jenispengeluaran_id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKJnsPenerimaanRekM']))
		{
                        $model->attributes=$_POST['AKJnsPenerimaanRekM'];
                        $view = 'UbahRekeningKredit';
                           
                        $update = AKJnsPengeluaranRekM::model()->updateByPk($id,array('rekening5_id'=>$_POST['AKJnsPengeluaranRekM']['rekening5_id'],
                                                                                'rekening4_id'=>$_POST['AKJnsPengeluaranRekM']['rekening4_id'],
                                                                                'rekening3_id'=>$_POST['AKJnsPengeluaranRekM']['rekening3_id'],
                                                                                'rekening2_id'=>$_POST['AKJnsPengeluaranRekM']['rekening2_id'],
                                                                                'rekening1_id'=>$_POST['AKJnsPengeluaranRekM']['rekening1_id']));
			if($update){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				if (isset($_GET['frame']) && !empty($_GET['idPengeluaran'])){
                                    $this->redirect(array(((isset($view)) ? $view : 'UbahRekeningKredit'),'id'=>$model->jnspengeluaranrek_id, 'frame'=>$_GET['frame'], 'idPengeluaran'=>$_GET['idPengeluaran']));
                                }else{
                                    $this->redirect(array(((isset($view)) ? $view : 'admin'),'id'=>$model->jenispengeluaranrek_id));
                                }
                        }
                }

		$this->render(((isset($view)) ? $view : '_ubahRekeningKredit'),array(
			'model'=>$model,
                        'modPengeluaran'=>$modPengeluaran
		));
	}
}
