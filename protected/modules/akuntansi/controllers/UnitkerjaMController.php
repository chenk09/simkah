<?php

class UnitkerjaMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
	public $defaultAction = 'admin';
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AKUnitkerjaM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKUnitkerjaM']))
		{
								$model->attributes=$_POST['AKUnitkerjaM'];
                          //       $hitung = count($_POST['ruangan_id']);
                          //       $model->ruangan_id = $_POST['ruangan_id'][$i];
                   
                          //       $unitkerja_id=$model->unitkerja_id;
                          //       for($i=0;$i<$hitung;$i++){
		                        //     $model->unitkerja_id = $unitkerja_id;
		                        //     $model->ruangan_id = $_POST['ruangan_id'][$i]; 
		                        // }

                                    if($model->save()) {
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->redirect(array('admin','id'=>$model->unitkerja_id));
                                    }
		
				}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function insertRuanganPemakai($id)
                {
                         $status = TRUE;
                         $hitung = count($_POST['ruangan_id']);
                         if($hitung < 1){return $status = TRUE;} //kondisi apabila tidak ada inputan di emultiselect
                         for($i=0;$i<$hitung;$i++){
                            $ruangan = new SARuanganM;
                            $ruangan->loginpemakai_id = $id;
                            $ruangan->ruangan_id = $_POST['ruangan_id'][$i];
                            if($ruangan->save()){
                                $status = TRUE;
                            }else{
                                $status = FALSE;
                            }
                         }
                             
                          return $status;
                    
                }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AKUnitkerjaM']))
		{
			$model->attributes=$_POST['AKUnitkerjaM'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->unitkerja_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			// $this->loadModel($id)->delete();
			$id = $_POST['id'];
            $this->loadModel($id)->delete();
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = AKUnitkerjaM::model()->updateByPk($id,array('unitkerja_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AKUnitkerjaM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AKUnitkerjaM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AKUnitkerjaM']))
			$model->attributes=$_GET['AKUnitkerjaM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=AKUnitkerjaM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='akunitkerja-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
