<?php
class LaporanAkuntansiController extends SBaseController
{
    public function actionLaporanJurnal()
    {
        $model = new AKLaporanJurnalV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['AKLaporanJurnalV']))
        {
            $model->attributes = $_GET['AKLaporanJurnalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalV']['tglAkhir']);

        }
        $this->render('jurnal/admin',array('model'=>$model));
    }

    public function actionPrintLaporanJurnal() {
        $model = new AKLaporanJurnalV('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Jurnal';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Jurnal Berdasarkan Jenis Jurnal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['AKLaporanJurnalV'])) {
            $model->attributes = $_REQUEST['AKLaporanJurnalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanJurnalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanJurnalV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jurnal/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionFrameGrafikLaporanJurnal() {
        $this->layout = '//layouts/frameDialog';
        $model = new AKLaporanJurnalV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jurnal Berdasarkan Jenis Jurnal';
        $data['type'] = $_GET['type'];
        if (isset($_GET['AKLaporanJurnalV'])) {
            $model->attributes = $_GET['AKLaporanJurnalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    
    public function actionLaporanJurnalNew()
    {
        $model = new AKLaporanJurnalVNew;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['AKLaporanJurnalVNew']))
        {
            $model->attributes = $_GET['AKLaporanJurnalVNew'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalVNew']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalVNew']['tglAkhir']);

        }
        $this->render('jurnalNew/admin',array('model'=>$model));
    }

    public function actionPrintLaporanJurnalNew() {
        $model = new AKLaporanJurnalVNew('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Jurnal';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Jurnal Berdasarkan Jenis Jurnal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['AKLaporanJurnalVNew'])) {
            $model->attributes = $_REQUEST['AKLaporanJurnalVNew'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanJurnalVNew']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanJurnalVNew']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jurnalNew/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionFrameGrafikLaporanJurnalNew() {
        $this->layout = '//layouts/frameDialog';
        $model = new AKLaporanJurnalVNew('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jurnal Berdasarkan Jenis Jurnal';
        $data['type'] = $_GET['type'];
        if (isset($_GET['AKLaporanJurnalVNew'])) {
            $model->attributes = $_GET['AKLaporanJurnalVNew'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalVNew']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanJurnalVNew']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    
    public function actionLaporanSaldoPerAkun()
    {
        $model = new AKBukubesarT();
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['AKBukubesarT']))
        {
            $model->attributes = $_GET['AKBukubesarT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKBukubesarT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKBukubesarT']['tglAkhir']);
        }
        $this->render('saldoperakun/admin',array('model'=>$model));
    }

    public function actionPrintLaporanSaldoPerAkun() {
        $model = new AKBukubesarT('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Saldo Per Akun';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Saldo Per Akun';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['AKBukubesarT'])) {
            $model->attributes = $_REQUEST['AKBukubesarT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKBukubesarT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKBukubesarT']['tglAkhir']);
            
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'saldoperakun/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionFrameGrafikLaporanSaldoPerAkun() {
        $this->layout = '//layouts/frameDialog';
        $model = new AKBukubesarT('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Saldo Per Akun';
        $data['type'] = $_GET['type'];
        if (isset($_GET['AKBukubesarT'])) {
            $model->attributes = $_GET['AKBukubesarT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKBukubesarT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKBukubesarT']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    
    public function actionLaporanBukuBesar()
    {
        $modelLaporan = new AKLaporanBukuBesarV;
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if(isset($_GET['AKLaporanBukuBesarV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanBukuBesarV'];
            $format = new CustomFormat();
            //$format = new MyFormatter();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAkhir']);
            $modelLaporan->nmrekening5 = $_GET['AKLaporanBukuBesarV']['namaRekening'];
            $modelLaporan->kdrekening5 = $_GET['AKLaporanBukuBesarV']['kodeRekening'];
            $criteria->compare('nmrekening5',$_GET['AKLaporanBukuBesarV']['namaRekening']);
            $criteria->compare('kdrekening5',$_GET['AKLaporanBukuBesarV']['kodeRekening']);
            $criteria->compare('ruangan_id',$_GET['AKLaporanBukuBesarV']['ruangan_id']);
            if(empty($_GET['AKLaporanBukuBesarV']['namaRekening'])){
                $qr_nmrekening5 = null;
            }else{
                $qr_nmrekening5 = "AND nmrekening5 = '".$_GET['AKLaporanBukuBesarV']['namaRekening']."'";
            }
            if(empty($_GET['AKLaporanBukuBesarV']['kodeRekening'])){
                $qr_kdrekening5 = null;
            }else{
                $qr_kdrekening5 = "AND kdrekening5 = '".$_GET['AKLaporanBukuBesarV']['kodeRekening']."'";
            }
        }else{
            $qr_nmrekening5 = null;
            $qr_kdrekening5 = null;
        }
        $criteria->addBetweenCondition('tglbukubesar',$modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['AKLaporanBukuBesarV']['ruangan_id']);
        $criteria->order = 'rekening1_id, rekening2_id, rekening3_id, rekening4_id, rekening5_id, tglbukubesar, nourut';
        $model = AKLaporanBukuBesarV::model()->findAll($criteria);

        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";
            
        $sql= "select count(nmrekening5) as urutan from LaporanBukuBesar_V WHERE ".$query_ruangan_id." tglbukubesar>='".$modelLaporan->tglAwal."' AND tglbukubesar<='".$modelLaporan->tglAkhir."' ".$qr_nmrekening5." ".$qr_kdrekening5." group by nmrekening5, rekening1_id, rekening2_id, rekening3_id, rekening4_id, rekening5_id order by rekening1_id, rekening2_id,rekening3_id, rekening4_id, rekening5_id";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();

        $this->render('bukuBesar/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan, 'jmlrekening'=>$jmlRekening));
    }

    public function actionPrintLaporanBukuBesar() {

        $modelLaporan = new AKLaporanBukuBesarV;
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $criteria = new CDbCriteria;
        if(isset($_GET['AKLaporanBukuBesarV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanBukuBesarV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAkhir']);
            $modelLaporan->nmrekening5 = $_GET['AKLaporanBukuBesarV']['namaRekening'];
            $modelLaporan->kdrekening5 = $_GET['AKLaporanBukuBesarV']['kodeRekening'];
            $criteria->compare('nmrekening5',$_GET['AKLaporanBukuBesarV']['namaRekening']);
            $criteria->compare('kdrekening5',$_GET['AKLaporanBukuBesarV']['kodeRekening']);

            if(empty($_GET['AKLaporanBukuBesarV']['namaRekening'])){
                $qr_nmrekening5 = null;
            }else{
                $qr_nmrekening5 = "AND nmrekening5 = '".$_GET['AKLaporanBukuBesarV']['namaRekening']."'";
            }
            if(empty($_GET['AKLaporanBukuBesarV']['kodeRekening'])){
                $qr_kdrekening5 = null;
            }else{
                $qr_kdrekening5 = "AND kdrekening5 = '".$_GET['AKLaporanBukuBesarV']['kodeRekening']."'";
            }
        }else{
            $qr_nmrekening5 = null;
            $qr_kdrekening5 = null;
        }
        $criteria->addBetweenCondition('tglbukubesar',$modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['AKLaporanBukuBesarV']['ruangan_id']);
        $criteria->order = 'rekening1_id, rekening2_id, rekening3_id, rekening4_id, rekening5_id, tglbukubesar, nourut';
        $model = AKLaporanBukuBesarV::model()->findAll($criteria);

        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";

        $sql= "select count(nmrekening5) as urutan from LaporanBukuBesar_V WHERE ".$query_ruangan_id." tglbukubesar>='".$modelLaporan->tglAwal."' AND tglbukubesar<='".$modelLaporan->tglAkhir."' ".$qr_nmrekening5." ".$qr_kdrekening5." group by nmrekening5, rekening1_id, rekening2_id, rekening3_id, rekening4_id, rekening5_id order by rekening1_id, rekening2_id,rekening3_id, rekening4_id, rekening5_id";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();
        
        if($_REQUEST['caraPrint']== 'GRAFIK'){
            $model = new AKLaporanBukuBesarV;
            
            if (isset($_REQUEST['AKLaporanBukuBesarV'])) {
                $model->attributes = $_REQUEST['AKLaporanBukuBesarV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanBukuBesarV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanBukuBesarV']['tglAkhir']);
            }

        }
        $judulLaporan = 'Laporan Buku Besar Berdasarkan Nama Rekening';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Buku Besar Berdasarkan Nama Rekening';
        $data['type'] = $_REQUEST['type'];
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuBesar/_print';
        
        $periode    = $format->formatDateINAtime($modelLaporan->tglAwal).' s/d '.$format->formatDateINAtime($modelLaporan->tglAkhir);

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Buku Besar';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('bukuBesar/_print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('bukuBesar/_print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('bukuBesar/_print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
    

    public function actionFrameGrafikLaporanBukuBesar() {
        $this->layout = '//layouts/frameDialog';
        $model = new AKLaporanBukuBesarV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Besar Berdasarkan Nama Rekening';
        $data['type'] = $_GET['type'];
        if (isset($_GET['AKLaporanBukuBesarV'])) {
            $model->attributes = $_GET['AKLaporanBukuBesarV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanBukuBesarV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
             $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanNeracaNew()
    {
        $modelLaporan = new AKLaporanneracanewV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanneracanewV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracanewV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracanewV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracanewV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanneracanewV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanneracanewV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanneracanewV::model()->findAll($criteria);
        
        $this->render('neracaNew/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan));
    }
    
    public function actionPrintLaporanNeracaNew()
    {
         $modelLaporan = new AKLaporanneracanewV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanneracanewV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracanewV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracanewV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracanewV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanneracanewV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanneracanewV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanneracanewV::model()->findAll($criteria);
        
        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Posisi Keuangan / Neraca';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('neracaNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('neracaNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('neracaNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
     
    public function actionLaporanNeracaDetail()
    {
        $modelLaporan = new AKLaporanneracadetailV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanneracadetailV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracadetailV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracadetailV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracadetailV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanneracadetailV::model()->findAll($criteria);
        
        $this->render('neracaDetail/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan));
    }
    
    public function actionPrintLaporanNeracaDetail()
    {
         $modelLaporan = new AKLaporanneracadetailV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanneracadetailV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracadetailV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracadetailV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracadetailV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanneracadetailV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanneracadetailV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanneracanewV::model()->findAll($criteria);
        
        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Posisi Keuangan / Neraca Detail';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('neracaDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('neracaDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('neracaDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanLabaRugiNew()
    {
        $modelLaporan = new AKLaporanrugilabanewV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanrugilabanewV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanrugilabanewV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabanewV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabanewV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanrugilabanewV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanrugilabanewV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanrugilabanewV::model()->findAll($criteria);
        
        $this->render('labarugiNew/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan));
    }
    
    public function actionPrintLaporanLabaRugiNew()
    {
         $modelLaporan = new AKLaporanrugilabanewV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanrugilabanewV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanrugilabanewV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabanewV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabanewV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanrugilabanewV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanrugilabanewV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanrugilabanewV::model()->findAll($criteria);
        
        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Laba Rugi';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('labarugiNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('labarugiNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('labarugiNew/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanLabaRugiDetail()
    {
        $modelLaporan = new AKLaporanrugilabadetailV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanrugilabadetailV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanrugilabadetailV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabadetailV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabadetailV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanrugilabadetailV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanrugilabadetailV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanrugilabadetailV::model()->findAll($criteria);
        
        $this->render('labarugiDetail/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan));
    }
    
    public function actionPrintLaporanLabaRugiDetail()
    {
         $modelLaporan = new AKLaporanrugilabadetailV();
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');
        $model = array();
        if(isset($_GET['AKLaporanrugilabadetailV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanrugilabadetailV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabadetailV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanrugilabadetailV']['tglAkhir']);
            
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;
        
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tgljurnalpost', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        if(!empty($_GET['AKLaporanrugilabadetailV']['rekperiod_id'])){
            $criteria->addCondition('rekperiod_id='.$_GET['AKLaporanrugilabadetailV']['rekperiod_id']);
        }
        $criteria->group = 'rekening1_id, nmrekening1, rekperiod_id';
        $criteria->select = $criteria->group;
        $model = AKLaporanrugilabadetailV::model()->findAll($criteria);
        
        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Laba Rugi Detail';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('labarugiDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('labarugiDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('labarugiDetail/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanNeraca()
    {
        $modelLaporan = new AKLaporanneracaV('searchNeraca');
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['AKLaporanneracaV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracaV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAkhir']);
            $modelLaporan->ruangan_id = $_GET['AKLaporanneracaV']['ruangan_id'];  
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglbukubesar', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['LaporanneracaV']['ruangan_id']);
        $criteria->group = 'struktur_id, kelompok_id, jenis_id, nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis, nmrekening4';
        $criteria->select = $criteria->group.', sum(saldonominal) as nominal';
        $criteria->order = 'kdstruktur, kdkelompok, kdjenis';
        $model = LaporanneracaV::model()->findAll($criteria);
        $modTotalNominal = new AKLaporanneracaV();
        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";

        $sql= "select count(tab.nmkelompok) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmkelompok, tab.nmstruktur, tab.kdstruktur, tab.kdkelompok order by tab.kdstruktur, tab.kdkelompok";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();

        $sql_struktur= "select count(tab.nmstruktur) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmstruktur, tab.kdstruktur order by tab.kdstruktur";
        $jmlStruktur = Yii::app()->db->createCommand($sql_struktur)->queryAll();
        $this->render('neraca/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur, 'modTotalNominal'=>$modTotalNominal));
    }

    public function actionPrintLaporanNeraca()
    {
        $modelLaporan = new AKLaporanneracaV('searchNeraca');
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');

   
        if(isset($_GET['AKLaporanneracaV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracaV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAkhir']);
            $modelLaporan->ruangan_id = $_GET['AKLaporanneracaV']['ruangan_id'];
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglbukubesar', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['LaporanneracaV']['ruangan_id']);
        $criteria->group = 'struktur_id, kelompok_id, jenis_id, nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
        $criteria->select = $criteria->group.', sum(saldonominal) as nominal';
        $criteria->order = 'kdstruktur, kdkelompok, kdjenis';
        $model = LaporanneracaV::model()->findAll($criteria);

        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";

        $sql= "select count(tab.nmkelompok) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmkelompok, tab.nmstruktur, tab.kdstruktur, tab.kdkelompok order by tab.kdstruktur, tab.kdkelompok";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();

        $sql_struktur= "select count(tab.nmstruktur) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmstruktur, tab.kdstruktur order by tab.kdstruktur";
        $jmlStruktur = Yii::app()->db->createCommand($sql_struktur)->queryAll();

        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Posisi Keuangan / Neraca';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            // $this->render('neraca/admin',array('model'=>$model,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur));
            $this->render('neraca/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            // $this->render('neraca/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
            $this->render('neraca/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('neraca/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }
    
    public function actionLaporanNeracaPerUnit()
    {
        $modelLaporan = new AKLaporanneracaV('searchNeraca');
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['AKLaporanneracaV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracaV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAkhir']);
            $modelLaporan->ruangan_id = $_GET['AKLaporanneracaV']['ruangan_id'];  
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglbukubesar', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['LaporanneracaV']['ruangan_id']);
        $criteria->group = 'struktur_id, kelompok_id, jenis_id, nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis, nmrekening4';
        $criteria->select = $criteria->group.', sum(saldonominal) as nominal';
        $criteria->order = 'kdstruktur, kdkelompok, kdjenis';
        $model = LaporanneracaV::model()->findAll($criteria);
        $modTotalNominal = new AKLaporanneracaV();
        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";

        $sql= "select count(tab.nmkelompok) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmkelompok, tab.nmstruktur, tab.kdstruktur, tab.kdkelompok order by tab.kdstruktur, tab.kdkelompok";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();

        $sql_struktur= "select count(tab.nmstruktur) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmstruktur, tab.kdstruktur order by tab.kdstruktur";
        $jmlStruktur = Yii::app()->db->createCommand($sql_struktur)->queryAll();
        $this->render('neraca/perunit/admin',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur, 'modTotalNominal'=>$modTotalNominal));
    }

    public function actionPrintLaporanNeracaPerUnit()
    {
        $modelLaporan = new AKLaporanneracaV('searchNeraca');
        $modelLaporan->unsetAttributes();
        $modelLaporan->tglAwal = date('d M Y 00:00:00');
        $modelLaporan->tglAkhir = date('d M Y 23:59:59');

   
        if(isset($_GET['AKLaporanneracaV']))
        {
            $modelLaporan->attributes = $_GET['AKLaporanneracaV'];
            $format = new CustomFormat();
            $modelLaporan->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAwal']);
            $modelLaporan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanneracaV']['tglAkhir']);
            $modelLaporan->ruangan_id = $_GET['AKLaporanneracaV']['ruangan_id'];
        }
        $tglAwal    = $modelLaporan->tglAwal;
        $tglAkhir   = $modelLaporan->tglAkhir;

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglbukubesar', $modelLaporan->tglAwal, $modelLaporan->tglAkhir);
        $criteria->compare('ruangan_id',$_GET['LaporanneracaV']['ruangan_id']);
        $criteria->group = 'struktur_id, kelompok_id, jenis_id, nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis';
        $criteria->select = $criteria->group.', sum(saldonominal) as nominal';
        $criteria->order = 'kdstruktur, kdkelompok, kdjenis';
        $model = LaporanneracaV::model()->findAll($criteria);

        if($modelLaporan->ruangan_id=='')
            $query_ruangan_id = null;
        else
            $query_ruangan_id = "ruangan_id=".$modelLaporan->ruangan_id." AND";

        $sql= "select count(tab.nmkelompok) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmkelompok, tab.nmstruktur, tab.kdstruktur, tab.kdkelompok order by tab.kdstruktur, tab.kdkelompok";
        $jmlRekening = Yii::app()->db->createCommand($sql)->queryAll();

        $sql_struktur= "select count(tab.nmstruktur) as urutan from (SELECT kdstruktur, kdkelompok, kdjenis, nmstruktur, nmkelompok, nmjenis, sum(saldonominal) as nominal, count(nmjenis) as jml_nmjenis FROM laporanneraca_v where ".$query_ruangan_id." tglbukubesar<='".$tglAkhir."' and tglbukubesar>='".$tglAwal."' group by nmjenis, nmkelompok, nmstruktur, kdstruktur, kdkelompok, kdjenis order by kdstruktur, kdkelompok, kdjenis) tab group by tab.nmstruktur, tab.kdstruktur order by tab.kdstruktur";
        $jmlStruktur = Yii::app()->db->createCommand($sql_struktur)->queryAll();

        $periode    = $tglAwal.' s/d '.$tglAkhir;

        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='Laporan Posisi Keuangan / Neraca';

        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            // $this->render('neraca/admin',array('model'=>$model,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur));
            $this->render('neraca/perunit/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            // $this->render('neraca/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
            $this->render('neraca/perunit/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('neraca/Print',array('model'=>$model,'modelLaporan'=>$modelLaporan,'jmlRekening'=>$jmlRekening, 'jmlStruktur'=>$jmlStruktur, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }
    }

    public function actionLaporanLabaRugiPerUnit()
    {
        $model = new LaporanlabarugiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['LaporanlabarugiV']))
        {
            $model->attributes = $_GET['LaporanlabarugiV'];
            $format = new CustomFormat();
             $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanlabarugiV']['tglAwal']);
             $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanlabarugiV']['tglAkhir']);
        }
        $this->render('labarugiperunit/admin',array('model'=>$model));
    }

    public function actionPrintLaporanLabaRugiPerUnit()
    {
        $model = new LaporanlabarugiV('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Laba Rugi';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Laba Rugi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporanlabarugiV'])) {
            $model->attributes = $_REQUEST['LaporanlabarugiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanlabarugiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanlabarugiV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'labarugiperunit/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionLaporanLabaRugi()
    {
        $model = new LaporanlabarugiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['LaporanlabarugiV']))
        {
            $model->attributes = $_GET['LaporanlabarugiV'];
            $format = new CustomFormat();
             $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanlabarugiV']['tglAwal']);
             $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanlabarugiV']['tglAkhir']);
        }
        $this->render('labarugi/admin',array('model'=>$model));
    }

    public function actionPrintLaporanLabaRugi()
    {
        $model = new LaporanlabarugiV('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Laba Rugi';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Laba Rugi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporanlabarugiV'])) {
            $model->attributes = $_REQUEST['LaporanlabarugiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanlabarugiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanlabarugiV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'labarugi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
    }

	/**
    * Laporan Arus Kas
    * Tgl 30 Mei 2014
    * @author : Hardi
    * EHJ-2004
    */
    public function actionLaporanArusKas()
    {
        $model = new AKLaporanaruskasV('searchArusKas');
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        $periode['tglAwal']     = date('Y-m-d 00:00:00');
        $periode['tglAkhir']    = date('Y-m-d H:i:s');

        if(isset($_GET['AKLaporanaruskasV']))
        {
            $model->attributes = $_GET['AKLaporanaruskasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanaruskasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanaruskasV']['tglAkhir']);
            $model->ruangan_id = $_GET['AKLaporanaruskasV']['ruangan_id'];

            $periode['tglAwal']     = $model->tglAwal;
            $periode['tglAkhir']    = $model->tglAkhir;
        }

        $this->render('aruskas/admin',array('model'=>$model, 'periode'=>$periode));
    }

    public function actionPrintLaporanArusKas() 
    {
        $model = new AKLaporanaruskasV('getInstalasi');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Arus Kas';
        $model->tglAwal     = date('Y-m-d 00:00:00');
        $model->tglAkhir    = date('Y-m-d H:i:s');
        
        //Data Grafik       
        $data['title'] = 'Grafik Laporan Arus Kas';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['AKLaporanaruskasV'])) {
            $model->attributes = $_REQUEST['AKLaporanaruskasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanaruskasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanaruskasV']['tglAkhir']);
            $model->ruangan_id = $_GET['LaporanaruskasV']['ruangan_id'];

        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'aruskas/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
	
	
    /**
    * Laporan Arus Kas Per Unit
    * Tgl 23 Feb 2015
    * @author : Arie Satriananta
    * EHJ-3286
    */
    public function actionLaporanArusKasPerUnit()
    {
        $model = new AKLaporanaruskasV('searchArusKas');
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        $periode['tglAwal']     = date('Y-m-d 00:00:00');
        $periode['tglAkhir']    = date('Y-m-d H:i:s');

        if(isset($_GET['AKLaporanaruskasV']))
        {
            $model->attributes = $_GET['AKLaporanaruskasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanaruskasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanaruskasV']['tglAkhir']);
            $model->ruangan_id = $_GET['AKLaporanaruskasV']['ruangan_id'];

            $periode['tglAwal']     = $model->tglAwal;
            $periode['tglAkhir']    = $model->tglAkhir;
        }

        $this->render('aruskasperunit/admin',array('model'=>$model, 'periode'=>$periode));
    }

    public function actionPrintLaporanArusKasPerUnit() 
    {
        $model = new AKLaporanaruskasV('getInstalasi');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Arus Kas Per Unit';
        $model->tglAwal     = date('Y-m-d 00:00:00');
        $model->tglAkhir    = date('Y-m-d H:i:s');
        
        //Data Grafik       
        $data['title'] = 'Grafik Laporan Arus Kas Per Unit';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['AKLaporanaruskasV'])) {
            $model->attributes = $_REQUEST['AKLaporanaruskasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanaruskasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanaruskasV']['tglAkhir']);
            $model->ruangan_id = $_GET['LaporanaruskasV']['ruangan_id'];

        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'aruskasperunit/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
	
    /**
    * Laporan Perubahan Modal
    * Tgl 02 Juni 2014
    * @author : Hardi
    * EHJ-2008
    */
    public function actionLaporanPerubahanModal()
    {
        $model = new AKLaporanperubahanmodalV('search');
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        $periode['tglAwal']     = date('Y-m-d 00:00:00');
        $periode['tglAkhir']    = date('Y-m-d H:i:s');

        if(isset($_GET['AKLaporanperubahanmodalV']))
        {
            $model->attributes = $_GET['AKLaporanperubahanmodalV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['AKLaporanperubahanmodalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['AKLaporanperubahanmodalV']['tglAkhir']);
            $model->ruangan_id = $_GET['AKLaporanperubahanmodalV']['ruangan_id'];
            $periode['tglAwal']     = $model->tglAwal;
            $periode['tglAkhir']    = $model->tglAkhir;
        }

        $this->render('perubahanmodal/admin',array('model'=>$model, 'periode'=>$periode,'format'=>$format));
    }

    public function actionPrintLaporanPerubahanModal() 
    {
        $model = new AKLaporanperubahanmodalV('search');
        $model->unsetAttributes();
        $format = new CustomFormat();
        $judulLaporan = 'Laporan Perubahan Modal';
        $model->tglAwal     = date('Y-m-d 00:00:00');
        $model->tglAkhir    = date('Y-m-d H:i:s');

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Arus Kas';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['AKLaporanperubahanmodalV'])) {
            $model->attributes = $_REQUEST['AKLaporanperubahanmodalV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanperubahanmodalV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['AKLaporanperubahanmodalV']['tglAkhir']);
            $model->ruangan_id = $_GET['AKLaporanperubahanmodalV']['ruangan_id'];
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'perubahanmodal/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionLaporanRasio()
    {
        $model = new AKSaldorekeningrasioV('search');
        $model->unsetAttributes();
        // $model->tglAwal = date('Y-m-d 00:00:00');
        // $model->tglAkhir = date('Y-m-d H:i:s');

        $periode['tglAwal']     = date('Y-m-d 00:00:00');
        $periode['tglAkhir']    = date('Y-m-d H:i:s');

        if(isset($_GET['LaporanrasioV']))
        {
            $model->attributes = $_GET['LaporanrasioV'];
            $format = new CustomFormat();
            // $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanrasioV']['tglAwal']);
            // $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanrasioV']['tglAkhir']);
            // $model->ruangan_id = $_GET['LaporanrasioV']['ruangan_id'];

            // $periode['tglAwal']     = $model->tglAwal;
            // $periode['tglAkhir']    = $model->tglAkhir;
        }

        $this->render('rasio/admin',array('model'=>$model, 'periode'=>$periode));
    }

    public function actionprintLaporanRasio() 
    {
        $model = new AKSaldorekeningrasioV('search');
        $model->unsetAttributes();
        $judulLaporan = 'Laporan Rasio';
        // $model->tglAwal     = date('Y-m-d 00:00:00');
        // $model->tglAkhir    = date('Y-m-d H:i:s');

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Rasio';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporanrasioV'])) {
            $model->attributes = $_REQUEST['LaporanrasioV'];
//            $format = new CustomFormat();
            // $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanrasioV']['tglAwal']);
            // $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanrasioV']['tglAkhir']);
            // $model->ruangan_id = $_GET['LaporanrasioV']['ruangan_id'];
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rasio/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
}
?>