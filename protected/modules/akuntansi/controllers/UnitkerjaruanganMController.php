<?php

class UnitkerjaruanganMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $defaultAction = 'admin';
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AKUnitKerjaRuanganM;
		// $modKerja = new AKUnitKerjaRuanganM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
						
					// echo "<pre>";
					// 			print_r($_POST);
					// 			exit();	
		if(isset($_POST['ruangan_id']))
		{
////                    $model->unitkerja_id=$_POST['unitkerja_id'];
////                    $model->ruangan_id=$_POST['ruangan_id'];
//                                                    // echo "<pre>";
//                                                    // print_r($model->ruangan_id);
//                                                    // exit();	
//                                                    // $modKerja->attributes=$_POST['AKUnitKerjaRuanganM'];			
//                    $hitung = count($_POST['ruangan_id']);
//                    // $model->ruangan_id = $_POST['ruangan_id'][$i];
                    $unitkerja_id=$_POST['unitkerja_id'];
                        $modDetails = $this->validasiTabular($_POST['ruangan_id'],$unitkerja_id);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $jumlah = 0;
                            for($i=0;$i<COUNT($_POST['ruangan_id']);$i++)
                            {
                                $model = new UnitkerjaruanganM;
                                $model->ruangan_id=$_POST['ruangan_id'][$i];
                                $model->unitkerja_id=$unitkerja_id;
                                if($model->save()) {
                                    $jumlah++;
                                }
                            }
                            if ($jumlah == count($_POST['ruangan_id'])){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
                                $this->redirect(array('admin','id'=>$model->unitkerja_id));
                            } else {
                                $transaction->rollback();
                            }
                        }
                        catch (Exception $ex) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal</strong> Data Gagal disimpan'.MyExceptionMessage::getMessage($ex));
                        } 
                    }		

		$this->render('create',array(
			'model'=>$model,
                        'modDetails'=>$modDetails
		));
	}

	public function insertRuanganPemakai($id)
                {
                         $status = TRUE;
                         $hitung = count($_POST['ruangan_id']);
                         if($hitung < 1){return $status = TRUE;} //kondisi apabila tidak ada inputan di emultiselect
                         for($i=0;$i<$hitung;$i++){
                            $ruangan = new SARuanganM;
                            $ruangan->loginpemakai_id = $id;
                            $ruangan->ruangan_id = $_POST['ruangan_id'][$i];
                            if($ruangan->save()){
                                $status = TRUE;
                            }else{
                                $status = FALSE;
                            }
                         }
                             
                          return $status;
                    
                }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
                $modRuangan=UnitkerjaruanganM::model()->findAll('unitkerja_id='.$id.'');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $modDetails= UnitkerjaruanganM::model()->findAll('unitkerja_id='.$id);
                $list = CHtml::listData($modDetails, 'ruangan_id', 'ruangan_id');
		if(isset($_POST['ruangan_id']))
		{
			$model->attributes=$_POST['AKUnitKerjaRuanganM'];
                        $unitkerja_id = $_POST['unitkerja_id'];
			$modDetails = $this->validasiTabular($_POST['ruangan_id'],$unitkerja_id);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $jumlah = 0;
                            if (count($modDetails) > 0){
                                foreach ($modDetails as $j=>$row)
                                {
                                    if (isset($list)){
                                        if (in_array($row->ruangan_id,$list)){
                                            unset($list[$row->ruangan_id]);
                                            $jumlah++;
                                            continue;
                                        }
                                    }
                                    
                                    if($row->save()) {
                                        $jumlah++;
                                    }
                                }
                            }
//                            for($i=0;$i<COUNT($_POST['ruangan_id']);$i++)
//                            {
//                                $model = new UnitkerjaruanganM;
//                                $model->ruangan_id=$_POST['ruangan_id'][$i];
//                                $model->unitkerja_id=$unitkerja_id;
//                                if($model->save()) {
//                                    $jumlah++;
//                                }
//                            }
                             if (count($list) > 0){
                                foreach($list as $row){
                                    UnitkerjaruanganM::model()->deleteAllByAttributes(array('unitkerja_id'=>$id, 'ruangan_id'=>$row));
                                }
                            }
                            if ($jumlah == count($_POST['ruangan_id'])){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success','<strong>Berhasil</strong>Data Berhasil disimpan');
                                $this->redirect(array('admin','id'=>$model->unitkerja_id));
                            } else {
                                $transaction->rollback();
                            }
                        }
                        catch (Exception $ex) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal</strong> Data Gagal disimpan'.MyExceptionMessage::getMessage($ex));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
                        'modDetails'=>$modDetails,
                        'modRuangan'=>$modRuangan
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			// $this->loadModel($id)->delete();
			$id = $_POST['id'];
//                $this->loadModel($id)->delete();
                        UnitkerjaruanganM::model()->deleteAllByAttributes(array('unitkerja_id'=>$id));
            if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = AKUnitKerjaRuanganM::model()->updateByPk($id,array('unitkerja_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AKUnitKerjaRuanganM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AKUnitKerjaRuanganM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AKUnitKerjaRuanganM']))
			$model->attributes=$_GET['AKUnitKerjaRuanganM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=AKUnitKerjaRuanganM::model()->find('unitkerja_id = '.$id.'');
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='akunitkerja-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        protected function validasiTabular($data,$unitkerja_id) {
            foreach ($data as $i => $row)
            {
                $modDetails[$i] = new UnitkerjaruanganM;
                $modDetails[$i]->ruangan_id = $row;
                $modDetails[$i]->unitkerja_id = $unitkerja_id;
                $modDetails[$i]->validate();
            }
            return $modDetails;
        }
        
        public function actionPrint()
        {
            $model= new AKUnitKerjaRuanganM;
            $model->attributes=$_REQUEST['AKUnitKerjaRuanganM'];
            $judulLaporan='Data Diagnosa Kasus Penyakit';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
}
?>
