<?php

class PenerimaanUmumController extends SBaseController
{
        protected $succesSave = true;
        protected $pesan = "succes";
        protected $is_action = "insert";
	
        public function filters()
	{
            return array(
                'accessControl',
            );
	}

       /* modified by @author Rahman Fad | simpan hanya sekali untuk tabel penerimaanumum_t (EHJ-1903) | 20-05-2014 */
        public function actionIndex()
	{
            $modPenUmum = new AKPenerimaanUmumT;
            $modPenUmum->volume = 1;
            $modPenUmum->hargasatuan = 0;
            $modPenUmum->totalharga = 0;
            $modPenUmum->nopenerimaan = MyGenerator::noPenerimaanUmum();
            $modUraian[0] = new AKUraianpenumumT;
            $modUraian[0]->volume = 1;
            $modUraian[0]->hargasatuan = 0;
            $modUraian[0]->totalharga = 0;
            $modTandaBukti = new AKTandabuktibayarT;
            $modTandaBukti->jmlpembulatan = 0;
            $modTandaBukti->biayaadministrasi = 0;
            $modTandaBukti->biayamaterai = 0;
            $modTandaBukti->jmlpembayaran = $modPenUmum->totalharga;
            $modInstalasi = new InstalasiM;

            $modPenUmum->is_posting = "false";
		
            if(isset($_POST['AKPenerimaanUmumT'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if($modPenUmum->isuraintransaksi && isset($_POST['AKUraianpenumumT'])){
                                $modUraian = $this->saveUraian($_POST['AKUraianpenumumT'], $modPenUmum);
                            }
                    $noUrut = 1;
                    $modTandaBukti = $this->saveTandaBukti($_POST['AKTandabuktibayarT']);
                    $modPenUmum = $this->savePenerimaan($_POST['AKPenerimaanUmumT'], $modTandaBukti);
                    $modJurnalRekening = $this->saveJurnalRekening($modPenUmum, $_POST['AKPenerimaanUmumT']); 
                    $updateTandaBukti = $this->updateTandaBukti($modTandaBukti,$modJurnalRekening);
                    
                    /*
                    * Insert ke tabel jurnalposting
                    * EHJ-1942
                    **/        
                    if($_POST['AKPenerimaanUmumT']['is_posting']=='true')
                    {
                        $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                    }else{
                        $modJurnalPosting = null;
                    }

                    foreach($_POST['RekeningakuntansiV'] AS $i => $post){
                            
                            $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, $modJurnalPosting);
                            
                            $params = array(
                                        'modJurnalRekening' => $modJurnalRekening, 
                                        'jenis_simpan'=>$_REQUEST['jenis_simpan'], 
                                        'RekeningakuntansiV'=>$data_parsing['RekeningakuntansiV'],
                                    );
                            $noUrut ++;
        //                    $insertDetailJurnal = $this->saveJurnalDetail(
        //                                $data_parsing['AKPenerimaanUmumT'],
        //                                $modJurnalRekening,
        //                                $modJurnalPosting,
        //                                $data_parsing['RekeningakuntansiV']
        //                            );
        //                    $this->succesSave = $insertDetailJurnal;
                    }

                    if($this->succesSave && $modJurnalDetail){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',
                array(
                    'modPenUmum'=>$modPenUmum,
                    'modUraian'=>$modUraian,
                    'modTandaBukti'=>$modTandaBukti,
                    'modJurnalRekening'=>$modJurnalRekening,
                    'modJurnalDetail'=>$modJurnalDetail,
                    'modJurnalPosting'=>$modJUrnalPosting,
                    'modInstalasi'=>$modInstalasi
                )
            );
	}
        
        public function actionSimpanPenerimaan()
	{
            if(Yii::app()->request->isAjaxRequest)
            {
                $modPenUmum = new AKPenerimaanUmumT;
                $modTandaBukti = new AKTandabuktibayarT;
                parse_str($_REQUEST['data'],$data_parsing);
                $format = new CustomFormat();

                if(isset($data_parsing['AKPenerimaanUmumT'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                            $modTandaBukti = $this->saveTandaBukti($data_parsing['AKTandabuktibayarT']);
                            $data_parsing['AKPenerimaanUmumT']['tglpenerimaan'] = $format->formatDateTimeMediumForDB($data_parsing['AKPenerimaanUmumT']['tglpenerimaan']);

                            $modPenUmum = $this->savePenerimaan($data_parsing['AKPenerimaanUmumT'], $modTandaBukti);
                            if(isset($data_parsing['pakeAsuransi']))
                            {
                                $modUraian = $this->saveUraian($data_parsing['AKUraianpenumumT'], $modPenUmum);
                            }
                            
                            $modJurnalRekening = $this->saveJurnalRekening($modPenUmum, $data_parsing['AKPenerimaanUmumT']);
                            
                            $params = array(
                                'modJurnalRekening' => $modJurnalRekening, 
                                'jenis_simpan'=>$_REQUEST['jenis_simpan'], 
                                'RekeningakuntansiV'=>$data_parsing['RekeningakuntansiV'],
                            );
                            $insertDetailJurnal = MyFunction::insertDetailJurnal($params);
                            $this->succesSave = $insertDetailJurnal;

                            /*
                            if($_REQUEST['jenis_simpan'] == 'posting')
                            {
                                $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                            }
                            $modJurnalDetail = $this->saveJurnalDetail(
                                $data_parsing['AKPenerimaanUmumT'],
                                $modJurnalRekening,
                                $modJurnalPosting,
                                $data_parsing['RekeningakuntansiV']
                            );
                             * 
                             */
                            
                        if($this->succesSave)
                        {
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $this->pesan = array(
                                'nopenerimaan'=>MyGenerator::noPenerimaanUmum()
                            );
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    } catch (Exception $exc){
                        print_r($exc);
                        $this->pesan = $exc;
                        $this->succesSave = false;
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                
                 $result = array(
                    'action'=>$this->is_action,
                    'pesan'=>$this->pesan,
                    'status' => ($this->succesSave == true ? 'ok' : 'not'),
                );
            
            echo json_encode($result);
            Yii::app()->end();
            }  
	}
        
        protected function saveTandaBukti($postTandaBukti)
        {
            $modTandaBukti = new AKTandabuktibayarT;
            $modTandaBukti->attributes = $postTandaBukti;
            $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modTandaBukti->nourutkasir = MyGenerator::noUrutKasir($modTandaBukti->ruangan_id);
            $modTandaBukti->nobuktibayar = MyGenerator::noBuktiBayar();
            if($modTandaBukti->validate()){
                $modTandaBukti->save();
                $this->succesSave = true;
            } else {
                $this->succesSave = false;
                $this->pesan = $modTandaBukti->getErrors();
            }
            return $modTandaBukti;
        }
        
        protected function savePenerimaan($postPenerimaan,$modTandaBukti)
        {
            $modPenUmum = new AKPenerimaanUmumT;
            $modPenUmum->attributes = $postPenerimaan;
            //$modPenUmum->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modPenUmum->penjamin_id = Params::ID_PENJAMIN_UMUM;
            $modPenUmum->tandabuktibayar_id = $modTandaBukti->tandabuktibayar_id;
            if($modPenUmum->validate()){
                $modPenUmum->save();
                $this->succesSave = true;
            } else {
                $this->succesSave = false;
                $this->pesan = $modPenUmum->getErrors();
            }
            return $modPenUmum;
        }
        
        protected function saveUraian($arrPostUraian, $modPenUmum)
        {
            $valid = false;
            $modUraian = array();
            for($i=0;$i<count($arrPostUraian);$i++){
                if(strlen($arrPostUraian[$i]['uraiantransaksi']) > 0)
                {
                    $modUraian[$i] = new AKUraianpenumumT;
                    $modUraian[$i]->attributes = $arrPostUraian[$i];
                    $modUraian[$i]->penerimaanumum_id = $modPenUmum->penerimaanumum_id;
                    if($modUraian[$i]->validate())
                    {
                        $modUraian[$i]->save();
                        $valid = true;
                    }else{
                        $this->pesan = $modUraian[$i]->getErrors();
                    }
                }
            }
            $this->succesSave = $valid;
            return $modUraian;
        }
        
        protected function saveJurnalRekening($modPenUmum, $postPenUmum)
        {
            $modJurnalRekening = new AKJurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = $modPenUmum->tglpenerimaan;
            $modJurnalRekening->nobuktijurnal = MyGenerator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = MyGenerator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = $modPenUmum->tglpenerimaan;
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = $postPenUmum['jenisKodeNama'];
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = $modPenUmum->tglpenerimaan;
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modJurnalRekening->ruangan_id = $modPenUmum->ruangan_id;
                       
            if($modJurnalRekening->validate()){
                $modJurnalRekening->save();
                $this->succesSave = true;
            } else {
                $this->succesSave = false;
                $this->pesan = $modJurnalRekening->getErrors();
            }
            return $modJurnalRekening;
        }
        
        public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
            $valid = true;
            $modJurnalDetail = new JurnaldetailT();
            $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
            $modJurnalDetail->saldodebit = $post['saldodebit'];
            $modJurnalDetail->saldokredit = $post['saldokredit'];
            $modJurnalDetail->nourut = $noUrut;
            $modJurnalDetail->rekening1_id = $post['struktur_id'];
            $modJurnalDetail->rekening2_id = $post['kelompok_id'];
            $modJurnalDetail->rekening3_id = $post['jenis_id'];
            $modJurnalDetail->rekening4_id = $post['obyek_id'];
            $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
            $modJurnalDetail->catatan = "";
            if($modJurnalDetail->validate()){
                $modJurnalDetail->save();
            }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $modJurnalDetail[$i]->getErrors();
                $valid = false;
//                break;
            }

            return $valid;        
        }
        
        protected function saveJurnalPosting($arrJurnalPosting)
        {
                $modJurnalPosting = new AKJurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                    $this->succesSave = true;
                } else {
                    $this->succesSave = false;
                    $this->pesan = $modJurnalPosting->getErrors();
                }
                return $modJurnalPosting;
        }
        
        protected function updateJurnalDetail($modJurnalDetail,$modJurnalPosting)
        {
            AKJurnaldetailT::model()->updateByPk($modJurnalDetail->jurnaldetail_id, array(
                                                'jurnalposting_id'=>$modJurnalPosting->jurnalposting_id));
        }
        protected function updateTandaBukti($modTandaBukti,$modJurnalRekening)
        {
            $update = AKTandabuktibayarT::model()->updateByPk($modTandaBukti->tandabuktibayar_id, array(
                                                'jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id));
            if($update){
                $valid = true;
            }else{
                $valid = false;
            }
            $this->succesSave = $valid;
        }
 
        public function actionAmbilDataRekening()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                $data = array();
                $params = array();
                foreach($_POST['id_rekening'] as $key=>$val)
                {
                    if($key != 'status')
                    {
                        if(strlen(trim($val)) > 0)
                        {
                            $data[] = $key . ' = :' . $key;
                            $params[(string) ':'.$key] = $val;
                        }                        
                    }
                }
                
                $criteria = new CDbCriteria;
                $criteria->select = '*';
                $criteria->condition = implode($data, ' AND ');
                $criteria->params = $params;
                $model = RekeningakuntansiV::model()->findAll($criteria);
                if($model)
                {
                    echo CJSON::encode(
                        $this->renderPartial('__formKodeRekening', array('model'=>$model, 'status'=>$_POST['id_rekening']['status']), true)
                    );                
                }
                Yii::app()->end();
            }
        }

        
}