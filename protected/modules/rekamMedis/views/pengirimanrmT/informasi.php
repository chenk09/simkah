<?php
Yii::app()->clientScript->registerScript('search', "
$('#rmpengirimanrm-t-search').submit(function(){
	$.fn.yiiGridView.update('informasipengiriman-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<fieldset>
    <legend class="rim2">Informasi Pengiriman Dokumen</legend>
</fieldset>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasipengiriman-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'peminjamanrm_id',
		array(
                                    'header'=>'Instalasi Tujuan',
                                    'value'=>'$data->instalasitujuan_nama',
                                ),
                                array(
                                    'header'=>'Ruangan Tujuan',
                                    'value'=>'$data->ruangantujuan_nama',
                                ),
                                array(
                                    'header'=>'Tanggal Pengiriman',
                                    'value'=>'$data->tglpengirimanrm',
                                ),
                                array(
                                    'header'=>'No Rekam Medik',
                                    'value'=>'$data->no_rekam_medik',
                                ),
                                array(
                                    'header'=>'Nama Pasien',
                                    'type'=>'raw',
                                    'value'=>'$data->namadepan.$data->nama_pasien',
                                ),
                                array(
                                    'header'=>'Jenis Kelamin',
                                    'value'=>'$data->jeniskelamin',
                                ),
                                array(
                                    'header'=>'Tanggal Lahir',
                                    'value'=>'$data->tanggal_lahir',
                                ),
                                array
                                    (
                                            'header'=>'Kelengkapan Dokumen',
                                            'type'=>'raw',
                                            'value'=>'($data->kelengkapandokumen==1)? Yii::t("mds","Lengkap") : Yii::t("mds","Belum")',
                                    ),
                                array(
                                    'header'=>'Petugas Pengirim',
                                    'value'=>'$data->petugaspengirim',
                                ),
//                                array(
//                                    'name'=>'ruanganpengirim_id',
//                                    'filter'=>CHtml::listData($model->getRuanganItems(),'ruangan_id','ruangan_nama'),
//                                    'value'=>'$data->ruanganpengirim->ruangan_nama',
//                                ),
                                array(
                                    'header'=>'Status Print',
                                    'type'=>'raw',
                                    'value'=>'($data->printpengiriman==1)? Yii::t("mds","Sudah") : Yii::t("mds","Belum")',
                                ),
                                array(
                                    'header'=>'Tanggal Dikembalikan',
                                    'value'=>'$data->tglkembali',
                                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->renderPartial($this->pathView.'_searchinformasi',array('model'=>$model)); ?>