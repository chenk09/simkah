<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('rkpeminjamandokumenrm-v-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
     <legend class="rim2">Transaksi Pengiriman Dokumen Rekam Medis</legend>
<div class='hide'>
<?php 
$this->widget('ext.colorpicker.ColorPicker', 
    array(
        'name'=>'Dokumen[warnadokrm_id][]',
        'value'=>WarnadokrmM::model()->getKodeWarnaId($warnadokrm_id),// string hexa decimal contoh 000000 atau 0000ff
        'height'=>'30px', // tinggi
        'width'=>'83px',        
        //'swatch'=>true, // default false jika ingin swatch
        'colors'=>  WarnadokrmM::model()->getKodeWarna(), //warna dalam bentuk array contoh array('0000ff','00ff00')
        'colorOptions'=>array(
            'transparency'=> true,
           ),
        )
    );
?>
</div>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
    <?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_searchPengiriman',array(
    'model'=>$modPengiriman,
)); ?>
</div><!-- search-form -->
</fieldset>
<br/>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rkpengirimanrm-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rkpeminjamandokumenrm-v-grid',
    'dataProvider'=>$modPengiriman->searchPeminjaman(),
    //'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        //'warnadokrm_id',
        array(
            'header'=> 'Pilih',
            'type'=>'raw',
            'value'=>'
                CHtml::hiddenField(\'Dokumen[dokrekammedis_id][]\', $data->dokrekammedis_id).
                CHtml::hiddenField(\'Dokumen[pasien_id][]\', $data->pasien_id).
                CHtml::hiddenField(\'Dokumen[pendaftaran_id][]\', $data->pendaftaran_id).
                CHtml::hiddenField(\'Dokumen[ruangan_id][]\', $data->ruangan_id).
                CHtml::hiddenField(\'Dokumen[peminjamanrm_id][]\', $data->peminjamanrm_id).
                CHtml::checkBox(\'cekList[]\', \'\', array(\'onclick\'=>\'setUrutan();setLengkap();\', \'class\'=>\'cekList\'));
                ',
        ),
        'lokasirak_nama',
        //'nodokumenrm',
        'subrak_nama',
        //'warnadokrm_namawarna',
        array(
            'header'=>'Warna Dokumen RM',
            'type'=>'raw',
            'value'=>'$this->grid->getOwner()->renderPartial(\'_warnaDokumen\', array(\'warnadokrm_id\'=>$data->warnadokrm_id), true)',
        ),
        'no_rekam_medik',
        'pendaftaran.tgl_pendaftaran',
        'no_pendaftaran',
        'nama_pasien',
        'tanggal_lahir',
        'jeniskelamin',
        'alamat_pasien',
        // 'instalasi_nama',
        array(
           'name'=>'Nama Instalasi',
           'type'=>'raw',
           'value'=>'CHtml::dropDownList("instalasi_id",$data->instalasi_id, InstalasiM::items());',
           'htmlOptions'=>array(
               'style'=>'text-align:center;',
               'class'=>'rajal',
               'width'=>'50px',
           )
        ),
        array(
           'name'=>'Nama Ruangan',
           'type'=>'raw',
           'value'=>'CHtml::dropDownList("ruangan_id",$data->ruangan_id, RuanganM::items());',
           'htmlOptions'=>array(
               'style'=>'text-align:center;',
               'class'=>'rajal'
           )
        ),

        // 'ruangan_nama',
        //'printpeminjaman',
        array(
            'header'=>'Kelengkapan',
            'type'=>'raw',
//            'selectableRows'=>0,
            'value'=>'CHtml::checkBox(\'Dokumen[kelengkapan][]\', \'true\', array(\'class\'=>\'lengkap\'))',
            
        ),
        array(
            'header'=>'Print',
            'class'=>'CCheckBoxColumn',     
            'selectableRows'=>0,
            'id'=>'rows',
            'checked'=>'$data->printpeminjaman',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
                        var colors = jQuery(\'input[rel="colorPicker"]\').attr(\'colors\').split(\',\');
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        jQuery(\'input[rel="colorPicker"]\').colorPicker({colors:colors});
                }',
)); ?> 

<?php $dokumen = CHtml::activeId($model, 'dokrekammedis_id'); ?>


    
<fieldset>
   

	    <!--<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->

	<?php echo $form->errorSummary($model); ?>

            <?php //echo $form->textFieldRow($model,'peminjamanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'kembalirm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'dokrekammedis_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'nourut_keluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>5)); ?>
            <?php //echo $form->textFieldRow($model,'tglpengirimanrm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
         <legend class="rim">Pengiriman Dokumen Rekam Medis</legend>   <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpengirimanrm', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglpengirimanrm',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                        ));
                        ?>
                    </div>
</div>
            <?php echo $form->textFieldRow($model,'petugaspengirim',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php //echo $form->checkBoxRow($model,'kelengkapandokumen', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->checkBoxRow($model,'printpengiriman', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'ruanganpengirim_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php //echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
//                        Yii::app()->createUrl($this->module->id.'/'.pengirimanrmT.'/admin'), 
//                        array('class'=>'btn btn-danger',
//                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-info', 'type'=>'button','onclick'=>'print()', 'disabled'=>($model->isNewRecord ? 'disabled' : '')))."&nbsp&nbsp";  ?>
					 <?php
                    echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), array('type' => 'reset', 'class' => 'btn btn-danger',
                        'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
                    ?>
					<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
            </div>
	
	</div>

<?php $this->endWidget(); ?>
<?php
    //================== Ganti Data Kelas Pelayanan Dialog ===================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editInstalasi',
            'options'=>array(
                'title'=>'Ganti Nama Instalasi',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranKP','',array('readonly'=>true));
    echo '<div class="divForFormEditInstalasi"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<script>
    function setUrutan(){
        noUrut = 0;
        $('.cekList').each(function(){
           $(this).attr('name','cekList['+noUrut+']');
           noUrut++;
        });
    }
    
    function setLengkap(){
        noUrut = 0;
        $('.lengkap').each(function(){
           $(this).attr('name','Dokumen[kelengkapan]['+noUrut+']');
           noUrut++;
        });
    }
    $(document).ready(function(){
        $('form#rkpengirimanrm-t-form').submit(function(){
            var jumlah = 0;
            $('.cekList').each(function(){
                if ($(this).is(':checked')){
                    jumlah++;
                }
            });
            if (jumlah < 1){
                alert('Pilih Dokumen yang akan dikirim');
                return false;
            }
        });
    });

    function ubahInstalasi(idPendaftaran)
    {
        $('#temp_idPendaftaranKP').val(idPendaftaran);
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahKelasPelayanan')?>',
            'data':$(this).serialize(),
            'type':'post',
            'dataType':'json',
            'success':function(data){
                if (data.status == 'create_form') {
                    $('#editInstalasi div.divForFormEditInstalasi').html(data.div);
                    $('#editInstalasi div.divForFormEditInstalasi form').submit(ubahKelasPelayanan);
                }else{
                    $('#editInstalasi div.divForFormEditInstalasi').html(data.div);
                    $.fn.yiiGridView.update('rkpeminjamandokumenrm-v-grid', {
                            data: $(this).serialize()
                    });
                    setTimeout("$('#editInstalasi').dialog('close') ",500);
                }
            },
            'cache':false
        });
        return false; 
    }
</script>