<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'monitoring-search-form',
                'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'peminjamanrm_id',array('class'=>'span5')); ?>

                <div class="control-group ">
                    <legend class="rim">Pencarian</legend>
                    <table>
                        <tr>
                            <td>
                                <div class="control-label">
                                    <?php echo CHtml::activeCheckBox($model, 'cekTanggalAdmisi'); ?>
                                    <?php echo CHtml::label('Tanggal admisi','tglAwal'); ?>
                                </div>
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglAwal',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'maxDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                                            ?>
                                </div>
                                <?php echo CHtml::label('Sampai dengan','tglAkhir',array('class'=>'control-label')); ?>
                                <div class="controls">
                                    <?php
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglAkhir',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'maxDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); ?>
                                </div>
                                <div class="control-label">
                                    <?php echo CHtml::label('Tgl masuk kamar','tglmasukkamar'); ?>
                                </div>
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglmasukkamar',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'maxDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                                            ?>
                                </div>
                            </td>
                            <td>
                                <?php echo $form->textFieldRow($model,'nama_pasien'); ?>
                                <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')); ?>
                                <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')); ?>
                                <?php //echo $form->dropDownListRow($model,'carakeluar',Carakeluar::items(),array('empty'=>'-- Pilih --','onkeypress'=>'$(this).focusNextInputField(event)')); ?>
                                <?php //echo $form->dropDownListRow($model,'kondisipulang',KondisiPulang::items(),array('empty'=>'-- Pilih --','class'=>'','onkeypress'=>'$(this).focusNextInputField(event)')); ?>
                            </td>
                        </tr>
                    </table>
                    </div>

	<div class="form-actions">
                    <?php
                        echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
                        echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/'.Monitoring.'/Rawatinap'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>