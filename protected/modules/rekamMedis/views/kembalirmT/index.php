<legend class="rim2">Transaksi Pengembalian Dokumen Rekam Medis</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('rkpeminjamandokumenrm-v-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<div class='hide'>
<?php 
$this->widget('ext.colorpicker.ColorPicker', 
    array(
        'name'=>'Dokumen[warnadokrm_id][]',
        'value'=>WarnadokrmM::model()->getKodeWarnaId($warnadokrm_id),// string hexa decimal contoh 000000 atau 0000ff
        'height'=>'30px', // tinggi
        'width'=>'83px',        
        //'swatch'=>true, // default false jika ingin swatch
        'colors'=>  WarnadokrmM::model()->getKodeWarna(), //warna dalam bentuk array contoh array('0000ff','00ff00')
        'colorOptions'=>array(
            'transparency'=> true,
           ),
        )
    );
?>
</div>
<?php
$this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
    <br/>
<div class="search-form" style="display:none;">
<?php $this->renderPartial('_searchPengembalian',array(
    'model'=>$modPengiriman,
)); ?>
</div><!-- search-form -->
</fieldset>
<br/>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rkkembalirm-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rkpeminjamandokumenrm-v-grid',
    'dataProvider'=>$modPengiriman->searchPengiriman(),
    //'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        //'warnadokrm_id',
        array(
            'header'=> 'Pilih',
            'type'=>'raw',
            'value'=>'
                CHtml::hiddenField(\'Dokumen[dokrekammedis_id][]\', $data->dokrekammedis_id).
                CHtml::hiddenField(\'Dokumen[pasien_id][]\', $data->pasien_id).
                CHtml::hiddenField(\'Dokumen[pendaftaran_id][]\', $data->pendaftaran_id).
                CHtml::hiddenField(\'Dokumen[ruangan_id][]\', $data->ruangan_id).
                CHtml::hiddenField(\'Dokumen[peminjamanrm_id][]\', $data->peminjamanrm_id).
                CHtml::hiddenField(\'Dokumen[pengirimanrm_id][]\', $data->pengirimanrm_id).
                CHtml::checkBox(\'cekList[]\', \'\', array(\'onclick\'=>\'setUrutan();setLengkap();\', \'class\'=>\'cekList\'));
                ',
        ),
        array(
            'header'=> 'Lokasi Rak',
            'type'=>'raw',
            'value'=>'
                CHtml::dropDownList(\'Dokumen[lokasirak_id]\', $data->lokasirak_id, CHtml::listData(LokasirakM::model()->findAll(\'lokasirak_aktif = true\'), \'lokasirak_id\', \'lokasirak_nama\'), array(\'empty\'=>\'-- Pilih\', \'class\'=>\'span1 lokasiRak\'));
                ',
        ),
        array(
            'header'=> 'Sub Rak',
            'type'=>'raw',
            'value'=>'
                CHtml::dropDownList(\'Dokumen[subrak_id]\', $data->subrak_id, CHtml::listData(SubrakM::model()->findAll(\'subrak_aktif = true\'), \'subrak_id\', \'subrak_nama\'), array(\'empty\'=>\'-- Pilih\', \'class\'=>\'span1 subRak\'));
                ',
        ),
        //'lokasirak_nama',
        //'nodokumenrm',
        //'subrak_nama',
        //'warnadokrm_namawarna',
        array(
            'header'=>'Warna Dokumen RM',
            'type'=>'raw',
            'value'=>'$this->grid->getOwner()->renderPartial(\'_warnaDokumen\', array(\'warnadokrm_id\'=>$data->warnadokrm_id), true)',
        ),
        'no_rekam_medik',
        'pendaftaran.tgl_pendaftaran',
        'no_pendaftaran',
        'nama_pasien',
        'tanggal_lahir',
        'jeniskelamin',
        'alamat_pasien',
        'instalasi_nama',
        'ruangan_nama',
        //'printpeminjaman',
        array(
            'header'=>'Kelengkapan',
            'type'=>'raw',
//            'selectableRows'=>0,
            'value'=>'CHtml::checkBox(\'Dokumen[kelengkapan][]\', \'true\', array(\'class\'=>\'lengkap\'))',
            
        ),
        array(
            'header'=>'Print',
            'class'=>'CCheckBoxColumn',     
            'selectableRows'=>0,
            'id'=>'rows',
            'checked'=>'$data->printpeminjaman',
        ),
        /*
        'tglrekammedis',
        'pasien_id',
        'no_rekam_medik',
        'tgl_rekam_medik',
        'nama_pasien',
        'nama_bin',
        'jeniskelamin',
        'tanggal_lahir',
        'alamat_pasien',
        'tempat_lahir',
        'tglmasukrak',
        'statusrekammedis',
        'nomortertier',
        'nomorsekunder',
        'nomorprimer',
        'warnanorm_i',
        'warnanorm_ii',
        'tglkeluarakhir',
        'tglmasukakhir',
        'dokrekammedis_id',
        'nourut_pinjam',
        'tglpeminjamanrm',
        'untukkepentingan',
        'keteranganpeminjaman',
        'tglakandikembalikan',
        'namapeminjam',
        'printpeminjaman',
        'create_time',
        'update_time',
        'create_loginpemakai_id',
        'update_loginpemakai_id',
        'create_ruangan',
        'ruangan_id',
        'ruangan_nama',
        ////'pendaftaran_id',
        array(
                        'name'=>'pendaftaran_id',
                        'value'=>'$data->pendaftaran_id',
                        'filter'=>false,
                ),
        'no_pendaftaran',
        'peminjamanrm_id',
        'pengirimanrm_id',
        'kembalirm_id',
        */
//        array(
//                        'header'=>Yii::t('zii','View'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{view}',
//        ),
//        array(
//                        'header'=>Yii::t('zii','Update'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{update}',
//                        'buttons'=>array(
//                            'update' => array (
//                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
//                                        ),
//                         ),
//        ),
//        array(
//                        'header'=>Yii::t('zii','Delete'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{remove} {delete}',
//                        'buttons'=>array(
//                                        'remove' => array (
//                                                'label'=>"<i class='icon-remove'></i>",
//                                                'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
//                                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->pendaftaran_id"))',
//                                                //'visible'=>'($data->kabupaten_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                                                'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
//                                        ),
//                                        'delete'=> array(
//                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
//                                        ),
//                        )
//        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
                        var colors = jQuery(\'input[rel="colorPicker"]\').attr(\'colors\').split(\',\');
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        jQuery(\'input[rel="colorPicker"]\').colorPicker({colors:colors});
                }',
)); ?> 
<fieldset>
    <legend class="rim">Pengembalian Dokumen Rekam Medis</legend>
            <?php //echo $form->textFieldRow($model,'pengirimanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'peminjamanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'dokrekammedis_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'tglkembali',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'petugaspenerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textAreaRow($model,'keterangan_pengembalian',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'ruanganasal_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->checkBoxRow($model,'lengkapdokumenkembali', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php //echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
//                        Yii::app()->createUrl($this->module->id.'/'.kembalirmT.'/admin'), 
//                        array('class'=>'btn btn-danger',
//                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	                 <?php if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        {
                          
                ?>
                            <script>
                                print(<?php echo $model->pendaftaran_id ?>);
                            </script>
                <?php echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       }else{
                        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       } 
                ?>

 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
	
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<script>
    function setUrutan(){
        noUrut = 0;
        $('.cekList').each(function(){
           $(this).attr('name','cekList['+noUrut+']');
           noUrut++;
        });
    }
    
    function setLengkap(){
        noUrut = 0;
        $('.lengkap').each(function(){
           $(this).attr('name','Dokumen[kelengkapan]['+noUrut+']');
           noUrut++;
        });
    }
    
    $(document).ready(function(){
        $('form#rkkembalirm-t-form').submit(function(){
            var jumlah = 0;
            var lokasiRak = 0;
            var subRak = 0;
            $('.cekList').each(function(){
                if ($(this).is(':checked')){
                    jumlah++;
                }
                if ($(this).parents('tr').find('.lokasiRak').val() != ''){
                    lokasiRak++;
                }
                if ($(this).parents('tr').find('.subRak').val() != ''){
                    subRak++;
                }
            });
            if (jumlah < 1){
                alert('Pilih Dokumen yang akan dikirim');
                return false;
            }
            else if (lokasiRak < 1){
                alert('Isi Lokasi Rak pada dokumen yang dipilih');
                return false;
            }
            else if (subRak < 1){
                alert('Isi Sub Rak pada dokumen yang dipilih');
                return false;
            }
        });
    });
</script>