<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rminfo-pasien-baru-v-search',
        'type'=>'horizontal',
)); ?>
    <fieldset>
        <table>
            <tr>
                <legend class="rim">Pencarian</legend>
                <td>
    
            <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        Tanggal Kunjungan
                    </label>
                    <div class="controls">
                        <?php   $format = new CustomFormat;
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        ));?> </div></div>
						<div class="control-group ">
                    <label for="namaPasien" class="control-label">
                       Sampai dengan
                      </label>
                    <div class="controls">
                              <?php   $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); ?>
                    </div>
                </div>
    <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','maxlength'=>10)); ?>
    <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','maxlength'=>50)); ?>
    <?php echo $form->textFieldRow($model,'penanggungJawab',array('class'=>'span3','maxlength'=>50)); ?>
            </td>
        <td>
            <?php echo $form->textFieldRow($model,'alamat_pasien',array('class'=>'span3','maxlength'=>50)); ?>
            <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'RMInfoPasienBaruV')), 
                                                    'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                ),
                        )); ?>

                 <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
            <?php echo $form->dropDownListRow($model,'statusperiksa', StatusPeriksa::items(),array('empty'=>'-- Pilih --')); ?>    
            
        </td>


        </tr>
        
    </table>

    </fieldset>
                 <div class="form-actions">
                        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
   
                        <?php
                            echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/infoPasienBaruV/admin'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                        ?>
                 </div> 
<?php $this->endWidget(); ?>
