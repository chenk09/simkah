<?php
Yii::app()->clientScript->registerScript('search', "
$('#rmpeminjamanrm-i-search').submit(function(){
	$.fn.yiiGridView.update('informasipeminjaman-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<fieldset>
    <legend class="rim2">Informasi Peminjaman Dokumen</legend>
</fieldset>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasipeminjaman-i-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'peminjamanrm_id',
		array(
                                    'header'=>'No Urut Rekam Medik',
                                    'value'=>'$data->nourut_pinjam',
                                ),
                                array(
                                    'header'=>'No Dokumen',
                                    'value'=>'$data->nodokumenrm',
                                ),
                                array(
                                    'header'=>'Warna Dokumen',
                                    'value'=>'$data->warnadokrm_namawarna',
                                ),
                                array(
                                    'header'=>'Tanggal Rekam Medik',
                                    'value'=>'$data->tgl_rekam_medik',
                                ),
//                                array(
//                                    'header'=>'Nama Pasien',
//                                    'type'=>'raw',
//                                    'value'=>'$data->nama_pasien',
//                                ),
                                array(
                                    'header'=>'Nama Pasien',
                                    'type'=>'raw',
                                    'value'=>'$data->namadepan." ".$data->nama_pasien',
                                ),
                                array(
                                    'header'=>'Tanggal Lahir',
                                    'value'=>'$data->tanggal_lahir',
                                ),
                                array(
                                    'header'=>'alamat',
                                    'value'=>'$data->alamat_pasien',
                                ),
                                array(
                                    'header'=>'Tanggal Peminjaman',
                                    'value'=>'$data->tglpeminjamanrm',
                                ),
                                array(
                                    'header'=>'Instalasi Tujuan',
                                    'value'=>'$data->instalasi_nama',
                                ),
                                array(
                                    'header'=>'Ruangan Tujuan',
                                    'value'=>'$data->ruangan_nama',
                                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->renderPartial('_searchinformasi',array('model'=>$model)); ?>