<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php 
$idDokumen = CHtml::activeId($model, 'dokrekammedis_id');
$idPeminjaman = CHtml::activeId($model, 'peminjamanrm_id');
$idPasien = CHtml::activeId($model, 'pasien_id');
$idRuangan = CHtml::activeId($model, 'ruangan_id');
$idPendaftaran = CHtml::activeId($model, 'pendaftaran_id');
$noRekamMedik = CHtml::activeId($model, 'no_rekam_medik');
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
            'id'=>'rkpeminjamanrm-t-form',
            'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
            'focus'=>'#',
    )); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
    <legend class="rim2">Transaksi Peminjaman Dokumen Rekam Medis</legend>
    

            <!--<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->

            <?php echo $form->errorSummary($model); ?>
            <?php 
                if (!$model->isNewRecord){
                    echo $form->hiddenField($model,'peminjamanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); 
                }
            ?>
    
            <?php echo $form->hiddenField($model,'dokrekammedis_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($model,'pasien_id',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($model,'pendaftaran_id',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($model,'ruangan_id',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($model,'pengirimanrm_id',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <table>
                <tr>
                    <td width="50%">
                        <?php //echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-group ">
                            <?php echo CHtml::activeLabel($model, 'no_rekam_medik', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                    'model' => $model,
                                    'attribute' => 'no_rekam_medik',
                                    'value' => '',
                                    'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/PasienLamauntukPeminjaman'),
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'minLength' => 2,
                                        'focus' => 'js:function( event, ui ) {
                                                $(this).val(ui.item.label);
                                                return true;
                                            }',
                                        'select' => 'js:function( event, ui ) {
                                            $(this).val(ui.item.label);
                                            $("#'.CHtml::activeId($model,'jenis_kelamin').'").val(ui.item.jeniskelamin);
                                            $("#'.CHtml::activeId($model,'nama_pasien').'").val(ui.item.nama_pasien);
                                            $("#'.CHtml::activeId($model,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                            $("#'.CHtml::activeId($model,'dokrekammedis_id').'").val(ui.item.dokrekammedis_id);
                                            $("#'.CHtml::activeId($model,'pasien_id').'").val(ui.item.pasien_id);
                                            $("#'.CHtml::activeId($model,'pendaftaran_id').'").val(ui.item.pendaftaran_id);
                                            $("#'.CHtml::activeId($model,'ruangan_id').'").val(ui.item.ruangan_id);
                                                 return true;
                                                      }',
                                    ),
                                    'htmlOptions'=>array(
                                        'onkeypress'=>'return $(this).focusNextInputField(event)',
                                        'disabled'=>($model->isNewRecord)?'':'disabled', 
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogRekamMedik'),
                                    
                                ));
                                ?>
                            </div>
                        </div>
                                                
                        <?php echo $form->textFieldRow($model, 'nama_pasien',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                        <?php echo $form->textFieldRow($model, 'jenis_kelamin',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                        <?php echo $form->textFieldRow($model, 'tanggal_lahir',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>

                        <?php //echo $form->textFieldRow($model,'tglpeminjamanrm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'tglpeminjamanrm', array('class' => 'control-label')) ?>
                            <div class="controls">
                                <?php
                                $this->widget('MyDateTimePicker', array(
                                    'model' => $model,
                                    'attribute' => 'tglpeminjamanrm',
                                    'mode' => 'datetime',
                                    'options' => array(
                                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                                ));
                                ?>
                            </div>
                        </div>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'tglakandikembalikan', array('class' => 'control-label')) ?>
                            <div class="controls">
                                <?php
                                $this->widget('MyDateTimePicker', array(
                                    'model' => $model,
                                    'attribute' => 'tglakandikembalikan',
                                    'mode' => 'datetime',
                                    'options' => array(
                                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                                ));
                                ?>
                            </div>
                        </div>

                        <?php // echo $form->textFieldRow($model,'namapeminjam',array('disabled'=>($model->isNewRecord)?'':'disabled', 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>           
                        <div class="control-group ">
                            <?php echo CHtml::activeLabel($model, 'namapeminjam', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                    'model' => $model,
                                    'attribute' => 'namapeminjam',
                                    'value' => '',
                                    'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/LoginPemakai'),
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'minLength' => 2,
                                        'focus' => 'js:function( event, ui ) {
                                                $(this).val(ui.item.nama_pemakai);
                                                return false;
                                            }',
                                        'select' => 'js:function( event, ui ) {
                                                $(this).val(ui.item.nama_pemakai);
                                                 return false;
                                                      }',
                                    ),
                                    'htmlOptions'=>array(
                                        'onkeypress'=>'return $(this).focusNextInputField(event)',
                                        'disabled'=>($model->isNewRecord)?'':'disabled', 
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogLoginpemakai'),
                                    
                                ));
                                ?>
                            </div>
                        </div>
                        <?php //echo $form->textFieldRow($model,'tglakandikembalikan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->checkBoxRow($model,'printpeminjaman', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                    <td width="50%">
                        <?php echo $form->textFieldRow($model,'untukkepentingan',array('disabled'=>($model->isNewRecord)?'':'disabled', 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        <?php echo $form->textAreaRow($model,'keteranganpeminjaman',array('disabled'=>($model->isNewRecord)?'':'disabled', 'rows'=>4, 'cols'=>7, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        
                        <?php //echo $form->textFieldRow($model,'nourut_pinjam',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>5)); ?>
                        <?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                </tr>
            </table>

                
                
            </fieldset>
            <div class="form-actions">
                    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Pinjam',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                         Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary','onclick'=>'setKonfirmasi(this);','onKeypress'=>'return formSubmit(this,event)', 'disabled'=>($model->isNewRecord ? '' : 'disabled'))); ?>

                    <?php //echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
//                            Yii::app()->createUrl($this->module->id.'/'.peminjamanrmT.'/admin'), 
//                            array('class'=>'btn btn-danger',
//                                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-info', 'type'=>'button','onclick'=>'print()', 'disabled'=>($model->isNewRecord ? 'disabled' : '')))."&nbsp&nbsp";  ?>
					 <?php
                    echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), array('type' => 'reset', 'class' => 'btn btn-danger',
                        'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
                    ?>
					<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
            </div>

    <?php $this->endWidget(); ?>

<?php 
        
        //$this->widget('TipsMasterData',array('type'=>'admin'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $cetak = Yii::app()->createUrl('rekamMedis/peminjamanrmT');

$js = <<< JSCRIPT
function print()
   {    
        id = $('#${idPeminjaman}').val();        
        idPasien = $('#${idPasien}').val();
        if (idPasien == ''){
            alert('Isi Data Pasien yang akan d print');
            return false;
        }
        
        
               window.open('${cetak}/printPeminjaman/id/'+id+'','printwin','left=100,top=100,width=355,height=450,scrollbars=0');
   }
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<!-- ======================== Begin Widget Dialog Rekam Medik ============================= -->
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogRekamMedik',
    'options' => array(
        'title' => 'Detail Data',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<?php 
$modDokumenPasienLama = new RKDokumenpasienrmlamaV(); 
$modDokumenPasienLama->unsetAttributes();
if (isset($_GET['RKDokumenpasienrmlamaV'])){
    $modDokumenPasienLama->attributes = $_GET['RKDokumenpasienrmlamaV'];
}
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rkdokumenpasienrmlama-v-grid',
    'dataProvider'=>$modDokumenPasienLama->searchPeminjaman(),
    'filter'=>$modDokumenPasienLama,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=> 'Pilih',
            'type'=>'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectDokumen",
                            "onClick" => "
                                        $(\'#'.CHtml::activeId($model,'nama_pasien').'\').val(\'$data->nama_pasien\');
                                        $(\'#'.CHtml::activeId($model,'jenis_kelamin').'\').val(\'$data->jeniskelamin\');
                                        $(\'#'.CHtml::activeId($model,'tanggal_lahir').'\').val(\'$data->tanggal_lahir\');
                                        $(\'#'.CHtml::activeId($model,'ruangan_id').'\').val(\'$data->ruangan_id\');
                                        $(\'#'.CHtml::activeId($model,'pengirimanrm_id').'\').val(\'$data->pengirimanrm_id\');
                                        submitRekamMedis(\'$data->no_rekam_medik\', $data->dokrekammedis_id, $data->pasien_id, $data->pendaftaran_id, $data->ruangan_id);
                                        $(\'#dialogRekamMedik\').dialog(\'close\');
                                        return false;"))',
        ),
//        'lokasirak_nama',
//        'subrak_nama',
        array(
            'name'=>'lokasirak_id',
            'filter'=>  CHtml::listData(LokasirakM::model()->findAll('lokasirak_aktif = true'), 'lokasirak_id', 'lokasirak_nama'),
            'value'=>'$data->lokasirak_nama',
            ),
        array(
            'name'=>'subrak_id',
            'filter'=>  CHtml::listData(SubrakM::model()->findAll('subrak_aktif = true'), 'subrak_id', 'subrak_nama'),
            'value'=>'$data->subrak_nama',
            ),
        array(
            'header'=>'Warna Dokumen RM',
            'type'=>'raw',
            'value'=>'$this->grid->getOwner()->renderPartial(\'_warnaDokumen\', array(\'warnadokrm_id\'=>$data->warnadokrm_id), true)',
        ),
        'no_rekam_medik',
        'pendaftaran.tgl_pendaftaran',
        'no_pendaftaran',
        'nama_pasien',
        array(
            'name'=>'tanggal_lahir',
            'filter'=>false,
            'value'=>'$data->tanggal_lahir',
            ),
        array(
            'name'=>'jeniskelamin',
            'filter'=> JenisKelamin::items(),
            'value'=>'$data->jeniskelamin',
        ),
        array(
            'name'=>'alamat_pasien',
            'filter'=>false,
            'value'=>'$data->jeniskelamin',
        ),
        array(
            'name'=>'instalasi_id',
            'filter'=>false,
            'value'=>'$data->instalasi_nama',
        ),
        array(
            'name'=>'instalasi_id',
            'filter'=>  CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'),
            'value'=>'$data->instalasi_nama',
        ),
        array(
            'name'=>'ruangan_id',
            'filter'=>  CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
            'value'=>'$data->ruangan_nama',
        ),
        array(
            'header'=>'Kelengkapan',
            'class'=>'CCheckBoxColumn',     
            'selectableRows'=>0,
            'id'=>'rows',
            'checked'=>'$data->pengiriman->kelengkapandokumen',
        ),
        array(
            'header'=>'Print',
            'class'=>'CCheckBoxColumn',     
            'selectableRows'=>0,
            'id'=>'rows',
            'checked'=>'$data->printpeminjaman',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
                        var colors = jQuery(\'input[rel="colorPicker"]\').attr(\'colors\').split(\',\');
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        jQuery(\'input[rel="colorPicker"]\').colorPicker({colors:colors});
                }',
)); ?>

<?php $this->endWidget(); ?>
<!-- =============================== endWidget Dialog Rekam Medik ============================ -->

<!-- ======================== Begin Widget Dialog Login Pemakai ============================= -->
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogLoginpemakai',
    'options' => array(
        'title' => 'Data User',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<?php 
$modLoginpemakai = new LoginpemakaiK(); 
$modLoginpemakai->unsetAttributes();
if (isset($_GET['LoginpemakaiK'])){
    $modDokumenPasienLama->attributes = $_GET['LoginpemakaiK'];
}
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'loginpemakai-grid',
    'dataProvider'=>$modLoginpemakai->search(),
    'filter'=>$modLoginpemakai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectLoginpemakai",
                                                    "onClick" => "$(\"#RKPeminjamanrmT_namapeminjam\").val($data->nama_pemakai);
                                                                          \$(\"#dialogLoginpemakai\").dialog(\"close\");"
                                             )
                             )',
                        ),
                        array(
                            'name'=>'pegawai_id',
                            'filter'=>  CHtml::listData(PegawaiM::model()->findAll(), 'pegawai_id', 'nama_pegawai'),
                            'value'=>'(!empty($data->pegawai->nama_pegawai)) ? $data->pegawai->nama_pegawai : ""',
                        ),
                        'nama_pemakai',
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->endWidget(); ?>
<!-- =============================== endWidget Dialog Login Pemakai ============================ -->

<?php 

$js = <<< JS
    function submitRekamMedis(no_rekam_medik, dokrekammedis_id, pasien_id, pendaftaran_id, ruangan_id){
        $('#${idDokumen}').val(dokrekammedis_id);
        $('#${idPasien}').val(pasien_id);
        $('#${idPendaftaran}').val(pendaftaran_id);
        $('#${noRekamMedik}').val(no_rekam_medik);
    }
JS;
        
$jsOnReady = <<< JS
   $('form').submit(function(){
       if ($('#${idDokumen}').val() == ''){
           alert('Pilih Dokumen Rekam Medis yang akan d Pinjam');
           return false;
       }
   });
JS;
        
Yii::app()->clientScript->registerScript('SubmitDokumen', $js, CClientScript::POS_HEAD);
Yii::app()->clientScript->registerScript('ONReady', $jsOnReady, CClientScript::POS_READY);

?>
<script>
    function setKonfirmasi(obj){
        if ($("#RKPeminjamanrmT_dokrekammedis_id").val() == ''){
            alert('Pilih Dokumen Rekam Medis yang akan d Pinjam');
            return false;
        }else{
            $(obj).attr('disabled',true);
            $(obj).removeAttr('onclick');
            $('#rkpeminjamanrm-t-form').find('.currency').each(function(){
                $(this).val(unformatNumber($(this).val()));
            });
            $('#rkpeminjamanrm-t-form').submit();
        }
    }
</script>