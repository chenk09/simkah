<?php
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('rkdokumenpasienrmlama-v-grid', {
		data: $(this).serialize()
	});
        
	return false;
        
});
");
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<legend class="rim2">Data Rekam Medis Peminjaman</legend><br/>
<fieldset>
    <?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_searchDokumenRM',array(
    'model'=>$modDokumenPasienLama,
)); ?>
</div><!-- search-form -->
</fieldset>

<br/>
<?php $dokumen = CHtml::activeId($model, 'dokrekammedis_id'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
            'id'=>'rkpeminjamanrm-t-form',
            'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
            'focus'=>'#',
    )); ?>
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rkdokumenpasienrmlama-v-grid',
    'dataProvider'=>$modDokumenPasienLama->searchPeminjaman(),
    //'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        //'warnadokrm_id',
        
        //'warnadokrm_kodewarna',
        //'lokasirak_id',
        array(
            'header'=> 'Pilih',
            'type'=>'raw',
            'value'=>'
                CHtml::hiddenField(\'Dokumen[dokrekammedis_id][]\', $data->dokrekammedis_id).
                CHtml::hiddenField(\'Dokumen[pasien_id][]\', $data->pasien_id).
                CHtml::hiddenField(\'Dokumen[pendaftaran_id][]\', $data->pendaftaran_id, array(\'class\'=>\'idPendaftaran\')).
                CHtml::hiddenField(\'Dokumen[ruangan_id][]\', $data->ruangan_id).
                CHtml::checkBox(\'cekList[]\', \'\', array(\'onclick\'=>\'setUrutan()\', \'class\'=>\'cekList\'));
                ',
        ),
        'lokasirak_nama',
        //'nodokumenrm',
        array(
            'header'=>'Nama Sub Rak',
            'value'=>'$data->subrak_nama',
        ),
//        'subrak_nama',
        //'warnadokrm_namawarna',
        array(
            'header'=>'Warna Dokumen RM',
            'type'=>'raw',
            'value'=>'$this->grid->getOwner()->renderPartial(\'_warnaDokumen\', array(\'warnadokrm_id\'=>$data->warnadokrm_id), true)',
        ),
        'no_rekam_medik',
        'pendaftaran.tgl_pendaftaran',
        'no_pendaftaran',
        'nama_pasien',
        'tanggal_lahir',
        'jeniskelamin',
        'alamat_pasien',
        array(
            'header'=>'Nama Instalasi',
            'value'=>'$data->instalasi_nama',
        ),
//        'instalasi_nama',
        array(
            'header'=>'Nama Ruangan',
            'value'=>'$data->ruangan_nama',
        ),
        array(
            'header'=>'Print',
            'class'=>'CCheckBoxColumn',     
            'selectableRows'=>0,
            'id'=>'rows',
            'checked'=>'$data->printpeminjaman',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
                        var colors = jQuery(\'input[rel="colorPicker"]\').attr(\'colors\').split(\',\');
                        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                        jQuery(\'input[rel="colorPicker"]\').colorPicker({colors:colors});
                }',
)); ?>
<fieldset>
    <legend class="rim">Peminjaman Dokumen Rekam Medis</legend>
    

            <!--<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->

            <?php echo $form->errorSummary($model); ?>

                <?php //echo $form->textFieldRow($model,'pengirimanrm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'dokrekammedis_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'kembalirm_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php // echo $form->textFieldRow($model,'nourut_pinjam',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>5)); ?>
                <?php //echo $form->textFieldRow($model,'tglpeminjamanrm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpeminjamanrm', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglpeminjamanrm',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                        ));
                        ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'untukkepentingan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo CHtml::activeHiddenField($model,'printArray'); ?>
                <?php echo $form->textAreaRow($model,'keteranganpeminjaman',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'tglakandikembalikan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model,'namapeminjam',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php //echo $form->checkBoxRow($model,'printpeminjaman', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="form-actions">
                    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Pinjam',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                         Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>

                <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('peminjamanrmT/index'), array('class'=>'btn btn-danger')); ?>
                     <?php if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE))) 
                        {  
                ?>
                            <script>
                                print(<?php echo $model->pendaftaran_id ?>);
                            </script>
                 <?php echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       }else{
                        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       } 
                ?>
           <?php $this->endWidget(); ?>
            
							<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'create','content'=>$content));?>   
			
			<?php 
        
   //   $this->widget('TipsMasterData',array('type'=>'admin'));
    //   $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    //    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    // $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#rkdokumenpasienrmlama-v-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
		    </div>

   
</fieldset>

<script>
    function setUrutan(){
        noUrut = 0;
        $('.cekList').each(function(){
            
           $(this).attr('name','cekList['+noUrut+']');
           
           noUrut++;
        });
    }
    
    $(document).ready(function(){
        $('form#rkpeminjamanrm-t-form').submit(function(){
            var jumlah = 0;
            $('.cekList').each(function(){
                if ($(this).is(':checked')){
                    jumlah++;
                }
            });
            if (jumlah < 1){
                alert('Pilih Dokumen yang akan dikirim');
                return false;
            }
        });
    });
</script>

<?php 
$printArray = CHtml::activeId($modDokumenPasienLama,'printArray');
$printArray2 = CHtml::activeId($model,'printArray');
$js = <<< JS
    function cekPrint(){
            var jumlah = 0;
            var isiPrint = '';
            $('.cekList').each(function(){
                if ($(this).is(':checked')){
                    
                    var idPendaftaran = $(this).parents('tr').find('.idPendaftaran').val();
                    if (isiPrint == ''){
                        isiPrint = idPendaftaran;
                    }else{
                        isiPrint += ','+idPendaftaran;
                    }
                    jumlah++;
                }
            });
            $('#${printArray}').val(isiPrint);
            $('#${printArray2}').val(isiPrint);
            if (jumlah < 1){
                alert('Pilih Dokumen yang akan dikirim');
                return false;
            }else{
                print('PRINT');
            }
    }
JS;
 Yii::app()->clientScript->registerScript('cekPrint', $js, CClientScript::POS_HEAD);
?>