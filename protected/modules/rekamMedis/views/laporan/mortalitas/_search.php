<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        #penjamin, #ruangan{
            width:250px;
        }
        #penjamin label.checkbox, #ruangan label.checkbox{
            width: 150px;
            display:inline-block;
        }

    </style><legend class="rim">Berdasarkan Tanggal Mortalitas</legend>
    <table width="200" style="margin-top:10px;">
        <tr>
            <td>
				<div class='form-control'>
					<?php echo CHtml::hiddenField('type', ''); ?>
					<?php //echo CHtml::hiddenField('src', ''); ?>
					<div class='control-label'>Tanggal Mortalitas</div>
					<div class="controls">  
						<?php
						$this->widget('MyDateTimePicker', array(
							'model' => $model,
							'attribute' => 'tglAwal',
							'mode' => 'datetime',
	//                                          'maxDate'=>'d',
							'options' => array(
								'dateFormat' => Params::DATE_FORMAT_MEDIUM,
							),
							'htmlOptions' => array('readonly' => true,
								'onkeypress' => "return $(this).focusNextInputField(event)"),
						));
						?>
					</div>
				</div>
				<?php/*echo $form->dropDownListRow($model,'instalasi_id',CHtml::listData(InstalasiM::model()->findAll("instalasi_id IN (3,4)"), 'instalasi_id', 'instalasi_nama'),
					array(
						'empty' => '-- Pilih --',
						'onkeypress' => "return $(this).focusNextInputField(event)",
					)
				);*/?>
            </td>
            <td style="padding:0px 130px 0 0px;"> <?php echo CHtml::label('Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
//                                         'maxdate'=>'d',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div> </td>
        </tr>
    </table>
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-refresh icon-white"></i>')), array('class' => 'btn btn-danger', 'onclick' => 'konfirmasi()', 'onKeypress' => 'return formSubmit(this,event)'));
        ?> 
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

