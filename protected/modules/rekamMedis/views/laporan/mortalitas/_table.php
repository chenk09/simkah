<?php 
if (isset($caraPrint)){
  $data = $model->searchLaporan();
  $template = "\n{items}";
} else{
  $data = $model->searchLaporan();
  $template = "{pager}{summary}\n{items}";
}
?>
<?php 
/**
 * css untuk membuat text head berada d tengah
 */
echo CHtml::css('.table thead tr th{
    vertical-align:middle;
}'); ?>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Golongan Umur</center>',
                'start'=>2, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Keadaan Meninggal</center>',
                'start'=>11, //indeks kolom 3
                'end'=>12, //indeks kolom 4
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
//        'tglmorbiditas',
            'diagnosa_nama',
//        'kasusdiagnosa',
            array(
                    'header'=>'0 - 6 hr',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BATITA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
           array(
                    'header'=>'7 - 28 hr',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BARU LAHIR",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'28hr - 1thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BAYI",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'1 - 4thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BALITA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'5 - 14thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("ANAK ANAK",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'15 - 24thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("REMAJA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'25 - 44thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("DEWASA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'45 - 64thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("ORANG TUA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'65thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("MANULA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),


             array(
                    'header'=>'< 48 jam',
                    'value'=> 'MyFunction::formatNumber($data->getSumKM("< 48 JAM",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
             array(
                    'header'=>'> 48 jam',
                    'value'=> 'MyFunction::formatNumber($data->getSumKM("> 48 JAM",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            
//            'kondisipulang1',
//            'kondisipulang2',
            array(
                'header'=>'Jumlah Kunjungan',
                'value'=> 'MyFunction::formatNumber($data->getSumTotalT($data->diagnosa_id))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
//            'jumlahkunjungan',
        /*
        'diagnosa_id',
        'diagnosa_kode',
        
        'diagnosa_namalainnya',
        'diagnosa_nourut',
        'golonganumur_id',
        'kondisipulang',
        'carakeluar',
           */
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>