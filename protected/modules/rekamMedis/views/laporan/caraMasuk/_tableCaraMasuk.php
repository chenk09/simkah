<?php
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchTable();
}
?>

<?php $this->widget($table,array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
//            'instalasi_nama',
            array(
                'header'=>'No',
                'type'=>'raw',
                'value'=>'$row+1',
            ),
            array(
                'header'=>'Status Masuk',
                'type'=>'raw',
                'value'=>'$data->statusmasuk',
            ),
            'no_rekam_medik',
            'nama_pasien',
            'no_pendaftaran',
            'umur',
            array(
                'header'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->jeniskelamin',
            ),
            'alamat_pasien',
            'kelaspelayanan_nama',
            'asalrujukan_nama',
            'jeniskasuspenyakit_nama',
//            'catatan_dokter_konsul',
//            'statusperiksa',
//            array(
//                   'header'=>'CaraBayar/Penjamin',
//                   'type'=>'raw',
//                   'value'=>'$data->CaraBayarPenjamin',
//                   'htmlOptions'=>array('style'=>'text-align: center')
//            ),  
//            'alamat_pasien',   
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>