<?php 
if (isset($caraPrint)){
  $data = $model->searchPrint();
  $template = "\n{items}";
} else{
  $data = $model->search();
  $template = "{pager}{summary}\n{items}";
}
?>
<?php 
/**
 * css untuk membuat text head berada d tengah
 */
echo CHtml::css('.table thead tr th{
    vertical-align:middle;
}'); ?>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'tableLaporan',
	'dataProvider'=>$data,
        'template'=>$template,
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Golongan Umur</center>',
                'start'=>2, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Jenis Kelamin</center>',
                'start'=>11, //indeks kolom 3
                'end'=>12, //indeks kolom 4
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            'diagnosa_nama',
            array(
                    'header'=>'0 - 6 hr',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BATITA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'7 - 28 hr',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BARU LAHIR",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'28hr - 1thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BAYI",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'1 - 4thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("BALITA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'5 - 14thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("ANAK ANAK",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'15 - 24thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("REMAJA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'25 - 44thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("DEWASA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'45 - 64thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("ORANG TUA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),
            array(
                    'header'=>'65thn',
                    'value'=> 'MyFunction::formatNumber($data->getSumGolUmur("MANULA",$data->diagnosa_id))',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
                ),

            array(
                'header' => 'Laki - Laki',
                'value'=> 'MyFunction::formatNumber($data->getSumJK("LAKI",$data->diagnosa_id))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
                'header' => 'Perempuan',
                'value'=> 'MyFunction::formatNumber($data->getSumJK("PEREMPUAN",$data->diagnosa_id))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
                'header'=>'Jumlah Kunjungan',
                'value'=> 'MyFunction::formatNumber($data->getSumTotalT($data->diagnosa_id))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            /*

            'diagnosa_id',
            'diagnosa_kode',
            
            'diagnosa_namalainnya',
            'diagnosa_nourut',
            'golonganumur_id',
            , */
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>