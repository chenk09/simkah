<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        #ruangan label{
            width: 120px;
            display:inline-block;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
    </style>
<table width="489">
            <tr>
                <td width="93">
                    <div class="control-group ">
                                    <?php
                                        echo CHtml::hiddenField('page',0, array('class'=>'number_page'));
                                        echo CHtml::hiddenField('filter_tab','rj', array('class'=>'number_page'));
                                    ?>
                     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php //echo CHtml::hiddenField('src', ''); ?>

                     <?php echo CHtml::label(' Tanggal Pasien Pulang ', ' s/d', array('class' => 'control-label')) ?>
                    
                    <div class = 'controls'>
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $modRJ,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
    //                          'maxDate'=>'d',
                                'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                  
              </td>
                <td width="330" style="padding:40px 130px 0 0;">
                   <?php echo CHtml::label(' sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $modRJ,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
//                          'maxdate'=>'d',
                            'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> 
              </td>
    
         </td>
  </table>

          <?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                        'id'=>'pasienpulang',
                        'slide'=>true,
                        'content'=>array(
                            'content2'=>array(
                                'header'=>'Berdasarkan Cara Bayar',
                                'isi'=>'<table><tr>
                                            <td>'.CHtml::hiddenField('filter', 'carabayar',array('disabled'=>'disabled')).'<label>Cara Bayar</label></td>
                                            <td>'.$form->dropDownList($modRJ, 'carabayar_id', CHtml::listData($modRJ->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                'ajax' => array('type' => 'POST',
                                                    'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien', array('encode' => false, 'namaModel' => ''.$modRJ->getNamaModel().'')),
                                                    'update' => '#'.CHtml::activeId($modRJ, 'penjamin_id').'',  //selector to update
                                                ),
                                            )).'</td>
                                                </tr><tr>
                                            <td><label>Penjamin</label></td><td>'.
                                            $form->dropDownList($modRJ, 'penjamin_id', CHtml::listData($modRJ->getPenjaminItems(), 'penjamin_id', 'penjamin_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",)).'</td></tr></table>', 'active'=>false,       
                                'active'=>true,
                                ),
                        ),
                )); ?>
        <div id="search_rj">
        </div>

         <div id="search_ri">
        </div>

         <div id="search_rd">
        </div>
 
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>
<script>
    function checkAll(){
        if($('#checkAllRuangan').is(':checked')){
           $('#searchLaporan input[name*="ruangan_id"]').each(function(){
                $(this).attr('checked',true);
           });
        }else{
             $('#searchLaporan input[name*="ruangan_id"]').each(function(){
                $(this).removeAttr('checked');
           });
        }
    }
</script>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>

<?php //Yii::app()->clientScript->registerScript('onclickButton','
//  var tampilGrafik = "<div class=\"tampilGrafik\" style=\"display:inline-block\"> <i class=\"icon-arrow-right icon-white\"></i> Grafik</div>";
//  $(".accordion-heading a.accordion-toggle").click(function(){
//            $(this).parents(".accordion").find("div.tampilGrafik").remove();
//            $(this).parents(".accordion-group").has(".accordion-body.in").length ? "" : $(this).append(tampilGrafik);
//            
//            
//  });
//',  CClientScript::POS_READY);
?>
