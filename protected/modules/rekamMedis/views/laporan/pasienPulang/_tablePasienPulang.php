<?php
$rim = 'width:600px;overflow-x:none;';
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)) {
    $data = $modRJ->searchPrint();
    $template = "{items}";
    $rim = '';
    $sort = false;
    if ($caraPrint == "EXCEL")
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
} else {
    $data = $modRJ->searchTable();
    $template = "{pager}{summary}\n{items}";
}
?>


<div id="div_rj">
    <legend class="rim">Tabel Pasien Pulang - Rawat Jalan</legend>                
    <?php
    $this->widget($table, array(
        'id' => 'tablePasienPulang',
        'dataProvider' => $data,
        'template' => "{pager}{summary}\n{items}",
        'itemsCssClass' => 'table table-striped table-bordered table-condensed',
        'enableSorting' => $sort,
        // 'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        'mergeColumns' => array('no_rekam_medik', 'no_pendaftaran', 'instalasi_nama', 'subspesialistik', 'jeniskasuspenyakit_nama', 'carabayar_nama', 'penjamin_nama', 'nama_pasien', 'kelaspelayanan_nama', 'nama_pj', 'no_telepon_pasien', 'kondisipulang', 'kondisipulang', 'dokterpenanggungjawab_nama', 'tglpasienpulang'),
        'columns' => array(
            'no_rekam_medik',
            'nama_pasien',
            'tanggal_lahir',
            array(
                'header' => 'Jenis Kelamin',
                'name' => 'jk_pasien',
            ),
            'alamat_pasien',
            'no_telepon_pasien',
            array(
                'header' => 'Tgl Masuk',
                'name' => 'tgl_pendaftaran',
            ),
            array(
                'header' => 'Cara Masuk',
                'name' => 'caramasuk_nama',
            ),
            array(
                'header' => 'Cara Bayar',
                'type' => 'raw',
                'name' => 'carabayar_nama',
                'value' => '$data->carabayar_nama'
            ),
            array(
                'header' => 'Nama PJP',
                'type' => 'raw',
                'name' => 'nama_pj',
                'value' => '$data->nama_pj'
            ),
            array(
                'header' => 'Kamar Perawatan',
                'type' => 'raw',
                'name' => 'nama_pj',
                'value' => '$data->kamarperawatan'
            ),
            array(
                'header' => 'Dokter Penanggung Jawab',
                'type' => 'raw',
                'name' => 'dokterpenanggungjawab_nama',
                'value' => '$data->dokterpenanggungjawab_gelardepan." ".$data->dokterpenanggungjawab_nama." ".$data->dokterpenanggungjawab_gelarbelakang'
            ),
            array(
                'header' => 'Tanggal Pulang',
                'name' => 'tglpasienpulang',
            ),
            array(
                'header' => 'Keterangan Keluar',
                'name' => 'kondisipulang',
            ),
        ),
        'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
    ));
    ?> 
</div>
<div id="div_ri">
    <legend class="rim">Tabel Pasien Pulang - Rawat Inap</legend>
    <?php
    $this->widget($table, array(
        'id' => 'tableRI',
        'dataProvider' => $data,
        'template' => "{pager}{summary}\n{items}",
        'itemsCssClass' => 'table table-striped table-bordered table-condensed',
        'enableSorting' => $sort,
        // 'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        'mergeColumns' => array('no_rekam_medik', 'no_pendaftaran', 'instalasi_nama', 'subspesialistik', 'jeniskasuspenyakit_nama', 'carabayar_nama', 'penjamin_nama', 'nama_pasien', 'kondisipulang', 'kondisipulang', 'dokter', 'tglpasienpulang'),
        'columns' => array(
            'no_rekam_medik',
            'nama_pasien',
            'tanggal_lahir',
            array(
                'header' => 'Jenis Kelamin',
                'name' => 'jk_pasien',
            ),
            'alamat_pasien',
            'no_telepon_pasien',
            array(
                'header' => 'Tgl Masuk',
                'name' => 'tgl_pendaftaran',
            ),
            array(
                'header' => 'Cara Masuk',
                'name' => 'caramasuk_nama',
            ),
            array(
                'header' => 'Cara Bayar',
                'type' => 'raw',
                'name' => 'carabayar_nama',
                'value' => '$data->carabayar_nama'
            ),
            array(
                'header' => 'Nama PJP',
                'type' => 'raw',
                'name' => 'nama_pj',
                'value' => '$data->nama_pj'
            ),
            array(
                'header' => 'Kamar Perawatan',
                'type' => 'raw',
                'name' => 'nama_pj',
                'value' => '$data->kamarperawatan'
            ),
            array(
                'header' => 'Dokter yang Merawat',
                'type' => 'raw',
                'name' => 'dokterpenanggungjawab_nama',
                'value' => '$data->dokterpenanggungjawab_gelardepan." ".$data->dokterpenanggungjawab_nama." ".$data->dokterpenanggungjawab_gelarbelakang'
            ),
            array(
                'header' => 'Tanggal Pulang',
                'name' => 'tglpasienpulang',
            ),
            array(
                'header' => 'Keterangan Keluar',
                'name' => 'kondisipulang',
            ),
        ),
        'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
    ));
    ?> 
</div>
<div id="div_rd">
    <legend class="rim">Tabel Pasien Pulang - Rawat Darurat</legend>
    <?php
    $this->widget($table, array(
        'id' => 'tableRD',
        'dataProvider' => $data,
        'template' => "{pager}{summary}\n{items}",
        'itemsCssClass' => 'table table-striped table-bordered table-condensed',
        'enableSorting' => $sort,
        // 'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//        'mergeColumns' => array('no_rekam_medik', 'instalasi_nama', 'dokter', 'tglpasienpulang', 'subspesialistik', 'carabayar_nama', 'kasus', 'kondisipulang', 'kondisipulang', 'penjamin_nama', 'nama_pj', 'no_pendaftaran', 'nama_pasien', 'nama_pj', 'no_telepon_pasien', 'tglpasienpulang'),
        'columns' => array(
            'no_rekam_medik',
            'nama_pasien',
            'tanggal_lahir',
            array(
                'header' => 'Jenis Kelamin',
                'name' => 'jk_pasien',
            ),
            'alamat_pasien',
            'no_telepon_pasien',
            array(
                'header' => 'Tgl Masuk',
                'name' => 'tgl_pendaftaran',
            ),
            array(
                'header' => 'Cara Masuk',
                'name' => 'caramasuk_nama',
            ),
            array(
                'header' => 'Cara Bayar',
                'type' => 'raw',
                'name' => 'carabayar_nama',
                'value' => '$data->carabayar_nama'
            ),
            array(
                'header' => 'Nama PJP',
                'type' => 'raw',
                'name' => 'nama_pj',
                'value' => '$data->nama_pj'
            ),
            array(
                'header' => 'Kamar Perawatan',
                'type' => 'raw',
                'name' => 'kamarperawatan',
            ),
            array(
                'header' => 'Dokter yang Merawat',
                'type' => 'raw',
                'name' => 'dokterpenanggungjawab_nama',
                'value' => '$data->dokterpenanggungjawab_gelardepan." ".$data->dokterpenanggungjawab_nama." ".$data->dokterpenanggungjawab_gelarbelakang'
            ),
            array(
                'header' => 'Tanggal Pulang',
                'name' => 'tglpasienpulang',
            ),
            array(
                'header' => 'Keterangan Keluar',
                'name' => 'kondisipulang',
            ),
        ),
        'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
    ));
    ?> 
</div>
