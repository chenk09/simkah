<?php
//$this->breadcrumbs=array(
//    'Ppinfo Kunjungan Rjvs'=>array('index'),
//    'Manage',
//);

$url = Yii::app()->createUrl('pendaftaranPenjadwalan/laporan/frameGrafikKunjunganRS&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
        $('#tablePasienPulang').addClass('srbacLoading');
        $('#tableRI').addClass('srbacLoading');
        if($('#filter_tab').val() == 'rj')
        {
            $.fn.yiiGridView.update('tablePasienPulang', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRI', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRD', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
        }else if($('#filter_tab').val() == 'ri')
        {            
            $.fn.yiiGridView.update('tablePasienPulang', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRI', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRD', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
        } else {
            $.fn.yiiGridView.update('tablePasienPulang', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRI', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
            $.fn.yiiGridView.update('tableRD', {
                    data: $(\"#searchLaporan\").serialize()
                }
            );
        }
        $('#Grafik').attr('src','').css('height','0px');
        return false;
});
");
?>


<!-- .......................................................................................... -->

<legend class="rim2"><?php echo $judulLaporan?></legend>
<div class="search-form">
    <?php $this->renderPartial('rekamMedis.views.laporan.pasienPulang._searchPasienPulang', array('modRI'=>$modRI, 'modRJ'=>$modRJ, 'modRD'=>$modRD, 'form'=>$form, 'model' => $model)); ?>
</div>    
<fieldset> 
    <div class="tab">
        <?php
            $this->widget('bootstrap.widgets.BootMenu',array(
                'type'=>'tabs',
                'stacked'=>false,
                'htmlOptions'=>array('id'=>'tabmenu'),
                'items'=>array(
                    array('label'=>'Rawat Jalan','url'=>'javascript:tab(0);','active'=>true),
                    array('label'=>'Rawat Inap','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
                    array('label'=>'Rawat Darurat','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2)),
                ),
            ))
        ?>
        <div>            
            <?php $this->renderPartial('rekamMedis.views.laporan.pasienPulang/_tablePasienPulang', array('modRJ' => $modRJ, 'modRI' => $modRI, 'modRD' => $modRD)); ?>
        </div>           
    </div>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>       
</fieldset>







<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanPasienPulang');
$this->renderPartial('rekamMedis.views.laporan._footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); ?>


<?php
$js= <<< JS
    $(document).ready(function() {
        $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_rj").show();
        $("#search_rj").show();
        $("#tgl_rj").show();

        $("#div_ri").hide();
        $("#search_ri").hide();
        $("#tgl_ri").hide();

        $("#div_rd").hide();
        $("#search_rd").hide();
        $("#tgl_rd").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){

            $("#filter_tab").val('rj');

            $("#div_rj").show();
            $("#search_rj").show();
            $("#tgl_rj").show();

            $("#div_ri").hide();
            $("#search_ri").hide();
            $("#tgl_ri").hide();

            $("#div_rd").hide();
            $("#search_rd").hide();
            $("#tgl_rd").hide();

            $.fn.yiiGridView.update('tablePasienPulang', {
                    data: $("#searchLaporan").serialize()
                }
            );  

        } else if (index==1){

            $("#filter_tab").val('ri');

            $("#div_rj").hide();
            $("#search_rj").hide();
            $("#tgl_rj").hide();

            $("#div_ri").show();
            $("#search_ri").show();
            $("#tgl_ri").show();

            $("#div_rd").hide();
            $("#search_rd").hide();
            $("#tgl_rd").hide();


            $.fn.yiiGridView.update('tableRI', {
                    data: $("#searchLaporan").serialize()
                }
            );
        }
        else if (index==2){

            $("#filter_tab").val('rd');

            $("#div_rj").hide();
            $("#search_rj").hide();
            $("#tgl_rj").hide();

            $("#div_ri").hide();
            $("#search_ri").hide();
            $("#tgl_ri").hide();

            $("#div_rd").show();
            $("#search_rd").show();
            $("#tgl_rd").show();

            $.fn.yiiGridView.update('tableRD', {
                    data: $("#searchLaporan").serialize()
                }
            );
        }
   }
function onReset()
{
   setTimeout(
        function()
        {
            if($("#filter_tab").val() == 'rj')
            {
                $('#tablePasienPulang').addClass('srbacLoading');
                $.fn.yiiGridView.update('tablePasienPulang', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableRI', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableRD', {
                        data: $("#searchLaporan").serialize()
                    }
                );
            }else if ($("#filter_tab").val() == 'ri'){
                $('#tableRI').addClass('srbacLoading');
                $.fn.yiiGridView.update('tableRI', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tablePasienPulang', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableRD', {
                        data: $("#searchLaporan").serialize()
                    }
                );
            }

            else if ($("#filter_tab").val() == 'rd'){
                $('#tableRD').addClass('srbacLoading');
                $.fn.yiiGridView.update('tableRD', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tablePasienPulang', {
                        data: $("#searchLaporan").serialize()
                    }
                );
                $.fn.yiiGridView.update('tableRI', {
                        data: $("#searchLaporan").serialize()
                    }
                );
            }

        }, 500
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pemeriksaanLab',$js,CClientScript::POS_HEAD);
?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
                $('#LBLaporanpemeriksaanpenunjangV_tglAwal').val(data.periodeawal);
                $('#LBLaporanpemeriksaanpenunjangV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>