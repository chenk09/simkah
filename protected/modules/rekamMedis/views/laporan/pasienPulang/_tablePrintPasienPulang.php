<?php 
    $rim = 'width:600px;overflow-x:none;';
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchTable();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchPrint();
      $rim = '';
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>

<div>
    <?php 
        $criteriaSearch = new CDbCriteria();
        $format = new CustomFormat();
        if (isset($_GET['RKLaporanpasienpulangRJV']['tglAwal'])){
            $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
        }
        if (isset($_GET['RKLaporanpasienpulangRJV']['tglAkhir'])){
            $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
        }



//        if (isset($_GET['RKLaporanpasienpulangRJV']['instalasi_id'])){
//            $instalasi = $_GET['RKLaporanpasienpulangRJV']['instalasi_id'];
//        }
//         if (isset($_GET['RKLaporanpasienpulangRJV']['ruangan_id'])){
//            $ruangan_id = $_GET['RKLaporanpasienpulangRJV']['ruangan_id'];
//        }

        //echo"<pre>"; 
        // print_r($_GET['RKLaporanpasienpulangV']['instalasi_nama']);
        // exit();

//        $namapasien = $_GET['RKLaporanpasienpulangRJV']['nama_pasien'];
         
//        $criteriaSearch->select ='instalasi_nama, subspesialistik';
//        $criteriaSearch->group ='instalasi_nama, subspesialistik';
//        $criteria->order = 'instalasi_nama, subspesialistik';
//        // $criteriaSearch->compare('LOWER(nama_pasien)',strtolower($namapasien),true);
//        $criteriaSearch->compare('LOWER(instalasi_nama)',strtolower($instalasi),true);
        if($_GET['filter_tab'] == "rj"){
            $criteriaSearch->compare('instalasi_id',PARAMS::INSTALASI_ID_RJ);
        }else if($_GET['filter_tab'] == "ri"){
            $criteriaSearch->compare('instalasi_id',PARAMS::INSTALASI_ID_RI);
        }else if($_GET['filter_tab'] == "rd"){
            $criteriaSearch->compare('instalasi_id',PARAMS::INSTALASI_ID_RD);
        }
        $criteriaSearch->order = 'instalasi_nama, subspesialistik';
        $criteriaSearch->addBetweenCondition('tglpasienpulang', $tglAwal, $tglAkhir);
        $models = RKLaporanpasienpulangRJV::model()->findAll($criteriaSearch);
      
            echo "<table width='100%' border='1'>
                            <tr style='font-weight:bold;'>
                                <td align='center'>No</td>
                                <td align='center'>No Rekam Medik</td>
                                <td align='center'>Nama Pasien</td>
                                <td align='center'>Tanggal Lahir</td>
                                <td align='center'>Jenis Kelamin</td>
                                <td align='center'>Alamat</td>
                                <td align='center'>No Telepon</td>
                                <td align='center'>Tgl Masuk</td>
                                <td align='center'>Cara Masuk</td>
                                <td align='center'>Cara Bayar</td>
                                <td align='center'>Nama PJP</td>
                                <td align='center'>Kamar Perawatan</td>
                                <td align='center'>Dokter yang Merawat</td>
                                <td align='center'>Tanggal Pulang</td>
                                <td align='center'>Keterangan Keluar</td>
                            </tr>";
               
            $criteria = new CDbCriteria;
            $format = new CustomFormat();
            if (isset($_GET['RKLaporanpasienpulangRJV']['tglAwal'])){
            $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
            }
            if (isset($_GET['RKLaporanpasienpulangRJV']['tglAkhir'])){
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
            }
            if (isset($_GET['RKLaporanpasienpulangRJV']['carabayar_id'])){
                $carabayar = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
            }
            if (isset($_GET['RKLaporanpasienpulangRJV']['penjamin_id'])){
                $penjamin = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
            }

        
            $term = $pasien;
           /** $criteria->select ='instalasi_id, instalasi_nama, subspesialistik, jeniskasuspenyakit_nama, kelaspelayanan_nama, nama_pj, no_rekam_medik, no_pendaftaran, nama_pasien, no_telepon_pasien, kondisipulang, carakeluar, dokterpenanggungjawab_nama, dokterpenanggungjawab_gelardepan, dokterpenanggungjawab_gelarbelakang, dokterspesialistik_nama, dokterspesialistik_gelardepan, dokterspesialistik_gelarbelakang, tglpasienpulang';
            $criteria->group ='instalasi_id, instalasi_nama, subspesialistik, jeniskasuspenyakit_nama, kelaspelayanan_nama, nama_pj, no_rekam_medik, no_pendaftaran, nama_pasien, no_telepon_pasien, kondisipulang, carakeluar, dokterpenanggungjawab_nama, dokterpenanggungjawab_gelardepan, dokterpenanggungjawab_gelarbelakang, dokterspesialistik_nama, dokterspesialistik_gelardepan, dokterspesialistik_gelarbelakang, tglpasienpulang';
            $criteria->order = 'instalasi_nama, subspesialistik';
            * 
            */
            
            // $condition  = "nama_pasien = '".$term."'";
//            $criteria->compare('instalasi_id',$instalasi);
//             $criteria->addInCondition('ruangan_id',$ruangan_id);
//            $criteria->addCondition($condition);
            if($_GET['filter_tab'] == "rj"){
                $criteria->compare('instalasi_id',PARAMS::INSTALASI_ID_RJ);
                $criteria->compare('penjamin_id',$penjamin);
                $criteria->compare('carabayar_id',$carabayar);
            }else if($_GET['filter_tab'] == "ri"){
                $criteria->compare('instalasi_id',PARAMS::INSTALASI_ID_RI);
                $criteria->compare('penjamin_id',$penjamin);
                $criteria->compare('carabayar_id',$carabayar);
            }else if($_GET['filter_tab'] == "rd"){
                $criteria->compare('instalasi_id',PARAMS::INSTALASI_ID_RD);
                $criteria->compare('penjamin_id',$penjamin);
                

            }
            $criteria->order = 'instalasi_nama, subspesialistik,tglpasienpulang';
            $criteria->addBetweenCondition('tglpasienpulang', $tglAwal, $tglAkhir);
            $criteria->limit = -1;

            
            $detail = RKLaporanpasienpulangRJV::model()->findAll($criteria);
            foreach($detail as $key=>$details){
                    echo "<tr>
                              <td width='3%;' style='text-align:center'>".($key+1)."</td>
                              <td width='10%;'>".$details->no_rekam_medik."</td>
                              <td width='10%;'>".$details->nama_pasien."</td>
                              <td width='10%;'>".$details->tanggal_lahir."</td>
                              <td width='5%;'>".$details->jk_pasien."</td>
                              <td width='10%;'><center>".$details->alamat_pasien."</center></td>
                              <td width='10%;'><center>".$details->no_telepon_pasien."</center></td>
                              <td width='10%;'><center>".$details->tgl_pendaftaran."</center></td>
                              <td width='25%;'>".$details->caramasuk_nama."</td>
                              <td width='10%;'>".$details->carabayar_nama."</td>
                              <td width='10%;'>".$details->nama_pj."</td>
                              <td width='10%;'><center>".$details->kamarperawatan."</center></td>
                              <td width='20%;'><center>".$details->dokterpenanggungjawab_gelardepan." ".$details->dokterpenanggungjawab_nama." ".$details->dokterpenanggungjawab_gelarbelakang."</center></td>
                              <td width='20%;'><center>".$details->tglpasienpulang."</center></td>
                              <td width='20%;'><center>".$details->kondisipulang."</center></td>
                              
                          </tr>";
            }
            
            echo "</table><br/><br/>";
         
    ?> 
</div>