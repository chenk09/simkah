<?php
$urlAjax = Yii::app()->createUrl('ActionDynamic/GetRuanganDariInstalasi', array('encode' => false, 'namaModel' => $model->getNamaModel()));
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('lap-penyatik-10', {
		data: $(this).serialize()
    });
    return false;
});
    $('#". CHtml::activeId($model, 'instalasi_id') ."').change(function(){
        $.ajax({
            type:'POST',
            data:$('#searchInfoKunjungan').serialize(),
            url:'". $urlAjax ."',
            success:function(data){
                $('#". CHtml::activeId($model, 'ruangan_id') ."').html('<option value>-- Pilih --</pilih>'+ data)
            }
        });
    })
");
?>
<div class="search-form" style="">
    <?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'type' => 'horizontal',
        'id' => 'searchInfoKunjungan',
        'htmlOptions' => array(
			'enctype' => 'multipart/form-data',
			'onKeyPress' => 'return disableKeyPress(event)'
		),
	));?>
    <fieldset>
		<legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
        <table>
            <tr>
                <td width="50%">
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <div class="control-group">
                        <div class="control-label">Tanggal Pemeriksaan</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                </td>
                <td>
					<?php
					$this->widget('MyDateTimePicker', array(
						'model' => $model,
						'attribute' => 'tglAkhir',
						'mode' => 'datetime',
						'options' => array(
							'dateFormat' => Params::DATE_FORMAT_MEDIUM,
						),
						'htmlOptions' => array('readonly' => true,
							'onkeypress' => "return $(this).focusNextInputField(event)"),
					));
					?>
                </td>
            </tr>
			<tr>
				<td><?php echo $form->dropDownListRow($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)"));?></td>
				<td><?php echo $form->dropDownList($model, 'ruangan_id', array(), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?></td>
			</tr>
			<tr>
				<td><?php echo $form->textFieldRow($model, 'jumlahTampil', array('onkeypress' => "return $(this).focusNextInputField(event)", 'class'=>'span1 numbersOnly'));?></td>
				<td>&nbsp;</td>
			</tr>
        </table>
    </fieldset>
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));?>
		<?php echo CHtml::htmlButton(
			Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
				array(
					'type'=>'reset',
					'class'=>'btn btn-danger'
				)
		);?>
    </div>
</div> 
<?php $this->endWidget();?>
<?php $this->renderPartial('10Besar/_table', array('model'=>$model)); ?>