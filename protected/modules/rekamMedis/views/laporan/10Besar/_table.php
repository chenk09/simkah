<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
	
    if(isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if($caraPrint == "EXCEL") $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }else{
        $data = $model->searchLaporan();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
    'id'=>'lap-penyatik-10',
    'dataProvider'=>$data,
	'template'=>$template,
	'enableSorting'=>$sort,
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header' => 'No',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
        ),
        'jumlah',
        'diagnosa_kode',
        'diagnosa_nama',
    ),
)); ?> 