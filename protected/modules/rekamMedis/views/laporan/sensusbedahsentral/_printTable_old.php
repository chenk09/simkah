<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#laporan-search').submit(function(){
                $('#laporan-grid').addClass('srbacLoading');
            $.fn.yiiGridView.update('laporan-grid', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'laporan-grid',
	'dataProvider'=>$model->searchLaporan(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>UMUR</center>',
                'start'=>3, 
                'end'=>4,
            ),
        ),
	    'columns'=>array(
            array(
                'header'=>'No',
                'type'=>'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            'nama_pasien',
            'no_rekam_medik',
            'tgl_rekam_medik',
            array(
                'header'=>'L',
                'type'=>'raw',
                'value'=>'(($data->jeniskelamin=="LAKI-LAKI") ? substr($data->umur,0,7) : "-")',
            ),
            array(
                'header'=>'P',
                'type'=>'raw',
                'value'=>'(($data->jeniskelamin=="PEREMPUAN") ? substr($data->umur,0,7) : "-")',
            ),
            array(
                'header'=>'Diagnosa',
                'type'=>'raw',
                'value'=>'',
            ),
            'kegiatanoperasi_nama',
            'golonganoperasi_nama',
            'jenisanastesi_nama',
            'kelaspelayanan_nama',
            array(
                'header'=>'Nama Dokter',
                'type'=>'raw',
                'value'=>'$data->NamaPegawai',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>