<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#laporan-search').submit(function(){
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">SENSUS HARIAN INSTALASI BEDAH SENTRAL</legend>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'laporan-search',
        'type'=>'horizontal',
)); ?>

<table>
    <td>
        <div class = 'control-label'>Tanggal Masuk Penunjang</div>
            <div class="controls">  
                <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
							'class'=>'dtPicker3',
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                ?>
            </div> 
        </div>
    </td>
    <td style="padding:0px 100px 0 0;"><?php echo CHtml::label(' Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
        <div class="controls">  
            <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true,
						'class'=>'dtPicker3',
                        'onkeypress' => "return $(this).focusNextInputField(event)"),
                ));
            ?>
        </div> 
    </td>
</table>
	
	<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
        Yii::app()->createUrl($this->module->id.'/'.Laporan.'/LaporanSensusBedahSentral'), 
        array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Sensus Bedah Sentral</legend>
<?php $this->renderPartial('sensusbedahsentral/_table',array('model'=>$model)); ?>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanSensusBedahSentral');
$this->renderPartial('_footerWithoutgrafik', array('urlPrint'=>$urlPrint, 'url'=>$url));
// $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>
