<?php
Yii::app()->clientScript->registerScript('search', "
$('#rmdokrekammedisrm-t-search').submit(function(){
	$.fn.yiiGridView.update('informasidokrekammedis-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Informasi Dokumen Rekam Medis</legend>
<div class='hide'>
<?php 
$this->widget('ext.colorpicker.ColorPicker', 
    array(
        'name'=>'Dokumen[warnadokrm_id][]',
        'value'=>WarnadokrmM::model()->getKodeWarnaId($warnadokrm_id),// string hexa decimal contoh 000000 atau 0000ff
        'height'=>'30px', // tinggi
        'width'=>'83px',        
        //'swatch'=>true, // default false jika ingin swatch
        'colors'=>  WarnadokrmM::model()->getKodeWarna(), //warna dalam bentuk array contoh array('0000ff','00ff00')
        'colorOptions'=>array(
            'transparency'=> true,
           ),
        )
    );
?>
</div>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasidokrekammedis-m-grid',
	'dataProvider'=>$model->searchinformasi(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
            'header'=>'Lokasi Rak',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-pencil-brown></i> ".$data->lokasirak->lokasirak_nama," ",array("onclick"=>"ubahLokasirak(\'$data->dokrekammedis_id\');$ (\'#editLokasiRak\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Lokasi Rak"))',
            'htmlOptions'=>array('style'=>'text-align: center')
        ),
		array(
            'header'=>'Sub Rak',
            // 'value'=>'$data->subrak->subrak_nama',
            'type'=>'raw',
            'value'=>'CHtml::link("<i class=icon-pencil-brown></i> ".$data->subrak->subrak_nama," ",array("onclick"=>"ubahSubrak(\'$data->dokrekammedis_id\');$ (\'#editSubRak\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Sub Rak"))',
            'htmlOptions'=>array('style'=>'text-align: center')
        ),
        array(
            'header'=>'Warna Dokumen RM',
            'type'=>'raw',
            'value'=>'$this->grid->getOwner()->renderPartial(\'_warnaDokumen2\', array(\'warnadokrm_id\'=>$data->warnadokrm_id), true)',
            // 'htmlOptions'=>'CHtml::link(array("onclick"=>"ubahWarna(\'$data->dokrekammedis_id\'))'
        ),
		array(
                                    'header'=>'Tanggal Rekam Medis',
                                    'value'=>'$data->tglrekammedis',
                                ),
                                array(
                                    'header'=>'No Dokumen<br/>Rekam Medis',
                                    'value'=>'$data->nodokumenrm'
                                ),
                                array(
                                    'header'=>'Nama Pasien',
                                    'value'=>'$data->pasien->namadepan.$data->pasien->nama_pasien',
                                ),
                                array(
                                    'header'=>'No Tertieer',
                                    'value'=>'$data->nomortertier'
                                ),
                                array(
                                    'header'=>'No Sekunder',
                                    'value'=>'$data->nomorsekunder',
                                ),
                                array(
                                    'header'=>'No Primer',
                                    'value'=>'$data->nomorprimer'
                                ),
                                array(
                                    'header'=>'Tanggal Lahir',
                                    'value'=>'$data->pasien->tanggal_lahir',
                                ),
                                array(
                                    'header'=>'Jenis Kelamin',
                                    'value'=>'$data->pasien->jeniskelamin',
                                ),
                                array(
                                    'header'=>'Status',
                                    'value'=>'$data->statusrekammedis',
                                ),
	),
        'afterAjaxUpdate'=>'function(id, data){
            var colors = jQuery(\'input[rel="colorPicker"]\').attr(\'colors\').split(\',\');
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            jQuery(\'input[rel="colorPicker"]\').colorPicker({colors:colors});
        }',
)); ?>
<?php $this->renderPartial('_searchinformasi',array('model'=>$model)); ?>

<script type="text/javascript">

function ubahLokasirak(dokrm)
{
    $('#temp_lokasirak').val(dokrm);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahLokasirak')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editLokasiRak div.divForFormEditLokasiRak').html(data.div);
                $('#editLokasiRak div.divForFormEditLokasiRak form').submit(ubahLokasirak);
            }else{
                $('#editLokasiRak div.divForFormEditLokasiRak').html(data.div);
                $.fn.yiiGridView.update('informasidokrekammedis-m-grid', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editLokasiRak').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function ubahSubrak(dokrm)
{
    $('#temp_subrak').val(dokrm);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahSubrak')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editSubRak div.divForFormEditSubRak').html(data.div);
                $('#editSubRak div.divForFormEditSubRak form').submit(ubahSubrak);
            }else{
                $('#editSubRak div.divForFormEditSubRak').html(data.div);
                $.fn.yiiGridView.update('informasidokrekammedis-m-grid', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editSubRak').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function ubahWarna(dokrm){
    alert(dokrm);
}

</script>

<?php
    //=================== Ganti Data Lokasi Rak ==================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editLokasiRak',
            'options'=>array(
                'title'=>'Ganti Lokasi Rak',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_lokasirak','',array('readonly'=>true));
    echo '<div class="divForFormEditLokasiRak"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');

    //=================== Ganti Data Sub Rak ==================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editSubRak',
            'options'=>array(
                'title'=>'Ganti Sub Rak',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_subrak','',array('readonly'=>true));
    echo '<div class="divForFormEditSubRak"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>