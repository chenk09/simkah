<?php

class RKrl13FastempattidurV extends Rl13FastempattidurV {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
     public function getSumPelayanan($jeniskasuspenyakit_id, $kelaspelayanan_id, $instalasi_id = null){
         
            
            if (empty($instalasi_id)) {
            	$instalasi_id = Yii::app()->user->getState('instalasi_id');
            }
            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'instalasi_id, jeniskasuspenyakit_id, kelaspelayanan_id';

           $criteria->addCondition('kelaspelayanan_id = '.$kelaspelayanan_id);
           $criteria->addCondition('instalasi_id = '.$instalasi_id);
           $criteria->addCondition('jeniskasuspenyakit_id ='.$jeniskasuspenyakit_id);


            $criteria->select = $criteria->group.', sum(jlmtt) AS jlmtt';
            $modTarif = Rl13FastempattidurV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jlmtt;
            }
             return $totTarif;
        }

        public function getSumTotal($kelaspelayanan_id, $instalasi_id = null){
          
            
            if (empty($instalasi_id)) {
            	$instalasi_id = Yii::app()->user->getState('instalasi_id');
            }

            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'instalasi_id, kelaspelayanan_id';

           $criteria->addCondition('kelaspelayanan_id = '.$kelaspelayanan_id);
           $criteria->addCondition('instalasi_id = '.$instalasi_id);


            // if(isset($_GET['GZLaporanJumlahPorsiV'])){
            //     $tglAwal = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAwal']);
            //     $tglAkhir = $format->formatDateTimeMediumForDB($_GET['GZLaporanJumlahPorsiV']['tglAkhir']);
            //     $criteria->addBetweenCondition('tglkirimmenu',$tglAwal,$tglAkhir);
            // }

            $criteria->select = $criteria->group.', sum(jlmtt) AS jlmtt';
            $modTarif = Rl13FastempattidurV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jlmtt;
            }
             return $totTarif;
        }
}