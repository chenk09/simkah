<?php

class RKLaporanrekapperunitpelayananV extends LaporanrekapperunitpelayananV
{
	public $tglAwal, $tglAkhir;
	public $dokter_nama;
	public $jumlahkunjunganbaru, $jumlahkunjunganlama, $jumlahkunjungan;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanrekapperunitpelayananV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 * digunakan di: 
	 * - rekamMedis/Laporan/LaporanPerUnitPelayanan
	 */
	
	public function searchLaporanPerUnit()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
		$criteria->addCondition("statusperiksa <> 'BATAL PERIKSA'");
		$criteria->group = "ruangan_id, ruangan_nama";
		$criteria->select = $criteria->group.", count(pendaftaran_id) as jumlahkunjungan, "
				. "COUNT(CASE statuspasien WHEN 'PENGUNJUNG BARU' THEN 1 ELSE NULL END) AS jumlahkunjunganbaru, "
				. "COUNT(CASE statuspasien WHEN 'PENGUNJUNG LAMA' THEN 1 ELSE NULL END) AS jumlahkunjunganlama";

		if(!empty($this->ruangan_id)){
			if(is_array($this->ruangan_id)){
				if($this->instalasi_id == Params::INSTALASI_ID_RI){ //EHJ-3642
					$criteria->addInCondition("ruangan_id",$this->ruangan_id);
				}else{
					$criteria->addInCondition("ruangan_id",$this->ruangan_id);
				}
			}else{
				if($this->instalasi_id == Params::INSTALASI_ID_RI){ //EHJ-3642
					$criteria->addCondition("ruangan_id = ".$this->ruangan_id);
				}else{
					$criteria->addCondition("ruangan_id = ".$this->ruangan_id);
				}
			}
		}
		
		$criteria->order = "ruangan_nama";
		if(isset($_REQUEST['caraPrint'])){
			$criteria->limit = -1;
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>false,
			));
		}else{
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
		}
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(nokode_rumahsakit)',strtolower($this->nokode_rumahsakit),true);
		$criteria->compare('LOWER(nama_rumahsakit)',strtolower($this->nama_rumahsakit),true);
		$criteria->compare('LOWER(kelas_rumahsakit)',strtolower($this->kelas_rumahsakit),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		$criteria->compare('warnadokrm_id',$this->warnadokrm_id);
		$criteria->compare('LOWER(warnadokrm_namawarna)',strtolower($this->warnadokrm_namawarna),true);
		$criteria->compare('LOWER(warnadokrm_kodewarna)',strtolower($this->warnadokrm_kodewarna),true);
		$criteria->compare('LOWER(warnadokrm_fungsi)',strtolower($this->warnadokrm_fungsi),true);
		$criteria->compare('subrak_id',$this->subrak_id);
		$criteria->compare('LOWER(subrak_nama)',strtolower($this->subrak_nama),true);
		$criteria->compare('lokasirak_id',$this->lokasirak_id);
		$criteria->compare('LOWER(lokasirak_nama)',strtolower($this->lokasirak_nama),true);
		$criteria->compare('LOWER(nodokumenrm)',strtolower($this->nodokumenrm),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower($this->statusrekammedis),true);
		$criteria->compare('LOWER(jenis_identitas_pasien)',strtolower($this->jenis_identitas_pasien),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(nama_ibu)',strtolower($this->nama_ibu),true);
		$criteria->compare('LOWER(nama_ayah)',strtolower($this->nama_ayah),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('LOWER(kode_pos)',strtolower($this->kode_pos),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(photopasien)',strtolower($this->photopasien),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
		$criteria->compare('ispasienluar',$this->ispasienluar);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(kelompokumur_nama)',strtolower($this->kelompokumur_nama),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('LOWER(suku_nama)',strtolower($this->suku_nama),true);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('LOWER(pekerjaan_nama)',strtolower($this->pekerjaan_nama),true);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('LOWER(pendidikan_nama)',strtolower($this->pendidikan_nama),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('ruanganinap_id',$this->ruanganinap_id);
		$criteria->compare('LOWER(ruanganinap_nama)',strtolower($this->ruanganinap_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		$criteria->compare('instalasiinap_id',$this->instalasiinap_id);
		$criteria->compare('LOWER(instalasiinap_nama)',strtolower($this->instalasiinap_nama),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
		$criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(no_urutantri)',strtolower($this->no_urutantri),true);
		$criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
		$criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
		$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
		$criteria->compare('alihstatus',$this->alihstatus);
		$criteria->compare('byphone',$this->byphone);
		$criteria->compare('kunjunganrumah',$this->kunjunganrumah);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(umur)',strtolower($this->umur),true);
		$criteria->compare('LOWER(no_asuransi)',strtolower($this->no_asuransi),true);
		$criteria->compare('LOWER(namapemilik_asuransi)',strtolower($this->namapemilik_asuransi),true);
		$criteria->compare('LOWER(nopokokperusahaan)',strtolower($this->nopokokperusahaan),true);
		$criteria->compare('kelastanggungan_id',$this->kelastanggungan_id);
		$criteria->compare('LOWER(namaperusahaan)',strtolower($this->namaperusahaan),true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('LOWER(jenis_identitas_karyawan)',strtolower($this->jenis_identitas_karyawan),true);
		$criteria->compare('LOWER(no_identitas_karyawan)',strtolower($this->no_identitas_karyawan),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('LOWER(gelarbelakang_nama)',strtolower($this->gelarbelakang_nama),true);
		$criteria->compare('LOWER(namalengkapdokter)',strtolower($this->namalengkapdokter),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}