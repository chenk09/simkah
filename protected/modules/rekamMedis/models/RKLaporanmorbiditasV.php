<?php
class RKLaporanmorbiditasV extends LaporanmorbiditasV
{
        public $lakilaki,$perempuan,$jumlahkunjungan;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         *  method untuk criteria
         * @return CDbCriteria 
         * modified by @author Rahman Fad | Ubah Select dan Group untuk golongan umur (EHJ-1907) | 21-05-2014
         */


        protected function functionCriteria()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->select = "golonganumur_nama, tglmorbiditas, golonganumur_namalainnya, jeniskelamin, pendaftaran_id , CASE WHEN jeniskelamin = '".Params::JENIS_KELAMIN_PEREMPUAN."' THEN 0 else 1 END AS lakilaki, CASE WHEN jeniskelamin = '".Params::JENIS_KELAMIN_PEREMPUAN."' THEN 1 else 0 END AS perempuan";
                $criteria->group = "golonganumur_nama, tglmorbiditas, golonganumur_namalainnya, jeniskelamin, pendaftaran_id";
//               
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
                
                

		return $criteria;
	}
        /**
         *  method untuk data provider pada table laporan
         * @return CActiveDataProvider 
         */
  //       public function searchTable(){
  //           $criteria = $this->functionCriteria();
  //           $crit = new MyCriteria();
  //           $crit->select = 'diagnosa_nama, sum(golonganumur_namalainnya) as golongan_umur, count(pendaftaran_id) as jumlah, sum(lakilaki) as lakilaki , sum(perempuan) as perempuan, count(pendaftaran_id) as jumlahkunjungan';
  //           $crit->group = 'diagnosa_nama';
  //           $crit->mergeWith($criteria);
  //           return new CActiveDataProvider($this, array(
		// 	'criteria'=>$crit,
		// ));
  //       }

        public function searchTable(){

            $criteria=new CDbCriteria;
            $criteria->select='diagnosa_nama, golonganumur_namalainnya';
            $criteria->group='diagnosa_nama, golonganumur_namalainnya';

            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            // $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(jmlgolumur)',strtolower($this->jmlgolumur),true);
            $criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
            $criteria->compare('LOWER(golonganumur_namalainnya)',strtolower($this->golonganumur_namalainnya), true);
            $criteria->compare('diagnosa_id',$this->diagnosa_id);
            $criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
            $criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
            $criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
            $criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
            $criteria->compare('golonganumur_id',$this->golonganumur_id);
            $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
        }
        
        public function search()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                $criteria->addBetweenCondition('DATE(tglmorbiditas)', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('pasien_id',$this->pasien_id);
                $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
                // $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
                $criteria->compare('LOWER(golonganumur_namalainnya)',strtolower($this->golonganumur_namalainnya), true);
                $criteria->compare('diagnosa_id',$this->diagnosa_id);
                $criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
                $criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
                $criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
                $criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
                $criteria->compare('golonganumur_id',$this->golonganumur_id);
                $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        /**
         * method untuk data provider pada print laporan
         * @return CActiveDataProvider 
         */
        public function searchPrint(){
            $criteria=new CDbCriteria;
                $criteria->addBetweenCondition('DATE(tglmorbiditas)', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('pasien_id',$this->pasien_id);
                $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
                // $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
                $criteria->compare('LOWER(golonganumur_namalainnya)',strtolower($this->golonganumur_namalainnya), true);
                $criteria->compare('diagnosa_id',$this->diagnosa_id);
                $criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
                $criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
                $criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
                $criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
                $criteria->compare('golonganumur_id',$this->golonganumur_id);
                $criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
                $criteria->limit=-1;
                return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        /**
         * method untuk data provider grafik
         * @return CActiveDataProvider 
         */
        public function searchGrafik(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'golonganumur_nama as tick, jeniskelamin as data, count(pendaftaran_id) as jumlah';
            $crit->group = 'golonganumur_nama,jeniskelamin';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }

        public function getSumGolUmur($nama_kolom = null, $iddiagnosa){
            
           
            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'diagnosa_id, golonganumur_id';
            
            if($nama_kolom == 'BATITA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BATITA);
            }else if($nama_kolom == 'BARU LAHIR'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BARU_LAHIR);
            }else if($nama_kolom == 'BAYI'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BAYI);
            }else if($nama_kolom == 'BALITA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BALITA);
            }else if($nama_kolom == 'ANAK ANAK'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_ANAK_ANAK);
            }else if($nama_kolom == 'REMAJA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_REMAJA);
            }else if($nama_kolom == 'DEWASA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_DEWASA);
            }else if($nama_kolom == 'ORANG TUA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_ORANG_TUA);
            }else if($nama_kolom == 'MANULA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_MANULA);
            }

            if(isset($_GET['RKLaporanmorbiditasV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAkhir']);
                $criteria->addBetweenCondition('tglmorbiditas',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jmlgolumur) AS jmlgolumur';
            $modTarif = LaporanmorbiditasV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jmlgolumur;
            }
             return $totTarif;
        }

        public function getSumJK($nama_kolom = null, $iddiagnosa)
        {

            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'diagnosa_id, jeniskelamin';
            
            if($nama_kolom == 'LAKI'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition("jeniskelamin like '%LAKI%'");
            }else if($nama_kolom == 'PEREMPUAN'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition("jeniskelamin like '%PEREMPUAN%'");
            }

            if(isset($_GET['RKLaporanmorbiditasV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAkhir']);
                $criteria->addBetweenCondition('tglmorbiditas',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jmljeniskelamin) AS jmljeniskelamin';
            $modJK = LaporanmorbiditasV::model()->findAll($criteria);
            $total = 0;
            foreach($modJK as $key=>$jml){
                $total += $jml->jmljeniskelamin;
            }
             return $total;
        }

        public function getSumTotalT($iddiagnosa){

        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'diagnosa_id, jeniskelamin';
        $criteria->compare('diagnosa_id',$iddiagnosa);

        if(isset($_GET['RKLaporanmorbiditasV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAkhir']);
                // $jenisdiet_nama =$_GET['GZLaporanJumlahPorsiV']['jenisdiet_nama'];
                $criteria->addBetweenCondition('tglmorbiditas',$tglAwal,$tglAkhir);
                // $criteria->compare('LOWER(jenisdiet_nama)',strtolower($jenisdiet_nama));
                // $criteria->addInCondition('jenisdiet_id',$this->jenisdiet_id);
            }
            $criteria->select = $criteria->group.',sum(jmljeniskelamin) AS jmljeniskelamin';
            $modTarif = LaporanmorbiditasV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jmljeniskelamin;
            }
             return $totTarif;
     }

}