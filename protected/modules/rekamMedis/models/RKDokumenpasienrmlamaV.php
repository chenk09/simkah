<?php
class RKDokumenpasienrmlamaV extends DokumenpasienrmlamaV {


    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchPeminjaman()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('subrak', 'pendaftaran', 'pengiriman');
		$criteria->addCondition('t.peminjamanrm_id is null or (t.peminjamanrm_id is not null and t.kembalirm_id is not null)');
                if (!empty($this->no_rekam_medik_akhir)){
                    $criteria->addCondition("CAST(t.no_rekam_medik as integer) between ".$this->no_rekam_medik." and ".$this->no_rekam_medik_akhir);
                } else {
                    $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                }
		
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
                if (!empty($this->tgl_rekam_medik_akhir)){
                    $criteria->addBetweenCondition('date(t.tgl_rekam_medik)', $this->tgl_rekam_medik, $this->tgl_rekam_medik_akhir);
                }
                else{
                    $criteria->compare('DATE(t.tgl_rekam_medik)',$this->tgl_rekam_medik);    
                }
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(t.statusrekammedis)',strtolower($this->statusrekammedis),true);
                $criteria->compare('t.ruangan_id',$this->ruangan_id);
                $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('t.instalasi_id',$this->instalasi_id);
		$criteria->compare('t.subrak_id',$this->subrak_id);
		$criteria->compare('t.lokasirak_id',$this->lokasirak_id);
                $criteria->compare('t.pendaftaran_id', $this->pendaftaran_id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPengiriman()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->with = array('subrak', 'pendaftaran', 'pengiriman');
                $criteria->addCondition('t.pengirimanrm_id is null');
                $criteria->addCondition('t.peminjamanrm_id is not null');
                
                if (!empty($this->no_rekam_medik_akhir)){
                    $criteria->addCondition("CAST(t.no_rekam_medik as integer) between ".$this->no_rekam_medik." and ".$this->no_rekam_medik_akhir);
                } else {
                    $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                }
		
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
                if (!empty($this->tgl_rekam_medik_akhir)){
                    $criteria->addBetweenCondition('date(t.tgl_rekam_medik)', $this->tgl_rekam_medik, $this->tgl_rekam_medik_akhir);
                }
                else{
                    $criteria->compare('DATE(t.tgl_rekam_medik)',$this->tgl_rekam_medik);    
                }
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(t.statusrekammedis)',strtolower($this->statusrekammedis),true);
                $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('t.instalasi_id',$this->instalasi_id);
                $criteria->compare('t.ruangan_id',$this->ruangan_id);
		//$criteria->compare('warnadokrm_id',$this->warnadokrm_id);
		//$criteria->compare('LOWER(warnadokrm_namawarna)',strtolower($this->warnadokrm_namawarna),true);
		//$criteria->compare('LOWER(warnadokrm_kodewarna)',strtolower($this->warnadokrm_kodewarna),true);
		//$criteria->compare('lokasirak_id',$this->lokasirak_id);
		//$criteria->compare('LOWER(lokasirak_nama)',strtolower($this->lokasirak_nama),true);
		//$criteria->compare('LOWER(nodokumenrm)',strtolower($this->nodokumenrm),true);
		//$criteria->compare('LOWER(tglrekammedis)',strtolower($this->tglrekammedis),true);
//		$criteria->compare('pasien_id',$this->pasien_id);
                
//		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
//		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
//		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
//		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
//		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
//		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
//		$criteria->compare('LOWER(tglmasukrak)',strtolower($this->tglmasukrak),true);
		
//		$criteria->compare('LOWER(nomortertier)',strtolower($this->nomortertier),true);
//		$criteria->compare('LOWER(nomorsekunder)',strtolower($this->nomorsekunder),true);
//		$criteria->compare('LOWER(nomorprimer)',strtolower($this->nomorprimer),true);
//		$criteria->compare('LOWER(warnanorm_i)',strtolower($this->warnanorm_i),true);
//		$criteria->compare('LOWER(warnanorm_ii)',strtolower($this->warnanorm_ii),true);
//		$criteria->compare('LOWER(tglkeluarakhir)',strtolower($this->tglkeluarakhir),true);
//		$criteria->compare('LOWER(tglmasukakhir)',strtolower($this->tglmasukakhir),true);
//		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
		
//		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);

//		$criteria->compare('LOWER(instalasi_nama)',strtolower($this->instalasi_nama),true);
		//$criteria->compare('pendaftaran_id',$this->pendaftaran_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}