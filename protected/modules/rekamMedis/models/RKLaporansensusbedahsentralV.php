<?php

class RKLaporansensusbedahsentralV extends LaporansensusbedahsentralV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporansensusbedahsentralV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchLaporan()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria = new CDbCriteria;
		$criteria->select = "pasienmasukpenunjang_id, tglmasukpenunjang,nama_pasien,no_rekam_medik,jeniskelamin,operasi_nama,kegiatanoperasi_nama,jenisanastesi_nama,kelaspelayanan_nama";
		$criteria->addBetweenCondition('tglmasukpenunjang',$this->tglAwal,$this->tglAkhir);
		$criteria->group = "pasienmasukpenunjang_id, tglmasukpenunjang,nama_pasien,no_rekam_medik,jeniskelamin,operasi_nama,kegiatanoperasi_nama,jenisanastesi_nama,kelaspelayanan_nama";
		$criteria->order = "tglmasukpenunjang";
		
		if(isset($_GET['caraPrint'])){
			$criteria->limit = -1;
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>false,
			));
		}else{
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
		}
	}
        
        
}