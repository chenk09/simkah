<?php

class RKPeminjamanrmT extends PeminjamanrmT {


    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchPengiriman()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran', 'warnadokrm');
//		$criteria->compare('peminjamanrm_id',$this->peminjamanrm_id);
//		$criteria->compare('pengirimanrm_id',$this->pengirimanrm_id);
//		$criteria->compare('dokrekammedis_id',$this->dokrekammedis_id);
//		$criteria->compare('pasien_id',$this->pasien_id);
//		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
//		$criteria->compare('kembalirm_id',$this->kembalirm_id);
//		$criteria->compare('ruangan_id',$this->ruangan_id);
//		$criteria->compare('LOWER(nourut_pinjam)',strtolower($this->nourut_pinjam),true);
//		$criteria->compare('LOWER(tglpeminjamanrm)',strtolower($this->tglpeminjamanrm),true);
//		$criteria->compare('LOWER(untukkepentingan)',strtolower($this->untukkepentingan),true);
//		$criteria->compare('LOWER(keteranganpeminjaman)',strtolower($this->keteranganpeminjaman),true);
//		$criteria->compare('LOWER(tglakandikembalikan)',strtolower($this->tglakandikembalikan),true);
//		$criteria->compare('LOWER(namapeminjam)',strtolower($this->namapeminjam),true);
//		$criteria->compare('printpeminjaman',$this->printpeminjaman);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
//		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
//		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
//		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}