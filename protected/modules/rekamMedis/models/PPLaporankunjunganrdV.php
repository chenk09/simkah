<?php
class PPLaporankunjunganrdV extends LaporankunjunganrdV
{
        public $jumlah;
        public $data;
        public $tick;
        public $pilihanx;
        public $Jenis_kasus_nama_penyakit;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRD()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
//			$criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
            $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->order = 'tgl_pendaftaran DESC';

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
	}

        public function criteriaSearch()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
            $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->order = 'tgl_pendaftaran DESC';
            return $criteria; 
	}	

	public function searchGrafik(){
                
            $criteria = MyFunction::criteriaGrafik1($this, 'data', array('tick'=>'ruangan_nama'));

            $criteria->order = 'ruangan_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
//		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
	public function searchTableLaporan()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->group = 'tgl_pendaftaran,no_pendaftaran,no_rekam_medik,nama_pasien,nama_bin,jeniskelamin,golonganumur_nama,agama,statusperkawinan,pekerjaan_nama,alamat_pasien,kecamatan_nama,kabupaten_nama,caramasuk_nama,kunjungan,nama_pj,nama_perujuk,jeniskasuspenyakit_nama,carabayar_nama,penjamin_nama,ruangan_nama,nama_pegawai,statusperiksa,create_time,diagnosa_kode,diagnosa_nama,carakeluar';
            $criteria->select = $criteria->group;
            $criteria->order ='nama_pasien';
			$criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);			
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
            $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
			$criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan));
			
//		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
	}
        public function searchTableLaporanPrint()
	{
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            
            $criteria->group = 'tgl_pendaftaran,no_pendaftaran,no_rekam_medik,nama_pasien,nama_bin,jeniskelamin,golonganumur_nama,agama,statusperkawinan,pekerjaan_nama,alamat_pasien,kecamatan_nama,kabupaten_nama,caramasuk_nama,kunjungan,nama_pj,nama_perujuk,jeniskasuspenyakit_nama,carabayar_nama,penjamin_nama,ruangan_nama,nama_pegawai,statusperiksa,create_time,diagnosa_kode,diagnosa_nama,carakeluar';
            $criteria->select = $criteria->group;
            $criteria->order ='nama_pasien';
            $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
            $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
//		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
            $criteria->compare('rujukan_id',$this->rujukan_id);
            $criteria->limit = -1;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
	}
        
        public function getNamaModel()
        {
            return __CLASS__;
        }

        public function searchAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        
        public function searchGrafikAgama(){
             
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, agama as data';
            $criteria->group = 'agama';
            $criteria->order = 'agama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchUmur(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printUmur(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikUmur(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, golonganumur_nama as data';
            $criteria->group = 'golonganumur_nama';
            $criteria->order = 'golonganumur_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        public function searchJk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printJk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikJk(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, jeniskelamin as data';
            $criteria->group = 'jeniskelamin';
            $criteria->order = 'jeniskelamin';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikStatus(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, statuspasien as data';
            $criteria->group = 'statuspasien';
            $criteria->order = 'statuspasien';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        } 
        
        public function searchPrint(){
            $criteria = $this->criteriaSearch();       
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikPekerjaan(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, pekerjaan_nama as data';
            $criteria->group = 'pekerjaan_nama';
            $criteria->order = 'pekerjaan_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikStatusPerkawinan(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, statusperkawinan as data';
            $criteria->group = 'statusperkawinan';
            $criteria->order = 'statusperkawinan';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
//Berdasarkan Kecamatan

        public function searchKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikKecamatan(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, kecamatan_nama as data';
            $criteria->group = 'kecamatan_nama';
            $criteria->order = 'kecamatan_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikKabupaten(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, kabupaten_nama as data';
            $criteria->group = 'kabupaten_nama';
            $criteria->order = 'kabupaten_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikCaraMasuk(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, caramasuk_nama as data';
            $criteria->group = 'caramasuk_nama';
            $criteria->order = 'caramasuk_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

//Berdasarkan Penjamin
        public function searchDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchGrafikDokterPemeriksa(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, nama_pegawai as data';
            $criteria->group = 'nama_pegawai';
            $criteria->order = 'nama_pegawai';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        public function searchGrafikUnitPelayanan(){
               
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, ruangan_nama as data';
            $criteria->group = 'ruangan_nama';
            $criteria->order = 'ruangan_nama';         
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }


        public function searchPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
         public function searchGrafikPenjamin(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, penjamin_nama as data';
            $criteria->group = 'penjamin_nama';
            $criteria->order = 'penjamin_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        public function searchAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikAlamat(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, alamat_pasien as data';
            $criteria->group = 'alamat_pasien';
            $criteria->order = 'alamat_pasien';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function searchRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        
        public function searchGrafikRujukan(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, nama_perujuk as data';
            $criteria->group = 'nama_perujuk';
            $criteria->order = 'nama_perujuk';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        public function searchPemeriksaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPemeriksaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikPemeriksaan(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, ruangan_nama as data';
            $criteria->group = 'ruangan_nama';
            $criteria->order = 'ruangan_nama';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        public function searchKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikKetPulang(){
            
            $criteria=new CDbCriteria;

            $criteria->select = 'count(pendaftaran_id) as jumlah, statusperiksa as data';
            $criteria->group = 'statusperiksa';
            $criteria->order = 'statusperiksa';

            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
            $criteria->compare('propinsi_id',$this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
            $criteria->compare('kabupaten_id',$this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
            $criteria->compare('kecamatan_id',$this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
            $criteria->compare('kelurahan_id',$this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
            $criteria->compare('instalasi_id',$this->instalasi_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        
        public function getNamaAlias()
        {
            if(!empty($this->nama_bin)){
                return $this->nama_pasien.' Alias '.$this->nama_bin;
            }else{
                return $this->nama_pasien;
            }
            
        }
        
        public function primaryKey() {
            return 'pendaftaran_id';
        }
        
        public function getCaraBayarItems()
        {
            return CarabayarM::model()->findAll('carabayar_aktif=TRUE') ;
        }
        
        public function getPenjaminItems()
        {
            return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE');
        }
        
        public function getPropinsiItems()
        {
            return PropinsiM::model()->findAll('propinsi_aktif=TRUE ORDER BY propinsi_nama');
        }
        
        public function getNamaNamaBIN()
        {
            if ($this->nama_bin == null){
            	return $this->nama_pasien;
            }
            return $this->nama_pasien.' Alias '.$this->nama_bin;
        }
        
        public function getCaraBayarPenjamin()
        {
                return $this->carabayar_nama.' / '.$this->penjamin_nama;
        }
        
        public function getRTRW()
        {
            return $this->rt.' / '.$this->rw;
        }
        
         public function getPekerjaanItems()
        {
            return PekerjaanM::model()->findAll('pekerjaan_aktif=TRUE ORDER BY pekerjaan_nama');
        }
        
         public function getPendidikanItems()
        {
            return PendidikanM::model()->findAll('pendidikan_aktif=TRUE ORDER BY pendidikan_nama');
        }
        
         public function getSukuItems()
        {
            return SukuM::model()->findAll('suku_aktif=TRUE ORDER BY suku_nama');
        }
        
         /**
         * untuk menampilkan daftar nama diagnosa pasien
         * @param type $pendaftaran_id
         * @param type $kelompokdiagnosa_id
         * @return string
         */
        public function getDiagnosaPasien($pendaftaran_id,$kelompokdiagnosa_id){
            $diagnosa = self::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id,'kelompokdiagnosa_id'=>$kelompokdiagnosa_id));
            if(count($diagnosa) > 0){
                $data = "";
                foreach($diagnosa as $val){
                    $data .= "- ".$val->diagnosa_nama."<br>";
                }
                return $data;
            }else{
                return "-";
            }
        }
         /**
         * untuk menampilkan daftar kode diagnosa pasien
         * @param type $pendaftaran_id
         * @param type $kelompokdiagnosa_id
         * @return string
         */
        public function getKodeDiagnosa($pendaftaran_id,$kelompokdiagnosa_id){
            $diagnosa = self::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id,'kelompokdiagnosa_id'=>$kelompokdiagnosa_id));
            if(count($diagnosa) > 0){
                $data = "";
                foreach($diagnosa as $val){
                    $data .= $val->diagnosa_kode."<br>";
                }
                return $data;
            }else{
                return "-";
            }
        }
}
?>