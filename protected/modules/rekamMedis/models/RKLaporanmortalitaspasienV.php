<?php
class RKLaporanmortalitaspasienV extends LaporanmortalitaspasienV
{
        public $kondisipulang1, $kondisipulang2, $jumlahkunjungan;
		public $instalasi_id;
		public $instalasi_nama;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * method untuk criteria
         * @return CActiveDataProvider 
         */
        protected function functionCriteria()
		{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, kondisipulang, pendaftaran_id , CASE WHEN kondisipulang = '".Params::KODE_MENINGGAL_1."' THEN 1 else 0 END AS kondisipulang1, CASE WHEN kondisipulang = '".Params::KODE_MENINGGAL_1."' THEN 0 else 1 END AS kondisipulang2";
                $criteria->group = "golonganumur_nama, tglmorbiditas, diagnosa_nama,umur_0_28hr,umur_28hr_1thn, umur_1_4thn,umur_5_14thn,umur_15_24thn,umur_25_44thn,umur_45_64thn, umur_65, kondisipulang, pendaftaran_id";
                    
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
//		$criteria->compare('LOWER(tglmorbiditas)',strtolower($this->tglmorbiditas),true);
                $criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(kasusdiagnosa)',strtolower($this->kasusdiagnosa),true);
		$criteria->compare('umur_0_28hr',$this->umur_0_28hr);
		$criteria->compare('umur_28hr_1thn',$this->umur_28hr_1thn);
		$criteria->compare('umur_1_4thn',$this->umur_1_4thn);
		$criteria->compare('umur_5_14thn',$this->umur_5_14thn);
		$criteria->compare('umur_15_24thn',$this->umur_15_24thn);
		$criteria->compare('umur_25_44thn',$this->umur_25_44thn);
		$criteria->compare('umur_45_64thn',$this->umur_45_64thn);
		$criteria->compare('umur_65',$this->umur_65);
		$criteria->compare('diagnosa_id',$this->diagnosa_id);
		$criteria->compare('LOWER(diagnosa_kode)',strtolower($this->diagnosa_kode),true);
		$criteria->compare('LOWER(diagnosa_nama)',strtolower($this->diagnosa_nama),true);
		$criteria->compare('LOWER(diagnosa_namalainnya)',strtolower($this->diagnosa_namalainnya),true);
		$criteria->compare('diagnosa_nourut',$this->diagnosa_nourut);
		$criteria->compare('golonganumur_id',$this->golonganumur_id);
		$criteria->compare('LOWER(kondisipulang)',strtolower($this->kondisipulang),true);
		$criteria->compare('LOWER(carakeluar)',strtolower($this->carakeluar),true);

		return $criteria;
	}
        /**
         * method data provider laporan
         * @return CActiveDataProvider 
         */
        
		public function searchLaporanPrint(){
			$criteria = new CDbCriteria;
			$criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
			/*
			$criteria->join = "JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id";
			$criteria->join .= " JOIN instalasi_m ON instalasi_m.instalasi_id = pendaftaran_t.instalasi_id";
			*/
            return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
				'pagination'=>false,
			));
        }
		
		public function searchLaporan(){
			$criteria = new CDbCriteria;
			$criteria->addBetweenCondition('tglmorbiditas',$this->tglAwal,$this->tglAkhir);
			/*
			$criteria->join = "JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id";
			$criteria->join .= " JOIN instalasi_m ON instalasi_m.instalasi_id = pendaftaran_t.instalasi_id";
			*/
            return new CActiveDataProvider($this, array(
				'criteria'=>$criteria
			));
        }
		
		public function searchPrint(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'diagnosa_nama, sum(umur_0_28hr) as umur_0_28hr,sum(umur_28hr_1thn) as umur_28hr_1thn, sum(umur_1_4thn) as umur_1_4thn, sum(umur_5_14thn) as umur_5_14thn, sum(umur_15_24thn) as umur_15_24thn,sum(umur_25_44thn) as umur_25_44thn,sum(umur_45_64thn) as umur_45_64thn, sum(umur_65) as umur_65, count(pendaftaran_id) as jumlah, sum(kondisipulang1) as kondisipulang1 , sum(kondisipulang2) as kondisipulang2, count(pendaftaran_id) as jumlahkunjungan';
            $crit->group = 'diagnosa_nama';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
				'criteria'=>$crit,
				'pagination'=>false,
			));
        }
        /**
         * method untuk data provider table
         * @return CActiveDataProvider 
         */
        public function searchTable(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'diagnosa_nama, sum(umur_0_28hr) as umur_0_28hr,sum(umur_28hr_1thn) as umur_28hr_1thn, sum(umur_1_4thn) as umur_1_4thn, sum(umur_5_14thn) as umur_5_14thn, sum(umur_15_24thn) as umur_15_24thn,sum(umur_25_44thn) as umur_25_44thn,sum(umur_45_64thn) as umur_45_64thn, sum(umur_65) as umur_65, count(pendaftaran_id) as jumlah, sum(kondisipulang1) as kondisipulang1 , sum(kondisipulang2) as kondisipulang2, count(pendaftaran_id) as jumlahkunjungan';
            $crit->group = 'diagnosa_nama';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
        /**
         * method untuk data provider Grafik
         * @return CActiveDataProvider 
         */
        public function searchGrafik(){
            $criteria = $this->functionCriteria();
            $crit = new MyCriteria();
            $crit->select = 'golonganumur_nama as data, kondisipulang as tick, count(pendaftaran_id) as jumlah';
            $crit->group = 'golonganumur_nama,kondisipulang';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
        
        public function primaryKey() {
            return 'pendaftaran_id';
        }

        public function getSumGolUmur($nama_kolom = null, $iddiagnosa){
            
           
            
            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'diagnosa_id, golonganumur_id';
            
            if($nama_kolom == 'BATITA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BATITA);
            }else if($nama_kolom == 'BARU LAHIR'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BARU_LAHIR);
            }else if($nama_kolom == 'BAYI'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BAYI);
            }else if($nama_kolom == 'BALITA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_BALITA);
            }else if($nama_kolom == 'ANAK ANAK'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_ANAK_ANAK);
            }else if($nama_kolom == 'REMAJA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_REMAJA);
            }else if($nama_kolom == 'DEWASA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_DEWASA);
            }else if($nama_kolom == 'ORANG TUA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_ORANG_TUA);
            }else if($nama_kolom == 'MANULA'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition('golonganumur_id = '.PARAMS::GOLONGAN_UMUR_MANULA);
            }

            if(isset($_GET['RKLaporanmortalitaspasienV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAkhir']);
                $criteria->addBetweenCondition('tglmorbiditas',$tglAwal,$tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jmlgolumur) AS jmlgolumur';
            $modTarif = LaporanmortalitaspasienV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jmlgolumur;
            }
             return $totTarif;
        }

        public function getSumKM($nama_kolom = null, $iddiagnosa)
        {

            $format = new CustomFormat();
            $criteria=new CDbCriteria();
            $criteria->group = 'diagnosa_id, kondisipulang';
            
            if($nama_kolom == '< 48 JAM')
			{
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition("kondisipulang ilike '%MENINGGAL < 48 JAM%'");
            }elseif($nama_kolom == '> 48 JAM'){
                $criteria->compare('diagnosa_id',$iddiagnosa);
                $criteria->addCondition("kondisipulang ilike '". PARAMS::MENINGGAL_LEBIH_48JAM . "'");
            }

            if(isset($_GET['RKLaporanmortalitaspasienV']))
			{
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAkhir']);
                $criteria->addBetweenCondition('tglmorbiditas', $tglAwal, $tglAkhir);
            }
            $criteria->select = $criteria->group.', sum(jmlkondisipulang) AS jmlkondisipulang';
            $modJK = LaporanmortalitaspasienV::model()->findAll($criteria);
            $total = 0;
            foreach($modJK as $key=>$jml){
                $total += $jml->jmlkondisipulang;
            }
             return $total;
        }

        public function getSumTotalT($iddiagnosa){

        $format = new CustomFormat();
        $criteria = new CDbCriteria();
        $criteria->group = 'diagnosa_id, kondisipulang';
        $criteria->compare('diagnosa_id',$iddiagnosa);

        if(isset($_GET['RKLaporanmortalitaspasienV'])){
                $tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAwal']);
                $tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAkhir']);
                // $jenisdiet_nama =$_GET['GZLaporanJumlahPorsiV']['jenisdiet_nama'];
                $criteria->addBetweenCondition('tglmorbiditas',$tglAwal,$tglAkhir);
                // $criteria->compare('LOWER(jenisdiet_nama)',strtolower($jenisdiet_nama));
                // $criteria->addInCondition('jenisdiet_id',$this->jenisdiet_id);
            }
            $criteria->select = $criteria->group.',sum(jmlkondisipulang) AS jmlkondisipulang';
            $modTarif = LaporanmortalitaspasienV::model()->findAll($criteria);
            $totTarif = 0;
            foreach($modTarif as $key=>$tarif){
                $totTarif += $tarif->jmlkondisipulang;
            }
             return $totTarif;
     }

}