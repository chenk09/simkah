<?php

class RMPasienmorbiditasT extends PasienmorbiditasT{
    public $tglAwal, $tglAkhir;
    public $diagnosa_nama, $diagnosa_kode;
	public $instalasi_id, $jumlahTampil, $jumlah;
	
	public function searchLaporan()
    {
		$criteria = new CDbCriteria;
		$group = 'diagnosa.diagnosa_id, diagnosa.diagnosa_nama, diagnosa.diagnosa_kode';
		
		if(strlen($this->instalasi_id) > 0 && $this->instalasi_id > 0)
		{
			$group .= ", pendaftaran_t.instalasi_id";
			$criteria->addCondition('pendaftaran_t.instalasi_id = ' . $this->instalasi_id);
		}
		
		if(strlen($this->ruangan_id) > 0 && $this->ruangan_id > 0)
		{
			$group .= ", pendaftaran_t.ruangan_id";
			$criteria->addCondition('pendaftaran_t.ruangan_id = ' . $this->ruangan_id);
		}
		$criteria->select = 'count(diagnosa.diagnosa_id) as jumlah, ' . $group;
		$criteria->group = $group;
		$criteria->addCondition('tglmorbiditas BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->join = 'INNER JOIN diagnosa_m diagnosa ON diagnosa.diagnosa_id = t.diagnosa_id';
		$criteria->join .= ' INNER JOIN pendaftaran_t ON t.pendaftaran_id = pendaftaran_t.pendaftaran_id';
		$criteria->order = 'jumlah DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => array('pageSize' => $this->jumlahTampil,),
			'totalItemCount' => $this->jumlahTampil,
		));
    }
	
    public function getNamaModel()
    {
        return __CLASS__;
    }	
}