<?php
class RMLaporankunjunganriV extends LaporankunjunganriV
{
        
        public $pilihanx;
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function criteriaSearch()
        {
            $criteria = new CDbCriteria;
            
            $criteria->group = 'statuspasien, pendaftaran_id,tgl_pendaftaran,no_pendaftaran,tgladmisi,tglpulang,no_rekam_medik,nama_pasien,jeniskelamin,golonganumur_nama,agama,statusperkawinan,pekerjaan_nama,alamat_pasien,kecamatan_nama,kabupaten_nama,caramasuk_nama,kunjungan,nama_pj,nama_perujuk,jeniskasuspenyakit_nama,kelaspelayanan_nama,carabayar_nama,penjamin_nama,ruanganasal_nama,ruangan_nama,nama_pegawai,statusperiksa,statusmasuk,kondisipulang,jumlahlamarawat,tglpindahkamar,nokamar_akhir,jumlahlamarawatruangpindahan';
            $criteria->select = $criteria->group;
            
            $criteria->addBetweenCondition('DATE(tgladmisi)', $this->tglAwal, $this->tglAkhir);
            $criteria->compare('pasien_id', $this->pasien_id);
            $criteria->compare('LOWER(jenisidentitas)', strtolower($this->jenisidentitas), true);
            $criteria->compare('LOWER(no_identitas_pasien)', strtolower($this->no_identitas_pasien), true);
            $criteria->compare('LOWER(namadepan)', strtolower($this->namadepan), true);
            $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
            $criteria->compare('LOWER(nama_bin)', strtolower($this->nama_bin), true);
            $criteria->compare('LOWER(jeniskelamin)', strtolower($this->jeniskelamin), true);
            $criteria->compare('LOWER(tempat_lahir)', strtolower($this->tempat_lahir), true);
            $criteria->compare('LOWER(tanggal_lahir)', strtolower($this->tanggal_lahir), true);
            $criteria->compare('LOWER(alamat_pasien)', strtolower($this->alamat_pasien), true);
            $criteria->compare('rt', $this->rt);
            $criteria->compare('rw', $this->rw);
            $criteria->compare('LOWER(agama)', strtolower($this->agama), true);
            $criteria->compare('LOWER(golongandarah)', strtolower($this->golongandarah), true);
            $criteria->compare('LOWER(photopasien)', strtolower($this->photopasien), true);
            $criteria->compare('LOWER(alamatemail)', strtolower($this->alamatemail), true);
            $criteria->compare('LOWER(statusrekammedis)', strtolower($this->statusrekammedis), true);
            $criteria->compare('LOWER(statusperkawinan)', strtolower($this->statusperkawinan), true);
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
            $criteria->compare('LOWER(tgl_rekam_medik)', strtolower($this->tgl_rekam_medik), true);
            $criteria->compare('propinsi_id', $this->propinsi_id);
            $criteria->compare('LOWER(propinsi_nama)', strtolower($this->propinsi_nama), true);
            $criteria->compare('kabupaten_id', $this->kabupaten_id);
            $criteria->compare('LOWER(kabupaten_nama)', strtolower($this->kabupaten_nama), true);
            $criteria->compare('kelurahan_id', $this->kelurahan_id);
            $criteria->compare('LOWER(kelurahan_nama)', strtolower($this->kelurahan_nama), true);
            $criteria->compare('kecamatan_id', $this->kecamatan_id);
            $criteria->compare('LOWER(kecamatan_nama)', strtolower($this->kecamatan_nama), true);
            $criteria->compare('pendaftaran_id', $this->pendaftaran_id);
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
            $criteria->compare('LOWER(no_urutantri)', strtolower($this->no_urutantri), true);
            $criteria->compare('LOWER(transportasi)', strtolower($this->transportasi), true);
            $criteria->compare('LOWER(keadaanmasuk)', strtolower($this->keadaanmasuk), true);
            $criteria->compare('LOWER(statusperiksa)', strtolower($this->statusperiksa), true);
            $criteria->compare('LOWER(statuspasien)', strtolower($this->statuspasien), true);
            $criteria->compare('LOWER(kunjungan)', strtolower($this->kunjungan), true);
            $criteria->compare('alihstatus', $this->alihstatus);
            $criteria->compare('byphone', $this->byphone);
            $criteria->compare('kunjunganrumah', $this->kunjunganrumah);
            $criteria->compare('LOWER(statusmasuk)', strtolower($this->statusmasuk), true);
            $criteria->compare('LOWER(umur)', strtolower($this->umur), true);
            $criteria->compare('LOWER(no_asuransi)', strtolower($this->no_asuransi), true);
            $criteria->compare('LOWER(namapemilik_asuransi)', strtolower($this->namapemilik_asuransi), true);
            $criteria->compare('LOWER(nopokokperusahaan)', strtolower($this->nopokokperusahaan), true);
            $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
            $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
            $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);
            $criteria->compare('carabayar_id', $this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
            $criteria->compare('penjamin_id', $this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)', strtolower($this->penjamin_nama), true);
            $criteria->compare('caramasuk_id', $this->caramasuk_id);
            $criteria->compare('LOWER(caramasuk_nama)', strtolower($this->caramasuk_nama), true);
            $criteria->compare('shift_id', $this->shift_id);
            $criteria->compare('LOWER(no_rujukan)', strtolower($this->no_rujukan), true);
            $criteria->compare('LOWER(nama_perujuk)', strtolower($this->nama_perujuk), true);
            $criteria->compare('LOWER(tanggal_rujukan)', strtolower($this->tanggal_rujukan), true);
            $criteria->compare('LOWER(diagnosa_rujukan)', strtolower($this->diagnosa_rujukan), true);
            $criteria->compare('asalrujukan_id', $this->asalrujukan_id);
            $criteria->compare('LOWER(asalrujukan_nama)', strtolower($this->asalrujukan_nama), true);
            $criteria->compare('ruangan_id', $this->ruangan_id);
            $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
            $criteria->compare('instalasi_id', $this->instalasi_id);
            $criteria->compare('LOWER(instalasi_nama)', strtolower($this->instalasi_nama), true);
            $criteria->compare('jeniskasuspenyakit_id', $this->jeniskasuspenyakit_id);
            $criteria->compare('LOWER(jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
            $criteria->compare('kelaspelayanan_id', $this->kelaspelayanan_id);
            $criteria->compare('LOWER(kelaspelayanan_nama)', strtolower($this->kelaspelayanan_nama), true);
            return $criteria;
        }
        
        public function searchTableLaporan() {
           $criteria = $this->criteriaSearch();
            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
        }
        
        public function searchPrintTableLaporan() {
            $criteria = $this->criteriaSearch();
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                        'pagination'=>false,
                    ));
        }
        
        public function searchUmur(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function printUmur() {
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama'; 
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                        'pagination'=>false,
                    ));
        }
        
        public function searchJk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function printJk() {
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin'; 
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                        'pagination'=>false,
                    ));
        }
        
         public function searchStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function printStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
        
        public function printAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }    
        
        public function searchPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        } 
        
        public function searchKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        } 
        
        public function searchRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }      
        
        public function searchRM(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printRM(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pasien';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchKamarRuangan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKamarRuangan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        } 
        
        public function searchPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }    
        
        public function searchDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            $criteria->limit=-1; 
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        /**
         * untuk menampilkan daftar nama diagnosa pasien
         * @param type $pendaftaran_id
         * @param type $kelompokdiagnosa_id
         * @return string
         */
        public function getDiagnosaPasien($pendaftaran_id,$kelompokdiagnosa_id){
            $diagnosa = self::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id,'kelompokdiagnosa_id'=>$kelompokdiagnosa_id));
            if(count($diagnosa) > 0){
                $data = "";
                foreach($diagnosa as $val){
                    $data .= "- ".$val->diagnosa_nama."<br>";
                }
                return $data;
            }else{
                return "-";
            }
        }
        
        /**
         * untuk menampilkan daftar kode diagnosa pasien
         * @param type $pendaftaran_id
         * @param type $kelompokdiagnosa_id
         * @return string
         */
        public function getKodeDiagnosa($pendaftaran_id,$kelompokdiagnosa_id){
            $diagnosa = self::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id,'kelompokdiagnosa_id'=>$kelompokdiagnosa_id));
            if(count($diagnosa) > 0){
                $data = "";
                foreach($diagnosa as $val){
                    $data .= $val->diagnosa_kode."<br>";
                }
                return $data;
            }else{
                return "-";
            }
        }

		public function getNamaModel()
		{
			return __CLASS__;
		}
}
?>