<?php

class RKLaporanpasienpulangRJV extends LaporanpasienpulangV{

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTable() {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();
        $criteria->order = 'instalasi_nama, subspesialistik,tglpasienpulang';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchGrafik() {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();

        $criteria->select = 'count(pendaftaran_id) as jumlah';

        if (!empty($this->ruangan_id)) {
            $criteria->select .= ', ruangan_nama as tick';
            $criteria->group .= 'ruangan_nama';
        } else if (!empty($this->instalasi_id)) {
            $criteria->select .= ', ruangan_nama as tick';
            $criteria->group .= 'ruangan_nama';
        } else {
            $criteria->select .= ', instalasi_nama as tick';
            $criteria->group .= 'instalasi_nama';
        }

        if ($this->pilihanx == 'pengunjung') {
            $criteria->select .= ', statuspasien as data';
            $criteria->group .= ', statuspasien';
        } else if ($this->pilihanx == 'kunjungan') {
            $criteria->select .= ', kunjungan as data';
            $criteria->group .= ', kunjungan';
        } else {
            if (!empty($this->ruangan_id)) {
                $criteria->select = 'count(pendaftaran_id) as jumlah, ruangan_nama as data';
                $criteria->group = 'ruangan_nama';
            }
            else if (!empty($this->instalasi_id)) {
                $criteria->select = 'count(pendaftaran_id) as jumlah, instalasi_nama as data';
                $criteria->group = 'instalasi_nama';
            }
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint()
    {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();
        $criteria->order = 'instalasi_nama, subspesialistik,tglpasienpulang';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'pagination' => false,
                ));
    }


    protected function functionCriteria() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

       $criteria=new CDbCriteria;
       


        $criteria->select = 'tanggal_lahir,jk_pasien,alamat_pasien,no_telepon_pasien,tgl_pendaftaran, kamarperawatan, caramasuk_nama,
                            count(pendaftaran_id) as pendaftaran_id, no_pendaftaran, nama_pasien, pasien_id, no_telepon_pasien, 
                            pasienpulang_id, keterangankeluar, carakeluar, no_telepon_pasien, kondisipulang, carabayar_nama,
                            penjamin_nama, kelaspelayanan_nama, nama_pj, dokterpenanggungjawab_gelardepan, dokterpenanggungjawab_nama,
                            
                            dokterspesialistik_gelarbelakang, tglpasienpulang, instalasi_nama, subspesialistik, no_rekam_medik, 
                            jeniskasuspenyakit_nama
                            ';
        $criteria->group = 'tanggal_lahir,jk_pasien,alamat_pasien,no_telepon_pasien,tgl_pendaftaran, kamarperawatan, caramasuk_nama,pendaftaran_id, no_pendaftaran, nama_pasien, pasien_id, no_telepon_pasien, 
                            pasienpulang_id, keterangankeluar, carakeluar, no_telepon_pasien, kondisipulang, carabayar_nama,
                            penjamin_nama, kelaspelayanan_nama, nama_pj, dokterpenanggungjawab_gelardepan, dokterpenanggungjawab_nama,
                            
                            dokterspesialistik_gelarbelakang, tglpasienpulang, instalasi_nama, subspesialistik, no_rekam_medik, 
                            jeniskasuspenyakit_nama
                            ';

        // $pendaftaran_id = 'pendaftaran_id';
        // $dokterspesialistik_nama = 'dokterspesialistik_nama';

        $criteria->addBetweenCondition('tglpasienpulang', $this->tglAwal, $this->tglAkhir);
        // $criteria->addCondition("carakeluar != 'RAWAT INAP'");
        // $criteria->addCondition("carakeluar != 'DIRAWAT INAP'");

        $criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
        $criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
        $criteria->compare('nama_pasien',$this->nama_pasien,true);
        $criteria->compare('pasien_id',$this->pasien_id);
        $criteria->compare('no_telepon_pasien',$this->no_telepon_pasien,true);
        $criteria->compare('pasienpulang_id',$this->pasienpulang_id);
        $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
        $criteria->compare('tglpasienpulang',$this->tglpasienpulang,true);
        $criteria->compare('keterangankeluar',$this->keterangankeluar,true);
        $criteria->compare('kondisipulang',$this->kondisipulang,true);
        $criteria->compare('carakeluar',$this->carakeluar,true);
        $criteria->compare('dokterpemeriksa',$this->dokterpemeriksa);
        $criteria->compare('gelardepan',$this->gelardepan,true);
        $criteria->compare('nama_pegawai',$this->nama_pegawai,true);
        $criteria->compare('gelarbelakang_nama',$this->gelarbelakang_nama,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('LOWER(instalasi_nama)', strtolower($this->instalasi_nama), true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('subspesialistik',$this->subspesialistik,true);
        $criteria->compare('carabayar_id',$this->carabayar_id);
        $criteria->compare('carabayar_nama', strtolower($this->carabayar_nama), true);
        $criteria->compare('penjamin_id',$this->penjamin_id);
        $criteria->compare('penjamin_nama',$this->penjamin_nama,true);
        // $criteria->limit = 10;
        if (isset($_REQUEST['RKLaporanpasienpulangRJV'])) {
            if (count('pendaftaran_id') > 1){
            $criteria->addCondition('dokterspesialistik_nama is NOT NULL');

        }
        // echo(COUNT('pendaftaran_id')); exit();
        }
        
        

        return $criteria;
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    public function getCaraBayarItems()
    {
        return CarabayarM::model()->findAll('carabayar_aktif=TRUE') ;
    }
    
    public function getPenjaminItems()
    {
        return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE');
    } 

    public static function berdasarkanStatus() {
        $status = array(
            'pengunjung' => 'Berdasarkan Pengunjung',
            'kunjungan' => 'Berdasarkan Kunjungan',
        );
        return $status;
    }
    
    public function searchDashboard() {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();

        $criteria->select = 'count(pendaftaran_id) as jumlah';

        if (!empty($this->ruangan_id)) {
            $criteria->select .= ', ruangan_nama as tick';
            $criteria->group .= 'ruangan_nama';
        } else if (!empty($this->instalasi_id)) {
            $criteria->select .= ', ruangan_nama as tick';
            $criteria->group .= 'ruangan_nama';
        } else {
            $criteria->select .= ', instalasi_nama as tick';
            $criteria->group .= 'instalasi_nama';
        }

        if ($this->pilihanx == 'pengunjung') {
            $criteria->select .= ', statuspasien as data';
            $criteria->group .= ', statuspasien';
        } else if ($this->pilihanx == 'kunjungan') {
            $criteria->select .= ', kunjungan as data';
            $criteria->group .= ', kunjungan';
        } else {
            if (!empty($this->ruangan_id)) {
                $criteria->select = 'count(pendaftaran_id) as jumlah, kecamatan_nama as data';
                $criteria->group = 'kecamatan_nama';
            }
            else if (!empty($this->instalasi_id)) {
                $criteria->select = 'count(pendaftaran_id) as jumlah, instalasi_nama as data';
                $criteria->group = 'instalasi_nama';
            }
        }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}