
<?php

class PeminjamanrmTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'peminjaman', 'printPeminjaman'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','informasi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKPeminjamanrmT;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKPeminjamanrmT']))
		{
			$model->attributes=$_POST['RKPeminjamanrmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                
				$this->redirect(array('view','id'=>$model->peminjamanrm_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKPeminjamanrmT']))
		{
			$model->attributes=$_POST['RKPeminjamanrmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->peminjamanrm_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('RKPeminjamanrmT');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKPeminjamanrmT;
                $format = new CustomFormat();
                $modDokumenPasienLama = new RKDokumenpasienrmlamaV('search');

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                $model->tglpeminjamanrm = date('Y-m-d H:i:s');
                $modDokumenPasienLama->tgl_rekam_medik = date('Y-m-d H:i:s');
                $modDokumenPasienLama->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
                $model->namapeminjam = Yii::app()->user->name;
                if(isset($_GET['RKDokumenpasienrmlamaV'])){
                    $modDokumenPasienLama->attributes=$_GET['RKDokumenpasienrmlamaV'];  
                    $modDokumenPasienLama->tgl_rekam_medik = $format->formatDateTimeMediumForDB($_GET['RKDokumenpasienrmlamaV']['tgl_rekam_medik']);
                    $modDokumenPasienLama->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($_GET['RKDokumenpasienrmlamaV']['tgl_rekam_medik_akhir']);
                }
                    
                
		if(isset($_POST['RKPeminjamanrmT']))
		{
                    $model->attributes=$_POST['RKPeminjamanrmT'];                   
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        $jumlah = count($_POST['cekList']);
                        for($i = 0; $i < $jumlah; $i++){
                            if ($_POST['cekList'][$i] == 1){
                                $models = new RKPeminjamanrmT();
                                $models->attributes = $model->attributes;
                                //$models->warnadokrm_id = WarnadokrmM::model()->findByAttributes(array('warnadokrm_kodewarna'=> str_replace('#','',strtolower($_POST['Dokumen']['warnadokrm_id'][$i]))))->warnadokrm_id;
                                $models->pasien_id = $_POST['Dokumen']['pasien_id'][$i];
                                $models->pendaftaran_id = $_POST['Dokumen']['pendaftaran_id'][$i];
                                $models->dokrekammedis_id = $_POST['Dokumen']['dokrekammedis_id'][$i];
                                $models->ruangan_id = $_POST['Dokumen']['ruangan_id'][$i];
                                $models->nourut_pinjam = Generator::noUrutPinjamRM();
                                if (!$models->save()){
                                    $success = false;
                                }
                                else{
                                    PendaftaranT::model()->updateByPK($models->pendaftaran_id, array('peminjamanrm_id'=>$models->peminjamanrm_id, 'pengiriman_id'=>null));
                                    if (isset($model->printArray)){
                                        $model->printArray = explode(',',$model->printArray);
                                        $this->updatePrint($model->printArray);
                                    }
                                    
                                    //PendaftaranT::model()->updateByPK($models->pasien_id, array('peminjamanrm_id'=>$models->peminjamanrm_id, 'pengiriman_id'=>null));
                                }
                            }
                        }
                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Peminjaman Dokumen Rekam Medis berhasil disimpan");
                            $this->refresh();
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");

                        }
                    }
                    catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                    }
                }
                    
		$this->render('index',array(
			'model'=>$model,
                        'modDokumenPasienLama'=>$modDokumenPasienLama,
		));
	}
        
        public function actionPeminjaman($id = null){
            if (isset($id)){
                $model = RKPeminjamanrmT::model()->findByPk($id);
                $modRekamMedis = RKDokrekammedisM::model()->with('pasien')->findByPk($model->dokrekammedis_id);
                
                $model->no_rekam_medik = $modRekamMedis->pasien->no_rekam_medik;
                $model->nama_pasien = $modRekamMedis->pasien->nama_pasien;
                $model->jenis_kelamin = $modRekamMedis->pasien->jeniskelamin;
                $model->tanggal_lahir = $modRekamMedis->pasien->tanggal_lahir;
            }else{
                $model = new RKPeminjamanrmT;
                $model->namapeminjam = Yii::app()->user->name;
                $model->nourut_pinjam = Generator::noUrutPinjamRM();
                $model->tglpeminjamanrm = date('Y-m-d H:i:s');
            }
            
            
            if (isset($_POST['RKPeminjamanrmT'])){
                $model->attributes=$_POST['RKPeminjamanrmT'];
                if($model->save()){
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        PendaftaranT::model()->updateByPk($model->pendaftaran_id, array('pengirimanrm_id'=>null, 'peminjamanrm_id'=>$model->peminjamanrm_id));
                        $this->redirect(array('peminjaman','id'=>$model->peminjamanrm_id));
                        //$this->refresh();
                }
            }
            
            $this->render('peminjaman', array(
                'model'=>$model
                ));
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKPeminjamanrmT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RKPeminjamanrmT']))
			$model->attributes=$_GET['RKPeminjamanrmT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
	public function actionInformasi()
	{
                            //$model = new RJPendaftaranT('searchDaftarPasien');
                                
		$model=new InformasipeminjamanrmV('search');
                                $model->unsetAttributes();
                                $model->tglAwal = date('Y-m-d 00:00:00');
                                $model->tglAkhir = date('Y-m-d H:i:s');
                                if(isset($_GET['InformasipeminjamanrmV'])){
                                    $model->attributes = $_GET['InformasipeminjamanrmV'];
                                    $format = new CustomFormat();
                                    $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['InformasipeminjamanrmV']['tglAwal']);
                                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['InformasipeminjamanrmV']['tglAkhir']);
                                }
		$this->render('informasi',array(
			'model'=>$model,
		));
                }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RKPeminjamanrmT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rkpeminjamanrm-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new RKDokumenpasienrmlamaV();
            $model->attributes=$_REQUEST['RKDokumenpasienrmlamaV'];      
            $model->pendaftaran_id = explode(',',$_REQUEST['RKDokumenpasienrmlamaV']['printArray']);
            
            $judulLaporan='Data Dokumen Rekam Medis';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
            
            $this->updatePrint($model->pendaftaran_id);
            
        }
        
        public function actionPrintPeminjaman($id)
	{   
                $this->layout='//layouts/printKartu';
                $modPeminjaman = RKPeminjamanrmT::model()->with('dokrekammedis')->findByPk($id);
                RKPeminjamanrmT::model()->updateByPk($id, array('printpeminjaman'=>true));
                $model = PasienM::model()->findByPk($modPeminjaman->dokrekammedis->pasien_id);
		$this->render('printKartu',array('modPasien'=>$model, 'modPinjam'=>$modPeminjaman));
	}
        
        protected function updatePrint($data = null){
            if (isset($data)){
                $jumlah = count($data);
                for($i=0;$i<$jumlah;$i++){
                    RKPeminjamanrmT::model()->updateAll(array('printpeminjaman'=>TRUE), 'pendaftaran_id = '.$data[$i]);
                }
            }
        }
}
