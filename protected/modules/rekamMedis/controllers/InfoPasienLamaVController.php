<?php

class InfoPasienLamaVController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','UbahRujukan','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','UbahCaraBayar','UbahPenanggungJawab','UbahPasien'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RMInfoPasienLamaV;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RMInfoPasienLamaV']))
		{
			$model->attributes=$_POST['RMInfoPasienLamaV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pendaftaran_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        public function actionUbahCaraBayar()
        {
            if (Yii::app()->getRequest()->getIsAjaxRequest()) 
             { 
                $carabayar_id=$_POST['carabayar_id'];
                $pendaftaran_id=$_POST['pendaftaran_id']; 
                $penjamin_id = $_POST['penjamin_id'];
                $alasan = $_POST['alasan'];
                
                $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                $modUbahCaraBayar = new UbahcarabayarR;
                $modUbahCaraBayar->pendaftaran_id = $pendaftaran_id;
                $modUbahCaraBayar->carabayar_id = $carabayar_id;
                $modUbahCaraBayar->penjamin_id = $penjamin_id;
                $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d H:i:s');
                $modUbahCaraBayar->alasanperubahan = $alasan;
                
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $updatePendaftaran = PendaftaranT::model()->updateByPk($pendaftaran_id, array('carabayar_id'=>$carabayar_id,'penjamin_id'=>$penjamin_id));
                    if($modUbahCaraBayar->save() && $updatePendaftaran){
                        $transaction->commit();
                        $data['message']='Ubah Cara Bayar berhasil dilakukan.';
                        $data['success']=true;
                    } else {
                        $transaction->rollback();
                        $data['message']='Gagal Merubah Cara Bayar!';
                        $data['success']=false;
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    $data['message']='Gagal Merubah Cara Bayar!';
                        $data['success']=false;
                }

                
              echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionUbahPasien($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model = $this->loadModel2($id);
                $temLogo=$model->photopasien;
                if(isset($_POST['PPPasienM'])) {                   
                    $random=rand(0000000,9999999);
                    $format = new CustomFormat();
                    $model->attributes = $_POST['PPPasienM'];
                    $model->kelompokumur_id = Generator::kelompokUmur($model->tanggal_lahir);
                    $model->photopasien = CUploadedFile::getInstance($model, 'photopasien');
                    $gambar=$model->photopasien;
                    if(!empty($model->photopasien)) { //if user input the photo of patient
                        $model->photopasien =$random.$model->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$model->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($model->save()) {
                            if(!empty($temLogo)) { 
                               if(file_exists(Params::pathPasienDirectory().$temLogo))
                                    unlink(Params::pathPasienDirectory().$temLogo);
                               if(file_exists(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo))
                                    unlink(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo);
                            }
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                 ->resize(200,200)
                                 ->save($fullThumbSource);

                            $model->tgl_rekam_medik  = $format->formatDateTimeMediumForDB($_POST['PPPasienM']['tgl_rekam_medik']);
                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin'));
                          } else {
                               Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                          }
                    } else { //if user not input the photo
                       $model->photopasien=$temLogo;
                       if($model->save()) {
                            $model->tgl_rekam_medik  = $format->formatDateTimeMediumForDB($_POST['PPPasienM']['tgl_rekam_medik']);
                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('admin'));
                       }
                    }

                }
		$this->render('ubahPasien',array('model'=>$model));
	}
        public function actionUbahRujukan($id)
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $modRujukan=RMRujukanT::model()->findByAttributes(array('asalrujukan_id'=>$id));

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['RMRujukanT']))
        {
            $modRujukan->attributes=$_POST['RMRujukanT'];
            if($modRujukan->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('admin','id'=>$modRujukan->rujukan_id));
                        }
        }

        $this->render('ubahRujukan',array(
            'modRujukan'=>$modRujukan,
        ));
    }

	/**
         * method to change penanggung jawab if penanggungjawab_id is not null if null this method will create new row in table penanggungjawab_m
         * digunakan pada :
         * 1. Modul Rekam Medis -> informasi pasien baru -> ubah penanggung jawab
         * @param type $id penanggungjawab_id default null
         * @throws CHttpException if user doesn't have enough access to create or update
         */
	public function actionUbahPenanggungJawab($id=null,$idPendaftaran=null)
	{              
            if (!empty($id)) {
                if (!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) {
                    throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
                }
                /*
                 * GET penanggungjawab_id berdasarkan no_rekam_medik dari tabel pendaftaran_t
                 */
                $model = PenanggungjawabM::model()->findByPk($id);
            } else if (isset($idPendaftaran)){
                if (!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) {
                    throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
                }
                $model = new PenanggungjawabM();
            }
            if (isset($_POST['PenanggungjawabM'])) {
                $model->attributes = $_POST['PenanggungjawabM'];
                if (!isset($id) && (isset($idPendaftaran))){
                    if ($model->validate()){
                        if ($model->save()){
                            $updatePendaftaran = PendaftaranT::model()->updateByPk($idPendaftaran, array('penanggungjawab_id'=>$model->penanggungjawab_id));
                            if ($updatePendaftaran){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->redirect(array('admin'));
                            }
                        }
                    }
                }else{
                    if ($model->save()) {
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        $this->redirect(array('admin'));
                    }
                }
            }
            $this->render('ubahPenanggungJawab',array('model'=>$model));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RMInfoPasienLamaV']))
		{
			$model->attributes=$_POST['RMInfoPasienLamaV'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pendaftaran_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RMInfoPasienLamaV');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
        public function actionAdmin()
	{
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}	
            $format = new CustomFormat();
            $model = new RMInfoPasienLamaV;
            $model->tglAwal=date("Y-m-d").' 00:00:00';
            $model->tglAkhir=date("Y-m-d H:i:s");
                if(isset($_REQUEST['RMInfoPasienLamaV']))
                {
                    $model->attributes=$_REQUEST['RMInfoPasienLamaV'];
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoPasienLamaV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMInfoPasienLamaV']['tglAkhir']);
                }
              
//                $modPPInfoKunjunganRJV->tglAwal = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modPPInfoKunjunganRJV->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//                $modPPInfoKunjunganRJV->tglAkhir = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modPPInfoKunjunganRJV->tglAkhir, 'yyyy-MM-dd hh:mm:ss'));  

            $this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RMInfoPasienLamaV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	public function loadModel2($id)
	{
		$model=  PPPasienM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rminfo-pasien-baru-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new RMInfoPasienLamaV;
            $model->attributes=$_REQUEST['RMInfoPasienLamaV'];
            $judulLaporan='Data RMInfoPasienLamaV';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
