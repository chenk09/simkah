<?php

class LaporanController extends SBaseController {
    
    public $pathViewPP = 'pendaftaranPenjadwalan.views.laporan.';
    public $pathViewRj = 'rawatJalan.views.laporan.';
    
    
// -- Laporan Kunjungan RJ --//
    // -- VIEW LAPORAN -- //
    public function actionLaporanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/adminRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKunjunganUmurRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Umur";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/umurRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKunjunganJkRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Jenis Kelamin";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/jkRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Kedatangan Lama / Baru";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/statusRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAgamaKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Agama";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/agamaRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanPekerjaanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Pekerjaan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/pekerjaanRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusPerkawinanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Status Perkawinan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/statusPerkawinanRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAlamatKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Alamat";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/alamatRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKecamatanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Kecamatan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/kecamatanRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKabKotaKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Kabupaten / Kota";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/kabupatenRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanCaraMasukKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Cara Masuk";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/caraMasukRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanRujukanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Rujukan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/rujukanRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPemeriksaanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Pemeriksaan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/pemeriksaanRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKetPulangKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Keterangan Pulang";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/ketPulangRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPenjaminKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Penjamin";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/penjaminRJ', array(
            'model' => $model,
        ));
    }
    public function actionLaporanDokterPemeriksaKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Dokter Pemeriksa";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/dokterPemeriksaRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanUnitPelayananKunjunganRJ() {
        $model = new PPRuanganM();
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->instalasi_id = 2;
		
        if (isset($_GET['PPRuanganM']))
		{
            $format = new CustomFormat();
            $model->attributes = $_GET['PPRuanganM'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/unitPelayananRJ', array(
            'model' => $model,
        ));
    }
    // -- END VIEW LAPORAN --//
    
    // -- PRINT LAPORAN --//
    public function actionPrintKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'.rawatJalan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintUmurKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y 23:59:59');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Umur';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Umur';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal =  $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'rawatJalan/_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAgamaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Agama';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Agama';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAlamatKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Alamat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Alamat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAlamt';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintJkKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Jenis Kelamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Jenis Kelamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kedatangan Lama / Baru';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusPerkawinanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Status Perkawinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Status Perkawinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintRujukanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Rujukan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Rujukan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintCaraMasukKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Cara Masuk';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Cara Masuk';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKetPulangKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Keterangan Pulang';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Keterangan Pulang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKabKotaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kabupaten / Kota';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kabupaten / Kota';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKecamatanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kecamatan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kecamatan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPenjaminKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Penjamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Penjamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPemeriksaanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pemeriksaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pemeriksaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintDokterPemeriksaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionPrintDokterPemeriksaKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintUnitPelayananKunjunganRJ() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Unit Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Unit Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printUnitPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
   
    // -- END PRINT LAPORAN -- //
    
    // -- GRAFIK LAPORAN -- //
    public function actionFrameGrafikRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAgamaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    // -- END GRAFIK LAPORAN -- //
// -- End Laporan Kunjungan RJ --//    

// -- Laporan Kunjungan RD -- //
    // -- VIEW LAPORAN --//
    public function actionLaporanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat";
        $model = new PPLaporankunjunganrdV('searchTableLaporan');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if(isset($_GET['PPLaporankunjunganrdV']))
		{
            $format = new CustomFormat();
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }
		
        $this->render($this->pathViewPP.'rawatDarurat/adminRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKunjunganUmurRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Umur";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/umurRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKunjunganJkRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Jenis Kelamin";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/jkRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kedatangan Lama / Baru";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/statusRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAgamaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Agama";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/agamaRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanPekerjaanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Pekerjaan";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/pekerjaanRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusPerkawinanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Status Perkawinan";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/statusPerkawinanRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAlamatKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Alamat";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/alamatRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKecamatanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kecamatan";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/kecamatanRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKabKotaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kabupaten / Kota";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/kabupatenRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanCaraMasukKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Cara Masuk";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/caraMasukRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanRujukanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Rujukan";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/rujukanRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPemeriksaanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Pemeriksaan";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/pemeriksaanRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKetPulangKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Keterangan Pulang";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/ketPulangRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPenjaminKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Penjamin";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/penjaminRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanDokterPemeriksaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Dokter Pemeriksa";
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrdV'])) {
            $model->attributes = $_GET['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/dokterPemeriksaRD', array(
            'model' => $model,
        ));
    }
    public function actionLaporanUnitPelayananKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Unit Pelayanan";
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/unitPelayananRD', array(
            'model' => $model,
        ));
    }
    // -- END VIEW LAPORAN --//
    
    // -- PRINT LAPORAN -- //
    public function actionPrintKunjunganRD() {
        $model = new PPLaporankunjunganrdV('searchTableLaporanPrint');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printRD';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintUmurKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Umur';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Umur';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAgamaKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Agama';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Agama';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAlamatKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Alamat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Alamat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAlamat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintJkKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Jenis Kelamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Jenis Kelamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kedatangan Lama / Baru';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusPerkawinanKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Status Perkawinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Status Perkawinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintRujukanKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Rujukan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Rujukan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintCaraMasukKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Cara Masuk';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Cara Masuk';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKetPulangKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Keterangan Pulang';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Keterangan Pulang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKabKotaKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kabupaten / Kota';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kabupaten / Kota';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKecamatanKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kecamatan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kecamatan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPenjaminKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Penjamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Penjamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPemeriksaanKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pemeriksaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pemeriksaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintDokterPemeriksaKunjunganRD() {
        $model = new PPLaporankunjunganrdV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Dokter Pemeriksa';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrdV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrdV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrdV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintUnitPelayananKunjunganRD() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Unit Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Unit Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printUnitPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    // -- END PRINT LAPORAN -- //
    
    // --GRAFIK LAPORAN -- //
    public function actionFrameGrafikRD() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
     public function actionFrameGrafikAgamaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    
    // -- END GRAFIK LAPORAN -- //
// -- End Laporan Kunjungan RD -- //
    
    
// -- Laporan Kunjungan RI -- //
    // -- VIEW LAPORAN --//
    public function actionLaporanKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/adminRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAlasanPulangKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Alasan Pulang";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/alasanPulangRI', array(
            'model' => $model,
        ));
    }
    
     public function actionLaporanKunjunganUmurRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Umur";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/umurRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKunjunganJkRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Jenis Kelamin";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/jkRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Kedatangan Lama / Baru";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/statusRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAgamaKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Agama";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/agamaRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanPekerjaanKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Pekerjaan";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/pekerjaanRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusPerkawinanKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Status Perkawinan";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/statusPerkawinanRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAlamatKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Alamat";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/alamatRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanKecamatanKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Kecamatan";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/kecamatanRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKabKotaKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Kabupaten / Kota";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/kabupatenRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanCaraMasukKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Cara Masuk";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/caraMasukRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanRujukanKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Rujukan";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/rujukanRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPemeriksaanKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Pemeriksaan";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/pemeriksaanRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKetPulangKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Keterangan Pulang";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/ketPulangRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanPenjaminKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Penjamin";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/penjaminRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanDokterPemeriksaKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Dokter Pemeriksa";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/dokterPemeriksaRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanUnitPelayananKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Unit Pelayanan";
            $model = new PPRuanganM('search');
            $model->tglAwal = date('d M Y').' 00:00:00';
            $model->tglAkhir = date('d M Y H:i:s');
            $model->instalasi_id = 4;
			
            if (isset($_GET['PPRuanganM']))
			{
                $model->attributes = $_GET['PPRuanganM'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
            }
			
            $this->render($this->pathViewPP.'rawatInap/unitPelayananRI', array(
                'model' => $model,
            ));
    }
     public function actionLaporanRMKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Rekam Medik";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/rmRI', array(
            'model' => $model,
        ));
    }
     public function actionLaporanKamarRuanganKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Kamar Ruangan";
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RMLaporankunjunganriV'])) {
            $model->attributes = $_GET['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMLaporankunjunganriV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/kamarRuanganRI', array(
            'model' => $model,
        ));
    }
    
    // -- END VIEW LAPORAN -- //
    
    // -- PRINT LAPORAN -- //
    public function actionPrintKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'rawatInap/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);      
    }
    
       public function actionPrintAlasanPulangKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alasan Pulang';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alasan Pulang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAlasanPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionPrintUmurKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Umur';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Umur';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAgamaKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Agama';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Agama';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintAlamatKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alamat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alamat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printAlamat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintJkKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Jenis Kelamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Jenis Kelamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kunjungan Lama / Baru';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kunjungan Lama / Baru';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintStatusPerkawinanKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Status Perkawinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Status Perkawinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintRujukanKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rujukan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rujukan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintCaraMasukKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Cara Masuk';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Cara Masuk';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKetPulangKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Keterangan Pulang';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Keterangan Pulang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKabKotaKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kabupaten / Kota';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kabupaten / Kota';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKecamatanKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kecamatan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kecamatan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPenjaminKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Penjamin';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Penjamin';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintPemeriksaanKunjunganRI() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pemeriksaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pemeriksaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionPrintPekerjaanKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pekerjaan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pekerjaan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printPekerjaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintUnitPelayananKunjunganRI() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Unit Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasrkan Unit Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printUnitPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintRMKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rekam Medik';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rekam Medik';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printrmRI';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintKamarRuanganKunjunganRI() {
        $model = new RMLaporankunjunganriV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $model->bulan = date('m');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kamar Ruangan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kamar Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RMLaporankunjunganriV'])) {
            $model->attributes = $_REQUEST['RMLaporankunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RMLaporankunjunganriV']['tglAkhir']);
//            $model->bulan =DATE_PART('MONTH',$_REQUEST['PPInfoKunjunganRJV']['bulan']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'_printKamarRuanganRI';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    // -- END PRINT LAPORAN --//
    
    // -- GRAFIK LAPORAN -- //
    public function actionFrameGrafikRI() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
     public function actionFrameGrafikAlasanPulangRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alasan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlasanPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionFrameGrafikAgamaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRekamMedikRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rekam Medik';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikrmRI', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKamarRuanganRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kamar Ruangan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKamarRuanganRI', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    // -- END GRAFIK LAPORAN --//
    
// -- End Laporan Kunjungan RI --//
    public function actionLaporanBukuRegister() {
        $model = new PPBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPBukuregisterpasienV'])) {
            $model->attributes = $_GET['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'bukuRegister/adminBukuRegister', array(
            'model' => $model,
        ));
    }

    public function actionPrintBukuRegister() {
        $model = new PPBukuregisterpasienV('search');
        $judulLaporan = 'Laporan Buku Register';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPBukuregisterpasienV'])) {
            $model->attributes = $_REQUEST['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPBukuregisterpasienV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'bukuRegister/_printBukuRegister';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Grafik Buku Register';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPBukuregisterpasienV'])) {
            $model->attributes = $_GET['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanBatalPeriksa() {
        $model = new PPPasienbatalperiksa('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPPasienbatalperiksa'])) {
            $model->attributes = $_GET['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'batalPeriksa/adminBatalPeriksa', array(
            'model' => $model,
        ));
    }

    public function actionPrintBatalPeriksa() {
        $model = new PPPasienbatalperiksa('search');
        $judulLaporan = 'Laporan Pasien Batal Periksa';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Batal Periksa';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPPasienbatalperiksa'])) {
            $model->attributes = $_REQUEST['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienbatalperiksa']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'batalPeriksa/_printBatalPeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBatalPeriksa() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPPasienbatalperiksa('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Batal Periksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPPasienbatalperiksa'])) {
            $model->attributes = $_GET['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

//    public function actionLaporan10Besar() {
//        $model = new PPLaporan10besarpenyakit('search');
//        $model->tglAwal = date('Y-m-d H:i:s');
//        $model->tglAkhir = date('Y-m-d H:i:s');
//        $model->jumlahTampil = 10;
//        if (isset($_GET['PPLaporan10besarpenyakit'])) {
//            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
//            $format = new CustomFormat();
//            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
//            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
//        }
//
//        $this->render($this->pathViewPP.'10Besar/admin10BesarPenyakit', array(
//            'model' => $model,
//        ));
//    }
//
//    public function actionPrint10BesarPenyakit() {
//        $model = new PPLaporan10besarpenyakit('search');
//        $judulLaporan = 'Laporan 10 Besar Penyakit';
//
//        //Data Grafik
//        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
//        $data['type'] = $_REQUEST['type'];
//        if (isset($_REQUEST['PPLaporan10besarpenyakit'])) {
//            $model->attributes = $_REQUEST['PPLaporan10besarpenyakit'];
//            $format = new CustomFormat();
//            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAwal']);
//            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAkhir']);
//        }
//        
//        $caraPrint = $_REQUEST['caraPrint'];
//        $target = $this->pathViewPP.'10Besar/_print10BesarPenyakit';
//        
//        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
//    }
//
//    public function actionFrameGrafik10BesarPenyakit() {
//        $this->layout = '//layouts/frameDialog';
//        $model = new PPLaporan10besarpenyakit('search');
//        $model->tglAwal = date('Y-m-d H:i:s');
//        $model->tglAkhir = date('Y-m-d H:i:s');
//
//        //Data Grafik
//        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
//        $data['type'] = $_GET['type'];
//        
//        if (isset($_GET['PPLaporan10besarpenyakit'])) {
//            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
//            $format = new CustomFormat();
//            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
//            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
//        }
//        
//        $this->render($this->pathViewPP.'_grafik', array(
//            'model' => $model,
//            'data' => $data,
//        ));
//    }


    public function actionLaporanKarcis() {
        $model = new PPLaporankarcispasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankarcispasien'])) {
            $model->attributes = $_GET['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'karcis/adminKarcis', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKarcis() {
        $model = new PPLaporankarcispasien('search');
        $judulLaporan = 'Laporan Karcis Pasien';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Karcis Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankarcispasien'])) {
            $model->attributes = $_REQUEST['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankarcispasien']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'karcis/_printLaporanKarcis';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameLaporanKarcis() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPLaporankarcispasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Karcis Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PPLaporankarcispasien'])) {
            $model->attributes = $_GET['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKunjunganRS() {
        $model = new PPLaporankunjunganrs('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrs'])) {
            $model->attributes = $_GET['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'kunjunganRS/adminKunjunganRS', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjunganRS() {
        $model = new PPLaporankunjunganrs('search');
        $judulLaporan = 'Laporan Kunjungan Rumah Sakit';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Rumah Sakit';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrs'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrs']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'kunjunganRS/_printKunjunganRS';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjunganRS() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPLaporankunjunganrs('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Rumah Sakit';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPLaporankunjunganrs'])) {
            $model->attributes = $_GET['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

     public function actionLaporanPasienPulang() {
        if(isset($_GET['filter_tab']) == "rj"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Jalan';
        }else if(isset($_GET['filter_tab']) == "ri"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Inap';
        }else if(isset($_GET['filter_tab']) == "rd"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Darurat';
        }else{
            $judulLaporan = 'Laporan Pasien Pulang';
        }

        $model = new RKLaporanpasienpulangV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        $modRJ = new RKLaporanpasienpulangRJV('functionCriteria');
        $modRJ->tglAwal = date('d M Y').' 00:00:00';
        $modRJ->tglAkhir = date('d M Y').' 23:59:59';

        $modRI = new RKLaporanpasienpulangRJV('functionCriteria');
        $modRI->tglAwal = date('d M Y').' 00:00:00';
        $modRI->tglAkhir = date('d M Y').' 23:59:59';


        $modRD = new RKLaporanpasienpulangRJV('functionCriteria');
        $modRD->tglAwal = date('d M Y').' 00:00:00';
        $modRD->tglAkhir = date('d M Y').' 23:59:59';


        if (isset($_GET['RKLaporanpasienpulangRJV'])) {
            $model->attributes = $_GET['RKLaporanpasienpulangRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
            $model->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
            $model->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
        }

//        if($_GET['filter_tab'] == "rj"){
                if (isset($_GET['RKLaporanpasienpulangRJV'])) {
                    $model->attributes = $_GET['RKLaporanpasienpulangRJV'];
                    $format = new CustomFormat();
                    $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
                    $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
                    $modRJ->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
                    $modRJ->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
                    if($_GET['filter_tab'] == "rj"){
//                        echo "c";exit;
                        $pendaftaran_id = $_GET['RKLaporanpasienpulangRJV']['pendaftaran_id'];
                        $modRJ->instalasi_id = PARAMS::INSTALASI_ID_RJ;
                        $modRJ->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
                    $modRJ->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
                    }else if($_GET['filter_tab'] == "ri"){
//                        echo "a";exit;
                        $modRJ->instalasi_id = PARAMS::INSTALASI_ID_RI;
                        $modRJ->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
                    $modRJ->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
                    }else if($_GET['filter_tab'] == "rd"){
//                        echo "b";exit;
                        $modRJ->instalasi_id = PARAMS::INSTALASI_ID_RD;
                        $modRJ->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
                    $modRJ->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
                    }
                    
                }
//            } 
//        else if($_GET['filter_tab'] == "ri"){
//                if (isset($_GET['RKLaporanpasienpulangRJV'])) {
//                    $modRI->attributes = $_GET['RKLaporanpasienpulangRJV'];
//                    $format = new CustomFormat();
//                    $modRI->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
//                    $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
//                    $modRI->instalasi_id = PARAMS::INSTALASI_ID_RI;
//                    
//                }
//            } 
//         else if($_GET['filter_tab'] == "rd"){
//                if (isset($_GET['RKLaporanpasienpulangRJV'])) {
//                    $modRD->attributes = $_GET['RKLaporanpasienpulangRJV'];
//                    $format = new CustomFormat();
//                    $modRD->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAwal']);
//                    $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanpasienpulangRJV']['tglAkhir']);
//                    $modRI->instalasi_id = PARAMS::INSTALASI_ID_RD;
//                }
//            } 

        $this->render('pasienPulang/adminPasienPulang', array(
            'model' => $model,
            'modRJ' => $modRJ,
            'modRI' => $modRI,
            'modRD' => $modRD,
            'tglAwal'=>$modRJ->tglAwal,
            'tglAkhir'=>$modRJ->tglAkhir,
            'judulLaporan' => $judulLaporan,
        ));
    }

    public function actionPrintLaporanPasienPulang() {
        $model = new RKLaporanpasienpulangRJV('searchPrint');
       
        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Pulang';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RKLaporanpasienpulangRJV'])) {
            $model->attributes = $_REQUEST['RKLaporanpasienpulangRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanpasienpulangRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanpasienpulangRJV']['tglAkhir']);
            $model->carabayar_id = $_GET['RKLaporanpasienpulangRJV']['carabayar_id'];
            $model->penjamin_id = $_GET['RKLaporanpasienpulangRJV']['penjamin_id'];
             if($_GET['filter_tab'] == "rj"){
                $model->instalasi_id = PARAMS::INSTALASI_ID_RJ;
            }else if($_GET['filter_tab'] == "ri"){
                $model->instalasi_id = PARAMS::INSTALASI_ID_RI;
            }else if($_GET['filter_tab'] == "rd"){
                $model->instalasi_id = PARAMS::INSTALASI_ID_RD;
            }
        }
        // echo"<pre>";
        // print_r($model->attributes);
        // exit();

        $caraPrint = $_REQUEST['caraPrint'];  
//        $judulLaporan='Laporan Pasien Pulang';
        if($_GET['filter_tab'] == "rj"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Jalan';
        }else if($_GET['filter_tab'] == "ri"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Inap';
        }else if($_GET['filter_tab'] == "rd"){
            $judulLaporan = 'Laporan Pasien Pulang - Rawat Darurat';
        }else{
            $judulLaporan = 'Laporan Pasien Pulang';
        }
        // if($caraPrint=='PRINT') {
        //     $this->layout='//layouts/printWindows';
        //     $this->render('pasienPulang/_printPasienPulang',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        // }
        // else if($caraPrint=='EXCEL') {
        //     $this->layout='//layouts/printExcel';
        //     $this->render('pasienPulang/_printPasienPulang',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
        // }
        // else if($_REQUEST['caraPrint']=='PDF') {
        //     $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
        //             $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
        //             $mpdf = new MyPDF(
        //                     '',
        //                     $ukuranKertasPDF, //format A4 Or
        //                 11, //Font SIZE
        //                 '', //default font family
        //                 3, //15 margin_left
        //                 3, //15 margin right
        //                 25, //16 margin top
        //                 10, // margin bottom
        //                 0, // 9 margin header
        //                 0, // 9 margin footer
        //                 'P' // L - landscape, P - portrait
        //                 );  
        //             $mpdf->useOddEven = 2;  
        //             $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
        //             $mpdf->WriteHTML($stylesheet,1);
        //     // $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
        //     // $posisi = 'L';                           //Posisi L->Landscape,P->Portait
        //     // $mpdf = new MyPDF('', $ukuranKertasPDF);
        //     // $mpdf->useOddEven = 2;
        //     // $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
        //     // $mpdf->WriteHTML($stylesheet, 1);
        //     // $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
        //     // $mpdf->tMargin=5;
        //     $mpdf->WriteHTML($this->renderPartial('pasienPulang/_printPasienPulang',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
        //     $mpdf->Output();
        // } 
        $target = 'pasienPulang/_printPasienPulang';
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionLaporan10Besar()
	{
        $model = new RMPasienmorbiditasT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
		$model->jumlahTampil = 10;
		
        if(isset($_GET['RMPasienmorbiditasT']))
		{
			$format = new CustomFormat();
			$model->instalasi_id = $_GET['RMPasienmorbiditasT']['instalasi_id'];
			$model->ruangan_id = $_GET['RMPasienmorbiditasT']['ruangan_id'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RMPasienmorbiditasT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RMPasienmorbiditasT']['tglAkhir']);			
		}
		
		/*
        if (isset($_GET['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
        }
		*/
        $this->render('10Besar/admin10BesarPenyakit', array(
            'model' => $model,
        ));
    }

    public function actionPrint10BesarPenyakit() {
        $model = new PPLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'10Besar/_print10BesarPenyakit';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPLaporan10besarpenyakit('search');
        $model->tglAwal = $this->tglAwal;
        $model->tglAkhir = $this->tglAkhir;

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanTindakLanjut() {
        $model = new LaporantindaklanjutV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $temp = array();
        foreach (CaraKeluar::items() as $i=>$data){
            $temp[] = strtoupper($data);
        }
        $model->carakeluar = $temp;
        
        if (isset($_GET['LaporantindaklanjutV'])) {
            $model->attributes = $_GET['LaporantindaklanjutV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporantindaklanjutV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporantindaklanjutV']['tglAkhir']);
            $model->carakeluar = $_GET['LaporantindaklanjutV']['carakeluar'];
        }

        $this->render('tindakLanjut/adminTindakLanjut', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanTindakLanjut() {
        $model = new LaporantindaklanjutV('search');
        $judulLaporan = 'Laporan Tindak Lanjut Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporantindaklanjutV'])) {
            $model->attributes = $_REQUEST['LaporantindaklanjutV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporantindaklanjutV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporantindaklanjutV']['tglAkhir']);
            $model->carakeluar = $_REQUEST['LaporantindaklanjutV']['carakeluar'];
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'tindakLanjut/_printTindakLanjut';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTindakLanjut() {
        $this->layout = '//layouts/frameDialog';
        $model = new LaporantindaklanjutV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik 
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LaporantindaklanjutV'])) {
            $model->attributes = $_GET['LaporantindaklanjutV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporantindaklanjutV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporantindaklanjutV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new LaporancaramasukpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'),'asalrujukan_id','asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        if (isset($_GET['LaporancaramasukpasienV'])) {
            $model->attributes = $_GET['LaporancaramasukpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporancaramasukpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporancaramasukpasienV']['tglAkhir']);
        }

        $this->render('caraMasuk/adminCaraMasukPasien', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new LaporancaramasukpasienV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporancaramasukpasienV'])) {
            $model->attributes = $_REQUEST['LaporancaramasukpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporancaramasukpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporancaramasukpasienV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new LaporancaramasukpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LaporancaramasukpasienV'])) {
            $model->attributes = $_GET['LaporancaramasukpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporancaramasukpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporancaramasukpasienV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanMorbiditas() {
        $model = new RKLaporanmorbiditasV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RKLaporanmorbiditasV'])) {
            $model->attributes = $_GET['RKLaporanmorbiditasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAkhir']);
        }

        $this->render('morbiditas/admin', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanMorbiditas() {
        $model = new RKLaporanmorbiditasV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Morbiditas Pasien';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Morbiditas Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RKLaporanmorbiditasV'])) {
            $model->attributes = $_REQUEST['RKLaporanmorbiditasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanmorbiditasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanmorbiditasV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'morbiditas/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
        
    public function actionFrameGrafikLaporanMorbiditas() {
        $this->layout = '//layouts/frameDialog';

        $model = new RKLaporanmorbiditasV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Morbiditas Pasien';
        $data['type'] = $_GET['type'];

        if (isset($_GET['RKLaporanmorbiditasV'])) {
            $model->attributes = $_GET['RKLaporanmorbiditasV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmorbiditasV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanMortalitas() {
        $model = new RKLaporanmortalitaspasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RKLaporanmortalitaspasienV'])) {
            $model->attributes = $_GET['RKLaporanmortalitaspasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAkhir']);
        }

        $this->render('mortalitas/admin', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanMortalitas()
	{
        $model = new RKLaporanmortalitaspasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Mortalitas Pasien';
		
        //Data Grafik
        $data['title'] = 'Grafik Laporan Mortalitas Pasien';
        $data['type'] = $_REQUEST['type'];
        if(isset($_REQUEST['RKLaporanmortalitaspasienV']))
		{
            $format = new CustomFormat();
            $model->attributes = $_REQUEST['RKLaporanmortalitaspasienV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanmortalitaspasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanmortalitaspasienV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'mortalitas/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanMortalitas() {
        $this->layout = '//layouts/frameDialog';

        $model = new RKLaporanmortalitaspasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Mortalitas Pasien';
        $data['type'] = $_GET['type'];

        if (isset($_GET['RKLaporanmortalitaspasienV'])) {
            $model->attributes = $_GET['RKLaporanmortalitaspasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanmortalitaspasienV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKunjunganDokter() {
        $model = new PPLaporankunjunganbydokterV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_GET['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'kunjunganDokter/adminKunjunganDokter', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjunganDokter() {
        $model = new PPLaporankunjunganbydokterV('search');
        $judulLaporan = 'Laporan Kunjungan Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganbydokterV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'kunjunganDokter/_printKunjunganDokter';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjunganDokter() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPLaporankunjunganbydokterV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Dokter';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_GET['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJumlahPemeriksaanDokter() {
        $model = new PPLaporanJumlahPemeriksaanDokterV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y 23:59:59');

        if (isset($_GET['PPLaporanJumlahPemeriksaanDokterV'])) {
            $model->attributes = $_GET['PPLaporanJumlahPemeriksaanDokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporanJumlahPemeriksaanDokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporanJumlahPemeriksaanDokterV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'jumlahPemeriksaanDokter/admin', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanJumlahPemeriksaanDokter() {
        $model = new PPLaporanJumlahPemeriksaanDokterV('search');
        $judulLaporan = 'Laporan Kunjungan Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporanJumlahPemeriksaanDokterV'])) {
            $model->attributes = $_REQUEST['PPLaporanJumlahPemeriksaanDokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporanJumlahPemeriksaanDokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporanJumlahPemeriksaanDokterV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'jumlahPemeriksaanDokter/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikJumlahPemeriksaanDokter() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPLaporanJumlahPemeriksaanDokterV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jumlah Pemeriksaan Dokter';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPLaporanJumlahPemeriksaanDokterV'])) {
            $model->attributes = $_GET['PPLaporanJumlahPemeriksaanDokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporanJumlahPemeriksaanDokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporanJumlahPemeriksaanDokterV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPerUnitPelayanan() {
        $model = new RKLaporanrekapperunitpelayananV('search');
		$modPendaftaran = new PendaftaranT();
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y').' 23:59:59';
		
        if (isset($_GET['RKLaporanrekapperunitpelayananV'])) {
            $model->attributes = $_GET['RKLaporanrekapperunitpelayananV'];
            $model->dokter_nama = $_GET['RKLaporanrekapperunitpelayananV']['dokter_nama'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporanrekapperunitpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporanrekapperunitpelayananV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'unitPelayanan/adminUnitPelayanan', array(
            'model' => $model,'modPendaftaran'=>$modPendaftaran,
        ));
    }

    public function actionPrintLaporanPerUnitPelayanan() {
        $model = new RKLaporanrekapperunitpelayananV('search');
        $judulLaporan = 'Laporan Kunjungan Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Per Unit Pelayanan';
        $data['type'] = $_REQUEST['type'];

        if (isset($_REQUEST['RKLaporanrekapperunitpelayananV'])) {
            $model->attributes = $_REQUEST['RKLaporanrekapperunitpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanrekapperunitpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporanrekapperunitpelayananV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'unitPelayanan/_printUnitPelayanan';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPerUnitPelayanan() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Per Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    


protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
        $tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
        $periode = $this->parserTanggal($tglAwal).' s/d '.$this->parserTanggal($tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
             $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    public function actionLaporanSensusBedahSentral()
    {
        $model = new RKLaporansensusbedahsentralV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        $format = new CustomFormat;
        
        if (isset($_GET['RKLaporansensusbedahsentralV'])) {
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RKLaporansensusbedahsentralV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RKLaporansensusbedahsentralV']['tglAkhir']);
        }
        $this->render('sensusbedahsentral/index',array(
            'model'=>$model,
        ));
    }

    public function actionPrintLaporanSensusBedahSentral()
    {
        $model = new RKLaporansensusbedahsentralV('searchPrint');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $periode = $model->tglAwal.' - '.$model->tglAkhir;
        if (isset($_REQUEST['RKLaporansensusbedahsentralV'])) {
            $model->attributes = $_REQUEST['RKLaporansensusbedahsentralV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporansensusbedahsentralV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RKLaporansensusbedahsentralV']['tglAkhir']);
            $periode = $_REQUEST['RKLaporansensusbedahsentralV']['tglAwal'].' - '.$_REQUEST['RKLaporansensusbedahsentralV']['tglAkhir'];
        }
        $caraPrint=$_REQUEST['caraPrint'];
        $judulLaporan='SENSUS HARIAN INSTALASI BEDAH SENTRAL';
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('sensusbedahsentral/Print',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('sensusbedahsentral/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = 'L';                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial('sensusbedahsentral/Print',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }                    
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
    
}