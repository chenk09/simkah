
<?php

class PengirimanrmTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathView = 'rekamMedis.views.pengirimanrmT.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','informasi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKPengirimanrmT;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKPengirimanrmT']))
		{
			$model->attributes=$_POST['RKPengirimanrmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pengirimanrm_id));
                        }
		}

		$this->render($this->pathView.'create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKPengirimanrmT']))
		{
			$model->attributes=$_POST['RKPengirimanrmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pengirimanrm_id));
                        }
		}

		$this->render($this->pathView.'update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
//		$dataProvider=new CActiveDataProvider('RKPengirimanrmT');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
                
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $modPengiriman=new RKDokumenpasienrmlamaV('search');
                $modPengiriman->tgl_rekam_medik = date('Y-m-d H:i:s');
                $modPengiriman->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
//                $modPengiriman->unsetAttributes();  // clear any default values
                if(isset($_GET['RKDokumenpasienrmlamaV'])){
                    $modPengiriman->attributes=$_GET['RKDokumenpasienrmlamaV'];
                    $format = new CustomFormat();
                    $modPengiriman->tgl_rekam_medik = $format->formatDateTimeMediumForDB($modPengiriman->tgl_rekam_medik);
                    $modPengiriman->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($modPengiriman->tgl_rekam_medik_akhir);
                }

//                $modPengiriman=new RKPeminjamanrmT('search');
//                $modPengiriman->unsetAttributes();  // clear any default values
//                if(isset($_GET['RKPeminjamanrmT']))
//                    $modPengiriman->attributes=$_GET['RKPeminjamanrmT'];
        
                $model=new RKPengirimanrmT;
                $model->tglpengirimanrm = date('Y-m-d H:i:s');
                
                $model->petugaspengirim = Yii::app()->user->name;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKPengirimanrmT']))
		{
			$model->attributes=$_POST['RKPengirimanrmT'];
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $success = true;
                            $jumlah = count($_POST['Dokumen']['pasien_id']);
                            for($i = 0; $i < $jumlah; $i++){
                                if ($_POST['cekList'][$i] == 1){
                                    $models = new RKPengirimanrmT();
                                    $models->attributes = $model->attributes;
                                    if ($_POST['Dokumen']['kelengpakan'][$i] == 1){
                                        $models->kelengkapandokumen = true;
                                    }
                                    else{
                                        $models->kelengkapandokumen = false;
                                    }
                                    $models->pasien_id = $_POST['Dokumen']['pasien_id'][$i];
                                    $models->pendaftaran_id = $_POST['Dokumen']['pendaftaran_id'][$i];
                                    $models->dokrekammedis_id = $_POST['Dokumen']['dokrekammedis_id'][$i];
                                    $models->ruangan_id = $_POST['Dokumen']['ruangan_id'][$i];
                                    $models->nourut_keluar = Generator::noUrutKeluarRM();
                                    $models->ruanganpengirim_id = Yii::app()->user->getState('ruangan_id');
                                    $models->peminjamanrm_id = $_POST['Dokumen']['peminjamanrm_id'][$i];
                                    if (!$models->save()){
                                        $success = false;
                                    }
                                    else{
                                        PendaftaranT::model()->updateByPK($models->pendaftaran_id, array('pengirimanrm_id'=>$models->pengirimanrm_id));
                                        //PendaftaranT::model()->updateByPK($models->pasien_id, array('pengirimanrm_id'=>$models->pengirimanrm_id));
                                        PeminjamanrmT::model()->updateByPk($models->peminjamanrm_id, array('pengirimanrm_id'=>$models->pengirimanrm_id));
                                    }
                                }
                            }
                            if ($success == true){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Pengiriman Data Peminjaman Dokumen Rekam Medis berhasil disimpan");
                                $this->refresh();
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");

                            }
                        }
                        catch (Exception $exc) {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                        }
//			if($model->save()){
//                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//				$this->redirect(array('view','id'=>$model->pengirimanrm_id));
//                        }
		}

		$this->render($this->pathView.'index',array(
			'model'=>$model,
                        'modPengiriman'=>$modPengiriman,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKPengirimanrmT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RKPengirimanrmT']))
			$model->attributes=$_GET['RKPengirimanrmT'];

		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}
        
	public function actionInformasi()
	{
		$model=new InformasipengirimanrmV('search');
                                $model->unsetAttributes();
                                $model->tglAwal = date('Y-m-d 00:00:00');
                                $model->tglAkhir = date('Y-m-d H:i:s');
                                if(isset($_GET['InformasipengirimanrmV'])){
                                    $model->attributes = $_GET['InformasipengirimanrmV'];
                                    $format = new CustomFormat();
                                    $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['InformasipengirimanrmV']['tglAwal']);
                                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['InformasipengirimanrmV']['tglAkhir']);
                                }
		$this->render($this->pathView.'informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RKPengirimanrmT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rkpengirimanrm-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new RKPengirimanrmT;
            $model->attributes=$_REQUEST['RKPengirimanrmT'];
            $judulLaporan='Data RKPengirimanrmT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
