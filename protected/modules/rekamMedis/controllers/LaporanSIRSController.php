<?php

class LaporanSIRSController extends SBaseController
{
    public function actionIndex()
    {
        $model = new MenumodulK('search');
        $this->render('sirs', array(
            'model' => $model,
        ));
    }
    
    public function actionPengunjungRUmahSakit()
    {
        $model = new RKRrl51PengunjungrsV;
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        $criteria = new CDbCriteria();
        $criteria->select = 'statuspasien, sum(jmlpasien) as jmlpasien';
        $criteria->group = 'statuspasien';
        $criteria->addBetweenCondition('DATE(tgl_pendaftaran)',$tgl_awal,$tgl_akhir);
        
        $records = Rl51PengunjungrsV::model()->findAll($criteria);

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
        );

        $rows = array();
        $i=0;
        $total_jumlah = 0;
        foreach($records as $row)
        {
            $rows[] = array(
                '<td>'. ($i+1).'</td>', '<td>'.$row['statuspasien'].'</td>', '<td>'.$row['jmlpasien'].'</td>'
            );
            $i++;
            $total_jumlah += $row['jmlpasien'];
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        foreach($header as $key=>$index)
        {
            $table .= '<td style="text-align:center;background-color:#AFAFAF">'. ($key+1) .'</td>';
        }        
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td><td>'. $total_jumlah .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 5.1',
                        'title'=>'KEGIATAN KUNJUNGAN RAWAT JALAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 5.1',
                    'title'=>'PENGUNJUNG RUMAH SAKIT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }        
    }
    
    public function actionKunjunganRawatJalan()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);

        $criteria = new CDbCriteria();
        $criteria->select = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama, sum(jmlpasien) as jmlpasien';
        $criteria->group = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama';
        $criteria->addBetweenCondition('DATE(tgl_pendaftaran)',$tgl_awal,$tgl_akhir);

        $records = Rl52KunjrwtjlnV::model()->findAll($criteria);
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
        );

        $rows = array();
        $i=0;
        $total_jumlah = 0;
        foreach($records as $row)
        {
            $rows[] = array(
                '<td>'. ($i+1).'</td>', '<td>'.$row['jeniskasuspenyakit_nama'].'</td>', '<td>'.$row['jmlpasien'].'</td>'
            );
            $i++;
            $total_jumlah += $row['jmlpasien'];
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        foreach($header as $key=>$index)
        {
            $table .= '<td style="text-align:center;background-color:#AFAFAF">'. ($key+1) .'</td>';
        }        
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;"></td><td><b>Total</b></td><td>'. $total_jumlah .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 5.2',
                        'title'=>'KEGIATAN KUNJUNGAN RAWAT JALAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 5.2',
                    'title'=>'KEGIATAN KUNJUNGAN RAWAT JALAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    public function action10besarPenyakitRawatInap()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
//        $sql = "
//            SELECT * FROM laporan10besarpenyakit_v
//            WHERE instalasi_id = ".PARAMS::INSTALASI_ID_RI."
//        ";
//        $records = YII::app()->db->createCommand($sql)->queryAll();
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('DATE(tglmorbiditas)',$tgl_awal,$tgl_akhir);
        $records = Rl5310bsrpenyakitriV::model()->findAll($criteria);
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;">No. Urut</th>', 
            '<th rowspan="2" style="text-align:center;">KODE ICD 10</th>', 
            '<th rowspan="2"  style="text-align:center;">DESKRIPSI</th>',
            '<th colspan="2" style="text-align:center;">Pasien Keluar Hidup Menurut Jenis Kelamin</th>',
            '<th colspan="2" style="text-align:center;">Pasien Keluar Mati Menurut Jenis Kelamin</th>',
            '<th rowspan="2" style="text-align:center;">Total (Hidup & Mati)</th>'
        );
        $header2 = array(
            '<th style="text-align:center;">LK</th>', 
            '<th style="text-align:center;">PR</th>',
            '<th style="text-align:center;">LK</th>', 
            '<th style="text-align:center;">PR</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
        );

        $rows = array();
        $i=0;
        $total_lh= 0;
        $total_lm= 0;
        $total_ph= 0;
        $total_pm= 0;
        $total_jmlpasien= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $diagnosa_id = $row->diagnosa_id;
            $jeniskelamin = $row->jeniskelamin;
            
            $datas[$diagnosa_id]['diagnosa_id']= $row->diagnosa_id;
            $datas[$diagnosa_id]['jeniskelamin']= $row->jeniskelamin;
            
            $datas[$diagnosa_id]['diagnosa'][$j]['diagnosa_kode'] = $row->diagnosa_kode;
            $datas[$diagnosa_id]['diagnosa'][$j]['jeniskelamin'] = $row->jeniskelamin;
            $datas[$diagnosa_id]['diagnosa'][$j]['pasienhidupmati'] = $row->pasienhidupmati;
            $datas[$diagnosa_id]['diagnosa'][$j]['diagnosa_nama'] = $row->diagnosa_nama;
            $datas[$diagnosa_id]['diagnosa'][$j]['jmlpasien'] = $row->jmlpasien;
                     
        }
        
        foreach($datas as $key=>$data){
            $diagnosa = $data['diagnosa'];
            $temp = array();
            foreach($diagnosa as $d) {
                $name = "$d[diagnosa_kode] - $d[jeniskelamin] - $d[pasienhidupmati]";
                if(!isset($temp[$name])) {
                    $temp[$name] = $d;
                } else {
                    $temp[$name]['jmlpasien'] += $d['jmlpasien'];
                }
            }
            $diagnosa = array_values($temp);
            $data['diagnosa'] = $diagnosa;
            
            foreach($data['diagnosa'] as $x => $val)
            {
                if($val['pasienhidupmati'] == 'HIDUP'){
                    if($val['jeniskelamin'] == 'LAKI-LAKI'){
                        $lh = $val['jmlpasien'];
                        $lm = '';
                        $ph = '';
                        $pm = '';
                    }else{
                        $ph = $val['jmlpasien'];
                        $pm = '';
                        $lh = '';
                        $lm = '';
                    }                    
                }else{
                    if($val['jeniskelamin'] == 'LAKI-LAKI'){
                        $lh = '';
                        $lm = $val['jmlpasien'];
                        $ph = '';
                        $pm = '';
                    }else{
                        $ph = '';
                        $pm = $val['jmlpasien'];
                        $lh = '';
                        $lm = '';
                    }   
                }
                $jmlpasien = $lh + $lm + $ph + $pm;
                $rows[] = array(
                    '<td style=text-align:center;>'. ($i+1).'</td>',
                    '<td>'.$val['diagnosa_kode'].'</td>',
                    '<td>'.$val['diagnosa_nama'].'</td>',
                    '<td style=text-align:center;>'.$lh.'</td>',
                    '<td style=text-align:center;>'.$ph.'</td>',
                    '<td style=text-align:center;>'.$lm.'</td>',
                    '<td style=text-align:center;>'.$pm.'</td>',
                    '<td style=text-align:center;>'.$jmlpasien.'</td>',                    
                );
                $i++;
                $total_lh += $lh;
                $total_lm += $lm;
                $total_ph += $ph;
                $total_pm += $pm;
                $total_pasien += $jmlpasien;
            }
        }   

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td></td><td></td><td><b>Total</b></td><td style=text-align:center;>'. $total_lh .'</td><td style=text-align:center;>'. $total_ph .'</td><td style=text-align:center;>'. $total_lm .'</td><td style=text-align:center;>'. $total_pm .'</td><td style=text-align:center;>'. $total_pasien .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 5.2',
                        'title'=>'KEGIATAN KUNJUNGAN RAWAT JALAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 5.3',
                    'title'=>'10 BESAR PENYAKIT RAWAT INAP',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
    public function action10besarPenyakitRawatJalan()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
//        $sql = "
//            SELECT * FROM laporan10besarpenyakit_v
//            WHERE instalasi_id = ".PARAMS::INSTALASI_ID_RJ."
//        ";
//        $records = YII::app()->db->createCommand($sql)->queryAll();
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('DATE(tglmorbiditas)',$tgl_awal,$tgl_akhir);
        $records = Rl5410bsrpenyakitrwtjalanV::model()->findAll($criteria);
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;">No. Urut</th>', 
            '<th rowspan="2" style="text-align:center;">KODE ICD 10</th>', 
            '<th rowspan="2"  style="text-align:center;">DESKRIPSI</th>',
            '<th colspan="2" style="text-align:center;">KASUS BARU MENURUT JENIS KELAMIN</th>',
            '<th rowspan="2" style="text-align:center;">Jumlah Kasus Baru (4 + 5)</th>',
            '<th rowspan="2" style="text-align:center;">Jumlah Kunjungan</th>'
        );
        $header2 = array(
            '<th style="text-align:center;">Laki - Laki</th>', 
            '<th style="text-align:center;">Perempuan</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
        );

        $rows = array();
        $i=0;
        $total_laki = 0;
        $total_perempuan = 0;
        $total_kasus = 0;
        $total_kunjungan = 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $diagnosa_id = $row->diagnosa_id;
            $jeniskelamin = $row->jeniskelamin;
            
            $datas[$diagnosa_id]['diagnosa_id']= $row->diagnosa_id;
            $datas[$diagnosa_id]['jeniskelamin']= $row->jeniskelamin;
            
            $datas[$diagnosa_id]['diagnosa'][$j]['diagnosa_kode'] = $row->diagnosa_kode;
            $datas[$diagnosa_id]['diagnosa'][$j]['jeniskelamin'] = $row->jeniskelamin;
            $datas[$diagnosa_id]['diagnosa'][$j]['diagnosa_nama'] = $row->diagnosa_nama;
            $datas[$diagnosa_id]['diagnosa'][$j]['jmlpasien'] = $row->jmlpasien;
                     
        }

        foreach($datas as $key=>$data){
            foreach($data['diagnosa'] as $x => $val)
            {
                if($val['jeniskelamin'] == 'LAKI-LAKI'){
                    $laki = $val['jmlpasien'];
                    $perempuan = '';
                }else{
                    $perempuan = $val['jmlpasien'];
                    $laki = '';
                }
                $jmlkasus = $laki + $perempuan;
                $jmlkunjungan = $laki + $perempuan;
                $rows[] = array(
                    '<td style=text-align:center;>'. ($i+1).'</td>',
                    '<td>'.$val['diagnosa_kode'].'</td>',
                    '<td>'.$val['diagnosa_nama'].'</td>',
                    '<td style=text-align:center;>'.$laki.'</td>',
                    '<td style=text-align:center;>'.$perempuan.'</td>',
                    '<td style=text-align:center;>'.$jmlkasus.'</td>',
                    '<td style=text-align:center;>'.$jmlkunjungan.'</td>',
                    
//                        '<td>'.$row['jmlpasien'].'</td>',
//                        '<td>'.$row['jmlpasien'].'</td>',
//                        '<td>'.$row['jmlpasien'].'</td>',
                );
                $i++;
                $total_laki += $laki;
                $total_perempuan += $perempuan;
                $total_kasus += $perempuan + $laki;
                $total_kunjungan += $jmlkunjungan;
            }
        }   
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td></td><td></td><td><b>Total</b></td><td style=text-align:center;>'. $total_laki .'</td><td style=text-align:center;>'. $total_perempuan .'</td><td style=text-align:center;>'. $total_kasus .'</td><td style=text-align:center;>'. $total_kunjungan .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 5.4',
                        'title'=>'10 BESAR PENYAKIT RAWAT JALAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 5.4',
                    'title'=>'10 BESAR PENYAKIT RAWAT JALAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
     public function actionDataDasarRS()
    {
        $model = new Rl1datarsV('search');
        $model->attributes = $_GET['Rl1datarsjmbedV'];
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);

        // $model->kelaspelayanan_nama = $_GET['Rl1datarsjmbedV'];
        // print_r($model->kelaspelayanan_nama);exit();

        $sql = " 
        SELECT * FROM rl1datars_v WHERE profilrs_id = ".PARAMS::PROFIL_RSJK."";
//        $sql2 ="
//        SELECT * FROM rl1datarsjmbed_v";

        $records = YII::app()->db->createCommand($sql)->queryAll();

//        $records2 = YII::app()->db->createCommand($sql2)->queryAll();

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $rows = array();
        $i=0;

        $table = '<table width="100%" height="100%" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' class="table table-striped table-bordered table-condensed">';
        $table .= '<tbody>';
        
        $i = 0;
        $judul[] = array("Nomor Kode RS","Tanggal Registrasi","Nama Rumah Sakit","Jenis Rumah Sakit","Kelas Rumah Sakit",
            "Nama Direktur RS","Nama Penyelenggara RS","Alamat/Lokasi RS","Kab/Kota", "Kode Pos", "Telepon","Fax","Email",
            "Nomor Telp Bag. Umum/Humas RS","Website","Luas Rumah Sakit","Tanah","Bangunan","Surat Izin/Penetapan",
            "Nomor","Tanggal","Oleh","Sifat","Masa Berlaku s/d thn", "Status Penyelenggara Swasta *","Akreditasi RS *",
            "Pentahapan *","Status *","Tanggal Akreditasi", "Jumlah Tempat Tidur", "Pernitalogi", "Kelas VVIP", "Kelas I",
            "Kelas II","Kelas III","ICU","PICU","NICU","ICCU","HCU","Ruang Isolasi","Ruang UGD","Ruang Bersalin","Ruang Operasi",
            "Jumlah Tenaga medis","Dokter Sp.A","Dokter Sp.OG","Dokter Sp.OG","Dokter Sp.Pd","Dokter Sp.B","Dokter Sp.Rad",
            "Dokter Sp.RM","Dokter Sp.An","Dokter Sp.Jp","Dokter Sp.M","Dokter Sp.THT","Dokter Sp.Kj","Dokter Sp.PK","Dokter Sub Spesialis",
            "Dokter Spesialis Lain", "Dokter Umum", "Dokter Gigi","Dokter Gigi Spesialis","Perawat","Bidan","Farmasi","Tenaga Kesehatan Lainnya",
            "Jumlah Tenaga Non Kesehatan");

        // $isi[] = array("OK");
        $hitung = array_count_values($judul);
        // echo $hitung["Nomor Kode RS"];
        // $arr = COUNT($hitung);
        // echo $arr;
        for($i=0; $i<68; $i++){
                $no = $i;                    
            $table .= '<tr>';
            $table .= '<td>'.($i+1). '</td>';
            foreach($judul as $a){
                $table .= '<td>'.$a[$i].'</td>';
                $table .= '<td>:</td>';
                // $table .= '<td>'.$b[$i].'</td>'; 
            }
            foreach ($records as $b) {
                $rows[] = array(
                    $b['nokode_rumahsakit']
                );

            }
            
            foreach ($c as $d) {
                $rows3[] = array(
                     $d 
                    );
            }
            

            if($i==0) {
                $table .= '<td>'.$b['nokode_rumahsakit'].'</td>';
            }elseif($i==1){
                $table .= '<td>'.$b['tglregistrasi'].'</td>';
            }elseif($i==2){
                $table .= '<td>'.$b['nama_rumahsakit'].'</td>';
            }elseif($i==3){
                $table .= '<td>'.$b['jenisrs_profilrs'].'</td>';
            }elseif($i==4){
                $table .= '<td>'.$b['kelas_rumahsakit'].'</td>';
            }elseif($i==5){
                $table .= '<td>'.$b['namadirektur_rumahsakit'].'</td>';
            }elseif($i==6){
                $table .= '<td>'.$b['namakepemilikanrs'].'</td>';
            }elseif($i==7){
                $table .= '<td></td>';
            }elseif($i==8){
                $table .= '<td>'.$b['kabupaten_nama'].'</td>';
            }elseif($i==9){
                $table .= '<td>'.$b['kode_pos'].'</td>';
            }elseif($i==10){
                $table .= '<td>'.$b['no_telp_profilrs'].'</td>';
            }elseif($i==11){
                $table .= '<td>'.$b['no_faksimili'].'</td>';
            }elseif($i==12){
                $table .= '<td>'.$b['email'].'</td>';
            }elseif($i==13){
                $table .= '<td>'.$b['notelphumas'].'</td>';
            }elseif($i==14){
                $table .= '<td>'.$b['website'].'</td>';
            }elseif($i==15){
                $table .= '<td></td>';
            }elseif($i==16){
                $table .= '<td>'.$b['luastanah'].'</td>';
            }elseif($i==17){
                $table .= '<td>'.$b['luasbangunan'].'</td>';
            }elseif($i==18){
                $table .= '<td></td>';
            }elseif($i==19){
                $table .= '<td>'.$b['nomor_suratizin'].'</td>';
            }elseif($i==20){
                $table .= '<td>'.$b['tgl_suratizin'].'</td>';
            }elseif($i==21){
                $table .= '<td>'.$b['oleh_suratizin'].'</td>';
            }elseif($i==22){
                $table .= '<td>'.$b['sifat_suratizin'].'</td>';
            }elseif($i==23){
                $table .= '<td>'.$b['masaberlakutahun_suratizin'].'</td>';
            }elseif($i==24){
                $table .= '<td>'.$b['statusrsswasta'].'</td>';
            }elseif($i==25){
                $table .= '<td>'.$b['akreditasirs'].'</td>';
            }elseif($i==26){
                $table .= '<td>'.$b['pentahapanakreditasirs'].'</td>';
            }elseif($i==27){
                $table .= '<td>'.$b['statusakreditasirs'].'</td>';
            }elseif($i==28){
                $table .= '<td>'.$b['tglakreditasi'].'</td>';
            }elseif($i==29){
                $table .= '<td></td>';
            }
//        foreach ($records2 as $c) {
//                // $rows2[] = array(
//                //     $c['kelaspelayanan_nama'], $c['kelaspelayanan_id']
//                // );
//                $kelas = $c['kelaspelayanan_id'];
//                $nama = $c['kelaspelayanan_nama'][$kelas];
//
//                if($i==30 && $c['Vip B']){
//                $table .= '<td>'.$c['jlmbed'].'</td>';
//                }elseif($i==31 && $c['Vip B']){
//                $table .= '<td>'.$c['jlmbed'].'</td>';
//                }elseif($i==32  && $c['Vip B']){
//                    $table .= '<td>'.$c['jlmbed'].'</td>';
//                }elseif($i==33  && $c['Vip B']){
//                    $table .= '<td>'.$c['jlmbed'].'</td>';
//                }elseif($i==34  && $c['Vip B']){
//                    $table .= '<td>'.$c['jlmbed'].'</td>';
//                }elseif($i==35  && $c['Vip B']){
//                    $table .= '<td>'.$c['jlmbed'].'</td>';
//                }
//           }
            if($i==36){
                $table .= '<td>'.$b['nama_rumahsakit'].'</td>';
            }elseif($i==37){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==38){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==39){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==40){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==41){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==42){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==43){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==44){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==45){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==46){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==47){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==48){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==49){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==50){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==51){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==52){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==53){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==54){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==55){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==56){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==57){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==58){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==59){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==60){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==61){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==62){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==63){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==64){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==65){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==66){
                $table .= '<td>'.$b[''].'</td>';
            }elseif($i==67){
                $table .= '<td>'.$b[''].'</td>';
            }
            $table .= '</tr>';
            // $table .= '<td>'.$rows[$i].'<td></tr>';
            //  foreach($rows as $hsl)
            // {
                // $table .= '<td>'.count($rows).'<td></tr>';
            // }
            
        }

       

        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'1000',
                        'formulir'=>'Formulir RL 5.2',
                        'title'=>'DATA DASAR RUMAH SAKIT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'1000',
                    'formulir'=>'Formulir RL 1.1',
                    'title'=>'DATA DASAR RUMAH SAKIT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }        
    }


     public function actionTempatTidurRI()
    {
        $model = new RKrl13FastempattidurV;
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        // $sql = "
        //     SELECT jeniskasuspenyakit_id, jeniskasuspenyakit_nama, sum(jlmtt) as jlmtt FROM rl1_3_fastempattidur_v WHERE instalasi_id = ".PARAMS::INSTALASI_ID_RI." group by jeniskasuspenyakit_id, jeniskasuspenyakit_nama
        //     order by jeniskasuspenyakit_id asc";


        // $sqljmltt = "
        //     SELECT jeniskasuspenyakit_nama FROM rl1_3_fastempattidur_v WHERE instalasi_id = ".PARAMS::INSTALASI_ID_RI." group by jeniskasuspenyakit_nama";
       
        $criteria=new CDbCriteria;
        $criteria->select = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama, sum(jlmtt) as jlmtt';
        $criteria->compare('instalasi_id',PARAMS::INSTALASI_ID_RI);
        $criteria->group = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama';
        $criteria->order = 'jeniskasuspenyakit_id asc';

        // $records = YII::app()->db->createCommand($sql)->queryAll();
        $records = RKrl13FastempattidurV::model()->findAll($criteria);

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;">NO</th>', 
            '<th rowspan="2" style="text-align:center;">JENIS PELAYANAN</th>', 
            '<th rowspan="2"  style="text-align:center;">JUMLAH TT</th>',
            '<th colspan="9" style="text-align:center;">PERINCIAN TEMPAT TIDUR PER KELAS</th>',
        );
        $header2 = array(
            '<th style="text-align:center;">VVIP</th>', 
            '<th style="text-align:center;">VIP A</th>',
            '<th style="text-align:center;">VIP B</th>',
            '<th style="text-align:center;">UTAMA</th>',
            '<th style="text-align:center;">MADYA A</th>',
            '<th style="text-align:center;">MADYA B</th>',
            '<th style="text-align:center;">PRATAMA A</th>',
            '<th style="text-align:center;">PRATAMA B</th>',
            '<th style="text-align:center;">PRATAMA C</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;background-color:#AFAFAF">12</td>',
        );

        $rows = array();
        $i=0;
        $total_jumlah = 0;
        foreach($records as $row)
        {
            
            $rows[] = array(
                '<td style=text-align:center;>'. ($i+1).'</td>',
                '<td>'.$row['jeniskasuspenyakit_nama'].'</td>',
                '<td>'.$row['jlmtt'].'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_VVIP, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_VIP_A, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_VIP_B, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_UTAMA, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_MADYA_A, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_MADYA_B, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_PRATAMA_A, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_PRATAMA_B, PARAMS::INSTALASI_ID_RI).'</td>',
                '<td>'.$row->getSumPelayanan($row['jeniskasuspenyakit_id'],PARAMS::KELASPELAYANAN_ID_PRATAMA_C, PARAMS::INSTALASI_ID_RI).'</td>',
            );
            $i++;
            $total_jumlah += $row['jlmtt'];
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td></td><td></td><td><b>Total</b></td><td>'. $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_VVIP, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_VIP_A, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_VIP_B, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_UTAMA, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_MADYA_A, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_MADYA_B, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_PRATAMA_A, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_PRATAMA_B, PARAMS::INSTALASI_ID_RI) .'</td><td>'
                                                                   . $model->getSumTotal(PARAMS::KELASPELAYANAN_ID_PRATAMA_C, PARAMS::INSTALASI_ID_RI) .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 1.3',
                        'title'=>'FASILITAS TEMPAT TIDUR RAWAT INAP',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 1.3',
                    'title'=>'FASISLITAS TEMPAT TIDUR RAWAT INAP',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }

    public function actionKegiatanPembedahan()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        $criteria = new CDbCriteria();
        $criteria->group = 'kegiatanoperasi_id,kegiatanoperasi_nama,golonganoperasi_id,golonganoperasi_nama';
        $criteria->select = $criteria->group.', sum(jmloperasi) as jmloperasi';
        $criteria->order = 'kegiatanoperasi_id,golonganoperasi_id';
        $criteria->addBetweenCondition('DATE(tglrencanaoperasi)',$tgl_awal,$tgl_akhir);
        $records = Rl36KegpembedahanV::model()->findAll($criteria);
        
        $criteria2 = new CDbCriteria();
        $criteria2->group = 'kegiatanoperasi_id,kegiatanoperasi_nama';
        $criteria2->select = $criteria2->group.' , sum(jmloperasi) as jmloperasi';
        $criteria2->order = 'kegiatanoperasi_id';
//        $criteria2->addColumnCondition("date_part('year', tglrencanaoperasi)");
        $criteria2->addBetweenCondition('DATE(tglrencanaoperasi)',$tgl_awal,$tgl_akhir);
        $recordsGroup = Rl36KegpembedahanV::model()->findAll($criteria2);
        
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        
        $header = array(
            '<th width="50" style="text-align:center;">NO</th>', 
            '<th style="text-align:center;">SPESIALISASI</th>', 
            '<th style="text-align:center;">TOTAL</th>',
            '<th style="text-align:center;">KHUSUS</th>',
            '<th style="text-align:center;">BESAR</th>',
            '<th style="text-align:center;">SEDANG</th>',
            '<th style="text-align:center;">KECIL</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        $dataGroup = array();
        foreach($records as $j=>$row)
        {
            $kegiatanoperasi = $row->kegiatanoperasi_id;
            $datas[$kegiatanoperasi][$row->golonganoperasi_id]['kegiatanoperasi_id'] = $row->kegiatanoperasi_id;
            $datas[$kegiatanoperasi][$row->golonganoperasi_id]['kegiatanoperasi_nama'] = $row->kegiatanoperasi_nama;
            $datas[$kegiatanoperasi][$row->golonganoperasi_id]['golonganoperasi_id'] = $row->golonganoperasi_id;
            $datas[$kegiatanoperasi][$row->golonganoperasi_id]['jmloperasi'] += $row->jmloperasi;                     
        }
        
        foreach($recordsGroup as $m=>$value)
        {
            $kegiatanoperasi = $value->kegiatanoperasi_id;
            $dataGroup[$kegiatanoperasi]['golonganoperasi_id'] = $value->golonganoperasi_id;                    
            $dataGroup[$kegiatanoperasi]['kegiatanoperasi_id'] = $value->kegiatanoperasi_id;                    
            $dataGroup[$kegiatanoperasi]['kegiatanoperasi_nama'] = $value->kegiatanoperasi_nama;                    
            $dataGroup[$kegiatanoperasi]['jmloperasi'] = $value->jmloperasi;                    
        }
        
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
            $a = 0;
           foreach($dataGroup as $key=>$val){
                    $jmloperasi = $val['jmloperasi'];
                    $jmlkhusus = 0;
                    $jmlbesar = 0;
                    $jmlkecil = 0;
                    $jmlsedang = 0;
                    foreach($datas[$val['kegiatanoperasi_id']] as $r=>$data){
                        if($data['kegiatanoperasi_id'] == $val['kegiatanoperasi_id']){
                            if($data['golonganoperasi_id'] == 1){
                                $jmlbesar += $data['jmloperasi'];
                            }else if($data['golonganoperasi_id'] == 3){
                                $jmlsedang += $data['jmloperasi'];
                            }else if($data['golonganoperasi_id'] == 2){
                                $jmlkecil += $data['jmloperasi'];
                            }
                        }                        
                    }  
                    
                    $table .= '<tr>';
                    $table .= '<td style=text-align:center;>'. ($a+1).'</td>
                        <td>'.$val['kegiatanoperasi_nama'].'</td>
                        <td style=text-align:center;>'.$val['jmloperasi'].'</td>
                        <td style=text-align:center;>'.$jmlkhusus.'</td>
                        <td style=text-align:center;>'.$jmlbesar.'</td>
                        <td style=text-align:center;>'.$jmlsedang.'</td>
                        <td style=text-align:center;>'.$jmlkecil.'</td></tr>';
                    $a++;
                    $total += $jmloperasi;
                    $total_khusus += $jmlkhusus;
                    $total_besar += $jmlbesar;
                    $total_sedang += $jmlsedang;
                    $total_kecil += $jmlkecil;
                    
            } 
        $table .= '<tr><td></td><td><b>Total</b></td><td style=text-align:center;>'.$total .'</td><td style=text-align:center;>'.$total_khusus .'</td><td style=text-align:center;>'.$total_besar .'</td><td style=text-align:center;>'.$total_sedang .'</td><td style=text-align:center;>'.$total_kecil .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.6',
                        'title'=>'KEGIATAN PEMBEDAHAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.6',
                    'title'=>'KEGIATAN PEMBEDAHAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
    public function actionKegiatanRadiologi()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        $criteria = new CDbCriteria();
        $criteria->group = 'pemeriksaanrad_jenis,pemeriksaanrad_nama';
        $criteria->select = $criteria->group.', sum(jumlah) as jumlah';
        $criteria->order = 'pemeriksaanrad_jenis,pemeriksaanrad_nama';
        $criteria->addBetweenCondition('tgl_tindakan',$tgl_awal,$tgl_akhir);
        $records = Rl37Kegradiologi::model()->findAll($criteria);
        
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        
        $criteria2 =  new CDbCriteria();
        $criteria2->group = 'pemeriksaanrad_jenis';
        $criteria2->select = $criteria2->group;
        $criteria2->order = 'pemeriksaanrad_jenis';
        $criteria2->addBetweenCondition('tgl_tindakan',$tgl_awal,$tgl_akhir);
        $recordsGroup = Rl37Kegradiologi::model()->findAll($criteria2);
        
        $header = array(
            '<th width="50" style="text-align:center;">No. Urut</th>', 
            '<th style="text-align:center;">Jenis Kegiatan</th>', 
            '<th style="text-align:center;">Jumlah</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        $dataGroup = array();
        foreach($records as $j=>$row)
        {
            $jenis = $row->pemeriksaanrad_jenis;
            $datas[$jenis][$row->pemeriksaanrad_nama]['pemeriksaanrad_nama'] = $row->pemeriksaanrad_nama;
            $datas[$jenis][$row->pemeriksaanrad_nama]['jmlpasien'] += $row->jumlah;                     
        }
        
        foreach($recordsGroup as $m=>$value){
            $jenis = $value->pemeriksaanrad_jenis;
            $dataGroup[$jenis]['pemeriksaanrad_jenis']= $value->pemeriksaanrad_jenis;           
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($dataGroup as $key=>$data){
            $table .= '<tr>';
            $table .= '<td colspan=3><b>'.strtoupper($data['pemeriksaanrad_jenis']).'</b></td></tr>';  
            $a = 0;
            foreach($datas[$data['pemeriksaanrad_jenis']] as $x => $val)
            {
                $jmlpasien = $val['jmlpasien'];
                $table .= '<tr>';
                $table .= '<td style=text-align:center;>'. ($a+1).'</td>
                    <td>'.$val['pemeriksaanrad_nama'].'</td>
                    <td style=text-align:center;>'.$val['jmlpasien'].'</td></tr>';
                $a++;
                $total += $jmlpasien;
            }
        }   
        $table .= '<tr><td></td><td><b>Total</b></td><td style=text-align:center;>'.$total .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.7',
                        'title'=>'KEGIATAN RADIOLOGI',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.7',
                    'title'=>'KEGIATAN RADIOLOGI',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    public function actionMorbiditasRawatInap()
    {
        $model = new RKRl4aMorbiditasriV();
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        $criteria =  new CDbCriteria();
        $criteria->group = 'diagnosa_id,diagnosa_kode,diagnosa_nama,golonganumur_id,jeniskelamin, jmlpsnhidupmeninggal';
        $criteria->select = $criteria->group.', sum(jmlgolumur) as jmlgolumur , sum(jmljeniskelamin) as jmljeniskelamin';
        $criteria->order = 'diagnosa_id,golonganumur_id';
//        $criteria->addBetweenCondition('tglmorbiditas',$tgl_awal,$tgl_akhir);
        $records = RKRl4aMorbiditasriV::model()->findAll($criteria);
        
        $criteria2 =  new CDbCriteria();
        $criteria2->group = 'diagnosa_id,diagnosa_kode,diagnosa_nama';
        $criteria2->select = $criteria2->group.', sum(jmlgolumur) as jmlgolumur,sum(jmljeniskelamin) as jmljeniskelamin';
        $criteria2->order = 'diagnosa_id';
//        $criteria2->addBetweenCondition('tglmorbiditas',$tgl_awal,$tgl_akhir);
        $recordsGroup = RKRl4aMorbiditasriV::model()->findAll($criteria2);
        
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="3" style="text-align:center;vertical-align:middle;">No. Urut</th>', 
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">No. DTD</th>', 
            '<th rowspan="3"  style="text-align:center;vertical-align:middle;">No. Daftar Terperinci</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Golongan Sebab Penyakit</th>',
            '<th colspan="18" style="text-align:center;vertical-align:middle;">Jumlah Pasien Hidup dan Mati menurut Golongan Umur & Jenis Kelamin</th>',
            '<th colspan="2" style="text-align:center;vertical-align:middle;">Pasien Keluar (Hidup & Mati) Menurut Jenis Kelamin</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Jumlah Pasien Keluar Hidup (23+24)</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Jumlah Pasien Keluar Mati</th>',
        );
        $header2 = array(
            '<th colspan="2" style="text-align:center;">0-6hr</th>', 
            '<th colspan="2" style="text-align:center;">7-28hr</th>', 
            '<th colspan="2" style="text-align:center;">28hr-<1th</th>', 
            '<th colspan="2" style="text-align:center;">1-4th</th>', 
            '<th colspan="2" style="text-align:center;">5-14th</th>', 
            '<th colspan="2" style="text-align:center;">15-24th</th>', 
            '<th colspan="2" style="text-align:center;">25-44th</th>', 
            '<th colspan="2" style="text-align:center;">45-64th</th>', 
            '<th colspan="2" style="text-align:center;">>65</th>', 
            '<th rowspan="2" style="text-align:center;">LK</th>', 
            '<th rowspan="2" style="text-align:center;">PR</th>',
        );
        $header3 = array(
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>',              
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;background-color:#AFAFAF">13</td>',
            '<td style="text-align:center;background-color:#AFAFAF">14</td>',
            '<td style="text-align:center;background-color:#AFAFAF">15</td>',
            '<td style="text-align:center;background-color:#AFAFAF">16</td>',
            '<td style="text-align:center;background-color:#AFAFAF">17</td>',
            '<td style="text-align:center;background-color:#AFAFAF">18</td>',
            '<td style="text-align:center;background-color:#AFAFAF">19</td>',
            '<td style="text-align:center;background-color:#AFAFAF">20</td>',
            '<td style="text-align:center;background-color:#AFAFAF">21</td>',
            '<td style="text-align:center;background-color:#AFAFAF">22</td>',
            '<td style="text-align:center;background-color:#AFAFAF">23</td>',
            '<td style="text-align:center;background-color:#AFAFAF">24</td>',
            '<td style="text-align:center;background-color:#AFAFAF">25</td>',
            '<td style="text-align:center;background-color:#AFAFAF">26</td>',
        );

        $rows = array();
        $i=0;
        $total_laki = 0;
        $total_perempuan = 0;
        $total_kasus = 0;
        $total_kunjungan = 0;
        $datas = array();
        $dataGroup = array();
        foreach($records as $j=>$row)
        {
            $diagnosa = $row->diagnosa_id;
            
            $datas[$diagnosa][$row->golonganumur_id]['diagnosa_kode'] = $row->diagnosa_kode;
            $datas[$diagnosa][$row->golonganumur_id]['diagnosa_nama'] = $row->diagnosa_nama;
            $datas[$diagnosa][$row->golonganumur_id]['golonganumur_id'] = $row->golonganumur_id;
            $datas[$diagnosa][$row->golonganumur_id]['jmlgolumur'] = $row->jmlgolumur;                     
            $datas[$diagnosa][$row->golonganumur_id]['jmljeniskelamin'] = $row->jmljeniskelamin;                     
            $datas[$diagnosa][$row->golonganumur_id]['jeniskelamin'] = $row->jeniskelamin;                     
            $datas[$diagnosa][$row->golonganumur_id]['jmlpasienhidupmeninggal'] = $row->jmlpsnhidupmeninggal;
        }
        
        foreach($recordsGroup as $m=>$value)
        {
            $diagnosa = $value->diagnosa_id;
            $dataGroup[$diagnosa]['diagnosa_id'] = $value->diagnosa_id;                    
            $dataGroup[$diagnosa]['diagnosa_kode'] = $value->diagnosa_kode;                    
            $dataGroup[$diagnosa]['diagnosa_nama'] = $value->diagnosa_nama;                    
            $dataGroup[$diagnosa]['jmlgolumur'] = $value->jmlgolumur;                    
            $dataGroup[$diagnosa]['jmljeniskelamin'] = $value->jmljeniskelamin;                    
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>'.'<tr>'. implode($header3, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        
            $a = 0;
           foreach($dataGroup as $key=>$val){               
                $table .= '<tr>';
                $table .= '<td style=text-align:center;>'. ($a+1).'</td>
                    <td>'.$val['diagnosa_kode'].'</td>
                    <td>'.$val['diagnosa_kode'].'</td>
                    <td>'.$val['diagnosa_nama'].'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BATITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BATITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BAYI,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BAYI,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BALITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BALITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_REMAJA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_REMAJA,'PEREMPUAN').'</td>  
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_DEWASA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_DEWASA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ORANG_TUA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ORANG_TUA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_MANULA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_MANULA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumKeluar($val['diagnosa_id'],'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumKeluar($val['diagnosa_id'],'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getTotalKeluar($val['diagnosa_id'],'HIDUP').'</td>
                    <td style=text-align:center;>'.$model->getTotalKeluar($val['diagnosa_id'],'MATI').'</td></tr>';
                $a++;
                $total += $jmloperasi;
                $total_khusus += $jmlkhusus;
                $total_besar += $jmlbesar;
                $total_sedang += $jmlsedang;
                $total_kecil += $jmlkecil;

        } 
        $table .= '<tr><td></td><td></td><td></td><td><b>Total</b></td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BATITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BATITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BAYI,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BAYI,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BALITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BALITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_REMAJA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_REMAJA,'PEREMPUAN').'</td>  
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_DEWASA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_DEWASA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ORANG_TUA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ORANG_TUA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_MANULA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_MANULA,'PEREMPUAN').'</td>'
                . '<td style=text-align:center;>'.$model->getSumKeluarTotal('LAKI-LAKI').'</td>'
                . '<td style=text-align:center;>'.$model->getSumKeluarTotal('PEREMPUAN').'</td>'
                . '<td style=text-align:center;>'.$model->getSumTotalKeluar('HIDUP').'</td>'
                . '<td style=text-align:center;>'.$model->getSumTotalKeluar('MATI').'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'1024',
                        'formulir'=>'Formulir RL 4.A',
                        'title'=>'DATA KEADAAN MORBIDITAS PASIEN RAWAT INAP RUMAH SAKIT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            
            $this->render('print_laporan',
                array(
                    'width'=>'1024',
                    'formulir'=>'Formulir RL 4.A',
                    'title'=>'DATA KEADAAN MORBIDITAS PASIEN RAWAT INAP RUMAH SAKIT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
    
    public function actionMorbiditasRawatJalan()
    {
        $model = new RKRl4bMorbiditasrwtjlnV();
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
        $criteria =  new CDbCriteria();
        $criteria->group = 'diagnosa_id,diagnosa_kode,diagnosa_nama,golonganumur_id,jeniskelamin, kasusdiagnosa';
        $criteria->select = $criteria->group.', sum(jmlgolumur) as jmlgolumur , sum(jmljeniskelamin) as jmljeniskelamin';
        $criteria->order = 'diagnosa_id,golonganumur_id';
//        $criteria->addBetweenCondition('tglmorbiditas',$tgl_awal,$tgl_akhir);
        $records = RKRl4bMorbiditasrwtjlnV::model()->findAll($criteria);
        
        $criteria2 =  new CDbCriteria();
        $criteria2->group = 'diagnosa_id,diagnosa_kode,diagnosa_nama';
        $criteria2->select = $criteria2->group.', sum(jmlgolumur) as jmlgolumur,sum(jmljeniskelamin) as jmljeniskelamin';
        $criteria2->order = 'diagnosa_id';
//        $criteria2->addBetweenCondition('tglmorbiditas',$tgl_awal,$tgl_akhir);
        $recordsGroup = RKRl4bMorbiditasrwtjlnV::model()->findAll($criteria2);
        
        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="3" style="text-align:center;vertical-align:middle;">No. Urut</th>', 
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">No. DTD</th>', 
            '<th rowspan="3"  style="text-align:center;vertical-align:middle;">No. Daftar Terperinci</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Golongan Sebab Penyakit</th>',
            '<th colspan="18" style="text-align:center;vertical-align:middle;">Jumlah Pasien Kasus Menurut Golongan Umur & Jenis Kelamin</th>',
            '<th colspan="2" style="text-align:center;vertical-align:middle;">Kasus Baru Menurut Jenis Kelamin</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Jumlah Kasus Baru(23+24)</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">Jumlah Kunjungan</th>',
        );
        $header2 = array(
            '<th colspan="2" style="text-align:center;">0-6hr</th>', 
            '<th colspan="2" style="text-align:center;">7-28hr</th>', 
            '<th colspan="2" style="text-align:center;">28hr-<1th</th>', 
            '<th colspan="2" style="text-align:center;">1-4th</th>', 
            '<th colspan="2" style="text-align:center;">5-14th</th>', 
            '<th colspan="2" style="text-align:center;">15-24th</th>', 
            '<th colspan="2" style="text-align:center;">25-44th</th>', 
            '<th colspan="2" style="text-align:center;">45-64th</th>', 
            '<th colspan="2" style="text-align:center;">>65</th>', 
            '<th rowspan="2" style="text-align:center;">LK</th>', 
            '<th rowspan="2" style="text-align:center;">PR</th>',
        );
        $header3 = array(
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>', 
            '<th style="text-align:center;">L</th>', 
            '<th style="text-align:center;">P</th>',              
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;background-color:#AFAFAF">13</td>',
            '<td style="text-align:center;background-color:#AFAFAF">14</td>',
            '<td style="text-align:center;background-color:#AFAFAF">15</td>',
            '<td style="text-align:center;background-color:#AFAFAF">16</td>',
            '<td style="text-align:center;background-color:#AFAFAF">17</td>',
            '<td style="text-align:center;background-color:#AFAFAF">18</td>',
            '<td style="text-align:center;background-color:#AFAFAF">19</td>',
            '<td style="text-align:center;background-color:#AFAFAF">20</td>',
            '<td style="text-align:center;background-color:#AFAFAF">21</td>',
            '<td style="text-align:center;background-color:#AFAFAF">22</td>',
            '<td style="text-align:center;background-color:#AFAFAF">23</td>',
            '<td style="text-align:center;background-color:#AFAFAF">24</td>',
            '<td style="text-align:center;background-color:#AFAFAF">25</td>',
            '<td style="text-align:center;background-color:#AFAFAF">26</td>',
        );

        $rows = array();
        $i=0;
        $total_laki = 0;
        $total_perempuan = 0;
        $total_kasus = 0;
        $total_kunjungan = 0;
        $datas = array();
        $dataGroup = array();
        foreach($records as $j=>$row)
        {
            $diagnosa = $row->diagnosa_id;
            
            $datas[$diagnosa][$row->golonganumur_id]['diagnosa_kode'] = $row->diagnosa_kode;
            $datas[$diagnosa][$row->golonganumur_id]['diagnosa_nama'] = $row->diagnosa_nama;
            $datas[$diagnosa][$row->golonganumur_id]['golonganumur_id'] = $row->golonganumur_id;
            $datas[$diagnosa][$row->golonganumur_id]['jmlgolumur'] = $row->jmlgolumur;                     
            $datas[$diagnosa][$row->golonganumur_id]['jmljeniskelamin'] = $row->jmljeniskelamin;                     
            $datas[$diagnosa][$row->golonganumur_id]['jeniskelamin'] = $row->jeniskelamin;                     
            $datas[$diagnosa][$row->golonganumur_id]['kasusdiagnosa'] = $row->kasusdiagnosa;
        }
        
        foreach($recordsGroup as $m=>$value)
        {
            $diagnosa = $value->diagnosa_id;
            $dataGroup[$diagnosa]['diagnosa_id'] = $value->diagnosa_id;                    
            $dataGroup[$diagnosa]['diagnosa_kode'] = $value->diagnosa_kode;                    
            $dataGroup[$diagnosa]['diagnosa_nama'] = $value->diagnosa_nama;                    
            $dataGroup[$diagnosa]['jmlgolumur'] = $value->jmlgolumur;                    
            $dataGroup[$diagnosa]['jmljeniskelamin'] = $value->jmljeniskelamin;                    
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>'.'<tr>'. implode($header3, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        
            $a = 0;
           foreach($dataGroup as $key=>$val){               
                $table .= '<tr>';
                $table .= '<td style=text-align:center;>'. ($a+1).'</td>
                    <td>'.$val['diagnosa_kode'].'</td>
                    <td>'.$val['diagnosa_kode'].'</td>
                    <td>'.$val['diagnosa_nama'].'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BATITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BATITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BAYI,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BAYI,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BALITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_BALITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_REMAJA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_REMAJA,'PEREMPUAN').'</td>  
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_DEWASA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_DEWASA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ORANG_TUA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_ORANG_TUA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_MANULA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumDiagnosa($val['diagnosa_id'],PARAMS::GOLONGAN_UMUR_MANULA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumKeluar($val['diagnosa_id'],'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumKeluar($val['diagnosa_id'],'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getTotalKeluar($val['diagnosa_id'],'KASUS BARU').'</td>
                    <td style=text-align:center;>'.$model->getTotalKeluar($val['diagnosa_id'],'KASUS BARU').'</td></tr>';
                $a++;
                $total += $jmloperasi;
                $total_khusus += $jmlkhusus;
                $total_besar += $jmlbesar;
                $total_sedang += $jmlsedang;
                $total_kecil += $jmlkecil;

        } 
        $table .= '<tr><td></td><td></td><td></td><td><b>Total</b></td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BATITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BATITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BARU_LAHIR,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BAYI,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BAYI,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BALITA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_BALITA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ANAK_ANAK,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_REMAJA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_REMAJA,'PEREMPUAN').'</td>  
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_DEWASA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_DEWASA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ORANG_TUA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_ORANG_TUA,'PEREMPUAN').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_MANULA,'LAKI-LAKI').'</td>
                    <td style=text-align:center;>'.$model->getSumTotalDiagnosa(PARAMS::GOLONGAN_UMUR_MANULA,'PEREMPUAN').'</td>'
                . '<td style=text-align:center;>'.$model->getSumKeluarTotal('LAKI-LAKI').'</td>'
                . '<td style=text-align:center;>'.$model->getSumKeluarTotal('PEREMPUAN').'</td>'
                . '<td style=text-align:center;>'.$model->getSumTotalKeluar('KASUS BARU').'</td>'
                . '<td style=text-align:center;>'.$model->getSumTotalKeluar('KASUS BARU').'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'1024',
                        'formulir'=>'Formulir RL 4.B',
                        'title'=>'DATA KEADAAN MORBIDITAS PASIEN RAWAT JALAN RUMAH SAKIT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            
            $this->render('print_laporan',
                array(
                    'width'=>'1024',
                    'formulir'=>'Formulir RL 4.A',
                    'title'=>'DATA KEADAAN MORBIDITAS PASIEN RAWAT JALAN RUMAH SAKIT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
     public function actionKetenagaan()
    {
        $model = new RKrl2KetenagaanV;
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
     
        $criteria=new CDbCriteria;
        $criteria->select = 'kelompokpegawai_id, kelompokpegawai_nama, jeniskelamin, pendkualifikasi_id, pendkualifikasi_nama, jmlkeadaanskrg, jmlkeblaki, jmlkebperempuan';
        $criteria->addCondition('pendkualifikasi_id IS NOT NULL');
        $criteria->group = 'kelompokpegawai_id, kelompokpegawai_nama, jeniskelamin, pendkualifikasi_id, pendkualifikasi_nama, jmlkeadaanskrg, jmlkeblaki, jmlkebperempuan';
        $criteria->order = 'kelompokpegawai_nama asc';

        $records = RKrl2KetenagaanV::model()->findAll($criteria);

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;">NO KODE</th>', 
            '<th rowspan="2" style="text-align:center;">KUALIFIKASI PENDIDIKAN</th>', 
            '<th colspan="2" style="text-align:center;">KEADAAN</th>',
            '<th colspan="2" style="text-align:center;">KEBUTUHAN</th>',
            '<th colspan="2" style="text-align:center;">KEKURANGAN</th>',
        );
        $header2 = array(
            '<th style="text-align:center;">Laki-Laki</th>', 
            '<th style="text-align:center;">Perempuan</th>',
            '<th style="text-align:center;">Laki-Laki</th>', 
            '<th style="text-align:center;">Perempuan</th>',
            '<th style="text-align:center;">Laki-Laki</th>', 
            '<th style="text-align:center;">Perempuan</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
        );

        $rows = array();
        $i=0;
        $total_jumlah = 0;

        // $kelompokpegawai_nama[-1] = '';
        $no = 0;
        $subno = 0;
        $kurangLaki = 0;
        $kurangPerempuan = 0;
        foreach($records as $row)
        {
            // $kelompokpegawai_nama[$row] = $value['kelompokpegawai_nama'];
            // $kelompokpegawai_nama[$row] = $value['kelompokpegawai_nama'];

            // echo $kelompokpegawai_nama[$row]; exit;
            //if($kelompokpegawai_nama[$row]!=$kelompokpegawai_nama[$val-0]){
            $no++;
            $totalLaki = ($row['jmlkeblaki'] - $model->getSumKeadaan($row['pendkualifikasi_id'], PARAMS::JENIS_KELAMIN_LAKI_LAKI));
            $totalPerempuan = ($row['jmlkeblaki'] - $model->getSumKeadaan($row['pendkualifikasi_id'], PARAMS::JENIS_KELAMIN_PEREMPUAN));
            if ($totalLaki < 0){
                $totalLaki = 0;
            } else {
                $kurangLaki = $kurangLaki + $totalLaki;
            }
            if ($totalPerempuan < 0){
                $totalPerempuan = 0;
            } else {
                $kurangPerempuan = $kurangPerempuan +$totalPerempuan;
            }
            $rows[] = array(
                '<td style=text-align:center;>'.$no.'</td>',
                '<td>'.$row['pendkualifikasi_nama'].'</td>',
                '<td>'.$model->getSumKeadaan($row['pendkualifikasi_id'],PARAMS::JENIS_KELAMIN_LAKI_LAKI).'</td>',
                '<td>'.$model->getSumKeadaan($row['pendkualifikasi_id'],PARAMS::JENIS_KELAMIN_PEREMPUAN).'</td>',
                '<td>'.$row['jmlkeblaki'].'</td>',
                '<td>'.$row['jmlkebperempuan'].'</td>',
                '<td>'.$totalLaki.'</td>',
                '<td>'.$totalPerempuan.'</td>',
              
            );
           // }
            // else{

            //     $no++;
            //     $subno++;
            //     $rows[] = array(

            //     '<td style=text-align:center;>'.($no+1).'.'.$subno.'</td>',
            //     '<td>'.$value['pendkualifikasi_nama'].'</td>',
            //     '<td>'.''.'</td>',
            //     '<td>'.''.'</td>',
            //     '<td>'.''.'</td>',
            //     '<td>'.''.'</td>',
            //     '<td>'.''.'</td>',
            //     '<td>'.''.'</td>',
              
            // );
            // }
            //$i++;
            // $total_jumlah += $row['jlmtt'];
        }


        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
         $table .= '<tr><td></td><td><b>Total</b></td><td>' . $model->getSumTotal(PARAMS::JENIS_KELAMIN_LAKI_LAKI) .'</td><td>'
                                                            . $model->getSumTotal(PARAMS::JENIS_KELAMIN_PEREMPUAN) .'</td><td>'
                                                            . $model->getSumkebutuhanL() .'</td><td>'
                                                            . $model->getSumkebutuhanP() .'</td><td>'
                                                            . $kurangLaki .'</td><td>'
                                                            . $kurangPerempuan .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 2',
                        'title'=>'KETENAGAAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 2',
                    'title'=>'KETENAGAAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }

     public function actionkunjunganRD()
    {
        $model = new RKrl23KunjunganrdV;
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
     
        $criteria=new CDbCriteria;
        $criteria->select = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama';
        $criteria->group = 'jeniskasuspenyakit_id, jeniskasuspenyakit_nama';
        // $criteria->order = 'jeniskasuspenyakit_nama asc';

        $records = RKrl23KunjunganrdV::model()->findAll($criteria);

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;">NO</th>', 
            '<th rowspan="2" style="text-align:center;">JENIS PELAYANAN</th>', 
            '<th colspan="2" style="text-align:center;">TOTAL PASIEN</th>',
            '<th colspan="3" style="text-align:center;">TINDAK LANJUT PELAYANAN</th>',
            '<th rowspan="2" style="text-align:center;">MENINGGAL</th>',
            '<th rowspan="2" style="text-align:center;">DOA</th>',
        );
        $header2 = array(
            '<th style="text-align:center;">RUJUKAN</th>', 
            '<th style="text-align:center;">NON RUJUKAN</th>',
            '<th style="text-align:center;">DIRAWAT</th>', 
            '<th style="text-align:center;">DIRUJUK</th>',
            '<th style="text-align:center;">PULANG</th>', 
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
        );


        $rows = array();
        $i=0;
        $total_jumlah = 0;
        $no = 0;
        $subno = 0;

        foreach($records as $row)
        {
                $no++;
            $rows[] = array(
                '<td style=text-align:center;>'.$no.'</td>',
                        '<td>'.$row['jeniskasuspenyakit_nama'].'</td>',
                        '<td>'.$model->getTotPasien(PARAMS::STATUS_RUJUKAN, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getTotPasien(PARAMS::STATUS_NON_RUJUKAN, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getSumCarakeluar(PARAMS::CARA_KELUAR_RI, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getSumCarakeluar(PARAMS::CARA_KELUAR_DIRUJUK, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getSumCarakeluar(PARAMS::CARA_KELUAR_DIPULANGKAN, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getSumCarakeluar(PARAMS::CARA_KELUAR_MENINGGAL, $row['jeniskasuspenyakit_id']).'</td>',
                        '<td>'.$model->getSumCarakeluar(PARAMS::CARA_KELUAR_MELARIKAN_DIRI, $row['jeniskasuspenyakit_id']).'</td>',
            );
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
         $table .= '<tr><td style=text-align:center;>99</td><td><b>Total</b></td><td>' . $model->getTotalStatus(PARAMS::STATUS_RUJUKAN) .'</td><td>'
                                                            . $model->getTotalStatus(PARAMS::STATUS_NON_RUJUKAN) .'</td><td>'
                                                            . $model->getTotalCaraKeluar(PARAMS::CARA_KELUAR_RI) .'</td><td>'
                                                            . $model->getTotalCaraKeluar(PARAMS::CARA_KELUAR_DIRUJUK) .'</td><td>'
                                                            . $model->getTotalCaraKeluar(PARAMS::CARA_KELUAR_DIPULANGKAN) .'</td><td>'
                                                            . $model->getTotalCaraKeluar(PARAMS::CARA_KELUAR_MENINGGAL) .'</td><td>'
                                                            . $model->getTotalCaraKeluar(PARAMS::CARA_KELUAR_MELARIKAN_DIRI) .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.2',
                        'title'=>'KUNJUNGAN RAWAT DARURAT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.2',
                    'title'=>'KUNJUNGAN RAWAT DARURAT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }

    /**
     * Formulir RL 1.3 KEGIATAN KESEHATAN GIGI DAN MULUT
     */
 public function actiongigiMulut()
    {
        $model = new RKrl33KeggigimulutV;
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        
     
        $criteria=new CDbCriteria;
        $criteria->select = 'kategoritindakan_nama, kategoritindakan_namalainnya, jmlpelayanan';
        $criteria->group = 'kategoritindakan_nama, kategoritindakan_namalainnya, jmlpelayanan';
        $criteria->order = 'kategoritindakan_nama asc';

        $records = RKrl33KeggigimulutV::model()->findAll($criteria);

        $periode = date('d M Y', strtotime($tgl_awal)) . ' sd ' . date('d M Y', strtotime($tgl_akhir));
        $header = array(
            '<th width="50" style="text-align:center;">NO</th>', 
            '<th style="text-align:center;">JENIS KEGIATAN</th>', 
            '<th style="text-align:center;">JUMLAH</th>',
        );

        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $i=0;
        $total_jumlah = 0;
        $no = 0;
        $subno = 0;

        foreach($records as $row)
        {
                $no++;
            $rows[] = array(
                '<td style=text-align:center;>'.$no.'</td>',
                '<td>'.$row['kategoritindakan_nama'].'</td>',
                '<td>'.$row['jmlpelayanan'].'</td>',
            );
            $total_jumlah += $row['jmlpelayanan'];
        }


        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $xxx)
        {
            $table .= '<tr>'. implode($xxx, '') . '<tr>';
        }
        $table .= '<tr><td style=text-align:center;>99</td><td><b>Total</b></td><td style="background-color:#AFAFAF">' . $total_jumlah .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">NO</th>', '<th style="text-align:center;">Jenis Kegiatan</th>', '<th style="text-align:center;">Jumlah</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.1',
                        'title'=>'KEGIATAN KESEHATAN GIGI DAN MULUT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.1',
                    'title'=>'KEGIATAN KESEHATAN GIGI DAN MULUT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }

    }
    
    /**
     * Formulir RL 1.2 INDIKATOR PELAYANAN RUMAH SAKIT
     */
    
    public function actionKegiatanPelayananRS()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria=new CDbCriteria;

        $criteria->select = 'tahun, nokode_rumahsakit,nama_rumahsakit,sum(bor) as bor,sum(los) as los,sum(bto) as bto,
                            sum(toi) as toi,((sum(jumlahpasienmati48jam)/sum(jumlahpasienkeluar))*0.1) as ndr,((sum(jumlahpasienmati)/sum(jumlahpasienkeluar))*0.1) as gdr,sum(kunjunganperhari) as kunjunganperhari';
        $criteria->group = 'tahun,nokode_rumahsakit,nama_rumahsakit';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        
        $records = RMRl12KegiatanpelayananrumahsakitV::model()->findAll($criteria);
        $header = array(
            '<th style="text-align:center;vertical-align:middle;">Tahun</th>', 
            '<th style="text-align:center;vertical-align:middle;">BOR</th>', 
            '<th style="text-align:center;vertical-align:middle;">ALOS</th>',
            '<th style="text-align:center;vertical-align:middle;">BTO</th>',
            '<th style="text-align:center;vertical-align:middle;">TOI</th>',
            '<th style="text-align:center;vertical-align:middle;">NDR</th>',
            '<th style="text-align:center;vertical-align:middle;">GDR</th>',
            '<th style="text-align:center;vertical-align:middle;">Rata-rata<br/>Kunjungan /hari</th>'
        );
        $tdCount = array(
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">8</td>',
        );

        $rows = array();
        $i=0;
        $total_lh= 0;
        $total_lm= 0;
        $total_ph= 0;
        $total_pm= 0;
        $total_jmlpasien= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $tahun = $row->tahun;
            
            $datas[$tahun]['tahun']= $row->tahun;            
            $datas[$tahun]['thn'] = $row->tahun;
            $datas[$tahun]['bor'] = number_format($row->bor,2,",",",")."%";
            $datas[$tahun]['los'] = number_format($row->los,0,"","")." hari";
            $datas[$tahun]['bto'] = number_format($row->bto,0,"","")." kali";
            $datas[$tahun]['toi'] = number_format($row->toi,0,"","")." hari";
            $datas[$tahun]['ndr'] = number_format($row->ndr,3,",",",")."%";
            $datas[$tahun]['gdr'] = number_format($row->gdr,3,",",",")."%";
            $datas[$tahun]['kunjungan'] = $row->kunjunganperhari;
                     
        }
        
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td style=text-align:center;>'. $data['tahun'].'</td>',
                    '<td style=text-align:center;>'.$data['bor'].'</td>',
                    '<td style="text-align:center;width:100px;">'.$data['los'].'</td>',
                    '<td style="text-align:center;width:100px;">'.$data['bto'].'</td>',
                    '<td style="text-align:center;width:100px;">'.$data['toi'].'</td>',
                    '<td style=text-align:center;>'.$data['ndr'].'</td>',
                    '<td style=text-align:center;>'.$data['gdr'].'</td>',
                    '<td style=text-align:center;>'.$data['kunjungan'].'</td>',                    
                );
            }   
        }else{
            $rows[] = array(
                    '<td colspan=8><i>Data tidak ditemukan.</i></td>',                
                );
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 1.2',
                        'title'=>'INDIKATOR PELAYANAN RUMAH SAKIT',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 1.2',
                    'title'=>'INDIKATOR PELAYANAN RUMAH SAKIT',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.1 KEGIATAN PELAYANAN RAWAT INAP
     */
    
    public function actionKegiatanPelayananRawatInap()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria=new CDbCriteria;

        $criteria->select = 'tahun,nokode_rumahsakit,nama_rumahsakit,jeniskasuspenyakit_id,jeniskasuspenyakit_nama,sum(pasienawaltahun) as pasienawaltahun,
                            sum(pasienmasuk)as pasienmasuk,sum(pasienkeluar) as pasienkeluar,sum(pasienmatikurang48jam) as pasienmatikurang48jam,sum(pasienmatilebih48jam) as pasienmatilebih48jam,
                            sum(lamadirawat) as lamadirawat,sum(pasienakhirtahun) as pasienakhirtahun,sum(hariperawatan) as hariperawatan,sum(rincianhariperawatanvvip) as rincianhariperawatanvvip,sum(rincianhariperawatanvip) as rincianhariperawatanvip,0::double precision as rincianhariperawatan1,0::double precision as rincianhariperawatan2,
                            0::double precision as rincianhariperawatan3,sum(rincianhariperawatankelaskhusus) as rincianhariperawatankelaskhusus';
        $criteria->group = 'tahun,nokode_rumahsakit,nama_rumahsakit,jeniskasuspenyakit_id,'
                . '         jeniskasuspenyakit_nama';
        $criteria->order = 'tahun,nokode_rumahsakit,nama_rumahsakit,jeniskasuspenyakit_nama asc';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        
        $records = RMRl31KegiatanpelayananrawatinapV::model()->findAll($criteria);
        $header = array(
            '<th width="50" rowspan=2 style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">JENIS PELAYANAN</th>', 
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">PASIEN AWAL TAHUN</th>',
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">PASIEN MASUK</th>',
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">PASIEN KELUAR HIDUP</th>',
            '<th colspan=2 style="text-align:center;vertical-align:middle;">PASIEN KELUAR MATI</th>',
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">JUMLAH LAMA DIRAWAT</th>',
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">PASIEN AKHIR TAHUN</th>',
            '<th rowspan=2 style="text-align:center;vertical-align:middle;">JUMLAH HARI PERAWATAN</th>',
            '<th colspan=6 style="text-align:center;vertical-align:middle;">RINCIAN HARI PERAWATAN PER KELAS</th>',
        );
        $header2 = array(
            '<th style="text-align:center;"> < 48 Jam</th>', 
            '<th style="text-align:center;"> >= 48 Jam</th>',
            '<th style="text-align:center;">VVIP</th>', 
            '<th style="text-align:center;">VIP</th>',
            '<th style="text-align: center;">I</th>', 
            '<th style="text-align:center;">II</th>', 
            '<th style="text-align:center;">III</th>', 
            '<th style="text-align:center;">Kelas Khusus</th>', 
        );
        $tdCount = array(
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">13</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">14</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">15</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">16</td>',
        );

        $rows = array();
        $i=0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $jeniskasuspenyakit_id = $row->jeniskasuspenyakit_id;
            
            $datas[$jeniskasuspenyakit_id]['jeniskasuspenyakit_id']= $row->jeniskasuspenyakit_id;            
            $datas[$jeniskasuspenyakit_id]['jeniskasuspenyakit_nama'] = $row->jeniskasuspenyakit_nama;
            $datas[$jeniskasuspenyakit_id]['pasienawaltahun'] = $row->pasienawaltahun;
            $datas[$jeniskasuspenyakit_id]['pasienmasuk'] = $row->pasienmasuk;
            $datas[$jeniskasuspenyakit_id]['pasienkeluar'] = $row->pasienkeluar;
            $datas[$jeniskasuspenyakit_id]['pasienmatikurang48jam'] = $row->pasienmatikurang48jam;
            $datas[$jeniskasuspenyakit_id]['pasienmatilebih48jam'] = $row->pasienmatilebih48jam;
            $datas[$jeniskasuspenyakit_id]['lamadirawat'] = $row->lamadirawat;
            $datas[$jeniskasuspenyakit_id]['pasienakhirtahun'] = $row->pasienakhirtahun;
            $datas[$jeniskasuspenyakit_id]['hariperawatan'] = $row->hariperawatan;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatanvvip'] = $row->rincianhariperawatanvvip;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatanvip'] = $row->rincianhariperawatanvip;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatan1'] = $row->rincianhariperawatan1;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatan2'] = $row->rincianhariperawatan2;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatan3'] = $row->rincianhariperawatan3;
            $datas[$jeniskasuspenyakit_id]['rincianhariperawatankelaskhusus'] = $row->rincianhariperawatankelaskhusus;
                     
        }
                
        if(count($datas) > 0){
            $total_awaltahun = 0;
            $total_masuk = 0;
            $total_keluarhidup = 0;
            $total_kurang48 = 0;
            $total_lebih48 = 0;
            $total_lamarawat = 0;
            $total_akhirtahun = 0;
            $total_perawatan = 0;
            $total_vvip = 0;
            $total_vip = 0;
            $total_1 = 0;
            $total_2 = 0;
            $total_3 = 0;
            $total_kelaskhusus = 0;
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['jeniskasuspenyakit_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienawaltahun'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienmasuk'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienkeluar'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienmatikurang48jam'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienmatilebih48jam'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['lamadirawat'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienakhirtahun'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['hariperawatan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatanvvip'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatanvip'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatan1'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatan2'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatan3'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rincianhariperawatankelaskhusus'].'</td>',
                );
                $i++;
                $total_awaltahun += $data['pasienawaltahun'];
                $total_masuk += $data['pasienmasuk'];
                $total_keluarhidup += $data['pasienkeluar'];
                $total_kurang48 += $data['pasienmatikurang48jam'];
                $total_lebih48 += $data['pasienmatilebih48jam'];
                $total_lamarawat += $data['lamadirawat'];
                $total_akhirtahun += $data['pasienakhirtahun'];
                $total_perawatan += $data['hariperawatan'];
                $total_vvip += $data['rincianhariperawatanvvip'];
                $total_vip += $data['rincianhariperawatanvip'];
                $total_1 += $data['rincianhariperawatan1'];
                $total_2 += $data['rincianhariperawatan2'];
                $total_3 += $data['rincianhariperawatan3'];
                $total_kelaskhusus += $data['rincianhariperawatankelaskhusus'];
            }   
        }else{
            $rows[] = array(
                    '<td colspan=8><i>Data tidak ditemukan.</i></td>',                
                );
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style=text-align:center;>99</td><td><b>Total</b></td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_awaltahun .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_masuk .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_keluarhidup .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_kurang48 .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_lebih48 .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_lamarawat .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_akhirtahun .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_perawatan .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_vvip .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_vip .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_1 .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_2 .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_3 .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_kelaskhusus .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.1',
                        'title'=>'KEGIATAN PELAYANAN RAWAT INAP',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.1',
                    'title'=>'KEGIATAN PELAYANAN RAWAT INAP',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.4 KEGIATAN KEBIDANAN
     */
    
    public function actionKegiatanKebidanan()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria=new CDbCriteria;

        $criteria->select = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_nama,
                            sum(rujukanrs) as rujukanrs,sum(rujukanbidan) as rujukanbidan,sum(rujukanpuskemas) as rujukanpuskemas,sum(rujukanfaskeslainnya) as rujukanfaskeslainnya,sum(rujukanjumlahhidup) as rujukanjumlahhidup,
                            sum(rujukanjumlahmati) as rujukanjumlahmati,sum(rujukanjumlahtotal) as rujukanjumlahtotal,sum(rujukannonmedisjumlahhidup) as rujukannonmedisjumlahhidup,sum(rujukannonmedisjumlahmati) as rujukannonmedisjumlahmati,sum(rujukannonmedisjumlahtotal) as rujukannonmedisjumlahtotal,sum(nonrujukanjumlahhidup) as nonrujukanjumlahhidup,
                            sum(nonrujukanjumlahmati) as nonrujukanjumlahmati,sum(nonrujukanjumlahtotal) as nonrujukanjumlahtotal';
        $criteria->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_nama';
        $criteria->order = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_nama asc';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        
        $records = RMRl34KegiatankebidananV::model()->findAll($criteria);
        $header = array(
            '<th rowspan=3 style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th rowspan=3 style="text-align:center;vertical-align:middle;">JENIS KEGIATAN</th>', 
            '<th colspan=10 style="text-align:center;vertical-align:middle;">RUJUKAN</th>',
            '<th colspan=3 rowspan=2 style="text-align:center;vertical-align:middle;">NON RUJUKAN</th>',
            '<th rowspan=3 style="text-align:center;vertical-align:middle;">DIRUJUK</th>',
        );
        $header2 = array(
            '<th colspan=7 style="text-align:center;">MEDIS</th>', 
            '<th colspan=3 style="text-align:center;">NON MEDIS</th>',
        );
        $header3 = array(
            '<th style="text-align:center;">RUMAH SAKIT</th>', 
            '<th style="text-align:center;">BIDAN</th>',
            '<th style="text-align:center;">PUSKESMAS</th>',
            '<th style="text-align:center;">FASKES LAINNYA</th>',
            '<th style="text-align:center;">JUMLAH HIDUP</th>',
            '<th style="text-align:center;">JUMLAH MATI</th>',
            '<th style="text-align:center;">JUMLAH TOTAL</th>',
            '<th style="text-align:center;">JUMLAH HIDUP</th>',
            '<th style="text-align:center;">JUMLAH MATI</th>',
            '<th style="text-align:center;">JUMLAH TOTAL</th>',
            '<th style="text-align:center;">JUMLAH HIDUP</th>',
            '<th style="text-align:center;">JUMLAH MATI</th>',
            '<th style="text-align:center;">JUMLAH TOTAL</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">13</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">14</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">15</td>',
            '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">16</td>',
        );

        $rows = array();
        $i=0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan_id = $row->daftartindakan_id;
            
            $datas[$daftartindakan_id]['daftartindakan_id']= $row->daftartindakan_id;            
            $datas[$daftartindakan_id]['daftartindakan_nama']= $row->daftartindakan_nama;            
            $datas[$daftartindakan_id]['rujukanrs']= $row->rujukanrs;            
            $datas[$daftartindakan_id]['rujukanbidan'] = $row->rujukanbidan;
            $datas[$daftartindakan_id]['rujukanpuskemas'] = $row->rujukanpuskemas;
            $datas[$daftartindakan_id]['rujukanfaskeslainnya'] = $row->rujukanfaskeslainnya;
            $datas[$daftartindakan_id]['rujukanjumlahhidup'] = $row->rujukanjumlahhidup;
            $datas[$daftartindakan_id]['rujukanjumlahmati'] = $row->rujukanjumlahmati;
            $datas[$daftartindakan_id]['rujukanjumlahtotal'] = $row->rujukanjumlahtotal;
            $datas[$daftartindakan_id]['rujukannonmedisjumlahhidup'] = $row->rujukannonmedisjumlahhidup;
            $datas[$daftartindakan_id]['rujukannonmedisjumlahmati'] = $row->rujukannonmedisjumlahmati;
            $datas[$daftartindakan_id]['rujukannonmedisjumlahtotal'] = $row->rujukannonmedisjumlahtotal;
            $datas[$daftartindakan_id]['nonrujukanjumlahhidup'] = $row->nonrujukanjumlahhidup;
            $datas[$daftartindakan_id]['nonrujukanjumlahmati'] = $row->nonrujukanjumlahmati;
            $datas[$daftartindakan_id]['nonrujukanjumlahtotal'] = $row->nonrujukanjumlahtotal;
            $datas[$daftartindakan_id]['dirujuk'] = $row->rujukanrs+$row->rujukanbidan+$row->rujukanpuskemas+$row->rujukanfaskeslainnya+$row->rujukanjumlahhidup+$row->rujukanjumlahmati+$row->rujukanjumlahtotal;
                     
        }
        
        $total_rujukanrs = 0;
        $total_rujukanbidan = 0;
        $total_rujukanpuskemas = 0;
        $total_rujukanfaskeslainnya = 0;
        $total_rujukanjumlahhidup = 0;
        $total_rujukanjumlahmati = 0;
        $total_rujukanjumlahtotal = 0;
        $total_rujukannonmedisjumlahhidup = 0;
        $total_rujukannonmedisjumlahmati = 0;
        $total_rujukannonmedisjumlahtotal = 0;
        $total_nonrujukanjumlahhidup = 0;
        $total_nonrujukanjumlahmati = 0;
        $total_nonrujukanjumlahtotal = 0;
        $total_dirujuk = 0;
            
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanrs'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanbidan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanpuskemas'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanfaskeslainnya'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanjumlahhidup'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanjumlahmati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanjumlahtotal'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukannonmedisjumlahhidup'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukannonmedisjumlahmati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukannonmedisjumlahtotal'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['nonrujukanjumlahhidup'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['nonrujukanjumlahmati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['nonrujukanjumlahtotal'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['dirujuk'].'</td>',
                );
                $i++;
                $total_rujukanrs += $data['rujukanrs'];
                $total_rujukanbidan += $data['rujukanbidan'];
                $total_rujukanpuskemas += $data['rujukanpuskemas'];
                $total_rujukanfaskeslainnya += $data['rujukanfaskeslainnya'];
                $total_rujukanjumlahhidup += $data['rujukanjumlahhidup'];
                $total_rujukanjumlahmati += $data['rujukanjumlahmati'];
                $total_rujukanjumlahtotal += $data['rujukanjumlahtotal'];
                $total_rujukannonmedisjumlahhidup += $data['rujukannonmedisjumlahhidup'];
                $total_rujukannonmedisjumlahmati += $data['rujukannonmedisjumlahmati'];
                $total_rujukannonmedisjumlahtotal += $data['rujukannonmedisjumlahtotal'];
                $total_nonrujukanjumlahhidup += $data['nonrujukanjumlahhidup'];
                $total_nonrujukanjumlahmati += $data['nonrujukanjumlahmati'];
                $total_nonrujukanjumlahtotal += $data['nonrujukanjumlahtotal'];
                $total_dirujuk += $data['dirujuk'];
            }   
        }else{
            $rows[] = array(
                    '<td colspan=8><i>Data tidak ditemukan.</i></td>',                
                );
        }

        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'.implode($header2,"").'</tr>'.'<tr>'.implode($header3,"").'</tr>';
        $table .= '</tr>';
        $table .= '</thead>';
        $table .= '<tbody>';
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style=text-align:center;>99</td><td><b>Total</b></td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanrs .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanbidan .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanpuskemas .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanfaskeslainnya .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanjumlahhidup .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanjumlahmati .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukanjumlahtotal .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukannonmedisjumlahhidup .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukannonmedisjumlahmati .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_rujukannonmedisjumlahtotal .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_nonrujukanjumlahhidup .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_nonrujukanjumlahmati .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_nonrujukanjumlahtotal .'</td><td style="background-color:#AFAFAF;text-align:center;vertical-align:middle;">' . $total_dirujuk .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
//            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//            $mpdf = new MyPDF('',$ukuranKertasPDF); 
//            $mpdf->useOddEven = 2;
//            $mpdf->AddPage('P','','','','',5,5,5,5,5,5);
            $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('c',$ukuranKertasPDF); 
    //                    $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            $footer = '
                    <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;"><tr>
                    <td width="50%"></td>
                    <td width="50%" align="right">{PAGENO} / {nb}</td>
                    </tr></table>
                    ';
            $mpdf->SetHTMLFooter($footer);
//            $header = 0.75 * 72 / (72/25.4);                    
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,0,0);
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporanPDF',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.4',
                        'title'=>'KEGIATAN KEBIDANAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.4',
                    'title'=>'KEGIATAN KEBIDANAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.8 PEMERIKSAAN LABORATORIUM
     */
    public function actionPemeriksaanLaboratorium()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria2 = new CDbCriteria();
        $criteria2->group = 'jenispemeriksaanlab_kelompok,jenispemeriksaanlab_nama,pemeriksaanlab_nama';
        $criteria2->select = $criteria2->group.', sum(jumlah) as jumlah';
        $criteria2->order = 'pemeriksaanlab_nama';
        $criteria2->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl38KegiatanlaboratoriumV::model()->findAll($criteria2);
        
        $criteria=new CDbCriteria;

        $criteria->group = 'tahun, jenispemeriksaanlab_kelompok,jenispemeriksaanlab_nama';
        $criteria->select = $criteria->group;
        $criteria->order = 'jenispemeriksaanlab_kelompok';
        $criteria->compare('tahun',$tahun);
        
        $recordsGroup = RMRl38KegiatanlaboratoriumV::model()->findAll($criteria);
        
        
        
        $header = array(
            '<th width="50" style="text-align:center;">NO</th>', 
            '<th style="text-align:center;">JENIS KEGIATAN</th>', 
            '<th style="text-align:center;">JUMLAH</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        $dataGroup = array();
        foreach($records as $j=>$row)
        {
            $kelompok = $row->jenispemeriksaanlab_kelompok;
            $datas[$kelompok][$row->pemeriksaanlab_nama]['jenispemeriksaanlab_nama'] = $row->jenispemeriksaanlab_nama;
            $datas[$kelompok][$row->pemeriksaanlab_nama]['pemeriksaanlab_nama'] = $row->pemeriksaanlab_nama;
            $datas[$kelompok][$row->pemeriksaanlab_nama]['jumlah'] += $row->jumlah;                     
        }
        
        foreach($recordsGroup as $m=>$value){
            $kelompok = $value->jenispemeriksaanlab_kelompok;
            $dataGroup[$kelompok]['jenispemeriksaanlab_kelompok']= $value->jenispemeriksaanlab_kelompok;           
            $dataGroup[$kelompok]['jenispemeriksaan'][$m]['jenispemeriksaanlab_nama']= $value->jenispemeriksaanlab_nama;           
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($dataGroup as $key=>$data){
            $table .= '<tr>';
            $table .= '<td colspan=3><b>'.strtoupper($data['jenispemeriksaanlab_kelompok']).'</b></td></tr>';  
            $jenispemeriksaanlab_nama = $data['jenispemeriksaanlab_nama'];
            
            foreach($data['jenispemeriksaan'] as $x => $val)
            {
                $table .= '<tr>';
                $table .= '<td style="background-color:#D8D8D8;text-align:center;">'. ($x+1).'</td>
                    <td style="background-color:#D8D8D8">'.$val['jenispemeriksaanlab_nama'].'</td>
                    <td style="background-color:#D8D8D8;text-align:center;"></td></tr>';
                $total = 0;
                $no = 0;
                foreach($datas[$data['jenispemeriksaanlab_kelompok']] as $j => $vals)
                {
                    $jmlpasien = $vals['jumlah'];
                    if($val['jenispemeriksaanlab_nama'] == $vals['jenispemeriksaanlab_nama']){
                        $table .= '<tr>';
                        $table .= '<td style=text-align:center;>'. ($x+1).'.'.($no+1).'</li></ol></td>
                            <td>'.$vals['pemeriksaanlab_nama'].'</td>
                            <td style=text-align:center;>'.$vals['jumlah'].'</td></tr>';                                          
                        $no++; 
                    }
                    
                    $total += $jmlpasien;
                }
            }
        }   
        $table .= '<tr><td></td><td><b>Total</b></td><td style=text-align:center;>'.$total .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.8',
                        'title'=>'PEMERIKSAAN LABORATORIUM',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.8',
                    'title'=>'PEMERIKSAAN LABORATORIUM',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.12 KEGIATAN KELUARGA BERENCANA
     */
    public function actionKegiatanKeluargaBerencana()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria2 = new CDbCriteria();
        $criteria2->group = 'tahun,daftartindakan_id,daftartindakan_kode,daftartindakan_nama';
        $criteria2->select = $criteria2->group.', sum(anc) as anc,'
                . '         sum(pascapersalinan) as pascapersalinan,sum(bukanrujukan) as bukanrujukan,sum(rujukanri) as rujukanri,sum(rujukanrj) as rujukanrj,'
                . '         sum(total) as total, sum(pascapersalinannifas) as pascapersalinannifas, sum(abortus) as abortus, sum(lainnya) as lainnya,'
                . '         sum(kunjunganulang) as kunjunganulang, sum(keluhanefeksampingjumlah) as keluhanefeksampingjumlah, sum(keluhanefeksampingdirujuk) as keluhanefeksampingdirujuk';
        $criteria2->order = 'daftartindakan_nama';
        $criteria2->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl312KegiatankeluargaberencanaV::model()->findAll($criteria2);
        
        
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th rowspan="2" style="text-align:center;vertical-align:middle;">METODA</th>', 
            '<th colspan="2" style="text-align:center;vertical-align:middle;">KONSELING</th>',
            '<th colspan="4" style="text-align:center;vertical-align:middle;">KB BARU DENGAN CARA MASUK</th>',
            '<th colspan="3" style="text-align:center;vertical-align:middle;">KB BARU DENGAN KONDISI</th>',
            '<th rowspan="2" style="text-align:center;vertical-align:middle;">KUNJUNGAN ULANG</th>',
            '<th colspan="2" style="text-align:center;vertical-align:middle;">KELUHAN EFEK SAMPING</th>',
        );
        $header2 = array(
            '<th style="text-align:center;vertical-align:middle;">ANC</th>', 
            '<th style="text-align:center;vertical-align:middle;">Pasca Persalinan</th>', 
            '<th style="text-align:center;vertical-align:middle;">BUKAN RUJUKAN</th>',
            '<th style="text-align:center;vertical-align:middle;">RUJUKAN R. INAP</th>',
            '<th style="text-align:center;vertical-align:middle;">RUJUKAN R. JALAN</th>',
            '<th style="text-align:center;vertical-align:middle;">TOTAL</th>',
            '<th style="text-align:center;vertical-align:middle;">PASCA PERSALINAN/NIFAS</th>',
            '<th style="text-align:center;vertical-align:middle;">ABORTUS</th>',
            '<th style="text-align:center;vertical-align:middle;">LAINNYA</th>',
            '<th style="text-align:center;vertical-align:middle;">JUMLAH</th>',
            '<th style="text-align:center;vertical-align:middle;">DIRUJUK</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;background-color:#AFAFAF">13</td>',
            '<td style="text-align:center;background-color:#AFAFAF">14</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan = $row->daftartindakan_id;
            $datas[$daftartindakan]['daftartindakan_nama'] = $row->daftartindakan_nama;
            $datas[$daftartindakan]['daftartindakan_kode'] = $row->daftartindakan_kode;
            $datas[$daftartindakan]['anc'] += $row->anc;                     
            $datas[$daftartindakan]['pascapersalinan'] += $row->pascapersalinan;                     
            $datas[$daftartindakan]['bukanrujukan'] += $row->bukanrujukan;                     
            $datas[$daftartindakan]['rujukanri'] += $row->rujukanri;    
            $datas[$daftartindakan]['rujukanrj'] += $row->rujukanrj;                     
            $datas[$daftartindakan]['total'] += $row->total;                     
            $datas[$daftartindakan]['pascapersalinannifas'] += $row->pascapersalinannifas;                     
            $datas[$daftartindakan]['abortus'] += $row->abortus;                     
            $datas[$daftartindakan]['lainnya'] += $row->lainnya;                     
            $datas[$daftartindakan]['kunjunganulang'] += $row->kunjunganulang;                     
            $datas[$daftartindakan]['keluhanefeksampingjumlah'] += $row->keluhanefeksampingjumlah;                                 
            $datas[$daftartindakan]['keluhanefeksampingdirujuk'] += $row->keluhanefeksampingdirujuk;                     
//            $datas[$daftartindakan][$row->daftartindakan_nama]['keluhanefeksampingdirujuk'] += $row->keluhanefeksampingdirujuk;                     
        }
            $total_anc = 0;                     
            $total_pascapersalinan = 0;                        
            $total_bukanrujukan = 0;                        
            $total_rujukanri = 0;        
            $total_rujukanrj = 0;                  
            $total = 0;                       
            $total_pascapersalinannifas = 0;                        
            $total_abortus = 0;                      
            $total_lainnya = 0;                         
            $total_kunjunganulang = 0;                    
            $total_keluhanefeksampingjumlah = 0;                                    
            $total_keluhanefeksampingdirujuk = 0;  
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['anc'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pascapersalinan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['bukanrujukan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanri'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanrj'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;background-color:#AFAFAF">'.$data['total'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pascapersalinannifas'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['abortus'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['lainnya'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['kunjunganulang'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['keluhanefeksampingjumlah'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['keluhanefeksampingdirujuk'].'</td>',
                );
                $i++;
                $total_anc += $data['anc'];                     
                $total_pascapersalinan += $data['pascapersalinan'];                               
                $total_bukanrujukan += $data['bukanrujukan'];                               
                $total_rujukanri += $data['rujukanri'];              
                $total_rujukanrj += $data['rujukanrj'];                         
                $total += $data['total'];                             
                $total_pascapersalinannifas += $data['pascapersalinannifas'];                              
                $total_abortus += $data['abortus'];                            
                $total_lainnya += $data['lainnya'];                                
                $total_kunjunganulang += $data['kunjunganulang'];                           
                $total_keluhanefeksampingjumlah += $data['keluhanefeksampingjumlah'];                                          
                $total_keluhanefeksampingdirujuk += $data['keluhanefeksampingdirujuk'];          
            }   
        }else{
            $rows[] = array(
                    '<td colspan=8><i>Data tidak ditemukan.</i></td>',                
                );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_anc .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_pascapersalinan .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_bukanrujukan .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanri .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanrj .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_pascapersalinannifas .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_abortus .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_lainnya .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_kunjunganulang .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_keluhanefeksampingjumlah .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_keluhanefeksampingdirujuk .'</td><tr>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.12',
                        'title'=>'KEGIATAN KELUARGA BERENCANA',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.12',
                    'title'=>'KEGIATAN KELUARGA BERENCANA',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    
    /**
     * Formulir RL 3.14 KEGIATAN RUJUKAN
     */
    public function actionKegiatanRujukan()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria2 = new CDbCriteria();
        $criteria2->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,jeniskasuspenyakit_id,jeniskasuspenyakit_nama';
        $criteria2->select = $criteria2->group.', sum(rujukanpuskesmas) as rujukanpuskesmas,sum(rujukanfaskeslain) as rujukanfaskeslain,sum(rujukanrs) as rujukanrs,
                            sum(kembalipuskesmas) as kembalipuskesmas,sum(kembalifaskeslain) as kembalifaskeslain,sum(kembalirslain) as kembalirslain,sum(pasienrujukankeluar) as pasienrujukankeluar,sum(pasiendatangsendiri) as pasiendatangsendiri,sum(diterimakembali) as diterimakembali';
        $criteria2->order = 'jeniskasuspenyakit_nama';
        $criteria2->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl314KegiatanrujukanV::model()->findAll($criteria2);
        
        
        $header = array(
            '<th width="50" rowspan="2" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th rowspan="2" style="text-align:center;vertical-align:middle;">JENIS SPESIALISASI</th>', 
            '<th colspan="6" style="text-align:center;vertical-align:middle;">RUJUKAN</th>',
            '<th colspan="3" style="text-align:center;vertical-align:middle;">DIRUJUKAN</th>',
        );
        $header2 = array(
            '<th style="text-align:center;vertical-align:middle;">DITERIMA DARI PUSKESMAS</th>', 
            '<th style="text-align:center;vertical-align:middle;">DITERIMA DARI FASILITAS KES. LAIN</th>', 
            '<th style="text-align:center;vertical-align:middle;">DITERIMA DARI RS LAIN</th>',
            '<th style="text-align:center;vertical-align:middle;">DIKEMBALIKAN KE PUSKESMAS</th>',
            '<th style="text-align:center;vertical-align:middle;">DIKEMBALIKAN KE FASILITAS KES. LAIN</th>',
            '<th style="text-align:center;vertical-align:middle;">DIKEMBALIKAN KE RS ASAL</th>',
            '<th style="text-align:center;vertical-align:middle;">PASIEN RUJUKAN</th>',
            '<th style="text-align:center;vertical-align:middle;">PASIEN DATANG SENDIRI</th>',
            '<th style="text-align:center;vertical-align:middle;">DITERIMA KEMBALI</th>',
        );
        
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $jeniskasuspenyakit = $row->jeniskasuspenyakit_id;
            $datas[$jeniskasuspenyakit]['jeniskasuspenyakit_nama'] = $row->jeniskasuspenyakit_nama;
            $datas[$jeniskasuspenyakit]['rujukanpuskesmas'] += $row->rujukanpuskesmas;                     
            $datas[$jeniskasuspenyakit]['rujukanfaskeslain'] += $row->rujukanfaskeslain;                     
            $datas[$jeniskasuspenyakit]['rujukanrs'] += $row->rujukanrs;                     
            $datas[$jeniskasuspenyakit]['kembalipuskesmas'] += $row->kembalipuskesmas;    
            $datas[$jeniskasuspenyakit]['kembalifaskeslain'] += $row->kembalifaskeslain;                     
            $datas[$jeniskasuspenyakit]['kembalirslain'] += $row->kembalirslain;                     
            $datas[$jeniskasuspenyakit]['pasienrujukankeluar'] += $row->pasienrujukankeluar;                     
            $datas[$jeniskasuspenyakit]['pasiendatangsendiri'] += $row->pasiendatangsendiri;                     
            $datas[$jeniskasuspenyakit]['diterimakembali'] += $row->diterimakembali;                     
        }
            $total_rujukanpuskesmas = 0;                     
            $total_rujukanfaskeslain = 0;                  
            $total_rujukanrs = 0;         
            $total_kembalipuskesmas = 0;    
            $total_kembalifaskeslain = 0;                  
            $total_kembalirslain = 0;                     
            $total_pasienrujukankeluar = 0;                     
            $total_pasiendatangsendiri = 0;                   
            $total_diterimakembali = 0;      
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['jeniskasuspenyakit_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanpuskesmas'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanfaskeslain'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanrs'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['kembalipuskesmas'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['kembalifaskeslain'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['kembalirslain'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasienrujukankeluar'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['pasiendatangsendiri'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['diterimakembali'].'</td>',
                );
                $i++;
                $total_rujukanpuskesmas += $data['rujukanpuskesmas'];                    
                $total_rujukanfaskeslain += $data['rujukanfaskeslain'];
                $total_rujukanrs += $data['rujukanrs'];         
                $total_kembalipuskesmas += $data['kembalipuskesmas'];    
                $total_kembalifaskeslain += $data['kembalifaskeslain'];                  
                $total_kembalirslain += $data['kembalirslain'];                     
                $total_pasienrujukankeluar += $data['pasienrujukankeluar'];                    
                $total_pasiendatangsendiri += $data['pasiendatangsendiri'];                   
                $total_diterimakembali += $data['diterimakembali'];               
            }   
        }else{
            $rows[] = array(
                    '<td colspan=8><i>Data tidak ditemukan.</i></td>',                
                );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanpuskesmas .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanfaskeslain .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanrs .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_kembalipuskesmas .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_kembalifaskeslain .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_kembalirslain .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_pasienrujukankeluar .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_pasiendatangsendiri .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_diterimakembali .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.14',
                        'title'=>'KEGIATAN RUJUKAN',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.14',
                    'title'=>'KEGIATAN RUJUKAN',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.10 KEGIATAN PELAYANAN KHUSUS
     */
    public function actionKegiatanPelayananKhusus()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_kode,daftartindakan_nama';
        $criteria->select = $criteria->group.', sum(jumlah) as jumlah';
        $criteria->order = 'daftartindakan_nama';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl310KegiatanpelayanankhususV::model()->findAll($criteria);
        
        
        $header = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th style="text-align:center;vertical-align:middle;">JENIS KEGIATAN</th>', 
            '<th style="text-align:center;vertical-align:middle;">JUMLAH</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan = $row->daftartindakan_id;
            $datas[$daftartindakan]['daftartindakan_kode'] = $row->daftartindakan_kode;
            $datas[$daftartindakan]['daftartindakan_nama'] = $row->daftartindakan_nama;                     
            $datas[$daftartindakan]['jumlah'] += $row->jumlah;                                        
        }
            $total = 0;                  
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlah'].'</td>',
                );
                $i++;
                $total += $data['jumlah'];                            
            }   
        }else{
            $rows[] = array(
                '<td colspan=3><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.10',
                        'title'=>'KEGIATAN PELAYANAN KHUSUS',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.10',
                    'title'=>'KEGIATAN PELAYANAN KHUSUS',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.15 CARA BAYAR
     */
    public function actionCaraBayar()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'profilrs_id,nokode_rumahsakit,nama_rumahsakit,carabayar_id,carabayar_nama,penjamin_id,penjamin_nama';
        $criteria->select = $criteria->group.', sum(pasienrikeluar) as pasienrikeluar,sum(pasienrilamadirawat) as pasienrilamadirawat,sum(pasienrj) as pasienrj,
                            sum(pasienrjlab) as pasienrjlab,sum(pasienrjrad) as pasienrjrad,sum(pasienrjlain) as pasienrjlain';
        $criteria->order = 'carabayar_nama,penjamin_nama';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl315KegiatancarabayarV::model()->findAll($criteria);
        
        $criteria2=new CDbCriteria;
        $criteria2->group = 'profilrs_id,nokode_rumahsakit,nama_rumahsakit,carabayar_id,carabayar_nama';
        $criteria2->select = $criteria2->group;
        $criteria2->order = 'carabayar_id';
        $criteria2->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);       
        $recordsGroup = RMRl315KegiatancarabayarV::model()->findAll($criteria2);
        
        
        $header = array(
            '<td rowspan="2" width="50px" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<td rowspan="2" width="250px" style="text-align:center;vertical-align:middle;">CARA PEMBAYARAN</th>', 
            '<td colspan="2" width="150px" style="text-align:center;vertical-align:middle;">PASIEN RAWAT INAP</th>',
            '<td rowspan="2" width="75px" style="text-align:center;vertical-align:middle;">JUMLAH PASIEN RAWAT JALAN</th>',
            '<td colspan="3" width="225px" style="text-align:center;vertical-align:middle;">JUMLAH PASIEN RAWAT JALAN</th>',
        );
        $header2 = array(
            '<td width="75px" style="text-align:center;vertical-align:middle;">JUMLAH PASIEN KELUAR</th>', 
            '<td width="75px"  style="text-align:center;vertical-align:middle;">JUMLAH LAMA DIRAWAT (Hari)</th>', 
            '<td width="75px" style="text-align:center;vertical-align:middle;">LABO<BR/>RATORIUM</th>',
            '<td width="75px" style="text-align:center;vertical-align:middle;">RADIOLOGI</th>',
            '<td width="75px" style="text-align:center;vertical-align:middle;">LAIN-LAIN</th>',
        );
        $tdCount = array(
            '<td width="50px" style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td width="250px" style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td width="75px" style="text-align:center;background-color:#AFAFAF">8</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $datas = array();
        $dataGroup = array();
        
        $total_pasienrikeluar = 0;
        $total_pasienrilamadirawat = 0;
        $total_pasienrj = 0;
        $total_pasienrjlab = 0;
        $total_pasienrjrad = 0;                     
        $total_pasienrjlain = 0;
            
        foreach($records as $j=>$row)
        {
            $carabayar = $row->carabayar_nama;
            $datas[$carabayar][$row->penjamin_nama]['carabayar_nama'] = $row->carabayar_nama;
            $datas[$carabayar][$row->penjamin_nama]['penjamin_nama'] = $row->penjamin_nama;
            $datas[$carabayar][$row->penjamin_nama]['pasienrikeluar'] += $row->pasienrikeluar;
            $datas[$carabayar][$row->penjamin_nama]['pasienrilamadirawat'] += $row->pasienrilamadirawat;                     
            $datas[$carabayar][$row->penjamin_nama]['pasienrj'] += $row->pasienrj;                     
            $datas[$carabayar][$row->penjamin_nama]['pasienrjlab'] += $row->pasienrjlab;                     
            $datas[$carabayar][$row->penjamin_nama]['pasienrjrad'] += $row->pasienrjrad;                     
            $datas[$carabayar][$row->penjamin_nama]['pasienrjlain'] += $row->pasienrjlain;                     
        }
        
        foreach($recordsGroup as $m=>$value){
            $carabayar = $value->carabayar_nama;
            $dataGroup[$carabayar]['carabayar_nama']= $value->carabayar_nama;           
            $dataGroup[$carabayar]['carabayar'][$m]['carabayar_nama'] = $value->carabayar_nama;           
        }
        
        $table = '<table width="750px" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': 'border="1"') .' cellpadding="0" cellspacing="0">';
        $table .= '<thead>';
        $table .= '<tr style="font-weight:bold;">'. implode($header, "") .'</tr>'.'<tr style="font-weight:bold;">'. implode($header2, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr style="font-weight:bold;">';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        
        $i = 0;   
        if(count($recordsGroup) > 0){
            foreach($dataGroup as $key=>$data){
                $table .='</table><table width="750px" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': 'border="1"') .' cellpadding="0" cellspacing="0">';
                $table .= '<tr>';
                $table .= '<td width="50px" style="text-align:center;vertical-align:center;"><b>'.($i+1).'</b></td>';  
                $table .= '<td colspan=7><b>'.strtoupper($data['carabayar_nama']).'</b></td></tr>';  
                $table .= '</table>';
                $carabayar_nama = $data['carabayar_nama'];

                $no = 0;
                foreach($datas[$data['carabayar_nama']] as $j => $vals)
                {
                    if($carabayar_nama == $vals['carabayar_nama']){
                        $table .='</table>';
                        $table .='<table width="750px" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': 'border="1"') .' cellpadding="0" cellspacing="0">';
                        $table .= '<tr>';
                        $table .= '<td width="50px" style=text-align:center;>'. ($i+1).'.'.($no+1).'</li></ol></td>
                            <td width="250px">'.$vals['penjamin_nama'].'</td>
                            <td width="75px" style=text-align:center;>'.$vals['pasienrikeluar'].'</td>                                        
                            <td width="75px" style=text-align:center;>'.$vals['pasienrilamadirawat'].'</td>                                          
                            <td width="75px" style=text-align:center;>'.$vals['pasienrj'].'</td>                                     
                            <td width="75px" style=text-align:center;>'.$vals['pasienrjlab'].'</td>                                   
                            <td width="75px" style=text-align:center;>'.$vals['pasienrjrad'].'</td>                                     
                            <td width="75px" style=text-align:center;>'.$vals['pasienrjlain'].'</td></tr>';                                          
                        $no++; 
                    }

                    $total_pasienrikeluar += $vals['pasienrikeluar'];
                    $total_pasienrilamadirawat += $vals['pasienrilamadirawat'];
                    $total_pasienrj += $vals['pasienrj'];
                    $total_pasienrjlab += $vals['pasienrjlab'];
                    $total_pasienrjrad += $vals['pasienrjrad'];                     
                    $total_pasienrjlain += $vals['pasienrjlain'];
                }
                $i++;
            }   
        }else{
            $table .='<tr><td colspan=8><i>Data tidak ditemukan.</i></td></tr>';
        }
        $table .= '<tr>'
                . '<td width="50px" style="text-align:center;">99</td>'
                . '<td width="250px" ><b>Total</b></td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrikeluar .'</td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrilamadirawat .'</td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrj .'</td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrjlab .'</td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrjrad .'</td>'
                . '<td width="75px" style="text-align:center;background-color:#AFAFAF">'.$total_pasienrjlain .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.15',
                        'title'=>'CARA BAYAR',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.15',
                    'title'=>'CARA BAYAR',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.11 KEGIATAN KESEHATAN JIWA
     */
    public function actionKegiatanKesehatanJiwa()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_nama';
        $criteria->select = $criteria->group.', sum(jumlah) as jumlah';
        $criteria->order = 'daftartindakan_nama';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl311KegiatankesehatanjiwaV::model()->findAll($criteria);
        
        
        $header = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th style="text-align:center;vertical-align:middle;">JENIS PELAYANAN</th>', 
            '<th style="text-align:center;vertical-align:middle;">JUMLAH</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan = $row->daftartindakan_id;
            $datas[$daftartindakan]['daftartindakan_nama'] = $row->daftartindakan_nama;                     
            $datas[$daftartindakan]['jumlah'] += $row->jumlah;                                        
        }
            $total = 0;                  
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlah'].'</td>',
                );
                $i++;
                $total += $data['jumlah'];                            
            }   
        }else{
            $rows[] = array(
                '<td colspan=3><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.11',
                        'title'=>'KEGIATAN KESEHATAN JIWA',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.11',
                    'title'=>'KEGIATAN KESEHATAN JIWA',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }       
    
    /**
     * Formulir RL 3.5 KEGIATAN PERINATOLOGI
     */
    public function actionKegiatanPerinatologi()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_nama';
        $criteria->select = $criteria->group.', sum(rujukanrs) as rujukanrs,sum(rujukanbidan) as rujukanbidan,sum(rujukanpuskesmas) as rujukanpuskesmas,sum(rujukanfaskeslain) as rujukanfaskeslain,sum(rujukanmedismati) as rujukanmedismati,sum(totalrujukanmedis) as totalrujukanmedis,sum(rujukannonmedismati) as rujukannonmedismati,sum(totalrujukannonmedis) as totalrujukannonmedis,sum(nonrujukanmati) as nonrujukanmati,sum(totalnonrujukan) as totalnonrujukan,sum(dirujukkeluar) as dirujukkeluar';
        $criteria->order = 'daftartindakan_nama';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl35KegiatanperinatologiV::model()->findAll($criteria);
        
        
        $header = array(
            '<th rowspan="3" width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">JENIS KEGIATAN</th>', 
            '<th colspan="8" style="text-align:center;vertical-align:middle;">RUJUKAN</th>',
            '<th colspan="2" style="text-align:center;vertical-align:middle;">NON RUJUKAN</th>',
            '<th rowspan="3" style="text-align:center;vertical-align:middle;">DIRUJUK</th>',
        );
        $header2 = array(
            '<th colspan="6" width="50" style="text-align:center;vertical-align:middle;">MEDIS</th>', 
            '<th colspan="2" style="text-align:center;vertical-align:middle;">NON MEDIS</th>', 
            '<th rowspan="2" style="text-align:center;vertical-align:middle;">MATI</th>',
            '<th rowspan="2" style="text-align:center;vertical-align:middle;">JUMLAH TOTAL</th>',
        );
        $header3 = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">RUMAH SAKIT</th>', 
            '<th style="text-align:center;vertical-align:middle;">BIDAN</th>', 
            '<th style="text-align:center;vertical-align:middle;">PUSKESMAS</th>',
            '<th style="text-align:center;vertical-align:middle;">FASKES LAINNYA</th>',
            '<th style="text-align:center;vertical-align:middle;">MATI</th>',
            '<th style="text-align:center;vertical-align:middle;">JUMLAH TOTAL</th>',
            '<th style="text-align:center;vertical-align:middle;">MATI</th>',
            '<th style="text-align:center;vertical-align:middle;">JUMLAH TOTAL</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
            '<td style="text-align:center;background-color:#AFAFAF">6</td>',
            '<td style="text-align:center;background-color:#AFAFAF">7</td>',
            '<td style="text-align:center;background-color:#AFAFAF">8</td>',
            '<td style="text-align:center;background-color:#AFAFAF">9</td>',
            '<td style="text-align:center;background-color:#AFAFAF">10</td>',
            '<td style="text-align:center;background-color:#AFAFAF">11</td>',
            '<td style="text-align:center;background-color:#AFAFAF">12</td>',
            '<td style="text-align:center;background-color:#AFAFAF">13</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan = $row->daftartindakan_nama;
            $datas[$daftartindakan]['daftartindakan_nama'] = $row->daftartindakan_nama;                     
            $datas[$daftartindakan]['rujukanrs'] += $row->rujukanrs;                                        
            $datas[$daftartindakan]['rujukanbidan'] += $row->rujukanbidan;                                        
            $datas[$daftartindakan]['rujukanpuskesmas'] += $row->rujukanpuskesmas;                                        
            $datas[$daftartindakan]['rujukanfaskeslain'] += $row->rujukanfaskeslain;                                        
            $datas[$daftartindakan]['rujukanmedismati'] += $row->rujukanmedismati;                                        
            $datas[$daftartindakan]['totalrujukanmedis'] += $row->totalrujukanmedis;                                        
            $datas[$daftartindakan]['rujukannonmedismati'] += $row->rujukannonmedismati;                                        
            $datas[$daftartindakan]['totalrujukannonmedis'] += $row->totalrujukannonmedis;                                        
            $datas[$daftartindakan]['nonrujukanmati'] += $row->nonrujukanmati;                                        
            $datas[$daftartindakan]['totalnonrujukan'] += $row->totalnonrujukan;                                        
            $datas[$daftartindakan]['dirujukkeluar'] += $row->dirujukkeluar;                                        
        }
        
        $total_rujukanrs = 0;                                    
        $total_rujukanbidan = 0;                                        
        $total_rujukanfaskeslain = 0;                                   
        $total_rujukanmedismati = 0;                                       
        $total_totalrujukanmedis = 0;                                        
        $total_rujukannonmedismati = 0;                                    
        $total_totalrujukannonmedis = 0;                                        
        $total_nonrujukanmati = 0;                                       
        $total_totalnonrujukan = 0;                                      
        $total_dirujukkeluar = 0;
        
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanrs'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanbidan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanpuskesmas'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanfaskeslain'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukanmedismati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['totalrujukanmedis'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['rujukannonmedismati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['totalrujukannonmedis'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['nonrujukanmati'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['totalnonrujukan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['dirujukkeluar'].'</td>',
                );
                $i++;
                $total_rujukanrs += $data['rujukanrs'];                                    
                $total_rujukanbidan += $data['rujukanbidan'];                                        
                $total_rujukanpuskesmas += $data['rujukanpuskesmas'];                                        
                $total_rujukanfaskeslain += $data['rujukanfaskeslain'];                                   
                $total_rujukanmedismati += $data['rujukanmedismati'];                                       
                $total_totalrujukanmedis += $data['totalrujukanmedis'];                                        
                $total_rujukannonmedismati += $data['rujukannonmedismati'];                                    
                $total_totalrujukannonmedis += $data['totalrujukannonmedis'];                                        
                $total_nonrujukanmati += $data['nonrujukanmati'];                                       
                $total_totalnonrujukan += $data['totalnonrujukan'];                                      
                $total_dirujukkeluar += $data['dirujukkeluar'];
            }   
        }else{
            $rows[] = array(
                '<td colspan=13><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>'.'<tr>'. implode($header3, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanrs .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanbidan .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanpuskesmas .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanfaskeslain .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukanmedismati .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_totalrujukanmedis .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rujukannonmedismati .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_totalrujukannonmedis .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_nonrujukanmati .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_totalnonrujukan .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_dirujukkeluar .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.5',
                        'title'=>'KEGIATAN PERINATOLOGI',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.5',
                    'title'=>'KEGIATAN PERINATOLOGI',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }       
    
    /**
     * Formulir RL 3.9 PELAYANAN REHABILITASI MEDIK
     */
    public function actionPelayananRehabMedik()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,daftartindakan_id,daftartindakan_nama';
        $criteria->select = $criteria->group.', sum(jumlah) as jumlah';
        $criteria->order = 'daftartindakan_nama';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $records = RMRl39PelayananrehabilitasimedikV::model()->findAll($criteria);
        
        
        $header = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th style="text-align:center;vertical-align:middle;">JENIS TINDAKAN</th>', 
            '<th style="text-align:center;vertical-align:middle;">JUMLAH</th>',
        );
        $tdCount = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
        );

        $rows = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datas = array();
        foreach($records as $j=>$row)
        {
            $daftartindakan = $row->daftartindakan_nama;
            $datas[$daftartindakan]['daftartindakan_nama'] = $row->daftartindakan_nama;                     
            $datas[$daftartindakan]['jumlah'] += $row->jumlah;                                           
        }
        
        $total = 0;           
        
        if(count($datas) > 0){
            foreach($datas as $key=>$data){
                $rows[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$data['daftartindakan_nama'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlah'].'</td>',
                );
                $i++;
                $total += $data['jumlah'];  
            }   
        }else{
            $rows[] = array(
                '<td colspan=3><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<thead>';
        $table .= '<tr>'. implode($header, "") .'</tr>'.'<tr>'. implode($header2, "") .'</tr>'.'<tr>'. implode($header3, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCount, "");
        $table .= '</tr>';
        foreach($rows as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.9',
                        'title'=>'PELAYANAN REHABILITASI MEDIK',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.9',
                    'title'=>'PELAYANAN REHABILITASI MEDIK',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * Formulir RL 3.13 PENGADAAN OBAT, PENULISAN DAN PELAYANAN RESEP
     */
    public function actionPengadaanObatResep()
    {
        $format = new CustomFormat();
        $tgl_awal = $format->formatDateTimeMediumForDB($_REQUEST['tgl_awal']);
        $tgl_akhir = $format->formatDateTimeMediumForDB($_REQUEST['tgl_akhir']);
        $tanggal_awal = explode('-',$tgl_awal);
        $tanggal_akhir = explode('-',$tgl_akhir);
        
        $tahun_awal = $tanggal_awal[0];
        $tahun_akhir = $tanggal_akhir[0];
        
        if($tahun_awal < $tahun_akhir){
            $periode = $tahun_awal." - ".$tahun_akhir;
        }else{
            $periode = $tahun_akhir;
        }
        
        $criteria = new CDbCriteria();
        $criteria->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,obatalkes_kategori';
        $criteria->select = $criteria->group.', obatalkes_kategori as golonganobat, sum(jumlahitemobat) as jumlahitemobat,sum(jumlahitemrs) as jumlahitemrs,sum(jumlahitemformulatoriumobat) as jumlahitemformulatoriumobat';
        $criteria->order = 'obatalkes_kategori';
        $criteria->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $recordsObat = RMRl313PengadaanobatV::model()->findAll($criteria);
        
        $criteria2 = new CDbCriteria();
        $criteria2->group = 'tahun,profilrs_id,nokode_rumahsakit,nama_rumahsakit,obatalkes_kategori';
        $criteria2->select = $criteria2->group.', obatalkes_kategori as golonganobat, sum(rawatjalan) as rawatjalan,sum(igd) as igd,sum(rawatinap) as rawatinap';
        $criteria2->order = 'obatalkes_kategori';
        $criteria2->addBetweenCondition('tahun',$tahun_awal,$tahun_akhir);
        $recordsResep = RMRl313PenulisandanpelayananresepV::model()->findAll($criteria2);
        
        
        $headerObat = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th width="350" style="text-align:center;vertical-align:middle;">GOLONGAN OBAT</th>', 
            '<th style="text-align:center;vertical-align:middle;">JUMLAH ITEM OBAT</th>',
            '<th style="text-align:center;vertical-align:middle;">JUMLAH ITEM OBAT YANG TERSEDIA DI RUMAH SAKIT</th>',
            '<th style="text-align:center;vertical-align:middle;">JUMLAH ITEM OBAT FORMULATORIUM TERSEDIA DI RUMAH SAKIT</th>',
        );        
        $tdCountObat = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
        );
        
        $headerResep = array(
            '<th width="50" style="text-align:center;vertical-align:middle;">NO</th>', 
            '<th width="350" style="text-align:center;vertical-align:middle;">GOLONGAN OBAT</th>', 
            '<th style="text-align:center;vertical-align:middle;">RAWAT JALAN</th>',
            '<th style="text-align:center;vertical-align:middle;">IGD</th>',
            '<th style="text-align:center;vertical-align:middle;">RAWAT INAP</th>',
        );
        $tdCountResep = array(
            '<td style="text-align:center;background-color:#AFAFAF">1</td>',
            '<td style="text-align:center;background-color:#AFAFAF">2</td>',
            '<td style="text-align:center;background-color:#AFAFAF">3</td>',
            '<td style="text-align:center;background-color:#AFAFAF">4</td>',
            '<td style="text-align:center;background-color:#AFAFAF">5</td>',
        );

        $rowsObat = array();
        $rowsResep = array();
        $judul = array();
        $i=0;
        $total= 0;
        $datasObat = array();
        $datasResep = array();
        foreach($recordsObat as $j=>$rowObat)
        {
            $obatalkes_kategori = $rowObat->golonganobat;
            $datasObat[$obatalkes_kategori]['obatalkes_kategori'] = $rowObat->golonganobat;                     
            $datasObat[$obatalkes_kategori]['jumlahitemobat'] += $rowObat->jumlahitemobat;                                           
            $datasObat[$obatalkes_kategori]['jumlahitemrs'] += $rowObat->jumlahitemrs;                                           
            $datasObat[$obatalkes_kategori]['jumlahitemformulatoriumobat'] += $rowObat->jumlahitemformulatoriumobat;                                           
        }
        
        foreach($recordsResep as $j=>$rowResep)
        {
            $obatalkes_kategori = $rowResep->golonganobat;
            $datasResep[$obatalkes_kategori]['obatalkes_kategori'] = $rowResep->golonganobat;                     
            $datasResep[$obatalkes_kategori]['rawatjalan'] += $rowResep->rawatjalan;                                           
            $datasResep[$obatalkes_kategori]['igd'] += $rowResep->igd;                                           
            $datasResep[$obatalkes_kategori]['rawatinap'] += $rowResep->rawatinap;                                           
        }
        
        $resep = 0;
        $total_jumlahitemobat = 0;           
        $total_jumlahitemrs = 0;           
        $total_jumlahitemformulatoriumobat = 0;           
        
        if(count($datasObat) > 0){
            foreach($datasObat as $key=>$data){
                $rowsObat[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($resep+1).'</td>',
                    '<td>'.$data['obatalkes_kategori'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlahitemobat'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlahitemrs'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$data['jumlahitemformulatoriumobat'].'</td>',
                );
                $resep++;
                $total_jumlahitemobat += $data['jumlahitemobat'];           
                $total_jumlahitemrs += $data['jumlahitemrs'];             
                $total_jumlahitemformulatoriumobat += $data['jumlahitemformulatoriumobat'];    
            }   
        }else{
            $rowsObat[] = array(
                '<td colspan=5><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        $total_rawatjalan = 0;           
        $total_rawatinap = 0;           
        $total_igd = 0;
        if(count($datasResep) > 0){
        foreach($datasResep as $key=>$dataResep){
                $rowsResep[] = array(
                    '<td width=50 style="text-align:center;vertical-align:middle;">'. ($i+1).'</td>',
                    '<td>'.$dataResep['obatalkes_kategori'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$dataResep['rawatjalan'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$dataResep['igd'].'</td>',
                    '<td style="text-align:center;vertical-align:middle;">'.$dataResep['rawatinap'].'</td>',
                );
                $i++;
                $total_rawatjalan += $dataResep['rawatjalan'];           
                $total_rawatinap += $dataResep['rawatinap'];             
                $total_igd += $dataResep['igd'];    
            }   
        }else{
            $rowsResep[] = array(
                '<td colspan=5><i>Data tidak ditemukan.</i></td>',                
            );
        }
        
        /**
         * untuk tabel kolom A. Pengadaan Obat
         */
        $table = '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<div style="font-weight:bold;margin-left:10px;">3.13 Pengadaan Obat, Penulisan dan Pelayanan Resep <br/<br/><br/></div>';
        $table .= '<div style="font-weight:bold;margin-left:10px;">A. Pengadaan Obat</div>';
        $table .= '<thead>';
        $table .= '<tr>'. implode($headerObat, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCountObat, "");
        $table .= '</tr>';
        foreach($rowsObat as $row)
        {
            $table .= '<tr>'. implode($row, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_jumlahitemobat .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_jumlahitemrs .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_jumlahitemformulatoriumobat .'</td>';
        $table .= '</tbody>';
        $table .= '</table><br/>';
        
        /**
         * untuk tabel kolom B. Penulisan dan Pelayanan Resep
         */
        $table .= '<table width="750" '. ($_GET['caraPrint'] == 'PDF' ? 'border="1"': " ") .' cellpadding="2" cellspacing="0" class="table table-striped table-bordered table-condensed">';
        $table .= '<div style="font-weight:bold;margin-left:10px;">B. Penulisan dan Pelayanan Resep </div>';
        $table .= '<thead>';
        $table .= '<tr>'. implode($headerResep, "") .'</tr>';
        $table .= '</thead>';
        $table .= '<tbody>'; 
        $table .= '<tr>';
        $table .= implode($tdCountResep, "");
        $table .= '</tr>';
        foreach($rowsResep as $rowResep)
        {
            $table .= '<tr>'. implode($rowResep, '') . '<tr>';
        }
        $table .= '<tr><td style="text-align:center;">99</td><td><b>Total</b></td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rawatjalan .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_igd .'</td>'
                . '<td style="text-align:center;background-color:#AFAFAF">'.$total_rawatinap .'</td>';
        $table .= '</tbody>';
        $table .= '</table>';
        
        if($_GET['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
            
            $header = array(
                '<th style="text-align:center;">Tahun</th>', '<th style="text-align:center;">BOR</th>', '<th style="text-align:center;">LOS</th>', '<th style="text-align:center;">BTO</th>','<th style="text-align:center;">TOI</th>','<th style="text-align:center;">NDR</th>','<th style="text-align:center;">GDR</th>','<th style="text-align:center;">Rata-rata <br/> Kunjungan /hari</th>'
            );
            
            $mpdf->WriteHTML(
                $this->renderPartial('print_laporan',
                    array(
                        'width'=>'750',
                        'formulir'=>'Formulir RL 3.13',
                        'title'=>'PENGADAAN OBAT, PENULISAN DAN PELAYANAN RESEP',
                        'periode'=>$periode,
                        'table'=>$table,
                        'caraPrint'=>$_GET['caraPrint']
                    ), true
                )
            );
            $mpdf->Output();
        }else{
            $this->layout = '//layouts/printWindows';
            if($_GET['caraPrint'] == 'EXCEL')
            {
                $this->layout = '//layouts/printExcel';
            }
            $this->render('print_laporan',
                array(
                    'width'=>'750',
                    'formulir'=>'Formulir RL 3.13',
                    'title'=>'PENGADAAN OBAT, PENULISAN DAN PELAYANAN RESEP',
                    'periode'=>$periode,
                    'table'=>$table,
                    'caraPrint'=>$_GET['caraPrint']
                )
            );            
        }
    }
    
    /**
     * function untuk format periode tanggal laporan
     */
    public function actionGetPeriodeLaporan()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $format = new CustomFormat();
            $periode = $_GET['periode'];
            
            $month = date('m');
            $year = date('Y');
            $jumHari = cal_days_in_month(CAL_GREGORIAN, $month, $year);

            $bulan =  date ("d M Y", mktime (0,0,0,$month,$jumHari,$year)); 
            $awalBulan =  date ("d M Y", mktime (0,0,0,$month,1,$year)); 
            $awalTahun=  date ("d M Y", mktime (0,0,0,1,1,$year)); 
            $akhirTahun=  date ("d M Y", mktime (0,0,0,12,31,$year)); 


            $lastmonth = mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"));
            $nextyear  = mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1);

            if($periode == "hari"){
               $tgl_awal = date('d M Y 00:00:00');
               $tgl_akhir = date('d M Y 23:59:59');
            }else if($periode == "bulan"){
               $tgl_awal = ''.$awalBulan.' 00:00:00';
               $tgl_akhir = ''.$bulan.' 23:59:59';

            }else if($periode == "tahun"){
                $tgl_awal = ''.$awalTahun.' 00:00:00';

                $tgl_akhir = ''.$akhirTahun.' 23:59:59';

            }else{
                $tgl_awal = date('d M Y 00:00:00 ');
                $tgl_akhir = date('d M Y 23:59:59');
            }
            
            $data = array(
                'tgl_awal'=>$tgl_awal,
                'tgl_akhir'=>$tgl_akhir
            );
            echo json_encode($data);
            Yii::app()->end();
        }
    }
    
}

?>