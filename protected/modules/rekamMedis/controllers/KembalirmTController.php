
<?php

class KembalirmTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathView = 'rekamMedis.views.kembalirmT.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','penyimpanan','informasi','PenyimpananDokumen'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKKembalirmT;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKKembalirmT']))
		{
			$model->attributes=$_POST['RKKembalirmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->kembalirm_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RKKembalirmT']))
		{
			$model->attributes=$_POST['RKKembalirmT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->kembalirm_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$modPengiriman=new RKPeminjamandokumenrmV('search');
                $modPengiriman->unsetAttributes();  // clear any default values
                $modPengiriman->tgl_rekam_medik = date('Y-m-d H:i:s');
                $modPengiriman->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
                if(isset($_GET['RKPeminjamandokumenrmV'])){
                    $modPengiriman->attributes=$_GET['RKPeminjamandokumenrmV'];  
                    $format = new CustomFormat();
                    $modPengiriman->tgl_rekam_medik = $format->formatDateTimeMediumForDB($modPengiriman->tgl_rekam_medik);
                    $modPengiriman->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($modPengiriman->tgl_rekam_medik_akhir);
                }
                
                $model=new RKKembalirmT;
                $model->petugaspenerima = Yii::app()->user->name;
                $model->tglkembali = date('Y-m-d H:i:s');
                
		if(isset($_POST['RKKembalirmT']))
		{
			$model->attributes=$_POST['RKKembalirmT'];
                        $jumlah  = count($_POST['Dokumen']['dokrekammedis_id']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $success = true;
                            for($i=0; $i < $jumlah; $i++){
                                if ($_POST['cekList'][$i] == 1){
                                    $models = new RKKembalirmT();
                                    $models->attributes = $model->attributes;
                                    $models->dokrekammedis_id = $_POST['Dokumen']['dokrekammedis_id'][$i];
                                    $models->pasien_id = $_POST['Dokumen']['pasien_id'][$i];
                                    $models->pendaftaran_id = $_POST['Dokumen']['pendaftaran_id'][$i];
                                    $models->peminjamanrm_id = $_POST['Dokumen']['peminjamanrm_id'][$i];
                                    $models->pengirimanrm_id = $_POST['Dokumen']['pengirimanrm_id'][$i];
//                                    $models->lengkapdokumenkembali = $_POST['Dokumen']['kelengkapan'][$i];
                                    if ($_POST['Dokumen']['kelengkapan'][$i] == 1){
                                        $models->lengkapdokumenkembali = true;
                                    }
                                    else{
                                        $models->lengkapdokumenkembali = false;
                                    }
                                    $models->ruanganasal_id = PengirimanrmT::model()->findByPk($models->pengirimanrm_id)->ruanganpengirim_id;
                                    if (!$models->save()){
                                        $success = false;
                                    }
                                    else{
                                        DokrekammedisM::model()->updateByPk($models->dokrekammedis_id, 
                                                array('subrak_id'=>$_POST['Dokumen']['subrak_id'][$i],'lokasirak_id'=>$_POST['Dokumen']['lokasirak_id'][$i]));
                                        //PendaftaranT::model()->updateByPk($models->pendaftaran_id, array('kembali'))
                                        PengirimanrmT::model()->updateByPk($models->pengirimanrm_id, array('kembalirm_id'=>$models->kembalirm_id));
                                        PeminjamanrmT::model()->updateByPk($models->peminjamanrm_id, array('kembalirm_id'=>$models->kembalirm_id));
                                    }
                                }
                            }
                            if ($success == true){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Pengembalian Data Dokumen Rekam Medis berhasil disimpan");
                                $this->refresh();
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                            }
                        }
                        catch (Exception $exc) {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                        }
		}
                
                $this->render('index',array(
			'model'=>$model,
                        'modPengiriman'=>$modPengiriman,
		));
	}
        
        public function actionPenyimpanan(){
                $modDokRekamMedis=new RKDokrekammedisM;
                //$modPengiriman = new PPPengirimanrmT;
//                $modPengiriman->tglpengirimanrm = date('Y-m-d H:i:s');
//                $modPengiriman->petugaspengirim = Yii::app()->user->name;
                if (isset($_POST['Dokumen'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    $jumlah = count($_POST['Dokumen']['dokrekammedis_id']);

                    try{
                        $success = true;
                        for ($i = 0; $i < $jumlah; $i++){
                            if ($_POST['cekList'][$i] == 1){
                                RKDokrekammedisM::model()->updateByPk($_POST['Dokumen']['dokrekammedis_id'][$i], 
                                        array('subrak_id'=>$_POST['Dokumen']['subrak_id'][$i], 'lokasirak_id'=>$_POST['Dokumen']['lokasirak_id'][$i]));
                            }
                        }
                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Pengiriman Dokumen Rekam Medis berhasil disimpan");
                            $this->refresh();
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                            $this->refresh();
                        }
                    }
                    catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                            $this->refresh();
                    }
                }
                
                $model=new RKPengirimanrmT('search');
                $model->tgl_rekam_medik = date('Y-m-d H:i:s');
                $model->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
                $model->unsetAttributes();  // clear any default values
                //$modDokRekamMedis->nodokumenrm = Generator::noDokumenRM();
                if(isset($_GET['RKPengirimanrmT'])){
                    $model->attributes=$_GET['RKPengirimanrmT'];
                    $format = new CustomFormat();
                    $model->tgl_rekam_medik = $format->formatDateTimeMediumForDB($model->tgl_rekam_medik);
                    $model->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($model->tgl_rekam_medik_akhir);
                }

                $this->render('penyimpanan',array(
                    'model'=>$model,
                    'modDokRekamMedis'=>$modDokRekamMedis,
                    //'modPengiriman'=>$modPengiriman,
                ));
        }
        
        public function actionInformasi()
	{      
            $model=new KembalirmT('search');
                            $model->unsetAttributes();
                            $model->tglAwal = date('Y-m-d 00:00:00');
                            $model->tglAkhir = date('Y-m-d H:i:s');
                            if(isset($_GET['KembalirmT'])){
                                $model->attributes = $_GET['KembalirmT'];
                                $format = new CustomFormat();
                                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['KembalirmT']['tglAwal']);
                                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['KembalirmT']['tglAkhir']);
                            }
            $this->render($this->pathView.'informasi',array(
                    'model'=>$model,
        ));
        }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new RKKembalirmT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RKKembalirmT']))
			$model->attributes=$_GET['RKKembalirmT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RKKembalirmT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rkkembalirm-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new RKKembalirmT;
            $model->attributes=$_REQUEST['RKKembalirmT'];
            $judulLaporan='Data RKKembalirmT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }

    public function actionPenyimpananDokumen(){
                $modDokRekamMedis=new RKDokrekammedisM;
                //$modPengiriman = new PPPengirimanrmT;
//                $modPengiriman->tglpengirimanrm = date('Y-m-d H:i:s');
//                $modPengiriman->petugaspengirim = Yii::app()->user->name;
                if (isset($_POST['Dokumen'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    $jumlah = count($_POST['Dokumen']['pasien_id']);

                    try{
                        $success = true;
                        for ($i = 0; $i < $jumlah; $i++){
                            if ($_POST['cekList'][$i] == 1){
                                
                                // RKDokrekammedisM::model()->updateByPk($_POST['Dokumen']['dokrekammedis_id'][$i], 
                                //         array('subrak_id'=>$_POST['Dokumen']['subrak_id'][$i], 'lokasirak_id'=>$_POST['Dokumen']['lokasirak_id'][$i]));
                                $modDokRekamMedis->subrak_id = $_POST['Dokumen']['subrak_id'][$i];
                                $modDokRekamMedis->lokasirak_id = $_POST['Dokumen']['lokasirak_id'][$i];
                                $modDokRekamMedis->warnadokrm_id = $_POST['Dokumen']['warnadokrm_id'][$i];
                                $modDokRekamMedis->pasien_id = $_POST['Dokumen']['pasien_id'][$i];
                                $modDokRekamMedis->tglrekammedis = $_POST['Dokumen']['tgl_rekam_medik'][$i];
                                $modDokRekamMedis->tglmasukrak = date('Y-m-d H:i:s');
                                $modDokRekamMedis->statusrekammedis = "AKTIF";
                                $modDokRekamMedis->nomortertier = substr($_POST['Dokumen']['no_rekam_medik'][$i], 0,2);
                                $modDokRekamMedis->nomorsekunder = substr($_POST['Dokumen']['no_rekam_medik'][$i], 2,2);
                                $modDokRekamMedis->nomorprimer = substr($_POST['Dokumen']['no_rekam_medik'][$i], 4,2);
                                $modDokRekamMedis->create_time = date('Y-m-d H:i:s');
                                $modDokRekamMedis->update_time = date('Y-m-d H:i:s');
                                $modDokRekamMedis->create_loginpemakai_id = Yii::app()->user->id;
                                $modDokRekamMedis->create_ruangan = Yii::app()->user->ruangan_id;
                                $modDokRekamMedis->nodokumenrm = Generator::noDokumenRM();

                                // echo "<pre>";
                                // print_r($modDokRekamMedis->attributes);
                                if (!$modDokRekamMedis->save()){
                                    $success = false;
                                }

                                PasienM::model()->updateByPk($_POST['Dokumen']['pasien_id'][$i], array('dokrekammedis_id'=>$modDokRekamMedis->dokrekammedis_id));

                            }
                        }
                        // exit();

                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Pengiriman Dokumen Rekam Medis berhasil disimpan");
                            $this->refresh();
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                            $this->refresh();
                        }
                    }
                    catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                            $this->refresh();
                    }
                }
                
                $model=new RMPasienM('searchPasien');
                $model->tgl_rekam_medik = date('Y-m-d H:i:s');
                $model->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
                
                //$modDokRekamMedis->nodokumenrm = Generator::noDokumenRM();
                if(isset($_GET['RMPasienM'])){
                    $model->unsetAttributes();  // clear any default values
                    $model->attributes=$_GET['RMPasienM'];
                    $format = new CustomFormat();
                    $model->tgl_rekam_medik = $format->formatDateTimeMediumForDB($model->tgl_rekam_medik);
                    $model->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($model->tgl_rekam_medik_akhir);
                }

                $this->render('penyimpananDokumen',array(
                    'model'=>$model,
                    'modDokRekamMedis'=>$modDokRekamMedis,
                    //'modPengiriman'=>$modPengiriman,
                ));
        }

}
