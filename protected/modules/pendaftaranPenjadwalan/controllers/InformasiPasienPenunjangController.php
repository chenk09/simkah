<?php

class InformasiPasienPenunjangController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        
        public $pathView = 'pendaftaranPenjadwalan.views.informasiPasienPenunjang.';
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Informasi Pasien Penunjang";
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		
		$format = new CustomFormat();
                $model=new PPPasienMasukPenunjangT('search');
		$model->unsetAttributes(); // clear any default values
                $model->tglAwal = date('d M Y').' 00:00:00';
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['PPPasienMasukPenunjangT']))
                {
			$model->attributes=$_GET['PPPasienMasukPenunjangT'];
                        $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienMasukPenunjangT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienMasukPenunjangT']['tglAkhir']);
                }
		$this->render($this->pathView.'index',array(
			'model'=>$model,
		));
	}
        

	
}