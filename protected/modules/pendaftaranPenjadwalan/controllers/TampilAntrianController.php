<?php

class TampilAntrianController extends Controller
{
        public $layout='//layouts/tampilAntrian'; 
        public function actionIndex()
	{
                $table = $this->loadFormat();
                
		$this->render('index',array('table'=>$table));
	}
          
        
        public function loadFormat()
        {
            $arrCaraBayar = CarabayarM::model()->findAll('carabayar_aktif = true order by carabayar_nama');
            $format = '';
            foreach ($arrCaraBayar as $caraBayar) {
                $result = $this->getNoAntrian($caraBayar->carabayar_id);
                if(!empty($result)) {
                    $antrian = $result;//$this->getNoAntrian($caraBayar->carabayar_id);
                }
                else {
                    $antrian = '-';
                }
                $format .= '<div class="boxrepeatAntrian">
                                <div class="boxAntrian">
                                    <center><b>'.$caraBayar->carabayar_nama.'</b></center>
                                </div>
                                </br>
                                <div class="boxrepeatinner">
                                    <center><font size = 70px>'.$antrian.'</font></center></br>
                                    <center>'.$caraBayar->carabayar_loket.'</center></br>
                                </div>
                            </div>';
            }
            return $format;
        }
        
        public function getNoAntrian($carabayar_id){
             $tgl = date('Y-m-d');
             $sqlNoRM = "select max(noantrian) noantri from antrian_t where date(tglantrian) = '".$tgl."' AND carabayar_id = $carabayar_id";
             
             return Yii::app()->db->createCommand($sqlNoRM)->queryScalar();
        }
        
        public function actionResponseTable(){
            $table = $this->loadFormat();
            echo $table;
        }

	public function actionPanggilAntrian()
        {
            $dropDownList='<select id="carabayar_loket" name="carabayar_loket" class="span2 hide">
                            <option value="">--Pilih Loket--</option>';

            $modLoket = LoketM::model()->findAll(array(),array('order'=>'loket_nama'));
            
            foreach ($modLoket AS $tampilLoket){
                $dropDownList .='<option value="'.$tampilLoket['loket_id'].'">'.$tampilLoket['loket_nama'].'</option>';
            }   
            
            $dropDownList .='</select>';
            
            if(isset ($_POST['carabayar_loket']))
            {
                $idLoket = $_POST['carabayar_loket'];
                /*
                 * cari no_antrian selanjutnya
                 */
                $dataLoket = LoketM::model()->findByPk($idLoket);
                $sqlCaraBayar = "SELECT antrian_id, noantrian FROM antrian_t
                                 WHERE loket_id = $idLoket AND panggil_flaq = FALSE
                                 AND date(tglantrian)='".date('Y-m-d')."' ORDER by noantrian limit 1";
                                 //ORDER by noantrian limit 1";
                $noAntrian   = Yii::app()->db->createCommand($sqlCaraBayar)->queryRow();
                AntrianT::model()->updateByPk($noAntrian['antrian_id'], array('panggil_flaq'=>TRUE));
                $noAntrianBaru = str_split($noAntrian['noantrian']);
                $loket = $dataLoket['loket_nourut'];
                $noAntrianUtuh=$noAntrian['noantrian'];
                
                $formSuaraMp3 = '';
                $formSuaraOgg = '';
                $suara = array();
                $forms = '';

                for ($f = 0; $f < count($noAntrianBaru); $f++) {
                    $suara['mp3'][$f] = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3';
                    $suara['ogg'][$f] = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3';
                    $forms .= '<div id="jquery_jplayer_'.$f.'" class="jp-jplayer"></div>';
                }

                $formLoketMp3 = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$loket.'.mp3';
                $formLoketOgg = ''.Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$loket.'.ogg';
                $jumlah = count($noAntrianBaru);
            }
            
            $this->render('panggilAntrian',array('cara_bayar'=>$modCaraBayar,
                                                 'formLoketMp3'=>$formLoketMp3,
                                                 'formLoketOgg'=>$formLoketOgg,
                                                 'suara'=>$suara,
                                                 'jumlah'=>$jumlah,
                                                 'forms'=>$forms,
                                                 'noAntrianBaru'=>$noAntrianBaru,
                                                 'carabayar_id'=>$carabayar_id,
                                                 'noAntrianUtuh'=>$noAntrianUtuh,
                                                 'dropDownList'=>$dropDownList
            ));
        }
        
        public function actionAjaxAmbilAntrian()
        {
            $carabayar_id = $_POST['carabayar_id'];
            /*
             * cari no_antrian selanjutnya
             */
            $dataCaraBayar = CarabayarM::model()->findByPk($carabayar_id);
            $sqlCaraBayar = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) noantrian FROM antrian_t
                                           WHERE carabayar_id = $carabayar_id and pendaftaran_id is not null
                                           AND date(tglantrian)='".date('Y-m-d')."' order by noantrian limit 1";
            $noAntrian   = Yii::app()->db->createCommand($sqlCaraBayar)->queryRow();
            $noAntrianBaru = $dataCaraBayar['carabayar_singkatan'].(str_pad($noAntrian['noantrian']+1, 3, 0,STR_PAD_LEFT));
            $noAntrianBaru = str_split($noAntrianBaru);
            $loket = $dataCaraBayar['carabayar_loket'];
            
            $form = '';
            $formSuaraMp3 = '';
            $formSuaraOgg = '';
            

            for ($f = 0; $f < count($noAntrianBaru); $f++) {
                $formSuaraMp3 .= CHtml::textfield('mp3_'.$f.'',Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3');
                $formSuaraOgg .= CHtml::textfield('ogg_'.$f.'',Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$noAntrianBaru[$f].'.ogg');
                $form .= '<div id="jquery_jplayer_'.$f.'" class="jp-jplayer"></div>';
            }

            $data['antrian'] = $noAntrianBaru;
            $data['formLoketMp3'] = CHtml::textfield('loketMp3',Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$loket.'.mp3');
            $data['formLoketOgg'] = CHtml::textfield('loketOgg',Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$loket.'.ogg');
            $data['formSuaraMp3'] = $formSuaraMp3;
            $data['formSuaraOgg'] = $formSuaraOgg;
            $data['form'] = $form;
            $data['jumlah'] = count($noAntrianBaru);
            echo json_encode($data);
            Yii::app()->end();
        }
        
        public function actionSetFormAntrian(){
            
            $lokasi = $_POST['lokasi'];
            $jenis_antrian = $_POST['jenis_antrian'];
            $loket = $_POST['loket'];
            $record = isset($_POST['record']) ? $_POST['record'] : null;
            $antrianpasien_id = isset($_POST['antrianpasien_id']) ? $_POST['antrianpasien_id'] : null;
            
            if(empty($record)){
                $criteria=new CDbCriteria;
                $criteria->addCondition('lokasiantrian_id = '.$lokasi);
                $criteria->addCondition('jenisantrian_id = '.$jenis_antrian);
                $criteria->addBetweenCondition('DATE(tglantrianpasien)', date('Y-m-d'), date('Y-m-d'));
                $criteria->addCondition('pendaftaran_id IS NULL AND pasien_id IS NULL');
                $criteria->addCondition('jmlpemanggilan IS NULL OR jmlpemanggilan = 0');
                $criteria->addCondition('loketantrian_id IS NULL OR loketantrian_id='.$loket);
                $criteria->order = "antrianpasien_id ASC";
                $modAntrian = AntrianpasienT::model()->find($criteria);

                if(count($modAntrian) > 0){
                    $data['pesan'] = "OK";
                    $data['model'] = $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaran._formPanggilAntrian', array(
                                    'modAntrian' => $modAntrian), true);
                }else{
                    $data['pesan'] = "Antrian Habis";
                    $data['model'] = null;
                }
            }else{
                if(!empty($antrianpasien_id)){
                    
                    $criteria=new CDbCriteria;
                    $criteria->addCondition('lokasiantrian_id = '.$lokasi);
                    $criteria->addCondition('jenisantrian_id = '.$jenis_antrian);
                    $criteria->addBetweenCondition('DATE(tglantrianpasien)', date('Y-m-d'), date('Y-m-d'));
                    $criteria->addCondition('pendaftaran_id IS NULL AND pasien_id IS NULL');
                    $criteria->addCondition('loketantrian_id IS NULL OR loketantrian_id='.$loket);
                    
                    if ($record == 'next') {
                        $criteria->order = "antrianpasien_id ASC";
                        $criteria->addCondition("antrianpasien_id > ".$antrianpasien_id);
//                        $criteria->addCondition('jmlpemanggilan IS NULL OR jmlpemanggilan = 0');
                        $criteria->limit = 1;
                    } else if ($record == 'prev') {
                        $criteria->order = "antrianpasien_id DESC";
                        $criteria->addCondition("antrianpasien_id < ".$antrianpasien_id);
                        $criteria->limit = 1;
                    }else{
                        $criteria->addCondition('jmlpemanggilan IS NULL OR jmlpemanggilan = 0');
                    }
                    $modAntrian = AntrianpasienT::model()->find($criteria);

                    if(count($modAntrian) > 0){
                        $data['pesan'] = "OK";
                        $data['model'] = $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaran._formPanggilAntrian', array(
                                        'modAntrian' => $modAntrian), true);
                    }else{
                        $data['pesan'] = "Antrian Habis";
                        $data['model'] = null;
                    }
                    
                }else{
                    $data['pesan'] = "Antrian Habis";
                    $data['model'] = null;
                }
                
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
        
        public function actionPanggilAntrianNew(){
            
            $lokasi = $_POST['lokasi'];
            $jenis_antrian = $_POST['jenis_antrian'];
            $loket = $_POST['loket'];
            $record = isset($_POST['record']) ? $_POST['record'] : null;
            $antrianpasien_id = isset($_POST['antrianpasien_id']) ? $_POST['antrianpasien_id'] : null;
            
            $modAntrian = AntrianpasienT::model()->findByPk($antrianpasien_id);
            if(empty($modAntrian->loketantrian_id) || $modAntrian->loketantrian_id == $loket){
                
                $modAntrian->loketantrian_id = $loket;
                $modAntrian->tglpemanggilanpasien = date('Y-m-d H:i:s');
                $modAntrian->jmlpemanggilan = empty($modAntrian->jmlpemanggilan)? 1 : ($modAntrian->jmlpemanggilan + 1);
                if ($modAntrian->update()) {
                    $data['pesan'] = "OK"; //berhasil
                }else{
                    $data['pesan'] = "GAGAL"; //gagal
                }
                
            }else if(!empty($modAntrian->loketantrian_id) && $modAntrian->loketantrian_id != $loket){
                $data['pesan'] = "LOKET_LAIN"; //sudah dipanggil di loket lain
            }
            $data['model'] = $modAntrian->attributes;
            
            echo json_encode($data);
            Yii::app()->end();
        }
        
        public function actionPanggil(){
            
            $pendaftaran_id = $_POST['pendaftaran_id'];
            $model = PendaftaranT::model()->findByPk($pendaftaran_id);
            
            if($model->panggilantrian == FALSE){
                $data['pesan'] = "OK";
                $update = PendaftaranT::model()->updateByPk($pendaftaran_id, array('panggilantrian' => TRUE));
                if($update){
                    $data['pesan'] = "OK";
                }else{
                    $data['pesan'] = "GAGAL";
                }
            }else{
                $data['pesan'] = "SUDAH";
            }
            
            echo json_encode($data);
            Yii::app()->end();
        }
}