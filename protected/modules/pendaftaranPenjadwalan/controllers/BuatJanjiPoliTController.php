
<?php

class BuatJanjiPoliTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $pathView = 'pendaftaranPenjadwalan.views.buatJanjiPoliT.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PPBuatJanjiPoliT;
                $modPasien=new PPPasienM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $format = new CustomFormat;
		if(isset($_POST['PPBuatJanjiPoliT']))
		{
                      
                         $transaction = Yii::app()->db->beginTransaction();
                           try 
                           {    
                                $model->attributes=$_POST['PPBuatJanjiPoliT'];
                                $model->tglbuatjanji=date('Y-m-d H:i:s');
                                $model->tgljadwal=$format->formatDateTimeMediumForDB($_POST['PPBuatJanjiPoliT']['tgljadwal']);
                                $model->create_time=date('Y-m-d H:i:s');
                                $model->update_time=date('Y-m-d H:i:s');
                                $model->update_loginpemakai_id=Yii::app()->user->id;
                                $model->create_loginpemakai_id=Yii::app()->user->id;
                                $model->create_ruangan= Yii::app()->user->getState('ruangan_id');
                                $model->no_rekam_medik = $_POST['no_rekam_medik'];
                                $idPegawai = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$_POST['no_rekam_medik']));
                                $model->pasien_id = $idPegawai['pasien_id'];
                               
                                if(!isset($_POST['isPasienLama']))
                                {   //Jika Pasiennya Lama
                                     $modPasien = $this->savePasien($_POST['PPPasienM']);
                                     $model->pasien_id=$modPasien->pasien_id;
                                }                                
                               
                                if($model->validate())
                                    {
                                        $model->save();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data Pasien Dan Janji Kunjungan berhasil disimpan.');
                                        $transaction->commit();
                                    }
                                else 
                                    {
                                        $transaction->rollback();
                                         Yii::app()->user->setFlash('error', 'Data Gagal disimpan ');
                                    }
                                

                           }
                           catch(Exception $exc)
                           {
                               $transaction->rollback();
                               Yii::app()->user->setFlash('error', 'Data Gagal disimpan'.MyExceptionMessage::getMessage($exc,true).'');

                           }
		}

		$this->render($this->pathView.'create',array(
                        'modPasien'=>$modPasien,
                        'model'=>$model

		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modPPBuatJanjiPoli=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PPBuatJanjiPoliT']))
		{
			$modPPBuatJanjiPoli->attributes=$_POST['PPBuatJanjiPoliT'];
			if($modPPBuatJanjiPoli->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$modPPBuatJanjiPoli->buatjanjipoli_id));
                        }
		}

		$this->render($this->pathView.'update',array(
			'modPPBuatJanjiPoli'=>$modPPBuatJanjiPoli,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PPBuatJanjiPoliT');
		$this->render($this->pathView.'index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}

                $format = new CustomFormat();
                $model=new PPBuatJanjiPoliT;
                $model->tglAwal=date('d M Y').' 00:00:00';
                $model->tglAkhir=date('d M Y H:i:s');
		if(isset($_REQUEST['PPBuatJanjiPoliT']))
                    {    
//                    echo $_REQUEST['PPBuatJanjiPoliT']['tglAwal'].'dddd'.$_REQUEST['PPBuatJanjiPoliT']['tglAkhir'];
			$model->attributes=$_REQUEST['PPBuatJanjiPoliT'];
                        $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['PPBuatJanjiPoliT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPBuatJanjiPoliT']['tglAkhir']);
                     }
//                $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//                $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss'));    
		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPBuatJanjiPoliT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppbuat-janji-poli-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new PPBuatJanjiPoliT;
            $model->attributes=$_REQUEST['PPBuatJanjiPoliT'];
            $judulLaporan='Data Buat Janji Poli';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function savePasien($attrPasien)
        {
            $modPasien = new PPPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikJanjiPoli('JP','TRUE');
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            
            } else {
//                echo var_dump($_POST['PPPasienM']);exit;
                   // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
     
}
