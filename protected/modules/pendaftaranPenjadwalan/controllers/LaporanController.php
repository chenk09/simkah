<?php

class LaporanController extends SBaseController {

    public $pathViewPP = 'pendaftaranPenjadwalan.views.laporan.';
    public $pathViewRj = 'rawatJalan.views.laporan.';
    
    // -- Laporan RD --//
    public function actionLaporanDokterPemeriksaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Dokter Pemeriksa";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/dokterPemeriksaRD', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanPenjaminKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Penjamin";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/penjaminRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanKetPulangKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Keterangan Pulang";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/ketPulangRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanPemeriksaanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Pemeriksaan";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/pemeriksaanRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanRujukanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Rujukan";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/rujukanRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanCaraMasukKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Cara Masuk";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/caraMasukRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanKabKotaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kabupaten / Kota";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/kabupatenRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanKecamatanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kecamatan";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/kecamatanRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanAlamatKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Alamat";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/alamatRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanStatusPerkawinanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Status Perkawinan";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/statusPerkawinanRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanPekerjaanKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Pekerjaan";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/pekerjaanRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanAgamaKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Agama";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/agamaRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanStatusKunjunganRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Kedatangan Lama / Baru";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/statusRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanKunjunganUmurRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Umur";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/umurRD', array(
            'model' => $model,
        ));
    }

    public function actionLaporanKunjunganJkRD() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Darurat Berdasarkan Jenis Kelamin";
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatDarurat/jkRD', array(
            'model' => $model,
        ));
    }

    //-- Akhir Laporan RD--//

    // -- Laporan RJ --//
    public function actionLaporanKunjunganUmurRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Umur";
        $model = new PPInfoKunjunganRJV('searchUmur');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/umurRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusPerkawinanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Status Perkawinan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/statusPerkawinanRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanAlamatKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Alamat";
        $model = new PPInfoKunjunganRJV('searchAlamat');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $this->render($this->pathViewPP.'rawatJalan/alamatRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionlaporanKecamatanKunjunganRJ(){
        $this->pageTitle = Yii::app()->name."- Laproan Kunjungan Rawat Jalan Berdasarkan Kecamatan";
        $model= new PPInfoKunjunganRJV('searchRJ2');
        $model->tglAwal=  date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');

        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
       }
       $this->render($this->pathViewPP.'rawatJalan/kecamatanRJ',array('model'=>$model));

    }
    
    public function actionlaporanKabKotaKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Agama";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/kabupatenRJ',array('model'=>$model));
    }
    
     public function actionlaporanCaraMasukKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Cara Masuk";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/caraMasukRJ',array('model'=>$model));
    }
    
    public function actionlaporanRujukanKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Rujukan";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/rujukanRJ',array('model'=>$model));
    }
    
    public function actionlaporanPemeriksaanKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Pemeriksaan";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/pemeriksaanRJ',array('model'=>$model));
    }
    
    public function actionlaporanKetPulangKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Keterangan Pulang";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/ketPulangRJ',array('model'=>$model));
    }
    
    public function actionlaporanPenjaminKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Penjamin Pasien";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/penjaminRJ',array('model'=>$model));
    }
    
    public function actionlaporanDokterPemeriksaKunjunganRJ(){
        $tihs->pageTitle= Yii::app()->name."- Laporan Kunjungan Rawat Jalan Berdasarakan Penjamin Pasien";
        $model=new PPInfoKunjunganRJV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir= date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRJV'])){
            $model->attributes=$_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatJalan/dokterPemeriksaRJ',array('model'=>$model));
    }
           
   
    public function actionLaporanKunjunganJkRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Jenis Kelamin";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/jkRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Kedatangan Lama / Baru";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/statusRJ', array(
            'model' => $model,
        ));
    }
    
     public function actionLaporanAgamaKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Agama";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/agamaRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanPekerjaanKunjunganRJ() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Jalan Berdasarkan Pekerjaan";
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/pekerjaanRJ', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanUnitPelayananKunjunganRJ() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('Y-m-d').' 00:00:00';
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatJalan/unitPelayananRJ', array(
            'model' => $model,
        ));
    }
     // -- END VIEW LAPORAN RJ --//
    
    
 // -- Laporan RJ --//
    // -- VIEW LAPORAN RJ --// 
    public function actionLaporanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render('rawatJalan/adminRJ', array(
            'model' => $model,
        ));
    }
   // -- END VIEW LAPORAN RJ --//
   
   // -- PRINT LAPORAN RJ --//
    public function actionPrintKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionprintUmurKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchUmur');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintJkKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchJk');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintStatusKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchStatus');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

      public function actionprintAgamaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchAgama');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    } 

       public function actionprintPekerjaanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchPekerjaan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPekerjaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

       public function actionprintStatusPerkawinanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchStatusPerkawinan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintAlamatKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchAlamat');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAlamat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKecamatanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('printKecamatan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKabKotaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('printKabKota');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintCaraMasukKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchCaraMasuk');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintRujukanKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchRujukan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintPemeriksaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchPemeriksaan');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKetPulangKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchKetPulang');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintPenjaminKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchPenjamin');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintDokterPemeriksaKunjunganRJ() {
        $model = new PPInfoKunjunganRJV('searchDokterPemeriksa');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRJV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRJV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintUnitPelayananKunjunganRJ() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Jalan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printUnitPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }                

    
    // -- END LAPORAN RJ --//
    
    // -- GRAFIK LAPORAN RJ --//
    public function actionFrameGrafikRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionFrameGrafikAgamaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRJV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRJV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRJV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRJV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRJ() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Jalan Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    // -- END GRAFIK LAPORAN RJ --//
// -- END LAPORAN RJ --// 

// -- LAPORAN RD --//
    // -- VIEW LAPORAN RD --//
    public function actionLaporanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $this->render('rawatDarurat/adminRD', array(
            'model' => $model,
        ));
    }
    
    // -- END VIEW LAPORAN RD --//
    
    // -- PRINT LAPORAN RD --//
    
    public function actionPrintKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionprintUmurKunjunganRD() {
        $model = new PPInfoKunjunganRDV('searchUmur');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionprintJkKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintStatusKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintAgamaKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintPekerjaanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPekerjaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintStatusPerkawinanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintAlamatKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAlamat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintKecamatanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintKabKotaKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintCaraMasukKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintRujukanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintPemeriksaanKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionprintKetPulangKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionprintPenjaminKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }                                                  

    public function actionprintDokterPemeriksaKunjunganRD() {
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRDV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    // -- END PRINT LAPORAN RD --//
    
    //-- GRAFIK LAPORAN RD --//
    public function actionFrameGrafikRD() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

     public function actionFrameGrafikAgamaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRDV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRDV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRDV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRDV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRD() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Darurat Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    // -- END GRAFIK LAPORAN RD --//
// -- END LAPORAN RD --//
    
// -- LAPORAN RI --//
    // -- VIEW LAPORAN RI --//
    public function actionLaporanKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render('rawatInap/adminRI', array(
            'model' => $model,
        ));
    }
    public function actionLaporanKunjunganUmurRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Umur";
        $model = new PPInfoKunjunganRIV('searchUmur');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/umurRI', array(
            'model' => $model,
        ));
    }
    
    public function actionlaporanKunjunganJkRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Jenis Kelamin";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/jkRI', array(
            'model' => $model,
        ));
    }
    
    public function actionLaporanStatusKunjunganRI() {
        $this->pageTitle = Yii::app()->name." - Laporan Kunjungan Rawat Inap Berdasarkan Kedatangan Lama / Baru";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'rawatInap/statusRI', array(
            'model' => $model,
        ));
    }
    
    public function actionlaporanAgamaKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Kedatangan Agama";
        $model=new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal= $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/agamaRI',array('model'=>$model));
    }
    
    public function actionlaporanPekerjaanKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Pekerjaan";
        $model= new PPInfoKunjunganRIV('search');
        $model->tglAwal=  date('Y-m-d 00:00:00');
        $model->tglAkhir =date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $this->render($this->pathViewPP.'rawatInap/pekerjaanRI',array('model'=>$model));
    }
    
    public function actionlaporanStatusPerkawinanKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Status Perkawinan";
        $model= new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format = New CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/statusPerkawinanRI',array('model'=>$model));
    }
    
    public function actionlaporanAlamatKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Alamat";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPinfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/alamatRI',array('model'=>$model));
    }
    
    public function actionlaporanKecamatanKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Kecamatan";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/kecamatanRI',array('model'=>$model));
    }

    public function actionlaporanKabKotaKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Kota / Kabupaten";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/kabupatenRI',array('model'=>$model));
    }    
    
    
    public function actionlaporanCaraMasukKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Cara Masuk";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir =$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/caraMasukRI',array('model'=>$model));
    }

     public function actionlaporanRujukanKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Rujukan";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/rujukanRI',array('model'=>$model));
    }   
    
      public function actionlaporanRMKunjunganRI(){
        $this->pageTitle=  Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Rekam Medik";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal=date('Y-m-d 00:00:00');
        $model->tglAkhir=date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes=$_GET['PPInfoKunjunganRIV'];
            $format= new CustomFormat();
            $model->tglAwal=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/rmRI',array('model'=>$model));
    }   
    
    public function actionlaporanKamarRuanganKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Kamar Ruangan";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/kamarRuanganRI',array('model'=>$model));
    }
    
    public function actionlaporanKetPulangKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Keterangan Pulang";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/ketPulangRI',array('model'=>$model));
    }
    
    public function actionlaporanAlasanPulangKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Alasan Pulang";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/alasanPulangRI',array('model'=>$model));
    }
    
    public function actionlaporanPenjaminKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Penjamin Pasien";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/penjaminRI',array('model'=>$model));
    }
    
    public function actionlaporanDokterPemeriksaKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Dokter Pemeriksa";
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPInfoKunjunganRIV'])){
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/dokterPemeriksaRI',array('model'=>$model));
    }
    
    public function actionlaporanUnitPelayananKunjunganRI(){
        $this->pageTitle = Yii::app()->name."- Laporan Kunjungan Rawat Inap Berdasarkan Unit Pelayanan";
        $model = new PPRuanganM('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        
        if(isset($_GET['PPRuanganM'])){
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }
        
        $this->render($this->pathViewPP.'rawatInap/unitPelayananRI',array('model'=>$model));
    }    
    // -- END VIEW LAPORAN RI --//
    
    //-- PRINT LAPORAN RI --//
    public function actionPrintKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Darurat';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);      
    }

    public function actionprintUmurKunjunganRI() {
        $model = new PPInfoKunjunganRIV('searchUmur');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
            
        }        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printUmur';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintJkKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printJk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionprintStatusKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatus';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

      public function actionprintAgamaKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAgama';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    } 

       public function actionprintPekerjaanKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPekerjaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

       public function actionprintStatusPerkawinanKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printStatusPerkawinan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintAlamatKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAlamat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKecamatanKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKecamatan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKabKotaKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKabupaten';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintCaraMasukKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printCaraMasuk';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintRujukanKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printRujukan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintRMKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printrmRI';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKamarRuanganKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKamarRuanganRI';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintAlasanPulangKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printAlasanPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }                        

       public function actionprintPemeriksaKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPemeriksaan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintKetPulangKunjunganRI() {
        $model = new PPInfoKunjunganRIV('searchKetPulang');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printKetPulang';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintPenjaminKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printPenjamin';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintDokterPemeriksaKunjunganRI() {
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPInfoKunjunganRIV'])) {
            $model->attributes = $_REQUEST['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printDokterPemeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

       public function actionprintUnitPelayananKunjunganRI() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $judulLaporan = 'Laporan Kunjungan Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_printUnitPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }    
    
    // -- END PRINT LAPORAN RI --//
    
    // -- GRAFIK LAPORAN RI --//
    public function actionFrameGrafikRI() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
     public function actionFrameGrafikAlasanPulangRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alasan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlasanPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionFrameGrafikAgamaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Agama';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAgama', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikAlamatRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Alamat';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikAlamat', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikCaraMasukRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Cara Masuk';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikCaraMasuk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikDokterPemeriksaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Dokter Pemeriksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikDokterPemeriksa', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikJkRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Jenis Kelamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikJk', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKabKotaRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kabupaten / Kota';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKabupaten', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKecamatanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kecamatan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKecamatan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikKetPulangRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Keterangan Pulang';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKetPulang', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPekerjaanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pekerjaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPekerjaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPemeriksaanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Pemeriksaan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPemeriksaan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikPenjaminRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Penjamin';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikPenjamin', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikRujukanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rujukan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikRujukan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusPerkawinanRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Status Perkawinan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatusPerkawinan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikStatusRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kedatangan Lama / Baru';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikStatus', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUmurRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Umur';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUmur', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    public function actionFrameGrafikUnitPelayananRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionFrameGrafikRMRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Rekam Medik';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikrmRI', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
     public function actionFrameGrafikKamarRuanganRI() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPInfoKunjunganRIV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pasien Rawat Inap Berdasarkan Kamar Ruangan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPInfoKunjunganRIV'])) {
            $model->attributes = $_GET['PPInfoKunjunganRIV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPInfoKunjunganRIV']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikKamarRuanganRI', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    // -- END GRAFIK LAPORAN RI --//
    
// -- END LAPORAN RI --//
    public function actionLaporanBukuRegister() {
        $model = new PPBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPBukuregisterpasienV'])) {
            $model->attributes = $_GET['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAkhir']);
        }

        $this->render('bukuRegister/adminBukuRegister', array(
            'model' => $model,
        ));
    }

    public function actionPrintBukuRegister() {
        $model = new PPBukuregisterpasienV('search');
        $judulLaporan = 'Laporan Buku Register';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPBukuregisterpasienV'])) {
            $model->attributes = $_REQUEST['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPBukuregisterpasienV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_printBukuRegister';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Grafik Buku Register';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPBukuregisterpasienV'])) {
            $model->attributes = $_GET['PPBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPBukuregisterpasienV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanBatalPeriksa() {
        $model = new PPPasienbatalperiksa('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPPasienbatalperiksa'])) {
            $model->attributes = $_GET['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAkhir']);
        }

        $this->render('batalPeriksa/adminBatalPeriksa', array(
            'model' => $model,
        ));
    }

    public function actionPrintBatalPeriksa() {
        $model = new PPPasienbatalperiksa('search');
        $judulLaporan = 'Laporan Pasien Batal Periksa';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Batal Periksa';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPPasienbatalperiksa'])) {
            $model->attributes = $_REQUEST['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPPasienbatalperiksa']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'batalPeriksa/_printBatalPeriksa';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBatalPeriksa() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPPasienbatalperiksa('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Batal Periksa';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPPasienbatalperiksa'])) {
            $model->attributes = $_GET['PPPasienbatalperiksa'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPPasienbatalperiksa']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporan10Besar() {
        $model = new PPLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;
        if (isset($_GET['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/admin10BesarPenyakit', array(
            'model' => $model,
        ));
    }

    public function actionPrint10BesarPenyakit() {
        $model = new PPLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print10BesarPenyakit';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['PPLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PPLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKarcis() {
        $model = new PPLaporankarcispasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankarcispasien'])) {
            $model->attributes = $_GET['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAkhir']);
        }

        $this->render('karcis/adminKarcis', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKarcis() {
        $model = new PPLaporankarcispasien('search');
        $judulLaporan = 'Laporan Karcis Pasien';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Karcis Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankarcispasien'])) {
            $model->attributes = $_REQUEST['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankarcispasien']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'karcis/_printLaporanKarcis';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameLaporanKarcis() {
        $this->layout = '//layouts/frameDialog';
        $model = new PPLaporankarcispasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Karcis Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PPLaporankarcispasien'])) {
            $model->attributes = $_GET['PPLaporankarcispasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankarcispasien']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanKunjunganRS() {
        $model = new PPLaporankunjunganrs('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganrs'])) {
            $model->attributes = $_GET['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
        }

        $this->render('kunjunganRS/adminKunjunganRS', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjunganRS() {
        $model = new PPLaporankunjunganrs('search');
        $judulLaporan = 'Laporan Kunjungan Rumah Sakit';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Rumah Sakit';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganrs'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganrs']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjunganRS/_printKunjunganRS';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjunganRS() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPLaporankunjunganrs('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Rumah Sakit';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPLaporankunjunganrs'])) {
            $model->attributes = $_GET['PPLaporankunjunganrs'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganrs']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
//    protected function parserTanggal($tgl){
//        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($tgl, 'yyyy-MM-dd hh:mm:ss'));
//    }
    
    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
    
    
    public function actionLaporanKunjunganDokter() {
        $model = new PPLaporankunjunganbydokterV('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_GET['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAkhir']);
        }

        $this->render('kunjunganDokter/adminKunjunganDokter', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjunganDokter() {
        $model = new PPLaporankunjunganbydokterV('search');
        $judulLaporan = 'Laporan Kunjungan Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_REQUEST['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPLaporankunjunganbydokterV']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjunganDokter/_printKunjunganDokter';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjunganDokter() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPLaporankunjunganbydokterV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Dokter';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPLaporankunjunganbydokterV'])) {
            $model->attributes = $_GET['PPLaporankunjunganbydokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPLaporankunjunganbydokterV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPerUnitPelayanan() {
        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'unitPelayanan/adminUnitPelayanan', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPerUnitPelayanan() {
        $model = new PPRuanganM('search');
        $judulLaporan = 'Laporan Kunjungan Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Per Unit Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PPRuanganM'])) {
            $model->attributes = $_REQUEST['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPRuanganM']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewPP.'unitPelayanan/_printUnitPelayanan';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPerUnitPelayanan() {
        $this->layout = '//layouts/frameDialog';

        $model = new PPRuanganM('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Per Unit Pelayanan';
        $data['type'] = $_GET['type'];

        if (isset($_GET['PPRuanganM'])) {
            $model->attributes = $_GET['PPRuanganM'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPRuanganM']['tglAkhir']);
        }

        $this->render($this->pathViewPP.'_grafikUnitPelayanan', array(
            'model' => $model,
            'data' => $data,
        ));
    }
}