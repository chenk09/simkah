<?php

class InfoKontrolPasienController extends SBaseController
{
    public $layout='//layouts/column1';
    public $defaultAction = 'admin';
    
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array('index','view'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('create','update','print'),
                'users'=>array('@'),
            ),
            array('allow',
                'actions'=>array('admin','delete','dynamicRuangan'),
                'users'=>array('@'),
            ),
            array('deny',
                'users'=>array('*'),
            ),
        );
    }

    public function actionIndex()
    {
//                if(!Yii::app()->user->checkAccess(Params::)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=new PPPendaftaranT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if(isset($_GET['PPPendaftaranT']))
        {
            $model->attributes = $_GET['PPPendaftaranT'];
            /*
            $model->no_rekam_medik = $_GET['PPPendaftaranT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['PPPendaftaranT']['nama_pasien'];
            $model->alamat_pasien = $_GET['PPPendaftaranT']['alamat_pasien'];
            $model->ruangan_id = $_GET['PPPendaftaranT']['ruangan_id'];
             * 
             */
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PPPendaftaranT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PPPendaftaranT']['tglAkhir']);
        }

        $this->render('index',
            array(
                'model'=>$model,
            )
        );
    }

    public function loadModel($id)
    {
        $model = PPPendaftaranT::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='ppinfokontrolpasien-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionDynamicRuangan()
    {
        
        $dataRuangan = array();
        if (isset($_POST['PPPendaftaranT']['instalasi_id']))
        {
            $instalasi = (int)$_POST['PPPendaftaranT']['instalasi_id'];
            $dataRuangan = RuanganM::model()->findAll('instalasi_id=:instalasi_id AND ruangan_aktif = TRUE order by ruangan_nama', array(':instalasi_id'=>$instalasi));
        }
        
        $data = CHtml::listData($dataRuangan,'ruangan_id','ruangan_nama');
        $i=0;
        if(count($data) > 0)
        {
            if($i==0)
            {
                echo CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
            }
            
            foreach($data as $value=>$name)
            {
                echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
            	$i++;
            }
        }
    }
}