<?php

class PemeriksaanFisikAnamnesaController extends SBaseController
{
    public $pathView = 'pendaftaranPenjadwalan.views.pemeriksaanFisikAnamnesa.';
    
    public function actionIndexAnamnesa($idPendaftaran)
    {
        $modPendaftaran=PPPendaftaranT::model()->findByPk($idPendaftaran);

        $modPasien = PPPasienM::model()->findByPk($modPendaftaran->pasien_id);

        $dataPendaftaran = PPPendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
        //print_r($lastPendaftaran);
        //echo $modPasien->pasien_id;exit();
        $i = 1;
        if (count($dataPendaftaran) > 1){
            foreach ($dataPendaftaran as $row){
                if ($i == 2){
                    $lastPendaftaran = $row->pendaftaran_id;
                }
                $i++;
            }
        }else{
            $lastPendaftaran = $idPendaftaran;
        }


        $cekAnamnesa=PPAnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
        $modDiagnosa = new PPDiagnosaM;

        if(COUNT($cekAnamnesa)>0) {  //Jika Pasien Sudah Melakukan Anamnesa Sebelumnya
            $modAnamnesa=$cekAnamnesa;
            //$modAnamnesa->riwayatimunisasi = $modPendaftaran->statuspasien;
        } else {  
            ////Jika Pasien Belum Pernah melakukan Anamnesa
            $modAnamnesa=new PPAnamnesaT;
            $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
            $modAnamnesa->pendaftaran_id=$modPendaftaran->pendaftaran_id;
            $modAnamnesa->pasien_id=$modPendaftaran->pasien_id;
            $modAnamnesa->tglanamnesis=date('Y-m-d H:i:s');

        }

        if ($modPendaftaran->statuspasien == "PENGUNJUNG LAMA"){
            $modDiagnosaTerdahulu = PPPasienMorbiditasT::model()->with('diagnosa')->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id, 'pendaftaran_id'=>$lastPendaftaran));

            $hasilImunisasi = array();
            $hasilDiagnosaDahulu = array();
            foreach($modDiagnosaTerdahulu as $row){
                if ($row->diagnosa->diagnosa_imunisasi == true)
                    $hasilImunisasi[] = $row->diagnosa->diagnosa_nama;
                else
                    $hasilDiagnosaDahulu[] = $row->diagnosa->diagnosa_nama;
            }
            if (empty($modAnamnesa->riwayatimunisais)){
                $modAnamnesa->riwayatimunisasi = implode(', ',$hasilImunisasi);
            }
            if (empty($modAnamnesa->riwayatpenyakitterdahulu)){
                $modAnamnesa->riwayatpenyakitterdahulu = implode(', ',$hasilDiagnosaDahulu);
            }
        }

        //echo $modAnamnesa->riwayatpenyakitterdahulu;exit();
        if(isset($_POST['PPAnamnesaT'])) {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $modAnamnesa->attributes=$_POST['PPAnamnesaT'];
                $modAnamnesa->keluhanutama = (count($_POST['PPAnamnesaT']['keluhanutama'])>0) ? implode(', ', $_POST['PPAnamnesaT']['keluhanutama']) : '';
                $modAnamnesa->keluhantambahan = (count($_POST['PPAnamnesaT']['keluhantambahan'])>0) ? implode(', ', $_POST['PPAnamnesaT']['keluhantambahan']) : '';
                $modAnamnesa->save();
                $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                $transaction->commit();
                Yii::app()->user->setFlash('success',"Data Anamnesa berhasil disimpan");
                $this->redirect($_POST['url']);       
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
        }

        $modAnamnesa->tglanamnesis = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modAnamnesa->tglanamnesis, 'yyyy-MM-dd hh:mm:ss'));    

        $modDataDiagnosa = new PPDiagnosaM('search');
        $modDataDiagnosa->unsetAttributes();
        if(isset($_GET['PPDiagnosaM']))
            $modDataDiagnosa->attributes = $_GET['PPDiagnosaM'];

        $this->render($this->pathView.'indexAnamnesis',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,
                    'modAnamnesa'=>$modAnamnesa, 'modDiagnosa'=>$modDiagnosa, 'modDataDiagnosa'=>$modDataDiagnosa,
            ));
    }
    
    public function actionIndexPemeriksaanFisik($idPendaftaran)
    {   
//            $result = $this->xmlParser();

        $format = new CustomFormat();
        $modPendaftaran = PPPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
        $modPasien = PPPasienM::model()->findByPk($modPendaftaran->pasien_id);
        //$modRJMetodeGSCM = RJMetodeGCSM::model()->findAll('metodegcs_aktif=TRUE ORDER BY metodegcs_singkatan,metodegcs_nilai DESC');
        $modRJMetodeGSCM = PPMetodeGCSM::model()->findAll('metodegcs_aktif=TRUE ORDER BY metodegcs_id');
        $cekPemeriksaanFisik=PPPemeriksaanFisikT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            if(COUNT($cekPemeriksaanFisik)>0)
                {  //Jika Pasien Sudah Melakukan Pemeriksaan Fisik  Sebelumnya
                    $modPemeriksaanFisik=$cekPemeriksaanFisik;
                }
            else
                {  //Jika Pasien Belum Pernah melakukan Pemeriksaan Fisik
                    $modPemeriksaanFisik=new PPPemeriksaanFisikT;
                    $modPemeriksaanFisik->pegawai_id=$modPendaftaran->pegawai_id;
                    $modPemeriksaanFisik->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                    $modPemeriksaanFisik->pasien_id=$modPasien->pasien_id;
                    $modPemeriksaanFisik->tglperiksafisik=date('Y-m-d H:i:s');
                }
//            $modPemeriksaanFisik->td_diastolic = $result[2];
//            $modPemeriksaanFisik->td_systolic = $result[1];
//            $modPemeriksaanFisik->detaknadi = $result[3];
//            
//            $modPemeriksaanFisik->tekanandarah = $this->panjangText($modPemeriksaanFisik->td_diastolic, $modPemeriksaanFisik->td_systolic);;
//            echo $modPemeriksaanFisik->tekanandarah;exit();
                if(isset($_POST['PPPemeriksaanFisikT']))
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                            $modPemeriksaanFisik->attributes=$_POST['PPPemeriksaanFisikT'];  
                            $modPemeriksaanFisik->keadaanumum = (count($_POST['PPPemeriksaanFisikT']['keadaanumum'])>0) ? implode(', ', $_POST['PPPemeriksaanFisikT']['keadaanumum']) : ''; 
                            $modPemeriksaanFisik->tglperiksafisik=$format->formatDateTimeMediumForDB($_POST['PPPemeriksaanFisikT']['tglperiksafisik']);

                            if($modPemeriksaanFisik->validate()){
                               if($modPemeriksaanFisik->save()){
                                  $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                                  $transaction->commit();
                               }else{
                                   echo "gagal Simpan";exit;
                               } 

                            }  
                            Yii::app()->user->setFlash('success',"Data Pemeriksaan Fisik berhasil disimpan");
                            $this->redirect($_POST['url']); 
                         }
                    catch (Exception $exc) 
                        {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data Pemeriksaan Fisik gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        }
                } 
              $modPemeriksaanFisik->tglperiksafisik = Yii::app()->dateFormatter->formatDateTime(
                                    CDateTimeParser::parse($modPemeriksaanFisik->tglperiksafisik, 'yyyy-MM-dd hh:mm:ss'));       

              $this->render($this->pathView.'indexPemeriksaanFisik',array('modPasien'=>$modPasien,
                    'modPemeriksaanFisik'=>$modPemeriksaanFisik,
                    'modPendaftaran'=>$modPendaftaran,
                    'modRJMetodeGSCM'=>$modRJMetodeGSCM
            ));

    }
    
}

?>