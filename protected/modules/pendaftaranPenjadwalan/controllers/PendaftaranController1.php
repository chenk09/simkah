<?php
Yii::import('rawatJalan.controllers.TindakanController');//untuk load rekening
class PendaftaranController extends SBaseController
{
        public $defaultAction = 'rawatJalan';
        public $successSave = false;
        public $successSaveAdmisi = false; //variabel untuk validasi admisi
        public $successSaveRujukan = true; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
        public $successSaveTindakanKomponen= true;
        public $successSaveMasukKamar=true;
        public $successSaveKecelakaan=true;
        public $pathView = 'pendaftaranPenjadwalan.views.pendaftaran.';
//        public $successSavePJ = '';
        
        /**
	 * @return array action filters
	 */
//        public function filters()
//	{
//            return array(
//                'accessControl', // perform access control for CRUD operations
//            );
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
        

        /*
         * diganti dengan yg di extend dari SBaseController
        public function accessRules()
	{
            return array(
                    array(
                        'allow',
                        'actions'=>array('rawatJalan'),
                        'users'=>array('yosef_heryanto'),
                    ),
                    array(
                        'deny',
                        'users'=>array('*'),
                    ),
            );
            return array(
                    array('allow',  // allow all users to perform 'index' and 'view' actions
                            'actions'=>array('rawatJalan','rawatDarurat','rawatInap', 'masukPenunjang', 'saveKartuPasien'),
                            'users'=>array('@'),
                    ),
                    array('deny',  // deny all users
                            'users'=>array('*'),
                    ),
            );
	}
         * 
         */
        
	public function actionRawatInap()
	{
            $insert_notifikasi = new MyFunction();
            $format = new CustomFormat();
            $this->pageTitle = Yii::app()->name." - Pendaftaran Rawat Inap";
            $modPasienAdmisi = new PPPasienAdmisiT;
            $modPasienAdmisi->tgladmisi = date('d M Y H:i:s');
            $model = new PPPendaftaranRi;
            $model->thn = '00';
            $model->bln = '00';
            $model->hr = '00';
            $modRujukan = new PPRujukanT;
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            $modSep = New SepT;
            $modSep->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
            $modSep->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
            $modSep->tglsep = date('Y-m-d');
            $modSep->jenisrujukan_kode_bpjs = 1;
            $modSep->polieksekutif = 0;
            $modSep->cob_bpjs = 0;
            $modSep->cob_status = "TIDAK";
            $modSep->lakalantas_kode = 0;
            $modSep->jnspelayanan_kode = 1;//RI
            $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
            if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            }else{
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            }
            
            if(!empty($_POST['pendaftaran_id'])){
                $model = PPPendaftaranRi::model()->findByPk($_POST['pendaftaran_id']);
                if(!empty($model->sep_id)){
                    $modSep = SepT::model()->findByPk($model->sep_id);
                }
                $model->isPasienLama = true;
                $model->statuspasien = Params::statusPasien('lama');
                $modPasienAdmisi->kelaspelayanan_id = $model->kelaspelayanan_id;
                $modPasienAdmisi->carabayar_id = $model->carabayar_id;
                $modPasienAdmisi->penjamin_id = $model->penjamin_id;
                $modPasienAdmisi->pegawai_id = $model->pegawai_id;
                $modPasienAdmisi->shift_id = Yii::app()->user->getState('shift_id');
                $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$model->pasien_id),array('order'=>'pendaftaran_id desc'));
                $umur = explode(" ",$pendaftaran[0]->umur); 
                // echo $pendaftaran[0]->umur;exit;
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                // $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
            } else {
                $model = new PPPendaftaranRi;
                $model->tgl_pendaftaran = date('d M Y H:i:s');
                $model->tgl_konfirmasi = date('d M Y H:i:s');
                $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = '00';
                $model->bln = '00';
                $model->hr = '00';
                $model->statuspasien = Params::statusPasien('baru');
//                $modPasienAdmisi->kelaspelayanan_id = Params::KELASPELAYANAN_RI; // menset default kelas pelayanan rawat inap
                $modPasienAdmisi->shift_id = Yii::app()->user->getState('shift_id');
            }
            
            if(!empty($model->penanggungjawab_id)){
                $modPenanggungJawab = PPPenanggungJawabM::model()->findByPk($model->penanggungjawab_id);
            } else {
                $modPenanggungJawab = new PPPenanggungJawabM;
            }
            
            if(isset($_POST['isPasienLama'])){
                $modPasien = PPPasienM::model()->findByPk($_POST['pasien_id']);
                $model->isPasienLama = true;
                $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                $model->noRekamMedik = $modPasien->no_rekam_medik;
                $model->statuspasien = Params::statusPasien('lama');
            } else{
                    $modPasien = new PPPasienM;
                    $model->statuspasien = Params::statusPasien('baru');
                    $modPasien->tanggal_lahir = date('d M Y');
            }
            
            if(!empty($_POST['bookingkamar_id'])){
                $modBooking = PPBookingKamarT::model()->findByPk($_POST['bookingkamar_id']);
                if ($modBooking->pasienadmisi_id){
                    $error["code"] = "403";
                    $error["title"] ='Pasien Sudah Dirawat Inap';
                    $error["message"] = 'Pasien Sudah Dirawat Inap';
                      $this->render(Yii::app()->getModule('srbac')->notAuthorizedView, array("error" => $error));
                }
//                $modPasien = PPPasienM::model()->findByPk($modBooking->pasien_id);
                $modPasienAdmisi->bookingkamar_id = $modBooking->bookingkamar_id;
                $modPasienAdmisi->ruangan_id = $modBooking->ruangan_id;
                $modPasienAdmisi->kamarruangan_id = $modBooking->kamarruangan_id;
                $modPasienAdmisi->kelaspelayanan_id = $modBooking->kelaspelayanan_id;
                $modPasienAdmisi->shift_id = Yii::app()->user->getState('shift_id');
                $model->kelaspelayanan_id = $modBooking->kelaspelayanan_id;
                $model->noRekamMedik = $modPasien->no_rekam_medik;
                
            } else {
                $modPasienAdmisi->bookingkamar_id = null;
            }
            
            
            
            if(isset($_POST['PPPendaftaranRi']))
            {
                $isDaftarLangsung = (!empty($_POST['PPPendaftaranRi']['pendaftaran_id'])) ? false : true;
                $model->attributes = $_POST['PPPendaftaranRi'];
                $model->tgl_pendaftaran = $format->formatDateTimeMediumForDB($_POST['PPPendaftaranRi']['tgl_pendaftaran']);
                if(isset($_POST['PPPasienM']))
                {
                    $modPasien->attributes = $_POST['PPPasienM'];
                    $modPasien->tanggal_lahir = (!empty($_POST['PPPasienM']['tanggal_lahir'])) ? $format->formatDateMediumForDB($_POST['PPPasienM']['tanggal_lahir']) : null;
                    $modPasien->jenisidentitas = (!empty($_POST['PPPasienM']['jenisidentitas'])) ? $_POST['PPPasienM']['jenisidentitas'] : "-";
                }
                if(isset($_POST['PPRujukanT']))
                {
                    $modRujukan->attributes = $_POST['PPRujukanT'];
                }
                $modPenanggungJawab->attributes = $_POST['PPPenanggungJawabM'];
                $modPasienAdmisi->attributes = $_POST['PPPasienAdmisiT'];
                $modPasienAdmisi->tgladmisi = $format->formatDateTimeMediumForDB($_POST['PPPasienAdmisiT']['tgladmisi']);
                $model->kelaspelayanan_id = $modPasienAdmisi->kelaspelayanan_id;
                
                $modPasienAdmisi->shift_id = Yii::app()->user->getState('shift_id');
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if(!isset($_POST['isPasienLama'])) {
                        $modPasien = $this->savePasien($_POST['PPPasienM']);
                        $model->statuspasien = Params::statusPasien('baru');
                        if($model->tgl_konfirmasi == null){
                            $model->tgl_konfirmasi == null;
                        }
                    }else {
                        $model->isPasienLama = true;
                        $model->statuspasien = Params::statusPasien('lama');
                        if(!empty($_POST['noRekamMedik'])) {
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien)) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }

                    if(isset($_POST['isRujukan'])) {
                        $model->isRujukan = true;
                        $model->statusmasuk = 'RUJUKAN';
                        $modRujukan = $this->saveRujukan($_POST['PPRujukanT']);
                    } else {
                        $model->statusmasuk = 'NON RUJUKAN';
                    }
                    
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['PPPenanggungJawabM']);
                    }
//                    echo "<pre>";
//                    print_r($isDaftarLangsung);
//                    exit();
                    
                    if(count($modPasien)>0) {
                        if($isDaftarLangsung) {
                            $model = $this->savePendaftaranRI($model,$modPasien,$modRujukan,$modPenanggungJawab,$modPasienAdmisi);
                        } else {
                            $model = PPPendaftaranRi::model()->findByPk($_POST['PPPendaftaranRi']['pendaftaran_id']);
                            if(isset($modPenanggungJawab)){
                                PendaftaranT::model()->updateByPk($model->pendaftaran_id, array('penanggungjawab_id'=>$modPenanggungJawab->penanggungjawab_id));
                            }
                            PendaftaranT::model()->updateByPk($model->pendaftaran_id, array('alihstatus'=>true, 'statusperiksa' => Params::statusPeriksa(6)));
                        }
                        if(isset($_POST['SepT'])){
                            $modSep = $this->saveSep($modSep,$modPasien,$model,$_POST['SepT']);
                        }
                        
                        $modPasienAdmisi = $this->savePasienAdmisi($model, $modPasien, $modPasienAdmisi);
                        if(!empty($modPasienAdmisi->kamarruangan_id)){
                            $kamar = KamarruanganM::model()->findByPk($modPasienAdmisi->kamarruangan_id);
                            if ($kamar->kamarruangan_status){
                                KamarruanganM::model()->updateByPk($modPasienAdmisi->kamarruangan_id,array('kamarruangan_status'=>false,
                                   'keterangan_kamar'=>'IN USE'));
                            }
                            else{
                                $model->addError('kamarruangan_id', 'Kamar masih digunakan.');
                                $this->successSaveMasukKamar = false;
                            }
                        }
                    } else {
                        $this->successSave = false;
                        $modPasien = new PPPasienM;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('danger',"Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar!");
                    }
                    $this->saveUbahCaraBayar($model);
                    if(isset($_POST['karcisTindakan']) && isset($_POST['TindakanPelayananT']) && !empty($_POST['TindakanPelayananT']['idTindakan']))
                    {
//                        $this->saveTindakanPelayanan($modPasien,$model);
                        $this->saveTindakanPelayanan($modPasien,$model,$kelas,$_POST['RekeningakuntansiV']);
                    }
                    if(isset($_POST['karcisTindakan']) && isset($_POST['TindakanPelayananT']) && !empty($_POST['TindakanPelayananT']['idTindakan']))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model,null,$_POST['RekeningakuntansiV']);
                    }

                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = 4;
                    $params['modul_id'] = 7;
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $model->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $params['create_ruangan'] = $model->ruangan_id;
                    $params['judulnotifikasi'] = ($model->isPasienLama == true ? 'Pendaftaran Pasien Lama' : 'Pendaftaran Pasien Baru' );
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                    if($this->successSaveAdmisi && $this->successSavePJ && $this->successSaveMasukKamar) {
                        $transaction->commit();
                        //$transaction->rollback();
                         $model->isNewRecord = FALSE;
                         $modPasienAdmisi->isNewRecord = FALSE;
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $modPasienAdmisi->isNewRecord = TRUE;
                    }
                    
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
            //$model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = FALSE;
            $model->adaPenanggungJawab = true;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
            if(empty($_POST['bookingkamar_id'])){
                $modPasien->tanggal_lahir = (!empty($modPasien->tanggal_lahir)) ? date('d M Y',strtotime($modPasien->tanggal_lahir)) : null;
            }
            $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$model->pasien_id),array('order'=>'pendaftaran_id desc'));
            if (isset($pendaftaran[0])){
                $umur = explode(" ",$pendaftaran[0]->umur); 
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
                // $model->umur = '00 Thn 00 Bln 00 Hr';
            }                 

            $this->render('rawatInap',array('model'=>$model,
                                            'modPasienAdmisi'=>$modPasienAdmisi,
                                            'modPasien'=>$modPasien,
                                            'modPenanggungJawab'=>$modPenanggungJawab,
                                            'modRujukan'=>$modRujukan,
                                            'modRekenings'=>$modRekenings,
                                            'modSep'=>$modSep,
                ));
	}
        
        public function savePendaftaranRI($model,$modPasien,$modRujukan,$modPenanggungJawab,$modPasienAdmisi)
        {
            $modelNew = new PPPendaftaranRi;
            $id = $modPasienAdmisi->pasien_id;
            $modPasienAd = PasienpulangT::model()->findByAttributes(array('pasien_id'=>$id));

            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->carabayar_id = $modPasienAdmisi->carabayar_id;
            $modelNew->penjamin_id = $modPasienAdmisi->penjamin_id;
            $modelNew->caramasuk_id = $modPasienAdmisi->caramasuk_id;
            $modelNew->kelaspelayanan_id = $modPasienAdmisi->kelaspelayanan_id;
            $modelNew->pegawai_id = $modPasienAdmisi->pegawai_id;
            $modelNew->ruangan_id = $modPasienAdmisi->ruangan_id;
            $modelNew->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            $modelNew->no_pendaftaran = KeyGenerator::noPendaftaran(Params::singkatanNoPendaftaranRI());
            $modelNew->instalasi_id = KeyGenerator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_urutantri = KeyGenerator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = KeyGenerator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
            //$modelNew->statuspasien = KeyGenerator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(6);
            $modelNew->kunjungan = KeyGenerator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            
            
            if($modelNew->validate()) {
                // form inputs are valid, do something here
                // if( !empty($modPasienAd) ) {
                //          echo "<script>
                //         alert('Pasien tidak bisa melakukan pendaftaran karena pasien belum pulang');
                //         window.top.location.href='".Yii::app()->createUrl('pendaftaranPenjadwalan/Pendaftaran/RawatInap')."';
                //     </script>";
                   
                // }else{
                    $modelNew->save();
                // }
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'),'medium',null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
                    
            return $modelNew;
        }
        
        public function savePasienAdmisi($model,$modPasien,$modPasienAdmisi)
        {
            $modPasienAdmisiNew = new PPPasienAdmisiT;
            $format = new CustomFormat();
//            $model = new PendaftaranT;
            $modPasienAdmisiNew->attributes = $modPasienAdmisi->attributes;
            $modPasienAdmisiNew->bookingkamar_id = $modPasienAdmisiNew->bookingkamar_id;
            $modPasienAdmisiNew->pendaftaran_id = $model->pendaftaran_id;
            $modPasienAdmisiNew->tglpendaftaran = $model->tgl_pendaftaran;
            $modPasienAdmisiNew->tgladmisi = date('Y-m-d H:i:s');
            $modPasienAdmisiNew->pasien_id = $model->pasien_id;
            $modPasienAdmisi->shift_id = Yii::app()->user->getState('shift_id');
            $modPasienAdmisiNew->kunjungan = KeyGenerator::getKunjungan($modPasien, $modPasienAdmisiNew->ruangan_id);
            $modPasienAdmisiNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modPasienAdmisiNew->tglpulang = null;
            $modPasienAdmisiNew->rencanapulang = null;
            $modPasienAdmisiNew->create_time = $modPasienAdmisiNew->tgladmisi;
            $modPasienAdmisiNew->update_time = $modPasienAdmisiNew->tgladmisi;
            
            if($modPasienAdmisiNew->validate()) {
                if($modPasienAdmisiNew->save()) {
                    $test = BookingkamarT::model()->updateByPk($modPasienAdmisiNew->bookingkamar_id,array('pasienadmisi_id'=>$modPasienAdmisiNew->pasienadmisi_id,
                        'pendaftaran_id'=>$modPasienAdmisiNew->pendaftaran_id, 'pasien_id' => $modPasienAdmisiNew->pasien_id));
                    $this->updatePendaftaranT($modPasienAdmisiNew);
                    if(!empty($modPasienAdmisiNew->masukkamar)){ // JIka Memasukan Kamar
                        $this->saveMasukKamar($model, $modPasien, $modPasienAdmisiNew);
                    }
                    $this->successSaveAdmisi = true;
                } else {
                    $this->successSaveAdmisi = false;
                }
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasienAdmisiNew->tgladmisi = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasienAdmisiNew->tgladmisi, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasienAdmisiNew;
        }
        
        protected function updatePendaftaranT($modAdmisi)
        {
            if(PPPendaftaranRi::model()->updateByPk($modAdmisi->pendaftaran_id,array('pasienadmisi_id'=>$modAdmisi->pasienadmisi_id))){
                return true;
            } else {
                return false;
            }
        }


        public function saveMasukKamar($model, $modPasien, $modPasienAdmisiNew)
        {

            $modMasukKamar = new MasukkamarT;
            $modMasukKamar->carabayar_id=$model->carabayar_id;
            $modMasukKamar->kamarruangan_id= (!empty($modPasienAdmisiNew->kamarruangan_id)) ? $modPasienAdmisiNew->kamarruangan_id : null;
            $modMasukKamar->kelaspelayanan_id = $modPasienAdmisiNew->kelaspelayanan_id;
            $modMasukKamar->ruangan_id= $modPasienAdmisiNew->ruangan_id;
            $modMasukKamar->pasienadmisi_id=$modPasienAdmisiNew->pasienadmisi_id;
            $modMasukKamar->pegawai_id=$model->pegawai_id;
            $modMasukKamar->penjamin_id=$model->penjamin_id;
            $modMasukKamar->shift_id=Yii::app()->user->getState('shift_id');
            $modMasukKamar->tglmasukkamar=date('Y-m-d H:i:s');
            $modMasukKamar->nomasukkamar=KeyGenerator::noMasukKamar($modMasukKamar->ruangan_id);
            $modMasukKamar->jammasukkamar=date('H:i:s');
            $modMasukKamar->tglkeluarkamar=null;
            $modMasukKamar->jamkeluarkamar=null;
            $modMasukKamar->lamadirawat_kamar=null;
            if($modMasukKamar->save()){
                $this->successSaveMasukKamar=true;
            }else{
                $this->successSaveMasukKamar=false;
            }
        }        

	public function actionRawatJalan($id = null)
	{
            $insert_notifikasi = new MyFunction();
            $this->pageTitle = Yii::app()->name." - Pendaftaran Rawat Jalan";
            
            $model = new PPPendaftaranRj;
//            if(!empty($_POST['pasien_id'])){
//                $model = PPPendaftaranRj::model()->findAllByAttributes(array('pasien_id'=>$pasien_id));
//                $model->ruangan_id = $model->ruangan_id;
////                echo $model->ruangan_id;exit;
//                $model->isNewRecord = TRUE;
//            }
            $modSep = New SepT;
            $modSep->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
            $modSep->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
            $modSep->tglsep = date('Y-m-d');
            $modSep->jenisrujukan_kode_bpjs = 1;
            $modSep->polieksekutif = 0;
            $modSep->cob_bpjs = 0;
            $modSep->cob_status = "TIDAK";
            $modSep->lakalantas_kode = 0;
            $modSep->jnspelayanan_kode = 2;//RJ/RD
            $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
            if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            }else{
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            }
            
            $modAntrian = New AntrianpasienT;
                    
            $model->kelaspelayanan_id = Params::kelasPelayanan('rawat_jalan');
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->tgl_konfirmasi = date('d M Y H:i:s');
            $model->umur = '00 Thn 00 Bln 00 Hr';
            $model->thn = '00';
            $model->bln = '00';
            $model->hr = '00';
            $format = new CustomFormat();
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
//           echo $_POST['pasien_id'];exit;
            if(!empty($id)){
                $model = PPPendaftaranRj::model()->findByPk($id);
                if(!empty($model->sep_id)){
                    $modSep = SepT::model()->findByPk($model->sep_id);
                }
                $modPasien = PPPasienM::model()->findByPk($model->pasien_id);
                $modPenanggungJawab = PPPenanggungJawabM::model()->findByPk($model->penanggungjawab_id);
                $modRujukan = PPRujukanT::model()->findByPk($model->rujukan_id);
                $modTindakanPelayan= PPTindakanPelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
                $modCaraBayar = CarabayarM::model()->findByPk($model->carabayar_id);
                $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$model->pasien_id),array('order'=>'pendaftaran_id desc'));
                $umur = explode(" ",$pendaftaran[0]->umur); 
                // echo $pendaftaran[0]->umur;exit;
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                // $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
                $model->noRekamMedik = $modPasien->no_rekam_medik;
                $model->ruangan_id = $model->ruangan_id;
                $model->pegawai_id = $model->pegawai_id;
            }else{
            
                if(!empty($_POST['pasien_id'])){//Jika pasien Lama   
                    if(isset($_POST['isPasienBaru'])){
                        $modPasien = PPPasienM::model()->findByPk($_POST['pasien_id']);
                        $modPasien->ispasienluar=FALSE;
                        $model->isPasienLama = false;
                        $model->statuspasien = Params::statusPasien('baru');
                        $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                        $modPasien->statusrekammedis = 'AKTIF';
                        $model->noRekamMedik = $_POST['noRekamMedikLama'];
                    }else{
                        $modPasien = PPPasienM::model()->findByPk($_POST['pasien_id']);
                        $modPasien->ispasienluar=FALSE;
                        $model->isPasienLama = true;
                        $model->statuspasien = Params::statusPasien('lama');
                        $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                        $model->noRekamMedik = $modPasien->no_rekam_medik;
                    }
                } else { //Pasien Baru
                    $modPasien = new PPPasienM;
                    $modPasien->tanggal_lahir = date('d M Y');
                    $modPasien->ispasienluar=FALSE;
                    $model->statuspasien = Params::statusPasien('baru');
                    $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                    $modPasien->statusrekammedis = 'AKTIF';
                    $model->noRekamMedik = $_POST['noRekamMedikLama'];
                    
               
                }
            }

            if(!empty($_POST['buatjanjipoli_id'])){
                $idPoli = $_POST['buatjanjipoli_id'];
                $modJanjiPoli = BuatjanjipoliT::model()->findByPk($idPoli);
                $modPasien = PPPasienM::model()->findByPk($modJanjiPoli->pasien_id);
                $model->ruangan_id = $modJanjiPoli->ruangan_id;
                $model->pegawai_id = $modJanjiPoli->pegawai_id;
                $model->noRekamMedik = $modPasien->no_rekam_medik; 
            }else{
                $modJanjiPoli = new BuatjanjipoliT;
                $modJanjiPoli->buatjanjipoli_id = null;
            }
            
            
            $modPenanggungJawab = new PPPenanggungJawabM;
            $modRujukan = new PPRujukanT;
            $modTindakanPelayan= New PPTindakanPelayananT;
            
            if(isset($_POST['PPPendaftaranRj']))
            {
                
                $model->attributes = $_POST['PPPendaftaranRj'];
                if(isset($_POST['PPPasienM']))
                {
                    $modPasien->attributes = $_POST['PPPasienM'];
                }

                if(isset($_POST['PPPenanggungJawabM']))
                {
                    $modPenanggungJawab->attributes = $_POST['PPPenanggungJawabM'];
                }

                if (isset($_POST['PPRujukanT']))
                    $modRujukan->attributes = $_POST['PPRujukanT'];

                if(empty($model->status_konfirmasi)){
                    $model->status_konfirmasi = "BELUM DIKONFIRMASI";
                }else{
                    $model->status_konfirmasi = "SUDAH DIKONFIRMASI";
                }
                $transaction = Yii::app()->db->beginTransaction();
                try 
                {
                    if(isset($_POST['PPPasienM'])){
                        if($_POST['PPPasienM']['namadepan'] == 'Tn.')
                        {
                            if($_POST['PPPasienM']['jeniskelamin'] != 'LAKI-LAKI')
                            {
                                 $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                                 Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                            }
                        }
                        else
                        {
                            if($_POST['PPPasienM']['namadepan'] == 'Ny.' || $_POST['PPPasienM']['namadepan'] == 'Nn')
                            {
                                if($_POST['PPPasienM']['jeniskelamin'] != 'PEREMPUAN')
                                {
                                     $modPasien->addError('jeniskelamin', 'pilih jenis kelamin yang sesuai');
                                     Yii::app()->user->setFlash('danger',"Inputan jenis kelamin kurang tepat, tolong di betulkan");
                                }
                            }
                        }

                        if($_POST['PPPasienM']['statusperkawinan'] != null){
                            if($_POST['PPPasienM']['statusperkawinan'] == 'BELUM KAWIN')
                            {
                                if($_POST['PPPasienM']['namadepan'] == 'BY. Ny.')
                                {
                                    $modPasien->addError('statusperkawinan', 'pilih status perkawinan yang sesuai');
                                    Yii::app()->user->setFlash('danger',"Inputan status perkawinan kurang tepat, tolong di betulkan");
                                }
                            }                        
                        }

                        if($_POST['PPPasienM']['pekerjaan_id'] != null){
                            if($_POST['PPPasienM']['pekerjaan_id'] == '12')
                            {
                                if($_POST['PPPasienM']['namadepan'] != 'BY. Ny.')
                                {
                                    $modPasien->addError('pendidikan_id', 'pilih pekerjaan yang sesuai');
                                    Yii::app()->user->setFlash('danger',"Inputan pekerjaan kurang tepat, tolong di betulkan");
                                }
                            }                        
                        }                        
                    }

                    if(!isset($_POST['isPasienLama']))
                    {
                        if(isset($_POST['isPasienBaru']))
                        {
                            if(!empty($_POST['noRekamMedikLama'])) {
                                $modPasien->statusrekammedis = "AKTIF";
                                $model->noRekamMedik = $_POST['noRekamMedikLama'];
                                $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                                if(!empty($modPasien) && isset($_POST['isPasienBaru']))
                                {
                                    $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                                }
                                }else{
                                     $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                                     Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                                }
                        }else{
                            $modPasien = $this->savePasien($_POST['PPPasienM']);
                            $model->statuspasien = Params::statusPasien('baru');
                        }
                    }else{
                        $model->isPasienLama = true;
                        $model->statuspasien = Params::statusPasien('lama');
                        if(!empty($_POST['noRekamMedik']))
                        {
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = PPPasienM::model()->findByAttributes(
                                array('no_rekam_medik'=>$model->noRekamMedik)
                            );
                            if(!empty($modPasien))
                            {
                                $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }

                    if(isset($_POST['isRujukan'])) {
                        $model->isRujukan = true;
                        $model->statusmasuk = 'RUJUKAN';
                        $modRujukan = $this->saveRujukan($_POST['PPRujukanT']);
                    } else {
                        $model->statusmasuk = 'NON RUJUKAN';
                    }

                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['PPPenanggungJawabM']);
                    }
                    
                    if(count($modPasien)>0){
                        $model = $this->savePendaftaranRJ($model,$modPasien,$modRujukan,$modPenanggungJawab, $_POST['PPPendaftaranRj']);
                        
                        if(isset($_POST['SepT'])){
                            $modSep = $this->saveSep($modSep,$modPasien,$model,$_POST['SepT']);
                        }
                        
                        if(isset($_POST['AntrianpasienT'])){
                            $modAntrian = $this->updateAtrianPendaftaran($model,$modAntrian,$_POST['AntrianpasienT']);
                        }
                        
                    } else {
                        $this->successSave = false;
                        $modPasien = new PPPasienM;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('danger',"Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar");
                    }
                    
                    $this->saveUbahCaraBayar($model);

                    if(isset($_POST['karcisTindakan']) && isset($_POST['TindakanPelayananT']) && !empty($_POST['TindakanPelayananT']['idTindakan']))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model,null,$_POST['RekeningakuntansiV']);
                    }

                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = 2;
                    $params['modul_id'] = 5;
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $model->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $params['create_ruangan'] = $model->ruangan_id;
                    $params['judulnotifikasi'] = ($model->isPasienLama == true ? 'Pendaftaran Pasien Lama' : 'Pendaftaran Pasien Baru' );
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);

                    if($this->successSave && $this->successSaveRujukan && $this->successSavePJ && $this->successSaveTindakanKomponen) {
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $model->isNewRecord = FALSE;
                        // $this->redirect(array('RawatJalan','id'=>$model->pendaftaran_id));
                        $this->redirect(array('rawatjalan','id'=>$model->pendaftaran_id,'sep_id'=>$modSep->sep_id));
                    } else {
                        $transaction->rollback();
                        $model->isNewRecord = TRUE;
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }

                }catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            $modCaraBayar = CarabayarM::model()->findAll('carabayar_loket IS  NOT NULL ORDER BY carabayar_nama');
            
            $this->render('rawatJalan',array('model'=>$model,
                                             'modPasien'=>$modPasien,
                                             'modPenanggungJawab'=>$modPenanggungJawab,
                                             'modRujukan'=>$modRujukan,
                                             'modCaraBayar'=>$modCaraBayar,
                                             'format'=>$format,
                                             'modTindakanPelayan'=>$modTindakanPelayan,
                                             'modJanjiPoli'=>$modJanjiPoli,
                                             'modRekenings'=>$modRekenings,
                                             'modSep'=>$modSep,
                                             'modAntrian'=>$modAntrian,
                                            ));
	}
        
        public function saveTindakanPelayanan($modPasien,$model,$kelas=null,$postRekenings=array())
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New PPTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$model->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
             
//            perubahan ini dilakukan guna menyesuaikan ketika ada perubahan di pendaftaran rawat inap
//            edit : jang wahyu
            if($kelas==null){
                $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
                $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            } else {
                $modTindakanPelayan->kelaspelayanan_id=$kelas;
                $modTindakanPelayan->tarif_satuan=$_POST['tarifKarcis'];
            }
            $modTindakanPelayan->instalasi_id=Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id=$model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id=$_POST['TindakanPelayananT']['idTindakan'];
            $modTindakanPelayan->carabayar_id=$model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
//            $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            // $modTindakanPelayan->ruangan_id = $model->ruangan_id;
            $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if(!empty($_POST['TindakanPelayananT']['idKarcis'])){
                $modTindakanPelayan->karcis_id=$_POST['TindakanPelayananT']['idKarcis'];
                $modTindakanPelayan->tipepaket_id = $this->tipePaketKarcis($model, $_POST['TindakanPelayananT']['idKarcis'], $_POST['TindakanPelayananT']['idTindakan']);
            }

                $tarifTindakan= PPTariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                foreach($tarifTindakan AS $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
             if($modTindakanPelayan->save()){
                    $tindakanKomponen= PPTariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                    $jumlahKomponen=COUNT($tindakanKomponen);

                    foreach ($tindakanKomponen AS $tampilKomponen):
                            $modTindakanKomponen=new PPTindakanKomponenT;
                            $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                            $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                            $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                            $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                            $modTindakanKomponen->tarifcyto_tindakankomp=0;
                            $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                            $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                            $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                            $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                            if($modTindakanKomponen->save()){
                                $cekTindakanKomponen++;
                            }
                    endforeach;
                    if($cekTindakanKomponen!=$jumlahKomponen){
                           $this->successSaveTindakanKomponen=false;
                        }
                    //simpan jurnal rekening
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                    }
            } 
            else{
                throw new Exception("Karcis Tindakan gagal disimpan");
            }
        }
        
        public function tipePaketKarcis($modPendaftaran,$idKarcis,$idTindakan)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $idTindakan);
            $criteria->compare('tipepaket.carabayar_id', $modPendaftaran->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $modPendaftaran->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $modPendaftaran->kelaspelayanan_id);
            $paket = PaketpelayananM::model()->find($criteria);
            $result = Params::TIPEPAKET_NONPAKET;
            if(isset($paket)) $result = $paket->tipepaket_id;
            
            return $result;
        }
        
        public function savePendaftaranRJ($model,$modPasien,$modRujukan,$modPenanggungJawab)
        {
            $format = new CustomFormat();
            $id = $modPasien->pasien_id;
            $modPasienPulang = PasienpulangT::model()->findByAttributes(array('pasien_id'=>$id));
            $modAd = PasienadmisiT::model()->findByAttributes(array('pasien_id'=>$id));


            $modelNew = new PPPendaftaranRj;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->no_pendaftaran = KeyGenerator::noPendaftaran(Params::singkatanNoPendaftaranRJ());
            $modelNew->instalasi_id = KeyGenerator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_urutantri = KeyGenerator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = KeyGenerator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
            // $modelNew->umur = $_POST['PPPendaftaranRj']['thn']." Thn ".$_POST['PPPendaftaranRj']['bln']." Bln ".$_POST['PPPendaftaranRj']['hr']." Hr ";
            //$modelNew->statuspasien = KeyGenerator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = KeyGenerator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            $modelNew->tgl_pendaftaran = $format->formatDateTimeMediumForDB($_POST['PPPendaftaranRj']['tgl_pendaftaran']);
            
            if($modelNew->validate()) {
                // echo"<pre>"; print_r($modPasienPulang->attributes); exit();
                // if( !empty($modAd) || empty($modPasienPulang) ) {
                //     echo "<script>
                //         alert('Pasien tidak bisa melakukan pendaftaran karena pasien belum pulang');
                //         window.top.location.href='".Yii::app()->createUrl('pendaftaranPenjadwalan/Pendaftaran/RawatJalan')."';
                //     </script>";
                // }else{

                    $modelNew->save();
                    $this->successSave = true;
                // }
                // form inputs are valid, do something here
               

            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'),'medium',null);
                $modelNew->tgl_konfirmasi = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_konfirmasi, 'yyyy-MM-dd'),'medium',null);
            }
            
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
                    
            return $modelNew;
        }
               
	public function actionRawatDarurat($id = null)
	{
            $insert_notifikasi = new MyFunction();
            $this->pageTitle = Yii::app()->name." - Pendaftaran Rawat Darurat";
            $format = new CustomFormat();
            $model=new PPPendaftaranRd;
            $model->kelaspelayanan_id = Params::kelasPelayanan('rawat_darurat');
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->tgl_konfirmasi = date('d M Y H:i:s');
            $model->umur = '00 Thn 00 Bln 00 Hr';
            $model->thn = '00';
            $model->bln = '00';
            $model->hr = '00';
            $modPasien = new PPPasienM;
            $modPasien->tanggal_lahir = date('d M Y'); //diset null
            $modPenanggungJawab = new PPPenanggungJawabM;
            $modRujukan = new PPRujukanT;
            $modKecelakaan = new PPPasienkecelakaanT;
            $modKecelakaan->tglkecelakaan = date('d M Y H:i:s');
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            $modSep = New SepT;
            $modSep->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
            $modSep->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
            $modSep->tglsep = date('Y-m-d');
            $modSep->jenisrujukan_kode_bpjs = 1;
            $modSep->polieksekutif = 0;
            $modSep->cob_bpjs = 0;
            $modSep->cob_status = "TIDAK";
            $modSep->lakalantas_kode = 0;
            $modSep->jnspelayanan_kode = 2;//RJ/RD
            $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
            if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
            }else{
                $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
            }
            
//            echo $_POST['pasien_id'];exit;
            if(!empty($_POST['pasien_id'])){//Jika pasien Lama   
                if(isset($_POST['isPasienBaru'])){
                    $modPasien = PPPasienM::model()->findByPk($_POST['pasien_id']);
                    $modPasien->ispasienluar=FALSE;
                    $model->isPasienLama = false;
                    $model->statuspasien = Params::statusPasien('baru');
                    $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                    $modPasien->statusrekammedis = 'AKTIF';
                    $model->noRekamMedik = $_POST['noRekamMedikLama'];
                    
                }else{
                    $modPasien = PPPasienM::model()->findByPk($_POST['pasien_id']);
                    $modPasien->ispasienluar=FALSE;
                    $model->isPasienLama = true;
                    $model->statuspasien = Params::statusPasien('lama');
                    $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                    $model->noRekamMedik = $modPasien->no_rekam_medik;
                }
            } else { //Pasien Baru
                $modPasien = new PPPasienM;
                $modPasien->tanggal_lahir = date('d M Y');
                $modPasien->ispasienluar=FALSE;
                $model->statuspasien = Params::statusPasien('baru');
                $model->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
                $modPasien->statusrekammedis = 'AKTIF';
                $model->noRekamMedik = $_POST['noRekamMedikLama'];


            }
            
             if(!empty($id)){
                $model = PPPendaftaranRd::model()->findByPk($id);
                if(!empty($model->sep_id)){
                    $modSep = SepT::model()->findByPk($model->sep_id);
                }
                $modPasien = PPPasienM::model()->findByPk($model->pasien_id);
                if(!empty($model->penanggungjawab_id)){
                    $modPenanggungJawab = PPPenanggungJawabM::model()->findByPk($model->penanggungjawab_id);
                }
                $pendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$model->pasien_id),array('order'=>'pendaftaran_id desc'));
                $umur = explode(" ",$pendaftaran[0]->umur); 
                // echo $pendaftaran[0]->umur;exit;
                $thn = $umur[0];
                $bln = $umur[2];
                $hr = $umur[4];
                
                // $model->umur = '00 Thn 00 Bln 00 Hr';
                $model->thn = $thn;
                $model->bln = $bln;
                $model->hr = $hr;
                if(!empty($model->rujukan_id)){
                    $modRujukan = PPRujukanT::model()->findByPk($model->rujukan_id);
                }

//                $modKecelakaan = PPPasienkecelakaanT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
                
                $model->noRekamMedik = $modPasien->no_rekam_medik;
                $model->ruangan_id = $model->ruangan_id;
            }
            
            if(isset($_POST['PPPendaftaranRd']))
            {
                $model->attributes = $_POST['PPPendaftaranRd'];
                $modPasien->attributes = $_POST['PPPasienM'];
                $modPasien->tanggal_lahir = (!empty($_POST['PPPasienM']['tanggal_lahir'])) ? $format->formatDateMediumForDB($_POST['PPPasienM']['tanggal_lahir']) : null;
                $modPasien->kabupaten_id = ($_POST['PPPasienM']['kabupaten_id']);
                $modPasien->kelurahan_id = ($_POST['PPPasienM']['kelurahan_id']);
                $modPasien->jenisidentitas = (!empty($_POST['PPPasienM']['jenisidentitas'])) ? $_POST['PPPasienM']['jenisidentitas'] : "-";
                $modPenanggungJawab->attributes = $_POST['PPPenanggungJawabM'];
                $modRujukan->attributes = $_POST['PPRujukanT'];
                $modKecelakaan->attributes = $_POST['PPPasienkecelakaanT'];
                $model->tgl_pendaftaran = $format->formatDateTimeMediumForDB($model->tgl_pendaftaran);
                $model->tgl_konfirmasi = $format->formatDateTimeMediumForDB($model->tgl_konfirmasi);
                if(empty($model->status_konfirmasi)){
                    $model->status_konfirmasi = "BELUM DIKONFIRMASI";
                }else{
                    $model->status_konfirmasi = "SUDAH DIKONFIRMASI";
                }
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if(!isset($_POST['isPasienLama'])) {
                         if(isset($_POST['isPasienBaru'])){
                            if(!empty($_POST['noRekamMedikLama'])) {
                                $model->statuspasien = Params::statusPasien('baru');
                                $modPasien->statusrekammedis = "AKTIF";
                                $model->noRekamMedik = $_POST['noRekamMedikLama'];
                                $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                                if(!empty($modPasien) && isset($_POST['isPasienBaru'])) {
                                    $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                                }
                                }else{
                                     $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                                     Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                                }
                        }else{
                            $modPasien = $this->savePasien($_POST['PPPasienM']);
                            $model->statuspasien = Params::statusPasien('baru');
                        }
                    } else {
                        $model->isPasienLama = true;
                        $model->statuspasien = Params::statusPasien('lama');
                        if(!empty($_POST['noRekamMedik'])) {
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien)) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    if(isset($_POST['isRujukan'])) {
                        $model->isRujukan = true;
                        $model->statusmasuk = 'RUJUKAN';
                        $modRujukan = $this->saveRujukan($_POST['PPRujukanT']);
                    } else {
                        $model->statusmasuk = 'NON RUJUKAN';
                    }
                    
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['PPPenanggungJawabM']);
                    }
                    
                    if(count($modPasien)>0) {
                        $model = $this->savePendaftaranRD($model,$modPasien,$modRujukan,$modPenanggungJawab);
                        if(!empty($_POST['isKecelakaan']))
                            $modKecelakaan = $this->saveKecelakaan($model, $modKecelakaan->attributes);
                        if(isset($_POST['SepT'])){
                            $modSep = $this->saveSep($modSep,$modPasien,$model,$_POST['SepT']);
                        }
                    } else {
                        $this->successSave = false;
                        $modPasien = new PPPasienM;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('danger',"Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar!");
                    }
                    $this->saveUbahCaraBayar($model);
                    
                    if(isset($_POST['karcisTindakan']) && isset($_POST['TindakanPelayananT']) && !empty($_POST['TindakanPelayananT']['idTindakan']))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model,null,$_POST['RekeningakuntansiV']);
                    }
                    
                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = 3;
                    $params['modul_id'] = 6;
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $model->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $params['create_ruangan'] = $model->ruangan_id;
                    $params['judulnotifikasi'] = ($model->isPasienLama == true ? 'Pendaftaran Pasien Lama' : 'Pendaftaran Pasien Baru' );
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);                    
                                            
                    if($this->successSave && $this->successSaveRujukan && $this->successSavePJ && $this->successSaveTindakanKomponen && $this->successSaveKecelakaan) {
                        $transaction->commit();
                         $model->isNewRecord = FALSE;
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect(array('RawatDarurat','id'=>$model->pendaftaran_id));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
//                        $model->isNewRecord = TRUE;
                    }
                    
                } catch (Exception $exc) {
                    $transaction->rollback();
//                    $model->isNewRecord = TRUE;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = true;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            $model->isKecelakaan = (isset($_POST['isKecelakaan'])) ? true : false;
            
            $this->render($this->pathView.'rawatDarurat',array('model'=>$model,
                                               'modPasien'=>$modPasien,
                                               'modPenanggungJawab'=>$modPenanggungJawab,
                                               'modRujukan'=>$modRujukan,
                                               'modKecelakaan'=>$modKecelakaan,
                                               'modRekenings'=>$modRekenings,
                                               'modSep'=>$modSep,));
	}
        
        public function savePendaftaranRD($model,$modPasien,$modRujukan,$modPenanggungJawab)
        {
            $modelNew = new PPPendaftaranRd;
            $id = $modPasien->pasien_id;
            $modPasienPulang = PasienpulangT::model()->findByAttributes(array('pasien_id'=>$id));
            $modAd = PasienadmisiT::model()->findByAttributes(array('pasien_id'=>$id));

            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->no_pendaftaran = KeyGenerator::noPendaftaran(Params::singkatanNoPendaftaranRD());
            $modelNew->instalasi_id = KeyGenerator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_urutantri = KeyGenerator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = KeyGenerator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
            //$modelNew->statuspasien = KeyGenerator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = KeyGenerator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            
            if($modelNew->validate()) {
                // form inputs are valid, do something here
                // echo"<pre>"; print_r($modAd->attributes); exit();
                // if( !empty($modAd) || empty($modPasienPulang) ) {
                //     echo "<script>
                //         alert('Pasien tidak bisa melakukan pendaftaran karena pasien belum pulang');
                //         window.top.location.href='".Yii::app()->createUrl('pendaftaranPenjadwalan/Pendaftaran/RawatDarurat')."';
                //     </script>";
                // }else{
                    $modelNew->save();
                    $this->successSave = true;
                // }
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'),'medium',null);
                $modelNew->tgl_konfirmasi = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_konfirmasi, 'yyyy-MM-dd'),'medium',null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
                    
            return $modelNew;
        }
        
        public function savePasien($attrPasien)
        { 
            $modPasien = new PPPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            if(isset($_POST['PPPasienM']['tempPhoto'])){
                $modPasien->photopasien = $_POST['PPPasienM']['tempPhoto'];
            }

            if(isset($_POST['isPasienBaru'])){
                $modPasien->no_rekam_medik = $_POST['noRekamMedikLama'];
            }else{
                $modPasien->no_rekam_medik = KeyGenerator::noRekamMedik();
            }
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = Params::statusRM(1);
            $modPasien->ispasienluar = FALSE;
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if(strlen(trim($_POST['PPPasienM']['kelurahan_id'])) == 0)
            {
                $modPasien->kelurahan_id = null;
            }
            
            if($modPasien->validate())
            {
                // form inputs are valid, do something here
                $modPasien->save();
                if(isset($_POST['PPPasienM']['nik_pegawai'])){
                    $nik = $_POST['PPPasienM']['nik_pegawai'];
                    $modPegawai = PegawaiM::model()->findByAttributes(array('nomorindukpegawai'=>$nik));
                    $pegawai_id = $modPegawai->pegawai_id;
                    if(count($modPegawai) > 0){
                     PegawaiM::model()->updateByPk($pegawai_id,array('pasien_id'=>$modPasien->pasien_id));   
                    }                    
                }
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien)
        {
            if(isset($_POST['PPPasienM']['tempPhoto']))
            {
                $modPasien->photopasien = $_POST['PPPasienM']['tempPhoto'];
            }
            
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            
            if(isset($_POST['isPasienBaru']))
            {
                $modPasienupdate->attributes = $_POST['PPPasienM'];
                $modPasienupdate->no_rekam_medik = $_POST['noRekamMedikLama'];
                $modPasienupdate->statusrekammedis = "AKTIF";
            }
            $janjiPoli = substr($modPasienupdate->no_rekam_medik,0,2);
            if($janjiPoli=="JP"){
                $modPasienupdate->no_rekam_medik = KeyGenerator::noRekamMedik();
            }
            
            $modPasienupdate->kelompokumur_id = KeyGenerator::kelompokUmur($modPasienupdate->tanggal_lahir);
            $modPasienupdate->statusrekammedis = "AKTIF";
            $modPasienupdate->kelurahan_id = $_POST['PPPasienM']['kelurahan_id'];

            if(strlen(trim($_POST['PPPasienM']['kelurahan_id'])) == 0)
            {
                $modPasienupdate->kelurahan_id = null;
            }

            if($modPasienupdate->validate())
            {
                // form inputs are valid, do something here
                $modPasienupdate->save();
                if(isset($_POST['PPPasienM']['nik_pegawai'])){
                    $nik = $_POST['PPPasienM']['nik_pegawai'];
                    $modPegawai = PegawaiM::model()->findByAttributes(array('nomorindukpegawai'=>$nik));
                    $pegawai_id = $modPegawai->pegawai_id;
                    if(count($modPegawai) > 0){
                        PegawaiM::model()->updateByPk($pegawai_id,array('pasien_id'=>$modPasienupdate->pasien_id));
                    }
                }
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
//                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
//                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);zzzzzzzzzzzzzzzzzzz
            }
            return $modPasienupdate;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $format = new CustomFormat();
            $modPenanggungJawab = new PPPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            $modPenanggungJawab->tgllahir_pj = $format->formatDateMediumForDB($modPenanggungJawab->tgllahir_pj);
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                    $this->successSavePJ = TRUE;
            } else {
                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                //if($modPenanggungJawab->tgllahir_pj != null)
                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
                $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
        protected function saveKecelakaan($modPendaftaran,$attrKecelakaan)
        {
            $modKecelakaan = new PPPasienkecelakaanT;
            $format = new CustomFormat;
            $modKecelakaan->attributes = $attrKecelakaan;
            $modKecelakaan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modKecelakaan->tglkecelakaan = $format->formatDateTimeMediumForDB($modKecelakaan->tglkecelakaan);
            //$modKecelakaan->tglkecelakaan = ($attrKecelakaan['tglkecelakaan'] == '') ? date('Y-m-d H:i:s') : $attrKecelakaan['tglkecelakaan'] ;
            if($modKecelakaan->validate()) {
                // form inputs are valid, do something here
                $modKecelakaan->save();
                $this->successSaveKecelakaan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modKecelakaan->tglkecelakaan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modKecelakaan->tglkecelakaan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveKecelakaan = FALSE;
            }
            
            return $modKecelakaan;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new PPRujukanT;
            $format = new CustomFormat();
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = (empty($attrRujukan['tanggal_rujukan'])) ? null : $format->formatDateTimeMediumForDB($attrRujukan['tanggal_rujukan']);
            $modRujukan->asalrujukan_id = ($attrRujukan['asalrujukan_id'] == '') ? null: $attrRujukan['asalrujukan_id'] ;
            $modRujukan->rujukandari_id = ($attrRujukan['rujukandari_id'] == '') ? null: $attrRujukan['rujukandari_id'] ;
            if(!empty($modRujukan->rujukandari_id)){
                $modCariRujukanDari = RujukandariM::model()->findByPk($modRujukan->rujukandari_id);
                $modRujukan->nama_perujuk = $modCariRujukanDari->namaperujuk;
            }
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function saveUbahCaraBayar($model) 
        {
            $modUbahCaraBayar = new PPUbahCaraBayarR;
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
            
        }
        
        public function saveKarcis()
        {
            
        }
        
        
        /**
         * Fungsi untuk menyimpan data ke model PPPendaftaranMP
         * @param type $model array object 
         * @param type $modPasien
         * @param type $modRujukan
         * @param type $modPenanggungJawab
         * @return PPPendaftaranMp 
         */
        public function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new PPPendaftaranMp;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            if (isset($modelNew->ruangan_id)){
                        if($modelNew->ruangan_id == Params::RUANGAN_ID_LAB){
                            $inisial_ruangan = Yii::app()->user->getState('nopendaftaran_lab');
                        }
                        else if($modelNew->ruangan_id == Params::RUANGAN_ID_RAD){
                            $inisial_ruangan = Yii::app()->user->getState('nopendaftaran_rad');
                        }
                        else if($modelNew->ruangan_id == Params::RUANGAN_ID_REHAB){
                            $inisial_ruangan = Yii::app()->user->getState('nopendaftaran_rehabmedis');
                        }
            }
            $modelNew->instalasi_id = KeyGenerator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_pendaftaran = KeyGenerator::noPendaftaran($inisial_ruangan);
            $modelNew->no_urutantri = KeyGenerator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = KeyGenerator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = KeyGenerator::hitungUmur($modPasien->tanggal_lahir);
            //$modelNew->statuspasien = KeyGenerator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = KeyGenerator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
            
            if ($modelNew->validate()){
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        /**
         * Fungsi action untuk proses pendaftaran penunjang
         */
        public function actionMasukPenunjang()
        {
            $insert_notifikasi = new MyFunction();
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Masuk Penunjang";
            $model=new PPPendaftaranMp;
            $model->kelaspelayanan_id = Params::kelasPelayanan('masuk_penunjang');
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->tgl_konfirmasi = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $format = new CustomFormat();
            $modPasien = new PPPasienM;
            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new PPPenanggungJawabM;
            $modRujukan = new PPRujukanT;
            $modPasienPenunjang = new PPPasienMasukPenunjangT;
            $modPengambilanSample = new PPPengambilanSampleT;
            $modPengambilanSample->no_pengambilansample = KeyGenerator::noPengambilanSample();
            
            
            if (isset($_POST['PPPendaftaranMp'])){
                $model->attributes = $_POST['PPPendaftaranMp'];
                $modPasien->attributes = $_POST['PPPasienM'];
                $modPenanggungJawab->attributes = $_POST['PPPenanggungJawabM'];
                $modRujukan->attributes = $_POST['PPRujukanT'];
                $modRujukan->tanggal_rujukan = $format->formatDateMediumForDB($_POST['PPRujukanT']['tanggal_rujukan'])." 00:00:00";
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    if(!isset($_POST['isPasienLama'])){
                        if(isset($_POST['isPasienBaru'])){
                            if(!empty($_POST['noRekamMedikLama'])) {
                                $model->statuspasien = Params::statusPasien('baru');
                                $modPasien->statusrekammedis = "AKTIF";
                                $model->noRekamMedik = $_POST['noRekamMedikLama'];
                                $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                                if(!empty($modPasien) && isset($_POST['isPasienBaru'])) {
                                    $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                                }
                                }else{
                                     $modPasien->addError('noRekamMedik', 'no rekam medik belum dipilih');
                                     Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                                }
                        }else{
                            $modPasien = $this->savePasien($_POST['PPPasienM']);
                            $model->statuspasien = Params::statusPasien('baru');
                        }
                    }
                    else{
                        $model->isPasienLama = true;
                        $model->statuspasien = Params::statusPasien('lama');
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = PPPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['PPPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                                       
                    $modRujukan = $this->saveRujukan($_POST['PPRujukanT']);
                    
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['PPPenanggungJawabM']);
                    }
                    
                    if (count($modPasien) != 1){
                        $modPasien = New PPPasienM;
                        $this->successSave = false;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                    }
                    
                    $model = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    
                    $modPasienPenunjang = $this->savePasienPenunjang($model,$modPasien);
                    
                    if(isset($_POST['karcisTindakan']) && isset($_POST['TindakanPelayananT']) && !empty($_POST['TindakanPelayananT']['idTindakan']))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model,null,$_POST['RekeningakuntansiV']);
                    }
                    
                    if(isset($_POST['pakeSample'])){
                        $model->pakeSample = true;
                        $modPengambilanSample = $this->savePengambilanSample($_POST['PPPengambilanSampleT'],$modPasienPenunjang);
                    }
                    
                    $this->saveUbahCaraBayar($model);
                    
                    
                    $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                    $params['create_time'] = date( 'Y-m-d H:i:s');
                    $params['create_loginpemakai_id'] = Yii::app()->user->id;
                    $params['instalasi_id'] = ($model->ruangan_id == 18 ? 5 : 6);
                    $params['modul_id'] = ($model->ruangan_id == 18 ? 8 : 9);
                    $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $model->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $params['create_ruangan'] = $model->ruangan_id;
                    $params['judulnotifikasi'] = ($model->isPasienLama == true ? 'Pendaftaran Pasien Lama' : 'Pendaftaran Pasien Baru' );
                    $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                    
                    if ($this->successSave && $modRujukan->save() && $this->successSaveTindakanKomponen){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }
                    else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            $model->pakeSample = (isset($_POST['pakeSample'])) ? true : false;
                       
            $this->render('masukPenunjang', array(
                'model'=>$model, 
                'format'=>$format,
                'modPasien'=>$modPasien, 
                'modPenanggungJawab'=>$modPenanggungJawab,
                'modRujukan'=>$modRujukan,
                'modPengambilanSample'=>$modPengambilanSample,
                'modPasienPenunjang'=>$modPasienPenunjang
            ));
        }
        
        /**
         * Fungsi untuk menyimpan data ke model PPPasienMasukPenunjangT
         * @param type $attrPendaftaran
         * @param type $attrPasien
         * @return PPPasienMasukPenunjangT 
         */
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            
            $modPasienPenunjang = new PPPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $attrPendaftaran->ruangan_id;
            // switch ($modPasienPenunjang->ruangan_id) {
            //     case Params::RUANGAN_ID_LAB:$inisial_ruangan = Yii::app()->user->getState('nopendaftaran_lab');break;
            //     case Params::RUANGAN_ID_RAD:$inisial_ruangan = Yii::app()->user->getState('nopendaftaran_rad');break;
            //     case Params::RUANGAN_ID_REHAB:$inisial_ruangan = Yii::app()->user->getState('nopendaftaran_rehabmedis');break;
            //     default:break;
            // }
            switch ($modPasienPenunjang->ruangan_id) {
                case Params::RUANGAN_ID_LAB:$inisial_ruangan = 'LK';break;
                case Params::RUANGAN_ID_RAD:$inisial_ruangan = 'RO';break;
                case Params::RUANGAN_ID_REHAB:$inisial_ruangan = Yii::app()->user->getState('nopendaftaran_rehabmedis');break;
                default:break;
            } 

            // echo $inisial_ruangan;
            // exit();           

            $modPasienPenunjang->no_masukpenunjang = KeyGenerator::noMasukPenunjang($inisial_ruangan);
            $modPasienPenunjang->tglmasukpenunjang = $attrPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  KeyGenerator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        /**
         * Fungsi untuk menyimpan data ke model PPPengambilanSampleT
         * @param type $attrSample array
         * @param type $modPasienPenunjang array object
         * @return PPPengambilanSampleT 
         */
        public function savePengambilanSample($attrSample,$modPasienPenunjang){
            $modPengambilanSample = new PPPengambilanSampleT;
            $modPengambilanSample->tglpengambilansample = $modPasienPenunjang->tglmasukpenunjang;
            $modPengambilanSample->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
            $modPengambilanSample->attributes = $attrSample;
            if ($modPengambilanSample->validate()){
                $modPengambilanSample->Save();
                $this->successSave = true;
            }else{
                $this->successSave = false;
                $modPengambilanSample->tglpengambilansample = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPengambilanSample->tglpengambilansample, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPengambilanSample;
        }
       
        public function actionSaveKartuPasien($pendaftaran_id){
            $model = PPPendaftaranRj::model()->findByPk($pendaftaran_id);
            $modelKartuPasienVer = PPInformasiprintkartupasienR::model()->findByAttributes(array('pasien_id'=>$model->pasien_id));
            if(!isset($modelKartuPasienVer))
            {
                $modelKartuPasien = new PPInformasiprintkartupasienR;
                $modelKartuPasien->pasien_id = $model->pasien_id;
                $modelKartuPasien->pendaftaran_id = $model->pendaftaran_id;
                $modelKartuPasien->tglprintkartu = date( 'Y-m-d H:i:s');
                $modelKartuPasien->statusprintkartu = true;
                $modelKartuPasien->create_time = date( 'Y-m-d H:i:s');
                $modelKartuPasien->create_loginpemakai_id = Yii::app()->user->id;
                if ($modelKartuPasien->validate()){
                    $modelKartuPasien->Save();
                }
            }
            
            return $modelKartuPasien;
        }
        
        public function saveSep($modSep, $modPasien, $modPendaftaran, $postSep) {
            $ruangan = RuanganM::model()->findByPk($modPendaftaran->ruangan_id);
            $modSep->attributes = $postSep;
            $modSep->pasien_id = $modPasien->pasien_id;
            $modSep->notelpon_peserta = $modPasien->no_mobile_pasien;
            $modSep->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            if($modPendaftaran->instalasi_id == 4){
                $modSep->jnspelayanan_kode = 1;
                $modSep->jnspelayanan_nama = "Rawat Inap";
            }else{
                $modSep->jnspelayanan_kode = 2;
                $modSep->jnspelayanan_nama = "Rawat Jalan";
                $modSep->politujuan_kode = $ruangan->kode_ruanganpoli;
                $modSep->politujuan_nama = $ruangan->ruangan_nama;
            }
            $modSep->hakkelas_kode = $postSep['hakkelas_kode'];
            $modSep->kelasrawat_kode = $postSep['kelasrawat_kode'];
            $modSep->kelasrawat_nama = $modSep->hakkelas_kode;
            $modSep->hakkelas_nama = $modSep->hakkelas_kode;
            $modSep->lakalantas_nama = $modSep->lakalantas_kode;
            $modSep->jenisrujukan_kode_bpjs = $postSep['jenisrujukan_kode_bpjs'];
            $modSep->jenisrujukan_nama_bpjs = ($postSep['jenisrujukan_kode_bpjs']==1)? "PCare" : "Rumah Sakit";
            $modSep->ppkrujukanasal_kode = $postSep['ppkrujukanasal_kode'];
            $modSep->jenispeserta_bpjs_kode = $postSep['jenispeserta_bpjs_kode'];
            $modSep->jenispeserta_bpjs_nama = $postSep['jenispeserta_bpjs_nama'];
            $modSep->tglrujukan_bpjs = date('Y-m-d H:i:s', strtotime($postSep['tglrujukan_bpjs']));

            $modSep->create_time = date('Y-m-d H:i:s');
            $modSep->create_loginpemakai_id = Yii::app()->user->id;
            $modSep->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if ($modSep->save()) {
                $modPendaftaran->sep_id = $modSep->sep_id;
                $modPendaftaran->no_asuransi = $modSep->nopeserta_bpjs;
                $modPendaftaran->namapemilik_asuransi = $modSep->namapeserta_bpjs;
                $modPendaftaran->kelastanggungan_id = $modSep->hakkelas_kode;
                $modPendaftaran->update();
            }
            
            return $modSep;
        }
        
        public function updateAtrianPendaftaran($model,$modAntrian,$postAntrian){
            if(!empty($postAntrian['antrianpasien_id_temp'])){
                $modAntrian = AntrianpasienT::model()->findByPk($postAntrian['antrianpasien_id_temp']);
                $modAntrian->pendaftaran_id = $model->pendaftaran_id;
                $modAntrian->pasien_id = $model->pasien_id;
                $modAntrian->is_dilayani = TRUE;
                $modAntrian->update();
            }
            
            return $modAntrian;
        }
}
