<?php
Yii::import('pendaftaranPenjadwalan.controllers.PendaftaranController');//untuk load simpan sep
class SettingBpjsController extends Controller {

    public function actionSetFormDiagnosa() {
        if (Yii::app()->request->isAjaxRequest) {
            $diagnosaList = $_POST['diagnosaList'];
            $form = '';
            $pesan = '';
            if (count($diagnosaList) > 0) {
                foreach ($diagnosaList AS $i => $diagnosa) {
                    $kddiagnosa = $diagnosa['kode'];
                    $nmdiagnosa = $diagnosa['nama'];
                    $form .= "<tr>
                            <td>
                                <a class='btn-small' href='javascript:void(0);' onclick=\"$('#SepT_diagnosaawal_kode').val('" . $kddiagnosa . "');$('#SepT_diagnosaawal_nama').val('" . $nmdiagnosa . "');$('#dialogDiagnosaBpjs').dialog('close'); \">
                                <i class='icon-check'></i></a>
                            </td>
                            <td>
                                <span id='kdPoli' name=[ii][kdPoli]'>" . $kddiagnosa . "</span>
                            </td>
                            <td>
                                <span id='nmPoli' name=[ii][nmPoli]'>" . $nmdiagnosa . "</span>
                            </td>
                        </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }

    public function actionSetFormFaskes() {
        if (Yii::app()->request->isAjaxRequest) {
            $faskesList = $_POST['faskesList'];
            $form = '';
            $pesan = '';
            if (count($faskesList) > 0) {
                foreach ($faskesList AS $i => $faskes) {
                    $kdfaskes = $faskes['kode'];
                    $nmfaskes = $faskes['nama'];
                    $form .= "<tr>
                            <td>
                                <a class='btn-small' href='javascript:void(0);' onclick=\" $('#SepT_ppkrujukanasal_kode').val('" . $kdfaskes . "');$('#SepT_ppkrujukanasal_nama').val('" . $nmfaskes . "');$('#dialogPpk').dialog('close'); \">
                                <i class='icon-check'></i></a>
                            </td>
                            <td>
                                <span id='kdPoli' name=[ii][kdPoli]'>" . $kdfaskes . "</span>
                            </td>
                            <td>
                                <span id='nmPoli' name=[ii][nmPoli]'>" . $nmfaskes . "</span>
                            </td>
                        </tr>";
                }
            } else {
                $pesan = "Data tidak ada!";
            }

            echo CJSON::encode(array('form' => $form, 'pesan' => $pesan));
            Yii::app()->end();
        }
    }

    /**
     * set bpjs Interface
     */
    public function actionBpjsInterface() {
        if (Yii::app()->getRequest()->getIsAjaxRequest()) {
            if (empty($_GET['param']) OR $_GET['param'] === '') {
                die('param can\'not empty value');
            } else {
                $param = $_GET['param'];
            }
            $jenis_rujukan = isset($_GET['jenis_rujukan']) ? $_GET['jenis_rujukan'] : 1;

            $bpjs = new Bpjs();

            switch ($param) {
                case '1':
                    $query = $_GET['query'];
                    print_r($bpjs->search_kartu($query));
                    break;
                case '2':
                    $query = $_GET['query'];
                    print_r($bpjs->search_nik($query));
                    break;
                case '3':
                    $query = $_GET['query'];
                    if ($jenis_rujukan == 1) {
                        print_r($bpjs->search_rujukan_no_rujukan($query));
                    } else {
                        print_r($bpjs->search_rujukan_no_rujukan_rs($query));
                    }
                    break;
                case '4':
                    $query = $_GET['query'];
                    print_r($bpjs->search_rujukan_no_bpjs($query));
                    break;
                case '13':
                    $format = new CustomFormat();
                    $ruangan = RuanganM::model()->findByPk($_GET['tujuan']);
                    $noMR = $_GET['noMR'];
                    $noKartu = $_GET['noKartu'];
                    $tglSep = $format->formatDateTimeMediumForDB($_GET['tglSep']);
                    $ppkPelayanan = $_GET['ppkPelayanan'];
                    $jnsPelayanan = $_GET['jnsPelayanan'];
                    $klsRawat = $_GET['klsRawat'];
                    $asalRujukan = $_GET['asalRujukan'];
                    $tglRujukan = date('Y-m-d', strtotime($_GET['tglRujukan']));
                    $noRujukan = $_GET['noRujukan'];
                    $ppkRujukan = $_GET['ppkRujukan'];
                    $catatan = $_GET['catatan'];
                    $diagAwal = $_GET['diagAwal'];
                    $tujuan = isset($ruangan->kode_ruanganpoli)? $ruangan->kode_ruanganpoli : "";
                    $eksekutif = $_GET['eksekutif'];
                    $cob = $_GET['cob'];
                    $lakaLantas = $_GET['lakaLantas'];
                    $penjamin = $_GET['penjamin'];
                    $lokasiLaka = $_GET['lokasiLaka'];
                    $noTelp = $_GET['noTelp'];
                    $user = $_GET['user'];
                    
                    $klsPelayanan = $_GET['klsPelayanan'];
                    if(!empty($klsPelayanan)){
                        $modKelasPelayanan = KelaspelayananM::model()->findByPk($klsPelayanan);
                        if(isset($modKelasPelayanan->kodekelaspelayanan_bpjs) && !empty($modKelasPelayanan->kodekelaspelayanan_bpjs)){
                            if($modKelasPelayanan->kodekelaspelayanan_bpjs >= $klsRawat){
                                $klsRawat = $klsRawat;
                            }else if($modKelasPelayanan->kodekelaspelayanan_bpjs <= $klsRawat){
                                $klsRawat = $modKelasPelayanan->kodekelaspelayanan_bpjs;
                            }
                        }
                    }

                    print_r($bpjs->create_sep_new($noKartu, $tglSep, $ppkPelayanan, $jnsPelayanan, $klsRawat, $noMR, $asalRujukan, $tglRujukan, $noRujukan, $ppkRujukan, $catatan, $diagAwal, $tujuan, $eksekutif, $cob, $lakaLantas, $penjamin, $lokasiLaka, $noTelp, $user));
                    break;
                case '100':
                    print_r($bpjs->help());
                    break;
                default:
                    die('error number, please check your parameter option');
                    break;
            }
            Yii::app()->end();
        }
    }
    
    /**
     * @param type $sep_id
     */
    public function actionPrintSep($sep_id) {
        $this->layout = '//layouts/printWindows';
        $modSep = SepT::model()->findByPk($sep_id);
        $modInfoKunjungan = PendaftaranT::model()->findByPk($modSep->pendaftaran_id);
        
        $judul_print = 'SURAT ELIGIBILITAS PESERTA';
        $this->render('asuransi.views.sep.printSep', array(
            'modSep' => $modSep,
            'modInfoKunjungan' => $modInfoKunjungan,
            'judul_print' => $judul_print,
        ));
    }
    
    public function actionProsesSEP($pendaftaran_id,$pasien_id,$jenispelayanan){
        $this->layout = '//layouts/frameDialog';
        $model = PPPendaftaranT::model()->findByPk($pendaftaran_id);
        if(!empty($model->pasienadmisi_id)){
            $modAdmisi = PPPasienAdmisiT::model()->findByPk($model->pasienadmisi_id);
        }else{
            $modAdmisi = new PasienadmisiT;
        }
        $modPasien = PPPasienM::model()->findByPk($pasien_id);
        $modSep = New SepT;
        if(!empty($model->sep_id)){
            $modSep = SepT::model()->findByPk($model->sep_id);
        }
        
        $model->ruangan_nama = $model->ruangan->ruangan_nama;
        $modSep->ppkpelayanan = Yii::app()->user->getState('kode_ppk_bpjs');
        $modSep->ppkpelayanan_nama = Yii::app()->user->getState('nama_ppk_pelayanan');
        $modSep->tglsep = date('Y-m-d');
        $modSep->jenisrujukan_kode_bpjs = 1;
        $modSep->polieksekutif = 0;
        $modSep->cob_bpjs = 0;
        $modSep->cob_status = "TIDAK";
        $modSep->lakalantas_kode = 0;
        $modSep->jnspelayanan_kode = $jenispelayanan;
        $modLogin = LoginpemakaiK::model()->findByPk(Yii::app()->user->id);
        if(isset($modLogin->user_pemakai_bpjs) && !empty($modLogin->user_pemakai_bpjs)){
            $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->user_pemakai_bpjs;
        }else{
            $modSep->pembuat_sep = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->nama_pemakai;
        }
        
        if(isset($_POST['PPPendaftaranT'])){
            try {
                $model->attributes = $_POST['PPPendaftaranT'];
                if(isset($_POST['SepT'])){
                    $modSep = PendaftaranController::saveSep($modSep,$modPasien,$model,$_POST['SepT']);
                }
                $model->sep_id = $modSep->sep_id;
                $model->save();
                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                $this->redirect(array('ProsesSEP',array('pendaftaran_id'=>$pendaftaran_id,'pasien_id'=>$pasien_id,'jenispelayanan'=>$jenispelayanan)));
            } catch (Exception $ex) {
            }
            
        }
            
        $this->render('pendaftaranPenjadwalan.views.pendaftaran._formProsesSep', array(
            'model' => $model,
            'modSep' => $modSep,
            'modPasien' => $modPasien,
            'modAdmisi' => $modAdmisi,
        ));
    }

}
