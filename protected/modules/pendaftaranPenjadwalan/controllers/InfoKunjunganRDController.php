<?php

class InfoKunjunganRDController extends SBaseController
{	
        public $pathView = 'pendaftaranPenjadwalan.views.infoKunjunganRD.';
        
        public function actionIndex()
	{
//            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}	
            $format = new CustomFormat();
            $modInfoKunjunganRDV = new PPInfoKunjunganRDV;
            $modInfoKunjunganRDV->tglAwal=date("d M Y").' 00:00:00';
            $modInfoKunjunganRDV->tglAkhir=date("d M Y H:i:s");
                if(isset($_REQUEST['PPInfoKunjunganRDV']))
                {
                    $modInfoKunjunganRDV->attributes=$_REQUEST['PPInfoKunjunganRDV'];
                    $modInfoKunjunganRDV->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAwal']);
                    $modInfoKunjunganRDV->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRDV']['tglAkhir']);
                   


                }
              
//               
//            $modInfoKunjunganRDV->tglAwal = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modInfoKunjunganRDV->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//            $modInfoKunjunganRDV->tglAkhir = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modInfoKunjunganRDV->tglAkhir, 'yyyy-MM-dd hh:mm:ss'));  

             $this->render($this->pathView.'index',array('modInfoKunjunganRDV'=>$modInfoKunjunganRDV));
	}
        
        public function jurnalPembalikPendaftaran($pendaftaran_id){
            $sukses = true;
            if(!empty($pendaftaran_id)){
                $modTindakan = TindakanpelayananT::model()->findByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
                if(isset($modTindakan->jurnalrekening_id)){
                    $modJurnalRekening = JurnalrekeningT::model()->findByAttributes(array('jurnalrekening_id'=>$modTindakan->jurnalrekening_id));
                    if(isset($modJurnalRekening)){
                        
                            $modJurnalDetail = JurnaldetailT::model()->findAllByAttributes(array('jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id));
                            if(isset($modJurnalDetail)){
                                if(count($modJurnalDetail) > 0){
                                    foreach($modJurnalDetail AS $i => $jurnal){
                                        $jurnalBaru = new JurnaldetailT;
                                        $jurnalBaru->attributes = $jurnal->attributes;
                                        $jurnalBaru->jurnaldetail_id = null; //dikosongkan agar record baru
                                        $jurnalBaru->saldokredit = $jurnal->saldodebit;
                                        $jurnalBaru->saldodebit = $jurnal->saldokredit;
                                        $jurnalBaru->uraiantransaksi = "Pembatalan Pendaftaran Pasien";
                                        if($jurnalBaru->save())
                                            $sukses_pembalik &= true;
                                        else
                                            $sukses_pembalik = false;
                                    }
                                }
                            }
                        
                        
                    }
                }
            }
            
            return $sukses;
        }
        
        //==================================Awal batal Periksa============================================================================        
        public function actionUbahPeriksa()
        {
             if (Yii::app()->getRequest()->getIsAjaxRequest()) 
             { 
                $statusperiksa=$_POST['statusperiksa'];
                $pendaftaran_id=$_POST['pendaftaran_id']; 
                $tglbatal = $_POST['tglbatal'];
                $keterangan_batal = $_POST['keterangan_batal'];
//                $data['message']='Masih Dalam Pengembangan Karena TAbel dan View Belum Ada';
                $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                $modBatalPeriksa = new PasienbatalperiksaR;
                $modBatalPeriksa->pasien_id = $modPendaftaran->pasien_id;
                $modBatalPeriksa->pendaftaran_id = $pendaftaran_id;
                $modBatalPeriksa->tglbatal = $tglbatal;
                $modBatalPeriksa->keterangan_batal = $keterangan_batal;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modJurnalRekening = $this->jurnalPembalikPendaftaran($pendaftaran_id);
                    $updatePendaftaran = PendaftaranT::model()->updateByPk($pendaftaran_id, array('statusperiksa'=>Params::statusPeriksa(5)));
                    if($modBatalPeriksa->save() && $updatePendaftaran && $modJurnalRekening){
                        $transaction->commit();
                        $data['message']='Batal periksa berhasil dilakukan.';
                        $data['success']=true;
                    } else {
                        $transaction->rollback();
                        $data['message']='Gagal Batal Periksa! Data tidak valid.';
                        $data['success']=false;
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    $data['message']='Gagal Batal Periksa!';
                }

                
              echo json_encode($data);
                Yii::app()->end();
            }
           
//             Dikomen dulu Bisi Prosesnya Beda N Karena Ada Tabel atwpun view yang Belum Ada
//           $dataTindakanPelID = TindakanpelayananT::model()->findAllByAttributes(array('no_pendaftaran'=>$nopend));
//            if(count($dataTindakanPelID)>0)
//            {    //Jika Tindakan Sudah Dilakukan
//                    if(count($dataTindakanPelID)>0)
//                     {
//                        foreach ($dataTindakanPelID AS $tampilTindakanPelID)
//                        {  
//                            $dataTindakanSudahBayar = TindakansudahbayarT::model()->findAllByAttributes(array('tindakanpel_id'=>$tampilTindakanPelID->tindakanpel_id));
//                            if(count($dataTindakanSudahBayar)>0)
//                            {  //Jika Ada Tindakan Sudah Dibayar
//                                Yii::app()->user->setFlash('error',"No. Pendaftaran ".$nopend." Sudah Melakukan Pembayan Tindakan"); 
//                                $this->redirect(''.bu().'/index.php/pendaftaran/informasiAntrianPasien/index');
////                                $this->redirect($url);
//                            }
//                            else
//                            {   //JIka Tindakab Belum Dibayar
//                                $transaction = Yii::app()->db->beginTransaction();
//                                try
//                                    {
//                                         foreach ($dataTindakanPelID AS $tampilTindakanPelID)
//                                         {  
//                                             $sqlHapusDokterTindakan = "DELETE FROM doktertindakan_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                             $hapusDokterTindakan = Yii::app()->db->createCommand($sqlHapusDokterTindakan)->queryAll();
//                                             $sqlHapusTindakanKomponen = "DELETE FROM tindakankomponen_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                             $hapusTindakanKomponen = Yii::app()->db->createCommand($sqlHapusTindakanKomponen)->queryAll();
//                                             $sqlHapusVerifikasiTindakan = "DELETE FROM verifikasitindakan_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                             $hapusVerifikasiTindakan = Yii::app()->db->createCommand($sqlHapusVerifikasiTindakan)->queryAll(); 
//                                             $sqlHapusParamedisNonParamedis= "DELETE FROM paramedisnonparamedis_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                             $hapusHapusParamedisNonParamedis = Yii::app()->db->createCommand($sqlHapusParamedisNonParamedis)->queryAll(); 
//                                             $hapusTindakanPelayan = TindakanpelayananT::model()->deleteByPk($tampilTindakanPelID['tindakanpel_id']);
//                                             $updatePendaftaran = PendaftaranT::model()->updateByPK($nopend,array('statusperiksa_id'=>Yii::app()->params['batal_periksa']));
//                                             $transaction->commit();
//                                             Yii::app()->user->setFlash('success',"Status Periksa No. Pendaftaran ".$nopend." Berhasil Diperbaharui"); 
//                                             header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");           
//                                          }
//                                       }
//                                catch (Exception $e)
//                                       {
//                                               $transaction->rollback();
//                                               Yii::app()->user->setFlash('error',"Proses Transaksi No. Pendaftaran ".$nopend." Gagal Diperbaharuiccdc"); 
//                                                header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");                                   
//                                       }     
//                            }    
//                        }
//
//                     }
//                    else
//                     { //Hapus Karcis
//                       $transaction = Yii::app()->db->beginTransaction();
//                        try
//                            { 
//                               foreach ($dataTindakanPelID AS $tampilTindakanPelID)
//                                {  
//                                    $sqlHapusDokterTindakan = "DELETE FROM doktertindakan_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                    $hapusDokterTindakan = Yii::app()->db->createCommand($sqlHapusDokterTindakan)->queryAll();
//                                    $sqlHapusTindakanKomponen = "DELETE FROM tindakankomponen_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                    $hapusTindakanKomponen = Yii::app()->db->createCommand($sqlHapusTindakanKomponen)->queryAll();
//                                    $sqlHapusVerifikasiTindakan = "DELETE FROM verifikasitindakan_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                    $hapusVerifikasiTindakan = Yii::app()->db->createCommand($sqlHapusVerifikasiTindakan)->queryAll(); 
//                                    $sqlHapusParamedisNonParamedis= "DELETE FROM paramedisnonparamedis_t WHERE tindakanpel_id=".$tampilTindakanPelID['tindakanpel_id']."";
//                                    $hapusHapusParamedisNonParamedis = Yii::app()->db->createCommand($sqlHapusParamedisNonParamedis)->queryAll(); 
//                                    $hapusTindakanPelayan = TindakanpelayananT::model()->deleteByPk($tampilTindakanPelID['tindakanpel_id']);
//                                    $updatePendaftaran = PendaftaranT::model()->updateByPK($nopend,array('statusperiksa_id'=>Yii::app()->params['batal_periksa']));
//                                    $transaction->commit();
//                                    Yii::app()->user->setFlash('success',"Status Periksa No. Pendaftaran ".$nopend." Berhasil Diperbaharui"); 
//                                    header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");           
//                                }
//                            }
//                         catch (Exception $e)
//                            {
//                             
//                                     $transaction->rollback();
//                                     Yii::app()->user->setFlash('error',"Proses Transaksi No. Pendaftaran ".$nopend." Gagal Diperbaharui zzz"); 
//                                     header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");           
//                            
//                             }  
//                    }  
//              } 
//              else
//              {   //Jika BVelum melakukan Tindakan
//                  $updatePendaftaran = PendaftaranT::model()->updateByPK($nopend,array('statusperiksa_id'=>Yii::app()->params['batal_periksa']));
//                  if($updatePendaftaran)
//                     { //Jika Update Berhasil
//                         Yii::app()->user->setFlash('success',"Status Periksa No. Pendaftaran ".$nopend." Berhasil Diperbaharui"); 
//                          header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");           
//                     }
//                  else
//                      { //Jika Updfate Gagal
//                         Yii::app()->user->setFlash('error',"Status Periksa No. Pendaftaran ".$nopend." Gagal Diperbaharui"); 
//                          header("Location:".bu()."/index.php/pendaftaran/informasiAntrianPasien/index");
//                      }  
//              }    
        }
//================================================Akhir batal Periksa===============================================================

        
        //================================================Awal Print Lembar Poli============================================================
        public function actionPrintLembarPoli($pendaftaran_id)
        {
//            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}	
            $this->layout = '//layouts/printLembarPoli';
            $sql = "SELECT pendaftaran_t.no_pendaftaran,
                           pendaftaran_t.no_urutantri,
                           pendaftaran_t.tgl_pendaftaran,
                           pendaftaran_t.umur,
                           ruangan_m.ruangan_nama,
                           pasien_m.no_rekam_medik, 
                           penjaminpasien_m.penjamin_nama, 
                           carabayar_m.carabayar_nama, 
                           pasien_m.jeniskelamin, 
                           pasien_m.nama_pasien, 
                           pasien_m.nama_bin,
                           pasien_m.alamat_pasien, 
                           pasien_m.tanggal_lahir  
                    FROM pendaftaran_t
                    JOIN ruangan_m ON pendaftaran_t.ruangan_id = ruangan_m.ruangan_id
                    JOIN pasien_m ON pendaftaran_t.pasien_id = pasien_m.pasien_id 
                    JOIN carabayar_m ON carabayar_m.carabayar_id = pendaftaran_t.carabayar_id
                    JOIN penjaminpasien_m ON penjaminpasien_m.penjamin_id = pendaftaran_t.penjamin_id
                    
                    WHERE pendaftaran_t.pendaftaran_id ='$pendaftaran_id'";
            $result = Yii::app()->db->createCommand($sql)->queryRow();
//            daftartindakan_m.daftartindakan_nama
//                                       tindakanpelayanan_t.tarif_tindakan

//             tipepaket_m.tipepaket_nama,
//                                LEFT JOIN pegawai_m ON pendaftaran_t.pegawai_id = pegawai_m.pegawai_id
//            LEFT JOIN tindakanpelayanan_t ON tindakanpelayanan_t.no_pendaftaran = pendaftaran_t.no_pendaftaran 
//                    LEFT JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = tindakanpelayanan_t.daftartindakan_id
//                    LEFT JOIN tipepaket_m ON tipepaket_m.tipepaket_id = tindakanpelayanan_t.tipepaket_id
// pegawai_m.nama_pegawai,
            $this->render('printLembarPoli',array(
			//'model'=>$model,
                        //'noPendaftaran'=>$idx,
                        'data'=>$result,
		));
        }
//==========================================================Akhir Print Lembar Poli===================================================        

}