<?php

class DokrekammedisController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PPDokrekammedisM;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PPDokrekammedisM']))
		{
			$model->attributes=$_POST['PPDokrekammedisM'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->dokrekammedis_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PPDokrekammedisM']))
		{
			$model->attributes=$_POST['PPDokrekammedisM'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->dokrekammedis_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                
		$modDokRekamMedis=new PPDokrekammedisM;
                $modPengiriman = new PPPengirimanrmT;
                $modPengiriman->tglpengirimanrm = date('Y-m-d H:i:s');
                $modPengiriman->petugaspengirim = Yii::app()->user->name;


                if(isset($_POST['PPPengirimanrmT']))
		{
//                    echo "a";
//                    exit;
                    if (isset($_POST['Dokumen'])){
                        $transaction = Yii::app()->db->beginTransaction();
                        $jumlah = count($_POST['Dokumen']['pasien_id']);
//                       echo "b";
//                       exit;
                        try{
//                            echo "c";
//                            exit;
                            $success = true;
                            for ($i = 0; $i < $jumlah; $i++){
//                                echo "d";
//                                exit;
                                if ($_POST['cekList'][$i] == 1){
//                                    echo "e";
//                                    exit;
                                    $models = new PPDokrekammedisM;
                                    $models->nodokumenrm = Generator::noDokumenRM();
                                    $models->pasien_id = $_POST['Dokumen']['pasien_id'][$i];
                                    $models->warnadokrm_id = $_POST['Dokumen']['warnadokrm_id'][$i];
                                    //$models->subrak_id = $_POST['Dokumen']['subrak_id'][$i];
                                    //$models->lokasirak_id = $_POST['Dokumen']['lokasirak_id'][$i];
                                    $models->tglrekammedis = $_POST['Dokumen']['tgl_rekam_medik'][$i];
                                    $models->tglmasukrak = date('Y-m-d H:i:s');
                                    $models->statusrekammedis = Params::STATUS_REKAMMEDIS_PASIENBARU;
                                    $models->nomorprimer = substr($_POST['Dokumen']['no_rekam_medik'][$i], 4, 2);
                                    $models->nomorsekunder = substr($_POST['Dokumen']['no_rekam_medik'][$i], 2, 2);
                                    $models->nomortertier = substr($_POST['Dokumen']['no_rekam_medik'][$i], 0, 2);
                                    $models->warnanorm_i = WarnanomorrmM::model()->findByAttributes(array('warnanomorrm_angka'=>substr($models->nomorprimer,0,1)))->warnanomorrm_id;
                                    $models->warnanorm_ii = WarnanomorrmM::model()->findByAttributes(array('warnanomorrm_angka'=>  substr($models->nomorprimer,1,1)))->warnanomorrm_id;
//                                    echo "<pre>";
//                                    echo print_r($models->getAttributes());
//                                    exit;
                                    if ($models->save()){
//                                        echo "f";
//                                        exit;
                                        $modelPengiriman = new PPPengirimanrmT();
                                        $modelPengiriman->attributes = $_POST['PPPengirimanrmT'];
                                        $modelPengiriman->dokrekammedis_id = $models->dokrekammedis_id;
                                        $modelPengiriman->nourut_keluar = Generator::noUrutKeluarRM();
                                        $modelPengiriman->pasien_id = $models->pasien_id;
                                        $modelPengiriman->pendaftaran_id = $_POST['Dokumen']['pendaftaran_id'][$i];
                                        $modelPengiriman->ruangan_id = $_POST['Dokumen']['ruangan_id'][$i];
                                        $modelPengiriman->ruanganpengirim_id = Yii::app()->user->getState('ruangan_id');
                                        if ($modelPengiriman->save()){
//                                            echo "g";
//                                            exit;
                                            PasienM::model()->updateByPk($models->pasien_id, array('dokrekammedis_id'=>$models->dokrekammedis_id));
                                        }
                                    }else{
//                                        echo "h";
//                                        exit;
                                        
                                        $success = false;
                                    }
                                }
                            }
                            if ($success == true){
//                                echo "i";
//                                exit;
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data Pengiriman Dokumen Rekam Medis berhasil disimpan");
                                $this->refresh();
                            }
                            else{
//                                echo "j";
//                                exit;
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                $this->refresh();
                            }
                        }
                        catch (Exception $exc) {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));       
                                $this->refresh();
                        }
                    }
//                    exit();
//			$model->attributes=$_POST['PPDokrekammedisM'];
//			if($model->save()){
//                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//				$this->redirect(array('view','id'=>$model->dokrekammedis_id));
//                        }
		}
                
                $model=new PPDokumenpasienrmbaruV('search');
                $model->unsetAttributes();  // clear any default values
                //$modDokRekamMedis->nodokumenrm = Generator::noDokumenRM();
                $model->tgl_rekam_medik = date('Y-m-d H:i:s');
                $model->tgl_rekam_medik_akhir = date('Y-m-d H:i:s');
                if(isset($_GET['PPDokumenpasienrmbaruV'])) {
                    $format = new CustomFormat;
                    $model->attributes=$_GET['PPDokumenpasienrmbaruV'];
                    $model->tgl_rekam_medik = $format->formatDateTimeMediumForDB($_GET['PPDokumenpasienrmbaruV']['tgl_rekam_medik']);
                    $model->tgl_rekam_medik_akhir = $format->formatDateTimeMediumForDB($_GET['PPDokumenpasienrmbaruV']['tgl_rekam_medik_akhir']);
                }
                
                $this->render('index',array(
                    'model'=>$model,
                    'modDokRekamMedis'=>$modDokRekamMedis,
                    'modPengiriman'=>$modPengiriman,
                ));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PPDokrekammedisM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PPDokrekammedisM']))
			$model->attributes=$_GET['PPDokrekammedisM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPDokrekammedisM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppdokrekammedis-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new PPDokumenpasienrmbaruV;
            $model->attributes=$_REQUEST['PPDokumenpasienrmbaruV'];
            $judulLaporan='Data Dokter Rekam Medis';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
