<?php

class InformasiJadwalPoliController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $_lastHari = null;
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Informasi Jadwal Buka PoliKlinik";
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PPJadwalBukaPoliM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PPJadwalBukaPoliM']))
			$model->attributes=$_GET['PPJadwalBukaPoliM'];

		$this->render('index',array(
			'model'=>$model,
		));
	}
        
        protected function gridHari($data,$row)
        {
           if($this->_lastHari != $data->hari)
           {
               $this->_lastHari=$data->hari;
               return $data->hari;
           }
           else{
               return '';
           }
        }
        

	
}