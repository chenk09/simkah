<?php

class InformasiJadwalDokterController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $_lastHari = null;
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Informasi Jadwal Dokter";
            $model=new RDJadwaldokterM;
            $model->instalasi_id=Params::INSTALASI_ID_RD;
            
            /**
             * handling ajax request dari form search 
             */
            if (Yii::app()->request->isAjaxRequest){
                if (isset($_GET['RDJadwaldokterM'])){
                    $mulai = (!empty($_GET['RDJadwaldokterM']['jadwaldokter_mulai'])) ? date('Y-m-d',strtotime('01 '.$_GET['RDJadwaldokterM']['jadwaldokter_mulai'])) : date('Y-m-d');
                    $tgl = explode('-',$mulai);
                    $day = cal_days_in_month(CAL_GREGORIAN, $tgl[1], $tgl[0]);
                    $grid = $this->createGrid($day,$tgl[1],$tgl[0],$_GET['RDJadwaldokterM']);
                    echo json_encode($grid);
                }
                
                if (isset($_POST['data'])){
                    $listHari = array( 'Senin'=> 'Senin',
                                   'Selasa'=> 'Selasa',
                                   'Rabu'=> 'Rabu',
                                   'Kamis'=> 'Kamis',
                                   'Jumat'=> 'Jumat',
                                   'Sabtu'=> 'Sabtu',
                                   'Minggu'=> 'Minggu',
                                );
                    $id = $_POST['data'];
                    $modJadwal = JadwaldokterM::model()->findByPk($id);
                    
                    if (isset($_POST['JadwaldokterM'])){
                        $id = $_POST['JadwaldokterM']['jadwaldokter_id'];
                        $modJadwal = JadwaldokterM::model()->findByPk($id);
                        $modJadwal->attributes = $_POST['JadwaldokterM'];
                        if ($modJadwal->save()){
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        }
                    }
                    $pegawai = (!empty($modJadwal->ruangan_id)) ? CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>$modJadwal->ruangan_id)), 'pegawai_id', 'nama_pegawai') : array();
                    $ruangan = (!empty($modJadwal->instalasi_id)) ? CHtml::listData(RuanganM::model()->findAll('instalasi_id = ?', array($modJadwal->instalasi_id)), 'ruangan_id','ruangan_nama') : array();
                    $return = $this->renderPartial('_createForm',array('pegawai'=>$pegawai,'model'=>$modJadwal,'ruangan'=>$ruangan, 'listHari'=>$listHari),true);
                    echo json_encode($return);
                }
                
                if (isset($_GET['JadwaldokterM'])){
                    
                }
                Yii::app()->end();
            }
            
            $tgl = explode('-',date('Y-m-d'));
            $day = cal_days_in_month(CAL_GREGORIAN, $tgl[1], $tgl[0]);
            $grid = $this->createGrid($day,$tgl[1],$tgl[0]);
            
            if(isset($_REQUEST['RDJadwaldokterM'])){
                $model->attributes = $_REQUEST['RDJadwaldokterM'];

            }
            $this->render('index',
                    array('model'=>$model,'listHari'=>$listHari,'grid'=>$grid)
                    );
	}
        
        protected function gridHari($data,$row)
        {
           if($this->_lastHari != $data->jadwaldokter_hari)
           {
               return $data->jadwaldokter_hari;
           }
           else{
               return '';
           }
        }
        
        protected function gridDokter($data,$row)
        {
           $this->_lastHari = $data->jadwaldokter_hari;
           return $data->pegawai->nama_pegawai;  
        }

	/**
         * method untuk membuat calendar jadwal dokter
         * @param sting $jumlahhari
         * @param string $bulan
         * @param string $tahun
         * @param array $variable
         * @return string berupa grid calender dengan jadwal dokter
         */
        protected function createGrid($jumlahhari,$bulan,$tahun,$variable=null){
            $tglMulai = strtotime($tahun.'-'.$bulan.'-'.'01');
            return $this->renderPartial("createGrid",array('tglMulai'=>$tglMulai, 'bulan'=>$bulan,'tahun'=>$tahun,'jumlahHari'=>$jumlahhari,'variable'=>$variable),true);
        }
}