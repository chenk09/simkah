<?php

class InformasiPegawaiController extends SBaseController
{
	public function actionIndex()
	{
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}	
            $format = new CustomFormat();
            $modPPPegawaiM = new PPPegawaiV;
            
                if(isset($_REQUEST['PPPegawaiV']))
                {
                    $modPPPegawaiM->attributes=$_REQUEST['PPPegawaiV'];
                }
              
             $this->render('index',array('modPPPegawaiM'=>$modPPPegawaiM));
	}
        
        public function actionViewPegawai($id)
        {
            $this->render('viewPegawai',array(
			'modPPPegawaiM'=>$this->loadModel($id),
		));
        }
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$modPPPegawaiM=PPPegawaiM::model()->findByPk($id);
		if($modPPPegawaiM===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $modPPPegawaiM;
	}

}