<?php

class ActionAjaxController extends SBaseController
{
    public function actionUbahDokterJadwal()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idJadwal=$_POST['idJadwal'];
            $idDokter=$_POST['idDokter'];
            $dokterSebelumnya=$_POST['dokterSebelumnya'];
            
            $criteria =new CDbCriteria;
            $criteria->compare('pegawai_id',$dokterSebelumnya);

            if(JadwaldokterM::model()->updateAll(array('pegawai_id'=>$idDokter,
                                                       'update_loginpemakai_id'=>Yii::app()->user->id,
                                                       'update_time'=>date('Y-m-d H:i:s')),$criteria)){
                $data['status'] = 'OK';
            } else {
                $data['status'] = 'gagal';
            }
            echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionUbahJamBukaPoli()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idJadwal=$_POST['idJadwal'];
            $jamMulai=$_POST['jamMulai'];
            $jamTutup=$_POST['jamTutup'];
            $jamBuka = $jamMulai.' s/d '.$jamTutup;
            
            if(JadwalbukapoliM::model()->updateByPk($idJadwal, 
                    array('jmabuka'=>$jamBuka,
                          'jammulai'=>$jamMulai,
                          'jamtutup'=>$jamTutup,
                          'update_loginpemakai_id'=>Yii::app()->user->id,
                          'update_time'=>date('Y-m-d H:i:s')))){
                $data['status'] = 'OK';
            } else {
                $data['status'] = 'gagal';
            }
            echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionUbahJamBukaDokter()
    {
        if(Yii::app()->request->isAjaxRequest) {
            $idJadwal=$_POST['idJadwal'];
            $jamMulai=$_POST['jamMulai'];
            $jamTutup=$_POST['jamTutup'];
            $jamBuka = $jamMulai.' s/d '.$jamTutup;
            
            if(JadwaldokterM::model()->updateByPk($idJadwal, 
                    array('jadwaldokter_buka'=>$jamBuka,
                          'jadwaldokter_mulai'=>$jamMulai,
                          'jadwaldokter_tutup'=>$jamTutup,
                          'update_loginpemakai_id'=>Yii::app()->user->id,
                          'update_time'=>date('Y-m-d H:i:s')))){
                $data['status'] = 'OK';
            } else {
                $data['status'] = 'gagal';
            }
            echo json_encode($data);
         Yii::app()->end();
        }
    }
    
    public function actionGetPengantar(){
        if(Yii::app()->request->isAjaxRequest) {
            $pengantar = $_POST['pengantar'];
            $norm = $_POST['norekammedik'];
            
                $modPasien = PasienM::model()->findAllByAttributes(array('no_rekam_medik'=>$norm));
                $modPendaftaran = PendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$modPasien[0]->pasien_id),array('order'=>'pendaftaran_id DESC','limit'=>1));
                $modPengantar = PenanggungjawabM::model()->findByPk($modPendaftaran[0]->penanggungjawab_id);
                if(count($modPendaftaran) > 0){
                    $data['status'] = 'ada';
                    $data['jk'] = $modPengantar->jeniskelamin;
                    $data['nama'] = $modPengantar->nama_pj;
                }else{
                    $data['status'] = 'kosong';
                    $data['jk'] = '';
                    $data['nama'] = '';
                }
            
            echo json_encode($data);
            Yii::app()->end();
        }        
    }

    public function actioncariPegawai(){
        if(Yii::app()->request->isAjaxRequest) {
            $nik    = $_POST['nik'];
            $modPegawai = PegawaiM::model()->findByAttributes(array('nomorindukpegawai'=>$nik));
            if(count($modPegawai)>0){
                $data['hasil'] = "Nama Karyawan : ".$modPegawai->gelardepan." ".$modPegawai->nama_pegawai." \nAlamat               : ".$modPegawai->alamat_pegawai;
                $data['status'] = true;
            }else{
                $data['hasil'] = "Nama Karyawan dengan NIK ".$nik." tidak ditemukan";
                $data['status'] = false;
            }
            echo json_encode($data);
            Yii::app()->end();
        }
    }
}
?>
