<?php

class BookingKamarTController extends SBaseController {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column1';
    public $defaultAction = 'create';
    public $successSave = false;
    public $pathView = 'pendaftaranPenjadwalan.views.bookingKamarT.';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'print', 'updateStatusKonfirmasi'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete', 'RemoveTemporary', 'updateStatusKonfirmasi'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render($this->pathView . 'view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = new PPBookingKamarT;
        $model->tglbookingkamar = date('d M Y H:i:s');
        $model->bookingkamar_no = Generator::noBookingKamar();
        $model->thn = '00';
        $model->bln = '00';
        $model->hr = '00';

        if (isset($_POST['PPBookingKamarT'])) {
            $model->attributes = $_POST['PPBookingKamarT'];
            $model->tgltransaksibooking = date('Y-m-d H:i:s');
            $model->statuskonfirmasi = "BELUM KONFIRMASI";

            if ($model->save()) {
                if ($model->statusbooking == "NON ANTRI") {
                    KamarruanganM::model()->updateByPk($model->kamarruangan_id, array('keterangan_kamar' => "BOOKING", 'kamarruangan_status' => true));
                }
                Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                $this->redirect(array('update', 'id' => $model->bookingkamar_id, 'print' => 1));
            } else {
                Yii::app()->user->setFlash('error', 'Data Gagal disimpan ');
            }
        }

        $this->render($this->pathView . 'create', array(
            'model' => $model,
            'modPasien' => $modPasien,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id, $print = null) {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model = $this->loadModel($id);
        $model->statusbooking_dropdown = $model->statusbooking;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['PPBookingKamarT'])) {
            $model->attributes = $_POST['PPBookingKamarT'];
            if ($model->save()) {
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('admin', 'id' => $model->bookingkamar_id));
            }
        }

        $this->render($this->pathView . 'update', array(
            'model' => $model,
            'print' => $print
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $model = $this->loadModel($id);
            $kamarruangan = KamarruanganM::model()->findByPk($model->kamarruangan_id);
            if ($kamarruangan && $kamarruangan->keterangan_kamar == 'BOOKING' && $kamarruangan->kamarruangan_status){
                $modBooking = BookingkamarT::model()->countByAttributes(array('kamarruangan_id'=>$model->kamarruangan_id, 'pasienadmisi_id' => null));
                if ($modBooking == 1){
                    $kamarruangan->keterangan_kamar = 'OPEN';
                    $kamarruangan->save();
                }
            }
            $model->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        } else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('PPBookingKamarT');
        $this->render($this->pathView . 'index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $format = new CustomFormat();
        $model = new PPBookingKamarT;
        $model->unsetAttributes(); // clear any default values
        $model->tglAwal = date('d M Y') . ' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['PPBookingKamarT'])) {
            $model->attributes = $_GET['PPBookingKamarT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPBookingKamarT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPBookingKamarT']['tglAkhir']);
        }
//                $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//                $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss')); 
        $this->render($this->pathView . 'admin', array(
            'model' => $model,
        ));
    }

    public function actionUpdateStatusKonfirmasi() {
        $satu = '';
        $model = BookingkamarT::model()->findByPk($idBooking);
        $idBooking = $_POST['idBooking'];
        $tglbookingkamar = $_POST['tglbookingkamar'];

        $model->tglbookingkamar = $_POST['tglbookingkamar'];
        $model->bookingkamar_id = $_POST['idBooking'];
        $waktuini = date('h:i:s');

        $jamtransaksi = $model->tgltransaksibooking;

        $test = date('H:i:s', strtotime($jamtransaksi));
        $test2 = date('H:i:s');
        $jamtrans = $test;
        $jamsaatini = $test2;
        $jml_jam = $this->selisih($jamtrans, $jamsaatini);

//              echo "test: ".$test; echo "<br>";
//              echo "test2:".$test2; echo "<br>";
//              echo "trans:".$jamtrans; echo "<br>";
//              echo "now:".$jamsaatini; echo "<br>";
//              echo "WAktu Kerja : ".selisih($jamtrans,$jamsaatini);

        if ($jml_jam >= 2 && $model->statuskonfirmasi != "SUDAH KONFIRMASI") {
            $update = BookingkamarT::model()->updateByPk($idBooking, array('statuskonfirmasi' => 'BATAL BOOKING'));
        }

        if ($update) {
            $satu = $this->createUrl('admin');
        }

        echo CJSON::encode(array
            (
            'satu' => $satu,
            'tglbooking' => $tglbookingkamar,
            'idBooking' => $idBooking,
        ));
        Yii::app()->end();
    }

    function selisih($jamtrans, $jamsaatini) {
        list($h, $m, $s) = explode(":", $jamtrans);
        $dtAwal = mktime($h, $m, $s, '1', '1', '1');
        list($h, $m, $s) = explode(':', $jamsaatini);
        $dtAkhir = mktime($h, $m, $s, '1', '1', '1');
        $dtSelisih = $dtAkhir - $dtAwal;
        $totalmenit = $dtSelisih / 60;
        $jam = explode(".", $totalmenit / 60);
        $sisamenit = ($totalmenit / 60) - $jam[0];
        $sisamenit2 = $sisamenit * 60;
        $jml_jam = $jam[0];

//          return $jml_jam." jam ".$sisamenit2." menit"; 
        return $jml_jam;
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = PPBookingKamarT::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'ppbooking-kamar-t-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    /**
     * Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary($id) {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionPrint() {
        $model = new PPBookingKamarT;
        $model->attributes = $_REQUEST['PPBookingKamarT'];
        $judulLaporan = 'Data Booking Kamar';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render($this->pathView . 'Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($this->pathView . 'Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($this->pathView . 'Print', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    public function savePasien($attrPasien) {
        $modPasien = new PPPasienM;
        $modPasien->attributes = $attrPasien;
        $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
        $modPasien->no_rekam_medik = Generator::noRekamMedikBK('BK');
        $modPasien->tgl_rekam_medik = date('Y-m-d', time());
        $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
        $modPasien->statusrekammedis = 'AKTIF';
        $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');

        if ($modPasien->validate()) {
            // form inputs are valid, do something here
            $modPasien->save();
            $this->successSave = true;
        } else {
            // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
            $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                    CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'), 'medium', null);
        }
        return $modPasien;
    }

}
