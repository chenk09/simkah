<?php

class InfoKunjunganRIController extends SBaseController
{
        public $pathView = 'pendaftaranPenjadwalan.views.infoKunjunganRI.';
        
	public function actionIndex()
	{
		
//            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_OPERATING)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}	
            $format = new CustomFormat();
            $modPPInfoKunjunganRIV = new PPInfoKunjunganRIV;
            $modPPInfoKunjunganRIV->tglAwal=date('d M Y').' 00:00:00';
            $modPPInfoKunjunganRIV->tglAkhir=date('d M Y H:i:s');
                if(isset($_REQUEST['PPInfoKunjunganRIV']))
                {
                    $modPPInfoKunjunganRIV->attributes=$_REQUEST['PPInfoKunjunganRIV'];
                    $modPPInfoKunjunganRIV->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAwal']);
                    $modPPInfoKunjunganRIV->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PPInfoKunjunganRIV']['tglAkhir']);
                }
                /*

             $this->render(
                'pendaftaranPenjadwalan.views.infoKunjunganRI.index',
                 array('modPPInfoKunjunganRIV'=>$modPPInfoKunjunganRIV)
             );
             */
             $this->render($this->pathView.'index',array('modPPInfoKunjunganRIV'=>$modPPInfoKunjunganRIV));
	}
}