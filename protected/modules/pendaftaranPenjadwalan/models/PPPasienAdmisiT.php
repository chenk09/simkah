<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PPPasienAdmisiT extends PasienadmisiT
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PPPasienAdmisiT the static model class
     */
    public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pegawai_id, penjamin_id, kelaspelayanan_id, caramasuk_id, pasien_id, ruangan_id, carabayar_id, tgladmisi, tglpendaftaran, kunjungan', 'required'),
			array('penjamin_id, kelaspelayanan_id, caramasuk_id, pendaftaran_id, kamarruangan_id, pegawai_id, pasien_id, ruangan_id, carabayar_id, bookingkamar_id', 'numerical', 'integerOnly'=>true),
			array('kunjungan', 'length', 'max'=>50),
			array('tglpulang, statuskeluar, rawatgabung', 'safe'),
                    
                        array('create_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'insert'),
                        array('update_time','default','value'=>date( 'Y-m-d H:i:s', time()),'setOnEmpty'=>false,'on'=>'update,insert'),
                        array('create_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'insert'),
                        array('update_loginpemakai_id','default','value'=>Yii::app()->user->id,'on'=>'update,insert'),
                        array('shift_id','default','value'=>Yii::app()->user->getState('shift_id'),'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pasienadmisi_id, penjamin_id, kelaspelayanan_id, caramasuk_id, pendaftaran_id, kamarruangan_id, pegawai_id, pasien_id, ruangan_id, carabayar_id, bookingkamar_id, tgladmisi, tglpendaftaran, tglpulang, kunjungan, statuskeluar, rawatgabung, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}
    protected function beforeValidate (){
        return parent::beforeValidate ();
    }
        
    protected function beforeSave() {  
        return parent::beforeSave();
    }
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
}
?>
