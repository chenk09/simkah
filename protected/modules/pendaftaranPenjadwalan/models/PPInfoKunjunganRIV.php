<?php

/**
 * This is the model class for table "infokunjunganri_v".
 *
 * The followings are the available columns in table 'infokunjunganri_v':
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property integer $pendaftaran_id
 * @property string $no_pendaftaran
 * @property string $tgl_pendaftaran
 * @property integer $pasienadmisi_id
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $caramasuk_id
 * @property string $caramasuk_nama
 * @property integer $kelaspelayanan_id
 * @property string $kelaspelayanan_nama
 * @property integer $kamarruangan_id
 * @property string $tgladmisi
 * @property string $kamarruangan_nokamar
 * @property integer $ruangan_id
 * @property string $ruangan_nama
 */
class PPInfoKunjunganRIV extends InfokunjunganriV
{
        public $jumlah;
        public $data;
        public $tick;
        public $ruanganasal_nama;
        public $kondisipulang;
		public $jumlahlamarawat,$tglpindahkamar,$nokamar_akhir,$jumlahlamarawatruangpindahan;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfokunjunganriV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
         * untuk set jenis kasus penyakit nama di laporan pasien rawat inap gatau kenapa pake bukan dari attribute tablenya langsung
         * @return string
         */
        public function getJenis_kasus_nama_penyakit(){
            return $this->jeniskasuspenyakit_nama;
        }
        
        /**
         * get status pasien
         * @return string
         */
        public function getStatusPasienRawatInap(){
            $pendaftaran = PendaftaranT::model()->findByPk($this->pendaftaran_id);
            return $status = (isset($pendaftaran->pasienpulang_id)) ? 'Dipulangkan' : 'Sedang Dirawat Inap';
        }
        
        public function searchRI()
	{

		$criteria=new CDbCriteria;
                
                $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('LOWER(caramasuk_nama)',strtolower($this->caramasuk_nama),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('kamarruangan_id',$this->kamarruangan_id);
		$criteria->compare('LOWER(tgladmisi)',strtolower($this->tgladmisi),true);
		$criteria->compare('LOWER(kamarruangan_nokamar)',strtolower($this->kamarruangan_nokamar),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->order = 'tgl_pendaftaran DESC';
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function criteriaSearch()
	{

		$criteria=new CDbCriteria;
                
                $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('LOWER(caramasuk_nama)',strtolower($this->caramasuk_nama),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('kamarruangan_id',$this->kamarruangan_id);
		$criteria->compare('LOWER(tgladmisi)',strtolower($this->tgladmisi),true);
		$criteria->compare('LOWER(kamarruangan_nokamar)',strtolower($this->kamarruangan_nokamar),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->order = 'tgl_pendaftaran DESC';
        return $criteria;
		}	
        
        public function searchGrafik(){
            
        $criteria = MyFunction::criteriaGrafik1($this, 'data', array('tick'=>'ruangan_nama'));
                
        $criteria->order = 'ruangan_nama';
                
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public function searchTableLaporan()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('LOWER(caramasuk_nama)',strtolower($this->caramasuk_nama),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('kamarruangan_id',$this->kamarruangan_id);
		$criteria->compare('LOWER(tgladmisi)',strtolower($this->tgladmisi),true);
		$criteria->compare('LOWER(kamarruangan_nokamar)',strtolower($this->kamarruangan_nokamar),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getNamaModel()
        {
            return __CLASS__;
        }
//Berdasarkan Agama        
        public function searchAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printAgama(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'agama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikAgama(){
             
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, agama as data';
        $criteria->group = 'agama';
        $criteria->order = 'agama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchUmur(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printUmur(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'golonganumur_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikUmur(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, golonganumur_nama as data';
        $criteria->group = 'golonganumur_nama';
        $criteria->order = 'golonganumur_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchJk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printJk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'jeniskelamin';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikJk(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, jeniskelamin as data';
        $criteria->group = 'jeniskelamin';
        $criteria->order = 'jeniskelamin';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Status

        public function searchStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printStatus(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statuspasien';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikStatus(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, statuspasien as data';
        $criteria->group = 'statuspasien';
        $criteria->order = 'statuspasien';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPekerjaan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'pekerjaan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikPekerjaan(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, pekerjaan_nama as data';
        $criteria->group = 'pekerjaan_nama';
        $criteria->order = 'pekerjaan_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Status Perkawinan        
        public function searchStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printStatusPerkawinan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperkawinan';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikStatusPerkawinan(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, statusperkawinan as data';
        $criteria->group = 'statusperkawinan';
        $criteria->order = 'statusperkawinan';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Kecamatan        
        public function searchKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKecamatan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kecamatan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikKecamatan(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, kecamatan_nama as data';
        $criteria->group = 'kecamatan_nama';
        $criteria->order = 'kecamatan_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKabKota(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'kabupaten_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikKabupaten(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, kabupaten_nama as data';
        $criteria->group = 'kabupaten_nama';
        $criteria->order = 'kabupaten_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
//Berdasarkan Cara Masuk
        public function searchCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printCaraMasuk(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'caramasuk_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikCaraMasuk(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, caramasuk_nama as data';
        $criteria->group = 'caramasuk_nama';
        $criteria->order = 'caramasuk_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
//Berdasarkan Penjamin
        public function searchDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printDokterPemeriksa(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pegawai';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
        
        public function searchGrafikDokterPemeriksa(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, nama_pegawai as data';
        $criteria->group = 'nama_pegawai';
        $criteria->order = 'nama_pegawai';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        public function searchGrafikUnitPelayanan(){
            
                $criteria=new CDbCriteria;
               
                $criteria->select = 'count(pendaftaran_id) as jumlah, ruangan_nama as data';
                $criteria->group = 'ruangan_nama';
                $criteria->order = 'ruangan_nama';
                
                $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
                $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Penjamin
        public function searchPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printPenjamin(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'penjamin_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        
         public function searchGrafikPenjamin(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, penjamin_nama as data';
        $criteria->group = 'penjamin_nama';
        $criteria->order = 'penjamin_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
//Berdasarkan Alamat        
        public function searchAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printAlamat(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'alamat_pasien';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        public function searchGrafikAlamat(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, alamat_pasien as data';
        $criteria->group = 'alamat_pasien';
        $criteria->order = 'alamat_pasien';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Rujukan
        public function searchRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printRujukan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_perujuk';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        
         public function searchGrafikRujukan(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, nama_perujuk as data';
        $criteria->group = 'nama_perujuk';
        $criteria->order = 'nama_perujuk';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchRM(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pasien';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printRM(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'nama_pasien';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }

        public function searchGrafikrmRI(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, nama_pasien as data';
        $criteria->group = 'nama_pasien';
        $criteria->order = 'nama_pasien';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

//Berdasarkan Keterangan Pulang
        public function searchKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKetPulang(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'statusperiksa';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }        
        
        public function searchGrafikKetPulang(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, statusperiksa as data';
        $criteria->group = 'statusperiksa';
        $criteria->order = 'statusperiksa';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

        public function searchKamarRuangan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }

        public function printKamarRuangan(){
            $criteria = $this->criteriaSearch();
            $criteria->order = 'ruangan_nama';          
            $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
        }
               
        public function searchGrafikKamarRuangan(){
            
        $criteria=new CDbCriteria;
       
        $criteria->select = 'count(pendaftaran_id) as jumlah, ruangan_nama as data';
        $criteria->group = 'ruangan_nama';
        $criteria->order = 'ruangan_nama';
        
        $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
        $criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public function getNamaAlias()
        {
            if(!empty($this->nama_bin)){
                return $this->nama_pasien.' Alias '.$this->nama_bin;
            }else{
                return $this->nama_pasien;
            }
            
        }
        
        public function primaryKey() {
            return 'pendaftaran_id';
        }
	
        /**
         * menampilkan morbiditas pasien
         * @return type
         */
        public function getMorbiditas(){
            $criteria = new CDbCriteria();
            $criteria->addCondition("pendaftaran_id = ".$this->pendaftaran_id);
            $modDiagnosa = PasienmorbiditasT::model()->find($criteria);
            return $modDiagnosa;
        }
}