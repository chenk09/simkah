<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PPRuanganM
 *
 * @author sujana
 */
class PPRuanganM extends RuanganM{
    
    public $jumlahkunjungan, $jumlahkunjunganlama, $jumlahkunjunganbaru, $pendaftaran_id;
    
    public $dokter_nama;
    
    public $tick, $data, $jumlah,$jml;
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function relations()
   {
       return array(
           'tindakanruangan'=>array(self::HAS_MANY,'PPTindakanruanganM','ruangan_id'),
           'daftartindakan'=>array(self::HAS_MANY,'PPDaftartindakanM',array('daftartindakan_id'=>'daftartindakan_id'),'through'=>'tindakanruangan'),
           'tariftindakan'=>array(self::HAS_MANY,'PPTariftindakan',array('daftartindakan_id'=>'daftar_tindakan_id'),'through'=>'daftartindakan'),
           'instalasi' => array(self::BELONGS_TO, 'InstalasiM', 'instalasi_id'),
       );
   }
   //INI HARUSNYA TIDAK DIGUNAKAN LAGI : EHJ-3642
		public function searchUnitPelayanan()
		{
			$criteria = new CDbCriteria();
			$criteria->group = 't.ruangan_nama,t.ruangan_id';
			$criteria->select = $criteria->group.', '
					. 'COUNT(pendaftaran_t.statuspasien) AS jumlahkunjungan, '
					. 'COUNT(CASE pendaftaran_t.statuspasien WHEN \'PENGUNJUNG BARU\' THEN 1 ELSE NULL END) AS jumlahkunjunganbaru, '
					. 'COUNT(CASE pendaftaran_t.statuspasien WHEN \'PENGUNJUNG LAMA\' THEN 1 ELSE NULL END) AS jumlahkunjunganlama';
			$criteria->order = 't.ruangan_nama';
			$criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.ruangan_id = t.ruangan_id LEFT JOIN pegawai_m ON pegawai_m.pegawai_id = pendaftaran_t.pegawai_id';
			$criteria->addBetweenCondition('pendaftaran_t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
			$criteria->compare('ruangan_aktif', true);
			
			if(!empty($this->instalasi_id)){
				$criteria->addCondition('t.instalasi_id = '.$this->instalasi_id);
			}
			if(!empty($this->ruangan_id)){
				if(is_array($this->ruangan_id)){
					$criteria->addInCondition('t.ruangan_id',$this->ruangan_id);
				}else{
					$criteria->addCondition('t.ruangan_id = '.$this->ruangan_id);
				}
			}
			
			/*
			$criteria->compare('LOWER(pegawai_m.nama_pegawai)',strtolower($this->dokter_nama),true);
			$criteria->addCondition("pendaftaran_t.tgl_pendaftaran BETWEEN '07 Jan 2016 00:00:0' AND '31 Jan 2016 00:00:0'");
			*/
			
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
		}
		
		public function searchUnitPelayananInap()
		{
			$criteria = new CDbCriteria();
			$criteria->group = 't.ruangan_nama,t.ruangan_id';
			$criteria->select = $criteria->group.', '
					. 'COUNT(pasienadmisi_t.kunjungan) AS jumlahkunjungan, '
					. 'COUNT(CASE pasienadmisi_t.kunjungan WHEN \'KUNJUNGAN BARU\' THEN 1 ELSE NULL END) AS jumlahkunjunganbaru, '
					. 'COUNT(CASE pasienadmisi_t.kunjungan WHEN \'KUNJUNGAN LAMA\' THEN 1 ELSE NULL END) AS jumlahkunjunganlama';
			$criteria->order = 't.ruangan_nama';
			$criteria->join = 'LEFT JOIN pasienadmisi_t ON pasienadmisi_t.ruangan_id = t.ruangan_id';
			$criteria->join .= ' JOIN pegawai_m ON pegawai_m.pegawai_id = pasienadmisi_t.pegawai_id';
			$criteria->compare('ruangan_aktif', true);
			$criteria->addBetweenCondition('pasienadmisi_t.tgladmisi', $this->tglAwal, $this->tglAkhir);
			
			if(!empty($this->instalasi_id)){
				$criteria->addCondition('t.instalasi_id = '.$this->instalasi_id);
			}
			
			if(!empty($this->ruangan_id)){
				if(is_array($this->ruangan_id)){
					$criteria->addInCondition('t.ruangan_id',$this->ruangan_id);
				}else{
					$criteria->addCondition('t.ruangan_id = '.$this->ruangan_id);
				}
			}
			
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
		}
		
		//INI HARUSNYA TIDAK DIGUNAKAN LAGI : EHJ-3642
		public function searchUnitPelayananPrint(){
			$criteria = new CDbCriteria();
			$criteria->group = 't.ruangan_nama,t.ruangan_id, t.instalasi_id, pegawai_m.nama_pegawai';
			$criteria->select = $criteria->group.', '
					. 'COUNT(pendaftaran_t.statuspasien) AS jumlahkunjungan, '
					. 'COUNT(CASE pendaftaran_t.statuspasien WHEN \'PENGUNJUNG BARU\' THEN 1 ELSE NULL END) AS jumlahkunjunganbaru, '
					. 'COUNT(CASE pendaftaran_t.statuspasien WHEN \'PENGUNJUNG LAMA\' THEN 1 ELSE NULL END) AS jumlahkunjunganlama';
			$criteria->order='t.instalasi_id';
			$criteria->join ='LEFT JOIN pendaftaran_t ON pendaftaran_t.ruangan_id = t.ruangan_id
								LEFT JOIN pegawai_m ON pegawai_m.pegawai_id = pendaftaran_t.pegawai_id';
			$criteria->addBetweenCondition('pendaftaran_t.tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
			if(!empty($this->instalasi_id)){
				$criteria->addCondition('t.instalasi_id = '.$this->instalasi_id);
			}
			if(!empty($this->ruangan_id)){
				if(is_array($this->ruangan_id)){
					$criteria->addInCondition('t.ruangan_id',$this->ruangan_id);
				}else{
					$criteria->addCondition('t.ruangan_id = '.$this->ruangan_id);
				}
			}
			$criteria->compare('LOWER(pegawai_m.nama_pegawai)',strtolower($this->dokter_nama),true);
			$criteira->limit = -1;
			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
							'pagination'=>false,
			));
		}
         public function searchGrafikUnitPelayanan(){
               
             $criteria = new CDbCriteria();
             
                $criteria->select = 't.ruangan_nama as data ,t.ruangan_id, count(pendaftaran_t.statuspasien) as jumlah';
                $criteria->group = 't.ruangan_nama,t.ruangan_id, t.instalasi_id';
                $criteria->order='t.instalasi_id';
//                $criteria->select = 'count(pendaftaran_t.pendaftaran_id), t.ruangan_nama';
//                $criteria->group = 'pendaftaran_t.pendaftaran_id, t.ruangan_nama';
//                $criteria->order = 't.ruangan_nama';
                $criteria->join ='LEFT JOIN pendaftaran_t ON pendaftaran_t.ruangan_id = t.ruangan_id';
                
                $criteria->compare('t.instalasi_id',array(2,3,4));
				if(!empty($this->ruangan_id)){
					if(is_array($this->ruangan_id)){
						$criteria->addInCondition('t.ruangan_id',$this->ruangan_id);
					}else{
						$criteria->addCondition('t.ruangan_id = '.$this->ruangan_id);
					}
				}
//                $criteria->limit = -1;
                
//                $criteria->addCondition('pendaftaran_t.tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
//                $criteria->compare('propinsi_id',$this->propinsi_id);
//		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
//		$criteria->compare('kabupaten_id',$this->kabupaten_id);
//		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
//		$criteria->compare('kecamatan_id',$this->kecamatan_id);
//		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
//		$criteria->compare('kelurahan_id',$this->kelurahan_id);
//		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
//		$criteria->compare('instalasi_id',$this->instalasi_id);
//		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
//		$criteria->compare('carabayar_id',$this->carabayar_id);
//		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
//		$criteria->compare('penjamin_id',$this->penjamin_id);
//		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
//		$criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//		                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
//                        'pagination'=>false,
		));
        }
        public function getRuanganItems()
                {
                    return RuanganM::model()->findAll('ruangan_aktif=true ORDER BY ruangan_nama');
                }
                
                public function getNamaModel() 
       {
            return __CLASS__;
       }
       
       public function Criteria()
                {
                    $criteria = new CDbCriteria;
                    
                    return $criteria;
                }
                
        public function getKunjunganbaru()
        {
            $criteria=$this->Criteria();
            
            $criteria->select = 'count(pendaftaran_t.statuspasien) as jumlahkunjunganbaru, pendaftaran_t.statuspasien, t.ruangan_nama ';
            $criteria->group = 'pendaftaran_t.statuspasien, t.ruangan_nama, t.instalasi_id';
            $criteria->order='t.instalasi_id';
            $criteria->compare('t.instalasi_id',array(2,3,4));
            $criteria->join ='LEFT JOIN pendaftaran_t ON pendaftaran_t.ruangan_id = t.ruangan_id';
            $criteria->compare('pendaftaran_t.statuspasien',"PENGUNJUNG BARU");
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
         public function getKunjunganlama()
        {
            $criteria=$this->Criteria();
            
            $criteria->select = 'count(pendaftaran_t.statuspasien) as jumlahkunjunganbaru, pendaftaran_t.statuspasien, t.ruangan_nama ';
            $criteria->group = 'pendaftaran_t.statuspasien, t.ruangan_nama, t.instalasi_id';
            $criteria->order='t.instalasi_id';
            $criteria->compare('t.instalasi_id',array(2,3,4));
            $criteria->join ='LEFT JOIN pendaftaran_t ON pendaftaran_t.ruangan_id = t.ruangan_id';
            $criteria->compare('pendaftaran_t.statuspasien',"PENGUNJUNG LAMA");
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
		
}

?>
