<?php
/**
 * This is the model class for table "pasienmasukpenunjang_t".
 *
 * The followings are the available columns in table 'pasienmasukpenunjang_t':
 * @property integer $pasienmasukpenunjang_id
 * @property integer $pasien_id
 * @property integer $jeniskasuspenyakit_id
 * @property integer $pendaftaran_id
 * @property integer $pegawai_id
 * @property integer $kelaspelayanan_id
 * @property integer $ruangan_id
 * @property integer $pasienadmisi_id
 * @property string $no_masukpenunjang
 * @property string $tglmasukpenunjang
 * @property string $no_urutperiksa
 * @property string $kunjungan
 * @property string $statusperiksa
 * @property string $ruanganasal_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PPPasienMasukPenunjangT extends PasienmasukpenunjangT{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PasienmasukpenunjangT the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchPasienPenunjang()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('pegawai_id',$this->pegawai_id);
            $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
            $criteria->compare('LOWER(no_masukpenunjang)',strtolower($this->no_masukpenunjang),true);
//            $criteria->compare('LOWER(tglmasukpenunjang)',strtolower($this->tglmasukpenunjang),true);
            $criteria->compare('LOWER(no_urutperiksa)',strtolower($this->no_urutperiksa),true);
            $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
            $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
            $criteria->compare('LOWER(ruanganasal_id)',strtolower($this->ruanganasal_id),true);
            $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
            $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
            $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
            $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
            $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
            $criteria->addBetweenCondition('tglmasukpenunjang', $this->tglAwal, $this->tglAkhir);
            $criteria->order = 'tglmasukpenunjang DESC';
            

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
}
?>
