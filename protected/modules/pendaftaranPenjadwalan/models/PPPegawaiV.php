<?php
class PPPegawaiV extends PegawaiV
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    public function getNamaLengkap()
    {
         $dokter = $this->model()->findByAttributes(array('pegawai_id'=> $this->pegawai_id));
         return $dokter->gelardepan." ".$dokter->nama_pegawai.", ".$dokter->gelarbelakang_nama;
    }
    
    public function getGelarBelakangItems()
    {
        return GelarbelakangM::model()->findAll(array('order'=>'gelarbelakang_nama'));
    } 

    public function getSukuItems()
    {
        return SukuM::model()->findAll(array('order'=>'suku_nama'));
    }  

    public function getkelompokPegawaiItems()
    {
        return KelompokpegawaiM::model()->findAll(array('order'=>'kelompokpegawai_nama'));
    }  

    public function getPendidikanKualifikasiItems()
    {
        return PendidikankualifikasiM::model()->findAll(array('order'=>'pendkualifikasi_nama'));
    }  

    public function getJabatanItems()
    {
        return JabatanM::model()->findAll(array('order'=>'jabatan_nama'));
    } 

    public function getPendidikanItems()
    {
        return PendidikanM::model()->findAll(array('order'=>'pendidikan_nama'));
    }

     public function getPangkatItems()
    {
        return PangkatM::model()->findAll(array('order'=>'pangkat_nama'));
    } 

    public function getPropinsiItems()
    {
        return PropinsiM::model()->findAll(array('order'=>'propinsi_nama'));
    }
    
}