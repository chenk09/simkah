<?php

class PPPendaftaranT extends PendaftaranT
{
        
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PendaftaranT the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getNamaModel() 
    {
        return __CLASS__;
    }

    public function searchTable()
    {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();
        return new CActiveDataProvider($this,
            array(
                'criteria' => $criteria,
            )
        );
    }

    public function searchPrint()
    {
        $criteria = new CDbCriteria();
        $criteria = $this->functionCriteria();
        $criteria->limit = -1;
        //        $criteria->order = 'instalasi_nama, ruangan_nama';

        return new CActiveDataProvider($this,
            array(
                'criteria' => $criteria,
                'pagination'=>false,
            )
        );
    }

    protected function functionCriteria()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $criteria = new CDbCriteria();
        /*
        if($this->statuspasien = "PENGUNJUNG LAMA")
        {
            $criteria->select = 'count(pasien_id) as pasien_id, ruangan_id, count(statuspasien) as kunjunganlama';
            $criteria->group = 'ruangan_id';
        }else if($this->statuspasien = "PENGUNJUNG BARU")
        {
            $criteria->select = 'count(pasien_id) as pasien_id, ruangan_id, count(statuspasien) as kunjunganbaru';
            $criteria->group = 'ruangan_id';
        }
         * 
         */
        $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
        $criteria->compare('penjamin_id',$this->penjamin_id);
        $criteria->compare('caramasuk_id',$this->caramasuk_id);
        $criteria->compare('carabayar_id',$this->carabayar_id);
        $criteria->compare('pasien_id',$this->pasien_id);
        $criteria->compare('shift_id',$this->shift_id);
        $criteria->compare('golonganumur_id',$this->golonganumur_id);
        $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
        $criteria->compare('rujukan_id',$this->rujukan_id);
        $criteria->compare('penanggungjawab_id',$this->penanggungjawab_id);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
        $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
        $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
        $criteria->compare('no_urutantri',$this->no_urutantri);
        $criteria->compare('LOWER(transportasi)',strtolower($this->transportasi),true);
        $criteria->compare('LOWER(keadaanmasuk)',strtolower($this->keadaanmasuk),true);
        $criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
        $criteria->compare('LOWER(statuspasien)',strtolower($this->statuspasien),true);
        $criteria->compare('LOWER(kunjungan)',strtolower($this->kunjungan),true);
        $criteria->compare('alihstatus',$this->alihstatus);
        $criteria->compare('byphone',$this->byphone);
        $criteria->compare('kunjunganrumah',$this->kunjunganrumah);
        $criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
        $criteria->compare('LOWER(umur)',strtolower($this->umur),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('nopendaftaran_aktif',$this->nopendaftaran_aktif);
        $criteria->compare('status_konfirmasi',$this->status_konfirmasi);
        $criteria->compare('date(tgl_konfirmasi)',$this->tgl_konfirmasi);

        return $criteria;
    }

    public function Criteria()
    {
        $criteria = new CDbCriteria;
        return $criteria;
    }

    public function getKunjunganbaru()
    {
        $criteria=$this->Criteria();
        $criteria->select = 'COUNT(statuspasien)';
        $criteria->compare('statuspasien',"PENGUNJUNG BARU");
        return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
    }

    public function getKunjunganlama()
    {
        $criteria=$this->Criteria();
        $criteria->select = 'COUNT(statuspasien)';
        $criteria->compare('statuspasien',"PENGUNJUNG LAMA");
        return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
    }

    public function searchGrafikUnitPelayanan()
    {


        $criteria->select = 'count(t.pasien_id) as jumlah, ruangan_m.ruangan_nama as data';
        $criteria->group = 't.pasien_id, ruangan_m.ruangan_nama';
        $criteria->order = 'ruangan_m.ruangan_nama';
        $criteria->join ='LEFT JOIN ruangan_m ON ruangan_m.ruangan_id = t.ruangan_id';

//            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
//            $criteria->compare('propinsi_id',$this->propinsi_id);
//            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
//            $criteria->compare('kabupaten_id',$this->kabupaten_id);
//            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
//            $criteria->compare('kecamatan_id',$this->kecamatan_id);
//            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
//            $criteria->compare('kelurahan_id',$this->kelurahan_id);
//            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
//            $criteria->compare('instalasi_id',$this->instalasi_id);
//            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
//            $criteria->compare('carabayar_id',$this->carabayar_id);
//            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
//            $criteria->compare('penjamin_id',$this->penjamin_id);
//            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
//            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);
//
        return new CActiveDataProvider($this,
            array(
                'criteria'=>$criteria,
            )
        );
    }

    public function searchUnitPelayanan()
    {
        $criteria->select = 'count(t.pendaftaran_id), ruangna_m.ruangan_nama';
        $criteria->group = 't.pendaftaran_id, ruangan_m.ruangan_nama';
        $criteria->order = 'ruangan_m.ruangan_nama';
        $criteria->join ='LEFT JOIN t ON ruangan_m.ruangan_id = t.ruangan_id';

//            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' and \''.$this->tglAkhir.'\'');
//            $criteria->compare('propinsi_id',$this->propinsi_id);
//            $criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
//            $criteria->compare('kabupaten_id',$this->kabupaten_id);
//            $criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
//            $criteria->compare('kecamatan_id',$this->kecamatan_id);
//            $criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
//            $criteria->compare('kelurahan_id',$this->kelurahan_id);
//            $criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
//            $criteria->compare('instalasi_id',$this->instalasi_id);
//            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
//            $criteria->compare('carabayar_id',$this->carabayar_id);
//            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
//            $criteria->compare('penjamin_id',$this->penjamin_id);
//            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
//            $criteria->compare('LOWER(status_konfirmasi)',strtolower($this->status_konfirmasi),true);

        return new CActiveDataProvider($this,
            array(
                'criteria'=>$criteria,
            )
        );
    }

    public function searchKontrolPasien()
    {
        $criteria = new CDbCriteria();
        $criteria->with = array('pasien');
        $criteria->addBetweenCondition('tgl_pendaftaran', $this->tglAwal, $this->tglAkhir,true);
        $criteria->compare('LOWER(pasien.no_rekam_medik)',  strtolower($this->no_rekam_medik),true);
        $criteria->compare('LOWER(pasien.nama_pasien)',  strtolower($this->nama_pasien),true);
        $criteria->compare('LOWER(pasien.alamat_pasien)',  strtolower($this->alamat_pasien),true);
        if(!empty($_GET['PPPendaftaranT']['ruangna_id'])){
                $criteria->compare('t.ruangan_id', $this->ruangan_id);
        }
         $criteria->compare('t.instalasi_id', $this->instalasi_id);
        /*
        $criteria->compare('instalasi_id', $this->instalasi_id);
         * 
         */

        return new CActiveDataProvider($this,
            array(
                'criteria'=>$criteria,
            )
        );
    }
}

?>