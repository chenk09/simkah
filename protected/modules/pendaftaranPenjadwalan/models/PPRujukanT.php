<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class PPRujukanT extends RujukanT
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KelompokmenuK the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('no_rujukan,asalrujukan_id', 'required'),
			array('no_rujukan', 'length', 'max'=>10),
			array('nama_perujuk', 'length', 'max'=>50),
			array('tanggal_rujukan, rujukandari_id, diagnosa_rujukan, aktif_rujukan', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('rujukan_id, rujukandari_id, asalrujukan_id, no_rujukan, nama_perujuk, tanggal_rujukan, diagnosa_rujukan, aktif_rujukan', 'safe', 'on'=>'search'),
		);
	}
}
?>
