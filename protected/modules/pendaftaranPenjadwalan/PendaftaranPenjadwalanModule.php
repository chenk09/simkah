<?php

class PendaftaranPenjadwalanModule extends CWebModule
{
        public $defaultController = 'pendaftaran';
    
        public $kelompokMenu = array();
        public $menu = array();
        
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(
                    array(
			'pendaftaranPenjadwalan.models.*',
			'pendaftaranPenjadwalan.components.*',
                        'rawatDarurat.models.*',
                        'rawatDarurat.views.*',
                        'rekamMedis.models.*',
                        'rekamMedis.controllers.*',
                        'rekamMedis.views.*',
                    )
                );
                
                if(!empty($_REQUEST['modulId']))
                    Yii::app()->session['modulId'] = $_REQUEST['modulId']; 
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
                    
                        $this->kelompokMenu = KelompokmenuK::model()->findAllAktif();
                        $this->menu = MenumodulK::model()->findAllAktif(array('modulk.modul_id'=>Yii::app()->session['modulId']));
			return true;
		}
		else
			return false;
	}
}
