<fieldset id="fieldsetPenanggungjawab">
    <legend class="accord1">
        <?php echo CHtml::checkBox('adaPenanggungJawab', $model->adaPenanggungJawab, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Data Penanggungjawab Pasien
    </legend>
    <div id="divPenanggungJawab" class="control-group toggle <?php echo ($model->adaPenanggungJawab) ? '':'hide'; ?>">
        <?php //echo $form->textFieldRow($model,'penanggungjawab_id', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

        <?php echo $form->dropDownListRow($modPenanggungJawab,'pengantar', Pengantar::items(), array('class'=>'reqPj','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'setPengantar();')); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'nama_pj', array('class'=>'reqPj','onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Penanggung Jawab','onchange'=>'convertToUpper(this)', 'onkeyup'=>'convertToUpper(this)')); ?>
        <?php echo $form->radioButtonListInlineRow($modPenanggungJawab,'jeniskelamin', JenisKelamin::items(), array('class'=>'reqPj','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php //echo $form->dropDownListRow($modPenanggungJawab,'jenisidentitas', JenisIdentitas::items(), array('empty'=>'-- Pilih --','class'=>'span1','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
            <?php echo $form->labelEx($modPenanggungJawab,'jenisidentitas', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php echo $form->dropDownList($modPenanggungJawab,'jenisidentitas', JenisIdentitas::items(), array('empty'=>'-- Pilih --','class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textField($modPenanggungJawab,'no_identitas', array('placeholder'=>$modPenanggungJawab->getAttributeLabel('no_identitas'),'class'=>'span2','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->error($modPenanggungJawab, 'no_identitas'); ?>
            </div>
        </div>
        <?php //echo $form->textFieldRow($modPenanggungJawab,'no_identitas', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->dropDownListRow($modPenanggungJawab,'hubungankeluarga', HubunganKeluarga::items(), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'tempatlahir_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Tempat Lahir Penanggung Jawab')); ?>
        <?php //echo $form->textFieldRow($modPenanggungJawab,'tgllahir_pj', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
            <?php echo $form->labelEx($modPenanggungJawab,'tgllahir_pj', array('class'=>'control-label')) ?>
            <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPenanggungJawab,
                                                    'attribute'=>'tgllahir_pj',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        //
                                                        'onkeypress'=>"js:function(){getUmurpj(this);}",
                                                        'onSelect'=>'js:function(){getUmurpj(this);}',
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3 reqPasien', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                <?php echo $form->error($modPenanggungJawab, 'tgllahir_pj'); ?>
            </div>
        </div>
        <div class="control-group ">
             <?php echo $form->labelEx($modPenanggungJawab,'umur', array('class'=>'control-label')) ?>
              <div class="controls">
               <?php
               $this->widget('CMaskedTextField', array(
               'model' => $modPenanggungJawab,
               'attribute' => 'umur_pj',
               'mask' => '99 Thn 99 Bln 99 Hr',
               'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahirpj(this)', 'onchange'=>'setNamaGelar()', 'class'=>'reqPasien')
                ));
                ?>
                <?php echo $form->error($modPenanggungJawab, 'umur_pj'); ?>
                </div>
               </div>
        <?php echo $form->textAreaRow($modPenanggungJawab,'alamat_pj', array('onchange'=>'convertToUpper(this)', 'onkeyup'=>'convertToUpper(this)','onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat Penanggung Jawab')); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'no_teleponpj', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Telepon Yang Dapat Dihubungi','class'=>'numbersOnly',)); ?>
        <?php echo $form->textFieldRow($modPenanggungJawab,'no_mobilepj', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'numbersOnly','placeholder'=>'No. Hp Yang Dapat Dihubungi')); ?>
        <?php //echo $form->checkBoxRow($modPenanggungJawab,'penanggungjawab_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </div>
</fieldset>

<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$idTagUmur = CHtml::activeId($modPenanggungJawab,'umur_pj');
$enableInputPJ = ($model->adaPenanggungJawab) ? 1 : 0;
$js = <<< JS
if(${enableInputPJ}) {
    $('#divPenanggungJawab input').removeAttr('disabled');
    $('#divPenanggungJawab select').removeAttr('disabled');
}
else {
    $('#divPenanggungJawab input').attr('disabled','true');
    $('#divPenanggungJawab select').attr('disabled','true');
}

$('#adaPenanggungJawab').change(function(){
    if ($(this).is(':checked')){
            $('#divPenanggungJawab input').removeAttr('disabled');
            $('#divPenanggungJawab select').removeAttr('disabled');
    }else{
            $('#divPenanggungJawab input').attr('disabled','true');
            $('#divPenanggungJawab select').attr('disabled','true');
    }
    $('#divPenanggungJawab').slideToggle(500);
});

function getTglLahirpj(obj)
{
    var str = obj.value;
    obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{umur: obj.value},
        function(data){
           $('#PPPenanggungJawabM_tgllahir_pj').val(data.tglLahir); 
    },"json");
}

function getUmurpj(obj)
{
    //alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){

           $('#PPPenanggungJawabM_umur_pj').val(data.umur); 

           $("#${idTagUmur}").val(data.umur);
    },"json");
}


JS;
Yii::app()->clientScript->registerScript('penanggungJawab',$js,CClientScript::POS_HEAD);
?>
<script>
function setPengantar(){
    var pengantar = $('#PPPenanggungJawabM_pengantar').val();
    var norekammedik = $('#noRekamMedik').val();
    if(pengantar == "DIRI SENDIRI"){
        var nama = $('#PPPasienM_nama_pasien').val();
        var jk =  $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        $('#PPPenanggungJawabM_nama_pj').val(nama);
        $('input[name="PPPenanggungJawabM[jeniskelamin]"]').each(function(){
            if(this.value == jk)
                $(this).attr('checked',true);
             }
        );
    }else{
        $.post('<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/ActionAjax/getPengantar'); ?>',{pengantar: pengantar, norekammedik:norekammedik},function(data){
            if(data.status != "kosong"){
                $('#PPPenanggungJawabM_nama_pj').val(data.nama);
                $('input[name="PPPenanggungJawabM[jeniskelamin]"]').each(function(){
                    if(this.value == data.jk)
                        $(this).attr('checked',true);
                     }
                ); 
            }else{
                $('#PPPenanggungJawabM_nama_pj').val('');
                $('input[name="PPPenanggungJawabM[jeniskelamin]"]').each(function(){
                    if(this.value == data.jk)
                        $(this).attr('checked',false);
                     }
                ); 
            }
        },"json");
    }
}
</script>
