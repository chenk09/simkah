<span class="required"><i>Bagian dengan tanda * harus diisi.</i></span>
<p>
<br/>1. Disamping label Data Pasien terdapat icon <input type="checkbox" />, icon tersebut bisa di check apabila form data pasien diperlukan dalam untuk merubah data pasien.
<br/>2. Gunakan Enter untuk pindah ke textfield selanjutnya agar mempermudah dan memempercepat pengunaan aplikasi.
<br/>3. Icon <i class="icon-list-alt icon-search"></i><i class="icon-search"></i> berfungsi untuk menampilkan list dialog data pasien.
<br/>4. Cek icon ini <i class="icon-user"></i> jika pasien merupakan pasien lama atau sudah memiliki nomor rekam medis atau Ketikkan pada textfield No Rekam Medis pasien tsb
<br/>5. Icon <i class="icon-calendar"></i><i class="icon-time"></i> berfungsi untuk menentukan tanggal dan waktu pendaftaran.
<br/>6. Icon <button id="btnAddKelurahan" class="btn btn-primary" type="button">
<i class="icon-plus-sign icon-white"></i>
</button> berfungsi untuk menambahkan jika tidak ada data di dalam nya.
<br/>7. Memiliki 4 Form tersembunyi, jika di perlukan untuk pengisisan data tersebut silahkan checklist icon maka akan muncul form yang tersedia. 
<br/>8. Cek icon ini <i class="icon-home"></i> jika pasien yang di daftarkan akan di lakukan pemeriksaan di rumah pasien
<br/>9. Gunakan tombol ini <a class="btn btn-danger"><i class="icon-refresh icon-white"></i> Ulang </a> untuk mendaftarkan pasien selanjutnya.

</p>