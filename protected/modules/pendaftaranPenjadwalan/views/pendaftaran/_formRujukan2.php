<fieldset class="">
    <legend class="accord1">
        <?php echo CHtml::checkBox('isRujukan', $model->isRujukan, array('onkeypress'=>"return $(this).focusNextInputField(event)")); //, 'onclick'=>'return false;' ?>
        Rujukan
    </legend>
    <div id="divRujukan" class="toggle <?php echo ($model->isRujukan) ? '':'hide' ?> toggle" >
        <div class="control-group ">
                    <?php echo $form->labelEx($modRujukan,'asalrujukan_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php echo $form->dropDownList($modRujukan,'asalrujukan_id', CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'), 
                                                      array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modRujukan, 'nama_perujuk')."')", 
                                                            'ajax'=>array('type'=>'POST',
                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetRujukanDari',array('encode'=>false,'namaModel'=>'PPRujukanT')),
                                                                          'update'=>'#PPRujukanT_rujukandari_id',),
                                                            'onchange'=>"clearRujukan();",)); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addAsalRujukan(); $('#dialogAddAsalRujukan').dialog('open');}",
                                                              'id'=>'btnAddAsalRujukan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modRujukan->getAttributeLabel('asalrujukan_id'))) ?>
                        <?php echo $form->error($modRujukan, 'asalrujukan_id'); ?>
                    </div>
                </div>
        <?php //echo $form->dropDownListRow($modRujukan,'asalrujukan_id', CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modRujukan,'no_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Rujukan')); ?>
       <?php //echo $form->dropDownListRow($modRujukan,'nama_perujuk', CHtml::listData(RujukandariM::model()->findAll(), 'rujukandari_id', 'namaperujuk'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
       <div class="control-group ">
            <?php echo $form->labelEx($modRujukan,'nama_perujuk', array('class'=>'control-label')); ?>
            <div class="controls">
                <?php echo $form->dropDownList($modRujukan,'rujukandari_id',CHtml::listData($modRujukan->getDaftarRujukanItems($modRujukan->asalrujukan_id), 'rujukandari_id', 'namaperujuk'),
                                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modRujukan, 'asalrujukan_id')."')")); ?>
                <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                array('class'=>'btn btn-primary','onclick'=>"{addNamaRujukan(); $('#dialogAddNamaRujukan').dialog('open');}",
                                                      'id'=>'btnAddNamaRujukan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modRujukan->getAttributeLabel('nama_perujuk'))) ?>
                <?php echo $form->error($modRujukan, 'nama_perujuk'); ?>
            </div>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($modRujukan,'tanggal_rujukan', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modRujukan,
                                        'attribute'=>'tanggal_rujukan',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($modRujukan, 'tanggal_rujukan'); ?>
            </div>
        </div>
        <?php echo $form->textFieldRow($modRujukan,'diagnosa_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Diagnosa Rujukan')); ?> 
    </div>
</fieldset>

<?php

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddAsalRujukan',
    'options'=>array(
        'title'=>'Menambah data Asal Rujukan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormAsalRujukan"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddNamaRujukan',
    'options'=>array(
        'title'=>'Menambah data Nama Rujukan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormNamaRujukan"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

?>

<?php
$enableInputPJ = ($model->isRujukan) ? 1 : 0;
$js = <<< JS
if(${enableInputPJ}) {
    $('#divRujukan input').removeAttr('disabled');
    $('#divRujukan select').removeAttr('disabled');
}
else {
    $('#divRujukan input').attr('disabled','true');
    $('#divRujukan select').attr('disabled','true');
}

$('#isRujukan').change(function(){
    if ($(this).is(':checked')){
            $('#divRujukan input').removeAttr('disabled');
            $('#divRujukan select').removeAttr('disabled');
    }else{
            $('#divRujukan input').attr('disabled','true');
            $('#divRujukan select').attr('disabled','true');
    }
    $('#divRujukan').slideToggle(500);
});

function clearRujukan()
{
    $('#PPRujukanT_nama_perujuk').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('rujukan',$js,CClientScript::POS_READY);
?>

<script>

    
function addNamaRujukan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addNamaRujukan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan').html(data.div);
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan form').submit(addNamaRujukan);
                }
                else
                {
                    $('#dialogAddNamaRujukan div.divForFormNamaRujukan').html(data.div);
                    $('#PPRujukanT_nama_perujuk').html(data.namarujukan);
                    setTimeout(\"$('#dialogAddNamaRujukan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addAsalRujukan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addAsalRujukan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan').html(data.div);
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan form').submit(addAsalRujukan);
                }
                else
                {
                    $('#dialogAddAsalRujukan div.divForFormAsalRujukan').html(data.div);
                    $('#PPRujukanT_asalrujukan_id').html(data.asalrujukan);
                    setTimeout(\"$('#dialogAddAsalRujukan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}
</script>