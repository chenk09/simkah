<?php
    $random=rand(0000000000000000, 9999999999999999);
    $pathPhotoPasienTumbs=Params::pathPasienTumbsDirectory();
    $pathPhotoPasien=Params::pathPasienDirectory();
    $base_dir = Yii::app()->baseUrl . '/protected/extensions/jpegcam';
    $assets_url = $base_dir.DIRECTORY_SEPARATOR.'assets/';
?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/protected/extensions/jpegcam/assets/webcam.js'); ?>

<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-rj-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $modLokasi = LokasiantrianM::model()->findAll('lokasiantrian_aktif IS TRUE AND lokasiantrian_id IN (1,2)');
        
        $modLoket = LoketM::model()->findAllByAttributes(array('loket_aktif'=>true));
        $loket = (isset($_POST['loket'])) ? $_POST['loket'] : '';
        
        echo CHtml::dropDownList('lokasiantrian', $loket, CHtml::listData($modLokasi, 'lokasiantrian_id', 'lokasiantrian_nama'), 
                array('empty'=>'-- Pilih Lokasi --','class'=>'span3',
                    'onchange'=>"listLoket(this.value);listJenisAntrian(this.value);",
                    ));
        echo CHtml::dropDownList('loket', $loket, array(), 
                array('empty'=>'-- Pilih Loket --','class'=>'span2','onchange'=>"$('#dialogAntrian').dialog('close');"));
        echo CHtml::dropDownList('jenis_antrian', $loket, array(), 
                array('empty'=>'-- Pilih Antrian --','class'=>'span2','onchange'=>"$('#dialogAntrian').dialog('close');"));
        
        echo CHtml::link(Yii::t('mds', 'Antrian {icon}', array('{icon}'=>'<i class="icon-chevron-right icon-white"></i>')),
                '#', array('class'=>'btn btn-info','onclick'=>"cekAntrian();return false;")); 
        echo $form->hiddenField($modAntrian,'antrianpasien_id_temp',array('readonly'=>true));
    }
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>  
        <legend class="rim2">Pendaftaran Pasien Rawat Jalan</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
	<?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modTindakanPelayan)); ?>
        <table class="table-condensed">
            <tr>
                <td width="33%">
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
						<div class="label_no">
                         <i class="icon-user"></i>   <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            No Rekam Medik					  </div>
                        </label>
                        <div class="controls">
                          <?php 
                                    $enable['readonly'] = true;
                                    $readOnly = ($model->isPasienLama) ? '' : $enable ; 
                            ?>
                          <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedik',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedik").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                                $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                                $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                                $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                                $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                                $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                                $("#'.CHtml::activeId($modPasien,'nama_ayah').'").val(ui.item.nama_ayah);
                                                $("#'.CHtml::activeId($modPasien,'nama_ibu').'").val(ui.item.nama_ibu);
                                                $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                                $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                                setJenisKelaminPasien(ui.item.jeniskelamin);
                                                setRhesusPasien(ui.item.rhesus);
                                                loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, ui.item.kelurahan_id);
                                                getUmur(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                                $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                                $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                                $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                                $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                                $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                                $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                                $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                                $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                                $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                                $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                                $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                                $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                                $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                                $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                                $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                                $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                                $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                                $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                                loadUmur(ui.item.tanggal_lahir);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik','onblur'=>'cariDataPasien(this.value);','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
//                                        'tombolDialog'=>array('idModal'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                            )); ?>
                            
                            <?php echo $form->error($model, 'noRekamMedik'); ?>                        
                        </div>
                    </div>                
                </td>
                <td width="33%" rowspan="2">
                    <div class="label_no">
                        <?php $minDate = (Yii::app()->user->getState('loginpemakai_id')) ? '' : 'd'; ?>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        'minDate' => $minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
							
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>                        
                        </div>
                    </div>                
                </td>
                <td width="33%" rowspan="2">
                        <?php echo CHtml::htmlButton('Ambil Photo', 
                                    array('class'=>'btn btn-primary','onclick'=>"{addPhoto(); $('#dialogAddPhoto').dialog('open');}",
                                          'id'=>'btnAddPhoto','onkeypress'=>"return $(this).focusNextInputField(event)",
                                          'rel'=>'tooltip','title'=>'Klik untuk Ambil Photo')) ?>
                        <br>&nbsp;&nbsp;&nbsp;&nbsp;<img id="cekFoto" src="<? echo 'http://localhost/ehospitaljk/data/images/pasien/no_photo.jpeg'; ?>"width="57px" height="85px"/>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group" id="controlNoRekamMedikLama">
                        <label class="control-label">
                        <div class="label_no">
                         <i class="icon-user"></i>   
                            <?php echo CHtml::checkBox('isPasienBaru','', array('rel'=>'tooltip',
                                    'title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 
                                        'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                           Gunakan No. RM yg tidak dipakai
                        </div>
                        </label>
                        <div class="controls">
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedikLama',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienNonAktif'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedikLama").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik',
                                                             'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                             'class'=>'span3 numbersOnly'),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasienLama','idTombol'=>'tombolPasienDialogBaru'),
//                                        'tombolModal'=>array('idModal'=>'dialogPasienLama','idTombol'=>'tombolPasienDialogBaru'),
										
                            )); ?>
                           
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <?php echo $this->renderPartial('_formPasienRJRD', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien,'format'=>$format)); ?>  
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <fieldset id="fieldsetKunjungan">
                        <legend class="rim">Data Kunjungan</legend>
                    <table>
                        <tr>
                            <td width="50%">
                                <div class='control-group'>
                                <div class="control-label">
                                    <?php echo CHtml::label('No. Antri Poliklinik','',array('class'=>'control-label')); ?>
                                    
                                </div>
                                    <div class='controls'>
                                            <?php echo $form->textField($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:50px;')); ?>
                                            Mak. Antrian <?php echo CHtml::textField('maxAntrianRuangan','', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:50px;','value'=>0)); ?>
                                            
                                    </div>
                                </div>

                                <table>
                                    <tr>
                                        <td width="143px">
                                            <div align="right">
                                                <i onclick="getListDokterPoli(<?php echo Params::INSTALASI_ID_RJ; ?>);" class="icon-file" style='cursor:pointer;'></i>
                                                    <?php echo $form->Label($model,'ruangan_id');?>                                            
                                            </div>                                        
                                        </td>
                                        <td><?php echo $form->dropDownList($model,'ruangan_id', CHtml::listData($model->getRuanganItems(Params::INSTALASI_ID_RJ), 'ruangan_id', 'ruangan_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'ajax'=>array('type'=>'POST',
                                                                                      'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranRj')),
                                                                                      'update'=>'#PPPendaftaranRj_jeniskasuspenyakit_id'),
                                                                        'onchange'=>"listDokterRuangan(this.value);listKarcis(this.value);listAntrianRuangan(this.value)",
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 reqKunjungan',
                                                                        'onfocus'=>'return cekFieldNoRm();')); ?></td>
                                        <td> <div class="checkbox inline" ><i class="icon-home" style="margin:0" ></i>
                                                       <?php echo $form->checkBox($model,'kunjunganrumah', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                                     <?php echo CHtml::activeLabel($model, 'kunjunganrumah'); ?>
                                                    </div>
                                                    <div class="checkbox inline"></div>                                       
                                        </td>
                                    </tr>
                                </table>

                                <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                        'class'=>'reqKunjungan')); ?>

                                
                                <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>"listKarcis(this.value)", 'class'=>'reqKunjungan')); ?>
                                <div class="control-group">
                                    <div class="control-label">
                                        <?php echo CHtml::label('Dokter <font color=red>*</font>','',array('class'=>'control-label')); ?>
                                    </div>
                                    <div class="controls">
                                        <?php echo $form->dropDownList($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('onchange'=>'listAntrianDokter(this.value);','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'reqKunjungan')); ?>
                                        Mak. Antrian <?php echo CHtml::textField('maxAntrianDokter','', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:50px;','value'=>0)); ?>
                                    </div>
                                </div>
                                <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type'=>'POST',
                                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPPendaftaranRj')), 
                                                                'update'=>'#PPPendaftaranRj_penjamin_id'  //selector to update
                                                            ),
                                                            'onchange'=>'caraBayarChange(this)',
                                                            'class'=>'reqKunjungan',
                                    )); ?>

                                <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'reqKunjungan')); ?>                          
                                <?php
                                //FORM REKENING
                                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                                        array(
                                            'form'=>$form,
                                            'modRekenings'=>$modRekenings,
                                        )
                                    );
                                ?>
                            </td>
                          <td width="50%">
                                <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan)); ?>

                                <?php 
                                if(Yii::app()->user->getState('is_bridging')==TRUE){
                                    echo $this->renderPartial('_formAsuransiSep', array('model'=>$model,'form'=>$form,'modSep'=>$modSep));
                                }else{
                                    echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form));
                                }
                                ?>

                                <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>


                                <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Poliklinik                                    
                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>                          
                          </td>
                        </tr>
                    </table>
                    </fieldset>                
                </td>
            </tr>
        </table>
        
    </fieldset>

    <?php echo $form->hiddenField($modPasien,'tempPhoto',array('readonly'=>TRUE,'value'=>'', 'id'=>'vartempPhoto'));?>
	<div class="form-actions">
		<?php 
                    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;')); 
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pendaftaran/rawatJalan',array('modulId'=> Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
                 <?php if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        {
                ?>
                        <script>
                            // print(<?php echo $model->pendaftaran_id.','.$model->pasien_id ?>);
                        </script>
                <?php
                        echo CHtml::link(Yii::t('mds', '{icon} Print Gelang', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status RM', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_status('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status V.2', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_dua('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Kartu Pasien', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKartuPasien('$model->pasien_id');return false",'disabled'=>FALSE  ));
                        
                        if(isset($modSep->sep_id) && !empty($modSep->sep_id)){
                            echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printSep('$modSep->sep_id');return false",'disabled'=>FALSE  ));
                        }else{
                            if($model->carabayar_id==18 && Yii::app()->user->getState('is_bridging')==TRUE){
                                echo CHtml::link(Yii::t('mds', '{icon} Proses SEP', array('{icon}' => '<i class="icon-ok icon-white"></i>')), $this->createUrl("SettingBpjs/prosesSEP",array("pendaftaran_id"=>$model->pendaftaran_id,"pasien_id"=>$modPasien->pasien_id,"jenispelayanan"=>2,"frame"=>true,'jnspelayanan'=>"RJ")), array(
                                    'rel' => 'tooltip', 'title' => 'Klik untuk Proses SEP!', 'class' => 'btn btn-info',
                                    "class"=>"btn btn-primary", "onclick"=>"$('#dialog-proses-sep').dialog('open');","target"=>"iframeProsesSEP"))."&nbsp;";
                            }
                            
                        }
                        
                       }else{
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  ));

                        echo CHtml::link(Yii::t('mds', '{icon} Print Kartu Pasien', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  ));
                       } 
                ?>
<?php $this->endWidget(); ?>

<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
		
	</div>
<div>
    <legend class="rim"> 10 Pasien yang terakhir di Daftarkan </legend>
    <div>
        <?php  echo CHtml::link(Yii::t('mds', '{icon} Refresh', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), '#', array('class'=>'btn btn-danger','onclick'=>"refreshList();",'disabled'=>FALSE  )); ?>
                 
    </div>
<?php 
    $modListPendaftaran = new PPPendaftaranRj();
    $tglAwal = date('Y-m-d 00:00:00');
    $tglAkhir = date('Y-m-d 23:59:59');
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
            'id'=>'pendaftarterakhir-rj-grid',
            'dataProvider'=>$modListPendaftaran->searchListPendaftaran($tglAwal,$tglAkhir),
            'template'=>"{pager}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header'=>'Tgl <br/> Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->tgl_pendaftaran',
                ),
                array(
                    'header'=>'No <br/>Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header'=>'No RM',
                    'type'=>'raw',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'$data->pasien->nama_pasien',
                ),
                array(
                    'header'=>'Tempat <br/> Lahir',
                    'type'=>'raw',
                    'value'=>'$data->pasien->tempat_lahir',
                ),
                array(
                    'header'=>'Tgl Lahir',
                    'type'=>'raw',
                    'value'=>'$data->pasien->tanggal_lahir',
                ),
                array(
                    'header'=>'Umur',
                    'type'=>'raw',
                    'value'=>'$data->umur',
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'type'=>'raw',
                    'value'=>'$data->pasien->jeniskelamin',
                ),
                array(
                    'header'=>'Alamat Pasien',
                    'type'=>'raw',
                    'value'=>'$data->pasien->alamat_pasien',
                ),
                array(
                    'header'=>'No Telp',
                    'type'=>'raw',
                    'value'=>'$dta->pasien->no_telepon_pasien',
                ),
                array(
                    'header'=>'Ruangan Tujuan',
                    'type'=>'raw',
                    'value'=>'$data->ruangan->ruangan_nama',
                ),
                array(
                    'header'=>'Dokter',
                    'type'=>'raw',
                    'value'=>'$data->pegawai->nama_pegawai',
                ),
                array(
                    'header'=>'Cara Bayar',
                    'type'=>'raw',
                    'value'=>'$data->carabayar->carabayar_nama',
                ),
                array(
                    'header'=>'Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->penjamin->penjamin_nama',
                ),
            ),
    )); 
?>
</div>

<script language="JavaScript">
    
    
    function take_snapshot() {
        document.getElementById('upload_results').innerHTML = '<h3>Proses Penyimpanan...</h3>';
        webcam.snap();
    }

    function do_upload() {
        
        webcam.upload();
    }

    function my_completion_handler(msg) {
                                  if (msg == 'OK') 
                           {
                                document.getElementById('upload_results').innerHTML = '<h3>OK! ...Photo Sedang Disimpan</h3>';
                                //webcam.reset();
                                setTimeout(function(){
                                document.getElementById('upload_results').innerHTML = '<h3>Photo Berhasil Disimpan</h3>';
                                },3000);
                                document.getElementById('vartempPhoto').value = "<?= $random ?>.jpg";
                                $('#dialogAddPhoto').dialog('close');
                                $('#cekFoto').attr('src','<?= "http://localhost/ehospitaljk/data/images/pasien/".$random ?>.jpg');
                            }
                         else
                            {
                                alert("PHP Error: " + msg);
                            }
    }
</script>
<script type="text/javascript">
// $('#<?php echo CHtml::activeId($model, 'ruangan_id'); ?>').val(15);
$('#PPPendaftaranRj_ruangan_id').val();
jQuery.ajax({'type':'POST',
             'url':'?r=ActionDynamic/GetKasusPenyakit&encode=&namaModel=<?php echo get_class($model); ?>',
             'cache':false,
             'data':jQuery("#<?php echo CHtml::activeId($model, 'jeniskasuspenyakit_id'); ?>").parents("form").serialize(),
             'success':function(html){jQuery("#<?php echo CHtml::activeId($model, 'jeniskasuspenyakit_id'); ?>").html(html)}
         });
listKarcis(15);    

if($('#isPasienLama').is(':checked')){
    $('#tombolPasienDialog').removeClass('hide');
}else{
    $('#tombolPasienDialog').addClass('hide');
}

function addPhoto()
{
    <?php
        echo CHtml::ajax(array(
            'url'=>'',
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddPhoto div.divForForm').html(data.div);
                    $('#dialogAddPhoto div.divForForm form').submit(addKabupaten);
                }
                else
                {
                    $('#dialogAddPhoto div.divForForm').html(data.div);
                    setTimeout(\"$('#dialogAddPhoto').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}
</script>

<?php
Yii::app()->clientScript->registerScript('status_masuk',"
    $(document).ready(function(){
        function photo(){
            webcam.set_api_url( 'index.php?r=photoWebCam/jpegcam.saveJpg&random=${random}&pathTumbs=${pathPhotoPasienTumbs}&path=${pathPhotoPasien}' );
            webcam.set_quality( 90 );
            webcam.set_shutter_sound( false );
            webcam.set_stealth( 1 );
            webcam.set_swf_url('${assets_url}webcam.swf');
            $('#cam_xx').append(webcam.get_html(303, 320));
            webcam.set_hook( 'onComplete', 'my_completion_handler' );
        }
        photo();
//        $(':input').keypress(function(e) {
//            if(e.keyCode == 13) {
//                $(this).focusNextInputField(); 
//            } 
//        });
    });
    "); ?>

<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id . '/' .$controller;
$urlKartu =  Yii::app()->createAbsoluteUrl($module);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>''));
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPasien'=>''));
$urlPrintSep = Yii::app()->createUrl(Yii::app()->controller->module->id.'/settingBpjs/PrintSep',array('sep_id'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$urlListAntrianRuangan =Yii::app()->createUrl('actionAjax/listAntrianRuangan');
$urlListAntrianDokter =Yii::app()->createUrl('actionAjax/listAntrianDokter');
$urlListLoket = Yii::app()->createUrl('actionDynamic/listLoket');
$urlListJenisAntrian = Yii::app()->createUrl('actionDynamic/listJenisAntrian');

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKunjunganPasien=PPKonfigSystemK::model()->find()->printkunjunganlsng;
if(!empty($cekKunjunganPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKunjunganPasien=$cekKunjunganPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKunjunganPasien=0;
}

//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKartuPasien=PPKonfigSystemK::model()->find()->printkartulsng;
if(!empty($cekKartuPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKartuPasien=$cekKartuPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKartuPasien=0;
}
//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= Awal Tambah Photo
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddPhoto',
    'options'=>array(
        'title'=>'Ambil Photo',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>340,
        'minHeight'=>350,
        'resizable'=>false,
    ),
));
?>
<div id="divCaraAmbilPhotoWebCam">
    <div id="cam_xx"></div>
    <br>
    <input type="button" onclick="webcam.configure()" value="Konfigurasi" class="buttonWebcam" style="font-size:10px; width:72px; height:24px;">
    <input type="button" onclick="webcam.freeze()" value="Ambil Photo" class="buttonWebcam" style="font-size:10px; width:72px; height:24px;">
    <input type="button" onclick="webcam.reset()" value="Ulang" class="buttonWebcam" style="font-size:10px; width:72px; height:24px;">
    <input type="button" onclick="take_snapshot()" value="Simpan" class="buttonWebcam" style="font-size:10px; width:72px; height:24px;">
    <div id="upload_results" style="background-color:#eee; margin-top:10px"></div>
</div>

<?php
$this->endWidget();
//================= Akhir Tambah Photo

//================= Awal Ngecek Status Pasien
$statusPasien=$model->statuspasien;

$statusPasienBaru=Params::statusPasien('baru');

//================= Akhir Ngecek Status Pasien
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;
$jscript = <<< JS


function print(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=940,height=400');
        }     
        
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");

        }
}

function print_dua(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran + '&status=2','printwin','left=100,top=100,width=940,height=400');
        }
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");
        }
}
            
function print_status(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran + '&status=3','printwin','left=100,top=100,width=940,height=400');
        }
}

function printKartuPasien(idPasien)
{       
        //if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");

        //}
}
            
function printSep(sep_id)
{       
        window.open('${urlPrintSep}'+sep_id,'printwin','left=100,top=100,width=860,height=480');

}

function listKarcis(obj)
{
     kelasPelayanan=$('#PPPendaftaranRj_kelaspelayanan_id').val();
     ruangan=$('#PPPendaftaranRj_ruangan_id').val();
     if($('#isPasienLama').is(':checked'))
        pasienLama = 1;
     else
        pasienLama = 0;
            
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                            getDataRekening(data.karcis.daftartindakan_id,kelasPelayanan,1,'tm');
                        }
                    }
             }, "json");
     }      
       
}
function listAntrianRuangan(obj)
{
     kelasPelayanan=$('#PPPendaftaranRj_kelaspelayanan_id').val();
     ruangan=$('#PPPendaftaranRj_ruangan_id').val();
     if($('#isPasienLama').is(':checked'))
        pasienLama = 1;
     else
        pasienLama = 0;
            
         $.post("${urlListAntrianRuangan}", {ruangan:ruangan, pasienLama:pasienLama },
            function(data){
              $('#maxAntrianRuangan').val(data.maxAntrianRuangan);
         }, "json");
}
function listAntrianDokter(obj)
{
     kelasPelayanan=$('#PPPendaftaranRj_kelaspelayanan_id').val();
     ruangan=$('#PPPendaftaranRj_ruangan_id').val();
     dokter=$('#PPPendaftaranRj_pegawai_id').val();
     if($('#isPasienLama').is(':checked'))
        pasienLama = 1;
     else
        pasienLama = 0;
            
         $.post("${urlListAntrianDokter}", {ruangan:ruangan, pasienLama:pasienLama,dokter:dokter},
            function(data){
              $('#maxAntrianDokter').val(data.maxAntrianDokter);
         }, "json");
}
             
function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}             
            
function listDokterRuangan(idRuangan,idPegawai)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranRj_pegawai_id').html(data.listDokter);
            if (jQuery.isNumeric(idPegawai)){
                $('#PPPendaftaranRj_pegawai_id').val(idPegawai);
            }
    }, "json");
}
    
function listLoket(lokasiantrian_id)
{
    $.post("${urlListLoket}", { lokasiantrian_id: lokasiantrian_id },
        function(data){
            $('#loket').html(data.listLoket);
            $('#dialogAntrian').dialog('close');
    }, "json");
}
function listJenisAntrian(lokasiantrian_id)
{
    $.post("${urlListJenisAntrian}", { lokasiantrian_id: lokasiantrian_id },
        function(data){
            $('#jenis_antrian').html(data.listJenisAntrian);
            $('#dialogAntrian').dialog('close');
    }, "json");
}
 

JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
/*
$(".dtPicker3").datepicker();
    $(".add-on").on("click", function() {
        $("#PPPendaftaranRj_tgl_pendaftaran").datepicker("show");
});
*/

JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogAntrian',
            'options'=>array(
                'title'=>'Antrian',
                'autoOpen'=>false,
                'resizable'=>false,
                'width'=>180,
//                'height'=>180,
                'position'=>'right',
            ),
        ));

//            echo '<iframe src="'.$this->createUrl('TampilAntrian/PanggilAntrian').'" width="100%" height="100px" scrolling="no"></iframe>';
?>
    <div class="dialog-content">
        <?php echo $this->renderPartial('_formPanggilAntrian',array('modAntrian'=>$modAntrian)); ?>
    </div>
    <div style="text-align: center;">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon}',array('{icon}'=>'<i class="icon-backward icon-white"></i>')),array('title'=>'Klik untuk tampilkan antrian sebelumnya','rel'=>'tooltip','class'=>'btn  btn-mini btn-danger','onclick'=>'SetFormAntrian("prev");','style'=>'font-size:10px; width:24px; height:24px;')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon}',array('{icon}'=>'<i class="icon-forward icon-white"></i>')),array('title'=>'Klik untuk tampilkan antrian berikutnya','rel'=>'tooltip','class'=>'btn  btn-mini btn-danger','onclick'=>'SetFormAntrian("next");','style'=>'font-size:10px; width:24px; height:24px;')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon}',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('title'=>'Klik untuk refresh antrian','rel'=>'tooltip','class'=>'btn btn-mini btn-danger','onclick'=>'SetFormAntrian("reset");','style'=>'font-size:10px; width:24px; height:24px;')); ?>
        <br>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Panggil / Daftar',array('id'=>'btn-panggilantrian','{icon}'=>'<i class="icon-volume-up icon-white"></i>')),array('title'=>'Klik untuk memanggil antrian ini','rel'=>'tooltip','class'=>'btn  btn-mini btn-primary', 'onclick'=>'panggilAntrian();','style'=>'font-size:10px; width:128px; height:24px;')); ?>
    </div>
<?php
        $this->endWidget('zii.widgets.jui.CJuiDialog');
        
    }
?>

<?php 
$url = Yii::app()->createUrl('actionAjax/getListJadwalDokter');
$jsListDokter = '
    function getListDokterPoli(id){
        $.post("'.$url.'",{id:id},function(data){
            $("#bodyListJadwal").html(data);
            $("#jadwalDokterPoli").dialog("open");
            $("#jadwalDokterPoli a.pilihDokter").bind("click",function(){
                valueRuangan = $(this).attr("valueRuangan");
                valuePegawai = $(this).attr("valuePegawai");
                $("#PPPendaftaranRj_ruangan_id").val(valueRuangan);
                jQuery.ajax({"type":"POST",
                             "url":"?r=ActionDynamic/GetKasusPenyakit&encode=&namaModel='.get_class($model).'",
                             "cache":false,
                             "data":jQuery("#'.CHtml::activeId($model, 'jeniskasuspenyakit_id').'").parents("form").serialize(),
                             "success":function(html){jQuery("#'.CHtml::activeId($model, 'jeniskasuspenyakit_id').'").html(html)}
                         });
                listDokterRuangan(valueRuangan, valuePegawai);
                    
                listKarcis(valueRuangan);
                $("#jadwalDokterPoli").dialog("close");
                return false;
            });
        },"json");
    }
';
Yii::app()->clientScript->registerScript("getListDokterRuangan",$jsListDokter, CClientScript::POS_HEAD); 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'jadwalDokterPoli',
    'options'=>array(
        'title'=>'Jadwal Dokter Poli',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>850,
        'height'=>440,
        'resizable'=>false,
    ),
));
echo '<table class="table table-striped table-bordered table-condensed"><thead><th>Poliklinik</th>';
foreach (Params::namaHari() as $key => $value) {
    echo "<th>$value</th>";
}
echo '</thead>
    <tbody id="bodyListJadwal">
    </tbody>
</table>';

$this->endWidget();

?>

<script>
    $('#pakeAsuransi').attr('style','display:none;');
    var ruangan_poli = '<?php echo $model->ruangan_id; ?>';
    var dokter = '<?php echo $model->pegawai_id; ?>';
    $("#PPPendaftaranRj_ruangan_id").val(ruangan_poli);
    listDokterRuangan(ruangan_poli,dokter);
    $("#PPPendaftaranRj_pegawai_id").val(dokter);

    function cekDaftar(pasien_id, no_rekam_medik){
        if(pasien_id!=null){
            $("#dialogPasien").dialog("open");
            $.post("?r=actionAjax/CekPasien", { pasien_id: pasien_id },
            function(data){
                if(data.jumlah>0){
                    var jawab = confirm('Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus ' +data.statusperiksa+ ' di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran);
                    if (jawab){
                        $('#dialogPasien').dialog('close');
                        cariDataPasien(no_rekam_medik);
                    }
                    // $('#dialogCekPasien').dialog('open');
                    // $('#dataCekPasien').find('tbody').empty();
                    // $('#dataCekPasien').find('tbody').append('<tr>\n\
                    //         <td><input id="no_rekam_medik_pasien" type="hidden" name="no_rekam_medik_pasien" value="'+data.no_rekam_medik+'"></td><tr>\n\
                    //         <td><h4>Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus <u>'+data.statusperiksa+'</u> di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran+'</h4></td>\n\
                    //     </tr>'
                    // );
                }else{
                    $('#dialogPasien').dialog('close');
                    cariDataPasien(no_rekam_medik);
                }
            }, "json");
        }else{
            $.post("?r=actionAjax/AmbilPasienId", { no_rekam_medik: no_rekam_medik },
            function(data){
                var pasien_id = data.pasien_id;
                $.post("?r=actionAjax/CekPasien", { pasien_id: pasien_id },
                function(data){
                    if(data.jumlah>0){
                        $('#dialogCekPasien').dialog('open');
                        $('#dataCekPasien').find('tbody').empty();
                        $('#dataCekPasien').find('tbody').append('<tr>\n\
                                <td><input id="no_rekam_medik_pasien" type="hidden" name="no_rekam_medik_pasien" value="'+data.no_rekam_medik+'"></td><tr>\n\
                                <td><h4>Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus <u>'+data.statusperiksa+'</u> di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran+'</h4></td>\n\
                            </tr>'
                        );
                    }else{
                        $('#dialogPasien').dialog('close');
                        cariDataPasien(no_rekam_medik);
                    }
                }, "json");
            }, "json"); 
        }

        
    }


    function caraBayarChange(obj){
        var noMr = $("#noRekamMedik").val();
        var noMrLama = $("#noRekamMedikLama").val();
        $("#carabayarSep").val(obj.value);
        if(obj.value == 1 || obj.value == 0 ){
//         ERROR >>  setTimeout( 'var penjamin = document.getElementById(\'PPPendaftaranT_penjamin_id\'); penjamin.selectedIndex = 1;', '500' );
           document.getElementById('pakeAsuransi').checked = false;
           $('#pakeAsuransi').attr('style','display:none;');
           $('#divAsuransi').hide(500);
           $('#divAsuransiSep').hide(500);
        }else{
            $('#pakeAsuransi').removeAttr('style');
            document.getElementById('pakeAsuransi').checked = true;
            
            <?php 
            if(Yii::app()->user->getState('is_bridging')==TRUE){ 
            ?>
                if((noMr != '') && $(obj).val() == 18){
                    $('#divAsuransi').hide(500);
                    $('#divAsuransiSep input').removeAttr('disabled');
                    $('#divAsuransiSep select').removeAttr('disabled');
                    $('#divAsuransiSep textarea').removeAttr('disabled');
                    $('#divAsuransiSep').show(500);
                }else{
                    $('#divAsuransiSep').hide(500);
                    $('#divAsuransiSep input').attr('disabled','true');
                    $('#divAsuransiSep select').attr('disabled','true');
                    $('#divAsuransiSep textarea').attr('disabled','true');
                    $('#divAsuransi input').removeAttr('disabled');
                    $('#divAsuransi select').removeAttr('disabled');
                    $('#divAsuransi').show(500);
                }
                
            <?php 
            }else{
            ?>
                $('#divAsuransi input').removeAttr('disabled');
                $('#divAsuransi select').removeAttr('disabled');
                $('#divAsuransi').show(500);
            <?php
            }
            ?>
                        
//            $('#divAsuransi input').removeAttr('disabled');
//            $('#divAsuransi select').removeAttr('disabled');
//            $('#divAsuransi').show(500);
        }
    }
    
    function formSubmit(obj,evt)
    {
         evt = (evt) ? evt : event;
         var form_id = $(obj).closest('form').attr('id');
         var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
         if(charCode == 13){
            setVerifikasi();
         }
    }

    function setVerifikasi()
    {
        var namaPasien = $('#PPPasienM_nama_pasien').val();             
        var namaDepan = $('#PPPasienM_namadepan').val();
        var noIdentitas = $('#PPPasienM_no_identitas_pasien').val();    
        var jenisIdentitas = $('#PPPasienM_jenisidentitas').val();
        var alias = $('#PPPasienM_nama_bin').val();
        var tempatLahir = $('#PPPasienM_tempat_lahir').val();           
        var tglLahir = $('#PPPasienM_tanggal_lahir').val();
        var umur = $('#PPPendaftaranRj_umur').val();
        
        var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        if(jenisKelamin == '')
        {
            var jenisKelamin = "-";
        }else{
            var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        }
        
        var alamatPasien = $('#PPPasienM_alamat_pasien').val();         
        var rt = $('#PPPasienM_rt').val();  var rw = $('#PPPasienM_rw').val();
        var noTelp = $('#PPPasienM_no_telepon_pasien').val();           
        var noMobile = $('#PPPasienM_no_mobile_pasien').val();
        
        var propinsi = $('#PPPasienM_propinsi_id').val();
            if(propinsi != ''){
                 var propinsi = $("#PPPasienM_propinsi_id option:selected").text();
            }else{
                var propinsi = "-";
            }
        var kabupaten = $('#PPPasienM_kabupaten_id').val();
            if(kabupaten != ''){
                var kabupaten = $("#PPPasienM_kabupaten_id option:selected").text();
            }else{
                var kabupaten = "-";
            }
        var kecamatan = $('#PPPasienM_kecamatan_id').val();
            if(kecamatan != ''){
                var kecamatan = $("#PPPasienM_kecamatan_id option:selected").text();
            }else{
                var kecamatan = "-";
            }
        var kelurahan = $('#PPPasienM_kelurahan_id').val();
            if(kelurahan != ''){
                var kelurahan = $("#PPPasienM_kelurahan_id option:selected").text();
            }else{
                var kelurahan = "-";
            }
        var agama = $('#PPPasienM_agama').val();
            if(agama != ''){
                var agama = $('#PPPasienM_agama').val();
            }else{
                var agama = "-";
            }
        var warganegara = $('#PPPasienM_warga_negara').val();
            if(warganegara != ''){
                var warganegara = $('#PPPasienM_warga_negara').val();
            }else{
                var warganegara = "-";
            }
        var namaAyah = $('#PPPasienM_nama_ayah').val();     
        var namaIbu = $('#PPPasienM_nama_ibu').val();
        
        var poliklinikTujuan = $('#PPPendaftaranRj_ruangan_id').val();
            if(poliklinikTujuan != ''){
                var poliklinikTujuan = $("#PPPendaftaranRj_ruangan_id option:selected").text();      
            }else{
                var poliklinikTujuan = "-";
            }
        var jenisKasusPenyakit = $('#PPPendaftaranRj_jeniskasuspenyakit_id').val();
            if(jenisKasusPenyakit != ''){
                var jenisKasusPenyakit = $("#PPPendaftaranRj_jeniskasuspenyakit_id option:selected").text();    
            }else{
                var jenisKasusPenyakit = "-";
            }
        var kelasPelayanan = $('#PPPendaftaranRj_kelaspelayanan_id').val();
            if(kelasPelayanan != ''){
                var kelasPelayanan = $("#PPPendaftaranRj_kelaspelayanan_id option:selected").text();
            }else{
                var kelasPelayanan = "-";
            }
        var dokter = $('#PPPendaftaranRj_pegawai_id').val();
            if(dokter != ''){
                var dokter = $("#PPPendaftaranRj_pegawai_id option:selected").text();
            }else{
                var dokter = "-";
            }
        var caraBayar = $('#PPPendaftaranRj_carabayar_id').val();
            if(caraBayar != ''){
                var caraBayar = $("#PPPendaftaranRj_carabayar_id option:selected").text();
            }else{
                var caraBayar = "-";
            }
        var penjamin = $('#PPPendaftaranRj_penjamin_id').val();
            if(penjamin !=''){
                var penjamin = $("#PPPendaftaranRj_penjamin_id option:selected").text();
            }else{
                var penjamin = "-";
            }

        var kosong = '';
        var reqPasien       = $("#fieldsetPasien").find(".reqPasien[value="+kosong+"]");
        var pasienKosong = reqPasien.length;
        
        var reqKunjungan = $('#fieldsetKunjungan').find(".reqKunjungan[value="+kosong+"]");
        var kunjunganKosong = reqKunjungan.length;
        
        var pjKosong = 0;
        if($('#adaPenanggungJawab').is(':checked')){ //jika ada penanggung jawab data * harus diisi
            var reqPj = $('#fieldsetPenanggungjawab').find(".reqPj[value="+kosong+"]");
            pjKosong = reqPj.length;
        }
        
        var sepKosong = 0;
        if($('#pakeAsuransi').is(':checked')){
            if($('#PPPendaftaranRj_carabayar_id').val() == 18){
                var reqSep = $('#fieldsetAsuransiSep').find(".required[value="+kosong+"]");
                sepKosong = reqSep.length;
            }
        }
        
        if(!$("#isPasienLama").is(':checked')){ //jika pasien baru
            if (!(namaDepan && namaPasien && jenisKelamin && alamatPasien && (pasienKosong === 0)))
            {
                if(pasienKosong != 0)
                {
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Pasien');
                }
               return false;
            }
        }
        if($("#isUpdatePasien").is(':checked'))
        {
            
            if (!(namaDepan && namaPasien && jenisKelamin && alamatPasien && (pasienKosong === 0)))
            {
                if(pasienKosong != 0)
                {
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Pasien');
                }
               return false;
            }else{
                if (!((kunjunganKosong === 0) && (pjKosong === 0) && (sepKosong == 0)))
                {
                    if(kunjunganKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                    else if(pjKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');
                    else if(sepKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Asuransi');

                   return false;
                }
                else
                {
                    var confirm = $('#dialogVerifikasi').dialog('open');
                    if(confirm){
                        $('#dataPasien').find('tbody').empty();
                        $('#dataPasien').find('tbody').append('<tr>\n\
                                <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                                <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                                <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                                <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                                <td>Propinsi</td><td>' + propinsi + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Umur</td><td>' + umur + '</td>\n\
                                <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                                \n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                                <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                                <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                                <td>Agama</td><td>' + agama + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>No Telp</td><td>' + noTelp +'</td>\n\
                                <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                            </tr>'
                        );
                        $('#dataKunjungan').find('tbody').empty();
                        $('#dataKunjungan').find('tbody').append('<tr>\n\
                                <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                                <td>Dokter</td><td>'+ dokter + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                                <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                                <td>Penjamin</td><td>' + penjamin + '</td>\n\
                            </tr>'
                        );
                    }
                   return false;
                }                
            }
        }else{
            if (!((kunjunganKosong === 0) && (pjKosong === 0) && (sepKosong == 0)))
            {
                if(kunjunganKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                else if(pjKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');
                else if(sepKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Asuransi');

               return false;
            }
            else
            {
                var confirm = $('#dialogVerifikasi').dialog('open');
                if(confirm){
                    $('#dataPasien').find('tbody').empty();
                    $('#dataPasien').find('tbody').append('<tr>\n\
                            <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                            <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                            <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                            <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                            <td>Propinsi</td><td>' + propinsi + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Umur</td><td>' + umur + '</td>\n\
                            <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                            \n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                            <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                            <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                            <td>Agama</td><td>' + agama + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>No Telp</td><td>' + noTelp +'</td>\n\
                            <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                        </tr>'
                    );
                    $('#dataKunjungan').find('tbody').empty();
                    $('#dataKunjungan').find('tbody').append('<tr>\n\
                            <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                            <td>Dokter</td><td>'+ dokter + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                            <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                            <td>Penjamin</td><td>' + penjamin + '</td>\n\
                        </tr>'
                    );
                }
               return false;
            }
        }
        return false;
    }
    
    function kembaliForm(){
        
       $('#dialogVerifikasi').dialog('close');
    }
    
    function setKonfirmasi(obj){
        $(obj).attr('disabled',true);
        $(obj).removeAttr('onclick');
        $('#pppendaftaran-rj-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
        $('#pppendaftaran-rj-form').submit();
    }
    
    function refreshList(){
        
            $.fn.yiiGridView.update('pendaftarterakhir-rj-grid', {
                data: $(this).serialize()
            });
            return false;
    }
    
</script>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogVerifikasi',
        'options'=>array(
            'title'=>'Verifikasi Data Pasien',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>850,
            'height'=>550,
            'resizable'=>false,
        ),
    ));
?>
<div id="dataPasien">
    <legend class=rim>Data Pasien</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>

        </tbody>
        
    </table>
</div>
<div id ="dataKunjungan">
    <legend class=rim>Data Kunjungan</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>
            
        </tbody>
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Teruskan', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                '#', array('class'=>'btn btn-primary','onclick'=>'setKonfirmasi(this);','disabled'=>FALSE  )); 
        ?>
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Kembali', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                '#', array('class'=>'btn btn-blue','onclick'=>'kembaliForm();','disabled'=>FALSE)); 
        ?>
    </div>
</div>
<?php
    $this->endWidget();
?>


<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogCekPasien',
        'options'=>array(
            'title'=>'Verifikasi Data Pasien',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>400,
            'height'=>250,
            'resizable'=>false,
        ),
    ));
?>
<div id="dataCekPasien">
    <!-- <legend class=rim>Data Pasien</legend> -->
    <table class="table table-bordered  table-condensed">
        <tbody>

        </tbody>
        
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Teruskan', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                '#', array('class'=>'btn btn-primary','onclick'=>'setCekPasien();return false;','disabled'=>FALSE  )); 
        ?>
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Kembali', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                '#', array('class'=>'btn btn-blue','onclick'=>'kembaliForm("dialogCekPasien");','disabled'=>FALSE)); 
        ?>
    </div>
</div>
<?php
    $this->endWidget();
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'		 => 'dialog-proses-sep',
        'options'	 => array(
        'title'		 => 'Proses SEP',
        'autoOpen'	 => false,
        'modal'		 => true,
        'minWidth'	 => 960,
        'minHeight'	 => 480,
        'resizable'	 => false,
        ),
));
echo '<iframe id="iframeProsesSEP"  name="iframeProsesSEP" width="100%" height="550" >
</iframe>';
?>
<?php $this->endWidget(); ?>

<?php //$this->renderPartial('_settingAntrian', array('modAntrian'=>$modAntrian)); ?>