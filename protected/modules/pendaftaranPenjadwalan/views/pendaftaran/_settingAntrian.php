<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<script>
    
    var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
    if (chatServer == ''){
     chatServer='http://localhost';
    }
    var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
    socket = io.connect(chatServer+':'+chatPort);
            
    function cekAntrian(){
        
        var lokasi = $('#lokasiantrian').val();
        var loket = $('#loket').val();
        var jenis_antrian = $('#jenis_antrian').val();
        
        if(lokasi == ""){
            alert("Pilih Lokasi Antrian Dahulu");
        }else if(loket == ""){
            alert("Pilih Loket Antrian Dahulu");
        }else if(jenis_antrian == ""){
            alert("Pilih Jenis Antrian Dahulu");
        }else{
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/TampilAntrian/SetFormAntrian'); ?>',
                data: {lokasi:lokasi, loket : loket, jenis_antrian:jenis_antrian},
                dataType: "json",
                success:function(data){
                    if(data.pesan !== "OK"){
                        alert(data.pesan);
                    }else{
                        $('#dialogAntrian').parent().css({position:'absolute'}).end().dialog('open');
                        $("#dialogAntrian > .dialog-content").html(data.model);
                        return true;
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
            });
//            $('#dialogAntrian').parent().css({position:'absolute'}).end().dialog('open');
        }
    }
    
    function SetFormAntrian(record){
        var lokasi = $("#<?php echo CHtml::activeId($modAntrian, 'lokasiantrian_id'); ?>").val();
        var loket = $('#loket').val();
        var jenis_antrian = $("#<?php echo CHtml::activeId($modAntrian, 'jenisantrian_id'); ?>").val();
        var antrianpasien_id = $("#<?php echo CHtml::activeId($modAntrian, 'antrianpasien_id'); ?>").val();
        
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/TampilAntrian/SetFormAntrian'); ?>',
            data: {lokasi:lokasi, loket : loket, jenis_antrian:jenis_antrian, record:record, antrianpasien_id:antrianpasien_id},
            dataType: "json",
            success:function(data){
                if(data.pesan !== "OK"){
                    alert(data.pesan);
                }else{
                    $('#dialogAntrian').parent().css({position:'absolute'}).end().dialog('open');
                    $("#dialogAntrian > .dialog-content").html(data.model);
                    return true;
                }

            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }
    
    function panggilAntrian(){
        var lokasi = $("#<?php echo CHtml::activeId($modAntrian, 'lokasiantrian_id'); ?>").val();
        var loket = $('#loket').val();
        var jenis_antrian = $("#<?php echo CHtml::activeId($modAntrian, 'jenisantrian_id'); ?>").val();
        var antrianpasien_id = $("#<?php echo CHtml::activeId($modAntrian, 'antrianpasien_id'); ?>").val();
        var tglpemanggilanpasien = $("#<?php echo CHtml::activeId($modAntrian, 'tglpemanggilanpasien'); ?>").val();
        var nourutantrian = $("#<?php echo CHtml::activeId($modAntrian, 'nourutantrian'); ?>").val();
        
        if(antrianpasien_id !== ""){
            var sudah = 1;
            if(tglpemanggilanpasien !== ""){
                if(confirm("Antrian sudah dipaggil. Apakah akan mengulang antrian "+nourutantrian+"?")){
                    sudah = 1;
                }else{
                    sudah = 0;
                }
            }
            if(sudah==0){return false;}
            $('#dialogAntrian .btn-primary').attr("disabled",true);
            $('#dialogAntrian .btn-primary').removeAttr("onclick");
            $.ajax({
                type:'POST',
                url:'<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/TampilAntrian/PanggilAntrianNew'); ?>',
                data: {lokasi:lokasi, loket : loket, jenis_antrian:jenis_antrian, antrianpasien_id:antrianpasien_id},
                dataType: "json",
                success:function(data){
                    if(data.pesan == "LOKET_LAIN"){
                        alert("Nomor antrian sudah dipanggil di loket lain");
                    }else{
                        if(data.lokasiantrian_id==1){
                            jenis = 'antrianPendaftaran';
                        }else{
                            jenis = 'antrianPendaftaran';
                        }
                        socket.emit('send',{
                            conversationID:jenis,
                            antrianpasien_id:data.model.antrianpasien_id,
                            lokasiantrian_id:data.model.lokasiantrian_id,
                            jenisantrian_id:data.model.jenisantrian_id,
                            loketantrian_id:data.model.loketantrian_id,
                            nourutantrian:data.model.nourutantrian,
                        });
                        setTimeout(function(){
                        $('#dialogAntrian .btn-primary').removeAttr("disabled");
                        $('#dialogAntrian .btn-primary').attr("onclick","panggilAntrian();");
			},5000); //5 detik tombol baru aktif
                        $("#<?php echo CHtml::activeId($modAntrian, 'antrianpasien_id_temp'); ?>").val(data.model.antrianpasien_id);
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
            });
        }else{
            alert("Pilih dahulu no antrian");
        }
    }

$(document).ready(function(){
    setTimeout(function(){

    },5000);
});
</script>