<?php $form_antrian=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'anantrian-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event);', 'onsubmit'=>'return requiredCheck(this);'),
        'focus'=>'#',
)); ?>
    <div class="row-fluid">
	<div class = "span12">
            <?php echo $form_antrian->hiddenField($modAntrian,'antrianpasien_id',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'lokasiantrian_id',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'jenisantrian_id',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'pendaftaran_id',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'loketantrian_id',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'tglantrianpasien',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'jmlpemanggilan',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'nourutantrian',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'maxlength'=>6)); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'is_dilayani',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form_antrian->hiddenField($modAntrian,'tglpemanggilanpasien',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <div style="width: 100%; text-align: center; font-size: 30px; font-weight: bold;" id="tampil_noantrian">
                <?php echo (isset($modAntrian->nourutantrian)&&!empty($modAntrian->nourutantrian))? $modAntrian->nourutantrian : "X-000"; ?>
            </div>
<!--            <div class="control-group">
                <?php // echo CHtml::label('Status Panggilan', 'panggil_flaq',array('class'=>'control-label')); ?>
                <div class="controls">-->
                    <?php 
//                        $statuspanggilan = (isset($modAntrian->antrian_id) ? (($modAntrian->panggil_flaq) ? "SUDAH" : "BELUM") : "");
//                        echo CHtml::hiddenField('statuspanggilan',$statuspanggilan,array('class'=>'span3'));
//						echo $form_antrian->hiddenField($modAntrian,'panggil_flaq',array('readonly'=>true,'class'=>'span3'));
                    ?>
<!--                </div>
            </div>-->
	</div>
    </div>
    
<?php $this->endWidget(); ?>

