<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-mp-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $modLoket = LoketM::model()->findAllByAttributes(array('loket_aktif'=>true));
        $loket = (isset($_POST['loket'])) ? $_POST['loket'] : '';
        echo CHtml::dropDownList('loket', $loket, CHtml::listData($modLoket, 'loket_id', 'loket_nama'), array('empty'=>'-- Pilih Loket --','class'=>'span2'));
        echo CHtml::link(Yii::t('mds', 'Antrian {icon}', array('{icon}'=>'<i class="icon-chevron-right icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"$('#dialogAntrian').parent().css({position:'fixed'}).end().dialog('open');return false;")); 
    }
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>
        <legend class="rim2">Pendaftaran Pasien Masuk Penunjang</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienPenunjang,$modPengambilanSample)); ?>
        <table class='table-condensed'>
            <tr>
                <td width="50%">
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
						<div class="label_no">
                           <i class="icon-user"></i> <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            No Rekam Medik
							</div>
                        </label>
                        <?php //echo CHtml::label('No Rekam Medik', 'noRekamMedik', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                                    $enable['readonly'] = true;
                                    $readOnly = ($model->isPasienLama) ? '' : $enable ; 
                            ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedik',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedik").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                                $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                                $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                                $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                                $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                                $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                                $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                                $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                                setJenisKelaminPasien(ui.item.jeniskelamin);
                                                setRhesusPasien(ui.item.rhesus);
                                                loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, ui.item.kelurahan_id);
                                                $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                                $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                                $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                                $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                                $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                                $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                                $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                                $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                                $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                                $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                                $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                                $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                                $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                                $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                                $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                                $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                                $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                                $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                                $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                                loadUmur(ui.item.tanggal_lahir);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('onblur'=>'cariDataPasien(this.value);','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                            )); ?>
                            <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                            //    array('onclick'=>'$("#dialogPasien").dialog("open");return false;',
                                                  //    'class'=>'btn btn-primary',
                                                //      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                   //   'rel'=>"tooltip",
                                                  //    'title'=>"Klik untuk mencari pasien",)); ?>
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                </td>
                
                <td width="50%">
                    <div class="label_no">
                        <?php $minDate = (Yii::app()->user->getState('tgltransaksimundur')) ? '' : 'd'; ?>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        'minDate' => $minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group" id="controlNoRekamMedikLama">
                        <label class="control-label">
						<div class="label_no">
                         <i class="icon-user"></i>   <?php echo CHtml::checkBox('isPasienBaru','', array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                           Gunakan No. RM yg tidak dipakai
							</div>
                        </label>
                        <div class="controls">
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedikLama',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienNonAktif'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedikLama").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasienLama','idTombol'=>'tombolPasienDialogBaru'),
										
                            )); ?>
                           
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
              <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien,'format'=>$format)); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend class="rim">Data Kunjungan</legend>
                    <table>
                        <tr>
                            <td width="50%">
                                <?php echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData(RuanganpenunjangV::model()->findAll("ruangan_aktif = true"), 'ruangan_id', 'ruangan_nama'), array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                        'ajax'=>array('type'=>'POST',
                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')),
                                                                          'update'=>'#PPPendaftaranMp_jeniskasuspenyakit_id'),'onChange'=>'listDokterRuangan(this.value);listKarcis(this.value);'
                                                                        )); ?>

                                <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type'=>'POST',
                                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPPendaftaranMp')), 
                                                                'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                            ),
                                    )); ?>

                                <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                            </td>
                            <td width="50%">

            <!--                    <fieldset>
                                    <legend>Rujukan</legend>-->
                                    <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan,'modPengambilanSample'=>$modPengambilanSample)); ?>
            <!--                    </fieldset>-->

                                <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>

                                <?php //echo $form->dropDownListRow($model,'caramasuk_id', CHtml::listData($model->getCaraMasukItems(), 'caramasuk_id', 'caramasuk_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php //echo $form->dropDownListRow($model,'keadaanmasuk', KeadaanMasuk::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php //echo $form->dropDownListRow($model,'transportasi', Transportasi::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>

                                <?php echo $this->renderPartial('_formPengambilanSample', array('model'=>$model,'form'=>$form,'modPengambilanSample'=>$modPengambilanSample)); ?>
            
                                <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Poliklinik                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </fieldset>
	
		<div class="form-actions">
		<?php 
                    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>false,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pendaftaran/MasukPenunjang',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
                 <?php if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        {
                ?>
                          <script>
                            print(<?php echo $model->pendaftaran_id; ?>);
                          </script>
                <?php echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       }else{
                        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       } 
                ?>
				<?php $this->endWidget(); ?>

<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>
	</div>

<script type="text/javascript">
if($('#isPasienLama').is(':checked')){
    $('#tombolPasienDialog').removeClass('hide');
}else{
    $('#tombolPasienDialog').addClass('hide');
}

</script>

<?php $url = CController::createUrl('ActionDynamic/GetKasusPenyakit'); ?>

<?php 
    /*
    Yii::app()->clientScript->registerScript('status_masuk',"
    $(document).ready(function(){
              
        $(':input').keypress(function(e) {
            if(e.keyCode == 13) {
                $(this).focusNextInputField(); 
            } 
        });
    });
    "); 
     * 
     */
    ?>

<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoli',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;
$jscript = <<< JS
function print(idPendaftaran)
{
        if(document.getElementById('isPasienLama').checked == true){
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=280');
        }else{
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
            window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
        }
}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranMp_pegawai_id').html(data.listDokter);
    }, "json");
}

function listKarcis(obj)
{
     kelasPelayanan=$('#PPPendaftaranMp_kelaspelayanan_id').val();
     ruangan=$('#PPPendaftaranMp_ruangan_id').val();
     if($('#isPasienLama').is(':checked'))
        pasienLama = 1;
     else
        pasienLama = 0;
            
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                            getDataRekening(data.karcis.daftartindakan_id,kelasPelayanan,1,'tm');
                        }
                    }
             }, "json");
     }      
       
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}        

JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogAntrian',
            'options'=>array(
                'title'=>'Antrian',
                'autoOpen'=>true,
                'resizable'=>false,
                'width'=>170,
                'height'=>150,
                'position'=>'right',
            ),
        ));

            echo '<iframe src="'.$this->createUrl('TampilAntrian/PanggilAntrian').'" width="100%" height="100px" scrolling="no"></iframe>';

        $this->endWidget('zii.widgets.jui.CJuiDialog');
        
    }
?>