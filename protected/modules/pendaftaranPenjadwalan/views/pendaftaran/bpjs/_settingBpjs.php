<script>
    
    function getAsuransiNoKartu(isi)
    {
            if (<?php echo (Yii::app()->user->getState('is_bridging') == TRUE) ? 1 : 0; ?>) {
            } else {
                    alert('Fitur Bridging tidak aktif!');
                    return false;
            }
            if (isi == "") {
                    alert('Isi no peserta terlebih dahulu!');
                    return false;
            }
            ;
            var aksi = 1; // 1 untuk mencari data peserta berdasarkan Nomor Kartu
            var setting = {
                    url: "<?php echo $this->createUrl('SettingBpjs/bpjsInterface'); ?>",
                    type: 'GET',
                    dataType: 'html',
                    data: 'param=' + aksi + '&query=' + isi,
                    beforeSend: function () {
                        $("#animation").addClass("loader");
                    },
                    success: function (data) {
                            $("#animation").removeClass("loader");
                            var obj = JSON.parse(data);
                            if (obj.response != null) {
                                    var peserta = obj.response.peserta;
                                        if (peserta.statusPeserta.keterangan == 'AKTIF') {
                                            $("#<?php echo CHtml::activeId($modSep, 'nopeserta_bpjs') ?>").val(peserta.noKartu);
                                            $("#<?php echo CHtml::activeId($modSep, 'namapeserta_bpjs') ?>").val(peserta.nama);
                                            $("#<?php echo CHtml::activeId($modSep, 'hakkelas_kode') ?>").val(peserta.hakKelas.kode);
                                            $("#<?php echo CHtml::activeId($modSep, 'kelasrawat_kode') ?>").val(peserta.hakKelas.kode);
                                            $("#<?php echo CHtml::activeId($modSep, 'hakkelas_nama') ?>").val(peserta.hakKelas.keterangan);
                                            $("#<?php echo CHtml::activeId($modSep, 'notelpon_peserta') ?>").val(peserta.mr.noTelepon);
                                            $("#<?php echo CHtml::activeId($modSep, 'ppkrujukanasal_kode') ?>").val(peserta.provUmum.kdProvider);
                                            $("#<?php echo CHtml::activeId($modSep, 'ppkrujukanasal_nama') ?>").val(peserta.provUmum.nmProvider);
                                            $("#<?php echo CHtml::activeId($modSep, 'jenispeserta_bpjs_kode') ?>").val(peserta.jenisPeserta.kode);
                                            $("#<?php echo CHtml::activeId($modSep, 'jenispeserta_bpjs_nama') ?>").val(peserta.jenisPeserta.keterangan);
                                            if(peserta.cob.nmAsuransi != null){
                                                $("#<?php echo CHtml::activeId($modSep, 'cob_bpjs') ?>").val(1);
                                                $("#<?php echo CHtml::activeId($modSep, 'cob_status') ?>").val("YA");
                                                $("#<?php echo CHtml::activeId($modSep, 'no_asuransi_cob') ?>").val(peserta.cob.noAsuransi);
                                                $("#<?php echo CHtml::activeId($modSep, 'nama_asuransi_cob') ?>").val(peserta.cob.nmAsuransi);
                                            }else{
                                                $("#<?php echo CHtml::activeId($modSep, 'cob_bpjs') ?>").val(0);
                                                $("#<?php echo CHtml::activeId($modSep, 'cob_status') ?>").val("TIDAK");
                                            }

                                            // OVERWRITES old selecor
                                            jQuery.expr[':'].contains = function (a, i, m) {
                                                    return jQuery(a).text().toUpperCase()
                                                                    .indexOf(m[3].toUpperCase()) >= 0;
                                            };
                                        } else {
                                                alert('Peserta Tidak Aktif');
                                        }
                            } else {
                                    alert(obj.metaData.message);
                            }
                    },
                    error: function (data) {
                            $("#animation").removeClass("loader");
                    }
            }

            if (typeof ajax_request !== 'undefined')
                    ajax_request.abort();
            ajax_request = $.ajax(setting);
    }

    function getRujukanNoRujukan(isi)
    {
            if (<?php echo (Yii::app()->user->getState('is_bridging') == TRUE) ? 1 : 0; ?>) {
            } else {
                    alert('Fitur Bridging tidak aktif!');
                    return false;
            }
            if (isi == "") {
                    alert('Isi no rujukan terlebih dahulu!');
                    return false;
            }
            ;
            var jenis_rujukan = $('input:radio[name=SepT[jenisrujukan]]:checked').val();
            var aksi = 3; // 3 untuk mencari data rujukan berdasarkan Nomor rujukan
            var setting = {
                url: "<?php echo $this->createUrl('SettingBpjs/bpjsInterface'); ?>",
                type: 'GET',
                dataType: 'html',
                data: 'param=' + aksi + '&query=' + isi + '&jenis_rujukan=' +jenis_rujukan,
                beforeSend: function () {
                        $("#animation").addClass("loader");
                },
                success: function (data) {
                        $("#animation").removeClass("loader");
                        var obj = JSON.parse(data);
                        if(obj.metaData.code == '201'){
                                alert(obj.metaData.message);
                        }else{
                            if (obj.response != null) {
                                var rujukan = obj.response.rujukan;
                                $("#<?php echo CHtml::activeId($modSep, 'nopeserta_bpjs') ?>").val(rujukan.peserta.noKartu);
                                $("#<?php echo CHtml::activeId($modSep, 'namapeserta_bpjs') ?>").val(rujukan.peserta.nama);

                                $("#<?php echo CHtml::activeId($modSep, 'diagnosaawal_kode') ?>").val(rujukan.diagnosa.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'norujukan_bpjs') ?>").val(rujukan.noKunjungan);
                                $("#<?php echo CHtml::activeId($modSep, 'jnspelayanan_kode') ?>").val(rujukan.pelayanan.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'hakkelas_kode') ?>").val(rujukan.peserta.hakKelas.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'kelasrawat_kode') ?>").val(rujukan.peserta.hakKelas.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'hakkelas_nama') ?>").val(rujukan.peserta.hakKelas.keterangan);
                                $("#<?php echo CHtml::activeId($modSep, 'ppkrujukanasal_kode') ?>").val(rujukan.provPerujuk.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'ppkrujukanasal_nama') ?>").val(rujukan.provPerujuk.nama);
                                $("#<?php echo CHtml::activeId($modSep, 'tglrujukan_bpjs') ?>").val(rujukan.tglKunjungan);
                                $("#<?php echo CHtml::activeId($modSep, 'politujuan_kode') ?>").val(rujukan.poliRujukan.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'politujuan_nama') ?>").val(rujukan.poliRujukan.nama);
                                $("#<?php echo CHtml::activeId($modSep, 'notelpon_peserta') ?>").val(rujukan.peserta.mr.noTelepon);
                                $("#<?php echo CHtml::activeId($modSep, 'jenispeserta_bpjs_kode') ?>").val(rujukan.peserta.jenisPeserta.kode);
                                $("#<?php echo CHtml::activeId($modSep, 'jenispeserta_bpjs_nama') ?>").val(rujukan.peserta.jenisPeserta.keterangan);
                                if(rujukan.peserta.cob.nmAsuransi != null){
                                    $("#<?php echo CHtml::activeId($modSep, 'cob_bpjs') ?>").val(1);
                                    $("#<?php echo CHtml::activeId($modSep, 'cob_status') ?>").val("YA");
                                    $("#<?php echo CHtml::activeId($modSep, 'no_asuransi_cob') ?>").val(rujukan.peserta.cob.noAsuransi);
                                    $("#<?php echo CHtml::activeId($modSep, 'nama_asuransi_cob') ?>").val(rujukan.peserta.cob.nmAsuransi);
                                }else{
                                    $("#<?php echo CHtml::activeId($modSep, 'cob_bpjs') ?>").val(0);
                                    $("#<?php echo CHtml::activeId($modSep, 'cob_status') ?>").val("TIDAK");
                                }
                            } else {
                                alert(obj.metaData.message);
                            }
                        }
                },
                error: function (data) {
                        $("#animation").removeClass("loader");
                }
            }

            if (typeof ajax_request !== 'undefined')
                    ajax_request.abort();
            ajax_request = $.ajax(setting);
    }
    
    function cekInputSep(param,jnsPelayanan){
        var status = 0;
        var klsPelayanan = null;
        var noTlp = $("#PPPasienM_no_mobile_pasien").val();
        
        if(noTlp.length < 6){
            alert("No Telepon Minimal 8 Digit");
        }else if(noTlp.length < 6){
            status = 0;
        }else if(noTlp.length >= 6){
             status = 1;
        }
        
        if($('#noRekamMedik').val() != ''){
            var noMR = $('#noRekamMedik').val();
        }else{
            var noMR = $('#noRekamMedikLama').val();
        }
        
        var ruangan = $("#<?php echo CHtml::activeId($model, 'ruangan_id') ?>").val();
        if(jnsPelayanan==1){ //RI
            ruangan = '';
        }
                
        var noKartu = $("#<?php echo CHtml::activeId($modSep, 'nopeserta_bpjs') ?>").val();
        var noSep = $("#<?php echo CHtml::activeId($modSep, 'no_sep') ?>").val();
        
        if(jnsPelayanan == 2){
            var tglSep = $("#<?php echo CHtml::activeId($model, 'tgl_pendaftaran') ?>").val();
            klsPelayanan = $("#<?php echo CHtml::activeId($model, 'kelaspelayanan_id') ?>").val();
        }else if(jnsPelayanan == 1){
            var tglSep = $("#PPPasienAdmisiT_tgladmisi").val();
            klsPelayanan = $("#PPPasienAdmisiT_kelaspelayanan_id").val();
        }else{
            var tglSep = '<?php echo $modSep->tglsep; ?>';
        }
        
        var ppkPelayanan = '<?php echo $modSep->ppkpelayanan; ?>';
        var jnsPelayanan = jnsPelayanan;
        var klsRawat = $("#<?php echo CHtml::activeId($modSep, 'hakkelas_kode') ?>").val();
        var asalRujukan = $('input:radio[name="SepT[jenisrujukan_kode_bpjs]"]:checked').val();
        var tglRujukan = $("#<?php echo CHtml::activeId($modSep, 'tglrujukan_bpjs') ?>").val();
        var noRujukan = $("#<?php echo CHtml::activeId($modSep, 'norujukan_bpjs') ?>").val();
        var ppkRujukan = $("#<?php echo CHtml::activeId($modSep, 'ppkrujukanasal_kode') ?>").val();
        var catatan = $("#<?php echo CHtml::activeId($modSep, 'catatan_sep') ?>").val();
        var diagAwal = $("#<?php echo CHtml::activeId($modSep, 'diagnosaawal_kode') ?>").val();
        var tujuan = ruangan;
        var eksekutif = $('input:radio[name="SepT[polieksekutif]"]:checked').val();
        var cob = $("#<?php echo CHtml::activeId($modSep, 'cob_bpjs') ?>").val();
        var lakaLantas = $('input:radio[name="SepT[lakalantas_kode]"]:checked').val();
        var penjamin = $("#<?php echo CHtml::activeId($modSep, 'penjaminlakalantas') ?>").val();
        var lokasiLaka = $("#<?php echo CHtml::activeId($modSep, 'lokasilakalantas') ?>").val();
        var noTelp = noTlp;
        var user = '<?php echo $modSep->pembuat_sep; ?>';

        if(status==1){
            var setting = {
                    url: "<?php echo $this->createUrl('SettingBpjs/bpjsInterface'); ?>",
                    type: 'GET',
                    dataType: 'html',
                    data: 'param=' +param+ '&noMR='+noMR+'&noKartu='+noKartu+'&tglSep='+tglSep+'&ppkPelayanan='+ppkPelayanan+'&jnsPelayanan='+jnsPelayanan+'&klsRawat='+klsRawat+'&asalRujukan='+asalRujukan+'&tglRujukan='+tglRujukan+'&noRujukan='+noRujukan+'&ppkRujukan='+ppkRujukan+'&catatan='+catatan+'&diagAwal='+diagAwal+'&tujuan='+tujuan+'&eksekutif='+eksekutif+'&cob='+cob+'&lakaLantas='+lakaLantas+'&penjamin='+penjamin+'&lokasiLaka='+lokasiLaka+'&noTelp='+noTelp+'&user='+user+'&noSep='+noSep+'&klsPelayanan='+klsPelayanan,
                    beforeSend: function () {
                        $("#animation").addClass("loader");
                    },
                    success: function (data) {
                        $("#animation").removeClass("loader");
                        var obj = JSON.parse(data);
                        if(obj.metaData.code != '200'){
                            alert(obj.metaData.message);
                        }else{
                            if (obj.response != null) {
                                if(param==13){//insert
                                    var sep = obj.response.sep;
                                    $("#<?php echo CHtml::activeId($modSep, 'no_sep') ?>").val(sep.noSep);
                                    alert("No SEP berhasil dibuat");
                                    $('#tombol_create_sep').attr('disabled','true');
                                    <?php 
                                    if(!empty($model->pendaftaran_id)){
                                    ?>
                                        $("#proses-sep").submit();
                                    <?php 
                                    }
                                    ?>
                                }
                            }
                        }
                    },
                    error: function (data) {
                        $("#animation").removeClass("loader");
                    }
            }
            if (typeof ajax_request !== 'undefined')
            ajax_request.abort();
            ajax_request = $.ajax(setting);
        }
    }
    
</script>