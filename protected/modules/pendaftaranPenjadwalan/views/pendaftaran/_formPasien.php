<fieldset id='fieldsetPasien'>
    <legend class="rim">
        Data Pasien <?php echo CHtml::checkBox('isUpdatePasien', '', array('rel'=>'tooltip','title'=>'Pilih untuk update data pasien','onclick'=>'updateInputPasien(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> <i class="icon-pencil"></i>
    </legend>
    <table class="table-condensed">
        <tr>
            <td width="33%">
                    <?php //echo $form->textFieldRow($modPasien,'no_rekam_medik'); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'no_identitas_pasien', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php $modPasien->jenisidentitas = (!empty($modPasien->jenisidentitas)) ? $modPasien->jenisidentitas : 'LAINNYA';?>
                            <?php echo $form->dropDownList($modPasien,'jenisidentitas', JenisIdentitas::items(),  
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                                )); ?>   
                            <?php echo $form->textField($modPasien,'no_identitas_pasien', array('placeholder'=>'No Identitas','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            
                            <?php echo $form->error($modPasien, 'jenisidentitas'); ?><?php echo $form->error($modPasien, 'no_identitas'); ?>
                        </div>
                    </div>

                    <?php //echo $form->dropDownListRow($modPasien,'jenisidentitas', JenisIdentitas::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php //echo $form->textFieldRow($modPasien,'no_identitas_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'nama_pasien', array('class'=>'control-label')) ?>
                        <div class="controls inline">

                            <?php echo $form->dropDownList($modPasien,'namadepan', NamaDepan::items(),  
                                                          array(
                                                              'empty'=>'-- Pilih --',
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'span2 reqPasien',
                                                              'readonly'=>true,
                                                              
                                                          )
                                    ); ?>   
                            <?php echo $form->textField($modPasien,'nama_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2 reqPasien','placeholder'=>'Nama Pasien', 'onchange'=>'convertToUpper(this)', 'onkeyup'=>'convertToUpper(this)')); ?>

                            <?php echo $form->error($modPasien, 'namadepan'); ?><?php echo $form->error($modPasien, 'nama_pasien'); ?>
                        </div>
                    </div>

                    <?php //echo $form->dropDownListRow($modPasien,'namadepan', NamaDepan::items(),array('empty'=>'-- Pilih --','class'=>'span1','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php //echo $form->textFieldRow($modPasien,'nama_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php echo $form->textFieldRow($modPasien,'nama_bin', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alias')); ?>
                    <?php echo $form->textFieldRow($modPasien,'tempat_lahir', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'placeholder'=>'Tempat Lahir')); ?>
                    <?php //echo $form->textFieldRow($modPasien,'tanggal_lahir'); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'tanggal_lahir', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPasien,
                                                    'attribute'=>'tanggal_lahir',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        //
                                                        'onkeypress'=>"js:function(){getUmur(this);}",
                                                        'onSelect'=>'js:function(){getUmur(this);}',
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3 reqPasien', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                            <?php echo $form->error($modPasien, 'tanggal_lahir'); ?>
                        </div>
                    </div>

                    <?php //echo $form->textFieldRow($model,'umur', array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'umur', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php
                                // $this->widget('CMaskedTextField', array(
                                // 'model' => $model,
                                // 'attribute' => 'umur',
                                // 'mask' => '99 Thn 99 Bln 99 Hr',
                                // 'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)', 'onchange'=>'setNamaGelar()', 'class'=>'reqPasien')
                                // ));
                                ?>
                              <?php echo $form->textField($model,'thn', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this); setNamaGelar();','onkeypress'=>"return $(this).focusNextInputField(event)", 'value'=>'00')); ?> Thn
                              <?php echo $form->textField($model,'bln', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this); setNamaGelar();','onkeypress'=>"return $(this).focusNextInputField(event)", 'value'=>'00')); ?> Bln
                              <?php echo $form->textField($model,'hr', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this); setNamaGelar();','onkeypress'=>"return $(this).focusNextInputField(event)", 'value'=>'00')); ?> Hr
                            <?php echo $form->error($model, 'umur'); ?>
                        </div>
                    </div>

                    <?php //echo $form->dropDownListRow($modPasien,'kelompokumur', KelompokUmur::items(),array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php echo $form->radioButtonListInlineRow($modPasien, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)", 'onchange'=>"setNamaGelar()", 'class'=>'reqPasien jk')); ?>
                    <?php //echo $form->dropDownListRow($modPasien,'jeniskelamin', JenisKelamin::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php echo $form->dropDownListRow($modPasien,'statusperkawinan', StatusPerkawinan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onchange'=>'setNamaGelar()')); ?>
            </td>
            <td width="33%">
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'golongandarah', array('class'=>'control-label')) ?>

                        <div class="controls">

                            <?php echo $form->dropDownList($modPasien,'golongandarah', GolonganDarah::items(),  
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php echo $form->radioButtonList($modPasien,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'rhesus')); ?>            
                                </div>
                           </div>
                            <?php echo $form->error($modPasien, 'golongandarah'); ?>
                            <?php echo $form->error($modPasien, 'rhesus'); ?>
                        </div>
                    </div>
                    <?php //echo $form->dropDownListRow($modPasien,'golongandarah', GolonganDarah::items(),array('empty'=>'-- Pilih --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php //echo $form->dropDownListRow($modPasien,'rhesus', Rhesus::items(),array('empty'=>'-- Pilih --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $form->textAreaRow($modPasien,'alamat_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat Lengkap Pasien', 'onchange'=>'convertToUpper(this)', 'onkeyup'=>'convertToUpper(this)', 'class'=>'reqPasien')); ?>
                    <?php //echo $form->textFieldRow($modPasien,'rt',array('class'=>'span1','maxlength'=>3, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'rt', array('class'=>'control-label inline')) ?>

                        <div class="controls">
                            <?php echo $form->textField($modPasien,'rt', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1 numbersOnly','maxlength'=>3,'placeholder'=>'RT')); ?>   / 
                            <?php echo $form->textField($modPasien,'rw', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1 numbersOnly','maxlength'=>3,'placeholder'=>'RW')); ?>            
                            <?php echo $form->error($modPasien, 'rt'); ?>
                            <?php echo $form->error($modPasien, 'rw'); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class ="control-label required">
                            <?php echo $form->labelEx($modPasien,'no_telepon_pasien') ?>
                        </label>
                        <div class="controls">
                            <?php echo $form->textField($modPasien,'no_telepon_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No Telepon Yang Dapat Dihubungi', 'class'=>'numbersOnly')); ?>
                        </div>
                    </div>
                    <?php // echo $form->textFieldRow($modPasien,'no_telepon_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No Telepon Yang Dapat Dihubungi', 'class'=>'numbersOnly reqPasien')); ?>
                    <?php  echo $form->textFieldRow($modPasien,'no_mobile_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'numbersOnly','placeholder'=>'No. Hp Yang Dapat Dihubungi')); ?>
                    <?php
                        echo $form->textFieldRow($modPasien,'nama_ibu',
                            array(
                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'placeholder'=>'Nama Ibu'
                            )
                        );
                    ?>
                    <?php
                        echo $form->textFieldRow($modPasien,'nama_ayah',
                            array(
                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                'placeholder'=>'Nama Ayah'
                            )
                        );
                    ?>
                    <?php //echo $form->textFieldRow($modPasien,'rw',array('class'=>'span1','maxlength'=>3, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td width="25%">
                <div class="control-group ">
                    <?php echo $form->labelEx($modPasien,'propinsi_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php $modPasien->propinsi_id = (!empty($modPasien->propinsi_id))?$modPasien->propinsi_id:Yii::app()->user->getState('propinsi_id');?>
                    <?php echo $form->dropDownList($modPasien,'propinsi_id', CHtml::listData($modPasien->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                                      array('class'=>'reqPasien','empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modPasien, 'kabupaten_id')."','".CHtml::activeId($modPasien, 'rw')."')", 
                                                            'ajax'=>array('type'=>'POST',
                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                          'update'=>'#PPPasienM_kabupaten_id',),
                                                            'onchange'=>"clearKecamatan();clearKelurahan();",)); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addPropinsi(); $('#dialogAddPropinsi').dialog('open');}",
                                                              'id'=>'btnAddPropinsi','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('propinsi_id'))) ?>
                        <?php echo $form->error($modPasien, 'propinsi_id'); ?>
                    </div>
                </div>

                <div class="control-group ">
                    <?php echo $form->labelEx($modPasien,'kabupaten_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $modPasien->kabupaten_id = (!empty($modPasien->kabupaten_id))?$modPasien->kabupaten_id:Yii::app()->user->getState('kabupaten_id');?>
                        <?php echo $form->dropDownList($modPasien,'kabupaten_id',CHtml::listData($modPasien->getKabupatenItems($modPasien->propinsi_id), 'kabupaten_id', 'kabupaten_nama'),
                                                          array('class'=>'reqPasien','empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modPasien, 'kecamatan_id')."','".CHtml::activeId($modPasien, 'kabupaten_id')."')",
                                                                'ajax'=>array('type'=>'POST',
                                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                              'update'=>'#PPPasienM_kecamatan_id'),
                                                                'onchange'=>"clearKelurahan();",)); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addKabupaten(); $('#dialogAddKabupaten').dialog('open');}",
                                                              'id'=>'btnAddKabupaten','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kabupaten_id'))) ?>
                        <?php echo $form->error($modPasien, 'kabupaten_id'); ?>
                    </div>
                </div>

                <div class="control-group ">
                    <?php echo $form->labelEx($modPasien,'kecamatan_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php //$modPasien->kecamatan_id = (!empty($modPasien->kecamatan_id))?$modPasien->kecamatan_id:Yii::app()->user->getState('kecamatan_id');?>
                        <?php //echo $form->dropDownList($modPasien,'kecamatan_id', CHtml::listData($modPasien->getKecamatanItems($modPasien->kabupaten_id), 'kecamatan_id', 'kecamatan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                       
                        <?php echo $form->dropDownList($modPasien,'kecamatan_id',CHtml::listData($modPasien->getKecamatanItems($modPasien->kabupaten_id), 'kecamatan_id', 'kecamatan_nama'),
                                                          array('class'=>'reqPasien','empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modPasien, 'kelurahan_id')."','".CHtml::activeId($modPasien, 'kecamatan_id')."')",
                                                                'ajax'=>array('type'=>'POST',
                                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                              'update'=>'#PPPasienM_kelurahan_id'))); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addKecamatan(); $('#dialogAddKecamatan').dialog('open');}",
                                                              'id'=>'btnAddKecamatan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kecamatan_id'))) ?>
                        <?php echo $form->error($modPasien, 'kecamatan_id'); ?>
                    </div>
                </div>

                <div class="control-group ">
                    <?php echo $form->labelEx($modPasien,'kelurahan_id', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $modPasien->kelurahan_id = (!empty($modPasien->kelurahan_id))?$modPasien->kelurahan_id:Yii::app()->user->getState('kelurahan_id');?>
                        <?php echo $form->dropDownList($modPasien,'kelurahan_id',CHtml::listData($modPasien->getKelurahanItems($modPasien->kecamatan_id), 'kelurahan_id', 'kelurahan_nama'),
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modPasien, 'agama')."','".CHtml::activeId($modPasien, 'kelurahan_id')."')")); ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                        array('class'=>'btn btn-primary','onclick'=>"{addKelurahan(); $('#dialogAddKelurahan').dialog('open');}",
                                                              'id'=>'btnAddKelurahan','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'rel'=>'tooltip','title'=>'Klik untuk menambah '.$modPasien->getAttributeLabel('kelurahan_id'))) ?>
                        <?php echo $form->error($modPasien, 'kelurahan_id'); ?>
                    </div>
                </div>

                <?php //echo $form->textFieldRow($modPasien,'tgl_rekam_medik'); ?>
                <?php //echo $form->textFieldRow($modPasien,'statusrekammedis'); ?>
                <?php $modPasien->agama = (!empty($modPasien->agama))?$modPasien->agama:Params::DEFAULT_AGAMA;?>
                <?php echo $form->dropDownListRow($modPasien,'agama', Agama::items(),array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->dropDownListRow($modPasien,'pendidikan_id', CHtml::listData($modPasien->getPendidikanItems(), 'pendidikan_id', 'pendidikan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->dropDownListRow($modPasien,'pekerjaan_id', CHtml::listData($modPasien->getPekerjaanItems(), 'pekerjaan_id', 'pekerjaan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'onchange'=>'cekStatusPekerjaan(this)', 'class'=>'reqPasien')); ?>
                <?php $modPasien->warga_negara = (!empty($modPasien->warga_negara))?$modPasien->warga_negara:Params::DEFAULT_WARGANEGARA;?>
                <?php echo $form->dropDownListRow($modPasien,'warga_negara', WargaNegara::items(),array('class'=>'reqPasien','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                <?php //echo $form->textFieldRow($modPasien,'tgl_meninggal'); ?>

            <fieldset id="fieldsetDetailPasien" class="">
                <legend class="accord1"><?php echo CHtml::checkBox('cex_detaildatapasien', false, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Detail Data Pasien 
                </legend>
                <div id='detail_data_pasien' class="toggle">
                <?php //echo $form->textFieldRow($modPasien,'anakke',array('class'=>'span1','maxlength'=>2, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <div class="control-group ">
                    <?php echo $form->labelEx($modPasien,'anakke', array('class'=>'control-label')) ?>
                    <div class="controls">

                    <?php echo $form->textField($modPasien,'anakke', array('class'=>'span1 numbersOnly','maxlength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event)", )).' dari '; ?> 
                    <?php echo $form->textField($modPasien,'jumlah_bersaudara', array('class'=>'span1 numbersOnly','maxlength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event)", )).' bersaudara'; ?>
                    <?php //echo CHtml::button('', array('class'=>'buttonTambahIcon','onclick'=>"{addPropinsi(); $('#dialogAddPropinsi').dialog('open');}",'id'=>'btnAddPropinsi',)) ?>
                        <?php echo $form->error($modPasien, 'anakke'); ?><?php echo $form->error($modPasien, 'jumlah_bersaudara'); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modPasien,'jumlah_bersaudara',array('class'=>'span1','maxlength'=>2, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->dropDownListRow($modPasien,'suku_id', CHtml::listData($modPasien->getSukuItems(), 'suku_id', 'suku_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <!--    <img id="imgFotoPasien" src="" alt="photo pasien">-->
                <?php //echo $form->fileFieldRow($modPasien,'photopasien', array('onchange'=>'previewFoto(this)','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($modPasien,'alamatemail', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat E-mail')); ?>

                <?php
                    $cekRI = explode('/', $_GET['r']);
                    if(!isset($cekRI[2]) || $cekRI[2]!='RawatInap'){
                ?>
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
                        <div class="label_no">
                        <?php echo CHtml::checkBox('nik_pegawai_cek', false, array('rel'=>'tooltip','title'=>'NIK Pegawai','onclick'=>'aktifkan_nik()', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            NIK Pegawai                    </div>
                        </label>
                        <div class="controls">
                            <?php echo CHtml::textField('PPPasienM[nik_pegawai]','', 
                                array(
                                    'onblur'=>'cek_pegawai(this)',
                                    'id'=>'nik_pegawai',
                                    'readonly'=>'readonly',
                                )); ?>                        
                        </div>
                    </div>
                <?php } ?>  
                </div>
            </fieldset>
            </td>
        </tr>
    </table>
</fieldset>


<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlGetDaerah = Yii::app()->createUrl('ActionAjax/getListDaerahPasien');
 $idTagUmur = CHtml::activeId($model,'umur');
$idTagUmurThn = CHtml::activeId($model,'thn');
$idTagUmurBln = CHtml::activeId($model,'bln');
$idTagUmurHr = CHtml::activeId($model,'hr');
$propinsiId = Yii::app()->user->getState('propinsi_id');
$kabupatenId = Yii::app()->user->getState('kabupaten_id');
$kecamatanId = Yii::app()->user->getState('kecamatan_id');
$agama = Params::DEFAULT_AGAMA;
$wargaNegara = Params::DEFAULT_WARGANEGARA;
$js = <<< JS
function previewFoto(obj)
{
    var pathFile = $(obj).val();
    $('#imgFotoPasien').attr('src','file://'+pathFile);
    $('#imgFotoPasien').load();
    $('#tempPhoto').load();
}

function enableInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien input').removeAttr('checked');
        $('#fieldsetPasien #isUpdatePasien').hide();
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        
        $('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');

        $('#noRekamMedik').val('');
        $('#fieldsetPasien input').not(':radio').val('');
        $('#fieldsetPasien select').val('');
        $('#fieldsetPasien textarea').val('');
        $('#fieldsetPasien button').val('');
        $('#fieldsetDetailPasien input').val('');
        $('#fieldsetDetailPasien select').val('');
        $('#tombolPasienDialog').addClass('hide');
        //$('#isPasienLama').attr('disabled','true');
        
        
        $('#isPasienBaru').removeAttr('disabled','true');
        $('#isPasienBaru').attr('checked','checked');
        $('#tombolPasienDialogBaru').removeClass('hide');
        $('#noRekamMedikLama').removeAttr('readonly','true');
        $('#controlNoRekamMedik button').removeAttr('disabled','true');
    }
    else {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').show();
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('readonly');
        $('#detail_data_pasien').slideDown(500);
        $('#cex_detaildatapasien').attr('checked','checked');
        $('#tombolPasienDialog').removeClass('hide');
        $('#noRekamMedik').val('');
        $('#noRekamMedik').focus();

        $('#noRekamMedikLama').attr('readonly','true');
        $('#controlNoRekamMedikLama button').attr('disabled','true');
        $('#tombolPasienDialogBaru').addClass('hide');
        $('#isPasienBaru').attr('disabled','true');
    }
}

//enableInputPasienBaru ada bugs di gantikan dengan pilihNoRm()
function enableInputPasienBaru(obj)
{
    if($('#isPasienBaru').is(':checked')){
        $('#noRekamMedikLama').removeAttr('readonly','true');
        $('#controlNoRekamMedikLama button').removeAttr('disabled','true');
        $('#tombolPasienDialogBaru').removeClass('hide');

        $('#isPasienLama').attr('disabled','true');
        $('#isPasienBaru').removeAttr('disabled','true');
    }else{
        $('#noRekamMedikLama').attr('readonly','true');
        $('#controlNoRekamMedikLama button').attr('disabled','true');
        $('#tombolPasienDialogBaru').addClass('hide');
        $('#isPasienBaru').attr('disabled','true');

        $('#isPasienLama').removeAttr('disabled','true');
        $('#isPasienLama').attr('checked','checked');
        $('#noRekamMedik').removeAttr('readonly','true');
        $('#controlNoRekamMedik button').removeAttr('disabled','true');
        $('#tombolPasienDialog').removeClass('hide');
   }
}

function resetFormPasien(){
    $('#fieldsetPasien input').not(':radio').val('');
    $('#fieldsetPasien select').val('');
    $('#fieldsetPasien textarea').val('');
    $('#fieldsetPasien button').val('');
    $('#fieldsetDetailPasien input').val('');
    $('#fieldsetDetailPasien select').val('');
    $('#detail_data_pasien').slideUp(500);
    $('#cex_detaildatapasien').removeAttr('checked');
    $('#fieldsetPasien #isUpdatePasien').hide();
    $('#PPPasienM_jenisidentitas').val('LAINNYA');
    $('#PPPasienM_propinsi_id').val(${propinsiId});
    $('#PPPasienM_kabupaten_id').val(${kabupatenId});
    $('#PPPasienM_kecamatan_id').val(${kecamatanId});
    $('#PPPasienM_agama').val('${agama}');
    $('#PPPasienM_warga_negara').val('${wargaNegara}');
    $('#PPPendaftaranRi_thn').val('00');
    $('#PPPendaftaranRi_bln').val('00');
    $('#PPPendaftaranRi_hr').val('00');

}

function disableFormPasien(stat){
    if(stat == true){
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').show();
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#detail_data_pasien').slideDown(500);
        $('#cex_detaildatapasien').attr('checked','checked');
    }else{
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien input').removeAttr('checked');
        $('#fieldsetPasien #isUpdatePasien').hide();
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#detail_data_pasien').slideUp(500);
        $('#cex_detaildatapasien').removeAttr('checked');
    }
}

function disableNoRm(){
        $('#noRekamMedikLama').val('');
        $('#noRekamMedik').val('');
        $('#isPasienBaru').removeAttr('checked');
        $('#isPasienLama').removeAttr('checked');
        $('#noRekamMedik').attr('readonly', 'true');
        $('#tombolPasienDialog').addClass('hide');
        $('#noRekamMedikLama').attr('readonly', 'true');
        $('#tombolPasienDialogBaru').addClass('hide');
}

function pilihNoRm(){
    if($('#isPasienLama').is(':checked')){
        $('#noRekamMedik').removeAttr('readonly', 'true');
        $('#tombolPasienDialog').removeClass('hide');
        $('#noRekamMedik').focus();
        $('#noRekamMedikLama').attr('readonly', 'true');
        $('#tombolPasienDialogBaru').addClass('hide');
        $('#noRekamMedikLama').val('');
        disableFormPasien(true);
        
    }else if($('#isPasienBaru').is(':checked')){
        $('#noRekamMedikLama').removeAttr('readonly', 'true');
        $('#tombolPasienDialogBaru').removeClass('hide');
        $('#noRekamMedikLama').focus();
        $('#noRekamMedik').attr('readonly', 'true');
        $('#tombolPasienDialog').addClass('hide');
        $('#noRekamMedik').val('');
        disableFormPasien(true);
    }else{
        disableNoRm();
        disableFormPasien(false);
        resetFormPasien();
    }
    //jika di centang dua duanya    
    if(($('#isPasienLama').is(':checked')) && ($('#isPasienBaru').is(':checked'))){
        alert("Anda Harus Memilih Salah satu No RM" );
        disableNoRm();
        resetFormPasien();
        disableFormPasien(false);
    }
}

function cekFieldNoRm(){
    var noRm = "ada";
    var idField = "";
    if($('#isPasienLama').is(':checked')){
        idField = '#noRekamMedik';
        noRm = ($('#noRekamMedik').val()).trim();
    }else if($('#isPasienBaru').is(':checked')){
        idField = '#noRekamMedikLama';
        noRm = ($('#noRekamMedikLama').val()).trim();
    }
    
    if(noRm.length == 0){
        alert("Silahkan isi field No. RM");
        if(idField.length > 0){
            $(idField).focus();
        }
        return false;
    }
    return true;
}
        
function updateInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        //$('#controlNoRekamMedik button').removeAttr('disabled');
        //$('#noRekamMedik').removeAttr('readonly');
        //$('#detail_data_pasien').slideDown(500);
        //$('#cex_detaildatapasien').attr('checked','checked');
    }
    else {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        //$('#controlNoRekamMedik button').attr('disabled','true');
        //$('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');
    }
}

function getTglLahir(obj)
{
    //alert(obj.value);
    var str = obj.value;
    // obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{thn: $("#${idTagUmurThn}").val(),bln: $("#${idTagUmurBln}").val(),hr: $("#${idTagUmurHr}").val()},
        function(data){
            $('#PPPasienM_tanggal_lahir').val(data.tglLahir);
           // $("#${idTagUmurThn}").val(data.thn);
           // $("#${idTagUmurBln}").val(data.bln);
           // $("#${idTagUmurHr}").val(data.hr);

    },"json");
}



function getUmur(obj)
{
    // alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){
            // $("#PPPendaftaranRj_thn).val(data.thn);
            // $("#PPPendaftaranRj_bln).val(data.bln);
            // $("#PPPendaftaranRj_hr).val(data.hr);

           $("#${idTagUmurThn}").val(data.thn);
           $("#${idTagUmurBln}").val(data.bln);
           $("#${idTagUmurHr}").val(data.hr);

           // $('#PPPendaftaranRj_umur').val(data.umur); 
           // $('#PPPendaftaranMp_umur').val(data.umur); 
           // $('#PPPendaftaranRd_umur').val(data.umur); 

           // $("#${idTagUmur}").val(data.umur);
    },"json");
}

function loadUmur(tglLahir)
{   
    $.post("${urlGetUmur}",{tglLahir: tglLahir},
        function(data){
            $("#${idTagUmur}").val(data.umur);

           $("#${idTagUmurThn}").val(data.thn);
           $("#${idTagUmurBln}").val(data.bln);
           $("#${idTagUmurHr}").val(data.hr);
    },"json");
}

function setJenisKelaminPasien(jenisKelamin)
{
    $('input[name="PPPasienM[jeniskelamin]"]').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
    $('.jk').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
}

function setRhesusPasien(rhesus)
{
    $('input[name="PPPasienM[rhesus]"]').each(function(){
            if(this.value == rhesus)
                $(this).attr('checked',true);
        }
    );
    $('.rhesus').each(function(){
            if(this.value == rhesus)
                $(this).attr('checked',true);
        }
    );
}

function loadDaerahPasien(idProp,idKab,idKec,idKel)
{
    $.post("${urlGetDaerah}", { idProp: idProp, idKab: idKab, idKec: idKec, idKel: idKel },
        function(data){
            $('#PPPasienM_propinsi_id').html(data.listPropinsi);
            $('#PPPasienM_kabupaten_id').html(data.listKabupaten);
            $('#PPPasienM_kecamatan_id').html(data.listKecamatan);
            $('#PPPasienM_kelurahan_id').html(data.listKelurahan);
    }, "json");
}

function clearKecamatan()
{
    $('#PPPasienM_kecamatan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}

function clearKelurahan()
{
    $('#PPPasienM_kelurahan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_HEAD);

$enableInputPasien = ($model->isPasienLama) ? 1 : 0;
$js = <<< JS
        
if(${enableInputPasien}) {
    $('#fieldsetPasien input').attr('disabled','true');
    $('#fieldsetPasien #isUpdatePasien').show();
    $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetPasien select').attr('disabled','true');
    $('#fieldsetPasien textarea').attr('disabled','true');
    $('#fieldsetDetailPasien input').attr('disabled','true');
    $('#fieldsetDetailPasien select').attr('disabled','true');
    $('#noRekamMedik').removeAttr('readonly');
    $('#controlNoRekamMedik button').removeAttr('disabled');
    $('#fieldsetPasien button').attr('disabled','true');
}
else {
    $('#fieldsetPasien input').removeAttr('disabled');
    $('#fieldsetPasien #isUpdatePasien').hide();
    $('#fieldsetPasien select').removeAttr('disabled');
    $('#fieldsetPasien textarea').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#noRekamMedik').attr('readonly','true');
    $('#controlNoRekamMedik button').attr('disabled','true');
    $('#fieldsetPasien button').removeAttr('disabled');
}
//disableNoRm();
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_READY);
?>

<?php 
//========= Dialog buat cari data pasien =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));
 
//$this->beginWidget('ext.bootstrap.widgets.BootModal',
//    array(
//	'id'=>'dialogPasien',
//	'config'=>array(
//            'header'=>'Pencarian Data Pasien',
//        ),
//	'htmlOptions'=>array(
//            'style'=>'width:850px'
//        ),
//    )
//);

$modDataPasien = new PPPasienM('searchWithDaerah');
$modDataPasien->unsetAttributes();

$modDataPasien->nama_pasien = 'RSJK';
$modDataPasien->tipe_pencarian = "depan";

if(isset($_GET['PPPasienM'])) {
    $modDataPasien->attributes = $_GET['PPPasienM'];
	$modDataPasien->tipe_pencarian = $_GET['PPPasienM']['tipe_pencarian'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchWithDaerah(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "cekDaftar($data->pasien_id, $data->no_rekam_medik)
                                        $(\"#dialogPasien\").dialog(\"close\");
                                        //$(\"#dialogPasien\").modal(\"hide\");
                                        $(\"#noRekamMedik\").val(\"$data->no_rekam_medik\");
                                        $(\"#PPPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                        $(\"#PPPasienM_rhesus\").val(\"$data->rhesus\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                        $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                        setJenisKelaminPasien(\"$data->jeniskelamin\");
                                        setRhesusPasien(\"$data->rhesus\");
                                        kelurahan = String.trim(\'$data->kelurahan_id\');
                                        if(kelurahan.length == 0){
                                            kelurahan = 0;
                                        }
//                                        alert($data->jeniskelamin);
                                        loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id,kelurahan);
                                        $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'namadepan').'\").val(\"$data->namadepan\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_bin').'\").val(\"$data->nama_bin\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_ayah').'\").val(\"$data->nama_ayah\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_ibu').'\").val(\"$data->nama_ibu\");
                                        $(\"#'.CHtml::activeId($modPasien,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                        $(\"#'.CHtml::activeId($modPasien,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                        $(\"#'.CHtml::activeId($modPasien,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                        $(\"#'.CHtml::activeId($modPasien,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                        $(\"#'.CHtml::activeId($modPasien,'golongandarah').'\").val(\"$data->golongandarah\");
                                        $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                        $(\"#'.CHtml::activeId($modPasien,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'rt').'\").val(\"$data->rt\");
                                        $(\"#'.CHtml::activeId($modPasien,'rw').'\").val(\"$data->rw\");
                                        $(\"#'.CHtml::activeId($modPasien,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                        $(\"#PPPasienM_kelurahan_id\").val(\"$data->kelurahan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'suku_id').'\").val(\"$data->suku_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'alamatemail').'\").val(\"$data->alamatemail\");
                                        $(\"#'.CHtml::activeId($modPasien,'anakke').'\").val(\"$data->anakke\");
                                        $(\"#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                        $(\"#'.CHtml::activeId($modPasien,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'agama').'\").val(\"$data->agama\");
                                        $(\"#'.CHtml::activeId($modPasien,'warga_negara').'\").val(\"$data->warga_negara\");
                                        photo = String.trim(\'$data->photopasien\');
                                        if(photo.length > 0)
                                        {
                                            $(\"#cekFoto\").attr(\"src\", \"http://localhost/ehospitaljk/data/images/pasien/$data->photopasien\");
                                        } else
                                        {
                                            $(\"#cekFoto\").attr(\"src\", \"http://localhost/ehospitaljk/data/images/pasien/no_photo.jpeg\");
                                        }
                                        loadUmur(\"$data->tanggal_lahir\");
                                        ubahUmur($data->CekUmurValid);
                                    "))',
                ),
				'no_rekam_medik',
                array(
                    'name'=>'nama_pasien',
					'filter'=>CHtml::activeTextField($modDataPasien,"nama_pasien",array()) . 
						CHtml::activeDropDownList($modDataPasien,"tipe_pencarian",array("depan"=>"Nama Depan", "belakang"=>"Nama Belakang", "semua"=>"Semua"), array()),
                ),
                'nama_bin',
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>
<?php 
// Dialog buat nambah data propinsi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddPropinsi',
    'options'=>array(
        'title'=>'Menambah data Propinsi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>450,
        'minHeight'=>350,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end propinsi dialog =============================

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKabupaten',
    'options'=>array(
        'title'=>'Menambah data Kabupaten',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKabupaten"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

// Dialog buat nambah data kecamatan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKecamatan',
    'options'=>array(
        'title'=>'Menambah data Kecamatan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKecamatan"></div>';


$this->endWidget();
//========= end kecamatan dialog =============================

// Dialog buat nambah data kelurahan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKelurahan',
    'options'=>array(
        'title'=>'Menambah data Kelurahan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKelurahan"></div>';


$this->endWidget();
//========= end kelurahan dialog =============================
?>
<script type="text/javascript">
// here is the magic
function aktifkan_nik()
{
    if($('#nik_pegawai_cek').is(':checked')){
        $('#nik_pegawai').removeAttr('readonly');        
        $('#nik_pegawai').focus();
    }else{
        $('#nik_pegawai').val('');
        $('#nik_pegawai').attr('readonly','true');
        $('#nik_pegawai_cek').removeAttr('checked');
    }
}
function cek_pegawai(obj)
{
    var nik = obj.value;
    if(nik!=''){
        $.post('<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/ActionAjax/cariPegawai');?>', {nik: nik}, function(data){
                alert(data.hasil);
                if(!data.status){
                    $('#nik_pegawai').val('');
                    $('#nik_pegawai').focus();
                }
            }, 
        'json');
    }else{
        // $('#nik_pegawai_cek').removeAttr('checked');
        $('#nik_pegawai').attr('readonly','true');    
    }
}

function convertToUpper(obj)
{
    var string = obj.value;
    $(obj).val(string.toUpperCase());
}

function setNamaGelar()
{
    var statusperkawinan = $('#PPPasienM_statusperkawinan').val();
    var namadepan = $('#PPPasienM_namadepan');
    var umur = $("#<?php echo CHtml::activeId($model,'thn');?>").val();
    // umur = parseInt(umur);
    
    if(umur == 0){
        var namadepan = $('#PPPasienM_namadepan').val('By.');
        if(statusperkawinan.length > 0 && statusperkawinan != "DIBAWAH UMUR"){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else if(umur <= 14){ //
        var namadepan = $('#PPPasienM_namadepan').val('An.');
        if(statusperkawinan.length > 0 && statusperkawinan != "DIBAWAH UMUR"){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else{
        if($('#PPPasienM_jeniskelamin_0').is(':checked')){
            if(statusperkawinan !== 'JANDA'){
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }
            
        }
        
        if($('#PPPasienM_jeniskelamin_1').is(':checked')){
            if(statusperkawinan !== 'DUDA'){
                if(statusperkawinan === 'KAWIN' || statusperkawinan == 'JANDA' || statusperkawinan == 'NIKAH SIRIH' || statusperkawinan == 'POLIGAMI'){
                    var namadepan = $('#PPPasienM_namadepan').val('Ny.');
                }else{
                    var namadepan = $('#PPPasienM_namadepan').val('Nn');
                }                
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Ny.');
            }
        }
        
        if (statusperkawinan == "DIBAWAH UMUR"){
            alert('Pilih status pernikahan yang sesuai');
            $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
        }
    }
    
}

function cekJenisKelamin(obj)
{
    var is_true = true;
    var namadepan = $('#PPPasienM_namadepan').val();
    if(namadepan.length != 0)
    {
        if(obj.value == 'PEREMPUAN')
        {
            if(namadepan != 'Nn.' && namadepan != 'Ny.' && namadepan != 'By.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_0').attr('checked',true);
                is_true = false;
            }
        }else{
            if(namadepan != 'Tn.' && namadepan != 'An.' && namadepan != 'By.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_1').attr('checked',true);
                is_true = false;
            }
        }
    }else{
        $(obj).attr('checked',false);
        alert('Pilih gelar kehormatan terlebih dahulu');
    }
}

function setValueStatus(obj)
{
    var gelar = obj.value;
    if(gelar === 'Tn.')
    {
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('KAWIN');
        
    }else if(gelar === 'An.'){
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
    }else{
        if(gelar === 'Nn' || gelar === 'By.')
        {
            $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
        }else{
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
        $('#PPPasienM_jeniskelamin_1').attr('checked',true);
    }
}

function setStatusPerkawinan(obj)
{
    var status = obj.value;
    var namaDepan = $('#PPPasienM_namadepan').val();
    
    if(status === 'BELUM KAWIN')
    {
        if(namaDepan !== 'An.' && namaDepan !== 'Nn' && namaDepan !== 'By.')
        {
            alert('Pilih status perkawinan yang sesuai');
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
    }else{
        if(status === 'KAWIN' || status === 'NIKAH SIRIH' || status === 'POLIGAMI')
        {
            if(namaDepan !== 'Tn.' && namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
            }
        }
        else if(status === 'JANDA')
        {
            if(namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Tn.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
        else if(status === 'DUDA')
        {
            if(namaDepan !== 'Tn.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Ny.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
    }
}

function cekStatusPekerjaan(obj)
{
    var namaDepan = $('#PPPasienM_namadepan').val();
    var namaPekerjaan = obj.value;
    var umur = $("#<?php echo CHtml::activeId($model,'thn');?>").val();
    // umur = parseInt(umur);
    
    if(namaDepan.length > 0)
    {
        if(umur < 15){
            if(namaPekerjaan !== '12'){
                if(namaPekerjaan !== ''){
                    alert('Pasien masih di bawah umur, coba cek ulang');
                }
                $(obj).val('');
            }else{
                $(obj).val(namaPekerjaan);
            }
        }else{
            if(namaPekerjaan === '12'){
                if(namaDepan === 'Ny.'){
                    $(obj).val('9');
                }else if(namaDepan === 'Nn' && namaPekerjaan === '9'){
                    alert('Pasien belum menikah, coba cek ulang');
                    $(obj).val('');
                }else{
                    $(obj).val('');
                }
                alert('Pilih pekerjaan yang tepat');
            }else{
                if(namaPekerjaan === '9'){
                    if(namaDepan !== 'Ny.'){
                        alert('Pasien seorang laki - laki, coba cek ulang');
                        $(obj).val('');                        
                    }
                }
            }
        }
/*        
        if(namaPekerjaan === '12' && umur < 17)
        {
            if(namaDepan !== 'BY. Ny.' && namaDepan !== 'An.' && namaDepan !== 'Nn')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }
        }else{
            if(namaDepan === 'BY. Ny.')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }else{
                if(namaPekerjaan === '11' || namaPekerjaan === '10')
                {
                    if(namaDepan !== 'An.' && namaDepan !== 'Nn'){
                        alert('Pilih pekerjaan yang sesuai');
                        $(obj).val('');
                    }
                }else{
                    if(namaPekerjaan !== '13' && namaPekerjaan !== '14')
                    {
                        if(namaPekerjaan === '9' && namaDepan !== 'Ny.')
                        {
                            alert('Pilih pekerjaan yang sesuai');
                            $(obj).val('');
                        }else{
                            if((namaDepan === 'An.' || namaDepan === 'Nn') && umur < 25){
                                alert('Pilih pekerjaan yang sesuai');
                                $(obj).val('');
                            }                        
                        }
                    }
                }
            }
        }
*/
    }else{
        $(obj).val('');
        alert('Pilih gelar kehormatan terlebih dahulu');
    }

}

function addPropinsi()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addPropinsi'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#dialogAddPropinsi div.divForForm form').submit(addPropinsi);
                }
                else
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#PPPasienM_propinsi_id').html(data.propinsi);
                    setTimeout(\"$('#dialogAddPropinsi').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKabupaten()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKabupaten'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#dialogAddKabupaten div.divForFormKabupaten form').submit(addKabupaten);
                }
                else
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#PPPasienM_kabupaten_id').html(data.kabupaten);
                    setTimeout(\"$('#dialogAddKabupaten').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKecamatan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKecamatan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#dialogAddKecamatan div.divForFormKecamatan form').submit(addKecamatan);
                }
                else
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#PPPasienM_kecamatan_id').html(data.kecamatan);
                    setTimeout(\"$('#dialogAddKecamatan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKelurahan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKelurahan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#dialogAddKelurahan div.divForFormKelurahan form').submit(addKelurahan);
                }
                else
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#PPPasienM_kelurahan_id').html(data.kelurahan);
                    setTimeout(\"$('#dialogAddKelurahan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function cariDataPasien(noRekammedik)
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/cariPasien');?>', {norekammedik: noRekammedik}, function(data){
        isiDataPasien(data);
    }, 'json');
}

function isiDataPasien(data)
{
    $("#<?php echo CHtml::activeId($modPasien,'pasien_id');?>").val(data.pasien_id);
    $("#<?php echo CHtml::activeId($modPasien,'jenisidentitas');?>").val(data.jenisidentitas);
    $("#<?php echo CHtml::activeId($modPasien,'no_identitas_pasien');?>").val(data.no_identitas_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'namadepan');?>").val(data.namadepan);
    $("#<?php echo CHtml::activeId($modPasien,'nama_pasien');?>").val(data.nama_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'nama_bin');?>").val(data.nama_bin);
    $("#<?php echo CHtml::activeId($modPasien,'tempat_lahir');?>").val(data.tempat_lahir);
    $("#<?php echo CHtml::activeId($modPasien,'tanggal_lahir');?>").val(data.tanggal_lahir);
    $("#<?php echo CHtml::activeId($modPasien,'kelompokumur_id');?>").val(data.kelompokumur_id);
    $("#<?php echo CHtml::activeId($modPasien,'jeniskelamin');?>").val(data.jeniskelamin);
    $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.rhesus);
    $("#<?php echo CHtml::activeId($model,'pendaftaran_id');?>").val(data.pendaftaran.pendaftaran_id);
    $("#<?php echo CHtml::activeId($model,'no_pendaftaran');?>").val(data.pendaftaran.no_pendaftaran);
    
    setJenisKelaminPasien(data.jeniskelamin);
    setRhesusPasien(data.rhesus);
    var kelurahan = data.kelurahan_id;
    
    loadDaerahPasien(data.propinsi_id, data.kabupaten_id, data.kecamatan_id, data.kelurahan_id);
        $("#<?php echo CHtml::activeId($modPasien,'statusperkawinan');?>").val(data.statusperkawinan);
    $("#<?php echo CHtml::activeId($modPasien,'golongandarah');?>").val(data.golongandarah);
    $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.rhesus);
    $("#<?php echo CHtml::activeId($modPasien,'alamat_pasien');?>").val(data.alamat_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'rt');?>").val(data.rt);
    $("#<?php echo CHtml::activeId($modPasien,'rw');?>").val(data.rw);
    $("#<?php echo CHtml::activeId($modPasien,'propinsi_id');?>").val(data.propinsi_id);
    $("#<?php echo CHtml::activeId($modPasien,'kabupaten_id');?>").val(data.kabupaten_id);
    $("#<?php echo CHtml::activeId($modPasien,'kecamatan_id');?>").val(data.kecamatan_id);
    $("#<?php echo CHtml::activeId($modPasien,'kelurahan_id');?>").val(data.kelurahan_id);
    $("#<?php echo CHtml::activeId($modPasien,'no_telepon_pasien');?>").val(data.no_telepon_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'no_mobile_pasien');?>").val(data.no_mobile_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'suku_id');?>").val(data.suku_id);
    $("#<?php echo CHtml::activeId($modPasien,'alamatemail');?>").val(data.alamatemail);
    $("#<?php echo CHtml::activeId($modPasien,'anakke');?>").val(data.anakke);
    $("#<?php echo CHtml::activeId($modPasien,'jumlah_bersaudara');?>").val(data.jumlah_bersaudara);
    $("#<?php echo CHtml::activeId($modPasien,'pendidikan_id');?>").val(data.pendidikan_id);
    $("#<?php echo CHtml::activeId($modPasien,'pekerjaan_id');?>").val(data.pekerjaan_id);
    $("#<?php echo CHtml::activeId($modPasien,'agama');?>").val(data.agama);
    $("#<?php echo CHtml::activeId($modPasien,'warga_negara');?>").val(data.warga_negara);
    $("#<?php echo CHtml::activeId($modPasien,'jeniskelamin');?>").val(data.jeniskelamin);
    $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.rhesus);
    loadUmur(data.tanggal_lahir);
    return false;
}

function ubahUmur(umurValid)
{
    if(!umurValid){
        alert('Tanggal Lahir tidak valid!');
        $('#isUpdatePasien').attr('checked',true);
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('readonly','true');
        $('#PPPasienM_tanggal_lahir').focus();
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');
    }
}

//function validasiEmail(){
//    var email = $('#PPPasienM_alamatemail').val();
//    
//    if ((email.indexOf('@',0)==-1) || (email.indexOf('.',0)==-1)){
//    alert("Email Kurang Tepat");
//}

$('#PPPasienM_namadepan').click(function(){
    document.getElementById('PPPasienM_nama_pasien').focus();
});

</script>

<?php Yii::app()->clientScript->registerScript('detail_data_pasien',"
    $('#detail_data_pasien').hide();
    $('#cex_detaildatapasien').change(function(){
        if ($(this).is(':checked')){
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').removeAttr('disabled');
                $('#fieldsetDetailPasien select').removeAttr('disabled');
        }else{
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').attr('disabled','true');
                $('#fieldsetDetailPasien select').attr('disabled','true');
                $('#fieldsetDetailPasien input').attr('value','');
                $('#fieldsetDetailPasien select').attr('value','');
        }
        $('#detail_data_pasien').slideToggle(500);
    });
");
?>

<?php 
//========= Dialog buat cari data pasien =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasienLama',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

/*$this->beginWidget('ext.bootstrap.widgets.BootModal',
    array(
	'id'=>'dialogPasienLama',
	'config'=>array(
            'header'=>'Pencarian Pasien',
        ),
	'htmlOptions'=>array(
            'style'=>'width:850px'
        ),
    )
);
*/
$modDataPasienLama = new PPPasiennonaktifV('search');
$modDataPasienLama->unsetAttributes();
if(isset($_GET['PPPasiennonaktifV'])) {
    $modDataPasienLama->attributes = $_GET['PPPasiennonaktifV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasienlama-m-grid',
	'dataProvider'=>$modDataPasienLama->search(),
	'filter'=>$modDataPasienLama,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasienLama",
                                    "onClick" => "
                                        $(\"#dialogPasienLama\").dialog(\"close\");
                                        $(\"#noRekamMedikLama\").val(\"$data->no_rekam_medik\");
                                        $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");                                        
                                    "))',
                ),
                'no_rekam_medik',
                'nama_pasien',
                'tgl_rekam_medik',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>
