
<fieldset id="fieldsetAdmisi">
    <legend class="rim">Admisi Ruangan
    </legend>
    <div id="divAdmisi">
        <div class="control-group ">
            <?php $minDate = (Yii::app()->user->getState('tgltransaksimundur')) ? '' : 'd'; ?>
            <?php echo $form->labelEx($modPasienAdmisi,'tgladmisi', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPasienAdmisi,
                                        'attribute'=>'tgladmisi',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
//                                            'minDate' => $minDate,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3 reqAdmisi'),
                )); ?>
                <?php echo $form->error($modPasienAdmisi, 'tgladmisi'); ?>
            </div>
        </div>
        <?php if(!empty($_POST['bookingkamar_id'])) {
                echo $form->hiddenField($modPasienAdmisi,'bookingkamar_id', array('onkeypress'=>"return $(this).focusNextInputField(event)"));
             }
        ?>
        <?php 
//        echo $form->dropDownListRow($modPasienAdmisi,'ruangan_id', CHtml::listData($modPasienAdmisi->getRuanganItems(Params::INSTALASI_ID_RI), 'ruangan_id', 'ruangan_nama') ,
//                                                      array('empty'=>'-- Pilih --',
//                                                            'ajax'=>array('type'=>'POST',
//                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKamarKosong',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT')),
//                                                                          'update'=>'#PPPasienAdmisiT_kamarruangan_id'),
//                                                            'onchange'=>"listDokterRuangan(this.value);listKasusPenyakitRuangan(this.value);updateKamar()",
//                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); 
        ?>
        <table>
         <tr>
             <td width="143px">
                                            <div align="right">
                                                <!-- function getListDokterPoli ada di partial _formPasien -->
                                                <i onclick="getListKamar(); $('#dialogListKamar').dialog('open');" class="icon-file" style='cursor:pointer;'></i>
                                                    <?php echo $form->Label($modPasienAdmisi,'ruangan_id')."<font color=red> *</font>";?>    
         <td>
                     <?php echo $form->dropDownList($modPasienAdmisi,'ruangan_id', CHtml::listData($modPasienAdmisi->getRuanganItems(Params::INSTALASI_ID_RI), 'ruangan_id', 'ruangan_nama') ,
                                              array('empty'=>'-- Pilih --',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                                    'onChange'=>'updateKamarRuangan(this.value)',
                                                    'class'=>'span2 reqAdmisi',
                                                    'ajax'=>array(
                                                          'type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKamarKosong',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT')),
                                                          'update'=>'#PPPasienAdmisiT_kamarruangan_id'),
                                                          'onchange'=>"listDokterRuangan(this.value);listKasusPenyakitRuangan(this.value);updateKamar();listKelasPelayananRI(this.value);",
                                                            'onfocus'=>'return cekFieldNoRm();'
                                                  )); ?>
         </td>
        </tr>
        </table>
                   
        <table>
            <tr>
                <td width="3px">
                    <?php echo $form->checkBox($modPasienAdmisi,'masukkamar', array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width :5px;','rel'=>"tooltip",
                                  'title'=>"Pilih jika pasien langsung masuk kamar",'onclick'=>'updateKamar()')); ?>
                </td>
                <td><?php echo $form->LabelEx($modPasienAdmisi,'masuk kamar',array('style'=>'width : 45px;'));?></td>
                <td><?php //echo $form->LabelEx($modPasienAdmisi,'kamarruangan_id');?></td>
                <td><?php 
//                echo $form->dropDownList($modPasienAdmisi,'kamarruangan_id', CHtml::listData($modPasienAdmisi->getKamarKosongItems($modPasienAdmisi->ruangan_id), 'kamarruangan_id', 'KamarDanTempatTidur') ,
//                              array('empty'=>'-- Pilih --',
//                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                    'class'=>'span2')); 
                ?>
                     <?php 
                     $dataKamarRuangan = array();
                     if(!empty($modPasienAdmisi->ruangan_id)) {
                        $kamarKosong = KamarruanganM::model()->findAllByAttributes(array('ruangan_id'=>$ruangan_id,'kamarruangan_status'=>true, 'kamarruangan_aktif'=>true, 'keterangan_kamar'=>'OPEN'));
                        if(!empty($modPasienAdmisi->bookingkamar_id)){
                            $modBookingKamar = BookingkamarT::model()->findByPk($modPasienAdmisi->bookingkamar_id);
                            if ($modBookingKamar){
                                $kamarKosong[] = KamarruanganM::model()->findByPk($modBookingKamar->kamarruangan_id);
                            }
                        }
                        $dataKamarRuangan = CHtml::listData($kamarKosong,'kamarruangan_id','KamarDanTempatTidur');
                    }
                     echo $form->dropDownList($modPasienAdmisi,'kamarruangan_id', $dataKamarRuangan ,
                                              array('empty'=>'-- Pilih --',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'onChange'=>'updateKelasPelayanan(this.value)',
                                                    'class'=>'span2',
                                                    'ajax'=>array(
                                                          'type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/getKelasPelayanan',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT')),
                                                          'update'=>'#PPPasienAdmisiT_kelaspelayanan_id'),
                                                  )); ?>
                </td>
                <td><?php echo $form->checkBox($modPasienAdmisi,'rawatgabung', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
                <td><?php echo $form->LabelEx($modPasienAdmisi,'rawatgabung');?></td>
            </tr>
        </table>


                       
                

        <?php //echo $form->textFieldRow($modPasienAdmisi,'kamarruangan_id', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
     

        
        <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,
                                                      array('class'=>'reqAdmisi',
                                                            'empty'=>'-- Pilih --',
                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        
        <?php echo $form->dropDownListRow($modPasienAdmisi,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('class'=>'reqAdmisi','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>"listKarcisRI(this.value)")); ?>
        <?php //echo $form->textFieldRow($modPasienAdmisi,'pegawai_id', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->dropDownListRow($modPasienAdmisi,'pegawai_id', CHtml::listData($modPasienAdmisi->getDokterItems($modPasienAdmisi->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('class'=>'reqAdmisi','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </div>
</fieldset>
<?php
     $url = Yii::app()->createUrl('actionAjaxRIRD/GetKamarKosong',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));
     $urlAll = Yii::app()->createUrl('actionAjaxRIRD/GetKamarKosongAll',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));
     $urlkelaspelayanan = Yii::app()->createUrl('actionAjaxRIRD/GetKelasPelayanan',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));
     $urlkelaspelayananRI = Yii::app()->createUrl('actionAjaxRIRD/GetKelasPelayananRI',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));

     $urlKamar = Yii::app()->createUrl('ActionDynamic/GetKamarKosong',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT'));
?>
<?php 
// Dialog untuk List Kamar =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogListKamar',
    'options'=>array(
        'title'=>'Informasi Kamar Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1000,
        'minHeight'=>1000,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end List Kamar Dialog =============================
?>

<script>

    var idRuangan = $('#PPPasienAdmisiT_ruangan_id').val();
    var idBookingKamar = $('#PPPasienAdmisiT_bookingkamar_id').val();
    getkamarruangan(idRuangan);
function getkamarruangan(idRuangan)
{
    listKasusPenyakitRuangan(idRuangan);
    listDokterRuangan(idRuangan);
    updateKamar();
    
    jQuery.ajax({'type':'POST',
                 'url':'<?php echo $urlKamar ?>',
                 'cache':false,
                 'data':{ ruangan_id:idRuangan, bookingkamar_id:idBookingKamar },
                 'success':function(html){
                     jQuery("#PPPasienAdmisiT_kamarruangan_id").html(html);
                     $('#PPPasienAdmisiT_kamarruangan_id').val(<?php echo $modPasienAdmisi['kamarruangan_id']; ?>)
                 }
             });
}

function updateKamarRuangan(idRuangan)
{
    jQuery.ajax({'type':'POST',
                 'url':'<?php echo $url ?>',
                 'cache':false,
                 'data':{ ruangan_id:idRuangan },
                 'success':function(html){
                     jQuery("#PPPasienAdmisiT_kamarruangan_id").html(html)
                 }
             });
}
function updateKamarRuanganAll(idRuangan)
{
    jQuery.ajax({'type':'POST',
                 'url':'<?php echo $urlAll ?>',
                 'cache':false,
                 'data':{ ruangan_id:idRuangan },
                 'success':function(html){
                     jQuery("#PPPasienAdmisiT_kamarruangan_id").html(html)
                 }
             });
}
function updateKelasPelayanan(idKamarruangan)
{
    jQuery.ajax({'type':'POST',
                 'url':'<?php echo $urlkelaspelayanan ?>',
                 'cache':false,
                 'data':{ kamarruangan_id:idKamarruangan },
                 'success':function(html){
                     jQuery("#PPPasienAdmisiT_kelaspelayanan_id").html(html)
                 }
             });
}
function listKelasPelayananRI(idRuangan){
    jQuery.ajax({'type':'POST',
                 'url':'<?php echo $urlkelaspelayananRI ?>',
                 'cache':false,
                 'data':{ ruangan:idRuangan },
                 'success':function(html){
                     jQuery("#PPPasienAdmisiT_kelaspelayanan_id").html(html)
                 }
             });
}

function listKasusPenyakitRuangan(idRuangan)
{
    // alert(idRuangan);
    $.post("<?php echo Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'','attr'=>'idRuangan','listDropdown'=>true));?>", { idRuangan: idRuangan },
        function(data){
            $("#<?php echo CHtml::activeId($model,'jeniskasuspenyakit_id');?>").html(data.listPenyakit);
        }, "json");
}
</script>

<script>
        function getListKamar()
{
    var idRuangan = $("#PPPasienAdmisiT_ruangan_id").val();
//    alert(idRuangan);
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/getListKamarRI'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'data'=>'{idRuangan:idRuangan}',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogListKamar div.divForForm').html(data.div);
                    $('#dialogListKamar div.divForForm form').submit(getListKamar);
                    
                }
                else
                {
                    $('#dialogListKamar div.divForForm').html(data.div);
//                    $.fn.yiiGridView.update('daftarpasien-v-grid');
                    setTimeout(\"$('#dialogListKamar').dialog('close') \",1000);
                }
 
            } ",
    ))
?>;
    return false; 
}
    
    
    
    
</script>
<?php
$idKamarRuangan = CHtml::activeId($modPasienAdmisi,'kamarruangan_id');
$cekListKamar = CHtml::activeId($modPasienAdmisi,'masukkamar');
$urlListKamar = Yii::app()->createUrl('actionAjax/getListKamarRI');

$js = <<< JS
function updateKamar()
{
    if($('#${cekListKamar}').is(':checked')){
        $('#${idKamarRuangan}').html('<option value="">-- Pilih --</option>')
    } else {
        $('#${idKamarRuangan}').html('<option value="">--Kamar Ditentukan Kemudian--</option>');
    }
}

function unCheckKamar()
{
    $('#${cekListKamar}').attr('checked', false);
}
//
//function getListKamar(){
//    idRuangan = $('#PPPasienAdmisiT_ruangan_id').val()
//    
//    if(!jQuery.isNumeric(idRuangan)){
//        alert('Ruangan Harus Diisi Terlebih Dahulu');
//    }else{
//        $.post("${urlListKamar}", { idRuangan: idRuangan },
//            function(data){
//               if (data.status == 'create_form')
//                {
//                    $('#dialogListKamar div.divForForm').html(data.div);
//                }
//            }, "json");
//    }
//    
//}
JS;
Yii::app()->clientScript->registerScript('updateKamarRI',$js,CClientScript::POS_HEAD);
?>