<fieldset class="">
    <legend class="accord1">
        <?php echo CHtml::checkBox('isKecelakaan', $model->isKecelakaan, array('onchange'=>'toggleKecelakaan();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Kecelakaan
    </legend>
    <div id="divKecelakaan" class="toggle <?php echo ($model->isKecelakaan) ? '':'hide' ?> toggle" >
        <?php echo $form->dropDownListRow($modKecelakaan,'jeniskecelakaan_id', CHtml::listData($modKecelakaan->getJenisKecelakaanItems(), 'jeniskecelakaan_id', 'jeniskecelakaan_nama'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modKecelakaan,'tempatkecelakaan', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modKecelakaan,'keterangankecelakaan', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        
        <div class="control-group ">
            <?php echo $form->labelEx($modKecelakaan,'tglkecelakaan', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modKecelakaan,
                                        'attribute'=>'tglkecelakaan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($modKecelakaan, 'tglkecelakaan'); ?>
            </div>
        </div>
    </div>
</fieldset>
    
<?php
$js = <<< JS
function toggleKecelakaan()
{
    $('#divKecelakaan').slideToggle(500);
}
JS;
Yii::app()->clientScript->registerScript('ceklistKecelakaan',$js,CClientScript::POS_HEAD);
?>