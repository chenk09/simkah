
<fieldset id="fieldsetAsuransiSep">
    <legend class="accord1">
        <?php echo CHtml::checkBox('pakeAsuransi', $model->pakeAsuransi, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        <?php echo CHtml::hiddenField('carabayarSep', '');?>
        Asuransi yang digunakan
    </legend>
    <div id="divAsuransi" class="toggle <?php echo ($model->pakeAsuransi) ? '':'hide' ?> toggle" >
        <?php echo $form->textFieldRow($model,'no_asuransi', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Asuransi')); ?>
        <?php echo $form->textFieldRow($model,'namapemilik_asuransi', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Pemilik Asuransi')); ?>
        <?php echo $form->textFieldRow($model,'nopokokperusahaan', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Pokok Perusahaan')); ?>
        <?php echo $form->textFieldRow($model,'namaperusahaan', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Perusahaan'));?>
        <?php echo $form->dropDownListRow($model,'kelastanggungan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('disabled'=>true,'empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;')); ?>
        <div class="control-group ">
            <div class="controls">
                 <?php echo $form->checkBox($model,'status_konfirmasi', array('onkeypress'=>"return $(this).focusNextInputField(event)",'checked'=>false)); ?>Status Konfirmasi
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
            </div>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model,'tgl_konfirmasi', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tgl_konfirmasi',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
            </div>
        </div>
    </div>
    <?php 
    if(Yii::app()->user->getState('is_bridging')==TRUE){ 
    ?>
    <div id="divAsuransiSep" class="toggle <?php echo ($model->pakeAsuransi) ? '':'hide' ?> toggle" >
        <?php echo $form->hiddenField($modSep,'jenispeserta_bpjs_kode',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'readonl'=>true)); ?>
        <?php echo $form->hiddenField($modSep,'jenispeserta_bpjs_nama',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'readonl'=>true)); ?>
        <?php echo $form->hiddenField($modSep,'no_asuransi_cob',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
        <?php echo $form->hiddenField($modSep,'nama_asuransi_cob',array('readonly'=>true,'class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);")); ?>
        <?php echo $form->hiddenField($modSep,'hakkelas_kode',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'readonl'=>true)); ?>
        <div id="animation"></div>
        <div class="control-group">
            <?php echo CHtml::label("No. Kartu BPJS <span class='required'>*</span> <i class=\"icon-search\" onclick=\"getAsuransiNoKartu($('#".CHtml::activeId($modSep,"nopeserta_bpjs")."').val());\", style=\"cursor:pointer;\" rel='tooltip' title='klik untuk mengecek peserta'></i>", 'nopeserta', array('class'=>'control-label'))?>
            <div class="controls">
                <?php echo $form->textField($modSep,'nopeserta_bpjs',array('placeholder'=>'Ketik No. Peserta','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group ">
            <?php echo CHtml::label("Nama Peserta <span class='required'>*</span>", 'kelastanggungan', array('class'=>'control-label'))?>
            <div class="controls">
            <?php echo $form->textField($modSep,'namapeserta_bpjs',array('placeholder'=>'Nama Lengkap Pemilik Asuransi','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>		
	</div>
        <div class="control-group ">
            <?php echo CHtml::label("Kelas Tanggungan <span class='required'>*</span>", 'kelastanggungan', array('class'=>'control-label'))?>
            <div class="controls">
            <?php echo $form->dropDownList($modSep,'kelasrawat_kode', array('1'=>'Kelas I','2'=>'Kelas II','3'=>'Kelas III'), array('empty'=>'-Pilih-','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                )); ?>
            </div>		
	</div>
        <div class="control-group ">
            <?php echo CHtml::label("Jenis/Asal Rujukan", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls form-inline">
                <?php 
                echo $form->radioButtonList($modSep,'jenisrujukan_kode_bpjs',array("1"=>"PCare&nbsp;&nbsp;","2"=>"Rumah Sakit"), array('onkeyup'=>"return $(this).focusNextInputField(event)"));
                ?>
            </div>		
	</div>
        <div class="control-group">
            <?php echo CHtml::label("No.Rujukan Faskes <span class='required'>*</span> <i class=\"icon-search\" onclick=\"getRujukanNoRujukan($('#".CHtml::activeId($modSep,"norujukan_bpjs")."').val());\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'norujukan_bpjs',array('placeholder'=>'No. Rujukan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group ">
            <label class="control-label required" for="ARRujukanbpjsT_tanggal_rujukan">
            Tanggal Rujukan
            <span class="required">*</span>
            </label>
            <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                        'model'=>$modSep,
                        'attribute'=>'tglrujukan_bpjs',
                        'mode'=>'date',
                        'options'=> array(
                            'dateFormat'=>"yy-mm-dd",
                            'showOn' => false,
                            'maxDate' => 'd',
                        ),
                        'htmlOptions'=>array('class'=>'dtPicker3 required','onkeyup'=>"return $(this).focusNextInputField(event)",),
                    )); ?>
            </div>
	</div>
        <div class="control-group ">
            <label class="control-label">
            No. SEP
            </label>
            <div class="controls">
                <?php echo $form->textField($modSep,'no_sep',array('placeholder'=>'No. SEP Otomatis','class'=>'span3', 'readonly'=>true ,'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
	<div class="control-group">
            <?php echo CHtml::label("Kode PPK Rujukan <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'ppkrujukanasal_kode',array('placeholder'=>'Kode PPK Rujukan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group">
            <?php echo CHtml::label("Nama PPK Rujukan <span class='required'>*</span><i class=\"icon-search\" onclick=\"$('#dialogPpk').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek ppk rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'ppkrujukanasal_nama',array('placeholder'=>'Nama PPK Rujukan','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group">
            <?php echo CHtml::label("Kode Diagnosa Awal <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogDiagnosaBpjs').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'diagnosaawal_kode',array('placeholder'=>'Diagnosa Awal','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group">
            <?php echo CHtml::label("Nama Diagnosa Awal <span class='required'>*</span> <i class=\"icon-search\" onclick=\"$('#dialogDiagnosaBpjs').dialog('open');\", style=\"cursor:pointer;\" rel=\"tooltip\" title=\"klik untuk mengecek rujukan\"></i>", 'no_rujukan', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'diagnosaawal_nama',array('placeholder'=>'Diagnosa Awal','class'=>'span3 required', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
            </div>
	</div>
        <div class="control-group form-inline">
            <?php echo CHtml::label("Poli Eksekutif", 'Eksekutif', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php 
                    echo $form->radioButtonList($modSep,'polieksekutif',array("1"=>"YA&nbsp;&nbsp;","0"=>"TIDAK"), array('onkeyup'=>"return $(this).focusNextInputField(event)"));
                    ?>
            </div>
	</div>
        <div class="control-group form-inline">
            <?php echo CHtml::label("COB", 'COB', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'cob_status',array('class'=>'span1', 'onkeyup'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                    <?php echo $form->hiddenField($modSep,'cob_bpjs',array('class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", 'readonl'=>true)); ?>
            </div>
	</div>
        <div class="control-group form-inline">
            <?php echo CHtml::label("Laka Lantas", 'Lantas', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php 
                    echo $form->radioButtonList($modSep,'lakalantas_kode',array("1"=>"YA&nbsp;&nbsp;","0"=>"TIDAK"), array('onkeyup'=>"return $(this).focusNextInputField(event)",'onchange'=>'setLakaLantas(this)'));
                    ?>
            </div>
	</div>
        <div class="control-group ">
            <?php echo CHtml::label("Penjamin", 'Penjamin', array('class'=>'control-label'))?>
            <div class="controls">
            <?php echo $form->dropDownList($modSep,'penjaminlakalantas', array('1'=>'Jasa Raharja PT','2'=>'BPJS Ketenagakerjaan','3'=>'TASPEN','4'=>'ASABRI PT'), array('empty'=>'-- Pilih --','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event)",
                )); ?> <i>pilih jika lakalantas</i>
            </div>		
	</div>
        <div class="control-group">
            <?php echo CHtml::label("Lokasi Laka Lantas", 'lokasi_lakalantas', array('class'=>'control-label'))?>
            <div class="controls">
                    <?php echo $form->textField($modSep,'lokasilakalantas',array('placeholder'=>'Lokasi laka lantas','class'=>'span3', 'onkeyup'=>"return $(this).focusNextInputField(event);", )); ?>
                    <i>isi jika lakalantas</i>
            </div>
	</div>
        <?php echo $form->textAreaRow($modSep,'catatan_sep', array('placeholder'=>'','class'=>'span3 required','onkeyup'=>"return $(this).focusNextInputField(event)")); ?>
        <div class="control-group ">
            <div class="controls">
                <?php echo $form->checkBox($model,'status_konfirmasi', array('onkeypress'=>"return $(this).focusNextInputField(event)",'checked'=>false)); ?>Status Konfirmasi
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
            </div>
        </div>
        <div class="control-group ">
            <?php echo $form->labelEx($model,'tgl_konfirmasi', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tgl_konfirmasi',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
                <?php echo CHtml::link(Yii::t('mds', '{icon} Verifikasi SEP', array('{icon}'=>'<i class="icon-search icon-white"></i>')), '#', array('id'=>'tombol_create_sep','class'=>'btn btn-info','onclick'=>"cekInputSep(13,".$modSep->jnspelayanan_kode.");return false",'disabled'=>FALSE)) ?>
            </div>
        </div>
        <?php
        }
        ?>
    </div>
</fieldset>
<?php
echo $this->renderPartial('bpjs/_settingBpjs',array('modSep'=>$modSep,'model'=>$model));
?>
<?php
$enableInputAsuransi = ($model->pakeAsuransi) ? 1 : 0;
$briging = (Yii::app()->user->getState('is_bridging')) ? 1 : 0;
$js = <<< JS
if(${enableInputAsuransi}) {
    $('#divAsuransi input').removeAttr('disabled');
    $('#divAsuransi select').removeAttr('disabled');
}
else {
    $('#divAsuransi input').attr('disabled','true');
    $('#divAsuransi select').attr('disabled','true');
}

$('#pakeAsuransi').change(function(){
        var noMr = $('#noRekamMedik').val();
        var noMrLama = $('#noRekamMedikLama').val();
        var carabayar = $('#carabayarSep').val();
        if ($(this).is(':checked')){
            if(${briging} && (noMr!='' || noMrLama!='') && carabayar==18){
                $('#divAsuransi input').attr('disabled','true');
                $('#divAsuransi select').attr('disabled','true');
                $('#divAsuransiSep input').removeAttr('disabled');
                $('#divAsuransiSep select').removeAttr('disabled');
                $('#divAsuransiSep textarea').removeAttr('disabled');
                $('#divAsuransiSep').show(500);
                $('#divAsuransi').hide(500);
            }else{
                $('#divAsuransi input').removeAttr('disabled');
                $('#divAsuransi select').removeAttr('disabled');
                $('#divAsuransiSep input').attr('disabled','true');
                $('#divAsuransiSep select').attr('disabled','true');
                $('#divAsuransiSep textarea').attr('disabled','true');
                $('#divAsuransi').show(500);
                $('#divAsuransiSep').hide(500);
            }
        }else{
                $('#divAsuransi input').attr('disabled','true');
                $('#divAsuransi select').attr('disabled','true');
//                $('#divAsuransi input').attr('value','');
//                $('#divAsuransi select').attr('value','');
                $('#divAsuransiSep input').attr('disabled','true');
                $('#divAsuransiSep select').attr('disabled','true');
//                $('#divAsuransiSep input').attr('value','');
//                $('#divAsuransiSep select').attr('value','');
                $('#divAsuransiSep textarea').attr('disabled','true');
                $('#divAsuransi').hide(500);
                $('#divAsuransiSep').hide(500);
        }
//        $('#divAsuransi').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('asuransi',$js,CClientScript::POS_READY);
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDiagnosaBpjs',
    'options' => array(
        'title' => 'Referensi Diagnosa BPJS',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('bpjs/_pencarianDiagnosa');
$this->endWidget();
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPpk',
    'options' => array(
        'title' => 'Referensi PPK Rujukan/Faskes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 960,
        'height' => 480,
        'resizable' => false,
    ),
));
echo $this->renderPartial('bpjs/_pencarianPpk');
$this->endWidget();
?>