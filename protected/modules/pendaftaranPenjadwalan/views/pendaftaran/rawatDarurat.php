<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-rd-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $modLoket = LoketM::model()->findAllByAttributes(array('loket_aktif'=>true));
        $loket = (isset($_POST['loket'])) ? $_POST['loket'] : '';
        echo CHtml::dropDownList('loket', $loket, CHtml::listData($modLoket, 'loket_id', 'loket_nama'), 
                array('empty'=>'-- Pilih Loket --','class'=>'span2'));
        echo CHtml::link(Yii::t('mds', 'Antrian {icon}', array('{icon}'=>'<i class="icon-chevron-right icon-white"></i>')), '#', 
                array('class'=>'btn btn-info','onclick'=>"$('#dialogAntrian').parent().css({position:'fixed'}).end().dialog('open');return false;")); 
    }
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>  
        <legend class="rim2">Pendaftaran Pasien Rawat Darurat</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
	<?php // echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modTindakanPelayan)); ?>
        <table class="table-condensed">
            <tr>
                <td width="50%">
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
                        <div class="label_no">
                             <i class="icon-user"></i>   <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, 
                                     array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 
                                         'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                No Rekam Medik			
                        </div>
                        </label>
                        <div class="controls">
                            <?php 
                                    $enable['readonly'] = true;
                                    $readOnly = ($model->isPasienLama) ? '' : $enable ; 
                            ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedik',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedik").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                                $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                                $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                                $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                                $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                                $("#'.CHtml::activeId($modPasien,'nama_ibu').'").val(ui.item.nama_ibu);
                                                $("#'.CHtml::activeId($modPasien,'nama_ayah').'").val(ui.item.nama_ayah);
                                                $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                                $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                                $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                                setJenisKelaminPasien(ui.item.jeniskelamin);
                                                setRhesusPasien(ui.item.rhesus);
                                                loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, ui.item.kelurahan_id);
                                                getUmur(ui.item.tanggal_lahir);
                                                $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                                $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                                $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                                $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                                $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                                $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                                $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                                $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                                $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                                $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                                $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                                $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                                $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                                $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                                $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                                $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                                $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                                $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                                $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                                loadUmur(ui.item.tanggal_lahir);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik','onblur'=>'cariDataPasien(this.value);','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly',),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),					
                            )); ?>
                           
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                </td>
                <td width="50%">
                    <div class="label_no">
                        <?php $minDate = (Yii::app()->user->getState('loginpemakai_id')) ? '' : 'd'; ?>
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        'minDate' => $minDate,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group" id="controlNoRekamMedikLama">
                        <label class="control-label">
                        <div class="label_no">
                             <i class="icon-user"></i>   <?php echo CHtml::checkBox('isPasienBaru', '', array('rel'=>'tooltip',
                                    'title'=>'Pilih jika pasien baru','onclick'=>'pilihNoRm()',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                               Gunakan No. RM yg tidak dipakai
                        </div>
                        </label>
                        <div class="controls">
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'noRekamMedikLama',
                                        'value'=>$model->noRekamMedik,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienNonAktif'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#noRekamMedikLama").val( ui.item.value );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);                                             
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik',
                                                             'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                             'class'=>'span3 numbersOnly',
                                                             
                                            ),
                                        'tombolDialog'=>array('idDialog'=>'dialogPasienLama','idTombol'=>'tombolPasienDialogBaru'),
										
                            )); ?>
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaran._formPasien', 
                                array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien)); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset id="fieldsetKunjungan">
                        <legend class="rim">Data Kunjungan</legend>
                    <table>
                        <tr>
                            <td width="50%">
                                <div >
                                    <?php echo CHtml::label('No. Antri Poliklinik','',array('class'=>'control-label')); ?>
                                    <div class='controls'>
                                            <?php echo $form->textField($model,'no_urutantri', array('readonly'=>true,
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                    </div>
                                </div><br>

                                <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData(InstalasipenddaruratV::model()->findAll(), 
                                                    'ruangan_id', 'ruangan_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'ajax'=>array('type'=>'POST',
                                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranRd')),
                                                                          'update'=>'#PPPendaftaranRd_jeniskasuspenyakit_id'),
                                                                        'onchange'=>"listDokterRuangan(this.value);listKarcis(this.value)",
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'reqKunjungan',
                                                                      'onfocus'=>'return cekFieldNoRm();'
                                                                      )); ?>

                                <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', 
                                            CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 
                                                    'jeniskasuspenyakit_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'reqKunjungan')); ?>

                                <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(),
                                            'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                        'onchange'=>"listKarcis(this.value)",
                                                                        'class'=>'reqKunjungan',
                                    )); ?>

                                <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 
                                            'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --',
                                                'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'class'=>'reqKunjungan')); ?>

                                <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type'=>'POST',
                                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPPendaftaranRd')), 
                                                                'update'=>'#'.CHtml::activeId($model,'penjamin_id').''  //selector to update
                                                            ),
                                                            'onchange'=>'caraBayarChange(this)',
                                                            'class'=>'reqKunjungan',
                                    )); ?>

                                <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 
                                        'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --',
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                            'class'=>'reqKunjungan')); ?>
                                
                                <?php echo $form->dropDownListRow($model,'keadaanmasuk', KeadaanMasuk::items(),array('empty'=>'-- Pilih --',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'transportasi', Transportasi::items(),array('empty'=>'-- Pilih --',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            
                                <?php echo $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaran._formKecelakaan', array('model'=>$model,
                                        'form'=>$form,'modKecelakaan'=>$modKecelakaan)); ?>
                                <?php
                                //FORM REKENING
                                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                                        array(
                                            'form'=>$form,
                                            'modRekenings'=>$modRekenings,
                                        )
                                    );
                                ?>
                            </td>
                            <td width="50%">
                                <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan)); ?>

                                <?php 
                                if(Yii::app()->user->getState('is_bridging')==TRUE){
                                    echo $this->renderPartial('_formAsuransiSep', array('model'=>$model,'form'=>$form,'modSep'=>$modSep));
                                }else{
                                    echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form));
                                }
                                ?>

                                <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,
                                        'modPenanggungJawab'=>$modPenanggungJawab)); ?>

            <!--                    <div class="control-group ">
                                    <div class="controls">
                                        <div class="checkbox inline">
                                            <?php // echo $form->checkBox($model,'kunjunganrumah', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                            <?php // echo CHtml::activeLabel($model, 'kunjunganrumah'); ?>
                                        </div>
                                        <div class="checkbox inline">
                                            <?php // echo $form->checkBox($model,'byphone', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                            <?php // echo CHtml::activeLabel($model, 'byphone'); ?>
                                        </div>
                                    </div>
                                </div>-->

                                <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1">
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, 
                                                array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Rawat Darurat
                                    </legend>
                                        <?php echo $this->renderPartial('pendaftaranPenjadwalan.views.pendaftaran._formKarcis', 
                                                array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </fieldset>
	<div class="form-actions">
		<?php 
                    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                              array('class'=>'btn btn-primary','type'=>'submit','onKeypress'=>'setVerifikasi(); return false;',
                                  'onClick'=>'setVerifikasi(); return false;')); 
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                              array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pendaftaran/rawatDarurat',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
                <?php if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        {
                ?>
                          <script>
                            // print(<?php echo $model->pendaftaran_id. ',' .$model->pasien_id ; ?>);
                          </script>
                <?php
                        echo CHtml::link(Yii::t('mds', '{icon} Print Gelang', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status RM', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_status('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
						echo CHtml::link(Yii::t('mds', '{icon} Print Status V.2', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_dua('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Kartu Pasien', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKartuPasien('$model->pasien_id');return false",'disabled'=>FALSE  ));
                        
                        if(isset($modSep->sep_id) && !empty($modSep->sep_id)){
                            echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printSep('$modSep->sep_id');return false",'disabled'=>FALSE  ));
                        }else{
                            if($model->carabayar_id==18 && Yii::app()->user->getState('is_bridging')==TRUE){
                                echo CHtml::link(Yii::t('mds', '{icon} Proses SEP', array('{icon}' => '<i class="icon-ok icon-white"></i>')), $this->createUrl("SettingBpjs/prosesSEP",array("pendaftaran_id"=>$model->pendaftaran_id,"pasien_id"=>$modPasien->pasien_id,"jenispelayanan"=>2,"frame"=>true,'jnspelayanan'=>"RJ")), array(
                                    'rel' => 'tooltip', 'title' => 'Klik untuk Proses SEP!', 'class' => 'btn btn-info',
                                    "class"=>"btn btn-primary", "onclick"=>"$('#dialog-proses-sep').dialog('open');","target"=>"iframeProsesSEP"))."&nbsp;";
                            }
                            
                        }
                        
                       }else{
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  ));

                        echo CHtml::link(Yii::t('mds', '{icon} Print Kartu Pasien', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  ));
                       } 
                ?>
<?php $this->endWidget(); ?>

<?php 
    $content = $this->renderPartial('../tips/transaksi',array(),true);
    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  
?>
	</div>
<div>
    <legend class="rim"> 10 Pasien yang terakhir di Daftarkan </legend>
    <div>
        <?php  echo CHtml::link(Yii::t('mds', '{icon} Refresh', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), '#', array('class'=>'btn btn-danger','onclick'=>"refreshList();",'disabled'=>FALSE  )); ?>
                 
    </div>
<?php 
    $modListPendaftaran = new PPPendaftaranRd();
    $tglAwal = date('Y-m-d 00:00:00');
    $tglAkhir = date('Y-m-d 23:59:59');
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
            'id'=>'pendaftarterakhir-rd-grid',
            'dataProvider'=>$modListPendaftaran->searchListPendaftaran($tglAwal,$tglAkhir),
            'template'=>"{pager}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                array(
                    'header'=>'Tgl <br/> Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->tgl_pendaftaran',
                ),
                array(
                    'header'=>'No <br/>Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header'=>'No RM',
                    'type'=>'raw',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'$data->pasien->nama_pasien',
                ),
                array(
                    'header'=>'Tempat <br/> Lahir',
                    'type'=>'raw',
                    'value'=>'$data->pasien->tempat_lahir',
                ),
                array(
                    'header'=>'Tgl Lahir',
                    'type'=>'raw',
                    'value'=>'$data->pasien->tanggal_lahir',
                ),
                array(
                    'header'=>'Umur',
                    'type'=>'raw',
                    'value'=>'$data->umur',
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'type'=>'raw',
                    'value'=>'$data->pasien->jeniskelamin',
                ),
                array(
                    'header'=>'Alamat Pasien',
                    'type'=>'raw',
                    'value'=>'$data->pasien->alamat_pasien',
                ),
                array(
                    'header'=>'No Telp',
                    'type'=>'raw',
                    'value'=>'$dta->pasien->no_telepon_pasien',
                ),
                array(
                    'header'=>'Ruangan Tujuan',
                    'type'=>'raw',
                    'value'=>'$data->ruangan->ruangan_nama',
                ),
                array(
                    'header'=>'Dokter',
                    'type'=>'raw',
                    'value'=>'$data->pegawai->nama_pegawai',
                ),
                array(
                    'header'=>'Cara Bayar',
                    'type'=>'raw',
                    'value'=>'$data->carabayar->carabayar_nama',
                ),
                array(
                    'header'=>'Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->penjamin->penjamin_nama',
                ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 
?>
</div>
<script type="text/javascript">
$('#<?php echo CHtml::activeId($model, 'ruangan_id'); ?>').val(16);
jQuery.ajax({'type':'POST',
             'url':'/simrs/index.php?r=ActionDynamic/GetKasusPenyakit&encode=&namaModel=<?php echo get_class($model); ?>',
             'cache':false,
             'data':jQuery("#<?php echo CHtml::activeId($model, 'jeniskasuspenyakit_id'); ?>").parents("form").serialize(),
             'success':function(html){jQuery("#<?php echo CHtml::activeId($model, 'jeniskasuspenyakit_id'); ?>").html(html)}
         });
listDokterRuangan(16);listKarcis(16);

if($('#isPasienLama').is(':checked')){
    $('#tombolPasienDialog').removeClass('hide');
}else{
    $('#tombolPasienDialog').addClass('hide');
}
</script>

<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id . '/' .$controller;
$urlKartu =  Yii::app()->createAbsoluteUrl($module);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>''));
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRD',array('idPendaftaran'=>''));
$urlPrintSep = Yii::app()->createUrl(Yii::app()->controller->module->id.'/settingBpjs/PrintSep',array('sep_id'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPasien'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$notif = Yii::t('mds','Apakah Pendaftaran Rawat Darurat Tanpa Karcis ?');

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKunjunganPasien=PPKonfigSystemK::model()->find()->printkunjunganlsng;
if(!empty($cekKunjunganPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKunjunganPasien=$cekKunjunganPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKunjunganPasien=0;
}

//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKartuPasien=PPKonfigSystemK::model()->find()->printkartulsng;
if(!empty($cekKartuPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKartuPasien=$cekKartuPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKartuPasien=0;
}
//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= Awal Ngecek Status Pasien
$statusPasien=$model->statuspasien;


$statusPasienBaru=Params::statusPasien('baru');

//================= Akhir Ngecek Status Pasien
$karcis = KonfigsystemK::getKonfigurasiKarcisPasien();
$karcis = ($karcis) ? true : 0;
$jscript = <<< JS
function print(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=940,height=400');
        }     
        
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");

        }
}

function print_status(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran + '&status=3','printwin','left=100,top=100,width=940,height=400');
        }
}            
            
function print_dua(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoli}'+idPendaftaran + '&status=2','printwin','left=100,top=100,width=940,height=400');
        }
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");
        }
}

function printKartuPasien(idPasien)
{       
        //if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");

        //}
}
function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPendaftaranRd_pegawai_id').html(data.listDokter);
    }, "json");
}

function printSep(sep_id)
{       
    window.open('${urlPrintSep}'+sep_id,'printwin','left=100,top=100,width=860,height=480');

}
        
function listKarcis(obj)
{
     kelasPelayanan=$('#PPPendaftaranRd_kelaspelayanan_id').val();
     ruangan=$('#PPPendaftaranRd_ruangan_id').val();
     if($('#isPasienLama').is(':checked'))
        pasienLama = 1;
     else
        pasienLama = 0;
    
     if(kelasPelayanan!='' && ruangan!=''){
            $('#tblFormKarcis tbody').remove();
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan, pasienLama:pasienLama },
                function(data){
                    $('#tblFormKarcis').append(data.form);
                    if (${karcis}){
                        if (jQuery.isNumeric(data.karcis.karcis_id)){
                            tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                            changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                            getDataRekening(data.karcis.daftartindakan_id,kelasPelayanan,1,'tm');
                        }
                    }
             }, "json");
     }      
       
}
             
function changeBackground(obj,idTindakan,tarifSatuan)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
     
}   

function checkKarcis(){  

//        if(!confirm("${notif}")) {
//            close();
//        }else{
//            alert('Ya');
//        }

    if(!$.isNumeric($('#TindakanPelayananT_idTindakan').val())){
        return window.confirm("${notif}");
    }
    else{
//        $('#pppendaftaran-rd-form').submit();
        return true;
    }
}
    
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});


JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogAntrian',
            'options'=>array(
                'title'=>'Antrian',
                'autoOpen'=>true,
                'resizable'=>false,
                'width'=>170,
                'height'=>150,
                'position'=>'right',
            ),
        ));

            echo '<iframe src="'.Yii::app()->createAbsoluteUrl('pendaftaranPenjadwalan/TampilAntrian/PanggilAntrian').'" width="100%" height="100px" scrolling="no"></iframe>';

        $this->endWidget('zii.widgets.jui.CJuiDialog');
        
    }
?>
<!--Validasi Form-->
<script>


function cekDaftar(pasien_id, no_rekam_medik){
        if(pasien_id!=null){
            $("#dialogPasien").dialog("open");
            $.post("?r=actionAjax/CekPasien", { pasien_id: pasien_id },
            function(data){
                if(data.jumlah>0){
                    var jawab = confirm('Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus ' +data.statusperiksa+ ' di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran);
                    if (jawab){
                        $('#dialogPasien').dialog('close');
                        cariDataPasien(no_rekam_medik);
                    }
                    // $('#dialogCekPasien').dialog('open');
                    // $('#dataCekPasien').find('tbody').empty();
                    // $('#dataCekPasien').find('tbody').append('<tr>\n\
                    //         <td><input id="no_rekam_medik_pasien" type="hidden" name="no_rekam_medik_pasien" value="'+data.no_rekam_medik+'"></td><tr>\n\
                    //         <td><h4>Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus <u>'+data.statusperiksa+'</u> di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran+'</h4></td>\n\
                    //     </tr>'
                    // );
                }else{
                    $('#dialogPasien').dialog('close');
                    cariDataPasien(no_rekam_medik);
                }
            }, "json");
        }else{
            $.post("?r=actionAjax/AmbilPasienId", { no_rekam_medik: no_rekam_medik },
            function(data){
                var pasien_id = data.pasien_id;
                $.post("?r=actionAjax/CekPasien", { pasien_id: pasien_id },
                function(data){
                    if(data.jumlah>0){
                        $('#dialogCekPasien').dialog('open');
                        $('#dataCekPasien').find('tbody').empty();
                        $('#dataCekPasien').find('tbody').append('<tr>\n\
                                <td><input id="no_rekam_medik_pasien" type="hidden" name="no_rekam_medik_pasien" value="'+data.no_rekam_medik+'"></td><tr>\n\
                                <td><h4>Pasien atas nama '+data.nama_pasien+' dengan No. Pendaftaran '+data.no_pendaftaran+' berstatus <u>'+data.statusperiksa+'</u> di Poliklinik/Ruangan '+data.ruangan_nama+' tanggal '+data.tgl_pendaftaran+'</h4></td>\n\
                            </tr>'
                        );
                    }else{
                        $('#dialogPasien').dialog('close');
                        cariDataPasien(no_rekam_medik);
                    }
                }, "json");
            }, "json"); 
        }

        
    }


function cekValidasi(){
    var adaKarcis = checkKarcis();
    var validPasienVK = cekPasienVK();
    
//    alert('ada karcis? '+adaKarcis);
    if(adaKarcis && validPasienVK)
        return true;
    else
        return false;
}
function cekPasienVK(){
//    alert($("#PPPasienM_namadepan").val());
    var valid = true;
    if($("#PPPendaftaranRd_ruangan_id").val() == '137'){ //Harus Menyesuaikan dengan ruangan_id VK(Persalinan)
        if (!($("#PPPasienM_namadepan").val() == 'Nn' || $("#PPPasienM_namadepan").val() == 'Ny.')){
            alert("Pasien untuk Persalinan (VK) ditolak! Silahkan periksa kembali Umur dan Jenis Kelamin Pasien.");
            valid = false;
        }else{
            valid = true;
        }
    }
    return valid;
}
$('#pakeAsuransi').attr('style','display:none;');
/*function caraBayarChange(obj){
//    var penjamin = document.getElementById('LKPendaftaranMp_penjamin_id');
    if(obj.value == 1 || obj.value == 0 ){
//     ERROR >>>  setTimeout( 'var penjamin = document.getElementById(\'PPPendaftaranT_penjamin_id\'); penjamin.selectedIndex = 1;', '500' );
       document.getElementById('pakeAsuransi').checked = false;
       $('#pakeAsuransi').attr('style','display:none;');
       $('#divAsuransi').hide(500);
    }else{
        $('#pakeAsuransi').removeAttr('style');
        document.getElementById('pakeAsuransi').checked = true;
        $('#divAsuransi input').removeAttr('disabled');
        $('#divAsuransi select').removeAttr('disabled');
        $('#divAsuransi').show(500);
//        $('#pakeAsuransi').click();
    }
}*/
    function caraBayarChange(obj){
        var noMr = $("#noRekamMedik").val();
        var noMrLama = $("#noRekamMedikLama").val();
        $("#carabayarSep").val(obj.value);
        if(obj.value == 1 || obj.value == 0 ){
           document.getElementById('pakeAsuransi').checked = false;
           $('#pakeAsuransi').attr('style','display:none;');
           $('#divAsuransi').hide(500);
           $('#divAsuransiSep').hide(500);
        }else{
            $('#pakeAsuransi').removeAttr('style');
            document.getElementById('pakeAsuransi').checked = true;
            
            <?php 
            if(Yii::app()->user->getState('is_bridging')==TRUE){ 
            ?>
                if((noMr != '') && $(obj).val() == 18){
                    $('#divAsuransi').hide(500);
                    $('#divAsuransiSep input').removeAttr('disabled');
                    $('#divAsuransiSep select').removeAttr('disabled');
                    $('#divAsuransiSep textarea').removeAttr('disabled');
                    $('#divAsuransiSep').show(500);
                }else{
                    $('#divAsuransiSep').hide(500);
                    $('#divAsuransiSep input').attr('disabled','true');
                    $('#divAsuransiSep select').attr('disabled','true');
                    $('#divAsuransiSep textarea').attr('disabled','true');
                    $('#divAsuransi input').removeAttr('disabled');
                    $('#divAsuransi select').removeAttr('disabled');
                    $('#divAsuransi').show(500);
                }
                
            <?php 
            }else{
            ?>
                $('#divAsuransi input').removeAttr('disabled');
                $('#divAsuransi select').removeAttr('disabled');
                $('#divAsuransi').show(500);
            <?php
            }
            ?>                        
        }
    }

function formSubmit(obj,evt)
{
     evt = (evt) ? evt : event;
     var form_id = $(obj).closest('form').attr('id');
     var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
     if(charCode == 13){
        setVerifikasi();
     }
}
    
function setVerifikasi(){
        var namaPasien = $('#PPPasienM_nama_pasien').val();             var namaDepan = $('#PPPasienM_namadepan').val();
        var noIdentitas = $('#PPPasienM_no_identitas_pasien').val();    var jenisIdentitas = $('#PPPasienM_jenisidentitas').val();
        var alias = $('#PPPasienM_nama_bin').val();
        var tempatLahir = $('#PPPasienM_tempat_lahir').val();           
        var tglLahir = $('#PPPasienM_tanggal_lahir').val();
        var umur = $('#PPPendaftaranRd_umur').val();
        
        var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        if(jenisKelamin == ''){
            var jenisKelamin = "-";
        }else{
            var jenisKelamin = $('#fieldsetPasien').find('input:radio[name$="[jeniskelamin]"][checked="checked"]').val();
        }
        
        var alamatPasien = $('#PPPasienM_alamat_pasien').val();         var rt = $('#PPPasienM_rt').val();  var rw = $('#PPPasienM_rw').val();
        var noTelp = $('#PPPasienM_no_telepon_pasien').val();           
        var noMobile = $('#PPPasienM_no_mobile_pasien').val();
        
        var propinsi = $('#PPPasienM_propinsi_id').val();
            if(propinsi != ''){
                 var propinsi = $("#PPPasienM_propinsi_id option:selected").text();
            }else{
                var propinsi = "-";
            }
        var kabupaten = $('#PPPasienM_kabupaten_id').val();
            if(kabupaten != ''){
                var kabupaten = $("#PPPasienM_kabupaten_id option:selected").text();
            }else{
                var kabupaten = "-";
            }
        var kecamatan = $('#PPPasienM_kecamatan_id').val();
            if(kecamatan != ''){
                var kecamatan = $("#PPPasienM_kecamatan_id option:selected").text();
            }else{
                var kecamatan = "-";
            }
        var kelurahan = $('#PPPasienM_kelurahan_id').val();
            if(kelurahan != ''){
                var kelurahan = $("#PPPasienM_kelurahan_id option:selected").text();
            }else{
                var kelurahan = "-";
            }
        var agama = $('#PPPasienM_agama').val();
            if(agama != ''){
                var agama = $('#PPPasienM_agama').val();
            }else{
                var agama = "-";
            }
        var warganegara = $('#PPPasienM_warga_negara').val();
            if(warganegara != ''){
                var warganegara = $('#PPPasienM_warga_negara').val();
            }else{
                var warganegara = "-";
            }
        var namaAyah = $('#PPPasienM_nama_ayah').val();     
        var namaIbu = $('#PPPasienM_nama_ibu').val();
        
        var poliklinikTujuan = $('#PPPendaftaranRd_ruangan_id').val();
            if(poliklinikTujuan != ''){
                var poliklinikTujuan = $("#PPPendaftaranRd_ruangan_id option:selected").text();      
            }else{
                var poliklinikTujuan = "-";
            }
        var jenisKasusPenyakit = $('#PPPendaftaranRd_jeniskasuspenyakit_id').val();
            if(jenisKasusPenyakit != ''){
                var jenisKasusPenyakit = $("#PPPendaftaranRd_jeniskasuspenyakit_id option:selected").text();    
            }else{
                var jenisKasusPenyakit = "-";
            }
        var kelasPelayanan = $('#PPPendaftaranRd_kelaspelayanan_id').val();
            if(kelasPelayanan != ''){
                var kelasPelayanan = $("#PPPendaftaranRd_kelaspelayanan_id option:selected").text();
            }else{
                var kelasPelayanan = "-";
            }
        var dokter = $('#PPPendaftaranRd_pegawai_id').val();
            if(dokter != ''){
                var dokter = $("#PPPendaftaranRd_pegawai_id option:selected").text();
            }else{
                var dokter = "-";
            }
        var caraBayar = $('#PPPendaftaranRd_carabayar_id').val();
            if(caraBayar != ''){
                var caraBayar = $("#PPPendaftaranRd_carabayar_id option:selected").text();
            }else{
                var caraBayar = "-";
            }
        var penjamin = $('#PPPendaftaranRd_penjamin_id').val();
            if(penjamin !=''){
                var penjamin = $("#PPPendaftaranRd_penjamin_id option:selected").text();
            }else{
                var penjamin = "-";
            }
        var keadaanMasuk = $('#PPPendaftaranRd_keadaanmasuk').val();
            if(keadaanMasuk !=''){
                var keadaanMasuk = $("#PPPendaftaranRd_keadaanmasuk").val();
            }else{
                var keadaanMasuk = "-";
            }
        var transportasi = $('#PPPendaftaranRd_transportasi').val();
            if(transportasi !=''){
                var transportasi = $("#PPPendaftaranRd_transportasi").val();
            }else{
                var transportasi = "-";
            }
        var kosong = '';
        var reqPasien       = $("#fieldsetPasien").find(".reqPasien[value="+kosong+"]");
        var pasienKosong = reqPasien.length;
        var reqKunjungan = $('#fieldsetKunjungan').find(".reqKunjungan[value="+kosong+"]");
        var kunjunganKosong = reqKunjungan.length;
        var pjKosong = 0;
        if($('#adaPenanggungJawab').is(':checked')){ //jika ada penanggung jawab data * harus diisi
            var reqPj = $('#fieldsetPenanggungjawab').find(".reqPj[value="+kosong+"]");
            pjKosong = reqPj.length;
        }
        if(!$("#isPasienLama").is(':checked')){ //jika pasien baru
            if (!(namaDepan && namaPasien && jenisKelamin && alamatPasien && (pasienKosong === 0)))
            {
                if(pasienKosong != 0)
                {
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Pasien');
                }
               return false;
            }
        }
        if($("#isUpdatePasien").is(':checked'))
        {
            if (!(namaDepan && namaPasien && jenisKelamin && alamatPasien && (pasienKosong === 0)))
            {
                if(pasienKosong != 0)
                {
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Pasien');
                }
               return false;
            }else{
                if (!((kunjunganKosong === 0) && (pjKosong === 0)))
                {
                    if(kunjunganKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                    else if(pjKosong != 0)
                        alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');
                   return false;
                }
                else{
                    var confirm = $('#dialogVerifikasi').dialog('open');
                    if(confirm){
                        $('#dataPasien').find('tbody').empty();
                        $('#dataPasien').find('tbody').append('<tr>\n\
                                <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                                <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                                <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                                <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                                <td>Propinsi</td><td>' + propinsi + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Umur</td><td>' + umur + '</td>\n\
                                <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                                \n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                                <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                                <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                                <td>Agama</td><td>' + agama + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>No Telp</td><td>' + noTelp +'</td>\n\
                                <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                            </tr>'
                        );
                        $('#dataKunjungan').find('tbody').empty();
                        $('#dataKunjungan').find('tbody').append('<tr>\n\
                                <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                                <td>Dokter</td><td>'+ dokter + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                                <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                                <td>Penjamin</td><td>' + penjamin + '</td>\n\
                            </tr>\n\
                            <tr>\n\
                                <td>Keadaan Masuk</td><td>' + keadaanMasuk + '</td><td>Transportasi</td><td>'+ transportasi + '</td>\n\
                           </tr>'
                        );
                    }
                    return false;                    
                }
            }       
        }else{
            
            if(!((kunjunganKosong === 0) && (pjKosong === 0)))
            {
                if(kunjunganKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Kunjungan');
                else if(pjKosong != 0)
                    alert ('Harap Isi Semua Bagian Yang Bertanda * pada Data Penanggung Jawab');
               return false;
            }else{
                var confirm = $('#dialogVerifikasi').dialog('open');
                if(confirm)
                {
                    $('#dataPasien').find('tbody').empty();
                    $('#dataPasien').find('tbody').append('<tr>\n\
                            <td>No Identitas </td><td>'+ jenisIdentitas + " - " + noIdentitas + '</td>\n\
                            <td>No. Mobile</td><td>'+ noMobile + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Nama Pasien</td><td>'+ namaDepan + " - " + namaPasien + '</td>\n\
                            <td>Nama Ibu</td><td>' + namaIbu + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tempat Lahir</td><td>'+ tempatLahir + '</td>\n\
                            <td>Nama Ayah</td><td>'+ namaAyah + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Tanggal Lahir</td><td>'+ tglLahir + '</td>\n\
                            <td>Propinsi</td><td>' + propinsi + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Umur</td><td>' + umur + '</td>\n\
                            <td>Kota / Kabupaten</td><td>' + kabupaten + '</td>\n\
                            \n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kelamin</td><td>' + jenisKelamin + '</td>\n\
                            <td>Kecamatan</td><td>' + kecamatan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Alamat Pasien</td><td>'+ alamatPasien +'</td>\n\
                            <td>Kelurahan</td><td>' + kelurahan + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>RT / RW </td><td>' + rt + " / " + rw + '</td>\n\
                            <td>Agama</td><td>' + agama + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>No Telp</td><td>' + noTelp +'</td>\n\
                            <td>Warga Negara</td><td>'+ warganegara +'</td>\n\
                        </tr>'
                    );
                    $('#dataKunjungan').find('tbody').empty();
                    $('#dataKunjungan').find('tbody').append('<tr>\n\
                            <td>Poliklinik Tujuan </td><td>'+ poliklinikTujuan + '</td>\n\
                            <td>Dokter</td><td>'+ dokter + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Jenis Kasus Penyakit</td><td>'+ jenisKasusPenyakit + '</td>\n\
                            <td>Cara Bayar</td><td>' + caraBayar + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Kelas Pelayanan</td><td>'+ kelasPelayanan + '</td>\n\
                            <td>Penjamin</td><td>' + penjamin + '</td>\n\
                        </tr>\n\
                        <tr>\n\
                            <td>Keadaan Masuk</td><td>' + keadaanMasuk + '</td><td>Transportasi</td><td>'+ transportasi + '</td>\n\
                       </tr>'
                    );
                }
                return false;                    
            }            
        }
    return false;
}

function kembaliForm(){
        
   $('#dialogVerifikasi').dialog('close');
}
    
function setKonfirmasi(obj){
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#pppendaftaran-rd-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
    $('#pppendaftaran-rd-form').submit();
}

function refreshList(){
    $.fn.yiiGridView.update('pendaftarterakhir-rd-grid', {
        data: $(this).serialize()
    });
    return false;
}
</script>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
        'id'=>'dialogVerifikasi',
        'options'=>array(
            'title'=>'Verifikasi Data Pasien',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>850,
            'height'=>550,
            'resizable'=>false,
        ),
    ));
?>
<div id="dataPasien">
    <legend class=rim>Data Pasien</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>

        </tbody>
        
    </table>
</div>
<div id ="dataKunjungan">
    <legend class=rim>Data Kunjungan</legend>
    <table class="table table-bordered  table-condensed">
        <tbody>
            
        </tbody>
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Teruskan', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                '#', array('class'=>'btn btn-primary','onclick'=>"setKonfirmasi(this);return false",'disabled'=>FALSE  )); 
        ?>
        <?php 
            echo CHtml::link(Yii::t('mds', '{icon} Kembali', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                '#', array('class'=>'btn btn-blue','onclick'=>"kembaliForm();return false",'disabled'=>FALSE  )); 
        ?>
    </div>
</div>
<?php
    $this->endWidget();
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'		 => 'dialog-proses-sep',
        'options'	 => array(
        'title'		 => 'Proses SEP',
        'autoOpen'	 => false,
        'modal'		 => true,
        'minWidth'	 => 960,
        'minHeight'	 => 480,
        'resizable'	 => false,
        ),
));
echo '<iframe id="iframeProsesSEP"  name="iframeProsesSEP" width="100%" height="550" >
</iframe>';
?>
<?php $this->endWidget(); ?>
