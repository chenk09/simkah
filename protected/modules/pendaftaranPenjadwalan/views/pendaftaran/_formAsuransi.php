<fieldset class="">
    <legend class="accord1">
        <?php echo CHtml::checkBox('pakeAsuransi', $model->pakeAsuransi, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Asuransi yang digunakan
    </legend>
    <div id="divAsuransi" class="toggle <?php echo ($model->pakeAsuransi) ? '':'hide' ?> toggle" >
        
        <?php echo $form->textFieldRow($model,'no_asuransi', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Asuransi')); ?>
        <?php echo $form->textFieldRow($model,'namapemilik_asuransi', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Pemilik Asuransi')); ?>
        <?php echo $form->textFieldRow($model,'nopokokperusahaan', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Pokok Perusahaan')); ?>
        <?php echo $form->textFieldRow($model,'namaperusahaan', array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Nama Perusahaan'));?>
        <?php echo $form->dropDownListRow($model,'kelastanggungan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('disabled'=>true,'empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;')); ?>
        <div class="control-group ">
            <div class="controls">
                 <?php echo $form->checkBox($model,'status_konfirmasi', array('onkeypress'=>"return $(this).focusNextInputField(event)",'checked'=>false)); ?>Status Konfirmasi
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
            </div>
        </div>
        
        <div class="control-group ">
            <?php echo $form->labelEx($model,'tgl_konfirmasi', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tgl_konfirmasi',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($model, 'tgl_konfirmasi'); ?>
            </div>
        </div>
    </div>
</fieldset>

<?php
$enableInputAsuransi = ($model->pakeAsuransi) ? 1 : 0;
$js = <<< JS
if(${enableInputAsuransi}) {
    $('#divAsuransi input').removeAttr('disabled');
    $('#divAsuransi select').removeAttr('disabled');
}
else {
    $('#divAsuransi input').attr('disabled','true');
    $('#divAsuransi select').attr('disabled','true');
}

$('#pakeAsuransi').change(function(){
        if ($(this).is(':checked')){
                $('#divAsuransi input').removeAttr('disabled');
                $('#divAsuransi select').removeAttr('disabled');
        }else{
                $('#divAsuransi input').attr('disabled','true');
                $('#divAsuransi select').attr('disabled','true');
                $('#divAsuransi input').attr('value','');
                $('#divAsuransi select').attr('value','');
        }
        $('#divAsuransi').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('asuransi',$js,CClientScript::POS_READY);
?>