 <style>
    fieldset legend.accord1{
        width:100%;
    }
 </style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pppendaftaran-rj-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#PPPasienAdmisiT_caramasuk_id',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return validasiForm();'),
)); ?>
<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $modLoket = LoketM::model()->findAllByAttributes(array('loket_aktif'=>true));
        $loket = (isset($_POST['loket'])) ? $_POST['loket'] : '';
        echo CHtml::dropDownList('loket', $loket, CHtml::listData($modLoket, 'loket_id', 'loket_nama'), array('empty'=>'-- Pilih Loket --','class'=>'span2'));
        echo CHtml::link(Yii::t('mds', 'Antrian {icon}', array('{icon}'=>'<i class="icon-chevron-right icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"$('#dialogAntrian').parent().css({position:'fixed'}).end().dialog('open');return false;")); 
    }
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset id="fieldsetRI">  
        <legend class="rim2">Pendaftaran Pasien Rawat Inap</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p> 
	<?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modPasienAdmisi)); ?>
        <table class="table-condensed">
            <tr>
                <td width="50%">
                    <div class="label_no">
                        <?php // echo $form->labelEx($model,'caramasuk_id', array('class'=>'control-label')) ?>
                        <label class="control-label">Cara Masuk <font color="red">*</font></label>
                        <div class="controls">
                            <table style="margin:0;">
                                <tr>
                                    <td><?php echo $form->dropDownList($modPasienAdmisi,'caramasuk_id',
                                            CHtml::listData($modPasienAdmisi->getCaraMasukItems(), 'caramasuk_id', 'caramasuk_nama') ,
                                                array('onchange'=>'formAdmisi(this.value)','empty'=>'-- Pilih --',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?></td>
                                    <td>
                                        <?php $minDate = (Yii::app()->user->getState('loginpemakai_id')) ? '' : 'd'; ?>
                                        <?php   
                                           $this->widget('MyDateTimePicker',array(
                                                                'model'=>$model,
                                                                'attribute'=>'tgl_pendaftaran',
                                                                'mode'=>'datetime',
                                                                'options'=> array(
                                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                    'maxDate' => 'd',
                                                                    'minDate' => $minDate,
                                                                ),
                                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','style'=>'width:120px'),
                                        )); ?>
                                    </td>
                                </tr>
                            </table>
                            
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                            <?php echo $form->hiddenField($model, 'pendaftaran_id', array('readonly'=>true)); ?>
                            <?php echo $form->hiddenField($modPasienAdmisi, 'bookingkamar_id', array('readonly'=>true)); ?>
                        </div>
                    </div>
                    
                    
                    <div class="label_no">
                        <?php //echo $form->labelEx($model,'no_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <table style="margin:0;">
                                <tr>
                                    <td>
                                        <?php echo $form->hiddenField($model, 'no_pendaftaran', array('readonly'=>true)); 
//                                                $this->widget('MyJuiAutoComplete',array(
//                                                        'model'=>$model,
//                                                        'attribute'=>'no_pendaftaran',
//                                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/NoPendaftaran'),
//                                                        'options'=>array(
//                                                           'showAnim'=>'fold',
//                                                           'minLength' => 4,
//                                                           'focus'=> 'js:function( event, ui ) {
//                                                                $("#'.CHtml::activeId($model, 'no_pendaftaran').'").val( ui.item.value );
//                                                                return false;
//                                                            }',
//                                                           'select'=>'js:function( event, ui ) {
//                                                                getPasienFromPendaftaran(ui.item.value);
//                                                                return false;
//                                                            }',
//
//                                                        ),
//                                                        'htmlOptions'=>array('disabled'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2'),
//                                            'tombolDialogS'=>array(1=>array('idDialog'=>'dialogPendaftaranRJ','idTombol'=>'tombolPendaftaranDialogRJ'),
//                                                                   2=>array('idDialog'=>'dialogPendaftaranRD','idTombol'=>'tombolPendaftaranDialogRD')),			
//                                            //'tombolDialog'=>array('idDialog'=>'dialogPendaftaranRD'),							
//                                            ));                     
                                        ?>
                                    </td>
                                    <td>
                                        <?php /*echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                            array('onclick'=>'$("#dialogPendaftaranRJ").dialog("open");return false;',
                                                  'class'=>'btn btn-primary hide',
                                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>"tooltip",
                                                  'disabled'=>TRUE,   
                                                  'title'=>"Klik untuk mencari pasien <br/><b>Rawat Jalan</b>",
                                                  'id'=>'btnPendaftaranRJ',));*/ ?>

                                        <?php /*echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                                            array('onclick'=>'$("#dialogPendaftaranRD").dialog("open");return false;',
                                                  'class'=>'btn btn-primary hide',
                                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'rel'=>"tooltip",
                                                  'disabled'=>TRUE,  
                                                  'title'=>"Klik untuk mencari pasien <br/><b>Rawat Darurat</b>",
                                                  'id'=>'btnPendaftaranRD'));*/ ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>                    
                    
                    <div class="control-group" id="controlNoRekamMedik">
                        <label class="control-label">
						<div class="label_no">
                         <i class="icon-user"></i>   <?php echo CHtml::checkBox('isPasienLama', $model->isPasienLama, array('rel'=>'tooltip','title'=>'Pilih jika pasien lama','onclick'=>'pilihNoRm()', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            No Rekam Medik
							</div>
                        </label>
                        <?php //echo CHtml::label('No Rekam Medik', 'noRekamMedik', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <table style="margin:0;">
                                <tr>
                                    <td>
                                        <?php 
                                                $enable['readonly'] = true;
                                                $readOnly = ($model->isPasienLama) ? '' : $enable ; 
                                        ?>
                                        <?php $this->widget('MyJuiAutoComplete',array(
                                                    'name'=>'noRekamMedik',
                                                    'value'=>$model->noRekamMedik,
                                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                                    'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 4,
                                                       'focus'=> 'js:function( event, ui ) {
                                                            $("#noRekamMedik").val( ui.item.value );
                                                            return false;
                                                        }',
                                                       'select'=>'js:function( event, ui ) {
                                                
                                                            $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                                            $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                                            $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                                            $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                                            $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                                            $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                                            $("#'.CHtml::activeId($modPasien,'nama_ibu').'").val(ui.item.nama_ibu);
                                                            $("#'.CHtml::activeId($modPasien,'nama_ayah').'").val(ui.item.nama_ayah);
                                                            $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                                            $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                                            $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                                            $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                                            $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                            setJenisKelaminPasien(ui.item.jeniskelamin);
                                                            setRhesusPasien(ui.item.rhesus);
                                                            loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, ui.item.kelurahan_id);
                                                            getUmur(ui.item.tanggal_lahir);
                                                            $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                                            $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                                            $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                                            $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                                            $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                                            $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                                            $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                                            $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                                            $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                                            $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                                            $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                                            $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                                            $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                                            $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                                            $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                                            $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                                            $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                                            $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                                            $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                                            $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                                            loadUmur(ui.item.tanggal_lahir);
                                                            return false;
                                                        }',

                                                    ),
                                                    'htmlOptions'=>array('placeholder'=>'Ketikan No. Rekam Medik','onblur'=>'cariDataPasien(this.value);','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly'),
                                                    'tombolDialog'=>array('idDialog'=>'dialogPasienRI','idTombol'=>'tombolPasienDialog'),
                                        )); ?>
                                    </td>
                                </tr>
                            </table>
                            <?php echo $form->error($model, 'noRekamMedik'); ?>
                        </div>
                    </div>
                    
                </td>
                    
                <td width="50%">
                  <div class="label_no">
                    <?php echo $form->dropDownListRow($model,'keadaanmasuk', KeadaanMasuk::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                
                    <?php echo $form->dropDownListRow($model,'transportasi', Transportasi::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                  </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php $format = new CustomFormat(); ?>
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien,'format'=>$format)); ?>
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <?php echo $this->renderPartial('_formAdmisi', array('model'=>$model,'form'=>$form,'modPasienAdmisi'=>$modPasienAdmisi)); ?>
                    
                    <?php echo $form->dropDownListRow($modPasienAdmisi,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPPasienAdmisiT')), 
                                                    'update'=>'#PPPasienAdmisiT_penjamin_id'  //selector to update
                                                ),
                                                'onchange'=>'caraBayarChange(this)',
                        )); ?>
                    
                    <?php echo $form->dropDownListRow($modPasienAdmisi,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                    
                    <?php 
                    if(Yii::app()->user->getState('is_bridging')==TRUE){
                        echo $this->renderPartial('_formAsuransiSep', array('model'=>$model,'form'=>$form,'modSep'=>$modSep));
                    }else{
                        echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form));
                    }
                    ?>
                    
                    <?php
                                //FORM REKENING
                                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                                        array(
                                            'form'=>$form,
                                            'modRekenings'=>$modRekenings,
                                        )
                                    );
                                ?>
                </td>
                <td width="50%">
                    <?php //echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php //echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData($model->getRuanganItems(Params::INSTALASI_ID_RI), 'ruangan_id', 'ruangan_nama') ,
                          //                            array('empty'=>'-- Pilih --',
                          //                                  'ajax'=>array('type'=>'POST',
                          //                                                'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PPPendaftaranRi')),
                          //                                                'update'=>'#PPPendaftaranRi_jeniskasuspenyakit_id'),
                          //                                  'onchange'=>"",
                          //                                  'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php //echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php //echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <?php //echo $form->dropDownListRow($model,'statusmasuk', StatusMasuk::items(), array('onchange'=>'enableRujukan(this);','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>
                    
<!--                    <div class="control-group ">
                        <div class="controls">
                            <div class="checkbox inline">
                                <?php //echo $form->checkBox($model,'kunjunganrumah', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                <?php //echo CHtml::activeLabel($model, 'kunjunganrumah'); ?>
                            </div>
                            <div class="checkbox inline">
                                <?php //echo $form->checkBox($model,'byphone', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                <?php //echo CHtml::activeLabel($model, 'byphone'); ?>
                            </div>
                        </div>
                    </div>-->
                    <fieldset id="fieldsetKarcis" class="">
                                    <legend class="accord1" >
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onclick'=>'return false','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Rawat Inap                                </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>   
                    <?php //echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                </td>
            </tr>
    </table>
    </fieldset>

	<div class="form-actions">
                <?php 
                    if($modPasienAdmisi->isNewRecord)
//                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)','onclick'=>'setKonfirmasi(this);',));
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)', 'onclick'=>'return formSubmit(this,event)'));
                    else 
//                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)','onclick'=>'setKonfirmasi(this);',));
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)', 'onclick'=>'return formSubmit(this,event)'));
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pendaftaran/rawatInap',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
                
                <?php if((!$modPasienAdmisi->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        {
                ?>
                          <script>
                            // print(<?php echo $modPasienAdmisi->pasienadmisi_id; ?>);
                          </script>
                <?php
                        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$modPasienAdmisi->pasienadmisi_id');return false",'disabled'=>FALSE  )); 
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status RM', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_status('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE  ));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Status V.2', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print_dua('$model->pendaftaran_id', '$model->pasien_id');return false",'disabled'=>FALSE));
                        echo CHtml::link(Yii::t('mds', '{icon} Print Gelang', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printGelang('$modPasienAdmisi->pasienadmisi_id');return false",'disabled'=>FALSE  )); 
                        
                        if(isset($modSep->sep_id) && !empty($modSep->sep_id)){
                            echo CHtml::link(Yii::t('mds', '{icon} Print SEP', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printSep('$modSep->sep_id');return false",'disabled'=>FALSE  ));
                        }else{
                            if($model->carabayar_id==18 && Yii::app()->user->getState('is_bridging')==TRUE){
                                echo CHtml::link(Yii::t('mds', '{icon} Proses SEP', array('{icon}' => '<i class="icon-ok icon-white"></i>')), $this->createUrl("SettingBpjs/prosesSEP",array("pendaftaran_id"=>$model->pendaftaran_id,"pasien_id"=>$modPasien->pasien_id,"jenispelayanan"=>1,"frame"=>true,'jnspelayanan'=>"RI")), array(
                                    'rel' => 'tooltip', 'title' => 'Klik untuk Proses SEP!', 'class' => 'btn btn-info',
                                    "class"=>"btn btn-primary", "onclick"=>"$('#dialog-proses-sep').dialog('open');","target"=>"iframeProsesSEP"))."&nbsp;";
                            }
                            
                        }
                        
                       }else{
							echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0)', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
							echo CHtml::link(Yii::t('mds', '{icon} Print Gelang', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 'javascript:void(0)', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       } 
                ?>
				<?php $this->endWidget(); ?>
	
<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  

?>
</div>
 <?php 
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddRuangan',
    'options'=>array(
        'title'=>'Add Ruangan Admisi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>

<script type="text/javascript">
if($('#isPasienLama').is(':checked')){
    $('#tombolPendaftaranDialogRJ').removeClass('hide');
    $('#tombolPendaftaranDialogRD').removeClass('hide');
    $('#tombolPasienDialog').removeClass('hide');
}else{
    $('#tombolPendaftaranDialogRJ').addClass('hide');
    $('#tombolPendaftaranDialogRD').addClass('hide');
    $('#tombolPasienDialog').addClass('hide');
}
</script>

<?php Yii::app()->clientScript->registerScript('status_masuk',"
//    $(document).ready(function(){          
//        $(':input').keypress(function(e) {
//            if(e.keyCode == 13) {
//                $(this).focusNextInputField(); 
//            } 
//        });
//    });
    "); 
?>

<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRI',array('idPasienAdmisi'=>''));
$urlPrintLembarPoliDua = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasienRI',array('idPasienAdmisi'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');
$urlPrintGelang = Yii::app()->createUrl('print/cetakGelangPasien',array('idPasienAdmisi'=>''));
$urlPrintSep = Yii::app()->createUrl(Yii::app()->controller->module->id.'/settingBpjs/PrintSep',array('sep_id'=>''));

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKunjunganPasien=PPKonfigSystemK::model()->find()->printkunjunganlsng;
if(!empty($cekKunjunganPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKunjunganPasien=$cekKunjunganPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKunjunganPasien=0;
}

//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKartuPasien=PPKonfigSystemK::model()->find()->printkartulsng;;
if(!empty($cekKartuPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKartuPasien=$cekKartuPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKartuPasien=0;
}
//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

$jscript = <<< JS
function listKarcisRI(obj)
{
     ruangan = $('#PPPasienAdmisiT_ruangan_id').val();
     kelasPelayanan=obj;
     if(kelasPelayanan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan },
                function(data){
                    $('#tblFormKarcis tbody').html(data.form);
                    if (jQuery.isNumeric(data.karcis.karcis_id)){
                        tdKarcis = $('#tblFormKarcis tbody tr').find("td a[data-karcis='"+data.karcis.karcis_id+"']");
                        changeBackground(tdKarcis, data.karcis.daftartindakan_id, data.karcis.harga_tariftindakan, data.karcis.karcis_id);
                        getDataRekening(data.karcis.daftartindakan_id,kelasPelayanan,1,'tm');
                    }
             }, "json");
     }      
       
}

function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}
                    
                    
function print(idPasienAdmisi)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('${urlPrintLembarPoli}'+idPasienAdmisi,'printwin','left=100,top=100,width=400,height=400');
        }     
        //PRINT KARTU PASIEN HANYA DILAKUKAN DI RAWAT DARURAT ATAU RAWAT JALAN
//        if(${statusKartuPasien}==1){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
//                    window.open('${urlPrintKartuPasien}'+idPasienAdmisi,'printwi','left=100,top=100,width=400,height=280');
//        }
}

function print_status(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoliDua}'+idPendaftaran + '&status=3','printwin','left=100,top=100,width=940,height=400');
        }
}
            
function print_dua(idPendaftaran, idPasien)
{
        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
            window.open('${urlPrintLembarPoliDua}'+idPendaftaran + '&status=2','printwin','left=100,top=100,width=940,height=400');
        }
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('${urlPrintKartuPasien}'+idPasien,'printwi','left=100,top=100,width=310,height=230');
            var urlKartu = '${urlKartu}/saveKartuPasien&pendaftaran_id='+idPendaftaran;
            $.post(urlKartu, {pendaftaran_id: idPendaftaran}, "json");
        }
}

function printGelang(idPasienAdmisi)
{
   window.open('${urlPrintGelang}'+idPasienAdmisi,'printwin','left=100,top=100,width=400,height=400');    
}

function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PPPasienAdmisiT_pegawai_id').html(data.listDokter);
    }, "json");
}

function printSep(sep_id)
{       
    window.open('${urlPrintSep}'+sep_id,'printwin','left=100,top=100,width=860,height=480');
}
    
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<?php 
//========= Dialog buat cari data pasien =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasienRI',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));
 
//$this->beginWidget('ext.bootstrap.widgets.BootModal',
//    array(
//	'id'=>'dialogPasien',
//	'config'=>array(
//            'header'=>'Pencarian Data Pasien',
//        ),
//	'htmlOptions'=>array(
//            'style'=>'width:850px'
//        ),
//    )
//);

$modDataPasien = new PPPasienM('searchWithDaerah');
$modDataPasien->unsetAttributes();
if(isset($_GET['PPPasienM'])) {
    $modDataPasien->attributes = $_GET['PPPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchWithDaerah(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "
                                        $(\"#dialogPasienRI\").dialog(\"close\");
                                        //$(\"#dialogPasienRI\").modal(\"hide\");
                                        $(\"#noRekamMedik\").val(\"$data->no_rekam_medik\");
                                        $(\"#PPPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                        $(\"#PPPasienM_rhesus\").val(\"$data->rhesus\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                        $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                        setJenisKelaminPasien(\"$data->jeniskelamin\");
                                        setRhesusPasien(\"$data->rhesus\");
                                        kelurahan = String.trim(\'$data->kelurahan_id\');
                                        if(kelurahan.length == 0){
                                            kelurahan = 0;
                                        }
//                                        alert($data->jeniskelamin);
                                        loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id,kelurahan);
                                        $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'namadepan').'\").val(\"$data->namadepan\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_bin').'\").val(\"$data->nama_bin\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_ayah').'\").val(\"$data->nama_ayah\");
                                        $(\"#'.CHtml::activeId($modPasien,'nama_ibu').'\").val(\"$data->nama_ibu\");
                                        $(\"#'.CHtml::activeId($modPasien,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                        $(\"#'.CHtml::activeId($modPasien,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                        $(\"#'.CHtml::activeId($modPasien,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                        $(\"#'.CHtml::activeId($modPasien,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                        $(\"#'.CHtml::activeId($modPasien,'golongandarah').'\").val(\"$data->golongandarah\");
                                        $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                        $(\"#'.CHtml::activeId($modPasien,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'rt').'\").val(\"$data->rt\");
                                        $(\"#'.CHtml::activeId($modPasien,'rw').'\").val(\"$data->rw\");
                                        $(\"#'.CHtml::activeId($modPasien,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'kelurahan_id').'\").val(\"$data->kelurahan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                        $(\"#'.CHtml::activeId($modPasien,'suku_id').'\").val(\"$data->suku_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'alamatemail').'\").val(\"$data->alamatemail\");
                                        $(\"#'.CHtml::activeId($modPasien,'anakke').'\").val(\"$data->anakke\");
                                        $(\"#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                        $(\"#'.CHtml::activeId($modPasien,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                        $(\"#'.CHtml::activeId($modPasien,'agama').'\").val(\"$data->agama\");
                                        $(\"#'.CHtml::activeId($modPasien,'warga_negara').'\").val(\"$data->warga_negara\");
                                        photo = String.trim(\'$data->photopasien\');
                                        if(photo.length > 0)
                                        {
                                            $(\"#cekFoto\").attr(\"src\", \"http://localhost/ehospitaljk/data/images/pasien/$data->photopasien\");
                                        } else
                                        {
                                            $(\"#cekFoto\").attr(\"src\", \"http://localhost/ehospitaljk/data/images/pasien/no_photo.jpeg\");
                                        }
                                        loadUmur(\"$data->tanggal_lahir\");
                                        ubahUmur($data->CekUmurValid);
                                        getStatusBooking($data->pasien_id);
                                    "))',
                ),
                'no_rekam_medik',
                array(
                    'name'=>'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                'nama_bin',
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
                array(
                    'header'=>'Status Bayar',
                    // 'name'=>'pasien_id',
                    'value'=>'PasienM::model()->statusBayar($data->pendaftaran->pembayaranpelayanan_id)',
                    // 'value'=>'$data->pendaftaran->pembayaranpelayanan_id',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>
<?php 
//========= Dialog buat cari data pendaftaranRJ =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPendaftaranRJ',
    'options'=>array(
        'title'=>'Pencarian Data Pendaftaran Rawat Jalan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPendaftaranRJ = new PPPasienTindaklanjutkeRiV('searchFromRJ');
$modPendaftaranRJ->unsetAttributes();
if(isset($_GET['PPPasienTindaklanjutkeRiV'])) {
    $modPendaftaranRJ->attributes = $_GET['PPPasienTindaklanjutkeRiV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasienpendaftaranRJ-m-grid',
	'dataProvider'=>$modPendaftaranRJ->searchFromRJ(),
	'filter'=>$modPendaftaranRJ,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "
                                        $(\"#dialogPendaftaranRJ\").dialog(\"close\");
                                        getPasienFromPendaftaran($data->pendaftaran_id);
                                        getStatusBooking($data->pasien_id);
                                        loadUmur(\"$data->tanggal_lahir\");
                                    "))',
                ),
                'no_pendaftaran',
                array(
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->no_rekam_medik',
                ),
                array(
                    'name'=>'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'header'=>'Nama Alias',
                    'name'=>'nama_bin',
                    'value'=>'$data->nama_bin',
                ),
                array(
                    'name'=>'alamat_pasien',
                    'value'=>'$data->alamat_pasien',
                ),
                array(
                    'name'=>'jeniskasuspenyakit_nama',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
                array(
                    'name'=>'nama_pegawai',
                    'value'=>'$data->nama_pegawai',
                ),
                array(
                    'name'=>'kelompokumur_nama',
                    'value'=>'$data->kelompokumur_nama',
                    'filter'=>  CHtml::listData(KelompokumurM::model()->findAll('kelompokumur_aktif = true'), 'kelompokumur_nama', 'kelompokumur_nama'),
                ),
                array(
                    'name'=>'penjamin_nama',
                    'value'=>'$data->penjamin_nama',
                    'filter'=>  CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'), 'penjamin_nama', 'penjamin_nama'),
                ),
                array(
                    'name'=>'ruangan_nama',
                    'value'=>'$data->ruangan_nama',
                    'filter'=> CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>Params::INSTALASI_ID_RJ),array('order'=>'ruangan_nama')), 'ruangan_nama', 'ruangan_nama'),
                ),
                array(
                    'header'=>'Status Bayar',
                    // 'value'=>'$data->pendaftaran->pembayaranpelayanan_id',
                    'value'=>'PasientindaklanjutkeriV::model()->statusBayar($data->pendaftaran->pembayaranpelayanan_id)',
                ),

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pendaftaranRJ dialog ====================================

//========= Dialog buat cari data pendaftaranRD =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPendaftaranRD',
    'options'=>array(
        'title'=>'Pencarian Data Pendaftaran Rawat Darurat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPendaftaranRD = new PPPasienTindaklanjutkeRiV('searchFromRD');
$modPendaftaranRD->unsetAttributes();
if(isset($_GET['PPPasienTindaklanjutkeRiV'])) {
    $modPendaftaranRD->attributes = $_GET['PPPasienTindaklanjutkeRiV'];
}
// this is the date picker
//$dateisOn = $this->widget('MyDateTimePicker', array(
//                                    'name' => CHtml::activeName($modPendaftaranRD, 'tanggalAwal'),
//                                    'language' => 'id',
//                                    'value' => $modPendaftaranRD->tanggalAwal,
//                                    'mode'=>'date',
//                                    'options'=>array(
//                                        'showAnim'=>'fold',
//                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                        'maxDate' => 'd',
//                                    ),
//                                    'htmlOptions'=>array(
//                                        'readonly'=>true, 'style'=>'width:80px;',
//                                        'rel'=>'tooltip','title'=>'Tanggal Awal',
//                                    ),
//                                ),true).$this->widget('MyDateTimePicker', array(
//                                    'name' => CHtml::activeName($modPendaftaranRD, 'tanggalAkhir'),
//                                    'language' => 'id',
//                                    'value' => $modPendaftaranRD->tanggalAkhir,
//                                    'mode'=>'date',
//                                    'options'=>array(
//                                        'showAnim'=>'fold',
//                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                        'maxDate' => 'd',
//                                    ),
//                                    'htmlOptions'=>array(
//                                        'readonly'=>true, 'style'=>'width:80px;',
//                                        'rel'=>'tooltip','title'=>'Tanggal Akhir',
//                                    ),
//                                ),true);

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasienpendaftaranRD-m-grid',
	'dataProvider'=>$modPendaftaranRD->searchFromRD(),
	'filter'=>$modPendaftaranRD,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "
                                        $(\"#dialogPendaftaranRD\").dialog(\"close\");
                                        getPasienFromPendaftaran($data->pendaftaran_id);
                                        getStatusBooking($data->pasien_id);
                                        loadUmur(\"$data->tanggal_lahir\");
                                    "))',
                ),
                'no_pendaftaran',
                array(
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->no_rekam_medik',
                ),
                array(
                    'name'=>'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'name'=>'nama_bin',
                    'value'=>'$data->nama_bin',
                ),
                array(
                    'name'=>'alamat_pasien',
                    'value'=>'$data->alamat_pasien',
                ),
                array(
                    'name'=>'jeniskasuspenyakit_nama',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
                array(
                    'name'=>'nama_pegawai',
                    'value'=>'$data->nama_pegawai',
                ),
                array(
                    'name'=>'kelompokumur_nama',
                    'value'=>'$data->kelompokumur_nama',
                    'filter'=>  CHtml::listData(KelompokumurM::model()->findAll('kelompokumur_aktif = true'), 'kelompokumur_nama', 'kelompokumur_nama'),
                ),
                array(
                    'name'=>'penjamin_nama',
                    'value'=>'$data->penjamin_nama',
                    'filter'=>  CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'), 'penjamin_nama', 'penjamin_nama'),
                ),
                array(
                    'name'=>'ruangan_nama',
                    'value'=>'$data->ruangan_nama',
                    'filter'=> CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=>Params::INSTALASI_ID_RD),array('order'=>'ruangan_nama')), 'ruangan_nama', 'ruangan_nama'),
                ),
	),
        'afterAjaxUpdate'=>"function(id, data){
                                jQuery('".Params::TOOLTIP_SELECTOR."').tooltip({'placement':'".Params::TOOLTIP_PLACEMENT."'});
                                jQuery('#".CHtml::activeId($modPendaftaranRD, 'tanggalAwal')."').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'".Params::DATE_FORMAT_MEDIUM."','changeMonth':'true','showButtonPanel':'false','changeYear':'true','maxDate':'d'}));
                                jQuery('#".CHtml::activeId($modPendaftaranRD, 'tanggalAkhir')."').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'showAnim':'fold','dateFormat':'".Params::DATE_FORMAT_MEDIUM."','changeMonth':'true','showButtonPanel':'false','changeYear':'true','maxDate':'d'}));
                            }",
));

$this->endWidget();
//========= end pendaftaranRD dialog ====================================

$urlGetPasienFromPendaftaran = Yii::app()->createUrl('ActionAjax/getPasienFromPendaftaran');
$urlGetPendaftaranFromPasien = Yii::app()->createUrl('ActionAjax/getPendaftaranFromPasien');
$urlGetStatusBooking = Yii::app()->createUrl('ActionAjax/getStatusBooking');

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>
<script type="text/javascript">
    function formSubmit(obj, evt)
    {
        $('#pppendaftaran-rj-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
        $('#pppendaftaran-rj-form').submit();
    }

    function formAdmisi(idCaraMasuk)
    {
//        clearForm();
        if(idCaraMasuk == <?php echo Params::CARAMASUK_ID_RJ; ?> || idCaraMasuk == <?php echo Params::CARAMASUK_ID_RD; ?>) {
            $("#<?php echo CHtml::activeId($model, 'no_pendaftaran');?>").removeAttr('disabled');
            $('#fieldsetPasien input').attr('disabled','true');
            $('#fieldsetPasien #isUpdatePasien').show();
            $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
            $('#fieldsetPasien select').attr('disabled','true');
            $('#fieldsetPasien textarea').attr('disabled','true');
            $('#fieldsetPasien button').attr('disabled','true');
            $('#fieldsetDetailPasien input').attr('disabled','true');
            $('#fieldsetDetailPasien select').attr('disabled','true');
            $('#controlNoRekamMedik button').attr('disabled','true');
            $('#controlNoRekamMedik input').attr('readonly','true');
            $('#isPasienLama').attr('checked','checked');
            $('#detail_data_pasien').slideDown(500);
            $('#cex_detaildatapasien').attr('checked','checked');
            $('#tombolPasienDialog').addClass('hide');
            if(idCaraMasuk == <?php echo Params::CARAMASUK_ID_RD; ?>){
                $("#<?php echo CHtml::activeId($model, 'no_pendaftaran'); ?>").attr('disabled','true');
                $('#btnPendaftaranRD').removeAttr('disabled');//show();
                $('#btnPendaftaranRJ').attr('disabled','true');//.hide();
                $('#btnPendaftaranRD').show();
                $('#btnPendaftaranRJ').hide();
                $('#tombolPendaftaranDialogRD').removeClass('hide');
                $('#tombolPendaftaranDialogRJ').addClass('hide');
                $('#dialogPendaftaranRD').dialog('open');
//                alert('rawatDarurat');
            }
            if(idCaraMasuk == <?php echo Params::CARAMASUK_ID_RJ; ?>){
                $("#<?php echo CHtml::activeId($model, 'no_pendaftaran'); ?>").attr('disabled','true');
                $('#btnPendaftaranRJ').removeAttr('disabled');//show();
                $('#btnPendaftaranRD').attr('disabled','true');//hide();
                $('#btnPendaftaranRJ').show();
                $('#btnPendaftaranRD').hide();
                $('#tombolPendaftaranDialogRD').addClass('hide');
                $('#tombolPendaftaranDialogRJ').removeClass('hide');
                $('#dialogPendaftaranRJ').dialog('open');
//                alert('rawatJalan');
            }
        } else {
            $("#<?php echo CHtml::activeId($model, 'no_pendaftaran'); ?>").attr('disabled','true');
            $('#PPPendaftaranRi_pendaftaran_id').val('');
            $('#PPPendaftaranRi_no_pendaftaran').val('');
            $('#PPPasienAdmisiT_bookingkamar_id').val('');
            $('#fieldsetPasien input').removeAttr('disabled');
            $('#fieldsetPasien #isUpdatePasien').hide();
            $('#fieldsetPasien select').removeAttr('disabled');
            $('#fieldsetPasien textarea').removeAttr('disabled');
            $('#fieldsetPasien button').removeAttr('disabled');
            $('#fieldsetDetailPasien input').removeAttr('disabled');
            $('#fieldsetDetailPasien select').removeAttr('disabled');
            $('#controlNoRekamMedik input').removeAttr('readonly');
            $('#isPasienLama').removeAttr('checked');
            $('#noRekamMedik').removeAttr('readonly');
            $('#noRekamMedik').attr('readonly','true');
            $('#btnPendaftaranRD').hide();
            $('#btnPendaftaranRJ').hide();
            $('#tombolPendaftaranDialogRD').addClass('hide');
            $('#tombolPendaftaranDialogRJ').addClass('hide');
//            $('#fieldsetPasien input').val('');
//            $('#controlNoRekamMedik input').val('');
//            $('#PPPendaftaranRi_keadaanmasuk').val('');
//            $('#PPPendaftaranRi_transportasi').val('');
        }
    }
    
    function getPasienFromPendaftaran(idPendaftaran){
        $.post("<?php echo $urlGetPasienFromPendaftaran;?>", { idPendaftaran: idPendaftaran },
            function(data){
                $('#noRekamMedik').val(data.pasien.no_rekam_medik);
                setJenisKelaminPasien(data.pasien.jeniskelamin);
                setRhesusPasien(data.pasien.rhesus);
                loadDaerahPasien(data.pasien.propinsi_id,data.pasien.kabupaten_id,data.pasien.kecamatan_id,data.pasien.kelurahan_id);
                $("#<?php echo CHtml::activeId($modPasien,'jenisidentitas');?>").val(data.pasien.jenisidentitas);
                $("#<?php echo CHtml::activeId($modPasien,'no_identitas_pasien');?>").val(data.pasien.no_identitas_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'namadepan');?>").val(data.pasien.namadepan);
                $("#<?php echo CHtml::activeId($modPasien,'nama_pasien');?>").val(data.pasien.nama_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'nama_bin');?>").val(data.pasien.nama_bin);
                $("#<?php echo CHtml::activeId($modPasien,'tempat_lahir');?>").val(data.pasien.tempat_lahir);
                $("#<?php echo CHtml::activeId($modPasien,'tanggal_lahir');?>").val(data.pasien.tanggal_lahir);
                $("#<?php echo CHtml::activeId($modPasien,'kelompokumur_id');?>").val(data.pasien.kelompokumur_id);
                $("#<?php echo CHtml::activeId($modPasien,'jeniskelamin');?>").val(data.pasien.jeniskelamin);
                $("#<?php echo CHtml::activeId($modPasien,'statusperkawinan');?>").val(data.pasien.statusperkawinan);
                $("#<?php echo CHtml::activeId($modPasien,'golongandarah');?>").val(data.pasien.golongandarah);
                $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.pasien.rhesus);
                $("#<?php echo CHtml::activeId($modPasien,'alamat_pasien');?>").val(data.pasien.alamat_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'rt');?>").val(data.pasien.rt);
                $("#<?php echo CHtml::activeId($modPasien,'rw');?>").val(data.pasien.rw);
                $("#<?php echo CHtml::activeId($modPasien,'propinsi_id');?>").val(data.pasien.propinsi_id);
                $("#<?php echo CHtml::activeId($modPasien,'kabupaten_id');?>").val(data.pasien.kabupaten_id);
                $("#<?php echo CHtml::activeId($modPasien,'kecamatan_id');?>").val(data.pasien.kecamatan_id);
                $("#<?php echo CHtml::activeId($modPasien,'kelurahan_id');?>").val(data.pasien.kelurahan_id);
                $("#<?php echo CHtml::activeId($modPasien,'no_telepon_pasien');?>").val(data.pasien.no_telepon_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'no_mobile_pasien');?>").val(data.pasien.no_mobile_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'suku_id');?>").val(data.pasien.suku_id);
                $("#<?php echo CHtml::activeId($modPasien,'alamatemail');?>").val(data.pasien.alamatemail);
                $("#<?php echo CHtml::activeId($modPasien,'anakke');?>").val(data.pasien.anakke);
                $("#<?php echo CHtml::activeId($modPasien,'jumlah_bersaudara');?>").val(data.pasien.jumlah_bersaudara);
                $("#<?php echo CHtml::activeId($modPasien,'pendidikan_id');?>").val(data.pasien.pendidikan_id);
                $("#<?php echo CHtml::activeId($modPasien,'pekerjaan_id');?>").val(data.pasien.pekerjaan_id);
                $("#<?php echo CHtml::activeId($modPasien,'agama');?>").val(data.pasien.agama);
                $("#<?php echo CHtml::activeId($modPasien,'warga_negara');?>").val(data.pasien.warga_negara);
                //loadUmur(data.pasien.tanggal_lahir);

                $("#<?php echo CHtml::activeId($model,'caramasuk_id');?>").val(data.pendaftaran.caramasuk_id);
                $("#<?php echo CHtml::activeId($model,'keadaanmasuk');?>").val(data.pendaftaran.keadaanmasuk);
                $("#<?php echo CHtml::activeId($model,'transportasi');?>").val(data.pendaftaran.transportasi);
                $("#<?php echo CHtml::activeId($model,'pegawai_id');?>").val(data.pendaftaran.pegawai_id);
                $("#<?php echo CHtml::activeId($model,'kelaspelayanan_id');?>").val(data.pendaftaran.kelaspelayanan_id);
                $("#<?php echo CHtml::activeId($model,'no_pendaftaran');?>").val(data.pendaftaran.no_pendaftaran);
                $("#<?php echo CHtml::activeId($model,'jeniskasuspenyakit_id');?>").val(data.pendaftaran.jeniskasuspenyakit_id);
                $("#<?php echo CHtml::activeId($model,'ruangan_id');?>").val(data.pendaftaran.ruangan_id);
                $("#<?php echo CHtml::activeId($model,'penanggungjawab_id');?>").val(data.pendaftaran.penanggungjawab_id);
                $("#<?php echo CHtml::activeId($model,'rujukan_id');?>").val(data.pendaftaran.rujukan_id);
                $("#<?php echo CHtml::activeId($model,'umur');?>").val(data.pendaftaran.umur);
                $("#<?php echo CHtml::activeId($model,'no_asuransi');?>").val(data.pendaftaran.no_asuransi);
                $("#<?php echo CHtml::activeId($model,'namapemilik_asuransi');?>").val(data.pendaftaran.namapemilik_asuransi);
                $("#<?php echo CHtml::activeId($model,'nopokokperusahaan');?>").val(data.pendaftaran.nopokokperusahaan);
                $("#<?php echo CHtml::activeId($model,'kelastanggungan_id');?>").val(data.pendaftaran.kelastanggungan_id);
                $("#<?php echo CHtml::activeId($model,'namaperusahaan');?>").val(data.pendaftaran.namaperusahaan);
                $("#<?php echo CHtml::activeId($model,'penjamin_id');?>").val(data.pendaftaran.penjamin_id);

                if($.trim(data.pendaftaran.penanggungjawab_id) != '')
                    loadPenanggungJawab(data.pendaftaran.penanggungjawab_id);
                else
                    clearPenanggungJawab();

                $("#<?php echo CHtml::activeId($model,'pendaftaran_id');?>").val(data.pendaftaran.pendaftaran_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'kelaspelayanan_id');?>").val(data.pendaftaran.kelaspelayanan_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'carabayar_id');?>").val(data.pendaftaran.carabayar_id);
                loadListPenjamin(data.pendaftaran.carabayar_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'penjamin_id');?>").val(data.pendaftaran.penjamin_id);

        }, "json");
    }

    function loadPenanggungJawab(idPenanggungJawab)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getPenanggungJawab');?>", { idPenanggungJawab: idPenanggungJawab },
            function(data){
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'pengantar');?>").val(data.penanggungjawab.pengantar);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'nama_pj');?>").val(data.penanggungjawab.nama_pj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'alamat_pj');?>").val(data.penanggungjawab.alamat_pj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'hubungankeluarga');?>").val(data.penanggungjawab.hubungankeluarga);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'jenisidentitas');?>").val(data.penanggungjawab.jenisidentitas);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_identitas_pj');?>").val(data.penanggungjawab.no_identitas_pj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_mobilepj');?>").val(data.penanggungjawab.no_mobilepj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_teleponpj');?>").val(data.penanggungjawab.no_teleponpj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'tempatlahir_pj');?>").val(data.penanggungjawab.tempatlahir_pj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'tgllahir_pj');?>").val(data.penanggungjawab.tgllahir_pj);
                $("#<?php echo CHtml::activeId($modPenanggungJawab,'jeniskelamin');?>").val(data.penanggungjawab.jeniskelamin);
                setJenisKelaminPenanggungJawab(data.penanggungjawab.jeniskelamin);
            }, "json");
    }

    function clearPenanggungJawab()
    {
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'pengantar');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'nama_pj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'alamat_pj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'hubungankeluarga');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'jenisidentitas');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_identitas_pj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_mobilepj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'no_teleponpj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'tempatlahir_pj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'tgllahir_pj');?>").val('');
        $("#<?php echo CHtml::activeId($modPenanggungJawab,'jeniskelamin');?>").val('');
        $('input[name="<?php echo CHtml::activeName($modPenanggungJawab, 'jeniskelamin') ?>"]').each(function(){
                $(this).attr('checked',false);
            }
        );
    }

    function setJenisKelaminPenanggungJawab(jenisKelamin)
    {
        $('input[name="<?php echo CHtml::activeName($modPenanggungJawab, 'jeniskelamin') ?>"]').each(function(){
                if(this.value == jenisKelamin)
                    $(this).attr('checked',true);
            }
        );
    }

    function loadListPenjamin(idCaraBayar)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getListPenjamin');?>", { idCaraBayar: idCaraBayar },
            function(data){
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'penjamin_id');?>").html(data.listPenjamin);
            }, "json");
    }
    
    function validasiForm()
    {
        var validPasien = validasiFormPasien();
        var validAdmisi = validasiFormAdmisi();
        var validPenanggungjawab = validasiFormPenanggungjawab();
        
        var caraMasuk = $("#PPPasienAdmisiT_caramasuk_id").val();
        var caraBayar = $("#PPPasienAdmisiT_carabayar_id").val();
        var penjamin = $("#PPPasienAdmisiT_penjamin_id").val();
        
        if($("#isUpdatePasien").is(':checked'))
        {
            if(validPasien && validAdmisi && validPenanggungjawab && caraBayar && penjamin)
            {
                return true;
                setKonfirmasi();
            }else{
                if(!caraMasuk){
                    alert ('Silahkan isi data Cara Masuk !');
                }else if(!validPasien){
                    alert ('Silahkan isi data dengan tanda * pada Form Pasien !');
                }else if(!validAdmisi){
                    alert ('Silahkan isi data dengan tanda * pada Form Admisi !');
                }else if(!(caraBayar && penjamin)){
                    alert('Silahkan isi data Cara Bayar dan Penjamin !');
                }else if(!validPenanggungjawab){
                    alert ('Silahkan isi data dengan tanda * pada Form Penanggung Jawab !');
                }   
                return false;        
            }
        }else{
            if(validAdmisi && validPenanggungjawab && caraBayar && penjamin)
            {
                return true;
                setKonfirmasi();
            }else{
                if(!caraMasuk){
                    alert ('Silahkan isi data Cara Masuk !');
                }else if(!validAdmisi){
                    alert ('Silahkan isi data dengan tanda * pada Form Admisi !');
                }else if(!(caraBayar && penjamin)){
                    alert('Silahkan isi data Cara Bayar dan Penjamin !');
                }else if(!validPenanggungjawab){
                    alert ('Silahkan isi data dengan tanda * pada Form Penanggung Jawab !');
                }   
                return false;        
            }            
        }   
        
    }
    
    function validasiFormPasien()
    {
        var valid = true;
        var kosong = "";
        var jumlahKosong = $("#fieldsetPasien").find(".reqPasien[value="+kosong+"]");
        if(jumlahKosong.length > 0){
//            alert ('Silahkan isi data dengan tanda * pada Form Pasien');
            valid = false;
        }
        return valid;
    }
    
    function validasiFormAdmisi()
    {
        var valid = true;
        var kosong = "";
        var jumlahKosong = $("#fieldsetAdmisi").find(".reqAdmisi[value="+kosong+"]");
        if(jumlahKosong.length > 0){
//            alert ('Silahkan isi data dengan tanda * pada Form Admisi');
            valid = false;
        }
        return valid;
    }
    
    function validasiFormPenanggungjawab()
    {
        var valid = true;
        var kosong = "";
        var jumlahKosong = $("#fieldsetPenanggungjawab").find(".reqPj[value="+kosong+"]");
        if(jumlahKosong.length > 0){
//            alert ('Silahkan isi data dengan tanda * pada Form Penanggung Jawab');
            valid = false;
        }
        return valid;
    }
    $('#pakeAsuransi').attr('style','display:none;');
    /*function caraBayarChange(obj)
    {
    //    var penjamin = document.getElementById('LKPendaftaranMp_penjamin_id');
        if(obj.value == 1 || obj.value == 0 ){
           setTimeout( 'var penjamin = document.getElementById(\'PPPendaftaranT_penjamin_id\'); penjamin.selectedIndex = 1;', '500' );
           document.getElementById('pakeAsuransi').checked = false;
           $('#pakeAsuransi').attr('style','display:none;');
           $('#divAsuransi').hide(500);
        }else{
            $('#pakeAsuransi').removeAttr('style');
            document.getElementById('pakeAsuransi').checked = true;
            $('#divAsuransi input').removeAttr('disabled');
            $('#divAsuransi select').removeAttr('disabled');
            $('#divAsuransi').show(500);
    //        $('#pakeAsuransi').click();
        }
    }*/

    function caraBayarChange(obj){
        var noMr = $("#noRekamMedik").val();
        var noMrLama = $("#noRekamMedikLama").val();
        $("#carabayarSep").val(obj.value);
        if(obj.value == 1 || obj.value == 0 ){
           document.getElementById('pakeAsuransi').checked = false;
           $('#pakeAsuransi').attr('style','display:none;');
           $('#divAsuransi').hide(500);
           $('#divAsuransiSep').hide(500);
        }else{
            $('#pakeAsuransi').removeAttr('style');
            document.getElementById('pakeAsuransi').checked = true;
            
            <?php 
            if(Yii::app()->user->getState('is_bridging')==TRUE){ 
            ?>
                if((noMr != '') && $(obj).val() == 18){
                    $('#divAsuransi').hide(500);
                    $('#divAsuransiSep input').removeAttr('disabled');
                    $('#divAsuransiSep select').removeAttr('disabled');
                    $('#divAsuransiSep textarea').removeAttr('disabled');
                    $('#divAsuransiSep').show(500);
                }else{
                    $('#divAsuransiSep').hide(500);
                    $('#divAsuransiSep input').attr('disabled','true');
                    $('#divAsuransiSep select').attr('disabled','true');
                    $('#divAsuransiSep textarea').attr('disabled','true');
                    $('#divAsuransi input').removeAttr('disabled');
                    $('#divAsuransi select').removeAttr('disabled');
                    $('#divAsuransi').show(500);
                }
                
            <?php 
            }else{
            ?>
                $('#divAsuransi input').removeAttr('disabled');
                $('#divAsuransi select').removeAttr('disabled');
                $('#divAsuransi').show(500);
            <?php
            }
            ?>                        
        }
    }
    
    function getPendaftaranFromPasien(idPasien)
    {
        $.post("<?php echo $urlGetPendaftaranFromPasien;?>", { idPasien: idPasien },
            function(data){
                $('#noRekamMedik').val(data.pasien.no_rekam_medik);
                setJenisKelaminPasien(data.pasien.jeniskelamin);
                setRhesusPasien(data.pasien.rhesus);
                loadDaerahPasien(data.pasien.propinsi_id,data.pasien.kabupaten_id,data.pasien.kecamatan_id,data.pasien.kelurahan_id);
                $("#<?php echo CHtml::activeId($modPasien,'jenisidentitas');?>").val(data.pasien.jenisidentitas);
                $("#<?php echo CHtml::activeId($modPasien,'no_identitas_pasien');?>").val(data.pasien.no_identitas_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'namadepan');?>").val(data.pasien.namadepan);
                $("#<?php echo CHtml::activeId($modPasien,'nama_pasien');?>").val(data.pasien.nama_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'nama_bin');?>").val(data.pasien.nama_bin);
                $("#<?php echo CHtml::activeId($modPasien,'tempat_lahir');?>").val(data.pasien.tempat_lahir);
                $("#<?php echo CHtml::activeId($modPasien,'tanggal_lahir');?>").val(data.pasien.tanggal_lahir);
                $("#<?php echo CHtml::activeId($modPasien,'kelompokumur_id');?>").val(data.pasien.kelompokumur_id);
                $("#<?php echo CHtml::activeId($modPasien,'jeniskelamin');?>").val(data.pasien.jeniskelamin);
                $("#<?php echo CHtml::activeId($modPasien,'statusperkawinan');?>").val(data.pasien.statusperkawinan);
                $("#<?php echo CHtml::activeId($modPasien,'golongandarah');?>").val(data.pasien.golongandarah);
                $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.pasien.rhesus);
                $("#<?php echo CHtml::activeId($modPasien,'alamat_pasien');?>").val(data.pasien.alamat_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'rt');?>").val(data.pasien.rt);
                $("#<?php echo CHtml::activeId($modPasien,'rw');?>").val(data.pasien.rw);
                $("#<?php echo CHtml::activeId($modPasien,'propinsi_id');?>").val(data.pasien.propinsi_id);
                $("#<?php echo CHtml::activeId($modPasien,'kabupaten_id');?>").val(data.pasien.kabupaten_id);
                $("#<?php echo CHtml::activeId($modPasien,'kecamatan_id');?>").val(data.pasien.kecamatan_id);
                $("#<?php echo CHtml::activeId($modPasien,'kelurahan_id');?>").val(data.pasien.kelurahan_id);
                $("#<?php echo CHtml::activeId($modPasien,'no_telepon_pasien');?>").val(data.pasien.no_telepon_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'no_mobile_pasien');?>").val(data.pasien.no_mobile_pasien);
                $("#<?php echo CHtml::activeId($modPasien,'suku_id');?>").val(data.pasien.suku_id);
                $("#<?php echo CHtml::activeId($modPasien,'alamatemail');?>").val(data.pasien.alamatemail);
                $("#<?php echo CHtml::activeId($modPasien,'anakke');?>").val(data.pasien.anakke);
                $("#<?php echo CHtml::activeId($modPasien,'jumlah_bersaudara');?>").val(data.pasien.jumlah_bersaudara);
                $("#<?php echo CHtml::activeId($modPasien,'pendidikan_id');?>").val(data.pasien.pendidikan_id);
                $("#<?php echo CHtml::activeId($modPasien,'pekerjaan_id');?>").val(data.pasien.pekerjaan_id);
                $("#<?php echo CHtml::activeId($modPasien,'agama');?>").val(data.pasien.agama);
                $("#<?php echo CHtml::activeId($modPasien,'warga_negara');?>").val(data.pasien.warga_negara);
                //loadUmur(data.pasien.tanggal_lahir);

                $("#<?php echo CHtml::activeId($model,'caramasuk_id');?>").val(data.pendaftaran.caramasuk_id);
                $("#<?php echo CHtml::activeId($model,'keadaanmasuk');?>").val(data.pendaftaran.keadaanmasuk);
                $("#<?php echo CHtml::activeId($model,'transportasi');?>").val(data.pendaftaran.transportasi);
                $("#<?php echo CHtml::activeId($model,'pegawai_id');?>").val(data.pendaftaran.pegawai_id);
                $("#<?php echo CHtml::activeId($model,'kelaspelayanan_id');?>").val(data.pendaftaran.kelaspelayanan_id);
                $("#<?php echo CHtml::activeId($model,'no_pendaftaran');?>").val(data.pendaftaran.no_pendaftaran);
                $("#<?php echo CHtml::activeId($model,'jeniskasuspenyakit_id');?>").val(data.pendaftaran.jeniskasuspenyakit_id);
                $("#<?php echo CHtml::activeId($model,'ruangan_id');?>").val(data.pendaftaran.ruangan_id);
                $("#<?php echo CHtml::activeId($model,'penanggungjawab_id');?>").val(data.pendaftaran.penanggungjawab_id);
                $("#<?php echo CHtml::activeId($model,'rujukan_id');?>").val(data.pendaftaran.rujukan_id);
                $("#<?php echo CHtml::activeId($model,'umur');?>").val(data.pendaftaran.umur);
                $("#<?php echo CHtml::activeId($model,'no_asuransi');?>").val(data.pendaftaran.no_asuransi);
                $("#<?php echo CHtml::activeId($model,'namapemilik_asuransi');?>").val(data.pendaftaran.namapemilik_asuransi);
                $("#<?php echo CHtml::activeId($model,'nopokokperusahaan');?>").val(data.pendaftaran.nopokokperusahaan);
                $("#<?php echo CHtml::activeId($model,'kelastanggungan_id');?>").val(data.pendaftaran.kelastanggungan_id);
                $("#<?php echo CHtml::activeId($model,'namaperusahaan');?>").val(data.pendaftaran.namaperusahaan);
                $("#<?php echo CHtml::activeId($model,'penjamin_id');?>").val(data.pendaftaran.penjamin_id);

                if($.trim(data.pendaftaran.penanggungjawab_id) != '')
                    loadPenanggungJawab(data.pendaftaran.penanggungjawab_id);
                else
                    clearPenanggungJawab();

                $("#<?php echo CHtml::activeId($model,'pendaftaran_id');?>").val(data.pendaftaran.pendaftaran_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'kelaspelayanan_id');?>").val(data.pendaftaran.kelaspelayanan_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'carabayar_id');?>").val(data.pendaftaran.carabayar_id);
                loadListPenjamin(data.pendaftaran.carabayar_id);
                $("#<?php echo CHtml::activeId($modPasienAdmisi,'penjamin_id');?>").val(data.pendaftaran.penjamin_id);

        }, "json");
    }
    
    function clearForm()
    {
        $('#fieldsetPasien input').val('');
    }
    
    function getStatusBooking(idPasien){
        var idPasien = idPasien;
//        alert(idPasien);
         $.post("<?php echo $urlGetStatusBooking;?>", { idPasien: idPasien },
            function(data){
                if(data.status == 'ada'){
                    alert('Anda telah melakukan Booking Sebelumnya ');
                    $('#dialogAddRuangan').dialog('open');
                    $('#dialogAddRuangan div.divForForm').html(data.div);
                }
                return false;
        }, "json");
    }
    
    function setKonfirmasi(obj){
        $(obj).attr('disabled',true);
        $(obj).removeAttr('onclick');
        $('#pppendaftaran-rj-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
        $('#pppendaftaran-rj-form .form-actions').addClass('srbacLoading');
        $('#pppendaftaran-rj-form').submit();
    }
</script>

<?php 
    if(PPKonfigSystemK::model()->find()->isantrian==TRUE){ 
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogAntrian',
            'options'=>array(
                'title'=>'Antrian',
                'autoOpen'=>true,
                'resizable'=>false,
                'width'=>170,
                'height'=>150,
                'position'=>'right',
            ),
        ));

            echo '<iframe src="'.$this->createUrl('TampilAntrian/PanggilAntrian').'" width="100%" height="100px" scrolling="no"></iframe>';

        $this->endWidget('zii.widgets.jui.CJuiDialog');
        
    }
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id'		 => 'dialog-proses-sep',
        'options'	 => array(
        'title'		 => 'Proses SEP',
        'autoOpen'	 => false,
        'modal'		 => true,
        'minWidth'	 => 960,
        'minHeight'	 => 480,
        'resizable'	 => false,
        ),
));
echo '<iframe id="iframeProsesSEP"  name="iframeProsesSEP" width="100%" height="550" >
</iframe>';
?>
<?php $this->endWidget(); ?>