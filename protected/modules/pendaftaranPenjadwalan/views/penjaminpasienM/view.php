<?php// $this->renderPartial('_tab'); ?>
<?php
$this->breadcrumbs=array(
	'Pppenjaminpasien Ms'=>array('index'),
	$model->penjamin_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Penjamin Pasien '/*.$model->penjamin_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Penjamin Pasien', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Penjamin Pasien', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Penjamin Pasien', 'icon'=>'pencil','url'=>array('update','id'=>$model->penjamin_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Penjamin Pasien','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->penjamin_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Penjamin Pasien', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'penjamin_id',
		'carabayar.carabayar_nama',
		'penjamin_nama',
		'penjamin_namalainnya',
		array(               // related city displayed as a link
                    'name'=>'penjamin_aktif',
                    'type'=>'raw',
                    'value'=>(($model->penjamin_aktif==1)? Yii::t('mds','Yes') : Yii::t('mds','No')),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>