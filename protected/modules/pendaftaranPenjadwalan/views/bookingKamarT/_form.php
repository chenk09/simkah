<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'ppbooking-kamar-t-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#',
        ));
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
    <legend class="rim2">Booking Kamar</legend>
    <p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>
    <?php echo $form->errorSummary(array($model)); ?>
    <fieldset>
        <legend class="rim">Data Booking</legend>

        <?php echo $form->textFieldRow($model, 'nama', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
        <?php echo $form->textFieldRow($model, 'no_telp', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
        <?php echo $form->textAreaRow($model, 'alamat', array('rows' => 3, 'cols' => 50, 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'placeholder' => 'Alamat')); ?>

        <?php
        echo $form->dropDownListRow($model, 'ruangan_id', CHtml::listData(KamarruanganM::model()->getRuanganItems(Params::INSTALASI_ID_RI), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --',
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'ajax' => array(
                'type' => 'POST',
                'url' => Yii::app()->createUrl('ActionDynamic/GetKamarRuangan', array('encode' => false, 'namaModel' => 'PPBookingKamarT')),
                'update' => '#PPBookingKamarT_kamarruangan_id',)));
        ?>
        <?php
        $dataKamarRuangan = array();
        if ($model->ruangan_id) {
            $kamarRuangan = KamarruanganM::model()->findAll('ruangan_id=' . $model->ruangan_id . ' and kamarruangan_aktif = true ORDER BY kamarruangan_nokamar');
            $dataKamarRuangan = CHtml::listData($kamarRuangan, 'kamarruangan_id', 'KamarDanTempatTidur');
        }
        echo $form->dropDownListRow($model, 'kamarruangan_id', $dataKamarRuangan, array('empty' => '-- Pilih --',
            'onkeypress' => "return $(this).focusNextInputField(event)", 'onChange' => 'getStatus(this)',
            'ajax' => array(
                'type' => 'POST',
                'url' => Yii::app()->createUrl('ActionDynamic/GetKelasPelayanan', array('encode' => false, 'namaModel' => 'PPBookingKamarT')),
                'update' => '#PPBookingKamarT_kelaspelayanan_id',)));
        ?>
        <div class="divForForm" style="margin-left:400px;margin-top:-25px;font-family:tahoma;">

        </div><br>
        <br><?php 
        $dataKelasPelayanan = array();
        if ($model->kamarruangan_id){
            $kelasPelayanan = KamarruanganM::model()->with('kelaspelayanan')->findAll('kamarruangan_id='.$model->kamarruangan_id.'',array());
            $dataKelasPelayanan=CHtml::listData($kelasPelayanan,'kelaspelayanan_id','kelaspelayanan.kelaspelayanan_nama');
        }
        echo $form->dropDownListRow($model, 'kelaspelayanan_id', $dataKelasPelayanan, array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)")); ?>

        <div class='control-group'>
            <?php echo $form->labelEx($model, 'tglbookingkamar', array('class' => 'control-label')) ?>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglbookingkamar',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        'minDate' => 'd',
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"),
                ));
                ?>
                <?php echo $form->error($model, 'tglbookingkamar'); ?>
            </div>
        </div>

        <?php echo $form->dropDownListRow($model, 'statusbooking_dropdown', StatusBooking::items(), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)", 'disabled' => true)); ?>
        <?php echo $form->hiddenField($model, 'statusbooking', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>

        <?php echo $form->textAreaRow($model, 'keteranganbooking', array('rows' => 3, 'cols' => 50, 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'placeholder' => 'Keterangan Booking')); ?> 
        </filedset>
    </fieldset>
    <div class='form-actions'>
        <?php
        echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array(
                            '{icon}' => '<i class="icon-ok icon-white"></i>')) : Yii::t('mds', '{icon} Save', array(
                            '{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'onclick' => 'setKonfirmasi(this);',
            'onKeypress' => 'return formSubmit(this,event)', 'disabled' => ($print == 1)));
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-refresh icon-white"></i>')), $this->createUrl('create'), array('class' => 'btn btn-danger')); ?>
        <?php if (($print == 1) AND ( PPKonfigSystemK::model()->find()->printkartulsng == TRUE)) {
            ?>
            <script>
                print(<?php echo $model->bookingkamar_id ?>);
            </script>
            <?php
        }
        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}' => '<i class="icon-print icon-white"></i>')), '#', array('class' => 'btn btn-info', 'onclick' => "print('$model->bookingkamar_id');return false", 'disabled' => FALSE));
        ?> 
        <?php
        $content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.transaksi', array(), true);
        $this->widget('TipsMasterData', array('type' => 'create', 'content' => $content));
        ?>   
        <?php $this->endWidget(); ?>
    </div>
    <script>
        function enableInputNoPend(obj)
        {

            if (!obj.checked) {
                $('#inputNoPendaftaran input').attr('disabled', 'true');
                $('#inputNoPendaftaran button').attr('disabled', 'true');
            } else {
                $('#inputNoPendaftaran input').removeAttr('disabled');
                $('#inputNoPendaftaran button').removeAttr('disabled');
                getRuanganberdasarkanRM(obj);

            }
        }
    </script>
    <?php
    $enableInputPendaftaran = ($model->isNoPendaftaran) ? 1 : 0;
    $js = <<< JS
if(${enableInputPendaftaran}) {
            $('#inputNoPendaftaran input').attr('disabled','true');
            $('#inputNoPendaftaran button').attr('disabled','true');
}
else {
            $('#inputNoPendaftaran input').attr('disabled','true');
            $('#inputNoPendaftaran button').attr('disabled','true');
}
JS;
    Yii::app()->clientScript->registerScript('formPasienNopend', $js, CClientScript::POS_READY);
    ?>
    <?php
    $urlgetRuanganberdasarkanRM = Yii::app()->createUrl('ActionAjax/RuanganberdasarkanRM');
    $urlgetStatusKamar = Yii::app()->createUrl('ActionAjax/getStatusKamar');
    $idNoRM = CHtml::activeId($modPasien, 'no_rekam_medik');
    $pendaftaranid = CHtml::activeId($model, 'pendaftaran_id');
    $pasien_id = CHtml::activeId($model, 'pasien_id');
    $ruangan_id = CHtml::activeId($model, 'ruangan_id');
    $pasienadmisi_id = CHtml::activeId($model, 'pasienadmisi_id');
    $urlPrintLembarBookingKamar = Yii::app()->createUrl('print/lembarBookingKamar', array('idBookingKamar' => ''));

    $cekKartuPasien = PPKonfigSystemK::model()->find()->printkartulsng;
    if (!empty($cekKartuPasien)) { //Jika Kunjungan Pasien diisi TRUE
        $statusKartuPasien = $cekKartuPasien;
    } else { //JIka Print Kunjungan Diset FALSE
        $statusKartuPasien = 0;
    }

    $js = <<< JS

function print(idBookingKamar)
{
        if (idBookingKamar=='') {
            alert("Kosong");
        }
        if(${statusKartuPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('${urlPrintLembarBookingKamar}'+idBookingKamar,'printwin','left=100,top=100,width=400,height=400');
        }             
}
function getRuanganberdasarkanRM(no_rekam_medik)
{

    $.post("${urlgetRuanganberdasarkanRM}",{no_rekam_medik: no_rekam_medik},
        function(data){
                    if(data.cek!=null)
                        {
                            $('#${pasien_id}').val(data.pasien_id);
                        }
                    $('#${pendaftaranid}').val(data.pendaftaran_id);
                    $('#dataPesan').html();
                    $('#ruangan').val(data.ruangan_nopendaftaran);
                      
                    $('#${ruangan_id}').val(data.ruangan_id); 
                    $('#${pasienadmisi_id}').val(data.pasienadmisi_id);    
                    $('#dataPesan').html(data.data_pesan);
    },"json");
  
}

function getStatus(obj){
    
    idKamarruangan = (obj.value);
   // alert(idKamarruangan);
     $.post("${urlgetStatusKamar}",{idKamarruangan: idKamarruangan},
        function(data){
            $('div.divForForm').html(data.status);
            if(data.kamar=="IN USE"){
                $('#PPBookingKamarT_statusbooking').val("ANTRI");
                $('#PPBookingKamarT_statusbooking_dropdown').val("ANTRI");
            }else{
                $('#PPBookingKamarT_statusbooking').val("NON ANTRI");
                $('#PPBookingKamarT_statusbooking_dropdown').val("NON ANTRI");
            }
    },"json");
}

JS;
    Yii::app()->clientScript->registerScript('formPasienNopend', $js, CClientScript::POS_HEAD);
    $js = <<< JS
$('#isPasienLama').click(function(){
    if ($(this).is(':checked'))
      {
         $('#PPPasienM_no_rekam_medik').removeAttr('disabled');
         $('#buttonSearch').removeAttr('disabled');
      }
    else
      {
        $('#PPPasienM_no_rekam_medik').attr('disabled','true');
        $('#no_rekam_medik').val('');
        $('#buttonSearch').attr('disabled','true');
        $('#PPPasienM_kabupaten_id').html('<option value=\'\'>--Pilih--</option>');
        $('#PPPasienM_kecamatan_id').html('<option value=\'\'>--Pilih--</option>');
        $('#PPPasienM_kelurahan_id').html('<option value=\'\'>--Pilih--</option>');
//        $('#isPasienLama').attr('checked',true);
         
       }

       
      
});
JS;
    Yii::app()->clientScript->registerScript('fungsipasien', $js, CClientScript::POS_READY);
    $js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
    Yii::app()->clientScript->registerScript('numberOnly', $js, CClientScript::POS_READY);
    ?>
    <script>
        function setKonfirmasi(obj) {
            $(obj).attr('disabled', true);
            $(obj).removeAttr('onclick');
            $('#ppbooking-kamar-t-form').find('.currency').each(function () {
                $(this).val(unformatNumber($(this).val()));
            });
            $('#ppbooking-kamar-t-form').submit();
        }
    </script>
