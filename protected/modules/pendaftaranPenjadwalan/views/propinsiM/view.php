<?php// $this->renderPartial('_tab'); ?>
<?php
$this->breadcrumbs=array(
	'Pppropinsi Ms'=>array('index'),
	$model->propinsi_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Propinsi '/*.$model->propinsi_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Propinsi', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Propinsi', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Propinsi', 'icon'=>'pencil','url'=>array('update','id'=>$model->propinsi_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Propinsi','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->propinsi_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Propinsi', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'propinsi_id',
		'propinsi_nama',
		'propinsi_namalainnya',
		//'propinsi_aktif',
                array(
                    'name'=>'propinsi_aktif',
                    'type'=>'raw',
                    'value'=>(($model->propinsi_aktif == 1) ? Yii::t('mds', 'Yes') : Yii::t('mds', 'No'))
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>