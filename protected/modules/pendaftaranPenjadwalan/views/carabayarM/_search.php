<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'ppcarabayar-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'carabayar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'carabayar_nama',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'carabayar_namalainnya',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'metode_pembayaran',array('class'=>'span3','maxlength'=>50)); ?>
            
	<?php echo $form->textFieldRow($model,'carabayar_loket',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->checkBoxRow($model,'carabayar_aktif',array('checked'=>'checked')); ?>

	<?php //echo $form->textFieldRow($model,'carabayar_singkatan',array('class'=>'span3','maxlength'=>1)); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
