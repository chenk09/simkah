<script type="text/javascript">
    var id_diagnosax = new Array();
</script>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$this->breadcrumbs=array(
	'Verifikasi Diagnosa',
);
$this->renderPartial($pathView . '_formDataPasien',array('modPendaftaran'=>$modPendaftaran));

$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'id'=>'uraian-diagnosax-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)'
        ),
        'focus'=>'#',
    )
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial($pathView . '_gridDiagnosaICDX',
    array(
        'form' => $form, 
        'modDiagnosa'=>$modDiagnosa,
        'model'=>$model,
        'modPendaftaran'=>$modPendaftaran, 
        'modUraian'=>$modUraian,
        'pathView'=>$pathView
    )
);
?>
<br>
<?php
$this->renderPartial($pathView . '_gridDiagnosaICDIX',
    array(
        'form' => $form, 
        'modDiagnosaix'=>$modDiagnosaix,
        'model'=>$model_ix,
        'modPendaftaran'=>$modPendaftaran, 
        'modUraian'=>$modUraianIx,
        'pathView'=>$pathView
    )
);
?>

<div class="form-actions">
    <?php
        $menu = (isset($_GET['menu']) ? $_GET['menu'] : "");
        if($menu == 'RJ')
        {
            $action = ((Yii::app()->controller->module->id == 'rekamMedis') ? "InfoPasienRJ" : "InfoKunjunganRJ");
            $url = $this->createUrl('/' . Yii::app()->controller->module->id . '/' . $action . '/Index');
        }else if($menu == 'RD')
        {
            $action = ((Yii::app()->controller->module->id == 'rekamMedis') ? "InfoPasienRD" : "InfoKunjunganRJ");
            $url = $this->createUrl('/' . Yii::app()->controller->module->id . '/' . $action . '/Index');
        }else if($menu == 'RI')
        {
            $action = ((Yii::app()->controller->module->id == 'rekamMedis') ? "InfoPasienRI" : "InfoKunjunganRJ");
            $url = $this->createUrl('/' . Yii::app()->controller->module->id . '/' . $action . '/Index');
        }
        echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
        echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('index',array('id'=>$modPendaftaran->pendaftaran_id)), array('class'=>'btn btn-danger'));
        
        if($menu == 'RJ')
        {
            
            echo CHtml::link(Yii::t('mds', '{icon} Back', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $url=$this->createUrl('InfoKunjunganRJ/Index'), array('class'=>'btn btn-primary'));
           
        }else if($menu == 'RD')
        {
           echo CHtml::link(Yii::t('mds', '{icon} Back', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $url=$this->createUrl('InfoKunjunganRD/Index'), array('class'=>'btn btn-primary'));
           
        } else if($menu == 'RI')
        {
            echo CHtml::link(Yii::t('mds', '{icon} Back', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $url=$this->createUrl('InfoKunjunganRI/Index'), array('class'=>'btn btn-primary'));
        }
       // echo CHtml::link(Yii::t('mds', '{icon} Back', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $url, array('class'=>'btn btn-primary'));
    ?>
</div>

<?php
    $this->endWidget();
?>