<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('PPInfoKunjungan-v', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pasien Rawat Inap</legend>
    <div style="width: 100%;overflow: auto;">
<?php
///$tglSkral = Yii::app()->dateFormatter->formatDateTime(/
//                                        CDateTimeParser::parse($modPPInfoKunjunganRIV->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//echo $tglSkr=date('Y-m-d H:i:s').'fffffffffffff';
//echo  $tanggalSaja=trim(substr($tglSkr,0,-8));
$this->widget('ext.bootstrap.widgets.BootGridView',
    array(
        'id'=>'PPInfoKunjungan-v',
        'dataProvider'=>$modPPInfoKunjunganRIV->searchRI(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=> array(
                array(
                  'header'=>'Tgl Pendaftaran',
                  'type'=>'raw',
                  'value'=>'$data->tgl_pendaftaran',
                ),
                array(
                    'header'=>'No. RM',
                    'name'=>'no_rm',
                    'type'=>'raw',
                    'value'=>'($data->statusperiksa!=Params::statusPeriksa(2) ? CHtml::link("<i class=icon-pencil-brown></i> ". $data->no_rekam_medik , Yii::app()->createUrl("'. Yii::app()->controller->module->id .'/'. Yii::app()->controller->id .'/ubahPasien",array("id"=>"$data->pasien_id", "menu"=>"RI")),array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Edit Data Pasien")) : $data->no_rekam_medik )',
                    'htmlOptions'=>array('style'=>'text-align: center;')
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'name'=>'no_pendaftaran',
                    'type'=>'raw',
                    'value'=>'(!empty($data->no_pendaftaran) ? CHtml::link("<i class=icon-print></i> Gelang Dewasa", "javascript:printLabel(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Gelang Dewasa")) . "<br>" .
          CHtml::link("<i class=icon-print></i> Gelang Anak", "javascript:printLabel2(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Gelang Anak")) . "<br>" . 
          
          CHtml::link("<i class=icon-print></i> Label RM", "javascript:printLabelBaru(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Label Pasien")) . "<br>" .

          CHtml::link("<i class=icon-print></i> Cover", "javascript:printcover(\'$data->pendaftaran_id&status=3\' );",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Cetak Cover")) . "<br>" .

          CHtml::link("<i class=icon-print></i> ".$data->no_pendaftaran, "javascript:print(\'$data->pasienadmisi_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Lembar Poli")) : "-")',
                    'htmlOptions'=>array('style'=>'text-align: left;')
                ),
            array(
                'header'=>'Nama Depan',
                'type'=>'raw',
                'value'=>'$data->namadepan'
            ),
            array(
                'header'=>'Nama',
                'type'=>'raw',
                'value'=>'$data->NamaAlias'
            ),
            'alamat_pasien',
            array(
                'name'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'((!empty($data->jeniskelamin)&&($data->statusperiksa!=Params::statusPeriksa(4))) ? CHtml::link("<i class=icon-pencil-brown></i> ".$data->jeniskelamin," ",array("onclick"=>"ubahJenisKelamin(\'$data->no_rekam_medik\');$(\'#editJenisKelamin\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Jenis Kelamin Pasien")):$data->jeniskelamin)',                
                'htmlOptions'=>array('style'=>'text-align: center')
            ),
            array(
               'header'=>'Status Masuk',
               'type'=>'raw',
               'value'=>'$data->statusmasuk',
            ),
            array(
               'header'=>'Status Masuk',
               'type'=>'raw',
               'value'=>'$data->caramasuk_nama',
            ),
            array(
                'header'=>'Status Konfirmasi',
                'type'=>'raw',
                'value'=>'($data->status_konfirmasi == "" ) ? "-" : $data->status_konfirmasi',
            ),
            array(
                'header'=>'Perujuk',
                'type'=>'raw',
                'value'=>'$data->nama_perujuk',
            ),
            // array(
            //     'header'=>'P3 / Asuransi',
            //     'type'=>'raw',
            //     'value'=>'$data->namapemilik_asuransi',
            // ),
            array(
               'name'=>'CaraBayar/Penjamin',
               'type'=>'raw',
               'value'=>'((!empty($data->CaraBayarPenjamin)&&($data->statusperiksa!=Params::statusPeriksa(5))) ? CHtml::link("<i class=icon-pencil-brown></i> ".$data->CaraBayarPenjamin," ",array("onclick"=>"ubahCaraBayar(\'$data->nama_pasien\');listCaraBayar(\'$data->carabayar_id\');setIdPendaftaran(\'$data->pendaftaran_id\',\'$data->no_pendaftaran\');$(\'#carabayardialog\').dialog(\'open\');return false;",
                                                                                                                                         "rel"=>"tooltip", "title"=>"Klik Untuk Mengubah Cara Bayar & Penjamin pasien")) : $data->CaraBayarPenjamin) ',
                'htmlOptions'=>array(
                    'style'=>'text-align: center',
                    'class'=>'inap'
                )
            ),
            array(
               'name'=>'Nama Dokter',
               'type'=>'raw',
               'value'=>'"<div style=\'width:120px;\'>" . CHtml::link("<i class=icon-pencil-brown></i> ". $data->gelardepan." ".$data->nama_pegawai." ".$data->gelarbelakang_nama," ",array("onclick"=>"ubahDokterPeriksa(\'$data->pendaftaran_id\');$(\'#editDokterPeriksa\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Dokter Periksa")) . "</div>"',
               'htmlOptions'=>array(
                    'style'=>'text-align:center;',
                    'class'=>'inap'
               )
            ),
            array(
               'name'=>'Kelas Pelayanan',
               'type'=>'raw',
               'value'=>'"<div style=\'width:50px;\'>" . CHtml::link("<i class=icon-pencil-brown></i>". $data->kelaspelayanan_nama," ",array("onclick"=>"ubahKelasPelayanan(\'$data->pendaftaran_id\');$(\'#editKelasPelayanan\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Kelas Pelayanan")) . "</div>"',
               'htmlOptions'=>array(
                    'style'=>'text-align:center;',
                    'class'=>'inap'
               )
            ),
            array(
                'header'=>'Ruangan Kamar',
                'type'=>'raw',
                'value'=>'"Kamar :".$data->kamarruangan_nokamar."<br>"."Bed :".$data->kamarruangan_nobed',
            ),
            array(
                'name'=>'ruangan_nama',
                'type'=>'raw',
                'value'=>'(
                    (!empty($data->ruangan_nama)&&($data->statusperiksa!=Params::statusPeriksa(5))) ? 
                        CHtml::link(
                            "<i class=icon-pencil></i> ".$data->ruangan_nama,
                            " ",
                            array(
                                "onCLick"=>"gantiPoli(\'$data->pendaftaran_id\',\'$data->ruangan_id\',\'$data->instalasi_id\',\'$data->pasien_id\',\'$data->nama_pasien\',\'$data->pasienadmisi_id\');return false;",
                                "rel"=>"tooltip",
                                "title"=>"Klik Untuk Mengubah Ruangan Pasien"
                            )
                        ) : 
                        $data->ruangan_nama
                    )',
                'htmlOptions'=>array(
                    'style'=>'text-align: center',
                    'class'=>'inap'
                )
            ),
            array(
               'header'=>'Verifikasi Diagnosa',
               'type'=>'raw',
               'value'=>''
                .'(isset($data->Morbiditas->pasienmorbiditas_id) ? "<div class=\"inap\" style=\"background-color:#33FF00; text-align: center\">" : "<div style=\"background-color:#FF0000; text-align: center\">")'
                .'.(CHtml::Link("<i class=icon-pencil></i>Verifikasi Diagnosa",Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/verifikasiDiagnosa/index",array("id"=>$data->pendaftaran_id,"menu"=>"RI","frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeVerifikasiDiagnosa",
                                  "onclick"=>"$(\"#dialogVerifikasiDiagnosa\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik Verifikasi Diagnosa",
                    )))."</div>"',  
            ),
//			DI HIDE EHJ-3426
            array( 
                'name'=>'statusperiksa',
                'type'=>'raw',
                'value'=>'$data->statusperiksa',
                'htmlOptions'=>array(
                    'style'=>'text-align: center',
                    'class'=>'status'
                )
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            disableLink();
        }',
    )
);
?>
</div>
<div class="search-form" style="">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'formCari',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
<fieldset>
     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table>
        <tr>
            <td>
              <?php echo $form->labelEx($modPPInfoKunjunganRIV,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPPInfoKunjunganRIV,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
              </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' Sampai Dengan', array('class'=>'control-label')) ?>
                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPPInfoKunjunganRIV,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                   <?php echo $form->textFieldRow($modPPInfoKunjunganRIV,'no_rekam_medik',array('class'=>'span3 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->textFieldRow($modPPInfoKunjunganRIV,'nama_pasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->textAreaRow($modPPInfoKunjunganRIV,'alamat_pasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                     <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'status_konfirmasi',Params::statusKonfirmasiAsuransi(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
            </td>
            <td width="52%">
   
   
   
                        <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'propinsi_id', CHtml::listData($modPPInfoKunjunganRIV->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
                                                          'update'=>'#PPInfoKunjunganRIV_kabupaten_id'),
                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                          )); ?>
                        <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'kabupaten_id',array(),
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
                                                          'update'=>'#PPInfoKunjunganRIV_kecamatan_id'),
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        )); ?>
                       <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'kecamatan_id',array(),
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
                                                          'update'=>'#PPInfoKunjunganRIV_kelurahan_id'),
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                          )); ?>    
                       <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'kelurahan_id',array(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            </td>
        </tr>
    <!--</table>

</fieldset>
    
                <fieldset>
                    <legend>Berdasarkan Cara Bayar</legend>
                        <table>
                            <tr> -->
                                <td width="48%">
                                    <?php echo $form->dropDownListRow($modPPInfoKunjunganRIV,'carabayar_id', CHtml::listData($modPPInfoKunjunganRIV->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')), 
                                                    'update'=>'#PPInfoKunjunganRIV_penjamin_id'  //selector to update
                                                ),
                                         )); ?>
                                    <?php echo CHtml::label('Penjamin',' Penjamin', array('class'=>'control-label')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo $form->dropDownList($modPPInfoKunjunganRIV,'penjamin_id', CHtml::listData($modPPInfoKunjunganRIV->getPenjaminItems(), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                                </td>
                                
                            </tr>
                        </table>
                </fieldset>
           
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
     ?>
     <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('infoKunjunganRI/index'), array('class'=>'btn btn-danger')); ?> 
	  <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE))) 
                        //{  
                ?>
                            <script>
                                //print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                 <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                       // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?>   
	 <?php 
	$content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
	 ?>    
</div>
      
</div>    
<?php $this->endWidget();
     $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
     $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
     $urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRI',array('idPasienAdmisi'=>''));
?>
    </fieldset>

<?php

//========= Ganti Poli Dialog =============================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'ganti_poli',
    'options'=>array(
        'title'=>'Ganti Ruangan Pasien - <span id="titleNamaPasien"></span>',
        'autoOpen'=>false,
        'minWidth'=>400,
        'modal'=>true,
    ),
));
?>
<table>
    <tr>
        <td>Ruangan</td>
        <td>:</td>
        <td>
            <?php echo CHtml::dropDownList('ruangan_sebelumnya','', array(),array('disabled'=>true));?>
            <?php echo CHtml::hiddenField('ruangan_awal','',array('readonly'=>true));?>
        </td>
    </tr>
    <tr>
        <td>Alasan Perubahan <span class="required">*</span></td>
        <td>:</td>
        <td><?php echo CHtml::textArea('alasanperubahan','', array());?></td>
    </tr>
    <tr>
        <td>Menjadi Ruangan</td>
        <td>:</td>
        <td><?php echo CHtml::dropDownList('ruangan_id_ganti','ruangan_id_ganti', array(),array('empty'=>'--pilih--',));?></td>
    </tr>
</table>
<?php

echo CHtml::hiddenField('pendaftaran_id');
echo CHtml::hiddenField('pasien_id');
echo CHtml::hiddenField('pasienadmisi_id');
echo CHtml::htmlButton(Yii::t('mds','{icon} Ya',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'simpanRuanganBaru();'));
echo '&nbsp;&nbsp;&nbsp;'.CHtml::htmlButton(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                                                array('class'=>'btn btn-danger', 'type'=>'button','onclick'=>'$(\'#ganti_poli\').dialog(\'close\');'));
												

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========= end Ganti Ruangan Dialog =========================

//Yii::app()->clientScript->registerScript('jsPendaftaran',$js, CClientScript::POS_HEAD);
// ===========================Dialog Batal Periksa=================
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                            'id'=>'confirm',
                                // additional javascript options for the dialog plugin
                                'options'=>array(
                                'title'=>'',
                                'autoOpen'=>false,
                                'show'=>'blind',
                                'hide'=>'explode',
                                'minWidth'=>500,
                                'minHeight'=>100,
                                'resizable'=>false,
                                'modal'=>true,    
                                 ),
                            ));
                            echo '<center>Apakah Anda Yakin Akan Membatalkan Pemeriksaan Pasien Ini?<br><br>' ;  
                            echo CHtml::htmlButton(Yii::t('mds','{icon} Ya',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'ubahPeriksa();'));
                            echo '&nbsp;&nbsp;&nbsp;'.CHtml::htmlButton(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                                                array('class'=>'btn btn-danger', 'type'=>'button','onclick'=>'$(\'#confirm\').dialog(\'close\');'));
                            echo CHtml::hiddenField('pendaftaran_id', '');
                            echo CHtml::hiddenField('statusperiksa', '');
//                            echo '14 April 2012 Belum Berjalan Karena Untuk <br> 
//                                Pengecekannya Harus Kasir Dulu N tabel yang diperlukan ataw 
//                                view yang diperlukan belum ada';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Batal Periksa=====================

   
    
//======================================================JAVA SCRIPT===================================================                          
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRI',array('idPasienAdmisi'=>''));

$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRI',array('idPasienAdmisi'=>''));
$urlPrintLembarPolidua = Yii::app()->createUrl('print/lembarPoliRJBlanko1',array('idPendaftaran'=>''));
$urlPrintLembarCover = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>'' ) );

$urlPrintLembarPoliBaru = Yii::app()->createUrl('print/lembarPoliRD',array('idPendaftaran'=>''));

$urlPrintLembar = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>''));

$urlPrintGelang = Yii::app()->createUrl('print/cetakGelangPasien',array('idPasienAdmisi'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetRuangan=Yii::app()->createUrl('ActionAjax/GetRuanganPasien'); 
$simpanRuanganBaru=Yii::app()->createUrl('ActionAjax/SaveRuanganBaru'); 
$statusPeriksaBatalPeriksa=Params::DEFAULT_BATAL_PERIKSA;

$js = <<< JSCRIPT
//====================================Awal Ubah Cara Bayar============================================================

function printLabel(idPendaftaran)
{
   window.open('${urlPrintLembar}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}

function printLabel2(idPendaftaran)
{
   window.open('${urlPrintLembarPolidua}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}

function printLabelBaru(idPendaftaran)
{
   window.open('${urlPrintLembarPoliBaru}'+idPendaftaran,'printwin','left=100,top=100,width=350,height=350');    
}

function printcover(idPendaftaran)
{
   window.open('${urlPrintLembarCover}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}

function ubahCaraBayar(pendaftaran_id,carabayar_id,penjamin_id)
   {
        $('#ubahCaraBayar_id').val(carabayar_id);
        $('#ubahPenjamin_id').val(penjamin_id);
        $('#ubahPendaftaranId').val(pendaftaran_id);
        $.post("${url}/ajaxGetPenjamin", {carabayar_id: carabayar_id, penjamin_id : penjamin_id},
                function(data){
                   $('#ubahPenjamin_id').html(data.penjamin);
                },"json");
        
        $('#carabayardialog').dialog('open');   
   }

function simpanCaraBayar()
   {
        carabayar_id=$('#ubahCaraBayar_id').val();
        penjamin_id=$('#ubahPenjamin_id').val();
        pendaftaran_id=$('#ubahPendaftaranId').val();
        alert(pendaftaran_id);
        $.post("${url}/ajaxUpdateCaraBayarAntrian", { pendaftaran_id: pendaftaran_id, carabayar_id: carabayar_id, penjamin_id:penjamin_id  },
                function(data){
                     alert(data.message);
                     $('#carabayardialog').dialog('close');   
                     window.location.reload();
                },"json");
        
    } 
    
 function dynamicPenjamin(obj) 
    {
       $.post("${url}/ajaxGetPenjamin", {carabayar_id: obj.value},
                function(data){
                   $('#ubahPenjamin_id').html(data.penjamin);
                },"json");
        
  
   }
   
//=====================================Akhir Ubah Cara Bayar============================================================    

//======================================Awal batal Periksa==============================================================

function dialogConfirm(pendaftaran_id,statusperiksa)
   {
        $('#confirm #pendaftaran_id').val(pendaftaran_id);
        $('#confirm #statusperiksa').val(statusperiksa);
        $('#confirm').dialog('open');
        
   } 
function ubahPeriksa()
    {
      var url =$('#url').val();
      var statusperiksa=$('#confirm #statusperiksa').val();
      var pendaftaran_id=$('#confirm #pendaftaran_id').val(); 
      if(statusperiksa=='${statusPeriksaBatalPeriksa}')
        {
            alert('PasienSudah Dibatalkan');
        }
      else
        {
             $.post("${url}/ubahPeriksa", {pendaftaran_id: pendaftaran_id,statusperiksa:statusperiksa},
                function(data){
                     alert(data.message);
                },"json");
            
        }
   
    }   
//=======================================Akhir Batal Periksa=============================================================   

//=======================================Awal Print Lembar Poli==========================================================

function print(idPasienAdmisi)
{
   window.open('${urlPrintLembarPoli}'+idPasienAdmisi,'printwin','left=100,top=100,width=400,height=400');    
}
function printGelang(idPasienAdmisi)
{
   window.open('${urlPrintGelang}'+idPasienAdmisi,'printwin','left=100,top=100,width=400,height=400');    
}
//========================================Akhir Print Lembar Poli========================================================

//========================================Awal Ganti Ruangan==================================================================

function gantiPoli(pendaftaran_id,ruangan_id,instalasi_id,pasien_id,namaPasien,pasienadmisi_id)
    {
        $('#titleNamaPasien').html(namaPasien);
           $.post("${urlGetRuangan}", { pendaftaran_id: pendaftaran_id, ruangan_id: ruangan_id,instalasi_id:instalasi_id,pasien_id:pasien_id},
           function(data){
            $('#ganti_poli').dialog('open');
            $('#ganti_poli #ruangan_awal').val(ruangan_id);
            $('#ganti_poli #ruangan_sebelumnya').html(data.dropDown);
            $('#ganti_poli #ruangan_id_ganti').html(data.dropDown);
            $('#ganti_poli #pendaftaran_id').val(pendaftaran_id);
            $('#ganti_poli #pasien_id').val(pasien_id);
            $('#ganti_poli #pasienadmisi_id').val(pasienadmisi_id);
        }, "json");
    }
    
 function simpanRuanganBaru()
    {
        if($('#ganti_poli #alasanperubahan').val()==''){
           alert('Alasan Perubahan tidak boleh kosong!');
           $('#ganti_poli #alasanperubahan').addClass('error');
           return false;
        }
        $('#ganti_poli').dialog('close');
        var pendaftaran_id= $('#ganti_poli #pendaftaran_id').val();
        var pasien_id= $('#ganti_poli #pasien_id').val();
        var pasienadmisi_id= $('#ganti_poli #pasienadmisi_id').val();
        var ruangan_id= $('#ganti_poli #ruangan_id_ganti').val();
        var ruangan_awal= $('#ganti_poli #ruangan_awal').val();
        var alasan = $('#ganti_poli #alasanperubahan').val();
        $.post("${simpanRuanganBaru}", { pendaftaran_id: pendaftaran_id, ruangan_id: ruangan_id, ruangan_awal: ruangan_awal, alasan:alasan, pasien_id:pasien_id,pasienadmisi_id:pasienadmisi_id},
            function(data){
                if(data.status=='Gagal')
                    alert(data.status);
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                            data: $('#formCari').serialize()
                });
            }, "json");
    }
//========================================Akhir Ganti Ruangan=========================================================

JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);                        

$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>


<?php
    //=============================== Ganti Data Jenis Kelamin Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editJenisKelamin',
            'options'=>array(
                'title'=>'Ganti Data Jenis Kelamin',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_norekammedik','',array('readonly'=>true));
    echo '<div class="divForFormEditJenisKelamin"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
    //=============================== Ganti Data Kelas Pelayanan Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editKelasPelayanan',
            'options'=>array(
                'title'=>'Ganti Kelas Pelayanan',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranKP','',array('readonly'=>true));
    echo '<div class="divForFormEditKelasPelayanan"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
//========================================= Cara Bayar dialog =============================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'carabayardialog',
    'options'=>array(
        'title'=>'Ganti Cara Bayar dan Penjamin - <span id="titleNamaPasienCaraBayar"></span>',
        'autoOpen'=>false,
        'minWidth'=>650,
        'modal'=>true,
        'resizable'=>false,
        //'hide'=>explode,
    ),
));
echo CHtml::hiddenField('tempCaraBayarId','',array('readonly'=>true));
echo CHtml::hiddenField('tempPendaftaranId','',array('readonly'=>true));
echo CHtml::hiddenField('tempNoPendaftaran','',array('readonly'=>true));
echo '<div class="divForFormUbahCaraBayar"></div>';
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
    //=============================== Ganti Data Dokter Pengganti Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editDokterPeriksa',
            'options'=>array(
                'title'=>'Ganti Dokter Periksa',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranDP','',array('readonly'=>true));
    echo '<div class="divForFormEditDokterPeriksa"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<script type="text/javascript">
    function disableLink()
    {
        var status = null;
        $("#PPInfoKunjungan-v tbody").find('tr > td[class="inap"]').each(
            function()
            {
                status = $(this).parent().find('td[class="status"]');
                var xxx = $(this).find('a');
                if(status.text() == 'SUDAH PULANG')
                {
                   $(this).text($.trim(xxx.text()));
                   $(this).find('a').remove();
                }
            }
        );
    }
    disableLink();
    
    
    function ubahCaraBayar(namaPasien)
    {   
        $('#titleNamaPasienCaraBayar').html(namaPasien);
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahCaraBayarRI')?>',
                     'data':$(this).serialize(),
                     'type':'post',
                     'dataType':'json',
                     'success':function(data) {
                                if (data.status == 'create_form') {
                                    $('#carabayardialog div.divForFormUbahCaraBayar').html(data.div);
                                    $('#carabayardialog div.divForFormUbahCaraBayar form').submit(ubahCaraBayar);
                                } else {
                                    $('#carabayardialog div.divForFormUbahCaraBayar').html(data.div);
                                    $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                                            data: $(this).serialize()
                                    });
                                    setTimeout("$('#carabayardialog').dialog('close') ",500);
                                }
                     } ,
                     'cache':false});
        return false; 
    }
    
    function listCaraBayar(idCaraBayar){
        $('#carabayardialog #tempCaraBayarId').val(idCaraBayar);
        return false;
    }

    function setIdPendaftaran(idPendaftaran,noPendaftaran)
    {
        $('#carabayardialog #tempPendaftaranId').val(idPendaftaran);
        $('#carabayardialog #tempNoPendaftaran').val(noPendaftaran);
    }
    
    function ubahJenisKelamin(norm)
    {
        $('#temp_norekammedik').val(norm);
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahJenisKelamin')?>',
            'data':$(this).serialize(),
            'type':'post',
            'dataType':'json',
            'success':function(data){
                if (data.status == 'create_form') {
                    $('#editJenisKelamin div.divForFormEditJenisKelamin').html(data.div);
                    $('#editJenisKelamin div.divForFormEditJenisKelamin form').submit(ubahJenisKelamin);
                }else{
                    $('#editJenisKelamin div.divForFormEditJenisKelamin').html(data.div);
                    $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                            data: $(this).serialize()
                    });
                    setTimeout("$('#editJenisKelamin').dialog('close') ",500);
                }
            },
            'cache':false
        });
        return false; 
    }
    
    function ubahKelasPelayanan(idPendaftaran)
    {
        $('#temp_idPendaftaranKP').val(idPendaftaran);
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahKelasPelayananRI')?>',
            'data':$(this).serialize(),
            'type':'post',
            'dataType':'json',
            'success':function(data){
                if (data.status == 'create_form') {
                    $('#editKelasPelayanan div.divForFormEditKelasPelayanan').html(data.div);
                    $('#editKelasPelayanan div.divForFormEditKelasPelayanan form').submit(ubahKelasPelayanan);
                }else{
                    $('#editKelasPelayanan div.divForFormEditKelasPelayanan').html(data.div);
                    $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                            data: $(this).serialize()
                    });
                    setTimeout("$('#editKelasPelayanan').dialog('close') ",500);
                }
            },
            'cache':false
        });
        return false; 
    }
    
    function ubahDokterPeriksa(idPendaftaran)
    {
        $('#temp_idPendaftaranDP').val(idPendaftaran);
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahDokterPeriksaRI')?>',
            'data':$(this).serialize(),
            'type':'post',
            'dataType':'json',
            'success':function(data){
                if (data.status == 'create_form') {
                    $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                    $('#editDokterPeriksa div.divForFormEditDokterPeriksa form').submit(ubahDokterPeriksa);
                }else{
                    $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                    $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                            data: $(this).serialize()
                    });
                    setTimeout("$('#editDokterPeriksa').dialog('close') ",500);
                }
            },
            'cache':false
        });
        return false; 
    }
    
</script>