<?php
$this->breadcrumbs=array(
	'Ppjadwal Buka Poli Ms'=>array('index'),
	$model->jadwalbukapoli_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jadwal BukaPoli'/*.$model->jadwalbukapoli_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jadwal BukaPoli ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'jadwalbukapoli_id',
		'ruangan.ruangan_nama',
		'hari',
		'jmabuka',
		'jammulai',
		'jamtutup',
		'maxantiranpoli',
//		'create_time',
//		'update_time',
//		'create_loginpemakai_id',
//		'update_loginpemakai_id',
//		'create_ruangan',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>