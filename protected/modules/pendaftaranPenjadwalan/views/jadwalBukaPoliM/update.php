
<?php
$this->breadcrumbs=array(
	'Ppjadwal Buka Poli Ms'=>array('index'),
	$model->jadwalbukapoli_id=>array('view','id'=>$model->jadwalbukapoli_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jadwal BukaPoli'/*.$model->jadwalbukapoli_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jadwal BukaPoli ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>
