<?php// $this->renderPartial('_tab'); ?>
<?php
$this->breadcrumbs=array(
	'Ppsuku Ms'=>array('index'),
	$model->suku_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Suku '/*.$model->suku_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PPSukuM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PPSukuM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PPSukuM', 'icon'=>'pencil','url'=>array('update','id'=>$model->suku_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PPSukuM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->suku_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Suku', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'suku_id',
		'suku_nama',
		'suku_namalainnya',
		//'suku_aktif',
                array(
                    'name'=>'suku_aktif',
                    'type'=>'raw',
                    'value'=>(($model->suku_aktif == 1) ? Yii::t('mds', 'Yes') : Yii::t('mds', 'No'))
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>