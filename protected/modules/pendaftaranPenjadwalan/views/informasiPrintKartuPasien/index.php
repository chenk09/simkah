<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ppinformasiprintkartupasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<fieldset>
    <legend class="rim2">Informasi Print Kartu Pasien</legend>
</fieldset>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ppinformasiprintkartupasien-grid',
	'dataProvider'=>$model->searchTable(),
//	'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
			'tglprintkartu',
            array(
                'name'=>'no_rekam_medik',
                'type'=>'raw',
                'value'=>'$data->pasien->no_rekam_medik . "<br>" . CHtml::link("<i class=\"icon-print\"></i>", "javascript:cetak(\'$data->kartupasien_id\',\'$data->pasien_id\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint kartu pasien"))',
                'htmlOptions'=>array(
                    'style'=>'width:80px;text-align:center'
                )
            ),
            array(
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->NamaAlias',
            ),
            array(
                'name'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->pasien->jeniskelamin',
            ),
            array(
                'name'=>'Alamat Pasien',
                'type'=>'raw',
                'value'=>'$data->pasien->alamat_pasien." Rt. ".$data->pasien->rt." / ".$data->pasien->rw ',
            ),
            /*
            array(
                'name'=>'Print Kartu Pasien',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\"icon-print\"></i>", "javascript:cetak(\'$data->kartupasien_id\',\'$data->pasien_id\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint kartu pasien"))',
            ),
             * 
             */
            array(
                'header'=>'Status Print',
                'type'=>'raw',
                'htmlOptions'=>array(
                	'name'=>'status_print',
                ),
                'value'=>'$data->statusprintkartu?"Sudah":"Belum"',
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){
        	var xxx = "";
        	$(\'#ppinformasiprintkartupasien-grid\').find(\'tbody > tr\').each(
        		function()
        		{
        			xxx = $(this).find(\'td[name="status_print"]\').text();

        			if(xxx == \'Belum\')
        			{
						$(this).addClass(\'rowFalse\');
        			}
        		}
        	);
    		jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
    	}',
)); ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
		'model'=>$model,
)); ?> 
<!-- search-form -->
<!-- search-form -->
<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id;
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPasien'=>''));
?>
<script type="text/javascript">
    function statusawal()
    {
        var status = "";
        $('#ppinfokartupasien-grid').find('tbody > tr').each(
            function()
            {
                status = $(this).find('td[name="status_print"]').text();
                if(status == 'Belum')
                {
                    $(this).find('td').attr('style', 'background-color:#f69696');
                }
            }
        );
    }
    statusawal();

    function cetak(idKartuPasien,idPendaftaran)
    {
        var url = '<?php echo $url."/update"; ?>';
        var urlPrintKartuPasien = '<?php echo $urlPrintKartuPasien; ?>';
        $.post(url, {kartupasien_id: idKartuPasien ,pendaftaran_id: idPendaftaran},
            function(data){
                $.fn.yiiGridView.update('ppinfokartupasien-grid', {
                        data: $('#ppinfokartupasien-search').serialize()
                });
            },"json");
        window.open(urlPrintKartuPasien+idPendaftaran,'printwi','left=100,top=100,width=310,height=230');
    }
</script>

</div>