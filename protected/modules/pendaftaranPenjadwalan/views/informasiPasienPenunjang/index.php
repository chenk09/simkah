<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('PPInformasiPasienPenunjang-v', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pasien Penunjang</legend>
<?php
///$tglSkral = Yii::app()->dateFormatter->formatDateTime(/
//                                        CDateTimeParser::parse($modPPInfoKunjunganRIV->tglAwal, 'yyyy-MM-dd hh:mm:ss'));
//echo $tglSkr=date('Y-m-d H:i:s').'fffffffffffff';
//echo  $tanggalSaja=trim(substr($tglSkr,0,-8));
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'PPInformasiPasienPenunjang-v',
	'dataProvider'=>$model->searchPasienPenunjang(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
            'pendaftaran.tgl_pendaftaran',
            array(
               'header'=>'No. Pendaftaran',
               'name'=>'pendaftaran.no_pendaftaran',
               'type'=>'raw',
               'value'=>'$data->pendaftaran->no_pendaftaran',
               'htmlOptions'=>array('style'=>'text-align: center; width:120px')
            ),
            array(
                'header'=>'No. RM',
                'type'=>'raw',
                'value'=>'$data->pasien->no_rekam_medik',
            ),
            
            array(
                'header'=>'Nama Pasien',
                'type'=>'raw',
                'value'=>'$data->pasien->nama_pasien',
            ),
            array(
                'header'=>'Ruangan Penunjang',
                'type'=>'raw',
                'value'=>'$data->ruangan->ruangan_nama',
            ),
            array(
                'header'=>'Ruangan Asal',
                'type'=>'raw',
                'value'=>'$data->ruanganasal->ruangan_nama',
            ),
            array(
                'header'=>'No. Masuk Penunjang',
                'type'=>'raw',
                'value'=>'$data->no_masukpenunjang',
            ),
            array(
                'header'=>'Tgl Masuk Penunjang',
                'type'=>'raw',
                'value'=>'$data->tglmasukpenunjang',
            ),
            array(
                'header'=>'Kelas Pelayanan',
                'type'=>'raw',
                'value'=>'$data->kelaspelayanan->kelaspelayanan_nama',
            ),
            array(
                'header'=>'Jenis Kasus Penyakit',
                'type'=>'raw',
                'value'=>'$data->jeniskasuspenyakit->jeniskasuspenyakit_nama',
            ),
            
           ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<div class="search-form" style="">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'formCari',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
<fieldset>
     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table>
        <tr>
            <td>
              <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$model,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
              </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' Sampai Dengan', array('class'=>'control-label')) ?>
                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$model,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                   <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php //echo $form->dropDownListRow($model,'status_konfirmasi',Params::statusKonfirmasiAsuransi(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
            </td>
            <td width="52%">
   
   
   
                        <?php 
//                        echo $form->dropDownListRow($model,'propinsi_id', CHtml::listData($model->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
//                                      array('empty'=>'-- Pilih --',
//                                            'ajax'=>array('type'=>'POST',
//                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
//                                                          'update'=>'#PPInfoKunjunganRIV_kabupaten_id'),
//                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                          )); 
                        ?>
                        <?php 
//                        echo $form->dropDownListRow($model,'kabupaten_id',array(),
//                                      array('empty'=>'-- Pilih --',
//                                            'ajax'=>array('type'=>'POST',
//                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
//                                                          'update'=>'#PPInfoKunjunganRIV_kecamatan_id'),
//                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                        )); 
                        ?>
                       <?php 
//                       echo $form->dropDownListRow($model,'kecamatan_id',array(),
//                                      array('empty'=>'-- Pilih --',
//                                            'ajax'=>array('type'=>'POST',
//                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')),
//                                                          'update'=>'#PPInfoKunjunganRIV_kelurahan_id'),
//                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                          ));
                       ?>    
                       <?php //echo $form->dropDownListRow($model,'kelurahan_id',array(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            </td>
        </tr>
    <!--</table>

</fieldset>
    
                <fieldset>
                    <legend>Berdasarkan Cara Bayar</legend>
                        <table>
                            <tr> -->
                                <td width="48%">
                                    <?php 
//                                    echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
//                                                'ajax' => array('type'=>'POST',
//                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRIV')), 
//                                                    'update'=>'#PPInfoKunjunganRIV_penjamin_id'  //selector to update
//                                                ),
//                                         )); 
                                    ?>                                </td>
                                <td>
                                    <div class="control-group ">
                                        <?php //echo $form->dropDownList($model,'penjamin_id', CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                </fieldset>
           
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
     ?>
     <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiPasienPenunjang/index'), array('class'=>'btn btn-danger')); ?> 
	  <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE))) 
                        //{  
                ?>
                            <script>
                                //print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                 <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                       // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?>   
	 <?php 
	$content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
	 ?>    
</div>
      
</div>    
<?php $this->endWidget();
     $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
     $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
     $urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRI',array('idPendaftaran'=>''));
?>
    </fieldset>

