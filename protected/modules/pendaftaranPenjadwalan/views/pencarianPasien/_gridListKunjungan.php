<table class="table table-condensed" id="dataPasienKunjungan">
    <tr>
        <td>No Rekam Medik</td>
        <td>: <?php echo $modPasien->no_rekam_medik; ?></td>
        <td>Nama Pasien</td>
        <td>: <?php echo $modPasien->nama_pasien; ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>: <?php echo $modPasien->jeniskelamin; ?></td>
        <td>Nama Alias</td>
        <td>: <?php echo $modPasien->nama_bin; ?></td>
    </tr>
</table>
<style>
    #pencarianlistkunjungan-grid table thead tr th{
        vertical-align: middle;
    }
</style>
<?php 
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
        'id'=>'pencarianlistkunjungan-grid',
        'mergeHeaders'=>array(
//            0=>array(
//                'start'=>5,
//                'end'=>6,
//                'name'=>'Penunjang',
//            ), 
            0=>array(
                'start'=>7,
                'end'=>10,
                'name'=>'Tindak Lanjut Ke Rawat Inap',
            )
        ),
        'dataProvider'=>$modPendaftaran->searchListKunjungan(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            'ruangan.ruangan_nama',
            'no_pendaftaran',
            'tgl_pendaftaran',
            'tglselesaiperiksa',
            array(
                'header'=>'Cara bayar',
                'type'=>'raw',
                'value'=>'$data->carabayar->carabayar_nama',
            ),
            array(
                'header'=>'Penjamin',
                'type'=>'raw',
                'value'=>'$data->penjamin->penjamin_nama',
            ),
            array(
                'header'=>'Ke Penunjang',
                'type'=>'raw',
                'value'=>'$this->grid->owner->renderPartial("pendaftaranPenjadwalan.views.pencarianPasien._pasienMasukPenunjang", array("idPendaftaran"=>$data->pendaftaran_id))',
                // 'value'=>'$this->grid->owner->renderPartial('.$this->pathView.'."_pasienMasukPenunjang", array("idPendaftaran"=>$data->pendaftaran_id))',
            ),
            'pasienadmisi.tgladmisi',
            'pasienadmisi.ruangan.ruangan_nama',
            'pasienadmisi.kelaspelayanan.kelaspelayanan_nama',
            array(
                'header'=>'Cara Pulang / Kondisi',
                'type'=>'raw',
                // 'value'=>'$data->pasienpulang->carakeluar."/".$data->pasienpulang->kondisipulang',
                'value'=>'$data->Pulang',

            ),
            'statusperiksa',
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>