          <?php echo $form->errorSummary($model); ?>
          <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
            <table border="1">
              <tr>
                  <td widht="50%">
                      <table>
                          <tr>
                              <td><?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>TRUE)); ?></td>
                          </tr>
                          <tr>
                              <td><?php echo $form->textFieldRow($model,'tgl_rekam_medik',array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')", 'maxlength'=>6,'readonly'=>TRUE)); ?></td>
                          </tr>
                          <tr>
                              <td>
                                  <div class="control-group">
                                      <div class="control-label">
                                          Photo Pasien
                                      </div>
                                      <div class="controls">
                                         <?php echo $form->fileField($model,'photopasien',array('onchange'=>"readURL(this);",'maxlength'=>254,'hint'=>'Isi jika akan mengganti photo','onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                                         <p class="help-block">Isi jika akan mengganti photo</p>
                                      </div>
                                  </div>
                              </td>
                          </tr>
                      </table>
                  </td>
                  <td valign="top" align="center">
                        <?php 
                            if(!empty($model->photopasien)){
                                echo CHtml::image(Params::urlPasienTumbsDirectory().'kecil_'.$model->photopasien, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                            } else {
                                echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                            }
                        ?>                       
                  </td>
              </tr>
            </table>
              <table class="table">
                    <tr>
                        <td>
                            <div class="control-group ">
                                <?php echo $form->labelEx($model,'no_identitas_pasien', array('class'=>'control-label')) ?>
                                <div class="controls">
                                    <?php echo $form->dropDownList($model,'jenisidentitas', JenisIdentitas::items(),  
                                                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                                        )); ?>   
                                    <?php echo $form->textField($model,'no_identitas_pasien', array('placeholder'=>'No Identitas','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            
                                    <?php echo $form->error($model, 'jenisidentitas'); ?><?php echo $form->error($model, 'no_identitas'); ?>
                                </div>
                            </div>
                            <div class="control-group ">
                                <?php echo $form->labelEx($model,'nama_pasien', array('class'=>'control-label')) ?>
                                <div class="controls inline">

                                    <?php echo $form->dropDownList($model,'namadepan', NamaDepan::items(),array(
										'empty'=>'-- Pilih --',
										'onkeypress'=>"return $(this).focusNextInputField(event)",
										'class'=>'span2',
										"disabled"=>false
									)); ?>   
                                    <?php echo $form->textField($model,'nama_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            

                                    <?php echo $form->error($model, 'namadepan'); ?><?php echo $form->error($model, 'nama_pasien'); ?>
                                </div>
                            </div>
                            <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            <?php echo $form->textFieldRow($model,'tempat_lahir',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            <div class="control-group ">
                                <?php echo $form->labelEx($model,'tanggal_lahir', array('class'=>'control-label')) ?>
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tanggal_lahir',
                                                            'mode'=>'date',
                                                            'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                            'maxDate' => 'd',
                                                            'onkeypress'=>"js:function(){getUmur(this);}",
                                                            'onSelect'=>'js:function(){getUmur(this);}',
                                                            'yearRange'=> "-150:+0",
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                                    <?php echo $form->error($model, 'tanggal_lahir'); ?>
                                </div>
                            </div>
                            <div class="control-group ">
                            <?php echo $form->labelEx($model,'umur', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php
                                    // $this->widget('CMaskedTextField', array(
                                    // 'model' => $model,
                                    // 'attribute' => 'umur',
                                    // 'mask' => '99 Thn 99 Bln 99 Hr',
                                    // 'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)', 'onchange'=>'setNamaGelar()', 'class'=>'reqPasien')
                                    // ));
                                    ?>
                                  <?php echo $form->textField($model,'thn', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this)','onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Thn
                                  <?php echo $form->textField($model,'bln', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this)','onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Bln
                                  <?php echo $form->textField($model,'hr', array('maxlength'=>'2','class'=>'numbersOnly','style'=>'width:20px;','onblur'=>'getTglLahir(this)','onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Hr
                                <?php echo $form->error($model, 'umur'); ?>
                        </div>
                    </div>
                            <?php echo $form->radioButtonListInlineRow($model, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)", 'onchange'=>"setNamaGelar()", 'class'=>'reqPasien jk')); ?>
                            <?php echo $form->dropDownListRow($model,'statusperkawinan', StatusPerkawinan::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'setNamaGelar()')); ?>
                            <div class="control-group ">
                                <?php echo $form->labelEx($model,'golongandarah', array('class'=>'control-label')) ?>

                                <div class="controls">

                                    <?php echo $form->dropDownList($model,'golongandarah', GolonganDarah::items(),  
                                                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
                                    <div class="radio inline">
                                        <div class="form-inline">
                                        <?php echo $form->radioButtonList($model,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                                        </div>
                                   </div>
                                    <?php echo $form->error($model, 'golongandarah'); ?>
                                    <?php echo $form->error($model, 'rhesus'); ?>
                                </div>
                            </div>
                            <?php
                                echo $form->textFieldRow($model,'nama_ibu',
                                    array(
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'placeholder'=>'Nama Ibu'
                                    )
                                );
                            ?>
                            <?php
                                echo $form->textFieldRow($model,'nama_ayah',
                                    array(
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'placeholder'=>'Nama Ayah'
                                    )
                                );
                            ?>                            
                        </td>
                        <td>
                            <?php echo $form->textFieldRow($model,'alamat_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            <div class="control-group ">
                                <?php echo $form->labelEx($model,'rt', array('class'=>'control-label inline')) ?>

                                <div class="controls">
                                    <?php echo $form->textField($model,'rt', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>   / 
                                    <?php echo $form->textField($model,'rw', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>            
                                    <?php echo $form->error($model, 'rt'); ?>
                                    <?php echo $form->error($model, 'rw'); ?>
                                </div>
                            </div>
                            <?php echo $form->dropDownListRow($model,'propinsi_id', CHtml::listData(PropinsiM::model()->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                                              array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                    'ajax'=>array('type'=>'POST',
                                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                                  'update'=>'#PPPasienM_kabupaten_id'
                                                                                  ))); ?>
                            <?php echo $form->dropDownListRow($model,'kabupaten_id',CHtml::listData(KabupatenM::model()->getKabupatenItemsProp($model->propinsi_id), 'kabupaten_id', 'kabupaten_nama'),
                                                              array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                    'ajax'=>array('type'=>'POST',
                                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                                  'update'=>'#PPPasienM_kecamatan_id'
                                                                                 ))); ?>
                            <?php echo $form->dropDownListRow($model,'kecamatan_id',CHtml::listData(KecamatanM::model()->getKecamatanItemsKab($model->kabupaten_id), 'kecamatan_id', 'kecamatan_nama'),
                                                              array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                    'ajax'=>array('type'=>'POST',
                                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                                  'update'=>'#PPPasienM_kelurahan_id',
                                                                                  ))); ?>
                            <?php echo $form->dropDownListRow($model,'kelurahan_id',CHtml::listData(KelurahanM::model()->getKelurahanItemsKec($model->kecamatan_id), 'kelurahan_id', 'kelurahan_nama'),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                            <?php echo $form->dropDownListRow($model,'pekerjaan_id', CHtml::listData(PekerjaanM::model()->getPekerjaanItems(), 'pekerjaan_id', 'pekerjaan_nama'), 
                                                              array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)", 'onchange'=>'cekStatusPekerjaan(this)',
                                                                    'ajax'=>array('type'=>'POST',
                                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetPekerjaan',array('encode'=>false,'namaModel'=>'PPPasienM')),
                                                                                  'update'=>'#PPPasienM_pekerjaan_id'
                                                                                  ))); ?>
                             
                            <?php echo $form->textFieldRow($model,'no_telepon_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                            <?php echo $form->textFieldRow($model,'no_mobile_pasien',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        </td>
                    </tr>
                </table>


<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlGetDaerah = Yii::app()->createUrl('ActionAjax/getListDaerahPasien');
// $idTagUmur = CHtml::activeId($model,'umur');
$idTagUmurThn = CHtml::activeId($model,'thn');
$idTagUmurBln = CHtml::activeId($model,'bln');
$idTagUmurHr = CHtml::activeId($model,'hr');
$propinsiId = Yii::app()->user->getState('propinsi_id');
$kabupatenId = Yii::app()->user->getState('kabupaten_id');
$kecamatanId = Yii::app()->user->getState('kecamatan_id');
$agama = Params::DEFAULT_AGAMA;
$wargaNegara = Params::DEFAULT_WARGANEGARA;
$js = <<< JS

function getTglLahir(obj)
{
    //alert(obj.value);
    var str = obj.value;
    // obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{thn: $("#${idTagUmurThn}").val(),bln: $("#${idTagUmurBln}").val(),hr: $("#${idTagUmurHr}").val()},
        function(data){
            $('#PPPasienM_tanggal_lahir').val(data.tglLahir);
           // $("#${idTagUmurThn}").val(data.thn);
           // $("#${idTagUmurBln}").val(data.bln);
           // $("#${idTagUmurHr}").val(data.hr);

    },"json");
}



function getUmur(obj)
{
    // alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){
            // $("#PPPendaftaranRj_thn).val(data.thn);
            // $("#PPPendaftaranRj_bln).val(data.bln);
            // $("#PPPendaftaranRj_hr).val(data.hr);

           $("#${idTagUmurThn}").val(data.thn);
           $("#${idTagUmurBln}").val(data.bln);
           $("#${idTagUmurHr}").val(data.hr);

           // $('#PPPendaftaranRj_umur').val(data.umur); 
           // $('#PPPendaftaranMp_umur').val(data.umur); 
           // $('#PPPendaftaranRd_umur').val(data.umur); 

           // $("#${idTagUmur}").val(data.umur);
    },"json");
}


// getUmur($('#PPPasienM_tanggal_lahir'));

function loadUmur(tglLahir)
{   
    $.post("${urlGetUmur}",{tglLahir: tglLahir},
        function(data){
            $("#${idTagUmur}").val(data.umur);
    },"json");
}

JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_HEAD);
?>
          
<script>
function readURL(input) {
if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
        $('#photo_pasien')
        .attr('src', e.target.result)
        .width(150)
        .height(200);
    };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
<script type="text/javascript">
// here is the magic
function convertToUpper(obj)
{
    var string = obj.value;
    $(obj).val(string.toUpperCase());
}

function setNamaGelar()
{
    var statusperkawinan = $('#PPPasienM_statusperkawinan').val();
    var namadepan = $('#PPPasienM_namadepan');
    var umur = $("#<?php echo CHtml::activeId($model,'thn');?>").val();
    // umur = parseInt(umur);
    
    if(umur <= 5){
        var namadepan = $('#PPPasienM_namadepan').val('By.');
        if(statusperkawinan.length > 0 && statusperkawinan != "DIBAWAH UMUR"){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else if(umur <= 13){ //
        var namadepan = $('#PPPasienM_namadepan').val('An.');
        if(statusperkawinan.length > 0 && statusperkawinan != "DIBAWAH UMUR"){
            $('#PPPasienM_statusperkawinan').val('');
            alert('Maaf status perkawinan belum cukup usia');
        }
    }else{
        if($('#PPPasienM_jeniskelamin_0').is(':checked')){
            if(statusperkawinan !== 'JANDA'){
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Tn.');
            }
            
        }
        
        if($('#PPPasienM_jeniskelamin_1').is(':checked')){
            if(statusperkawinan !== 'DUDA'){
                if(statusperkawinan === 'KAWIN' || statusperkawinan == 'JANDA' || statusperkawinan == 'NIKAH SIRIH' || statusperkawinan == 'POLIGAMI'){
                    var namadepan = $('#PPPasienM_namadepan').val('Ny.');
                }else{
                    var namadepan = $('#PPPasienM_namadepan').val('Nn');
                }                
            }else{
                alert('Pilih status pernikahan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('KAWIN');
                var namadepan = $('#PPPasienM_namadepan').val('Ny.');
            }
        }
        
        if (statusperkawinan == "DIBAWAH UMUR"){
            alert('Pilih status pernikahan yang sesuai');
            $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
        }
    }
    
}

function cekJenisKelamin(obj)
{
    var is_true = true;
    var namadepan = $('#PPPasienM_namadepan').val();
    if(namadepan.length != 0)
    {
        if(obj.value == 'PEREMPUAN')
        {
            if(namadepan != 'Nn.' && namadepan != 'Ny.' && namadepan != 'By.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_0').attr('checked',true);
                is_true = false;
            }
        }else{
            if(namadepan != 'Tn.' && namadepan != 'An.' && namadepan != 'By.')
            {
                alert('Pilih Jenis kelamin yang sesuai');
                $('#PPPasienM_jeniskelamin_1').attr('checked',true);
                is_true = false;
            }
        }
    }else{
        $(obj).attr('checked',false);
        alert('Pilih gelar kehormatan terlebih dahulu');
    }
}

function setValueStatus(obj)
{
    var gelar = obj.value;
    if(gelar === 'Tn.')
    {
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('KAWIN');
        
    }else if(gelar === 'An.'){
        $('#PPPasienM_jeniskelamin_0').attr('checked',true);
        $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
    }else{
        if(gelar === 'Nn' || gelar === 'By.')
        {
            $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
        }else{
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
        $('#PPPasienM_jeniskelamin_1').attr('checked',true);
    }
}

function setStatusPerkawinan(obj)
{
    var status = obj.value;
    var namaDepan = $('#PPPasienM_namadepan').val();
    
    if(status === 'BELUM KAWIN')
    {
        if(namaDepan !== 'An.' && namaDepan !== 'Nn' && namaDepan !== 'By.')
        {
            alert('Pilih status perkawinan yang sesuai');
            $('#PPPasienM_statusperkawinan').val('KAWIN');
        }
    }else{
        if(status === 'KAWIN' || status === 'NIKAH SIRIH' || status === 'POLIGAMI')
        {
            if(namaDepan !== 'Tn.' && namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
            }
        }
        else if(status === 'JANDA')
        {
            if(namaDepan !== 'Ny.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Tn.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
        else if(status === 'DUDA')
        {
            if(namaDepan !== 'Tn.')
            {
                alert('Pilih status perkawinan yang sesuai');
                if(namaDepan === 'Ny.')
                {
                    $('#PPPasienM_statusperkawinan').val('KAWIN');
                }else{
                    $('#PPPasienM_statusperkawinan').val('BELUM KAWIN');
                }
            }
        }
    }
}

function cekStatusPekerjaan(obj)
{
    var namaDepan = $('#PPPasienM_namadepan').val();
    var namaPekerjaan = obj.value;
    var umur = $("#<?php echo CHtml::activeId($model,'thn');?>").val();
    // umur = parseInt(umur);
    
    if(namaDepan.length > 0)
    {
        if(umur < 15){
            if(namaPekerjaan !== '12'){
                if(namaPekerjaan !== ''){
                    alert('Pasien masih di bawah umur, coba cek ulang');
                }
                $(obj).val('');
            }else{
                $(obj).val(namaPekerjaan);
            }
        }else{
            if(namaPekerjaan === '12'){
                if(namaDepan === 'Ny.'){
                    $(obj).val('9');
                }else if(namaDepan === 'Nn' && namaPekerjaan === '9'){
                    alert('Pasien belum menikah, coba cek ulang');
                    $(obj).val('');
                }else{
                    $(obj).val('');
                }
                alert('Pilih pekerjaan yang tepat');
            }else{
                if(namaPekerjaan === '9'){
                    if(namaDepan !== 'Ny.'){
                        alert('Pasien seorang laki - laki, coba cek ulang');
                        $(obj).val('');                        
                    }
                }
            }
        }
/*        
        if(namaPekerjaan === '12' && umur < 17)
        {
            if(namaDepan !== 'BY. Ny.' && namaDepan !== 'An.' && namaDepan !== 'Nn')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }
        }else{
            if(namaDepan === 'BY. Ny.')
            {
                alert('Pilih pekerjaan yang sesuai');
                $(obj).val('');
            }else{
                if(namaPekerjaan === '11' || namaPekerjaan === '10')
                {
                    if(namaDepan !== 'An.' && namaDepan !== 'Nn'){
                        alert('Pilih pekerjaan yang sesuai');
                        $(obj).val('');
                    }
                }else{
                    if(namaPekerjaan !== '13' && namaPekerjaan !== '14')
                    {
                        if(namaPekerjaan === '9' && namaDepan !== 'Ny.')
                        {
                            alert('Pilih pekerjaan yang sesuai');
                            $(obj).val('');
                        }else{
                            if((namaDepan === 'An.' || namaDepan === 'Nn') && umur < 25){
                                alert('Pilih pekerjaan yang sesuai');
                                $(obj).val('');
                            }                        
                        }
                    }
                }
            }
        }
*/
    }else{
        $(obj).val('');
        alert('Pilih gelar kehormatan terlebih dahulu');
    }

}
</script>

