<?php 
if (isset($idPendaftaran)){
    $modPasienMasuk = PasienmasukpenunjangT::model()->findAll('pendaftaran_id =:pendaftaran', array(':pendaftaran'=>$idPendaftaran));
    if (count($modPasienMasuk) > 0){
        echo '<ul>';
        foreach ($modPasienMasuk as $key => $value) {
            $tanggal = date('d M Y H:i:s', strtotime($value->tglmasukpenunjang));
            echo "<li>{$tanggal} - {$value->ruangan->ruangan_nama}</li>";
        }
        echo '</ul>';
    }
}
?>