<legend class="rim2">Informasi Pencarian Pasien</legend>

<?php
//$arrMenu = array();
 //               array_push($arrMenu,array('label'=>Yii::t('mds',''), 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//$this->menu=$arrMenu;
//$this->widget('bootstrap.widgets.BootAlert');
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#no_rekam_medik',
                'method'=>'GET',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");?>
<?php
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchPasien(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Daftarkan</center>',
                'start'=>11, //indeks kolom 3
                'end'=>13, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                    array(
                        'name'=>'tgl_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->tgl_rekam_medik'
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\"icon-print\"></i>", "javascript:print(\'$data->pasien_id\',\'$data->umur\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint kartu pasien"))." ".CHtml::link($data->no_rekam_medik, "javascript:print(\'$data->pasien_id\',\'$data->umur\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint kartu pasien"))',
                    ),
                    array(
                        'name'=>'Nama Pasien',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\"icon-pencil\"></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien"))." ".CHtml::link($data->nama_pasien, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien"))',
                    ),
                  
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'name'=>'alamat_pasien',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                        'name'=>'Rt/Rw',
                        'value'=>'$data->rt." / ".$data->rw',
                    ),
                    /*
                     * Jangan Dihapus, takutnya nanti kepake lagi  . . .You Know lah :p
                    array(
                        'name'=>'Penanggung Jawab',
                        'type'=>'raw',
                        'value'=>'(!empty($data->penanggungjawab_id) ? CHtml::link($data->penanggungJawab->nama_pj, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPenanggungJawab",array("id"=>"$data->penanggungjawab_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data penanggung jawab"))." ".CHtml::link("<i class=\"icon-pencil\"></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPenanggungJawab",array("id"=>"$data->penanggungjawab_id")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data penanggung jawab")) : "-") ',
                    ),
                    */
                    array(
                        'name'=>'propinsi_id',
                        'filter'=>  CHtml::listData($modProp, 'propinsi_id', 'propinsi_nama'),
                        'value'=>  'PropinsiM::model()->findByPk($data->propinsi_id)->propinsi_nama',
                    ),
                    array(
                        'name'=>'kabupaten_id',
                        'filter'=>  CHtml::listData($modKab, 'kabupaten_id', 'kabupaten_nama'),
                        'value'=>  'KabupatenM::model()->findByPk($data->kabupaten_id)->kabupaten_nama',
                    ),
                    array(
                        'name'=>'kecamatan_id',
                        'filter'=>  CHtml::listData($modKec, 'kecamatan_id', 'kecamatan_nama'),
                        'value'=> ' KecamatanM::model()->findByPk($data->kecamatan_id)->kecamatan_nama',
                    ),
                    array(
                        'name'=>'kelurahan_id',
                         'filter'=>  CHtml::listData($modKel, 'kelurahan_id', 'kelurahan_nama'),
                        'value'=>  'KelurahanM::model()->findByPk($data->kelurahan_id)->kelurahan_nama',
                    ),
                    array(
                        'header'=>'<center>Data Penanggung <br/>Jawab</center>',
                        'type'=>'raw',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->createUrl("pendaftaranPenjadwalan/PencarianPasien/DaftarPjP",array("id"=>$data->pasien_id, "frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianTagihan",
                                          "onclick"=>"$(\"#dialogPjP\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Penanggung Jawab",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    array(
                        'header'=>'<center>Riwayat <br/> Kunjungan</center>',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<icon class=\'icon-list-alt\'></icon>","", 
                            array("href"=>"", "onclick"=>"getListKunjungan(".$data->pasien_id.");return false;"))',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'<center>Rawat <br/> Jalan</center>',
                        'type'=>'raw',
                        'value'=>'(empty($data->ruangan_id)  ? CHtml::link("<i class=\'icon-list-alt\'></i> ", 
                            "javascript:daftarKeRJ(\'$data->pasien_id\');",array("id"=>"$data->pasien_id",
                                "title"=>"Klik Untuk Mendaftarkan ke Rawat Jalan")) : "Pasien Sudah Didaftarkan <br/> Ke Rawat Jalan") ',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
                    array(
                        'header'=>'<center>Rawat <br/> Darurat</center>',
                        'type'=>'raw',
                        'value'=>'(empty($data->ruangan_id)  ? CHtml::link("<i class=\'icon-list-alt\'></i> ", 
                            "javascript:daftarKeRD(\'$data->pasien_id\');",array("id"=>"$data->pasien_id",
                                "title"=>"Klik Untuk Mendaftarkan ke Rawat Darurat")) : "Pasien Sudah Didaftarkan <br/> Ke Rawat Darurat") ',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                    ),
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
<hr></hr>
<fieldset>
    <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table class="table">
        <tr>
            <td>
                <div class="control-group ">
                   <div class="control-label inline"
                        <?php echo CHtml::activecheckBox($model, 'ceklis', array('uncheckValue'=>0,'rel'=>'tooltip' ,
                                'data-original-title'=>'Cek untuk pencarian berdasarkan tanggal','onclick'=>'cekAll();')); ?>
                        Tgl Rekam Medik
                    </div>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_rm_awal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                    //
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo $form->labelEx($model,'tgl_rm_akhir', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_rm_akhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'minDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'alamat_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($model,'rt', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'rt', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1 numberOnly','maxlength'=>3)); ?>   / 
                        <?php echo $form->textField($model,'rw', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1 numberOnly','maxlength'=>3)); ?> 
                    </div>
                </div>
                <?php echo $form->dropDownListRow($model,'propinsi_id', CHtml::listData($modProp, 'propinsi_id', 'propinsi_nama'),array('empty'=>'-- Pilih --',
                                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                                    'ajax'=>array(
                                                                                        'type'=>'POST',
                                                                                        'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPPasienM','attr'=>'propinsi_id')),
                                                                                        'update'=>'#PPPasienM_kabupaten_id',))); 
                ?>
                <?php echo $form->dropDownListRow($model,'kabupaten_id', array(),array('empty'=>'-- Pilih --',
                                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                                    'ajax'=>array(
                                                                                        'type'=>'POST',
                                                                                        'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPPasienM','attr'=>'kabupaten_id')),
                                                                                        'update'=>'#PPPasienM_kecamatan_id',))); 
                ?>

                <?php echo $form->dropDownListRow($model,'kecamatan_id', array(),array('empty'=>'-- Pilih --',
                                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                                    'ajax'=>array(
                                                                                        'type'=>'POST',
                                                                                        'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPPasienM','attr'=>'kecamatan_id')),
                                                                                        'update'=>'#PPPasienM_kelurahan_id',))); 
                ?>
                <?php echo $form->dropDownListRow($model,'kelurahan_id', array(),array('empty'=>'-- Pilih --',
                                                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                                    )); 
                ?>
            </td>
        </tr>
    </table>
</fieldset>
      
    <div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pencarianPasien/'), array('class'=>'btn btn-danger')); ?>
                  <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE))) 
                        //{  
                ?>
                            <script>
                              //  print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                 <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                        //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?> 
<?php  
$content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>	
		
</div>

<?php $this->endWidget(); ?>
<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//$url =  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$url = Yii::app()->createUrl('print/kartuPasien',array('idPasien'=>''));
$cetak = Yii::app()->createUrl('pendaftaranPenjadwalan/pencarianPasien/printKartu',array('id'=>''));
$urlPendaftaranRJ=Yii::app()->createAbsoluteUrl('pendaftaranPenjadwalan/Pendaftaran/RawatJalan');
$urlPendaftaranRD=Yii::app()->createAbsoluteUrl('pendaftaranPenjadwalan/Pendaftaran/RawatDarurat');

$js = <<< JSCRIPT

//function daftarRj(noRM)
//   {
//           
//            $('#norek').val(noRM);
//            document.getElementById("daftarRJ").submit();
//           
//   }

// ==* Fungsi Print *== //

function print(id,umur)
   {    
//               window.open('${url}'+id+'/umur/'+umur,'printwin','left=100,top=100,width=310,height=200,scrollbars=0');
               window.open('${url}'+id,'printwin','left=100,top=100,width=310,height=200,scrollbars=0');
   }
   
function getListKunjungan(id){
    if (jQuery.isNumeric(id)){
        $.fn.yiiGridView.update('pencarianlistkunjungan-grid', {
		data: 'PendaftaranT[pasien_id]='+id, 
                success: function (data) {
                        var hasil = $('<div>' + data + '</div>');
                        var updateId = '#pencarianlistkunjungan-grid';
                        var update2 = '#dataPasienKunjungan';
                        $(updateId).replaceWith($(updateId, hasil));
                        $(update2).replaceWith($(update2, hasil));
                        $("#dialogRiwayatKunjungan").dialog("open");
                }
	});
        
        
	return false;
    }
}

function daftarKeRJ(pasien_id)
{
    $('#pasien_id').val(pasien_id);
    $('#form_hidden_rj').submit();
}
function daftarKeRD(pasien_id)
{
    $('#pasien_id').val(pasien_id);
    $('#form_hidden_rd').submit();
}

JSCRIPT;

Yii::app()->clientScript->registerScript('jsPencarianPasien',$js, CClientScript::POS_HEAD);

$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRiwayatKunjungan',
    'options'=>array(
        'title'=>'Riwayat Kunjungan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<?php $this->renderPartial($this->pathView.'_gridListKunjungan', array('modPendaftaran'=>$modPendaftaran, 'modPasien'=>$model)); ?>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPjP',
    'options'=>array(
        'title'=>'Daftar Penanggung Jawab',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hidden_rj',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>$urlPendaftaranRJ,
        'htmlOptions'=>array('target'=>'_new'),
)); ?>
    <?php echo CHtml::hiddenField('pasien_id','',array('readonly'=>true));?>
    <?php //echo CHtml::hiddenField('pendaftaran_id','',array('readonly'=>true));?>
<?php $this->endWidget(); ?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hidden_rd',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>$urlPendaftaranRD,
        'htmlOptions'=>array('target'=>'_new'),
)); ?>
    <?php echo CHtml::hiddenField('pasien_id','',array('readonly'=>true));?>
    <?php //echo CHtml::hiddenField('pendaftaran_id','',array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<script>
    cekAll();
    function cekAll(){
        if ($("#PPPasienM_ceklis").is(":checked")) {
            $("#PPPasienM_tgl_rm_awal").removeAttr('disabled');
            $("#PPPasienM_tgl_rm_akhir").removeAttr('disabled');
        }else{
            $("#PPPasienM_tgl_rm_awal").attr('disabled','true');
            $("#PPPasienM_tgl_rm_akhir").attr('disabled','true');
        }
    }
</script>