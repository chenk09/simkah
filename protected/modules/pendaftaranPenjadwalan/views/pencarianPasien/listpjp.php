<?php
if (isset($caraPrint)){
    if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulLaporan'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');     
    }
    echo $this->renderPartial('application.views.headerReport.headerDefault', array('judulLaporan'=>$data['judulLaporan']));      
}
?>
<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    table{
        font-size:11px;
    }
');
?>

<table class="table table-condensed">
    <tr>
        <td>No Rekam Medik</td>
        <td>: <?php echo $modPasien->no_rekam_medik; ?></td>
        <td>Nama Pasien</td>
        <td>: <?php echo $modPasien->nama_pasien; ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>: <?php echo $modPasien->jeniskelamin; ?></td>
        <td>Nama Alias</td>
        <td>: <?php echo $modPasien->nama_bin; ?></td>
    </tr>
</table>
<div align="center" style="border-bottom: 1px solid #000000;padding: 10px;margin-bottom: 15px;">
    <?php echo strtoupper($data['judulLaporan']);?>
</div>
<table width="100%" style='margin-left:auto; margin-right:auto;' class='table table-striped table-bordered table-condensed'>
    <thead>
        <tr>
            <th rowspan='2'><center>TANGGAL PENDAFTARAN</center></th>
            <th rowspan='2'><center>NO PENDAFTARAN</center></th>
            <th colspan='3'><center>PENANGGUNG JAWAB</center></th>
        </tr>
        <tr>
            <th><center>NAMA</center></th>
            <th><center>ALAMAT</center></th>
            <th><center>HUBUNGAN</center></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($modPendaftaran as $i => $datas) 
        {
            echo "<tr>";
            echo "<td>";
            echo $datas['tgl_pendaftaran'];
            echo "</td>";               
            echo "<td>";
            echo $datas['no_pendaftaran'];
            echo "</td>";            
            echo "<td>";
            echo $datas->penanggungJawab->nama_pj;
            // echo $datas['penanggungjawab_id'];
            echo "</td>";
            echo "<td>";
            echo $datas->penanggungJawab->alamat_pj;
            echo "</td>";
            echo "<td>";
            echo $datas->penanggungJawab->hubungankeluarga;
            echo "</td>";            
            echo "</tr>";

        }
        ?>
    </tbody>
</table>
