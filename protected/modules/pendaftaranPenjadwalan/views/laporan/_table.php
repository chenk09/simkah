<?php
  $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
if (isset($caraPrint)){
  $data = $model->searchPrint();
  $template = '{items}';
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchTableLaporan();
  $template = "{summary}{pager}\n{items}";
}
?>
<?php if(isset($caraPrint)){ ?>

<?php }else{ ?>
<div style='max-width:1000px;overflow-y: scroll;'>
<?php } ?>
<?php $this->widget($table,array(
	'id'=>'PPInfoKunjungan-v',
	'dataProvider'=>$data,
        'template'=>$template,
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kelompok Diagnosa</center>',
                'start'=>25,
                'end'=>27,
            ),
            array(
                'name'=>'<center>Kode Diagnosa</center>',
                'start'=>28,
                'end'=>30,
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
             array(
              'header'=>'No',
              'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
              'type'=>'raw',
            ),
            array(
              'header'=>'Tanggal Entry',
              'type'=>'raw',
              'value'=>'$data->create_time',
            ),
            array(
              'header'=>'Tanggal Pelayanan',
              'type'=>'raw',
              'value'=>'$data->tgl_pendaftaran',
            ),
            array(
              'header'=>'No Pendaftaran',
              'type'=>'raw',
              'value'=>'$data->no_pendaftaran',
            ),
            array(
              'header'=>'No Rekam Medik',
              'type'=>'raw',
              'value'=>'$data->no_rekam_medik',
            ),
//            'no_rekam_medik',    
            array(
              'header'=>'Nama Pasien / Alias',
              'type'=>'raw',
              'value'=>'$data->NamaNamaBIN',
            ),
//            'NamaNamaBIN',
            array(
              'header'=>'Jenis Kelamin',
              'type'=>'raw',
              'value'=>'$data->jeniskelamin',
            ),
            array(
              'header'=>'Golongan Umur',
              'type'=>'raw',
              'value'=>'$data->golonganumur_nama',
            ),
            array(
              'header'=>'Agama',
              'type'=>'raw',
              'value'=>'$data->agama',
            ),
            array(
              'header'=>'Status Perkawinan',
              'type'=>'raw',
              'value'=>'$data->statusperkawinan',
            ),
            array(
              'header'=>'Pekerjaan',
              'type'=>'raw',
              'value'=>'$data->pekerjaan_nama',
            ),
            array(
              'header'=>'Alamat Lengkap',
              'type'=>'raw',
              'value'=>'$data->alamat_pasien',
            ),
//            'alamat_pasien',
            array(
              'header'=>'Kecamatan',
              'type'=>'raw',
              'value'=>'$data->kecamatan_nama',
            ),
            array(
              'header'=>'Kab. / Kota. ',
              'type'=>'raw',
              'value'=>'$data->kabupaten_nama',
            ),
            
            array(
              'header'=>'Cara Masuk',
              'type'=>'raw',
              'value'=>'$data->caramasuk_nama',
            ),
            array(
              'header'=>'Kunjungan',
              'type'=>'raw',
              'value'=>'$data->kunjungan',
            ),
            
            array(
              'header'=>'Penanggung Jawab / Pihak Ke-3',
              'type'=>'raw',
              'value'=>'$data->nama_pj',
            ),
            array(
              'header'=>'Nama Perujuk',
              'type'=>'raw',
              'value'=>'$data->nama_perujuk',
            ),
           
            'Jenis_kasus_nama_penyakit',
            array(
               'name'=>'CaraBayar/Penjamin',
               'type'=>'raw',
               'value'=>'$data->CaraBayarPenjamin',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),            
            array(
               'header'=>'Nama Ruangan',
               'name'=>'ruangan_nama',
               'type'=>'raw',
               'value'=>'$data->ruangan_nama',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),
            array(
               'name'=>'Nama Dokter ',
               'type'=>'raw',
               'value'=>'$data->nama_pegawai',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),
            array(
               'name'=>'Ket. Pulang',
               'type'=>'raw',
               'value'=>'$data->nama_pegawai',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),
            array(
               'header'=>'Status Periksa',
               'name'=>'statusperiksa',
               'type'=>'raw',
               'value'=>'$data->statusperiksa',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),
            array(
               'header'=>'Cara Keluar',
               'name'=>'carakeluar',
               'type'=>'raw',
               'value'=>'$data->carakeluar',
               'htmlOptions'=>array('style'=>'text-align: center')
            ),
            //nama diagnosa
            array(
               'header'=>'Diagnosa Masuk',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_MASUK)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Utama',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_UTAMA)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Tambahan',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN)',
               'value'=>'($data->kelompokdiagnosa_id == PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN ) ? $data->diagnosa_nama : "-"',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            //kode diagnosa
            array(
               'header'=>'Diagnosa Masuk',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_MASUK)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Utama',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_UTAMA)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Tambahan',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN)',
               'value'=>'($data->kelompokdiagnosa_id == PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN ) ? $data->diagnosa_nama : "-"',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php if(isset($caraPrint)){ ?>

<?php }else{ ?>
</div>
<?php } ?>