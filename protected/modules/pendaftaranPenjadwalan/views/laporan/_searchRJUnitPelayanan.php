<div class="search-form">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchInfoKunjungan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
        <table class="table table-bordered">
            <tr>
                <td class="span3">
                   <div class="control-group ">
						<?php echo CHtml::label('Periode ','Periode ', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPPInfoKunjunganV,'bulan',
                                array(
                                    'hari'=>'Hari Ini',
                                    'bulan'=>'Bulan',
                                    'tahun'=>'Tahun',
                                ),
                                array(
                                    'id'=>'PeriodeName',
                                    'onChange'=>'setPeriode()',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                )
							);?>
                        </div>
                    </div>
                </td>
                <td class="span9">
					<div class="control-group">
						<?php echo CHtml::label('Tanggal Kunjungan ','Tanggal Kunjungan ', array('class'=>'control-label')) ?>
						<div class="controls form-inline">
							<?php echo CHtml::hiddenField('type', ''); ?>
							<?php $this->widget('MyDateTimePicker', array(
								'model' => $modPPInfoKunjunganV,
								'attribute' => 'tglAwal',
								'mode' => 'datetime',
								'options' => array(
									'dateFormat' => Params::DATE_FORMAT_MEDIUM,
								),
								'htmlOptions' => array(
									'readonly' => true,
									'class' => 'dtPicker3',
									'onclick'=>'checkPilihan(event)',
									'onkeypress' => "return $(this).focusNextInputField(event)"
								),
							)); ?>
							<?php $this->widget('MyDateTimePicker', array(
								'model' => $modPPInfoKunjunganV,
								'attribute' => 'tglAkhir',
								'mode' => 'datetime',
								'options' => array(
									'dateFormat' => Params::DATE_FORMAT_MEDIUM,
								),
								'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
								),
							)); ?>
						</div>
					</div>
					<?php echo CHtml::hiddenField('filter', 'wilayah');?>
					<?php echo CHtml::hiddenField('filter', 'carabayar',array('disabled'=>'disabled'));?>
                </td>
            </tr>
        </table>
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));?>
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>


<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
	$.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
		$('#PPLaporankunjunganrjV_tglAwal').val(data.periodeawal);
		$('#PPLaporankunjunganrjV_tglAkhir').val(data.periodeakhir);
		$('#PPRuanganM_tglAwal').val(data.periodeawal);
		$('#PPRuanganM_tglAkhir').val(data.periodeakhir);
	},'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>
