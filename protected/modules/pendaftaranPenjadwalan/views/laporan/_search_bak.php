<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchInfoKunjungan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
        <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
        <table>
            <tr>
                <td>
                   <div class="control-group ">
                    <?php echo CHtml::label('Tanggal Kunjungan ','Tanggal Kunjungan ', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPPInfoKunjunganV,'bulan',
                                array(
                                    'hari'=>'Hari Ini',
                                    'bulan'=>'Bulan',
                                    'tahun'=>'Tahun',
                                ),
                                array(
                                    'id'=>'PeriodeName',
                                    'onChange'=>'setPeriode()',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                )
                                );
                            ?>
                        </div>
                    </div>
                </td>
                <td width="250px">
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $modPPInfoKunjunganV,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onclick'=>'checkPilihan(event)', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </td>
                <td width="50px">s/d</td>
                <td>
                    <?php
                        $this->widget('MyDateTimePicker', array(
                        'model' => $modPPInfoKunjunganV,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </td>
            </tr>
        </table>

<table width="600" border="0">
  <tr>
    <td>  <div id='searching'>
                    <fieldset>
	<?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                            'id'=>'big',
//                                    'disabled'=>true,
                            'content'=>array(
                                'content1'=>array(
                                    'header'=>'Berdasarkan Wilayah',
                                    'isi'=>'<table><tr><td>'.CHtml::hiddenField('filter', 'wilayah').'<label>Propinsi</label></td><td>'.$form->dropDownList($modPPInfoKunjunganV, 'propinsi_id', CHtml::listData($modPPInfoKunjunganV->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), array('empty' => '-- Pilih --',
                                                    'ajax' => array('type' => 'POST',
                                                        'url' => Yii::app()->createUrl('ActionDynamic/GetKabupaten', array('encode' => false, 'namaModel' => ''.$modPPInfoKunjunganV->getNamaModel().'')),
                                                        'update' => '#'.CHtml::activeId($modPPInfoKunjunganV, 'kabupaten_id').''),
                                                    'onkeypress' => "return $(this).focusNextInputField(event)"
                                                )).'</td></tr><tr><td><label>Kabupaten</label></td><td>'.
                                                $form->dropDownList($modPPInfoKunjunganV, 'kabupaten_id', array(), array('empty' => '-- Pilih --',
                                                    'ajax' => array('type' => 'POST',
                                                    'url' => Yii::app()->createUrl('ActionDynamic/GetKecamatan', array('encode' => false, 'namaModel' => ''.$modPPInfoKunjunganV->getNamaModel().'')),
                                                    'update' => '#'.CHtml::activeId($modPPInfoKunjunganV, 'kecamatan_id').''),
                                                    'onkeypress' => "return $(this).focusNextInputField(event)"
                                                )).'</td></tr></table>',
//                                                        .$form->dropDownList($modPPInfoKunjunganV, 'kecamatan_id', array(), array('empty' => '-- Pilih --',
//                                                            'ajax' => array('type' => 'POST',
//                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetKelurahan', array('encode' => false, 'namaModel' => ''.$modPPInfoKunjunganV->getNamaModel().'')),
//                                                                'update' => '#'.CHtml::activeId($modPPInfoKunjunganV, 'kelurahan_id').''),
//                                                            'onkeypress' => "return $(this).focusNextInputField(event)"
//                                                        )).'</td></tr><tr><td><label>Kabupaten</label></td><td>'.
//                                                        $form->dropDownList($modPPInfoKunjunganV, 'kelurahan_id', array(), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)")).'</td></tr></table>',
                                    'active'=>true,
                                    ),   ),
//                                    'htmlOptions'=>array('class'=>'aw',)
                    )); ?>      </fieldset>
      </td>
    <td> <fieldset>
                        <?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                                    'id'=>'kunjungan',
                                    'slide'=>true,
                                        'content'=>array(
                                        'content2'=>array(
                                            'header'=>'Berdasarkan Cara Bayar',
                                            'isi'=>'<table><tr>
                                                        <td>'.CHtml::hiddenField('filter', 'carabayar',array('disabled'=>'disabled')).'<label>Cara Bayar</label></td>
                                                        <td>'.$form->dropDownList($modPPInfoKunjunganV, 'carabayar_id', CHtml::listData($modPPInfoKunjunganV->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien', array('encode' => false, 'namaModel' => ''.$modPPInfoKunjunganV->getNamaModel().'')),
                                                                'update' => '#'.CHtml::activeId($modPPInfoKunjunganV, 'penjamin_id').'',  //selector to update
                                                            ),
                                                        )).'</td>
                                                            </tr><tr>
                                                        <td><label>Penjamin</label></td><td>'.
                                                        $form->dropDownList($modPPInfoKunjunganV, 'penjamin_id', CHtml::listData($modPPInfoKunjunganV->getPenjaminItems(), 'penjamin_id', 'penjamin_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",)).'</td></tr></table>', 'active'=>false,       
                                           
                                        'active'=>true,
                                            ),
                                    ),
//                                    'htmlOptions'=>array('class'=>'aw',)
                            )); ?>
							</fieldset>
							  </fieldset>
      </td>
  </tr>
</table>

	 
			
                   

    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>



<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
    
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#PPLaporankunjunganrdV_tglAwal').val(data.periodeawal);
            $('#PPLaporankunjunganrdV_tglAkhir').val(data.periodeakhir);
            $('#PPRuanganM_tglAwal').val(data.periodeawal);
            $('#PPRuanganM_tglAkhir').val(data.periodeakhir);
//            if(data.namaPeriode == 1 ){
//                alert("Pencarian Berdasarkan : "+data.namaPeriode);
//            }
        },'json');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>
