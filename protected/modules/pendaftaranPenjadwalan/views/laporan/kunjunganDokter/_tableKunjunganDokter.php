<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
//    'filter'=>$model,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
       'mergeColumns' => array('instalasi_nama', 'ruangan_nama', 'dokter_nama','jeniskasuspenyakit_nama'),
    'columns'=>array(
        array(
          'header'=>'No',
          'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
          'type'=>'raw',
        ),
       'instalasi_nama',
       'ruangan_nama',
        'dokter_nama',
        array(
          'header'=>'No Rekam Medis'."/"."<br/>".'No Pendaftaran',
          'value'=>'$data->no_rekam_medik."<br/>".$data->no_pendaftaran',
          'type'=>'raw',
        ),
        array(
          'header'=>'Nama Pasien'." /"."<br/>".'Alias',
          'value'=>'$data->nama_pasien."<br/>".$data->nama_bin',
          'type'=>'raw',
        ),
        array(
          'header'=>'Tgl Pendaftaran',
          'value'=>'$data->tgl_pendaftaran',
          'type'=>'raw',
        ),
        array(
          'header'=>'Jenis Kelamin'."/"."<br/>".'Umur',
          'value'=>'$data->jeniskelamin."<br/>".$data->umur',
          'type'=>'raw',
        ),
        array(
          'header'=>'Kelas Pelayanan'."/"."<br/>".'Kelas penyakit',
          'value'=>'$data->kelaspelayanan_nama."<br/>".$data->jeniskasuspenyakit_nama',
          'type'=>'raw',
        ),
        array(
            'header'=>'Jenis Kasus <br/> Penyakit <br/>Ruangan',
            'name'=>'jeniskasuspenyakit_nama',
            'type'=>'raw',
            'value'=>'$this->grid->owner->renderPartial("'.$this->pathViewPP.'/kunjunganDokter/_jenisKasusPenyakit",array(ruangan_id=>$data->ruangan_id),true)',
            'htmlOptions'=>array('style'=>'text-align:center'),
        ),
        array(
          'header'=>'Alamat'."/"."<br/>".'RT'." / ".'RW',
          'value'=>'$data->alamat_pasien."<br/>".$data->rt." / ".$data->rw',
          'type'=>'raw',
        ),
        array(
          'header'=>'Cara Bayar'."/"."<br/>".'Penjamin',
          'value'=>'$data->carabayar_nama."<br/>".$data->penjamin_nama',
          'type'=>'raw',
        ),
        /*
        
        'tempat_lahir',
        'tanggal_lahir',
        
        'rt',
        'rw',
        'agama',
        'golongandarah',
        'photopasien',
        
        'statusrekammedis',
        'statusperkawinan',

        'tgl_rekam_medik',
        ////'pendaftaran_id',
        array(
                        'name'=>'pendaftaran_id',
                        'value'=>'$data->pendaftaran_id',
                        'filter'=>false,
                ),
        'no_pendaftaran',
        
        'no_urutantri',
        'transportasi',
        'keadaanmasuk',
        'statusperiksa',
        'statuspasien',
        'kunjungan',
        'alihstatus',
        'byphone',
        'kunjunganrumah',
        'statusmasuk',
        
        'no_asuransi',
        'namapemilik_asuransi',
        'nopokokperusahaan',
        'create_time',
        'create_loginpemakai_id',
        'create_ruangan',
        'shift_id',
        'ruangan_id',

        'instalasi_id',
        
        'jeniskasuspenyakit_id',
        
        'kelaspelayanan_id',
        
        'rujukan_id',
        'pasienpulang_id',
        'profilrs_id',
        */
        
        
        /*
        'ruangan_nama',
        'instalasi_id',
         * 'diagnosa_id',
        'instalasi_nama',*/
    ),
//        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 
