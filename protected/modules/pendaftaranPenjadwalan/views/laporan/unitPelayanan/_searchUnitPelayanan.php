<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchInfoKunjungan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        #ruangan label{
            width: 120px;
            display:inline-block;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
    </style>
     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
<table>
            <tr>
                <td width="50%">
                    <?php
                        echo CHtml::hiddenField('type', '');
                        echo CHtml::hiddenField('PPRuanganM_page',0, array('class'=>'number_page'));
                    ?>
                    <div class='control-group'>
                        <div class = 'control-label'>Tanggal Kunjungan</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                //                                          'maxDate'=>'d',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="control-group ">
                         <div class = 'control-label'>Sampai Dengan</div>
                         <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
                            //                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div>
                    </div>
                    </div>
                </td>
            </tr>
        </table>

         <table width="200" border="0">
  <tr>
    <td><div id='searching'>
                    <fieldset>
                        <?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                                    'id'=>'big',
                                    'slide'=>false,
                                    'content'=>array(
                                        'content2'=>array(
                                        'header'=>'Berdasarkan Instalasi dan Ruangan',
                                        'isi'=>'<table>
                                                    <tr>
                                                        <td>'.CHtml::hiddenField('filter', 'carabayar', array('disabled'=>'disabled')).'<label>Instalasi</label></td>
                                                        <td>'.$form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true and instalasi_id in(2,3,4)'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganForCheckBox', array('encode' => false, 'namaModel' => ''.get_class($model).'')),
                                                                'update' => '#ruangan',  //selector to update
                                                            ),
                                                        )).'
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Ruangan</label>
                                                        </td>
                                                        <td>
                                                            <div id="ruangan">
                                                                <label>Data Tidak Ditemukan</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                 </table>',
                                         'active'=>true
                                        ),
                                    ),
//                                    'htmlOptions'=>array('class'=>'aw',)
                            )); ?>
                 </fieldset> </div>
      </td>
    <td> <div id='searching'>
                    <fieldset>
                        <?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                                    'id'=>'dokter',
                                    'slide'=>false,
//                                    'parent'=>false,
//                                    'disabled'=>true,
//                                    'accordion'=>false, //default
                                    'content'=>array(
                                        'content3'=>array(
                                            'header'=>'Berdasarkan Dokter Pemeriksa',
                                            'isi'=>'<table >
                                                        <tr>
                                                        <td >'.CHtml::hiddenField('namadokter')
                                                        .'<div class="input-append"><span class="add-on">'.$form->textField($model, 'dokter_nama', array('id'=>'dokternama','data-offset-top'=>200,'data-spy'=>'affix','style'=>'margin-top:-3px; margin-left:-3px','inline'=>false, 
                                                        'onkeypress' => "return $(this).focusNextInputField(event)",'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/getDokter'),'placeholder'=>'Ketikan Nama Dokter')).'<a href="javascript:void(0);" id="tombolDokterDialog" onclick="$(&quot;#dialogDokter&quot;).dialog(&quot;open&quot;);return false;">
                                                    <i class="icon-list-alt"></i>
                                                    <i class="icon-search">
                                                    </i>
                                                    </a>
                                                    </span>
                                                    </div></td></tr></table>',
                                            'active'=>true,
                                            ),
                                    ),
                                    'htmlOptions'=>array('class'=>'aw',)
                            ));
                        
                        
                        echo CHtml::hiddenField('idSupplier'); ?>
                            <?php 
//                            $this->widget('MyJuiAutoComplete',array(
//                                        'model'=>$model, 
////                                        'name'=>'namapegawai',
//                                        'attribute'=>'dokter_nama',
//                                        'id'=>'dokternama',
//                                        'value'=>$namapegawai,
//                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/getDokter'),
//                                        'options'=>array(
//                                           'showAnim'=>'fold',
//                                           'minLength' => 2,
//                                           'focus'=> 'js:function( event, ui ) {
//                                                $("#idSupplier").val( ui.item.pegawai_id);
//                                                $("#dokternama").val( ui.item.nama_pegawai );
//                                                $("#PPLaporankunjunganbydokterV_dokter_nama").val( ui.item.nama_pegawai );
//                                                return false;
//                                            }',
//                                           'select'=>'js:function( event, ui ) {
//                                                $("#idSupplier").val( ui.item.pegawai_id);
//                                                $("#namadokter").val( ui.item.pegawai_id);
//                                                return false;
//                                            }',
//
//                                        ),
//                                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','placeholder'=>'Ketikan Nama Dokter'),
//                                        'tombolDialog'=>array('idDialog'=>'dialogDokter','idTombol'=>'tombolDokterDialog'),
//                                
//                            )); ?>
                    </fieldset>
                        </div></td>
  </tr>
</table>
 
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#big").find("input").attr("checked", "checked");
  $("#kelasPelayanan").find("input").attr("checked", "checked");
',  CClientScript::POS_READY);
?>

<?php //Yii::app()->clientScript->registerScript('onclickButton','
//  var tampilGrafik = "<div class=\"tampilGrafik\" style=\"display:inline-block\"> <i class=\"icon-arrow-right icon-white\"></i> Grafik</div>";
//  $(".accordion-heading a.accordion-toggle").click(function(){
//            $(this).parents(".accordion").find("div.tampilGrafik").remove();
//            $(this).parents(".accordion-group").has(".accordion-body.in").length ? "" : $(this).append(tampilGrafik);
//            
//            
//  });
//',  CClientScript::POS_READY);
?>

<?php
/**
 * Dialog untuk nama Supplier
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Daftar Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDokter = new PPDokterpegawaiV;
if(isset($_GET['PPDokterpegawaiV'])){
	$modDokter->attributes = $_GET['PPDokterpegawaiV'];
}

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modDokter->search(),
	'filter'=>$modDokter,
	'template'=>"{pager}{summary}\n{items}",
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
			'header'=>'Pilih',
			'type'=>'raw',
			'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
							"id" => "selectPegawai",
							"href"=>"",
							"onClick" => "
										  $(\"#idDokter\").val(\"$data->pegawai_id\");
										  $(\"#dokternama\").val(\"$data->nama_pegawai\");
										  $(\"#dialogDokter\").dialog(\"close\");    
										  return false;
								"))',
		),
		array(
			'header'=>'ID',
			'filter'=>false,
			'value'=>'$data->pegawai_id',
		),
		array(
			'header'=>'Nama Dokter',
			'name'=>'nama_pegawai',
			'value'=>'$data->nama_pegawai',
		),
		array(
		   'header'=>'Gelar Depan/'."<br/>".'Gelar Belakang',
		   'type'=>'raw',
		   'name'=>'gelardepan',
			'filter'=>false,
		   'value'=>'$data->gelardepan." / "."<br/>".$data->gelarbelakang_nama',
		),
		array(
			'header'=>'Jenis Kelamin',
			'name'=>'jeniskelamin',
			'value'=>'$data->jeniskelamin',
		),
		array(
			'header'=>'Alamat',
			'name'=>'alamat_pegawai',
			'value'=>'$data->alamat_pegawai',
		),
		array(
			'header'=>'Tempat,'."<br/>".'Tanggal Lahir',
			'type'=>'raw',
			'filter'=>false,
			'name'=>'tempatlahir_pegawai',
			'value'=>'$data->tempatlahir_pegawai.","."<br/>".$data->tgl_lahirpegawai',
		),
	),
'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<script>
    $(document).ready(function(){
    jQuery('#dokternama').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
$("#idSupplier").val( ui.item.pegawai_id);
$("#dokternama").val( ui.item.nama_pegawai );
$("#PPLaporankunjunganbydokterV_dokter_nama").val( ui.item.nama_pegawai );
return false;
},'select':function( event, ui ) {
$("#idSupplier").val( ui.item.pegawai_id);
$("#namadokter").val( ui.item.pegawai_id);
return false;
},'source':'/simrs/index.php?r=ActionAutoComplete/getDokter'}); 
    });
	
function checkAll() {
	if ($("#checkAllRuangan").is(":checked")) {
		$('#ruangan input[name*="ruangan_id"]').each(function(){
		   $(this).attr('checked',true);
		})
	} else {
	   $('#ruangan input[name*="ruangan_id"]').each(function(){
		   $(this).removeAttr('checked');
		})
	}
}
</script>