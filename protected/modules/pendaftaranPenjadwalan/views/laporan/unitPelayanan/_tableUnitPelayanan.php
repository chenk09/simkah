<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
//        $data = $model->searchUnitPelayananPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
//        $data = $model->searchUnitPelayanan();
         $template = "{pager}{summary}\n{items}";
    }
	$data = $model->searchLaporanPerUnit();
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
//    'filter'=>$model,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//       'mergeColumns' => array('instalasi_nama', 'ruangan_nama', 'dokter_nama'),
   'columns'=>array(
             array(
          'header'=>'<center>No</center>',
            'value' =>'
                $this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1 . CHtml::hiddenField("paging", $this->grid->dataProvider->pagination->currentPage)
            ',
          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
          'type'=>'raw',
        ),
        array(
          'header'=>'<center>Nama Ruangan</center>',
          'name'=>'ruangan_nama',
          'value'=>'$data->ruangan_nama',
          'htmlOptions'=>array('style'=>'text-align:left;'),
          'type'=>'raw',
        ),
	    array(
            'header'=>'Kunjungan Baru',
            'value'=>'MyFunction::formatNumber($data->jumlahkunjunganbaru)',
        ),
	   array(
            'header'=>'Kunjungan Lama',
            'value'=>'MyFunction::formatNumber($data->jumlahkunjunganlama)',
        ),
        array(
          'header'=>'<center>Jumlah</center>',
          'value'=>'MyFunction::formatNumber($data->jumlahkunjungan)',
          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
          'type'=>'raw',
        ),
       
    ),
    'afterAjaxUpdate'=>'function(id, data){
        
        var paging = $("#tableLaporan table").find("input[name=\'paging\']").val();
        if(typeof paging == \'undefined\')
        {
            paging = 0;
        }
        paging = parseInt(paging) + 1;
        $(".number_page").val(paging);

        $("#tableLaporan").parent().find("li").each(
            function()
            {
                if($(this).attr("class") == "active")
                {
                    setType(this);
                }
            }
        );
        
        jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
    }',
)); ?> 
