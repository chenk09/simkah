<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchTable();
         $template = "{pager}{summary}\n{items}";
    }
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
//    'filter'=>$model,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
       'mergeColumns' => array('instalasi_nama', 'ruangan_nama', 'dokter_nama'),
    'columns'=>array(
        array(
          'header'=>'No',
          'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
          'type'=>'raw',
        ),
    
        'dokter_nama',
          array(
          'header'=>'Status',
          'value'=>'$data->statusdokter',
          'type'=>'raw',
        ),
       
        array(
          'header'=>'No Rekam Medis',
          'value'=>'$data->no_rekam_medik',
          'type'=>'raw',
        ),
        array(
          'header'=>'Nama Pasien',
          'value'=>'$data->nama_pasien',
          'type'=>'raw',
        ),
        array(
          'header'=>'Tgl Tindakan',
          'value'=>'$data->tgl_tindakan',
          'type'=>'raw',
        ),
       
          array(
          'header'=>'Instalasi',
          'value'=>'$data->instalasi_nama',
          'type'=>'raw',
        ),
        array(
          'header'=>'Ruangan',
          'value'=>'$data->ruangan_nama',
          'type'=>'raw',
        ),
         array(
          'header'=>'Jenis Pelayanan',
          'value'=>'$data->daftartindakan_nama',
          'type'=>'raw',
        ),
        array(
          'header'=>'Tarif',
          'value'=>'MyFunction::formatNumber($data->tarif_tindakan)',
          'type'=>'raw',
        ),
//          array(
//            'header'=>'<center>Harga</center>',
//            'name'=>'tarif_satuan',
//            'type'=>'raw',
//            'value'=>'MyFunction::formatNumber($data->tarif_satuan)',
//            'htmlOptions'=>array('style'=>'text-align:right'),
//            'footerHtmlOptions'=>array('style'=>'text-align:right;'),
//            'footer'=>'sum(tarif_satuan)',
//        ),
      
        array(
          'header'=>'Penjamin',
          'value'=>'$data->penjamin_nama',
          'type'=>'raw',
        ),
        /*
        
        'tempat_lahir',
        'tanggal_lahir',
        
        'rt',
        'rw',
        'agama',
        'golongandarah',
        'photopasien',
        
        'statusrekammedis',
        'statusperkawinan',

        'tgl_rekam_medik',
        ////'pendaftaran_id',
        array(
                        'name'=>'pendaftaran_id',
                        'value'=>'$data->pendaftaran_id',
                        'filter'=>false,
                ),
        'no_pendaftaran',
        
        'no_urutantri',
        'transportasi',
        'keadaanmasuk',
        'statusperiksa',
        'statuspasien',
        'kunjungan',
        'alihstatus',
        'byphone',
        'kunjunganrumah',
        'statusmasuk',
        
        'no_asuransi',
        'namapemilik_asuransi',
        'nopokokperusahaan',
        'create_time',
        'create_loginpemakai_id',
        'create_ruangan',
        'shift_id',
        'ruangan_id',

        'instalasi_id',
        
        'jeniskasuspenyakit_id',
        
        'kelaspelayanan_id',
        
        'rujukan_id',
        'pasienpulang_id',
        'profilrs_id',
        */
        
        
        /*
        'ruangan_nama',
        'instalasi_id',
         * 'diagnosa_id',
        'instalasi_nama',*/
    ),
//        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 
