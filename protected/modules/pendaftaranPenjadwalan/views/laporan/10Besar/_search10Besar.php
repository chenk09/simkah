
<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchInfoKunjungan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
	 
    <fieldset>
	 <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
       <!-- <legend> Kunjungan</legend> -->
        <table>
            <tr>
                <td width="50%">
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <div class='control-group'>
                        <div class = 'control-label'>Tanggal Pemeriksaan</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $modPPInfoKunjunganV,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                //                                          'maxDate'=>'d',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                </td>
                <td>
                    <div class="control-group ">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $modPPInfoKunjunganV,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
                            //                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset>
    <fieldset>
        <!--<legend> Ruangan</legend> -->
        <table width="15%">
            <tr>
                <td width="30%">
                    <?php
                    echo $form->dropDownListRow($modPPInfoKunjunganV, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                    ));
                    ?>

                </td>
                <td style="padding-left:90px;">
                    <div class="control-group ">
                        <?php echo $form->dropDownList($modPPInfoKunjunganV, 'ruangan_id', array(), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>
                    </div>
                </td>
            </tr>
    </table>
    </fieldset>
    <fieldset>
       <!-- <legend>Jumlah Tampil</legend> -->
        <table>
            <tr>
                <td width="30%">
                    <?php
                    echo $form->textFieldRow($modPPInfoKunjunganV, 'jumlahTampil', array('onkeypress' => "return $(this).focusNextInputField(event)", 'class'=>'span1 numbersOnly')); 
                    ?>

                </td>
            </tr>
        </table>
    </fieldset>
    
    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
		<?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));
?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ', array('idPendaftaran' => ''));
?>

<?php 
$urlAjax = Yii::app()->createUrl('ActionDynamic/GetRuanganDariInstalasi', array('encode' => false, 'namaModel' => $modPPInfoKunjunganV->getNamaModel()));
        Yii::app()->clientScript->registerScript('numbers','
    $(".numbersOnly").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) || 
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
        if ($(this).val() == 0){
            $(this).val(1);
        }
    });
    $(".numbersOnly").keyup(function(event){
        if ($(this).val() == 0){
            $(this).val(1);
        }
    });
    $("#'.CHtml::activeId($modPPInfoKunjunganV, 'instalasi_id').'").change(function(){
        $.ajax({
            type:"POST",
            data:$("#searchInfoKunjungan").serialize(),
            url:"'.$urlAjax.'",
            success:function(data){
                $("#'.CHtml::activeId($modPPInfoKunjunganV, 'ruangan_id').'").html("<option value>-- Pilih --</pilih>"+data)
            }
        });
    })
',  CClientScript::POS_READY
);?>


