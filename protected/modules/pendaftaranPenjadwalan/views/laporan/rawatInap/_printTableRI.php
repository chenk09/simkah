<?php
   $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
if (isset($caraPrint)){
  $data = $model->searchPrintTableLaporan();
  $template = '{items}';
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';

  }
} else{
  $data = $model->searchTableLaporan();
  $template = "{summary}{pager}\n{items}";
}
?>
<div style='max-width:1214px;'>
<?php $this->widget($table,array(
	'id'=>'PPInfoKunjungan-v',
	'dataProvider'=>$data,
        'template'=>$template,
//        'mergeColumns'=>array('diagnosa_nama'),
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Kelompok Diagnosa</center>',
                'start'=>31,
                'end'=>33,
            ),
            array(
                'name'=>'<center>Kode Diagnosa</center>',
                'start'=>34,
                'end'=>36,
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
              'header'=>'Tanggal Pelayanan',
              'type'=>'raw',
              'value'=>'$data->tgl_pendaftaran',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'No. Pendaftaran',
              'type'=>'raw',
              'value'=>'$data->no_pendaftaran',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
              'header'=>'Tanggal Masuk ',
              'type'=>'raw',
              'value'=>'$data->tgladmisi',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
              'header'=>'Tanggal Keluar',
              'type'=>'raw',
              'value'=>'$data->tglpulang',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
              'header'=>'No. Rekam Medik',
              'type'=>'raw',
              'value'=>'$data->no_rekam_medik',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),    
            array(
              'header'=>'Nama Pasien / Alias',
              'type'=>'raw',
              'value'=>'(isset($data->nama_pasien)?$data->nama_pasien:"")',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Jenis Kelamin',
              'type'=>'raw',
              'value'=>'$data->jeniskelamin',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Golongan Umur',
              'type'=>'raw',
              'value'=>'$data->golonganumur_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Agama',
              'type'=>'raw',
              'value'=>'$data->agama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Status Perkawinan',
              'type'=>'raw',
              'value'=>'$data->statusperkawinan',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Pekerjaan',
              'type'=>'raw',
              'value'=>'$data->pekerjaan_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Alamat Lengkap',
              'type'=>'raw',
              'value'=>'$data->alamat_pasien',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Kecamatan',
              'type'=>'raw',
              'value'=>'$data->kecamatan_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Kab. / Kota. ',
              'type'=>'raw',
              'value'=>'$data->kabupaten_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
              'header'=>'Cara Masuk',
              'type'=>'raw',
              'value'=>'$data->caramasuk_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Kunjungan',
              'type'=>'raw',
              'value'=>'$data->kunjungan',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            
            array(
              'header'=>'Penanggung Jawab / Pihak Ke-3',
              'type'=>'raw',
              'value'=>'$data->nama_pj',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
              'header'=>'Nama Perujuk',
              'type'=>'raw',
              'value'=>'$data->nama_perujuk',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
           array(
             'header'=>'Jenis Kasus Penyakit',
              'type'=>'raw',
              'value'=>'$data->jeniskasuspenyakit_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
           ),
           array(
             'header'=>'Kelas Pelayanan',
              'type'=>'raw',
              'value'=>'$data->kelaspelayanan_nama',
              'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
           ),
            array(
               'name'=>'CaraBayar/Penjamin',
               'type'=>'raw',
               'value'=>'$data->CaraBayarPenjamin',
               'htmlOptions'=>array('style'=>'text-align: center'),
               'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Ruangan Asal',
               'name'=>'ruanganasal_nama',
               'type'=>'raw',
               'value'=>'$data->ruanganasal_nama',
               'htmlOptions'=>array('style'=>'text-align: center'),
               'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Nama Ruangan',
               'name'=>'ruangan_nama',
               'type'=>'raw',
               'value'=>'$data->ruangan_nama',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'name'=>'Dokter Rawat Inap',
               'type'=>'raw',
               'value'=>'$data->nama_pegawai',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'name'=>'statusperiksa',
               'type'=>'raw',
               'value'=>'$data->statusperiksa',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'name'=>'statusmasuk',
               'type'=>'raw',
               'value'=>'$data->statusmasuk',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'name'=>'kondisipulang',
               'type'=>'raw',
               'value'=>'$data->kondisipulang',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Jumlah Lama Rawat',
               'name'=>'jumlahlamarawat',
               'type'=>'raw',
               'value'=>'$data->jumlahlamarawat',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Tanggal Pindahan',
               'name'=>'tglpindahkamar',
               'type'=>'raw',
               'value'=>'$data->tglpindahkamar',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Kamar Pindahan',
               'name'=>'nokamar_akhir',
               'type'=>'raw',
               'value'=>'$data->nokamar_akhir',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Hari Rawat Ruangan Pindahan',
               'name'=>'jumlahlamarawatruangpindahan',
               'type'=>'raw',
               'value'=>'$data->jumlahlamarawatruangpindahan',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            //nama diagnosa
            array(
               'header'=>'Diagnosa Masuk',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_MASUK)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Utama',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_UTAMA)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Tambahan',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getDiagnosaPasien($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN)',
               'value'=>'($data->kelompokdiagnosa_id == PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN ) ? $data->diagnosa_nama : "-"',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            //kode diagnosa
            array(
               'header'=>'Diagnosa Masuk',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_MASUK)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Utama',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_UTAMA)',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
            array(
               'header'=>'Diagnosa Tambahan',
               'name'=>'diagnosa_nama',
               'type'=>'raw',
               'value'=>'$data->getKodeDiagnosa($data->pendaftaran_id,PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN)',
               'value'=>'($data->kelompokdiagnosa_id == PARAMS::KELOMPOKDIAGNOSA_TAMBAHAN ) ? $data->diagnosa_nama : "-"',
               'htmlOptions'=>array('style'=>'text-align: center'),
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
</div>
<br/>