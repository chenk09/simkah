<?php
  $table = 'ext.bootstrap.widgets.MergeHeaderGroupGridView';
if (isset($caraPrint)){
  $data = $model->searchUnitPelayananPrint();
  $template = '{items}';
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchUnitPelayananInap();
  $template = "{summary}{pager}\n{items}";
}
?>
<?php if(isset($caraPrint)){ ?>

<?php }else{ ?>
<div style='max-width:1000px;overflow-x: scroll;'>
<?php } ?>
<?php 
    $this->widget($table,array(
	'id'=>'PPInfoKunjungan-v',
	'dataProvider'=>$data,
        'template'=>$template,
//        'mergeColumns'=>array('ruangan_nama'),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
          'header'=>'<center>No</center>',
          'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
          'type'=>'raw',
        ),
        array(
          'header'=>'<center>Nama Ruangan</center>',
          'name'=>'ruangan_nama',
          'value'=>'$data->ruangan_nama',
          'htmlOptions'=>array('style'=>'text-align:left;'),
          'type'=>'raw',
        ),
		"jumlahkunjunganlama",
		"jumlahkunjunganbaru",
		/*
         array(
            'header'=>'Kunjungan Baru',
            'value'=>'$this->grid->owner->renderPartial("'.$this->pathViewPP.'unitPelayanan/_kunjunganbaru",array(statuspasien=>"PENGUNJUNG BARU",ruangan_id=>$data->ruangan_id),true)',
        ),
        array(
            'header'=>'Kunjungan Lama',
            'value'=>'$this->grid->owner->renderPartial("'.$this->pathViewPP.'unitPelayanan/_kunjunganlama",array(statuspasien=>"PENGUNJUNG LAMA",ruangan_id=>$data->ruangan_id),true)',
        ),
		*/
//        array(
//          'header'=>'<center>Kunjungan Baru</center>',
//          'value'=>'($data->statuspasien == "PENGUNJUNG BARU") ? $data->jumlahkunjunganbaru : "0"',
//          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
//          'type'=>'raw',
//        ),
//        array(
//          'header'=>'<center>Kunjungan Lama</center>',
//          'value'=>$model->getKunjunganlama(),
//          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
//          'type'=>'raw',
//        ),
        array(
          'header'=>'<center>Jumlah</center>',
          'value'=>'MyFunction::formatNumber($data->jumlahkunjungan)',
          'htmlOptions'=>array('style'=>'text-align:center;width:30px;'),
          'type'=>'raw',
        ),
       
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php if(isset($caraPrint)){ ?>

<?php }else{ ?>
</div>
<?php } ?>
<br/>