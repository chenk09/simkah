<?php
$this->breadcrumbs=array(
    'Ppinfo Kunjungan Rjvs'=>array('index'),
    'Manage',
);

$url = Yii::app()->createUrl('pendaftaranPenjadwalan/laporan/frameGrafikBatalPeriksa&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('PPInfoKunjungan-v', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?><legend class="rim2">Laporan Batal Periksa</legend>
<div class="search-form">
<?php $this->renderPartial('_search',array(
    'modPPInfoKunjunganV'=>$model,
)); ?>
</div><!-- search-form --> 
<fieldset> 
    <?php $this->renderPartial('batalPeriksa/_tableBatalPeriksa', array('model'=>$model)); ?>
    <?php $this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>
<?php 
        
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printBatalPeriksa');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));

?>