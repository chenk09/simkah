
<?php

$this->widget('bootstrap.widgets.BootAlert');
?>
<legend class="rim2">Penjadwalan Buka Poliklinik</legend><br/>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'carijadwal-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#RDJadwaldokterM_jadwaldokter_hari',
                'method'=>'GET',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#carijadwal-form').submit(function(){
	$.fn.yiiGridView.update('pencarianjadwal-grid', {
		data: $(this).serialize()
	});
	return false;
});
");?>
<?php
$timePickerUpdate = <<< timePicker
jQuery('.jam').timepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'onSelect':function(){},'timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
timePicker;
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianjadwal-grid',
	'dataProvider'=>$model->searchInformasi(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                   'ruangan.ruangan_nama',
                   array(
                     'header'=>'Senin',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Senin\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Selasa',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Selasa\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Rabu',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Rabu\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Kamis',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Kamis\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Jumat',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Jumat\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Sabtu',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Sabtu\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                   array(
                     'header'=>'Minggu',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_formHari\',array(\'ruangan_id\'=>$data[ruangan_id],\'hariCari\'=>\'Minggu\'),true)',
                     'htmlOptions'=>array('style'=>"width:200px;"),
                   ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                           '.$timePickerUpdate.' 
                            }',
    ));
     

?>
<hr></hr>
<fieldset>
     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table class="table">
        <tr>
            <td>
                <?php //echo $form->dropDownListRow($model,'hari', Params::namaHari() ,array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'empty'=>'- Pilih -')); ?>
                <?php echo $form->dropDownListRow($model,'ruangan_id', CHtml::listData(RDPendaftaran::model()->getRuanganItems(Params::INSTALASI_ID_RJ), 'ruangan_id', 'ruangan_nama') ,
                                                      array('empty'=>'-- Pilih --',
                                                            'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td>
                <div class="control-group ">
                <?php echo $form->labelEx($model,'jammulai', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'jammulai',
                                            'mode'=>'time',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,
                                                                 'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                 ),
                    )); ?> <?php echo $form->error($model, 'jammulai'); ?>
                   
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model,'jamtutup', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'jamtutup',
                                            'mode'=>'time',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,
                                                                 'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                 ),
                    )); ?><?php echo $form->error($model, 'jamtutup'); ?>
                    
                </div>
            </div>
            </td>
        </tr>
    </table>
</fieldset>
      
   <div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiJadwalPoli/index'), array('class'=>'btn btn-danger')); ?>
                 <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                       // {  
                ?>
                            <script>
                               // print(<?php// echo $model->pendaftaran_id ?>);
                            </script>
                <?php// echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                        //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       //} 
                ?> 
<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
		
</div>

<?php $this->endWidget(); ?>

