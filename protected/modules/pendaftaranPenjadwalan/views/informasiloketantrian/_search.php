<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'ppinformasiloketantrian-search',
        'type'=>'horizontal',
)); ?>
<fieldset>
 <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table class="table-condensed">
        <tbody>
            <tr>
                <td>
                    <?php //echo  $form->textFieldRow($model,'tgl_pendaftaran'); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'tglantrian', array('class' => 'control-label')) ?>
                        <div class="controls">
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class='control-label'>Sampai Dengan</label>
                        <div class="controls">
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAkhir',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                            ));
                            ?>
                        </div>
                    </div>
                    <?php echo $form->textFieldRow($model,'loket_nama',array('class'=>'span3','maxlength'=>50)); ?>
                </td>
                <td>
                    	<?php echo $form->textFieldRow($model,'noantrian',array('class'=>'span3','maxlength'=>6)); ?>
                        <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','maxlength'=>20)); ?>
                </td>
            </tr>
    </table>
</fieldset>

	<div class="form-actions">
		                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	 <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiloketantrian/index'), array('class'=>'btn btn-danger')); ?>
	 <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE))) 
                        //{  
                ?>
                            <script>
                               // print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                 <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                       // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?> 
				<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>	
	
	</div>

<?php $this->endWidget(); ?>
