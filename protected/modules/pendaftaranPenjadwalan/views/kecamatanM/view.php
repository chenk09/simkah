<?php// $this->renderPartial('_tab'); ?>
<?php
$this->breadcrumbs=array(
	'Ppkecamatan Ms'=>array('index'),
	$model->kecamatan_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Kecamatan '/*.$model->kecamatan_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Kecamatan', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Kecamatan', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Kecamatan', 'icon'=>'pencil','url'=>array('update','id'=>$model->kecamatan_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PPKecamatanM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->kecamatan_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Kecamatan', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'kecamatan_id',
		'kabupaten.kabupaten_nama',
		'kecamatan_nama',
		'kecamatan_namalainnya',
		//'kecamatan_aktif',
                array(
                    'name'=>'kecamatan_aktif',
                    'type'=>'raw',
                    'value'=>(($model->kecamatan_aktif == 1) ? Yii::t('mds', 'Yes') : Yii::t('mds', 'No')) 
                )
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>