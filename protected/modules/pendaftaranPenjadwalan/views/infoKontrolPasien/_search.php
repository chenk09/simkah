<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'ppinfokontrolpasien-search',
    'type' => 'horizontal',
        ));
?>

<style>
    #ruangan label{
        width: 200px;
            display:inline-block;
        }
</style>
<fieldset>
    <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table class="table-condensed">
        <tbody>
            <tr>
                <td>
                    <?php //echo  $form->textFieldRow($model,'tgl_pendaftaran'); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'tgl_pendaftaran', array('class' => 'control-label')) ?>
                        <div class="controls">
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAwal',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                            ));
                            ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <label class='control-label'>Sampai dengan</label>
                        <div class="controls">
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglAkhir',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3'),
                            ));
                            ?>
                        </div>
                    </div>
                    <?php echo $form->textFieldRow($model, 'no_rekam_medik', array('class' => 'span3', 'maxlength' => 10)); ?>

                    <?php echo $form->textFieldRow($model, 'nama_pasien', array('class' => 'span3', 'maxlength' => 50)); ?>
                </td>
                <td>
                    <?php echo $form->textFieldRow($model,'alamat_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'instalasi_id', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'class' => 'span3', 'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganDariInstalasi', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
                                                                'update' => '#PPPendaftaranT_ruangan_id',  //selector to update
                                                            ),)); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'ruangan_id', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model, 'ruangan_id',array('empty'=>'-- Pilih --'));  ?>
                        </div>
                    </div>
                </td>
            </tr>
    </table>
</fieldset>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiPrintKartuPasien/index'), array('class'=>'btn btn-danger')); ?>
<?php 
$content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>	
		
</div>

<?php $this->endWidget(); ?>
