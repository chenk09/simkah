<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#ppinfokontrolpasien-search').submit(function(){
	$.fn.yiiGridView.update('ppinfokontrolpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style type="text/css">
input[readonly]{
    background-color: #F5F5F5;
    border-color: #DDDDDD;
    cursor: auto;
}
</style>
<fieldset>
    <legend class="rim2">Informasi Rencana Kontrol</legend>
</fieldset>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ppinfokontrolpasien-grid',
	'dataProvider'=>$model->searchKontrolPasien(),
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
			'tglrenkontrol',
            array(
                'name'=>'No Pendaftaran <br> No Rekam Kedik',
                'type'=>'raw',
                'value'=>'$data->no_pendaftaran. "<br>" .$data->pasien->no_rekam_medik',
            ),
            array(
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->pasien->nama_pasien',
            ),
            array(
                'name'=>'Alamat Pasien <br> RT / RW',
                'type'=>'raw',
                'value'=>'$data->pasien->alamat_pasien."<br>RT. ".$data->pasien->rt." / ".$data->pasien->rw ',
            ),
            array(
                'name'=>'No Telp <br> No HP',
                'type'=>'raw',
                'value'=>'$data->pasien->no_telepon_pasien."<br>". $data->pasien->no_mobile_pasien',
            ),
            array(
                'name'=>'Alamat Email',
                'type'=>'raw',
                'value'=>'$data->pasien->alamatemail',
            ),
            array(
                'name'=>'Instalasi',
                'type'=>'raw',
                'value'=>'$data->instalasi->instalasi_nama. "<br>" .$data->ruangan->ruangan_nama',
            ),
            'tgl_pendaftaran',
	),
        'afterAjaxUpdate'=>'function(id, data){
    		jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
    	}',
)); ?>

<div class="search-form">

<?php
$this->renderPartial('_search',
    array(
        'model'=>$model,
    )
);
?> 
<!-- search-form -->
</div>