<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('PPPegawai-m', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
    <legend class="rim2">Informasi Pegawai</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'PPPegawai-m',
	'dataProvider'=>$modPPPegawaiM->search(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'nomorindukpegawai', 
             array(
               'name'=>'nama_pegawai',
               'type'=>'raw',
               'value'=>'(!empty($data->pegawai_id) ? CHtml::link("<i class=icon-eye-open></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/viewPegawai",array("id"=>"$data->pegawai_id")), array("rel"=>"tooltip","title"=>"Klik Untuk Melihat Data Pegawi Lebih Lanjut"))." ".CHtml::link($data->nama_pegawai, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/viewPegawai",array("id"=>"$data->pegawai_id")), array("rel"=>"tooltip","title"=>"Klik Untuk Melihat Data Pegawi Lebih Lanjut")) : "-") ',  
               'htmlOptions'=>array('style'=>'text-align: left')
            ),
            'tempatlahir_pegawai',
            'tgl_lahirpegawai',
            'alamat_pegawai',
            'jeniskelamin'
           
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<div class="search-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
 <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
<table>
    <tr>
        <td>
           <?php echo $form->textFieldRow($modPPPegawaiM,'nama_pegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
           <?php echo $form->dropDownListRow($modPPPegawaiM,'pendidikan_nama',KategoriPegawai::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?> 
            <?php echo $form->dropDownListRow($modPPPegawaiM,'pangkat_id',  CHtml::listData($modPPPegawaiM->getPangkatItems(), 'pangkat_id', 'pangkat_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?> 
        </td>
        <td>
           <?php echo $form->textFieldRow($modPPPegawaiM,'nomorindukpegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
           <?php echo $form->dropDownListRow($modPPPegawaiM,'kelompokpegawai_id',  CHtml::listData($modPPPegawaiM->getKelompokPegawaiItems(), 'kelompokpegawai_id', 'kelompokpegawai_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?> 
            <?php echo $form->dropDownListRow($modPPPegawaiM,'jabatan_id',  CHtml::listData($modPPPegawaiM->getJabatanItems(), 'jabatan_id', 'jabatan_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?> 
        </td>
    </tr>
</table>
    <div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiPegawai/index'), array('class'=>'btn btn-danger')); ?>
                 <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        //{  
                ?>
                            <script>
                                //print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                       // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?> 
<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>	
		
</div>
</div>    <?php $this->endWidget(); ?>

</fieldset>