
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      


 $table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
    
    
$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>$sort,
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'kelaspelayanan_id',
		array(
                        'header'=>'NO',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                ),
    array(
              'name'=>'jeniskelas_id',
              //'type'=>'raw',
              // 'filter'=>  CHtml::listData($model->getJeniskelasItems(), 'jeniskelas_id', 'jeniskelas_nama'),
              'value'=>'$data->jeniskelas->jeniskelas_nama',
    ),
		'kelaspelayanan_nama',
		'kelaspelayanan_namalainnya',
             array(
                     'header'=>'Ruangan ',
                     'type'=>'raw',
                     'value'=>'$this->grid->getOwner()->renderPartial(\'_ruangan\',array(\'kelaspelayanan_id\'=>$data[kelaspelayanan_id]),true)',
                 ), 
		array(
                  'header'=>'Aktif',
                  'value'=>'($data->kelaspelayanan_aktif == 1) ? Yii::t("mds","Yes") : Yii::t("mds","No")',
                ),
 
        ),
    )); 
?>