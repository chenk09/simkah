<legend class="rim2">Informasi Tarif Pelayanan</legend> 
<?php
$this->widget('ext.bootstrap.widgets.BootGridView',
    array(
	'id'=>'daftarTindakan-grid',
	'dataProvider'=>$modTarifTindakanRuanganV->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Instalasi / Ruangan ',
                'type'=>'raw',
                'value'=>'$data->InstalasiRuangan',
            ),
            'kelompoktindakan_nama',
            'kategoritindakan_nama',
            'daftartindakan_nama',
            array(
                'header'=>'Kelas Pelayanan',
                'value'=>'$data->kelaspelayanan_nama',
                'filter'=>false,
            ),
            array(
                'name'=>'tarifTotal',
                'value'=>'$this->grid->getOwner()->renderPartial(\'_tarifTotal\',array(\'kelaspelayanan_id\'=>$data[kelaspelayanan_id],\'daftartindakan_id\'=>$data[daftartindakan_id]),true)',
            ),
            array(
                'header'=>'Cyto <br> Tindakan (%)',
                'value'=>'$data->persencyto_tind',
            ),
            array(
                'header'=>'Diskon <br> Tindakan (%)',
                'value'=>'$data->persendiskon_tind',
            ),
            array(
                'name'=>'Komponen Tarif',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/detailsTarif",array("idKelasPelayanan"=>$data->kelaspelayanan_id,"idDaftarTindakan"=>$data->daftartindakan_id, "idKategoriTindakan"=>$data->kategoritindakan_id)) ,array("title"=>"Klik Untuk Melihat Detail Tarif","target"=>"iframe", "onclick"=>"$(\"#dialogDetailsTarif\").dialog(\"open\");", "rel"=>"tooltip"))','htmlOptions'=>array('style'=>'text-align: center')
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )
);

?>

<?php
// ===========================Dialog Details Tarif=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog',
    array(
    'id'=>'dialogDetailsTarif',
    'options'=>
        array(
            'title'=>'Komponen Tarif',
            'autoOpen'=>false,
            'width'=>250,
            'height'=>300,
            'resizable'=>false,
            'scroll'=>false    
        ),
    )
);
?>
<iframe src="" name="iframe" width="100%" height="100%"></iframe>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details Tarif================================


Yii::app()->clientScript->registerScript('search', "

$('#formCari').submit(function(){
	$.fn.yiiGridView.update('daftarTindakan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

?>

<legend class="rim">Pencarian</legend> 
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'id'=>'formCari',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SARuanganM_instalasi_id',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
    )
);
?>

<table>
    <tr>
        <td>
            <?php
                echo $form->dropDownListRow($modTarifTindakanRuanganV,'instalasi_id',
                    CHtml::listData($modTarifTindakanRuanganV->InstalasiItems, 'instalasi_id', 'instalasi_nama'),
                    array(
                        'empty'=>'-- Pilih --',
                        'class'=>'span3', 
                        'onkeypress'=>"return nextFocus(this,event,'','')",
                        'ajax'=>array(
                            'type'=>'POST',
                            'url'=>Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi',array('encode'=>false,'namaModel'=>'PPTarifTindakanPerdaRuanganV')),
                            'update'=>'#PPTarifTindakanPerdaRuanganV_ruangan_id'
                        )
                    )
                );
            ?>
            <?php
                echo $form->dropDownListRow($modTarifTindakanRuanganV,'ruangan_id',
                    CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'),
                    array(
                        'empty'=>'-- Pilih --', 
                        'class'=>'span3', 
                        'onkeypress'=>"return nextFocus(this,event,'','')"
                    )
                );
            ?>
            <?php
                echo $form->dropDownListRow($modTarifTindakanRuanganV,'kelaspelayanan_id',  
                    CHtml::listData(KelasruanganM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan.kelaspelayanan_nama'),
                    array(
                        'class'=>'span3', 
                        'onkeypress'=>"return nextFocus(this,event,'','')",
                        'empty'=>'-- Pilih --'
                    )
                );
            ?>
        </td>
        <td>
            <?php
                echo $form->dropDownListRow($modTarifTindakanRuanganV,'kategoritindakan_id',
                    CHtml::listData($modTarifTindakanRuanganV->getTariftindakanperdaruangan(), 'kategoritindakan_id', 'kategoritindakan_nama'),
                    array(
                        'class'=>'span3', 
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'empty'=>'-- Pilih --'
                    )
                );
            ?>
            <?php
                echo $form->textFieldRow($modTarifTindakanRuanganV,'daftartindakan_id',
                    array(
                        'onkeypress'=>"return $(this).focusNextInputField(event);",
                        'maxlength'=>30
                    )
                );
            ?>
        </td>
    </tr>
</table>

<div class="form-actions">
    <?php
        echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
    ?>
    <?php
        echo CHtml::link(
            Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
            $this->createUrl('informasiJadwalPoli/index'),
            array(
                'class'=>'btn btn-danger'
            )
        );
    ?>
</div>
<?php $this->endWidget(); ?>

