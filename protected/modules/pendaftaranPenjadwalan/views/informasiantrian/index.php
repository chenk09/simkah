<?php
//$this->breadcrumbs=array(
//	'Ppinformasiantrianpasiens'=>array('index'),
//	'Manage',
//);
//
//$arrMenu = array();
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' PPInformasiantrianpasien ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PPInformasiantrianpasien', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PPInformasiantrianpasien', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                
//$this->menu=$arrMenu;
//
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ppinformasiantrianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php //$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>

<fieldset>
    <legend class="rim2">Informasi Antrian Pasien</legend>
</fieldset>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ppinformasiantrianpasien-grid',
	'dataProvider'=>$model->searchTable(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		'instalasi_nama',
//                'ruangan_nama',
                array(
                    'header'=>'Nama Ruangan',
                    'value'=>'$data->ruangan_nama',
                ),
                'no_urutantri',
                'tgl_pendaftaran',
                'no_rekam_medik',
                'nama_pasien',
                'no_pendaftaran',
                'alamat_pasien',
                array(
                    'name'=>'caraBayarPenjamin',
                    'value'=>'$data->caraBayarPenjamin',
                    'filter'=>false,
                ),
                'statusperiksa',
//		'pasien_id',
//		'jenisidentitas',
//		'no_identitas_pasien',
//		'namadepan',
//		
//		'nama_bin',
		/*
		'jeniskelamin',
		'tempat_lahir',
		'tanggal_lahir',
		
		'rt',
		'rw',
		'agama',
		'golongandarah',
		'photopasien',
		'alamatemail',
		'statusrekammedis',
		'statusperkawinan',
		
		'tgl_rekam_medik',
		'propinsi_id',
		'propinsi_nama',
		'kabupaten_id',
		'kabupaten_nama',
		'kelurahan_id',
		'kelurahan_nama',
		'kecamatan_id',
		'kecamatan_nama',
		////'pendaftaran_id',
		array(
                        'name'=>'pendaftaran_id',
                        'value'=>'$data->pendaftaran_id',
                        'filter'=>false,
                ),
		
		'tgl_pendaftaran',
		
		'transportasi',
		'keadaanmasuk',
		'statusperiksa',
		'statuspasien',
		'kunjungan',
		'alihstatus',
		'byphone',
		'kunjunganrumah',
		'statusmasuk',
		'umur',
		'no_asuransi',
		'namapemilik_asuransi',
		'nopokokperusahaan',
		'create_time',
		'create_loginpemakai_id',
		'create_ruangan',
		'carabayar_id',
		'carabayar_nama',
		'penjamin_id',
		'penjamin_nama',
		'shift_id',
		'ruangan_id',
		'ruangan_nama',
		'instalasi_id',
		'instalasi_nama',
		'jeniskasuspenyakit_id',
		'jeniskasuspenyakit_nama',
		*/
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
<?php $this->renderPartial('pendaftaranPenjadwalan.views.informasiantrian._search',array(
	'model'=>$model,
)); ?> 
<!-- search-form -->


</div>