<fieldset>
    <legend>Pencarian Kamar</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
    <?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
        'id'=>'informasiKamar-t-form',
        'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'htmlOptions'=>array(
                'onKeyPress'=>'return disableKeyPress(event)',
                'onSubmit'=>'return getInfoKamar(this);'
            ),
            'focus'=>'#',
    )); ?>
    
    <table>
        <tr>
            <td width="200px" height="80px">

                <?php
                    echo $form->dropDownListRow($modKamarRuangan,'ruangan_id',
                        CHtml::listData($modKamarRuangan->RuanganItems, 'ruangan_id', 'ruangan_nama'),
                        array(
                            'class'=>'span3',
                            'onkeypress'=>"return nextFocus(this,event,'','')",
                            'ajax'=>array(
                                'type'=>'POST',
                                'url'=>Yii::app()->createUrl('ActionDynamic/GetRuanganKamarRuangan',array('encode'=>false,'namaModel'=>'PPKamarruanganM')),
                                'update'=>'#PPKamarruanganM_kelaspelayanan_id',
                            ),
                        )
                    );
                ?>
                <?php echo $form->dropDownListRow($modKamarRuangan,'kelaspelayanan_id',  array(),array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')",'empty'=>'-- Pilih --',
                                                    'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetRuanganNoKamarRuangan',array('encode'=>false,'namaModel'=>'PPKamarruanganM')),
                                                              'update'=>'#PPKamarruanganM_kamarruangan_nokamar',),
                                                )); ?>
                <?php echo $form->dropDownListRow($modKamarRuangan,'kamarruangan_nokamar',  array(),array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')",'empty'=>'-- Pilih --')); ?>
                
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));?>
           </td>
           <td rowspan="2">
               <table>
                   <tr>
                       <td>
                           <table id="infoRuangan">
                               <tr>
                                   <td>
                                       <fieldset>
                                           <legend id="photo">Foto Ruangan <?php echo PPRuanganM::model()->findByPk($idRuangan)->ruangan_nama;?></legend>
                                                <img src="<?php echo Params::urlRuanganTumbsDirectory().'kecil_'.$modRuangan->ruangan_image ?>" />
                                       </fieldset>
                                   </td>
                                   <td>
                                       <fieldset>
                                        <legend>Fasilitas</legend>
                                            <div id="fasilitasRuangan"><?php echo $modRuangan->ruangan_fasilitas ?></div>
                                       </fieldset>
                                   </td>
                               </tr>
                           </table>
                          
                       <td>
                               
                       </td>
                   </tr>
                   <tr>
                       <td>
                          
                              
                       </td>
                   </tr>
               </table>
           </td>
        </tr>
        <tr>
            <td>
            </td> 
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend>Data Tempat Tidur</legend>
        <div id="form_kasur"></div>
</fieldset>
<?php $this->endWidget(); ?>

<?php

$idRuangan=CHtml::activeId($modKamarRuangan,'ruangan_id');
$idKelasPelayanan=  CHtml::activeId($modKamarRuangan,'kelaspelayanan_id');
$idNoKamar=  CHtml::activeId($modKamarRuangan,'kamarruangan_nokamar');
$jscript = <<< JS
function cekValidasi()
{
    idKelas=$('#${idKelasPelayanan}').val();
    idKamar=$('#${idNoKamar}').val();
    idRuangan=$('#${idRuangan}').val();
    
    if(idRuangan==''){
        alert('Anda Belum Memilih Ruangan');
    }
    else if(idKelas==''){
        alert('Anda Belum Memilih Kelas Pelayanan');
    }else if(idKamar==''){
        alert('Anda Belum Memilih Kamar');
    }else{
        $('#btn_simpan').click();
    }
}
JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
?>


<?php 
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahStatusKamar',
    'options'=>array(
        'title'=>'Ubah Status Kamar',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>
<script>
    $(document).ready(
        function()
        {
            var idRuangan = 207;
            getDataKamar(idRuangan);
            /*
            $.post('<?php echo Yii::app()->createUrl('/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/getInfoKamar');?>', {idRuangan:idRuangan},
                function(data)
                {
                    $("#infoRuangan").find('legend[id="photo"]').text('Foto Ruangan ' + data.data_ruangan.nama);
                    $("#infoRuangan").find('img').attr('src', data.data_ruangan.foto);
                    $("#infoRuangan").find('div[id="fasilitasRuangan"]').text(data.data_ruangan.fasilitas);
                },
            'json');
            */
        }
    );
    
    function getKelasPelayanan(obj)
    {
        $.post('<?php echo Yii::app()->createUrl('ActionDynamic/GetRuanganKamarRuangan');?>', {idRuangan:idRuangan},
            function(data)
            {
                $("#infoRuangan").find('legend[id="photo"]').text('Foto Ruangan ' + data.data_ruangan.nama);
                $("#infoRuangan").find('img').attr('src', data.data_ruangan.foto);
                $("#infoRuangan").find('div[id="fasilitasRuangan"]').text(data.data_ruangan.fasilitas);
            },
        'json');        
    }
    
    function getDataKamar(obj)
    {
        var idRuangan = null;
        if(typeof obj == 'obj')
        {
            idRuangan = $(obj).val();
        }else{
            idRuangan = obj;
        }
        
        $.post('<?php echo Yii::app()->createUrl('/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/getInfoKamar');?>', {idRuangan:idRuangan},
            function(data)
            {
                $("#infoRuangan").find('legend[id="photo"]').text('Foto Ruangan ' + data.data_ruangan.nama);
                $("#infoRuangan").find('img').attr('src', data.data_ruangan.foto);
                $("#infoRuangan").find('div[id="fasilitasRuangan"]').text(data.data_ruangan.fasilitas);
            },
        'json');
    }
    
    function getInfoKamar(obj)
    {
        var is_kosong = 0;
        $(obj).find('select').each(
            function()
            {
                if($(this).val() == "")
                {
                    is_kosong++;
                }
            }
        );
        if(is_kosong > 0)
        {
            alert('pilihan belum lengkap, coba cek lagi');
        }else{
            var idRuangan = 207;
            var data = {
                ruangan_id:$(obj).find('select[name$="[ruangan_id]"]').val(),
                kelaspelayanan_id:$(obj).find('select[name$="[kelaspelayanan_id]"]').val(),
                kamarruangan_nokamar:$(obj).find('select[name$="[kamarruangan_nokamar]"]').val()
            };
            $("#form_kasur").empty();
            $.post('<?php echo Yii::app()->createUrl('/' . Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/getDetailKamar');?>', data,
                function(data)
                {
                    if(data.form != "")
                    {
                        $("#form_kasur").append(data.form);
                    }
                    
                },
            'json');
        }
        return false;
    }
    
    function updateKamar(idKamar){
        var idKamar = idKamar;
        var answer = confirm('Yakin Akan Merubah Status Keterangan Kamar ?');
        if (answer){
            $.post('<?php echo Yii::app()->createUrl('ActionAjax/UbahStatusKamar');?>', {idKamar:idKamar}, function(data){
                        {
                            if (data.status == 'create_form')
                            {
                                $('#dialogUbahStatusKamar div.divForForm').html(data.div);
                                $('#dialogUbahStatusKamar div.divForForm form').submit(updateKetKamar);
                                $('#idKamar').val(idKamar);

                            }
                            else if(data.status == 'proses_form')
                            {
                                $('#dialogUbahStatusKamar div.divForForm').html(data.div);
                                setTimeout("$('#dialogUbahStatus').dialog('close')",1000);
                            }

                        }
            }, 'json');
            return false;
        }else{
            preventDefault();
        }
    }
    
         function updateKetKamar()
        {
            <?php 
                    echo CHtml::ajax(array(
                    'url'=>Yii::app()->createUrl('ActionAjax/UbahStatusKetKamar'),
                    'data'=> "js:$(this).serialize()",
                    'type'=>'post',
                    'dataType'=>'json',
                    'success'=>"function(data)
                    {
                        if (data.status == 'create_form')
                        {
                            $('#dialogUbahStatusKamar div.divForForm').html(data.div);
                            $('#dialogUbahStatusKamar div.divForForm form').submit(updateKetKamar);
                        }
                        else
                        {
                            $('#dialogUbahStatusKamar div.divForForm').html(data.div);
                            setTimeout(\"$('#dialogUbahStatusKamar').dialog('close') \",1000);
                        }

                    } ",
            ))
        ?>;
            return false; 
        }
</script>