<?php// $this->renderPartial('_tab'); ?>
<?php
$this->breadcrumbs=array(
	'Ppgolonganumur Ms'=>array('index'),
	$model->golonganumur_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Golongan umur '/*.$model->golonganumur_id*/, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Golongan Umur', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PPGolonganumurM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PPGolonganumurM', 'icon'=>'pencil','url'=>array('update','id'=>$model->golonganumur_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PPGolonganumurM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->golonganumur_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Golongan Umur', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'golonganumur_id',
		'golonganumur_nama',
		'golonganumur_namalainnya',
		'golonganumur_minimal',
		'golonganumur_maksimal',
		//'golonganumur_aktif',
                array(
                    'name'=>'gologanumur_aktif',
                    'type'=>'raw',
                    'value'=>(($model->golonganumur_aktif == 1) ? Yii::t('mds', 'Yes') : Yii::t('mds', 'No'))
                )
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>