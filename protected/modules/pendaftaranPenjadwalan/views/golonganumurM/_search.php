<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'ppgolonganumur-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'golonganumur_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'golonganumur_nama',array('class'=>'span3','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'golonganumur_namalainnya',array('class'=>'span3','maxlength'=>25)); ?>

	<?php echo $form->textFieldRow($model,'golonganumur_minimal',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'golonganumur_maksimal',array('class'=>'span3')); ?>

	<?php echo $form->checkBoxRow($model,'golonganumur_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
