<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
  $.fn.yiiGridView.update('PPInfoKunjungan-v', {
    data: $(this).serialize()
  });
  return false;
});
");
?>
<?php 
  $ruangan_id = Yii::app()->user->getState('ruangan_id');
  $link = explode("/", $_GET['r']);
  if($link[0]=='rekamMedis'){
    $anamnesa_link = 'pemeriksaanFisikAnamnesaRM';
  }else{
    $anamnesa_link = 'pemeriksaanFisikAnamnesa';
  }
?>

<fieldset>
    <legend class="rim2">Informasi Pasien Rawat Jalan</legend>
    <div style="max-width:1270px;overflow-x:scroll;">
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView',
        array(
            'id'=>'PPInfoKunjungan-v',
            'dataProvider'=>$modPPInfoKunjunganRJV->searchRJ(),
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                  'header'=>'Tgl Pendaftaran',
                  'type'=>'raw',
                  'value'=>'$data->tgl_pendaftaran',
                ),
                array(
                    'header'=>'No. RM',
                    'name'=>'no_rm',
                    'type'=>'raw',
                    'value'=>'($data->statusperiksa!=Params::statusPeriksa(2) ? CHtml::link("<i class=icon-pencil-brown></i> ". $data->no_rekam_medik , Yii::app()->createUrl("'. Yii::app()->controller->module->id .'/'. Yii::app()->controller->id .'/ubahPasien",array("id"=>"$data->pasien_id", "menu"=>"RJ")),array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Edit Data Pasien")) : $data->no_rekam_medik )',
                    'htmlOptions'=>array('style'=>'text-align: center;')
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'name'=>'no_pendaftaran',
                    'type'=>'raw',
                    'value'=>'(!empty($data->no_pendaftaran) ? CHtml::link("<i class=icon-print></i> Gelang Dewasa", "javascript:printLabel(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Gelang Dewasa")) . "<br>" .
          CHtml::link("<i class=icon-print></i> Gelang Anak", "javascript:printLabel2(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Gelang Anak")) . "<br>" . 
          
          CHtml::link("<i class=icon-print></i> Label RM", "javascript:printLabelBaru(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Label Pasien")) . "<br>" .

          CHtml::link("<i class=icon-print></i> Cover", "javascript:printcover(\'$data->pendaftaran_id&status=3\' );",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Cetak Cover")) . "<br>" .

          CHtml::link("<i class=icon-print></i> ".$data->no_pendaftaran, "javascript:print(\'$data->pendaftaran_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Print Lembar Poli")) : "-")',
                    'htmlOptions'=>array('style'=>'text-align: left;')
                ),
                array(
                    'header'=>'Nama Depan',
                    'type'=>'raw',
                    'value'=>'$data->namadepan'
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->NamaAlias'
                ),
                'alamat_pasien',
                array(
          'name'=>'Jenis Kelamin',
          'type'=>'raw',
          'value'=>'(!empty($data->jeniskelamin) && ($data->statusperiksa != Params::statusPeriksa(4) && $data->statusperiksa != Params::statusPeriksa(2)) ? CHtml::link("<i class=icon-pencil-brown></i> ".$data->jeniskelamin," ",array("onclick"=>"ubahJenisKelamin(\'$data->no_rekam_medik\');$(\'#editJenisKelamin\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Jenis Kelamin Pasien")):$data->jeniskelamin)',                
          'htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
                   'name'=>'Kelompok Penyakit',
                   'type'=>'raw',
                   'value'=>'((!empty($data->jeniskasuspenyakit_nama) && ($data->statusperiksa!=Params::statusPeriksa(4) && $data->statusperiksa!=Params::statusPeriksa(2))) ? CHtml::link("<i class=icon-pencil-brown></i> ".$data->jeniskasuspenyakit_nama," ",array("onclick"=>"ubahKelompokPenyakit(\'$data->pendaftaran_id\');$(\'#editKelPenyakit\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Kelompok Penyakit")): $data->jeniskasuspenyakit_nama)',
                   'htmlOptions'=>array(
                       'style'=>'text-align: center',
                       'class'=>'rajal'
                   )
                ),
                array(
                   'header'=>'Cara Masuk',
                   'type'=>'raw',
                   'value'=>'$data->statusmasuk',
                ),
                array(
                    'header'=>'Status Konfirmasi',
                    'type'=>'raw',
                    'value'=>'($data->status_konfirmasi == "" ) ? "-" : $data->status_konfirmasi',
                ),
                array(
                   'header'=>'Perujuk',
                   'type'=>'raw',
                   'value'=>'$data->nama_perujuk',
                ),
                //  array(
                //    'header'=>'P3 / Asuransi',
                //    'type'=>'raw',
                //    'value'=>'$data->namapemilik_asuransi',
                // ),
                array(
                   'name'=>'CaraBayar/Penjamin',
                   'type'=>'raw',
                   'value'=>'((!empty($data->CaraBayarPenjamin)&&($data->statusperiksa!=Params::statusPeriksa(4) && $data->statusperiksa!=Params::statusPeriksa(2))) ? CHtml::link("<i class=icon-pencil-brown></i> ".$data->CaraBayarPenjamin," ",array("onclick"=>"ubahCaraBayar(\'$data->nama_pasien\');listCaraBayar(\'$data->carabayar_id\');setIdPendaftaran(\'$data->pendaftaran_id\',\'$data->no_pendaftaran\');$(\'#carabayardialog\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Cara Bayar & Penjamin pasien")) : $data->CaraBayarPenjamin)',
                   'htmlOptions'=>array(
                       'style'=>'text-align: center',
                       'class'=>'rajal'
                   )
                ),
                array(
                   'name'=>'Nama Dokter',
                   'type'=>'raw',
                   'value'=>'"<div style=\'width:100px;\'>" . ((!empty($data->nama_pegawai) && ($data->statusperiksa!=Params::statusPeriksa(4) && $data->statusperiksa!=Params::statusPeriksa(2))) ? CHtml::link("<i class=icon-pencil-brown></i> ". $data->gelardepan." ".$data->nama_pegawai." ".$data->gelarbelakang_nama," ",array("onclick"=>"ubahDokterPeriksa(\'$data->pendaftaran_id\');$(\'#editDokterPeriksa\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Dokter Periksa")) : $data->nama_pegawai) . "</div>"',
                   'htmlOptions'=>array(
                       'style'=>'text-align:center;',
                       'class'=>'rajal'
                   )
                ),
                array(
                   'name'=>'Kelas Pelayanan',
                   'type'=>'raw',
                   'value'=>'"<div style=\'width:50px;\'>" . ((!empty($data->kelaspelayanan_nama) && ($data->statusperiksa!=Params::statusPeriksa(4) && $data->statusperiksa!=Params::statusPeriksa(2))) ? CHtml::link("<i class=icon-pencil-brown></i>". $data->kelaspelayanan_nama," ",array("onclick"=>"ubahKelasPelayanan(\'$data->pendaftaran_id\');$(\'#editKelasPelayanan\').dialog(\'open\');return false;", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Data Kelas Pelayanan")) : $data->kelaspelayanan_nama) . "</div>"',
                   'htmlOptions'=>array(
                       'style'=>'text-align:center;',
                       'class'=>'rajal'
                   )
                ),
                array(
                   'header'=>'Poliklinik',
                   'name'=>'ruangan_nama',
                   'type'=>'raw',
                   'value'=>'((!empty($data->ruangan_nama)&&($data->statusperiksa==Params::statusPeriksa(1))) ? "<div style=\'width:100px;\'>" . CHtml::link("<i class=icon-pencil-brown></i> ".$data->ruangan_nama,"javascript:gantiPoli(\'$data->pendaftaran_id\',\'$data->ruangan_id\',\'$data->instalasi_id\',\'$data->pasien_id\',\'$data->nama_pasien\',\'$data->jeniskasuspenyakit_id\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Poliklinik")) . "</div>" : $data->ruangan_nama) ',
                   'htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
               'header'=>'Verifikasi Diagnosa',
               'type'=>'raw',
               'value'=>''
                .'(isset($data->Morbiditas->pasienmorbiditas_id) ? "<div class=\"inap\" style=\"background-color:#33FF00; text-align: center\">" : "<div style=\"background-color:#FF0000; text-align: center\">")'
                .'.(CHtml::Link("<i class=icon-pencil></i>Verifikasi Diagnosa",Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/verifikasiDiagnosa/index",array("id"=>$data->pendaftaran_id,"menu"=>"RJ","frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeVerifikasiDiagnosa",
                                  "onclick"=>"$(\"#dialogVerifikasiDiagnosa\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik Verifikasi Diagnosa",
                    )))."</div>"',
                'htmlOptions'=>array('style'=>'text-align: center')
                ),
                array(
                   'header'=>'Pemeriksaan Fisik <br/> & Anamnesa',
                   'type'=>'raw',
                   'value'=>'CHtml::link("<i class=icon-list-alt></i>", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.$anamnesa_link.'/indexAnamnesa",array("idPendaftaran"=>"$data->pendaftaran_id", "menu"=>"RD")), array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Verifikasi Diagnosa"))',
                   'htmlOptions'=>array(
                   'style'=>'text-align: center',
                   )
                ),  
                array(
        'name'=>'statusperiksa',
        'type'=>'raw',
        'value'=>'((!empty($data->statusperiksa)&& ($data->statusperiksa==Params::statusPeriksa(1))) ? CHtml::link("<i class=icon-remove-sign></i> ".$data->statusperiksa, "javascript:dialogBatalPeriksa(\'$data->pendaftaran_id\',\'$data->statusperiksa\',\'$data->nama_pasien\');",array("rel"=>"tooltip","rel"=>"tooltip","title"=>"Klik Membatalkan Pemeriksaan")) : $data->statusperiksa) ',
        'htmlOptions'=>array(
          'style'=>'text-align: center',
          'class'=>'status'
        )
    ),
//    
            ),
            'afterAjaxUpdate'=>'function(id, data){
                jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                disableLink();
            }',
        )
    );
?>
    </div>
    
<div class="search-form" style="">
<?php
    $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'type'=>'horizontal',
            'id'=>'formCari',
            'htmlOptions'=>array(
                'enctype'=>'multipart/form-data',
                'onKeyPress'=>'return disableKeyPress(event)'
            ),
        )
    );
?>
<fieldset>
     <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table>
        <tr>
            <td>
              <?php echo $form->labelEx($modPPInfoKunjunganRJV,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPPInfoKunjunganRJV,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
                                         'options'=> array(
                                            'maxDate'=>'d',
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                         ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
              </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' Sampai Dengan', array('class'=>'control-label')) ?>
                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPPInfoKunjunganRJV,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                   <?php echo $form->textFieldRow($modPPInfoKunjunganRJV,'no_rekam_medik',array('class'=>'span3 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->textFieldRow($modPPInfoKunjunganRJV,'nama_pasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->textAreaRow($modPPInfoKunjunganRJV,'alamat_pasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                   <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'status_konfirmasi',Params::statusKonfirmasiAsuransi(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
            </td>
            <td width="52%">
   
   
   
                   <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'propinsi_id', CHtml::listData($modPPInfoKunjunganRJV->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRJV')),
                                                          'update'=>'#PPInfoKunjunganRJV_kabupaten_id'),
                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                          )); ?>
                   <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'kabupaten_id',array(),
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRJV')),
                                                          'update'=>'#PPInfoKunjunganRJV_kecamatan_id'),
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        )); ?>
                   <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'kecamatan_id',array(),
                                      array('empty'=>'-- Pilih --',
                                            'ajax'=>array('type'=>'POST',
                                                          'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRJV')),
                                                          'update'=>'#PPInfoKunjunganRJV_kelurahan_id'),
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)"
                                          )); ?>
                   <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'kelurahan_id',array(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            </td>
        </tr>
                            <tr>
                                <td width="48%">
                                    <?php echo $form->dropDownListRow($modPPInfoKunjunganRJV,'carabayar_id', CHtml::listData($modPPInfoKunjunganRJV->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                'ajax' => array('type'=>'POST',
                                                    'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PPInfoKunjunganRJV')), 
                                                    'update'=>'#PPInfoKunjunganRJV_penjamin_id'  //selector to update
                                                ),
                                         )); ?>
                                    <?php echo CHtml::label('Penjamin',' Penjamin', array('class'=>'control-label')) ?>&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php echo $form->dropDownList($modPPInfoKunjunganRJV,'penjamin_id', CHtml::listData($modPPInfoKunjunganRJV->getPenjaminItems(), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                                </td>                               
                            </tr>
                        </table>
                </fieldset>
           
<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
    <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('infoKunjunganRJ/index'), array('class'=>'btn btn-danger')); ?>
                 <?php //if((!$model->isNewRecord) AND ((PPKonfigSystemK::model()->find()->printkartulsng==TRUE) OR (PPKonfigSystemK::model()->find()->printkartulsng==TRUE)))
                        //{  
                ?>
                            <script>
                                //print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                       //}else{
                       // echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                      // } 
                ?> 
<?php 
$content = $this->renderPartial('pendaftaranPenjadwalan.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>  
    
</div>

<?php $this->endWidget(); ?>

   <?php 
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahStatus',
    'options'=>array(
        'title'=>'Ubah Status Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>
    
<script type="text/javascript">
// here is the magic


function disableLink()
{
    var status = null;
    $("#PPInfoKunjungan-v tbody").find('tr > td[class="rajal"]').each(
        function()
        {
            status = $(this).parent().find('td[class="status"]');
            var xxx = $(this).find('a');
            if(status.text() == 'SUDAH PULANG')
            {
               $(this).text($.trim(xxx.text()));
               $(this).find('a').remove();
            }
        }
    );
}
disableLink();

function ubahCaraBayar(namaPasien)
{   
    $('#titleNamaPasienCaraBayar').html(namaPasien);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahCaraBayar')?>',
                 'data':$(this).serialize(),
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                            if (data.status == 'create_form') {
                                $('#carabayardialog div.divForFormUbahCaraBayar').html(data.div);
                                $('#carabayardialog div.divForFormUbahCaraBayar form').submit(ubahCaraBayar);
                            } else {
                                $('#carabayardialog div.divForFormUbahCaraBayar').html(data.div);
                                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                                        data: $(this).serialize()
                                });
                                setTimeout("$('#carabayardialog').dialog('close') ",500);
                            }
                 } ,
                 'cache':false});
    return false; 
}

function ubahJenisKelamin(norm)
{
    $('#temp_norekammedik').val(norm);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahJenisKelamin')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editJenisKelamin div.divForFormEditJenisKelamin').html(data.div);
                $('#editJenisKelamin div.divForFormEditJenisKelamin form').submit(ubahJenisKelamin);
            }else{
                $('#editJenisKelamin div.divForFormEditJenisKelamin').html(data.div);
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editJenisKelamin').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function ubahKelompokPenyakit(idPendaftaran)
{
    $('#temp_idPendaftaran').val(idPendaftaran);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahKelompokPenyakit', array('menu'=>'RJ'))?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editKelPenyakit div.divForFormEditKelPenyakit').html(data.div);
                $('#editKelPenyakit div.divForFormEditKelPenyakit form').submit(ubahKelompokPenyakit);
            }else{
                $('#editKelPenyakit div.divForFormEditKelPenyakit').html(data.div);
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editKelPenyakit').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function ubahDokterPeriksa(idPendaftaran)
{
    $('#temp_idPendaftaranDP').val(idPendaftaran);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahDokterPeriksa')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa form').submit(ubahDokterPeriksa);
            }else{
                $('#editDokterPeriksa div.divForFormEditDokterPeriksa').html(data.div);
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editDokterPeriksa').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function ubahKelasPelayanan(idPendaftaran)
{
    $('#temp_idPendaftaranKP').val(idPendaftaran);
    jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/ubahKelasPelayanan')?>',
        'data':$(this).serialize(),
        'type':'post',
        'dataType':'json',
        'success':function(data){
            if (data.status == 'create_form') {
                $('#editKelasPelayanan div.divForFormEditKelasPelayanan').html(data.div);
                $('#editKelasPelayanan div.divForFormEditKelasPelayanan form').submit(ubahKelasPelayanan);
            }else{
                $('#editKelasPelayanan div.divForFormEditKelasPelayanan').html(data.div);
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                        data: $(this).serialize()
                });
                setTimeout("$('#editKelasPelayanan').dialog('close') ",500);
            }
        },
        'cache':false
    });
    return false; 
}

function listCaraBayar(idCaraBayar){
    $('#carabayardialog #tempCaraBayarId').val(idCaraBayar);
    return false;
}

function setIdPendaftaran(idPendaftaran,noPendaftaran)
{
    $('#carabayardialog #tempPendaftaranId').val(idPendaftaran);
    $('#carabayardialog #tempNoPendaftaran').val(noPendaftaran);
}
</script>
    
<?php
    
//========================================= Cara Bayar dialog =============================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'carabayardialog',
    'options'=>array(
        'title'=>'Ganti Cara Bayar dan Penjamin - <span id="titleNamaPasienCaraBayar"></span>',
        'autoOpen'=>false,
        'minWidth'=>650,
        'modal'=>true,
        'resizable'=>false,
        //'hide'=>explode,
    ),
));
echo CHtml::hiddenField('tempCaraBayarId','',array('readonly'=>true));
echo CHtml::hiddenField('tempPendaftaranId','',array('readonly'=>true));
echo CHtml::hiddenField('tempNoPendaftaran','',array('readonly'=>true));
echo '<div class="divForFormUbahCaraBayar"></div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');
//========================================================= end cara bayar dialog =========

// ===========================Dialog Batal Periksa=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'DialogBatalperiksa',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Batal Periksa - <span id="titleNamaPasienBatal"></span>',
                        'autoOpen'=>false,
                        'show'=>'blind',
                        'hide'=>'explode',
                        'minWidth'=>500,
                        'minHeight'=>100,
                        'resizable'=>false,
                        'modal'=>true,    
                         ),
                    ));
$this->renderPartial($this->pathView.'_formBatalPeriksaDialog');                    

$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Batal Periksa================================

//=============================== Ganti Poli Dialog =======================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'ganti_poli',
    'options'=>array(
        'title'=>'Ganti Ruangan Pasien - <span id="titleNamaPasien"></span>',
        'autoOpen'=>false,
        'minWidth'=>400,
        'modal'=>true,
    ),
));
?>
<table>
    <tr>
        <td>Poliklinik</td>
        <td>:</td>
        <td>
            <?php echo CHtml::dropDownList('ruangan_sebelumnya','', array(),array('disabled'=>true));?>
            <?php echo CHtml::hiddenField('ruangan_awal','',array('readonly'=>true));?>
        </td>
    </tr>
    <tr>
        <td>Jenis Kasus Penyakit</td>
        <td>:</td>
        <td>
            <?php echo CHtml::dropDownList('jeniskasuspenyakit_sebelumnya','', array(),array('disabled'=>true));?>
            <?php echo CHtml::hiddenField('jeniskasuspenyakit_awal','',array('readonly'=>true));?>
        </td>
    </tr>
    <tr>
        <td>Alasan Perubahan <span class="required">*</span></td>
        <td>:</td>
        <td><?php echo CHtml::textArea('alasanperubahan','', array());?></td>
    </tr>
    <tr>
        <td>Menjadi Poliklinik</td>
        <td>:</td>
        <td><?php echo CHtml::dropDownList('ruangan_id_ganti','ruangan_id_ganti', array(),
                array('empty'=>'--pilih--','onChange'=>'getKasusPenyakit();'));?></td>
    </tr>
    <tr>
        <td>Menjadi Jenis Kasus Penyakit</td>
        <td>:</td>
        <td><?php echo CHtml::dropDownList('jeniskasuspenyakit_id_ganti','jeniskasuspenyakit_id_ganti', array(),
                array('empty'=>'--pilih--' ));?></td>
    </tr>
</table>
<?php
echo CHtml::hiddenField('pendaftaran_id');
echo CHtml::hiddenField('pasien_id');
echo CHtml::hiddenField('instalasi_id');
echo CHtml::htmlButton(Yii::t('mds','{icon} Ya',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'simpanRuanganBaru();'));
echo '&nbsp;&nbsp;&nbsp;'.CHtml::htmlButton(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                                                array('class'=>'btn btn-danger', 'type'=>'button','onclick'=>'$(\'#ganti_poli\').dialog(\'close\');'));
                        

$this->endWidget('zii.widgets.jui.CJuiDialog');
//================================ end Ganti Ruangan Dialog =================================


//======================================================JAVA SCRIPT===================================================                          
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ',array('idPendaftaran'=>''));
$urlPrintLembar = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>''));
$urlPrintLembarPolidua = Yii::app()->createUrl('print/lembarPoliRJBlanko1',array('idPendaftaran'=>''));
$urlPrintLembarCover = Yii::app()->createUrl('print/lembarPoliRJBlanko',array('idPendaftaran'=>'' ) );

$urlPrintLembarPoliBaru = Yii::app()->createUrl('print/lembarPoliRD',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlGetRuangan=Yii::app()->createUrl('ActionAjax/GetRuanganPasien'); 
$simpanRuanganBaru=Yii::app()->createUrl('ActionAjax/SaveRuanganBaru'); 
$statusPeriksaBatalPeriksa=Params::statusPeriksa(5);
$js = <<< JSCRIPT
//======================================Awal batal Periksa============================================================

function dialogBatalPeriksa(pendaftaran_id,statusperiksa,namaPasien)
   {
        $('#titleNamaPasienBatal').html(namaPasien);
        $('#DialogBatalperiksa #pendaftaran_id').val(pendaftaran_id);
        $('#DialogBatalperiksa #statusperiksa').val(statusperiksa);
        $('#DialogBatalperiksa').dialog('open');
        
   } 
function ubahPeriksa()
    {
      var url =$('#url').val();
      var statusperiksa=$('#DialogBatalperiksa #statusperiksa').val();
      var pendaftaran_id=$('#DialogBatalperiksa #pendaftaran_id').val(); 
      var tglbatal=$('#DialogBatalperiksa #tglbatal').val();
      var keterangan_batal=$('#DialogBatalperiksa #keterangan_batal').val();
      if(statusperiksa=='${statusPeriksaBatalPeriksa}')
        {
            alert('Pasien Sudah Dibatalkan');
        }
      else
        {
             $.post("${url}/ubahPeriksa", {pendaftaran_id: pendaftaran_id,statusperiksa:statusperiksa,tglbatal:tglbatal,keterangan_batal:keterangan_batal,},
                function(data){
                    if(data.success) {
                        $('#DialogBatalperiksa').dialog('close');
                        alert(data.message);
                    } else {
                        alert(data.message);
                        $('#DialogBatalperiksa #keterangan_batal').attr('class','error');
                    }
                     $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                            data: $(this).serialize()
                    });
                },"json");
            
        }
   
    }   
//=======================================Akhir Batal Periksa=============================================================   

//=======================================Awal Print Lembar Poli==========================================================

function print(idPendaftaran)
{
   window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}
//========================================Akhir Print Lembar Poli========================================================

//=======================================Awal Print Label Pasien ==========================================================
function printLabel(idPendaftaran)
{
   window.open('${urlPrintLembar}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}
//========================================Akhir Print Label Pasien ========================================================

//=======================================Awal Print Label Pasien Baru ==========================================================
function printLabelBaru(idPendaftaran)
{
   window.open('${urlPrintLembarPoliBaru}'+idPendaftaran,'printwin','left=100,top=100,width=350,height=350');    
}
//========================================Akhir Print Label Pasien ========================================================

//=======================================Awal Print Label Pasien gelang anak ==========================================================
function printLabel2(idPendaftaran)
{
   window.open('${urlPrintLembarPolidua}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}

function printcover(idPendaftaran)
{
   window.open('${urlPrintLembarCover}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');    
}
//========================================Akhir Print Label Pasien ========================================================

//========================================Awal Ganti Ruangan=============================================================

function gantiPoli(pendaftaran_id,ruangan_id,instalasi_id,pasien_id,namaPasien,jeniskasuspenyakit_id)
    {
        $('#titleNamaPasien').html(namaPasien);
           $.post("${urlGetRuangan}", { pendaftaran_id: pendaftaran_id, ruangan_id: ruangan_id, instalasi_id:instalasi_id, pasien_id:pasien_id,
           jeniskasuspenyakit_id:jeniskasuspenyakit_id},
           function(data){
            $('#ganti_poli').dialog('open');
            $('#ganti_poli #ruangan_awal').val(ruangan_id);
            $('#ganti_poli #jeniskasuspenyakit_awal').val(jeniskasuspenyakit_id);
            $('#ganti_poli #ruangan_sebelumnya').html(data.dropDown);
            $('#ganti_poli #ruangan_id_ganti').html(data.dropDown);
            $('#ganti_poli #jeniskasuspenyakit_sebelumnya').html(data.jenisKasusPenyakit);
            $('#ganti_poli #jeniskasuspenyakit_id_ganti').html(data.jenisKasusPenyakit);
            $('#ganti_poli #pendaftaran_id').val(pendaftaran_id);
            $('#ganti_poli #pasien_id').val(pasien_id);
            $('#ganti_poli #instalasi_id').val(instalasi_id);
        }, "json");
    }
    
 function simpanRuanganBaru()
    {
        if($('#ganti_poli #alasanperubahan').val()==''){
           alert('Alasan Perubahan tidak boleh kosong!');
           $('#ganti_poli #alasanperubahan').addClass('error');
           return false;
        }
        $('#ganti_poli').dialog('close');
        var pendaftaran_id= $('#ganti_poli #pendaftaran_id').val();
        var pasien_id= $('#ganti_poli #pasien_id').val();
        var ruangan_id= $('#ganti_poli #ruangan_id_ganti').val();
        var jeniskasuspenyakit_id= $('#ganti_poli #jeniskasuspenyakit_id_ganti').val();
        var ruangan_awal= $('#ganti_poli #ruangan_awal').val();
        var alasan = $('#ganti_poli #alasanperubahan').val();
        $.post("${simpanRuanganBaru}", { jeniskasuspenyakit_id:jeniskasuspenyakit_id, pendaftaran_id: pendaftaran_id, ruangan_id: ruangan_id, ruangan_awal: ruangan_awal, alasan:alasan, pasien_id:pasien_id},
            function(data){
                if(data.status=='Gagal'){                
                    alert(data.status);
                }else{
                    alert("Data berhasil disimpan");
                }
                $.fn.yiiGridView.update('PPInfoKunjungan-v', {
                    data: $('#formCari').serialize()
                });
            }, "json");
    }
//========================================Akhir Ganti Ruangan===========================================================

JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);                        

$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php
    //=============================== Ganti Data Jenis Kelamin Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editJenisKelamin',
            'options'=>array(
                'title'=>'Ganti Data Jenis Kelamin',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_norekammedik','',array('readonly'=>true));
    echo '<div class="divForFormEditJenisKelamin"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<?php
    //=============================== Ganti Data Jenis Kelamin Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editKelPenyakit',
            'options'=>array(
                'title'=>'Ganti Data Kelompok Penyakit',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaran','',array('readonly'=>true));
    echo '<div class="divForFormEditKelPenyakit"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>


<?php
    //=============================== Ganti Data Jenis Kelamin Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editDokterPeriksa',
            'options'=>array(
                'title'=>'Ganti Dokter Periksa',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranDP','',array('readonly'=>true));
    echo '<div class="divForFormEditDokterPeriksa"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
    //=============================== Ganti Data Kelas Pelayanan Dialog =======================================
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'editKelasPelayanan',
            'options'=>array(
                'title'=>'Ganti Kelas Pelayanan',
                'autoOpen'=>false,
                'minWidth'=>500,
                'modal'=>true,
            ),
        )
    );
    echo CHtml::hiddenField('temp_idPendaftaranKP','',array('readonly'=>true));
    echo '<div class="divForFormEditKelasPelayanan"></div>';
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<?php
$urlSessionUbahStatus = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionUbahStatus ');
$jscript = <<< JS
function buatSessionUbahStatus(idPendaftaran)
{
    var answer = confirm('Yakin Akan Merubah Status Periksa Pasien?');
        if (answer){
            $.post("${urlSessionUbahStatus}", {idPendaftaran: idPendaftaran },
                function(data){
                    'sukses';
            }, "json");
            ubahStatusPeriksa();
        }
        else
            {

            }
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<script>
        function ubahStatusPeriksa()
{
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/ubahStatusPeriksaRJ'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogUbahStatus').dialog('open');
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $('#dialogUbahStatus div.divForForm form').submit(ubahStatusPeriksa);
                    
                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false}, 
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    return false;
                }
                else
                {
                    $('#dialogUbahStatus div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('PPInfoKunjungan-v');
                    setTimeout(\"$('#dialogUbahStatus').dialog('close') \",1000);
                }
 
            } ",
    ))
?>;
    return false; 
}    
</script>
<!-- UNTUK PERUBAHAN JENIS KASUS PENYAKIT DI UBAH POLI -->
<?php
$js = <<< JSCRIPT

function getKasusPenyakit(){
    ruangan_id = $('#ruangan_id_ganti').val();
    pendaftaran_id = $('#pendaftaran_id').val();
    pasien_id = $('#pasien_id').val();
    instalasi_id = $('#instalasi_id').val();
    jeniskasuspenyakit_id = '';  
        
   $.post("${urlGetRuangan}", { pendaftaran_id: pendaftaran_id, ruangan_id: ruangan_id, instalasi_id:instalasi_id, pasien_id:pasien_id,
   jeniskasuspenyakit_id:jeniskasuspenyakit_id},
   function(data){
            $('#ganti_poli').dialog('open');            
            $('#ganti_poli #ruangan_id_ganti').html(data.dropDown);
            $('#ganti_poli #jeniskasuspenyakit_id_ganti').html(data.jenisKasusPenyakit);
    }, "json");
}
    
JSCRIPT;
Yii::app()->clientScript->registerScript('getKasusPenyakit',$js,CClientScript::POS_HEAD);
?>
<!-- UNTUK PERUBAHAN JENIS KASUS PENYAKIT DI UBAH POLI -->