<?php
class SendToSep extends CApplicationComponent{
    private $c_cons_id;
    private $c_secret_key;
    private $base_url;
    private $service_name;
    public function __construct(){
        $accountConfig = AccountConfig::model()->findByPk('simjk');
        $this->c_cons_id = $accountConfig->cons_id;
        $this->c_secret_key = $accountConfig->secret_key;
        $this->base_url = $accountConfig->base_url;
        $this->service_name = $accountConfig->service_name;
    }
    private function sendToServer($params, $jenis = 'GET'){
        date_default_timezone_set('UTC');
        $time_stamp= strval(time() - strtotime('1970-01-01 00:00:00'));
        $signature = hash_hmac('sha256', $this->c_cons_id . "&" . $time_stamp, $this->c_secret_key, true);
        $encode_signature = base64_encode($signature);
        $hade = array(
            "X-cons-id: " . $this->c_cons_id,
            "X-timestamp: $time_stamp",
            "X-signature: $encode_signature"
        );
        $ch = curl_init();
        $curl_params = array(
            CURLOPT_URL => $params['path_uri'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTPHEADER => array(
                "X-cons-id: " . $this->c_cons_id,
                "X-timestamp: $time_stamp",
                "X-signature: $encode_signature"
            )
        );
        if($jenis == 'POST'){
            $curl_params[CURLOPT_POST] = true;
            $curl_params[CURLOPT_POSTFIELDS] = $params['posting'];
        }
        curl_setopt_array($ch, $curl_params);
        $output = curl_exec($ch);
        if($output){
            $return = array();
            $hasil = json_decode($output, true);
            $return['code'] = $hasil['metadata']['code'];
            $return['message'] = $hasil['metadata']['message'];
            if(is_array($hasil['response'])){
                $rec = array();
                foreach($hasil['response'] as $key => $value){
                    $rec[$key] = $value;
                }
                $return['record'] = $rec;
            }else{
                $return['record'] = $hasil['response'];
            }
            return $return;
        }else return false;
    }
    public function requestFromClient($params, $jenis_api){
        $result = array(
            'status'=>'OK'
        );
        try{
            $daftarApi = DaftarApi::model()->findByAttributes(array(
                'kode'=>$jenis_api
            ));
            if(!$daftarApi){
                throw new Exception("Don't have privileges", 403);
            }
            $url = str_replace('{service_name}', $this->service_name, str_replace('{base_url}', $this->base_url, $daftarApi->url));
            $posting = array();
            if(count($params) > 0){
                if($daftarApi->method == 'GET'){
                    $_params_api = json_decode($daftarApi->params, true);
                    $rec = array();
                    foreach($_params_api as $key=>$val){
                        $rec[$key] = $key . "/" . $params[$key];
                    }
                    $url = $url . "/" . implode("/", $rec);
                }else{
                    $posting = $params;
                }
            }
            $hasil = $this->sendToServer(array(
                'path_uri'=>$url,
                'posting'=>$posting
            ), $daftarApi->method);
            if(!$hasil){
                throw new Exception("Koneksi server putus, cek koneksi server", 500);
            }
            if($hasil['code'] != 200){
                throw new Exception($hasil['message'], 500);
            }else{
                $result['record'] = $hasil['record'];
            }
        }catch(Exception $exc){
            $result['status'] = 'NOT';
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        return $result;
    }
}
?>
