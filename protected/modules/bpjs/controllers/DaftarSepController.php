<?php

class DaftarSepController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/iframe';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'printSep'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new DaftarSep;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DaftarSep']))
		{
			$model->attributes=$_POST['DaftarSep'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['DaftarSep']))
		{
			$model->attributes=$_POST['DaftarSep'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('DaftarSep');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new DaftarSep('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['DaftarSep']))
			$model->attributes=$_GET['DaftarSep'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=DaftarSep::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='daftar-sep-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
    public function insetSEP($params){
        $model = DaftarSep::model()->findByAttributes(array(
            'no_sep'=>$params['no_sep']
        ));
        if($model){
            $model->attributes = $params;
            $model->save();
        }else{
            $daftarSep = new DaftarSep;
            $daftarSep->attributes = $params;
            $daftarSep->save();
        }
    }
	
	public function actionPrintSep()
	{
		$noSep = Yii::app()->request->getParam('noSep');
		$daftarSep = DaftarSep::model()->findByAttributes(array(
			'no_sep'=>$noSep
		));
		if(!$daftarSep){
			echo "Data tidak ditemukan";
			exit;
		}
		
		$daftarPeserta = DaftarPeserta::model()->findByAttributes(array(
			'nik_peserta'=>$daftarSep['nik_peserta']
		));
		if(!$daftarPeserta){
			$sep = new SendToSep;
			$params = array(
				"nik"=>'1208115710080001'
			);
			$_result = $sep->requestFromClient($params, 'peserta_nik');
			if($_result['status'] == 'OK' ){
				$_daftarSep = DaftarPeserta::model()->findByPk($_result['record']['peserta']['nik']);
				if(!$_daftarSep){
					$peserta = new DaftarPeserta;
					$peserta->nik_peserta = $_result['record']['peserta']['nik'];
					$peserta->no_peserta = $_result['record']['peserta']['noKartu'];
					$peserta->no_rekam_medik = '';
				}else $peserta = $_daftarSep;
				$peserta->response = json_encode($_result['record']);
				$peserta->save();
			}
		}
		$response = json_decode($daftarPeserta->response, true);
		$this->render('print_sep', array(
			'daftarSep'=>$daftarSep,
			'daftarPeserta'=>$response
		));
	}
	
}
