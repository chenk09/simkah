<?php
class TriggerApiController extends Controller
{
    public $layout = false;
	
    public function actionAmbilDataBpjs()
	{
        $result = array();
        try{
            if(Yii::app()->request->isPostRequest)
            {
                $params = Yii::app()->request->getParam('no_asuransi');
                $sep = new SendToSep;
                $params = array(
                    'Peserta'=>$params
                );
                $result = $sep->requestFromClient($params, 'peserta_kartu');
            }else{
                throw new Exception('Requets not valid', 403);
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
	}
    
    public function actionAmbilDataBpjsByNik()
    {
        $result = array();
        try{
            if(Yii::app()->request->isPostRequest)
            {
                $params = Yii::app()->request->getParam('nik');
                $sep = new SendToSep;
                $params = array(
                    'nik'=>$params
                );
                $result = $sep->requestFromClient($params, 'peserta_nik');
            }else{
                throw new Exception('Requets not valid', 403);
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionAmbilDataFormSep()
    {
        $result = array();
        try{
            $mapDiagnosa = MapDiagnosa::model()->findAll();
            foreach($mapDiagnosa as $key=>$val){
                $result['diagnosa'][$val->kodeDiagnosa] = array(
                    'value'=>$val->kodeDiagnosa,
                    'text'=>$val->namaDiagnosa
                );
            }
            
            $mapPoli = MapPoli::model()->findAll();
            foreach($mapPoli as $key=>$val){
                $result['poli'][$val->kdPoli] = array(
                    'value'=>$val->kdPoli,
                    'text'=>$val->nmPoli
                );
            }
            
            $mapParams = MapParams::model()->findAll();
            foreach($mapParams as $key=>$val){
                $result[$val->jenis][$val->value] = array(
                    'value'=>$val->value,
                    'text'=>$val->code
                );
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionAmbilRujukan()
    {
        $result = array();
        try{
            if(Yii::app()->request->isPostRequest)
            {
                $params = Yii::app()->request->getParam('no_rujukan');
                $sep = new SendToSep;
                $params = array(
                    'Rujukan'=>$params
                );
                $result = $sep->requestFromClient($params, 'rujukan_no_pcare');
            }else{
                throw new Exception('Requets not valid', 403);
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionAmbilRujukanByNoKartu()
    {
        $result = array();
        try{
            if(Yii::app()->request->isPostRequest)
            {
                $params = Yii::app()->request->getParam('no_asuransi');
                $sep = new SendToSep;
                $params = array(
                    'Peserta'=>$params
                );
                $result = $sep->requestFromClient($params, 'rujukan_kartu_pcare');
            }else{
                throw new Exception('Requets not valid', 403);
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionCreateSep()
    {
        $result = array();
        try{
            if(Yii::app()->request->isPostRequest)
            {
                $par = array();
                foreach($_POST as $key=>$val){
                    $par[$key] = $val;
                }
                $par['tglSep'] = date('Y-m-d H:i:s');
                $par['user'] = Yii::app()->user->getState('nama_pegawai');
                $par['noMr'] = 'RM909090';
                $sep = new SendToSep;
                $params = array(
                    "request"=>array(
                        "t_sep"=>$par
                    )
                );
                $result = $sep->requestFromClient(json_encode($params), 'insert_sep');
                //$result = json_decode('{"code": "200", "status": "OK", "record": "0301R00105160000569" }', true);
                if($result['status'] == 'OK' ){
                    $is_exist = DaftarSep::model()->findByAttributes(array(
                        'no_sep' => $result['record']
                    ));
                    if(!$is_exist){
                        $new_sep = new DaftarSep;
                        $new_sep->id = hash('md5', $result['record'].date('YmdHis'));
                        $new_sep->no_sep = $result['record'];
                        $new_sep->no_peserta = $par['noKartu'];
                        $new_sep->response = json_encode($params);
                        foreach($params['request']['t_sep'] as $key => $value){
                            if(isset($new_sep->$key)){
                                $new_sep->$key = $value;
                            }
                        }
                        $new_sep->save();
                        
                        $sep = new SendToSep;
                        $params = array(
                            "nik"=>$par['nik']
                        );
                        $_result = $sep->requestFromClient($params, 'peserta_nik');
                        if($_result['status'] == 'OK' ){
            				$_daftarSep = DaftarPeserta::model()->findByPk($par['nik']);
            				if(!$_daftarSep){
            					$_daftarSep = new DaftarPeserta;
            					$_daftarSep->nik_peserta = $par['nik'];
            					$_daftarSep->no_peserta = $par['noKartu'];
            				}
            				$_daftarSep->response = json_encode($_result['record']);
            				$_daftarSep->save();
                        }
                    }else{
                        $is_exist->response = json_encode($params);
                        $is_exist->save();
                    }
                }
            }else{
                throw new Exception('Requets not valid', 403);
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionTestingReq()
    {
        $result = array();
        try{
            $params = Yii::app()->request->getParam('no_asuransi');
            $sep = new SendToSep;
            $params = array(
                'Peserta'=>'0001523523903'
            );
            $result = $sep->requestFromClient($params, 'rujukan_kartu_pcare');
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionDaftarPoli()
    {
        $result = array();
        try{
            $sep = new SendToSep;
            $params = array();
            $result = $sep->requestFromClient($params, 'cari_poli');
            $list = $result['record']['list'];
            foreach($list as $key=>$val){
                $mapPoli = MapPoli::model()->findByPk($val['kdPoli']);
                if(!$mapPoli){
                    $_mapPoli = new MapPoli;
                    $_mapPoli->attributes = $val;
                    $_mapPoli->save();
                }
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionCariDiagnosa()
    {
        $result = array();
        try{
            $sep = new SendToSep;
            $diagAwal = Yii::app()->request->getParam('diagAwal');
            $diagAwalNama = Yii::app()->request->getParam('diagAwalNama');
            $diagnosa = $diagAwal;
            if(strlen($diagAwalNama) > 0){
                $diagnosa = $diagAwalNama;
            }
            $params = array(
                'diagnosa'=>$diagnosa
            );
            $result = $sep->requestFromClient($params, 'cari_diagnosa');
            if(isset($result['record']['list']) && count($result['record']['list']) > 0){
                foreach($result['record']['list'] as $key => $value){
                    $mapDiagnosa = MapDiagnosa::model()->findByPk($value['kodeDiagnosa']);
                    if(!$mapDiagnosa){
                        $_mapDiagnosa = new MapDiagnosa;
                        $_mapDiagnosa->attributes = $value;
                        $_mapDiagnosa->save();
                    }
                }
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionCariFaskes()
    {
        $result = array();
        try{
            $sep = new SendToSep;
            $ppkRujukan = Yii::app()->request->getParam('ppkRujukan');
            $ppkRujukanNama = Yii::app()->request->getParam('ppkRujukanNama');
            $diagnosa = $ppkRujukan;
            if(strlen($ppkRujukanNama) > 0){
                $diagnosa = $ppkRujukanNama;
            }
            $params = array(
                'provider'=>"query?nama=$diagnosa&start=0&limit=10"
            );
            $result = $sep->requestFromClient($params, 'cari_faskes');
            if(isset($result['record']['list']) && count($result['record']['list']) > 0){
                foreach($result['record']['list'] as $key => $value){
                    $mapFaskes = MapFaskes::model()->findByAttributes(array(
                        'kdProvider'=>$value['kdProvider']
                    ));
                    if(!$mapFaskes){
                        $_mapFaskes = new MapFaskes;
                        $_mapFaskes->attributes = $value;
                        $_mapFaskes->id = md5($value['kdProvider'] . date('His'));
                        $_mapFaskes->save();
                    }
                }
            }
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
    public function actionCetakSep()
    {
        $result = array();
        try{
            $result['code'] = 0;
            $sep = new SendToSep;
            $noSep = Yii::app()->request->getParam('noSep');
            $params = array(
                'noSep'=>$noSep
            );
            $result = $sep->requestFromClient($params, 'det_sep');
            $uname = $sep->requestFromClient($params, 'uname');
            
            $cetakSep = new CetakSep;
            $cetakSep->no_sep = $noSep;
            $cetakSep->created = $uname;
            $cetakSep->create_time = date('Y-m-d H:i:s', time());
            $cetakSep->save();
            
            $result['uri'] = Yii::app()->createUrl('bpjs/daftarSep/printSep', array('noSep'=>$noSep));
        }catch(Exception $exc){
            $result['code'] = $exc->getCode();
            $result['message'] = $exc->getMessage();
        }
        echo json_encode($result);
        Yii::app()->end();
    }
    
}