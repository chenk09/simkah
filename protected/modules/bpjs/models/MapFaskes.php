<?php

/**
 * This is the model class for table "bpjs.map_faskes".
 *
 * The followings are the available columns in table 'bpjs.map_faskes':
 * @property string $kdCabang
 * @property string $kdProvider
 * @property string $nmCabang
 * @property string $nmProvider
 * @property integer $id
 */
class MapFaskes extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapFaskes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs.map_faskes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kdCabang, kdProvider, id', 'required'),
			array('kdCabang', 'length', 'max'=>10),
			array('kdProvider', 'length', 'max'=>20),
			array('nmCabang, nmProvider', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('kdCabang, kdProvider, nmCabang, nmProvider, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kdCabang' => 'Kd Cabang',
			'kdProvider' => 'Kd Provider',
			'nmCabang' => 'Nm Cabang',
			'nmProvider' => 'Nm Provider',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kdCabang',$this->kdCabang,true);
		$criteria->compare('kdProvider',$this->kdProvider,true);
		$criteria->compare('nmCabang',$this->nmCabang,true);
		$criteria->compare('nmProvider',$this->nmProvider,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}