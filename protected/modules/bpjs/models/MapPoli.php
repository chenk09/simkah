<?php

/**
 * This is the model class for table "bpjs.map_poli".
 *
 * The followings are the available columns in table 'bpjs.map_poli':
 * @property string $kdPoli
 * @property string $nmPoli
 */
class MapPoli extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MapPoli the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs.map_poli';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('kdPoli', 'required'),
			array('kdPoli', 'length', 'max'=>32),
			array('nmPoli', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('kdPoli, nmPoli', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'kdPoli' => 'Kd Poli',
			'nmPoli' => 'Nm Poli',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('kdPoli',$this->kdPoli,true);
		$criteria->compare('nmPoli',$this->nmPoli,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}