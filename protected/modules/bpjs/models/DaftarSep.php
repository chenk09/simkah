<?php

/**
 * This is the model class for table "bpjs.daftar_sep".
 *
 * The followings are the available columns in table 'bpjs.daftar_sep':
 * @property string $id
 * @property string $no_sep
 * @property string $no_rekam_medik
 * @property string $no_pendaftaran
 * @property string $nik_peserta
 * @property string $no_peserta
 * @property string $response
 * @property string $create_time
 * @property string $created
 * @property string $noKartu
 * @property string $noRujukan
 * @property string $tglRujukan
 * @property string $ppkRujukan
 * @property string $ppkPelayanan
 * @property string $diagAwal
 * @property string $poliTujuan
 * @property integer $jnsPelayanan
 * @property integer $klsRawat
 * @property integer $lakaLantas
 * @property string $lokasiLaka
 * @property string $catatan
 * @property string $tglSep
 * @property string $noMr
 */
class DaftarSep extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DaftarSep the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs.daftar_sep';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id', 'required'),
			array('jnsPelayanan, klsRawat, lakaLantas', 'numerical', 'integerOnly'=>true),
			array('id, no_sep, no_rekam_medik, no_pendaftaran, ppkRujukan, ppkPelayanan, noMr', 'length', 'max'=>32),
			array('nik_peserta, no_peserta, created', 'length', 'max'=>20),
			array('create_time', 'length', 'max'=>6),
			array('noKartu, noRujukan', 'length', 'max'=>64),
			array('diagAwal, poliTujuan', 'length', 'max'=>15),
			array('lokasiLaka', 'length', 'max'=>128),
			array('response, tglRujukan, catatan, tglSep', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, no_sep, no_rekam_medik, no_pendaftaran, nik_peserta, no_peserta, response, create_time, created, noKartu, noRujukan, tglRujukan, ppkRujukan, ppkPelayanan, diagAwal, poliTujuan, jnsPelayanan, klsRawat, lakaLantas, lokasiLaka, catatan, tglSep, noMr', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'no_sep' => 'No Sep',
			'no_rekam_medik' => 'No Rekam Medik',
			'no_pendaftaran' => 'No Pendaftaran',
			'nik_peserta' => 'Nik Peserta',
			'no_peserta' => 'No Peserta',
			'response' => 'Response',
			'create_time' => 'Create Time',
			'created' => 'Created',
			'noKartu' => 'No Kartu',
			'noRujukan' => 'No Rujukan',
			'tglRujukan' => 'Tgl Rujukan',
			'ppkRujukan' => 'Ppk Rujukan',
			'ppkPelayanan' => 'Ppk Pelayanan',
			'diagAwal' => 'Diag Awal',
			'poliTujuan' => 'Poli Tujuan',
			'jnsPelayanan' => 'Jns Pelayanan',
			'klsRawat' => 'Kls Rawat',
			'lakaLantas' => 'Laka Lantas',
			'lokasiLaka' => 'Lokasi Laka',
			'catatan' => 'Catatan',
			'tglSep' => 'Tgl Sep',
			'noMr' => 'No Mr',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('no_sep',$this->no_sep,true);
		$criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
		$criteria->compare('no_pendaftaran',$this->no_pendaftaran,true);
		$criteria->compare('nik_peserta',$this->nik_peserta,true);
		$criteria->compare('no_peserta',$this->no_peserta,true);
		$criteria->compare('response',$this->response,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('noKartu',$this->noKartu,true);
		$criteria->compare('noRujukan',$this->noRujukan,true);
		$criteria->compare('tglRujukan',$this->tglRujukan,true);
		$criteria->compare('ppkRujukan',$this->ppkRujukan,true);
		$criteria->compare('ppkPelayanan',$this->ppkPelayanan,true);
		$criteria->compare('diagAwal',$this->diagAwal,true);
		$criteria->compare('poliTujuan',$this->poliTujuan,true);
		$criteria->compare('jnsPelayanan',$this->jnsPelayanan);
		$criteria->compare('klsRawat',$this->klsRawat);
		$criteria->compare('lakaLantas',$this->lakaLantas);
		$criteria->compare('lokasiLaka',$this->lokasiLaka,true);
		$criteria->compare('catatan',$this->catatan,true);
		$criteria->compare('tglSep',$this->tglSep,true);
		$criteria->compare('noMr',$this->noMr,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}