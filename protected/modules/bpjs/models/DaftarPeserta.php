<?php

/**
 * This is the model class for table "bpjs.daftar_peserta".
 *
 * The followings are the available columns in table 'bpjs.daftar_peserta':
 * @property string $nik_peserta
 * @property string $no_peserta
 * @property string $no_rekam_medik
 * @property string $response
 * @property string $created
 * @property string $create_time
 * @property string $updated
 * @property string $update_time
 */
class DaftarPeserta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DaftarPeserta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'bpjs.daftar_peserta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik_peserta', 'required'),
			array('nik_peserta, no_peserta', 'length', 'max'=>20),
			array('no_rekam_medik, created, updated', 'length', 'max'=>32),
			array('response, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nik_peserta, no_peserta, no_rekam_medik, response, created, create_time, updated, update_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nik_peserta' => 'Nik Peserta',
			'no_peserta' => 'No Peserta',
			'no_rekam_medik' => 'No Rekam Medik',
			'response' => 'Response',
			'created' => 'Created',
			'create_time' => 'Create Time',
			'updated' => 'Updated',
			'update_time' => 'Update Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nik_peserta',$this->nik_peserta,true);
		$criteria->compare('no_peserta',$this->no_peserta,true);
		$criteria->compare('no_rekam_medik',$this->no_rekam_medik,true);
		$criteria->compare('response',$this->response,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('updated',$this->updated,true);
		$criteria->compare('update_time',$this->update_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}