<?php

/**
 * This is the model class for table "antrian_t".
 *
 * The followings are the available columns in table 'antrian_t':
 * @property integer $antrian_id
 * @property integer $pendaftaran_id
 * @property integer $profilrs_id
 * @property integer $ruangan_id
 * @property string $tglantrian
 * @property string $noantrian
 * @property string $statuspasien
 * @property string $loket_antrian
 */
class ANAntrianT extends AntrianT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AntrianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}