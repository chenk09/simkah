<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.sound/jquery.jplayer.min.js'); ?>
<script type="text/javascript">
/**
 * set semua antrian 
 * @param {type} antrian_id
 * @returns {undefined} */

    
function loadDataPanggilAntrian(pendaftaran_id){
    var temp_lokasiantrianklinik_id = $("#lokasiantrianklinik_id").val();
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('AmbilAntrian'); ?>',
        data: {pendaftaran_id:pendaftaran_id}, //
        dataType: "json",
        success: function (data) {
            $("#ruangan_"+data.model.ruangan_id).find(".dokter").html("<span>"+data.dokter+"</span>");
            $("#ruangan_"+data.model.ruangan_id).find(".no-antrian").html(data.model.no_urutantri);
            $("#ruangan_"+data.model.ruangan_id).find(".pasien-deskripsi").html("<span>RM : "+data.RM+"<span>");
            if(data.lokasiantrianklinik_id == temp_lokasiantrianklinik_id){
                panggilAntrian(data.lokasiantrianklinik_id,pendaftaran_id);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
}

function panggilAntrian(lokasiantrianklinik_id,pendaftaran_id){
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('PanggilAntrian'); ?>',
        data: {pendaftaran_id:pendaftaran_id,lokasiantrianklinik_id:lokasiantrianklinik_id},
        dataType: "json",
        success: function (data) {
            $("#suarapanggilan").html(data.suarapanggilan);
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
}

$( document ).ready(function(){


    var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
    if (chatServer == ''){
     chatServer='http://localhost';
    }
    var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
    socket = io.connect(chatServer+':'+chatPort);
    socket.emit('subscribe', 'antrianPoli');
    socket.on('antrianPoli', function(data){
        loadDataPanggilAntrian(data.pendaftaran_id);
    });

});    
</script>