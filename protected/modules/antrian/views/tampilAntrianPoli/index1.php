<style>
    body{
        background-repeat:no-repeat;
        width:100%;
        height:100%;
        color:#000;
        margin-left:10px;
    }
    div{
        font-size: 20px;
        font-weight:bold;
        letter-spacing:2px;
        color: #fff;
        text-shadow:
            -1px -1px 0 #000,  
             1px -1px 0 #000,
             -1px 1px 0 #000,
              1px 1px 0 #000;
    }
    td{
        text-align: right;
        padding-right: 20px;
    }
    .content {
        /*margin: 0;*/
        margin: 146px 20px 20px 20px;
    }
    .judul{
        text-align: center;
        font-size: 35px;
        font-weight: bold;
        padding-bottom: 0px;
    }
    .ruangan,.dokter{
        color: #FFFF00;
        /*width: <?php // echo $modLayar->layarantrian_itemwidth; ?>;*/
        height: 29px;
        /*font-size: 85%;*/
        overflow: hidden;
        text-align: center;
        background-color:rgba(0,0,0,1);
    }
    .dokter{
        /*font-size: 70%;*/
        color: #00FF00;
    }
    .no-antrian, .pasien-deskripsi{
        color:#fff;
        text-align: center;
        font-size: 75px;
        font-weight: bold;
        background-color:rgba(0,0,0,0.5);
        text-shadow:
            -2px -2px 0 #000,  
             1px -2px 0 #000,
             -2px 2px 0 #000,
              2px 2px 0 #000;
    }

    .statistik{
        color:#fff;
        text-shadow:
            -1px -1px 0 #000,  
             1px -1px 0 #000,
             -1px 1px 0 #000,
              1px 1px 0 #000;
        background-color:rgba(0,0,0,0.8);
    }
    .pasien-deskripsi span {
        font-size: 24px !important;
    }
</style>
<div class="row-fluid judul"><?php echo strtoupper("Antrian ".$modLayar->lokasiantrianklinik_nama); ?></div><br><br>
    <?php 
    if(count($modAntrian) > 0){
        foreach($modAntrian AS $i=>$ruangan){
            $criteria = new CDbCriteria;
            $criteria->addCondition("panggilantrian IS TRUE");
            $criteria->addBetweenCondition("DATE(tgl_pendaftaran)", date('Y-m-d'), date('Y-m-d'));
            $criteria->addCondition('ruangan_id = '.$ruangan->ruangan_id);
            $criteria->order = "no_urutantri DESC";
            $model = PendaftaranT::model()->find($criteria);
        
            if(($i==0)||($i) % 3 == 0){
                echo '<div class="row-fluid">';
            }    ?>
            <div class="span4">
                <input type="hidden" id="lokasiantrianklinik_id" value="<?php echo $_GET['lokasiantrianklinik_id'];?>">
                <div id="ruangan_<?php echo $ruangan->ruangan_id; ?>" class="antrian" style="width:395px;height:217px;">
                    <div class="ruangan" id="ruangan_<?php echo $i; ?>">
                        <span><?php echo strtoupper($ruangan->ruangan->ruangan_nama); ?></span>
                    </div>
                    <div class="dokter" id="dokter_<?php echo $i; ?>">
                        <span><?php echo (isset($model->pegawai_id)&&!empty($model->pegawai_id))? $model->pegawai->gelardepan." ".$model->pegawai->nama_pegawai: "Nama Dokter"; ?></span>
                    </div>
                    <div class="no-antrian">
                        <?php echo (isset($model->no_urutantri)&&!empty($model->no_urutantri))? $model->no_urutantri : "000"; ?>
                    </div>
                    <div class="pasien-deskripsi" id="pasien-deskripsi_<?php echo $i; ?>">
                        <span><?php echo (isset($model->pasien_id)&&!empty($model->pasien_id))? "RM : ".$model->pasien->no_rekam_medik : "No Rekam Medis"; ?></span>
                    </div>
                    <br>
                </div>
            </div>
    <?php
            if(($i+1>0)&&(($i+1) % 3 == 0)){
                echo '</div>';
            }
        }
    } 
    ?>
<input id="waktupanggil" type="hidden">
<?php echo $this->renderPartial('_jsFunctions'); ?>
<div id="suarapanggilan" ></div>