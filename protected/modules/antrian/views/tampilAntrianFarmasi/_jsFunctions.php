<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.sound/jquery.jplayer.min.js'); ?>
<script type="text/javascript">
/**
 * set semua antrian 
 * @param {type} antrian_id
 * @returns {undefined} */

    
function loadDataPanggilAntrian(lokasiantrian_id,jenisantrian_id,antrianpasienfarmasi_id,nourutantrian){
    $("#Lt"+lokasiantrian_id+" #loket_"+lokasiantrian_id+" .no-antrian").html((nourutantrian).toUpperCase());
    panggilAntrian(antrianpasienfarmasi_id,lokasiantrian_id);
}

function panggilAntrian(antrianpasienfarmasi_id,lokasiantrian_id){
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('PanggilAntrian'); ?>',
        data: {antrianpasienfarmasi_id:antrianpasienfarmasi_id},
        dataType: "json",
        success: function (data) {
            if(lokasiantrian_id == 3){
                $("#suarapanggilanLt2").html(data.suarapanggilan);
            }else{
                $("#suarapanggilanLt5").html(data.suarapanggilan);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
}

$( document ).ready(function(){


    var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
    if (chatServer == ''){
     chatServer='http://localhost';
    }
    var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
    socket = io.connect(chatServer+':'+chatPort);
    socket.emit('subscribe', 'antrianFarmasi');
    socket.on('antrianFarmasi', function(data){
        loadDataPanggilAntrian(data.lokasiantrian_id,data.jenisantrian_id,data.antrianpasienfarmasi_id,data.nourutantrian);
    });

});    
</script>