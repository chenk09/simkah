
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'antrian-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
)); ?>
<?php echo CHtml::hiddenField('loket'); ?>
<?php echo CHtml::hiddenField('idCaraBayar'); ?>
<?php echo CHtml::hiddenField('noUrut'); ?>  
<?php echo CHtml::hiddenField('statusPasien'); ?>
<?php echo CHtml::hiddenField('caraBayarLoket'); ?>

<?php echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>'')); ?>
                <div id="antrian"> 
<!--                    <div class="pasienLamaBaru">-->

                                     <?php 
//                                            echo CHtml::htmlButton('<h1>PASIEN LAMA</h1>',array('class'=>'btn','style'=>'height:50%; width:100%; font-size: 40px;padding:.4em;',
//                                                    'id'=>'pasienLama','onclick'=>'ubahWarnaClick(this,\'PASIEN LAMA\')',
//                                                    'onMouseOver'=>'ubahWarna(this)',
//                                                    'onmouseout'=>'kembaliWarna(this)')); ?>

                                     <?php 
//                                            echo CHtml::htmlButton('<h1>PASIEN BARU</h1>',array('class'=>'btn','style'=>'height:50%; width:100%;font-size: 40px;',
//                                                      'id'=>'pasienBaru','onclick'=>'ubahWarnaClick(this,\'PASIEN BARU\')',
//                                                      'onMouseOver'=>'ubahWarna(this)',
//                                                      'onmouseout'=>'kembaliWarna(this)')); ?>

<!--                    </div>-->

                    <div class="groupNoAntrian">
                           <?php echo $tableNoAntri ?>
                   </div>          


<?php $this->endWidget(); ?>

<?php

$js = <<< JSCRIPT
//============================Pasien Lama Pasien Baru===========================
function ubahWarna(obj)   
{ 
    $(obj).attr("class","btn btn-success");
}


function kembaliWarna(obj)
{
   $(obj).attr("class","btn");
}


function ubahWarnaClick(obj,statusPasien)   
{
  if(statusPasien=='PASIEN BARU')
    {
        $(obj).attr("class","btn btn-success");
        $(obj).removeAttr('onmouseout');
        $('#pasienLama').attr("class","btn");
        $('#pasienLama').attr("onmouseout", "kembaliWarna(this);");
        $('#statusPasien').val(statusPasien);


     }
  else
    {
        $(obj).attr("class","btn btn-success");
        $(obj).removeAttr('onmouseout');
        $('#pasienBaru').attr("class","btn");
        $('#pasienBaru').attr("onmouseout", "kembaliWarna(this);");
        $('#statusPasien').val(statusPasien);


    }
}

//==============================Akhir Pasien Lama Pasien Baru===================

function ubahWarnaAntrian(obj)   
{ 
    $(obj).attr("class","btn btn-danger");
}

function kembaliWarnaAntrian(obj)
{
   $(obj).attr("class","btn btn-primary");
}
 
function simpan(loket,idCaraBayar,noUrut,caraBayarLoket)
{
   $('#loket').val(loket);
   $('#idCaraBayar').val(idCaraBayar);
   $('#noUrut').val(noUrut);
   $('#caraBayarLoket').val(caraBayarLoket);
  
   $('#antrian-form').submit();
     
}
JSCRIPT;
Yii::app()->clientScript->registerScript('cekStatusPasien',$js, CClientScript::POS_HEAD);
?>


