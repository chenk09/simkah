<iframe src="<?=$url; ?>" style="height:0px;">
</iframe>
<div id="container" style="height:100%;width:98%;background-size:cover;background-color: transparent;">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'antrian-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
)); ?> 
    <div id="headerAntrian"></div>
    <div id="contentAntrian" style="height:500px;width:99%;background-size:cover;">
     <div class="content">

        <?php echo CHtml::hiddenField('loket'); ?>
        <?php echo CHtml::hiddenField('idCaraBayar'); ?>
        <?php echo CHtml::hiddenField('noUrut'); ?>  
        <?php echo CHtml::hiddenField('statusPasien'); ?>
        <?php echo CHtml::hiddenField('caraBayarLoket'); ?>


<?php echo $tableNoAntri ?>

<?php $this->endWidget(); ?>

     </div>
    </div>
    <div id="footerAntrian">
        <marquee direction="left" scrollamount="10" id="textrunning" style="margin-top: 95px;">
            <?php echo Yii::app()->user->getState('running_text_kiosk'); ?>
        </marquee>
    </div>
    <div id="footerClock"><div id="clock" style="margin: 60px 0 0 36px;"></div></div>
</div>
    

<script type="text/javascript">
  setInterval(   // fungsi untuk menjalankan suatu fungsi berdasarkan waktu
    function(){
        $.post('<?php echo $this->createUrl('getRunningText') ?>',{},function(data){
            $('#textrunning').html(data);
        },'json');
     return false;
 }, 
 10000  // fungsi di eksekusi setiap 10 detik sekali
);
</script>

<?php

$js = <<< JSCRIPT
//============================Pasien Lama Pasien Baru===========================
function ubahWarna(obj)   
{ 
    $(obj).attr("class","btn btn-success");
}


function kembaliWarna(obj)
{
   $(obj).attr("class","btn");
}


function ubahWarnaClick(obj,statusPasien)   
{
  if(statusPasien=='PASIEN BARU')
    {
        $(obj).attr("class","btn btn-success");
        $(obj).removeAttr('onmouseout');
        $('#pasienLama').attr("class","btn");
        $('#pasienLama').attr("onmouseout", "kembaliWarna(this);");
        $('#statusPasien').val(statusPasien);


     }
  else
    {
        $(obj).attr("class","btn btn-success");
        $(obj).removeAttr('onmouseout');
        $('#pasienBaru').attr("class","btn");
        $('#pasienBaru').attr("onmouseout", "kembaliWarna(this);");
        $('#statusPasien').val(statusPasien);


    }
}

//==============================Akhir Pasien Lama Pasien Baru===================

function ubahWarnaAntrian(obj)   
{ 
    $(obj).attr("class","btn btn-danger");
}

function kembaliWarnaAntrian(obj)
{
   $(obj).attr("class","btn btn-primary");
}
 
function simpan(loket,idCaraBayar,noUrut,caraBayarLoket)
{
   $('#loket').val(loket);
   $('#idCaraBayar').val(idCaraBayar);
   $('#noUrut').val(noUrut);
   $('#caraBayarLoket').val(caraBayarLoket);
  
   $('#antrian-form').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('cekStatusPasien',$js, CClientScript::POS_HEAD);
?>
