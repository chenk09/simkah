<div style='width:585px; margin-top:15px;margin-left:auto;margin-right:auto; padding: 3px 0 0 0;background:transparent;'>
<style>
    thead th {background:transparent;}
</style>

<fieldset>
<!--        <legend><center>Antrian Poliklinik</center></legend>-->
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'andisplayantrianpasien-v-grid',
    'dataProvider'=>$model->searchTable(),
//  'filter'=>$model,
    'template'=>"{items}",
    'itemsCssClass'=>'table table-condensed',
    'columns'=>array(
//      'pasien_id',
        ////'pendaftaran_id',
//        array(
//                        'name'=>'pendaftaran_id',
//                        'value'=>'$data->pendaftaran_id',
//                        'filter'=>false,
//                ),
        'no_urutantri',
        // array(
        //     'header'=>'Tanggal Pendaftaran',
        //     'value'=>'$data->tanggalDaftar',
        //     ),
        array(
            'header'=>'Jam Pendaftaran',
            'value'=>'$data->jamDaftar',
            ),
        'no_rekam_medik',
        'nama_pasien',
        'jeniskelamin',
        'no_pendaftaran',
        'ruangan_nama',
        'statusperiksa',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
</fieldset>
</div>

<?php 
Yii::app()->clientScript->registerScript('reloadTable',"
    function reloadTable(){
        // $.fn.yiiGridView.update('andisplayantrianpasien-v-grid', {}); 
    }
    ",  CClientScript::POS_HEAD);

Yii::app()->clientScript->registerScript('interval',"
    setInterval('reloadTable()', 1000);
    ",  CClientScript::POS_READY); 
?>