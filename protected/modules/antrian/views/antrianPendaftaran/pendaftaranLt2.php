<style>
    button.btn-tiket {
        width: auto;
        height:100px;
        /*background:	url("images/antrian/button a tanpa text.png") no-repeat;*/
        background-color: #1a92c2; background-image: -webkit-gradient(linear, left top, left bottom, from(#1a92c2), to(#1a92c2));
        background-image: -webkit-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -moz-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -ms-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -o-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: linear-gradient(to bottom, #1a92c2, #1a92c2);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#ffaf46, endColorstr=#1a92c2);
        background-size: 100% 100%;
        border:none;
        /*margin: 256px -15px 0px 28px;*/
        vertical-align: top;
        font-family: Arial, Helvetica, sans-serif;
        color:white;
        font-size:38px;
        letter-spacing:0px;
        font-weight: bold;
        text-shadow: 2px 2px 6px #000000;
        padding: 0px 100px;
        display: inline-block;
        border-radius: 20px;
    }
    button.btn-tiket:hover{
        /*background:	url("images/antrian/button a tanpa text (hover).png") no-repeat;*/
        background-size: 100% 100%;
        background-color: #778899; background-image: -webkit-gradient(linear, left top, left bottom, from(#1ab0ec), to(#778899));
        background-image: -webkit-linear-gradient(top, #778899, #778899);
        background-image: -moz-linear-gradient(top, #778899, #778899);
        background-image: -ms-linear-gradient(top, #778899, #778899);
        background-image: -o-linear-gradient(top, #778899, #778899);
        background-image: linear-gradient(to bottom, #778899, #778899);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#1ab0ec, endColorstr=#778899);
    }
    .pelayanan{
        /*width:1800px;*/
        /*height:150px;*/
        border:none;
        /*margin: 32px 0px 0px 70px;*/
        /*padding: 0px 0px 0px 420px;*/
        /*vertical-align: top;*/
        font-size:25px;
        /*text-indent: 10px;*/
        text-align: left;
        font-weight: bold;
        color: white;
        text-shadow: 1px 1px 1px #000;
    }
    #footerAntrian{
        width:90%;
    }
    #footerClock{
        width:10%;
    }
    button.disabled{
        background:url("images/process-working.gif") no-repeat !important;
        background-position: top center !important;
    }
</style>
<h1 style="text-align:center;margin-top: 20px;margin-bottom: 20px;color: white"><?php echo $lokAntrian->lokasiantrian_nama ?></h1>
<div class="row-fluid" style="margin-left:20px">
    <?php 
        $jum = count($modLokAntrian);
        if($jum == 1){
            $class = "span12";
        }else if($jum == 2){
            $class = "span6";
        }else{
            $class = "span4";
        }
        foreach ($modLokAntrian as $value) { 
    ?>
        <div class = "<?php echo $class; ?>">
            <button id="btn-antrian-b" class="btn-tiket" type="button" name="yt0" onclick="simpan(this,<?php echo $lokAntrian->lokasiantrian_id; ?>, <?php echo $value->jenisantrian_id; ?>)"><?php echo $value->jenisantrian_nama; ?></button>
            <?php
            $modPelayanan = JenislayananlokasiantrianV::model()->findAllByAttributes(array('lokasiantrian_id' => 1, 'jenisantrian_id' => $value->jenisantrian_id));
            foreach ($modPelayanan as $layanan) {
                ?>
                <div class="pelayanan">
                    &nbsp;&nbsp;<?php echo $layanan->layananantrian_nama ?>
                </div>
                <?php
            }
            ?>
        </div>
    <?php } ?>
</div>
<div class="block-footer-antrian" style="position:fixed;bottom:0">
    <div id="footerAntrian" style="color:black">
        <marquee direction="left" scrollamount="10" id="textrunning">
            <?php 
            $konfig = KonfigsystemK::model()->findByPk(1);
            echo $konfig->running_text; 
            ?>
        </marquee>
    </div> 
    <div id="footerClock">
        <div id="clock"></div>
    </div>
</div>
<iframe id="print_win" src="" style="display: none;"></iframe>

<script type="text/javascript">
    function simpan(obj, lokasiantrian_id, jenisantrian_id) {
        //salin ke form
        $("button").attr("disabled");
//        $("button").addClass("disabled");
        if (!$(obj).hasClass("disabled")) {
            $("button").attr("disabled");
            $("button").addClass("disabled");
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('SimpanTiket'); ?>',
                data: {lokasiantrian_id:lokasiantrian_id,jenisantrian_id:jenisantrian_id}, //
                dataType: "json",
                success: function (data) {
                    var delaytombol = parseInt(data.delaytombol) * parseInt(1000);
                    print(data.model.antrianpasien_id);
                    setTimeout(function () {
                            $("button").removeAttr("disabled");
                            $("button").removeClass("disabled");
                    }, delaytombol);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                }
            });
        }
    }
    function print(antrian_id) {
        $("#print_win").attr('src', "<?php echo $this->createUrl('Print') ?>&antrian_id=" + antrian_id);
    }
</script>
