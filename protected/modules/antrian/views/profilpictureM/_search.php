<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'anprofilpicture-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'profilpicture_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'profilrs_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'profilpicture_tgl',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'profilpicture_nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php //echo $form->textAreaRow($model,'profilpicture_desc',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkBoxRow($model,'display_antrian'); ?>

	<?php //echo $form->textAreaRow($model,'profilpicture_path',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
