<?php
$this->breadcrumbs=array(
	'Anprofilpicture Ms'=>array('index'),
	$model->profilpicture_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Profil Picture #'.$model->profilpicture_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' ANProfilpictureM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' ANProfilpictureM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' ANProfilpictureM', 'icon'=>'pencil','url'=>array('update','id'=>$model->profilpicture_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' ANProfilpictureM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->profilpicture_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Profil Picture', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'profilpicture_id',
		'profilrs_id',
		'profilpicture_tgl',
		'profilpicture_nama',
		'profilpicture_desc',
		'display_antrian',
		'profilpicture_path',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>