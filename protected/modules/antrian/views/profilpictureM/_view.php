<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilpicture_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->profilpicture_id),array('view','id'=>$data->profilpicture_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilrs_id')); ?>:</b>
	<?php echo CHtml::encode($data->profilrs_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilpicture_tgl')); ?>:</b>
	<?php echo CHtml::encode($data->profilpicture_tgl); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilpicture_nama')); ?>:</b>
	<?php echo CHtml::encode($data->profilpicture_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilpicture_desc')); ?>:</b>
	<?php echo CHtml::encode($data->profilpicture_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('display_antrian')); ?>:</b>
	<?php echo CHtml::encode($data->display_antrian); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profilpicture_path')); ?>:</b>
	<?php echo CHtml::encode($data->profilpicture_path); ?>
	<br />


</div>