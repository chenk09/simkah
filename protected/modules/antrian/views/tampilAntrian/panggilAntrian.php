<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'panggilAntrian-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
)); ?>
<h1><font color="white">Panggil Antrian</font></h1></br>
    <div class="control-group">
        <font color="white">
        <?php echo '<font color ="white">Loket</font>' ?>
        <div class="controls">
            <?php echo CHtml::dropDownList('carabayar_loket', $carabayar_id, CHtml::listData($cara_bayar, 'carabayar_id', 'carabayar_loket'),array('onChange'=>'submit()','empty'=>'- Pilih -')) ?> 
            <?php echo CHtml::htmlButton('<h1>Pasien Selanjutnya</h1>',array(
                                                    'id'=>'pasienLama','onclick'=>'mainkan()','class'=>'btn btn-danger'
                                                    )); ?>
        </div>
        </font>
    </div>

</div>
<div id="player">
    <div id="jquery_jplayer_antrian" class="jp-jplayer"></div>
    <?php 
       if(!empty($forms))
       {
           echo $forms;
       }
    ?>
    <div id="jquery_jplayer_loket" class="jp-jplayer"></div>
    <div id="jquery_jplayer_loket_pilihan" class="jp-jplayer"></div>
</div>




<?php $this->endWidget(); ?>

<script type="text/javascript">
//<![CDATA[
function submit()
{
    document.forms["panggilAntrian-form"].submit();
}

function mainkan()
{

    
    $("#jquery_jplayer_antrian").jPlayer({
        
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/mp3/antrian.mp3",
				oga:  "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/ogg/antrian.ogg"
			}).jPlayer("play");
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_0").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, oga",
		wmode: "window",
		autoPlay : true

	});

	
}

    <?php 
        for ($f = 0; $f< $jumlah; $f++) 
        {
            $g = $f + 1;
            if($f == $jumlah - 1){$loket = 'loket';}else{$loket = $g;}
            echo  '$("#jquery_jplayer_'.$f.'").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
                           
				mp3: "'.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3",
				oga: "'.Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$noAntrianBaru[$f].'.ogg"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
                                        
					$("#jquery_jplayer_'.$loket.'").jPlayer("play", 0);
				});
			}
		},
		swfPath: "'.Yii::app()->request->baseUrl.'/js/js.sound",
		supplied: "mp3, oga",
		cssSelectorAncestor: "#jp_container_2",
		wmode: "window"
	});';
           
           
        }
    ?>


	
	
	$("#jquery_jplayer_loket").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/mp3/loket2.mp3",
				oga:  "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/ogg/loket2.ogg"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_loket_pilihan").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, oga",
		cssSelectorAncestor: "#jp_container_7",
		wmode: "window"
	});

	$("#jquery_jplayer_loket_pilihan").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo $formLoketMp3 ?>",
				oga: "<?php echo $formLoketOgg ?>"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_9").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, oga",
		cssSelectorAncestor: "#jp_container_8",
		wmode: "window"
	});
//$(document).ready(function(){
//	
//
//	
//
//	
//	
//	
//});
//]]>
</script>
