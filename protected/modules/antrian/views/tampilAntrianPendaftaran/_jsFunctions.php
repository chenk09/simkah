<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/js.sound/jquery.jplayer.min.js'); ?>
<script type="text/javascript">
/**
 * set semua antrian 
 * @param {type} antrian_id
 * @returns {undefined} */

    
function loadDataPanggilAntrian(lokasiantrian_id,jenisantrian_id,loketantrian_id,antrianpasien_id,nourutantrian){
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('AmbilAntrian'); ?>',
        data: {lokasiantrian_id:lokasiantrian_id,jenisantrian_id:jenisantrian_id,loketantrian_id:loketantrian_id,antrianpasien_id:antrianpasien_id,nourutantrian:nourutantrian}, //
        dataType: "json",
        success: function (data) {
            if(data.lokasiantrian_id==1){
                var nama = data.nourutantrian;
                $("#Lt2 #loket_"+data.loketantrian_id+" .namaAntiran").html((nama.substr(0,1)).toUpperCase());
                $("#Lt2 #loket_"+data.loketantrian_id+" .namaLoket").html((data.loketantrian_nama).toUpperCase());
                $("#Lt2 #loket_"+data.loketantrian_id+" .no-antrian").html((data.nourutantrian).toUpperCase());
                panggilAntrian(antrianpasien_id,lokasiantrian_id);
            }else{
                var nama = data.nourutantrian;
                $("#Lt5 #loket_"+data.loketantrian_id+" .namaAntiran").html((nama.substr(0,1)).toUpperCase());
                $("#Lt5 #loket_"+data.loketantrian_id+" .namaLoket").html((data.loketantrian_nama).toUpperCase());
                $("#Lt5 #loket_"+data.loketantrian_id+" .no-antrian").html((data.nourutantrian).toUpperCase());
                panggilAntrian(antrianpasien_id,lokasiantrian_id);
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
}

function panggilAntrian(antrianpasien_id,lokasiantrian_id){
    $.ajax({
        type: 'POST',
        url: '<?php echo $this->createUrl('PanggilAntrian'); ?>',
        data: {antrianpasien_id:antrianpasien_id},
        dataType: "json",
        success: function (data) {
            if(data.lokasiantrian_id == 1){
                $("#suarapanggilanLt2").append("<div id='panggilanLt2_"+antrianpasien_id+"'></div>");
                $("#panggilanLt2_"+antrianpasien_id).html(data.suarapanggilan);
            }else{
                $("#suarapanggilanLt5").append("<div id='panggilanLt5_"+antrianpasien_id+"'></div>");
                $("#panggilanLt5_"+antrianpasien_id).html(data.suarapanggilan);
            }
            duration = $("#jquery_jplayer_"+antrianpasien_id+"_diloket").data("jPlayer").status.currentTime;
            console.log(duration);
        },
        error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
        }
    });
}

$( document ).ready(function(){


    var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
    if (chatServer == ''){
     chatServer='http://localhost';
    }
    var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
    socket = io.connect(chatServer+':'+chatPort);
    socket.emit('subscribe', 'antrianPendaftaran');
    socket.on('antrianPendaftaran', function(data){
        loadDataPanggilAntrian(data.lokasiantrian_id,data.jenisantrian_id,data.loketantrian_id,data.antrianpasien_id,data.nourutantrian);
    });

});    
</script>