<style>
    body{
        background-image: url("images/antrian/bg_antrian_new.jpg");
		background-repeat: no-repeat;
		background-position: center top;
		background-size: 100% auto;
		/* width: 980px; */
		color: #000;
/*		height: 600px;
                width: 1280px;*/
    }
	.content {
		margin: 240px 20px 5px 20px;
	}
	.row-fluid{
		margin: 0;
	}
    div{
        font-size: 20px;
        font-weight:bold;
        letter-spacing:0;
        color: #fff;
        text-shadow:
            -1px -1px 0 #000,  
             1px -1px 0 #000,
             -1px 1px 0 #000,
              1px 1px 0 #000;
    }
    .span4{
        margin-bottom: 17px;
    }
    td{
        text-align: right;
        padding-right: 15px;
    }
    .judul{
        text-align: center;
        font-size: 65px;
        font-weight: bold;
        padding-bottom: 0px;
		margin: 10px 10px 5px 10px;
    }
	.loketA{
		padding-right: 11px;
	}
    .loket-nama{
        font-size: 40px;
        text-align: center;
        background-color:rgba(0,0,0,1);
    }
    .no-antrian{
        color:#fff;
        text-align: center;
        font-size: 140px;
        font-weight: bold;
        background-color:rgba(0,0,0,0.5);
        text-shadow:
            -2px -2px 0 #000,  
             1px -2px 0 #000,
             -2px 2px 0 #000,
              2px 2px 0 #000;
        /*height: 250px;*/
    }
	.box-antrian{
		margin-top: 40px;
	}
    .statistik{
        font-size: 20px;
        color:#fff;
        text-shadow:
            -1px -1px 0 #000,  
             1px -1px 0 #000,
             -1px 1px 0 #000,
              1px 1px 0 #000;
        background-color:rgba(0,0,0,0.8);
		letter-spacing: 1px;
                height: 105px;
    }
	.statistikNoData{
        text-shadow:
            -1px -1px 0 #000,  
             1px -1px 0 #000,
             -1px 1px 0 #000,
              1px 1px 0 #000;
        background-color:rgba(0,0,0,0.8);
		height: 105px;
    }
    #loket_1 .loket-nama, #loket_2 .loket-nama, #loket_3 .loket-nama{
        background-color:#009900 !important; 
    }
/*	#loket_2 .loket-nama, #loket_5 .loket-nama{
        background-color:#000099 !important; 
    }*/
	#loket_4 .loket-nama, #loket_5 .loket-nama, #loket_6 .loket-nama{
        background-color:#bb0c0c !important; 
    }
</style>
<div class="row-fluid judul">NO. ANTRIAN PENDAFTARAN LANTAI 5</div>
<?php echo CHtml::hiddenField('jamsekarang',"",array('readonly'=>true,'class'=>'realtime')) ;?>
<br><br><br><br>
<div class="row-fluid loketA">
	
    <?php
    $w=1;
    foreach ($modLokAntrian as $value) {
        
        $criteria = new CDbCriteria();
        $criteria->compare("DATE(tglantrianpasien)", date("Y-m-d"));
        $criteria->compare("DATE(tglpemanggilanpasien)", date("Y-m-d"));
        $criteria->addCondition("loketantrian_id = ".$value->loketantrian_id);
        $criteria->order = 'tglpemanggilanpasien DESC';
        $criteria->limit = 1;
        $modLokets = AntrianpasienT::model()->findAll($criteria);
        $namaAntiran = 'X';
        $noantrian = 'X000';

        foreach($modLokets as $loket => $kol){
            $namaAntiran = $kol->jenisantrian->jenisantrian_singkatan;
            $noantrian = $kol->nourutantrian;

        }
        
   ?>
            <div class="span4" id="Lt2">
                <div id="loket_<?php echo $w; ?>" class="antrian">
                    <div class="loket-nama">
                        ANTRIAN <span class="namaAntiran"><?php echo $namaAntiran; ?></span> 
                        DI 
                        <span class="namaLoket">LOKET <?php echo $w; ?></span>
                    </div>
                    <div class="no-antrian">
                        <?php echo $noantrian; ?>
                    </div>
                    
                </div>

            </div>
    <?php
        $w++;
    }
    ?>
   
</div>

<?php echo $this->renderPartial('_jsFunctions'); ?>

<div id="suarapanggilanLt5" >
    
</div>
