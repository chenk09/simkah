<?php

class AntrianSlideController extends Controller {

    public function actionIndex(){
        $this->layout = '//layouts/frameDialog';
        $modProfilPicture = ANProfilpictureM::model()->findAll('display_antrian = true');
        $displayAntrian = array();
        foreach ($modProfilPicture as $i=>$v){

            if (!empty($v->profilpicture_path)){
                if (!file_exists(Params::pathAntrianSliderGambar().$v->profilpicture_path)){
                        $v->profilpicture_path = Params::urlAntrianSliderGambar().'error404.jpg';
                }else{
                    $v->profilpicture_path = Params::urlAntrianSliderGambar().$v->profilpicture_path;
                }
            }
            
            if (strlen($v->profilpicture_desc) > 200){
                $v->profilpicture_desc = substr($v->profilpicture_desc, 1, 200).'....';
            }
            
            $displayAntrian[] = array(
                'image'=>$v->profilpicture_path, 
                'label'=>$v->profilpicture_nama, 
                'caption'=>$v->profilpicture_desc,
                'imageOptions'=>array('style'=>'width:570px;height:245px;')
                );
        }
        $model = new ANDisplayantrianpasienV('search');
        $this->render('index', array(
            'model'=>$model, 'modProfilPicture'=>$modProfilPicture, 'displayAntrian'=>$displayAntrian,
        ));
    }
    
    public function actionTable(){
        $this->layout = '//layouts/frameDialog';
        $model = new ANDisplayantrianpasienV('search');
        $this->render('table', array(
            'model'=>$model, 
        ));
    }

}