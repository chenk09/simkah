<?php

class TampilAntrianFarmasiController extends Controller
{
    public $layout='//layouts/antrian2';
    public function actionFarmasiLt2()
    {
        $model = new AntrianpasienfarmasiT;
        $lokAntrian = LokasiantrianM::model()->findByPk(3);
        $modJenisAntrian = JenisantrianM::model()->findAll('jenisantrian_id IN (6,7)');

        $this->render('indexLt2',array('model'=>$model,'lokAntrian'=>$lokAntrian,'modJenisAntrian'=>$modJenisAntrian));
    }
    
    public function actionFarmasiLt5()
    {
        $model = new AntrianpasienfarmasiT;
        $lokAntrian = LokasiantrianM::model()->findByPk(4);
        $modJenisAntrian = JenisantrianM::model()->findAll('jenisantrian_id IN (6,7)');

        $this->render('indexLt5',array('model'=>$model,'lokAntrian'=>$lokAntrian,'modJenisAntrian'=>$modJenisAntrian));
    }
    
    public function actionAmbilAntrian() {
        if (Yii::app()->request->isAjaxRequest) {
            
            $lokasiantrian_id = $_POST['lokasiantrian_id'];
            $jenisantrian_id = $_POST['jenisantrian_id'];
            $antrianpasienfarmasi_id = $_POST['antrianpasienfarmasi_id'];
            $nourutantrian = $_POST['nourutantrian'];
            
            $modelLokasiAntrian = LokasiantrianM::model()->findByPk($lokasiantrian_id);
            $modelJenisAntrian = JenisantrianM::model()->findByPk($jenisantrian_id);
            $modelAntrian = AntrianpasienT::model()->findByPk($antrianpasien_id); 
            
            $data['lokasiantrian_id'] = $modelLokasiAntrian->lokasiantrian_id;
            $data['jenisantrian_singkatan'] = $modelJenisAntrian->jenisantrian_singkatan;
            $data['loketantrian_id'] = $modelLoketAntrian->loketantrian_id;
            $data['loketantrian_nama'] = $modelLoketAntrian->loketantrian_nama;
            $data['nourutantrian'] = $modelAntrian->nourutantrian;
            
        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
    
    public function TerbilangAngka($angka){
        
        $bilangan = array("","satu","dua","tiga","empat","lima","enam","tujuh","delapn","sembilan","sepuluh","sebelas");
        if($angka < 12){
            return $bilangan[$angka];
        }
        elseif ($angka < 20) {
            return $this->TerbilangAngka($angka - 10) . " belas";
        }
        elseif ($angka < 100) {
            return $this->TerbilangAngka($angka / 10) . " puluh " . $this->TerbilangAngka($angka % 10);
        }
        elseif ($angka < 200){
            return "seratus " . $this->TerbilangAngka($angka - 100);
        }
        elseif ($angka < 1000){
            return $this->TerbilangAngka($angka / 100) . " ratus " . $this->TerbilangAngka($angka % 100);
        }
    }
    
    public function actionPanggilAntrian(){
        if (Yii::app()->request->isAjaxRequest) {
            
            $antrianpasienfarmasi_id = $_POST['antrianpasienfarmasi_id'];
            $modelAntrian = AntrianpasienfarmasiT::model()->findByPk($antrianpasienfarmasi_id);
            $modelJenisAntrian = JenisantrianM::model()->findByPk($modelAntrian->jenisantrian_id);
            $modelAntrian->nourutantrian = (int)substr($modelAntrian->nourutantrian, 1, 3);
            $antrian = $this->TerbilangAngka($modelAntrian->nourutantrian);
            $noantrians = array($modelAntrian->nourutantrian);
            
            $data["suarapanggilan"] = $this->renderPartial('suaraPanggilan',array('antrian'=>$antrian, 'modelAntrian'=>$modelAntrian, 'modelJenisAntrian'=>$modelJenisAntrian, 'noantrians'=>$noantrians),true);
            $data['lokasiantrian_id'] = $modelAntrian->lokasiantrian_id;
            
        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
}