<?php

class TampilAntrianPoliController extends Controller
{
    public $layout='//layouts/antrian2';
    public function actionIndexPoli()
    {
        $lokasiantrianklinik_id = $_GET['lokasiantrianklinik_id'];
        $modLayar = LokasiantrianklinikM::model()->findByPk($lokasiantrianklinik_id);
        $modAntrian = LayarantrianklinikM::model()->findAllByAttributes(array('lokasiantrianklinik_id'=>$modLayar->lokasiantrianklinik_id));
        
        $this->render('index1',array('modLayar'=>$modLayar,'modAntrian'=>$modAntrian, 'model'=>$model));
        
    }
    
    public function actionAmbilAntrian(){
            
        $pendaftaran_id = $_POST['pendaftaran_id'];
        $model = PendaftaranT::model()->findByPk($pendaftaran_id);
        $modAntrian = LayarantrianklinikM::model()->findByAttributes(array('ruangan_id'=>$model->ruangan_id));
        $data['model'] = $model->attributes;
        $data['dokter'] = $model->dokter->gelardepan." ".$model->dokter->nama_pegawai;
        $data['RM'] = $model->pasien->no_rekam_medik;
        $data['lokasiantrianklinik_id'] = $modAntrian->lokasiantrianklinik_id;
        
        echo json_encode($data);
        Yii::app()->end();
    }
    
    public function TerbilangAngka($angka){
        
        $bilangan = array("","satu","dua","tiga","empat","lima","enam","tujuh","delapn","sembilan","sepuluh","sebelas");
        if($angka < 12){
            return $bilangan[$angka];
        }
        elseif ($angka < 20) {
            return $this->TerbilangAngka($angka - 10) . " belas";
        }
        elseif ($angka < 100) {
            return $this->TerbilangAngka($angka / 10) . " puluh " . $this->TerbilangAngka($angka % 10);
        }
        elseif ($angka < 200){
            return "seratus " . $this->TerbilangAngka($angka - 100);
        }
        elseif ($angka < 1000){
            return $this->TerbilangAngka($angka / 100) . " ratus " . $this->TerbilangAngka($angka % 100);
        }
    }
    
    public function actionPanggilAntrian(){
        if (Yii::app()->request->isAjaxRequest) {
            
            $pendaftaran_id = $_POST['pendaftaran_id'];
            $model = PendaftaranT::model()->findByPk($pendaftaran_id);
            $model->no_urutantri = (int)$model->no_urutantri;
            $antrian = $this->TerbilangAngka($model->no_urutantri);
            $modRuangan = RuanganM::model()->findByPk($model->ruangan_id);
            $data["suarapanggilan"] = $this->renderPartial('suaraPanggilan',array('antrian'=>$antrian, 'modRuangan'=>$modRuangan, 'model'=>$model),true);
            
        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
}