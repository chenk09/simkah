<?php
 
class TampilAntrianController extends SBaseController
{
        public $layout='//layouts/tampilAntrian2'; 
        public function actionIndex()
	{
                $table = $this->loadFormat();
                
		$this->render('index',array('table'=>$table));
	}
          
        
        public function loadFormat(){
            $arrLoket = LoketM::model()->findAll('loket_aktif = true order by loket_nama');
            $tgl = date('Y-m-d');
           
            $format = '';
            $i=1;
            foreach ($arrLoket as $tampilLoket) {
                //$countantrian = count(AntrianT::model()->findAll('date(tglantrian) = '."'".$tgl."'".' AND loket_id = '.$tampilLoket->loket_id.' AND panggil_flaq=true ORDER BY antrian_id DESC'));
                $countantrian = count(AntrianT::model()->findAll('date(tglantrian) = '."'".$tgl."'".' AND loket_id = '.$tampilLoket->loket_id.' ORDER BY antrian_id DESC'));
                $result = $this->getNoAntrian($tampilLoket->loket_id);
                if(!empty($result))
                {
                    $antrian = $this->getNoAntrian($tampilLoket->loket_id);
                }
                else
                {
                    $antrian = '0000';
                }
                $format .= '
                        <div id="pasien'. $i .'">
                            <center>'.$antrian.'</center><br/>
                            <div id="jumlah">
                                <center>Jumlah Antrian : '.$countantrian.'</center>
    						    <center>Jumlah Maxs : '.$countantrian.'</center>
                            </div>
                        </div>
                ';
                $i++;
            }
            return $format;
        }
        
        public function getNoAntrian($idLoket){
             $tgl = date('Y-m-d');
             $sqlNoRM = "select noantrian from antrian_t where date(tglantrian) = '".$tgl."' AND loket_id = $idLoket AND panggil_flaq=true ORDER BY antrian_id DESC";
             
             return Yii::app()->db->createCommand($sqlNoRM)->queryScalar();
        }
        
     /*   public function actionResponseTable(){
            $table = $this->loadFormat();
            echo $table; exit;

        }
     */   
        public function actionResponseTable(){
            if(Yii::app()->getRequest()->getIsAjaxRequest()) {
                 //$table = $this->loadFormat();
                $table = "test";
                $data['isi_konten'] = $table;
                echo json_encode($data);
                Yii::app()->end();
            }
        }  
         

	public function actionPanggilAntrian(){
            $modCaraBayar = CarabayarM::model()->findAll('carabayar_loket is not null',array('order'=>'carabayar_nama'));
            
                if(isset ($_POST['carabayar_loket']))
                {
                    $carabayar_id = $_POST['carabayar_loket'];
                /*
                 * cari no_antrian selanjutnya
                 */
                $dataCaraBayar = CarabayarM::model()->findByPk($carabayar_id);
                $sqlCaraBayar = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) noantrian FROM antrian_t
                                               WHERE carabayar_id = $carabayar_id and pendaftaran_id is not null
                                               AND date(tglantrian)='".date('Y-m-d')."' order by noantrian limit 1";
                $noAntrian   = Yii::app()->db->createCommand($sqlCaraBayar)->queryRow();
                $noAntrianBaru = $dataCaraBayar['carabayar_singkatan'].(str_pad($noAntrian['noantrian']+1, 3, 0,STR_PAD_LEFT));
                $noAntrianBaru = str_split($noAntrianBaru);
                $loket = $dataCaraBayar['carabayar_loket'];
                
                $formSuaraMp3 = '';
                $formSuaraOgg = '';
                $suara = array();
                $forms = '';


                for ($f = 0; $f < count($noAntrianBaru); $f++) {
                    $suara['mp3'][$f] = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3';
                    $suara['ogg'][$f] = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3';
                    $forms .= '<div id="jquery_jplayer_'.$f.'" class="jp-jplayer"></div>';
                }

                
                $formLoketMp3 = ''.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$loket.'.mp3';
                $formLoketOgg = ''.Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$loket.'.ogg';
                $jumlah = count($noAntrianBaru);
                }
            
            $this->render('panggilAntrian',array('cara_bayar'=>$modCaraBayar,
                                                 'formLoketMp3'=>$formLoketMp3,
                                                 'formLoketOgg'=>$formLoketOgg,
                                                 'suara'=>$suara,
                                                 'jumlah'=>$jumlah,
                                                 'forms'=>$forms,
                                                 'noAntrianBaru'=>$noAntrianBaru,
                                                 'carabayar_id'=>$carabayar_id,
                                                 
            
            ));
        }
        
        public function actionAjaxAmbilAntrian()
        {
            $carabayar_id = $_POST['carabayar_id'];
            /*
             * cari no_antrian selanjutnya
             */
            $dataCaraBayar = CarabayarM::model()->findByPk($carabayar_id);
            $sqlCaraBayar = "SELECT CAST(MAX(SUBSTR(noantrian,2,3)) AS integer) noantrian FROM antrian_t
                                           WHERE carabayar_id = $carabayar_id and pendaftaran_id is not null
                                           AND date(tglantrian)='".date('Y-m-d')."' order by noantrian limit 1";
            $noAntrian   = Yii::app()->db->createCommand($sqlCaraBayar)->queryRow();
            $noAntrianBaru = $dataCaraBayar['carabayar_singkatan'].(str_pad($noAntrian['noantrian']+1, 3, 0,STR_PAD_LEFT));
            $noAntrianBaru = str_split($noAntrianBaru);
            $loket = $dataCaraBayar['carabayar_loket'];
            
            $form = '';
            $formSuaraMp3 = '';
            $formSuaraOgg = '';
            

            for ($f = 0; $f < count($noAntrianBaru); $f++) {
                $formSuaraMp3 .= CHtml::textfield('mp3_'.$f.'',Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3');
                $formSuaraOgg .= CHtml::textfield('ogg_'.$f.'',Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$noAntrianBaru[$f].'.ogg');
                $form .= '<div id="jquery_jplayer_'.$f.'" class="jp-jplayer"></div>';
            }

            $data['antrian'] = $noAntrianBaru;
            $data['formLoketMp3'] = CHtml::textfield('loketMp3',Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$loket.'.mp3');
            $data['formLoketOgg'] = CHtml::textfield('loketOgg',Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$loket.'.ogg');
            $data['formSuaraMp3'] = $formSuaraMp3;
            $data['formSuaraOgg'] = $formSuaraOgg;
            $data['form'] = $form;
            $data['jumlah'] = count($noAntrianBaru);
            echo json_encode($data);
            Yii::app()->end();
        }
        
       /* public function actionGetRunningText()
        {
            $konfig = KonfigsystemK::model()->find();
            
            $text = $konfig->running_text_display;
            
            echo json_encode($text);
        }
*/
        public function actionContentatas()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('contentAtas', array(
                'model'=>$model, 
            )); 
        }
        
        public function actionContentkiri()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('contentKiri', array(
                'model'=>$model, 
            )); 
        }

        public function actionContentkatas()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('contentKatas', array(
                'model'=>$model, 
            )); 
        }

        public function actionContentkawah()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('contentKawah', array(
                'model'=>$model, 
            )); 
        } 

        public function actionContentkananatasa()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('contentKananatasa', array(
                'model'=>$model, 
            )); 
        }

        public function actionContentkananatasb()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('antrian/TampilAntrian/ab/contentKananatasb', array(
                'model'=>$model, 
            )); 
        }

        public function actionContentkananbawaha()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('antrian/TampilAntrian/ab/contentKananbawaha', array(
                'model'=>$model, 
            )); 
        } 

        public function actionContentkananbawahb()
        {
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('antrian/TampilAntrian/ab/contentKnanbawahb', array(
                'model'=>$model, 
            )); 
        }  

        public function actionPanggilAntrian2(){
            $this->layout = '//layouts/frameDialog';
            $model = new ANDisplayantrianpasienV('search');
            $this->renderPartial('panggilAntrian', array(
                'model'=>$model, 
            )); 
        }
}