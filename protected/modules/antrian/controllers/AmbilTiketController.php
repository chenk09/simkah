<?php 

class AmbilTiketController extends SBaseController
{
        public $layout='//layouts/antrian2'; 
        public function actionIndex()
	{
            
            if(isset($_POST['idCaraBayar']))
                        {
//                            echo $_POST['loket'];
//                            exit();
                            $modAntrian = new ANAntrianT;
                            $modAntrian->loket_id=$_POST['loket'];
                            $modAntrian->profilrs_id=(int)Params::DEFAULT_PROFIL_RUMAH_SAKIT;
                            $modAntrian->ruangan_id=Yii::app()->user->getState('ruangan_id');
                            $modAntrian->carabayar_loket=$_POST['caraBayarLoket'];
                            $modAntrian->tglantrian=date('Y-m-d H:i:s');
//                            if(trim($_POST['statusPasien'])!=''){
//                                $modAntrian->statuspasien=$_POST['statusPasien'];
//                            }
                            $modAntrian->noantrian=$_POST['noUrut'];
//                            echo   $modAntrian->ruangan_id."-".$modAntrian->profilrs_id;exit;
                            $modAntrian->carabayar_id=$_POST['idCaraBayar'];
                            if($modAntrian->validate())
                                {   
                                    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print', array('id'=>$modAntrian->noantrian,'status'=>$_POST['loket']));
                                    $modAntrian->save();
                                    
//                                    $this->layout='//layouts/printAntri';
//                                    $data = $this->renderPartial('printNoAntrian',array('noantrian'=>$modAntrian->noantrian,'statuspasien'=>$modAntrian->statuspasien),true);
//                                    echo '<div id="printArea" style="display:none;">'.$data.'</div>';
                                    
//                                    exit();
//                                    echo '<script>
//                                            window.open("'.$url.'&id='.$modAntrian->noantrian.'&status='.$modAntrian->statuspasien.'","location=_new, width=900px");
//                                        </script>';
//                                    $this->refresh();
//                                    $this->redirect(array('index'));
                                }else{
                                    echo "ga validate";exit;
                                }
                          
                        }
                        
//                $noUmum=Generator::noAntrianUmum();
//                $noAskes=Generator::noAntrianAskes();
//                $noJamSostek=Generator::noAntrianJamsostek();
//                $noJamKesMas=Generator::noAntrianJamkesmas();
                $modLoket = ANLoketM::model()->findAll('loket_aktif=TRUE ORDER BY loket_nama');
                $jumlahLoket=count($modLoket);
                $tr='';
                $count = 0;
                $tinggiButton=25/ceil($jumlahLoket/2);
                foreach ($modLoket as $dataLoket)
                    {
                            $panjangSingkatan=strlen($dataLoket['loket_singkatan'])+1;
                            $panjangNomor=strlen($dataLoket['loket_formatnomor']);
                            $caraBayarLoket=  ANCaraBayarM::model()->findByPK($dataLoket['carabayar_id'])->carabayar_loket;
//                            $sqlLOket = "SELECT CAST(MAX(SUBSTR(noantrian,".$panjangSingkatan.",".$panjangNomor.")) AS integer) noantrian FROM antrian_t
//                                           WHERE loket_id=".$dataLoket['loket_id']."
//                                           AND date(tglantrian)='".date('Y-m-d')."'";
                            $sqlLOket = "SELECT CAST(MAX(SUBSTRING(noantrian FROM CHAR_LENGTH(TRIM(noantrian))-4)) AS integer) noantrian FROM antrian_t 
                                         WHERE loket_id = ".$dataLoket['loket_id']."
                                             AND date(tglantrian)='".date('Y-m-d')."'";
                            $noAntrian   = Yii::app()->db->createCommand($sqlLOket)->queryRow();
                            $noAntrianBaru = $dataLoket['loket_singkatan'].(str_pad($noAntrian['noantrian']+1, $panjangNomor, 0,STR_PAD_LEFT));
                            if($count % 2 ==0)
                                {
                                     // $tableNoAntri .=CHtml::htmlButton(strtoupper('<font style="font-size:'.$tinggiButton.'px">'.$dataLoket['loket_nama']).'</font><br/><br/><br/><font size="+7">'.$noAntrianBaru.'</font>',
                                     //         array('onclick'=>'simpan(\''.$dataLoket['loket_id'].'\',\''.$dataLoket['carabayar_id'].'\',\''.$noAntrianBaru.'\',\''.$caraBayarLoket.'\')',
                                     //   // 'onMouseOver'=>'ubahWarnaAntrian(this)',
                                     //    // 'onmouseout'=>'kembaliWarnaAntrian(this)',
                                     //     'id'=>'but'))."";
                                    $tableNoAntri .=CHtml::htmlButton($noAntrianBaru,
                                             array('onclick'=>'simpan(\''.$dataLoket['loket_id'].'\',\''.$dataLoket['carabayar_id'].'\',\''.$noAntrianBaru.'\',\''.$caraBayarLoket.'\')',
                                       // 'onMouseOver'=>'ubahWarnaAntrian(this)',
                                        // 'onmouseout'=>'kembaliWarnaAntrian(this)',
                                         'id'=>'but'))."";
                                }
                            else
                                {
                                    $tableNoAntri .=CHtml::htmlButton($noAntrianBaru,
                                            array('onclick'=>'simpan(\''.$dataLoket['loket_id'].'\',\''.$dataLoket['carabayar_id'].'\',\''.$noAntrianBaru.'\',\''.$caraBayarLoket.'\')',
                                       //  'onMouseOver'=>'ubahWarnaAntrian(this)',
                                         //'onmouseout'=>'kembaliWarnaAntrian(this)',
                                         'id'=>'but2'))."";
                                }
                   $count++;             
                    }
                
                    
                
		$this->render('index2',array('caraBayar'=>$caraBayar,
                                'jumlahCaraBayar'=>$jumlahCaraBayar,
                                'tr'=>$tr,'arrNoAntrian'=>$arrNoAntrian,
                                'tableNoAntri'=>$tableNoAntri,
                                'url'=>$url,
                                'pixel'=>$pixel));
	}
        
        public function actionPrint($id,$status)
        {
            if ($status == 1){
                $status = 'Pasien Baru';
            }
            else{
                $status ='Pasien Lama';
            }
            
            $this->layout='//layouts/printAntri';
            $this->render('printNoAntrian',array('noantrian'=>$id,'statuspasien'=>$status));
        }

        public function actionGetRunningText()
        {
            $konfig = KonfigsystemK::model()->find();
            
            $text = $konfig->running_text_kiosk;
            
            echo json_encode($text);
        }
	
}