<?php

class AntrianPendaftaranController extends Controller
{
    public $layout='//layouts/antrian2';
    public function actionPendaftaranLt2()
    {
        $model = new AntrianpasienT;
        $lokAntrian = LokasiantrianM::model()->findByPk(1);
        $modLokAntrian = LokasiantrianV::model()->findAllByAttributes(array('lokasiantrian_id'=>1),array('order'=>'jenisantrian_id'));

        $this->render('pendaftaranLt2',array('model'=>$model,'lokAntrian'=>$lokAntrian,'modLokAntrian'=>$modLokAntrian));
    }
    
    public function actionPendaftaranLt5()
    {
        $model = new AntrianpasienT;
        $lokAntrian = LokasiantrianM::model()->findByPk(2);
        $modLokAntrian = LokasiantrianV::model()->findAllByAttributes(array('lokasiantrian_id'=>2),array('order'=>'jenisantrian_id'));

        $this->render('pendaftaranLt5',array('model'=>$model,'lokAntrian'=>$lokAntrian,'modLokAntrian'=>$modLokAntrian));
    }
    
    public function actionSimpanTiket() {
        if (Yii::app()->request->isAjaxRequest) {
            $data = array();
            $data['delaytombol'] = 1;
            $lokasiantrian_id = $_POST['lokasiantrian_id'];
            $jenisantrian_id = $_POST['jenisantrian_id'];
            
            $data['pesan'] = "Data gagal disimpan! ";
            
            $sqlLOket = "SELECT CAST(MAX(SUBSTRING(nourutantrian, 2, 3)) AS integer) nourutantrian FROM antrianpasien_t 
                        WHERE lokasiantrian_id = ".$lokasiantrian_id."
                        AND jenisantrian_id = ".$jenisantrian_id."
                        AND date(tglantrianpasien)='".date('Y-m-d')."'";
            $noAntrian   = Yii::app()->db->createCommand($sqlLOket)->queryRow();
            $noAntrianBaru = (str_pad($noAntrian['nourutantrian']+1, 3, 0,STR_PAD_LEFT));
            
            $modJenAntrian = JenisantrianM::model()->findByPk($jenisantrian_id);
            
            $model = new AntrianpasienT;
            $model->lokasiantrian_id = $lokasiantrian_id;
            $model->jenisantrian_id = $jenisantrian_id;
            $model->nourutantrian = $modJenAntrian->jenisantrian_singkatan.$noAntrianBaru;
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->tglantrianpasien = date('Y-m-d H:i:s');
            $model->create_time = date('Y-m-d H:i:s');
            if ($model->validate()) {
                $model->save();
                $data['model'] = $model;
                $data['pesan'] = "Data berhasil disimpan!";
            }else{
                $data['pesan'] = "Data gagal disimpan! " . CHtml::errorSummary($model);
            }

        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
    
    public function actionPrint($antrian_id) {
        if ($status == 1){
            $status = 'Pasien Baru';
        }
        else{
            $status ='Pasien Lama';
        }
        
        $model = AntrianpasienT::model()->findByPk($antrian_id);
        $modLok = LokasiantrianM::model()->findByPk($model->lokasiantrian_id);
            
        $this->layout='//layouts/printAntri';
        $this->render('printNoAntrian',array('noantrian'=>$model->nourutantrian,'lokasi'=>$modLok->lokasiantrian_nama,'statuspasien'=>$status));
    }
}