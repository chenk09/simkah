<?php

class PemakaianbarangTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $defaultAction = 'index';
    public $pathView = 'gudangUmum.views.pemakaianbarangT.';
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
		$model = new GUPemakaianbarangT;
        $model->tglpemakaianbrg = date('Y-m-d H:i:s');
        $ruangan_id 			= Yii::app()->user->getState('ruangan_id');
        $model->ruangan_id		= $ruangan_id;
        $model->nopemakaianbrg 	= Generator::noPemakaianBarang();
        $model->create_ruangan	= $ruangan_id;
        $model->create_loginpemakai_id = Yii::app()->user->id;
        $modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id' => Yii::app()->user->id));
        $model->create_time = date('Y-m-d H:i:s');
        // $model->peg_pemesanan_id = $modLogin->pegawai_id;
        // $model->peg_pemesan_nama = $modLogin->pegawai->nama_pegawai;                
		if(isset($_POST['GUPemakaianbarangT']))
		{
			$model->attributes=$_POST['GUPemakaianbarangT'];
			$model->nopemakaianbrg 	= Generator::noPemakaianBarang();
            if (count($_POST['PemakaianbrgdetailT']) > 0){
                if ($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            $modDetails = $this->validasiTabular($model, $_POST['PemakaianbrgdetailT']);
                            foreach ($modDetails as $i=>$data){
                                if ($data->jmlpakai > 0){
                                    if ($data->save()){
                                    	InventarisasiruanganT::kurangiStok($data->jmlpakai, $data->barang_id);
                                    }
                                    else{
                                        $success = false;
                                    }
                                }
                            }

                        }
                        else{
                            $success = false;
                        }
                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index','id'=>$model->pemakaianbarang_id));
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch (Exception $ex){
                         $transaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
                }
            }else{
                $model->validate();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
		}

		$this->render($this->pathView.'index',array(
			'model'=>$model, 'modDetails'=>$modDetails,'pathView'=>$pathView,
		));
	}
        
    protected function validasiTabular($model, $data){
        $valid = true;
        foreach ($data as $i=>$row){
            $modDetails[$i] = new PemakaianbrgdetailT;
            $modDetails[$i]->attributes = $row;
            $modDetails[$i]->pemakaianbarang_id = $model->pemakaianbarang_id;
            $modDetails[$i]->ppn = 0;
            $modDetails[$i]->disc = 0;
            $modDetails[$i]->hpp = 0;
            $modDetails[$i]->catatanbrg = '-';

            $valid = $modDetails[$i]->validate() && $valid;
        }
        	// exit();

        return $modDetails;
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUPemakaianbarangT']))
		{
			$model->attributes=$_POST['GUPemakaianbarangT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pemakaianbarang_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GUPemakaianbarangT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GUPemakaianbarangT']))
			$model->attributes=$_GET['GUPemakaianbarangT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUPemakaianbarangT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gupemakaianbarang-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionInformasi()
	{
		$model = new GUPemakaianbarangT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GUPemakaianbarangT'])){
            $model->attributes=$_GET['GUPemakaianbarangT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GUPemakaianbarangT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GUPemakaianbarangT']['tglAkhir']);

        }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	public function actionDetail($id)
	{
		$this->layout = '//layouts/frameDialog';
        $modPemakaianbarang = GUPemakaianbarangT::model()->findByPk($id);
		if(count($modPemakaianbarang)>0){
			$modDetailPemakaian = GUPemakaianbrgdetailT::model()->findAllByAttributes(array('pemakaianbarang_id'=>$id));
			$this->render('detailInformasi', array(
                'modPemakaianbarang' => $modPemakaianbarang,
                'modDetailPemakaian' => $modDetailPemakaian,
            ));
		}
	}
        
}
