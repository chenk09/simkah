<?php

class LaporanController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction ='PembelianBarang';
	public function actionIndex()
	{
		$this->render('index');
	}
        
        // Laporan Pembelian Barang Gudang Umum //
                public function actionPembelianBarang()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanPembelianbarangT;
                        $model->tglAwal = date('d M Y 00:00:00');
                        $model->tglAkhir = date('d M Y 23:59:59');
                        if (isset($_GET['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                            $model->sumberdana_id = $_GET['GULaporanPembelianbarangT']['sumberdana_id'];
                            $model->supplier_id = $_GET['GULaporanPembelianbarangT']['supplier_id'];
                            $model->peg_pemesanan_id = $_GET['GULaporanPembelianbarangT']['peg_pemesanan_id'];
                        }
                        $this->render('pembelianBarang/pembelianBarang',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintPembelianBarang()
                {
                    
                    $model = new GULaporanPembelianbarangT;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');
                    $judulLaporan = 'Laporan Pembelian Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Pembelian Barang';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'pembelianBarang/printPembelianBarang';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFramePembelianBarang() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanPembelianbarangT;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Pembelian Barang';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanPembelianbarangT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanPembelianbarangT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPembelianbarangT']['tglAkhir']);
                    }
                    $searchdata = $model->searchPembelianBaranggrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
           // Akhir Laporan Pembelian Barang Gudang Umum //   
                
           // Laporan Penerimaan Persediaan Gudang Umum //
           public function actionPenerimaanPersediaan()
			{
				//if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
					$model = new GULaporanPenerimaanpersediaanT;
					$model->tglAwal = date('d M Y 00:00:00');
					$model->tglAkhir = date('d M Y 23:59:59');
					if (isset($_GET['GULaporanPenerimaanpersediaanT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
						$model->sumberdana_id = $_GET['GULaporanPenerimaanpersediaanT']['sumberdana_id'];
						$model->ruanganpenerima_id = $_GET['GULaporanPenerimaanpersediaanT']['ruanganpenerima_id'];
						$model->peg_penerima_id = $_GET['GULaporanPenerimaanpersediaanT']['peg_penerima_id'];
					}
					$this->render('penerimaanPersediaan/penerimaanPersediaan',array(
						'model'=>$model,
					));
			}

			public function actionPrintPenerimaanPersediaan()
			{

				$model = new GULaporanPenerimaanpersediaanT;
				$model->tglAwal = date('d M Y 00:00:00');
				$model->tglAkhir = date('d M Y 23:59:59');
				$judulLaporan = 'Laporan Penerimaan Persediaan';

				//Data Grafik
				$data['title'] = 'Grafik Penerimaan Persediaan';
				$data['type'] = $_REQUEST['type'];
				if (isset($_REQUEST['GULaporanPenerimaanpersediaanT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
				}
				$caraPrint = $_REQUEST['caraPrint'];
				$target = 'penerimaanPersediaan/printPenerimaanPersediaan';

				$this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
			}   

			public function actionFramePenerimaanPersediaan() {
				$this->layout = '//layouts/frameDialog';

				$model = new GULaporanPenerimaanpersediaanT;
				$model->tglAwal = date('d M Y 00:00:00');
				$model->tglAkhir = date('d M Y 23:59:59');

				//Data Grafik
				$data['title'] = 'Grafik Penerimaan Persediaan';
				$data['type'] = $_GET['type'];

				if (isset($_REQUEST['GULaporanPenerimaanpersediaanT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GULaporanPenerimaanpersediaanT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanPenerimaanpersediaanT']['tglAkhir']);
				}
				$searchdata = $model->searchPenerimaanPersediaangrafik();
				$this->render('_grafik', array(
					'model' => $model,
					'data' => $data,
					'searchdata'=>$searchdata,
				));
			}
			
			public function actiondetailPesanBarang($id){
				$this->layout ='//layouts/frameDialog';
				$format = new CustomFormat;
				$modPesan = GULaporanPenerimaanpersediaanT::model()->findByPk($id);
				$modDetailPesan = TerimapersdetailT::model()->findAllByAttributes(array('terimapersediaan_id'=>$modPesan->terimapersediaan_id));
				$this->render('gudangUmum.views.laporan.penerimaanPersediaan.detailInformasi', array(
					'modPesan'=>$modPesan,
					'modDetailPesan'=>$modDetailPesan,
					'format'=>$format,
				));
			}


			// Akhir Laporan Penerimaan Persediaan Gudang Umum //
           
            // Laporan Penerimaan Barang Gudang Umum //
           public function actionPenerimaanBarangGU()
			{
				//if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
					$model = new GUPenerimaanbarangT;
					$model->tglAwal = date('d M Y 00:00:00');
					$model->tglAkhir = date('d M Y 23:59:59');
					if (isset($_GET['GUPenerimaanbarangT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GUPenerimaanbarangT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAkhir']);
					}
					$this->render('penerimaanBarang/penerimaanBarang',array(
						'model'=>$model,
					));
			}

			public function actionPrintPenerimaanBarangGU()
			{

				$model = new GUPenerimaanbarangT;
				$model->tglAwal = date('d M Y 00:00:00');
				$model->tglAkhir = date('d M Y 23:59:59');
				$judulLaporan = 'Laporan Penerimaan Persediaan';

				//Data Grafik
				$data['title'] = 'Grafik Penerimaan Persediaan';
				$data['type'] = $_REQUEST['type'];
				if (isset($_REQUEST['GUPenerimaanbarangT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GUPenerimaanbarangT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAkhir']);
				}
				$caraPrint = $_REQUEST['caraPrint'];
				$target = 'penerimaanBarang/printPenerimaanBarang';

				$this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
			}   

			public function actionFramePenerimaanBarangGU() {
				$this->layout = '//layouts/frameDialog';

				$model = new GUPenerimaanbarangT;
				$model->tglAwal = date('d M Y 00:00:00');
				$model->tglAkhir = date('d M Y 23:59:59');

				//Data Grafik
				$data['title'] = 'Grafik Penerimaan Persediaan';
				$data['type'] = $_GET['type'];

				if (isset($_REQUEST['GUPenerimaanbarangT'])) {
						$format = new CustomFormat;
						$model->attributes = $_GET['GUPenerimaanbarangT'];
						$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAwal']);
						$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GUPenerimaanbarangT']['tglAkhir']);
				}
				$searchdata = $model->searchPenerimaanBaranggrafik();
				$this->render('_grafik', array(
					'model' => $model,
					'data' => $data,
					'searchdata'=>$searchdata,
				));
			}
			
			public function actionDetailBarangGU($id){
				$this->layout ='//layouts/frameDialog';
				$format = new CustomFormat;
				$modPesan = GUPenerimaanbarangT::model()->findByPk($id);
				$modDetailPesan = GUPenerimaandetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$modPesan->penerimaanbarang_id));
				$this->render('gudangUmum.views.laporan.penerimaanBarang.detailInformasi', array(
					'modPesan'=>$modPesan,
					'modDetailPesan'=>$modDetailPesan,
					'format'=>$format,
				));
			}


			// Akhir Laporan Penerimaan Persediaan Gudang Umum //
           
            
            // Laporan Retur Penerimaan Gudang Umum //
            public function actionReturPenerimaan()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanReturPenerimaanT;
                        $model->tglAwal = date('d M Y 00:00:00');
                        $model->tglAkhir = date('d M Y 23:59:59');
                        if (isset($_GET['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                            $model->peg_retur_id = $_GET['GULaporanReturPenerimaanT']['peg_retur_id'];
                            $model->peg_mengetahui_id = $_GET['GULaporanReturPenerimaanT']['peg_mengetahui_id'];
                        }
                        $this->render('returPenerimaan/returPenerimaan',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintReturPenerimaan()
                {
                    
                    $model = new GULaporanReturPenerimaanT;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');
                    $judulLaporan = 'Laporan Retur Penerimaan';

                    //Data Grafik
                    $data['title'] = 'Grafik Retur Penerimaan';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'returPenerimaan/printReturPenerimaan';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameReturPenerimaan() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanReturPenerimaanT;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Retur Penerimaan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanReturPenerimaanT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanReturPenerimaanT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanReturPenerimaanT']['tglAkhir']);
                    }
                    $searchdata = $model->searchReturPenerimaangrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }    
                
            
           // Akhir Laporan Retur Penerimaan Gudang Umum //


                       // Laporan Penerimaan Persediaan Gudang Umum //
                public function actionMutasiBarang()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new GULaporanmutasibarangV;
                        $model->tglAwal = date('d M Y 00:00:00');
                        $model->tglAkhir = date('d M Y 23:59:59');
                        if (isset($_GET['GULaporanmutasibarangV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanmutasibarangV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAkhir']);

                        }
                        $this->render('mutasiBarang/mutasiBarang',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintMutasiBarang()
                {
                    
                    $model = new GULaporanmutasibarangV;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');
                    $judulLaporan = 'Laporan Mutasi Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Mutasi Barang';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['GULaporanmutasibarangV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanmutasibarangV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'mutasiBarang/printMutasiBarang';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameMutasiBarang() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new GULaporanmutasibarangV;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Penerimaan Persediaan';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['GULaporanmutasibarangV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['GULaporanmutasibarangV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GULaporanmutasibarangV']['tglAkhir']);
                    }
                    $searchdata = $model->searchMutasiBaranggrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
           
           // Akhir Laporan Penerimaan Persediaan Gudang Umum //
                
                protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
                    $format = new CustomFormat();
                    $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

                    if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
                        $this->layout = '//layouts/printWindows';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($caraPrint == 'EXCEL') {
                        $this->layout = '//layouts/printExcel';
                         $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($_REQUEST['caraPrint'] == 'PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('', $ukuranKertasPDF);
                        $mpdf->useOddEven = 2;
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
                        $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
                        $mpdf->Output();
                    }
                }
                
           
                public function actionStock()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new RELaporanstokprodukposV;
                        $model->tglAwal = date('d M Y 00:00:00');
                        $model->tglAkhir = date('d M Y 23:59:59');
                        if (isset($_GET['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                        }
                        $this->render('stock/stock',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintStock()
                {
                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');
                    $judulLaporan = 'Stock Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Stock';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'stock/printStock';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameStock() {
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('d M Y 00:00:00');
                    $model->tglAkhir = date('d M Y 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Stock Barang';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $searchdata = $model->searchGrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
    
                protected function parserTanggal($tgl){
                    $tgl = explode(' ', $tgl);
                    $result = array();
                    foreach ($tgl as $row){
                        if (!empty($row)){
                            $result[] = $row;
                        }
                    }
                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

                }
}