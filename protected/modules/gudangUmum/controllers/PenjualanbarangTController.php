<?php

class PenjualanbarangTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi', 'detail'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
		$model = new GUPenjualanbarangT;
        $model->tglpenjualanbrg = date('Y-m-d H:i:s');
        $ruangan_id 			= Yii::app()->user->getState('ruangan_id');
        $model->ruangan_id		= $ruangan_id;
        $model->nopenjualan 	= Generator::noPenjualanBarang();
        $model->create_ruangan	= $ruangan_id;
        $model->create_loginpemakai_id = Yii::app()->user->id;
        $modLogin = LoginpemakaiK::model()->findByAttributes(array('loginpemakai_id' => Yii::app()->user->id));
        $model->create_time = date('Y-m-d H:i:s');
        $model->pegawai_id = $modLogin->pegawai_id;
        $model->penerimabarang = $modLogin->pegawai->nama_pegawai;                
        $model->profilepemesan_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
		if(isset($_POST['GUPenjualanbarangT']))
		{
			$model->attributes=$_POST['GUPenjualanbarangT'];
			$model->nopenjualan 	= Generator::noPenjualanBarang();
            if (count($_POST['PenjualanbrgdetailT']) > 0){
                if ($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            $modDetails = $this->validasiTabular($model, $_POST['PenjualanbrgdetailT']);
                            foreach ($modDetails as $i=>$data){
                                if ($data->jmljual > 0){
                                    if ($data->save()){
                                    	InventarisasiruanganT::kurangiStok($data->jmljual, $data->barang_id);
                                    }
                                    else{
                                        $success = false;
                                    }
                                }
                            }

                        }
                        else{
                            $success = false;
                        }
                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index','id'=>$model->penjualanbarang_id));
                        }
                        else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch (Exception $ex){
                         $transaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
                }
            }else{
                $model->validate();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
		}

		$this->render('index',array(
			'model'=>$model, 'modDetails'=>$modDetails,
		));
	}
        
    protected function validasiTabular($model, $data){
        $valid = true;
        foreach ($data as $i=>$row){
            $modDetails[$i] = new PenjualanbrgdetailT;
            $modDetails[$i]->attributes = $row;
            $modDetails[$i]->penjualanbarang_id = $model->penjualanbarang_id;
            $modDetails[$i]->catatan = '-';

            $modDetails[$i]->satuanjual = !empty($row['satuanjual']) ? $row['satuanjual'] : '-';

            $valid = $modDetails[$i]->validate() && $valid;
        }

        return $modDetails;
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GUPembelianbarangT']))
		{
			$model->attributes=$_POST['GUPembelianbarangT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pembelianbarang_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new GUPenjualanbarangT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GUPenjualanbarangT']))
			$model->attributes=$_GET['GUPenjualanbarangT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GUPenjualanbarangT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gupenjualanbarang-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
    public function actionInformasi()
	{
		$model = new GUPenjualanbarangT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['GUPenjualanbarangT'])){
            $model->attributes=$_GET['GUPenjualanbarangT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['GUPenjualanbarangT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['GUPenjualanbarangT']['tglAkhir']);
        }

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	public function actionDetail($id)
	{
		$this->layout = '//layouts/frameDialog';
        $modPenjualanbarang = GUPenjualanbarangT::model()->findByPk($id);
		if(count($modPenjualanbarang)>0){
			$modDetailPenjualan = GUPenjualanbrgdetailT::model()->findAllByAttributes(array('penjualanbarang_id'=>$id));
			$this->render('detailInformasi', array(
                'modPenjualanbarang' => $modPenjualanbarang,
                'modDetailPenjualan' => $modDetailPenjualan,
            ));
		}
	}

}
