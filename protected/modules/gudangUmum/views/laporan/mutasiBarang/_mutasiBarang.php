<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchMutasiBarangPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchMutasiBarang();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
    'mergeColumns'=>array('nomutasibrg'),
	'columns'=>array(
                    array(
                        'header'=>'No Mutasi Barang',
                        'value' => '$data->nomutasibrg',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Tanggal Mutasi Barang',
                        'type'=>'raw',
                        'value'=>'$data->tglmutasibrg',
                    ),
                    array(
                        'header'=>'Nama Karyawan',
                        'type'=>'raw',
                        'value'=>'$data->karyawanpemesan_nama',
                    ),
                   array(
                        'header'=>'Nama Barang',
                        'type'=>'raw',
                        'value'=>'$data->barang_nama',
                    ),
                   array(
                        'header'=>'Qty',
                        'type'=>'raw',
                        'value'=>'$data->qty_mutasi',
                    ),
                   array(
                        'header'=>'Keterangan Mutasi',
                        'type'=>'raw',
                        'value'=>'$data->keterangan_mutasi',
                    ),
                 array(
                        'header'=>'No Pemesanan',
                        'type'=>'raw',
                        'value'=>'$data->nopemesanan',
                    ),
	),
)); ?>