<?php
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPenerimaanBarangPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchPenerimaanBarang();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
	'itemsCssClass'=>'table table-bordered table-striped table-condensed',
	'template'=>$template,
	'mergeColumns'=>array('suplayer','tglterima','pegawai','ruangan'),
    'extraRowColumns'=> array('suplayer'),
	'columns'=>array(
					array(
                        'header'=>'Nama Supplier',
			'name'=>'suplayer',
                        'type'=>'raw',
                        'value'=>'$data->supplier->supplier_nama',
                    ),
                   'noterima',
                    array(
                        'header'=>'Tanggal Terima',
                        'type'=>'raw',
                        'value'=>'$data->tglterima',
                    ),
                   
                   array(
                        'header'=>'No Pembelian',
                        'type'=>'raw',
			'name'=>'nopermintaan',
                        'value'=>'$data->permintaanpembelian->nopermintaan',
//                        'footer'=>'<b><right>Total:</right></b>',
                        //  'htmlOptions'=>array(
                        //     'style'=>'text-align:right;'
                        // ),
                    ),
//                     array(
//                        'header'=>'Total Harga',
//                        'type'=>'raw',
//                        'value'=>'number_format($data->totalharga,0,"",",")',
//                        'footer'=>number_format($model->getTotalharga()),
//                    ),
                    array(
                        'header'=>'Pegawai Penerima',
                        'type'=>'raw',
			'name'=>'pegawai',
                        'value'=>'$data->loginpemakai->pegawai->nama_pegawai',
                    ),
                   array(
                        'header'=>'Pegawai Mengetahui',
                        'type'=>'raw',
                        'value'=>'isset($data->permintaanpembelian->pegawai->nama_pegawai) ? $data->permintaanpembelian->pegawai->nama_pegawai : ""',
                    ),
                 array(
                        'header'=>'Ruangan',
                        'type'=>'raw',
			'name'=>'ruangan',
                        'value'=>'$data->ruangan->ruangan_nama',
                    ),
		array(
                        'header'=>'Detail',
                        'type'=>'raw',
			'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ",  Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/DetailBarangGU",array("id"=>$data->penerimaanbarang_id)),array("id"=>"$data->penerimaanbarang_id","target"=>"frameDetail","rel"=>"tooltip","title"=>"Klik untuk Detail Pemesanan Barang", "onclick"=>"window.parent.$(\'#dialogDetail\').dialog(\'open\')"));',
			'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                )
	),
)); ?>

<?php
//========= Dialog untuk Melihat detail Pengajuan Bahan Makanan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogDetail',
    'options' => array(
        'title' => 'Detail Pemesanan Barang',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

echo '<iframe src="" name="frameDetail" width="100%" height="500">
</iframe>';

$this->endWidget();
?>