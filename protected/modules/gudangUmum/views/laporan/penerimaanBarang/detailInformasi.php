<?php if (isset($judulLaporan)){
    echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));      
}
?>

<table class='table'>
    <tr>
        <td>
             <b><?php echo CHtml::encode($modPesan->getAttributeLabel('supplier_id')); ?>:</b>
            <?php echo CHtml::encode($modPesan->supplier->supplier_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modPesan->getAttributeLabel('tglterima')); ?>:</b>
            <?php echo CHtml::encode($modPesan->tglterima); ?>
             <br/>
			 <b>Penerima:</b>
            <?php echo CHtml::encode($modPesan->loginpemakai->pegawai->nama_pegawai); ?>
             <br/>
             
        </td>
        <td>
             <b><?php echo CHtml::encode($modPesan->getAttributeLabel('noterima')); ?>:</b>
            <?php echo CHtml::encode($modPesan->noterima); ?>
            <br />
             <b><?php echo CHtml::encode($modPesan->getAttributeLabel('totalharga')); ?>:</b>
            0 <?php // echo CHtml::encode(number_format($modPesan->totalharga,0,"",",")); ?>
            <br />
			<b><?php echo CHtml::encode($modPesan->getAttributeLabel('ruangan')); ?>:</b>
            <?php echo CHtml::encode($modPesan->ruangan->ruangan_nama); ?>
            <br />
        </td>
    </tr>   
</table>

<table id="tableObatAlkes" class="table table-striped table-bordered table-condensed">
    <thead>
    
        <th>No.Urut</th>
        <th>Barang</th>
		<th>Satuan Beli</th>
		<th>Jumlah dalam kemasan</th>
		<th>Kondisi</th>
		<th>Jumlah Beli</th>
        <th>Harga Satuan</th>
		<th>Total</th>
    </thead>
    <tbody>
    <?php
    $no=1;
    $hargabelibesarJml = 0;
        foreach($modDetailPesan AS $detail): ?>
             <tr>   
                <td><?php echo $no; ?></td>                
                <td><?php echo $detail->obatalkes->obatalkes_nama; ?></td>                
                <td><?php echo $detail->satuanbesar->satuanbesar_nama; ?></td>
                <td>
                <?php 
                    echo $detail->jmlkemasan;
                ?>
                </td>
                <td><?php // echo $detail->kondisibarang; ?>-</td> 
				<td><?php echo $detail->jmlterima; ?></td> 
                <td><?php echo number_format($detail->hargabelibesar,0,"",","); ?></td>
				<td><?php echo number_format($detail->hargabelibesar*$detail->jmlterima,0,"",","); ?></td>
            </tr>   
            <?php 
            $hargabelibesarJml += $hargabelibesarJml + ($detail->hargabelibesar * $detail->jmlterima);
        $no++;
        
        endforeach;
    ?>
    </tbody>
	<tfoot>
		<tr>
			<td colspan="7" style="text-align:right;">Subtotal : </td>
			<td><?php echo number_format($hargabelibesarJml,0,"",","); ?></td>
		</tr>
	</tfoot>
</table>

<?php if(isset($caraPrint)){
    
}else{ ?>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; ?>
<?php
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print&id='.$modPesan->penerimaanbarang_id);
        $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#gupesanbarang-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php } ?>