<?php
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchReturPenerimaanPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchReturPenerimaan();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                        'header'=>'No Retur Penerimaan',
                        'value'=>'$data->noreturterima',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Tgl Retur',
                        'value'=>'$data->tglreturterima',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Alasan Retur',
                        'value'=>'$data->alasanreturterima',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Keterangan Retur',
                        'value'=>'$data->keterangan_retur',
                        'type'=>'raw',
                         'footer'=>'<b><right>Total:</right></b>',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
                    array(
                        'header'=>'Total Retur',
                        'value'=>'$data->totalretur',
                        'type'=>'raw',
                        'footer'=>number_format($model->getTotalRetur()),
                    ),
                    array(
                        'header'=>'Pegawai Retur',
                        'value'=>'$data->pegawairetur->nama_pegawai',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Pegawai Mengetahui',
                        'value'=>'$data->pegawaimengetahui->nama_pegawai',
                        'type'=>'raw',
                    ),
	),
)); ?>