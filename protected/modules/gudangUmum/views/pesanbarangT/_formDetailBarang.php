<?php 
if (isset($modDetail)){
echo $form->errorSummary($modDetail); 
}
?>
<?php if ($model->isNewRecord) { ?>
<div id="formDetailBarang">
<div class="control-group ">
    <label class='control-label'>Barang</label>
    <div class="controls">
        <?php echo CHtml::hiddenField('idBarang'); ?>
        <!--                <div class="input-append" style='display:inline'>-->
        <?php
        $this->widget('MyJuiAutoComplete', array(
            'name' => 'namaBarang',
            'source' => 'js: function(request, response) {
                               $.ajax({
                                   url: "' . Yii::app()->createUrl('ActionAutoComplete/barang') . '",
                                   dataType: "json",
                                   data: {
                                       term: request.term,
                                   },
                                   success: function (data) {
                                           response(data);
                                   }
                               })
                            }',
            'options' => array(
                'showAnim' => 'fold',
                'minLength' => 2,
                'focus' => 'js:function( event, ui ) {
                                    $(this).val( ui.item.label);
                                    return false;
                                }',
                'select' => 'js:function( event, ui ) {
                                    $("#idBarang").val(ui.item.barang_id); 
                                    return false;
                                }',
            ),
            'htmlOptions' => array(
                'onkeypress' => "return $(this).focusNextInputField(event)",
                'class' => 'span2',
            ),
            'tombolDialog' => array('idDialog' => 'dialogBarang'),
        ));
        ?>

    </div>
</div>
<div class="control-group ">
    <label class='control-label'>Qty</label>
    <div class="controls">
        <?php echo Chtml::textField('jumlah', 1, array('class' => 'span1 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
        <?php echo Chtml::dropDownList('satuan', '', Satuanbarang::items(), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>                
        <?php
        echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', 
                array('onclick' => 'inputBarang();return false;',
                    'class' => 'btn btn-primary',
                    'onkeypress' => "inputBarang();return $(this).focusNextInputField(event)",
                    'rel' => "tooltip",
                    'title' => "Klik untuk menambahkan Barang",));
        ?>
    </div>
</div>

</div>
    <?php } ?>
<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogBarang',
    'options' => array(
        'title' => 'Daftar Barang',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000,
        'height' => 500,
        'resizable' => false,
    ),
));

$modBarang = new SABarangM('search');
$modBarang->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['SABarangM']))
    $modBarang->attributes = $_GET['SABarangM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'barang-m-grid',
    'dataProvider'=>$modBarang->searchDialog(),
    'filter'=>$modBarang,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'barang_id',
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBarang",
                                    "onClick" => "
                                        
                                        $(\'#idBarang\').val(\'$data->barang_id\');
                                        $(\'#namaBarang\').val(\'$data->barang_nama\');
                                        $(\'#satuan\').val(\'$data->barang_satuan\');
                                        $(\'#dialogBarang\').dialog(\'close\');
                                        return false;"))',
        ),
        array(
                        'name'=>'barang_id',
                        'value'=>'$data->barang_id',
                        'filter'=>false,
                ),
      'barang_kode','barang_type','barang_nama',
      'barang_merk','barang_noseri',
      'barang_ukuran','barang_bahan',
      'barang_warna', 
      //'barang_satuan',
        // 'bidang.subkelompok.kelompok.golongan.golongan_nama',
        // 'bidang.subkelompok.kelompok.kelompok_nama',
        // 'bidang.subkelompok.subkelompok_nama',
        // 'bidang.bidang_nama',
//        'bidang_id',
//        'barang_type',
//        'barang_kode',
        // 'barang_nama',
//        'barang_satuan',
        array(
            'name'=>'barang_satuan',
            'filter'=>Satuanbarang::items(),
            'value'=>'$data->barang_satuan',
        ),
        // 'barang_ukuran',
        // 'barang_bahan',
//        'barang_namalainnya',
        
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php 
$urlAjax = Yii::app()->createUrl('actionAjax/getPesanBarang');
$notif = Yii::t('mds','Do You want to cancel?');
$js = <<< JS
    function inputBarang(){
        idBarang = $('#idBarang').val();
        jumlah = $('#jumlah').val();
        satuan = $('#satuan').val();

        if (!jQuery.isNumeric(idBarang)){
            alert('Isi Barang yang akan dipesan');
            return false;
        }
        else if (!jQuery.isNumeric(jumlah)){
            alert('Isi jumlah barang yang akan dipesan');
            return false;
        }
        else{
            if (cekList(idBarang) == true){
                $.post('${urlAjax}', {idBarang:idBarang, jumlah:jumlah, satuan:satuan}, function(data){
                    $('#tableDetailBarang tbody').append(data);
                    $("#tableDetailBarang tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                    clear();
                    renameInput();
                }, 'json');
            }
        }
        
    }
            
    function cekList(id){
        x = true;
        $('.barang').each(function(){
            if ($(this).val() == id){
                alert('Barang telah ada d List');
                clear();
                x = false;
            }
        });
        return x;
    }
    
    function clear(){
        $('#formDetailBarang').find('input, select').each(function(){
            $(this).val('');
        });
        $('#jumlah').val(1);
    }
    
    function batal(obj){
        if(confirm('Apakah anda akan menghapus obat?'))
        $(obj).parent().parent().remove();
        
        renameInput();
    }
    function renameInput(){
        noUrut = 0;
        $('.cancel').each(function(){
            baris = 1;
            urutan = 0;
            $(this).parents('td').find('[name*="PesanbarangdetailT"]').each(function(){
                
                var nama = $(this).attr('name');
                data = nama.split('PesanbarangdetailT[]');

                if (typeof data[1] === "undefined"){                    
                    if(baris%3==0){
                        urutan++;
                    }
                }else{
                    $(this).attr('name','PesanbarangdetailT['+urutan+']'+data[1]);
                    
                }
                baris++;

            });
            noUrut++;
        });        
    }
JS;
Yii::app()->clientScript->registerScript('onhead',$js,  CClientScript::POS_HEAD);
?>

<?php 
Yii::app()->clientScript->registerScript('onready','
    $("form").submit(function(){
        pesan = false;
        idRuangan = $("#'.CHtml::activeId($model, 'ruanganpemesan_id').'").val();
        idPemesan = $("#'.CHtml::activeId($model, 'pegpemesan_id').'").val();
            
        $(".pesan").each(function(){
            if ($(this).val() > 0){
                pesan = true
            }
        });
        
        if(!jQuery.isNumeric(idRuangan)){
            alert("'.CHtml::encode($model->getAttributeLabel('ruanganpemesan_id')).' harus diisi");
            return false;
        }
        else if (!jQuery.isNumeric(idPemesan)){
            alert("'.CHtml::encode($model->getAttributeLabel('pegpemesan_id')).' harus diisi");
            return false;
        }
        
        if ($(".cancel").length < 1){
            alert("Detail Barang Harus Diisi");
            return false;
        }
        else if (pesan == false){
            alert("'.CHtml::encode($model->getAttributeLabel('qty_pesan')).' harus memiliki value yang lebih dari 0");
            return false;
        }
    });
',CClientScript::POS_READY);?>