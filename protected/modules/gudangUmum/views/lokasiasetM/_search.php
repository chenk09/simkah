<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'salokasiaset-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'lokasi_id',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'lokasiaset_kode',array('class'=>'span1','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'lokasiaset_namainstalasi',array('class'=>'span3','maxlength'=>100)); ?>

	<?php //echo $form->textFieldRow($model,'lokasiaset_namabagian',array('class'=>'span3','maxlength'=>50)); ?>
	<div class="control-group ">
        <label class="control-label" for="instalasi">Instalasi Lokasi Aset</label>
        <div class="controls">
        <?php 
                $this->widget('MyJuiAutoComplete', array(
                                'model'=>$model,
                                'attribute'=>'lokasiaset_namainstalasi',
                                'source'=>'js: function(request, response) {
                                               $.ajax({
                                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/getInstalasi').'",
                                                   dataType: "json",
                                                   data: {
                                                       term: request.term,
                                                   },
                                                   success: function (data) {
                                                           response(data);
                                                   }
                                               })
                                            }',
                                 'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 2,
                                       'focus'=> 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) { 
                                            $("#'.CHtml::activeId($model, 'lokasiaset_namainstalasi').'").val(ui.item.instalasi_nama);
                                            return false;
                                        }',
                                ),
                                'htmlOptions'=>array(
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogInstalasi'),
                            )); 
            ?>
        </div>
    </div>

    <div class="control-group ">
            <label class="control-label" for="ruangan">Ruangan Lokasi Aset</label>
            <div class="controls">
            <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$model,
                                    'attribute'=>'lokasiaset_namabagian',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/getRuangan').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val( ui.item.label);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) { 
                                                $("#'.CHtml::activeId($model, 'lokasiaset_namabagian').'").val(ui.item.ruangan_nama);
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array(
                                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogRuangan'),
                                )); 
                ?>
            </div>
        </div>

	<?php echo $form->textFieldRow($model,'lokasiaset_namalokasi',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'lokasiaset_aktif',array('checked'=>'lokasiaset_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>

<?php
//========= Dialog buat cari data instalasi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogInstalasi',
    'options'=>array(
        'title'=>'Instalasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modinstalasi = new SAInstalasiM('search');
$modinstalasi->unsetAttributes();
if(isset($_GET['SAInstalasiM']))
    $moObatAlkes->attributes = $_GET['SAInstalasiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sainstalasi-m-grid',
	'dataProvider'=>$modinstalasi->search(),
	'filter'=>$modinstalasi,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectInstalasi",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($model, 'lokasiaset_namainstalasi').'\").val(\'$data->instalasi_nama\');
                                    $(\'#dialogInstalasi\').dialog(\'close\');return false;"))',
                ),
            'instalasi_nama',
            'instalasi_singkatan',
            'instalasi_lokasi',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>
        
<?php
//========= Dialog buat cari data ruangan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogRuangan',
    'options'=>array(
        'title'=>'Ruangan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>750,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modRuangan = new SARuanganM('search');
$modRuangan->unsetAttributes();
if(isset($_GET['SARuanganM']))
    $modRuangan->attributes = $_GET['SARuanganM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'saruangan-m-grid',
	'dataProvider'=>$modRuangan->search(),
	'filter'=>$modRuangan,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",
                                "#",
                                array(
                                    "class"=>"btn-small", 
                                    "id" => "selectRuangan",
                                    "onClick" => "
                                    $(\"#'.CHtml::activeId($model, 'lokasiaset_namabagian').'\").val(\'$data->ruangan_nama\');
                                    $(\'#dialogRuangan\').dialog(\'close\');return false;"))',
                ),
            'ruangan_nama',
		        'ruangan_jenispelayanan',
            'ruangan_lokasi',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget();
?>