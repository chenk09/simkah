<?php
/**
* gudangUmum/views/baranM/informasiStokBarang.php : view informasi stok barang gudang umum
* Author    : Hardi
* Date      : 17-04-2014
* Issue     : EHJ-1602
**/
?>
<legend class="rim2">Informasi Stok Barang</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('informasistokbarang-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'informasistokbarang-t-grid',
	'dataProvider'=>$model->search(),
	//	'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		'ruangan_nama',
		'barang_type',
        'inventarisasi_kode',
        'barang_kode',
        'barang_nama',
        'barang_merk',
        array(
            'header'=>'Stok',
            'value'=>'$data->qty',
            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
        ),
        'barang_satuan',
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<div class="search-form">
<?php $this->renderPartial('_searchInformasi',array(
	'model'=>$model,
)); ?>
</div>