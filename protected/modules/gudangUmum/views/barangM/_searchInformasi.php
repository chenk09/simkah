<?php
/**
* gudangUmum/views/baranM/_searchInformasi.php : search informasi stok barang gudang umum
* Author    : Hardi
* Date      : 17-04-2014
* Issue     : EHJ-1602
**/
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>

<legend class="rim">Pencarian</legend>
    <div class="control-group ">
        <table>
            <tr>
                <td>
                   <?php echo $form->dropDownListRow($model,'ruangan_id',CHtml::listData($model->getRuanganItems(),'ruangan_id','ruangan_nama'),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>     
                </td>
                <td>
                   <?php echo $form->textFieldRow($model,'barang_kode',array('class'=>'span3')); ?>     
                </td>
            </tr>
            <tr>
                <td>
                     <?php echo $form->textFieldRow($model,'inventarisasi_kode',array('class'=>'span3')); ?>     
                </td>
                <td>
                    <?php echo $form->textFieldRow($model,'barang_nama',array('class'=>'span3')); ?>
                </td>
            </tr>
            <tr>
                <td>
                   <?php echo $form->textFieldRow($model,'barang_type',array('class'=>'span3')); ?>     
                </td>
            </tr>
        </table>
    </div>

	<div class="form-actions">
        <?php
            echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
            echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                    Yii::app()->createUrl($this->route), 
                    array('class'=>'btn btn-danger',
                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
	</div>

<?php $this->endWidget(); ?>
<?php
        $controller = Yii::app()->controller->id; 
        $module = Yii::app()->controller->module->id;
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printInformasi');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>