
<?php
$this->breadcrumbs=array(
	'Guinvasetlain Ts'=>array('index'),
	$model->invasetlain_id=>array('view','id'=>$model->invasetlain_id),
	'Update',
);
$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_dataBarang', array('modBarang' => $modBarang ,));
$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Aset Tetap Lainnya', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GUInvasetlainT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GUInvasetlainT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' GUInvasetlainT', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->invasetlain_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Aset Tetap Lainnya', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model,'data'=>$data,'dataAsalAset'=>$dataAsalAset , 'dataLokasi'=>$dataLokasi)); ?>
