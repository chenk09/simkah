<fieldset>
    <legend>Retur Penerimaan Persediaan Barang</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gureturpenerimaan-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
    <table>
        <tr>
            <td>
            <?php if (isset($modTerima)) {
                $this->renderPartial('_dataTerima', array('modTerima'=>$modTerima));
            }?>
            <?php //echo $form->textFieldRow($model,'terimapersediaan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->hiddenField($model, 'is_posting'); ?>
            <?php echo $form->textFieldRow($model,'noreturterima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'tglreturterima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglreturterima', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglreturterima',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)",),
                    ));
                    ?>
                    <?php echo $form->error($model, 'tglreturterima'); ?>
                </div>
            </div>

            <?php echo $form->textFieldRow($model,'alasanreturterima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
        </td>
        <td>
            <?php echo $form->textAreaRow($model,'keterangan_retur',array('rows'=>4, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($model,'totalretur',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'peg_retur_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'peg_mengetahui_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'peg_retur_id', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->hiddenField($model, 'peg_retur_id'); ?>
                    <!--                <div class="input-append" style='display:inline'>-->
                    <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model'=>$model,
                        'attribute' => 'peg_retur_nama',
                        'source' => 'js: function(request, response) {
                                           $.ajax({
                                               url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                                        $(this).val( ui.item.label);
                                                                        return false;
                                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                                        $("#'.Chtml::activeId($model, 'peg_retur_id') . '").val(pegawai_id); 
                                                                        return false;
                                                                    }',
                        ),
                        'htmlOptions' => array(
                            'class'=>'namaPegawai',
                            'onkeypress' => "return $(this).focusNextInputField(event)",
                        ),
                        'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'openDialog("'.Chtml::activeId($model, 'peg_retur_id').'");'),
                    ));
                    ?>
                    <?php echo $form->error($model, 'peg_retur_id'); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'peg_mengetahui_id', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php echo $form->hiddenField($model, 'peg_mengetahui_id'); ?>
                    <!--                <div class="input-append" style='display:inline'>-->
                    <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model'=>$model,
                        'attribute' => 'peg_mengetahui_nama',
                        'source' => 'js: function(request, response) {
                                           $.ajax({
                                               url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                                        $(this).val( ui.item.label);
                                                                        return false;
                                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                                        $("#'.Chtml::activeId($model, 'peg_mengetahui_id') . '").val(pegawai_id); 
                                                                        return false;
                                                                    }',
                        ),
                        'htmlOptions' => array(
                            'class'=>'namaPegawai',
                            'onkeypress' => "return $(this).focusNextInputField(event)",
                        ),
                        'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'openDialog("'.Chtml::activeId($model, 'peg_mengetahui_id').'");'),
                    ));
                    ?>
                    <?php echo $form->error($model, 'peg_mengetahui_id'); ?>
                </div>
            </div>
        </td>
        </tr>
    </table>
            <?php if (isset($modDetails)) {
                echo $form->errorSummary($modDetails);
            }?>
            <?php $this->renderPartial('_tableDetailBarang', array('model'=>$model, 'form'=>$form, 'modDetails'=>$modDetails, 'modPesan'=>$modPesan)); ?>
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	
    <legend class="rim">Jurnal Rekening</legend>
    <table>
        <tr>
            <td width="70%">
                <div>
                    <?php
                        $this->renderPartial('gudangUmum.views.returpenerimaanT.rekening._rowListRekening',
                            array(
                                'form'=>$form,
                            )
                        );
                    ?>
                </div>                
            </td>
        </tr>
    </table>

    <div class="form-actions">
        <div style="float:left;margin-right:6px;">
        <?php //echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
            // Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            // array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php 
            if($model->isNewRecord)
            {
                $this->widget('bootstrap.widgets.BootButtonGroup', array(
                'type'=>'primary',
                'buttons'=>array(
                    array(
                        'label'=>'Simpan',
                        'icon'=>'icon-ok icon-white',
                        'url'=>"#",
                        'htmlOptions'=>
                            array(
                                'onclick'=>'simpanPengeluaran(\'retur\');return false;',
                            )
                   ),
                    array(
                        'label'=>'',
                        'items'=>array(
                            array(
                                'label'=>'Posting',
                                'icon'=>'icon-ok',
                                'url'=>"#",
                                'itemOptions' => array(
                                    'onclick'=>'simpanPengeluaran(\'posting\');return false;'
                                )
                            ),
                        )
                    ),
                    ),
                ));
                echo"</div>";
            }
        ?>

        <?php
            echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), array('type' => 'reset', 'class' => 'btn btn-danger',
            'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
        ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            Yii::app()->createUrl($this->module->id.'/'.terimapersediaanT.'/informasi'), 
            array('class'=>'btn btn-danger',
        'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>

    
	</div>

<?php $this->endWidget(); ?>
</fieldset>

<?php 
$urlAjax = Yii::app()->createUrl('actionAjax/getMutasiBarang');
$urlAjaxStok = Yii::app()->createUrl('actionAjax/getStokBarang');
$notif = Yii::t('mds','Do You want to cancel?');
$js = <<< JS
    
    function batal(obj){
        if(!confirm("${notif}")) {
        return false;
        }else{
        $(obj).parents('tr').remove();
        rename();
        }
    }
    
    function rename(){
        noUrut = 1;
        $('.cancel').each(function(){
            $(this).parents('tr').find('[name*="MutasibrgdetailT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('MutasibrgdetailT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','MutasibrgdetailT['+(noUrut-1)+']'+data[1]);
                }
            });
            noUrut++;
        });        
    }
    
    function cekRetur(obj){
        var terima = $(obj).parents('tr').find('.terima').val();
        var retur = $(obj).val();
        if (retur > terima){
            alert('Jumlah Retur tidak boleh lebih dari '+terima);
            $(obj).val(terima);
            return false;
        }
    }
    
    function openDialog(obj){
        $('#dialogPegawai').attr('parentClick',obj);
        $('#dialogPegawai').dialog('open');   
    }
JS;
Yii::app()->clientScript->registerScript('onhead',$js,  CClientScript::POS_HEAD);
?>

<script type="text/javascript">
    getDataRekeningReturPersediaanBarang();
    function simpanPengeluaran(params)
    {
        // alert(params);
        $('#GUReturpenerimaanT_is_posting').val(params);
        jenis_simpan = params;
        var kosong = "" ;
        var dataKosong = $("#input-pengeluaran").find(".reqForm[value="+ kosong +"]");
        if(dataKosong.length > 0){
            alert('Bagian dengan tanda * harus diisi ');
        }else{
            
            $('.currency').each(
                function(){
                    this.value = unformatNumber(this.value)
                }
            );
            $('#gureturpenerimaan-t-form').submit();
        }
    }

</script>

<?php 
Yii::app()->clientScript->registerScript('onready','
    $("form").submit(function(){
        retur = false;
        idRetur = $("#'.CHtml::activeId($model, 'peg_retur_id').'").val();
        alasan = $("#'.CHtml::activeId($model, 'alasanreturterima').'").val();

        $(".retur").each(function(){
            if ($(this).val() > 0){
                retur = true
            }
        });
        
        if (alasan == ""){
            alert("'.CHtml::encode($model->getAttributeLabel('alasanreturterima')).' harus diisi");
            return false;
        }
        else if (!jQuery.isNumeric(idRetur)){
            alert("'.CHtml::encode($model->getAttributeLabel('peg_retur_id')).' harus diisi");
            return false;
        }
        
        if ($(".cancel").length < 0){
            alert("Detail Barang Harus Diisi");
            return false;
        }
        else if (retur == false){
            alert("'.CHtml::encode($model->getAttributeLabel('jmlretur')).' harus memiliki value yang lebih dari 0");
            return false;
        }
    });
',CClientScript::POS_READY);?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPegawai',
    'options' => array(
        'title' => 'Daftar Pegawai',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPegawai = new GUPegawaiM('search');
$modPegawai->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['GUPegawaiM']))
    $modPegawai->attributes = $_GET['GUPegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawai-m-grid',
    'dataProvider'=>$modPegawai->searchDialog(),
    'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'pegawai_id',
        
            'nama_pegawai',
            'nomorindukpegawai',
                'alamat_pegawai',
        'agama',
            array(
                'name'=>'jeniskelamin',
                'filter'=>  JenisKelamin::items(),
                'value'=>'$data->jeniskelamin',
                ),
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "
                                    var parent = $(\"#dialogPegawai\").attr(\"parentclick\");
                                    $(\"#\"+parent+\"\").val($data->pegawai_id);
                                    $(\"#\"+parent+\"\").parents(\".controls\").find(\".namaPegawai\").val(\"$data->nama_pegawai\");
                                    $(\'#dialogPegawai\').dialog(\'close\');
                                    return false;"))',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>