<?php echo CHtml::css('#tableDetailBarang thead tr th{vertical-align:middle;}'); ?>

<table class="table table-bordered table-condensed" id="tableDetailBarang">
    <thead>
        <tr>
            <th>Kode Barang</th>
            <th>Type Barang</th>
            <th>Nama Barang</th>
            <th>Merk / No Seri</th>
            <th>Ukuran / Bahan Barang</th>
            <th>Harga Netto</th>
            <th>Harga Satuan</th>
            <th>Jumlah Pakai</th>
            <th>Satuan</th>
            <!-- <th>Jumlah Dalam Kemasan</th> -->
            <th>Batal</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if (isset($modDetails)){
        foreach ($modDetails as $i=>$detail){?>
        <?php $modBarang = BarangM::model()->findByPk($detail->barang_id); ?>
            <tr>   
                <td><?php 
                    echo CHtml::activeHiddenField($detail, '['.$i.']barang_id',array('class'=>'barang')); 
                    echo $modBarang->bidang->subkelompok->kelompok->golongan->golongan_nama; 
                    ?>
                </td>
                <td><?php echo $modBarang->bidang->subkelompok->kelompok->kelompok_nama; ?></td>
                <td><?php echo $modBarang->bidang->subkelompok->subkelompok_nama; ?></td>
                <td><?php echo $modBarang->bidang->bidang_nama; ?></td>
                <td><?php echo $modBarang->barang_nama; ?></td>
                <td>
                <?php 
                    echo CHtml::activeTextField($detail, '['.$i.']harganetto', array('class'=>'span1 numbersOnly mutasi', 'onblur'=>'cekStok(this);'));
                    echo '<br/>';
                    echo $form->error($detail, '['.$i.']harganetto');
                ?>
                </td>
                <td>
                <?php 
                    echo CHtml::activeTextField($detail, '['.$i.']hargajual', array('class'=>'span1 numbersOnly mutasi', 'onblur'=>'cekStok(this);'));
                    echo '<br/>';
                    echo $form->error($detail, '['.$i.']hargajual');
                ?>
                </td>
                <td>
                <?php 
                    echo CHtml::activeTextField($detail, '['.$i.']jmlpakai', array('class'=>'span1 numbersOnly qty', 'onblur'=>'cekStok(this);'));
                    echo '<br/>';
                    echo $form->error($detail, '['.$i.']jmlpakai');
                ?>
                </td>
                <td><?php echo CHtml::activeDropDownList($detail, '['.$i.']satuanpakai', Satuanbarang::items(), array('empty'=>'-- Pilih --', 'class'=>'span2')); ?></td>
                <!-- <td><?php //echo CHtml::activeTextField($detail, '['.$i.']jmldlmkemasan', array('class'=>'span1 numbersOnly qty', 'onblur'=>'cekStok(this);'));
                    //echo '<br/>';
                    //echo $form->error($detail, '['.$i.']jmlpakai'); ?></td> -->
                <td><?php echo Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel')); ?></td>
            </tr>   
        <?php }
        }
        ?>
    </tbody>
</table>