<legend class="rim2">Transaksi Pemakaian Barang</legend>
<?php
$this->breadcrumbs=array(
	'Gupemakaianbarang Ts'=>array('index'),
	'Create',
);

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php 
    if(!empty($_GET['id'])){
        
?>
  <div class="alert alert-block alert-success">
      <a class="close" data-dismiss="alert">x</a>
      Data Berhasil Disimpan
  </div>
<?php } ?>

<?php echo $this->renderPartial($this->pathView.'_form', array('model'=>$model, 'modDetails'=>$modDetail)); ?>