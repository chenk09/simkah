<fieldset>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gupemakaianbarang-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary($model); ?>
<table>
    <tr>
        <td width='50%'>
        	<div class="control-group ">
                <?php echo $form->labelEx($model, 'tglpemakaianbrg', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglpemakaianbrg',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)",),
                    ));
                    ?>
                    <?php echo $form->error($model, 'tglpemakaianbrg'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'nopemakaianbrg',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>

            <?php echo $form->textAreaRow($model,'untukkeperluan',array('rows'=>3, 'cols'=>80, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        </td>
        <td>
        	<?php echo $form->textAreaRow($model,'keteranganpakai',array('rows'=>5, 'cols'=>180, 'class'=>'span4', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        </td>
    </tr>
</table>
<legend class="rim">Detail Barang</legend>
<?php $this->renderPartial($this->pathView.'_formDetailBarang', array('model'=>$model, 'form'=>$form, 'modPesan'=>$modPesan)); ?>
<?php $this->renderPartial($this->pathView.'_tableDetailBarang', array('model'=>$model, 'form'=>$form, 'modDetails'=>$modDetails, 'modBeli'=>$modBeli)); ?>
	<div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
            Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
	    <?php
	        echo CHtml::htmlButton(Yii::t('mds', '{icon} Reset', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), array('type' => 'reset', 'class' => 'btn btn-danger',
	            'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
        ?>
    	<?php
			$content = $this->renderPartial('gudangUmum.views.tips.informasi',array(),true);
			$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
		?>
	</div>

</fieldset>
<?php $this->endWidget(); ?>