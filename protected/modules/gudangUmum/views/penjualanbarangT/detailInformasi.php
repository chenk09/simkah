<table class='table'>
    <tr>
        <td>
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('nopenjualan')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->nopenjualan); ?>
            <br />
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('nopemesanan')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->nopemesanan); ?>
            <br />
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('tglpenjualanbrg')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->tglpenjualanbrg); ?>
             <br/>
        </td>
        <td>
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('ruangan_id')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->ruangan->ruangan_nama); ?>
            <br />
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('penerimabarang')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->penerimabarang); ?>
            <br />
            <b><?php echo CHtml::encode($modPenjualanbarang->getAttributeLabel('keterangan')); ?>:</b>
            <?php echo CHtml::encode($modPenjualanbarang->keterangan); ?>
            <br />
        </td>
    </tr>   
</table>

<table id="tablePenjualanbrgdetail" class="table table-striped table-bordered table-condensed">
    <thead>
        <th>No. Urut</th>
        <th>Barang</th>
        <th>Jml Pakai</th>
        <th>Satuan</th>
        <th>Catatan</th>
    </thead>
    <tbody>
    <?php
        $no=1;
        foreach($modDetailPenjualan AS $detail): ?>
            <tr>   
                <td><?php echo $no; ?></td>
                <td><?php echo $detail->barang->barang_nama; ?></td>
                <td><?php echo $detail->jmljual; ?></td>
                <td><?php echo $detail->satuanjual; ?></td>
                <td><?php echo $detail->catatan; ?></td>
            </tr>
    <?php 
        $no++; 
        endforeach;     
    ?>
    </tbody>
</table>