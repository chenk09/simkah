
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GUInvgedungT extends InvgedungT
{
    public $barang_nama;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KabupatenM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(invgedung_tglguna)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('invgedung_id',$this->invgedung_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('LOWER(invgedung_kode)',strtolower($this->invgedung_kode),true);
        $criteria->compare('LOWER(invgedung_noregister)',strtolower($this->invgedung_noregister),true);
        $criteria->compare('LOWER(invgedung_namabrg)',strtolower($this->invgedung_namabrg),true);
        $criteria->compare('LOWER(invgedung_kontruksi)',strtolower($this->invgedung_kontruksi),true);
        $criteria->compare('invgedung_luaslantai',$this->invgedung_luaslantai);
        $criteria->compare('LOWER(invgedung_alamat)',strtolower($this->invgedung_alamat),true);
        $criteria->compare('LOWER(invgedung_tgldokumen)',strtolower($this->invgedung_tgldokumen),true);
        $criteria->compare('LOWER(invgedung_tglguna)',strtolower($this->invgedung_tglguna),true);
        $criteria->compare('LOWER(invgedung_nodokumen)',strtolower($this->invgedung_nodokumen),true);
        $criteria->compare('invgedung_harga',$this->invgedung_harga);
        $criteria->compare('invgedung_akumsusut',$this->invgedung_akumsusut);
        $criteria->compare('LOWER(invgedung_ket)',strtolower($this->invgedung_ket),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

}
?>
