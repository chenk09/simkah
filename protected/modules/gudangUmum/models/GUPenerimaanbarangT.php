<?php
class GUPenerimaanbarangT extends PenerimaanbarangT
{
    public $peg_penerima_nama, $peg_mengetahui_nama, $instalasi_id;
        public $jumlah;
        public $data;
        public $tick;
        public $pilihanx;
    public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchPenerimaanBarang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('date(tglterima)', $this->tglAwal, $this->tglAkhir);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPenerimaanBarangPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('date(tglterima)', $this->tglAwal, $this->tglAkhir);
		$criteria->limit = -1;
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
        public function searchPenerimaanBaranggrafik()
     {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

            $criteria=new CDbCriteria;
            
            $criteria->select = 'count(penerimaanbarang_id) as jumlah, date(tglterima) as data';
            $criteria->group = 'date(tglterima)';
            $criteria->addBetweenCondition('date(tglterima)', $this->tglAwal, $this->tglAkhir);
            

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
}