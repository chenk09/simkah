<?php
/**
* gudangUmum/models/GUPemakaianbrgdetailT.php : Model GUPemakaianbrgdetailT untuk detail transaksi pemakaian barang
* Author    : Hardi
* Date      : 30-04-2014
* Issue     : EHJ-1728
**/
class GUPemakaianbrgdetailT extends PemakaianbrgdetailT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesfarmasiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}   

}