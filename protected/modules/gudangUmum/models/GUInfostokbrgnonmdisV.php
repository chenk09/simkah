<?php
/**
* gudangUmum/models/GUInfostokbrgnonmdisV.php : Model GUInfostokbrgnonmdisV untuk informasi Stok Barang
* Author    : Hardi
* Date      : 17-04-2014
* Issue     : EHJ-1602
**/
class GUInfostokbrgnonmdisV extends InfostokbrgnonmdisV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesfarmasiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
    public function getRuanganItems() {
  		return RuanganM::model()->findAll('ruangan_aktif=TRUE ORDER BY ruangan_nama');
    }   

}