<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GUInvjalanT extends InvjalanT
{
    public $barang_nama;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KabupatenM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }


    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(invjalan_tglguna)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('invjalan_id',$this->invjalan_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('LOWER(invjalan_kode)',strtolower($this->invjalan_kode),true);
        $criteria->compare('LOWER(invjalan_noregister)',strtolower($this->invjalan_noregister),true);
        $criteria->compare('LOWER(invjalan_namabrg)',strtolower($this->invjalan_namabrg),true);
        $criteria->compare('LOWER(invjalan_kontruksi)',strtolower($this->invjalan_kontruksi),true);
        $criteria->compare('LOWER(invjalan_panjang)',strtolower($this->invjalan_panjang),true);
        $criteria->compare('LOWER(invjalan_lebar)',strtolower($this->invjalan_lebar),true);
        $criteria->compare('LOWER(invjalan_luas)',strtolower($this->invjalan_luas),true);
        $criteria->compare('LOWER(invjalan_letak)',strtolower($this->invjalan_letak),true);
        $criteria->compare('LOWER(invjalan_tgldokumen)',strtolower($this->invjalan_tgldokumen),true);
        $criteria->compare('LOWER(invjalan_tglguna)',strtolower($this->invjalan_tglguna),true);
        $criteria->compare('LOWER(invjalan_nodokumen)',strtolower($this->invjalan_nodokumen),true);
        $criteria->compare('LOWER(invjalan_statustanah)',strtolower($this->invjalan_statustanah),true);
        $criteria->compare('LOWER(invjalan_keadaaan)',strtolower($this->invjalan_keadaaan),true);
        $criteria->compare('invjalan_harga',$this->invjalan_harga);
        $criteria->compare('invjalan_akumsusut',$this->invjalan_akumsusut);
        $criteria->compare('LOWER(invjalan_ket)',strtolower($this->invjalan_ket),true);
        $criteria->compare('LOWER(craete_time)',strtolower($this->craete_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }


}
?>
