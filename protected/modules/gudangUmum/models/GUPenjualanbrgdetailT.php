<?php
/**
* gudangUmum/models/GUPenjualanbrgdetailT.php : Model GUPenjualanbrgdetailT untuk transaksi Penjualan barang
* Author    : Hardi
* Date      : 01-05-2014
* Issue     : EHJ-1729
**/
class GUPenjualanbrgdetailT extends PenjualanbrgdetailT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesfarmasiV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}   

}