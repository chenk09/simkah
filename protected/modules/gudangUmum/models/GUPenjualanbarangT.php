<?php
/**
* gudangUmum/models/GUPenjualanbarangT.php : Model GUPenjualanbarangT untuk transaksi Penjualan barang
* Author    : Hardi
* Date      : 01-05-2014
* Issue     : EHJ-1729
**/
class GUPenjualanbarangT extends PenjualanbarangT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesfarmasiV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}   

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('penjualanbarang_id',$this->penjualanbarang_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->addBetweenCondition('tglpenjualanbrg',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(nopenjualan)',strtolower($this->nopenjualan),true);
		$criteria->compare('LOWER(nopemesanan)',strtolower($this->nopemesanan),true);
		$criteria->compare('LOWER(unitpemesan)',strtolower($this->unitpemesan),true);
		$criteria->compare('LOWER(penerimabarang)',strtolower($this->penerimabarang),true);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('profilepemesan_id',$this->profilepemesan_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}