<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GUInvtanahT extends InvtanahT
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KabupatenM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function getTahunNama(){
        return $this->invtanah_thnpengadaan.PHP_EOL."".$this->invtanah_tglguna;
    }
    public function getSertifikat(){
        return $this->invtanah_nosertifikat.'--'.$this->invtanah_tglsertifikat;
    }

    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(invtanah_tglguna)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('invtanah_id',$this->invtanah_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('LOWER(invtanah_kode)',strtolower($this->invtanah_kode),true);
        $criteria->compare('LOWER(invtanah_noregister)',strtolower($this->invtanah_noregister),true);
        $criteria->compare('LOWER(invtanah_namabrg)',strtolower($this->invtanah_namabrg),true);
        $criteria->compare('LOWER(invtanah_luas)',strtolower($this->invtanah_luas),true);
        $criteria->compare('LOWER(invtanah_thnpengadaan)',strtolower($this->invtanah_thnpengadaan),true);
        $criteria->compare('LOWER(invtanah_tglguna)',strtolower($this->invtanah_tglguna),true);
        $criteria->compare('LOWER(invtanah_alamat)',strtolower($this->invtanah_alamat),true);
        $criteria->compare('LOWER(invtanah_status)',strtolower($this->invtanah_status),true);
        $criteria->compare('LOWER(invtanah_tglsertifikat)',strtolower($this->invtanah_tglsertifikat),true);
        $criteria->compare('LOWER(invtanah_nosertifikat)',strtolower($this->invtanah_nosertifikat),true);
        $criteria->compare('LOWER(invtanah_penggunaan)',strtolower($this->invtanah_penggunaan),true);
        $criteria->compare('invtanah_harga',$this->invtanah_harga);
        $criteria->compare('LOWER(invtanah_ket)',strtolower($this->invtanah_ket),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
?>
