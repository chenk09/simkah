<?php

class GUMutasibrgT extends MutasibrgT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        // $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('mutasibrg_id', $this->mutasibrg_id);
        $criteria->compare('pesanbarang_id', $this->pesanbarang_id);
		$criteria->compare('LOWER(tglmutasibrg)',strtolower($this->tglmutasibrg),true);
        $criteria->compare('LOWER(nomutasibrg)', strtolower($this->nomutasibrg), true);
        $criteria->compare('LOWER(keterangan_mutasi)', strtolower($this->keterangan_mutasi), true);
        $criteria->compare('pegpengirim_id', $this->pegpengirim_id);
        $criteria->compare('totalhargamutasi', $this->totalhargamutasi);
        $criteria->compare('pegmengetahui_id', $this->pegmengetahui_id);
        $criteria->compare('ruangantujuan_id',$this->ruangantujuan_id);
        // $criteria->compare('ruangantujuan_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(update_time)', strtolower($this->update_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(update_loginpemakai_id)', strtolower($this->update_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchInformasiGudang() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        // $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('mutasibrg_id', $this->mutasibrg_id);
        $criteria->compare('pesanbarang_id', $this->pesanbarang_id);
		$criteria->compare('LOWER(tglmutasibrg)',strtolower($this->tglmutasibrg),true);
        $criteria->compare('LOWER(nomutasibrg)', strtolower($this->nomutasibrg), true);
        $criteria->compare('LOWER(keterangan_mutasi)', strtolower($this->keterangan_mutasi), true);
        $criteria->compare('pegpengirim_id', $this->pegpengirim_id);
        $criteria->compare('totalhargamutasi', $this->totalhargamutasi);
        $criteria->compare('pegmengetahui_id', $this->pegmengetahui_id);
        $criteria->compare('ruangantujuan_id', $this->ruangantujuan_id);
        $criteria->compare('LOWER(create_time)', strtolower($this->create_time), true);
        $criteria->compare('LOWER(update_time)', strtolower($this->update_time), true);
        $criteria->compare('LOWER(create_loginpemakai_id)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('LOWER(update_loginpemakai_id)', strtolower($this->update_loginpemakai_id), true);
        $criteria->compare('LOWER(create_ruangan)', strtolower($this->create_ruangan), true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

}