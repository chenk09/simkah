
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GUInvasetlainT extends InvasetlainT
{
    public $barang_nama;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KabupatenM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(invasetlain_tglguna)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('invasetlain_id',$this->invasetlain_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('LOWER(invasetlain_kode)',strtolower($this->invasetlain_kode),true);
        $criteria->compare('LOWER(invasetlain_noregister)',strtolower($this->invasetlain_noregister),true);
        $criteria->compare('LOWER(invasetlain_namabrg)',strtolower($this->invasetlain_namabrg),true);
        $criteria->compare('LOWER(invasetlain_judulbuku)',strtolower($this->invasetlain_judulbuku),true);
        $criteria->compare('LOWER(invasetlain_spesifikasibuku)',strtolower($this->invasetlain_spesifikasibuku),true);
        $criteria->compare('LOWER(invasetlain_asalkesenian)',strtolower($this->invasetlain_asalkesenian),true);
        $criteria->compare('invasetlain_jumlah',$this->invasetlain_jumlah);
        $criteria->compare('LOWER(invasetlain_thncetak)',strtolower($this->invasetlain_thncetak),true);
        $criteria->compare('invasetlain_harga',$this->invasetlain_harga);
        $criteria->compare('LOWER(invasetlain_tglguna)',strtolower($this->invasetlain_tglguna),true);
        $criteria->compare('invasetlain_akumsusut',$this->invasetlain_akumsusut);
        $criteria->compare('LOWER(invasetlain_ket)',strtolower($this->invasetlain_ket),true);
        $criteria->compare('LOWER(invasetlain_penciptakesenian)',strtolower($this->invasetlain_penciptakesenian),true);
        $criteria->compare('LOWER(invasetlain_bahankesenian)',strtolower($this->invasetlain_bahankesenian),true);
        $criteria->compare('LOWER(invasetlain_jenishewan_tum)',strtolower($this->invasetlain_jenishewan_tum),true);
        $criteria->compare('LOWER(invasetlain_ukuranhewan_tum)',strtolower($this->invasetlain_ukuranhewan_tum),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
?>
