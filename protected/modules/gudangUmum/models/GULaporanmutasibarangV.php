<?php

class GULaporanmutasibarangV extends LaporanmutasibarangV{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
		      $criteria->compare('mutasibrg_id',$this->mutasibrg_id);
        $criteria->compare('nomutasibrg',$this->nomutasibrg,true);
        $criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
        $criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
        $criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
        $criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('qty_mutasi',$this->qty_mutasi);
        $criteria->compare('totalhargamutasi',$this->totalhargamutasi);
        $criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
        $criteria->compare('pesanbarang_id',$this->pesanbarang_id);
        $criteria->compare('nopemesanan',$this->nopemesanan,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('instalasi_nama',$this->instalasi_nama,true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('ruangan_nama',$this->ruangan_nama,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getNamaModel(){
            return __CLASS__;
        }
        
        public function searchMutasiBarang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

            $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
              $criteria->compare('mutasibrg_id',$this->mutasibrg_id);
        $criteria->compare('nomutasibrg',$this->nomutasibrg,true);
        $criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
        $criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
        $criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
        $criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('qty_mutasi',$this->qty_mutasi);
        $criteria->compare('totalhargamutasi',$this->totalhargamutasi);
        $criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
        $criteria->compare('pesanbarang_id',$this->pesanbarang_id);
        $criteria->compare('nopemesanan',$this->nopemesanan,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('instalasi_nama',$this->instalasi_nama,true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('ruangan_nama',$this->ruangan_nama,true);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function searchMutasiBarangPrint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('mutasibrg_id',$this->mutasibrg_id);
        $criteria->compare('nomutasibrg',$this->nomutasibrg,true);
        $criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
        $criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
        $criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
        $criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('qty_mutasi',$this->qty_mutasi);
        $criteria->compare('totalhargamutasi',$this->totalhargamutasi);
        $criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
        $criteria->compare('pesanbarang_id',$this->pesanbarang_id);
        $criteria->compare('nopemesanan',$this->nopemesanan,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('instalasi_nama',$this->instalasi_nama,true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('ruangan_nama',$this->ruangan_nama,true);

                $criteria->limit = -1;
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
           public function searchTable() {
            $criteria = new CDbCriteria();
            $criteria = $this->functionCriteria();
            $criteria->order = 'tglmutasibrg';

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
         }
     
        protected function functionCriteria() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('mutasibrg_id',$this->mutasibrg_id);
        $criteria->compare('nomutasibrg',$this->nomutasibrg,true);
        $criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
        $criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
        $criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
        $criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('qty_mutasi',$this->qty_mutasi);
        $criteria->compare('totalhargamutasi',$this->totalhargamutasi);
        $criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
        $criteria->compare('pesanbarang_id',$this->pesanbarang_id);
        $criteria->compare('nopemesanan',$this->nopemesanan,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('instalasi_nama',$this->instalasi_nama,true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('ruangan_nama',$this->ruangan_nama,true);
        return $criteria;
    }
     
     public function searchMutasiBaranggrafik()
     {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

            $criteria=new CDbCriteria;
            
            $criteria->select = 'count(mutasibrg_id) as jumlah, date(tglmutasibrg) as data';
            $criteria->group = 'date(tglmutasibrg)';
                 $criteria->addBetweenCondition('date(tglmutasibrg)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('mutasibrg_id',$this->mutasibrg_id);
        $criteria->compare('nomutasibrg',$this->nomutasibrg,true);
        $criteria->compare('karyawanpemesan_id',$this->karyawanpemesan_id);
        $criteria->compare('karyawanpemesan_gelardepan',$this->karyawanpemesan_gelardepan,true);
        $criteria->compare('karyawanpemesan_nama',$this->karyawanpemesan_nama,true);
        $criteria->compare('karyawan_gelarbelakang',$this->karyawan_gelarbelakang,true);
        $criteria->compare('golongan_id',$this->golongan_id);
        $criteria->compare('golongan_kode',$this->golongan_kode,true);
        $criteria->compare('golongan_nama',$this->golongan_nama,true);
        $criteria->compare('kelompok_id',$this->kelompok_id);
        $criteria->compare('kelompok_kode',$this->kelompok_kode,true);
        $criteria->compare('kelompok_nama',$this->kelompok_nama,true);
        $criteria->compare('subkelompok_id',$this->subkelompok_id);
        $criteria->compare('subkelompok_kode',$this->subkelompok_kode,true);
        $criteria->compare('subkelompok_nama',$this->subkelompok_nama,true);
        $criteria->compare('bidang_id',$this->bidang_id);
        $criteria->compare('bidang_kode',$this->bidang_kode,true);
        $criteria->compare('bidang_nama',$this->bidang_nama,true);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('barang_kode',$this->barang_kode,true);
        $criteria->compare('barang_nama',$this->barang_nama,true);
        $criteria->compare('barang_type',$this->barang_type,true);
        $criteria->compare('barang_merk',$this->barang_merk,true);
        $criteria->compare('barang_noseri',$this->barang_noseri,true);
        $criteria->compare('barang_ukuran',$this->barang_ukuran,true);
        $criteria->compare('barang_bahan',$this->barang_bahan,true);
        $criteria->compare('barang_thnbeli',$this->barang_thnbeli,true);
        $criteria->compare('barang_warna',$this->barang_warna,true);
        $criteria->compare('barang_statusregister',$this->barang_statusregister);
        $criteria->compare('barang_ekonomis_thn',$this->barang_ekonomis_thn);
        $criteria->compare('qty_mutasi',$this->qty_mutasi);
        $criteria->compare('totalhargamutasi',$this->totalhargamutasi);
        $criteria->compare('keterangan_mutasi',$this->keterangan_mutasi,true);
        $criteria->compare('pesanbarang_id',$this->pesanbarang_id);
        $criteria->compare('nopemesanan',$this->nopemesanan,true);
        $criteria->compare('instalasi_id',$this->instalasi_id);
        $criteria->compare('instalasi_nama',$this->instalasi_nama,true);
        $criteria->compare('ruangan_id',$this->ruangan_id);
        $criteria->compare('ruangan_nama',$this->ruangan_nama,true);

                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
               // $criteria->limit=-1; 

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
      public function Criteria()
                {
                    $criteria = new CDbCriteria;
                    
                    $criteria->compare('mutasibrg_id',$this->terimapersediaan_id);
                    $criteria->addBetweenCondition('tglmutasibrg',$this->tglAwal,$this->tglAkhir);
                    return $criteria;
                }  
      public function getTotalharga()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(totalhargamutasi)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
}

?>
