<?php
/**
* gudangUmum/models/GUPemakaianbarangT.php : Model GUPemakaianbarangT untuk transaksi pemakaian barang
* Author    : Hardi
* Date      : 30-04-2014
* Issue     : EHJ-1728
**/
class GUPemakaianbarangT extends PemakaianbarangT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesfarmasiV the static model class
	 */
	public $tglAwal, $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pemakaianbarang_id',$this->pemakaianbarang_id);
		$criteria->compare('pindahkamar_id',$this->pindahkamar_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->addBetweenCondition('tglpemakaianbrg',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('LOWER(nopemakaianbrg)',strtolower($this->nopemakaianbrg),true);
		$criteria->compare('LOWER(untukkeperluan)',strtolower($this->untukkeperluan),true);
		$criteria->compare('LOWER(keteranganpakai)',strtolower($this->keteranganpakai),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id);
		$criteria->compare('create_ruangan',$this->create_ruangan);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}   

}