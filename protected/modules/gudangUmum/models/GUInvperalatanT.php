
<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class GUInvperalatanT extends InvperalatanT
{
    public $barang_nama, $kd_urutan, $jenisinventaris, $tglpenyusutanperalatan;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KabupatenM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    public function searchInformasi()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(invperalatan_tglguna)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('invperalatan_id',$this->invperalatan_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('LOWER(invperalatan_kode)',strtolower($this->invperalatan_kode),true);
        $criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
        $criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
        $criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
        $criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
        $criteria->compare('LOWER(invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
        $criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
        $criteria->compare('LOWER(invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
        $criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
        $criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
        $criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
        $criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
        $criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
        $criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
        $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function searchKendaraan()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
        $criteria->addBetweenCondition('date(t.create_time)', $this->tglAwal, $this->tglAkhir);
        // $criteria->select = 't.*, barang_m.*, asalaset_m.*, lokasiaset_m.*, pemilikbarang_m.*';
        $criteria->compare('invperalatan_id',$this->invperalatan_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('LOWER(invperalatan_kode)',strtolower($this->invperalatan_kode),true);
        $criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
        $criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
        $criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
        $criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
        $criteria->compare('LOWER(invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
        $criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
        $criteria->compare('LOWER(invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
        $criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
        $criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
        $criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
        $criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
        $criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
        $criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);
        $criteria->compare('umurekonomis',$this->umurekonomis);
        $criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
        $criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
        $criteria->compare('hargajualaktiva',$this->hargajualaktiva);
        $criteria->compare('kerugian',$this->kerugian);
        $criteria->compare('keuntungan',$this->keuntungan);
        // $criteria->join = 'LEFT JOIN barang_m on barang_m.barang_id = t.barang_id 
        //     LEFT JOIN asalaset_m on asalaset_m.asalaset_id = t.asalaset_id
        //     LEFT JOIN lokasiaset_m on lokasiaset_m.lokasi_id = t.lokasi_id
        //     LEFT JOIN pemilikbarang_m on pemilikbarang_m.pemilikbarang_id = t.pemilikbarang_id';
            $criteria->addCondition("t.tipepenghapusan is null AND split_part(t.invperalatan_noregister, '-',3)='03'");

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }

    public function searchAsetlain()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria=new CDbCriteria;
//        $criteria->addBetweenCondition('date(t.create_time)', $this->tglAwal, $this->tglAkhir);
        // $criteria->select = 't.*, barang_m.*, asalaset_m.*, lokasiaset_m.*, pemilikbarang_m.*';
        $criteria->compare('invperalatan_id',$this->invperalatan_id);
        $criteria->compare('lokasi_id',$this->lokasi_id);
        $criteria->compare('barang_id',$this->barang_id);
        $criteria->compare('asalaset_id',$this->asalaset_id);
        $criteria->compare('pemilikbarang_id',$this->pemilikbarang_id);
        $criteria->compare('LOWER(invperalatan_kode)',strtolower($this->invperalatan_kode),true);
        $criteria->compare('LOWER(invperalatan_noregister)',strtolower($this->invperalatan_noregister),true);
        $criteria->compare('LOWER(invperalatan_namabrg)',strtolower($this->invperalatan_namabrg),true);
        $criteria->compare('LOWER(invperalatan_merk)',strtolower($this->invperalatan_merk),true);
        $criteria->compare('LOWER(invperalatan_ukuran)',strtolower($this->invperalatan_ukuran),true);
        $criteria->compare('LOWER(invperalatan_bahan)',strtolower($this->invperalatan_bahan),true);
        $criteria->compare('LOWER(invperalatan_thnpembelian)',strtolower($this->invperalatan_thnpembelian),true);
        $criteria->compare('LOWER(invperalatan_tglguna)',strtolower($this->invperalatan_tglguna),true);
        $criteria->compare('LOWER(invperalatan_nopabrik)',strtolower($this->invperalatan_nopabrik),true);
        $criteria->compare('LOWER(invperalatan_norangka)',strtolower($this->invperalatan_norangka),true);
        $criteria->compare('LOWER(invperalatan_nomesin)',strtolower($this->invperalatan_nomesin),true);
        $criteria->compare('LOWER(invperalatan_nopolisi)',strtolower($this->invperalatan_nopolisi),true);
        $criteria->compare('LOWER(invperalatan_nobpkb)',strtolower($this->invperalatan_nobpkb),true);
        $criteria->compare('invperalatan_harga',$this->invperalatan_harga);
        $criteria->compare('invperalatan_akumsusut',$this->invperalatan_akumsusut);
        $criteria->compare('LOWER(invperalatan_ket)',strtolower($this->invperalatan_ket),true);
        $criteria->compare('LOWER(invperalatan_kapasitasrata)',strtolower($this->invperalatan_kapasitasrata),true);
        $criteria->compare('invperalatan_ijinoperasional',$this->invperalatan_ijinoperasional);
        $criteria->compare('LOWER(invperalatan_serftkkalibrasi)',strtolower($this->invperalatan_serftkkalibrasi),true);
        $criteria->compare('invperalatan_umurekonomis',$this->invperalatan_umurekonomis);
        $criteria->compare('LOWER(invperalatan_keadaan)',strtolower($this->invperalatan_keadaan),true);
        $criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
        $criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
        $criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
        $criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
        $criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('terimapersdetail_id',$this->terimapersdetail_id);
        $criteria->compare('umurekonomis',$this->umurekonomis);
        $criteria->compare('LOWER(tglpenghapusan)',strtolower($this->tglpenghapusan),true);
        $criteria->compare('LOWER(tipepenghapusan)',strtolower($this->tipepenghapusan),true);
        $criteria->compare('hargajualaktiva',$this->hargajualaktiva);
        $criteria->compare('kerugian',$this->kerugian);
        $criteria->compare('keuntungan',$this->keuntungan);
        // $criteria->join = 'LEFT JOIN barang_m on barang_m.barang_id = t.barang_id 
        //     LEFT JOIN asalaset_m on asalaset_m.asalaset_id = t.asalaset_id
        //     LEFT JOIN lokasiaset_m on lokasiaset_m.lokasi_id = t.lokasi_id
        //     LEFT JOIN pemilikbarang_m on pemilikbarang_m.pemilikbarang_id = t.pemilikbarang_id';
            $criteria->addCondition("t.tipepenghapusan is null AND split_part(t.invperalatan_noregister, '-',3)='05'");

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
}
?>
