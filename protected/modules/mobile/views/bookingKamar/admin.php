<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ppbooking-kamar-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo CHtml::hiddenField('statuta','');?>
<fieldset>
    <legend class="rim2">Informasi Pesan Kamar</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ppbooking-kamar-t-grid',
	'dataProvider'=>$model->searchBooking(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'bookingkamar_id',
		
		'tgltransaksibooking',
                array(
                        'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'CHtml::hiddenField("BookingkamarT[$data->bookingkamar_id][idBooking]", $data->bookingkamar_id, array("id"=>"idBooking","class"=>"span3"))."".$data->pasien->pendaftaran->no_pendaftaran',
                ),
                array(
                        'name'=>'tglbookingkamar',
                        'type'=>'raw',
                        'value'=>'CHtml::hiddenField("BookingkamarT[$data->tglbookingkamar][tglbookingkamar]", $data->tglbookingkamar, array("id"=>"tglbookingkamar","class"=>"span3"))."".$data->tglbookingkamar',
                ),
//                'tglbookingkamar',
                  array(
                   'name'=>'pasien_id',
                   'type'=>'raw',
                   'value'=>'(!empty($data->pasien_id) ? CHtml::link("<i class=\'icon-user\'></i> ".$data->pasien->no_rekam_medik, "javascript:daftarKeRI(\'$data->pasien_id\',\'$data->pendaftaran_id\',\'$data->bookingkamar_id\');",array("id"=>"$data->pasien_id","title"=>"Klik Untuk Mendaftarkan ke Rawat Inap")) : "-") ',
                   'htmlOptions'=>array('style'=>'text-align: center')
                ),
               'NamaNamaBIN',
                array(
                        'name'=>'ruangan_id',
                        'type'=>'raw',
                        'value'=>'$data->ruangan->ruangan_nama',
                ),
                array(
                        'name'=>'kamarruangan_id',
                        'type'=>'raw',
                        'value'=>'$data->kamarruangan->kamarruangan_nokamar',
                ),
                array(
                        'name'=>'kelaspelayanan_id',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan->kelaspelayanan_nama',
                ),
//                'bookingkamar_no',
                'statusbooking',
                array(
                    'header'=>'Status Konfirmasi',
//                    'name'=>'statuskonfirmasi',
                    'type'=>'raw',
                    'value'=>'($data->statuskonfirmasi != "BATAL BOOKING" OR $data->statuskonfirmasi == "") ? "$data->statuskonfirmasi".CHtml::link("<i class=icon-pencil></i>","",array("href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Status Konfirmasi Booking Kamar","onclick"=>"{buatSessionUbahStatusKonfirmasiBooking($data->bookingkamar_id); ubahStatusKonfirmasiBooking(); $(\'#dialogUbahStatusKonfirmasiBooking\').dialog(\'open\');}return false;")) : "$data->statuskonfirmasi"',
                ),
		array(
                        'header'=>Yii::t('zii','View'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{view}',
		),
		array(
                        'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{update}',
                        'buttons'=>array(
                            'update' => array (
                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                                        ),
                         ),
		),
		array(
                        'header'=>Yii::t('zii','Batal'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{delete}',
                        'buttons'=>array(
                                        'delete'=> array(
                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
                                        ),
                        )
		),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<div class="search-form" style="display:block">
<?php $this->renderPartial($this->pathView.'_search',array(
	'model'=>$model,
)); ?>
</div>

</fieldset>
<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
      //  $this->widget('TipsMasterData',array('type'=>'admin'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
      //  $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
       $urlPendaftaranRI=Yii::app()->createAbsoluteUrl($module.'/Pendaftaran/RawatInap');


$js = <<< JSCRIPT

function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#ppbuat-janji-poli-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

function daftarKeRI(pasien_id,pendaftaran_id,bookingkamar_id)
{
    $('#pasien_id').val(pasien_id);
    $('#pendaftaran_id').val(pendaftaran_id);
    $('#bookingkamar_id').val(bookingkamar_id);
    $('#form_hidden').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hidden',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>$urlPendaftaranRI,
        'htmlOptions'=>array('target'=>'_new'),
)); ?>
    <?php echo CHtml::hiddenField('pasien_id','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('pendaftaran_id','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('bookingkamar_id','',array('readonly'=>true));?>
<?php $this->endWidget(); ?>

<?php 
// Dialog untuk ubah status konfirmasi booking =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahStatusKonfirmasiBooking',
    'options'=>array(
        'title'=>'Ubah Status Konfirmasi Booking Kamar',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status konfirmasibooking dialog =============================
?>

<script type="text/javascript">
setInterval(   // fungsi untuk menjalankan suatu fungsi berdasarkan waktu
    function(){
        $.fn.yiiGridView.update('ppbooking-kamar-t-grid', {   // fungsi untuk me-update data pada Cgridview yang memiliki id=category_grid
            data: $('#ppbooking-kamar-t-search').serialize()
        });
        return false;
    }, 
 20000  // fungsi di eksekusi setiap 5 detik sekali
);
</script>
<script>
        function ubahStatusKonfirmasiBooking()
{
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/ubahStatusKonfirmasiBooking'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogUbahStatusKonfirmasiBooking div.divForForm').html(data.div);
                    $('#dialogUbahStatusKonfirmasiBooking div.divForForm form').submit(ubahStatusKonfirmasiBooking);
                    
                }
                else
                {
                    $('#dialogUbahStatusKonfirmasiBooking div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('ppbooking-kamar-t-grid');
                    setTimeout(\"$('#dialogUbahStatusKonfirmasiBooking').dialog('close') \",1000);
                }
 
            } ",
    ))
?>;
    return false; 
}
    
    
    
    
</script>
<?php
$urlSessionUbahStatus = Yii::app()->createUrl('ActionAjax/buatSessionUbahStatusKonfirmasiBooking ');
$jscript = <<< JS
function buatSessionUbahStatusKonfirmasiBooking(idBooking)
{
    var answer = confirm('Yakin Akan Merubah Status Booking Kamar Pasien?');
        if (answer){
            $.post("${urlSessionUbahStatus}", {idBooking: idBooking },
                function(data){
                    'sukses';
            }, "json");
        }
        else
        {
          
        }
}
JS;
Yii::app()->clientScript->registerScript('jsBookingKamar',$jscript, CClientScript::POS_BEGIN);
?>

<script type="text/javascript">
  setInterval(
     function(){
        var statusnya=$("#statuta").val(); // mengetahui status apakah sedang ada fungsi autosave yang sedang berjalan
        if(statusnya !="on")  // jika tidak ada fungsi yang berjalan, maka jalankan fungsi
        {
//           $("#btn-save").attr("disabled",true);  // selama fungsi berjalan, user tidak bisa menekan tombol save
           var tglbookingkamar = $("#tglbookingkamar").val();  // menangkap nilai dari form input
           var idBooking = $("#idBooking").val();  // menangkap nilai dari form input
//           alert(tglbookingkamar);
           $.ajax({
              url: "<?php echo Yii::app()->createUrl('pendaftaranPenjadwalan/bookingKamarT/updateStatusKonfirmasi')?>",  // memanggil sebuah fungsi untuk autosave
              type:"post",
              dataType :"json",
              data:{"tglbookingkamar":tglbookingkamar,"idBooking":idBooking},
              beforeSend: function() {
                    $("#statuta").val("on");
              },
              success : function(data){
                    $("#statuta").val("off");
                    if(data.satu.length > 0){
                       window.location=data.satu;  // jika data berhasil disimpan, maka akan redirect
                    }
                    else{
//                       $("#btn-save").attr("disabled",false);
                    }
              },
          });
       }
    }, 
    200000  // operasi auto save dilakukan 2 jam  sekali
  );
</script>