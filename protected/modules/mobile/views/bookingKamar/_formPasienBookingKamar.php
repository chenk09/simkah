
<fieldset id='fieldsetPasien'>
    <legend class="rim">Data Pasien</legend>
    <?php //echo $form->textFieldRow($modPasien,'no_rekam_medik'); ?>
	
	<table width="1039" border="0">
  <tr>
    <td>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'nama_pasien', array('class'=>'control-label')) ?>
        <div class="controls inline">
            
            <?php echo $form->dropDownList($modPasien,'namadepan', NamaDepan::items(),  
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                )); ?>   
            <?php echo $form->textField($modPasien,'nama_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2','placeholder'=>'Nama Pasien')); ?>            

            <?php echo $form->error($modPasien, 'namadepan'); ?><?php echo $form->error($modPasien, 'nama_pasien'); ?>
        </div>
    </div>
    <?php echo $form->textFieldRow($modPasien,'tempat_lahir', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Tempat Lahir')); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($modPasien,'tanggal_lahir', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php   
                    $this->widget('MyDateTimePicker',array(
                                    'model'=>$modPasien,
                                    'attribute'=>'tanggal_lahir',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                        //
                                        'onkeypress'=>"js:function(){getUmur(this);}",
                                        'onSelect'=>'js:function(){getUmur(this);}',
                                        'yearRange'=> "-60:+0",
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
            )); ?>
            <?php echo $form->error($modPasien, 'tanggal_lahir'); ?>
        </div>
    </div>
    
    <?php //echo $form->textFieldRow($model,'umur', array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')); ?>
    <div class="control-group ">
        <?php echo $form->labelEx($model,'umur', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php
                $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'umur',
                'mask' => '99 Thn 99 Bln 99 Hr',
                'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)','placeholder'=>'Umur Pasien')
                ));
                ?>
            <?php echo $form->error($model, 'umur'); ?>
        </div>
    </div>
    <?php echo $form->radioButtonListInlineRow($modPasien, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    
    <?php echo $form->textAreaRow($modPasien,'alamat_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat Pasien')); ?>
	</td>
  </tr>
</table>
  
</fieldset>
<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlGetDaerah = Yii::app()->createUrl('ActionAjax/getListDaerahPasien');
$idTagUmur = CHtml::activeId($model,'umur');
$js = <<< JS
function enableInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('disabled','true');
        $('#detail_data_pasien').slideUp(500);
        $('#cex_detaildatapasien').removeAttr('checked','checked');
    }
    else {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('disabled');
        $('#detail_data_pasien').slideDown(500);
        $('#cex_detaildatapasien').attr('checked','checked');
    }
}

function getTglLahir(obj)
{
    var str = obj.value;
    obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{umur: obj.value},
        function(data){
           $('#PPPasienM_tanggal_lahir').val(data.tglLahir); 
    },"json");
}

function getUmur(obj)
{
    //alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){

           $('#PPPendaftaranRj_umur').val(data.umur); 
           $('#PPPendaftaranMp_umur').val(data.umur); 
           $('#PPPendaftaranRd_umur').val(data.umur); 

           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function loadUmur(tglLahir)
{
    $.post("${urlGetUmur}",{tglLahir: tglLahir},
        function(data){
           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function setJenisKelaminPasien(jenisKelamin)
{
    $('input[name="PPPasienM[jeniskelamin]"]').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
}

function setRhesusPasien(rhesus)
{
    $('input[name="PPPasienM[rhesus]"]').each(function(){
            if(this.value == rhesus)
                $(this).attr('checked',true);
        }
    );
}

function loadDaerahPasien(idProp,idKab,idKec,idKel)
{
    $.post("${urlGetDaerah}", { idProp: idProp, idKab: idKab, idKec: idKec, idKel: idKel },
        function(data){
            $('#PPPasienM_propinsi_id').html(data.listPropinsi);
            $('#PPPasienM_kabupaten_id').html(data.listKabupaten);
            $('#PPPasienM_kecamatan_id').html(data.listKecamatan);
            $('#PPPasienM_kelurahan_id').html(data.listKelurahan);
    }, "json");
}

function clearKecamatan()
{
    $('#PPPasienM_kecamatan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}

function clearKelurahan()
{
    $('#PPPasienM_kelurahan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_HEAD);

$enableInputPasien = ($model->isPasienLama) ? 1 : 0;
$js = <<< JS
if(${enableInputPasien}) {
    $('#fieldsetPasien input').attr('disabled','true');
    $('#fieldsetPasien select').attr('disabled','true');
    $('#fieldsetDetailPasien input').attr('disabled','true');
    $('#fieldsetDetailPasien select').attr('disabled','true');
    $('#PPPasienM_no_rekam_medik').removeAttr('disabled');
    $('#controlNoRekamMedik button').removeAttr('disabled');
    $('#fieldsetPasien button').attr('disabled','true');
}
else {
    $('#fieldsetPasien input').removeAttr('disabled');
    $('#fieldsetPasien select').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#PPPasienM_no_rekam_medik').attr('disabled','true');
    $('#controlNoRekamMedik button').attr('disabled','true');
    $('#fieldsetPasien button').removeAttr('disabled');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_READY);
?>


<?php Yii::app()->clientScript->registerScript('detail_data_pasien',"
    $('#detail_data_pasien').hide();
    $('#cex_detaildatapasien').change(function(){
        if ($(this).is(':checked')){
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').removeAttr('disabled');
                $('#fieldsetDetailPasien select').removeAttr('disabled');
        }else{
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').attr('disabled','true');
                $('#fieldsetDetailPasien select').attr('disabled','true');
                $('#fieldsetDetailPasien input').attr('value','');
                $('#fieldsetDetailPasien select').attr('value','');
        }
        $('#detail_data_pasien').slideToggle(500);
    });
");
$js = <<< JS
$('.numberOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>