<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'ppinformasiantrianpasien-search',
    'type' => 'horizontal',
        ));
?>

<style>
    #ruangan label{
        width: 200px;
            display:inline-block;
        }
</style>
<fieldset>
    <legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
    <table class="table-condensed">
            <tr>
                <td>
                    <?php echo $form->dropDownListRow($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'class' => 'span3', 'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
                                                                'update' => '#ruangan',  //selector to update
                                                            ),)); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'ruangan_id', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <div id='ruangan'>
                                <label> Data Tidak Ditemukan</label>
                            <?php //echo $form->checkBoxList($model, 'ruangan_id', array(), array('empty'=>'-- Pilih --', 'class' => 'span5')); ?>
                                </div>
                        </div>
                    </div>
                    
                </td>
            </tr>
    </table>
</fieldset>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('informasiAntrian/index'), array('class'=>'btn btn-danger')); ?>
</div>

<?php $this->endWidget(); ?>