<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ppinformasiantrianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<fieldset>
    <legend class="rim2">Informasi Antrian Pasien</legend>
</fieldset>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ppinformasiantrianpasien-grid',
	'dataProvider'=>$model->searchTable(),
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
            'header'=>'Nama Instalasi',
            'value'=>'$data->instalasi_nama',
        ),
        array(
            'header'=>'Nama Ruangan',
            'value'=>'$data->ruangan_nama',
        ),
        'no_urutantri',
        'no_rekam_medik',
        'nama_pasien',
	),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?> 
<!-- search-form -->


</div>