<?php

class MobileModule extends CWebModule
{
	public $defaultController = 'default';
    public $kelompokMenu = array();
    public $menu = array();
    
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'mobile.models.*',
			'mobile.components.*',
		));
		if(!empty($_REQUEST['modulId'])) Yii::app()->session['modulId'] = '41';
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}

	public function getMenu()
    {
        return $this->menu;
    }
}
