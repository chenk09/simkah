<?php

class BookingKamarController extends SBaseController
{
	public $layout='//layouts/columnmobile';
    public $defaultAction = 'index';
    public $successSave = false;
    public $pathView = 'mobile.views.bookingKamar.';

	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
		);
	}

	public function actionIndex()
	{
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=new PPBookingKamarT;
        $model->tglbookingkamar = date('d M Y H:i:s');
        $model->bookingkamar_no = Generator::noBookingKamar();
        $modPasien = new PPPasienM;
        $modPasien->tanggal_lahir = date('d M Y');

		if(isset($_POST['PPBookingKamarT']))
		{
			$model->attributes= $_POST['PPBookingKamarT'];
			$model->tgltransaksibooking=date('Y-m-d H:i:s');
			$model->statuskonfirmasi = "BELUM KONFIRMASI";
            $model->create_loginpemakai_id = Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
            $model->update_loginpemakai_id = Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
			// if(!isset($_POST['isPasienLama']))
			// {
			$modPasien = $this->savePasien($_POST['PPPasienM']);
			$model->pasien_id = $modPasien->pasien_id;
			//}
			if($model->save())
			{
				KamarruanganM::model()->updateByPk($model->kamarruangan_id,array('keterangan_kamar'=>"BOOKING"));
				Yii::app()->user->setFlash('success',"Data berhasil disimpan");
			}
			else 
			{
			    Yii::app()->user->setFlash('error', 'Data Gagal disimpan ');
			}
		}

		$this->render('index',array(
			'model'=>$model,
            'modPasien'=>$modPasien,
		));
	}
        
    public function actionUpdateStatusKonfirmasi(){
          $satu='';
          $model= BookingkamarT::model()->findByPk($idBooking);
          $idBooking = $_POST['idBooking'];
          $tglbookingkamar = $_POST['tglbookingkamar'];
          
          $model->tglbookingkamar = $_POST['tglbookingkamar'];
          $model->bookingkamar_id = $_POST['idBooking'];
          $waktuini = date('h:i:s');
          
          $jamtransaksi = $model->tgltransaksibooking;
                    
          $test = date('H:i:s', strtotime($jamtransaksi));
          $test2 = date('H:i:s');
          $jamtrans = $test;
          $jamsaatini = $test2;
          $jml_jam = $this->selisih($jamtrans, $jamsaatini);
          
//              echo "test: ".$test; echo "<br>";
//              echo "test2:".$test2; echo "<br>";
//              echo "trans:".$jamtrans; echo "<br>";
//              echo "now:".$jamsaatini; echo "<br>";
//              echo "WAktu Kerja : ".selisih($jamtrans,$jamsaatini);

          if($jml_jam >= 2 && $model->statuskonfirmasi != "SUDAH KONFIRMASI"){
             $update = BookingkamarT::model()->updateByPk($idBooking,array('statuskonfirmasi'=>'BATAL BOOKING'));
          }
          
          if($update)
          {
              $satu=$this->createUrl('admin');
          }

          echo CJSON::encode(array
          (
             'satu'=>$satu,
             'tglbooking'=>$tglbookingkamar,
             'idBooking'=>$idBooking,
          ));
          Yii::app()->end();
    }

    function selisih($jamtrans,$jamsaatini) 
    { 
      list($h,$m,$s) = explode(":",$jamtrans); 
      $dtAwal = mktime($h,$m,$s,'1','1','1');
      list($h,$m,$s) = explode(':',$jamsaatini); 
      $dtAkhir = mktime($h,$m,$s,'1','1','1'); 
      $dtSelisih = $dtAkhir-$dtAwal; 
      $totalmenit=$dtSelisih/60; 
      $jam =explode(".",$totalmenit/60);
      $sisamenit=($totalmenit/60)-$jam[0]; 
      $sisamenit2=$sisamenit*60; 
      $jml_jam=$jam[0]; 
      
//          return $jml_jam." jam ".$sisamenit2." menit"; 
      return $jml_jam;
      
    }
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPBookingKamarT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppbooking-kamar-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    
    public function savePasien($attrPasien)
    {
        $modPasien = new PPPasienM;
        $modPasien->attributes = $attrPasien;
        $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
        $modPasien->no_rekam_medik = Generator::noRekamMedik();
        $modPasien->tgl_rekam_medik = date('Y-m-d', time());
        $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
        $modPasien->statusrekammedis = 'AKTIF';
        $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
		$modPasien->agama = Params::DEFAULT_AGAMA;
        $modPasien->warga_negara = Params::DEFAULT_WARGANEGARA;
        $modPasien->kabupaten_id = Yii::app()->user->getState('kabupaten_id');
        $modPasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
        $modPasien->kelurahan_id = Yii::app()->user->getState('kelurahan_id');
        $modPasien->propinsi_id = Yii::app()->user->getState('propinsi_id');
        $modPasien->jenisidentitas = Params::DEFAULT_JENIS_IDENTITAS;
        $modPasien->create_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        $modPasien->update_loginpemakai_id=Params::DEFAULT_CREATE_LOGIN_PEMAKAI;
        if($modPasien->validate()) {
            $modPasien->save();
            $this->successSave = true;
        } else {
            // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
            $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                            CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
        }
        return $modPasien;
    }
        
        
}
