<?php

class InformasiAntrianController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/columnmobile';
    public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	// public function filters()
	// {
	// 	return array(
	// 		'accessControl', // perform access control for CRUD operations
	// 	);
	// }

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
//                if(!Yii::app()->user->checkAccess(Params::)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new PPInformasiantrianpasien('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('d M Y 00:00:00');
                $model->tglAkhir = date('d M Y H:i:s');
		if(isset($_GET['PPInformasiantrianpasien'])){
			$model->attributes=$_GET['PPInformasiantrianpasien'];
                        $format = new CustomFormat();
                        $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                        echo $model->tglAwal;
                        echo $model->tglAkhir;
                }
                
		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PPInformasiantrianpasien::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ppinformasiantrianpasien-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
