<?php
/**
 * This is the model class for table "bookingkamar_t".
 *
 * The followings are the available columns in table 'bookingkamar_t':
 * @property integer $bookingkamar_id
 * @property integer $kelaspelayanan_id
 * @property integer $pendaftaran_id
 * @property integer $kamarruangan_id
 * @property integer $pasien_id
 * @property integer $ruangan_id
 * @property integer $pasienadmisi_id
 * @property string $bookingkamar_no
 * @property string $tglbookingkamar
 * @property string $statusbooking
 * @property string $keteranganbooking
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class PPBookingKamarT extends BookingkamarT{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return BookingkamarT the static model class
     */
    public $no_pendaftaran;
    public $pendaftaran_id;
    public $isNoPendaftaran;
    public $ruanganJalanGd;
    public $ruanganInap;
    public $noRekamMedik;
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
}
?>
