<?php

class KPRegistrasifingerprint extends PegawaiM
{
        public $nofinger, $nipsampai, $namasampai, $jabatansampai, $kelompoksampai, $alatfinger_id, $norekening, $banknorekening,$ruangansampai,$ruangan_id;
    
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'jabatan'=>array(self::BELONGS_TO,'JabatanM','jabatan_id'),
            'kelompokpegawai'=>array(self::BELONGS_TO, 'KelompokpegawaiM', 'kelompokpegawai_id'),
            'pangkat'=>array(self::BELONGS_TO, 'PangkatM', 'pangkat_id'),
            'pendidikan'=>array(self::BELONGS_TO, 'PendidikanM', 'pendidikan_id'),
            'golonganpegawai'=>array(self::BELONGS_TO,'GolonganpegawaiM','golonganpegawai_id'),
             'ruangan'=>array(self::BELONGS_TO,'RuanganM','ruangan_id'), 
             'ruanganpegawai'=>array(self::BELONGS_TO,'RuanganpegawaiM','ruangan_id'),        
		);
	}
        
//        public function rules()
//	{
//		// NOTE: you should only define rules for those attributes that
//		// will receive user inputs.
//		return array(
//			array('profilrs_id, kelompokpegawai_id, nama_pegawai, jeniskelamin, statusperkawinan, agama', 'required'),
//			array('kelurahan_id, kecamatan_id, profilrs_id, gelarbelakang_id, suku_id, kelompokpegawai_id, pendkualifikasi_id, jabatan_id, pendidikan_id, propinsi_id, pangkat_id, kabupaten_id, esselon_id, statuskepemilikanrumah_id', 'numerical', 'integerOnly'=>true),
//			array('tinggibadan, beratbadan', 'numerical'),
//			array('nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, tempatlahir_pegawai, kelompokjabatan', 'length', 'max'=>30),
//			array('gelardepan, kategoripegawai', 'length', 'max'=>10),
//			array('nama_pegawai, nama_keluarga, notelp_pegawai, nomobile_pegawai, kategoripegawaiasal, jenisidentitas', 'length', 'max'=>50),
//			array('jeniskelamin, statusperkawinan, agama, rhesus, jeniswaktukerja', 'length', 'max'=>20),
//			array('golongandarah', 'length', 'max'=>2),
//			array('alamatemail, kemampuanbahasa', 'length', 'max'=>100),
//			array('warganegara_pegawai', 'length', 'max'=>25),
//			array('photopegawai', 'length', 'max'=>200),
//                        array('nofingerprint', 'unique', 'allowEmpty'=>true),
//                        array('nofingerprint','numerical','integerOnly'=>true, 'min'=>1),
//			array('tgl_lahirpegawai, alamat_pegawai, pegawai_aktif, noidentitas, nofingerprint, warnakulit', 'safe'),
//			// The following rule is used by search().
//			// Please remove those attributes that should not be searched.
//			array('jabatansampai, pegawai_id, kelurahan_id, kecamatan_id, profilrs_id, gelarbelakang_id, suku_id, kelompokpegawai_id, pendkualifikasi_id, jabatan_id, pendidikan_id, propinsi_id, pangkat_id, kabupaten_id, nomorindukpegawai, no_kartupegawainegerisipil, no_karis_karsu, no_taspen, no_askes, gelardepan, nama_pegawai, nama_keluarga, tempatlahir_pegawai, tgl_lahirpegawai, jeniskelamin, statusperkawinan, alamat_pegawai, agama, golongandarah, rhesus, alamatemail, notelp_pegawai, nomobile_pegawai, warganegara_pegawai, jeniswaktukerja, kelompokjabatan, kategoripegawai, kategoripegawaiasal, photopegawai, pegawai_aktif, esselon_id, statuskepemilikanrumah_id, jenisidentitas, noidentitas, nofingerprint, tinggibadan, beratbadan, kemampuanbahasa, warnakulit', 'safe', 'on'=>'search'),
//		);
//	}
//        
        public function searchRF()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                                    $criteria->select = 't.*, ruangan_m.*, ruanganpegawai_m.*';
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
		$criteria->compare('jabatan_id',$this->jabatan_id);
                                    $criteria->compare('ruanganpegawai.ruangan_id',$this->ruangan_id);
                                  //  $criteria->compare('ruanganpegawai.pegawai_id',$this->pegawai_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('LOWER(no_kartupegawainegerisipil)',strtolower($this->no_kartupegawainegerisipil),true);
		$criteria->compare('LOWER(no_karis_karsu)',strtolower($this->no_karis_karsu),true);
		$criteria->compare('LOWER(no_taspen)',strtolower($this->no_taspen),true);
		$criteria->compare('LOWER(no_askes)',strtolower($this->no_askes),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tempatlahir_pegawai)',strtolower($this->tempatlahir_pegawai),true);
		$criteria->compare('LOWER(tgl_lahirpegawai)',strtolower($this->tgl_lahirpegawai),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(alamat_pegawai)',strtolower($this->alamat_pegawai),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(notelp_pegawai)',strtolower($this->notelp_pegawai),true);
		$criteria->compare('LOWER(nomobile_pegawai)',strtolower($this->nomobile_pegawai),true);
		$criteria->compare('LOWER(warganegara_pegawai)',strtolower($this->warganegara_pegawai),true);
		$criteria->compare('LOWER(jeniswaktukerja)',strtolower($this->jeniswaktukerja),true);
		$criteria->compare('LOWER(kelompokjabatan)',strtolower($this->kelompokjabatan),true);
		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);
		$criteria->compare('LOWER(kategoripegawaiasal)',strtolower($this->kategoripegawaiasal),true);
		$criteria->compare('LOWER(photopegawai)',strtolower($this->photopegawai),true);
		$criteria->compare('pegawai_aktif',$this->pegawai_aktif);
		$criteria->compare('esselon_id',$this->esselon_id);
		$criteria->compare('statuskepemilikanrumah_id',$this->statuskepemilikanrumah_id);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
		$criteria->compare('LOWER(nofingerprint)',strtolower($this->nofingerprint),true);
		$criteria->compare('tinggibadan',$this->tinggibadan);
		$criteria->compare('beratbadan',$this->beratbadan);
		$criteria->compare('LOWER(kemampuanbahasa)',strtolower($this->kemampuanbahasa),true);
		$criteria->compare('LOWER(warnakulit)',strtolower($this->warnakulit),true);
                                  //   $criteria->join = 'LEFT JOIN ruanganpegawai_m on tariftindakan_m.daftartindakan_id = t.daftartindakan_id 
                                 //  LEFT JOIN jenisdiet_m on jenisdiet_m.jenisdiet_id = t.jenisdiet_id
                                  // LEFT JOIN kelaspelayanan_m on kelaspelayanan_m.kelaspelayanan_id = tariftindakan_m.kelaspelayanan_id';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
//                        'pagination'=>false,
		));
	}
        public function searchRegistrasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
//		$criteria->compare('pegawai_id',$this->pegawai_id);
//		$criteria->compare('kelurahan_id',$this->kelurahan_id);
//		$criteria->compare('kecamatan_id',$this->kecamatan_id);
//		$criteria->compare('profilrs_id',$this->profilrs_id);
//		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
//		$criteria->compare('suku_id',$this->suku_id);
//		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
//		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
                if(isset($this->alatfinger_id) && strlen($this->alatfinger_id) > 0)
                {
                    $criteria->addCondition('
                        pegawai_id NOT IN (SELECT pegawai_id FROM nofingeralat_m WHERE alatfinger_id = '. $this->alatfinger_id .')
                    ');
                }else{
                    $criteria->compare('pegawai_id','99889988');
                }
                
                if (!empty($this->jabatansampai)){
                    $criteria->addBetweenCondition('jabatan_id',$this->jabatan_id, $this->jabatansampai);
                }
                else{
                    $criteria->compare('jabatan_id',$this->jabatan_id);
                }
                
                  if (!empty($this->ruangansampai)){
                    $criteria->addBetweenCondition('ruangan_pegawai.ruangan_id',$this->ruangan_id, $this->ruangansampai);
                }
                else{
                    $criteria->compare('ruangan_pegawai.ruangan_id',$this->ruangan_id);
                }
                
                if (!empty($this->nipsampai)){
                    $criteria->addBetweenCondition('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai), strtolower($this->nipsampai));
                }
                else{
                    $criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
                }
                if (!empty($this->namasampai)){
                    $criteria->addBetweenCondition('LOWER(nama_pegawai)',strtolower($this->nama_pegawai), strtolower($this->namasampai));
                }
                else{
                    $criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
                }
                if (!empty($this->kelompoksampai)){
                    $criteria->addBetweenCondition('kelompokpegawai_id',strtolower($this->kelompokpegawai_id), strtolower($this->kelompoksampai));
                }
                else{
                    $criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
                }
            $criteria->addCondition('pegawai_aktif is true');
//		$criteria->compare('pendidikan_id',$this->pendidikan_id);
//		$criteria->compare('propinsi_id',$this->propinsi_id);
//		$criteria->compare('pangkat_id',$this->pangkat_id);
//		$criteria->compare('kabupaten_id',$this->kabupaten_id);
//		$criteria->compare('LOWER(no_kartupegawainegerisipil)',strtolower($this->no_kartupegawainegerisipil),true);
//		$criteria->compare('LOWER(no_karis_karsu)',strtolower($this->no_karis_karsu),true);
//		$criteria->compare('LOWER(no_taspen)',strtolower($this->no_taspen),true);
//		$criteria->compare('LOWER(no_askes)',strtolower($this->no_askes),true);
//		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
//
//		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
//		$criteria->compare('LOWER(tempatlahir_pegawai)',strtolower($this->tempatlahir_pegawai),true);
//		$criteria->compare('LOWER(tgl_lahirpegawai)',strtolower($this->tgl_lahirpegawai),true);
//		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
//		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
//		$criteria->compare('LOWER(alamat_pegawai)',strtolower($this->alamat_pegawai),true);
//		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
//		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
//		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
//		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
//		$criteria->compare('LOWER(notelp_pegawai)',strtolower($this->notelp_pegawai),true);
//		$criteria->compare('LOWER(nomobile_pegawai)',strtolower($this->nomobile_pegawai),true);
//		$criteria->compare('LOWER(warganegara_pegawai)',strtolower($this->warganegara_pegawai),true);
//		$criteria->compare('LOWER(jeniswaktukerja)',strtolower($this->jeniswaktukerja),true);
//		$criteria->compare('LOWER(kelompokjabatan)',strtolower($this->kelompokjabatan),true);
//		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);
//		$criteria->compare('LOWER(kategoripegawaiasal)',strtolower($this->kategoripegawaiasal),true);
//		$criteria->compare('LOWER(photopegawai)',strtolower($this->photopegawai),true);
//		$criteria->compare('pegawai_aktif',$this->pegawai_aktif);
//		$criteria->compare('esselon_id',$this->esselon_id);
//		$criteria->compare('statuskepemilikanrumah_id',$this->statuskepemilikanrumah_id);
//		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
//		$criteria->addCondition('nofingerprint is null');
//		$criteria->compare('LOWER(nofingerprint)',strtolower($this->nofingerprint),true);
//		$criteria->compare('tinggibadan',$this->tinggibadan);
//		$criteria->compare('beratbadan',$this->beratbadan);
//		$criteria->compare('LOWER(kemampuanbahasa)',strtolower($this->kemampuanbahasa),true);
//		$criteria->compare('LOWER(warnakulit)',strtolower($this->warnakulit),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
        public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
		$criteria->compare('jabatan_id',$this->jabatan_id);
                $criteria->compare('ruanganpegawai.ruangan_id',$this->ruangan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('LOWER(no_kartupegawainegerisipil)',strtolower($this->no_kartupegawainegerisipil),true);
		$criteria->compare('LOWER(no_karis_karsu)',strtolower($this->no_karis_karsu),true);
		$criteria->compare('LOWER(no_taspen)',strtolower($this->no_taspen),true);
		$criteria->compare('LOWER(no_askes)',strtolower($this->no_askes),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tempatlahir_pegawai)',strtolower($this->tempatlahir_pegawai),true);
		$criteria->compare('LOWER(tgl_lahirpegawai)',strtolower($this->tgl_lahirpegawai),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(alamat_pegawai)',strtolower($this->alamat_pegawai),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(notelp_pegawai)',strtolower($this->notelp_pegawai),true);
		$criteria->compare('LOWER(nomobile_pegawai)',strtolower($this->nomobile_pegawai),true);
		$criteria->compare('LOWER(warganegara_pegawai)',strtolower($this->warganegara_pegawai),true);
		$criteria->compare('LOWER(jeniswaktukerja)',strtolower($this->jeniswaktukerja),true);
		$criteria->compare('LOWER(kelompokjabatan)',strtolower($this->kelompokjabatan),true);
		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);
		$criteria->compare('LOWER(kategoripegawaiasal)',strtolower($this->kategoripegawaiasal),true);
		$criteria->compare('LOWER(photopegawai)',strtolower($this->photopegawai),true);
		$criteria->compare('pegawai_aktif',$this->pegawai_aktif);
		$criteria->compare('esselon_id',$this->esselon_id);
		$criteria->compare('statuskepemilikanrumah_id',$this->statuskepemilikanrumah_id);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
                
                /*
		$criteria->addCondition('nofingerprint is not null');
		$criteria->addCondition("trim(nofingerprint) != ''");
                 * 
                 */
		$criteria->compare('tinggibadan',$this->tinggibadan);
		$criteria->compare('beratbadan',$this->beratbadan);
		$criteria->compare('LOWER(kemampuanbahasa)',strtolower($this->kemampuanbahasa),true);
		$criteria->compare('LOWER(warnakulit)',strtolower($this->warnakulit),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
//                        'pagination'=>false,
		));
	}
        
        public function searchInformasiprint()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('gelarbelakang_id',$this->gelarbelakang_id);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('kelompokpegawai_id',$this->kelompokpegawai_id);
		$criteria->compare('pendkualifikasi_id',$this->pendkualifikasi_id);
		$criteria->compare('jabatan_id',$this->jabatan_id);
                                   $criteria->compare('ruanganpegawai.ruangan_id',$this->ruangan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('pangkat_id',$this->pangkat_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(nomorindukpegawai)',strtolower($this->nomorindukpegawai),true);
		$criteria->compare('LOWER(no_kartupegawainegerisipil)',strtolower($this->no_kartupegawainegerisipil),true);
		$criteria->compare('LOWER(no_karis_karsu)',strtolower($this->no_karis_karsu),true);
		$criteria->compare('LOWER(no_taspen)',strtolower($this->no_taspen),true);
		$criteria->compare('LOWER(no_askes)',strtolower($this->no_askes),true);
		$criteria->compare('LOWER(gelardepan)',strtolower($this->gelardepan),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(nama_keluarga)',strtolower($this->nama_keluarga),true);
		$criteria->compare('LOWER(tempatlahir_pegawai)',strtolower($this->tempatlahir_pegawai),true);
		$criteria->compare('LOWER(tgl_lahirpegawai)',strtolower($this->tgl_lahirpegawai),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(alamat_pegawai)',strtolower($this->alamat_pegawai),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(notelp_pegawai)',strtolower($this->notelp_pegawai),true);
		$criteria->compare('LOWER(nomobile_pegawai)',strtolower($this->nomobile_pegawai),true);
		$criteria->compare('LOWER(warganegara_pegawai)',strtolower($this->warganegara_pegawai),true);
		$criteria->compare('LOWER(jeniswaktukerja)',strtolower($this->jeniswaktukerja),true);
		$criteria->compare('LOWER(kelompokjabatan)',strtolower($this->kelompokjabatan),true);
		$criteria->compare('LOWER(kategoripegawai)',strtolower($this->kategoripegawai),true);
		$criteria->compare('LOWER(kategoripegawaiasal)',strtolower($this->kategoripegawaiasal),true);
		$criteria->compare('LOWER(photopegawai)',strtolower($this->photopegawai),true);
		$criteria->compare('pegawai_aktif',$this->pegawai_aktif);
		$criteria->compare('esselon_id',$this->esselon_id);
		$criteria->compare('statuskepemilikanrumah_id',$this->statuskepemilikanrumah_id);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(noidentitas)',strtolower($this->noidentitas),true);
		$criteria->addCondition('nofingerprint is not null');
		$criteria->addCondition("trim(nofingerprint) != ''");
		$criteria->compare('tinggibadan',$this->tinggibadan);
		$criteria->compare('beratbadan',$this->beratbadan);
		$criteria->compare('LOWER(kemampuanbahasa)',strtolower($this->kemampuanbahasa),true);
		$criteria->compare('LOWER(warnakulit)',strtolower($this->warnakulit),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                                                'pagination'=>false,
//                        'pagination'=>false,
		));
	}

	
}