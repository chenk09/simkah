<?php

class KPPegawaiM extends PegawaiM
{
    
    public $nama_pemakai;
    public $new_password;
    public $new_password_repeat;
    public $jenispendidikan;
    public $umur_bekerja;
    public $norekening;
    public $banknorekening;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PegawaiM the static model class
	 */
    public $tempPhoto;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         * digunakan di modul Kepegawaian pelmar file kontrakPelamar.php
         */
       public function getGelarDepanItems(){
           return LookupM::model()->findAllByAttributes(array('lookup_type'=>'gelardepan'), array('order'=>'lookup_urutan'));
       }
       public function getGelarBelakangItems(){
           return  GelarbelakangM::model()->findAll();
       }
       
       public function getPropinsiItems(){
            return PropinsiM::model()->findAllByAttributes(array('propinsi_aktif'=>true));
        }
        
        public function getKabupatenItems($propinsi_id){
            if (!empty($propinsi_id))
                return KabupatenM::model()->findAllByAttributes (array('kabupaten_aktif'=>TRUE, 'propinsi_id'=>$propinsi_id));
            else
                return array();
       }
       public function getKecamatanItems($kabupaten_id){
            if (!empty($kabupaten_id))
                return KecamatanM::model()->findAllByAttributes (array('kecamatan_aktif'=>TRUE, 'kabupaten_id'=>$kabupaten_id));
            else
                return array();
       }
       public function getKelurahanItems($kecamatan_id){
            if (!empty($kecamatan_id))
                return KelurahanM::model()->findAllByAttributes (array('kelurahan_aktif'=>TRUE, 'kecamatan_id'=>$kecamatan_id));
            else
                return array();
       }
      
       public function getStatuskepemilikanrumahItems()
        {
            return StatuskepemilikanrumahM::model()->findAll('statuskepemilikanrumah_aktif=TRUE ORDER BY statuskepemilikanrumah_nama');
        }
       
       public function getRumahSakitItems(){
            return ProfilrumahsakitM::model()->findAll();
        }
	      public function getPendidikanItems(){
            return PendidikanM::model()->findAllByAttributes(array('pendidikan_aktif'=>TRUE));
        }
        public function getPendKualifikasiItems(){
            return PendidikankualifikasiM::model()->findAllByAttributes(array('pendkualifikasi_aktif'=>true));
        }
        public function getSukuItems(){
            return SukuM::model()->findAllByAttributes(array('suku_aktif'=>TRUE), array('order'=>'suku_nama'));
        }
        public function getKategoriPegawai(){
            return LookupM::model()->findAllByAttributes(array('lookup_type'=>'kategorikaryawan'));
        }
        public function getStatusPegawai(){
            return LookupM::model()->findAllByAttributes(array('lookup_type'=>'statuskaryawan'));
        }
        public function getKelompokPegawaiItems(){
            return KelompokpegawaiM::model()->findAllByAttributes(array('kelompokpegawai_aktif'=>true));
        }
        public function getPangkatItems(){
            return PangkatM::model()->findAllByAttributes(array('pangkat_aktif'=>true));
        }
        public function getGolonganItems(){
            return GolonganpegawaiM::model()->findAllByAttributes(array('golonganpegawai_aktif'=>true));
        }
                
        public function beforeSave() 
        {
            if($this->tgl_lahirpegawai===null || trim($this->tgl_lahirpegawai)=='')
            {
                $this->setAttribute('tgl_lahirpegawai', null);
            }
            return parent::beforeSave();
        }        
}