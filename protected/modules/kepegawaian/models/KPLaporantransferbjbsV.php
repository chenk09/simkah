<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class KPLaporantransferbjbsV extends LaporantransferbjbsV {

	public $jns_periode;
    public $tglAwal, $tglAkhir, $bln_awal, $bln_akhir, $thn_awal, $thn_akhir;


    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

	public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria = $this->functionCriteria();

        // $criteria->select = 'count(t.pendaftaran_id) as jumlah, t.kelaspelayanan_nama as data';
        // $criteria->group = 't.kelaspelayanan_nama';
        // if (!empty($this->carabayar_id)) {
        //     $criteria->select .= ', t.penjamin_nama as tick';
        //     $criteria->group .= ', t.penjamin_nama';
        // } else {
        //     $criteria->select .= ', t.carabayar_nama as tick';
        //     $criteria->group .= ', t.carabayar_nama';
        // }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

       $criteria=new CDbCriteria;

		 $criteria->addBetweenCondition('tglpenggajian', $this->tglAwal, $this->tglAkhir);

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('nama_keluarga',$this->nama_keluarga,true);
		$criteria->compare('gelarbelakang_nama',$this->gelarbelakang_nama,true);
		$criteria->compare('tempatlahir_pegawai',$this->tempatlahir_pegawai,true);
		$criteria->compare('tgl_lahirpegawai',$this->tgl_lahirpegawai,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('jabatan_id',$this->jabatan_id);
		$criteria->compare('jabatan_nama',$this->jabatan_nama,true);
		$criteria->compare('kode_bank',$this->kode_bank,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominalgaji',$this->nominalgaji);
		$criteria->compare('periodegaji',$this->periodegaji,true);
		$criteria->compare('tglkaskeluar',$this->tglkaskeluar,true);
		$criteria->compare('nokaskeluar',$this->nokaskeluar,true);
		$criteria->compare('carabayarkeluar',$this->carabayarkeluar,true);
		$criteria->compare('melaluibank',$this->melaluibank,true);
		$criteria->compare('denganrekening',$this->denganrekening,true);
		$criteria->compare('atasnamarekening',$this->atasnamarekening,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
		));
    }

	public function functionCriteria()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		 $criteria->addBetweenCondition('tglpenggajian', $this->tglAwal, $this->tglAkhir);

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('nama_keluarga',$this->nama_keluarga,true);
		$criteria->compare('gelarbelakang_nama',$this->gelarbelakang_nama,true);
		$criteria->compare('tempatlahir_pegawai',$this->tempatlahir_pegawai,true);
		$criteria->compare('tgl_lahirpegawai',$this->tgl_lahirpegawai,true);
		$criteria->compare('jeniskelamin',$this->jeniskelamin,true);
		$criteria->compare('jabatan_id',$this->jabatan_id);
		$criteria->compare('jabatan_nama',$this->jabatan_nama,true);
		$criteria->compare('kode_bank',$this->kode_bank,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('nominalgaji',$this->nominalgaji);
		$criteria->compare('periodegaji',$this->periodegaji,true);
		$criteria->compare('tglkaskeluar',$this->tglkaskeluar,true);
		$criteria->compare('nokaskeluar',$this->nokaskeluar,true);
		$criteria->compare('carabayarkeluar',$this->carabayarkeluar,true);
		$criteria->compare('melaluibank',$this->melaluibank,true);
		$criteria->compare('denganrekening',$this->denganrekening,true);
		$criteria->compare('atasnamarekening',$this->atasnamarekening,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function model($className = __CLASS__) {
        parent::model($className);
    }



}

?>
