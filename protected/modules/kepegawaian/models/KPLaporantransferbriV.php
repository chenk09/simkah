<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class KPLaporantransferbriV extends LaporantransferbriV {

  public $jns_periode;
   public $tglAwal, $tglAkhir, $bln_awal, $bln_akhir, $thn_awal, $thn_akhir;


    public function searchTable() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria = $this->functionCriteria();

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

	public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria = $this->functionCriteria();

        // $criteria->select = 'count(t.pendaftaran_id) as jumlah, t.kelaspelayanan_nama as data';
        // $criteria->group = 't.kelaspelayanan_nama';
        // if (!empty($this->carabayar_id)) {
        //     $criteria->select .= ', t.penjamin_nama as tick';
        //     $criteria->group .= ', t.penjamin_nama';
        // } else {
        //     $criteria->select .= ', t.carabayar_nama as tick';
        //     $criteria->group .= ', t.carabayar_nama';
        // }

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function searchPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

       $criteria=new CDbCriteria;

		 $criteria->addBetweenCondition('tglpenggajian', $this->tglAwal, $this->tglAkhir);

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('jmlkaskeluar',$this->jmlkaskeluar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination' => false,
		));
    }

	public function functionCriteria()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		 $criteria->addBetweenCondition('tglpenggajian', $this->tglAwal, $this->tglAkhir);

		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
		$criteria->compare('nama_pegawai',$this->nama_pegawai,true);
		$criteria->compare('no_rekening',$this->no_rekening,true);
		$criteria->compare('jmlkaskeluar',$this->jmlkaskeluar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

    public static function model($className = __CLASS__) {
        parent::model($className);
    }



}

?>
