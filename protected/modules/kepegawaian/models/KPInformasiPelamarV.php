<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class KPInformasiPelamarV extends InformasipelamarV {

    public static function model($className = __CLASS__) {
        parent::model($className);
    }

    public function getJenisIdentitas()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'jenisidentitas'));
    }
    public function getJenisKelamin()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'jeniskelamin'));
    }
    public function getAgama()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'agama'), array('order'=>'lookup_urutan'));
    }
    public function getStatusPerkawinan()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'statusperkawinan'));
    }
    public function getPendidikanItems(){
        return PendidikanM::model()->findAll();
    }
    public function getpendidikannama(){
        $pendidikan = PendidikanM::model()->findByPk($this->pendidikan_id);
        if (!empty($pendidikan->pendidikan_nama)){
            $nama_pendidikan = $pendidikan->pendidikan_nama;
            return $nama_pendidikan;
        }
    }
    public function getPendkualifikasiItems(){
        return PendidikankualifikasiM::model()->findAllByAttributes(array('pendkualifikasi_aktif'=>true));
    }
    public function getPendkualifikasiNama(){
        $pendidikan = PendidikankualifikasiM::model()->findByPk($this->pendkualifikasi_id);
        if (!empty($pendidikan->pendkualifikasi_nama)){
            return $pendidikan->pendkualifikasi_nama;
        }
    }
    public function getMinatPekerjaan(){
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'minatpekerjaan'));
    }
    public function getSuku(){
        return SukuM::model()->findAll();
    }
    public function getProfilRS(){
        return ProfilrumahsakitM::model()->findAll(array('order'=>'profilrs_id'));
    }

    public function getnokontakpelamar(){
        $nokontak = $this->notelp_pelamar." / ".$this->nomobile_pelamar;
        return $nokontak;
    }

    public function getstatuskawin(){
        return $this->statusperkawinan.' / '.$this->jmlanak;
    }

    public function getnamasuku(){
        $suku = SukuM::model()->findByPk($this->suku_id);
        if(!empty($suku->suku_nama))
            return $suku->suku_nama;
    }

    public function getprofilnama(){
        $profile = ProfilrumahsakitM::model()->findByPk($this->profilrs_id);
        return $profile->nama_rumahsakit;
    }

    public function  getWargaNegara(){
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'warganegara'));
    }
}

?>
