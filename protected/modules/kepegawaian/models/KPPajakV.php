<?php

class KPPajakV extends LaporanpajakpegawaiV
{    
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
        
        public $tglAwal, $tglAkhir, $nama_pegawai, $jabatan_id, $periodegaji, $harikerja, $jmlefektifharikerja;
        public $gajipph;
        public $biayajabatan;
        public $iuranpensiun;
        public $penerimaanpph;
        public $ptkp;
        public $pkp;
        public $pphpersen;
        public $pph21;
        
        
        public function criteriaLaporan()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                // echo $this->periodegaji;exit;
                $criteria=new CDbCriteria;
                // $criteria->addBetweenCondition('tglpenggajian',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('penggajianpeg_id',$this->penggajianpeg_id);
                $criteria->compare('periodegaji',date_diff($this->periodegaji));
                $criteria->compare('pegawai_id',$this->pegawai_id);
                $criteria->compare('gelardepan',$this->gelardepan,true);
                $criteria->compare('nama_pegawai',$this->nama_pegawai,true);
                $criteria->compare('nama_keluarga',$this->nama_keluarga,true);
                $criteria->compare('nopenggajian',$this->nopenggajian,true);
                $criteria->compare('statusperkawinan',$this->statusperkawinan,true);
                $criteria->compare('jeniskelamin',$this->jeniskelamin,true);
                $criteria->compare('keterangan',$this->keterangan,true);
                $criteria->compare('mengetahui',$this->mengetahui,true);
                $criteria->compare('menyetujui',$this->menyetujui,true);
                $criteria->compare('pengeluaranumum_id',$this->pengeluaranumum_id);
                $criteria->compare('nomorindukpegawai',$this->nomorindukpegawai,true);
                $criteria->compare('notelp_pegawai',$this->notelp_pegawai,true);
                $criteria->compare('nomobile_pegawai',$this->nomobile_pegawai,true);
                $criteria->compare('photopegawai',$this->photopegawai,true);
                $criteria->compare('alamatemail',$this->alamatemail,true);
                $criteria->compare('tgl_lahirpegawai',$this->tgl_lahirpegawai,true);
                $criteria->compare('tempatlahir_pegawai',$this->tempatlahir_pegawai,true);
                $criteria->compare('noidentitas',$this->noidentitas,true);
                $criteria->compare('jenisidentitas',$this->jenisidentitas,true);
                $criteria->compare('gajibersih',$this->gajibersih);
                $criteria->compare('totalpajak',$this->totalpajak);
                return $criteria;
        }
        
        public function searchLaporan()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaLaporan(),
                        'pagination'=>array(
                            'pageSize'=>10,
                        )
                ));
        }
        public function searchPrintLaporan()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaLaporan(),
                        'pagination'=>false,
                ));
        }
        
        public function searchLaporanprint()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaLaporan(),
                        'pagination'=>false,
                ));
        }
        
        
}
?>
