<?php

/**
 * This is the model class for table "pelamar_t".
 *
 * The followings are the available columns in table 'pelamar_t':
 * @property integer $pelamar_id
 * @property integer $pendkualifikasi_id
 * @property integer $profilrs_id
 * @property integer $suku_id
 * @property integer $pendidikan_id
 * @property string $tgllowongan
 * @property string $jenisidentitas
 * @property string $noidentitas
 * @property string $nama_pelamar
 * @property string $nama_keluarga
 * @property string $tempatlahir_pelamar
 * @property string $tgl_lahirpelamar
 * @property string $jeniskelamin
 * @property string $statusperkawinan
 * @property integer $jmlanak
 * @property string $alamat_pelamar
 * @property string $kodepos
 * @property string $agama
 * @property string $alamatemail
 * @property string $notelp_pelamar
 * @property string $nomobile_pelamar
 * @property string $warganegara_pelamar
 * @property string $photopelamar
 * @property double $gajiygdiharapkan
 * @property string $tglditerima
 * @property string $tglmulaibekerja
 * @property string $ingintunjangan
 * @property string $keterangan_pelamar
 * @property string $minatpekerjaan
 * @property string $filelamaran
 * @property string $crate_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 *
 * The followings are the available model relations:
 * @property SukuM $suku
 * @property PendidikanM $pendidikan
 * @property PendidikankualifikasiM $pendkualifikasi
 * @property ProfilrumahsakitM $profilrs
 * @property KemampuanbahasaR[] $kemampuanbahasaRs
 * @property LingkungankerjaR[] $lingkungankerjaRs
 */
class KPPelamarT extends PelamarT
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PelamarT the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     * 
     */
    
    public function getJenisIdentitas()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'jenisidentitas'));
    }
    public function getJenisKelamin()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'jeniskelamin'));
    }
    public function getAgama()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'agama'), array('order'=>'lookup_urutan'));
    }
    public function getStatusPerkawinan()
    {
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'statusperkawinan'));
    }
    public function getPendidikanItems(){
        return PendidikanM::model()->findAll();
    }
    public function getpendidikannama(){
        $pendidikan = PendidikanM::model()->findByPk($this->pendidikan_id);
        if (!empty($pendidikan->pendidikan_nama)){
            $nama_pendidikan = $pendidikan->pendidikan_nama;
            return $nama_pendidikan;
        }
    }
    public function getPendkualifikasiItems(){
        return PendidikankualifikasiM::model()->findAllByAttributes(array('pendkualifikasi_aktif'=>true));
    }
    public function getPendkualifikasiNama(){
        $pendidikan = PendidikankualifikasiM::model()->findByPk($this->pendkualifikasi_id);
        if (!empty($pendidikan->pendkualifikasi_nama)){
            return $pendidikan->pendkualifikasi_nama;
        }
    }
    public function getMinatPekerjaan(){
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'minatpekerjaan'));
    }
    public function getSuku(){
        return SukuM::model()->findAll();
    }
    public function getProfilRS(){
        return ProfilrumahsakitM::model()->findAll(array('order'=>'profilrs_id'));
    }

    public function getnokontakpelamar(){
        $nokontak = $this->notelp_pelamar." / ".$this->nomobile_pelamar;
        return $nokontak;
    }

    public function getstatuskawin(){
        return $this->statusperkawinan.' / '.$this->jmlanak;
    }

    public function getnamasuku(){
        $suku = SukuM::model()->findByPk($this->suku_id);
        if(!empty($suku->suku_nama))
            return $suku->suku_nama;
    }

    public function getprofilnama(){
        $profile = ProfilrumahsakitM::model()->findByPk($this->profilrs_id);
        return $profile->nama_rumahsakit;
    }

    public function  getWargaNegara(){
        return LookupM::model()->findAllByAttributes(array('lookup_type'=>'warganegara'));
    }
    /**
     * End by ichan
     */
    
}
?>
