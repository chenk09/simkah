<?php

class KPPenggajianpegT extends PenggajianpegT
{    
        public static function model($className = __CLASS__) {
            return parent::model($className);
        }
        
        public $tglAwal, $tglAkhir, $nama_pegawai, $jabatan_id, $periodegaji, $harikerja, $jmlefektifharikerja;
        public $gajipph;
        public $biayajabatan;
        public $iuranpensiun;
        public $penerimaanpph;
        public $ptkp;
        public $pkp;
        public $pphpersen;
        public $pph21;
        public $periodegaji_info;
        public $lamakerja;
        public $cuti;
        public $alpha;
        public $ijin;
        public $kategoripegawai;
        public $nama_mengetahui, $nama_menyetujui;
        public $totalterima_bersih, $totalpotongan_bersih;
        
        public function attributeLabels()
        {
                return array(
                        'penggajianpeg_id' => 'Penggajian Karyawan',
                        'pegawai_id' => 'Karyawan',
                        'tglpenggajian' => 'Tanggal Penggajian',
                        'nopenggajian' => 'No Penggajian',
                        'keterangan' => 'Cara Bayar',
                        'mengetahui' => 'Mengetahui',
                        'menyetujui' => 'Menyetujui',
                        'totalterima' => 'Total Terima',
                        'totalpajak' => 'Total Pajak',
                        'totalpotongan' => 'Total Potongan',
                        'penerimaanbersih' => 'Penerimaan Bersih',
                        'nomorindukpegawai'=>'NRK',
						'nama_pegawai'=>'Nama Karyawan',
                        'tglAwal'=>'Tanggal Penggajian',
                        'tglAkhir'=>'Sampai dengan',
                        'jabatan_id'=>'Jabatan',
                        'periodegaji'=>'Periode Penggajian',
                        'harikerja'=>'Jml Hari Kerja',
                        'jmlefektifharikerja'=>'Jml Efektif Hari Kerja',
                        'penerimaanpph'=>'Penerimaan PPH',
                        'pphpersen'=>'PPH Persen',
                );
        }
        
        public function criteriaLaporan()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                $criteria->compare('LOWER(pegawai.nama_pegawai)',strtolower($this->nama_pegawai),true);
                $criteria->compare('pegawai.jabatan_id',$this->jabatan_id);
				$criteria->compare('pegawai.kategoripegawai', $this->kategoripegawai);
				$criteria->compare('LOWER(keterangan)', strtolower($this->keterangan));
                $criteria->addBetweenCondition('tglpenggajian',$this->tglAwal,$this->tglAkhir);
                $criteria->with = 'pegawai';
                
                return $criteria;
        }
        
        public function searchLaporan()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaLaporan(),
                        'pagination'=>array(
                            'pageSize'=>10,
                        )
                ));
        }
		
		public function searchInformasi($print = false)
        {
			$criteria = new CDbCriteria;
			$criteria->with = 'pegawai';
			$criteria->compare('EXTRACT(MONTH FROM periodegaji)', date("m", strtotime($this->periodegaji)));
			$criteria->compare('LOWER(pegawai.nama_pegawai)',strtolower($this->pegawai_id),true);
			$criteria->compare('nopenggajian', $this->nopenggajian);
			$criteria->compare('pegawai.kategoripegawai', $this->kategoripegawai);
			$criteria->order = 'tglpenggajian desc';
			if($print)
			{
				return new CActiveDataProvider($this, array(
						'criteria'=>$criteria,
						'pagination'=>false
				));
			}else{
				return new CActiveDataProvider($this, array(
						'criteria'=>$criteria,
						'pagination'=>array(
							'pageSize'=>10,
						)
				));
			}
        }
        
        public function searchLaporanprint()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaLaporan(),
                        'pagination'=>false,
                ));
        }
        
        public function getTotalColumnKomponen($potongan)
        {
                if ($potongan=='potongan') {
                    $total = Yii::app()->db->createCommand('SELECT COUNT(komponengaji_id) AS totalcolumn FROM komponengaji_m WHERE ispotongan=TRUE')->queryAll();
                } else if ($potongan=='gaji') {
                    $total = Yii::app()->db->createCommand('SELECT COUNT(komponengaji_id) AS totalcolumn FROM komponengaji_m WHERE ispotongan=FALSE')->queryAll();
                }
                
                return $total[0]['totalcolumn'];
        }
        
        public function getColumnKomponen($potongan)
        {
                if ($potongan=='potongan') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=TRUE");
                } else if ($potongan=='gaji') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=FALSE");
                }
                foreach ($modKomponengaji as $komponengaji)
                {
                    $column .= "<th id='childcolumn'>$komponengaji->komponengaji_nama</th>";
                }
            
            return $column;
        }
        
        public function getValueKomponen($potongan, $penggajianpeg_id)
        {
                if ($potongan=='potongan') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=TRUE");
                } else if ($potongan=='gaji') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=FALSE");
                }
                foreach ($modKomponengaji as $komponengaji)
                {
                    $komponengaji_id = $komponengaji->komponengaji_id;
                    $column .= "<td>".$this->getValue($komponengaji_id, $penggajianpeg_id)."</td>";
                }
            
            return $column;
        }
        
        public function getValue($komponengaji_id, $penggajianpeg_id)
        {
            $query = Yii::app()->db->createCommand(
                    "SELECT jumlah
                    FROM penggajiankomp_t
                    WHERE
                        komponengaji_id=$komponengaji_id
                        AND
                        penggajianpeg_id=$penggajianpeg_id"
            )->queryAll();
            
            $jumlah = number_format($query[0]['jumlah'],0,"",".");
            
            return $jumlah;
        }
        
        public function getFooterKomponen($potongan)
        {
                if ($potongan=='potongan') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=TRUE");
                } else if ($potongan=='gaji') {
                    $modKomponengaji = KomponengajiM::model()->findAll("ispotongan=FALSE");
                }
                foreach ($modKomponengaji as $komponengaji)
                {
                    $sql = Yii::app()->db->createCommand(
                            "SELECT SUM(jumlah) AS totalkomponen
                            FROM penggajiankomp_t, penggajianpeg_t
                            WHERE
                                komponengaji_id=$komponengaji->komponengaji_id
                                AND
                                penggajianpeg_t.tglpenggajian BETWEEN '$this->tglAwal' AND '$this->tglAkhir'
                                AND
                                penggajianpeg_t.penggajianpeg_id=penggajiankomp_t.penggajianpeg_id"
                    )->queryAll();
                    $totalkomponen = $sql[0]['totalkomponen'];
                    $komponengaji_id = $komponengaji->komponengaji_id;
                    $column .= "<td>".number_format($totalkomponen,0,'','.')."</td>";
                }
            
            return $column;
        }
        
        public function getJabatanItems()
        {
            return JabatanM::model()->findAll('jabatan_aktif=TRUE ORDER BY jabatan_nama');
        }
        
        public function getTotalgaji()
        {
            $modPenggajian = PenggajianpegT::model()->findAll($this->criteriaLaporan());
            foreach ($modPenggajian as $dataPenggajian)
            {
                $totalgajipokok += $dataPenggajian->totalterima;
            }
            
            return number_format($totalgajipokok,0,"",".");
        }

        public function getperiode()
        {
            $format     = new CustomFormat;
            $periode    = explode('-', $this->periodegaji);
            $periode_bulan  = $periode[1];
            $periode_tahun  = $periode[0];
            $bulan      = $format->formatBulanInaLengkap($periode_bulan);

            return $bulan.' '.$periode_tahun;
        }
}
?>
