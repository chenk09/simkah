<?php

/**
 * This is the model class for table "ptkp_m".
 *
 * The followings are the available columns in table 'ptkp_m':
 * @property integer $ptkp_id
 * @property string $tglberlaku
 * @property string $statusperkawinan
 * @property integer $jmltanggunan
 * @property double $wajibpajak_thn
 * @property double $wajibpajak_bln
 * @property boolean $berlaku
 */
class KPPinjamanPegT extends PinjamanpegT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PtkpM the static model class
	 */
	public $idpinjaman, $tglAwal, $tglAkhir;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->addBetweenCondition('tglpinjampeg',$this->tglAwal,$this->tglAkhir);
		$criteria->compare('pinjamanpeg_id',$this->pinjamanpeg_id);
		$criteria->compare('tandabuktikeluar_id',$this->tandabuktikeluar_id);
		$criteria->compare('komponengaji_id',$this->komponengaji_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(nopinjam)',strtolower($this->nopinjam),true);
		$criteria->compare('LOWER(untukkeperluan)',strtolower($this->untukkeperluan),true);
		$criteria->compare('LOWER(keterangan)',strtolower($this->keterangan),true);
		$criteria->compare('jumlahpinjaman',$this->jumlahpinjaman);
		$criteria->compare('lamapinjambln',$this->lamapinjambln);
		$criteria->compare('persenpinjaman',$this->persenpinjaman);
		$criteria->compare('sisapinjaman',$this->sisapinjaman);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}