<?php
/**
 * @author ichan | Ihsan Fauzi Rahman <ichan90@yahoo.co.id>
 */
$arrMenu = array();
                 (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Lihat Detail Rencana Lembur ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
$this->menu=$arrMenu;
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'lihat-detail-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->uneditableRow($modRealisasiLembur,'tglrealisasi', array('class'=>'span3')); ?>
            </div>
            <div class="control-group">
                <?php echo $form->uneditableRow($modRealisasiLembur,'norealisasi',array('class'=>'span3')); ?>
                </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php $modRealisasiLembur->mengetahui_nama = $modRealisasiLembur->mengetahui->nama_pegawai; ?>
                <?php echo $form->uneditableRow($modRealisasiLembur,  'mengetahui_nama',array('class'=>'span3')); ?>
            </div>
            <div class="control-group">
                <?php $modRealisasiLembur->menyetujui_nama = $modRealisasiLembur->menyetujui->nama_pegawai; ?>
                <?php echo $form->uneditableRow($modRealisasiLembur,  'menyetujui_nama',array('class'=>'span3')); ?>
            </div>
        </td>
    </tr>
</table>

<table id="tabelKaryawanLembur" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th style="text-align: center;">No.</th>
        <th style="text-align: center;">No. Induk Pegawai</th>
        <th style="text-align: center;">Nama Karyawan</th>
        <!--<th style="text-align: center;">Departemen</th>-->
        <th style="text-align: center;">Jam Mulai</th>
        <th style="text-align: center;">Jam Selesai</th>
        <th style="text-align: center;">Alasan Lembur</th>
        
    </tr>
    </thead>
    <tbody>
        <?php                    
            $tr = '';
            $no = 1;
            $format = new CustomFormat;
             foreach ($modDetail as $key => $detail) {
                    $modDetail[$key]->jamMulai = date('H:i', strtotime($modDetail[$key]->tglmulai));
                    $modDetail[$key]->jamSelesai = date('H:i', strtotime($modDetail[$key]->tglselesai));
                    $tr.="<tr>
                       <td>". CHtml::TextField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE))."</td>
                       <td>".$modDetail[$key]->pegawai->nomorindukpegawai."</td>
                       <td>".$modDetail[$key]->pegawai->nama_pegawai."</td>
                       
                       <td style='text-align:center;'>".$modDetail[$key]->jamMulai."</td>
                       <td style='text-align:center;'>".$modDetail[$key]->jamSelesai."</td>
                       <td>".$modDetail[$key]->alasanlembur."</td>
                       </tr>   
                   "; //<td>".$modDetail[$key]->pegawai->departement->departement_nama."</td>
                    
             }
             echo $tr;
        ?>
    </tbody>
</table>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRealisasiLembur, 'keterangan', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textArea($modRealisasiLembur, 'keterangan', array('readonly'=>true)); ?>
                </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php $modRealisasiLembur->pemberitugas_nama = $modRealisasiLembur->pemberitugas->nama_pegawai; ?>
                <?php echo $form->uneditableRow($modRealisasiLembur,  'pemberitugas_nama',array('class'=>'span3')); ?>
            </div>
        </td>
    </tr>    

</table>
            

<?php $this->endWidget(); ?>


