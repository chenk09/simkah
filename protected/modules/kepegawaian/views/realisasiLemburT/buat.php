<?php
/**
 * @author ichan | Ihsan Fauzi Rahman <ichan90@yahoo.co.id>
 */
$arrMenu = array();
                 (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Realisasi Rencana Lembur ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
$this->menu=$arrMenu;
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'buat-realisasi-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onSubmit'=>'return cekValidasi();'),
        'focus'=>'#',
)); ?>
<?php echo $form->errorSummary($modRealisasiLembur); ?>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php //echo $form->uneditableRow($modRencanaLembur,'norencana',array('class'=>'span3')); ?>
                <div class="control-label">Tgl/No. Rencana</div>
                <div class="controls">
                    <?php //echo $modRencanaLembur->tglrencana." / ".$modRencanaLembur->norencana; ?>
                    <?php echo CHtml::textField('tglrencana', $modRencanaLembur->tglrencana, array('class'=>'span2', 'readonly'=>true)); ?>
                    <?php echo CHtml::textField('norencana', $modRencanaLembur->norencana, array('class'=>'span2', 'readonly'=>true)); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($modRealisasiLembur,'tglrealisasi', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modRealisasiLembur,
                                                    'attribute'=>'tglrealisasi',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',                                                       
                                                    ),
                                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                     )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php // echo $form->uneditableRow($modRencanaLembur,'norencana',array('class'=>'span3')); ?>
                <?php echo $form->labelEx($modRealisasiLembur,'norealisasi', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modRealisasiLembur,'norealisasi',array('class'=>'span3 isRequiredNoRea', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
            
                </div>
            </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php $modRencanaLembur->mengetahui_nama = $modRencanaLembur->getPegawaiAttributes($modRencanaLembur->mengetahui_id,'nama_pegawai'); ?>
                <?php //echo $form->uneditableRow($modRencanaLembur,  'mengetahui_nama',array('class'=>'span3')); ?>
                <?php echo $form->labelEx($modRencanaLembur,'mengetahui_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur,'mengetahui_id', array('class'=>'isRequiredMeng'));?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'mengetahui_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/Mengetahui'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_mengetahui_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogMengetahui'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
            <div class="control-group">
                <?php $modRencanaLembur->menyetujui_nama = $modRencanaLembur->getPegawaiAttributes($modRencanaLembur->menyetujui_id,'nama_pegawai'); ?>
                <?php //echo $form->uneditableRow($modRencanaLembur,  'menyetujui_nama',array('class'=>'span3')); ?>
                <?php echo $form->labelEx($modRencanaLembur,'menyetujui_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur, 'menyetujui_id', array('class'=>'isRequiredMeny'));?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'menyetujui_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/Menyetujui'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_menyetujui_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogMenyetujui'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
        </td>
    </tr>
</table>

<table id="tabelPegawaiLembur" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th style="text-align: center;">No.</th>
        <th style="text-align: center;">No. Induk Pegawai</th>
        <th style="text-align: center;">Nama Pegawai</th>
        <!--<th style="text-align: center;">Departemen</th>-->
        <th style="text-align: center;">Jam Mulai</th>
        <th style="text-align: center;">Jam Selesai</th>
        <th style="text-align: center;">Alasan Lembur</th>
        <th style="text-align: center;">Pilih</th>
        
    </tr>
    </thead>
    <tbody>
        <?php                    
            $tr = '';
            $no = 1;
            $index = 0;
            $format = new CustomFormat;
             foreach ($modDetail as $key => $detail) {
                    if($modDetail[$key]->tglmulai != null)
                        $modDetail[$key]->jamMulai = date('H:i',strtotime($modDetail[$key]->tglmulai));
                    if($modDetail[$key]->tglselesai != null)
                        $modDetail[$key]->jamSelesai = date('H:i',strtotime($modDetail[$key]->tglselesai));
                    $tr.="<tr>
                       <td>".CHtml::TextField('nourut['.$index.']',$no,array('class'=>'span1 noUrut','readonly'=>TRUE))
                            .CHtml::activeHiddenField($modDetail[$key], '['.$index.']rencanalembur_id')
                            .CHtml::activeHiddenField($modDetail[$key], '['.$index.']pegawai_id')
                            ."</td>
                       <td>".$modDetail[$key]->pegawai->nomorindukpegawai."</td>
                       <td>".$modDetail[$key]->pegawai->nama_pegawai."</td>
                       <td>".CHtml::activetextField($modDetail[$key],'['.$index.']jamMulai',array('placeholder'=>'00:00','class'=>'span1 detailRequired','readonly'=>false, 'maxLength'=>5,'onkeypress'=>"return $(this).focusNextInputField(event)", 'onblur'=>'checkTime(this);'))."</td>
                       <td>".CHtml::activetextField($modDetail[$key],'['.$index.']jamSelesai',array('placeholder'=>'00:00','class'=>'span1 detailRequired','readonly'=>false, 'maxLength'=>5,'onkeypress'=>"return $(this).focusNextInputField(event)", 'onblur'=>'checkTime(this);'))."</td>
                       <td>".CHtml::activetextField($modDetail[$key],'['.$index.']alasanlembur',array('class'=>'span3 detailRequired','readonly'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>
                       <td>".CHtml::checkBox('pilih['.$index.']', true, array('class'=>'span1 pilih','onkeypress'=>"return $(this).focusNextInputField(event)"))."</td>
                       </tr>   
                   "; // <td>".$modDetail[$key]->pegawai->departement->departement_nama."</td>
                   $no++;
                   $index++;
             }
             echo $tr;
        ?>
    </tbody>
</table>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur, 'keterangan', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textArea($modRencanaLembur, 'keterangan',array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php $modRencanaLembur->pemberitugas_nama = $modRencanaLembur->getPegawaiAttributes($modRencanaLembur->pemberitugas_id,'nama_pegawai'); ?>
                <?php //echo $form->uneditableRow($modRencanaLembur,  'pemberitugas_nama',array('class'=>'span3')); ?>
                <?php echo $form->labelEx($modRencanaLembur, 'pemberitugas_id', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur,'pemberitugas_id', array('class'=>'isRequiredPemb'));?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'pemberitugas_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/PemberiTugas'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_pemberitugas_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogPemberiTugas'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
        </td>
    </tr>    

</table>
<div class="form-actions">
    <?php echo CHtml::htmlButton($modRencanaLembur->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php 
//    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
//            Yii::app()->createUrl($this->module->id.'/rencanaLemburT/informasi'), 
//            array('class'=>'btn btn-danger',
//                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            '#',
            array('class'=>'btn btn-danger',
                'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) {return false;} else{window.parent.$(\'#dialogRealisasiLembur\').dialog(\'close\');}')); ?>
</div>           

<?php $this->endWidget(); ?>

<?php
/**
 * Java Script untuk Cek Validasi jam mulai dan jam selesai tidak boleh kosong
 */

$jscript = <<<JS
function cekValidasi(event)
{ 
  kosong = ' ';
  pilih = true;
  detailRequired = $("#tabelPegawaiLembur tbody").find(".detailRequired[value="+kosong+"]");
  karlemburPilih = $("#tabelPegawaiLembur tbody").find(".pilih[value="+pilih+"]");
  jumlah =  detailRequired.length;      
  jumPilih =  karlemburPilih.length;      
  if ($('.isRequiredNoRea').val()==''){
    alert ('Silahkan Isi No. Realisasi');
    return false;
  }else 
  if ($('.isRequiredMeng').val()==''){
    alert ('Silahkan Isi Mengetahui');
    return false;
  }else 
  if ($('.isRequiredMeny').val()==''){
    alert ('Silahkan Isi Menyetujui');
    return false;
  }else
  if ($('.isRequiredPemb').val()==''){
    alert ('Silahkan Isi Pemberi Tugas');
    return false;
  }
//  else{
//    $('#btn_simpan').click();
//    return true; 
//  }
      else 
  if (jumlah==0){        
    $('#btn_simpan').click();
    return true;        
  }else{
    alert ('Jam Mulai, Jam Selesai dan Alasan Lembur Tidak Boleh Kosong!');
    return false;
  }
}
// Original JavaScript code by Chirp Internet: www.chirp.com.au 
// Please acknowledge use of this code by including this header. 
    function checkTime(field) { 
        var errorMsg = ""; 
        // regular expression to match required time format 
        re = /^(\d{1,2}):(\d{2})(:00)?([ap]m)?$/; 
        if(field.value != '') { 
            if(regs = field.value.match(re)) { 
                 
                // 24-hour time format 
                if(regs[1] > 23) { 
                    errorMsg = "Kesalahan format jam : " + regs[1] + ". Masukan jam antara 00 s.d 23 !"; 
                } 
                 
                if(!errorMsg && regs[2] > 59) { 
                    errorMsg = "Kesalahan format menit: " + regs[2] + ". Masukan menit antara 00 s.d 59 !"; 
                } 
            } else { 
                errorMsg = "Kesalahan format waktu: " + field.value + ". Masukan jam dan waktu antara 00:00 s.d 23:59 !"; 
            } 
       } 
       if(errorMsg != "") { 
           alert(errorMsg);
           field.value = "";
           field.focus();
           return false; 
       } 
       return true; 
}
JS;
Yii::app()->clientScript->registerScript('inputPegawai',$jscript, CClientScript::POS_HEAD);
?>

<?php 
//========= Dialog buat cari data Pegawai Mengetahui =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMengetahui',
    'options'=>array(
        'title'=>'Pencarian Mengetahui Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modMengetahui = new PegawaiM('search');
$modMengetahui -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modMengetahui->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'mengetahui-m-grid',
	'dataProvider'=>$modMengetahui->search(),
	'filter'=>$modMengetahui,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectMengetahui",
                                            "onClick" => "$(\"#KPRencanaLemburT_mengetahui_id\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'mengetahui_nama').'\").val(\"$data->nama_pegawai\");
                                                          $(\"#dialogMengetahui\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Departement',
//                            'value'=>'$data->departement->departement_nama',
//                            'filter'=>false,
//                        ),       
                

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Mengetahui dialog =============================
?>

<?php 
//========= Dialog buat cari data Pegawai Menyetujui =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMenyetujui',
    'options'=>array(
        'title'=>'Pencarian Menyetujui Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modMenyetujui = new PegawaiM('search');
$modMenyetujui -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modMenyetujui->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'menyetujui-m-grid',
	'dataProvider'=>$modMenyetujui->search(),
	'filter'=>$modMenyetujui,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectMenyetujui",
                                            "onClick" => "$(\"#KPRencanaLemburT_menyetujui_id\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'menyetujui_nama').'\").val(\"$data->nama_pegawai\");
                                                          $(\"#dialogMenyetujui\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Departement',
//                            'value'=>'$data->departement->departement_nama',
//                            'filter'=>false,
//                        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Menyetujui dialog =============================
?>

<?php 
//========= Dialog buat cari data Pegawai Lembur =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawaiLembur',
    'options'=>array(
        'title'=>'Pencarian Pegawai Lembur',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawaiLembur = new PegawaiM('search');
$modPegawaiLembur -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modPegawaiLembur->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'karlembur-m-grid',
	'dataProvider'=>$modPegawaiLembur->search(),
	'filter'=>$modPegawaiLembur,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPegawaiLembur",
                                            "onClick" => "$(\"#idPegawaiLembur\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'karlembur_nama').'\").val(\"$data->nama_pegawai\");
                                                          submitPegawaiLembur();
                                                          $(\"#dialogPegawaiLembur\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Departement',
//                            'value'=>'$data->departement->departement_nama',
//                            'filter'=>false,
//                        ),
                

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Lembur dialog =============================
?>

<?php 
//========= Dialog buat cari data Pemberi Tugas =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemberiTugas',
    'options'=>array(
        'title'=>'Pencarian Pemberi Tugas',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPemberiTugas = new PegawaiM('search');
$modPemberiTugas -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modPemberiTugas->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pemberitugas-m-grid',
	'dataProvider'=>$modPemberiTugas->search(),
	'filter'=>$modPemberiTugas,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPemberiTugas",
                                            "onClick" => "$(\"#KPRencanaLemburT_pemberitugas_id\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'pemberitugas_nama').'\").val(\"$data->nama_pegawai\");
                                                          $(\"#dialogPemberiTugas\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Departement',
//                            'value'=>'$data->departement->departement_nama',
//                            'filter'=>false,
//                        ),

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pemberi Tugas dialog =============================
?>