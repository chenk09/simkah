<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'golongan-gaji-m-search',
    'type'=>'horizontal',
)); ?>

                <?php echo $form->textFieldRow($model,'tunjangan_id',array('class'=>'span3','maxlength'=>15)); ?>

	<?php echo $form->dropDownListRow($model,'pangkat_id', CHtml::listData(PangkatM::model()->findAll('pangkat_aktif = true'), 'pangkat_id', 'pangkat_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>
         <?php echo $form->dropDownListRow($model,'jabatan_id', CHtml::listData(JabatanM::model()->findAll('jabatan_aktif = true'), 'jabatan_id', 'jabatan_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>

<?php echo $form->dropDownListRow($model,'komponengaji_id', CHtml::listData(KomponengajiM::model()->findAll('komponengaji_aktif = true'), 'komponengaji_id', 'komponengaji_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>

	<?php echo $form->textFieldRow($model,'nominaltunjangan',array('class'=>'span3','maxlength'=>50)); ?>

	<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
