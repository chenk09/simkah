<?php
$this->breadcrumbs=array(
	'Golongan Gaji Ms'=>array('index'),
	$model->tunjangan_id,
);

$arrMenu = array();
    array_push($arrMenu,array('label'=>Yii::t('mds','View').' Tunjangan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
    (Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Golongan Gaji', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'tunjangan_id',
		//'golonganpegawai.golonganpegawai_nama',
                                    'pangkat.pangkat_nama',
		'jabatan.jabatan_nama',
		'komponengaji.komponengaji_nama',
		'nominaltunjangan',
      
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>