<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'golongan-gaji-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#KPTunjanganM_tunjangan_id',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

         <?php echo $form->textFieldRow($model,'tunjangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
  
        <?php echo $form->dropDownListRow($model,'pangkat_id', CHtml::listData(PangkatM::model()->findAll('pangkat_aktif = true'), 'pangkat_id', 'pangkat_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>
         <?php echo $form->dropDownListRow($model,'jabatan_id', CHtml::listData(JabatanM::model()->findAll('jabatan_aktif = true'), 'jabatan_id', 'jabatan_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>

<?php echo $form->dropDownListRow($model,'komponengaji_id', CHtml::listData(KomponengajiM::model()->findAll('komponengaji_aktif = true'), 'komponengaji_id', 'komponengaji_nama'), array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", "empty"=>'-- Pilih --')); ?>
    
        <?php echo $form->textFieldRow($model,'nominaltunjangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
	<div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'setVerifikasi();return formSubmit(this,event)','onClick'=>'setVerifikasi();return false;')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.tunjanganM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('kepegawaian.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
        </div>

<?php $this->endWidget(); ?>

        <script type="text/javascript">
function setVerifikasi()

{

     var pangkat_id = $('#KPTunjanganM_pangkat_id').val();
      var jabatan_id = $('#KPTunjanganM_jabatan_id').val();
  if(pangkat_id != '' && jabatan_id !='')
 {
    alert('Hanya Boleh Memilih Pangkat atau Jabatan, tidak boleh Keduanya');

     } else{ 
         
          submit();
  }

}


</script>


