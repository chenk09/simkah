<legend class="rim">Pencarian</legend>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'kppengangkatantphl-t-form',
        'type'=>'horizontal',
)); ?>
<table class="table-condensed">
    <tr>
        <td>
            <?php //echo  $form->textFieldRow($model,'tgl_pendaftaran'); ?>
             <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'span3', 'maxlength'=>20)); ?>
            <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3', 'maxlength'=>10)); ?>
            <div class="control-group ">
                <?php echo CHtml::label('TMT','', array('class'=>'control-label')) ?>
                <div class="controls">
                     <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                    ),
                        )); ?> 
                </div>
            </div>
            
        </td>
        <td>
            
            <?php echo $form->textFieldRow($model,'pengangkatantphl_nosk',array('class'=>'span3','maxlength'=>50)); ?>
            <div class="control-group ">
                <label class='control-label'>Sampai Dengan</label>
                <div class="controls">
                    <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                    ),
                        )); ?> 
                </div>
            </div>
           
        </td>
    </tr>
</table>

	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
				<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/pengangkatantphlT/informasi'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
							  	<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	
	</div>

<?php $this->endWidget(); ?>
