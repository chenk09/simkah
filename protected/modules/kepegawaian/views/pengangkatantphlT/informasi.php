<fieldset>
    <legend class="rim2">Informasi Pengangkatan TPHL</legend>
</fieldset>


<?php
$this->breadcrumbs=array(
	'Kppengangkatantphl Ts'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kppengangkatantphl-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>


<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'kppengangkatantphl-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Pengangkatan TPHL</center>',
                'start'=>8, //indeks kolom 3
                'end'=>14, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                   // 'header'=>'NIP',
                    'name'=>'nomorindukpegawai',
                    'value'=>'$data->pegawai->nomorindukpegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
		array(
                    'header'=>'Gelar Depan',
                    'name'=>'gelardepan',
                    'value'=>'$data->pegawai->gelardepan',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'nama_pegawai',
                    'value'=>'$data->pegawai->nama_pegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Tempat, <br> Tanggal Lahir',
                    'value'=>'$data->pegawai->tempatlahir_pegawai." , ".$data->pegawai->tgl_lahirpegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'name'=>'jeniskelamin',
                    'value'=>'$data->pegawai->jeniskelamin',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Agama',
                    'name'=>'agama',
                    'value'=>'$data->pegawai->agama',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Status Perkawinan',
                    'name'=>'statusperkawinan',
                    'value'=>'$data->pegawai->statusperkawinan',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                //    'header'=>'Alamat Pegawai',
                    'name'=>'alamat_pegawai',
                    'value'=>'$data->pegawai->alamat_pegawai',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'No Perjanjian',
                    'name'=>'pengangkatantphl_noperjanjian',
                    'value'=>'$data->pengangkatantphl_noperjanjian',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'TMT',
                    'name'=>'pengangkatantphl_tmt',
                    'value'=>'$data->pengangkatantphl_tmt',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                 array(
                    'header'=>'No SK',
                    'name'=>'pengangkatantphl_nosk',
                    'value'=>'$data->pengangkatantphl_nosk',
                     'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Tugas Pekerjaan',
                    'name'=>'pengangkatantphl_tugaspekerjaan',
                    'value'=>'$data->pengangkatantphl_tugaspekerjaan',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                array(
                    'header'=>'Tgl SK',
                    'name'=>'pengangkatantphl_tglsk',
                    'value'=>'$data->pengangkatantphl_tglsk',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
                 array(
                    'header'=>'TMT SK',
                    'name'=>'pengangkatantphl_tmtsk',
                    'value'=>'$data->pengangkatantphl_tmtsk',
                     'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                ),
		/*
		'pengangkatantphl_tglsk',
		'pengangkatantphl_tmtsk',
		'pengangkatantphl_noskterakhir',
		'pengangkatantphl_keterangan',
		'pimpinannama',
		*/
//		array(
//                        'header'=>Yii::t('zii','View'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{view}',
//		),
//		array(
//                        'header'=>Yii::t('zii','Update'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{update}',
//                        'buttons'=>array(
//                            'update' => array (
//                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
//                                        ),
//                         ),
//		),
//		array(
//                    'header'=>Yii::t('zii','Delete'),
//                    'class'=>'bootstrap.widgets.BootButtonColumn',
//                    'template'=>'{remove} {delete}',
//                    'buttons'=>array(
//                    'remove' => array (
//                            'label'=>"<i class='icon-remove'></i>",
//                            'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
//                            'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->pengangkatantphl_id"))',
//                            //'visible'=>'($data->kabupaten_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                            'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
//                    ),
//                    'delete'=> array(
//                            'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
//                    ),
//                 )
//                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->renderPartial('_search',array('model'=>$model)); ?>

<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//        $this->widget('TipsMasterData',array('type'=>'admin'));
//        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
//        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
//
//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//    window.open("${urlPrint}/"+$('#kppengangkatantphl-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>