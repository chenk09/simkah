<?php
echo $this->renderPartial('application.views.headerReport.headerDefault', array('judulLaporan' => $judulLaporan));
?>
<table width="100%" style="margin-top:20px;">
    <tr>
        <td width ="13%">
        </td>
        <td  width ="33%">
            <?php
            $this->widget('ext.bootstrap.widgets.BootDetailView', array(
                'data' => $modPegawai,
                'attributes' => array(
//                    'pengangkatantphl_id',
//                    'pegawai_id',
//                'kelurahan.kelurahan_nama',
//                'kecamatan_id',
//                'profilrs_id',
//                'gelarbelakang_id',
//                'suku_id',
//                'kelompokpegawai_id',
//                'pendkualifikasi_id',
                    'nama_pegawai',
                'nama_keluarga',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'alamat_pegawai',
                'agama',
                'golongandarah',
                'jabatan.jabatan_nama',
                'pendidikan.pendidikan_nama',
//                'propinsi_id',
//                'pangkat_id',
//                'kabupaten_id',
                'nomorindukpegawai',
//                'no_kartupegawainegerisipil',
//                'no_karis_karsu',
//                'no_taspen',
//                'no_askes',
//                'gelardepan',
                
//                'rhesus',
                'alamatemail',
                'notelp_pegawai',
                'nomobile_pegawai',
//                'warganegara_pegawai',
//                'jeniswaktukerja',
//                'kelompokjabatan',
//                'kategoripegawai',
//                'kategoripegawaiasal',
//                'photopegawai',
//                'pegawai_aktif',
//                'esselon_id',
//                'statuskepemilikanrumah_id',
//                'jenisidentitas',
//                'noidentitas',
//                'nofingerprint',
//                'tinggibadan',
//                'beratbadan',
//                'kemampuanbahasa',
//                'warnakulit',
//                'nip_lama',
//                'no_rekening',
//                'bank_no_rekening',
//                'npwp',
//                'tglditerima',
//                'tglberhenti',
                ),
            ));
            ?>
        </td>
        <td width ="8%">
            </td>
        <td width ="33%"  style='vertical-align:top;'>
            <?php
            $this->widget('ext.bootstrap.widgets.BootDetailView', array(
                'data' => $model,
                'attributes' => array(
//                    'pengangkatantphl_id',
                    array(
                        'label'=>'Photo',
                        'type'=>'raw',
                        'value'=>CHtml::image(((!empty($model->pegawai->photopegawai))&&(file_exists(Params::pathPegawaiTumbsDirectory().$model->pegawai->photopegawai))) ? Params::urlPegawaiTumbsDirectory().'kecil_'.$modPegawai->photopegawai : 'http://localhost/simrs/data/images/pasien/no_photo.jpeg', '', array('style'=>'width:150px')),
                    ),
                    'pengangkatantphl_noperjanjian',
                    'pengangkatantphl_tmt',
                    'pengangkatantphl_tugaspekerjaan',
                    'pengangkatantphl_nosk',
                    'pengangkatantphl_tglsk',
                    'pengangkatantphl_tmtsk',
                    'pengangkatantphl_noskterakhir',
                    'pengangkatantphl_keterangan',
                    'pimpinannama',
                ),
            ));
            ?>
        </td>
        <td width ="13%">
        </td>
    </tr>
</table>
<?php //$this->widget('TipsMasterData', array('type' => 'view')); ?>