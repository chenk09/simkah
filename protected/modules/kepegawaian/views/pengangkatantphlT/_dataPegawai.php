<?php echo $form->errorSummary($model); ?>        
<table>
            <tr>
                <td width="60%">
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'required')); ?>
                    <div class="control-group">
                            <?php echo CHtml::label('Jenis Identitas','jenisidentitas',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'jenisidentitas',JenisIdentitas::items(),array('empty'=>'-- Pilih --','id'=>'jenisidentitas','style'=>'width:70px;')); ?>
                            <?php echo $form->textField($model,'jenisidentitas',array('empty'=>'-- Pilih --','id'=>'jenisidentitas','style'=>'width:135px;')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                      <?php echo $form->labelEx($model,'nama_pegawai',array('class'=>'control-label required')); ?>
                         <div class="controls inline">
                             <?php echo $form->dropDownList($model,'gelardepan',GelarDepan::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'style'=>'width:70px;')); ?>
                             <?php echo $form->textField($model,'nama_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'class'=>'inputRequire','style'=>'width:135px;')); ?>
                             <?php echo $form->dropDownList($model,'gelarbelakang_id',  CHtml::listData($model->getGelarBelakangItems(), 'gelarbelakang_id', 'gelarbelakang_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'style'=>'width:70px;')); ?>
                          </div>
                    </div>
                     <?php echo $form->textFieldRow($model,'nama_keluarga',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                      <?php echo $form->dropDownListRow($model,'jabatan_id',  CHtml::listData($model->getJabatanItems(), 'jabatan_id', 'jabatan_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                     <?php echo $form->dropDownListRow($model,'pangkat_id',  CHtml::listData($model->getPangkatItems(), 'pangkat_id', 'pangkat_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    </div>
                    <?php echo $form->textFieldRow($model,'tempatlahir_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                     
                    <?php echo $form->labelEx($model,'tgl_lahirpegawai', array('class'=>'control-label')) ?>
                       <div class="controls">  
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_lahirpegawai',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                    ),
                        )); ?> 
                        </div>
                    <?php echo $form->dropDownListRow($model,'kelompokpegawai_id',  CHtml::listData($model->getKelompokPegawaiItems(), 'kelompokpegawai_id', 'kelompokpegawai_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    <?php echo $form->radioButtonListInlineRow($model, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputRequire')); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('Tinggi / Berat Badan','tinggiberatbadan',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'tinggibadan',array('style'=>'width:50px;','id'=>'tinggiberatbadan')) ?>
                            <?php echo ' / '; ?>
                            <?php echo $form->textField($model,'beratbadan',array('style'=>'width:50px;','id'=>'tinggiberatbadan')) ?>
                        </div>
                    </div>
                     <?php echo $form->dropDownListRow($model,'statusperkawinan',StatusPerkawinan::items(), 
                                      array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                            'class'=>'inputRequire')); ?>
                     <?php echo $form->dropDownListRow($model,'agama',Agama::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                      <div class="control-group ">
                        <?php echo $form->labelEx($model,'golongandarah', array('class'=>'control-label')) ?>

                        <div class="controls">

                            <?php echo $form->dropDownList($model,'golongandarah', GolonganDarah::items(),  
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php echo $form->radioButtonList($model,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                                </div>
                           </div>
                            <?php echo $form->error($model, 'golongandarah'); ?>
                            <?php echo $form->error($model, 'rhesus'); ?>
                        </div>
                       </div>
                    <?php echo $form->dropDownListRow($model,'warganegara_pegawai',WargaNegara::items(),array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>25)); ?>
                     <?php echo $form->dropDownListRow($model,'suku_id',  CHtml::listData($model->getSukuItems(), 'suku_id', 'suku_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                     <?php echo $form->dropDownListRow($model,'pendidikan_id',  CHtml::listData($model->getPendidikanItems(), 'pendidikan_id', 'pendidikan_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    <?php echo $form->dropDownListRow($model,'pendkualifikasi_id',  CHtml::listData($model->getPendidikanKualifikasiItems(), 'pendkualifikasi_id', 'pendkualifikasi_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                </td>
                <td>
        <table>
             <tr>
                <td rowspan="4">
                     <?php echo $form->textAreaRow($model,'alamat_pegawai',array('rows'=>6, 'cols'=>50,  'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <?php echo $form->dropDownListRow($model,'propinsi_id', CHtml::listData($model->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                              'update'=>'#KPPegawaiM_kabupaten_id'))); ?>
                      <?php echo $form->dropDownListRow($model,'kabupaten_id', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                              'update'=>'#KPPegawaiM_kecamatan_id'))); ?>
                     <?php echo $form->dropDownListRow($model,'kecamatan_id', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                              'update'=>'#KPPegawaiM_kelurahan_id'))); ?>
                     <?php echo $form->dropDownListRow($model,'kelurahan_id', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                               )); ?>
                </td>
                <td>
                    <!--  ==kosong==-->
                </td>
             </tr>   
             <tr>
                <td>
                    <!-- == kosong == -->
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('No Telp / Hp','nomorcontact',array('class'=>'control-label')); ?>
                        <div class="controls">
                        <?php echo $form->textField($model,'notelp_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>15,'style'=>'width:97px;','id'=>'nomorcontact')); ?>
                            <?php echo ' / '; ?>
                        <?php echo $form->textField($model,'nomobile_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>15,'style'=>'width:97px;','id'=>'nomorcontact')); ?>
                        </div>
                    </div>
                </td>
                <td>
                </td>
            </tr>
            <tr>
                <td>
                      <?php echo $form->textFieldRow($model,'alamatemail',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </td>
                <td>
                   
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                         <div class="control-group">
                            <?php echo $form->labelEx($model,'caraAmbilPhoto', array('class'=>'control-label','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                               <div class="controls">  
                        <?php echo CHtml::radioButton('caraAmbilPhoto',false,array('value'=>'webCam','onclick'=>'caraAmbilPhotoJS(this)'));?> Web Cam
                        <?php echo CHtml::radioButton('caraAmbilPhoto',true,array('value'=>'file','onclick'=>'caraAmbilPhotoJS(this)'));?> File                               
                               </div>
                          </div>
                       
                    </div>
                    <div id="divCaraAmbilPhotoWebCam"  style="display:none;">
                    <?php 
                        $random=rand(0000000000000000, 9999999999999999);                    
                        $pathPhotoPegawaiTumbs=Params::pathPegawaiTumbsDirectory();
                        $pathPhotoPegawai=Params::pathPegawaiDirectory();
                    ?>
                    <?php $onBeforeSnap = "document.getElementById('upload_results').innerHTML = '<h1>Proses Penyimpanan...</h1>';";
                          $completionHandler = <<<BLOCK
                          if (msg == 'OK') 
                           {
                                document.getElementById('upload_results').innerHTML = '<h1>OK! ...Photo Sedang Disimpan</h1>';

                                // reset camera for another shot
                                webcam.reset();
                                setTimeout(function(){
                                document.getElementById('upload_results').innerHTML = '<h1>Photo Berhasil Disimpan</h1>';
                                },3000);
//                              $('#sapegawai-m-form').submit();           
                            }
                         else
                            {
                                alert("PHP Error: " + msg);
                            }
BLOCK;

      $this->widget('application.extensions.jpegcam.EJpegcam', array(
            'apiUrl' => 'index.php?r=photoWebCam/jpegcam.saveJpg&random='.$random.'&pathTumbs='.$pathPhotoPegawaiTumbs.'&path='.$pathPhotoPegawai.'',
            'shutterSound' => false,
            'stealth' => true,
            'buttons' => array(
                'configure' => 'Konfigurasi',
//                'takesnapshot' => 'Ambil Photo',
                'freeze'=>'Ambil Photo',
                'reset'=>'Ulang',
                
            ),
            'onBeforeSnap' => $onBeforeSnap,
            'completionHandler' => $completionHandler
        ));
?>     
<!--<img src="<?php //echo Params::urlPegawaiDirectory()?>9680901rizky.jpg " id="gambar">  -->

                        <div id="upload_results" style="background-color:#eee; margin-top:10px"></div>
                    </div>
                    <div id="divCaraAmbilPhotoFile" style="display: block;">
                          <div class="control-group">
                            <?php echo $form->labelEx($model,'photopegawai', array('class'=>'control-label','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                               <div class="controls">  
                                <?php echo Chtml::activeFileField($model,'photopegawai',array('maxlength'=>254,'Hint'=>'Isi Jika Akan Menambahkan Logo')); ?>
                                </div>
                          </div>
                      </div>
                    </td>
                    </tr>
        </table>
                </td>
            </tr> 
                    
        </table>