<?php
	$this->breadcrumbs = array(
		'Kppenggajianpeg Ts'=>array('index'),
		'Create',
	);
	$arrMenu = array();
	array_push($arrMenu, array(
		'label'=>'Penggajian Karyawan',
		'header'=>true,
		'itemOptions'=>array('class'=>'heading-master')
	));
	$this->menu = $arrMenu;
	$this->widget('bootstrap.widgets.BootAlert');
?>
<?php echo $this->renderPartial('_form', array(
	'model'=>$model,
	'modPegawai'=>$modPegawai
)); ?>
<?php
$urlPrint = Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id .'/'. Yii::app()->controller->id .'/printStruk', array("penggajianpeg_id"=>$model->penggajianpeg_id));
Yii::app()->clientScript->registerScript('search', "
	$('.cetak').on('click', function(){
		window.open('${urlPrint}','','location=_new, width=900px, scrollbars=yes');
		return false;
	});
");
?>