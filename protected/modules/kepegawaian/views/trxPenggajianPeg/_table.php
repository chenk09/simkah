<?php
$template = "{pager}{summary}\n{items}";
if($caraPrint)
{
	$template = "{items}";
}

$this->widget('ext.bootstrap.widgets.HeaderGroupGridViewNonRp',array(
	'id'=>'inf-kppenggajianpeg-t-grid',
	'dataProvider'=>$model->searchInformasi($caraPrint),
	'template'=>$template,
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
			'name'=>'pegawai_id',
			'value'=>'$data->pegawai->nama_pegawai',
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'colspan'=>2,
			),				
			'footer'=>'Total',
		),
		'pegawai.kategoripegawai',
		'nopenggajian',
		array(
			'name'=>'totalterima',
			'value'=>'MyFunction::formatNumber($data->totalterima)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(totalterima)',
		),
		array(
			'name'=>'totalpotongan',
			'value'=>'MyFunction::formatNumber($data->totalpotongan)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(totalpotongan)',
		),
		array(
			'name'=>'penerimaanbersih',
			'value'=>'MyFunction::formatNumber($data->penerimaanbersih)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(penerimaanbersih)',
		),
		array(
			'name'=>'periodegaji',
			'footer'=>'&nbsp;',
		),
		array(
			'name'=>'gajipertahun',
			'value'=>'MyFunction::formatNumber($data->gajipertahun)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(gajipertahun)',
		),
		array(
			'name'=>'biayajabatan',
			'value'=>'MyFunction::formatNumber($data->biayajabatan)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(biayajabatan)',
		),
		array(
			'name'=>'potonganpensiun',
			'value'=>'MyFunction::formatNumber($data->potonganpensiun)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(potonganpensiun)',
		),
		array(
			'name'=>'kodeptkp',           
			'footer'=>'&nbsp;',
		),
		array(
			'name'=>'ptkppertahun',
			'value'=>'MyFunction::formatNumber($data->ptkppertahun)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(ptkppertahun)',
		),
		array(
			'name'=>'penerimaanbersihpertahun',
			'value'=>'MyFunction::formatNumber($data->penerimaanbersihpertahun)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(penerimaanbersihpertahun)',
		),
		array(
			'name'=>'pkp',
			'value'=>'MyFunction::formatNumber($data->pkp)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),                            
			'footer'=>'sum(pkp)',
		),
		array(
			'name'=>'pph21pertahun',
			'value'=>'MyFunction::formatNumber($data->pph21pertahun)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),
			'footer'=>'sum(pph21pertahun)',
		),
		array(
			'name'=>'pph21perbulan',
			'value'=>'MyFunction::formatNumber($data->pph21perbulan)',
			'htmlOptions'=>array('style'=>'text-align:right;'),
			'footerHtmlOptions'=>array(
				'style'=>'text-align:right;font-weight:bold;',
				'class'=>'currency'
			),
			'footer'=>'sum(pph21perbulan)',
		),
		array(
			'header'=>'Ubah Penggajian',
			'visible'=>($caraPrint ? false : true),
			'type'=>'raw',
			'value'=>'(isset($data->pengeluaranumum_id)) ? "-" : CHtml::link("<i class=\'icon-user\'></i>",Yii::app()->createUrl(\'kepegawaian/TrxPenggajianPeg/update&updateid=\'.$data->penggajianpeg_id),array("rel"=>"tooltip","title"=>"Klik untuk Mengubah Rincian Penggajian"))',
			'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
		),
		array(
			'header'=>'Cetak Faktur',
			'visible'=>($caraPrint ? false : true),
			'type'=>'raw',
			'value'=>'(isset($data->pengeluaranumum_id)) ? "-" : CHtml::link("<i class=\'icon-print\'></i>", "#", array("url_print"=>Yii::app()->createUrl(\'kepegawaian/TrxPenggajianPeg/printStruk&penggajianpeg_id=\'.$data->penggajianpeg_id),"onClick"=>"cetak_faktur(this);","rel"=>"tooltip","title"=>"Klik untuk Mencetak Rincian Penggajian"))',
			'htmlOptions'=>array(
				'style'=>'text-align: center;width:60px'
			),
		),		
	),
	'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php
Yii::app()->clientScript->registerScript('cetak_printer', "
	function cetak_faktur(obj){
		var urlPrint = $(obj).attr('url_print');
		window.open(urlPrint,'','location=_new, width=600px, scrollbars=yes');
		return false;
	}
", CClientScript::POS_BEGIN);
?>