<?php
$this->breadcrumbs = array(
	'Penggajian'=>array('admin'),
	'Informasi',
);

$urlPrint = Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id .'/'. Yii::app()->controller->id .'/print');
Yii::app()->clientScript->registerScript('search', "
	function set_width()
	{
		var width = $(document).width();
		var width = width - 50;
		$('#body_table').width(width);
	}
	set_width();
	
	$('#search-kppenggajianpeg-t-form').submit(function(){
		$.fn.yiiGridView.update('inf-kppenggajianpeg-t-grid',{
			data: $(this).serialize()
		});
		return false;
	});
	
	$('.cetak').on('click', function(){
		var caraPrint = $(this).attr('jenis');
		window.open('${urlPrint}&'+$('#search-kppenggajianpeg-t-form').serialize()+'&caraPrint='+caraPrint,'','location=_new, width=900px, scrollbars=yes');
		return false;
	});
	
");

?>

<fieldset>
	<legend class="rim">Filter Pencarian</legend>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'search-kppenggajianpeg-t-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'), 
	)); ?>
<table>
	<tr>
		<td width="400">
			<div class="control-group">
				<?php echo $form->labelEx($model, 'periodegaji', array('class' => 'control-label')); ?>
				<div class="controls">
					<?php $this->widget('MyDateTimePicker',array(
						'model'=>$model,
						'attribute'=>'periodegaji',
						'mode'=>'datetime',
						'options'=> array(
							'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
						),
						'htmlOptions'=>array('readonly'=>true,
							'onkeypress'=>"return $(this).focusNextInputField(event)",
							'class'=>'dtPicker3',
						 ),
					)); ?> 
				</div>
				<?php echo $form->textFieldRow($model,'nopenggajian',array(
					'class'=>'span3',
				)); ?>
			</div>
		</td>
		<td>
			<?php echo $form->textFieldRow($model,'pegawai_id',array(
				'class'=>'span3'
			)); ?>
			<?php echo $form->dropDownListRow($model,'kategoripegawai',KategoriPegawai::items(), array(
				'empty'=>'-- Pilih --',
				'onkeypress'=>"return $(this).focusNextInputField(event)", 
			)); ?>
		</td>
	</tr>
</table>

	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cari',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')), array(
			'class'=>'btn btn-success cetak',
			'jenis'=>'print',
			'type'=>'button',
			'style'=>'float:right'
		)); ?>
		
		<?php $this->widget('bootstrap.widgets.BootButtonGroup', array(
				'type'=>'success',
				'buttons'=>array(
					array(
						'label'=>'Export File', 'icon'=>'icon-print icon-white', 'url'=>"javascript:void(0)",
						'htmlOptions'=>array()
					),
					array(
						'label'=>'', 'items'=>array(
							array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>"#", 'itemOptions'=>array(
								'jenis'=>'pdf',
								'class'=>'cetak'
							)),
							array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>"#", 'itemOptions'=>array(
								'jenis'=>'excel',
								'class'=>'cetak'
							)),
						)
					)
				),
				'htmlOptions'=>array('style'=>'float:right')
		));?>
	</div>
	
	<?php $this->endWidget();?>
</fieldset>

<fieldset>
	<legend class="rim">Daftar Penggajian</legend>
	<div id="body_table" style="overflow-x:scroll;">
		<?php $this->renderPartial('_table',array('model'=>$model));?>
	</div>
</fieldset>