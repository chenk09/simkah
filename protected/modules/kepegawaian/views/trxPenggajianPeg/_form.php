<?php
if(strlen($modPegawai->pegawai_id) > 0 && is_null($model->penggajianpeg_id))
{
	Yii::app()->clientScript->registerScript('auto_load',"
		set_data_pegawai('$modPegawai->pegawai_id');
	");
}else{
	Yii::app()->clientScript->registerScript('auto_load',"
		$('.currency').each(function(index){
			var format_uang = $(this).val();
			$(this).val(formatNumber(format_uang));
		});
	");
}

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'kppenggajianpeg-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<fieldset>
    <legend class="rim">Data Karyawan</legend>
	<table id="daftar_info_karyawan">
		<tr>
			<td width="400">
				<?php echo $form->textFieldRow($modPegawai,'nomorindukpegawai',array(
					'id'=>'NIP',
					'onkeypress'=>"if (event.keyCode == 13){setNip(this);}return $(this).focusNextInputField(event)",
					'class'=>'span3',
					'readonly'=>true
				)); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Karyawan','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPegawai,'photopegawai',array()) ?>
                        <?php echo $form->hiddenField($modPegawai,'pegawai_id',array()) ?>
                        <?php echo $form->hiddenField($modPegawai,'lamakerja',array()) ?>
                        <?php echo $form->hiddenField($modPegawai,'kelompokpegawai_id',array('readonly'=>true,'id'=>'kelompokpegawai_id')) ?>
                        <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$modPegawai,
                            'attribute'=>'nama_pegawai',
                            'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/getDataPegawai'),
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 4,
                               'focus'=> 'js:function( event, ui ){
                                    return false;
                                }',
                               'select'=>'js:function( event, ui ){
								   set_data_pegawai(ui.item.value);
								   return false;
							   }',
                            ),
                            'htmlOptions'=>array(
								'onkeypress'=>"return $(this).focusNextInputField(event)",
								'class'=>'span2'
							),
                            'tombolDialog'=>array(
								'idDialog'=>"dialogPegawai",
								'idTombol'=>'tombolPasienDialog'
							),
                        )); ?>
                    </div>
				</div>
                <?php echo $form->textFieldRow($modPegawai,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai,'jeniskelamin',array('readonly'=>true)); ?>
                <?php echo $form->textFieldRow($modPegawai,"golonganpegawai_id",array('readonly'=>true, 'id'=>'golongan')); ?>
                <?php echo $form->textFieldRow($modPegawai,"pangkat_id",array('readonly'=>true, 'id'=>'pangkat')); ?>
                <?php echo $form->textFieldRow($modPegawai,"jabatan_id",array('readonly'=>true,'id'=>'jabatan')); ?>
                <?php echo $form->textFieldRow($modPegawai,"kategoripegawai",array('readonly'=>true,'id'=>'kategoripegawai')); ?>
			</td>
			<td width="400">
                <div class="control-group">
                    <?php echo $form->labelEx($modPegawai,'no_rekening',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modPegawai,'no_rekening',array('readonly'=>true,'class'=>'span2')); ?>
                        <?php echo $form->textField($modPegawai,'bank_no_rekening',array('readonly'=>true,'class'=>'span1','style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPegawai,'npwp',array('readonly'=>true,'id'=>'npwp')); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($modPegawai,'notelp_pegawai',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modPegawai,'notelp_pegawai',array('readonly'=>true,'id'=>'notelp', 'class'=>'span2')); ?>
                        <?php echo $form->textField($modPegawai,'nomobile_pegawai',array('readonly'=>true,'id'=>'nomobile', 'class'=>'span1', 'style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPegawai,'agama',array('readonly'=>true,'id'=>'agama')); ?>
                <?php echo $form->textAreaRow($modPegawai,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($modPegawai,'jml_tanggungan',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modPegawai,'jml_tanggungan',array('readonly'=>true,'id'=>'jml_tanggungan')); ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('Masa Kerja Awal','addmasakerja',array('class'=>'control-label')); ?>
                    <div class="controls">
						<?php echo $form->textField($modPegawai,'addmasakerja',array(
							'readonly'=>true,
							'style'=>'width:30px;',
							'class'=>'span2 number'
						)); ?> Tahun
                    </div>
                </div>
			</td>
			<td style="text-align:center">
                <?php
                    if(!empty($modPegawai->photopegawai))
					{
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'kecil_'.$modPegawai->photopegawai, 'photo pegawai', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'no_photo.jpeg', 'photo pegawai', array('id'=>'photo_pasien','width'=>150));
                    }
                ?>
			</td>
		</tr>
	</table>
</fieldset>

<fieldset>
    <legend class="rim">Informasi Lainnya</legend>
    <table>
        <tr>
            <td width="400">
				<?php echo $form->hiddenField($model,'pegawai_id',array()) ?>
                <div class="control-group">
					<?php echo $form->labelEx($model, 'tglpenggajian', array('class' => 'control-label')); ?>
                    <div class="controls">
						<?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tglpenggajian',
							'mode'=>'datetime',
							'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
							),
							'htmlOptions'=>array('readonly'=>true,
								'onkeypress'=>"return $(this).focusNextInputField(event)",
								'class'=>'dtPicker3',
							 ),
						)); ?>
                    </div>
					<?php echo $form->textFieldRow($model,'nopenggajian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                </div>
			</td>
            <td>
				<div class="control-group">
					<?php echo $form->labelEx($model, 'periodegaji', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'periodegaji',
							'mode'=>'date',
							'options'=> array(
								'dateFormat'=> 'MM yy',
								'changeYear' => true,
								'changeMonth' => true,
								'changeDate' => false,
								'showSecond' => false,
								'showDate' => false,
								'showMonth' => false,
								'onChangeMonthYear'=>'js:function(y, m, i){
										var d = i.selectedDay;
										$(this).datepicker("setDate", new Date(y, m - 1, d));
									}'
							),
							'htmlOptions'=>array('readonly'=>true,
								'onkeypress'=>"return $(this).focusNextInputField(event)",
								'class'=>'dtPicker3',
							),
						)); ?>
					</div>
				</div>
			</td>
			<td>
               <div class="control-group">
                    <?php echo CHtml::label('Hari Kerja','harikerja',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'harikerja',array(
							'class'=>'currency',
							'style'=>'width:40px;',
							'id'=>'harikerja',
							'readonly'=>true
						)) ?>
                        <?php echo ' Cuti '; ?>
                        <?php echo $form->textField($model,'cuti',array(
							'onKeyup'=>'hitung_total_hari_kerja()',
							'class'=>'currency potongan_hari',
							'style'=>'width:40px;',
							'id'=>'cuti',
							'readonly'=>false
						)) ?>
                        <?php echo ' Alpha '; ?>
                        <?php echo $form->textField($model,'alpha',array(
							'onKeyup'=>'hitung_total_hari_kerja()',
							'class'=>'currency potongan_hari',
							'style'=>'width:40px;',
							'id'=>'alpha',
							'readonly'=>false
						)) ?>
                        <?php echo ' Ijin '; ?>
                        <?php echo $form->textField($model,'ijin',array(
							'onKeyup'=>'hitung_total_hari_kerja()',
							'class'=>'currency potongan_hari',
							'style'=>'width:40px;',
							'id'=>'ijin',
							'readonly'=>false
						)) ?>
                    </div>
                </div>
				<?php echo $form->textFieldRow($model,'jmlefektifharikerja',array(
					'class'=>'currency',
					'readonly'=>true
				));?>
			</td>
		</tr>
	</table>
    <div class="control-group">
        <div class="controls">
            <table class='table'>
                <thead>
                    <tr>
                        <th width="250">Deskripsi</th>
                        <th>Penerimaan</th>
                        <th>Potongan</th>
                    </tr>
                </thead>
                <tbody>
					<?php
						$modKomponen = KomponengajiM::model()->findAll('komponengaji_aktif = true ORDER BY nourutgaji');
						foreach($modKomponen as $key=>$val)
						{
							$komponen = new PenggajiankompT;
							if(!is_null($model->penggajianpeg_id))
							{
								$_komponen = PenggajiankompT::model()->findByAttributes(array(
									"penggajianpeg_id"=>$model->penggajianpeg_id,
									"komponengaji_id"=>$val->komponengaji_id
								));
								$komponen = $_komponen;
							}
					?>
					<tr>
						<td>
							<?=$val->komponengaji_nama; ?>
							<?php
								echo $form->hiddenField($komponen, "[". $val->komponengaji_id ."]komponengaji_id", array(
									'value'=>$val->komponengaji_id,
									'class'=>'inputFormTabel'
								));
							?>
							<?php
								echo $form->hiddenField($komponen, "[". $val->komponengaji_id ."]penggajiankomp_id", array(
									'class'=>'inputFormTabel'
								));
							?>
						</td>
						<td>
							<?php
								if($val->ispotongan == false)
								{
									echo $form->textField($komponen, "[". $val->komponengaji_id ."]jumlah", array(
										'onKeyup'=>'hitung_total()',
										'class'=>'inputFormTabel currency ' .  ($val->komponengaji_nama == "PPh Pasal 21" ? "tunjangan_pajak" : "tunjangan" )
									));
									echo CHtml::hiddenField(CHtml::activeId($komponen, "[". $val->komponengaji_id ."]jumlah") . '_temp', 0, array());
								}
							?>
						</td>
						<td>
							<?php
								if($val->ispotongan == true)
								{
									echo $form->textField($komponen, "[". $val->komponengaji_id ."]jumlah", array(
										'onKeyup'=>'hitung_total()',
										'class'=>'inputFormTabel currency ' .  ($val->komponengaji_id == "35" ? "potongan_pajak" : "potongan" )
									));
								}
							?>
						</td>
					</tr>
					<?php
						}
					?>
				</tbody>
				<tfoot>
                    <tr>
                        <th>Total</th>
                        <th>
							<?php echo $form->textField($model,'totalterima_bersih',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
						</th>
                        <th>
							<?php echo $form->textField($model,'totalpotongan_bersih',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
						</th>
                    </tr>
					<tr>
                        <th>Total + Pajak</th>
                        <th>
							<?php echo $form->textField($model,'totalterima',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
						</th>
                        <th>
							<?php echo $form->textField($model,'totalpotongan',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
						</th>
                    </tr>
				</tfoot>
			</table>
		</div>
	</div>
	<table>
		<tr>
			<td width="400">
                <?php echo $form->textFieldRow($model,'totalpajak',array('class'=>'span3 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model,'penerimaanbersih',array('class'=>'span3 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
				<?php echo $form->dropDownListRow($model,'keterangan', CaraBayarKeluar::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
			</td>
			<td>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'mengetahui',array('class'=>'control-label')) ?>
					<div class="controls">
						<?php $this->widget('MyJuiAutoComplete',array(
								'model'=>$model,
								'attribute'=>'mengetahui',
								'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
								'options'=>array(
								   'showAnim'=>'fold',
								   'minLength' => 4,
								   'focus'=> 'js:function( event, ui ) {
										$("#'.CHtml::activeId($model, 'mengetahui').'").val(ui.item.nama_pegawai);
										return false;
									}',
								   'select'=>'js:function( event, ui ) {
									   $("#'.CHtml::activeId($model, 'mengetahui').'").val(ui.item.nama_pegawai);
										return false;
									}',

								),
								'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
								'tombolDialog'=>array('idDialog'=>'dialogPegawai2','idTombol'=>'tombolPasienDialog'),
						)); ?>
					</div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'menyetujui',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $this->widget('MyJuiAutoComplete',array(
							'model'=>$model,
							'attribute'=>'menyetujui',
							'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
							'options'=>array(
							   'showAnim'=>'fold',
							   'minLength' => 4,
							   'focus'=> 'js:function( event, ui ) {
									$("#'.CHtml::activeId($model, 'menyetujui').'").val(ui.item.nama_pegawai);
									return false;
								}',
							   'select'=>'js:function( event, ui ) {
									$("#'.CHtml::activeId($model, 'menyetujui').'").val(ui.item.nama_pegawai);
									return false;
								}',
							),
							'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
							'tombolDialog'=>array('idDialog'=>'dialogPegawai3','idTombol'=>'tombolPasienDialog'),
                        )); ?>
                    </div>
                </div>
			</td>
			<td>
                <fieldset id="fieldsetpph21" class="">
					<legend class="accord1"><?php echo CHtml::checkBox('cex_pph21', false, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
						Perhitungan PPH 21
					</legend>
					<div id='pph21' class="toggle">
						<div class="control-group ">
							<?php echo $form->labelEx($model,'gajipertahun', array('class'=>'control-label')) ?>
							<div class="controls">
								<?php echo $form->textField($model,'gajipertahun',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								<?php echo $form->hiddenField($model,'persentasepph21',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								<?php echo $form->hiddenField($model,'kodeptkp',array('class'=>'inputFormTabel', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								/ Tahun
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'Biaya Jabatan (5%)', array('class'=>'control-label')) ?>
							<div class="controls">
								(<?php echo $form->textField($model,'biayajabatan',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>)
								Maks. 6.000.000 / Tahun
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'potonganpensiun', array('class'=>'control-label')) ?>
							<div class="controls">
								(<?php echo $form->textField($model,'potonganpensiun',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>)
								Maks. 2.400.000 / Tahun
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'ptkppertahun', array('class'=>'control-label')) ?>
							<div class="controls">
								(<?php echo $form->textField($model,'ptkppertahun',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>)
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'PKP', array('class'=>'control-label')) ?>
							<div class="controls">
								<?php echo $form->textField($model,'pkp',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'pphpersen', array('class'=>'control-label', 'id'=>'label_persen')) ?>
							<div class="controls">
								<?php echo $form->textField($model,'pphpersen',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								/ Tahun
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'pph21pertahun', array('class'=>'control-label', 'id'=>'label_persen')) ?>
							<div class="controls">
								<?php echo $form->textField($model,'pph21pertahun',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								/ Tahun
							</div>
						</div>
						<div class="control-group ">
							<?php echo $form->labelEx($model,'pph21perbulan', array('class'=>'control-label')) ?>
							<div class="controls">
								<?php echo $form->textField($model,'pph21perbulan',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
								/ Bulan
							</div>
						</div>
					</div>
                </fieldset>
			</td>
		</tr>
	</table>
</fieldset>

<div class="form-actions">
	<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;')); ?>
	<?php
		echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
			Yii::app()->createUrl($this->module->id.'/trxPenggajianPeg/create'),
			array(
				'class'=>'btn btn-danger',
				'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'
			)
		);
	?>
	<?php
		if(!is_null($model->penggajianpeg_id))
		{
			echo CHtml::htmlButton(Yii::t('mds','{icon} Print Struk',array('{icon}'=>'<i class="icon-print icon-white"></i>')), array(
				'penggajianpeg_id'=>$model->penggajianpeg_id,
				'class'=>'btn btn-success cetak',
				'type'=>'button',
			));
		}
	?>
</div>
<?php $this->endWidget();?>

<?php
$url_photo = Params::urlPegawaiTumbsDirectory();
$base_url = Yii::app()->baseUrl;
$urlAmbilDataKepegawaian = Yii::app()->createUrl('actionAjax/ambilDataKepegawaian');
$form_info = json_encode($this->renderPartial('_info_gaji', array(), true));
$kategoriPegawaiKhusus = Params::KATEGORI_PEGAWAI_KHUSUS;

$js = <<< JS

function hitung_total_hari_kerja()
{
	var harikerja = $("#KPPenggajianpegT_jmlefektifharikerja").val();
	var potongan_hari = 0;

	$(".potongan_hari").each(function(index){
		potongan_hari += parseInt(unformatNumber($(this).val()));
	});

	harikerja = harikerja - potongan_hari;
	$("#harikerja").val(harikerja);

	var alpha = $("#alpha").val();
	var ijin = $("#ijin").val();
	var tunjangan_harian = unformatNumber($("#PenggajiankompT_10_jumlah_temp").val());
	var sisa_tunjangan_harian = 0;

	if(alpha.length > 0 && alpha > 0)
	{
		var persent_aplha = (alpha * 30) / 100;
		sisa_tunjangan_harian = tunjangan_harian - (tunjangan_harian * persent_aplha);
	}

	if(ijin.length > 0 && ijin > 0)
	{
		var persent_ijin = (ijin * 10) / 100;

		if(sisa_tunjangan_harian > 0)
		{
			sisa_tunjangan_harian = sisa_tunjangan_harian - (sisa_tunjangan_harian * persent_ijin);
		}else if(sisa_tunjangan_harian == 0){
			sisa_tunjangan_harian = tunjangan_harian - (tunjangan_harian * persent_ijin);
		}

	}

	var sub_total = 0;
	if((alpha.length > 0 && alpha > 0 || ijin.length > 0 && ijin > 0))
	{
		if(sisa_tunjangan_harian > 0){
			sub_total = sisa_tunjangan_harian;
		}
	}else sub_total = tunjangan_harian;

	$("#PenggajiankompT_10_jumlah").val(formatNumber(sub_total));
	hitung_total();
}

function hitung_total()
{
	var kategoripegawai = $("#kategoripegawai").val();
	if (kategoripegawai != '$kategoriPegawaiKhusus'){
		hitung_jam_sostek();
		hitung_jam_sosial();
		hitung_jam_kesehatan();
		hitung_pot_kesehatan();
	}

	var tot_tunjangan = 0;
	$(".tunjangan").each(function(index){
		tot_tunjangan += unformatNumber($(this).val());
	});

	var tot_potongan = 0;
	$(".potongan").each(function(index){
		tot_potongan += unformatNumber($(this).val());
	});
	$("#KPPenggajianpegT_totalterima_bersih").val(formatNumber(tot_tunjangan));
	// $("#KPPenggajianpegT_penerimaanbersih").val(formatNumber(tot_tunjangan - tot_potongan));

	// $("#KPPenggajianpegT_totalterima").val(formatNumber(tot_tunjangan));
	// $("#KPPenggajianpegT_totalpotongan").val(formatNumber(tot_potongan));

	$("#KPPenggajianpegT_totalpotongan_bersih").val(formatNumber(tot_potongan));
	setTimeout(function(){
			pph_dua_satu();
	}, 500)
}

function set_data_pegawai(id_pasien)
{
	$.post('{$urlAmbilDataKepegawaian}&id_pasien='+id_pasien, {id_pasien:id_pasien}, function(data){

		$.each(data, function(key, value){
			$('form:not("#dialogPegawai")').find('input[name*="['+ key +']"]').val(value);
			$('#dataPegawai').find('td[name*="['+ key +']"]').text(value);
		});

		$("#PenggajiankompT_9_jumlah").val(data.gapok);
		if(Object.keys(data.tunjangan).length > 0)
		{
			$.each(data.tunjangan, function(key, value){
				$('#' + key).val(formatNumber(value));
				$('#' + key + '_temp').val(value);
			});
		}

		if(data.photopegawai != null){
			$("#photo_pasien").attr('src', '{$url_photo}kecil_'+data.photopegawai);
			$("#photo_pasien").attr('rand', Math.random());
		} else {
			$("#photo_pasien").attr('src','{$base_url}/data/images/pasien/no_photo.jpeg');
		}

		setTimeout(function(){
			hitung_total();
		}, 200);


	}, 'json');
}

function hitung_jam_sostek()
{
	var gapok = unformatNumber($("#PenggajiankompT_9_jumlah").val());
	var harian = unformatNumber($("#PenggajiankompT_10_jumlah").val());
	var jabatan = unformatNumber($("#PenggajiankompT_11_jumlah").val());
	var fungsional = unformatNumber($("#PenggajiankompT_12_jumlah").val());
	var khusus = unformatNumber($("#PenggajiankompT_13_jumlah").val());
	var kompetensi = unformatNumber($("#PenggajiankompT_19_jumlah").val());

	var total_jamsostek = (gapok + harian + jabatan + fungsional + khusus + kompetensi) * (6.24/100);
	var kategoripegawai = $("#kategoripegawai").val();
	if(kategoripegawai == "PERCOBAAN"){
		total_jamsostek = 0;
	}
	$("#PenggajiankompT_25_jumlah").val(formatNumber(total_jamsostek));
}

function hitung_jam_sosial()
{
	var gapok = unformatNumber($("#PenggajiankompT_9_jumlah").val());
	var harian = unformatNumber($("#PenggajiankompT_10_jumlah").val());
	var jabatan = unformatNumber($("#PenggajiankompT_11_jumlah").val());
	var fungsional = unformatNumber($("#PenggajiankompT_12_jumlah").val());
	var khusus = unformatNumber($("#PenggajiankompT_13_jumlah").val());
	var kompetensi = unformatNumber($("#PenggajiankompT_19_jumlah").val());
	var pensiun = unformatNumber($("#PenggajiankompT_17_jumlah").val());
	

	var total_jam_sosial = (gapok + harian + jabatan + fungsional + khusus + kompetensi + pensiun ) * (4.54/100);
	var kategoripegawai = $("#kategoripegawai").val();
	if(kategoripegawai == "PERCOBAAN"){
		total_jam_sosial = 0;
	}
	$("#PenggajiankompT_14_jumlah").val(formatNumber(total_jam_sosial));
}

function hitung_jam_kesehatan()
{
	var gapok = unformatNumber($("#PenggajiankompT_9_jumlah").val());
	var harian = unformatNumber($("#PenggajiankompT_10_jumlah").val());
	var jabatan = unformatNumber($("#PenggajiankompT_11_jumlah").val());
	var fungsional = unformatNumber($("#PenggajiankompT_12_jumlah").val());
	var khusus = unformatNumber($("#PenggajiankompT_13_jumlah").val());
	var kompetensi = unformatNumber($("#PenggajiankompT_19_jumlah").val());

	var total_jam_kesehatan = (gapok + harian + jabatan + fungsional + khusus + kompetensi) * (4/100);
	var kategoripegawai = $("#kategoripegawai").val();
	if(kategoripegawai == "PERCOBAAN"){
		total_jam_kesehatan = 0;
	}
	/*$("#PenggajiankompT_18_jumlah").val(formatNumber(total_jam_kesehatan));*/
}

function hitung_pot_kesehatan()
{
	var gapok = unformatNumber($("#PenggajiankompT_9_jumlah").val());
	var harian = unformatNumber($("#PenggajiankompT_10_jumlah").val());
	var jabatan = unformatNumber($("#PenggajiankompT_11_jumlah").val());
	var fungsional = unformatNumber($("#PenggajiankompT_12_jumlah").val());
	var khusus = unformatNumber($("#PenggajiankompT_13_jumlah").val());
	var kompetensi = unformatNumber($("#PenggajiankompT_19_jumlah").val());

	var total_pot_kesehatan = (gapok + harian + jabatan + fungsional + khusus + kompetensi) * (5/100);
		var kategoripegawai = $("#kategoripegawai").val();
		if(kategoripegawai == "PERCOBAAN"){
			total_pot_kesehatan = 0;
		}
	/*$("#PenggajiankompT_27_jumlah").val(formatNumber(total_pot_kesehatan));*/
	/*
	var addmasakerja = $("#KPPegawaiM_addmasakerja").val();
	if(addmasakerja > 0)
	{
		var kategoripegawai = $("#kategoripegawai").val();
		if(kategoripegawai == "PERCOBAAN"){
			total_pot_kesehatan = 0;
		}
		$("#PenggajiankompT_27_jumlah").val(formatNumber(total_pot_kesehatan));
	}else{
		$("#PenggajiankompT_27_jumlah").val(0);
	}*/
}

function pph_dua_satu()
{
	var totalterima = unformatNumber($("#KPPenggajianpegT_totalterima_bersih").val());
	var totalpotongan = unformatNumber($("#KPPenggajianpegT_totalpotongan_bersih").val());

	// var totalterima = unformatNumber($("#KPPenggajianpegT_totalterima").val());
	// var totalpotongan = unformatNumber($("#KPPenggajianpegT_totalpotongan").val());

	//var penerimaanbersih = unformatNumber($("#KPPenggajianpegT_penerimaanbersih").val());
	//var gapok = unformatNumber($("#PenggajiankompT_9_jumlah").val());
	//var penerimaanbersih = totalterima - totalpotongan;

	var penerimaanbersih = totalterima;
	var ptkp = unformatNumber($("#KPPenggajianpegT_ptkppertahun").val());
	var gajipph = penerimaanbersih * 12;

	var biayajabatan = (gajipph*0.05);
	if(biayajabatan > 6000000)
	{
		biayajabatan = 6000000;
	}

	var iuranpensiun = (gajipph*0.05);
	if(iuranpensiun > 2400000)
	{
		iuranpensiun = 2400000;
	}
	$("#KPPenggajianpegT_gajipertahun").val(formatNumber(gajipph));
	$("#KPPenggajianpegT_biayajabatan").val(formatNumber(biayajabatan));
	$("#KPPenggajianpegT_potonganpensiun").val(formatNumber(iuranpensiun));

	var penghasilan_netto = parseInt(gajipph) - (parseInt(biayajabatan) + parseInt(iuranpensiun));
	var val_pkp = parseInt(penghasilan_netto) - parseInt(ptkp);

	var pkp = 0;
	if(val_pkp > 0)
	{
		pkp = val_pkp;
	}
	$("#KPPenggajianpegT_pkp").val(formatNumber(pkp));

	var pph_dua_satu = 0;
	var persen_tahunan = 0;
	if(pkp > 500000000){
		pph_dua_satu = 50000000*0.05+200000000*0.15+250000000*0.25+(pkp-500000000)*0.3;
	}else if(pkp > 250000000){
		pph_dua_satu = 50000000*0.05+200000000*0.15+(pkp-250000000)*0.25;
	}else if(pkp > 50000000){
		pph_dua_satu = 50000000*0.05+(pkp-50000000)*0.15;
	}else{
		pph_dua_satu = pkp*0.05;
	}

	var pph_dua_satu_persent = 0;
	if(pph_dua_satu > 0)
	{
		pph_dua_satu_persent = (100 / pph_dua_satu) * 12;
		$("#KPPenggajianpegT_totalpotongan").val(totalpotongan + (pph_dua_satu/12));
		$("#KPPenggajianpegT_totalterima").val(totalterima + (pph_dua_satu/12));
	}else{
		$("#KPPenggajianpegT_totalpotongan").val(totalpotongan);
		$("#KPPenggajianpegT_totalterima").val(totalterima);
	}
	$("#PenggajiankompT_15_jumlah").val(formatNumber(pph_dua_satu/12));
	$("#PenggajiankompT_35_jumlah").val(formatNumber(pph_dua_satu/12));

	$("#KPPenggajianpegT_pphpersen").val(pph_dua_satu_persent);
	$("#KPPenggajianpegT_pph21perbulan").val(formatNumber(pph_dua_satu/12));
	$("#KPPenggajianpegT_totalpajak").val(formatNumber(pph_dua_satu/12));

	//var sisa_bersih = penerimaanbersih - (pph_dua_satu/12);
	var sisa_bersih = totalterima - totalpotongan;
	$("#KPPenggajianpegT_penerimaanbersih").val(formatNumber(sisa_bersih));
	$("#KPPenggajianpegT_pph21pertahun").val(formatNumber(pph_dua_satu));

	$('.currency').each(function(index){
		var format_uang = $(this).val();
		$(this).val(formatNumber(format_uang));
	});


}

function setVerifikasi()
{
	var penerimaanbersih = unformatNumber($("#KPPenggajianpegT_penerimaanbersih").val());
	if(penerimaanbersih > 0)
	{
		var confirm = $('#dialogVerifikasi').dialog('open');
		var penerimaanbersih = $('#KPPenggajianpegT_penerimaanbersih').val();
		var totalpajak = $('#KPPenggajianpegT_totalpajak').val();

		var totalterima = $('#KPPenggajianpegT_totalterima').val();
		var totalpotongan = $('#KPPenggajianpegT_totalpotongan').val();
		$('#dataPegawai').find('td[name*="calc_total"]').text(totalterima + '  - ' + totalpotongan);

		var subtotal = unformatNumber(totalterima) - unformatNumber(totalpotongan);
		$('#dataPegawai').find('td[name*="sub_total"]').text(formatNumber(subtotal));
		$('#dataPegawai').find('td[name*="total_pajak"]').text(totalpajak);
		$('#dataPegawai').find('td[name*="penerimaan_bersih"]').text(penerimaanbersih);

	}else{
		alert("Isian kurang lengkap, harap periksa kembali!!");
	}
	return false;
}

function submitData()
{
	$("#dialogVerifikasi").dialog("close");

	$(".currency").each(function(index){
		var nilai_uang = unformatNumber($(this).val());
		$(this).val(nilai_uang);
	});
	$("#kppenggajianpeg-t-form").submit();
}

function closeVerifikasi()
{
	$("#dialogVerifikasi").dialog("close");
}

$("#PenggajiankompT_10_jumlah").on("keyup",function(obj){
	console.log(1234);
	var jumlah = unformatNumber($(this).val());
	$("#PenggajiankompT_10_jumlah_temp").val(jumlah);
});

JS;
Yii::app()->clientScript->registerScript('common_script', $js, CClientScript::POS_BEGIN);
?>

<?php

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));
$modPegawaiReg = new KPRegistrasifingerprint();
if(isset($_GET['KPRegistrasifingerprint'])) $modPegawaiReg->attributes = $_GET['KPRegistrasifingerprint'];
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawaiReg->search(),
	'filter'=>$modPegawaiReg,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
			'header'=>'Pilih',
			'type'=>'raw',
			'value'=> function($data){
				$result = json_encode($data);
				$rec = CHtml::Link("<i class=\"icon-check\"></i>","",array(
					"class"=>"btn-small",
					"href"=>"#",
					"onClick" =>"$(\"#dialogPegawai\").dialog(\"close\");set_data_pegawai($data->pegawai_id);return false;"
				));
				return $rec;
			},
		),
        'nomorindukpegawai',
        'nama_pegawai',
        'tempatlahir_pegawai',
        'tgl_lahirpegawai',
        'jeniskelamin',
        'statusperkawinan',
        'jabatan.jabatan_nama',
        'alamat_pegawai',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();

?>


<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai2',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawaiFinger = new KPRegistrasifingerprint();
if (isset($_GET['KPRegistrasifingerprint'])) $modPegawaiFinger->attributes = $_GET['KPRegistrasifingerprint'];
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai4-m-grid',
	'dataProvider'=>$modPegawaiFinger->search(),
	'filter'=>$modPegawaiFinger,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array(
							"class"=>"btn-small",
							"id" => "selectPegawai",
							"href"=>"",
							"onClick" => "
								$(\"#'.CHtml::activeId($model, 'mengetahui').'\").val(\"$data->nama_pegawai\");
								$(\"#dialogPegawai2\").dialog(\"close\");
								return false;"
							)
						)',
                    ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'jabatan.jabatan_nama',
                'alamat_pegawai',
            ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>
<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai3',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawaiFingerDua = new KPRegistrasifingerprint();
if (isset($_GET['KPRegistrasifingerprint']))
    $modPegawaiFingerDua->attributes = $_GET['KPRegistrasifingerprint'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai5-m-grid',
	'dataProvider'=>$modPegawaiFingerDua->search(),
	'filter'=>$modPegawaiFingerDua,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
			array(
				'header'=>'Pilih',
				'type'=>'raw',
				'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small",
					"id" => "selectPegawai",
					"href"=>"",
					"onClick" => "
						$(\"#'.CHtml::activeId($model, 'menyetujui').'\").val(\"$data->nama_pegawai\");
						$(\"#dialogPegawai3\").dialog(\"close\");
						return false;
				"))',
			),
		'nomorindukpegawai',
		'nama_pegawai',
		'tempatlahir_pegawai',
		'tgl_lahirpegawai',
		'jeniskelamin',
		'statusperkawinan',
		'jabatan.jabatan_nama',
		'alamat_pegawai',
	),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>

<!-- VERIFIKASI PENGGAJIAN PEGAWAI -->
<?php
	$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
		'id'=>'dialogVerifikasi',
		'options'=>array(
			'title'=>'Verifikasi Penggajian',
			'autoOpen'=>false,
			'modal'=>true,
			'width'=>850,
			'height'=>350,
			'resizable'=>false,
		),
	));
?>
<div id="dataPegawai">
	<form>
		<legend class=rim>Informasi Penggajian Pegawai</legend>
		<table class="table table-bordered  table-condensed">
			<tbody>
				<tr>
					<td width="150">NRK</td>
					<td width="10" align="center">:</td>
					<td name="KPPegawaiM[nomorindukpegawai]"><b><?=$modPegawai->nomorindukpegawai?></b></td>
				</tr>
				<tr>
					<td>Nama</td>
					<td align="center">:</td>
					<td name="KPPegawaiM[nama_pegawai]"><b><?=$modPegawai->nama_pegawai?></b></td>
				</tr>
				<tr>
					<td>Sub Total</td>
					<td align="center">:</td>
					<td name="calc_total">&nbsp;</td>
				</tr><tr>
					<td>&nbsp;</td>
					<td align="center">:</td>
					<td name="sub_total">&nbsp;</td>
				</tr>
				<tr>
					<td>Total Pajak</td>
					<td align="center">:</td>
					<td name="total_pajak">&nbsp;</td>
				</tr>
				<tr>
					<td>Penerimaan Bersih</td>
					<td align="center">:</td>
					<td name="penerimaan_bersih">&nbsp;</td>
				</tr>
			</tbody>
		</table>
		<div class="form-actions" align="center">
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array(
				'class'=>'btn btn-primary',
				'type'=>'button',
				'onKeypress'=>'submitData();return false;',
				'onClick'=>'submitData();return false;'
			)); ?>
			<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Batal',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array(
				'class'=>'btn btn-success',
				'type'=>'button',
				'onKeypress'=>'closeVerifikasi();return false;',
				'onClick'=>'closeVerifikasi();return false;'
			)); ?>
		</div>
	</form>
</div>
<!-- END FORM VERIFIKASI PENGGAJIAN PEGAWAI -->
<?php $this->endWidget(); ?>
