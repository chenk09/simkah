<div align="center" style="font-size:18px;"><b>LAPORAN PENGGAJIAN</b></div>
<hr>
<div>&nbsp;</div>

<?php
	if($caraPrint == 'excel')
	{
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="lap-penggajian-'.date("Y/m/d").'.xls"');
		header('Cache-Control: max-age=0');
	}
?>

<?php $this->renderPartial('_table',array(
	'model'=>$model,
	'caraPrint'=>true
));?>