<?php
	$this->breadcrumbs=array(
		'Kppenggajianpeg Ts'=>array('index'),
		'Create',
	);
	$arrMenu = array();
	array_push($arrMenu, array(
		'label'=>'Penggajian Karyawan',
		'header'=>true,
		'itemOptions'=>array('class'=>'heading-master')
	));
	$this->menu = $arrMenu;
	$this->widget('bootstrap.widgets.BootAlert');
?>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modPegawai'=>$modPegawai, 'komponen'=>$komponen)); ?>