<?php 
if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
          header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
          header('Cache-Control: max-age=0');     
    }    
?>

   
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php 
$format=new CustomFormat(); $tgl = explode(" ",$model->periodegaji); $tgl_periksa = $format->formatDateMediumForUser($tgl[0]." ".$tgl[1]);
$format2=new CustomFormat(); $tgl2 = explode(" ",$model->tglpenggajian); $tgl_periksa2 = $format2->formatDateMediumForUser($tgl2[0]." ".$tgl2[1]." ".$tgl2[2]);
?>
    <table id='kwitansiGaji' width='500px'>
        <tr>
            <td colspan='3'>
                <?php echo $this->renderPartial('application.views.headerReport.headerDefaultPolos',array('judulLaporan'=>$judulLaporan));  ?>
            </td>
        </tr>
      <tr>
           <td>Periode Gaji</td>
           <td>:</td>
           <td><?php echo $format->formatDateINAbulantanggal(date($tgl_periksa)); ?></td>
      </tr>
      <tr>
            <td>Tanggal Penggajian</td>
            <td>:</td>
            <td><?php echo $format2->formatDateINAtime(date($tgl_periksa2)); ?></td>
       </tr>
       <tr>
             <td>NRK</td>
             <td>:</td>
             <td><?php echo $model->pegawai->nomorindukpegawai; ?> </td>
       </tr>
       <tr>
             <td>Nama Karyawan</td>
             <td>:</td>
             <td><?php echo $form->hiddenField($model->pegawai,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?><?php echo $model->pegawai->nama_pegawai; ?> </td>
       </tr>
       <tr>
           <td>No Rekening</td>
           <td>:</td>
           <td><?php echo $model->pegawai->no_rekening; ?><?php echo $model->pegawai->bank_no_rekening; ?></td>
       </tr>
    </table>
     <br />
          <div class="span-25tmp">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            GAJI :
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                  <tbody>
                <?php
                $modKomponen = KomponengajiM::model()->findAll('komponengaji_aktif = true AND ispotongan=FALSE ORDER BY nourutgaji'); 
                if (count($modKomponen > 0)) { 
                foreach ($modKomponen as $i=>$v){ 
                    $modgaji = PenggajiankompT::model()->findByAttributes(array('komponengaji_id'=>$v->komponengaji_id,'penggajianpeg_id'=>$model->penggajianpeg_id))
                ?>
                <tr>
                      <?php 
                    if ($v->ispotongan == false){
                        ?>
                    <td> <?php echo $v->komponengaji_nama; ?> </td>
                     <td><?php  echo "Rp. ".MyFunction::formatNumber($modgaji->jumlah); ?> </td>
                    <?php }
                    else { ?>
                      <td> <?php echo $v->komponengaji_nama; ?></td>
                      <td> <?php  echo "Rp. ".MyFunction::formatNumber($modgaji->jumlah); ?> </td>
                <?php } 
                        ?>
                      </tr>
                <?php }}?>
                 
                </tbody>
                <tfoot>
                    <tr>
                       <th>
                            Total Gaji
                        </th>
                        <th>
                          <?php  echo "Rp. ".MyFunction::formatNumber($model->totalterima); ?>
                        </th>
                    </tr>
                </tfoot>
            </table>
          </div>
          <div class="span-25tmp">
            <table class="table">
              <thead>
                  <tr>
                      <th>
                          POTONGAN :
                      </th>
                      <th>
                      </th>
                  </tr>
              </thead>
              <tbody>
                <?php
                  $modKomponen = KomponengajiM::model()->findAll('komponengaji_aktif = true AND ispotongan=TRUE ORDER BY nourutgaji'); 
                  if (count($modKomponen > 0)) { 
                    foreach ($modKomponen as $i=>$v){ 
                      $modgaji = PenggajiankompT::model()->findByAttributes(array('komponengaji_id'=>$v->komponengaji_id,'penggajianpeg_id'=>$model->penggajianpeg_id))
                ?>
                <tr>
                  <td> <?php echo $v->komponengaji_nama; ?></td>
                  <td> <?php echo "Rp. ".MyFunction::formatNumber($modgaji->jumlah); ?> </td>
                </tr>
                <?php 
                    }
                  }
                ?>
              </tbody>
              <tfoot>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                  <th>
                      Total Potongan
                  </th>
                  <th>
                    <?php  echo "Rp. ".MyFunction::formatNumber($model->totalpotongan); ?>
                  </th>
                </tr>
              </tfoot>
            </table>
          </div>
<div style="clear:both;">
     <table class="table" style='width:550px;'>
         <tr>
            <td><B>Gaji Bersih</B></td>
            <td><B><?php echo "Rp. ".MyFunction::formatNumber($model->penerimaanbersih); ?></B></td>
          </tr>
          <tr>
            <td><B>Terbilang</B></td>
            <td><?php echo $this->terbilang($model->penerimaanbersih); ?> rupiah</td>
         </tr>
      </table>
</div>
<br/>     
 <table width="100%" id='kwitansiGaji' style='width:360px;'>
    <tr><td COLSPAN=6 HEIGHT=17 ALIGN=RIGHT VALIGN=MIDDLE><?php echo Tasikmalaya.', '.date('d M Y'); ?></td></tr>
             <tr><td COLSPAN=6 HEIGHT=17 ALIGN=RIGHT VALIGN=MIDDLE>Kepegawaian RSJK</td></tr>
             <tr><td>&nbsp;</td></tr>
             <tr><td>&nbsp;</td></tr>
              <tr><td COLSPAN=6 HEIGHT=17 ALIGN=RIGHT VALIGN=MIDDLE><?php echo $model->menyetujui; ?></td></tr>
</table>
 	
<?php

$this->endWidget(); ?>
    