<table class='table'>
    <?php $format = new CustomFormat; ?>
    <tr>
        <td>
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('pegawai_id')); ?>:</b>
            <?php echo CHtml::encode($modpinjaman->pegawai->nama_pegawai); ?>
            <br />
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('nopinjam')); ?>:</b>
            <?php echo CHtml::encode($modpinjaman->nopinjam); ?>
            <br />
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('tglpinjampeg')); ?>:</b>
            <?php echo $format->formatDateMediumForUserDMY($modpinjaman->tglpinjampeg); ?>
             <br/>
        </td>
        <td>
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('pegawai.nomorindukpegawai')); ?>:</b>
            <?php echo CHtml::encode($modpinjaman->pegawai->nomorindukpegawai); ?>
            <br />
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('jumlahpinjaman')); ?>:</b>
            <?php echo CHtml::encode(number_format($modpinjaman->jumlahpinjaman)); ?>
            <br />
            <b><?php echo CHtml::encode($modpinjaman->getAttributeLabel('lamapinjambln')); ?>:</b>
            <?php echo CHtml::encode($modpinjaman->lamapinjambln); ?>
            <br />
        </td>
    </tr>   
</table>
<table id="tableObatAlkes" class="table table-striped table-bordered table-condensed">
    <thead>
        <th>Angsuran/Bulan ke</th>
        <th>Tgl. Pembayaran</th>
        <th>Jumlah Bayar</th>
    </thead>
    <tbody>
    <?php
        $no=1;
        foreach($moddetailpinjam AS $detail): ?>
            <tr>   
                <td><?php echo $detail->angsuranke; ?></td>
                <td><?php echo $format->formatDateINA($detail->tglakanbayar); ?></td>
                <td><?php echo number_format($detail->jmlcicilan); ?></td>
            </tr>
    <?php 
        $no++; 
        endforeach;     
    ?>
    </tbody>
</table>

<div class="form-actions">
    <?php 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'onKeypress'=>'print("PRINT");', 'onclick'=>'print("PRINT");'));
    ?>
</div>

<?php
$urlPrint = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print&id='.$modpinjaman->pinjamanpeg_id);
$jscript = <<< JS

function print(string){
    window.open("${urlPrint}&caraPrint=PRINT","",'location=_new, width=900px');
}
JS;
Yii::app()->clientScript->registerScript('detailinformasi',$jscript, CClientScript::POS_HEAD);
?>