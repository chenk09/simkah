<script type="text/javascript">
    
    function hitung_pinjam(){
        var jml_pinjam  = $("#KPPinjamanPegT_jumlahpinjaman").val();
        var lama_pinjam = $("#KPPinjamanPegT_lamapinjambln").val();
        var bunga       = $("#KPPinjamanPegT_persenpinjaman").val();

        // $.ajax({
        //     url: "hitung.php",
        //     data: "jml_pinjam=" + jml_pinjam + "&lama_pinjam="+lama_pinjam,
        //     success: function(data){
        //         // jika data sukses diambil dari server, tampilkan di <select id=kota>
        //         $("#detail").html(data);
        //     }
        // });
        //alert(nopinjam);
        return false;
    }
    
</script>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pinjamanpeg-t-form',
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
    'focus'=>'#',
)); ?>

<?php $this->renderPartial('_pegawai', array('model'=>$modPegawai, 'form'=>$form)); ?>
<?php echo $form->errorSummary($model); ?>

<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglpinjampeg', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php $this->widget('MyDateTimePicker',array(
                        'model'=>$model,
                        'attribute'=>'tglpinjampeg',
                        'mode'=>'date',
                        'options'=> array(
                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions'=>array('readonly'=>true,
                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                            'class'=>'dtPicker3',
                        ),
                    )); ?> 
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'nopinjam',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'jumlahpinjaman', array('class' => 'control-label')); ?>
                <div class="controls">                
                    <?php echo $form->textField($model,'jumlahpinjaman',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Rupiah
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'lamapinjambln', array('class' => 'control-label')); ?>
                <div class="controls">                
                    <?php echo $form->textField($model,'lamapinjambln',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Bulan
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'persenpinjaman', array('class' => 'control-label')); ?>
                <div class="controls">                
                    <?php echo $form->textField($model,'persenpinjaman',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> %
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textAreaRow($model,'untukkeperluan',array('rows'=>3, 'cols'=>30, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($model,'keterangan',array('rows'=>3, 'cols'=>30, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?php
                if(empty($model->pinjamanpeg_id)){
            ?>
            <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>HITUNG',
                array('onclick'=>'submithitung();return false;',
                      'class'=>'btn btn-primary',
                      'onkeypress'=>"submithitung();return $(this).focusNextInputField(event);",
                      'rel'=>"tooltip",
                      'title'=>"Klik Untuk Menghitung Tabel Pembayaran",
                      
                )); 
                } 
            ?>
        </td>
    </tr>
</table>
	
<?php if (isset($modPinjamDetail)) { 
    echo $form->errorSummary($modPinjamDetail);
}
?>
<table id="tabledetailpinjaman" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.</th>
        <th>Bulan Ke</th>
        <th>Tgl Pembayaran</th>
        <th>Jumlah Bayar</th>
    </tr>
    </thead>
    <tbody>
        <?php 
            $idpinjaman = $model->pinjamanpeg_id;
            $format = new CustomFormat;
            if (isset($idpinjaman)){
                $modPinjamDetail = KPPinjamPegDetT::model()->findAllByAttributes(array('pinjamanpeg_id'=>$idpinjaman),array('order'=>'angsuranke'));
                // print_r(count($modPinjamDetail));
                if (count($modPinjamDetail) > 0){
                    $tr = '';
                    $no = 1;
                    foreach ($modPinjamDetail as $key => $detail) {
                        $tr="<tr>
                            <td>". CHtml::TextField('noUrut',$no++,array('class'=>'span1 noUrut','readonly'=>TRUE)).
                                   CHtml::activeHiddenField($detail,'['.$key.']pinjampegdet_id',array('class'=>'pinjamDetail')).
                                   CHtml::activeHiddenField($detail,'['.$key.']pinjamanpeg_id'). 
                                   CHtml::activeHiddenField($detail,'['.$key.']tandabuktibayar_id').
                           "</td>
                            
                            <td>".CHtml::activeTextField($detail,'['.$key.']angsuranke',array('class'=>'span1 numbersOnly ','readonly'=>true))."</td>
                            <td>".$format->formatDateINA($detail['tglakanbayar'])."</td>
                            <td>".CHtml::activeTextField($detail,'['.$key.']jmlcicilan',array('class'=>'span1 numbersOnly','readonly'=>true))."</td>".
                            "</tr>   
                        ";
                        echo $tr;
                    }
                    
                }
            }
        ?>

    </tbody>
</table>


<div class="form-actions">
    <?php 
        if(isset($model->pinjamanpeg_id)){
            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'onKeypress'=>'print("PRINT");', 'onclick'=>'print("PRINT");'));
        }else{
    ?>
    <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); 
        }
    ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            Yii::app()->createUrl($this->module->id.'/PinjamanPegT/create'), 
            array('class'=>'btn btn-danger',
            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
        ?>
        <?php 
            $content = $this->renderPartial('../tips/transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'create','content'=>$content));
                
        ?>
</div>

<?php $this->endWidget(); ?>

<?php
$urlGetObatAlkes = Yii::app()->createUrl('actionAjax/getObatAlkes');
// $pinjamDetail=CHtml::activeId($modPinjamDetail,'pinjamDetail');

$urlPrint = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print&id='.$model->pinjamanpeg_id);
$jscript = <<< JS

function print(string){
    window.open("${urlPrint}&caraPrint=PRINT","",'location=_new, width=900px');
}

function submithitung()
{
    jumlah_pinjam   = $('#KPPinjamanPegT_jumlahpinjaman').val();
    lama_pinjam     = $('#KPPinjamanPegT_lamapinjambln').val();
    bunga_pinjam    = $('#KPPinjamanPegT_persenpinjaman').val();
    tgl_pinjam      = $('#KPPinjamanPegT_tglpinjampeg').val();
    
    n = tgl_pinjam.split(" ");
    tgl = n[0];
    bln = angkabulan(n[1]);
    thn = n[2];

    $('#tabledetailpinjaman').find('tbody').empty();
    
    if(tgl_pinjam=='')
    {
        alert('Silahkan isi Tanggal Peminjaman terlebih dahulu');
    }
    else if(jumlah_pinjam==''){
        alert('Silahkan isi Jumlah Pinjaman terlebih dahulu');
    }
    else if(lama_pinjam=='' || lama_pinjam==0)
    {
        alert('Silahkan isi Lama Pinjaman terlebih dahulu dan tidak boleh bernilai nol');
    }
    else if(bunga_pinjam=='')
    {
        alert('Silahkan isi Bunga Pinjam terlebih dahulu');
    }
    else
    {
        detailpinjaman = $("#tabledetailpinjaman tbody").find(".pinjamDetail[value="+jumlah_pinjam+"]");

        jumlah =  detailpinjaman.length;
        i = 1;

        bunga = parseFloat(bunga_pinjam)/100 * parseFloat(jumlah_pinjam);
        total_pinjam = parseFloat(jumlah_pinjam) + parseFloat(bunga);
        cicilan = parseFloat(total_pinjam) / parseInt(lama_pinjam); 
        total_cicilan = 0;

        for(i;i<=lama_pinjam;i++){
            lastRow = $('#tabledetailpinjaman' + " tr:last");
            bln = parseInt(bln) + 1;

            if(bln>12 && bln<=24){
                bln = bln%12;
                if(bln==0){
                    bln = 12;
                }
                thn = parseInt(thn) + 1;
            }
            tgl_cicilan = cek_tanggal(tgl,bln,thn);

            tgl_jatuh_tempo     = tgl_cicilan + ' - ' + bln + ' - ' + thn;

            bln_db = bln - 1;
            //tgl_jatuh_tempo_db  = new Date(thn,bln_db,tgl_cicilan);
            tgl_jatuh_tempo_db  = thn + '-' + formatnolbulan(bln) + '-' + tgl_cicilan;
            tgl_jatuh_tempo_view = tgl_cicilan + ' - ' + bulanangka(bln) + ' - ' + thn;

            cicilana = cicilan.toFixed(-2);
            total_cicilan = parseFloat(total_cicilan) + parseFloat(cicilana);
            sisa_bayar = parseFloat(total_pinjam) - parseFloat(total_cicilan);
            if(total_cicilan > total_pinjam){
                lebih_bayar = parseFloat(total_cicilan) - parseFloat(total_pinjam);
                cicilana  = parseFloat(cicilan.toFixed(-2)) - parseFloat(lebih_bayar);
            }else if(sisa_bayar < cicilana){
                if(sisa_bayar < 1000){
                    cicilana = (parseFloat(cicilan) + parseFloat(sisa_bayar)).toFixed(-2);
                }
            }

            newRow = '<tr><td><input type="text" name="no['+ i +']" value="'+ i +'" class="span1" disabled></td><td><input type="text" name="angs_visible['+ i +']" value="'+ i +'" class="span1" disabled><input type="hidden" name="angsuranke['+ i +']" value="'+ i +'"></td><td>'+ tgl_jatuh_tempo_view +'<input type="hidden" name="tglakanbayar['+ i +']" value="'+ tgl_jatuh_tempo_db +'"></td><td><input type="text" name="cicilan['+ i +']" value="'+ cicilana +'" class="span2" disabled ><input type="hidden" name="jmlcicilan['+ i +']" value="'+ cicilana +'"></td></tr>';
            
            $('#tabledetailpinjaman').append(newRow);

        }
        
    }   
}

function cek_tanggal(tgl,bln,thn){
    if(bln==4 || bln==9 || bln==6 || bln==11){
        if(tgl==31){
            tgl_skrg = 30;
        }else{
            tgl_skrg = tgl;
        }
        return tgl_skrg;
    }else if(bln==2){
        if(tgl>28){
            if(thn%4==0){
                tgl_skrg = 29;
            }else{
                tgl_skrg = 28;
            }
        }else{
            tgl_skrg = tgl;
        }
        return tgl_skrg;
    }else{
        return tgl;
    }
}

function hitungSemua()
{
     noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut++;
     });

}

noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut = noUrut + 1;
     });

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;
    value = value.replace(/[0-9]*/g, "");
    var msg = "Only Integer Values allowed.";

    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }
}

function remove (obj) {
    $(obj).parents('tr').remove();
    hitungSemua();
}
   
function cekValidasi(event)
{   
  banyaknyaObat = $('.obatAlkes').length;
  hargaNettoPenawaran =  $('#PenawarandetailT_harganetto').val(); 
  if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
    return false;
  }else if(banyaknyaObat<1){
     alert('Anda belum memilih Obat Yang Akan Diminta');   
     return false;
  }else if(hargaNettoPenawaran==0){
     alert('Anda Belum memilih Obat Yang Akan Diminta');   
     return false;
  }else{
     $('#btn_simpan').click();
     return true;
  }
}

function angkabulan(bln){
    switch (bln) {
        case 'Jan':
            return '1';
            break;
        case 'Feb':
            return '2';
            break;
        case 'Mar':
            return '3';
            break;
        case 'Apr':
            return '4';
            break;
        case 'Mei':
            return '5';
            break;
        case 'Jun':
            return '6';
            break;
        case 'Jul':
            return '7';
            break;    
        case 'Agus':
            return '8';
            break;    
        case 'Sep':
            return '9';
            break;
        case 'Okt':
            return '10';
            break;
        case 'Nop':
            return '11';
            break;
        case 'Des':
            return '12';
            break;
        default:
            return '01';
            break;
    }
}

function bulanangka(bln){
    switch (bln) {
        case 1:
            return 'Januari';
            break;
        case 2:
            return 'Februari';
            break;
        case 3:
            return 'Maret';
            break;
        case 4:
            return 'April';
            break;
        case 5:
            return 'Mei';
            break;
        case 6:
            return 'Juni';
            break;
        case 7:
            return 'Juli';
            break;    
        case 8:
            return 'Agustus';
            break;    
        case 9:
            return 'September';
            break;
        case 10:
            return 'Oktober';
            break;
        case 11:
            return 'November';
            break;
        case 12:
            return 'Desember';
            break;
        default:
            return 'Desember';
            break;
    }
}

function formatnolbulan(bln){
    if(bln<10){
        return '0'+bln;
    }else{
        return bln;
    }
}

JS;
Yii::app()->clientScript->registerScript('masukandetailpinjaman',$jscript, CClientScript::POS_HEAD);
?>