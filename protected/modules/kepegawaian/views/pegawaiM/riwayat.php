
<?php
$this->breadcrumbs=array(
	'Sapegawai Ms'=>array('index'),
	'Create',
);


$this->widget('bootstrap.widgets.BootAlert'); ?>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>


<fieldset>
    <legend>Data Karyawan</legend>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('readonly'=>true,'id'=>'NIP')); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Karyawan','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo $form->hiddenField($model,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                           <?php echo $form->textField($model, 'nama_pegawai',array('readonly'=>true, 'id'=>'pegawai_id'));?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($model, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
                <?php echo $form->textFieldRow($model, 'jeniskelamin',array('readonly'=>true,'id'=>'jeniskelamin')); ?>
                <?php echo $form->textFieldRow($model,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
                <?php echo $form->textFieldRow($model,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                <?php echo $form->textAreaRow($model,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
            </td>
            <td>
                <?php
                    if(!empty($model->photopegawai)){
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'kecil_'.$model->photopegawai, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'no_photo.jpeg', 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
<!-- ==================================== View Riwayat pendidikan ============================= -->
<fieldset id="divRiwayatPendidikanpegawai">
    <div>
        <legend class="accord1" style="width:460px;">
      <?php echo CHtml::checkBox('cekRiwayatPendidikanpegawai',true, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatPendidikanpegawai(this).Checked=ture;','checked'=>'checked')) ?> Riwayat Karyawan </legend>  
        
        <table class="table table-bordered table-striped table-condensed" id="tableRiwayatpendidikanpegawai" style="display:none;">
               
            <thead>
                    <tr>
                        <th colspan="12" bgcolor="white">Pendidikan Karyawan</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Pendidikan</th>
                        <th rowspan="2">Nama Sekolah / Universitas</th>
                        <th rowspan="2">Alamat Sekolah / Universitas</th>
                        <th rowspan="2">Tgl masuk</th>
                        <th rowspan="2">Lama pendidikan</th>
                        <th colspan="3" style="text-align:center;">Kolom ijazah</th>
                        <th rowspan="2">Nilai lulus / grade lulus</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Hapus</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Tanda tangan</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
        </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pendidikan ============================= -->
<!-- ==================================== View Riwayat diklat ============================= -->
<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegawaidiklat">
                <thead>
                    <tr>
                        <th colspan="11" bgcolor="white">Karyawan Diklat</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Jenis diklat</th>
                        <th rowspan="2">Nama diklat</th>
                        <th rowspan="2">Tgl mulai diklat</th>
                        <th rowspan="2">Lama diklat</th>
                        <th rowspan="2">Tempat</th>
                        <th colspan="3" style="text-align:center;">Keputusan diklat</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Hapus</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl penetapan</th>
                        <th>Nama pimpinan</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat diklat ============================= -->
<!-- ==================================== View Riwayat pengalaman kerja ============================= -->
<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPengalamankerja">
                <thead>
                    <tr>
                        <th colspan="10" bgcolor="white">Pengalaman Kerja</th>
                    </tr>
                    <tr>
                        <th>No urut</th>
                        <th>Nama perusahaan</th>
                        <th>Bidang usaha</th>
                        <th>Jabatan</th>
                        <th>Tgl masuk</th>
                        <th>Tgl keluar</th>
                        <th>Lama kerja</th>
                        <th>Alasan berhenti</th>
                        <th>Keterangan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pengalaman kerja ============================= -->
<!-- ==================================== View Riwayat jabatan ============================= -->
<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatJabatan">
                <thead>
                    <tr>
                        <th colspan="8" bgcolor="white">Jabatan Karyawan</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>No SK</th>
                        <th>Tgl ditetapkan</th>
                        <th>TMT jabatan</th>
                        <th>Tgl akhir jabatan</th>
                        <th>Keterangan</th>
                        <th>Pejabat yang menjabatkan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat jabatan ============================= -->
<!-- ==================================== View Riwayat Pegawai mutasi ============================= -->
<fieldset id="">
    <div>
          <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegmutasi">
                <thead>
                    <tr>
                        <th colspan="12" bgcolor="white">Mutasi Karyawan</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>No Surat</th>
                        <th>Jabatan</th>
                        <th>No SK</th>
                        <th>Tgl SK</th>
                        <th>TMT SK</th>
                        <th>Jabatan baru</th>
                        <th>Mengetahui</th>
                        <th>Pimpinan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pegawai mutasi ============================= -->
<!-- ==================================== View Riwayat Pegawai cuti ============================= -->
<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegawaicuti">
                <thead>
                    <tr>
                        <th colspan="11" bgcolor="white">Cuti Karyawan</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Jenis cuti</th>
                        <th>Tgl mulai</th>
                        <th>Lama cuti</th>
                        <th>No SK</th>
                        <th>Tgl SK</th>
                        <th>Keperluan</th>
                        <th>Keterangan</th>
                        <th>Pejabat mengetahui</th>
                        <th>Pejabat menyetujui</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pegawai cuti ============================= -->
<!-- ==================================== View Riwayat Izin tugas belajar ============================= -->
<fieldset id="">
    <div>
        <div class="">    
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatIjintugasbelajar">
                <thead>
                    <tr>
                        <th colspan="7" bgcolor="white">Izin Tugas Belajar</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl mulai belajar</th>
                        <th>Nomor keputusan</th>
                        <th>Tgl ditetapkan</th>
                        <th>Keterangan</th>
                        <th>Pejabat yg memutuskan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat Izin tugas belajar ============================= -->
<!-- ==================================== View Riwayat Hukuman disiplin ============================= -->
<fieldset id="">
    <div>
        <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatHukdisiplin">
            
                <thead>
                    <tr>
                        <th colspan="9" bgcolor="white">Hukuman Disiplin</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Jenis hukuman</th>
                        <th>Jabatan</th>
                        <th>Tgl hukuman</th>
                        <th>Ruangan Karyawan</th>
                        <th>No SK</th>
                        <th>Lama hukuman</th>
                        <th>Keterangan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                     
                     
                </tbody>
            </table>
    </div>
</fieldset>

<!-- ======================================== View Riwayat Pengorganisasian Pegawai ================================== -->
<fieldset id="">
    <div>
       
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatOrganisasi">
                <thead>
                    <tr>
                        <th colspan="7" bgcolor="white">Pengorganisasian Karyawan</th>
                    </tr>
                    <tr>
                        <th>No Urut</th>
                        <th>Nama Organisasi</th>
                        <th>Kedudukan</th>
                        <th>Tanggal Mulai</th>
                        <th>Lama</th>
                        <th>Tempat</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ========================================== View Riwayat Susunan Keluarga =========================================== -->
<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatSusunanKeluarga">
                <thead>
                    <tr>
                        <th colspan="13" bgcolor="white">Susunan Keluarga</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">No Urut</th>
                        <th rowspan="2">Hubungan Keluarga</th>
                        <th rowspan="2">Nama</th>
                        <th rowspan="2">Jenis Kelamin</th>
                        <th rowspan="2">Tempat Lahir</th>
                        <th rowspan="2">Tanggal Lahir</th>
                        <th rowspan="2">Pekerjaan</th>
                        <th rowspan="2">Pendidikan</th>
                        <th rowspan="2">Tanggal Pernikahan</th>
                        <th rowspan="2">Tempat Pernikahan</th>
                        <th rowspan="2">NIP</th>
<!--                        <th rowspan="2">Tambah / Batal</th>-->
<!--                        <th>Keterangan</th>
                        <th>Pejabat yang menjabatkan</th>
-->                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ======================================= View Riwayat Prestasi Kerja ================================================= -->

<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPrestasiKerja">
                <thead>
                    <tr>
                        <th colspan="8" bgcolor="white">Prestasi Kerja</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">No Urut</th>
                        <th rowspan="2">Tanggal Perolehan</th>
                        <th rowspan="2">Instansi Pemberi</th>
                        <th rowspan="2">Penjabat Pemberi</th>
                        <th rowspan="2">Nama Penghargaan</th>
                        <th rowspan="2">Keterangan</th>
<!--                        <th rowspan="2">Tambah / Batal</th>-->
<!--                        <th>Keterangan</th>
                        <th>Pejabat yang menjabatkan</th>
-->                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ========================================= View Riwayat Perjalanan Dinas ================================ -->

<fieldset id="">
    <div>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPerjalananDinas">
                <thead>
                    <tr>
                        <th colspan="12" bgcolor="white">Perjalanan Dinas</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No</th>
                        <th rowspan="2">No Urut</th>
                        <th rowspan="2">Tujuan Dinas</th>
                        <th rowspan="2">Tugas Dinas</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Alamat Tujuan</th>
                        <th rowspan="2">Propinsi</th>
                        <th rowspan="2">Kota</th>
                        <th rowspan="2">Tanggal Mulai</th>
                        <th rowspan="2">Tanggal Akhir</th>
                        <th rowspan="2">Negara Tujuan</th>
<!--                        <th rowspan="2">Tambah / Batal</th>-->
<!--                        <th>Keterangan</th>
                        <th>Pejabat yang menjabatkan</th>
-->                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!--
<fieldset id="">
    <div>
                    
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPangkat">
                <thead>
                    <tr>
                        <th colspan="11" bgcolor="white">Kenaikan Pangkat</th>
                    </tr>
                    <tr>
                        <th rowspan="2">No Urut</th>
                        <th rowspan="2">Jabatan</th>
                        <th rowspan="2">Pangkat</th>
                        <th rowspan="2">Tahun</th>
                        <th rowspan="2">Bulan</th>
                        <th rowspan="2">Gaji Pokok</th>
                        <th rowspan="2">No SK</th>
                        <th rowspan="2">Tanggal SK</th>
                        <th rowspan="2">Mengetahui</th>
                        <th rowspan="2">Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
-->
</fieldset>
<div class="form-actions">
<table border="0" >
  <tr>
   <td width="90"> 
        <?php 
            $this->widget('bootstrap.widgets.BootButtonGroup', array(
                'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
                'buttons'=>array(
                    array('label'=>'Print', 'icon'=>'icon-print icon-white', 'url'=>'#', 'htmlOptions'=>array('onclick'=>'print(\'PRINT\')')),
                    array('label'=>'', 'items'=>array(
                        array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'PDF\')')),
                        array('label'=>'EXCEL','icon'=>'icon-pdf', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'EXCEL\')')),
                        array('label'=>'PRINT','icon'=>'icon-print', 'url'=>'', 'itemOptions'=>array('onclick'=>'print(\'PRINT\')')),
                    )),       
                ),
            )); 
        ?>	
   </td >
    <td>

        <?php
             $content = $this->renderPartial('../tips/master',array(),true);
             $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
    </td>

  </tr>
</table>
</div>
    <?php 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintRiwayat&id='.$model->pegawai_id);

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}   
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);  

?>
    
<?php $this->endWidget(); ?>

<script type="text/javascript">
ViewRiwayatPendidikanpegawai();
</script>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM;
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                        "id" => "selectPasien",
                                        "onClick" => "$(\"#NIP\").val(\"$data->nomorindukpegawai\");
                                                      $(\"#pegawai_id\").val(\"$data->pegawai_id\");
                                                      $(\"#namapegawai\").val(\"$data->nama_pegawai\");
                                                      $(\"#tempatlahir_pegawai\").val(\"$data->tempatlahir_pegawai\");
                                                      $(\"#tgl_lahirpegawai\").val(\"$data->tgl_lahirpegawai\");
                                                      $(\"#jeniskelamin\").val(\"$data->jeniskelamin\");
                                                      $(\"#statusperkawinan\").val(\"$data->statusperkawinan\");
                                                      $(\"#jabatan\").val(\"$data->jabatan_id\");
                                                      $(\"#alamat_pegawai\").val(\"$data->alamat_pegawai\");
                                                      $(\"#dialogPegawai\").dialog(\"close\");    
                                            "))',
                    ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                array(
                    'header'=>'Jabatan',
                    'value'=>'(isset($data->jabatan->jabatan_nama) ? $data->jabatan->jabatan_nama : "")',
                ),
                'alamat_pegawai',
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php
$urlGetPendidikanpegawai = Yii::app()->createUrl('actionAjax/GetPendidikanpegawai');
$urlGetPegawaidiklat = Yii::app()->createUrl('actionAjax/GetPegawaidiklat');
$urlGetPengalamankerja = Yii::app()->createUrl('actionAjax/GetPengalamankerja');
$urlGetPegawaijabatan = Yii::app()->createUrl('actionAjax/GetPegawaijabatan');
$urlGetPegmutasi = Yii::app()->createUrl('actionAjax/GetPegmutasi');
$urlGetPegawaicuti = Yii::app()->createUrl('actionAjax/GetPegawaicuti');
$urlGetIjintugasbelajar = Yii::app()->createUrl('actionAjax/GetIjintugasbelajar');
$urlGetHukdisiplin = Yii::app()->createUrl('actionAjax/GetHukdisiplin');
$urlGetPerjalananDinas = Yii::app()->createUrl('actionAjax/GetPerjalananDinas');
$urlGetPrestasiKerja = Yii::app()->createUrl('actionAjax/GetPrestasiKerja');
$urlGetSusunanKeluarga = Yii::app()->createUrl('actionAjax/GetSusunanKeluarga');
$urlGetPengorganisasi = Yii::app()->createUrl('actionAjax/GetPengOrganisasi');
$urlGetPangkat = Yii::app()->createUrl('actionAjax/GetPangkat');
$urlGetTahun = Yii::app()->createUrl('actionAjax/GetTahun');
$js= <<< JS
    $(document).ready(function() {
        
          $(".btn-primary").click(function(){
            if (!$("#pegawai_id").val()) {
                alert("Anda belum memilih pegawai");
                return false;
            } else {
                return true;
            }
          });

          $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
          });

            // == Form Riwayat ==
            $("#tablePendidikanpegawai").show();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").show();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();

    });

    function tab(index) {
        $(this).hide();
        if (index==0) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").show();
            $("#tableDiklatpegawai").show();
            $("#tablePengalamankerja").show();
            $("#tableJabatan").show();
            $("#tablePegawaimutasi").show();
            $("#tablePegawaicuti").show();
            $("#tableIjintugasbelajar").show();
            $("#tableHukdisiplin").show();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").show();
            $("#pegawaimutasi").show();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
            $("#divRiwayatOrganisasi").hide();
            $("#divRiwayatPrestasiKerja").hide();
            $("#divRiwayatSusunanKeluarga").hide();
            $("#divRiwayatPerjalananDinas").hide();

        } else if (index==1) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").show();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").show();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==2) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").show();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").show();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==4) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").show();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").show();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==6) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").show();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").show();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==7) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").show();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").show();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==8) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").show();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").show();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==9) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").show();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").show();
        }
    }
    
// ========================== Script pendidikan pegawai ===================================   
    function tambahPendidikanpegawai(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInputpendidikanpegawai();
    }

    function tambahPendidikanpegawaidrinput(obj) {
        $("#hapus").show();
        $("#tambah").hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInputpendidikanpegawai();
    }

    function hapusPendidikanpegawai(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpendidikanpegawai(){
        nourut = 1;
        $(".keterangan").each(function(){
            $(this).parents('tr').find('[name*="KPPendidikanpegawaiR"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPendidikanpegawaiR[]');
                var id = input.split('KPPendidikanpegawaiR[][');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPendidikanpegawaiR['+nourut+']'+data[1]);
//                    $(this).attr('id','date_'+nourut);
                };
            });
            
            $(this).parents('tr').find('[id*="date"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('date-');
                    $(this).attr('id','date-'+data[1]+nourut);
                    
                $(function() {
                    $( "#date-"+data[1]+nourut).datepicker({
                        firstDay: 7,
                        dateFormat:'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                    });
                });
                $.datepicker.setDefaults($.datepicker.regional['id']);

            });
            nourut++;
        });
    }
    
        function Pendidikanpegawaidata()
        {
            pegawai_id = $('#pegawai_id').val();
           {
                $.post("${urlGetPendidikanpegawai}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatpendidikanpegawai").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPendidikanpegawai() {
            
            if ($("#cekRiwayatPendidikanpegawai").is(":checked")) {
                Pendidikanpegawaidata();
                Pegawaidiklatdata();
                Hukdisiplindata();
                Pengalamankerjadata();
                Pegawaijabatandata();
                Pegmutasidata();
                Pegawaicutidata();
                Ijintugasbelajardata();
                Pengorganisasidata();
                Prestasikerjadata();
                Susunankeluargadata();
                Perjalanandinasdata();
                //Pangkatdata();
                
                
                
                $("#tableRiwayatpendidikanpegawai").slideDown(60);
                $("#tableRiwayatPegawaidiklat").slideDown(60);
                $("#tableRiwayatHukdisiplin").slideDown(60);
                $("#tableRiwayatPengalamankerja").slideDown(60);
                $("#tableRiwayatJabatan").slideDown(60);
                $("#tableRiwayatPegmutasi").slideDown(60);
                $("#tableRiwayatPegawaicuti").slideDown(60);
                $("#tableRiwayatIjintugasbelajar").slideDown(60);
                $("#tableRiwayatOrganisasi").slideDown(60);
                $("#tableRiwayatPrestasiKerja").slideDown(60);
                $("#tableRiwayatSusunanKeluarga").slideDown(60);
                $("#tableRiwayatPerjalananDinas").slideDown(60);
                //$("#tableRiwayatPangkat").slideDown(60);
                
            } else {
                $("#tableRiwayatpendidikanpegawai").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaidiklat").children("tbody").children("tr").remove();
                $("#tableRiwayatHukdisiplin").children("tbody").children("tr").remove();
                $("#tableRiwayatPengalamankerja").children("tbody").children("tr").remove();
                $("#tableRiwayatJabatan").children("tbody").children("tr").remove();
                $("#tableRiwayatPegmutasi").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaicuti").children("tbody").children("tr").remove();
                $("#tableRiwayatIjintugasbelajar").children("tbody").children("tr").remove();
                $("#tableRiwayatOrganisasi").children("tbody").children("tr").remove();
                $("#tableRiwayatPrestasiKerja").children("tbody").children("tr").remove();
                $("#tableRiwayatSusunanKeluarga").children("tbody").children("tr").remove();
                $("#tableRiwayatPerjalananDinas").children("tbody").children("tr").remove();
                //$("#tableRiwayatPangkat").children("tbody").children("tr").remove();
                
                $("#tableRiwayatpendidikanpegawai").slideUp(60);
                $("#tableRiwayatPegawaidiklat").slideUp(60);
                $("#tableRiwayatHukdisiplin").slideUp(60);
                $("#tableRiwayatPengalamankerja").slideUp(60);
                $("#tableRiwayatJabatan").slideUp(60);
                $("#tableRiwayatPegmutasi").slideUp(60);
                $("#tableRiwayatPegawaicuti").slideUp(60);
                $("#tableRiwayatIjintugasbelajar").slideUp(60);
                $("#tableRiwayatOrganisasi").slideUp(60);
                $("#tableRiwayatPrestasiKerja").slideUp(60);
                $("#tableRiwayatSusunanKeluarga").slideUp(60);
                $("#tableRiwayatPerjalananDinas").slideUp(60);
                //$("#tableRiwayatPangkat").slideUp(60);
            }
        }
// ========================== Akhir script pendidikan pegawai ===================================

// ========================== Script Pegawai diklat ===================================   
    function tambahPegawaidiklat(obj) {
        $(obj).parents("td").children("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPegawaidiklat.replace());
        renameInputpegawaidiklat();
    }

    function tambahPegawaidiklatdrinput(obj) {
        $("#hapus").show();
        $("#tambah").hide();
        $(obj).parents("table").children("tbody").append(trPegawaidiklat.replace());
        renameInputpegawaidiklat();
    }

    function hapusPegawaidiklat(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpegawaidiklat(){
        nourutdk = 1;
        gilirandk = 2;
        $(".keterangan").each(function(){
            $(this).parents('tr').find('[name*="KPPegawaidiklatT"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPegawaidiklatT[]');
                var id = input.split('KPPegawaidiklatT[][');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPegawaidiklatT['+nourutdk+']'+data[1]);
                };
            });
            
            $(this).parents('tr').find('[id*="date"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('date-');
                    $(this).attr('id','date-'+data[1]+nourutdk);
                    
                $(function() {
                    $( "#date-"+data[1]+nourutdk).datepicker({
                        firstDay: 7,
                        dateFormat:'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                    });
                });
                $.datepicker.setDefaults($.datepicker.regional['id']);

             $(this).parents('tr').find('[id*="KPPegawaidiklat_no"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('KPPegawaidiklat_');
                $(this).attr('id','KPPegawaidiklat_'+data[1]+nourutdk);
                $("#KPPegawaidiklat_"+data[1]+nourutdk).val(gilirandk);
             });

            });
            nourutdk++;
            gilirandk++;
        });
    }
    
        function Pegawaidiklatdata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
            }else{
                $.post("${urlGetPegawaidiklat}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegawaidiklat").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPegawaidiklat() {
            
            if ($("#cekRiwayatPegawaidiklat").is(":checked")) {
                Pegawaidiklatdata();
                $("#tableRiwayatPegawaidiklat").slideDown(60);
            } else {
                $("#tableRiwayatPegawaidiklat").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaidiklat").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai diklat ===================================

// ========================== Script Pengalaman kerja ===================================   
    function tambahPengalamankerja(obj) {
        $(obj).parents("td").children("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPengalamankerja.replace());
        renameInputpengalamankerja();
    }

    function hapusPengalamankerja(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpengalamankerja(){
        nourutpk = 0;
        giliranpk = 1;
        $(".keterangan").each(function(){
            $(this).parents('tr').find('[name*="KPPengalamankerjaR"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPengalamankerjaR[]');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPengalamankerjaR['+nourutpk+']'+data[1]);
                };
            });
            
            $(this).parents('tr').find('[id*="date"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('date-');
                    $(this).attr('id','date-'+data[1]+nourutpk);
                    
                $(function() {
                    $( "#date-"+data[1]+nourutpk).datepicker({
                        firstDay: 7,
                        dateFormat:'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                    });
                });
                $.datepicker.setDefaults($.datepicker.regional['id']);

             $(this).parents('tr').find('[id*="KPPengalamankerjaR_no"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('KPPengalamankerjaR_');
                $(this).attr('id','KPPengalamankerjaR_'+data[1]+nourutpk);
                $("#KPPengalamankerjaR_"+data[1]+nourutpk).val(giliranpk);
             });

            });
            nourutpk++;
            giliranpk++;
        });
    }
    
        function Pengalamankerjadata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
            }else{
                $.post("${urlGetPengalamankerja}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPengalamankerja").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPengalamankerja() {
            
            if ($("#cekRiwayatPengalamankerja").is(":checked")) {
                Pengalamankerjadata();
                $("#tableRiwayatPengalamankerja").slideDown(60);
            } else {
                $("#tableRiwayatPengalamankerja").children("tbody").children("tr").remove();
                $("#tableRiwayatPengalamankerja").slideUp(60);
            }
        }
// ========================== Akhir script Pengalaman kerja ===================================
                
// ========================== Script Pegawai jabatan =======================================
        function Pegawaijabatandata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegawaijabatan}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatJabatan").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatJabatan() {
            
            if ($("#cekRiwayatjabatan").is(":checked")) {
                Pegawaijabatandata();
                $("#tableRiwayatJabatan").slideDown(60);
            } else {
                $("#tableRiwayatJabatan").children("tbody").children("tr").remove();
                $("#tableRiwayatJabatan").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai jabatan ===================================
                
// ========================== Script Mutasi kerja =======================================
        function Pegmutasidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegmutasi}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegmutasi").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewPegmutasi() {
            
            if ($("#cekRiwayatPegawaimutasi").is(":checked")) {
                Pegmutasidata();
                $("#tableRiwayatPegmutasi").slideDown(60);
            } else {
                $("#tableRiwayatPegmutasi").children("tbody").children("tr").remove();
                $("#tableRiwayatPegmutasi").slideUp(60);
            }
        }
// ========================== Akhir script Mutasi kerja ===================================
                
// ========================== Script Pegawai cuti =======================================
        function Pegawaicutidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegawaicuti}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegawaicuti").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewPegawaicuti() {
            
            if ($("#cekRiwayatPegawaicuti").is(":checked")) {
                Pegawaicutidata();
                $("#tableRiwayatPegawaicuti").slideDown(60);
            } else {
                $("#tableRiwayatPegawaicuti").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaicuti").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai cuti ===================================

// ========================== Script Ijin tugas belajar =======================================
        function Ijintugasbelajardata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetIjintugasbelajar}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatIjintugasbelajar").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewIjintugasbelajar() {
            
            if ($("#cekRiwayatIjintugasbelajar").is(":checked")) {
                Ijintugasbelajardata();
                $("#tableRiwayatIjintugasbelajar").slideDown(60);
            } else {
                $("#tableRiwayatIjintugasbelajar").children("tbody").children("tr").remove();
                $("#tableRiwayatIjintugasbelajar").slideUp(60);
            }
        }
// ========================== Akhir script Ijin tugas belajar ===================================

// ========================== Script Hukuman disiplin =======================================
        function Hukdisiplindata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetHukdisiplin}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatHukdisiplin").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatHukdisiplin() {
            
            if ($("#cekRiwayatHukdisiplin").is(":checked")) {
                Hukdisiplindata();
                $("#tableRiwayatHukdisiplin").slideDown(60);
            } else {
                $("#tableRiwayatHukdisiplin").children("tbody").children("tr").remove();
                $("#tableRiwayatHukdisiplin").slideUp(60);
            }
        }
// ========================== Akhir script Hukuman disiplin ===================================

// ========================== Script Pengorganisasian Pegawai =================================
 function tambahOrganisasi(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInput();
    }
    
    function hapusOrganisasi(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
        renameInput();
    }
    
    function renameInput(){
        nourut = 0;
        $(".pegawai").each(function(){
            $(this).parents('tr').find('[name*="KPSusunankelM"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPSusunankelM[]');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPSusunankelM['+nourut+']'+data[1]);
                    if (data[1] == '[susunankel_tanggallahir]'){
                        $(this).attr('id','KPSusunankelM_'+nourut+'_susunankel_tanggallahir');
                        jQuery('#KPSusunankelM_'+nourut+'_susunankel_tanggallahir').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                    if (data[1] == '[susunankel_tanggalpernikahan]'){
                        $(this).attr('id','KPSusunankelM_'+nourut+'_susunankel_tanggalpernikahan');
                        jQuery('#KPSusunankelM_'+nourut+'_susunankel_tanggalpernikahan').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                    }
            });            
            nourut++;
        });
    }
    function Pengorganisasidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPengorganisasi}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatOrganisasi").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatOrganisasi() {
            
            if ($("#cekRiwayatpegawai").is(":checked")) {
                Pengorganisasidata();
                $("#tableRiwayatOrganisasi").slideDown(60);
            } else {
                $("#tableRiwayatOrganisasi").children("tbody").children("tr").remove();
                $("#tableRiwayatOrganisasi").slideUp(60);
            }
        }

    $('#cekRiwayatpegawai').change(function(){
            $('#divRiwayatpendidikanpegawai').slideToggle(500);
    });
 // ==================================== Akhir Script =====================================
 // ==================================== Script Prestasi Kerja ==========================
 function tambahOrganisasi(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInput();
    }
    
    function hapusOrganisasi(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
        renameInput();
    }
    
    function renameInput(){
        nourut = 0;
        $(".pegawai").each(function(){
            $(this).parents('tr').find('[name*="KPPrestasikerjaR"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPrestasikerjaR[]');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPrestasikerjaR['+nourut+']'+data[1]);
                    if (data[1] == '[tglprestasidiperoleh]'){
                        $(this).attr('id','KPPrestasikerjaR_'+nourut+'_tglprestasidiperoleh');
                        jQuery('#KPPrestasikerjaR_'+nourut+'_tglprestasidiperoleh').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                }
            });            
            nourut++;
        });
    }
    function Prestasikerjadata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPrestasiKerja}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPrestasiKerja").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPengalamanKerja() {
            
            if ($("#cekRiwayatpegawai").is(":checked")) {
                Prestasikerjadata();
                $("#tableRiwayatPrestasiKerja").slideDown(60);
            } else {
                $("#tableRiwayatPrestasiKerja").children("tbody").children("tr").remove();
                $("#tableRiwayatPrestasiKerja").slideUp(60);
            }
        }

    $('#cekRiwayatpegawai').change(function(){
            $('#divRiwayatpendidikanpegawai').slideToggle(500);
    });
// ======================================== Akhir Script ========================================
// ======================================== Script Susunan Keluarga =============================
function tambahOrganisasi(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInput();
    }
    
    function hapusOrganisasi(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
        renameInput();
    }
    
    function renameInput(){
        nourut = 0;
        $(".pegawai").each(function(){
            $(this).parents('tr').find('[name*="KPSusunankelM"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPSusunankelM[]');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPSusunankelM['+nourut+']'+data[1]);
                    if (data[1] == '[susunankel_tanggallahir]'){
                        $(this).attr('id','KPSusunankelM_'+nourut+'_susunankel_tanggallahir');
                        jQuery('#KPSusunankelM_'+nourut+'_susunankel_tanggallahir').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                    if (data[1] == '[susunankel_tanggalpernikahan]'){
                        $(this).attr('id','KPSusunankelM_'+nourut+'_susunankel_tanggalpernikahan');
                        jQuery('#KPSusunankelM_'+nourut+'_susunankel_tanggalpernikahan').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                    }
            });            
            nourut++;
        });
    }
    function Susunankeluargadata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetSusunanKeluarga}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatSusunanKeluarga").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatSusunanKeluarga() {
            
            if ($("#cekRiwayatpegawai").is(":checked")) {
                Susunakeluargadata();
                $("#tableRiwayatSusunanKeluarga").slideDown(60);
            } else {
                $("#tableRiwayatSusunanKeluarga").children("tbody").children("tr").remove();
                $("#tableRiwayatSusunanKeluarga").slideUp(60);
            }
        }

    $('#cekRiwayatpegawai').change(function(){
            $('#divRiwayatpendidikanpegawai').slideToggle(500);
    });
// ======================================== Akhir Script ========================================
// ======================================== Script Perjalanan Dinas =============================
 function tambahOrganisasi(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInput();
    }
    
    function hapusOrganisasi(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
        renameInput();
    }
    
    function renameInput(){
        nourut = 0;
        $(".pegawai").each(function(){
            $(this).parents('tr').find('[name*="KPPerjalanandinasR"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPerjalanandinasR[]');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPerjalanandinasR['+nourut+']'+data[1]);
                    if (data[1] == '[tglmulaidinas]'){
                        $(this).attr('id','KPPerjalanandinasR_'+nourut+'_tglmulaidinas');
                        jQuery('#KPPerjalanandinasR_'+nourut+'_tglmulaidinas').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                    if (data[1] == '[sampaidengan]'){
                        $(this).attr('id','KPPerjalanandinasR_'+nourut+'_sampaidengan');
                        jQuery('#KPPerjalanandinasR_'+nourut+'_sampaidengan').datepicker(jQuery.extend({showMonthAfterYear:false}, jQuery.datepicker.regional['id'], {'dateFormat':'yy-mm-dd','timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    }
                }
            });            
            nourut++;
        });
    }
    
    function Perjalanandinasdata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPerjalananDinas}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPerjalananDinas").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatJabatan() {
            
            if ($("#cekRiwayatpegawai").is(":checked")) {
                Perjalanandinasdata();
                $("#tableRiwayatPerjalananDinas").slideDown(60);
            } else {
                $("#tableRiwayatPerjalananDinas").children("tbody").children("tr").remove();
                $("#tableRiwayatPerjalananDinas").slideUp(60);
            }
        }

    $('#cekRiwayatpegawai').change(function(){
            $('#divRiwayatpendidikanpegawai').slideToggle(500);
    });
// ======================================== Akhir Script ========================================
// ======================================== Script Kenaikan Pangkat =============================
/*
    function Pangkatdata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPangkat}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPangkat").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatJabatan() {
            
            if ($("#cekRiwayatpegawai").is(":checked")) {
                Pangkatdata();
                $("#tableRiwayatPangkat").slideDown(60);
            } else {
                $("#tableRiwayatPangkat").children("tbody").children("tr").remove();
                $("#tableRiwayatPangkat").slideUp(60);
            }
        }
        
        function kurangiTanggal(tahun)
        {
            $.post("${urlGetTahun}", {tahun:tahun}, function(hasil){
                $("#UskenpangkatR_uskenpangkat_masakerjatahun").val(hasil.tahun);
                $("#UskenpangkatR_uskenpangkat_masakerjabulan").val(hasil.bulan);
                $("#RealisasikenpangkatR_realisasikenpangkat_masakerjath").val(hasil.tahun);
                $("#RealisasikenpangkatR_realisasiken_masakerjabln").val(hasil.bulan);
            },"json");
        
        }

    $('#cekRiwayatpegawai').change(function(){
            $('#divRiwayatpendidikanpegawai').slideToggle(500);
    });
*/
// ================================== Akhir Script ========================================
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>
<ul class="nav nav-tabs" id="tabmenu"><li class=""></li></ul>
<?php if (isset($_GET['tab'])){
    Yii::app()->clientScript->registerScript('onreadyfunction','
        $("#tabmenu").find("li[index=\''.$_GET['tab'].'\']").find("a").click();
        tab('.$_GET['tab'].');
        ', CClientScript::POS_READY);
} ?>


<?php
$js = <<< JS

//===============Awal untu Mengecek Form Sudah DiUbah Atw Belum====================    
    $(":input").keyup(function(event){
            $('#berubah').val('Ya');
         });
    $(":input").change(function(event){
            $('#berubah').val('Ya');
         });  
    $(":input").click(function(event){
            $('#berubah').val('Ya');
         });  
//================Akhir Untuk Mengecek  Form Sudah DiUbah Atw Belum===================         
JS;
Yii::app()->clientScript->registerScript('tableDiklatpegawai', $js, CClientScript::POS_READY);
?>

<?php
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 34 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"

function palidasiForm(obj)
   {
        var berubah = $('#berubah').val();
        if(berubah=='Ya') 
        {
           if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?'))
               {
                    $('#url').val(obj);
                    $('#btn_simpan').click();
          
               }

        }      
   }
JS;
Yii::app()->clientScript->registerScript('validasi', $js, CClientScript::POS_HEAD);
?> 

