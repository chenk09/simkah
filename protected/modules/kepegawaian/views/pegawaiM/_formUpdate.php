
<?php
//$this->breadcrumbs=array(
//	'Sapegawai Ms'=>array('index'),
//	'Create',
//);

/*
 * start tidak tahu untuk apa 
 */
$random = rand(000000, 999999);
/*
 * end tidak tahu untuk apa 
 */

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Ubah').' Data Karyawan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pegawai', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pegawai', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

<fieldset>
    <legend class="rim">Data Karyawan</legend>
        <table>
            <tr>
                <td width="60%">
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'required')); ?>
                    <div class="control-group">
                            <?php echo CHtml::label('Jenis Identitas','jenisidentitas',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'jenisidentitas',JenisIdentitas::items(),array('empty'=>'-- Pilih --','id'=>'jenisidentitas','style'=>'width:70px;')); ?>
                            <?php echo $form->textField($model,'noidentitas',array('empty'=>'-- Pilih --','id'=>'jenisidentitas','style'=>'width:135px;')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                      <?php echo $form->labelEx($model,'nama_pegawai',array('class'=>'control-label required')); ?>
                         <div class="controls inline">
                             <?php echo $form->dropDownList($model,'gelardepan',GelarDepan::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'style'=>'width:70px;')); ?>
                             <?php echo $form->textField($model,'nama_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'class'=>'inputRequire','style'=>'width:135px;')); ?>
                             <?php echo $form->dropDownList($model,'gelarbelakang_id',  CHtml::listData($model->getGelarBelakangItems(), 'gelarbelakang_id', 'gelarbelakang_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'style'=>'width:70px;')); ?>
                          </div>
                    </div>
                     <?php echo $form->textFieldRow($model,'bank_no_rekening',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                     <?php echo $form->textFieldRow($model,'no_rekening',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                     <?php echo $form->textFieldRow($model,'nama_keluarga',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                     <?php echo $form->dropDownListRow($model,'golonganpegawai_id',  CHtml::listData($model->getGolonganItems(), 'golonganpegawai_id', 'golonganpegawai_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                              )); ?>
                     
                     <?php echo $form->dropDownListRow($model,'pangkat_id',  CHtml::listData($model->getPangkatItems(), 'pangkat_id', 'pangkat_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                     <?php echo $form->dropDownListRow($model,'jabatan_id',  CHtml::listData($model->getJabatanItems(), 'jabatan_id', 'jabatan_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    </div>
                    <?php echo $form->textFieldRow($model,'tempatlahir_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                     
                    <?php echo $form->labelEx($model,'tgl_lahirpegawai', array('class'=>'control-label')) ?>
                       <div class="controls">  
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_lahirpegawai',
                                                'mode'=>'date',                              
                                                'options'=> array(
                                                    'yearRange'=>'-70y:+20y',
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                    ),
                        )); ?> 
                        </div>
                    <?php echo $form->radioButtonListInlineRow($model, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputRequire')); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('Tinggi / Berat Badan','tinggiberatbadan',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'tinggibadan',array('style'=>'width:50px;','id'=>'tinggiberatbadan')) ?>
                            <?php echo ' / '; ?>
                            <?php echo $form->textField($model,'beratbadan',array('style'=>'width:50px;','id'=>'tinggiberatbadan')) ?>
                        </div>
                    </div>
                     <?php echo $form->dropDownListRow($model,'statusperkawinan',StatusPerkawinan::items(), 
                                      array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                            'class'=>'inputRequire')); ?>
                     <?php echo $form->dropDownListRow($model,'agama',Agama::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'class'=>'inputRequire')); ?>
                      <div class="control-group ">
                        <?php echo $form->labelEx($model,'golongandarah', array('class'=>'control-label')) ?>

                        <div class="controls">

                            <?php echo $form->dropDownList($model,'golongandarah', GolonganDarah::items(),  
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php echo $form->radioButtonList($model,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                                </div>
                           </div>
                            <?php echo $form->error($model, 'golongandarah'); ?>
                            <?php echo $form->error($model, 'rhesus'); ?>
                        </div>
                       </div>
                    <?php echo $form->dropDownListRow($model,'warganegara_pegawai',WargaNegara::items(),array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>25)); ?>
                     <?php echo $form->dropDownListRow($model,'suku_id',  CHtml::listData($model->getSukuItems(), 'suku_id', 'suku_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                     <?php echo $form->dropDownListRow($model,'pendidikan_id',  CHtml::listData($model->getPendidikanItems(), 'pendidikan_id', 'pendidikan_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    <?php echo $form->dropDownListRow($model,'pendkualifikasi_id',  CHtml::listData($model->getPendidikanKualifikasiItems(), 'pendkualifikasi_id', 'pendkualifikasi_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('No Telp / Hp','nomorcontact',array('class'=>'control-label')); ?>
                        <div class="controls">
                        <?php echo $form->textField($model,'notelp_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>15,'style'=>'width:97px;','id'=>'nomorcontact')); ?>
                            <?php echo ' / '; ?>
                        <?php echo $form->textField($model,'nomobile_pegawai',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>15,'style'=>'width:97px;','id'=>'nomorcontact')); ?>
                        </div>
                    </div>
                    <?php echo $form->textFieldRow($model,'alamatemail',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->dropDownListRow($model,'unit_perusahaan',  CHtml::listData($model->getUnitPerusahaan(), 'lookup_name', 'lookup_value'),array(
						'empty'=>'-- Pilih --',
						'onkeypress'=>"return $(this).focusNextInputField(event)", 
					)); ?>					
                </td>
                <td>
                    
<!--Menampilkan Foto Pegawai Jika Ada-->
<?php if (!empty($model->photopegawai)){
     
    echo '<div class="control-group">
        <div class="control-label">Foto Karyawan</div>
        <div class="controls">';
    echo '<img src="'.Params::urlPegawaiTumbsDirectory().'kecil_'.$model->photopegawai.'" width="200px"/>';
    echo '</div></div>';
}else{
    echo '<div class="control-group">
        <div class="control-label">Foto Pegawai</div>
        <div class="controls">';
//    echo '<img src="'.Params::urlPegawaiDirectory().'no_photo.jpeg" width="200px"/>';
    echo '-- Belum memiliki foto --';
    echo '</div></div>';
}
?>
                       <?php echo $form->hiddenField($model,'tempPhoto',array('readonly'=>TRUE,'value'=>$random.'.jpg')); ?>
                        <?php// echo $form->dropDownListRow($model,'kategoripegawaiasal',KategoriAsalPegawai::items(), 
                                     //     array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                      //          )); ?>
                        <?php echo $form->dropDownListRow($model,'kategoripegawai',KategoriPegawai::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                         <?php echo $form->dropDownListRow($model,'kelompokpegawai_id',  CHtml::listData($model->getKelompokPegawaiItems(), 'kelompokpegawai_id', 'kelompokpegawai_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>
                         <?php //echo $form->dropDownListRow($model,'kelompokjabatan',KelompokJabatan::items(), 
                                       //   array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                        //        )); ?>
                       <?php echo $form->dropDownListRow($model,'jeniswaktukerja',JenisWaktuKerja::items(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                )); ?>  
                        <?php //echo $form->textFieldRow($model,'no_kartupegawainegerisipil',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                        <?php //echo $form->textFieldRow($model,'no_karis_karsu',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                        <?php //echo $form->textFieldRow($model,'no_askes',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                        <?php //echo $form->textFieldRow($model,'no_taspen',array( 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                        <?php //echo $form->dropDownListRow($model,'esselon_id',CHtml::listData($model->getEsselonItems(),'esselon_id','esselon_nama'),array('empty'=>'-- Pilih --')); ?>
                        <?php echo $form->dropDownListRow($model,'statuskepemilikanrumah_id',CHtml::listData($model->getStatuskepemilikanrumahItems(),'statuskepemilikanrumah_id','statuskepemilikanrumah_nama'),array('empty'=>'-- Pilih --')); ?>
                        <?php //echo $form->textFieldRow($model,'nofingerprint'); ?>
                        <?php echo $form->textFieldRow($model,'kemampuanbahasa'); ?>
                        <?php echo $form->textFieldRow($model,'warnakulit'); ?>
                    
                         <?php echo $form->textAreaRow($model,'alamat_pegawai',array('rows'=>6, 'cols'=>50,  'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                         <?php echo $form->dropDownListRow($model,'propinsi_id', CHtml::listData($model->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kabupaten_id'))); ?>
                          <?php echo $form->dropDownListRow($model,'kabupaten_id', CHtml::listData($model->getKabupatenItems($model->propinsi_id),'kabupaten_id','kabupaten_nama'), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kecamatan_id'))); ?>
                         <?php echo $form->dropDownListRow($model,'kecamatan_id', CHtml::listData($model->getKecamatanItems($model->kabupaten_id),'kecamatan_id','kecamatan_nama'), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kelurahan_id'))); ?>
                         <?php echo $form->dropDownListRow($model,'kelurahan_id', CHtml::listData($model->getKelurahanItems($model->kecamatan_id),'kelurahan_id','kelurahan_nama'), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                   )); ?>
 <?php// echo $form->dropDownListRow($model,'statuspegawai',StatusPegawai::items(), 
              //                        array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
               //                            'class'=>'inputRequire')); ?>
                             <?php //echo $form->dropDownListRow($model,'carapembayarangaji',StatusPembayaran::items(), 
                                          //  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                          // 'class'=>'inputRequire')); ?>
<?php echo $form->labelEx($model,'tglditerima', array('class'=>'control-label')) ?>
                       <div class="controls">  
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglditerima',
                                                'mode'=>'date',                              
                                                'options'=> array(
                                                    'yearRange'=>'-70y:+20y',
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                    ),
                        )); ?> 
                           </div>
              
        <label class="control-label">
            
            No Rekam Pasien &nbsp; 
        </label>
                <div class="controls">  
                         <?php echo $form->hiddenField($model,'pasien_id',array('readonly'=>TRUE,'value'=>$model->pasien_id)); ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                  'name'=>'pasien_id',
                                    'value'=>$model->pasien_id,
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLama'),
                                    'options'=>array(
                                       'showAnim'=>'fold',
                                       'style'=>'height:20px;',
                                       'minLength' => 4,
                                       'focus'=> 'js:function( event, ui ) {
                                            $("#noRekamMedik").val( ui.item.value );
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) {
                                            $(\'#KPPegawaiM_pasien_id\').val(ui.item.pasien_id);
                                            $(\'#pasien_id\').val(ui.item.no_rekam_medik);
                                            $("#'.CHtml::activeId($model,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                            $("#'.CHtml::activeId($model,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                            $("#'.CHtml::activeId($model,'namadepan').'").val(ui.item.namadepan);
                                            $("#'.CHtml::activeId($model,'nama_pasien').'").val(ui.item.nama_pasien);
                                            $("#'.CHtml::activeId($model,'nama_bin').'").val(ui.item.nama_bin);
                                            $("#'.CHtml::activeId($model,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                            $("#'.CHtml::activeId($model,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                            $("#'.CHtml::activeId($model,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                            $("#'.CHtml::activeId($model,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                            setJenisKelaminPasien(ui.item.jeniskelamin);
                                            setRhesusPasien(ui.item.rhesus);
                                            loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, ui.item.kelurahan_id);
                                            $("#'.CHtml::activeId($model,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                            $("#'.CHtml::activeId($model,'golongandarah').'").val(ui.item.golongandarah);
                                            $("#'.CHtml::activeId($model,'rhesus').'").val(ui.item.rhesus);
                                            $("#'.CHtml::activeId($model,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                            $("#'.CHtml::activeId($model,'rt').'").val(ui.item.rt);
                                            $("#'.CHtml::activeId($model,'rw').'").val(ui.item.rw);
                                            $("#'.CHtml::activeId($model,'propinsi_id').'").val(ui.item.propinsi_id);
                                            $("#'.CHtml::activeId($model,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                            $("#'.CHtml::activeId($model,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                            $("#'.CHtml::activeId($model,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                            $("#'.CHtml::activeId($model,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                            $("#'.CHtml::activeId($model,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                            $("#'.CHtml::activeId($model,'suku_id').'").val(ui.item.suku_id);
                                            $("#'.CHtml::activeId($model,'alamatemail').'").val(ui.item.alamatemail);
                                            $("#'.CHtml::activeId($model,'anakke').'").val(ui.item.anakke);
                                            $("#'.CHtml::activeId($model,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                            $("#'.CHtml::activeId($model,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                            $("#'.CHtml::activeId($model,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                            $("#'.CHtml::activeId($model,'agama').'").val(ui.item.agama);
                                            $("#'.CHtml::activeId($model,'warga_negara').'").val(ui.item.warga_negara);
                                            loadUmur(ui.item.tanggal_lahir);
                                            return false;
                                        }',

                                    ),
                                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3 numbersOnly','disabled'=>TRUE),
										'tombolDialog'=>array('idDialog'=>'dialogPasien'),   )); ?>
                       
                      </div>

                  <div class="control-group">
                    <?php echo CHtml::label('Masa Kerja Awal','addmasakerja',array('class'=>'control-label')); ?>
                    <div class="controls">
                      <?php echo $form->textField($model,'addmasakerja',array('style'=>'width:30px;')); ?>
                        Tahun
                    </div>
                  </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                         <?php echo CHtml::label('Ruangan / Unit kerja','ruangan',array('class'=>'control-label'));  ?>
                        <div class="control-group">
                            <div class="controls">

                                 <?php 
                                        $arrRuanganPegawai = array();
                                         foreach($modRuanganPegawai as $tampilRuanganPegawai){
                                            $arrRuanganPegawai[] = $tampilRuanganPegawai['ruangan_id'];
                                        }                                  
                                       $this->widget('application.extensions.emultiselect.EMultiSelect',
                                                     array('sortable'=>true, 'searchable'=>true)
                                                );
                                        echo CHtml::dropDownList(
                                        'ruangan_id[]',
                                        $arrRuanganPegawai,
                                        CHtml::listData(KPRuanganM::model()->findAll(array('order'=>'ruangan_nama')), 'ruangan_id', 'ruangan_nama'),
                                        array('multiple'=>'multiple','key'=>'ruangan_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                                );
                                  ?>
                            </div>
                        </div>
                </td>
            </tr>
             
             <tr>
                <td>
                    <div>
                         <div class="control-group">
                            <?php echo $form->labelEx($model,'caraAmbilPhoto', array('class'=>'control-label','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                               <div class="controls">  
                        <?php echo CHtml::radioButton('caraAmbilPhoto',false,array('value'=>'webCam','onclick'=>'caraAmbilPhotoJS(this)'));?> Web Cam
                        <?php echo CHtml::radioButton('caraAmbilPhoto',true,array('value'=>'file','onclick'=>'caraAmbilPhotoJS(this)'));?> File                               
                               </div>
                          </div>
                       
                    </div>
                    <div id="divCaraAmbilPhotoWebCam"  style="display:none;">
                    <?php 
                        $random=rand(0000000000000000, 9999999999999999);                    
                        $pathPhotoPegawaiTumbs=Params::pathPegawaiTumbsDirectory();
                        $pathPhotoPegawai=Params::pathPegawaiDirectory();
                        $urlAjaxSessionPhoto = '';
                    ?>
                    <?php $onBeforeSnap = "document.getElementById('upload_results').innerHTML = '<h1>Proses Penyimpanan...</h1>';";
                          $completionHandler = <<<BLOCK
                          if (msg == 'OK') 
                           {
                                document.getElementById('upload_results').innerHTML = '<h1>OK! ...Photo Sedang Disimpan</h1>';

                                // reset camera for another shot
                                webcam.reset();
                                setTimeout(function(){
                                document.getElementById('upload_results').innerHTML = '<h1>Photo Berhasil Disimpan</h1>';
                                },3000);
//                              $('#sapegawai-m-form').submit();           
                                $.post("${urlAjaxSessionPhoto}",{},
                                    function(data){
                                    $('#gambar').attr('src',data.photo);

                                },"json");
                            }
                         else
                            {
                                alert("PHP Error: " + msg);
                            }
BLOCK;

      $this->widget('application.extensions.jpegcam.EJpegcam', array(
            'apiUrl' => 'index.php?r=photoWebCam/jpegcam.saveJpg&random='.$random.'&pathTumbs='.$pathPhotoPegawaiTumbs.'&path='.$pathPhotoPegawai.'',
            'shutterSound' => false,
            'stealth' => true,
            'buttons' => array(
                'configure' => 'Konfigurasi',
//                'takesnapshot' => 'Ambil Photo',
                'freeze'=>'Ambil Photo',
                'reset'=>'Ulang',
                
            ),
            'onBeforeSnap' => $onBeforeSnap,
            'completionHandler' => $completionHandler
        ));
?>     
<!--<img src="<?php //echo Params::urlPegawaiDirectory()?>9680901rizky.jpg " id="gambar">  -->

                        <div id="upload_results" style="background-color:#eee; margin-top:10px"></div>
                    </div>
                    <div id="divCaraAmbilPhotoFile" style="display: block;">
                          <div class="control-group">
                            <?php echo $form->labelEx($model,'photopegawai', array('class'=>'control-label','onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                               <div class="controls">  
                                <?php echo Chtml::activeFileField($model,'photopegawai',array('maxlength'=>254,'Hint'=>'Isi Jika Akan Menambahkan Logo')); ?>
                                </div>
                          </div>
                      </div>
                </td>
                <td>
                    <!-- == kosong == -->
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->checkBoxRow($model,'pegawai_aktif', array('onkeypress'=>"return nextFocus(this,event,'','')")); ?>
                     
                </td>
                <td>
                    <!-- == kosong == -->
                </td>
                 
            </tr>
            <tr>
                <td>
                    <!-- == kosong == -->
                </td>
                 <td>
                     <!-- == kosong == -->
                </td>
            </tr>
             <tr>
                <td>
                    <!-- == kosong == -->
                </td>
                <td>
                    <!-- == kosong == -->
                </td>
                
            </tr>
            <tr>
                <td>
                    <!-- == kosong == ->
                </td>
                 <td>
                    <!-- == kosong == -->
                </td>
            </tr>            
        </table>
</fieldset>    

	<div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 
                                                    'onKeypress'=>'return formSubmit(this,event)',
                                                    'id'=>'btn_simpan',
//                                                    'onclick'=>'do_upload()',
                                                   )); ?>
                <?php 
                echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/pegawaiM/Informasi'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false; else {$(\'#dialogUbahPegawai\').dialog(\'close\')}')); ?>
	<?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
        <?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDataPasien = new PPPasienM('searchWithDaerah');
$modDataPasien->unsetAttributes();
if(isset($_GET['PPPasienM'])) {
    $modDataPasien->attributes = $_GET['PPPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchWithDaerah(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "
                                                $(\"#dialogPasien\").dialog(\"close\");
                                                $(\"#'.CHtml::activeId($model,'pasien_id').'\").val(\"$data->pasien_id\");
                                                $(\"#pasien_id\").val(\"$data->no_rekam_medik\");
                                                $(\"#'.CHtml::activeId($model,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                $(\"#'.CHtml::activeId($model,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                                $(\"#'.CHtml::activeId($model,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                                $(\"#'.CHtml::activeId($model,'namadepan').'\").val(\"$data->namadepan\");
                                                $(\"#'.CHtml::activeId($model,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                $(\"#'.CHtml::activeId($model,'nama_bin').'\").val(\"$data->nama_bin\");
                                                $(\"#'.CHtml::activeId($model,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                                $(\"#'.CHtml::activeId($model,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                                $(\"#'.CHtml::activeId($model,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                                $(\"#'.CHtml::activeId($model,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                                $(\"#'.CHtml::activeId($model,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                                $(\"#'.CHtml::activeId($model,'golongandarah').'\").val(\"$data->golongandarah\");
                                                $(\"#'.CHtml::activeId($model,'rhesus').'\").val(\"$data->rhesus\");
                                                $(\"#'.CHtml::activeId($model,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                                $(\"#'.CHtml::activeId($model,'rt').'\").val(\"$data->rt\");
                                                $(\"#'.CHtml::activeId($model,'rw').'\").val(\"$data->rw\");
                                                $(\"#'.CHtml::activeId($model,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                                $(\"#'.CHtml::activeId($model,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                                $(\"#'.CHtml::activeId($model,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                                $(\"#'.CHtml::activeId($model,'kelurahan_id').'\").val(\"$data->kelurahan_id\");
                                                $(\"#'.CHtml::activeId($model,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                                $(\"#'.CHtml::activeId($model,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                                $(\"#'.CHtml::activeId($model,'suku_id').'\").val(\"$data->suku_id\");
                                                $(\"#'.CHtml::activeId($model,'alamatemail').'\").val(\"$data->alamatemail\");
                                                $(\"#'.CHtml::activeId($model,'anakke').'\").val(\"$data->anakke\");
                                                $(\"#'.CHtml::activeId($model,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                                $(\"#'.CHtml::activeId($model,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                                $(\"#'.CHtml::activeId($model,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                                $(\"#'.CHtml::activeId($model,'agama').'\").val(\"$data->agama\");
                                                $(\"#'.CHtml::activeId($model,'warga_negara').'\").val(\"$data->warga_negara\");
                                                loadUmur(\"$data->tanggal_lahir\");
                                                setJenisKelaminPasien(\"$data->jeniskelamin\");
                                                setRhesusPasien(\"$data->rhesus\");
                                                loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id,$data->pasien_id);
                                               
                                            "))',
                        ),
                'no_rekam_medik',
                'nama_pasien',
                'nama_bin',
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php
$js = <<< JS

function caraAmbilPhotoJS(obj)
{
    caraAmbilPhoto=obj.value;
    
    if(caraAmbilPhoto=='webCam')
        {
          $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
            
        }
    else
        {
         $('#divCaraAmbilPhotoWebCam').slideToggle(500);
          $('#divCaraAmbilPhotoFile').slideToggle(500);
        }
} 

function simpanDataPegawai()
{
    var caraAmbilPhoto = $('#caraAmbilPhoto');
     if(caraAmbilPhoto=='webCam')
        {
          $('#upload').click();  
          do_upload();
          $('#sapegawai-m-form').submit();           
        }
     else
        {
          $('#sapegawai-m-form').submit();           
        }
}    

JS;
Yii::app()->clientScript->registerScript('caraAmbilPhoto212',$js,CClientScript::POS_BEGIN);
?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>
        