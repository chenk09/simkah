
<?php
$this->breadcrumbs=array(
	'Sapegawai Ms'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Pencatatan Riwayat').' Karyawan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Pegawai', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Pegawai', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

<fieldset>
    <legend>Data Karyawan</legend>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('readonly'=>true,'id'=>'NIP')); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Karyawan','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo $form->hiddenField($model,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                            <?php
                            
                                $modul = ModulK::model()->findByAttributes(
                                    array('modul_key'=>$this->module->id)
                                );
                                $modul_id = (isset($modul['modul_id']) ? $modul['modul_id'] : '' );
                                $this->widget('MyJuiAutoComplete',array(
                                        'name'=>'namapegawai',
                                        'value'=>$namapegawai,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#namapegawai").val( ui.item.nama_pegawai );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                alert("aaaaaa");
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#NIP").val( ui.item.nomorindukpegawai);
                                                $("#tempatlahir_pegawai").val( ui.item.tempatlahir_pegawai);
                                                $("#tgl_lahirpegawai").val( ui.item.tgl_lahirpegawai);
                                                $("#namapegawai").val( ui.item.nama_pegawai);
                                                $("#jeniskelamin").val( ui.item.jeniskelamin);
                                                $("#statusperkawinan").val( "ui.item.statusperkawinan");
                                                $("#alamat_pegawai").val( ui.item.alamat_pegawai);
                                                var url_string = "index.php?r=kepegawaian/PegawaiM/Pencatatanriwayat&modulId='. $modul_id .'";
                                                url_string = url_string + "&id=" + ui.item.value;
                                                window.location.href = url_string;
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
                                        'tombolDialog'=>array('idDialog'=>'dialogPegawai','idTombol'=>'tombolPasienDialog'),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($model, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
                <?php echo $form->textFieldRow($model, 'jeniskelamin',array('readonly'=>true,'id'=>'jeniskelamin')); ?>
                <?php echo $form->textFieldRow($model,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
                <?php echo $form->textFieldRow($model,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                <?php echo $form->textAreaRow($model,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
            </td>
            <td>
                <?php 
                    if(file_exists(Params::pathPegawaiTumbsDirectory().'kecil_'.$model->photopegawai)){
                        echo CHtml::image(Params::pathPegawaiTumbsDirectory().'kecil_'.$model->photopegawai, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
<!-- ==================================== View Riwayat pendidikan ============================= -->
<fieldset id="divRiwayatPendidikanpegawai">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatPendidikanpegawai',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatPendidikanpegawai()')) ?> Riwayat Pendidikan Karyawan </legend>
        <table class="table table-bordered table-striped table-condensed" id="tableRiwayatpendidikanpegawai" style="display:none;">
                <thead>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Pendidikan</th>
                        <th rowspan="2">Nama Sekolah / Universitas</th>
                        <th rowspan="2">Alamat Sekolah / Universitas</th>
                        <th rowspan="2">Tgl masuk</th>
                        <th rowspan="2">Lama pendidikan</th>
                        <th colspan="3" style="text-align:center;">Kolom ijazah</th>
                        <th rowspan="2">Nilai lulus / grade lulus</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Hapus</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Tanda tangan</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
        </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pendidikan ============================= -->
<!-- ==================================== View Riwayat diklat ============================= -->
<fieldset id="divRiwayatPegawaidiklat">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatPegawaidiklat',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatPegawaidiklat()')) ?> Riwayat Diklat Karyawan </legend>  
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegawaidiklat">
                <thead>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Jenis diklat</th>
                        <th rowspan="2">Nama diklat</th>
                        <th rowspan="2">Tgl mulai diklat</th>
                        <th rowspan="2">Lama diklat</th>
                        <th rowspan="2">Tempat</th>
                        <th colspan="3" style="text-align:center;">Keputusan diklat</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Hapus</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl penetapan</th>
                        <th>Nama pimpinan</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat diklat ============================= -->
<!-- ==================================== View Riwayat pengalaman kerja ============================= -->
<fieldset id="divRiwayatPengalamankerja">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatPengalamankerja',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatPengalamankerja()')) ?> Riwayat pengalaman kerja </legend>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPengalamankerja">
                <thead>
                    <tr>
                        <th>No urut</th>
                        <th>Nama perusahaan</th>
                        <th>Bidang usaha</th>
                        <th>Jabatan</th>
                        <th>Tgl masuk</th>
                        <th>Tgl keluar</th>
                        <th>Lama kerja</th>
                        <th>Alasan berhenti</th>
                        <th>Keterangan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pengalaman kerja ============================= -->
<!-- ==================================== View Riwayat jabatan ============================= -->
<fieldset id="divRiwayatJabatan">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatjabatan',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatJabatan()')) ?> Riwayat Jabatan Karyawan </legend>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatJabatan">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No SK</th>
                        <th>Tgl ditetapkan</th>
                        <th>TMT jabatan</th>
                        <th>Tgl akhir jabatan</th>
                        <th>Keterangan</th>
                        <th>Pejabat yang menjabatkan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat jabatan ============================= -->
<!-- ==================================== View Riwayat Pegawai mutasi ============================= -->
<fieldset id="divRiwayatPegawaimutasi">
        <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatPegawaimutasi',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewPegmutasi()')) ?> Riwayat Karyawan mutasi </legend>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegmutasi">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>No Surat</th>
                        <th>Jabatan</th>
                        <th>Pangkat</th>
                        <th>No SK</th>
                        <th>Tgl SK</th>
                        <th>TMT SK</th>
                        <th>Jabatan baru</th>
                        <th>Pangkat baru</th>
                        <th>Mengetahui</th>
                        <th>Pimpinan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pegawai mutasi ============================= -->
<!-- ==================================== View Riwayat Pegawai cuti ============================= -->
<fieldset id="divRiwayatPegawaicuti">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatPegawaicuti',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewPegawaicuti()')) ?> Riwayat Karyawan cuti </legend>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatPegawaicuti">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Jenis cuti</th>
                        <th>Tgl mulai</th>
                        <th>Lama cuti</th>
                        <th>No SK</th>
                        <th>Tgl SK</th>
                        <th>Keperluan</th>
                        <th>Keterangan</th>
                        <th>Pejabat mengetahui</th>
                        <th>Pejabat menyetujui</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat pegawai cuti ============================= -->
<!-- ==================================== View Riwayat Izin tugas belajar ============================= -->
<fieldset id="divRiwayatIjintugasbelajar">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatIjintugasbelajar',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewIjintugasbelajar()')) ?> Riwayat Izin tugas belajar </legend>
        <div class="RiwayatIjintugasbelajar">    
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatIjintugasbelajar">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Tgl mulai belajar</th>
                        <th>Nomor keputusan</th>
                        <th>Tgl ditetapkan</th>
                        <th>Keterangan</th>
                        <th>Pejabat yg memutuskan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat Izin tugas belajar ============================= -->
<!-- ==================================== View Riwayat Hukuman disiplin ============================= -->
<fieldset id="divRiwayatHukdisiplin">
    <div>
        <legend class="accord1" style="width:460px;"><?php echo CHtml::checkBox('cekRiwayatHukdisiplin',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewRiwayatHukdisiplin()')) ?> Riwayat Hukuman disiplin </legend>
            <table class="table table-bordered table-striped table-condensed" style="display:none;" id="tableRiwayatHukdisiplin">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Jenis hukuman</th>
                        <th>Jabatan</th>
                        <th>Tgl hukuman</th>
                        <th>Ruangan Karyawan</th>
                        <th>No SK</th>
                        <th>Lama hukuman</th>
                        <th>Keterangan</th>
                        <th>Hapus</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
    </div>
</fieldset>
<!-- ==================================== Akhir view Riwayat Hukuman disiplin ============================= -->

    <div class="tab">
        <?php
            $id_pegawai = isset($_GET['id']) ? "&id=" . $_GET['id'] : '';
            $this->widget('bootstrap.widgets.BootMenu',array(
                'type'=>'tabs',
                'stacked'=>false,
                'htmlOptions'=>array('id'=>'tabmenu'),
                'items'=>array(
                    array('label'=>'Pendidikan','url'=>'javascript:tab(0);','active'=>true,'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Diklat','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Pengalaman kerja','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
//                    array('label'=>'Organisasi','url'=>$this->createUrl('/kepegawaian/pengorganisasiR/create'),),
                    array('label'=>'Pengalaman organisasi','url'=>$this->createUrl('/kepegawaian/pengorganisasiR/create' . $id_pegawai),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Jabatan','url'=>'javascript:tab(4);', 'itemOptions'=>array("index"=>4),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
//                    array('label'=>'Pangkat','url'=>$this->createUrl('/kepegawaian/kenaikanpangkatT/create&id=' . $id_pegawai),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Mutasi kerja','url'=>'javascript:tab(6);', 'itemOptions'=>array("index"=>6),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Cuti','url'=>'javascript:tab(7);', 'itemOptions'=>array("index"=>7),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Izin tugas belajar','url'=>'javascript:tab(8);', 'itemOptions'=>array("index"=>8),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Hukuman disiplin','url'=>'javascript:tab(9)', 'itemOptions'=>array("index"=>9),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Susunan Keluarga','url'=>$this->createUrl('/kepegawaian/susunankelM/create' . $id_pegawai),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Prestasi Kerja','url'=>$this->createUrl('/kepegawaian/prestasikerjaR/create' . $id_pegawai),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),
                    array('label'=>'Perjalanan Dinas','url'=>$this->createUrl('/kepegawaian/perjalanandinasR/create' . $id_pegawai),'linkOptions' => array('onclick' => 'return palidasiForm(this);'),),

                ),
            ))
        ?>
<!-- ================================== Form pendidikan ===================================== -->
        <div id="tablePendidikanpegawai">
            <legend class="rim">Pendidikan Karyawan</legend>
            <table class="table table-bordered table-striped table-condensed" style="padding-left:0px; padding-right:0px;">
                <thead>
                    <tr>
                        <th colspan="12">
                            Jenis pendidikan<a style="list-style:none;color:#ff0000;text-decoration:none;"> * </a>
                            <?php echo $form->dropDownList($model,'jenispendidikan',Jenispendidikan::items(),array('empty'=>'-- Pilih --')); ?>
                        </th>
                    </tr>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Pendidikan</th>
                        <th rowspan="2">Nama Sekolah / Universitas</th>
                        <th rowspan="2">Alamat Sekolah / Universitas</th>
                        <th rowspan="2">Tgl masuk</th>
                        <th rowspan="2">Lama pendidikan</th>
                        <th colspan="3" style="text-align:center;">Kolom ijazah</th>
                        <th rowspan="2">Nilai lulus / grade lulus</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Tambah / Batal</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl</th>
                        <th>Tanda tangan</th>
                    </tr>
                </thead>
                <?php
                    $nourut_pend = 1;
                    $i = 0;
                ?>
                <tbody>
                    <tr>
                        <td>
                            <?php echo CHtml::hiddenField('url', $this->createUrl('', array('pegawai_id' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']nourut_pend',array('onkeypress'=>"return $(this).focusNextInputField(event)",'value'=>'1','style'=>'width:30px;')) ?>
                        </td>
                        <td>
                            <?php echo $form->dropDownList($modPendidikanpegawai,'['.$i.']pendidikan_id',CHtml::listData($modPendidikanpegawai->getPendidikanItems(),'pendidikan_id','pendidikan_nama'),array('onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --','style'=>'width:60px;')) ?>
                        </td>
                        <td style="padding-right:0px;">
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']namasek_univ',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                            <?php echo $form->textArea($modPendidikanpegawai,'['.$i.']almtsek_univ',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:100px;')); ?>
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPendidikanpegawai,
                                                'attribute'=>'['.$i.']tglmasuk',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                   
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                                      'id'=>'tglmasuk',
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']lamapendidikan_bln',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px')).' bulan'; ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']no_ijazah_sert',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span1')); ?>
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPendidikanpegawai,
                                                'attribute'=>'['.$i.']tgl_ijazah_sert',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                                      'id'=>'tgl_ijazah_sert'
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']ttd_ijazah_sert',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span1')); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']nilailulus',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px;')); ?>
                            <?php echo ' / '; ?>
                            <?php echo $form->textField($modPendidikanpegawai,'['.$i.']gradelulus',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px;')); ?>
                        </td>
                        <td>
                            <?php echo $form->textArea($modPendidikanpegawai,'['.$i.']keteranganpend',array('onkeypress'=>"(this)",'style'=>'width:50px;')); ?>
                        </td>
                        <td>
                            <?php echo CHtml::link('<i class="icon-plus">&nbsp;</i>','',array('title'=>'Tambah data','rel'=>'tooltip','onclick'=>'tambahPendidikanpegawai(this);return false','id'=>'tambah','style'=>'cursor:pointer;')); ?>
                            <?php echo CHtml::link('<i class="icon-minus">&nbsp;</i>','#',array('title'=>'Hapus data','rel'=>'tooltip','id'=>'hapus','onclick'=>'hapusPendidikanpegawai(this);return false','style'=>'cursor:pointer;display:none;')); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','name'=>'submitpendidikan')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form pendidikan ==================================== -->

<!-- ================================== Form Diklat pegawai =========================================== -->
        <div id="tableDiklatpegawai">
            <legend class="rim">Diklat Karyawan</legend>
            <table class="table table-bordered table-striped table-condensed" style="padding-left:0px; padding-right:0px;">
                <thead>
                    <tr>
                        <th rowspan="2">No urut</th>
                        <th rowspan="2">Jenis diklat <li style="list-style:none;color:#FF0000;">*</li></th>
                        <th rowspan="2">Nama diklat</th>
                        <th rowspan="2">Tgl mulai diklat</th>
                        <th rowspan="2">Lama diklat</th>
                        <th rowspan="2">Tempat</th>
                        <th colspan="3" style="text-align:center;">Keputusan diklat</th>
                        <th rowspan="2">Keterangan</th>
                        <th rowspan="2">Tambah / Batal</th>
                    </tr>
                    <tr>
                        <th>No</th>
                        <th>Tgl penetapan</th>
                        <th>Nama pimpinan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if (count($detailPegawaidiklat)>0){
                            $i = 0;
                            foreach ($detailPegawaidiklat as $i=>$detail) : 
                                $i++;
                    ?>
                        <tr>
                            <td>
                                <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $detail->pegawai_id)), array('readonly' => TRUE)); ?>
                                <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                                <?php echo $form->hiddenField($detail,'['.$i.']pegawaidiklat_id'); ?>
                                <?php echo $form->textField($detail,'['.$i.']no',array('readonly'=>true,'style'=>'width:20px;','value'=>$i)) ?>
                            </td>
                            <td>
                                <?php echo $form->dropDownList($detail,'['.$i.']jenisdiklat_id',CHtml::listData($modPegawaidiklat->getJenisdiklatItems(),'jenisdiklat_id','jenisdiklat_nama'),array('onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --','style'=>'width:60px;')) ?>
                            </td>
                            <td style="padding-right:0px;">
                                <?php echo $form->textField($detail,'['.$i.']pegawaidiklat_nama',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 keterangan')); ?>
                            </td>
                            <td>
                              <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$detail,
                                                    'attribute'=>'['.$i.']pegawaidiklat_tahun',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>'yy-mm-dd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                          'class'=>'dtPicker1',
                                                        ),
                            )); ?> 
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']pegawaidiklat_lamanya',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:30px;')); ?>
                                <?php echo $form->dropDownList($detail,'['.$i.']pegawaidiklat_lamanyasatuan',array('tahun'=>'tahun','bulan'=>'bulan','hari'=>'hari'),array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:55px;')) ?>
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']pegawaidiklat_tempat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']nomorkeputusandiklat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px')); ?>
                            </td>
                            <td>
                              <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$detail,
                                                    'attribute'=>'['.$i.']tglditetapkandiklat',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>'yy-mm-dd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                          'class'=>'dtPicker1',
                                                        ),
                            )); ?> 
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']pejabatygmemdiklat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span1')) ?>
                            </td>
                            <td>
                                <?php echo $form->textArea($detail,'['.$i.']pegawaidiklat_keterangan',array('onkeypress'=>"(this)",'style'=>'width:50px;')); ?>
                            </td>
                            <td style="width:50px;">
                                <?php echo CHtml::link('<i class="icon-plus">&nbsp;</i>','',array('title'=>'Tambah data','rel'=>'tooltip','onclick'=>'tambahPegawaidiklat(this);return false','id'=>'tambah','style'=>'cursor:pointer;')); ?>
                                <?php echo CHtml::link('<i class="icon-minus">&nbsp;</i>','#',array('title'=>'Hapus data','rel'=>'tooltip','id'=>'hapus','onclick'=>'hapusPegawaidiklat(this);return false','style'=>'cursor:pointer;display:none;')); ?>
                            </td>
                        </tr>                    
                    <?php
                            endforeach;
                        }
                    ?>
                    <?php
                        $no = $i+1;
                        $x = 0;
                    ?>
                    <tr>
                        <td>
                            <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $modPegawaidiklat->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']no',array('readonly'=>true,'style'=>'width:20px;','value'=>$no)) ?>
                            <?php echo $form->hiddenField($modPegawaidiklat,'['.$x.']pegawaidiklat_id'); ?>
                        </td>
                        <td>
                            <?php echo $form->dropDownList($modPegawaidiklat,'['.$x.']jenisdiklat_id',CHtml::listData($modPegawaidiklat->getJenisdiklatItems(),'jenisdiklat_id','jenisdiklat_nama'),array('onkeypress'=>"return $(this).focusNextInputField(event)",'empty'=>'-- Pilih --','style'=>'width:60px;')) ?>
                        </td>
                        <td style="padding-right:0px;">
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']pegawaidiklat_nama',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPegawaidiklat,
                                                'attribute'=>'['.$x.']pegawaidiklat_tahun',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']pegawaidiklat_lamanya',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:30px;')); ?>
                            <?php echo $form->dropDownList($modPegawaidiklat,'['.$x.']pegawaidiklat_lamanyasatuan',array('tahun'=>'tahun','bulan'=>'bulan','hari'=>'hari'),array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:55px;')) ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']pegawaidiklat_tempat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']nomorkeputusandiklat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px')); ?>
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPegawaidiklat,
                                                'attribute'=>'['.$x.']tglditetapkandiklat',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                            <?php echo $form->textField($modPegawaidiklat,'['.$x.']pejabatygmemdiklat',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span1')) ?>
                        </td>
                        <td>
                            <?php echo $form->textArea($modPegawaidiklat,'['.$x.']pegawaidiklat_keterangan',array('onkeypress'=>"(this)",'style'=>'width:50px;')); ?>
                        </td>
                        <td style="width:50px;">
                            <?php echo CHtml::link('<i class="icon-plus">&nbsp;</i>','',array('title'=>'Tambah data','rel'=>'tooltip','onclick'=>'tambahPegawaidiklat(this);return false','id'=>'tambah','style'=>'cursor:pointer;')); ?>
                            <?php echo CHtml::link('<i class="icon-minus">&nbsp;</i>','#',array('title'=>'Hapus data','rel'=>'tooltip','id'=>'hapus','onclick'=>'hapusPegawaidiklat(this);return false','style'=>'cursor:pointer;display:none;')); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitdiklat')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form diklat ======================================== -->

<!-- ================================== Form Pengalaman kerja =========================================== -->
        <div id="tablePengalamankerja">
            <legend class="rim">Pengalaman kerja</legend>
            <table class="table table-bordered table-striped table-condensed" style="padding-left:0px; padding-right:0px;">
                <thead>
                    <tr>
                        <th>No urut</th>
                        <th>Nama perusahaan <li style="list-style:none;color:#FF0000;">*</li></th>
                        <th>Bidang usaha</th>
                        <th>Jabatan</th>
                        <th>Tgl masuk</th>
                        <th>Tgl keluar</th>
                        <th>Lama kerja</th>
                        <th>Alasan berhenti</th>
                        <th>Keterangan</th>
                        <th>Tambah / Batal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $i = 0;
                        if (count($detailPengalamankerja)>0){
                            foreach ($detailPengalamankerja as $i=>$detail) : 
                                $i++;
                    ?>
                        <tr>
                            <td>
                                <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                                <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                                <?php echo $form->textField($detail,'['.$i.']pengalamankerja_nourut',array('style'=>'width:20px;','value'=>$i)) ?>
                                <?php echo $form->hiddenField($detail,'['.$i.']pengalamankerja_id'); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']namaperusahaan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']bidangperusahaan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']jabatanterahkir',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                            </td>
                            <td>
                              <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$detail,
                                                    'attribute'=>'['.$i.']tglmasuk',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>'yy-mm-dd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                          'class'=>'dtPicker1',
                                                        ),
                            )); ?> 
                            </td>
                            <td>
                              <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$detail,
                                                    'attribute'=>'['.$i.']tglkeluar',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>'yy-mm-dd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                          'class'=>'dtPicker1',
                                                        ),
                            )); ?> 
                            </td>
                            <td>
                                <?php echo $form->textField($detail,'['.$i.']lama_tahun',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px',)).' thn'; ?>
                                <?php echo $form->textField($detail,'['.$i.']lama_bulan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px',)).' bln'; ?>
                            </td>
                            <td>
                                <?php echo $form->textArea($detail,'['.$i.']alasanberhenti',array('onkeypress'=>"(this)",'style'=>'width:50px;')); ?>
                            </td>
                            <td>
                                <?php echo $form->textArea($detail,'['.$i.']keterangan',array('onkeypress'=>"(this)",'style'=>'width:50px;','class'=>'keterangan')); ?>
                            </td>
                            <td style="width:50px;">
                                <?php echo CHtml::link('<i class="icon-plus">&nbsp;</i>','',array('title'=>'Tambah data','rel'=>'tooltip','onclick'=>'tambahPengalamankerja(this);return false','id'=>'tambah','style'=>'cursor:pointer;')); ?>
                                <?php echo CHtml::link('<i class="icon-minus">&nbsp;</i>','#',array('title'=>'Hapus data','rel'=>'tooltip','id'=>'hapus','onclick'=>'hapusPengalamankerja(this);return false','style'=>'cursor:pointer;display:none;')); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <?php } ?>

                    <?php
                        $no = $i+1;
                        $x = 0 ;
                    ?>                        
                    <tr>
                        <td>
                            <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']pengalamankerja_nourut',array('value'=>$no,'style'=>'width:20px;')) ?>
                            <?php echo $form->hiddenField($modPengalamankerja,'['.$x.']pengalamankerja_id'); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']namaperusahaan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']bidangperusahaan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']jabatanterahkir',array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPengalamankerja,
                                                'attribute'=>'['.$x.']tglmasuk',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                          <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPengalamankerja,
                                                'attribute'=>'['.$x.']tglkeluar',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'yy-mm-dd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker1',
                                                    ),
                        )); ?> 
                        </td>
                        <td>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']lama_tahun',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px',)).' thn'; ?>
                            <?php echo $form->textField($modPengalamankerja,'['.$x.']lama_bulan',array('onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:20px',)).' bln'; ?>
                        </td>
                        <td>
                            <?php echo $form->textArea($modPengalamankerja,'['.$x.']alasanberhenti',array('onkeypress'=>"(this)",'style'=>'width:50px;')); ?>
                        </td>
                        <td>
                            <?php echo $form->textArea($modPengalamankerja,'['.$x.']keterangan',array('onkeypress'=>"(this)",'style'=>'width:50px;','class'=>'keterangan')); ?>
                        </td>
                        <td style="width:50px;">
                            <?php echo CHtml::link('<i class="icon-plus">&nbsp;</i>','',array('title'=>'Tambah data','rel'=>'tooltip','onclick'=>'tambahPengalamankerja(this);return false','id'=>'tambah','style'=>'cursor:pointer;')); ?>
                            <?php echo CHtml::link('<i class="icon-minus">&nbsp;</i>','#',array('title'=>'Hapus data','rel'=>'tooltip','id'=>'hapus','onclick'=>'hapusPengalamankerja(this);return false','style'=>'cursor:pointer;display:none;')); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitPengalamankerja')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form pengalaman kerja ======================================== -->
<!-- ================================== Form Pegawai jabatan ============================================ -->
        <div id="tableJabatan">
            <legend class="rim">Jabatan Karyawan</legend>
            <table>
                <tr>
                    <td>
                        <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                        <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                        <?php echo $form->textFieldRow($modPegawaijabatan,'nomorkeputusanjabatan',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                        <div class="control-group">
                            <?php echo $form->labelEx($modPegawaijabatan,'tglditetapkanjabatan',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modPegawaijabatan,
                                'attribute'=>'tglditetapkanjabatan',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo $form->labelEx($modPegawaijabatan,'tmtjabatan',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modPegawaijabatan,
                                'attribute'=>'tmtjabatan',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="control-group">
                            <?php echo $form->labelEx($modPegawaijabatan,'tglakhirjabatan',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modPegawaijabatan,
                                'attribute'=>'tglakhirjabatan',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                        <?php echo $form->textAreaRow($modPegawaijabatan,'keterangan',array('onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                        <?php echo $form->dropDownListRow($modPegawaijabatan,'pejabatygmemjabatan',CHtml::listData($modPegawaijabatan->getPegawaiItems(),'nama_pegawai','nama_pegawai'),array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)','empty'=>'-- Pilih --')) ?>
                    </td>
                </tr>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitPegawaijabatan')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form Pegawai jabatan ========================================= -->
<!-- ================================== Form Pegawai mutasi =========================================== -->
        <div id="tablePegawaimutasi">
            <legend class="rim">Mutasi Karyawan</legend>
            <table style="width:100%;">
                <tr>
                    <td colspan="2" style="width:100%;">
                          <?php echo $form->textFieldRow($modPegmutasi,'nomorsurat',array('class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    </td>
                <tr>
                    <td style="width:50%;">
                        <?php echo $form->dropDownListRow($modPegmutasi,'jabatan_nama',CHtml::listData($modPegmutasi->getJabatanItems(),'jabatan_nama','jabatan_nama'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                        <?php echo $form->dropDownListRow($modPegmutasi,'unitkerja',CHtml::listData($modPegmutasi->getRuanganItems(),'ruangan_nama','ruangan_nama'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                        <?php //echo $form->dropDownListRow($modPegmutasi,'pangkat_nama',CHtml::listData($modPegmutasi->getPangkatItems(),'pangkat_nama','pangkat_nama'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    </td>
                    <td style="width:50%;">
                        <?php echo $form->dropDownListRow($modPegmutasi,'jabatan_baru',CHtml::listData($modPegmutasi->getJabatanItems(),'jabatan_nama','jabatan_nama'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                        <?php echo $form->dropDownListRow($modPegmutasi,'unitkerja_baru',CHtml::listData($modPegmutasi->getRuanganItems(),'ruangan_nama','ruangan_nama'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                        <?php //echo $form->dropDownListRow($modPegmutasi,'pangkat_baru',CHtml::listData($modPegmutasi->getPangkatItems(),'pangkat_nama','pangkat_nama'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <br/>Surat Keputusan
                    </td>
                </tr>
                <tr>
                    <td>
                        <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                        <?php echo $form->textFieldRow($modPegmutasi,'nosk',array('class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)','value'=>date('Ymd'))) ?>
                        <div class="control-group">
                            <?php echo $form->labelEx($modPegmutasi,'tglsk',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modPegmutasi,
                                'attribute'=>'tglsk',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo $form->labelEx($modPegmutasi,'tmtsk',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modPegmutasi,
                                'attribute'=>'tmtsk',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                    </td>
                    <td>
                        <?php echo $form->dropDownListRow($modPegmutasi,'mengetahui_nama',CHtml::listData($modPegmutasi->getMengetahuiItems(),'nama_pegawai','nama_pegawai'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                        <?php echo $form->dropDownListRow($modPegmutasi,'pimpinan_nama',CHtml::listData($modPegmutasi->getMengetahuiItems(),'nama_pegawai','nama_pegawai'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    </td>
                </tr>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitPegmutasi')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form Pegawai mutasi ======================================== -->
<!-- ================================== Form Pegawai cuti =========================================== -->
        <div id="tablePegawaicuti">
            <legend class="rim">Cuti Karyawan</legend>
            <table>
                <tr>
                    <td>
                        <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $model->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                                <?php echo $form->dropDownListRow($modPegawaicuti,'jeniscuti_id',CHtml::listData($modPegawaicuti->getJeniscutiItems(),'jeniscuti_id','jeniscuti_nama'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                                <div class="control-group">
                                    <?php echo $form->labelEx($modPegawaicuti,'tglmulaicuti',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                        <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPegawaicuti,
                                        'attribute'=>'tglmulaicuti',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>'yy-mm-dd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                           'onChange'=>'hitungLamaCuti()',
                                           'onkeypress'=>"return $(this).focusNextInputField(event)",
                                           'class'=>'dtPicker3',
                                        ),
                                        )); ?>
                                    </div>
                                </div>
                            <div class="control-group">
                                <?php echo $form->labelEx($modPegawaicuti,'tglakhircuti',array('class'=>'control-label')); ?>
                                <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$modPegawaicuti,
                                            'attribute'=>'tglakhircuti',
                                            'mode'=>'date',
                                            'options'=> array(
                                                'dateFormat'=>'yy-mm-dd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,
                                                'onChange'=>'hitungLamaCuti()',
                                                'onkeypress'=>"return $(this).  focusNextInputField(event)",
                                                'class'=>'dtPicker3',
                                            ),
                                            )); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <?php echo $form->labelEx($modPegawaicuti,'lamacuti',array('class'=>'control-label')); ?>
                                <div class="controls">
                                    <?php echo $form->textField($modPegawaicuti,'lamacuti',array('class'=>'span1','onkeypress'=>'$(this).focusNextInputField(event)')).' hari'; ?>
                                </div>
                            </div>
            </td>
            <td>
                    <?php echo $form->textFieldRow($modPegawaicuti,'noskcuti',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawaicuti,'tglditetapkanskcuti',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php $this->widget('MyDateTimePicker',array(
                            'model'=>$modPegawaicuti,
                            'attribute'=>'tglditetapkanskcuti',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>'yy-mm-dd',
                            ),
                            'htmlOptions'=>array('readonly'=>true,
                                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                  'class'=>'dtPicker3',
                            ),
                            )); ?>
                        </div>
                    </div>
                    <?php echo $form->textFieldRow($modPegawaicuti,'keperluancuti',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                    <?php echo $form->textAreaRow($modPegawaicuti,'keterangan',array('onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                    <?php echo $form->dropDownListRow($modPegawaicuti,'pejabatmengetahui',CHtml::listData($modPegawaicuti->getPegawaiItems(),'nama_pegawai','nama_pegawai'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    <?php echo $form->dropDownListRow($modPegawaicuti,'pejabatmenyetujui',CHtml::listData($modPegawaicuti->getPegawaiItems(),'nama_pegawai','nama_pegawai'),array('empty'=>'-- Pilih --','onkeypress'=>'return $(this).focusNextInputField(event)')) ?>
                    </td>
                </tr>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitPegawaicuti')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form Pegawai cuti ======================================== -->
<!-- ================================== Form Izin tugas belajar =========================================== -->
        <div id="tableIjintugasbelajar">
            <legend class="rim">Izin Tugas Belajar Karyawan</legend>
            <table>
                <tr>
                    <td>
                        <div class="control-group">
                            <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $modIzintugasbelajar->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                            <?php echo $form->labelEx($modIzintugasbelajar,'tglmulaibelajar',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modIzintugasbelajar,
                                'attribute'=>'tglmulaibelajar',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                        <?php echo $form->textFieldRow($modIzintugasbelajar,'nomorkeputusan',array('class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')); ?>
                        <div class="control-group">
                            <?php echo $form->labelEx($modIzintugasbelajar,'tglditetapkan',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modIzintugasbelajar,
                                'attribute'=>'tglditetapkan',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                    </td>
                    <td>
            <?php echo $form->textAreaRow($modIzintugasbelajar,'keteranganizin',array('onkeypress'=>'$(this).focusNextInputField(event)')); ?>
            <?php echo $form->dropDownListRow($modIzintugasbelajar,'pejabatmemutuskan',CHtml::listData($modIzintugasbelajar->getPegawaiItems(),'nama_pegawai','nama_pegawai'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'$(this).focusNextInputField(event)')) ?>
                    </td>
                </tr>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitIzintugasbelajar')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form Izin tugas belajar ======================================== -->
<!-- ================================== Form Hukuman Disiplin =========================================== -->
        <div id="tableHukdisiplin">
            <legend class="rim">Hukuman Disiplin Karyawan</legend>
            <table style="width:100%;">
                <tr>
                    <td style="width:50%;">
                        <?php echo CHtml::hiddenField('url', $this->createUrl('', array('idPegawai' => $modHukdisiplin->pegawai_id)), array('readonly' => TRUE)); ?>
                            <?php echo CHtml::hiddenField('berubah', '', array('readonly' => TRUE)); ?>
                        <?php echo $form->dropDownListRow($modHukdisiplin,'jnshukdisiplin_id',CHtml::listData($modHukdisiplin->getJnshukdisiplinItems(),'jnshukdisiplin_id','jnshukdisiplin_nama'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                        <?php echo $form->dropDownListRow($modHukdisiplin,'hukdisiplin_jabatan',CHtml::listData($modHukdisiplin->getJabatanItems(),'jabatan_id','jabatan_nama'),array('class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)','empty'=>'-- Pilih ')) ?>
                        <div class="control-group">
                            <?php echo $form->labelEx($modHukdisiplin,'hukdisiplin_tglhukuman',array('class'=>'control-label')); ?>
                            <div class="controls">
                                <?php $this->widget('MyDateTimePicker',array(
                                'model'=>$modHukdisiplin,
                                'attribute'=>'hukdisiplin_tglhukuman',
                                'mode'=>'date',
                                'options'=> array(
                                    'dateFormat'=>'yy-mm-dd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                      'class'=>'dtPicker3',
                                ),
                                )); ?>
                            </div>
                        </div>
                        <?php echo $form->dropDownListRow($modHukdisiplin,'hukdisiplin_ruangan',CHtml::listData($modHukdisiplin->getRuanganItems(),'ruangan_id','ruangan_nama'),array('empty'=>'-- Pilih --','class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                        <?php echo $form->textFieldRow($modHukdisiplin,'hukdisiplin_nosk',array('class'=>'span3','onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                    </td>
                    <td style="width:50%;">
                        <div class="control-group">
                            <?php echo $form->labelEx($modHukdisiplin,'hukdisiplin_lamabln',array('class'=>'control-label')); ?>
                            <div class="controls">
                               <?php echo $form->textField($modHukdisiplin,'hukdisiplin_lamabln',array('class'=>'span1','onkeypress'=>'return $(this).focusNextInputField(event)')).' bulan'; ?>
                            </div>
                        </div>
                         <?php echo $form->textAreaRow($modHukdisiplin,'hukdisiplin_keterangan',array('onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                    </td>
                </tr>
            </table>
            <div class="form-actions">
                        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                              array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'submitButton','onKeypress'=>'return formSubmit(this,event)','name'=>'submitHukdisiplin')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                                              Yii::app()->createUrl($this->module->id.'/PegawaiM/Pencatatanriwayat'), 
                                                              array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            </div>
        </div>
<!-- ================================== Akhir form Hukuman disiplin ======================================== -->
        </div>
</fieldset>
<?php $this->endWidget(); ?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM;
if (isset($_GET['PegawaiM']))
    $modPegawai->attributes = $_GET['PegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPasien",
                            "onClick" => "
                                          getDataPegawai(\"$data->pegawai_id\");
                                          $(\"#dialogPegawai\").dialog(\"close\");    
                                          return false;
                                "))',
        ),
        'nomorindukpegawai',
        'nama_pegawai',
        'tempatlahir_pegawai',
        'tgl_lahirpegawai',
        'jeniskelamin',
        'statusperkawinan',
        array(
            'header'=>'Jabatan',
            'value'=>'(isset($data->jabatan->jabatan_nama) ? $data->jabatan->jabatan_nama : "-")',
        ),
        'alamat_pegawai',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<script type="text/javascript">
    var trPendidikanpegawai=new String(<?php echo CJSON::encode($this->renderPartial('_rowPendidikanpegawai',array('form'=>$form,'modPendidikanpegawai'=>$modPendidikanpegawai,),true));?>);
    var trPegawaidiklat=new String(<?php echo CJSON::encode($this->renderPartial('_rowPegawaidiklat',array('form'=>$form,'modPegawaidiklat'=>$modPegawaidiklat,),true));?>);
    var trPengalamankerja=new String(<?php echo CJSON::encode($this->renderPartial('_rowPengalamankerja',array('form'=>$form,'modPengalamankerja'=>$modPengalamankerja,),true));?>);

    function getDataPegawai(params)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai');?>", {idPegawai:params },
            function(data){
                $("#NIP").val(data.nomorindukpegawai);
                $("#pegawai_id").val(data.pegawai_id);
                $("#namapegawai").val(data.nama_pegawai);
                $("#tempatlahir_pegawai").val(data.tempatlahir_pegawai);
                $("#tgl_lahirpegawai").val(data.tgl_lahirpegawai);
                $("#jabatan").val(data.jabatan_nama);
                $("#jeniskelamin").val(data.jeniskelamin);
                $("#statusperkawinan").val(data.statusperkawinan);
                $("#alamat_pegawai").val(data.alamat_pegawai);
                if(data.photopegawai != ""){
                    var url = "<?php echo Params::urlPegawaiTumbsDirectory() . 'kecil_'; ?>" + data.photopegawai;
                    $("#photo_pasien").attr('src', url);
                } else {
                    var url = "<?php echo Params::urlPegawaiDirectory() . 'no_photo.jpeg'; ?>";
                    $("#photo_pasien").attr('src',url);
                }                
            }, "json");
    }

    function hitungLamaCuti()
    {
        var tglmulaicuti = $('#KPPegawaicutiT_tglmulaicuti').val();
        var tglAkhircuti = $('#KPPegawaicutiT_tglakhircuti').val();
        var thnawal = tglmulaicuti.substring(0,4);
        var blnawal = tglmulaicuti.substring(5,7);
        var tglawal = tglmulaicuti.substring(8,10);

        var thnakhir = tglAkhircuti.substring(0,4);
        var blnakhir = tglAkhircuti.substring(5,7);
        var tglakhir = tglAkhircuti.substring(8,10);

        var tglAwal     = new Date(thnawal, blnawal, tglawal);
        var tglAkhir    = new Date(thnakhir, blnakhir, tglakhir);
        var oneDay = 1000 * 24 * 60 *60;
        var diff = Math.ceil((tglAkhir.getTime() - tglAwal.getTime()) / oneDay) + 1;
        if(diff>0){
            $('#KPPegawaicutiT_lamacuti').val(diff);
        }else{
            alert('Tanggal Mulai Cuti Harus Kurang Dari Tanggal Akhir Cuti!');
            $('#KPPegawaicutiT_tglakhircuti').val(tglmulaicuti);
            $('#KPPegawaicutiT_lamacuti').val(1);
        }
        
    }

</script>
<?php
$urlGetPendidikanpegawai = Yii::app()->createUrl('actionAjax/GetPendidikanpegawai');
$urlGetPegawaidiklat = Yii::app()->createUrl('actionAjax/GetPegawaidiklat');
$urlGetPengalamankerja = Yii::app()->createUrl('actionAjax/GetPengalamankerja');
$urlGetPegawaijabatan = Yii::app()->createUrl('actionAjax/GetPegawaijabatan');
$urlGetPegmutasi = Yii::app()->createUrl('actionAjax/GetPegmutasi');
$urlGetPegawaicuti = Yii::app()->createUrl('actionAjax/GetPegawaicuti');
$urlGetIjintugasbelajar = Yii::app()->createUrl('actionAjax/GetIjintugasbelajar');
$urlGetHukdisiplin = Yii::app()->createUrl('actionAjax/GetHukdisiplin');
$js= <<< JS
    $(document).ready(function() {
        
          $(".btn-primary").click(function(){
            if (!$("#pegawai_id").val()) {
                alert("Anda belum memilih pegawai");
                return false;
            } else {
                return true;
            }
          });

          $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
          });

            // == Form Riwayat ==
            $("#tablePendidikanpegawai").show();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").show();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();

    });

    function tab(index) {
        $(this).hide();
        if (index==0) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").show();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").show();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==1) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").show();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").show();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==2) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").show();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").show();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==4) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").show();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").show();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==6) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").show();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").show();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==7) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").show();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").show();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==8) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tableJabatan").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").show();
            $("#tableHukdisiplin").hide();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatJabatan").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").show();
            $("#divRiwayatHukdisiplin").hide();
        } else if (index==9) {
            // == Form Riwayat ==
            $("#tablePendidikanpegawai").hide();
            $("#tableDiklatpegawai").hide();
            $("#tablePengalamankerja").hide();
            $("#tablePegawaimutasi").hide();
            $("#tablePegawaicuti").hide();
            $("#tableIjintugasbelajar").hide();
            $("#tableHukdisiplin").show();
            
            // == View Riwayat ==
            $("#divRiwayatPendidikanpegawai").hide();
            $("#divRiwayatPegawaidiklat").hide();
            $("#divRiwayatPengalamankerja").hide();
            $("#divRiwayatPegawaimutasi").hide();
            $("#divRiwayatPegawaicuti").hide();
            $("#divRiwayatIjintugasbelajar").hide();
            $("#divRiwayatHukdisiplin").show();
        }
    }
    
// ========================== Script pendidikan pegawai ===================================   
    function tambahPendidikanpegawai(obj) {
        $("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInputpendidikanpegawai();
    }

    function tambahPendidikanpegawaidrinput(obj) {
        $("#hapus").show();
        $("#tambah").hide();
        $(obj).parents("table").children("tbody").append(trPendidikanpegawai.replace());
        renameInputpendidikanpegawai();
    }

    function hapusPendidikanpegawai(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpendidikanpegawai(){
        nourut = 1;
        $(".keterangan").each(function(){
            $(this).parents('tr').find('[name*="KPPendidikanpegawaiR"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPendidikanpegawaiR[]');
                var id = input.split('KPPendidikanpegawaiR[][');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPendidikanpegawaiR['+nourut+']'+data[1]);
//                    $(this).attr('id','date_'+nourut);
                    if(data[1] === '[nourut_pend]'){
                        $(this).attr('name','KPPendidikanpegawaiR['+nourut+']'+data[1]).val(nourut+1);
                    }
                };
            });
            
            $(this).parents('tr').find('[id*="date"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('date-');
                    $(this).attr('id','date-'+data[1]+nourut);
                    
                $(function() {
                    $( "#date-"+data[1]+nourut).datepicker({
                        firstDay: 7,
                        dateFormat:'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                    });
                });
                $.datepicker.setDefaults($.datepicker.regional['id']);

            });
            nourut++;
        });
    }
    
        function Pendidikanpegawaidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
            }else{
                $.post("${urlGetPendidikanpegawai}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatpendidikanpegawai").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPendidikanpegawai() {
            
            if ($("#cekRiwayatPendidikanpegawai").is(":checked")) {
                Pendidikanpegawaidata();
                $("#tableRiwayatpendidikanpegawai").slideDown(60);
            } else {
                $("#tableRiwayatpendidikanpegawai").children("tbody").children("tr").remove();
                $("#tableRiwayatpendidikanpegawai").slideUp(60);
            }
        }
// ========================== Akhir script pendidikan pegawai ===================================

// ========================== Script Pegawai diklat ===================================   
    function tambahPegawaidiklat(obj) {
        $(obj).parents("td").children("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPegawaidiklat.replace());
        renameInputpegawaidiklat();
    }

    function tambahPegawaidiklatdrinput(obj) {
        $("#hapus").show();
        $("#tambah").hide();
        $(obj).parents("table").children("tbody").append(trPegawaidiklat.replace());
        renameInputpegawaidiklat();
    }

    function hapusPegawaidiklat(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpegawaidiklat(){
        nourutdk = 1;
        gilirandk = 2;
        $(".keterangan").each(function(){
            $(this).parents('tr').find('[name*="KPPegawaidiklatT"]').each(function(){
                var input = $(this).attr('name');
                var data = input.split('KPPegawaidiklatT[]');
                var id = input.split('KPPegawaidiklatT[][');
                if (typeof data[1] === 'undefined'){} else{
                    $(this).attr('name','KPPegawaidiklatT['+nourutdk+']'+data[1]);
                };
            });
            
            $(this).parents('tr').find('[id*="date"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('date-');
                    $(this).attr('id','date-'+data[1]+nourutdk);
                    
                $(function() {
                    $( "#date-"+data[1]+nourutdk).datepicker({
                        firstDay: 7,
                        dateFormat:'yy-mm-dd',
                        changeMonth: true,
                        changeYear: true,
                    });
                });
                $.datepicker.setDefaults($.datepicker.regional['id']);

             $(this).parents('tr').find('[id*="KPPegawaidiklat_no"]').each(function() {
                var input = $(this).attr('id');
                var data = input.split('KPPegawaidiklat_');
                $(this).attr('id','KPPegawaidiklat_'+data[1]+nourutdk);
                $("#KPPegawaidiklat_"+data[1]+nourutdk).val(gilirandk);
             });

            });
            nourutdk++;
            gilirandk++;
        });
    }
    
        function Pegawaidiklatdata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
            }else{
                $.post("${urlGetPegawaidiklat}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegawaidiklat").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPegawaidiklat() {
            
            if ($("#cekRiwayatPegawaidiklat").is(":checked")) {
                Pegawaidiklatdata();
                $("#tableRiwayatPegawaidiklat").slideDown(60);
            } else {
                $("#tableRiwayatPegawaidiklat").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaidiklat").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai diklat ===================================

// ========================== Script Pengalaman kerja ===================================   
    function tambahPengalamankerja(obj) {
        $(obj).parents("td").children("#hapus").show();
        $(obj).hide();
        $(obj).parents("table").children("tbody").append(trPengalamankerja.replace());
        renameInputpengalamankerja(obj);
    }

    function hapusPengalamankerja(obj) {
        $("#tambah").show();
        $(obj).hide();
        $(obj).parents("tr").remove();
    }
    
    function renameInputpengalamankerja(obj){
        nourutpk = 0;
        giliranpk = 1;
        $(obj).parents("tbody").find('[class="keterangan"]').each(
            function(){
                $(this).parents('tr').find('[name*="KPPengalamankerjaR"]').each(function(){
                    var input = $(this).attr('name');
                    var data = input.split('KPPengalamankerjaR[]');
                    if (typeof data[1] === 'undefined'){} else{
                        $(this).attr('name','KPPengalamankerjaR['+nourutpk+']'+data[1]);
                        var input_dua = $(this).attr('id');
                        var data_dua = input_dua.split('KPPengalamankerjaR_');
                        if (typeof data_dua[1] === 'undefined'){} else{
                            $(this).attr('id','KPPengalamankerjaR_'+nourutpk+'_'+data_dua[1]);
                        }
                    };
                });

                if(nourutpk > 0){
                    $(this).parents('tr').find('[id*="date"]').each(function() {
                        var input = $(this).attr('id');
                        var data = input.split('date-');
                            $(this).attr('id','date-'+data[1]+nourutpk);
                            $(function(){
                                $( "#date-"+data[1]+nourutpk).datepicker({
                                    firstDay: 7,
                                    dateFormat:'yy-mm-dd',
                                    changeMonth: true,
                                    changeYear: true,
                                });
                            });
                        $.datepicker.setDefaults($.datepicker.regional['id']);
                        //$('#date-undefined0').removeAttr('class');
                        //$("#date-undefined0").remove();
                    
                        $("#KPPengalamankerjaR_"+nourutpk+"_pengalamankerja_nourut").val(giliranpk);
                        /*
                        $(this).parents('tr').find('[id*="KPPengalamankerjaR_no"]').each(function() {
                            var input = $(this).attr('id');
                            var data = input.split('KPPengalamankerjaR_');
                            $(this).attr('id','KPPengalamankerjaR_'+data[1]+nourutpk);
                            $("#KPPengalamankerjaR_"+nourutpk+"_"+data[1]).val(giliranpk);
                            $("#KPPengalamankerjaR_"+nourutpk+"_pengalamankerja_nourut").val(giliranpk);
                        });
                        */
                    });
                }
                nourutpk++;
                giliranpk++;                
            }
        );


    }
    
        function Pengalamankerjadata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
            }else{
                $.post("${urlGetPengalamankerja}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPengalamankerja").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatPengalamankerja() {
            
            if ($("#cekRiwayatPengalamankerja").is(":checked")) {
                Pengalamankerjadata();
                $("#tableRiwayatPengalamankerja").slideDown(60);
            } else {
                $("#tableRiwayatPengalamankerja").children("tbody").children("tr").remove();
                $("#tableRiwayatPengalamankerja").slideUp(60);
            }
        }
// ========================== Akhir script Pengalaman kerja ===================================
                
// ========================== Script Pegawai jabatan =======================================
        function Pegawaijabatandata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegawaijabatan}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatJabatan").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatJabatan() {
            
            if ($("#cekRiwayatjabatan").is(":checked")) {
                Pegawaijabatandata();
                $("#tableRiwayatJabatan").slideDown(60);
            } else {
                $("#tableRiwayatJabatan").children("tbody").children("tr").remove();
                $("#tableRiwayatJabatan").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai jabatan ===================================
                
// ========================== Script Mutasi kerja =======================================
        function Pegmutasidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegmutasi}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegmutasi").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewPegmutasi() {
            
            if ($("#cekRiwayatPegawaimutasi").is(":checked")) {
                Pegmutasidata();
                $("#tableRiwayatPegmutasi").slideDown(60);
            } else {
                $("#tableRiwayatPegmutasi").children("tbody").children("tr").remove();
                $("#tableRiwayatPegmutasi").slideUp(60);
            }
        }
// ========================== Akhir script Mutasi kerja ===================================
                
// ========================== Script Pegawai cuti =======================================
        function Pegawaicutidata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetPegawaicuti}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatPegawaicuti").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewPegawaicuti() {
            
            if ($("#cekRiwayatPegawaicuti").is(":checked")) {
                Pegawaicutidata();
                $("#tableRiwayatPegawaicuti").slideDown(60);
            } else {
                $("#tableRiwayatPegawaicuti").children("tbody").children("tr").remove();
                $("#tableRiwayatPegawaicuti").slideUp(60);
            }
        }
// ========================== Akhir script Pegawai cuti ===================================

// ========================== Script Ijin tugas belajar =======================================
        function Ijintugasbelajardata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetIjintugasbelajar}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatIjintugasbelajar").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewIjintugasbelajar() {
            
            if ($("#cekRiwayatIjintugasbelajar").is(":checked")) {
                Ijintugasbelajardata();
                $("#tableRiwayatIjintugasbelajar").slideDown(60);
            } else {
                $("#tableRiwayatIjintugasbelajar").children("tbody").children("tr").remove();
                $("#tableRiwayatIjintugasbelajar").slideUp(60);
            }
        }
// ========================== Akhir script Ijin tugas belajar ===================================

// ========================== Script Hukuman disiplin =======================================
        function Hukdisiplindata()
        {
            pegawai_id = $('#pegawai_id').val();
            if(pegawai_id==''){
                alert('Anda belum memilih pegawai');
                return false;
            }else{
                $.post("${urlGetHukdisiplin}", {pegawai_id:pegawai_id,},
                function(data){
                    $("#tableRiwayatHukdisiplin").children("tbody").append(data.tr);
                }, "json");
            }   
        }

        function ViewRiwayatHukdisiplin() {
            
            if ($("#cekRiwayatHukdisiplin").is(":checked")) {
                Hukdisiplindata();
                $("#tableRiwayatHukdisiplin").slideDown(60);
            } else {
                $("#tableRiwayatHukdisiplin").children("tbody").children("tr").remove();
                $("#tableRiwayatHukdisiplin").slideUp(60);
            }
        }
// ========================== Akhir script Hukuman disiplin ===================================
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>
<ul class="nav nav-tabs" id="tabmenu"><li class=""></li></ul>
<?php if (isset($_GET['tab'])){
    Yii::app()->clientScript->registerScript('onreadyfunction','
        $("#tabmenu").find("li[index=\''.$_GET['tab'].'\']").find("a").click();
        tab('.$_GET['tab'].');
        ', CClientScript::POS_READY);
} ?>


<?php
$js = <<< JS

//===============Awal untu Mengecek Form Sudah DiUbah Atw Belum====================    
    $(":input").keyup(function(event){
            $('#berubah').val('Ya');
         });
    $(":input").change(function(event){
            $('#berubah').val('Ya');
         });  
    $(":input").click(function(event){
            $('#berubah').val('Ya');
         });  
//================Akhir Untuk Mengecek  Form Sudah DiUbah Atw Belum===================         
JS;
Yii::app()->clientScript->registerScript('tableDiklatpegawai', $js, CClientScript::POS_READY);
?>

<?php
$js = <<< JS
//==================================================Validasi===============================================
//*Jangan Lupa Untuk menambahkan hiddenField dengan id "berubah" di setiap form
//* hidden field dengan id "url"
//*Copas Saja hiddenfield di Line 34 dan 35
//* ubah juga id button simpannya jadi "btn_simpan"

function palidasiForm(obj)
   {
        var berubah = $('#berubah').val();
        if(berubah=='Ya') 
        {
           if(confirm('Apakah Anda Akan menyimpan Perubahan Yang Sudah Dilakukan?'))
               {
                    $('#url').val(obj);
                    $('#btn_simpan').click();
          
               }

        }      
   }
JS;
Yii::app()->clientScript->registerScript('validasi', $js, CClientScript::POS_HEAD);
?>   

