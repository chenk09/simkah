
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
//echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>5));     
?>
<br>
<table width="100%">
    <tr>
        <td> <?php echo "Tanggal Pengajuan : ".$model->tglpenajuan; ?></td>
        
        <td rowspan="2" widt="40%"> <?php echo "Keterangan : ".$model->keterangan; ?></td>
    </tr>   
    <tr>
        <td><?php echo "No Pengajuan : ".$model->nopengajuan; ?></td>
    </tr>
</table>
<br><br>
<table class="items table table-striped table-bordered table-condensed">
    <thead>
            <tr>
                <th>No Urut</th>
                <th>Jabatan/ Pekerjaan</th>
                <th>Jumlah Orang</th>
                <th>Untuk Keperluan</th>
                <th>Keterangan</th>
            </tr>
        </thead>
    <?php
    foreach($modDetailPengcals as $i=>$modDetailPengcal) {
        $id_occupation = $modDetailPengcal->occupation_id;
        $occopation = OccupationM::model()->findByPk($id_occupation);
    ?>

        <tr>
            <td><?php echo $modDetailPengcal->nourut;?></td>
            <td><?php echo $occopation->occupation_nama;?>
            </td>
            <td><?php echo $modDetailPengcal->jmlorang;?></td>
            <td><?php echo $modDetailPengcal->untukkeperluan; ?></td>
            <td><?php echo $modDetailPengcal->keterangan; ?></td>

        </tr>

    <?php }  ?>
</table>
<br><br>
<table width="100%" style="margin-left: 20px">
    <tr>
        <td>
            <?php echo "Yang Mengajukan : ".$model->yangmengajukan; ?>
        </td>
        <td width="30%">&nbsp;</td>
        <td>
            <?php echo "Yang Mengetahui : ".$model->mengetahui?>
       </td>
    </tr>
</table>    