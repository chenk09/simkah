<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pengpegawai-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return validasiForm()'),
        'focus'=>'#KPPengajuanPegawaiT_nopengajuan',
));
$this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php echo $form->errorSummary(array($model,$modPengpegdet)); ?>
<fieldset>
    <legend class="rim2">Pengajuan Karyawan</legend>
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>    
</fieldset>
<br>

<table>
    <tr>
        <td> 
            <div class="control-group">
                <?php echo $form->labelEx($model,'tglpengajuan',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php
                        $this->widget('MyDateTimePicker', array(
                            'model'=>$model,
                            'attribute'=>'tglpengajuan',
                            'mode'=>'date',
                            'options'=>array(
                                'dateFormat'=>  Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions'=>array('readonly'=>true, 'class'=>'dtPicker2 isRequired','onkeypress'=>"return $(this).focusNextInputField(event)"),
                        ));
                    ?>
                </div>
            </div>
        
        <td rowspan="2"> <?php echo $form->textAreaRow($model,'keterangan',array('rows'=>2, 'cols'=>30, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?></td>
    </tr>   
    <tr>
        <td><?php echo $form->textFieldRow($model,'nopengajuan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?></td>
    </tr>
</table>
<?php 
if (isset($modPengpegdets)){
    echo $form->errorSummary($modPengpegdets); 
}
?>
<table class="items table table-striped table-bordered table-condensed" id="tblInputPengcal">
    <?php if(empty($_GET['id'])) { ?> <thead>
        <tr>
            <th>No Urut</th>
            <th>Jumlah Orang <font color="red">*</font></th>
            <th>Untuk Keperluan</th>
            <th>Keterangan</th>
            <th>Disetujui <font color="red">*</font></th>
            <th></th>
        </tr>
    </thead>
    
    <?php } ?>
    <?php
    if(isset($_GET['i']))
        $id = $_GET['id'];
    else
        $id = 0;
    echo $this->renderPartial('_addPengcal',array('modPengpegdet'=>$modPengpegdet,'modPengpegdets'=>$modPengpegdets,'id'=>$id,),true); ?>
     
</table>
<br>
<table>
    <tr>
        <td>
            <?php //if(!empty($_GET['id'])){ echo "Yang Mengajukan : ".$model->namayangmengajukan; } else{ ?>
            <?php echo $form->labelEx($model,'mengajukan_id', array('class'=>'control-label')); ?> 
            <div class="controls">
                <?php echo CHtml::activeHiddenField($model,'mengajukan_id'); ?>
                <?php //echo CHtml::hiddenField('ygmengajukan_id'); ?>
                    <div style="float:left;">
                        <?php
                            $this->widget('MyJuiAutoComplete',array(
                                'model'=>$model,
                                'attribute'=>'mengajukanNama',
                                'sourceUrl'=>  Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/PegawaiUntukKP'),
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'minLength'=>2,
                                    'select'=>'js:function( event, ui ) {
                                            $("#KPPengajuanPegawaiT_mengajukan_id").val(ui.item.pegawai_id);
                                                }',
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogPegawaiYangMengajukan'),
                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;')
                            ));
                        ?>
                    </div>
            </div>
            <?php //} ?>
        </td>
        <td>
             <?php //if(!empty($_GET['id'])){ echo "Yang Mengetahui : ".$model->namayangmengetahui; } else{ ?>
             <?php echo $form->labelEx($model,'mengetahui', array('class'=>'control-label')); ?> 
            <div class="controls">
                <?php echo CHtml::activeHiddenField($model,'mengetahui_id');?>
                    <div style="float:left;">
                        <?php
                            $this->widget('MyJuiAutoComplete',array(
                                'model'=>$model,
                                'attribute'=>'mengetahui',
                                'sourceUrl'=>  Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/PegawaiUntukKP'),
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'minLength'=>2,
                                    'select'=>'js:function( event, ui ) {
                                            $("#KPPengajuanPegawaiT_mengetahui_id").val(ui.item.pegawai_id);
                                                }',
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogPegawaiYangMengetahui'),
                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;')
                            ));
                        ?>
                    </div>
            </div>
            <?php //} ?>
       </td>
    </tr>
</table>        
                     
<div class="form-actions">
     <?php if(empty($_GET['id'])) { echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('id'=>'btnSubmit','class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
     <?php }else {  echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')'));  }?>
     <?php 
        if(isset($_GET['modulId']))
            $moduleId = '&modulId='.$_GET['modulId'];
        else
            $moduleId = null;
        echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/pengajuanPegawaiT/Index'.$moduleId), 
                array('class'=>'btn btn-danger',
                        'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
       
        
</div>
<?php $this->endWidget(); ?>
<?php
//===============Dialog buat pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogPegawaiYangMengajukan',
    'options'=>array(
        'title'=>'Pencarian Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new KPPegawaiM('search');
$modPegawai->unsetAttributes();
if(isset($_GET['KPPegawaiM'])){
    $modPegawai->attributes = $_GET['KPPegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengajukan-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"$(\"#KPPengajuanPegawaiT_mengajukan_id\").val(\"$data->pegawai_id\");
                            $(\"#'.CHtml::activeId($model,'mengajukanNama').'\").val(\"$data->gelardepan $data->nama_pegawai\");
                            $(\"#dialogPegawaiYangMengajukan\").dialog(\"close\");
                            return false;"
                ))'
        ),
        
        'gelardepan',
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai',
        'jeniswaktukerja',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>

<?php
//===============Dialog buat pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogPegawaiYangMengetahui',
    'options'=>array(
        'title'=>'Pencarian Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new KPPegawaiM('search');
$modPegawai->unsetAttributes();
if(isset($_GET['KPPegawaiM'])){
    $modPegawai->attributes = $_GET['KPPegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengetahui-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"$(\"#KPPengajuanPegawaiT_mengetahui_id\").val(\"$data->pegawai_id\");
                            $(\"#'.CHtml::activeId($model,'mengetahui').'\").val(\"$data->gelardepan  $data->nama_pegawai\");
//                            submitPegawai();
                            $(\"#dialogPegawaiYangMengetahui\").dialog(\"close\");
                            return false;"
                ))'
        ),
        
        'gelardepan',
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai',
        'jeniswaktukerja',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>

<?php
//$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id'=>$model->pengpegawai_id));
//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=570px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('printCaraPrint',$js,  CClientScript::POS_HEAD);
?>
<script type="text/javascript">
    
function addRowTindakan(obj)
{
    var trAddPengcal=new String(<?php echo CJSON::encode($this->renderPartial('_addPengcal',array('modPengpegdet'=>$modPengpegdet,'removeButton'=>true),true));?>);
    $(obj).parents('table').children('tbody').append(trAddPengcal.replace());
    <?php 
        $attributes = $modPengpegdet->attributeNames(); 
        foreach($attributes as $i=>$attribute){
            echo "renameInput('KPPengpegawaidetT','$attribute');";
        }
    ?>

    renameInput('KPPengpegawaidetT','jmlorang');
    renameInput('KPPengpegawaidetT','untukkeperluan');
    renameInput('KPPengpegawaidetT','keterangan');
    renameInput('KPPengpegawaidetT','disetujui');

}

function batalTindakan(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan tindakan?')){
//        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
        <?php 
            foreach($attributes as $i=>$attribute){
                echo "renameInput('KPPengpegawaidetT','$attribute');";
            }
        ?>
        renameInput('KPPengpegawaidetT','jmlorang');
        renameInput('KPPengpegawaidetT','untukkeperluan');
        renameInput('KPPengpegawaidetT','keterangan');
        renameInput('KPPengpegawaidetT','disetujui');
    }
}
function renameInput(modelName,attributeName)
{
    var trLength = $('#tblInputPengcal tr').length;
    var i = -1;
    $('#tblInputPengcal tr').each(function(){
        if($(this).has('input[name$="[nourut]"]').length){
            i++;
            $("#KPPengpegawaidetT_"+i+"_nourut").val((i+1));
            
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('textarea[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('textarea[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
//        $(this).find('input[name^="occupation["]').attr('name','occupation['+i+']');
//        $(this).find('input[name^="occupation["]').attr('id','occupation_'+i+'');
    });
    
//    if (attributeName == 'occupation'){
//        jQuery('#KPPengpegawaidetT_'+i+'_occupation').autocomplete({'showAnim':'fold','minLength':2,'select':function( event, ui ) {
////                console.log("#KPPengpegawaidetT_"+i+"_occupation");
//        $("#KPPengpegawaidetT_"+i+"_occupation_id").val(ui.item.occupation_id);
//        $("#KPPengpegawaidetT_"+i+"_occupation").val(ui.item.occupation_nama);
//        },'source':'/ehospitaljk/index.php?r=kepegawaian/ActionAutoCompleteKP/OccupationKP'});
//    }
}

//function setDialog(obj){
//    parent = $(obj).parents(".input-append").find("input").attr("id");
//    dialog = "#dialogOccupation";
//    $(dialog).attr("parent-dialogs",parent);
//    $(dialog).dialog("open");
//}

//function setTindakanAuto(params){
//    dialog = "#dialogOccupation";
//    parent = $(dialog).attr("parent-dialogs");
//    obj = $("#"+parent);
////    console.log(params);
//    $(obj).parents('tr').find('input[name$="[occupation_id]"]').val(params[0]);
//    $(obj).parents('tr').find('input[name$="[occupation]"]').val(params[1]);
//    $(dialog).dialog("close");
//    
//}

function cekNumerik(obj)
{
   
  number = obj.value;
  var list = new Array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
  var status = true;
  for (i=0; i<=number.length-1; i++)
  {
     if (number[i] in list) cek = true;
     else cek = false;
     status = status && cek;
  }

  if (status === false)
  {
     alert("Input Harus angka");
     obj.value = '';
     return false;
  }
  else
  {
     return true;
  }
}
function validasiForm(event){
//    var required = $('.isReq').val('');
    kosong = '';
    required = $('#pengpegawai-t-form').find(".isReq[value="+kosong+"]");
    jumlah =  required.length; 
    if(jumlah == 0){
        $('#btnSubmit').click();
        return true;
    }else{
        alert("Silahkan isi yang bertanda * !");
        return false;
    }
}
</script>