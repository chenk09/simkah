<?php
if(isset($modPengpegdets) && count($modPengpegdets) > 0) {
    $this->renderPartial('_daftarAddPengcal',array('modPengpegdets'=>$modPengpegdets,'id'=>$id,'removeButton'=>true));
} else {
?>

<tr>
    <td><?php echo CHtml::activeTextField($modPengpegdet,'[0]nourut',array('class'=>'span1', 'readonly'=>true)); ?></td>
<!--        <td>
            <?php // echo CHtml::activeHiddenField($modPengpegdet,'[0]occupation_id');?>
                <div style="float:left;">
                    <?php
//                        $this->widget('MyJuiAutoComplete',array(
//                            'model'=>$modPengpegdet,
//                            'attribute'=>'[0]occupation',
//                            'sourceUrl'=>  Yii::app()->createUrl('hrd/ActionAutoComplete/OccupationKP'),
//                            'options'=>array(
//                                'showAnim'=>'fold',
//                                'minLength'=>2,
//                                'select'=>'js:function( event, ui ) {
//                                        $("#DetailpengkaryawanT_0_occupation_id").val(ui.item.occupation_id);
//                                        $("#DetailpengkaryawanT_0_occupation").val(ui.item.occupation_nama);
//                                }',
//                            ),
//                            'tombolDialog'=>array('idDialog'=>'dialogOccupation','jsFunction'=>"setDialog(this);"),
//                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;'),
//                            
//                        ));
                    ?>
                </div>
        </td>-->
        <td><?php echo CHtml::activeTextField($modPengpegdet,'[0]jmlorang',array('class'=>'span1 isReq', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'cekNumerik(this);')); ?></td>
        <td><?php echo CHtml::activeTextArea($modPengpegdet,'[0]untukkeperluan', array('style'=>'resize:none;','rows'=>2, 'cols'=>20, 'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
        <td><?php echo CHtml::activeTextArea($modPengpegdet,'[0]keterangan', array('style'=>'resize:none;','rows'=>2, 'cols'=>20, 'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
        <td><?php echo CHtml::activeCheckBox($modPengpegdet,'[0]disetujui', array('checked'=>true,'class'=>'span1 isReq','onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
        <td>
            <?php 
            $removeButton = (isset($removeButton)) ? $removeButton : false;
            if($removeButton){
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah tindakan')); 
                echo "<br/><br/>";
                echo CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'batalTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan tindakan'));
            } 
            else {
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah tindakan'));
            }
        ?>
        </td>
    </tr>
            
            
<?php } ?>