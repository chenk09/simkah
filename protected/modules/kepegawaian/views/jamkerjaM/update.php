
<?php
$this->breadcrumbs=array(
	'Statusscan Ms'=>array('index'),
	$model->jamkerja_id=>array('view','id'=>$model->jamkerja_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Jam Kerja', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//      
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jam Kerja', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model)); ?>
