<?php
$this->breadcrumbs=array(
	'Statusscan Ms'=>array('index'),
	$model->jamkerja_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Jam Kerja', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//           
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Jam Kerja', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'jamkerja_id',
		'shift_id',
		'jamkerja_nama',
		'jammasuk',
                                   'jamisitrahat',
		'jammasukistirahat',
		'jammulaiscanmasuk',
		'jamakhirscanmasuk',
                                     'jammulaiscanplng',
		'jamakhirscanplng',
		'toleransiterlambat',
		'toleransiplgcpt',
		'jamkerja_aktif',
                                array(
                                    'label'=>'Aktif',
                                    'value'=>(($model->jamkerja_aktif==1)? "Ya" : "Tidak"),
                                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>