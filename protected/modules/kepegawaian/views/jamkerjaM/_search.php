<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'statusscan-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'statusscan_id',array('class'=>'span5')); ?>

	   <?php echo $form->textFieldRow($model,'shift_id',array('class'=>'span3',  'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'jamkerja_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
 <div class="control-group ">
    <?php echo $form->labelEx($model, 'jammasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jampulang', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jampulang',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamisitrahat', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamisitrahat',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammasukistirahat', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammasukistirahat',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammulaiscanmasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammulaiscanmasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamakhirscanmasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamakhirscanmasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammulaiscanplng', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammulaiscanplng',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>       

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamakhirscanplng', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamakhirscanplng',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>      
         
            <?php echo $form->textFieldRow($model,'toleransiterlambat',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
          <?php echo $form->textFieldRow($model,'toleransiplgcpt',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
       
            <?php echo $form->textFieldRow($model,'toleransiplgcpt',array('class'=>'span3','maxlength'=>50)); ?>
<?php echo $form->checkBoxRow($model,'jamkerja_aktif',array('checked'=>true)); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
