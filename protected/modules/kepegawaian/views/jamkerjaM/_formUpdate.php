<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'statusscan-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#JamkerjaM_jamkerja_nama',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

          <?php echo $form->textFieldRow($model,'shift_id',array('class'=>'span3',  'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'jamkerja_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
 <div class="control-group ">
    <?php echo $form->labelEx($model, 'jammasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jampulang', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jampulang',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamisitrahat', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamisitrahat',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammasukistirahat', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammasukistirahat',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>
        
<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammulaiscanmasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammulaiscanmasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamakhirscanmasuk', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamakhirscanmasuk',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jammulaiscanplng', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jammulaiscanplng',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>       

<div class="control-group ">
    <?php echo $form->labelEx($model, 'jamakhirscanplng', array('class' => 'control-label')); ?>
<div class="controls">
    <?php
    $this->widget('MyDateTimePicker', array(
        'model' => $model,
        'attribute' => 'jamakhirscanplng',
        'mode' => 'time',
        'options' => array(
            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
        ),
        'htmlOptions' => array('readonly' => true,
            'onkeypress' => "return $(this).focusNextInputField(event)",
            'class' => 'dtPicker3',
        ),
    ));
    ?> 
</div>
</div>      
         
            <?php echo $form->textFieldRow($model,'toleransiterlambat',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
          <?php echo $form->textFieldRow($model,'toleransiplgcpt',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
       
            <?php echo $form->checkBoxRow($model,'jamkerja_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.jamkerjaM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('kepegawaian.views.tips.tipsaddedit',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('StatusscanM_statusscan_namalain').value = nama.value.toUpperCase();
    }
</script>