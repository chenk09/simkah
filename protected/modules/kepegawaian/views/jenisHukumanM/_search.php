<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'sagelar-belakang-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'gelarbelakang_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jnshukdisiplin_nama',array('class'=>'span3','maxlength'=>15)); ?>

	<?php echo $form->textFieldRow($model,'jnshukdisiplin_namalain',array('class'=>'span3','maxlength'=>15)); ?>

	<?php echo $form->checkBoxRow($model,'jnshukdisiplin_aktif',array('checked'=>'gelarbelakang_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
