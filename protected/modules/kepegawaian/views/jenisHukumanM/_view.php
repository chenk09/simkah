<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('jnshukdisiplin_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->jnshukdisiplin_id),array('view','id'=>$data->jnshukdisiplin_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jnshukdisiplin_nama')); ?>:</b>
	<?php echo CHtml::encode($data->jnshukdisiplin_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jnshukdisiplin_namalain')); ?>:</b>
	<?php echo CHtml::encode($data->jnshukdisiplin_namalain); ?>
	<br />
        
        <b><?php echo CHtml::encode($data->getAttributeLabel('jnshukdisiplin_aktif')); ?>:</b>
	<?php echo CHtml::encode((($data->jnshukdisiplin_aktif==1)? Yii::t('mds','Yes') : Yii::t('mds','No')))?>
	<br />
</div>