
<?php
$this->breadcrumbs=array(
	'Sakelompokpegawai Ms'=>array('index'),
	'Create',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Kelompok Karyawan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Kelompok Pegawai', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Kelompok Karyawan', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php //echo //$this->renderPartial('_tabMenu',array()); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>