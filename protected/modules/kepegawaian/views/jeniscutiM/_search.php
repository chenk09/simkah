<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'kpjeniscuti-m-search',
    'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'jeniscuti_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jeniscuti_nama',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'jeniscuti_namalainnya',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'jeniscuti_aktif',array('checked'=>'jeniscuti_aktif')); ?>

	<div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>
<?php $this->endWidget(); ?>
