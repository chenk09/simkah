
<?php
$this->breadcrumbs=array(
	'Golongan Gaji Ms'=>array('index'),
	'Create',
);

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Potongan Pph 21 ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
(Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Potongan Pph 21', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>