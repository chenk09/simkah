<?php
$this->breadcrumbs=array(
	'Golongan Gaji Ms'=>array('index'),
	$model->potonganpph21_id,
);

$arrMenu = array();
    array_push($arrMenu,array('label'=>Yii::t('mds','View').' Potongan Pph 21 ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
    (Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Golongan Gaji', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'potonganpph21_id',
		//'golonganpegawai.golonganpegawai_nama',
                                    'penghasilandari',
		'sampaidgn_thn',
		'persentarifpenghsl',
		
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>