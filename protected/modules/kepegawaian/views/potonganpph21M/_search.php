<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'golongan-gaji-m-search',
    'type'=>'horizontal',
)); ?>

                <?php echo $form->textFieldRow($model,'potonganpph21_id',array('class'=>'span3','maxlength'=>15)); ?>

	
	<?php echo $form->textFieldRow($model,'penghasilandari',array('class'=>'span3','maxlength'=>50)); ?>
<?php echo $form->textFieldRow($model,'sampaidgn_thn',array('class'=>'span3','maxlength'=>50)); ?>
<?php echo $form->textFieldRow($model,'persentarifpenghsl',array('class'=>'span3','maxlength'=>50)); ?>

	<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
