<?php 
if($caraPrint == 'EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));      
?>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
    'id'=>'laporanpajak-v-grid',
    'dataProvider'=>$model->searchPrint(),
//  'filter'=>$model,
        'template'=>"{pager}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Pengurang</center>',
                'start'=>'7',
                'end'=>'9',
            ),
        ),
        'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    // 'footerHtmlOptions'=>array('colspan'=>3,'style'=>'font-weight:bold;text-align:right;'),
                    // 'footer'=>'Total',
                ),
                array(
                    'header'=>'Nama Karyawan',
                    'value'=>'$data->nama_pegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'NRK',
                    'type'=>'raw',
                    'value'=>'$data->nomorindukpegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Kategori Pegawai',
                    'type'=>'raw',
                    'value'=>'$data->kategoripegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'type'=>'raw',
                    'value'=>'$data->jeniskelamin',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'STA',
                    'type'=>'raw',
                    'value'=>'$data->kodeptkp',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Gaji',
                    'type'=>'raw',
                    'value'=>'number_format($data->totalterima)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jumlah',
                    'type'=>'raw',
                    'value'=>'number_format($data->gajipertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Biaya Jabatan',
                    'type'=>'raw',
                    'value'=>'number_format($data->biayajabatan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'JHT/Pensiun',
                    'type'=>'raw',
                    'value'=>'number_format($data->potonganpensiun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jumlah',
                    'type'=>'raw',
                    'value'=>'number_format($data->potonganpensiun + $data->biayajabatan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Penghasilan Netto',
                    'type'=>'raw',
                    'value'=>'number_format($data->gajikotor)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PTKP',
                    'type'=>'raw',
                    'value'=>'number_format($data->ptkppertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PKP',
                    'type'=>'raw',
                    'value'=>'number_format($data->pkp)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PPh 21 Terutang',
                    'type'=>'raw',
                    'value'=>'number_format($data->pph21pertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PPh 21 per Bulan',
                    'type'=>'raw',
                    'value'=>'number_format($data->pph21perbulan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),              
            ),
)); ?>