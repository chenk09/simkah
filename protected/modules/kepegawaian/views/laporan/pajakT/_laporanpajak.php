<legend class="rim2">Laporan Pajak</legend>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'laporan-search',
        'type'=>'horizontal',
)); ?>

<table>
    <tr>

        <td>
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'periodegaji_awal', array('class' => 'control-label')); ?>
            <div class="controls">
            <?php $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'periodegaji_awal',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=> 'MM yy',
                                        'changeYear' => true,
                                        'changeMonth' => true,
                                        'changeDate' => false,
                                        'showSecond' => false,
                                        'showDate' => false,
                                        'showMonth' => false,
                                        'firstDay' => 1,
                                        //TANGGAL HANYA BULAN DAN TAHUN SAJA 
                                        //BY RAFA
                                        'onChangeMonthYear'=>'js:function(y, m, i){                                
                                                              var d = i.selectedDay;
                                                              $(this).datepicker("setDate", new Date(y, m - 1, d));
                                                              }'
                                        ),
                                    'htmlOptions'=>array('readonly'=>true,
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                          'class'=>'dtPicker3',
                                     ),
            )); ?> 
            </div>

        </td>
        <td>
                <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'span3','maxlength'=>30)); ?>
                <?php
                    echo $form->dropDownListRow(
                        $model,'unit_perusahaan',LookupM::model()->items('unit_perusahaan'),
                        array('class'=>'span3', 'empty'=>'-- Pilih --')
                    );
                ?>
        </td>
    </tr>
    <tr>
        <td>
            <?php // echo $form->textFieldRow($model,'kategoripegawai',array('class'=>'span3','maxlength'=>30)); ?>
            <?php
                    echo $form->dropDownListRow(
                        $model,'kategoripegawai',LookupM::model()->items('kategoripegawai'),
                        array('class'=>'span3', 'empty'=>'-- Pilih --')
                    );
                ?>
        </td>
    </tr>
</table>
	
	<div class="form-actions">
           <?php 
            echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
            echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('onClick'=>'reseting()', 'class'=>'btn btn-danger', 'type'=>'reset'));
         ?>
	</div>


<legend class="rim">Tabel Pajak</legend>
 <style type="text/css">
    .ui-datepicker table{
        display: none;
    }
</style>
<?php $this->renderPartial('pajakT/laporan',array('model'=>$model)); ?>
<?php
$this->endWidget();
?>
<?php //$this->renderPartial('_tab'); ?>
<!-- <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
</iframe>   -->
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanPajak');
$url = '';
$this->renderPartial('_footerWithoutgrafik', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>