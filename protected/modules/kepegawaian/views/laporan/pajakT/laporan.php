<?php
// $this->breadcrumbs=array(
// 	'KppajakV Ts'=>array('index'),
// 	'Manage',
// );

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#laporan-search').submit(function(){
        $('#laporanpajak-v-grid').addClass('srbacLoading');
    $.fn.yiiGridView.update('laporanpajak-v-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'laporanpajak-v-grid',
	'dataProvider'=>$model->search(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Pengurang</center>',
                'start'=>'7',
                'end'=>'9',
            ),
        ),
	    'columns'=>array(
		        array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                    'htmlOptions'=>array('style'=>'text-align:center;'),
                    // 'footerHtmlOptions'=>array('colspan'=>3,'style'=>'font-weight:bold;text-align:right;'),
                    // 'footer'=>'Total',
                ),
                array(
                    'header'=>'Nama Karyawan',
                    'value'=>'$data->nama_pegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'NRK',
                    'type'=>'raw',
                    'value'=>'$data->nomorindukpegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Kategori Pegawai',
                    'type'=>'raw',
                    'value'=>'$data->kategoripegawai',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'type'=>'raw',
                    'value'=>'$data->jeniskelamin',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'STA',
                    'type'=>'raw',
                    'value'=>'$data->kodeptkp',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Gaji',
                    'type'=>'raw',
                    'value'=>'number_format($data->totalterima)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jumlah',
                    'type'=>'raw',
                    'value'=>'number_format($data->gajipertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Biaya Jabatan',
                    'type'=>'raw',
                    'value'=>'number_format($data->biayajabatan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'JHT/Pensiun',
                    'type'=>'raw',
                    'value'=>'number_format($data->potonganpensiun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Jumlah',
                    'type'=>'raw',
                    'value'=>'number_format($data->potonganpensiun + $data->biayajabatan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'Penghasilan Netto',
                    'type'=>'raw',
                    'value'=>'number_format($data->gajikotor)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PTKP',
                    'type'=>'raw',
                    'value'=>'number_format($data->ptkppertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PKP',
                    'type'=>'raw',
                    'value'=>'number_format($data->pkp)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PPh 21 Terutang',
                    'type'=>'raw',
                    'value'=>'number_format($data->pph21pertahun)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                array(
                    'header'=>'PPh 21 per Bulan',
                    'type'=>'raw',
                    'value'=>'number_format($data->pph21perbulan)',
                    'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                ),
                // array(
                //     'header'=>'Total Pajak',
                //     'name'=>'totalpajak',
                //     'type'=>'raw',
                //     'value'=>'MyFunction::formatNumber($data->totalpajak)',
                //     'headerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:center;'),
                //     'htmlOptions'=>array('style'=>'text-align:right;'),
                //     'footerHtmlOptions'=>array('style'=>'font-weight:bold;text-align:right;'),
                //     'footer'=>'sum(totalpajak)',
                // ),
            ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
        }',
)); ?>