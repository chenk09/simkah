<style type="text/css">
    .ui-datepicker table{
        display: none;
    }
</style>
<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#laporan-search').submit(function(){
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Laporan Rekap Gaji</legend>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'laporan-search',
        'type'=>'horizontal',
)); ?>

<table>
    <tr>
        <td>
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'periodegaji', array('class' => 'control-label')); ?>
            <div class="controls">
                <?php $this->widget('MyDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'periodegaji_view',
                    'mode'=>'date',
                    'options'=> array(
                        'dateFormat'=> 'MM yy',
                        'changeYear' => true,
                        'changeMonth' => true,
                        'changeDate' => false,
                        'showSecond' => false,
                        'showDate' => false,
                        'showMonth' => false,
                        'onChangeMonthYear'=>'js:function(y, m, i){                                
                                var d = i.selectedDay;
                                $(this).datepicker("setDate", new Date(y, m - 1, d));
                            }'
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                    ),
                )); ?> 
            </div>
        </div>
        <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3','maxlength'=>30)); ?>
        </td>
        
    </tr>
    <tr>
            <td><div id='searching'>
                    <fieldset>
                        <?php $this->Widget('ext.bootstrap.widgets.BootAccordion',array(
                                    'id'=>'big',
                                    'slide'=>false,
                                    'content'=>array(
                                        'content2'=>array(
                                        'header'=>'Berdasarkan Instalasi dan Ruangan',
                                        'isi'=>'<table>
                                                    <tr>
                                                        <td>'.CHtml::hiddenField('filter', 'carabayar', array('disabled'=>'disabled')).'<label>Instalasi</label></td>
                                                        <td>'.$form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type' => 'POST',
                                                                'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
                                                                'update' => '#ruangan',  //selector to update
                                                            ),
                                                        )).'
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <label>Ruangan</label>
                                                        </td>
                                                        <td>
                                                            <div id="ruangan">
                                                                <label>Data Tidak Ditemukan</label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                 </table>',
                                         'active'=>true
                                        ),
                                    ),
//                                    'htmlOptions'=>array('class'=>'aw',)
                            )); ?>
                 </fieldset> </div>
      </td>
    </tr>   
</table>
                
	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); 
                // echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                //            Yii::app()->createUrl($this->module->id.'/'.Laporan.'/RekapPenggajian'), 
                //            array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
            ?>
	</div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Penggajian</legend>
<?php $this->renderPartial('rekapGaji/_table',array('model'=>$model)); ?>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintRekapPenggajian');
$this->renderPartial('_footerWithoutgrafik', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
