<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#laporan-search').submit(function(){
                $('#laporan-grid').addClass('srbacLoading');
            $.fn.yiiGridView.update('laporan-grid', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 

$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchInformasi();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
    $model->print = true;
    $template = "{items}";
    if ($caraPrint == "EXCEL") {
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'laporan-grid',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>TUNJANGAN TETAP</center>',
                'start'=>5, 
                'end'=>16,
            ),
            array(
                'name'=>'<center>TUNJANGAN TIDAK TETAP</center>',
                'start'=>17, 
                'end'=>19,
            ),
            array(
                'name'=>'<center>POTONGAN</center>',
                'start'=>21, 
                'end'=>33,
            ),
        ),
	    'columns'=>array(
            array(
                'header'=>'No',
                'type'=>'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
             array(
                'header'=>'No Induk Pegawai',
                'type'=>'raw',
                'value'=>'$data->nomorindukpegawai',
            ),
            'nama_pegawai',
            // 'pegawai.statuspegawai',
            'periode',
            array(
                'header'=>'Gaji pokok',
                'type'=>'raw',
                'value'=>'number_format($data->gapok,0,"",".")',
            ),
            array(
                'header'=>'Tunjangan Harian',
                'type'=>'raw',
                'value'=>'number_format($data->harian,0,"",".")',
            ),
            array(
                'header'=>'Total Makan & Transport',
                'type'=>'raw',
                'value'=>'number_format($data->jumlah,0,"",".")',
            ),
            array(
                'header'=>'Jabatan',
                'type'=>'raw',
                'value'=>'number_format($data->jabatan,0,"",".")',
            ),
            array(
                'header'=>'Fungsional',
                'type'=>'raw',
                'value'=>'number_format($data->fungsional,0,"",".")',
            ),
            array(
                'header'=>'Khusus',
                'type'=>'raw',
                'value'=>'number_format($data->khusus,0,"",".")',
            ),
            array(
                'header'=>'Sosial',
                'type'=>'raw',
                'value'=>'number_format($data->sosial,0,"",".")',
            ),
            array(
                'header'=>'PPH 21',
                'type'=>'raw',
                'value'=>'number_format($data->pph21,0,"",".")',
            ),
            array(
                'header'=>'Essensial',
                'type'=>'raw',
                'value'=>'number_format($data->essensial,0,"",".")',
            ),
            array(
                'header'=>'Pensiun',
                'type'=>'raw',
                'value'=>'number_format($data->pensiun,0,"",".")',
            ),
            array(
                'header'=>'Kesehatan',
                'type'=>'raw',
                'value'=>'number_format($data->kesehatan,0,"",".")',
            ),
            array(
                'header'=>'Kompetensi',
                'type'=>'raw',
                'value'=>'number_format($data->kompetensi,0,"",".")',
            ),
            array(
                'header'=>'Kemahalan',
                'type'=>'raw',
                'value'=>'number_format($data->kemahalan,0,"",".")',
            ),
            array(
                'header'=>'Insentif',
                'type'=>'raw',
                'value'=>'number_format($data->insentif,0,"",".")',
            ),
            array(
                'header'=>'Jasa Lainnya',
                'type'=>'raw',
                'value'=>'number_format($data->jasa,0,"",".")',
            ),
            array(
                'header'=>'Lembur',
                'type'=>'raw',
                'value'=>'number_format($data->lembur,0,"",".")',
            ),
            array(
                'header'=>'Gaji Kotor',
                'type'=>'raw',
                'value'=>'number_format($data->gajikotor,0,"",".")',
            ),
            array(
                'header'=>'Koperasi',
                'type'=>'raw',
                'value'=>'number_format($data->koperasi,0,"",".")',
            ),
            array(
                'header'=>'Jamsostek',
                'type'=>'raw',
                'value'=>'number_format($data->jamsostek,0,"",".")',
            ),
            array(
                'header'=>'Pot. Pensiun',
                'type'=>'raw',
                'value'=>'number_format($data->PotPensiun,0,"",".")',
            ),
            array(
                'header'=>'Pot. Kesehatan',
                'type'=>'raw',
                'value'=>'number_format($data->PotKesehatan,0,"",".")',
            ),
            array(
                'header'=>'BRI',
                'type'=>'raw',
                'value'=>'number_format($data->potbri,0,"",".")',
            ),
            array(
                'header'=>'BSJ',
                'type'=>'raw',
                'value'=>'number_format($data->potbsj,0,"",".")',
            ),
            array(
                'header'=>'BTN',
                'type'=>'raw',
                'value'=>'number_format($data->potbtn,0,"",".")',
            ),
            array(
                'header'=>'Intern',
                'type'=>'raw',
                'value'=>'number_format($data->PotIntern,0,"",".")',
            ),
            array(
                'header'=>'Apotek',
                'type'=>'raw',
                'value'=>'number_format($data->apotek,0,"",".")',
            ),
            array(
                'header'=>'Lab',
                'type'=>'raw',
                'value'=>'number_format($data->lab,0,"",".")',
            ),
            array(
                'header'=>'TM. KRJ',
                'type'=>'raw',
                'value'=>'number_format($data->tmkrj,0,"",".")',
            ),
            array(
                'header'=>'Pajak',
                'type'=>'raw',
                'value'=>'number_format($data->pajak,0,"",".")',
            ),
            array(
                'header'=>'Lain2',
                'type'=>'raw',
                'value'=>'number_format($data->lain2,0,"",".")',
            ),
            array(
                'header'=>'Tot. Potongan',
                'type'=>'raw',
                'value'=>'number_format($data->totalpotongan,0,"",".")',
            ),
            array(
                'header'=>'Gaji Bersih',
                'type'=>'raw',
                'value'=>'number_format($data->gajibersih,0,"",".")',
            ),
            array(
                'header'=>'Bank',
                'type'=>'raw',
                'value'=>'(($data->keterangan=="TUNAI") ? 0 : number_format($data->gajibersih,0,"","."))',
            ),
            array(
                'header'=>'Tunai',
                'type'=>'raw',
                'value'=>'(($data->keterangan=="TUNAI") ? number_format($data->gajibersih,0,"",".") : 0)',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>