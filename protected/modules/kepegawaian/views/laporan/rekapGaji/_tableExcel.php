<?php
    Yii::app()->clientScript->registerScript('cari cari', "
        $('#laporan-search').submit(function(){
                $('#laporan-grid').addClass('srbacLoading');
            $.fn.yiiGridView.update('laporan-grid', {
                data: $(this).serialize()
            });
            return false;
        });
    ");
?>

<?php 
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'laporan-grid',
	'dataProvider'=>$model->search(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>TUNJANGAN TETAP</center>',
                'start'=>6, 
                'end'=>16,
            ),
            array(
                'name'=>'<center>TUNJANGAN TIDAK TETAP</center>',
                'start'=>17, 
                'end'=>19,
            ),
            array(
                'name'=>'<center>POTONGAN</center>',
                'start'=>21, 
                'end'=>33,
            ),
        ),
	    'columns'=>array(
            array(
                'header'=>'No',
                'type'=>'raw',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            'pegawai.nomorindukpegawai',
            'pegawai.nama_pegawai',
            // 'pegawai.statuspegawai',
            'periode',
            array(
                'header'=>'Gaji pokok',
                'type'=>'raw',
                // 'value'=>'number_format($data->gapok,0,"",".")',
                'value'=>'$data->gapok',
            ),
            array(
                'header'=>'Harian',
                'type'=>'raw',
                // 'value'=>'number_format($data->harian,0,"",",")',
                'value'=>'$data->harian',
            ),
            array(
                'header'=>'Jabatan',
                'type'=>'raw',
                // 'value'=>'number_format($data->jabatan,0,"",".")',
                'value'=>'$data->jabatan',
            ),
            array(
                'header'=>'Fungsional',
                'type'=>'raw',
                // 'value'=>'number_format($data->fungsional,0,"",".")',
                'value'=>'$data->fungsional',
            ),
            array(
                'header'=>'Khusus',
                'type'=>'raw',
                // 'value'=>'number_format($data->khusus,0,"",".")',
                'value'=>'$data->khusus',
            ),
            array(
                'header'=>'Sosial',
                'type'=>'raw',
                // 'value'=>'number_format($data->sosial,0,"",".")',
                'value'=>'$data->sosial',
            ),
            array(
                'header'=>'PPH 21',
                'type'=>'raw',
                // 'value'=>'number_format($data->pph21,0,"",".")',
                'value'=>'$data->pph21',
            ),
            array(
                'header'=>'Essensial',
                'type'=>'raw',
                // 'value'=>'number_format($data->essensial,0,"",".")',
                'value'=>'$data->essensial',
            ),
            array(
                'header'=>'Pensiun',
                'type'=>'raw',
                // 'value'=>'number_format($data->pensiun,0,"",".")',
                'value'=>'$data->pensiun',
            ),
            array(
                'header'=>'Kesehatan',
                'type'=>'raw',
                // 'value'=>'number_format($data->kesehatan,0,"",".")',
                'value'=>'$data->kesehatan',
            ),
            array(
                'header'=>'Kompetensi',
                'type'=>'raw',
                // 'value'=>'number_format($data->kompetensi,0,"",".")',
                'value'=>'$data->kompetensi',
            ),
            array(
                'header'=>'Kemahalan',
                'type'=>'raw',
                // 'value'=>'number_format($data->kemahalan,0,"",".")',
                'value'=>'$data->kemahalan',
            ),
            array(
                'header'=>'Insentif',
                'type'=>'raw',
                // 'value'=>'number_format($data->insentif,0,"",".")',
                'value'=>'$data->insentif',
            ),
            array(
                'header'=>'Jasa Lainnya',
                'type'=>'raw',
                // 'value'=>'number_format($data->jasa,0,"",".")',
                'value'=>'$data->jasa',
            ),
            array(
                'header'=>'Lembur',
                'type'=>'raw',
                // 'value'=>'number_format($data->lembur,0,"",".")',
                'value'=>'$data->lembur',
            ),
            array(
                'header'=>'Gaji Kotor',
                'type'=>'raw',
                // 'value'=>'number_format($data->gajikotor,0,"",".")',
                'value'=>'$data->gajikotor',
            ),
            array(
                'header'=>'Koperasi',
                'type'=>'raw',
                // 'value'=>'number_format($data->koperasi,0,"",".")',
                'value'=>'$data->koperasi',
            ),
            array(
                'header'=>'Jamsostek',
                'type'=>'raw',
                // 'value'=>'number_format($data->jamsostek,0,"",".")',
                'value'=>'$data->jamsostek',
            ),
            array(
                'header'=>'Pot. Pensiun',
                'type'=>'raw',
                // 'value'=>'number_format($data->PotPensiun,0,"",".")',
                'value'=>'$data->PotPensiun',
            ),
            array(
                'header'=>'Pot. Kesehatan',
                'type'=>'raw',
                // 'value'=>'number_format($data->PotKesehatan,0,"",".")',
                'value'=>'$data->PotKesehatan',
            ),
            array(
                'header'=>'BRI',
                'type'=>'raw',
                // 'value'=>'number_format($data->potbri,0,"",".")',
                'value'=>'$data->potbri',
            ),
            array(
                'header'=>'BSJ',
                'type'=>'raw',
                // 'value'=>'number_format($data->potbsj,0,"",".")',
                'value'=>'$data->potbsj',
            ),
            array(
                'header'=>'BTN',
                'type'=>'raw',
                // 'value'=>'number_format($data->potbtn,0,"",".")',
                'value'=>'$data->potbtn',
            ),
            array(
                'header'=>'Intern',
                'type'=>'raw',
                // 'value'=>'number_format($data->PotIntern,0,"",".")',
                'value'=>'$data->PotIntern',
            ),
            array(
                'header'=>'Apotek',
                'type'=>'raw',
                // 'value'=>'number_format($data->apotek,0,"",".")',
                'value'=>'$data->apotek',
            ),
            array(
                'header'=>'Lab',
                'type'=>'raw',
                // 'value'=>'number_format($data->lab,0,"",".")',
                'value'=>'$data->lab',
            ),
            array(
                'header'=>'TM. KRJ',
                'type'=>'raw',
                // 'value'=>'number_format($data->tmkrj,0,"",".")',
                'value'=>'$data->tmkrj',
            ),
            array(
                'header'=>'Pajak',
                'type'=>'raw',
                // 'value'=>'number_format($data->pajak,0,"",".")',
                'value'=>'$data->pajak',
            ),
            array(
                'header'=>'Lain2',
                'type'=>'raw',
                // 'value'=>'number_format($data->lain2,0,"",".")',
                'value'=>'$data->lain2',
            ),
            array(
                'header'=>'Tot. Potongan',
                'type'=>'raw',
                // 'value'=>'number_format($data->totalpotongan,0,"",".")',
                'value'=>'$data->totalpotongan',
            ),
            array(
                'header'=>'Gaji Bersih',
                'type'=>'raw',
                // 'value'=>'number_format($data->gajibersih,0,"",".")',
                'value'=>'$data->gajibersih',
            ),
            array(
                'header'=>'Bank',
                'type'=>'raw',
                // 'value'=>'(($data->keterangan=="TUNAI") ? 0 : number_format($data->gajibersih,0,"","."))',
                'value'=>'(($data->keterangan=="TUNAI") ? 0 : $data->gajibersih)',
            ),
            array(
                'header'=>'Tunai',
                'type'=>'raw',
                // 'value'=>'(($data->keterangan=="TUNAI") ? number_format($data->gajibersih,0,"",".") : 0)',
                'value'=>'(($data->keterangan=="TUNAI") ? $data->gajibersih : 0)',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>