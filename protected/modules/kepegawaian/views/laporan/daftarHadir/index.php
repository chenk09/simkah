<div class="search-form">
    <?php
        $this->renderPartial('daftarHadir/_search',
            array(
                'model'=>$model,
            )
        );
    ?>
</div>
<?php
      $this->widget('ext.bootstrap.widgets.BootGridView',
        array(
            'id'=>'lapegawai-m-grid',
            'dataProvider'=>$model->searchByNofinger(),
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$row+1'
                ),
                'nomorindukpegawai',
                'nama_pegawai',
                'unit_perusahaan',
                'jeniskelamin',
                'kelompokpegawai.kelompokpegawai_nama',
                'jabatan.jabatan_nama',
//                'jml_kehadiran',
                array(
                    'header' => 'Jumlah Kehadiran',
                    'value' => '$data->getPresensi($data->tglpresensi,$data->tglpresensi_akhir)'
                ),
                array(
                   'type'=>'raw',
                   'value'=>'CHtml::link("<i class=icon-list-alt></i><br>Daftar Hadir", Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/laporan/detailLaporanAbsen",array("id"=>"$data->pegawai_id")), array("target"=>"frame_detail", "onclick"=>"$(\'#detailAbsen\').dialog(\'open\');", "rel"=>"tooltip","rel"=>"tooltip","title"=>"Detail Daftar Hadir"))',
                   'htmlOptions'=>array('style'=>'text-align: center')
                ),
            ),
            'afterAjaxUpdate'=>'
                function(id, data){
                    jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            }',
        )
  );
?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'detailAbsen',
            'options'=>array(
                'title'=>'Detail Absen Karyawan',
                'autoOpen'=>false,
                'minWidth'=>500,
                'width'=>900,
                'modal'=>true,
            ),
        )
    );
?>
<iframe src="" height="500" width="100%"  name="frame_detail"></iframe>
<?php
    $this->endWidget('zii.widgets.jui.CJuiDialog');
?>