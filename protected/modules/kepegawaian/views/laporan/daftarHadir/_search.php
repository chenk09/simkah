<script type="text/javascript">
    function reseting()
    {
        setTimeout(function(){
            $.fn.yiiGridView.update('lapegawai-m-grid', {
                    data: $('#lapegawai-m-search').serialize()
            });
        },1000);

    }   
</script>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'lapegawai-m-search',
            'type'=>'horizontal',
        )
    );
    ?>
    <table width="100%">
        <tr>
            <td>
                <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'span3','maxlength'=>30)); ?>
                <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3','maxlength'=>50)); ?>
                <?php
                    echo $form->dropDownListRow(
                        $model,'unit_perusahaan',LookupM::model()->items('unit_perusahaan'),
                        array('class'=>'span3', 'empty'=>'-- Pilih --')
                    );
                ?>
            </td>
            <td>
                <?php echo $form->dropDownListRow($model,'jabatan_id',CHtml::listData(JabatanM::model()->findAll('jabatan_aktif = true'), 'jabatan_id', 'jabatan_nama'),array('class'=>'span3','maxlength'=>50, 'empty'=>'-- Pilih --')); ?>
                <?php echo $form->dropDownListRow($model,'kelompokpegawai_id',CHtml::listData(KelompokpegawaiM::model()->findAll('kelompokpegawai_aktif = true'), 'kelompokpegawai_id', 'kelompokpegawai_nama'),array('class'=>'span3', 'empty'=>'-- Pilih --')); ?>
            </td>
            <td>
                <div class="control-group ">
            <?php echo $form->labelEx($model, 'tglpresensi', array('class' => 'control-label')); ?>
            <div class="controls">
            <?php $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tglpresensi',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'changeYear'=>true,
                                        'changeMonth'=>true,
                                        'yearRange'=>'-70y:+4y',
                                        'maxDate'=>'d',
                                        'showAnim'=>'fold',
                                        'timeText'=>'Waktu',
                                        'hourText'=>'Jam',
                                        'minuteText'=>'Menit',
                                        'secondText'=>'Detik',
                                        'showSecond'=>true,
                                        'timeFormat'=>'hh:mm:ss',
                                        
                                       
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,
                                                          'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                          'class'=>'dtPicker3',
                                     ),
            )); ?> 
            </div>
        </div>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpresensi_akhir', array('class' => 'control-label')); ?>
                    <div class="controls">
                    <?php $this->widget('MyDateTimePicker',array(
                                            'model'=>$model,
                                            'attribute'=>'tglpresensi_akhir',
                                            'mode'=>'date',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'changeYear'=>true,
                                                'changeMonth'=>true,
                                                'yearRange'=>'-70y:+4y',
                                                'maxDate'=>'d',
                                                'showAnim'=>'fold',
                                                'timeText'=>'Waktu',
                                                'hourText'=>'Jam',
                                                'minuteText'=>'Menit',
                                                'secondText'=>'Detik',
                                                'showSecond'=>true,
                                                'timeFormat'=>'hh:mm:ss',
                                              
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,
                                                                  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                  'class'=>'dtPicker3',
                                             ),
                    )); ?> 
                    </div>
                </div>
            </td>
        </tr>
        
        
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
            echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('onClick'=>'reseting()', 'class'=>'btn btn-danger', 'type'=>'reset'));
         ?>
    </div>
    <?php $this->endWidget(); ?>
</fieldset>
<?php
Yii::app()->clientScript->registerScript('search', "
$('#lapegawai-m-search').submit(function(){
    $.fn.yiiGridView.update('lapegawai-m-grid', {
            data: $(this).serialize()
    });
    return false;
});
");
?>