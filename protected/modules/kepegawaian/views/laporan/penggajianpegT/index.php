<?php

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#laporan-search').submit(function(){
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Laporan Penggajian</legend>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'laporan-search',
        'type'=>'horizontal',
)); ?>

<table>
    <tr>
        <td>
        <div class="control-group ">
            <?php echo $form->labelEx($model, 'tglAwal', array('class' => 'control-label')); ?>
            <div class="controls">
            <?php $this->widget('MyDateTimePicker',array(
				'model'=>$model,
				'attribute'=>'tglAwal',
				'mode'=>'date',
				'options'=> array(
					'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
					'changeYear'=>true,
					'changeMonth'=>true,
					'yearRange'=>'-70y:+4y',
					'maxDate'=>'d',
					'showAnim'=>'fold',
					'timeText'=>'Waktu',
					'hourText'=>'Jam',
					'minuteText'=>'Menit',
					'secondText'=>'Detik',
					'showSecond'=>true,
					'timeFormat'=>'hh:mm:ss',
				),
				'htmlOptions'=>array(
					'readonly'=>true,
					'onkeypress'=>"return $(this).focusNextInputField(event)",
					'class'=>'dtPicker3',
				 ),
            )); ?> 
            </div>
        </div>
        <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3','maxlength'=>30)); ?>
		<?php echo $form->dropDownListRow($model,'keterangan', CaraBayarKeluar::items(),array(
			'empty'=>'-- Pilih --',
			'class'=>'span3',
			'onkeypress'=>"return $(this).focusNextInputField(event);"
		)); ?>		
        </td>
        <td>
			   <div class="control-group ">
					<?php echo $form->labelEx($model, 'tglAkhir', array('class' => 'control-label')); ?>
					<div class="controls">
						<?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tglAkhir',
							'mode'=>'date',
							'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
								'changeYear'=>true,
								'changeMonth'=>true,
								'yearRange'=>'-70y:+4y',
								'maxDate'=>'d',
								'showAnim'=>'fold',
								'timeText'=>'Waktu',
								'hourText'=>'Jam',
								'minuteText'=>'Menit',
								'secondText'=>'Detik',
								'showSecond'=>true,
								'timeFormat'=>'hh:mm:ss',
							  
							),
							'htmlOptions'=>array(
								'readonly'=>true,
								'onkeypress'=>"return $(this).focusNextInputField(event)",
								'class'=>'dtPicker3',
							 ),
						)); ?> 
					</div>
                </div>
				<?php echo $form->dropDownListRow($model,'jabatan_id',CHtml::listData($model->getJabatanItems(),'jabatan_id','jabatan_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
				<?php echo $form->dropDownListRow($model,'kategoripegawai',KategoriPegawai::items(), array(
					'empty'=>'-- Pilih --',
					'onkeypress'=>"return $(this).focusNextInputField(event)", 
				)); ?>
        </td>
    </tr>
</table>
	
	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                       Yii::app()->createUrl($this->module->id.'/'.Laporan.'/laporanPenggajian'), 
                       array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Penggajian</legend>
<?php $this->renderPartial('penggajianpegT/_table',array('model'=>$model)); ?>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanPenggajian');
$this->renderPartial('_footerWithoutgrafik', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
