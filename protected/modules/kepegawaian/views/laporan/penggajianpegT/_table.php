<?php 
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'laporan-grid',
	'dataProvider'=>$model->searchLaporan(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                     array(
                        'header'=>'No',
                        'type'=>'raw',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    'tglpenggajian',
                    'pegawai.nomorindukpegawai',
                    'pegawai.nama_pegawai',
                    'pegawai.jabatan.jabatan_nama',
                    'pegawai.kategoripegawai',
                    'pegawai.no_rekening',
                    'keterangan',
                    array(
                        'header'=>'Total gaji',
                        'type'=>'raw',
                        'value'=>'number_format($data->totalterima,0,"",".")',
                    ),
                    array(
                        'header'=>'Total Potongan',
                        'type'=>'raw',
                        'value'=>'number_format($data->totalpotongan,0,"",".")',
                    ),
                    array(
                        'header'=>'Total Bersih',
                        'type'=>'raw',
                        'value'=>'number_format($data->penerimaanbersih,0,"",".")',
                    )
                ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>