<?php
$this->breadcrumbs=array(
	'Pelamar Ts'=>array('index'),
	$model->pelamar_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Pelamar '.$model->nama_pelamar, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PelamarT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PelamarT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PelamarT', 'icon'=>'pencil','url'=>array('update','id'=>$model->pelamar_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PelamarT','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->pelamar_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' PelamarT', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'pelamar_id',
		'namasuku',
//		'pegawai_id',
		'pendkualifikasiNama',
		'pendidikannama',
		'profilnama',
		'tgllowongan',
		'jenisidentitas',
		'noidentitas',
		'nama_pelamar',
		'nama_keluarga',
		'tempatlahir_pelamar',
		'tgl_lahirpelamar',
		'jeniskelamin',
		'statusperkawinan',
		'jmlanak',
		'alamat_pelamar',
		'kodepos',
		'agama',
		'alamatemail',
		'notelp_pelamar',
		'nomobile_pelamar',
		'warganegara_pelamar',
		'photopelamar',
		'gajiygdiharapkan',
		'tglditerima',
		'tglmulaibekerja',
		'ingintunjangan',
		'keterangan_pelamar',
		'minatpekerjaan',
		'filelamaran',
		'crate_time',
//                  'create_user',
//		'pelamar_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>