
<fieldset>
    <legend class="rim">Kontrak Karyawan</legend>
<table>
    <tr>
        <td>
            <div class="control-group">
                <?php echo $form->labelEx($modKontrak,'nosuratkontrak', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modKontrak,'nosuratkontrak',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>   
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
           <div class="control-group">
                <?php echo $form->labelEx($modKontrak,'tglkontrak', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$modKontrak,
                            'attribute'=>'tglkontrak',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'minDate' => 'd',
                            ),
                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        )); 
                    ?>            
                </div>
            </div> 
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group">
            <?php echo $form->labelEx($modKontrak,'tglmulaikontrak', array('class'=>'control-label')) ?>
                <div class="controls">
                    <table style="width: 350px; margin: 0; padding: 0">
                        <tr>
                            <td> <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$modKontrak,
                            'attribute'=>'tglmulaikontrak',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'minDate' => 'd',
                                'onkeypress'=>"js:function(){lamaKontrak(this);}",
                                'onSelect'=>'js:function(){lamaKontrak(this);}',
                            ),
                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                            )); 
                    ?>  </td>
                            <td>Sampai : </td>
                            <td><?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$modKontrak,
                            'attribute'=>'tglakhirkontrak',
                            'mode'=>'date',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'minDate' => 'd',
                                'onkeypress'=>"js:function(){lamaKontrak(this);}",
                                'onSelect'=>'js:function(){lamaKontrak(this);}',
                            ),
                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )); 
                    ?> </td>
                        </tr>
                    </table>
                    
                      
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group">
                <?php echo $form->labelEx($modKontrak,'lamakontrak', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modKontrak,'lamakontrak',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="control-group">
                <?php echo $form->labelEx($modKontrak,'keterangankontrak', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textArea($modKontrak,'keterangankontrak',array('rows'=>3, 'cols'=>50, 'class'=>'span4', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
        </td>
    </tr>
</table>


    
<?php //echo $form->textFieldRow($modKontrak,'tglkontrak',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($modKontrak,'nourutkontrak',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>


    
<div class="control-group">
    <?php //echo $form->labelEx($modKontrak,'tglmulaikontrak', array('class'=>'control-label')) ?>
    <div class="controls">
        <?php //echo $form->textField($modKontrak,'tglmulaikontrak',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<!--        &nbsp; S/d : &nbsp;&nbsp;-->
        <?php //echo $form->textField($modKontrak,'tglakhirkontrak',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    </div>
</div>


<?php //echo $form->textFieldRow($modKontrak,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($modKontrak,'create_user',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
</fieldset>