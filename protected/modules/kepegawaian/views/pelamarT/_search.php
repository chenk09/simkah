<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'pelamar-t-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'pelamar_id',array('class'=>'span5')); ?>
<table>
    <tr>
        <td>
            <?php echo $form->textFieldRow($model,'nama_pelamar',array('class'=>'span3')); ?>
            <?php //echo $form->labelEx($model,'nama_pelamar', array('class'=>'control-label')); ?> 
            <!-- <div class="controls">
                <?php //echo CHtml::activeHiddenField($model,'nama_pelamar'); ?>
                <?php //echo CHtml::hiddenField('ygmengajukan_id'); ?>
                    <div style="float:left;">
                        <?php
                            $this->widget('MyJuiAutoComplete',array(
                                'model'=>$model,
                                'attribute'=>'nama_pelamar',
                                'sourceUrl'=>  Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/NamaPelamar'),
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'minLength'=>2,
                                    'select'=>'js:function( event, ui ) {
                                            $("#HRDPelamarT_nama_pelamar").val(ui.item.nama_pelamar);
                                                }',
                                ),
//                                'tombolDialog'=>array('idDialog'=>'dialogPegawaiYangMengajukan'),
                                'htmlOptions'=>array('class'=>'span2','style'=>'float:left;')
                            ));
                        ?>
                    </div>
            </div> -->
        </td>
<!--    </tr>
    <tr>
        <td><?php //echo $form->textFieldRow($model,'nama_pelamar',array('class'=>'span3','maxlength'=>100)); ?></td>-->
        <td><?php echo $form->dropDownListRow($model,'pendidikan_id', CHtml::listData($model->PendidikanItems, 'pendidikan_id', 'pendidikan_nama') ,array('class'=>'span3', 'empty'=>'-- Pilih Pendidikan Pelamar --')); ?></td>
    </tr>
    
</table>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
</div>

<?php $this->endWidget(); ?>
