<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pelamar-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return validasiPelamar();'),
        'focus'=>'#KPPelamarT_jenisidentitas',
)); ?>
<fieldset>
    <legend class="rim2">Pencatatan Pelamar</legend>
</fieldset>
<br>
<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary(array($model, $modBahasa, $modLingkunganKerja)); ?>
<legend class="rim">Data Pelamar</legend>
 <?php echo $this->renderPartial('_formPelamar', array('model'=>$model, 'form'=>$form)); ?>            
            
<!--================================================================================== INPUT KEMAMPUAN BAHASA PELAMAR ===============================-->

<br>
<fieldset>
    <legend class="rim">Data Kemampuan Bahasa Pelamar</legend>
    <br>
    <table class="items table table-striped table-bordered table-condensed" id="tblInputBahasa">
        <thead>
            <th>No</th>
            <th>Bahasa</th>
            <th>Mengerti</th>
            <th>Berbicara</th>
            <th>Menulis</th>
            <th>&nbsp;</th>
        </thead>
        <?php
            echo $this->renderPartial('_addBahasa',array('modBahasa'=>$modBahasa,'modBahasas'=>$modBahasas,'form'=>$form,'btnHapus'=>false),true); 
        ?>
            
    </table>
</fieldset>

<!--================================================================================== AKHIR INPUT KEMAMPUAN BAHASA===============================-->

            
            
            
<!--================================================================================== INPUT LINGKUNGAN KERJA PELAMAR ===============================-->

<br>
<fieldset>
    <legend class="rim">Data Lingkungan Kerja Pelamar</legend>
    <br>
    <table class="items table table-striped table-bordered table-condensed" id="tblInputLingkunganKerja">
        <thead>
            <th>No</th>
            <th>Dengan Lingkungan</th>
            <th>Keterangan</th>
            <th>&nbsp;</th>
        </thead>
        <?php
            echo $this->renderPartial('_addLingkunganKerja',array('modLingkunganKerja'=>$modLingkunganKerja,'modLingkunganKerjas'=>$modLingkunganKerjas,'form'=>$form, 'btnHapus'=>false),true); 
        ?>
            
    </table>
</fieldset>

<!--================================================================================== AKHIR INPUT LINGKUNGAN KERJA===============================-->


<div class="form-actions">
                        <?php if(empty($_GET['id'])){ echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/pelamarT/create'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
     <?php }else {  echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')'));  }?>
     
</div>

<?php $this->endWidget(); ?>
<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('id'=>$model->pelamar_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=570px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printCaraPrint',$js,  CClientScript::POS_HEAD);
?>
<script type="text/javascript">
//========================================================== ADD ROW KEMAMPUAN BAHASA
function addRowBahasa(obj)
{
    if(validasiDetailBahasa()){
        var trAddBahasa=new String(<?php echo CJSON::encode($this->renderPartial('_addBahasa',array('modBahasa'=>$modBahasa,'modBahasas'=>$modBahasas,'form'=>$form,'btnHapus'=>true),true));?>);
        $(obj).parents('table').children('tbody').append(trAddBahasa.replace());
        <?php 
            $attributes = $modBahasa->attributeNames(); 
            foreach($attributes as $i=>$attribute){
                echo "renameInputBahasa('KPKemampuanBahasaR','$attribute');";
            }
        ?>
        renameInputBahasa('KPKemampuanBahasaR','no_urut');
        renameInputBahasa('KPKemampuanBahasaR','bahasa');
        renameInputBahasa('KPKemampuanBahasaR','mengerti_l');
        renameInputBahasa('KPKemampuanBahasaR','berbicara_l');
        renameInputBahasa('KPKemampuanBahasaR','menulis_l');
    }
}

function batalBahasa(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan data ini?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
    }
}
function renameInputBahasa(modelName,attributeName)
{
    var trLength = $('#tblInputBahasa tr').length;
    var i = -1;
    $('#tblInputBahasa tr').each(function(){
        if($(this).has('input[name$="[bahasa]"]').length){
            i++;
            $("#KPKemampuanBahasaR_"+i+"_no_urut").val((i+1));
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        
       
    });
}
//INI UNTUK LINGKUNGAN KERJA
function addRowLingkunganKerja(obj)
{
    if(validasiDetailLingker()){
        var trAddLingkunganKerja=new String(<?php echo CJSON::encode($this->renderPartial('_addLingkunganKerja',array('modLingkunganKerja'=>$modLingkunganKerja,'modLingkunganKerjas'=>$modLingkunganKerjas ,'form'=>$form,'btnHapus'=>true),true));?>);
        $(obj).parents('table').children('tbody').append(trAddLingkunganKerja.replace());
        <?php 
            $attributes = $modLingkunganKerja->attributeNames(); 
            foreach($attributes as $i=>$attribute){
                echo "renameInputLingkunganKerja('LingkungankerjaR','$attribute');";
            }
        ?>
        renameInputLingkunganKerja('LingkungankerjaR','dgnlingkungan_l');
        renameInputLingkunganKerja('LingkungankerjaR','keterangan');
        renameInputLingkunganKerja('LingkungankerjaR','nourut');
    }
}

function batalLingkunganKerja(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan data ini?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        
    }
}
function renameInputLingkunganKerja(modelName,attributeName)
{
    var trLength = $('#tblInputLingkunganKerja tr').length;
    var i = -1;
    $('#tblInputLingkunganKerja tr').each(function(){
        if($(this).has('select[name$="[dgnlingkungan_l]"]').length){
            i++;
            $("#LingkungankerjaR_"+i+"_nourut").val((i+1));
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('textarea[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('textarea[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result)
                .width(130)
                .height(150);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

function validasiPelamar(){
    var required = document.getElementsByClassName("isRequired");
    var jml = required.length;
    var adaKosong = false;
    for(i=0;i<jml;i++){
        if(required[i].value == ""){
            alert('Silahkan isi yang bertanda * pada Data Pelamar');
            adaKosong = true;
            break;
        }
    }
    if(adaKosong)
        return false;
    else{
        if(validasiDetail())
            return true;
        else
            return false;
    }
}
function validasiDetail(){
    if(validasiDetailBahasa() && validasiDetailLingker())
        return true;
    else
        return false
}
function validasiDetailBahasa(){
    var detailReq = document.getElementsByClassName("isDetailReq");
    var jml = detailReq.length;
    var adaKosong = false;
    for(i=0;i<jml;i++){
        if(detailReq[i].value == ""){
            alert('Silahkan lengkapi semua Data Kemampuan Bahasa !');
            adaKosong = true;
            break;
        }
    }
    if(adaKosong)
        return false;
    else
        return true;
}
function validasiDetailLingker(){
    var detailReq = document.getElementsByClassName("isDetailReq2");
    var jml = detailReq.length;
    var adaKosong = false;
    for(i=0;i<jml;i++){
        if(detailReq[i].value == ""){
            alert('Silahkan Isi Field \'Dengan Lingkungan\' ! ');
            adaKosong = true;
            break;
        }
    }
    if(adaKosong)
        return false;
    else
        return true;
}

</script>