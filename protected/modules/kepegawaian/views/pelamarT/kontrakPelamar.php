
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); 
$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Kontrak').' Karyawan ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                $this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert');
?>
<fieldset id="fieldsetPasien">
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary(array($modKontrak,$modPegawai)); ?><br>     
        <legend class="rim">Data Karyawan</legend>
        <table class="table-condensed">
            <tr>
                <td width="50%">
                    <div class="control-group">                        
                        <?php echo $form->labelEx($modPegawai,'nomorindukpegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'nomorindukpegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'jenisidentitas', array('class'=>'control-label')) ?>
                        <div class="controls inline">
                            <?php echo $form->textField($modPegawai,'jenisidentitas',array('readonly'=>true,'class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            <?php //echo $form->dropDownList($modPegawai,'jenisidentitas',  JenisIdentitas::items(),
                                   // array('empty'=>'-- Pilih --', 'class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);",)
                                   // ); ?>
                            <?php echo $form->textField($modPegawai,'noidentitas',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'nama_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls inline">
                            <?php //echo $form->textField($modPegawai,'gelardepan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                            <?php echo $form->dropDownList($modPegawai, 'gelardepan', CHtml::listData($modPegawai->getGelarDepanItems(), 'lookup_name', 'lookup_value'),
                                  array('empty'=>'-Pilih-', 'style'=>"width:60px",'onkeypress'=>"return $(this).focusNextInputField(event);",)); ?>
                            <?php echo $form->textField($modPegawai,'nama_pegawai',array('placeholder'=>'Nama Pegawai','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                            <?php //echo $form->textField($modPegawai,'gelarbelakang',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                            <?php echo $form->dropDownList($modPegawai, 'gelarbelakang', CHtml::listData($modPegawai->getGelarBelakangItems(), 'gelarbelakang_id', 'gelarbelakang_nama'),
                                  array('empty'=>'-Pilih-', 'style'=>"width:90px", 'onkeypress'=>"return $(this).focusNextInputField(event);",)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'nama_keluarga', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'nama_keluarga',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php //echo $form->labelEx($modPegawai,'jeniskelamin', array('class'=>'control-label')) ?>
                        <!--<div class="controls inline">-->
                            <?php //echo $form->textField($modPegawai,'jeniskelamin',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                            <?php echo $form->radioButtonListInlineRow($modPegawai,'jeniskelamin', JenisKelamin::items(),
                                    array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        <!--</div>-->
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'tempatlahir_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'tempatlahir_pegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
                            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'tgl_lahirpegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'tgl_lahirpegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPegawai,
                                                    'attribute'=>'tgl_lahirpegawai',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',                                                        
                                                        'onkeypress'=>"js:function(){getUmur(this);}",
                                                        'onSelect'=>'js:function(){getUmur(this);}',
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'umur_bekerja', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'umur_bekerja',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            <?php
                                $this->widget('CMaskedTextField', array(
                                'model' => $modPegawai,
                                'attribute' => 'umur_bekerja',
                                'mask' => '99 Thn 99 Bln 99 Hr',
                                'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')
                                ));
                                ?>
                            <?php echo $form->error($modPegawai, 'umur'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php //echo $form->labelEx($modPegawai,'statusperkawinan', array('class'=>'control-label')) ?>
                        
                        <!--<div class="controls">-->
                            <?php //echo $form->textField($modPegawai,'statusperkawinan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>      
                            <?php echo $form->dropDownListRow($modPegawai,'statusperkawinan', StatusPerkawinan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        <!--</div>-->
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'tglperkawinan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'tglperkawinan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
//                                    $this->widget('MyDateTimePicker',array(
//                                                    'model'=>$modPegawai,
//                                                    'attribute'=>'tglperkawinan',
//                                                    'mode'=>'date',
//                                                    'options'=> array(
//                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',                                                        
//                                                        'yearRange'=> "-150:+0",
//                                                    ),
//                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                    ),
//                            )); ?>
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'nm_i_s', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'nm_i_s',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200)); ?>
            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'tempatlahir_i_s', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'tempatlahir_i_s',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'tgllahir_i_s', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'tgllahir_i_s',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
//                                    $this->widget('MyDateTimePicker',array(
//                                                    'model'=>$modPegawai,
//                                                    'attribute'=>'tgllahir_i_s',
//                                                    'mode'=>'date',
//                                                    'options'=> array(
//                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',                                                        
//                                                        'yearRange'=> "-150:+0",
//                                                        'onkeypress'=>"js:function(){getUmurIs(this);}",
//                                                        'onSelect'=>'js:function(){getUmurIs(this);}',
//                                                    ),
//                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'style'=>'display:inline;','onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                    ),
//                            )); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'usia_i_s', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'usia_i_s',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            <?php
//                                $this->widget('CMaskedTextField', array(
//                                'model' => $modPegawai,
//                                'attribute' => 'usia_i_s',
//                                'mask' => '99 Thn 99 Bln 99 Hr',
//                                'htmlOptions' => array('onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'getTglLahir(this)')
//                                ));
//                                ?>
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'jmlanak', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'jmlanak',array('style'=>'width:30px;', 'maxLength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'jmltanggungan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'jmltanggungan',array('class'=>'span3', 'style'=>'width:30px;', 'maxLength'=>2,'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'propinsi_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
//                            echo $form->dropDownList($modPegawai, 'propinsi_id', CHtml::listData($modPegawai->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'),
////                                    array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
//                                     array('empty'=>'-- Pilih --', 'onblur'=>"return nextFocus(this,event,'".CHtml::activeId($modPegawai, 'propinsi_id')."','".CHtml::activeId($modPegawai, 'kabupaten_id')."')",
//                                        'ajax'=>array('type'=>'POST',
//                                            'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
//                                            'update'=>'#KPPegawaiM_kabupaten_id',),
//                                            'onchange'=>"clearKabupaten();",
//                                            )); ?>
                            <?php echo $form->dropDownList($modPegawai,'propinsi_id', CHtml::listData($modPegawai->getPropinsiItems(), 'propinsi_id', 'propinsi_nama'), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKabupaten',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kabupaten_id'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'kabupaten_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'kabupaten_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php 
//                            echo $form->dropDownList($modPegawai,'kabupaten_id',CHtml::listData($modPegawai->getKabupatenItems($modPegawai->propinsi_id), 'kabupaten_id', 'kabupaten_nama'),
//                                             array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                                )); 
                            ?>
                            <?php echo $form->dropDownList($modPegawai,'kabupaten_id', array(), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKecamatan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kecamatan_id'))); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'kecamatan_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPegawai,'kecamatan_id', array(), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                    'ajax'=>array('type'=>'POST',
                                                                  'url'=>Yii::app()->createUrl('ActionDynamic/GetKelurahan',array('encode'=>false,'namaModel'=>'KPPegawaiM')),
                                                                  'update'=>'#KPPegawaiM_kelurahan_id'))); ?>
                         
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'kelurahan_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPegawai,'kelurahan_id', array(), 
                                              array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                   )); ?>
                         
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'alamat_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textArea($modPegawai,'alamat_pegawai',array('rows'=>4, 'cols'=>50, 'class'=>'span4', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'rtrw', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'rtrw',array('placeholder'=>'RT/RW','class'=>'span3', 'style'=>'width:50px;', 'maxLength'=>7,'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                            <?php // echo $form->labelEx($modPegawai,'kodepos') ?>
                            <?php // echo $form->textField($modPegawai,'kodepos',array('class'=>'span2', 'style'=>'width:80px;', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>8)); ?>
            
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'alamat_lain', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textArea($modPegawai,'alamat_lain',array('rows'=>4, 'cols'=>50, 'class'=>'span4', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php //echo $form->labelEx($modPegawai,'agama', array('class'=>'control-label')) ?>
                        <!--<div class="controls">-->
                            <?php //echo $form->textField($modPegawai,'agama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                            <?php echo $form->dropDownListRow($modPegawai,'agama', Agama::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                        <!--</div>-->
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'golongandarah', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'golongandarah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>2)); ?>
                            <?php echo $form->dropDownList($modPegawai,'golongandarah', GolonganDarah::items(),  
                                                          array('empty'=>'Pilih', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1')); ?>
                            <?php //echo $form->textField($modPegawai,'rhesus',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php echo $form->radioButtonList($modPegawai,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'statuskepemilikanrumah_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'statusrumah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php 
                                echo $form->radioButtonList($modPegawai,'statuskepemilikanrumah_id', CHtml::listData($modPegawai->getStatuskepemilikanrumahItems(), 'statuskepemilikanrumah_id', 'statuskepemilikanrumah_nama'),  
                                                          array('empty'=>'Pilih', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                
                                </div>
                            </div>
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'jikasewa_rp', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'jikasewa_rp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                        </div>
                    </div>                   -->
                    
                </td>
                <td>
                    <?php if(!empty($modPegawai->photopegawai)){ 
                        $url = Params::urlPelamarPhotoTumbsDirectory();
                        $image = $url."kecil_".$modPegawai->photopegawai;
                     ?>
                    <div class="control-group">
                        <p class="control-label">Photo Karyawan </p> 
                        <div class="controls">
                            <img src="<?php echo $image; ?>"  width="120" height="120" />
                        </div>
                    </div>
                            
                    <?php } else { ?>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'photopegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'photopegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200)); ?>
                            <?php echo CHtml::activeFileField($modPegawai, 'photopegawai'); ?>
                        </div>
                    </div>
                    <?php } ?>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'sign_image', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textArea($modPegawai,'sign_image',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php // echo CHtml::activeFileField($modPegawai, 'sign_image'); ?>
                        </div>
                    </div>-->
<!--                     <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'hobby', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'hobby',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'nomobile_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'nomobile_pegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'notelp_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'notelp_pegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'alamatemail', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'alamatemail',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'socialid', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'socialid',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                        </div>
                    </div> -->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'profilperusahaan_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'profilperusahaan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->dropDownList($modPegawai, 'profilrs_id', CHtml::listData($modPegawai->getRumahSakitItems(), 'profilrs_id', 'nama_rumahsakit'),
                                    array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3'));?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'kelompokpegawai_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPegawai, 'kelompokpegawai_id', CHtml::listData($modPegawai->getKelompokPegawaiItems(), 'kelompokpegawai_id', 'kelompokpegawai_nama'),
                                    array('empty'=>'-- Kelompok Pegawai --','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3'));?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'pangkat_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modPegawai, 'pangkat_id', CHtml::listData($modPegawai->getPangkatItems(), 'pangkat_id', 'pangkat_nama'),
                                    array('empty'=>'-- Pangkat --','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3'));?>
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php //echo $form->labelEx($modPegawai,'departemen_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'departement_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php 
//                                echo $form->dropDownListRow($modPegawai, 'departement_id', CHtml::listData($modPegawai->getDepartementItems(), 'departement_id', 'departement_nama'),
//                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3'));?>
                        </div>
                    </div>-->
                    
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'tglditerima', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'tglditerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPegawai,
                                                    'attribute'=>'tglditerima',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',                                                        
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>    
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'tglberhenti', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'tglberhenti',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPegawai,
                                                    'attribute'=>'tglberhenti',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',                                                        
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>            
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'pendidikan_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'pendidikan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->dropDownList($modPegawai, 'pendidikan_id', CHtml::listData($modPegawai->getPendidikanItems(), 'pendidikan_id', 'pendidikan_nama'),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'pendkualifikasi_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'pendkualifikasi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->dropDownList($modPegawai, 'pendkualifikasi_id', CHtml::listData($modPegawai->getPendKualifikasiItems(), 'pendkualifikasi_id', 'pendkualifikasi_nama'),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'npwp', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'npwp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                            <?php //echo $form->textField($modPegawai,'tglnpwp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            
                        </div>
                    </div>
<!--                   <div class="control-group">                        
                        <?php // echo $form->labelEx($modPegawai,'tglnpwp', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
//                                    $this->widget('MyDateTimePicker',array(
//                                                    'model'=>$modPegawai,
//                                                    'attribute'=>'tglnpwp',
//                                                    'mode'=>'date',
//                                                    'options'=> array(
//                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',                                                        
//                                                        'yearRange'=> "-150:+0",
//                                                    ),
//                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'style'=>'display:inline;','onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                    ),
//                            )); ?>
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'warganegara_pegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'warganegara_pegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                            <?php echo $form->dropDownList($modPegawai, 'warganegara_pegawai', WargaNegara::items(),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'suku_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'suku_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->dropDownList($modPegawai, 'suku_id', CHtml::listData($modPegawai->getSukuItems(), 'suku_id', 'suku_nama'),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'kategoripegawai', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'kategoripegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                            <?php echo $form->dropDownList($modPegawai, 'kategoripegawai', CHtml::listData($modPegawai->Kategoripegawai, 'lookup_name','lookup_name'),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'Status Karyawan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'statuspegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                            <?php echo $form->dropDownList($modPegawai, 'statuspegawai', CHtml::listData($modPegawai->StatusPegawai, 'lookup_name', 'lookup_name'),
                                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
            
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'no_jamsostek', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'no_jamsostek',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'namabank', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'namabank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'norekening', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'norekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'namaakunbank', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->textField($modPegawai,'namaakunbank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
                        </div>
                    </div>-->
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'pphditanggungperusahaan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php // echo $form->checkBox($modPegawai,'pphditanggungperusahaan', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            Ditanggung perusahaan
                        </div>
                    </div>-->
                    <div class="control-group">
                        <?php echo $form->labelEx($modPegawai,'nofingerprint', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($modPegawai,'nofingerprint',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>30)); ?>
            
                        </div>
                    </div>
<!--                    <div class="control-group">
                        <?php // echo $form->labelEx($modPegawai,'tglmulaibekerja', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php //echo $form->textField($modPegawai,'tglmulaibekerja',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php   
//                                    $this->widget('MyDateTimePicker',array(
//                                                    'model'=>$modPegawai,
//                                                    'attribute'=>'tglmulaibekerja',
//                                                    'mode'=>'date',
//                                                    'options'=> array(
//                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',                                                        
//                                                        'yearRange'=> "-150:+0",
//                                                    ),
//                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
//                                                    ),
//                            )); ?>            
                        </div>
                    </div>-->
                </td>
            </tr>               
        </table>
        
         <?php //echo $form->errorSummary($modKontrak); ?><br> 
        <?php echo $this->renderPartial('_formKontrak', array('modPegawai'=>$modPegawai,'modKontrak'=>$modKontrak,'form'=>$form)); ?>               
	<div class="form-actions">
		                <?php if(empty($_GET['idPegawai'])) { echo CHtml::htmlButton($modPegawai->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/pelamarT/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); } ?>
	</div>
</fieldset>
<?php $this->endWidget(); ?>

<?php
if(!empty($_GET['idPelamar'])){
    ?><script type="text/javascript">setTimeout('usia()', 1000);</script><?php
}
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlLamaKontrak = Yii::app()->createUrl('ActionAjax/GetLamaKontrak');
$idTagUmur = CHtml::activeId($modPegawai,'umur');
$js = <<< JS
function usia()
{
    var valll = $('#KPPegawaiM_tgl_lahirpegawai').val(); 
    $.post("${urlGetUmur}",{tglLahir:valll},
        function(data){
           $('#KPPegawaiM_umur_bekerja').val(data.umur);
           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function lamaKontrak()
{
    var tglAwal = $('#KPKontrakKaryawanR_tglmulaikontrak').val(); 
    var tglAkhir = $('#KPKontrakKaryawanR_tglakhirkontrak').val(); 
    if(tglAwal != '' && tglAkhir != ''){
        $.post("${urlLamaKontrak}",{tglAwal:tglAwal, tglAkhir:tglAkhir},
            function(data){
               $('#KPKontrakKaryawanR_lamakontrak').val(data.kontrak);
        },"json");
    }
}
           
function getTglLahir(obj)
{
    var str = obj.value;
    obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{umur: obj.value},
        function(data){
           $('#KPPegawaiM_tgl_lahirpegawai').val(data.tglLahir); 
    },"json");
}    
        
    function getUmur(obj)
{
    //alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){
           $('#KPPegawaiM_umur_bekerja').val(data.umur);
           $("#${idTagUmur}").val(data.umur);
    },"json");
}
//    function getUmurIs(obj)
//{
//    //alert(obj.value);
//    if(obj.value == '')
//        obj.value = 0;
//    $.post("${urlGetUmur}",{tglLahir: obj.value},
//        function(data){
//           $('#KPPegawaiM_usia_i_s').val(data.umur);
//           $("#${idTagUmur}").val(data.umur);
//    },"json");
//} 
function clearKabupaten()
{
    $('#KPPegawaiM_kabupaten_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('form',$js,CClientScript::POS_HEAD);
?>
