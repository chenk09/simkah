<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'kppenggajianpeg-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));
?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'currency'=>'PHP',
    'config'=>array(
//        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>

<!--	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->
<?php
    if(isset($_GET['id'])){
        echo '<div class="alert alert-block alert-success">';
        echo '<a class="close" data-dismiss="alert">×</a>';
        echo 'Data berhasil disimpan';
        echo '</div>';
    }
?>
    <?php $this->renderPartial('_pegawai',array('model'=>$modPegawai, 'form'=>$form)); ?>
	<?php echo $form->errorSummary($model); ?>
    <legend class="rim">Penggajian</legend>
    <table>
	
        <tr>
            <td>
                <?php echo $form->labelEx($model, 'tglpenggajian', array('class' => 'control-label')); ?>
                <div class="controls">
                <?php $this->widget('MyDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'tglpenggajian',
                    'mode'=>'datetime',
                    'options'=> array(
                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                     ),
                )); ?> 
                </div>  
            </td>
            <td>
                <?php echo $form->labelEx($model, 'periodegaji', array('class' => 'control-label')); ?>  
                <div class="controls">
                <?php $this->widget('MyDateTimePicker',array(
                    'model'=>$model,
                    'attribute'=>'periodegaji',
                    'mode'=>'date',
                    'options'=> array(
                        'dateFormat'=> 'MM yy',
                        'changeYear' => true,
                        'changeMonth' => true,
                        'changeDate' => false,
                        'showSecond' => false,
                        'showDate' => false,
                        'showMonth' => false,
                        'onChangeMonthYear'=>'js:function(y, m, i){                                
                                var d = i.selectedDay;
                                $(this).datepicker("setDate", new Date(y, m - 1, d));
                            }'
                    ),
                    'htmlOptions'=>array('readonly'=>true,
                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                        'class'=>'dtPicker3',
                        'onChange'=>'ambilTunjangan()',
                    ),
                )); ?> 
                </div>
            </td>
            <td>
               <?php //echo $form->textFieldRow($model,'harikerja',array('style'=>'width:30px;','readonly'=>true, 'id'=>'harikerja')); ?>
               <div class="control-group">
                    <?php echo CHtml::label('Hari Kerja','harikerja',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'harikerja',array(
							'style'=>'width:40px;',
							'id'=>'harikerja',
							'readonly'=>false
						)) ?>
                        <?php echo ' Cuti '; ?>
                        <?php echo $form->textField($model,'cuti',array(
							'class'=>'potongan_hari',
							'style'=>'width:40px;',
							'id'=>'cuti',
							'readonly'=>false
						)) ?>
                        <?php echo ' Alpha '; ?>
                        <?php echo $form->textField($model,'alpha',array(
							'class'=>'potongan_hari',
							'style'=>'width:40px;',
							'id'=>'alpha',
							'readonly'=>false
						)) ?>
                        <?php echo ' Ijin '; ?>
                        <?php echo $form->textField($model,'ijin',array(
							'class'=>'potongan_hari',
							'style'=>'width:40px;',
							'id'=>'ijin',
							'readonly'=>false
						)) ?>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->textFieldRow($model,'nopenggajian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            </td>
            <td>
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'jmlefektifharikerja',array('id'=>'jmlefektifharikerja', 'onblur'=>'hitungharian()')); ?>
                <?php echo $form->hiddenField($model,'jmlefektifharikerja',array('id'=>'tunjharian')); ?>
            </td>
        </tr>
    </table>
            
    <div class="control-group">    
        <div class="controls">
            <table class='table'>
                <thead>
                    <tr>
                        <th>
                            Deskripsi
                        </th>
                        <th>
                            Penerimaan
                        </th>
                        <th>
                            Potongan
                        </th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $modKomponen = KomponengajiM::model()->findAll('komponengaji_aktif = true ORDER BY nourutgaji'); 
                if (count($modKomponen > 0)) { 
                foreach ($modKomponen as $i=>$v){ 
                    $kenapajak = $v->kenapajak;
                    if($kenapajak==1){
                        $hitung_pph = ",hitungpph()";
                        $pph = "pph";
                    }else{
                        $hitung_pph = "";
                        $pph = "";
                    }
                ?>
                <tr>
                    <td>
                        <?php echo $v->komponengaji_nama; ?>
                    </td>
                    <?php 
                        echo ($v->ispotongan == false) ? "<td>".$form->textField($komponen, "komponengaji_id[".$v->komponengaji_id."]", array('value'=>0,'class'=>'inputFormTabel currency gaji '.$pph.'', 'onblur'=>'setGaji()'.$hitung_pph.';'))."</td><td></td>" : "<td></td><td>".$form->textField($komponen, "komponengaji_id[".$v->komponengaji_id."]", array('class'=>'inputFormTabel currency potongan', 'onblur'=>'setPotongan();', 'onKeypress'=>'return $(this).focusNextInputField(event)', 'value'=>0))."</td>"; 
                    ?>
                </tr>
                <?php } 
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th style="text-align: right">
                            Total
                        </th>
                        <th>
                            <?php echo $form->textField($model,'totalterima',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </th>
                        <th>
                            <?php echo $form->textField($model,'totalpotongan',array('class'=>'span2 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <table>
        <tr>
            <td>
                <?php echo $form->textFieldRow($model,'totalpajak',array('class'=>'span3 currency', 'onblur'=>'setHarga();','onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
                <?php echo $form->textFieldRow($model,'penerimaanbersih',array('class'=>'span3 currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'mengetahui',array('class'=>'control-label')) ?>
                <div class="controls">
                        <?php echo $form->hiddenField($model,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                        <?php $this->widget('MyJuiAutoComplete',array(
                                    'model'=>$model, 
//                                        'name'=>'namapegawai',
                                    'attribute'=>'mengetahui',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                                    'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 4,
                                       'focus'=> 'js:function( event, ui ) {
                                            $("#'.CHtml::activeId($model, 'mengetahui').'").val(ui.item.nama_pegawai);
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) {
                                            $("#'.CHtml::activeId($model, 'mengetahui').'").val(ui.item.nama_pegawai);
                                            return false;
                                        }',

                                    ),
                                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
                                    'tombolDialog'=>array('idDialog'=>'dialogPegawai2','idTombol'=>'tombolPasienDialog'),
                        )); ?>
                    </div>
                </div>
            <?php //echo $form->textFieldRow($model,'menyetujui',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($model, 'menyetujui',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($model,'menyetujui',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                        <?php $this->widget('MyJuiAutoComplete',array(
                                    'model'=>$model, 
//                                        'name'=>'namapegawai',
                                    'attribute'=>'menyetujui',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                                    'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 4,
                                       'focus'=> 'js:function( event, ui ) {
                                            $("#'.CHtml::activeId($model, 'menyetujui').'").val(ui.item.nama_pegawai);
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) {
                                            $("#'.CHtml::activeId($model, 'menyetujui').'").val(ui.item.nama_pegawai);
                                            return false;
                                        }',

                                    ),
                                    'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
                                    'tombolDialog'=>array('idDialog'=>'dialogPegawai3','idTombol'=>'tombolPasienDialog'),
                        )); ?>
                    </div>
                </div>
            </td>
            <td rowspan="2">
                <fieldset id="fieldsetpph21" class="">
                <legend class="accord1"><?php echo CHtml::checkBox('cex_pph21', false, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Perhitungan PPH 21
                </legend>
                <div id='pph21' class="toggle">
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'Gaji dan Tunjangan', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'gajipph',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->hiddenField($model,'persentasepph21',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            <?php echo $form->hiddenField($model,'kodeptkp',array('class'=>'inputFormTabel', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            / Tahun
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'Biaya Jabatan (5%)', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'biayajabatan',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            Maks. 6.000.000 / Tahun
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'Iuran Pensiun', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'iuranpensiun',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            Maks. 2.400.000 / Tahun
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'Penerimaan Bersih', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'penerimaanpph',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            / Tahun
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'PTKP', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'ptkp',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'PKP', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'pkp',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'', array('class'=>'control-label', 'id'=>'label_persen')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'pphpersen',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            / Tahun
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'PPh 21', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'pph21',array('class'=>'inputFormTabel currency', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                            / Bulan
                        </div>
                    </div>
                </div>
                </fieldset>

            </td>    
        </tr>
        <tr>
            <td colspan="2">
                <!-- Form Keterangan -->        
                <?php //echo $form->textAreaRow($model,'keterangan',array('rows'=>3, 'cols'=>50, 'class'=>'span7', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                  <?php echo $form->dropDownListRow($model,'keterangan', CaraBayarKeluar::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
        <?php    if($model->isNewRecord == TRUE )
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'setVerifikasi();return false;','onClick'=>'setVerifikasi();return false;')); 
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>

            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/penggajianpegT/create'), 
                    array('class'=>'btn btn-danger',
                    'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>

            <?php if($model->isNewRecord == FALSE)
                    {
            ?>
                        <script>
                            //print(<?php echo $model->penggajianpeg_id ?>);
                        </script>
            <?php echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->penggajianpeg_id');return false",'disabled'=>FALSE  )); 
                      //   echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->penggajianpeg_id');return false",'disabled'=>FALSE  ));
 }else{
                    echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                   } 
            ?>

                <?php 
                    $content = $this->renderPartial('../tips/transaksi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'create','content'=>$content));       
                ?>
    </div>
<?php $this->endWidget(); ?>

<!-- VERIFIKASI PENGGAJIAN PEGAWAI -->
    <?php
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
            'id'=>'dialogVerifikasi',
            'options'=>array(
                'title'=>'Verifikasi Penggajian',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>850,
                'height'=>550,
                'resizable'=>false,
            ),
        ));
    ?>
    <div id="dataPegawai">
        <legend class=rim>Data Pegawai</legend>
        <table class="table table-bordered  table-condensed">
            <tbody>

            </tbody>
            
        </table>
    </div>
    <div id ="dataRinciangaji">
        <legend class=rim>Rincian Penggajian</legend>
        <table class="table table-bordered  table-condensed">          
            <thead>
                <tr>
                    <th>Deskripsi</th>
                    <th>Penerimaan</th>
                    <th>Potongan</th>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
        <div class="form-actions">
            <?php 
                echo CHtml::link(Yii::t('mds', '{icon} Teruskan', array('{icon}'=>'<i class="icon-ok icon-white"></i>')), 
                    '#', array('class'=>'btn btn-primary','onclick'=>'setKonfirmasi(this);','disabled'=>FALSE  )); 
            ?>
            <?php 
                echo CHtml::link(Yii::t('mds', '{icon} Kembali', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                    '#', array('class'=>'btn btn-blue','onclick'=>'kembaliForm();','disabled'=>FALSE)); 
            ?>
        </div>
    </div>
    <!-- END FORM VERIFIKASI PENGGAJIAN PEGAWAI -->
    <?php $this->endWidget(); ?>

<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php Yii::app()->clientScript->registerScript('onheadfunction','
    function setGaji(){
        setTunjanganHarian();
        setHarga();
        var totalGaji = 0;
        $(".gaji").each(function(){
            value = unformatNumber($(this).val());
            if (value > 0){
                totalGaji += value;
            }
        });
        // var sosial = unformatNumber($("#PenggajiankompT_komponengaji_id_20").val()) + unformatNumber($("#PenggajiankompT_komponengaji_id_17").val()) + unformatNumber($("#PenggajiankompT_komponengaji_id_18").val()) + unformatNumber($("#PenggajiankompT_komponengaji_id_19").val());

        // $("#PenggajiankompT_komponengaji_id_14").val(formatDesimal(sosial));
        $("#'.CHtml::activeId($model, 'totalterima').'").val(formatDesimal(totalGaji));
        
    }

    function setPotongan(){
        var totalPotongan = 0;
        $(".potongan").each(function(){
        value = unformatNumber($(this).val());
            if (jQuery.isNumeric(value)){
                totalPotongan += value;
            }
        });
        $("#'.CHtml::activeId($model, 'totalpotongan').'").val(formatDesimal(totalPotongan));
        setHarga();
    }
    function setHarga(){
        var pajak = unformatNumber($("#'.CHtml::activeId($model, 'totalpajak').'").val());
        var gaji = unformatNumber($("#'.CHtml::activeId($model, 'totalterima').'").val());
        var potongan = unformatNumber($("#'.CHtml::activeId($model, 'totalpotongan').'").val());
        value = gaji-potongan;
            
        if (jQuery.isNumeric(value)){
            $("#'.CHtml::activeId($model, 'penerimaanbersih').'").val(formatDesimal(value));
        }
    }

',  CClientScript::POS_HEAD); ?>

<?php Yii::app()->clientScript->registerScript('pph21',"
    $('#pph21').hide();
    $('#cex_pph21').change(function(){
        $('#pph21').slideToggle(500);
    });
");
?>

<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai2',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new KPRegistrasifingerprint();
if (isset($_GET['KPRegistrasifingerprint']))
    $modPegawai->attributes = $_GET['KPRegistrasifingerprint'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai4-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                        "id" => "selectPegawai",
                                        "href"=>"",
                                        "onClick" => "
                                                      $(\"#'.CHtml::activeId($model, 'mengetahui').'\").val(\"$data->nama_pegawai\");
                                                      $(\"#dialogPegawai2\").dialog(\"close\");    
                                                      return false;
                                            "))',
                    ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'jabatan.jabatan_nama',
                'alamat_pegawai',
            ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai3',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new KPRegistrasifingerprint();
if (isset($_GET['KPRegistrasifingerprint']))
    $modPegawai->attributes = $_GET['KPRegistrasifingerprint'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai5-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                        "id" => "selectPegawai",
                                        "href"=>"",
                                        "onClick" => "
                                                      $(\"#'.CHtml::activeId($model, 'menyetujui').'\").val(\"$data->nama_pegawai\");
                                                      $(\"#dialogPegawai3\").dialog(\"close\");    
                                                      return false;
                                            "))',
                    ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'jabatan.jabatan_nama',
                'alamat_pegawai',
            ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id . '/' .$controller;
$urlKartu =  Yii::app()->createAbsoluteUrl($module);
$urlPrintgaji = Yii::app()->createUrl('print/kartuPasien',array('penggajianpeg_id'=>''));

?>
<script type="text/javascript">
    $('.currency').each(function(){this.value = formatDesimal(this.value)});
    function ambilTunjangan()
    {
        var pegawai_id  = $('#pegawai_id').val();
        var periode     = $('#KPPenggajianpegT_periodegaji').val();
        var pangkat_id  = $('#pangkat_id').val();
        var jabatan_id  = $('#jabatan_id').val();
        var kelompokpegawai_id  = $('#kelompokpegawai_id').val();
        var komponen_id = <?php echo Params::ID_TUNJANGAN_HARIAN; ?>;
        if(pegawai_id==null || pegawai_id==''){
            alert("Nama Karyawan belum dipilih...!");
            $('#KPPegawaiM_nama_pegawai').focus();
        }else{
            $.post('<?php echo Yii::app()->createUrl('kepegawaian/PenggajianpegT/AmbilTunjangan');?>', {pegawai_id:pegawai_id, periode:periode, jabatan_id:jabatan_id, komponen_id:komponen_id, kelompokpegawai_id:kelompokpegawai_id}, function(data){
                    // alert(data.tunjanganharian);
                    $('#PenggajiankompT_komponengaji_id_10').val(formatDesimal(data.tunjanganharian));
                    var tm_krj = data.tunjanganharian-data.jmltunjangan;

                    pot_ijin = data.jmlijin * (0.10 * data.tunjanganharian);
                    pot_alpha = data.jmlalpha * (0.30 * data.tunjanganharian);
                    total_pot = parseFloat(pot_ijin) + parseFloat(pot_alpha);
                    if(total_pot >= data.tunjanganharian){
                        tm_krj = data.tunjanganharian;
                    }else{
                        tm_krj = total_pot;
                    }
                    // tm_krj = Math.round(tm_krj*Math.pow(10,-2))/Math.pow(10,-2);
                    $('#PenggajiankompT_komponengaji_id_34').val(formatDesimal(tm_krj));

                    $('#harikerja').val(data.jmlhadir);
                    $('#cuti').val(data.jmlcuti);
                    $('#alpha').val(data.jmlalpha);
                    $('#ijin').val(data.jmlijin);

                    $('#jmlefektifharikerja').val(data.jml_hari); 
                    $('#tunjharian').val(data.tunjanganharian);
                    setTunjanganHarian();
            }, 'json');
        }
    }
    
    function print(penggajianpeg_id)
{       
        //if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('index.php?r=kepegawaian/penggajianpegT/printrincian&id='+penggajianpeg_id,'printwindows','left=200,top=600,width=1010,height=900,resizable=true');
            var urlKartu = 'index.php?r=kepegawaian/penggajianpegT/printrincian&id='+penggajianpeg_id;
            $.post(urlKartu, {penggajianpeg_id: penggajianpeg_id}, "json");

        //}
}

    function hitungpph()
    {
        var totalgajipph = 0;
        $(".pph").each(function(){
            value = unformatNumber($(this).val());
            if (value > 0){
                totalgajipph += value;
            }
        });
        totalgajipphtahun = totalgajipph * 12;
        $("#KPPenggajianpegT_gajipph").val(formatDesimal(totalgajipphtahun));

        var biayajabatan = 0.05 * totalgajipphtahun;
        if(biayajabatan>=6000000){
            biayajabatan=6000000;
        }
        $("#KPPenggajianpegT_biayajabatan").val(formatDesimal(biayajabatan));
        $("#KPPenggajianpegT_iuranpensiun").val(formatDesimal(2400000));
        var penerimaanbersih = totalgajipphtahun - biayajabatan - unformatNumber($("#KPPenggajianpegT_iuranpensiun").val());
        $("#KPPenggajianpegT_penerimaanpph").val(formatDesimal(penerimaanbersih));

        var ptkp = unformatNumber($('#KPPenggajianpegT_ptkp').val());
        var pkp  = penerimaanbersih - ptkp;
        if(pkp<=0)
            pkp = 0;
        $("#KPPenggajianpegT_pkp").val(formatDesimal(pkp));
        $.post('<?php echo Yii::app()->createUrl('kepegawaian/PenggajianpegT/ambilpph');?>', {pkp:pkp}, function(data){
                // $('#PenggajiankompT_komponengaji_id_10').val(data.jmltunjangan);
                // $('#harikerja').val(data.jmlhadir);
                // setTunjanganHarian();
                var persen = data.percent/100;
                var persenpertahun = persen * pkp;
                var persenperbulan = persenpertahun / 12;
                var pembulatan = Math.round(persenperbulan*Math.pow(10,0))/Math.pow(10,0);
                $('#KPPenggajianpegT_pphpersen').val(formatDesimal(persenpertahun));
                $('#KPPenggajianpegT_pph21').val(formatDesimal(persenperbulan));
                $('#PenggajiankompT_komponengaji_id_15').val(formatDesimal(pembulatan));
                $('#PenggajiankompT_komponengaji_id_35').val(formatDesimal(pembulatan)); 
                $('#KPPenggajianpegT_totalpajak').val(formatDesimal(pembulatan));

                $("#label_persen").html('PPh ('+data.percent+' %)');
                $('#KPPenggajianpegT_persentasepph21').val(data.percent);
                var statuskawin = $('#statusperkawinan').val();
                if(statuskawin=='KAWIN'){
                    var kodekawin = 'K';
                }else{
                    var kodekawin = 'TK';
                }
                var jmlanak = $('#jml_tanggungan').val();
                if(jmlanak > 3){
                    jmlanak = 3;
                }
                var kdptkp = kodekawin+"/"+jmlanak;
                $('#KPPenggajianpegT_kodeptkp').val(kdptkp);
            }, 'json');

        setPotongan();
        setHarga();
        
    }

    function setTunjanganHarian()
    {
        var gapok   = unformatNumber($("#PenggajiankompT_komponengaji_id_9").val());
        var harian  = unformatNumber($("#PenggajiankompT_komponengaji_id_10").val());
        var jabatan = unformatNumber($("#PenggajiankompT_komponengaji_id_11").val());
        var fungsi  = unformatNumber($("#PenggajiankompT_komponengaji_id_12").val());
        var khusus  = unformatNumber($("#PenggajiankompT_komponengaji_id_13").val());
        var kompetensi = unformatNumber($("#PenggajiankompT_komponengaji_id_19").val());

        tunjSosial     = (gapok + harian + jabatan + fungsi + khusus + kompetensi) * (4.54/100);
        tunjjamsostek  = (gapok + harian + jabatan + fungsi + khusus + kompetensi) * (6.54/100);
        var pembulatan_sosial = Math.round(tunjSosial*Math.pow(10,0))/Math.pow(10,0);
        var pembulatan_jamsos = Math.round(tunjjamsostek*Math.pow(10,0))/Math.pow(10,0);

        if (jQuery.isNumeric(tunjSosial)){
            $("#PenggajiankompT_komponengaji_id_14").val(formatDesimal(pembulatan_sosial));
        }

        if (jQuery.isNumeric(tunjjamsostek)){
            $("#PenggajiankompT_komponengaji_id_25").val(formatDesimal(pembulatan_jamsos));
        }

        hitungpph();
        setPotongan();

    }

    function hitungharian()
    {
        var tunjharian  = $('#tunjharian').val();
        var harikerja   = $('#harikerja').val();
        var cuti        = $('#cuti').val();
        var alpha       = $('#alpha').val();
        var ijin        = $('#ijin').val();
        var jmlefektifharikerja = $('#jmlefektifharikerja').val(); 

        var tunjangan = ( harikerja / jmlefektifharikerja) * tunjharian ;
        var pembulatan = Math.round(tunjangan*Math.pow(10,-2))/Math.pow(10,-2);
        $('#PenggajiankompT_komponengaji_id_10').val(formatDesimal(tunjharian));

        var tm_krj = tunjharian - tunjangan;
        if(tm_krj < 0){
            tm_krj = 0;
        }
        pot_ijin = ijin * (0.10 * tunjharian);
        pot_alpha = alpha * (0.30 * tunjharian);
        total_pot = parseFloat(pot_ijin) + parseFloat(pot_alpha);
        if(total_pot >= tunjharian){
            tm_krj = tunjharian;
        }else{
            tm_krj = total_pot;
        }
        // tm_krj = Math.round(tm_krj*Math.pow(10,-2))/Math.pow(10,-2);
        $('#PenggajiankompT_komponengaji_id_34').val(formatDesimal(tm_krj));

        setGaji();
        setPotongan();
    }

    function setVerifikasi()
    {
        var nip             = $('#NIP').val();     
        var nama_pegawai    = $('#KPPegawaiM_nama_pegawai').val();     
        var jabatan         = $('#jabatan').val();     
        var golongan        = $('#golongan').val();     
        var pangkat         = $('#pangkat').val();     
        var statuskawin     = $('#statusperkawinan').val(); 
        var jml_tanggungan  = $('#jml_tanggungan').val(); 
        var statuspegawai   = $('#statuspegawai').val();
        var gapok           = $('#PenggajiankompT_komponengaji_id_9').val();
        var tunjharian      = $('#PenggajiankompT_komponengaji_id_10').val();
        var tunjjabatan     = $('#PenggajiankompT_komponengaji_id_11').val();
        var tunjfunsional   = $('#PenggajiankompT_komponengaji_id_12').val();
        var tunjkhusus      = $('#PenggajiankompT_komponengaji_id_13').val();
        var tunjsosial      = $('#PenggajiankompT_komponengaji_id_14').val();
        var tunjpph         = $('#PenggajiankompT_komponengaji_id_15').val();
        var tunjesensial    = $('#PenggajiankompT_komponengaji_id_16').val();
        var tunjpensiun     = $('#PenggajiankompT_komponengaji_id_17').val();
        var tunjsehat       = $('#PenggajiankompT_komponengaji_id_18').val();
        var tunjkompetensi  = $('#PenggajiankompT_komponengaji_id_19').val();
        var tunjkemahalan   = $('#PenggajiankompT_komponengaji_id_20').val();
        var insentif        = $('#PenggajiankompT_komponengaji_id_21').val();
        var jasalain        = $('#PenggajiankompT_komponengaji_id_22').val();
        var lembur          = $('#PenggajiankompT_komponengaji_id_23').val();
        
        var koperasi        = $('#PenggajiankompT_komponengaji_id_24').val();
        var jamsostek       = $('#PenggajiankompT_komponengaji_id_25').val();
        var pensiun         = $('#PenggajiankompT_komponengaji_id_26').val();
        var kesehatan       = $('#PenggajiankompT_komponengaji_id_27').val();
        var bri             = $('#PenggajiankompT_komponengaji_id_28').val();
        var bsj             = $('#PenggajiankompT_komponengaji_id_29').val();
        var btn             = $('#PenggajiankompT_komponengaji_id_30').val();
        var intern          = $('#PenggajiankompT_komponengaji_id_31').val();
        var apotek          = $('#PenggajiankompT_komponengaji_id_32').val();
        var laboratorium    = $('#PenggajiankompT_komponengaji_id_33').val();
        var krj             = $('#PenggajiankompT_komponengaji_id_34').val();
        var pajak           = $('#PenggajiankompT_komponengaji_id_35').val();
        var lainnya         = $('#PenggajiankompT_komponengaji_id_36').val();

        var totalpenerimaan = $('#KPPenggajianpegT_totalterima').val();
        var totalpotongan   = $('#KPPenggajianpegT_totalpotongan').val();
        
        var penerimaanbersih   = $('#KPPenggajianpegT_penerimaanbersih').val();
        var periode         = $('#KPPenggajianpegT_periodegaji').val();
        var nogaji          = $('#KPPenggajianpegT_nopenggajian').val();
        var harikerja       = $('#harikerja').val();
        var jmlharikerja    = $('#jmlefektifharikerja').val();
        
        
        // for(var i = 9; i <= 23; i++) {
        //     nilai+'_'+i  = $('#PenggajiankompT_komponengaji_id_'+i).val(); 
        // };

        var confirm = $('#dialogVerifikasi').dialog('open');
        if(confirm){
            $('#dataPegawai').find('tbody').empty();
            $('#dataPegawai').find('tbody').append('<tr>\n\
                    <td>NIP </td><td> '+ nip + '</td>\n\
                    <td>Pangkat</td><td> '+ pangkat + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Nama Karyawan </td><td> '+ nama_pegawai +'</td>\n\
                    <td>Jabatan </td><td> ' + jabatan + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Status Perkawinan </td><td> '+ statuskawin +'</td>\n\
                    <td>Golongan </td><td> ' + golongan + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Jumlah Tanggungan </td><td> '+ jml_tanggungan +'</td>\n\
                    <td>Status Karyawan</td><td> ' + statuspegawai + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Periode Penggajian </td><td> '+ periode +'</td>\n\
                    <td>Jumlah Hari Kerja</td><td> ' + harikerja + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>No. Penggajian </td><td> '+ nogaji +'</td>\n\
                    <td>Jml. Efektif Hari Kerja</td><td> ' + jmlharikerja + '</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Penerimaan Bersih </td><td> '+ penerimaanbersih +'</td>\n\
                    <td></td><td></td>\n\
                </tr>\n\
                '
            );
            $('#dataRinciangaji').find('tbody').empty();
            $('#dataRinciangaji').find('tbody').append('\n\
                <tr>\n\
                    <td>Gaji Pokok</td> <td>'+gapok+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Harian</td> <td>'+tunjharian+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Jabatan</td> <td>'+tunjjabatan+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Fungsional</td> <td>'+tunjfunsional+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Khusus</td> <td>'+tunjkhusus+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Sosial</td> <td>'+tunjsosial+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>PPh Pasal 21</td> <td>'+tunjpph+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Essensial</td> <td>'+tunjesensial+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Pensiun</td> <td>'+tunjpensiun+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Kesehatan</td> <td>'+tunjsehat+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Kompetensi</td> <td>'+tunjkompetensi+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Tunjangan Kemahalan</td> <td>'+tunjkemahalan+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Insentif</td> <td>'+insentif+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Jasa Lainnya</td> <td>'+jasalain+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Lembur</td> <td>'+lembur+'</td> <td></td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Koperasi</td> <td></td> <td>'+koperasi+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Jamsostek</td> <td></td> <td>'+jamsostek+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Potongan Pensiun</td> <td></td> <td>'+pensiun+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Potongan Kesehatan</td> <td></td> <td>'+kesehatan+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>BRI</td> <td></td> <td>'+bri+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>BSJ</td> <td></td> <td>'+bsj+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>BTN</td> <td></td> <td>'+btn+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Potongan Intern</td> <td></td> <td>'+intern+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Apotek</td> <td></td> <td>'+apotek+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Laboratorium</td> <td></td> <td>'+laboratorium+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>T.M. KRJ</td> <td></td> <td>'+krj+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Pajak</td> <td></td> <td>'+pajak+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td>Lain-Lain</td> <td></td> <td>'+lainnya+'</td>\n\
                </tr>\n\
                <tr>\n\
                    <td><b>Total</b></td> <td>'+totalpenerimaan+'</td> <td>'+totalpotongan+'</td>\n\
                </tr>\n\
                '
            );
        }else{
            return false;
        }
    }

    function kembaliForm()
    {        
       $('#dialogVerifikasi').dialog('close');
    }

    function setKonfirmasi(obj)
    {
         $('.currency').each(function(){this.value = unformatNumber(this.value)});
        $(obj).attr('disabled',true);
        $(obj).removeAttr('onclick');
        $('#pkppenggajianpeg-t-form').find('.currency').each(function(){
            $(this).val(unformatNumber($(this).val()));
        });

        $('#kppenggajianpeg-t-form').submit();
    }
</script>