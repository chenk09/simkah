<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
    if(isset($update)){
        $readonly = true;
        $dialogPegawai = '';
        if($model->jabatan_id!='' || $model->jabatan_id!=null){
            $modjabatan = $model->jabatan; $namajabatan = 'jabatan_nama';
        }else{
            $modjabatan = $model; $namajabatan = 'jabatan_id';
        }
      if($model->golonganpegawai_id!='' || $model->golonganpegawai_id!=null){
           $modgolongan = $model->golonganpegawai; $namagolongan = 'golonganpegawai_nama';
            $statuspegawai = 'kategoripegawai';
        }else{
              $modgolongan = $model; $namagolongan = 'golonganpegawai_id';
            $statuspegawai = 'kategoripegawai';
        }
            
        if($model->pangkat_id!='' || $model->pangkat_id!=null){
            $modpangkat = $model->pangkat; $namapangkat = 'pangkat_nama';
        }else{
            $modpangkat = $model; $namapangkat = 'pangkat_id';    
        }
        $jml_tanggungan = 'Jumlahtanggungan';

        // echo"<pre>";
        // print_r($model->attributes);
        // exit();

    }else{
        $readonly = false;
        $dialogPegawai = 'dialogPegawai'; 
        $modjabatan = $model; $namajabatan = 'jabatan_id';
        $modpangkat = $model; $namapangkat = 'pangkat_id';
        $statuspegawai = 'kategoripegawai';
        $modgolongan = $model; $namagolongan = 'golonganpegawai_id';
        $jml_tanggungan = 'jml_tanggungan';
    }

?>
<fieldset>
    <legend class="rim">Data Karyawan</legend>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($model,'nomorindukpegawai',array('id'=>'NIP', 'onkeypress'=>"if (event.keyCode == 13){setNip(this);}return $(this).focusNextInputField(event)", 'class'=>'span3', 'readonly'=>$readonly)); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Karyawan','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($model,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                        <?php echo $form->hiddenField($model,'pangkat_id',array('readonly'=>true,'id'=>'pangkat_id')) ?>
                        <?php echo $form->hiddenField($model,'jabatan_id',array('readonly'=>true,'id'=>'jabatan_id')) ?>
                        <?php echo $form->hiddenField($model,'lamakerja',array('readonly'=>true,'id'=>'lamakerja')) ?>
                        <?php echo $form->hiddenField($model,'kelompokpegawai_id',array('readonly'=>true,'id'=>'kelompokpegawai_id')) ?>
                        <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$model, 
							//'name'=>'namapegawai',
                            'attribute'=>'nama_pegawai',
                            'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 4,
                               'focus'=> 'js:function( event, ui ) {
                                    $("#pegawai_id").val( ui.item.value );
                                    $("#namapegawai").val( ui.item.nama_pegawai );
                                    return false;
                                }',
                               'select'=>'js:function( event, ui ) {
                                    $("#pegawai_id").val( ui.item.value );
                                    $("#NIP").val( ui.item.nomorindukpegawai);
                                    $("#tempatlahir_pegawai").val( ui.item.tempatlahir_pegawai);
                                    $("#tgl_lahirpegawai").val( ui.item.tgl_lahirpegawai);
                                    $("#KPPegawaiM_nama_pegawai").val( ui.item.nama_pegawai);
                                    $("#'.CHtml::activeId($model, 'jabatan').'").val(ui.item.jabatan_nama);
                                    $("#jeniskelamin").val( ui.item.jeniskelamin);
                                    $("#golongan").val( ui.item.golonganpegawai_nama);
                                    $("#pangkat").val( ui.item.pangkat_nama);
                                    $("#pangkat_id").val( ui.item.pangkat_id);
                                    $("#jabatan_id").val( ui.item.jabatan_id);
                                    $("#kelompokpegawai_id").val( ui.item.kelompokpegawai_id);
                                    $("#statuspegawai").val( ui.item.kategoripegawai);
                                    $("#statusperkawinan").val( ui.item.statusperkawinan);
                                    $("#jabatan").val( ui.item.jabatan_nama);
                                    $("#alamat_pegawai").val( ui.item.alamat_pegawai);
                                    setJenisKelaminPasien(ui.item.jeniskelamin);
                                    $("#agama").val(ui.item.agama);
                                    $("#jml_tanggungan").val(ui.item.jumlahtanggungan);
                                    $("#KPPegawaiM_addmasakerja").val(ui.item.addmasakerja); 
                                    $("#id_golongan_penggajian").val(ui.item.golonganpegawai_id);
                                    $("#KPPegawaiM_addmasakerja").val(ui.item.addmasakerja);
                                    $("#lamakerja").val(ui.item.LamaKerjaTanpaMasaKerja);
                                    setKomponenGaji(ui.item.gapok,ui.item.tunjanganjabatan, ui.item.tunjanganfungsional, ui.item.ptkp, ui.item.PotonganIntern);
                                    if(ui.item.photopegawai != null){
                                        $("#photo_pasien").attr(\'src\',\''.Params::urlPegawaiTumbsDirectory().'kecil_\'+ui.item.photopegawai);
                                    } else {
                                        $("#photo_pasien").attr(\'src\',\''.Yii::app()->baseUrl.'/data/images/pasien/no_photo.jpeg\');
                                    }
                                    $("#PenggajiankompT_komponengaji_id_9").focus();
                                    return false;
                                }',

                            ),
                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 ','readonly'=>$readonly),
                            'tombolDialog'=>array('idDialog'=>$dialogPegawai,'idTombol'=>'tombolPasienDialog'),
                        )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($model, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
             
                <?php echo $form->radioButtonListInlineRow($model, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputRequire', 'readonly'=>true)); ?>
                <?php echo $form->textFieldRow($modgolongan,$namagolongan,array('readonly'=>true, 'id'=>'golongan')); ?>
                <?php echo $form->textFieldRow($modpangkat,$namapangkat,array('readonly'=>true, 'id'=>'pangkat')); ?>
                <?php echo $form->textFieldRow($modjabatan,$namajabatan,array('readonly'=>true,'id'=>'jabatan')); ?>

                <div class="control-group">
                    <?php echo CHtml::label('Kategori Karyawan','kategoripegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'kategoripegawai',array('readonly'=>true,'id'=>'kategoripegawai')); ?>
                    </div>
                </div>
                
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'norekening',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'norekening',array('readonly'=>true,'class'=>'span2','id'=>'norek')); ?>
                        <?php echo $form->textField($model,'banknorekening',array('readonly'=>true,'class'=>'span1','id'=>'banknorek', 'style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'npwp',array('readonly'=>true,'id'=>'npwp')); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'notelp_pegawai',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,'notelp_pegawai',array('readonly'=>true,'id'=>'notelp', 'class'=>'span2')); ?>
                        <?php echo $form->textField($model,'nomobile_pegawai',array('readonly'=>true,'id'=>'nomobile', 'class'=>'span1', 'style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'agama',array('readonly'=>true,'id'=>'agama')); ?>
                <?php echo $form->textAreaRow($model,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
                <?php echo $form->textFieldRow($model,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'jml_tanggungan',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($model,$jml_tanggungan,array('readonly'=>true,'id'=>'jml_tanggungan')); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($model,'golonganpegawai_id', CHtml::listData($model->getGolonganItems(), 'golonganpegawai_id', 'golonganpegawai_nama'), 
                    array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'id'=>'id_golongan_penggajian', 'onblur'=>'cariGapok()',
                )); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Masa Kerja Awal','addmasakerja',array('class'=>'control-label')); ?>
                    <div class="controls">
                      <?php echo $form->textField($model,'addmasakerja',array('style'=>'width:30px;', 'class'=>'span2 number', 'onblur'=>'cariGapok()')); ?>
                        Tahun
                    </div>
                </div>
            </td>
            <td>
                <?php 
                    if(!empty($model->photopasien)){
                        echo CHtml::image(Params::urlPasienTumbsDirectory().'kecil_'.$model->photopasien, 'photo pegawai', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pegawai', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
</fieldset>
<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new KPRegistrasifingerprint();
if (isset($_GET['KPRegistrasifingerprint']))
    $modPegawai->attributes = $_GET['KPRegistrasifingerprint'];


$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                    "id" => "selectPegawai",
                    "href"=>"",
                    "onClick" => "
                      $(\"#NIP\").val(\"$data->nomorindukpegawai\");
                      $(\"#pegawai_id\").val(\"$data->pegawai_id\");
                      $(\"#pangkat_id\").val(\"$data->pangkat_id\");
                      $(\"#jabatan_id\").val(\"$data->jabatan_id\");
                      $(\"#kelompokpegawai_id\").val(\"$data->kelompokpegawai_id\");
                      $(\"#'.CHtml::activeId($model, 'nama_pegawai').'\").val(\"$data->nama_pegawai\");
                      $(\"#tempatlahir_pegawai\").val(\"$data->tempatlahir_pegawai\");
                      $(\"#tgl_lahirpegawai\").val(\"$data->tgl_lahirpegawai\");
                      // $(\"#jeniskelamin\").val(\"$data->jeniskelamin\");
                      setJenisKelaminPasien(\"$data->jeniskelamin\");

                      $(\"#jabatan\").val(\"". (isset($data->jabatan->jabatan_nama) ? $data->jabatan->jabatan_nama : "") ."\");
                      $(\"#golongan\").val(\"". (isset($data->golonganpegawai->golonganpegawai_nama) ? $data->golonganpegawai->golonganpegawai_nama : "-") ."\");
                      $(\"#pangkat\").val(\"". (isset($data->pangkat->pangkat_nama) ? $data->pangkat->pangkat_nama : "-") ."\");
                      $(\"#statuspegawai\").val(\"". (isset($data->golonganpegawai->golonganpegawai_namalainnya) ? $data->golonganpegawai->golonganpegawai_namalainnya : "-") ."\");

                      setKomponenGaji(\"$data->Gapok\",\"$data->Tunjanganjabatan\", \"$data->Tunjanganfungsional\", \"$data->Ptkp\", \"$data->PotonganIntern\");
                      $(\"#jml_tanggungan\").val(\"$data->Jumlahtanggungan\");
                      $(\"#KPPegawaiM_addmasakerja\").val(\"$data->addmasakerja\");
                      $(\"#lamakerja\").val(\"$data->LamaKerjaTanpaMasaKerja\");
                      $(\"#id_golongan_penggajian\").val(\"$data->golonganpegawai_id\");
                      // $(\"#KPPenggajianpegT_ptkp\").val(\"formatDesimal($data->Ptkp)\");

                      $(\"#norek\").val(\"$data->norekening\");
                      $(\"#banknorek\").val(\"$data->banknorekening\");
                      $(\"#npwp\").val(\"$data->npwp\");
                      $(\"#notelp\").val(\"$data->notelp_pegawai\");
                      $(\"#nomobile\").val(\"$data->nomobile_pegawai\");
                      $(\"#agama\").val(\"$data->agama\");
                      $(\"#statusperkawinan\").val(\"$data->statusperkawinan\");
                      $(\"#alamat_pegawai\").val(\"$data->alamat_pegawai\");
                      if(\"$data->photopegawai\" != \"\"){
                            $(\"#photo_pasien\").attr(\'src\',\"'.Params::urlPegawaiTumbsDirectory().'kecil_$data->photopegawai\");
                      } else {
                            $(\"#photo_pasien\").attr(\'src\',\"'.Yii::app()->baseUrl.'/data/images/pasien/no_photo.jpeg\");
                      }

                      $(\"#dialogPegawai\").dialog(\"close\");   
                      $(\"#PenggajiankompT_komponengaji_id_9\").focus();
                      return false;
                    "))',
            ),
        'nomorindukpegawai',
        'nama_pegawai',
        'tempatlahir_pegawai',
        'tgl_lahirpegawai',
        'jeniskelamin',
        'statusperkawinan',
        'jabatan.jabatan_nama',
        'alamat_pegawai',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php 
$urlNip = Yii::app()->createUrl('actionAjax/getPegawaiFromNip');
$urlTunjangan = Yii::app()->createUrl('actionAjax/ambilTunjangan');
Yii::app()->clientScript->registerScript('onhead2','
    function setNip(obj){
        var value = $(obj).val();
        $.post("'.$urlNip.'",{nip:value},function(hasil){
            $("#pegawai_id").val(hasil.pegawai_id);
            $("#pangkat_id").val(hasil.pangkat_id);
            $("#jabatan_id").val(hasil.jabatan_id);
            $("#kelompokpegawai_id").val(hasil.kelompokpegawai_id);
            $("#NIP").val(hasil.nomorindukpegawai);
            $("#tempatlahir_pegawai").val(hasil.tempatlahir_pegawai);
            $("#tgl_lahirpegawai").val(hasil.tgl_lahirpegawai);
            $("#namapegawai").val(hasil.nama_pegawai);
            $("#'.CHtml::activeId($model, 'jabatan').'").val(hasil.jabatan_nama)
            $("#'.CHtml::activeId($model, 'nama_pegawai').'").val(hasil.nama_pegawai)
            $("#jeniskelamin").val(hasil.jeniskelamin);
            $("#statusperkawinan").val(hasil.statusperkawinan);
            $("#jabatan").val(hasil.jabatan_nama);
            $("#alamat_pegawai").val(hasil.alamat_pegawai);
            if(hasil.photopegawai != null){
                $("#photo_pasien").attr(\'src\',\''.Params::urlPegawaiTumbsDirectory().'kecil_\'+hasil.photopegawai);
            } else {
                $("#photo_pasien").attr(\'src\',\''.Yii::app()->baseUrl.'/data/images/pasien/no_photo.jpeg\');
            }
        }, "json");
    }

    // function ambilTunjangan()
    // {
    //     var tgl = $("#KPPenggajianpegT_tglpenggajian").val();
    //     $.post("'.$urlTunjangan.'",{nip:tgl},function(hasil){
            
    //     }, "json");
    // }

',  CClientScript::POS_HEAD); ?>

<script type="text/javascript">
function setJenisKelaminPasien(jenisKelamin)
{
    $('input[name="KPPegawaiM[jeniskelamin]"]').each(function(){
        if(this.value == jenisKelamin)
            $(this).attr('checked',true);
        }
    );
    $('.jk').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
}

function setKomponenGaji(gapok,tunjJabatan, tunjFungsi, ptkp, potonganIntern)
{
    $('#PenggajiankompT_komponengaji_id_9').val(formatDesimal(gapok));
    $('#PenggajiankompT_komponengaji_id_12').val(formatDesimal(tunjFungsi));
    $('#PenggajiankompT_komponengaji_id_11').val(formatDesimal(tunjJabatan));
    $('#PenggajiankompT_komponengaji_id_31').val(formatDesimal(potonganIntern));
    $("#KPPenggajianpegT_ptkp").val(formatDesimal(ptkp));
    setGaji();
    setPotongan();
    ambilTunjangan();
    // $('#PenggajiankompT_komponengaji_id_9').val(formatUang(gapok));
    // var tgl = $('#KPPenggajianpegT_tglpenggajian').val();
    // alert(tgl);
}

function cariGapok()
{
    var pegawai_id    = $('#pegawai_id').val();
    if(pegawai_id!=null || pegawai_id!=''){
        var masakerjaawal = $('#KPPegawaiM_addmasakerja').val();
        var golongangaji  = $('#id_golongan_penggajian').val();
        var lamakerja     = $('#lamakerja').val();
        //var masakerja     = parseInt(lamakerja) + parseInt(masakerjaawal);
        $.post('<?php echo Yii::app()->createUrl('kepegawaian/PenggajianpegT/ambilGapok');?>', {golongangaji:golongangaji, masakerja:masakerja}, function(data){
                $('#PenggajiankompT_komponengaji_id_9').val(formatDesimal(data.gapok));
            setGaji();
        }, 'json');
        setGaji();
        setPotongan();
        ambilTunjangan();
    }else{
        alert('Pegawai belum dipilih!');
        $('#KPPegawaiM_nama_pegawai').focus();
    }
}

</script>