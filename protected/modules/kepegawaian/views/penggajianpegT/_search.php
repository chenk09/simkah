<legend class="rim">Pencarian</legend>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'kppenggajianpeg-t-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'penggajianpeg_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nomorindukpegawai',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3')); ?>
        
        <?php echo $form->textFieldRow($model,'nopenggajian',array('class'=>'span3')); ?>

    <?php echo $form->labelEx($model, 'periodegaji', array('class' => 'control-label')); ?>  
    <div class="controls">
        <?php $this->widget('MyDateTimePicker',array(
            'model'=>$model,
            'attribute'=>'periodegaji_info',
            'mode'=>'date',
            'options'=> array(
                'dateFormat'=> 'MM yy',
                'changeYear' => true,
                'changeMonth' => true,
                'changeDate' => false,
                'showSecond' => false,
                'showDate' => false,
                'showMonth' => false,
                // 'timeFormat' => 'hh:mm:ss',
            ),
            'htmlOptions'=>array('readonly'=>true,
                'onkeypress'=>"return $(this).focusNextInputField(event)",
                'class'=>'dtPicker3',
            ),
        )); ?> 
    </div>
	<div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            				<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/penggajianpegT/informasi'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
							  	<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
