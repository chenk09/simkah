<fieldset>
    <legend class="rim2">Informasi Penggajian Karyawan</legend>
</fieldset>



<?php
$this->breadcrumbs=array(
	'Kppenggajianpeg Ts'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('kppenggajianpeg-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'kppenggajianpeg-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                   // 'header'=>'NIP',
                    'name'=>'nomorindukpegawai',
                    'value'=>'$data->pegawai->nomorindukpegawai',
                ),
                array(
                  //  'header'=>'Nama Pegawai',
                    'name'=>'nama_pegawai',
                    'value'=>'$data->pegawai->nama_pegawai',
                ),
                 array(
                    'header'=>'Jabatan',
                    'value'=>'(isset($data->pegawai->jabatan->jabatan_nama) ? $data->pegawai->jabatan->jabatan_nama : "-")',
                ),
                array(
                    'header'=>'NPWP',
                    'value'=>'$data->pegawai->npwp',
                ),
                array(
                    'header'=>'Periode Gaji',
                    'value'=>'$data->periode',
                ),
		'tglpenggajian',
		'nopenggajian',
		'keterangan',
		'mengetahui',
                // array(
                //     'header'=>'Detail',
                //     'type'=>'raw',
                //     'value'=>'CHtml::link("<i class=\'icon-user\'></i>",Yii::app()->createUrl(\'kepegawaian/PenggajianpegT/detailPenggajian&id=\'.$data->pegawai_id),array("rel"=>"tooltip","title"=>"Klik untuk Detail Penggajian"))',
                //   'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                //     ),
                array(
                    'header'=>'Ubah Penggajian',
                    'type'=>'raw',
                    'value'=>'(isset($data->pengeluaranumum_id)) ? "-" : CHtml::link("<i class=\'icon-user\'></i>",Yii::app()->createUrl(\'kepegawaian/PenggajianpegT/update&updateid=\'.$data->penggajianpeg_id),array("rel"=>"tooltip","title"=>"Klik untuk Mengubah Rincian Penggajian"))',
                  'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                    ),
            
            array(
                        'header'=>'Rincian Gaji',
                        'type'=>'raw',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("printrincian",array("id"=>$data->penggajianpeg_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianGaji",
                                          "onclick"=>"$(\"#dialogRincianGaji\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Gaji",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
             array(
                        'header'=>'Amplop',
                        'type'=>'raw',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("printamplop",array("id"=>$data->penggajianpeg_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianAmplop",
                                          "onclick"=>"$(\"#dialogAmplop\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Amplop",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
//                
		/*
		'menyetujui',
		'totalterima',
		'totalpajak',
		'totalpotongan',
		'penerimaanbersih',
		*/
//		array(
//                        'header'=>Yii::t('zii','View'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{view}',
//		),
//		array(
//                        'header'=>Yii::t('zii','Update'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{update}',
//                        'buttons'=>array(
//                            'update' => array (
//                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
//                                        ),
//                         ),
//		),
//		array(
//                        'header'=>Yii::t('zii','Delete'),
//			'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{remove} {delete}',
//                        'buttons'=>array(
//                                        'remove' => array (
//                                                'label'=>"<i class='icon-remove'></i>",
//                                                'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
//                                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->penggajianpeg_id"))',
//                                                //'visible'=>'($data->kabupaten_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                                                'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
//                                        ),
//                                        'delete'=> array(
//                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
//                                        ),
//                        )
//		),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php $this->renderPartial('_search',array('model'=>$model)); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianGaji',
    'options'=>array(
        'title'=>'Rincian Gaji',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianGaji" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAmplop',
    'options'=>array(
        'title'=>'Amplop',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianAmplop" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>