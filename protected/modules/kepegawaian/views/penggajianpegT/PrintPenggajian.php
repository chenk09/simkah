<?php 
if($caraPrint=='EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
          header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
          header('Cache-Control: max-age=0');     
    }
    echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));  
?>
<fieldset>
    <legend class="rim2">Informasi Penilaian Pegawai</legend>
</fieldset>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sapegawai-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>


<fieldset>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($modelpegawai,'nomorindukpegawai',array('id'=>'NIP', 'onkeypress'=>"if (event.keyCode == 13){setNip(this);}return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama pegawai','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo $form->hiddenField($modelpegawai,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                            <?php echo $form->textField($modelpegawai,'nama_pegawai',array('readonly'=>true, 'value'=>$namapegawai)); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modelpegawai,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($modelpegawai, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
                <?php echo $form->textFieldRow($modelpegawai, 'jeniskelamin',array('readonly'=>true,'id'=>'jeniskelamin')); ?>
                <?php echo $form->textFieldRow($modelpegawai,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
                <div class="control-group">
                    <?php echo $form->labelEx($model,'no_rekening',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modelpegawai,'no_rekening',array('readonly'=>true,'class'=>'span2','id'=>'norek')); ?>
                        <?php echo $form->textField($modelpegawai,'bank_no_rekening',array('readonly'=>true,'class'=>'span1','id'=>'banknorek', 'style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modelpegawai,'npwp',array('readonly'=>true,'id'=>'npwp')); ?>
                <div class="control-group">
                    <?php echo $form->labelEx($modelpegawai,'notelp_pegawai',array('readonly'=>true,'class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modelpegawai,'notelp_pegawai',array('readonly'=>true,'id'=>'notelp', 'class'=>'span2')); ?>
                        <?php echo $form->textField($modelpegawai,'nomobile_pegawai',array('readonly'=>true,'id'=>'nomobile', 'class'=>'span1', 'style'=>'width:70px;')); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modelpegawai,'agama',array('readonly'=>true,'id'=>'agama')); ?>
                <?php echo $form->textAreaRow($modelpegawai,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
            </td>
            <td>
                <?php 
                    if(!empty($modelpegawai->photopegawai)){
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'kecil_'.$modelpegawai->photopegawai, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPegawaiDirectory().'no_photo.jpeg', 'Photo Pegawai', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
</fieldset>
    <legend> Data Penggajian Pegawai </legend>
    <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglpenggajian', array('class' => 'control-label')); ?>
                <div class="controls">
                <?php echo $form->textField($model,'tglpenggajian',array('class'=>'control-label')); ?> 
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'nopenggajian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span5', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'mengetahui',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            
            <div class="control-group">
            
                    <div class="controls">
            <table class='table'>
                <thead>
                    <tr>
                        <th>
                            Deskripsi
                        </th>
                        <th>
                            Gaji
                        </th>
                        <th>
                            Potongan
                        </th>
                    </tr>
                </thead>
                </tbody>
               
                <tbody>
                <tfoot>
                    <tr>
                        <th style="text-align: right">
                            Total
                        </th>
                        <th>
                            <?php echo $form->textField($model,'totalterima',array('class'=>'span2', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </th>
                        <th>
                            <?php echo $form->textField($model,'totalpotongan',array('class'=>'span2', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </th>
                    </tr>
                </tfoot>
            </table>
                        </div>
                </div>
            
            <?php echo $form->textFieldRow($model,'totalpajak',array('class'=>'span3 numbersOnly', 'onblur'=>'setHarga();','onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            <?php echo $form->textFieldRow($model,'penerimaanbersih',array('class'=>'span3 numbersOnly', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'mengetahui',array('class'=>'control-label')) ?>
                <div class="controls">
                        <?php echo $form->hiddenField($model,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                        <?php echo $form->textField($model,'mengetahui',array('readonly'=>true, 'value'=>$namapegawai)); ?>
                </div>
            </div>
            <?php //echo $form->textFieldRow($model,'menyetujui',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <div class="control-group">
                <?php echo $form->labelEx($model, 'menyetujui',array('class'=>'control-label')) ?>
                <div class="controls">
                        <?php echo $form->hiddenField($model,'menyetujui',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                        <?php echo $form->textField($model,'menyetujui',array('readonly'=>true, 'value'=>$namapegawai)); ?>
                </div>
            </div>
	
<?php $this->endWidget(); ?>
    