<?php
$this->breadcrumbs=array(
	'PTKP Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
    (Yii::app()->user->checkAccess('Admin')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' PTKP ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
    (Yii::app()->user->checkAccess('Create')) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PTKP ', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                        
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('ptkp-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
$this->renderPartial('_tabMenu',array());
$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ptkp-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
            'name'=>'ptkp_id',
            'value'=>'$data->ptkp_id',
            'filter'=>false,
        ),
		'tglberlaku',
                                    'kodeptkp',       
		'statusperkawinan',
		'jmltanggunan',
		'wajibpajak_thn',
        'wajibpajak_bln', 
        array(
            'header'=>'Berlaku',
            'class'=>'CCheckBoxColumn',
            'id'=>'rows',
            'selectableRows'=>0,
            'checked'=>'$data->berlaku',
        ),
		array(
            'header'=>Yii::t('zii','View'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{view}',
		),
		array(
            'header'=>Yii::t('zii','Update'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{update}',
            'buttons'=>array(
                'update' => array (
                  'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
                ),
            ),
		),
		array(
            'header'=>Yii::t('zii','Delete'),
			'class'=>'bootstrap.widgets.BootButtonColumn',
            'template'=>'{remove} {delete}',
            'buttons'=>array(
                'remove' => array (
                    'label'=>"<i class='icon-remove'></i>",
                    'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
                    'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->ptkp_id"))',
                    'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
                ),
                'delete'=> array(
                    'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
                ),
            )
		),
	),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
    echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
    echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
    $content = $this->renderPartial('../tips/master',array(),true); 
    $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#ptkp-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>