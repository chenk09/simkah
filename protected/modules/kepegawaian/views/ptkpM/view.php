<?php
$this->breadcrumbs=array(
	'PTKP Ms'=>array('index'),
	$model->ptkp_id,
);

$arrMenu = array();
array_push($arrMenu,array('label'=>Yii::t('mds','View').'  PTKP  ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
(Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').'  PTKP ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'ptkp_id',
		'tglberlaku',
                                    'kodeptkp',
		'statusperkawinan',
		'jmltanggunan',
		'wajibpajak_thn',
		'wajibpajak_bln',
        array(
            'label'=>'Berlaku',
            'value'=>(($model->berlaku==1)? "Ya" : "Tidak"),
        ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>