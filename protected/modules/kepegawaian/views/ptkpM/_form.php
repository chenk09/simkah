<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'ptkp-m-form',
	'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
    'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

    <div class="control-group ">
        <?php echo $form->labelEx($model, 'tglberlaku', array('class' => 'control-label')); ?>
        <div class="controls">
            <?php $this->widget('MyDateTimePicker',array(
                'model'=>$model,
                'attribute'=>'tglberlaku',
                'mode'=>'date',
                'options'=> array(
                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                ),
                'htmlOptions'=>array('readonly'=>true,
                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                    'class'=>'dtPicker3',
                ),
            )); ?> 
        </div>
    </div>
        <?php echo $form->textFieldRow($model,'kodeptkp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
    <?php echo $form->textFieldRow($model,'jmltanggunan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
    <?php echo $form->textFieldRow($model,'wajibpajak_thn',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
    <?php echo $form->textFieldRow($model,'wajibpajak_bln',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
    <?php echo $form->dropDownListRow($model, 'statusperkawinan', StatusPerkawinan::items(), array('onkeypress' => "return $(this).focusNextInputField(event);", 'prompt' => '-- Pilih --')); ?>
    <?php echo $form->checkboxRow($model, 'berlaku', array('onkeypress' => "return $(this).focusNextInputField(event);")); ?>

	<div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
        Yii::app()->createUrl($this->module->id.'/'.PtkpM.'/admin'), 
        array('class'=>'btn btn-danger',
        'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>

<?php $this->endWidget(); ?>
