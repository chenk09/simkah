<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'nilai-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'nilai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nilai_angkamin',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'nilai_angkamaks',array('class'=>'span3')); ?>

	<?php echo $form->textFieldRow($model,'nilai_sebutan',array('class'=>'span3','maxlength'=>15)); ?>

	<?php echo $form->checkBoxRow($model,'nilai_aktif',array('checked'=>true)); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
