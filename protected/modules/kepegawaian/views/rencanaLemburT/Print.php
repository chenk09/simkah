
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'rencanalembur_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->rencanalembur_id',
                ),
		'realisasilembur_id',
		'karyawan_id',
		'tglrencana',
		'norencana',
		'nourut',
		/*
		'alasanlembur',
		'tglmulai',
		'tglselesai',
		'keterangan',
		'pemberitugas_id',
		'mengetahui_id',
		'menyetujui_id',
		'create_time',
		'create_user',
		*/
 
        ),
    )); 
?>