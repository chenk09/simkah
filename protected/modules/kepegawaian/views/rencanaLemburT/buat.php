
<?php
/**
 * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
 */
$this->breadcrumbs=array(
	'Rencana Lembur'=>array('index'),
	'Create',
);

$arrMenu = array();
                 (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Buat Rencana Lembur ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Rencana Lembur ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' KaryawanM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Rencana Lembur', 'icon'=>'folder-open', 'url'=>array('Admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_form', array('modRencanaLembur'=>$modRencanaLembur)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'create'));?>