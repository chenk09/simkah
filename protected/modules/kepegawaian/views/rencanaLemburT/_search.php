<?php
/**
 * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
 */
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rencana-lembur-t-search',
        'type'=>'horizontal',
)); ?>

	<div class="control-group ">
                        <?php echo $form->labelEx($modRencanaLembur,'tglAwal', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php   
                                        $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modRencanaLembur,
                                                        'attribute'=>'tgl_awal',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                )); ?>
                            </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modRencanaLembur,'tglAkhir', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php   
                                        $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modRencanaLembur,
                                                        'attribute'=>'tgl_akhir',
                                                        'mode'=>'datetime',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_TIME_FORMAT,
                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                )); ?>
                            </div>
                    </div> 

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
