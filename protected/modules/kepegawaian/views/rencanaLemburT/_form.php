
<?php 
/**
 * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
 */
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rencana-lembur-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onSubmit'=>'return cekValidasi();'),
        'focus'=>'#',
)); ?>
<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
<?php echo $form->errorSummary($modRencanaLembur); ?>
<?php if (isset($modRealisasiLembur)) echo $form->errorSummary($modRealisasiLembur); ?>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur,'tglrencana', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modRencanaLembur,
                                                    'attribute'=>'tglrencana',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        //'maxDate' => 'd',                                                       
                                                    ),
                                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                     )); ?>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur,'norencana', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modRencanaLembur,'norencana',array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
            
                </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur,'mengetahui_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur,'mengetahui_id');?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'mengetahui_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/Mengetahui'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_mengetahui_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogMengetahui'),
                                            'htmlOptions'=>array('onkeypress'=>'return $(this).focusNextInputField(event)','class'=>'span3','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
            <div class="control-group">
                <?php //echo $form->textFieldRow($modRencanaLembur,'menyetujui_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->labelEx($modRencanaLembur,'menyetujui_id', array('class'=>'control-label')) ?>
                <?php echo $form->labelEx($modRencanaLembur,'menyetujui_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur, 'menyetujui_id');?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'menyetujui_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/Menyetujui'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_menyetujui_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogMenyetujui'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
        </td>
    </tr>
</table>
<table class="table table-bordered table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur,'nama_pegawai', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::hiddenField('idPegawaiLembur');?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'karlembur_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/PegawaiLembur'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#idPegawaiLembur").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogPegawaiLembur'),
                                            'htmlOptions'=>array('onkeypress'=>"if(event.keyCode == 13){submitPegawaiLembur();} return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur,'rencana_nip', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modRencanaLembur, 'rencana_nip', array('class'=>'span3','onkeypress'=>"if(event.keyCode == 13){submitPegawaiLembur();} return $(this).focusNextInputField(event);" )); ?>
                    <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'submitPegawaiLembur();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"if(event.keyCode == 13){submitPegawaiLembur();} return $(this).focusNextInputField(event);",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik Untuk Menambahkan Pegawai Lembur",
                                      
                                    )); ?>
                </div>
            </div>
        </td>
    </tr>
</table>

<?php if (isset($modDetails)){
            echo $form->errorSummary($modDetails); }?>
<table id="tabelPegawaiLembur" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th style="text-align: center;">No.</th>
        <th style="text-align: center;">No. Induk Karyawan</th>
        <th style="text-align: center;">Nama Karyawan</th>
        <!--<th style="text-align: center;">Jabatan</th>-->
        <th style="text-align: center;">Jam Mulai</th>
        <th style="text-align: center;">Jam Selesai</th>
        <th style="text-align: center;">Alasan Lembur</th>
        <th style="text-align: center;">Batal</th>
        
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>
<table class="table-condensed">
    <tr>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur, 'keterangan', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textArea($modRencanaLembur, 'keterangan', array('onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                </div>
            </div>
        </td>
        <td width="50%">
            <div class="control-group">
                <?php echo $form->labelEx($modRencanaLembur, 'pemberitugas_id', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modRencanaLembur,'pemberitugas_id');?>
                                <div style="float:left;">
                                <?php $this->widget('MyJuiAutoComplete',array(
                                            'model'=>$modRencanaLembur,
                                            'attribute'=>'pemberitugas_nama',
                                            'sourceUrl'=> Yii::app()->createUrl('kepegawaian/ActionAutoCompleteKP/PemberiTugas'),
                                            'options'=>array(
                                               'showAnim'=>'fold',
                                               'minLength' => 2,
                                               'select'=>'js:function( event, ui ) {
                                                          $("#KPRencanaLemburT_pemberitugas_id").val(ui.item.pegawai_id);
                                                }',
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogPemberiTugas'),
                                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;'),
                                )); ?>
                                </div>
                </div>
            </div>
        </td>
    </tr>    

</table>
<div class="form-actions">
    <?php echo CHtml::htmlButton($modRencanaLembur->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
            Yii::app()->createUrl($this->module->id.'/rencanaLemburT/buat'), 
            array('class'=>'btn btn-danger',
                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
</div>

<?php $this->endWidget(); ?>


<?php
$urlGetPegawaiLembur = Yii::app()->createUrl('kepegawaian/actionAjaxKP/getPegawaiLembur');
$karlembur_nama=CHtml::activeId($modRencanaLembur,'karlembur_nama');
$karlembur_nomorindukpegawai=CHtml::activeId($modRencanaLembur,'rencana_nip');
$jscript = <<< JS
var nomorindukpegawaiPegawaiLembur;
function submitPegawaiLembur()
{
      idPegawaiLembur = $('#idPegawaiLembur').val();      
      nomorindukpegawaiPegawaiLembur = $('#KPRencanaLemburT_rencana_nip').val();
      if((nomorindukpegawaiPegawaiLembur.length==0)&&(idPegawaiLembur.length==0)){
        alert('Silahkan isi field Nama atau No. Induk Karyawan Lembur !');
      }else{
        karlemburNama = $("#tabelPegawaiLembur tbody").find(".karlemburNama[value="+idPegawaiLembur+"]");
        karlemburNik = $("#tabelPegawaiLembur tbody").find(".karlemburNik[value="+nomorindukpegawaiPegawaiLembur+"]");
        jumlahNama =  karlemburNama.length;
        jumlahNik =  karlemburNik.length;
        
        if (jumlahNama == 0){
            $.post("${urlGetPegawaiLembur}", { idPegawaiLembur: idPegawaiLembur, nomorindukpegawaiPegawaiLembur: nomorindukpegawaiPegawaiLembur},
            function(data){
                $('#tabelPegawaiLembur tbody').append(data.tr);
                hitungSemua();
            }, "json");
        }else{
            alert ('Pegawai ini sudah diinputkan !');
        }        
        $('#${karlembur_nama}').val('');
        $('#idPegawaiLembur').val('');
        $('#${karlembur_nomorindukpegawai}').val('');
        $('#nomorindukpegawaiPegawaiLembur').val('');
        
    }              
}

function hitungSemua()
{
     noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut++;
     });

}
        
function remove (obj) {
    $(obj).parents('tr').remove();
    hitungSemua();
}
        
function cekValidasi(event)
{ 
  tglPegawaiLembur = '';
  karlemburTglMulai = $("#tabelPegawaiLembur tbody").find(".detailRequired[value="+tglPegawaiLembur+"]");
  jumlah =  karlemburTglMulai.length;      
  if ($('.isRequired').val()==''){
    alert ('Silahkan Isi No. Perencanaan');
    return false;
  }else  
      if (jumlah==0){        
        $('#btn_simpan').click();
        return true;        
  }else{
     alert ('Jam Mulai Tidak Boleh Kosong!');
     return false;
  }
}
// Original JavaScript code by Chirp Internet: www.chirp.com.au 
// Please acknowledge use of this code by including this header. 
    function checkTime(field) { 
        var errorMsg = ""; 
        // regular expression to match required time format 
        re = /^(\d{1,2}):(\d{2})(:00)?([ap]m)?$/; 
        if(field.value != '') { 
            if(regs = field.value.match(re)) { 
                 
                // 24-hour time format 
                if(regs[1] > 23) { 
                    errorMsg = "Kesalahan format jam : " + regs[1] + ". Masukan jam antara 00 s.d 23 !"; 
                } 
                 
                if(!errorMsg && regs[2] > 59) { 
                    errorMsg = "Kesalahan format menit: " + regs[2] + ". Masukan menit antara 00 s.d 59 !"; 
                } 
            } else { 
                errorMsg = "Kesalahan format waktu: " + field.value + ". Masukan jam dan waktu antara 00:00 s.d 23:59 !"; 
            } 
       } 
       if(errorMsg != "") { 
           alert(errorMsg);
           field.value = "";
           field.focus();
           return false; 
       } 
       return true; 
}
        
        
//function validasiSelisihJam(jm,js){
//       jm = jm.split(/:/);
//       js = js.split(/:/);
//       
//       if (jm[0] < js[0] || (jm[0] == js[0] && jm[1] < js[1])){
//            return true;
//        }else{
//            alert('Jam Selesai harus lebih besar dari Jam Mulai!');
//            
//            return false;
//        }
//    }
JS;
Yii::app()->clientScript->registerScript('inputPegawai',$jscript, CClientScript::POS_HEAD);

?>

<?php 
//========= Dialog buat cari data Pegawai Mengetahui =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMengetahui',
    'options'=>array(
        'title'=>'Pencarian Mengetahui Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modMengetahui = new PegawaiM('search');
$modMengetahui -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modMengetahui->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'mengetahui-m-grid',
	'dataProvider'=>$modMengetahui->search(),
	'filter'=>$modMengetahui,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                        "id" => "selectMengetahui",
                                        "onClick" => "$(\"#KPRencanaLemburT_mengetahui_id\").val(\"$data->pegawai_id\");
                                                      $(\"#'.CHtml::activeId($modRencanaLembur,'mengetahui_nama').'\").val(\"$data->nama_pegawai\");
                                                      $(\"#dialogMengetahui\").dialog(\"close\");    
                                                      return false;
                                            "))',
                    ),
                    array(
                        'header'=>'No.',
                        'type'=>'raw',
                        'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                        'filter'=>false,
                    ),
                    'nomorindukpegawai',                
                    'nama_pegawai',
//                    array(
//                        'header'=>'Jabatan',
//                        'value'=>'$data->jabatan->jabatan_nama',
//                        'filter'=>false,
//                    ),

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Mengetahui dialog =============================
?>

<?php 
//========= Dialog buat cari data Pegawai Menyetujui =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogMenyetujui',
    'options'=>array(
        'title'=>'Pencarian Menyetujui Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modMenyetujui = new PegawaiM('search');
$modMenyetujui -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modMenyetujui->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'menyetujui-m-grid',
	'dataProvider'=>$modMenyetujui->search(),
	'filter'=>$modMenyetujui,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectMenyetujui",
                                            "onClick" => "$(\"#KPRencanaLemburT_menyetujui_id\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'menyetujui_nama').'\").val(\"$data->nama_pegawai\");
                                                          $(\"#dialogMenyetujui\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Jabatan',
//                            'value'=>'$data->jabatan->jabatan_nama',
//                            'filter'=>false,
//                        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Menyetujui dialog =============================
?>

<?php 
//========= Dialog buat cari data Pegawai Lembur =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawaiLembur',
    'options'=>array(
        'title'=>'Pencarian Karyawan Lembur',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawaiLembur = new PegawaiM('search');
$modPegawaiLembur -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modPegawaiLembur->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'karlembur-m-grid',
	'dataProvider'=>$modPegawaiLembur->search(),
	'filter'=>$modPegawaiLembur,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPegawaiLembur",
                                            "onClick" => "$(\"#idPegawaiLembur\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'karlembur_nama').'\").val(\"$data->nama_pegawai\");
                                                          submitPegawaiLembur();
                                                          $(\"#dialogPegawaiLembur\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Jabatan',
//                            'value'=>'$data->jabatan->jabatan_nama',
//                            'filter'=>false,
//                        ),    
                

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pegawai Lembur dialog =============================
?>

<?php 
//========= Dialog buat cari data Pemberi Tugas =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemberiTugas',
    'options'=>array(
        'title'=>'Pencarian Pemberi Tugas',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>600,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPemberiTugas = new PegawaiM('search');
$modPemberiTugas -> unsetAttributes();
if(isset($_GET['PegawaiM'])) {
    $modPemberiTugas->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pemberitugas-m-grid',
	'dataProvider'=>$modPemberiTugas->search(),
	'filter'=>$modPemberiTugas,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPemberiTugas",
                                            "onClick" => "$(\"#KPRencanaLemburT_pemberitugas_id\").val(\"$data->pegawai_id\");
                                                          $(\"#'.CHtml::activeId($modRencanaLembur,'pemberitugas_nama').'\").val(\"$data->nama_pegawai\");
                                                          $(\"#dialogPemberiTugas\").dialog(\"close\");    
                                                          return false;
                                                "))',
                        ),
                        array(
                            'header'=>'No.',
                            'type'=>'raw',
                            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
                            'filter'=>false,
                        ),
                        'nomorindukpegawai',                
                        'nama_pegawai',
//                        array(
//                            'header'=>'Jabatan',
//                            'value'=>'$data->jabatan->jabatan_nama',
//                            'filter'=>false,
//                        ),

	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end Pemberi Tugas dialog =============================
?>