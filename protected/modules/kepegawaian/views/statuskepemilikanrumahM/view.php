<?php
$this->breadcrumbs=array(
	'Statuskepemilikanrumah Ms'=>array('index'),
	$model->statuskepemilikanrumah_id,
);

$this->menu=array(
        array('label'=>Yii::t('mds','View').' Status Kepemilikan Rumah', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master')),
//	array('label'=>'List StatuskepemilikanrumahM', 'url'=>array('index')),
//	array('label'=>'Create StatuskepemilikanrumahM', 'url'=>array('create')),
//	array('label'=>'Update StatuskepemilikanrumahM', 'url'=>array('update', 'id'=>$model->statuskepemilikanrumah_id)),
//	array('label'=>'Delete StatuskepemilikanrumahM', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->statuskepemilikanrumah_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Status Kepemilikan Rumah', 'url'=>array('admin')),
);
?>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'statuskepemilikanrumah_id',
		'statuskepemilikanrumah_nama',
		'statuskepemilikanrumah_namalain',
		'statuskepemilikanrumah_aktif',
	),
)); ?>
