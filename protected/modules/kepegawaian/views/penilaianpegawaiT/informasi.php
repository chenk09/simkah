<legend class="rim2">Informasi Personal Scoring</legend>
<?php
$this->breadcrumbs=array(
	'Kppenilaianpegawai Ts'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' KPPenilaianpegawaiT ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' KPPenilaianpegawaiT', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' KPPenilaianpegawaiT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
//$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search-t-form').submit(function(){
	$.fn.yiiGridView.update('penilaianpegawai-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'penilaianpegawai-t-grid',
	'dataProvider'=>$model->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'penilaianpegawai_id',
		array(
                        'name'=>'penilaianpegawai_id',
                        'value'=>'$data->penilaianpegawai_id',
                        'filter'=>false,
                ),
		'pegawai.nama_pegawai',
		'tglpenilaian',
		'periodepenilaian',
		'sampaidengan',
		array(
                                    'header'=>'Detail',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link(\'<i class="icon-eye-open"></i>\',Yii::app()->createUrl(\'remunerasi/PenilaianpegawaiT/detail&id=\'.$data->penilaianpegawai_id))',
                                ),
//		'kesetiaan',
		/*
		'prestasikerja',
		'tanggungjawab',
		'ketaatan',
		'kejujuran',
		'kerjasama',
		'prakarsa',
		'kepemimpinan',
		'jumlahpenilaian',
		'nilairatapenilaian',
		'performanceindex',
		'penilaianpegawai_keterangan',
		'keberatanpegawai',
		'tanggal_keberatanpegawai',
		'tanggapanpejabat',
		'tanggal_tanggapanpejabat',
		'keputusanatasan',
		'tanggal_keputusanatasan',
		'lainlain',
		'dibuattanggalpejabat',
		'diterimatanggalpegawai',
		'diterimatanggalatasan',
		'penilainama',
		'penilainip',
		'penilaipangkatgol',
		'penilaijabatan',
		'penilaiunitorganisasi',
		'pimpinannama',
		'pimpinannip',
		'pimpinanpangkatgol',
		'pimpinanjabatan',
		'pimpinanunitorganisasi',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<fieldset>
    <legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search-t-form',
        'type'=>'horizontal',
)); ?>

        <table>
            <tr>
                <td>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'tglpenilaian',array('class'=>'control-label')); ?>
                        <div class="controls">
                          <?php   $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tglpenilaian',
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'periodepenilaian',array('class'=>'control-label')); ?>
                        <div class="controls">
                          <?php   $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'periodepenilaian',
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'sampaidengan',array('class'=>'control-label')); ?>
                        <div class="controls">
                          <?php   $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'sampaidengan',
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                        </div>
                    </div>
                    <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3')); ?>
                </td>
                <td>
                </td>
            </tr>
        </table>
</fieldset>
                                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit', 'name'=>'search')); ?>&nbsp;
                                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),Yii::app()->createUrl($this->module->id.'/'.PenilaianpegawaiT.'/informasi'), array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                                <?php 
                                   $content = $this->renderPartial('kepegawaian.views./tips/informasi',array(),true);
                                   $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
                                ?>

<?php $this->endWidget(); ?>