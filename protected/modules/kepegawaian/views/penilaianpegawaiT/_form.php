
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'kppenilaianpegawai-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

<!--	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->

	<?php echo $form->errorSummary($model); ?>
        <?php // $this->renderPartial('kepegawaian.views.penilaianpegawaiT._pegawai', array('model'=>$modPegawai, 'form'=>$form)); ?>
            <?php //echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<fieldset>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($modPegawai,'nomorindukpegawai',array('readonly'=>true,'id'=>'NIP')); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Karyawan','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo $form->hiddenField($modPegawai,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'model'=>$modPegawai, 
//                                        'name'=>'namapegawai',
                                        'attribute'=>'nama_pegawai',
                                        'value'=>$namapegawai,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#namapegawai").val( ui.item.nama_pegawai );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#NIP").val( ui.item.nomorindukpegawai);
                                                $("#tempatlahir_pegawai").val( ui.item.tempatlahir_pegawai);
                                                $("#tgl_lahirpegawai").val( ui.item.tgl_lahirpegawai);
                                                $("#namapegawai").val( ui.item.nama_pegawai);
                                                $("#jeniskelamin").val( ui.item.jeniskelamin);
                                                $("#statusperkawinan").val( ui.item.statusperkawinan);
                                                $("#jabatan").val( ui.item.jabatan_nama);
                                                $("#alamat_pegawai").val( ui.item.alamat_pegawai);
                                                if(ui.item.photopegawai != null){
                                                    $("#photo_pasien").attr(\'src\',\''.Params::urlPegawaiTumbsDirectory().'kecil_\'+ui.item.photopegawai);
                                                } else {
                                                    $("#photo_pasien").attr(\'src\',\'http://localhost/simrs/data/images/pasien/no_photo.jpeg\');
                                                }
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
                                        'tombolDialog'=>array('idDialog'=>'dialogPegawai','idTombol'=>'tombolPasienDialog'),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPegawai,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai, 'jeniskelamin',array('readonly'=>true,'id'=>'jeniskelamin')); ?>
                <?php echo $form->textFieldRow($modPegawai,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
<!--                <div class="control-group">
                    <?php //echo $form->labelEx($model, 'jabatan_id',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php //echo $form->textFieldRow($model,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                    </div>
                </div>-->
                <?php echo $form->textFieldRow($modPegawai,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                <?php echo $form->textFieldRow($modPegawai,'pangkat_id',array('readonly'=>true,'id'=>'pangkat')); ?>
                <?php echo $form->textFieldRow($modPegawai,'kategoripegawai',array('readonly'=>true,'id'=>'kategoripegawai')); ?>
                <?php //echo $form->textFieldRow($modPegawai,'kategoripegawaiasal',array('readonly'=>true,'id'=>'kategoripegawaiasal')); ?>
                <?php echo $form->textFieldRow($modPegawai,'kelompokpegawai_id',array('readonly'=>true,'id'=>'kelompokpegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai,'pendidikan_id',array('readonly'=>true,'id'=>'pendidikan')); ?>
                <?php //echo $form->textAreaRow($model,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
            </td>
            <td>
                <?php 
                    if(!empty($modPegawai->photopegawai)){
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'kecil_'.$modPegawai->photopegawai, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPegawaiDirectory().'no_photo.jpeg', 'Photo Pegawai', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
</fieldset>
         <?php //echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<table>
    <tbody>
        <tr>
            <td colspan ="2">
                <?php //echo $form->textFieldRow($model, 'tglpenilaian', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpenilaian', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglpenilaian',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                 ),
                        )); ?> 
                    </div>
                </div>
                
            </td>
        </tr>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'periodepenilaian', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'periodepenilaian',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                 ),
                        )); ?> 
                    </div>
                </div>
                <?php //echo $form->textFieldRow($model, 'periodepenilaian', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'sampaidengan', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'sampaidengan',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,
                                                                      'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                      'class'=>'dtPicker3',
                                                 ),
                        )); ?> 
                    </div>
                </div>
                <?php //echo $form->textFieldRow($model, 'sampaidengan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $form->textFieldRow($model, 'kesetiaan', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'prestasikerja', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'tanggungjawab', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'ketaatan', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($model, 'kejujuran', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'kerjasama', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'prakarsa', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'kepemimpinan', array('class' => 'span3 numbersOnly pointNilai', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <td>
                </td>
            <td>
                <?php echo $form->textFieldRow($model, 'jumlahpenilaian', array('class' => 'span3 totalNilai', 'onkeypress' => "return $(this).focusNextInputField(event);", 'readonly'=>true)); ?>
                 <?php echo $form->textFieldRow($model,'nilairatapenilaian',array('class'=>'span3 rataNilai', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'readonly'=>True)); ?>
            <?php echo $form->textFieldRow($model,'performanceindex',array('class'=>'span3 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
    </tfoot>
</table>

<table>
    <tr>
        <td>
            <?php echo $form->textAreaRow($model,'penilaianpegawai_keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tanggal_keberatanpegawai', array('class'=>'control-label'));?>
                <div class="controls">  
                  <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tanggal_keberatanpegawai',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'dtPicker3',
                                            ),
                )); ?> 
                 <?php echo $form->textArea($model,'keberatanpegawai',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'diterimatanggalpegawai', array('class'=>'control-label'));?>
                <div class="controls">  
                  <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'diterimatanggalpegawai',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'dtPicker3',
                                            ),
                )); ?> 
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'diterimatanggalatasan', array('class'=>'control-label'));?>
                <div class="controls">  
                  <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'diterimatanggalatasan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'dtPicker3',
                                            ),
                )); ?> 
                </div>
            </div>
        </td>
        <td>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tanggapanpejabat', array('class'=>'control-label'));?>
                <div class="controls">  
                  <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tanggal_tanggapanpejabat',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'dtPicker3',
                                            ),
                )); ?> 
                 <?php echo $form->textArea($model,'tanggapanpejabat',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'keputusanatasan', array('class'=>'control-label'));?>
                <div class="controls">  
                  <?php $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tanggal_keputusanatasan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                              'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                              'class'=>'dtPicker3',
                                            ),
                )); ?> 
                 <?php echo $form->textArea($model,'keputusanatasan',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
        </td>
    </tr>
</table>

            
<table>
    <tr>
        <td>
            <?php //echo $form->textFieldRow($model,'penilainama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'penilainama', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model'=>$model,
                        'attribute' => 'penilainama',
                        'source' => 'js: function(request, response) {
                                           $.ajax({
                                               url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                                        $(this).val( ui.item.label);
                                                                        return false;
                                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                                        $("#'.Chtml::activeId($model, 'penilainama') . '").val(nama_pegawai); 
                                                                        return false;
                                                                    }',
                        ),
                        'htmlOptions' => array(
                            'onkeypress' => "return $(this).focusNextInputField(event)",
                        ),
                        'tombolDialog' => array('idDialog' => 'dialogPenilai'),
                    ));
                    ?>
                    <?php echo $form->error($model, 'penilainama'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'penilainip',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'penilaipangkatgol',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'penilaijabatan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($model,'penilaiunitorganisasi',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
        </td>
        <td>
            <?php //echo $form->textFieldRow($model,'pimpinannama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'pimpinannama', array('class' => 'control-label')); ?>
                <div class="controls">
                    <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model'=>$model,
                        'attribute' => 'pimpinannama',
                        'source' => 'js: function(request, response) {
                                           $.ajax({
                                               url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                                                        $(this).val( ui.item.label);
                                                                        return false;
                                                                    }',
                            'select' => 'js:function( event, ui ) {
                                                                        $("#'.Chtml::activeId($model, 'pimpinannama') . '").val(nama_pegawai); 
                                                                        return false;
                                                                    }',
                        ),
                        'htmlOptions' => array(
                            'onkeypress' => "return $(this).focusNextInputField(event)",
                        ),
                        'tombolDialog' => array('idDialog' => 'dialogPimpinan'),
                    ));
                    ?>
                    <?php echo $form->error($model, 'pimpinannama'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'pimpinannip',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php //echo $form->textFieldRow($model,'pimpinanpangkatgol',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'pimpinanjabatan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($model,'pimpinanunitorganisasi',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
        </td>
    </tr>
</table>
            <?php //echo $form->textAreaRow($model,'lainlain',array('rows'=>6, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'dibuattanggalpejabat',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'diterimatanggalpegawai',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'diterimatanggalatasan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	

        <div class="form-actions">
        <?php    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    else
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>

            <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('penilaianpegawaiT/create',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>

            <?php 
				if(isset($_GET['sukses'])){
					echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->penilaianpegawai_id');return false",'disabled'=>FALSE)); 
                }else{
                    echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                } 
            ?>

                <?php 
                    $content = $this->renderPartial('../tips/transaksi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'create','content'=>$content));       
                ?>
    </div>
<?php 
        // $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        // $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        // $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintDetailPenilaian&id='.$modPegawai->pegawai_id);

// $js = <<< JSCRIPT
// // function print(caraPrint)
// // {
// //     window.open("${urlPrint}/"+$('#search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
// // }   
// JSCRIPT;
// Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>

<?php $this->endWidget(); ?>

<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php 
Yii::app()->clientScript->registerScript('onreadyfunction','
    $(".pointNilai").keypress(function(){
        var value = $(this).val();
        var jumlah = $(".pointNilai").length;
        var total = 0;
        if (value > 10){
            $(this).val(10);
        }
        $(".pointNilai").each(function(data){
            total += parseFloat($(this).val());
        });
        if (jQuery.isNumeric(total)){
            $(".totalNilai").val(total);
            $(".rataNilai").val(total/jumlah);
        }
    });
', CClientScript::POS_READY); ?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPimpinan',
    'options' => array(
        'title' => 'Daftar Karyawan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPegawai = new KPPegawaiM('search');
$modPegawai->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['KPPegawaiM']))
    $modPegawai->attributes = $_GET['KPPegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawai2-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                "id" => "selectBahan",
                "href"=>"",
                "onClick" => "
                      getDataPemimpin(\"$data->pegawai_id\");
                      $(\"#dialogPimpinan\").dialog(\"close\");    
                      return false;
            "))',
        ),
        'nama_pegawai',
        'nomorindukpegawai',
        'alamat_pegawai',
        'agama',
        array(
            'name'=>'jeniskelamin',
            'filter'=>JenisKelamin::items(),
            'value'=>'$data->jeniskelamin',
        ),
        array(
            'header' => 'Jabatan',
            'value'=>'$data->jabatan->jabatan_nama',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPenilai',
    'options' => array(
        'title' => 'Daftar Karyawan',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPegawai = new KPPegawaiM('search');
$modPegawai->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['KPPegawaiM']))
    $modPegawai->attributes = $_GET['KPPegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawai3-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                    "id" => "selectBahan",
                    "href"=>"",
                    "onClick" => "
                      getDataPenilai(\"$data->pegawai_id\");
                      $(\"#dialogPenilai\").dialog(\"close\");    
                      return false;
                "))',
            ),
            'nama_pegawai',
            'nomorindukpegawai',
            'alamat_pegawai',
            'agama',
            array(
                'name'=>'jeniskelamin',
                'filter'=>  JenisKelamin::items(),
                'value'=>'$data->jeniskelamin',
            ),
            array(
                'header' => 'Jabatan',
                'value'=>'$data->jabatan->jabatan_nama',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM;

$modPegawai->unsetAttributes();
if (isset($_GET['PegawaiM']))
    $modPegawai->attributes = $_GET['PegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawai->search(),
	'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	    'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                        "id" => "selectPegawai",
                        "href"=>"",
                        "onClick" => "
                              getDataPegawai(\"$data->pegawai_id\");
                              $(\"#dialogPegawai\").dialog(\"close\");    
                              return false;
                    "))',
                ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'jabatan.jabatan_nama',
                'alamat_pegawai',
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>
<?php
    $url = new KPRegistrasifingerprint;
?>
<?php
$controller = Yii::app()->controller->id;
$module = Yii::app()->controller->module->id . '/' .$controller;
$urlKartu =  Yii::app()->createAbsoluteUrl($module);
// $urlPrintgaji = Yii::app()->createUrl('print/kartuPasien',array('penggajianpeg_id'=>''));
?>
<script type="text/javascript">
    function getDataPegawai(params)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai');?>", {idPegawai:params },
            function(data){
                $("#NIP").val(data.nomorindukpegawai);
                $("#pegawai_id").val(data.pegawai_id);
                $("#KPRegistrasifingerprint_nama_pegawai").val(data.nama_pegawai);
                $("#tempatlahir_pegawai").val(data.tempatlahir_pegawai);
                $("#tgl_lahirpegawai").val(data.tgl_lahirpegawai);
                $("#jabatan").val(data.jabatan_nama);
                $("#pangkat").val(data.pangkat_nama);
                $("#kategoripegawai").val(data.kategoripegawai);
                $("#kategoripegawaiasal").val(data.kategoripegawaiasal);
                $("#kelompokpegawai").val(data.kelompokpegawai_nama);
                $("#pendidikan").val(data.pendidikan_nama);
                $("#jeniskelamin").val(data.jeniskelamin);
                $("#statusperkawinan").val(data.statusperkawinan);
                $("#alamat_pegawai").val(data.alamat_pegawai);
                if(data.photopegawai != ""){
                    var url = "<?php echo Params::urlPegawaiTumbsDirectory() . 'kecil_'; ?>" + data.photopegawai;
                    $("#photo_pasien").attr('src', url);
                } else {
                    $("#photo_pasien").attr('src',"http://localhost/simrs/data/images/pasien/no_photo.jpeg");
                }                
            }, "json");
    }

    function getDataPemimpin(params)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai');?>", {idPegawai:params },
            function(data){
                $("#KPPenilaianpegawaiT_pimpinannama").val(data.nama_pegawai);
                $("#KPPenilaianpegawaiT_pimpinannip").val(data.nomorindukpegawai);
                $("#KPPenilaianpegawaiT_pimpinanjabatan").val(data.jabatan_nama);
                $("#KPPenilaianpegawaiT_pimpinanunitorganisasi").val(data.nama_pegawai);             
            }, "json");
    }

    function getDataPenilai(params)
    {
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai');?>", {idPegawai:params },
            function(data){
                $("#KPPenilaianpegawaiT_penilainama").val(data.nama_pegawai);
                $("#KPPenilaianpegawaiT_penilainip").val(data.nomorindukpegawai);
                $("#KPPenilaianpegawaiT_penilaijabatan").val(data.jabatan_nama);
                $("#KPPenilaianpegawaiT_penilaiunitorganisasi").val(data.nama_pegawai);             
            }, "json");
    }

    function print(penilaianpegawai_id)
{       
        //if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
            window.open('index.php?r=kepegawaian/penilaianpegawaiT/PrintDetailPenilaian&id='+penilaianpegawai_id,'printwindows','left=200,top=600,width=1010,height=900,resizable=true');
            var urlKartu = 'index.php?r=kepegawaian/penilaianpegawaiT/PrintDetailPenilaian&id='+penilaianpegawai_id;
            $.post(urlKartu, {penilaianpegawai_id: penilaianpegawai_id}, "json");

        //}
}
</script>