
<?php

class RealisasiLemburTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'buat';
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('buat','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('informasi', 'lihatdetail','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	/**
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function actionBuat($norencana)
	{
            $this->layout='//layouts/frameDialog';
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $modRealisasiLembur = new KPRealisasiLemburT();
            
            
            if($norencana != null){
                $modRencanaLembur = KPRencanaLemburT::model()->findByAttributes(array('norencana'=>$norencana));
                $modRencanaLemburDetail = KPRencanaLemburT::model()->findAllByAttributes(array('norencana'=>$norencana, 'realisasilembur_id'=>null));
            }
            
            $format = new CustomFormat;
            $modRencanaLembur->tglrencana = date('d M Y H:i:s',strtotime($modRencanaLembur->tglrencana));
            if (isset($_POST['KPRencanaLemburT'])){
                $i = 0;
                $sukses = 0;
                $gagal = 0;
                $tgljamrealisasi = $_POST['KPRealisasiLemburT']['tglrealisasi'];
                $tgljamrealisasi = $format->formatDateTimeMediumForDB($tgljamrealisasi);
                $tglrealisasi = date('Y-m-d', strtotime($tgljamrealisasi));
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    foreach ($_POST['KPRencanaLemburT'] as $attr => $val){
                        if(isset($_POST['pilih'][$i])){
                            $modRealisasiLembur = new KPRealisasiLemburT();                        
                            $modRealisasiLembur->attributes = $_POST['KPRencanaLemburT'][$i];
                            $modRealisasiLembur->jamMulai = $_POST['KPRencanaLemburT'][$i]['jamMulai'];
                            $modRealisasiLembur->jamSelesai = $_POST['KPRencanaLemburT'][$i]['jamSelesai'];
                            if(!empty($modRealisasiLembur->jamMulai)){
                                $modRealisasiLembur->tglmulai = $tglrealisasi." ".$modRealisasiLembur->jamMulai.":00";
                            }
                            if(!empty($modRealisasiLembur->jamSelesai)){
                                $modRealisasiLembur->tglselesai = $tglrealisasi." ".$modRealisasiLembur->jamSelesai.":00";
                            }
                            $modRealisasiLembur->tglrealisasi = $tgljamrealisasi;
                            $modRealisasiLembur->norealisasi = $_POST['KPRealisasiLemburT']['norealisasi'];
                            $modRealisasiLembur->nourut = $_POST['nourut'][$i];
                            $modRealisasiLembur->mengetahui_id = $_POST['KPRencanaLemburT']['mengetahui_id'];
                            $modRealisasiLembur->menyetujui_id = $_POST['KPRencanaLemburT']['menyetujui_id'];
                            $modRealisasiLembur->pemberitugas_id = $_POST['KPRencanaLemburT']['pemberitugas_id'];
                            $modRealisasiLembur->keterangan = $_POST['KPRencanaLemburT']['keterangan'];
                            $modRealisasiLembur->create_time = date('Y-m-d H:i:s');
                            $modRealisasiLembur->create_user = Yii::app()->user->id;

                            $modRencanaLemburUpdate = KPRencanaLemburT::model()->findByPk($modRealisasiLembur->rencanalembur_id);

                            if($modRealisasiLembur->save()){
                                $sukses++;
                                $modRealisasiLemburCari = KPRealisasiLemburT::model()->findByAttributes(array('rencanalembur_id'=>$modRencanaLemburUpdate->rencanalembur_id));
                                $modRencanaLemburUpdate->realisasilembur_id = $modRealisasiLemburCari->realisasilembur_id;
                                $modRencanaLemburUpdate->update(array('realisasilembur_id'));
                            }else{
                                $gagal++;
                                $nama[$gagal] = $modRealisasiLembur->pegawai->nama_pegawai;
                            }

                        }
                    $i++;                
                    }
                    if($sukses > 0){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',$sukses." pegawai lembur telah berhasil direalisasikan !");
//                        echo "<script>setTimeout(function reload(){parent.location.reload();},2000);</script>";
                    }
                    if($gagal > 0){
                        foreach($nama as $i=>$val){
                            $namanama .= "<br>".$i.". ".$nama[$i];
                        }
                            Yii::app()->user->setFlash('error',"Data : ". $namanama."<br>gagal disimpan ! [Total = ".$gagal."]");
                            $transaction->rollback();
                    }
                //Yii::app()->request->redirect('index.php?r=hrd/rencanaLemburT/informasi');
                }catch (Exception $exc){
                //   $model->isNewRecord = true;
                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }else{
                $modRealisasiLembur->tglrealisasi = date('d M Y H:i:s');
                
            }
            
            $modRealisasiLembur->tglrealisasi = date('d M Y H:i:s', strtotime($modRealisasiLembur->tglrealisasi));
            $this->render('buat',array(
			'modRealisasiLembur'=>$modRealisasiLembur, 
			'modRencanaLembur'=>$modRencanaLembur, 
                        'modDetail'=>$modRencanaLemburDetail,
                        'norencana'=>$norencana
                    ));
                
            
            
                
	}
        
	     

	/**
	 * Informasi Realisasi Lembur.
	 */
	public function actionInformasi()
	{
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $modRealisasiLembur=new KPRealisasiLemburT('search');
            $modRealisasiLembur->unsetAttributes();  // clear any default values

            if(isset($_GET['KPRealisasiLemburT'])){
                    $modRealisasiLembur->tgl_awal=$_GET['KPRealisasiLemburT']['tgl_awal'];
                    $modRealisasiLembur->tgl_akhir=$_GET['KPRealisasiLemburT']['tgl_akhir'];
            }else{
                    $modRealisasiLembur->tgl_awal = date ('d M Y 00:00:00');
                    $modRealisasiLembur->tgl_akhir = date ('d M Y H:i:s');
            }

            $this->render('informasi',array(
                    'modRealisasiLembur'=>$modRealisasiLembur,
            ));
	}

	/**
         * Untuk melihat detail transaksi rencana lembur
         */
        public function actionLihatDetail($norealisasi)
	{
            $this->layout='//layouts/frameDialog';
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $modRealisasiLembur = KPRealisasiLemburT::model()->findByAttributes(array('norealisasi'=>$norealisasi));
            $modRealisasiLemburDetail = KPRealisasiLemburT::model()->findAllByAttributes(array('norealisasi'=>$norealisasi));
            $format = new CustomFormat;
            $modRealisasiLembur->tglrealisasi = $format->formatDateINAtime($modRealisasiLembur->tglrealisasi);

            $this->render('lihatdetail',array(
                    'modRealisasiLembur'=>$modRealisasiLembur, 'modDetail'=>$modRealisasiLemburDetail,
                    'norealisasi'=>$norealisasi
            ));
                
	}
        
        /**
         * end by ichan
         */
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPRealisasiLemburT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rencana-lembur-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new KPRealisasiLemburT;
            $model->attributes=$_REQUEST['KPRealisasiLemburT'];
            $judulLaporan='Data RealisasiLemburT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }        
        
}
