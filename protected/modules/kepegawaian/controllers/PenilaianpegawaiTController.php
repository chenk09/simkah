<?php

class PenilaianpegawaiTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary','detailPenilaian','Penilaian','PrintDetailPenilaian'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new KPPenilaianpegawaiT;
		$model->tglpenilaian = date('Y-m-d H:i:s');
		$model->periodepenilaian = date('Y-m-d H:i:s');
		$model->sampaidengan = date('Y-m-d H:i:s');
		$model->kejujuran = 0;
		$model->ketaatan = 0;
		$model->jumlahpenilaian = 0;
		$model->nilairatapenilaian = 0;
		$model->performanceindex = 0;
		$model->kesetiaan = 0;
		$model->prestasikerja = 0;
		$model->tanggungjawab = 0;
		$model->kerjasama = 0;
		$model->prakarsa = 0;
		$model->kepemimpinan = 0;
		$modPegawai = new KPRegistrasifingerprint;
               
		
		if(isset($_GET['penilaianpegawai_id'])){
			$model = KPPenilaianpegawaiT::model()->findByPk($_GET['penilaianpegawai_id']);
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
		if(isset($_POST['KPPenilaianpegawaiT']))
		{
			$model->attributes=$_POST['KPPenilaianpegawaiT'];
            $model->pegawai_id = $_POST['KPRegistrasifingerprint']['pegawai_id'];
			if($model->save()){
				$this->redirect(array('create','penilaianpegawai_id'=>$model->penilaianpegawai_id,'sukses'=>1));
                $this->refresh();
            } else {
				$transaction->rollback();
            	Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
            }
		}

		$this->render('create',array(
			'modPegawai'=>$modPegawai, 
                        'model'=>$model,
                        'namapegawai'=>''						
		));
	}
        
        public function actionPenilaian($id)
	{
//               $this->layout = 'frameDialog';
               if (!empty($id)) {
                $modPegawai = KPPegawaiM::model()->find('pegawai_id = ' . $id . '');
                $modPegawai->jabatan_id = (isset($modPegawai->jabatan->jabatan_nama) ? $modPegawai->jabatan->jabatan_nama : "-");
                $model = PenilaianpegawaiT::model()->find('pegawai_id = ' . $modPegawai->pegawai_id . ' ');
                
            } 

            if(empty($model)){
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model = new PenilaianpegawaiT;
                $model->tglpenilaian = date('d M Y H:i:s');
                $model->periodepenilaian = date('d M Y H:i:s');
                $model->sampaidengan = date('d M Y H:i:s');
                $model->kejujuran = 0;
                $model->ketaatan = 0;
                $model->jumlahpenilaian = 0;
                $model->nilairatapenilaian = 0;
                $model->performanceindex = 0;
                $model->kesetiaan = 0;
                $model->prestasikerja = 0;
                $model->tanggungjawab = 0;
                $model->kerjasama = 0;
                $model->prakarsa = 0;
                $model->kepemimpinan = 0;
                
                if(isset($_POST['PenilaianpegawaiT']))
				{
//                    echo '<pre>';
//                    echo print_r($_POST['PenilaianpegawaiT']);
//                    echo '</pre>';
//                    exit;
                    $model->attributes=$_POST['PenilaianpegawaiT'];
                    $model->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];


	            	$format = new CustomFormat();
                    $model->tglpenilaian = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tglpenilaian']);
                    $model->periodepenilaian = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['periodepenilaian']);
                    $model->sampaidengan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['sampaidengan']);
                    $model->tanggal_keberatanpegawai = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_keberatanpegawai']);
                    $model->tanggal_tanggapanpejabat = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_tanggapanpejabat']);
                    $model->tanggal_keputusanatasan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_keputusanatasan']);
                    $model->diterimatanggalpegawai = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['diterimatanggalpegawai']);
                    $model->diterimatanggalatasan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['diterimatanggalatasan']);


                    if($model->save()){
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//                            $this->refresh();
//				$this->redirect(array('create','id'=>$model->pegawai_id));
                    }else{
                        Yii::app()->user->setFlash('error', '<strong>Gagal !</strong> Data gagal disimpan.');
                    }
				}
            } else {
            	if(isset($_POST['PenilaianpegawaiT']))
				{
	            	$model->attributes=$_POST['PenilaianpegawaiT'];


	            	$format = new CustomFormat();
                    $model->tglpenilaian = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tglpenilaian']);
                    $model->periodepenilaian = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['periodepenilaian']);
                    $model->sampaidengan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['sampaidengan']);
                    $model->tanggal_keberatanpegawai = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_keberatanpegawai']);
                    $model->tanggal_tanggapanpejabat = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_tanggapanpejabat']);
                    $model->tanggal_keputusanatasan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['tanggal_keputusanatasan']);
                    $model->diterimatanggalpegawai = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['diterimatanggalpegawai']);
                    $model->diterimatanggalatasan = $format->formatDateTimeMediumForDB($_POST['PenilaianpegawaiT']['diterimatanggalatasan']);


					if($model->save()){
		                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
		                    $this->refresh();
	                } else {
	                		Yii::app()->user->setFlash('error', '<strong>Gagal !</strong> Data gagal disimpan.');
	                }
	            }
            }
            

            $this->render('create',array(
                'modPegawai'=>$modPegawai,
                'model'=>$model,
            ));
	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KPPenilaianpegawaiT']))
		{
			$model->attributes=$_POST['KPPenilaianpegawaiT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->refresh();
//				$this->redirect(array('create'));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPenilaianpegawaiT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPenilaianpegawaiT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPenilaianpegawaiT']))
			$model->attributes=$_GET['KPPenilaianpegawaiT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPPenilaianpegawaiT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kppenilaianpegawai-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {

            $model= new KPPenilaianpegawaiT;
            $model->attributes=$_REQUEST['KPPenilaianpegawaiT'];
            $judulLaporan='Data KPPenilaianpegawaiT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
                       
     public function actionPrintDetailPenilaian($id) {
        $model = PenilaianpegawaiT::model()->findByPk($id);
        $modelpegawai = PegawaiM::model()->findByPk($model->pegawai_id);
        $modelpegawai->attributes = $_REQUEST['KPPegawaiM'];
        if(empty($model)){
            $model = new PenilaianpegawaiT;
        }

        $judulLaporan = 'Penilaian Pegawai';
		$caraPrint= isset($_GET['caraPrint']) ? $_GET['caraPrint'] : 'PRINT';
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('PrintPenilaian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('PrintPenilaian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint=='PDF') {
         
        	// var_dump($modelpegawai);
        	// exit();
				$ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
	            $mpdf->WriteHTML($this->renderPartial('PrintPenilaian', array('model'=>$model, 'modelpegawai'=>$modelpegawai, 'judulLaporan'=>$judulLaporan, 'caraPrint'=>$caraPrint), true));
	            // $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
	            $mpdf->Output();
         }
        }

      
}
