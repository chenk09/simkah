
<?php

class RencanaLemburTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'buat';
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('buat','print','lihatdetail'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('informasi','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionBuat()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modRencanaLembur = new KPRencanaLemburT();

                $namanama = "";
                $format = new CustomFormat();
                // Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                if(isset($_POST['KPRencanaLemburT']))
		{                        
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            //Mendefinisikan variabel konstan
                            $create_time = date('Y-m-d H:i:s');
                            $create_loginpemakai_id = Yii::app()->user->id;
                            //Membuat objek dari form 
                            $alldata = $_POST['KPRencanaLemburT'];                                                                       
                            $sukses = 0;
                            $gagal = 0;
                            
                            foreach ($alldata as $attr => $value) {                            
                                if(is_array($value)){
                                    $i = 0;
                                    $modRencanaLemburTab[$i] = new KPRencanaLemburT();                                   
                                    $modRencanaLemburTab[$i]->attributes = $alldata;
                                    $modRencanaLemburTab[$i]->attributes = $value;
                                    $modRencanaLemburTab[$i]->jamMulai = $value['jamMulai'];
                                    $modRencanaLemburTab[$i]->jamSelesai = $value['jamSelesai'];
                                    $modRencanaLemburTab[$i]->create_time=$create_time;
                                    $modRencanaLemburTab[$i]->create_loginpemakai_id=$create_loginpemakai_id;
                                    $modRencanaLemburTab[$i]->create_ruangan=Yii::app()->user->getState('ruangan_id');
                                    
                                    $modRencanaLemburTab[$i]->tglrencana = $format->formatDateTimeMediumForDB($modRencanaLemburTab[$i]->tglrencana);
                                    $tglrencana = date('Y-m-d',strtotime($modRencanaLemburTab[$i]->tglrencana));
                                    
                                    if(empty($modRencanaLemburTab[$i]->jamMulai)){
                                        $modRencanaLemburTab[$i]->tglmulai = null;
                                    }else{
                                        $tglmulai = $tglrencana." ".$modRencanaLemburTab[$i]->jamMulai.":00";
                                        $modRencanaLemburTab[$i]->tglmulai = $tglmulai;
                                    }
                                    if(empty($modRencanaLemburTab[$i]->jamSelesai)){
                                        $modRencanaLemburTab[$i]->tglselesai = null;
                                    }else{
                                        $tglselesai = $tglrencana." ".$modRencanaLemburTab[$i]->jamSelesai.":00";
                                        $modRencanaLemburTab[$i]->tglselesai = $tglselesai;
                                    }
                                    
                                        if($modRencanaLemburTab[$i]->validate()){
                                            $modRencanaLemburTab[$i]->save();
                                            $sukses++;
                                        }else{
                                            $gagal++;
                                            $nama[$gagal] = $modRencanaLemburTab[$i]->pegawai->nama_pegawai;
                                        }
                                    $i++;
                                }

                            }
                            if($sukses > 0){
                                Yii::app()->user->setFlash('success',$sukses." data telah berhasil disimpan !");
                                $transaction->commit();
                            }else{
                                foreach($nama as $i=>$val){
                                    $namanama .= "<br>".$i.". ".$nama[$i];
                                }
                                Yii::app()->user->setFlash('error',$namanama."<br>Gagal disimpan");
                                $transaction->rollback();
                            }
                        }catch (Exception $exc){
//                            $model->isNewRecord = true;
                            $transaction->rollback();
        //                    Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        }
            
		}
                $modRencanaLembur->tglrencana = date('d M Y H:i:s');

		$this->render('buat',array(
			'modRencanaLembur'=>$modRencanaLembur,
		));
	}

	      

	/**
	 * Manages all models.
	 */
	public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modRencanaLembur=new KPRencanaLemburT('searchInformasiRencanaLembur');
		$modRencanaLembur->unsetAttributes();  // clear any default values
		
                if(isset($_GET['KPRencanaLemburT'])){
			$modRencanaLembur->tgl_awal=$_GET['KPRencanaLemburT']['tgl_awal'];
			$modRencanaLembur->tgl_akhir=$_GET['KPRencanaLemburT']['tgl_akhir'];
                }else{
                    $modRencanaLembur->tgl_awal = date ('d M Y 00:00:00');
                    $modRencanaLembur->tgl_akhir = date ('d M Y H:i:s');
                }
                
                
		$this->render('informasi',array(
			'modRencanaLembur'=>$modRencanaLembur,
		));
	}

	/**
         * Untuk melihat detail transaksi rencana lembur
         */
        public function actionLihatDetail($norencana)
	{
                $this->layout='//layouts/frameDialog';
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modRencanaLembur = KPRencanaLemburT::model()->findByAttributes(array('norencana'=>$norencana));
                $modRencanaLemburDetail = KPRencanaLemburT::model()->findAllByAttributes(array('norencana'=>$norencana));
                $format = new CustomFormat;
                $modRencanaLembur->tglrencana = $format->formatDateINAtime($modRencanaLembur->tglrencana);
                
                $this->render('lihatdetail',array(
			'modRencanaLembur'=>$modRencanaLembur, 'modDetail'=>$modRencanaLemburDetail,
                        'norencana'=>$norencana
		));
                
	}
        
        
        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPRencanaLemburT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rencana-lembur-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new KPRencanaLemburT;
            $model->attributes=$_REQUEST['KPRencanaLemburT'];
            $judulLaporan='Data RencanaLemburT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
