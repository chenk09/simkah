
<?php

class PelamarTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
        private $successSaveBahasa      = false;
        private $successSaveLingkunganKerja = false;
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','KontrakPelamar'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPelamarT;
                $modBahasa = new KPKemampuanBahasaR;
                $modLingkunganKerja = new KPLingkunganKerjaR;
                $modBahasas = null;
                $modLingkunganKerjas = null;
                
                
                $modBahasa->no_urut = 1;
                $modLingkunganKerja->nourut= 1;
                
                if(!empty($id)){
                    $model          = KPPelamarT::model()->findByPk($id);
                    $modBahasas     = KPKemampuanBahasaR::model()->findAllByAttributes(array('pelamar_id'=>$model->pelamar_id));
                    $modLingkunganKerjas = KPLingkunganKerjaR::model()->findAllByAttributes(array('pelamar_id'=>$model->pelamar_id));
                }
                
                
                
                
                if(isset($_POST['KPPelamarT'])){
                      $format = new CustomFormat();                     
                      $model->attributes = $_POST['KPPelamarT'];
                      $model->tgllowongan = $format->formatDateMediumForDB($_POST['KPPelamarT']['tgllowongan']);                      
                      $model->tglditerima=null;
                      $model->crate_time = date('Y-m-d H:i:s');
                      $model->update_time = null;
                      $model->create_loginpemakai_id = Yii::app()->user->id;
                      // $model->create_loginpemakai_id = null;
                      $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                      $file = $model->filelamaran;
                      $model->photopelamar = CUploadedFile::getInstance($model, 'photopelamar');
                      $model->filelamaran = CUploadedFile::getInstance($model, 'filelamaran');
                      $gambar = $model->photopelamar;
                      $fileLamaran = $model->filelamaran;
                      $random = rand(000000, 999999);
                      
                      if(empty($_POST['KPPelamarT']['tgl_lahirpelamar']))
                          $model->tgl_lahirpelamar = null;
                      else
                          $model->tgl_lahirpelamar = $format->formatDateMediumForDB ($model->tgl_lahirpelamar);
                      if(empty($_POST['KPPelamarT']['tglmulaibekerja']))
                          $model->tglmulaibekerja = null;
                      else
                          $model->tglmulaibekerja = $format->formatDateMediumForDB ($model->tglmulaibekerja);
//                    ==========pengecekan file photo pelamar
                      if(!empty($model->photopelamar))//Klo User Memasukan Logo
                      { 

                            Yii::import("ext.EPhpThumb.EPhpThumb");

                             $thumb=new EPhpThumb();
                             $thumb->init(); //this is needed
                             $model->photopelamar = $random.$model->photopelamar; 
                             $fullImgName =$model->photopelamar;   
                             $fullImgSource = Params::pathPelamarPhotosDirectory().$fullImgName;
                             
                             $fullThumbSource = Params::pathPelamarThumbsDirectory().'kecil_'.$fullImgName;
                             
                             
                             
                      }
//                    ===========pengecekan file lamaran
                      if(!empty($model->filelamaran)){
                          $model->filelamaran = $random.$model->filelamaran;
                          $namaFile = $model->filelamaran; 
                          $dataLamaran = Params::pathPelamarFilesDirectory().$namaFile;
                          
                          
                      }
                      
//                    ======================= VALIDASI TABULAR
                      echo "<pre>";
            
                      if(isset($_POST['KPKemampuanBahasaR'])){
                            $hasilBahasa = $this->validasiTabularBahasa($_POST['KPKemampuanBahasaR']);
                            $modBahasas = $hasilBahasa['modBahasa'];
                            $bahasa = $hasilBahasa['bahasa'];
                      }
                      if(isset ($_POST['KPLingkunganKerjaR'])){
                        $hasilLingkunganKerja = $this->validasiTabularLingkunganKerja($_POST['KPLingkunganKerjaR']);
                        $modLingkunganKerjas = $hasilLingkunganKerja['modLingkunganKerja'];
                        $lingkunganKerja = $hasilLingkunganKerja['lingkunganKerja'];
                      }
                      
                                           
                      if($model->validate()){
                        $transaction = Yii::app()->db->beginTransaction();
                        try{    
                            
                            if($model->save()){
                               
//                              ==========ini digunakan untuk menyimpan photo pelamar
                                if (!empty($model->photopelamar)){  
                                  $gambar->saveAs($fullImgSource);

                                   $thumb->create($fullImgSource)
                                         ->resize(200,200)
                                         ->save($fullThumbSource);

                                 }
                                 
    //                           =========ini digunakan untuk menyimpan file lamaran pelamar
                                 if(!empty($model->filelamaran)){
                                     $fileLamaran->saveAs($dataLamaran);
                                 }
                                 
                                 if($bahasa){
                                     foreach ($modBahasas as $key => $modBahasa) {
                                         $modBahasa->pelamar_id = $model->pelamar_id;
                                         if($modBahasa->save()){
                                             $this->successSaveBahasa = true;
                                         }
                                     }
                                 }
                                 
                                 if($lingkunganKerja){
                                     foreach ($modLingkunganKerjas as $key => $modLingkunganKerja) {
                                         $modLingkunganKerja->pelamar_id = $model->pelamar_id;
                                         if($modLingkunganKerja->save()){
                                             $this->successSaveLingkunganKerja = true;
                                         }
                                     }
                                 }
                                
                            }
                            if ($model && $this->successSaveBahasa && $this->successSaveLingkunganKerja){
                                  $transaction->commit();
                                  Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data Berhasil Disimpan.');
//                                  $this->redirect(array('create','id'=>$model->pelamar_id));
                                  $this->redirect(array('create'));
                            }else{
                                $transaction->rollback();
                                 Yii::app()->user->setFlash('error',"Data Gagal Disimpan");
                            }
                         } catch (Exception $e){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data Gagal Disimpan". $e->getMessage());
                        }
                    }else{
                         Yii::app()->user->setFlash('error',"Data Gagal Disimpan. Silahkan Periksa Kembali Data Pelamar !");
                    }
                    
                      
                      
                }
                
		$this->render('create',array(
			'model'=>$model,
                        'modBahasa'=>$modBahasa,
                        'modLingkunganKerja'=>$modLingkunganKerja,
                        'modBahasas'=>$modBahasas,
                        'modLingkunganKerjas'=>$modLingkunganKerjas,
		));
	}

        
        private function cekSemua($model, $except=array()){
            $ada = false;
            foreach ($model as $counter => $value) {
                if (!in_array($counter,$except)){
                    if (!empty($value)){
                        $ada = true;
                        break;
                    }
                }
            }
            return $ada;
        }
        
        private function validasiTabularBahasa($modBahasa){
            $modBahasas = null;
            $result = array();
            $bahasa = false;
            if(count($modBahasa)>0){
                foreach ($modBahasa as $key => $modelBahasa) {
                    $ada = $this->cekSemua($modelBahasa,array('no_urut'));
                    if($ada){
                        $modBahasas[$key] = new KPKemampuanBahasaR;
                        $modBahasas[$key]->attributes  = $modelBahasa;
                        $bahasa = true;
                        $modBahasas[$key]->validate();
                    }
                    
                }
                sort($modBahasas);
            }
            $result['modBahasa'] = $modBahasas;
            $result['bahasa'] = $bahasa;
            return $result;
        }
        
        private function validasiTabularLingkunganKerja($modLingkunganKerja){
            $modLingkunganKerjas = null;
            $result = array();
            $lingkunganKerja = false;
            if(count($modLingkunganKerja)>0){
                foreach ($modLingkunganKerja as $key => $modelLingkunganKerja) {
                    $ada = $this->cekSemua($modelLingkunganKerja, array('nourut'));
                    if($ada){
                        $modLingkunganKerjas[$key] = new KPLingkunganKerjaR;
                        $modLingkunganKerjas[$key]->attributes  = $modelLingkunganKerja;
                        $lingkunganKerja = true;
                        $modLingkunganKerjas[$key]->validate();
                    }
                }
                sort($modLingkunganKerjas);
            }
            $result['modLingkunganKerja'] = $modLingkunganKerjas;
            $result['lingkunganKerja'] = $lingkunganKerja;
            return $result;
        }
        
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPelamarT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		
    $model=new KPInformasiPelamarV();
		$model->unsetAttributes();  // clear any default values
                
		if(isset($_GET['KPInformasiPelamarV']))
			$model->attributes=$_GET['KPInformasiPelamarV'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPPelamarT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='pelamar-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionKontrakPelamar($idPelamar=null, $idKaryawan=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$format = new CustomFormat;
                $modPegawai =new KPPegawaiM;
                
//              =================model kontrak pegawai
                $modKontrak = new KPKontrakKaryawanR;
                $criteria = new CDbCriteria();
                $criteria->select = 'max(nourutkontrak) as nourut';                         
                $nourut = KPKontrakKaryawanR::model()->find($criteria);      
                $modKontrak->nourutkontrak = $nourut->nourut+1;
                $modKontrak->create_time = date('Y-m-d');
                $modKontrak->create_loginpemakai_id = Yii::app()->user->id;
//              ===================end kontrak pegawai
                
                $thisdate = date('Y-m-d');
                $random = rand(00000, 99999);
                if(!empty($idPelamar)){
                    $pelamar = KPPelamarT::model()->findByPk($idPelamar);
                    $modPegawai->attributes = $pelamar->attributes;
                    $modPegawai->jenisidentitas  = $pelamar->jenisidentitas;
                    $modPegawai->noidentitas       = $pelamar->noidentitas;
                    $modPegawai->nama_pegawai     = $pelamar->nama_pelamar;
                    $modPegawai->nama_keluarga     = $pelamar->nama_keluarga;
                    $modPegawai->jeniskelamin    = $pelamar->jeniskelamin;
                    $modPegawai->tempatlahir_pegawai = $pelamar->tempatlahir_pelamar;
                    if(!empty($pelamar->tgl_lahirpelamar))
                        $modPegawai->tgl_lahirpegawai = date('d M Y',strtotime($pelamar->tgl_lahirpelamar));
                    else
                        $modPegawai->tgl_lahirpegawai = null;
                    $modPegawai->statusperkawinan = $pelamar->statusperkawinan;
    //                $modPegawai->jmlanak           = $pelamar->jmlanak;
                    $modPegawai->alamat_pegawai   = $pelamar->alamat_pelamar;
    //                $modPegawai->kodepos           = $pelamar->kodepos;
                    $modPegawai->agama           = $pelamar->agama;
                    $modPegawai->nomobile_pegawai = $pelamar->nomobile_pelamar;
                    $modPegawai->notelp_pegawai   = $pelamar->notelp_pelamar;
                    $modPegawai->alamatemail       = $pelamar->alamatemail;
    //                $modPegawai->occupation_id     = $pelamar->occupation_id;
                    $modPegawai->pendidikan_id     = $pelamar->pendidikan_id;
                    $modPegawai->pendkualifikasi_id        = $pelamar->pendkualifikasi_id;
                    $modPegawai->warganegara_pegawai     = $pelamar->warganegara_pelamar;
                    $modPegawai->suku_id           = $pelamar->suku_id;    
    //                $modPegawai->tglmulaibekerja   = date('d M Y',  strtotime($pelamar->tglmulaibekerja));
                    $modPegawai->photopegawai    = $pelamar->photopelamar;
                }
                if(!empty($idKaryawan)){
                    $modPegawai = KPPegawaiM::model()->findByPk($idKaryawan);
                    $modKontrak = KPKontrakKaryawanR::model()->findByAttributes(array('pegawai_id'=>$modPegawai->pegawai_id));
                }
                
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KPPegawaiM']))
		{                     
                        $modPegawai->attributes=$_POST['KPPegawaiM'];
                        
                        $modPegawai->create_time = $thisdate;
                        $modPegawai->create_loginpemakai_id = Yii::app()->user->id;
                        $modPegawai->pegawai_aktif = TRUE;
//                        $modPegawai->pelamar_id = $idPelamar;
                        $modPegawai->create_ruangan = Yii::app()->user->getState('ruangan_id');
                        if(!empty($modPegawai->tgl_lahirpegawai))
                            $modPegawai->tgl_lahirpegawai = $format->formatDateMediumForDB($modPegawai->tgl_lahirpegawai);
                        else
                            $modPegawai->tgl_lahirpegawai = null;

                        if(!empty($modPegawai->tglberhenti))
                            $modPegawai->tglberhenti = $format->formatDateMediumForDB($modPegawai->tglberhenti);
                        else
                            $modPegawai->tglberhenti = null;
                        if(!empty($modPegawai->tglditerima))
                            $modPegawai->tglditerima = $format->formatDateMediumForDB($modPegawai->tglditerima);
                        else
                            $modPegawai->tglditerima = null;
                        //Menyesuaikan format tanggal untuk database & Set null untuk tanggal yang kosong
                        if(!empty($modPegawai->tgl_lahirpegawai))
                            $modPegawai->tgl_lahirpegawai = $format->formatDateMediumForDB($modPegawai->tgl_lahirpegawai);
                        else
                            $modPegawai->tgl_lahirpegawai = null;
                        if(!empty($modPegawai->tglditerima))
                            $modPegawai->tglditerima = $format->formatDateMediumForDB($modPegawai->tglditerima);
                        else
                            $modPegawai->tglditerima = null;

//                      =================upload photo pelamar
                        $modPegawai->photopegawai = CUploadedFile::getInstance($modPegawai, 'photopegawai');
                        $photoKaryawan= $modPegawai->photopegawai;
//                        $modPegawai->sign_image = CUploadedFile::getInstance($modPegawai, 'sign_image');
//                        $signImage =  $modPegawai->sign_image;
//                        echo "<pre>";
                        if(isset($_POST['KPKontrakKaryawanR'])){
                            $modKontrak->attributes = $_POST['KPKontrakKaryawanR'];
                            $modKontrak->create_ruangan = Yii::app()->user->getState('ruangan_id');

                        } 
                        if($modPegawai->validate()){  
//                          if($modPegawai->validate()){
                              
                            Yii::import("ext.EPhpThumb.EPhpThumb");

                            $thumb=new EPhpThumb();
                            $thumb->init(); //this is needed
                            
                            $adaphoto = true;
//                            ==================
                            if(!empty($pelamar->photopelamar)){
                                $modPegawai->photopegawai = $pelamar->photopelamar;
                                $file = Params::pathPelamarPhotosDirectory().$modPegawai->photopegawai;
                                $newFile_photo = Params::pathPegawaiDirectory().$modPegawai->photopegawai;
                                $newFileTumbs_photo = Params::pathPegawaiTumbsDirectory().$modPegawai->photopegawai;
                                copy($file,$newFile_photo);
                                copy($newFile_photo,$newFileTumbs_photo);
                                $adaphoto = false;
                            }
                            
                            if(!empty($modPegawai->photopegawai) && $adaphoto){
                                
                                $modPegawai->photopegawai = $random.$photoKaryawan;  
                                $fullimage_photo = Params::pathPegawaiDirectory().$modPegawai->photopegawai;
                                
                                $tumbsimage_photo = Params::pathPegawaiTumbsDirectory().$modPegawai->photopegawai;
                               
                                $photoKaryawan->saveAs($fullimage_photo);
                               
                                $thumb->create($fullimage_photo)
                                      ->resize(200,200) 
                                      ->save($tumbsimage_photo);
                             } 
//                            if(!empty($modPegawai->sign_image)){
//                               
//                                $modPegawai->sign_image = $random.$photoKaryawan;  
//                                $fullsign_image = '/var/www/simjk/data/images/pegawai/sign_images/'.$modPegawai->sign_image;
//                                
//                                $tumbsign_image = '/var/www/simjk/data/images/pegawai/sign_images/tumbs/kecil_'.$modPegawai->sign_image;
//                                
//                                $signImage->saveAs($fullsign_image);
//                                $thumb->create($fullsign_image)
//                                      ->resize(200,200) 
//                                      ->save($tumbsign_image);
//                            }    
                            
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    if($modPegawai->save()){
                                        $modKontrak->attributes = $_POST['KPKontrakKaryawanR'];  
                                        $modKontrak->pegawai_id = $modPegawai->pegawai_id;
                                        if($modKontrak->validate()){
                                            $modKontrak->tglkontrak = $format->formatDateMediumForDB($modKontrak->tglkontrak);
                                            $modKontrak->tglmulaikontrak = $format->formatDateMediumForDB($modKontrak->tglmulaikontrak);
                                            $modKontrak->tglakhirkontrak = $format->formatDateMediumForDB($modKontrak->tglakhirkontrak);
                                            if($modKontrak->save()){
//                                                $cekKendaraan = KendaraanR::model()->findAllByAttributes(array('pelamar_id'=>$modPegawai->pelamar_id));
//                                                $cekSim       = SimR::model()->findAllByAttributes(array('pelamar_id'=>$modPegawai->pelamar_id));
//                                                $pegawai_id = $modPegawai->pegawai_id;
//                                                $idPelamar = $modPegawai->pelamar_id;
//                                                
//                                                if(count($cekKendaraan)>0){
//                                                    foreach ($cekKendaraan as $key => $kendaraan) {
//                                                            $kendaraan->pegawai_id = $pegawai_id;
//                                                            $kendaraan->save();
//                                                    }
//                                                }
//                                                if(count($cekSim)>0){
//                                                    foreach ($cekSim as $key => $sim) {
//                                                        $sim->pegawai_id = $pegawai_id;
//                                                        $sim->save();
//                                                    }
//                                                }
                                              PelamarT::model()->updateByPk($idPelamar,array('pegawai_id'=>$modPegawai->pegawai_id));

                                                    $transaction->commit();
                                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                                    $this->redirect(array('kontrakPelamar','idKaryawan'=>$modPegawai->pegawai_id));
                                            }else{
                                                $transaction->rollback();
                                                 Yii::app()->user->setFlash('error',"Data gagal disimpan");
                                            }                                  

                                        }else{
                                             $transaction->rollback();
                                             Yii::app()->user->setFlash('error',"Data gagal disimpan");
                                        }
                                    }else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan");
                                    }
                                }  catch (Exception $e){
                                     Yii::app()->user->setFlash('error',"Data Gagal Disimpan". $e->getMessage());
                                     
                                }
                                
                                
                        }else{
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            
                        }
		}
		$this->render('kontrakPelamar',array(
			'modPegawai'=>$modPegawai, 'modKontrak'=>$modKontrak,
		));
	}
        
        public function actionPrint($id,$caraPrint)
        {
//            $model= new HRDPengcalpegawaiT;
//            $model->attributes=$_REQUEST['HRDPengcalpegawaiT'];
//            $this->layout = '//layouts/frameDialog';  
            $judulLaporan='Data Pelamar';
            
            $model          = KPPelamarT::model()->findByPk($id);
            $modBahasas     = KPKemampuanBahasaR::model()->findAllByAttributes(array('pelamar_id'=>$model->pelamar_id));
            $modLingkunganKerjas = KPLingkunganKerjaR::model()->findAllByAttributes(array('pelamar_id'=>$model->pelamar_id));
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'modBahasas'=>$modBahasas, 'modLingkunganKerjas'=>$modLingkunganKerjas));
            }
//            else if($caraPrint=='EXCEL') {
//                $this->layout='//layouts/printExcel';
//                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
//            }
//            else if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//                $mpdf = new MyPDF('',$ukuranKertasPDF); 
//                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//                $mpdf->WriteHTML($stylesheet,1);  
//                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
//                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
//                $mpdf->Output();
//            }                       
        }
        
        
}
