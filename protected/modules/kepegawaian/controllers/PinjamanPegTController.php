<?php

class PinjamanPegTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','informasi','detail'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id = null)
	{
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPinjamanPegT;
        $modPegawai = new KPPegawaiM();
        $modPinjamDetail = new KPPinjamPegDetT();
        $model->nopinjam = Generator::noPeminjaman();
        $model->persenpinjaman = 0;
        if(!empty($id)){
        	$model = KPPinjamanPegT::model()->findByPk($id);
        	$modPegawai = KPPegawaiM::model()->findByPk($model->pegawai_id);
        	$modPinjamDetail = KPPinjamPegDetT::model()->findAllbyAttributes(array('pinjamanpeg_id'=>$id));
        }

        $format = new CustomFormat();

		if(isset($_POST['KPPinjamanPegT'])||isset($_POST['KPPinjamPegDetT']))
		{
			$transaction = Yii::app()->db->beginTransaction();
            try {

			$model->attributes=$_POST['KPPinjamanPegT'];
            $model->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
            $idpegawai = $_POST['KPPegawaiM']['pegawai_id'];
            $model->tglpinjampeg = $format->formatDateMediumForDB($_POST['KPPinjamanPegT']['tglpinjampeg']);
            $model->nopinjam = Generator::noPeminjaman();

            //$modPegawai = KPPegawaiM::model()->findByPk($idpegawai);

            $model->sisapinjaman = 0;
            $params['komponengaji_id'] = Params::ID_KOMPONEN_GAJI;
            $model->komponengaji_id = $params['komponengaji_id'];
            
            $model->lamapinjambln = $_POST['KPPinjamanPegT']['lamapinjambln'];
            $angsuranke     = $_POST['angsuranke'];
            
            if(empty($_POST['angsuranke'])){
            	$transaction->rollback();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Anda belum men-generate tabel angsuran pembayaran.');
            }else{

    			if($model->save()){
                    $i = 1;
                    foreach ($angsuranke as $angsuran => $data) {
                        $modPinjamDetail = new KPPinjamPegDetT();
                        $tgl[$i]            = $_POST['tglakanbayar'][$i];
                        $cicilan[$i]        = $_POST['jmlcicilan'][$i];
                        $modPinjamDetail->angsuranke        = $data;
                        $tgla                               = date_create($tgl[$i]);
                        $tgl_db                             = date_format($tgla, 'Y-m-d');
                        // $modPinjamDetail->tglakanbayar      = $tgl_db;
                        $modPinjamDetail->tglakanbayar      = $tgl[$i];
                        $modPinjamDetail->jmlcicilan        = $cicilan[$i];  
                        $modPinjamDetail->pinjamanpeg_id    = $model->pinjamanpeg_id;
                        $modPinjamDetail->bulan             = substr($tgl[$i], 5,2);
                        $modPinjamDetail->tahun             = substr($tgl[$i], 0,4);

                        $modPinjamDetail->save();
                        $i++;
                    }

                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
    				$model->isNewRecord = False;
    				$transaction->commit();
    				$this->redirect(array('create','id'=>$model->pinjamanpeg_id));
                } else {
                	$transaction->rollback();
                	Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');
                }
            }

        	}
        	catch (Exception $e)
            {
                $transaction->rollback();
      	        Yii::app()->user->setFlash('error',"<strong>Gagal!</strong> Data Gagal Disimpan".MyExceptionMessage::getMessage($e,true));
            }
		}

		$this->render('create',array(
			'model'=>$model, 'modPegawai'=>$modPegawai, 'modPinjamDetail'=>$modPinjamDetail,
		));
	}

	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPinjamanPegT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
        
        
	public function actionAdmin()
	{        
		$model=new KPPinjamanPegT('search');
        $model->tglpinjampeg = date('Y-m-d');
        //$model->tglpresensi_akhir = date('Y-m-d 23:59:59');
		// $model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPinjamanPegT'])){
			$model->attributes=$_GET['KPPinjamanPegT'];

            $format = new CustomFormat();
            $model->tglpinjampeg = $format->formatDateTimeMediumForDB($model->tglpinjampeg); 
        }

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
    public function actionPrint()
    {
        
        $idpinjaman = $_GET['id'];

        $model = KPPinjamanPegT::model()->findByPk($idpinjaman);
        $idpegawai = $model->pegawai_id;

        $modPegawai = KPPegawaiM::model()->findByPk($idpegawai);
        $model->attributes=$_REQUEST['KPPinjamanPegT'];

        // $modPinjamDetail = KPPinjamPegDetT::model()->findByAttributes(array('pinjamanpeg_id'=>$idpinjaman),array('order'=>'angsuranke'));
        // echo"<pre>";
        // print_r($modPinjamDetail->attributes);
        // exit();          

        $judulLaporan='Data Peminjaman';
        $caraPrint=$_REQUEST['caraPrint'];
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('Print',array('model'=>$model, 'modPegawai'=>$modPegawai, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('Print',array('model'=>$model,'modPegawai'=>$modPegawai, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model, 'modPegawai'=>$modPegawai, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }                       
    }

    public function actionInformasi()
    {
        $model = new KPPinjamanPegT('searchInformasi');
        $model->unsetAttributes();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if(isset($_GET['KPPinjamanPegT'])){
            $model->attributes=$_GET['KPPinjamanPegT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['KPPinjamanPegT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['KPPinjamanPegT']['tglAkhir']);
        }

        $this->render('informasi',array(
            'model'=>$model,
        ));
    }

    public function actionDetail($id)
    {
        $this->layout = '//layouts/frameDialog';
        $modPinjaman = KPPinjamanPegT::model()->findByPk($id);
        if(count($modPinjaman)>0){
            $modDetailPinjam = KPPinjamPegDetT::model()->findAllByAttributes(array('pinjamanpeg_id'=>$id));
            $this->render('detailInformasi', array(
                'modpinjaman' => $modPinjaman,
                'moddetailpinjam' => $modDetailPinjam,
            ));
        }
    }
}
