<?php
    class LaporanController extends SBaseController{
        public $layout='//layouts/column1';
        
        public function actionLaporanPegawai()
        {
            $model = new PegawaiM();
            if(isset($_GET['PegawaiM']))
            {     
                $model->attributes = $_GET['PegawaiM'];
                $model->tglpresensi = $_GET['PegawaiM']['tglpresensi'];
                $model->tglpresensi_akhir = $_GET['PegawaiM']['tglpresensi_akhir'];

            }
            $this->render('daftarHadir/index',array(
                'model'=>$model,
            ));            
        }
        
        public function actionDetailLaporanAbsen($id)
        {
            $this->layout='//layouts/frameDialog';
            $model = new KPPresensiT('detailPresensi');
            $model->pegawai_id = $id;
            $model->tglpresensi = date('Y-m-d 00:00:00');
            $model->tglpresensi_akhir = date('Y-m-d 23:59:59');
            
            /*
            $model->tglpresensi = date('Y-m-d', strtotime('2013-05-25'));
            $model->tglpresensi_akhir = date('Y-m-d');
            
            if(isset($_GET['PresensiT']))
            {
                $criteria->addBetweenCondition('DATE(tglpresensi)', $_GET['PresensiT']['tglpresensi'], $_GET['PresensiT']['tglpresensi_akhir']);
            }
             * 
             */
            
            if(isset($_GET['KPPresensiT']))
            {   
                $format = new CustomFormat();
                $tglpresensi = $format->formatDateTimeMediumForDB($_GET['KPPresensiT']['tglpresensi']);
                $tglpresensi_akhir = $format->formatDateTimeMediumForDB($_GET['KPPresensiT']['tglpresensi_akhir']);
                $model->tglpresensi = $tglpresensi;
                $model->tglpresensi_akhir = $tglpresensi_akhir;
            }
            
            $modPegawai = KPPegawaiM::model()->findByPk($id);
            $this->render('daftarHadir/detailAbdesensi',array(
                'model'=>$model,
                'modPegawai' => $modPegawai
            ));            
        }        
        
        public function actionPrintDetailLaporanPresensi($id)
        {
            $model = new KPPresensiT('detailPresensi');
            $model->pegawai_id = $id;
            $modPegawai = KPPegawaiM::model()->findByPk($id);
            /*
            if(isset($_GET['PresensiT']))
            {
                $criteria->addBetweenCondition('DATE(tglpresensi)', $_GET['PresensiT']['tglpresensi'], $_GET['PresensiT']['tglpresensi_akhir']);
            }
             * 
             */
            $modPegawai = KPPegawaiM::model()->findByPk($id);
            
            $judulLaporan = 'Laporan Presensi Per Karyawan';
            $caraPrint = $_REQUEST['caraPrint'];
            $periode = null;
            if(isset($_REQUEST['tglpresensi']))
            {   
                $tglpresensi = date('Y-m-d ', strtotime($_REQUEST['tglpresensi']));
                $tglpresensi_akhir = date('Y-m-d ', strtotime($_REQUEST['tglpresensi_akhir']));
                $model->tglpresensi = $tglpresensi;
                $model->tglpresensi_akhir = $tglpresensi_akhir;
            }
            
            if($caraPrint == 'PRINT')
            {
                $this->layout='//layouts/printWindows';
                $this->render(
                    'daftarHadir/_printWindows',
                    array(
                        'model'=>$model,
                        'modPegawai'=>$modPegawai,
                        'periode'=>$periode,
                        'judulLaporan'=>$judulLaporan,
                        'caraPrint'=>$caraPrint
                    )
                );
            }
            else if($caraPrint=='EXCEL')
            {
                $this->layout = '//layouts/printExcel';
                $judulLaporan = $modPegawai->nama_pegawai;
                $this->render(
                    'daftarHadir/_printExel',
                    array(
                        'model'=>$model,
                        'modPegawai'=>$modPegawai,
                        'periode'=>$periode,
                        'judulLaporan'=>$judulLaporan,
                        'caraPrint'=>$caraPrint
                    )
                );
            }
            else if($_REQUEST['caraPrint'] == 'PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); // Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',5,5,5,5,5,5);
                $mpdf->WriteHTML(
                    $this->renderPartial(
                        'daftarHadir/_printPdf',
                        array(
                            'model'=>$model,
                            'modPegawai'=>$modPegawai,
                            'periode'=>$periode,
                            'judulLaporan'=>$judulLaporan,
                            'caraPrint'=>$caraPrint
                        ),true
                    )
                );
                $mpdf->Output();
            }
        }
        
        public function actionLaporanPresensi()
	{
            $model = new KPPresensiT('search');
            $model->tglpresensi = date('Y-m-d 00:00:00');
            $model->tglpresensi_akhir = date('Y-m-d 23:59:59');
            if (isset($_GET['KPPresensiT'])) {
                $format = new CustomFormat();
                $model->attributes = $_GET['KPPresensiT'];
                $tglpresensi = $format->formatDateMediumForDB($_GET['KPPresensiT']['tglpresensi']);
                $tglpresensi_akhir = $format->formatDateMediumForDB($_GET['KPPresensiT']['tglpresensi_akhir']);
                $tglpresensi = date('Y-m-d ', strtotime($_GET['KPPresensiT']['tglpresensi']));
                $tglpresensi_akhir = date('Y-m-d ', strtotime($_GET['KPPresensiT']['tglpresensi_akhir']));
                $model->tglpresensi = $tglpresensi;
                $model->tglpresensi_akhir = $tglpresensi_akhir;
                $model->ruangan_id = $_GET['KPPresensiT']['ruangan_id'];
                $model->unit_perusahaan = $_GET['KPPresensiT']['unit_perusahaan'];
            }
            $this->render('presensiT/_laporanpresensi',array(
                'model'=>$model,
            ));
	}
        
        public function actionPrintLaporanPresensi()
        {
            $model= new KPPresensiT('search');
            $model->tglpresensi = date('Y-m-d 00:00:00');
            $model->tglpresensi_akhir = date('Y-m-d 23:59:59');
            $format = new CustomFormat();
            $model->attributes = $_GET['KPPresensiT'];
            $model->tglpresensi = date('Y-m-d ', strtotime($_GET['KPPresensiT']['tglpresensi']));
            $model->tglpresensi_akhir = date('Y-m-d ', strtotime($_GET['KPPresensiT']['tglpresensi_akhir']));
            $model->unit_perusahaan = $_GET['KPPresensiT']['unit_perusahaan'];
            
            $judulLaporan = 'Laporan Presensi';
            $caraPrint = $_REQUEST['caraPrint'];
            $periode = $this->parserTanggal($model->tglpresensi).' s/d '.$this->parserTanggal($model->tglpresensi_akhir);
            
            if($caraPrint == 'PRINT')
            {
                $this->layout = '//layouts/printWindows';
                $this->render('presensiT/Print',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint == 'EXCEL') {
                $this->layout = '//layouts/printExcel';
                $this->render('presensiT/Print',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint'] == 'PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); // Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('presensiT/Print',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                    
        }
                
        public function actionFramePresensi() 
        {
            $this->layout = '//layouts/frameDialog';

            $model = new PresensiT;
            $model->tglpresensi = date('Y-m-d 00:00:00');
            $model->tglpresensi_akhir = date('Y-m-d 23:59:59');

            //Data Grafik
            $data['title'] = 'Grafik Presensi';
            $data['type'] = $_GET['type'];

            if (isset($_REQUEST['PresensiT'])) {
                    $format = new CustomFormat;
                    $model->attributes = $_GET['PresensiT'];
                    $tglpresensi = date('Y-m-d ', strtotime($_GET['PresensiT']['tglpresensi']));
                            $tglpresensi_akhir = date('Y-m-d ', strtotime($_GET['PresensiT']['tglpresensi_akhir']));
                            
                            $model->tglpresensi = $tglpresensi;
                            $model->tglpresensi_akhir = $tglpresensi_akhir;
            }
            $searchdata = $model->searchPresensiGrafik();
            $this->render('_grafik', array(
                'model' => $model,
                'data' => $data,
                'searchdata'=>$searchdata,
            ));
        }
        
        public function actionLaporanPenggajian()
        {
            $model = new KPPenggajianpegT('search');
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:59');
			
            if (isset($_GET['KPPenggajianpegT']))
			{
                $format = new CustomFormat;
                $model->attributes = $_GET['KPPenggajianpegT'];
                $model->nama_pegawai = $_GET['KPPenggajianpegT']['nama_pegawai'];
                $model->jabatan_id = $_GET['KPPenggajianpegT']['jabatan_id'];
                $model->kategoripegawai = $_GET['KPPenggajianpegT']['kategoripegawai'];
                $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['KPPenggajianpegT']['tglAwal']);
                $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['KPPenggajianpegT']['tglAkhir']);
            }
            $this->render('penggajianpegT/index',array(
                'model'=>$model,
            ));
        }
        
        public function actionPrintLaporanPenggajian()
                {
                    $model= new KPPenggajianpegT('search');
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    
                    $format = new CustomFormat;
                    if (isset($_GET['KPPenggajianpegT'])) {
                        $model->nama_pegawai = $_GET['KPPenggajianpegT']['nama_pegawai'];
                        $model->jabatan_id = $_GET['KPPenggajianpegT']['jabatan_id'];
                        $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['KPPenggajianpegT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['KPPenggajianpegT']['tglAkhir']);
                    }
                    $caraPrint=$_REQUEST['caraPrint'];
                    $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
                    $judulLaporan='Laporan Penggajian';
                    if($caraPrint=='PRINT') {
                        $this->layout='//layouts/printWindows';
                        $this->render('penggajianpegT/Print',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($caraPrint=='EXCEL') {
                        $this->layout='//layouts/printExcel';
                        $this->render('penggajianpegT/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
                    }
                    else if($_REQUEST['caraPrint']=='PDF') {
                        $ukuranKertasPDF = Yii::app()->session['ukuran_kertas'];                  // Ukuran Kertas Pdf
                        $posisi = Yii::app()->session['posisi_kertas'];                                      // Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('',$ukuranKertasPDF); 
                        $mpdf->useOddEven = 2;  
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet,1);
                        $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                        $mpdf->WriteHTML($this->renderPartial('penggajianpegT/Print',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                        $mpdf->Output();
                    }                    
                }


    public function actionLaporanPajak(){
        $model= new Laporanpph21V('search');
        $format = new CustomFormat;
        $bulan  = $format->formatBulanInaLengkap(date('M'));
        $model->periodegaji_awal = $bulan.' '.date('Y');

        $bulan_angka    = $format->formatAngkaBulan($bulan);
        $periode_gaji   = date('Y').'-'.$bulan_angka.'-01';
        $model->periodegaji = $periode_gaji;
        
        if(isset($_GET['Laporanpph21V']))
        {
            $model->attributes=$_REQUEST['Laporanpph21V'];
            $model->nomorindukpegawai = $_GET['Laporanpph21V']['nomorindukpegawai'];
            $periode        = explode(' ', $_REQUEST['Laporanpph21V']['periodegaji_awal']);
            $periode_bulan  = $periode[0];
            $periode_tahun  = $periode[1];
            $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
            $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

            $model->periodegaji = $periode_gaji;

        }
        $this->render('pajakT/_laporanpajak',array(
            'model'=>$model,
        ));

    }


    public function actionPrintLaporanPajak(){
        $model= new Laporanpph21V('search');
        $format = new CustomFormat;
        $bulan  = $format->formatBulanInaLengkap(date('M'));
        $model->periodegaji_awal = $bulan.' '.date('Y');

        $bulan_angka    = $format->formatAngkaBulan($bulan);
        $periode_gaji   = date('Y').'-'.$bulan_angka.'-01';
        $model->periodegaji = $periode_gaji;
        $periode_pajak  = $model->periodegaji_awal;

        if(isset($_GET['Laporanpph21V']))
        {
           $model->attributes = $_REQUEST['Laporanpph21V'];
           $model->nomorindukpegawai = $_GET['Laporanpph21V']['nomorindukpegawai'];
           $periode        = explode(' ', $_REQUEST['Laporanpph21V']['periodegaji_awal']);
           $periode_bulan  = $periode[0];
           $periode_tahun  = $periode[1];
           $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
           $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

           $model->periodegaji = $periode_gaji;

           $periode_pajak = $_REQUEST['Laporanpph21V']['periodegaji_awal'];
        }
       
        $caraPrint = $_REQUEST['caraPrint'];
       
        $judulLaporan = "Laporan Pajak";

        if($caraPrint == 'PRINT')
        {
            $this->layout = '//layouts/printWindows';
            $this->render('pajakT/Print',array('model'=>$model,'periode'=>$periode_pajak,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') 
        {
            $this->layout='//layouts/printExcel';
            $this->render('pajakT/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
        }
         else if($_REQUEST['caraPrint'] == 'PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas'); // Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas'); //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('pajakT/Print',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }      
   } 


//        protected function parserTanggal($tglpresensi){
//                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($tgl, 'yyyy-MM-dd hh:mm:ss'));
//
//                }
                
                protected function parserTanggal($tgl){
                    $tgl = explode(' ', $tgl);
                    $result = array();
                    foreach ($tgl as $row){
                        if (!empty($row)){
                            $result[] = $row;
                        }
                    }
                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

                }


        public function actionRekapPenggajian()
        {
            // $model = new LaporanrekapitulasigajiV('search');
            $model = new LaporanrekapitulasigajiV('search');
            $model->periodegaji = date('Y-m').'-01';
            $format = new CustomFormat;
            $periode_bulan  = date('m');
            $periode_tahun  = date('Y');
            $bulan    = $format->formatBulanInaLengkap($periode_bulan);
            $model->periodegaji_view = $bulan.' '.$periode_tahun;
            if (isset($_GET['LaporanrekapitulasigajiV'])) {

                $model->attributes = $_GET['LaporanrekapitulasigajiV'];
                
                $periode     = explode(' ', $_GET['LaporanrekapitulasigajiV']['periodegaji_view']);
                $periode_bulan  = $periode[0];
                $periode_tahun  = $periode[1];
                $bulan_angka    = $format->formatAngkaBulan($periode_bulan);
                $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

                $model->periodegaji = $periode_gaji;                
                $model->nama_pegawai = $_GET['LaporanrekapitulasigajiV']['nama_pegawai'];
                // $model->instalasi_id = $_GET['LaporanrekapitulasigajiV']['instalasi_id'];
                // $model->ruangan_id = $_GET['LaporanrekapitulasigajiV']['ruangan_id'];
            }
            $this->render('rekapGaji/index',array(
                'model'=>$model,
            ));
        }

        public function actionPrintRekapPenggajian()
        {
            $model= new LaporanrekapitulasigajiV('search');
            $model->periodegaji = date('Y-m').'-01';
            $format = new CustomFormat;
            $periode_bulan  = date('m');
            $periode_tahun  = date('Y');
            $bulan    = $format->formatBulanInaLengkap($periode_bulan);
            $model->periodegaji_view = $bulan.' '.$periode_tahun;
            
            if (isset($_GET['LaporanrekapitulasigajiV'])) {
                $periode     = explode(' ', $_GET['LaporanrekapitulasigajiV']['periodegaji_view']);
                $bulan  = $periode[0];
                $periode_tahun  = $periode[1];
                $bulan_angka    = $format->formatAngkaBulan($bulan);
                $periode_gaji   = $periode_tahun.'-'.$bulan_angka.'-01';

                $model->periodegaji = $periode_gaji;                
                $model->nama_pegawai = $_GET['LaporanrekapitulasigajiV']['nama_pegawai'];
                $model->instalasi_id = $_GET['LaporanrekapitulasigajiV']['instalasi_id'];
                $model->ruangan_id = $_GET['LaporanrekapitulasigajiV']['ruangan_id'];
            }
            $caraPrint=$_REQUEST['caraPrint'];
            $periode = $bulan.' '.$periode_tahun;
            $judulLaporan='Rekap Penggajian Pegawai';
            if($caraPrint=='PRINT') {
                $model->print = true;
                $this->layout='//layouts/printWindows';
                $this->render('rekapGaji/Print',array('model'=>$model,'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $model->print = true;
                $this->layout='//layouts/printExcel';
                $this->render('rekapGaji/Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'periode'=>$periode));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $model->print = true;
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = 'L';                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('', $ukuranKertasPDF);
                $mpdf->useOddEven = 2;
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet, 1);
                $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
                $mpdf->tMargin=5;
                $mpdf->WriteHTML($this->renderPartial('rekapGaji/Print',array('model'=>$model, 'periode'=>$periode,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                    
        }

        public function actionLaporantransferbjbs() {
        $model = new KPLaporantransferbjbsV('searchLaporantransferbjbs');
        $format = new CustomFormat();
        $model->jns_periode = "hari";
        $model->tglAwal = date('Y-m-d', strtotime('first day of this month'));
        $model->tglAkhir = date('Y-m-d');
        $model->bln_awal = date('Y-m', strtotime('first day of january'));
        $model->bln_akhir = date('Y-m');
        $model->thn_awal = date('Y');
        $model->thn_akhir = date('Y');

        $model->tglAwal = $format->formatDateINAShort(date('Y-m-d',(strtotime($model->tglAwal))));
            $model->tglAkhir = $format->formatDateINAShort(date('Y-m-d',(strtotime($model->tglAkhir))));
            $model->bln_awal = $format->formatMonthINAShort(date('Y-m',(strtotime($model->bln_awal))));
            $model->bln_akhir = $format->formatMonthINAShort(date('Y-m',(strtotime($model->bln_akhir))));
        
        if (isset($_GET['KPLaporantransferbjbsV'])) {
            $model->attributes = $_GET['KPLaporantransferbjbsV'];
            $model->jns_periode = $_GET['KPLaporantransferbjbsV']['jns_periode'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['KPLaporantransferbjbsV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['KPLaporantransferbjbsV']['tglAkhir']);
            $model->bln_awal = $format->formatMonthMediumForDB($_GET['KPLaporantransferbjbsV']['bln_awal']);
            $model->bln_akhir = $format->formatMonthMediumForDB($_GET['KPLaporantransferbjbsV']['bln_akhir']);
            $bln_akhir = $model->bln_akhir."-".date("t",strtotime($model->bln_akhir));
            $model->thn_awal = $format->formatDateMediumForDB($_GET['KPLaporantransferbjbsV']['thn_awal']);
            $model->thn_akhir = $format->formatDateMediumForDB($_GET['KPLaporantransferbjbsV']['thn_akhir']);
            $thn_akhir = $model->thn_akhir."-".date("m-t",strtotime($model->thn_akhir."-12"));
            switch($model->jns_periode){
                case 'bulan' : $model->tglAwal = $model->bln_awal."-01"; $model->tglAkhir = $bln_akhir; break;
                case 'tahun' : $model->tglAwal = $model->thn_awal."-01-01"; $model->tglAkhir = $thn_akhir; break;
                default : null;
            }
            $model->tglAwal = $model->tglAwal." 00:00:00";
            $model->tglAkhir = $model->tglAkhir." 23:59:59";
        }

            

        if (Yii::app()->request->isAjaxRequest) {
                    echo $this->render('transferbjbsV/admin', array('model'=>$model),true);
                }else{

                   $this->render('transferbjbsV/admin', array(
                    'model' => $model
                ));

                }
       
    }

    public function actionPrintLaporantransferbjbs() {
        $model = new KPLaporantransferbjbsV('search');
        $judulLaporan = 'Laporan Transfer BJBS';

        //Data Grafik        
        $data['title'] = 'Laporan Transfer BJBS';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['KPLaporantransferbjbsV'])) {
            $model->attributes = $_REQUEST['KPLaporantransferbjbsV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['KPLaporantransferbjbsV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['KPLaporantransferbjbsV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'transferbjbsV/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporantransferbjbs() {
        $this->layout = '//layouts/frameDialog';
        $model = new KPLaporantransferbjbsV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Laporan Transfer BJBS';
        $data['type'] = $_GET['type'];
        if (isset($_GET['KPLaporantransferbjbsV'])) {
            $model->attributes = $_GET['KPLaporantransferbjbsV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['KPLaporantransferbjbsV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['KPLaporantransferbjbsV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }


    public function actionLaporanTransferBRI() {
        $model = new KPLaporantransferbriV('searchLaporantransferbjbs');
        $format = new CustomFormat();
        $model->jns_periode = "hari";
        $model->tglAwal = date('Y-m-d', strtotime('first day of this month'));
        $model->tglAkhir = date('Y-m-d');
        $model->bln_awal = date('Y-m', strtotime('first day of january'));
        $model->bln_akhir = date('Y-m');
        $model->thn_awal = date('Y');
        $model->thn_akhir = date('Y');

        $model->tglAwal = $format->formatDateINAShort(date('Y-m-d',(strtotime($model->tglAwal))));
            $model->tglAkhir = $format->formatDateINAShort(date('Y-m-d',(strtotime($model->tglAkhir))));
            $model->bln_awal = $format->formatMonthINAShort(date('Y-m',(strtotime($model->bln_awal))));
            $model->bln_akhir = $format->formatMonthINAShort(date('Y-m',(strtotime($model->bln_akhir))));
        
        if (isset($_GET['KPLaporantransferbriV'])) {
            $model->attributes = $_GET['KPLaporantransferbriV'];
            $model->jns_periode = $_GET['KPLaporantransferbriV']['jns_periode'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['KPLaporantransferbriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['KPLaporantransferbriV']['tglAkhir']);
            $model->bln_awal = $format->formatMonthMediumForDB($_GET['KPLaporantransferbriV']['bln_awal']);
            $model->bln_akhir = $format->formatMonthMediumForDB($_GET['KPLaporantransferbriV']['bln_akhir']);
            $bln_akhir = $model->bln_akhir."-".date("t",strtotime($model->bln_akhir));
            $model->thn_awal = $format->formatDateMediumForDB($_GET['KPLaporantransferbriV']['thn_awal']);
            $model->thn_akhir = $format->formatDateMediumForDB($_GET['KPLaporantransferbriV']['thn_akhir']);
            $thn_akhir = $model->thn_akhir."-".date("m-t",strtotime($model->thn_akhir."-12"));
            switch($model->jns_periode){
                case 'bulan' : $model->tglAwal = $model->bln_awal."-01"; $model->tglAkhir = $bln_akhir; break;
                case 'tahun' : $model->tglAwal = $model->thn_awal."-01-01"; $model->tglAkhir = $thn_akhir; break;
                default : null;
            }
            $model->tglAwal = $model->tglAwal." 00:00:00";
            $model->tglAkhir = $model->tglAkhir." 23:59:59";
        }

            

        if (Yii::app()->request->isAjaxRequest) {
                    echo $this->render('transferbriV/admin', array('model'=>$model),true);
                }else{

                   $this->render('transferbriV/admin', array(
                    'model' => $model
                ));

                }
       
    }

    public function actionPrintLaporanTransferBRI() {
        $model = new KPLaporantransferbriV('search');
        $judulLaporan = 'Laporan Transfer BRI';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['KPLaporantransferbriV'])) {
            $model->attributes = $_REQUEST['KPLaporantransferbriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['KPLaporantransferbriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['KPLaporantransferbriV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'transferbriV/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTransferBRI() {
        $this->layout = '//layouts/frameDialog';
        $model = new KPLaporantransferbriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['KPLaporantransferbriV'])) {
            $model->attributes = $_GET['KPLaporantransferbriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['KPLaporantransferbriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['KPLaporantransferbriV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }


    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
             $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    }
?>