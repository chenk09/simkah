<?php

class PenggajianpegTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','printrincian','printamplop'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','informasi','DetailPenggajian','PrintPenggajian', 'ambilTunjangan', 'ambilPph', 'ambilGapok'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		//$model 	=new KPPenggajianpegT;
               $modPegawai = new KPPegawaiM();
               $model = new KPPenggajianpegT;  
            
		$format = new CustomFormat;
		$bulan 	= $format->formatBulanInaLengkap(date('M'));
        $model->tglpenggajian = date('Y-m-d H:i:s');
        $model->periodegaji = $bulan.' '.date('Y');
        $model->nopenggajian = Generator::noPenggajian();
        $model->penerimaanbersih= 0;
        $model->totalpajak = 0;
        $model->totalpotongan = 0;
        $model->totalterima = 0;
		$komponen = new PenggajiankompT();  
       
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		if (!empty($id)) {
            $model = KPPenggajianpegT::model()->find('penggajianpeg_id = ' . $id . '');
            $modPegawai = KPPegawaiM::model()->find('pegawai_id = ' . $model->pegawai_id . '');
        }

		if(isset($_POST['KPPenggajianpegT']))
		{
			$model->attributes=$_POST['KPPenggajianpegT'];
            $model->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
            $model->pkp 	= $_POST['KPPenggajianpegT']['pkp'];
            $model->gajipertahun 	= $_POST['KPPenggajianpegT']['gajipph'];
            $model->biayajabatan 	= $_POST['KPPenggajianpegT']['biayajabatan'];
            $model->potonganpensiun	= $_POST['KPPenggajianpegT']['iuranpensiun'];
            $model->ptkppertahun	= $_POST['KPPenggajianpegT']['ptkp'];
            $model->pph21pertahun	= $_POST['KPPenggajianpegT']['pphpersen'];
            $model->pph21perbulan	= $_POST['KPPenggajianpegT']['pph21'];
            $model->persentasepph21 = $_POST['KPPenggajianpegT']['persentasepph21'];
            $model->kodeptkp 		= $_POST['KPPenggajianpegT']['kodeptkp'];
            $model->penerimaanbersihpertahun	= $_POST['KPPenggajianpegT']['penerimaanpph'];

 			$periode		= explode(' ', $_POST['KPPenggajianpegT']['periodegaji']);
    		$periode_bulan  = $periode[0];
    		$periode_tahun  = $periode[1];
    		$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
    		$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';

            $model->periodegaji = $periode_gaji;
            $model->tglpenggajian = $format->formatDateTimeMediumForDB($_POST['KPPenggajianpegT']['tglpenggajian']);


            $cekValidasi = KPPenggajianpegT::model()->findByAttributes(array('pegawai_id'=>$model->pegawai_id, 'periodegaji'=>$periode_gaji));

            if(count($cekValidasi)>0){
            	Yii::app()->user->setFlash('error', '<strong>Penggajian sudah dilakukan!</strong> Silahkan ulangi kembali dengan periode selanjutnya.');
            }else{

            
            
         	// echo print_r($model->attributes);
            $data = $_POST['PenggajiankompT']['komponengaji_id'];

			$transaction = Yii::app()->db->beginTransaction();
            try{
                if($model->save()){
                     if (count($data) > 0){
                         $jumlah = 0;
                         foreach ($data as $i=>$v){
                             $row = new PenggajiankompT();
                             $row->komponengaji_id = $i;
                             $row->jumlah = $v;
                             $row->penggajianpeg_id = $model->penggajianpeg_id;
                             if ($row->save()){
                             	if($i==31 && $v > 0){
                             		$this->saveBuktiBayar($model, $_POST['KPPegawaiM'], $row->jumlah);
                             	}
                                 $jumlah++;
                             }
                         }
                    }
                }
                
                if ((count($data) > 0) && ($jumlah == count($data))){
                	// throw new Exception('Data tidak valid');
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                    $model->isNewRecord = FALSE;
                    $this->redirect(array('create','id'=>$model->penggajianpeg_id,'modPegawai'=>$model->pegawai_id));
                }
                else{
                    throw new Exception('Data tidak valid');
                }
            }
            catch(Exception $ex){
                $transaction->rollback();
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
            }
		}

	}

		$this->render('create',array(
			'model'=>$model, 'modPegawai'=>$modPegawai, 'komponen'=>$komponen
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($updateid)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model = $this->loadModel($updateid);
		$modpegawai = KPPegawaiM::model()->find('pegawai_id = ' . $model->pegawai_id . '');
		// $komponen = PenggajiankompT::model()->findAllByAttributes(array('penggajianpeg_id'=>$model->penggajianpeg_id));
		$modpegawai->lamakerja = $modpegawai->LamaKerjaTanpaMasaKerja;
		$komponen = new PenggajiankompT();
		$format = new CustomFormat;
		$periode = explode('-', $model->periodegaji);
		$periode_bulan  = $periode[1];
		$periode_tahun  = $periode[0];
		$bulan    = $format->formatBulanInaLengkap($periode_bulan);

		$conditions = "periodeharikerjaawl <= '".$model->periodegaji."' AND periodeharikerjaakhir >= '".$model->periodegaji."' AND kelompokpegawai_id='".$modpegawai->kelompokpegawai_id."' AND harikerjagol_aktif=TRUE ";
		$criteria 		= new CDbCriteria;
		$criteria->addCondition($conditions);
		$modHariKerja 	= HariKerjaGolM::model()->findAll($criteria);
		
		foreach ($modHariKerja as $key => $harikerja) {
			$tgl_awal 	= $harikerja->periodeharikerjaawl;
			$tgl_akhir 	= $harikerja->periodeharikerjaakhir;
			$jml_hari	= $harikerja->jmlharibln;
			$criteria=new CDbCriteria;
	        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
	        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
	        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
	        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
	        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
	        $criteria->compare('t.pegawai_id',$model->pegawai_id);
	        $criteria->compare('t.statuskehadiran_id',Params::STATUSHADIR);
	        $jumlah 	= PresensiT::model()->findAll($criteria);

			$jmlhadir 	= count($jumlah);

			$criteria=new CDbCriteria;
	        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
	        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
	        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
	        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
	        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
	        $criteria->compare('t.pegawai_id',$pegawai_id);
	        $criteria->compare('t.statuskehadiran_id',Params::STATUSCUTI);
	        $jumlah_cuti 	= PresensiT::model()->findAll($criteria);
	        $jmlcuti 		= count($jumlah_cuti);

			$criteria=new CDbCriteria;
	        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
	        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
	        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
	        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
	        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
	        $criteria->compare('t.pegawai_id',$pegawai_id);
	        $criteria->compare('t.statuskehadiran_id',Params::STATUSIJIN);
	        $jumlah_ijin 	= PresensiT::model()->findAll($criteria);
	        $jmlijin 		= count($jumlah_ijin);

			$criteria=new CDbCriteria;
	        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
	        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
	        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
	        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
	        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
	        $criteria->compare('t.pegawai_id',$pegawai_id);
	        $criteria->compare('t.statuskehadiran_id',Params::STATUSALPHA);
	        $jumlah_alpha 	= PresensiT::model()->findAll($criteria);
	        $jmlalpha 		= count($jumlah_alpha);
		}	
		$model->jmlefektifharikerja = $jml_hari;
		$model->harikerja = $jmlhadir;
		$model->ijin = $jmlijin;
		$model->alpha = $jmlalpha;
		$model->cuti = $jmlcuti;
		$model->periodegaji = $bulan.' '.$periode_tahun;

		if(isset($_POST['KPPenggajianpegT']))
		{
			$model->attributes=$_POST['KPPenggajianpegT'];
			$periode		= explode(' ', $_POST['KPPenggajianpegT']['periodegaji']);
			$model->pkp 	= $_POST['KPPenggajianpegT']['pkp'];
            $model->gajipertahun 	= $_POST['KPPenggajianpegT']['gajipph'];
            $model->biayajabatan 	= $_POST['KPPenggajianpegT']['biayajabatan'];
            $model->potonganpensiun	= $_POST['KPPenggajianpegT']['iuranpensiun'];
            $model->ptkppertahun	= $_POST['KPPenggajianpegT']['ptkp'];
            $model->pph21pertahun	= $_POST['KPPenggajianpegT']['pphpersen'];
            $model->pph21perbulan	= $_POST['KPPenggajianpegT']['pph21'];
            $model->persentasepph21 = $_POST['KPPenggajianpegT']['persentasepph21'];
            $model->kodeptkp 		= $_POST['KPPenggajianpegT']['kodeptkp'];
            $model->penerimaanbersihpertahun	= $_POST['KPPenggajianpegT']['penerimaanpph'];

    		$periode_bulan  = $periode[0];
    		$periode_tahun  = $periode[1];
    		$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
    		$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';

            $model->periodegaji = $periode_gaji;

			$data = $_POST['PenggajiankompT']['komponengaji_id'];
			if($model->save()){

				foreach ($data as $i=>$v){
					$modId = PenggajiankompT::model()->findByAttributes(array('komponengaji_id'=>$i,'penggajianpeg_id'=>$model->penggajianpeg_id));

					PenggajiankompT::model()->updateByPk($modId->penggajiankomp_id,array('jumlah'=>$v));
				}
                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('informasi','id'=>$model->penggajianpeg_id));
            }
		}

		$this->render('update',array(
			'model'=>$model, 'modPegawai'=>$modpegawai, 'komponen'=>$komponen
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPenggajianpegT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPenggajianpegT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPenggajianpegT']))
			$model->attributes=$_GET['KPPenggajianpegT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
    public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPenggajianpegT;
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['KPPenggajianpegT'])){
			
			$model->attributes=$_GET['KPPenggajianpegT'];
			$model->nama_pegawai = $_GET['KPPenggajianpegT']['nama_pegawai'];
			$model->nomorindukpegawai = $_GET['KPPenggajianpegT']['nomorindukpegawai'];
			if(isset($_GET['KPPenggajianpegT']['periodegaji_info'])){
				$periode		= explode(' ', $_GET['KPPenggajianpegT']['periodegaji_info']);
	    		$periode_bulan  = $periode[0];
	    		$periode_tahun  = $periode[1];
	    		$format 		= new CustomFormat;
	    		$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
	    		$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';
	    		$model->periodegaji = $periode_gaji;
	    	}
	    	$model->periodegaji_info = $_GET['KPPenggajianpegT']['periodegaji_info']; 
		}

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPPenggajianpegT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        public function actionDetail($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kppenggajianpeg-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $model= KPPenggajianpegT::model()->findbyPk($id);
            $modKomponen = PenggajiankompT::model()->findAllByAttributes(array('penggajianpeg_id'=>$id));
//            $model->attributes=$_REQUEST['id'];
            $judulLaporan='Data KPPenggajianpegT';
            $caraPrint='PDF';
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'moKomponen'=>$modKomponen,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
           public function actionPrintrincian()
        {
      
            $id = $_REQUEST['id'];
            $model= KPPenggajianpegT::model()->findbyPk($id);

               //$model= RinciangajipegawaiV::model()->findbyPk($id);
            $modKomponen = PenggajiankompT::model()->findAllByAttributes(array('penggajianpeg_id'=>$id));
//            $model->attributes=$_REQUEST['id'];
            $judulLaporan='Bukti Pembayaran Gaji';
            $caraPrint='PRINT';
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printKwGaji';
                $this->render('Printrincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'modKomponen'=>$modKomponen,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Printrincian',array('model'=>$model,'modKomponen'=>$modKomponen,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
         public function actionPrintamplop()
        {
         $id = $_REQUEST['id'];
            $model= KPPenggajianpegT::model()->findbyPk($id);

               //$model= RinciangajipegawaiV::model()->findbyPk($id);
            $modKomponen = PenggajiankompT::model()->findAllByAttributes(array('penggajianpeg_id'=>$id));

            $judulLaporan='AMPLOP BUKTI PEMBAYARAN GAJI';
            $caraPrint='PRINT';
          
                $this->layout='//layouts/printKwGaji';
                $this->render('Printamplop',array('model'=>$model,'judulLaporan'=>$judulLaporan,'modKomponen'=>$modKomponen,'caraPrint'=>$caraPrint));
            
            }
        
       public function actionDetailPenggajian($id) {
            if (!empty($id)) {
                $modelpegawai = KPPegawaiM::model()->find('pegawai_id = ' . $id . '');
                $model = PenggajianpegT::model()->find('pegawai_id = ' . $modelpegawai->pegawai_id . ' ');
            } 

            if(empty($model)){
                $model = new PenggajianpegT;
            }

            $this->render('detailPenggajian',array(
                'modelpegawai'=>$modelpegawai,
                'model'=>$model,
            ));
        }
        
         public function terbilang($x, $style=4, $strcomma=",") {
        if ($x < 0) {
            $result = "minus " . trim($this->ctword($x));
        } else {
            $arrnum = explode("$strcomma", $x);
            $arrcount = count($arrnum);
            if ($arrcount == 1) {
                $result = trim($this->ctword($x));
            } else if ($arrcount > 1) {
                $result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
            }
        }
        switch ($style) {
            case 1: //1=uppercase  dan
                $result = strtoupper($result);
                break;
            case 2: //2= lowercase
                $result = strtolower($result);
                break;
            case 3: //3= uppercase on first letter for each word
                $result = ucwords($result);
                break;
            default: //4= uppercase on first letter
                $result = ucfirst($result);
                break;
        }
        return $result;
    }

    public function ctword($x) {
        $x = abs($x);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($x < 12) {
            $temp = " " . $number[$x];
        } else if ($x < 20) {
            $temp = $this->ctword($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->ctword($x - 100);
        } else if ($x < 1000) {
            $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->ctword($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
        }
        return $temp;
    }
        
        
    public function actionPrintPenggajian($id) {
        $modelpegawai = PegawaiM::model()->findByPk($id);
        $model = PenggajianpegT::model()->find('pegawai_id = ' . $modelpegawai->pegawai_id . ' ');
        $modelpegawai->attributes = $_REQUEST['KPPegawaiM'];
        if(empty($model)){
            $model = new PenggajianpegT;
        }
        $judulLaporan = 'Penilaian Pegawai';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('PrintPenggajian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('PrintPenggajian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');              // Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                                        // Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($this->renderPartial('PrintPenggajian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
         }
    }

    public function actionAmbilTunjangan()
    {
    	if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    	{
    		$komponen_id 	= $_POST['komponen_id'];
    		$pangkat_id		= $_POST['pangkat_id'];
    		$jabatan_id		= $_POST['jabatan_id'];
    		$pegawai_id		= $_POST['pegawai_id'];
    		$kelompokpegawai_id		= $_POST['kelompokpegawai_id'];
    		$komponengaji_id= Params::ID_TUNJANGAN_HARIAN;
    		$periode		= explode(' ', $_POST['periode']);
    		$periode_bulan  = $periode[0];
    		$periode_tahun  = $periode[1];
    		$format 		= new CustomFormat;
    		$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
    		$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';

    		$data['komponen_id'] = $komponen_id;
    		$conditions = "periodeharikerjaawl <= '".$periode_gaji."' AND periodeharikerjaakhir >= '".$periode_gaji."' AND kelompokpegawai_id='".$kelompokpegawai_id."' AND harikerjagol_aktif=TRUE ";

    		$criteria 		= new CDbCriteria;
    		$criteria->addCondition($conditions);
    		$modHariKerja 	= HariKerjaGolM::model()->findAll($criteria);

    		if(count($modHariKerja)>0){
	    		foreach ($modHariKerja as $key => $harikerja)
				{
	    			$tgl_awal 	= $harikerja->periodeharikerjaawl;
	    			$tgl_akhir 	= $harikerja->periodeharikerjaakhir;
	    			$jml_hari	= $harikerja->jmlharibln;
	    			$statusscan = 1;

	    			$criteria=new CDbCriteria;
			        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
			        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
			        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
			        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
			        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
			        $criteria->compare('t.pegawai_id',$pegawai_id);
			        $criteria->compare('t.statuskehadiran_id',Params::STATUSHADIR);
			        $jumlah 	= PresensiT::model()->findAll($criteria);

	    			$jmlhadir 	= count($jumlah);
	    			$data['jmlhadir'] 	= $jmlhadir;
	    			$data['jml_hari']	= $jml_hari;

	    			$criteria=new CDbCriteria;
			        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
			        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
			        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
			        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
			        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
			        $criteria->compare('t.pegawai_id',$pegawai_id);
			        $criteria->compare('t.statuskehadiran_id',Params::STATUSCUTI);
			        $jumlah_cuti 	= PresensiT::model()->findAll($criteria);
			        $jmlcuti 		= count($jumlah_cuti);
	    			$data['jmlcuti'] 	= $jmlcuti;

	    			$criteria=new CDbCriteria;
			        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
			        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
			        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
			        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
			        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
			        $criteria->compare('t.pegawai_id',$pegawai_id);
			        $criteria->compare('t.statuskehadiran_id',Params::STATUSIJIN);
			        $jumlah_ijin 	= PresensiT::model()->findAll($criteria);
			        $jmlijin 		= count($jumlah_ijin);
	    			$data['jmlijin'] 	= $jmlijin;

	    			$criteria=new CDbCriteria;
			        $criteria->select = 'date(t.tglpresensi) as datepresensi, t.pegawai_id, t.no_fingerprint';
			        $criteria->order = 't.pegawai_id, date(t.tglpresensi)';
			        $criteria->group = 'date(t.tglpresensi), t.pegawai_id, t.no_fingerprint';
			        $criteria->join = 'INNER JOIN pegawai_m ON pegawai_m.pegawai_id=t.pegawai_id';
			        $criteria->addBetweenCondition('DATE(tglpresensi)',$tgl_awal, $tgl_akhir);
			        $criteria->compare('t.pegawai_id',$pegawai_id);
			        $criteria->compare('t.statuskehadiran_id',Params::STATUSALPHA);
			        $jumlah_alpha 	= PresensiT::model()->findAll($criteria);
			        $jmlalpha 		= count($jumlah_alpha);
	    			$data['jmlalpha'] 	= $jmlalpha;

    			if(!empty($jabatan_id)){
	    			$modTunjangan = TunjanganM::model()->findByAttributes(
						array(
							'jabatan_id'=>$jabatan_id,
							'komponengaji_id'=>$komponengaji_id
						)
					);
	    			$tunjanganharian 	= $modTunjangan->nominaltunjangan;
	    			$jmltunjanganharian = ($jmlhadir / $jml_hari) * $tunjanganharian;
					
	    			$data['tunjanganharian'] = $tunjanganharian;
	    			$data['jmltunjangan'] = round($jmltunjanganharian);
	    		}else{
	    			$data['tunjanganharian'] = 0;
	    			$data['jmltunjangan'] = 0;
	    		}

	    		}
	    	}else{
				$data['jmltunjangan'] 	= 0;	 
				$data['jmlhadir'] 		= 0;
    			$data['jml_hari']		= 0;   
    			$data['tunjanganharian']= 0;	
    			$data['jmlcuti'] 		= 0;
    			$data['jmlijin'] 		= 0;	
    			$data['jmlalpha'] 		= 0;	
	    	}
			echo json_encode($data);
			Yii::app()->end();
    	}
    }

    public function actionAmbilPph()
    {
    	if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    	{
    		$pkp 	= $_POST['pkp'];
			// $sql = "SELECT persentarifpenghsl FROM potonganpph21_m WHERE penghasilandari >= $pkp AND sampaidgn_thn <=$pkp ";
            // $persen_pph = Yii::app()->db->createCommand($sql)->queryAll();

            $conditions = "penghasilandari <= ".$pkp." AND sampaidgn_thn >=".$pkp." ";
    		$criteria 	= new CDbCriteria;
    		$criteria->addCondition($conditions);
    		$modpph 	= Potonganpph21M::model()->findAll($criteria);

    		foreach ($modpph as $key => $pph) {
    			$data['percent'] 	= $pph->persentarifpenghsl;
    		}

    		echo json_encode($data);
            Yii::app()->end();
    	}
    }

    public function actionAmbilGapok()
    {
    	if(Yii::app()->getRequest()->getIsAjaxRequest()) 
    	{
    		$golongangaji 	= $_POST['golongangaji'];
    		$masakerja 		= $_POST['masakerja'];
      		$modGolongangaji = GolongangajiM::model()->findbyAttributes(array('golonganpegawai_id'=>$golongangaji, 'masakerja'=>$masakerja));
      		if(isset($modGolongangaji->jmlgaji))
      			$data['gapok'] 	= $modGolongangaji->jmlgaji;	
      		else
      			$data['gapok'] 	= 0;
			
    		echo json_encode($data);
            Yii::app()->end();
    	}
    }

    protected function saveBuktiBayar($model, $modPegawai, $jumlah)
    {
    	$modBuktiBayar = new TandabuktibayarT();
    	$modBuktiBayar->ruangan_id = Yii::app()->user->getState('ruangan_id');
    	$modBuktiBayar->shift_id = Yii::app()->user->getState('shift_id');
    	$modBuktiBayar->nourutkasir = Generator::noUrutKasir($modBuktiBayar->ruangan_id);
    	$modBuktiBayar->nobuktibayar = Generator::noBuktiBayar();
    	$modBuktiBayar->tglbuktibayar = $model->tglpenggajian;
    	$modBuktiBayar->carapembayaran = "TUNAI";
    	$modBuktiBayar->darinama_bkm = $modPegawai['nama_pegawai'];
    	$modBuktiBayar->alamat_bkm = "RSUD TASIKMALAYA";
    	$modBuktiBayar->sebagaipembayaran_bkm = "Peminjaman";
    	$modBuktiBayar->jmlpembulatan = 0;
    	$modBuktiBayar->jmlpembayaran = $jumlah;
    	$modBuktiBayar->biayaadministrasi = 0;
    	$modBuktiBayar->biayamaterai = 0;
    	$modBuktiBayar->uangditerima = $jumlah;
    	$modBuktiBayar->uangkembalian = 0;
    	$modBuktiBayar->create_time = date('Y-m-d H:i:s');
    	$modBuktiBayar->create_loginpemakai_id = Yii::app()->user->getState('user_id');
    	$modBuktiBayar->create_ruangan = Yii::app()->user->getState('ruangan_id');
    	$modBuktiBayar->save();

    	$criteria = new CDbCriteria;
    	$criteria->compare('pegawai_id', $model->pegawai_id);
        $criteria->addCondition('sisapinjaman < jumlahpinjaman');
        $pinjaman = PinjamanpegT::model()->find($criteria);
        $tandabuktibayar = null;
        // foreach ($pinjaman as $key => $modPinjamDetail) {
			$pinjamanpeg_id = $pinjaman['pinjamanpeg_id'];
			$sisapinjaman = $pinjaman['sisapinjaman'] + $jumlah;
			$pinjDetail = PinjampegdetT::model()->findbyAttributes(array('pinjamanpeg_id'=>$pinjamanpeg_id, 'tandabuktibayar_id'=>$tandabuktibayar));
			$pinjampegdet_id = $pinjDetail->pinjampegdet_id;
        	if($jumlah>=$pinjDetail->jmlcicilan){
        		PinjampegdetT::model()->updateByPk($pinjampegdet_id, array('tandabuktibayar_id'=>$modBuktiBayar->tandabuktibayar_id));	
        	}
        	PinjamanpegT::model()->updateByPk($pinjamanpeg_id, array('sisapinjaman'=>$sisapinjaman));
			
		// }
    }
}
