<?php
class PegawaiMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','ViewUser','ProfilKlinik'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(
                                    'admin',
                                    'delete',
                                    'RemoveTemporary',
                                    'Pencatatanpegawai',
                                    'Pencatatanriwayat',
                                    'deletePendidikanpegawai',
                                    'deletePegawaidiklat',
                                    'deletePengalamankerja',
                                    'deletePegawaijabatan',
                                    'deletePegmutasi',
                                    'deletePegawaicuti',
                                    'deleteIzintugasbelajar',
                                    'deleteHukdisiplin',
                                    'create',
                                    'informasi',
                                    'detailRiwayat',
                                    'detailPenilaian',
                                    'Riwayat',
                                    'Penilaian',
                                    'DataPenilaian',
                                    'PrintDetailPenilaian',
                                    'PrintRiwayat',
                                ),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
      
        /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
	public function actionViewUser($id='')
	{
                                    $loginpemakai = Yii::app()->user->id;
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('loginpemakai_id',$loginpemakai);
                                    $pegawai = LoginpemakaiK::model()->find($criteria);
                                    echo $pegawai->pegawai_id;
                                    if(empty($id))
                                        $id = $pegawai->pegawai_id;
                                    
		$this->render('viewUser',array(
			'model'=>$this->loadModel($id),
		));
	}
        
                public function actionProfilKlinik()
                {
                                    $loginpemakai_id = Yii::app()->user->id;
                                    $criteria = new CDbCriteria;
                                    $criteria->compare('loginpemakai_id',$loginpemakai_id);
                                    $pegawai = LoginpemakaiK::model()->find($criteria);
                                    if(empty($idPegawai))
                                        $idPegawai = $pegawai->pegawai_id;
                                    
		$this->render('kepegawaian.views.pegawaiM._viewprofilKlinik',array(
			'model'=>$this->loadModel($idPegawai),
                        'modelklinik'=>$modelklinik,
		));
                }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
        
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPegawaiM;
    $modRuanganPegawai = new RuanganpegawaiM;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KPPegawaiM']))
		{
//			  echo '<pre>';
//                        echo print_r($_POST['KPPengangkatantphlT']);
//                        echo '</pre>';
//                        exit;

			$model->attributes=$_POST['KPPegawaiM'];
                         $model->profilrs_id=1;
      
      $model->bank_no_rekening = $_POST['KPPegawaiM']['bank_no_rekening'];
      $model->no_rekening = $_POST['KPPegawaiM']['no_rekening'];                  
                            if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
                              { 
                                  
                                  $model->pegawai_aktif=true;
                               //   $model->profilrs_id=Params::DEFAULT_PROFIL_RUMAH_SAKIT;
                                  $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
                                  $gambar = $model->photopegawai;
                                  $random = rand(000000, 999999);
                                  
                                  if(!empty($model->photopegawai))//Klo User Memasukan Logo
                                  { 

                                        $model->photopegawai =$random.$model->photopegawai;

                                        Yii::import("ext.EPhpThumb.EPhpThumb");

                                         $thumb=new EPhpThumb();
                                         $thumb->init(); //this is needed

                                         $fullImgName =$model->photopegawai;   
                                         $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
                                         $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;

                                    }
                              }   

                            if ($model->validate()){
                                if ($model->save()){
                                    if (!empty($model->photopegawai)){
                                          $gambar->saveAs($fullImgSource);
                                          
                                           $thumb->create($fullImgSource)
                                                 ->resize(200,200)
                                                 ->save($fullThumbSource);
                                          
                                    }
                                   
                                    if ($model->validate()){
                                        if($model->save()){
                                            $jumlahRuanganPegawai=COUNT($_POST['ruangan_id']);
                                            $pegawai_id=$model->pegawai_id;
                //                            $hapusRuanganPegawai =  RuanganpegawaiM::model()->deleteAll('pegawai_id='.$pegawai_id.'');
                                            for($i=0; $i<=$jumlahRuanganPegawai; $i++)
                                                {
                                                    $modRuanganPegawai = new RuanganpegawaiM;
                                                    $modRuanganPegawai->ruangan_id=$_POST['ruangan_id'][$i];
                                                    $modRuanganPegawai->pegawai_id=$pegawai_id;
                                                    $modRuanganPegawai->save();
                                                        
                                                }
                                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                                $this->redirect(array('admin','id'=>$model->pegawai_id));
                                        }
                                    }
                                }
                            }
              
		}

		$this->render('create',array(
			'model'=>$model,'modRuanganPegawai'=>$modRuanganPegawai
		));
	}
  
        
  public function actionPencatatanpegawai($id = null)
	{
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        
        $format = new CustomFormat();
        $model = new KPPegawaiM;
        $model->tglditerima = date('d M y');
        $modelgol = new KPGolpegawai;
        $modRuanganPegawai = new RuanganpegawaiM;
        $model->isNewRecord = TRUE;
        // if($id != null){
        //   $model = KPPegawaiM::model()->findByPk($id);
        // }
// 		    if(isset($_POST['KPPegawaiM'])){
//             $model->attributes=$_POST['KPPegawaiM'];
//             $model->profilrs_id=Params::DEFAULT_PROFIL_RUMAH_SAKIT;
//               // if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
//               //   { 
                    
//               //       $model->pegawai_aktif=true;
//               //       $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
//               //       $gambar = $model->photopegawai;
//               //       $random = rand(000000, 999999);
                    
//               //       if(!empty($model->photopegawai))//Klo User Memasukan Logo
//               //       { 

//               //             $model->photopegawai =$random.$model->photopegawai;

//               //             Yii::import("ext.EPhpThumb.EPhpThumb");

//               //              $thumb=new EPhpThumb();
//               //              $thumb->init(); //this is needed

//               //              $fullImgName =$model->photopegawai;   
//               //              $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//               //              $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;

//               //       }
//               //   }
//               if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
//               { 
//                   $model->pegawai_aktif=true;
//                   $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
//                   $gambar=$model->photopegawai;

//                   if(!empty($model->photopegawai))//Klo User Memasukan Logo
//                   { 

//                         $model->photopegawai =$random.$model->photopegawai;

//                         Yii::import("ext.EPhpThumb.EPhpThumb");

//                          $thumb=new EPhpThumb();
//                          $thumb->init(); //this is needed

//                          $fullImgName =$model->photopegawai;   
//                          $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//                          $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;

//                          if($model->save())
//                               {
//                                    $gambar->saveAs($fullImgSource);
//                                    $thumb->create($fullImgSource)
//                                          ->resize(200,200)
//                                          ->save($fullThumbSource);
//                               }
//                           else
//                               {
//                                    Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
//                               }
//                     }
//               }

//               $model->photopegawai=$_POST['KPPegawaiM']['tempPhoto'];
//               if ($model->validate()){
//                   if ($model->save()){
//                       // if ($_POST['caraAmbilPhoto']=='webCam'){
//                       //       $model->photopegawai=$_POST['KPPegawaiM']['tempPhoto'];
//                       //       $gambar=$model->photopegawai;
//                       //       $fullImgName =$model->photopegawai;   
//                       //       $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//                       //       $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;
//                       //       // $thumb=new EPhpThumb();
//                       //       // $thumb->init(); //this is needed

//                       //       $gambar->saveAs($fullImgSource);
//                       //       var_dump($fullImgSource);
//                       //       exit();
//                       //       // $thumb->create($fullImgSource)
//                       //       //      ->resize(200,200)
//                       //       //      ->save($fullThumbSource);
                            
//                       // }
                     
//                       if ($model->validate()){
//                           if($model->save()){
//                               $jumlahRuanganPegawai=COUNT($_POST['ruangan_id']);
//                               $pegawai_id=$model->pegawai_id;
//   //                            $hapusRuanganPegawai =  RuanganpegawaiM::model()->deleteAll('pegawai_id='.$pegawai_id.'');
//                               for($i=0; $i<=$jumlahRuanganPegawai; $i++)
//                                   {
//                                       $modRuanganPegawai = new RuanganpegawaiM;
//                                       $modRuanganPegawai->ruangan_id=$_POST['ruangan_id'][$i];
//                                       $modRuanganPegawai->pegawai_id=$pegawai_id;
//                                       if ($modRuanganPegawai->save()) {
//                                           Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');   
//                                       }else{
// //                                                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');   
//                                       }

//                                   }
//                                   Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//                                   //$this->redirect(array('admin','id'=>$model->pegawai_id));
//                           }
//                       }
//                   }
//               }
// 		    }
        if(isset($_POST['KPPegawaiM'])){
          // echo "a";exit;        
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                      // echo "b";exit;
                              $model=new KPPegawaiM;
                              $model->attributes=$_POST['KPPegawaiM'];
                              $model->tgl_lahirpegawai = $format->formatDateMediumForDB($_POST['KPPegawaiM']['tgl_lahirpegawai']);
                              $model->tglditerima = $format->formatDateMediumForDB($_POST['KPPegawaiM']['tglditerima']);
                              $model->bank_no_rekening = $_POST['KPPegawaiM']['bank_no_rekening'];
                              $model->no_rekening = $_POST['KPPegawaiM']['no_rekening'];
                              $model->create_loginpemakai_id = Yii::app()->user->id;
                              $model->update_loginpemakai_id = Yii::app()->user->id;
                              $model->create_time = date('Y-m-d H:i:s');
                              $model->update_time = date('Y-m-d H:i:s');
                              $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                              $random=rand(0000000,9999999);

                              
                              $model->profilrs_id=Params::DEFAULT_PROFIL_RUMAH_SAKIT;
                              if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
                              { 
                                  // echo "c";exit;
                                  // $model->pegawai_aktif=true;
                                  $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
                                  $gambar=$model->photopegawai;

                                  if(!empty($model->photopegawai))//Klo User Memasukan Logo
                                  { 
                                    // echo "d";exit;

                                        $model->photopegawai =$random.$model->photopegawai;

                                        Yii::import("ext.EPhpThumb.EPhpThumb");

                                         $thumb=new EPhpThumb();
                                         $thumb->init(); //this is needed

                                         $fullImgName =$model->photopegawai;   
                                         $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
                                         $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;

                                         if($model->save())
                                              {
                                                // echo "e";exit;
                                                   $gambar->saveAs($fullImgSource);
                                                   $thumb->create($fullImgSource)
                                                         ->resize(200,200)
                                                         ->save($fullThumbSource);
                                                   $model->isNewRecord = FALSE;
                                              }
                                          else
                                              {
                                                  // echo "f";exit;
                                                   Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                                              }
                                    }
                              }   
                             else 
                              {
                                // echo "g";exit;
                                 $model->photopegawai=$_POST['KPPegawaiM']['tempPhoto'];
                                 if($model->validate())
                                    {
                                      // echo "h";exit;
                                        $model->save();
                                        $model->isNewRecord = FALSE;
                                    }
                                 else 
                                    {
                                      // echo "i";exit;
                                         unlink(Params::pathPegawaiDirectory().$_POST['KPPegawaiM']['tempPhoto']);
                                         unlink(Params::pathPegawaiTumbsDirectory().$_POST['KPPegawaiM']['tempPhoto']);
                                    }
                               }
                            
                             // echo '<pre>';
                             // echo print_r($model->attributes);exit;
                            if($model->validate()){
                              if($model->save()){
                                $jumlahRuanganPegawai=COUNT($_POST['ruangan_id']);
                                $pegawai_id=$model->pegawai_id;

                                if($pegawai_id!=null){
                                  // echo "j";exit;
                                  $hapusRuanganPegawai=  RuanganpegawaiM::model()->deleteAll('pegawai_id='.$pegawai_id.''); 
                                }
                                for($i=0; $i<=$jumlahRuanganPegawai; $i++)
                                  {
                                    // echo "k";exit;
                                      $modRuanganPegawai = new RuanganpegawaiM;
                                      $modRuanganPegawai->ruangan_id=$_POST['ruangan_id'][$i];
                                      $modRuanganPegawai->pegawai_id=$pegawai_id;
                                      $modRuanganPegawai->save();

                                  }
                                  // echo "l";exit;
                                // exit;
                                  $model->isNewRecord = FALSE;
                                  $transaction->commit();
                                  Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                 // $this->redirect(array('Pencatatanpegawai','id'=>$model->pegawai_id));
                                  $this->redirect(array('Pencatatanpegawai'));
                                  // $this->redirect('informasi')
                              }
                            } else{
                              // echo "m";exit;
                              $transaction->rollback();
                              Yii::app()->user->setFlash('error',"<strong>Gagal!</strong> Data Gagal Disimpan.");
                            }
                     }
                    catch (Exception $e)
                     {
                      // echo "n";exit;
                          $transaction->rollback();
                          Yii::app()->user->setFlash('error',"<strong>Gagal!</strong> Data Gagal Disimpan".MyExceptionMessage::getMessage($e,true));
                     }
              
    }

		$this->render('pencatatanpegawai',array(
			'model'=>$model, 'modRuanganPegawai'=>$modRuanganPegawai, 'modelgol'=>$modelgol
		));
	}
        
//	public function actionPencatatanpegawai()
//	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//		
//                
//		// Uncomment the following line if AJAX validation is needed
//		// $this->performAjaxValidation($model);
//                $model=new KPPegawaiM;
//                $modRuanganPegawai = new RuanganpegawaiM;
//		if(isset($_POST['KPPegawaiM']))
//		{
////			echo '<pre>';
////                        echo print_r($_POST['KPPegawaiM']);
////                        echo '</pre>';
////                        exit;
//                    $transaction = Yii::app()->db->beginTransaction();
//                    try {
//                              $model=new KPPegawaiM;
//                              $random=rand(0000000,9999999);
//                              $model->attributes=$_POST['KPPegawaiM'];
//                              $model->profilrs_id=Params::DEFAULT_PROFIL_RUMAH_SAKIT;
//                              $model->pegawai_aktif=false;
//                              if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
//                              { 
//                                  $model->pegawai_aktif=true;
//                                  $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
//                                  $gambar=$model->photopegawai;
//
//                                  if(!empty($model->photopegawai))//Klo User Memasukan Logo
//                                  { 
//
//                                        $model->photopegawai =$random.$model->photopegawai;
//
//                                        Yii::import("ext.EPhpThumb.EPhpThumb");
//
//                                         $thumb=new EPhpThumb();
//                                         $thumb->init(); //this is needed
//
//                                         $fullImgName =$model->photopegawai;   
//                                         $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
//                                         $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;
//
//                                         if($model->save())
//                                              {
//                                                   $gambar->saveAs($fullImgSource);
//                                                   $thumb->create($fullImgSource)
//                                                         ->resize(200,200)
//                                                         ->save($fullThumbSource);
//                                              }
//                                          else
//                                              {
//                                                   Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
//                                              }
//                                    }
//                              } else {
//                                 $model->photopegawai=$_POST['KPPegawaiM']['tempPhoto'];
//                                 if($model->validate())
//                                    {
//                                        $model->save();
//                                    }
//                                 else 
//                                    {
//                                         unlink(Params::pathPegawaiDirectory().$_POST['KPPegawaiM']['tempPhoto']);
//                                         unlink(Params::pathPegawaiTumbsDirectory().$_POST['KPPegawaiM']['tempPhoto']);
//                                    }
//                               }
//                  
//                            $jumlahRuanganPegawai=COUNT($_POST['ruangan_id']);
//                            $pegawai_id=$model->pegawai_id;
////                            $hapusRuanganPegawai =  RuanganpegawaiM::model()->deleteAll('pegawai_id='.$pegawai_id.'');
//                            for($i=0; $i<=$jumlahRuanganPegawai; $i++)
//                                {
//                                    $modRuanganPegawai = new RuanganpegawaiM;
//                                    $modRuanganPegawai->ruangan_id=$_POST['ruangan_id'][$i];
//                                    $modRuanganPegawai->pegawai_id=$pegawai_id;
//                                    if ($modRuanganPegawai->save()) {
//                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');   
//                                    }else{
//                                        Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.');   
//                                    }
//
//                                }
//                         $transaction->commit();
////                         Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');    
////                       $this->redirect(array('admin'));  
//                     }
//                    catch (Exception $e)
//                     {
//                          $transaction->rollback();
//                          Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($e,true));
//                     }   
//              
//		}
//
//		$this->render('pencatatanpegawai',array(
//			'model'=>$model,'modRuanganPegawai'=>$modRuanganPegawai
//		));
//	}
/* ========================= Pencatatan riwayat ============================================== */        
                 public function actionPencatatanriwayat($id = null){
                    $modPendidikanpegawai = new KPPendidikanpegawaiR;
                    $modPegawaidiklat = new KPPegawaidiklatT;
                    $modPengalamankerja = new KPPengalamankerjaR;
                    $modPegawaijabatan = new KPPegawaijabatanR;
                    $modPegmutasi = new KPPegmutasiR;
                    $modPegawaicuti = new KPPegawaicutiT;
                    $modPegawaicuti->tglmulaicuti = date('Y-m-d');
                    $modPegawaicuti->tglakhircuti = date('Y-m-d');
                    $modPegawaicuti->lamacuti     = 1;
                    $modIzintugasbelajar = new KPIzintugasbelajarR;
                    $modHukdisiplin = new KPHukdisiplinR;
                    $detailPengalamankerja = array();
                    $detailPegawaidiklat = array();
                    $model = new KPPegawaiM;
                    
                    if (!empty($id)) {
                        $model = KPPegawaiM::model()->findByPk($id);
                        $detailPegawaidiklat = KPPegawaidiklatT::model()->findAllByAttributes(
                            array('pegawai_id'=>$id)
                        );
                        $detailPengalamankerja = KPPengalamankerjaR::model()->findAllByAttributes(
                            array('pegawai_id'=>$id)
                        );
                    }
                    
                    if (isset($_POST['KPPegawaiM'])) {
                        $model->attributes = $_POST['KPPegawaiM'];
                        $model->nama_pegawai = $_POST['namapegawai'];
                        $model->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                        $transaction = Yii::app()->db->beginTransaction();
/* ========================= Proses simpan Pendidikan pegawai===================================== */
//                    if (Yii::app()->request->isAjaxRequest) {
                        if (isset($_POST['submitpendidikan'])) {
                            $jmlhsavependidikan = 0;
                            foreach ($_POST['KPPendidikanpegawaiR'] as $i=>$row)
                            {
                                $modPendidikanpegawai = new KPPendidikanpegawaiR;
                                $modPendidikanpegawai->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                                $modPendidikanpegawai->attributes = $row;
                                if (empty($row['tglmasuk'])) {
                                    $modPendidikanpegawai->tglmasuk = null;
                                }
                                if (empty($row['tgl_ijazah_sert'])) {
                                    $modPendidikanpegawai->tgl_ijazah_sert = null;
                                }
                                $modPendidikanpegawai->create_time = date('Y-m-d');
                                $modPendidikanpegawai->jenispendidikan = $_POST['KPPegawaiM']['jenispendidikan'];
                                $modPendidikanpegawai->create_loginpemakai_id = Yii::app()->user->id;
                                $modPendidikanpegawai->create_ruangan = Yii::app()->user->ruangan_id;
                                if ($modPendidikanpegawai->validate()) {
                                    if ($modPendidikanpegawai->save()) {
                                        $jmlhsavependidikan++;
                                    }
                                }
                            }
                          if ($jmlhsavependidikan==COUNT($_POST['KPPendidikanpegawaiR'])) {
                              $transaction->commit();
                              Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                              $modPendidikanpegawai->unsetAttributes();
                          } else {
                              $transaction->rollback();
                              Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                          }
                        }
/* ========================= Akhir simpan Pendidikan pegawai===================================== */
/* ========================= Proses simpan Pegawai diklat======================================= */
                        else if (isset($_POST['submitdiklat'])){
                            $details = $this->validasiTabularDiklat($_POST['KPPegawaidiklatT'], $model);
                            $jumlah = count($details);
                            $tersimpan = 0;
                            foreach ($details as $i=>$row){
                                $pegawaidiklat_lamanyasatuan = $_POST['KPPegawaidiklatT'][$i]['pegawaidiklat_lamanyasatuan'];
                                $row->pegawaidiklat_lamanya = $row['pegawaidiklat_lamanya'] .' '. $pegawaidiklat_lamanyasatuan;
                                $row->create_loginpemakai_id = Yii::app()->user->id;
                                $row->create_ruangan = Yii::app()->user->ruangan_id;
                                $row->pegawaidiklat_tahun = date('Y-m-d H:i:s');
                                if($row->save()){
                                    $tersimpan++;
                                }
                            }
                            if (($tersimpan > 0) && ($tersimpan == $jumlah)){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                    $module = '/'.$this->module->id.'/';
                                    $id_pegawai = (is_null($id) ? '' : '&id='.$id);
                                    $urlDiklat = $module.'PegawaiM/Pencatatanriwayat'.$id_pegawai;
                                    $this->redirect(array($urlDiklat));
                            }else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                            }
                        }

/* ========================= Akhir simpan Pegawai diklat===================================== */
/* ========================= Proses simpan Pengalaman kerja===================================== */
                        else if (isset($_POST['submitPengalamankerja'])){
                            $jmlhsavepengalamankerja = 0;
                            $submitPengalamankerja = $this->validasiTabularPengalamanKerja($_POST['KPPengalamankerjaR'], $model);
                            $jumlah = count($submitPengalamankerja);
                            $tersimpan = 0;
                            foreach ($submitPengalamankerja as $i=>$row){
                                if (empty($row['tglmasuk'])) {
                                    $row->tglmasuk = null;
                                }
                                if (empty($row['tglkeluar'])) {
                                    $row->tglkeluar = null;
                                }
                                $row->create_time = date('Y-m-d');
                                $row->create_loginpemakai_id = Yii::app()->user->id;
                                $row->create_ruangan = Yii::app()->user->ruangan_id;                                
                                if($row->save()){
                                    $tersimpan++;
                                }
                            }
                            if (($tersimpan > 0) && ($tersimpan == $jumlah)){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                    $module = '/'.$this->module->id.'/';
                                    $id_pegawai = (is_null($id) ? '' : '&id='.$id);
                                    $urlDiklat = $module.'PegawaiM/Pencatatanriwayat'.$id_pegawai;
                                    $this->redirect(array($urlDiklat));
                            }else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan');
                            }
                        }
/* ========================= Akhir simpan Pengalaman kerja===================================== */
/* ========================= Proses simpan Pegawai jabatan ==================================== */
                        else if (isset($_POST['submitPegawaijabatan'])) {
                            $modPegawaijabatan = new KPPegawaijabatanR;
                            $modPegawaijabatan->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                            $modPegawaijabatan->attributes = $_POST['KPPegawaijabatanR'];
                            if (empty($_POST['KPPegawaijabatanR']['tglditetapkanjabatan'])) {
                                $modPegawaijabatan->tglditetapkanjabatan = null;
                            }
                            if (empty($_POST['KPPegawaijabatanR']['tmtjabatan'])) {
                                $modPegawaijabatan->tmtjabatan = null;
                            }
                            if (empty($_POST['KPPegawaijabatanR']['tglakhirjabatan'])) {
                                $modPegawaijabatan->tglakhirjabatan = null;
                            }
                            $modPegawaijabatan->create_time = date('Ymd');
                            $modPegawaijabatan->create_loginpemakai_id = Yii::app()->user->id;
                            $modPegawaijabatan->create_ruangan = Yii::app()->user->ruangan_id;
                            if ($modPegawaijabatan->validate()) {
                                if ($modPegawaijabatan->save()) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                                    $modPegawaijabatan->unsetAttributes();
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error','<strong>Gagal </strong> Data gagal disimpan');
                                }
                            }
                        }
/* ========================= Akhir simpan Pegawai jabatan ===================================== */
/* ========================= Proses simpan Pegawai mutasi =====================================*/
                        else if (isset($_POST['submitPegmutasi'])) {
                            $modPegmutasi = new KPPegmutasiR;
                            $modPegmutasi->attributes = $_POST['KPPegmutasiR'];
                            if (empty($_POST['KPPegmutasiR']['tglsk'])) {
                                $modPegmutasi->tglsk = null;
                            }
                            if (empty($_POST['KPPegmutasiR']['tmtsk'])) {
                                $modPegmutasi->tmtsk = null;
                            }
                            $modPegmutasi->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                            if ($modPegmutasi->validate()) {
                                if ($modPegmutasi->save()) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                                    $modPegmutasi->unsetAttributes();
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error','<strong>Gagal </strong> Data gagal disimpan');
                                }
                            }
                        }
/* ========================= Akhir simpan Pegawai mutasi =====================================*/
/* ========================= Proses simpan Pegawai cuti ====================================== */
                        else if (isset($_POST['submitPegawaicuti'])) {
                            $modPegawaicuti = new KPPegawaicutiT;
                            $modPegawaicuti->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                            $modpegawai = PegawaiM::model()->findByPk($modPegawaicuti->pegawai_id);
                            $finger = isset($modpegawai->nofingerprint) ? $modpegawai->nofingerprint : ' ' ;
                            $modPegawaicuti->attributes = $_POST['KPPegawaicutiT'];
                            if (empty($_POST['KPPegawaicutiT']['tglakhircuti'])) {
                                $modPegawaicuti->tglakhircuti = null;
                            }
                            if ($modPegawaicuti->validate()) {
                                if ($modPegawaicuti->save()) {

                                    $lamacuti = $modPegawaicuti->lamacuti;
                                    $tglcuti = $modPegawaicuti->tglmulaicuti;
                                    $tglawalcuti = date('Y-m-d', strtotime('-1 days', strtotime($tglcuti)));
                                    for ($i=1; $i <=$lamacuti ; $i++) { 
                                      $presensi = new KPPresensiT;
                                      $presensi->statuskehadiran_id = Params::STATUSCUTI;
                                      $presensi->pegawai_id = $modPegawaicuti->pegawai_id;
                                      $presensi->statusscan_id = null;
                                      $tglawalcuti = date('Y-m-d 00:00:00', strtotime('+1 days', strtotime($tglawalcuti)));
                                      $presensi->tglpresensi = $tglawalcuti;
                                      $presensi->no_fingerprint = $finger;
                                      $presensi->verifikasi = 'F';
                                      $presensi->keterangan = "CUTI";
                                      $presensi->create_time = date('Ymd');
                                      $presensi->create_loginpemakai_id = Yii::app()->user->id;
                                      $presensi->create_ruangan = Yii::app()->user->ruangan_id; 
                                      $presensi->save();
                                      // echo"<pre>";
                                      // print_r($presensi->attributes);

                                    }
                                    // exit();
                                    
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                                    $modPegawaicuti->unsetAttributes();
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error','<strong>Gagal </strong> Data gagal disimpan');
                                }
                            }
                        }
/* ========================= Akhir simpan Pegawai cuti ======================================= */
/* ========================= Proses simpan Izin tugas belajar =================================== */
                        else if (isset($_POST['submitIzintugasbelajar'])) {
                            $modIzintugasbelajar = new KPIzintugasbelajarR;
                            $modIzintugasbelajar->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                            $modIzintugasbelajar->attributes = $_POST['KPIzintugasbelajarR'];
                            if (empty($_POST['KPIzintugasbelajarR']['tglditetapkan'])) {
                                $modIzintugasbelajar->tglditetapkan = null;
                            }
                            $modIzintugasbelajar->create_time = date('Ymd');
                            $modIzintugasbelajar->create_loginpemakai_id = Yii::app()->user->id;
                            $modIzintugasbelajar->create_ruangan = Yii::app()->user->ruangan_id;
                            if ($modIzintugasbelajar->validate()) {
                                if ($modIzintugasbelajar->save()) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                                    $modIzintugasbelajar->unsetAttributes();
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error','<strong>Gagal </strong> Data gagal disimpan');
                                }
                            }
                        }
/* ========================= Akhir simpan izin tugas belajar ==================================== */
/* ========================= Proses simpan Hukuman disiplin =====================================*/
                        else if (isset($_POST['submitHukdisiplin'])) {
                            $modHukdisiplin = new KPHukdisiplinR;
                            $modHukdisiplin->pegawai_id = $_POST['KPPegawaiM']['pegawai_id'];
                            $modHukdisiplin->attributes = $_POST['KPHukdisiplinR'];
                            $modHukdisiplin->create_time = date('Ymd');
                            $modHukdisiplin->create_loginpemakai_id = Yii::app()->user->id;
                            $modHukdisiplin->create_ruangan = Yii::app()->user->ruangan_id;
                            if ($modHukdisiplin->validate()) {
                                if ($modHukdisiplin->save()) {
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success','<strong>Berhasil </strong> Data berhasil disimpan');
                                    $modHukdisiplin->unsetAttributes();
                                } else {
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error','<strong>Gagal </strong> Data gagal disimpan');
                                }
                            }
                        }
//                    }
/* ========================= Akhir simpan Hukuman disiplin =====================================*/
                    }
                    $this->render(
                            'pencatatanriwayat'
                            ,array(
                                'model'=>$model,
                                'modPendidikanpegawai'=>$modPendidikanpegawai,
                                'modPegawaidiklat'=>$modPegawaidiklat,
                                'modPengalamankerja'=>$modPengalamankerja,
                                'modPegawaijabatan'=>$modPegawaijabatan,
                                'modPegmutasi'=>$modPegmutasi,
                                'modPegawaicuti'=>$modPegawaicuti,
                                'modIzintugasbelajar'=>$modIzintugasbelajar,
                                'modHukdisiplin'=>$modHukdisiplin,
                                'namapegawai'=> (isset($model->nama_pegawai) ? $model->nama_pegawai : ''),
                                'detailPegawaidiklat'=>$detailPegawaidiklat,
                                'detailPengalamankerja'=>$detailPengalamankerja
                            )
                    );
                }
/* ======================= Akhir Pencatatan riwayat ============================================== */

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
//            $this->layout='//layouts/frameDialog'; Jika pakai dialog box
            
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);
                $modelgol = new KPGolpegawai;
                $modRuanganPegawai=RuanganpegawaiM::model()->findAll('pegawai_id='.$id.'');
                $temLogo=$model->photopegawai;
                $format = new CustomFormat();
               if(isset($_POST['KPPegawaiM']))
		{

                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                              $random=rand(0000000,9999999);
                              $model->attributes=$_POST['KPPegawaiM'];
                              $model->pasien_id=$_POST['KPPegawaiM']['pasien_id'];
                              $model->bank_no_rekening = $_POST['KPPegawaiM']['bank_no_rekening'];
                              $model->no_rekening = $_POST['KPPegawaiM']['no_rekening'];

                              $model->profilrs_id=Params::DEFAULT_PROFIL_RUMAH_SAKIT;
                              $model->update_time = date('Y-m-d');
                              $model->update_loginpemakai_id = Yii::app()->user->id;
                              if(!empty($_POST['KPPegawaiM']['tgl_lahirpegawai'])){
                                    $model->tgl_lahirpegawai = $format->formatDateMediumForDB($model->tgl_lahirpegawai);
                              }else{
                                  $model->tgl_lahirpegawai = null;
                              }
                              
                              if(!empty($_POST['KPPegawaiM']['tglditerima'])){
                                    $model->tglditerima = $format->formatDateMediumForDB($model->tglditerima);
                              }else{
                                  $model->tglditerima = null;
                              }
                              
                        if($model->validate())
                        {  
                            if($_POST['caraAmbilPhoto']=='file')//Jika User Mengambil photo pegawai dengan cara upload file
                            { 
                              // $model->pegawai_aktif=true;
                              $model->photopegawai = CUploadedFile::getInstance($model, 'photopegawai');
                              $gambar=$model->photopegawai;

                              if(!empty($model->photopegawai))//Klo User Memasukan Logo
                              { 

                                    $model->photopegawai =$random.$model->photopegawai;
                                    Yii::import("ext.EPhpThumb.EPhpThumb");
                                    $thumb=new EPhpThumb();
                                    $thumb->init(); //this is needed
                                    $fullImgName =$model->photopegawai;   
                                    $fullImgSource = Params::pathPegawaiDirectory().$fullImgName;
                                    $fullThumbSource = Params::pathPegawaiTumbsDirectory().'kecil_'.$fullImgName;
//                                    if($model->save())
                                    if($model->update())
                                    {
                                        if(!empty($temLogo))
                                        { 
                                            if(file_exists(Params::pathPegawaiDirectory().$temLogo))
                                            {
                                                unlink(Params::pathPegawaiDirectory().$temLogo);
                                            }
                                            if(file_exists(Params::pathIconModulThumbsDirectory().'kecil_'.$temLogo))
                                            {
                                                unlink(Params::pathIconModulThumbsDirectory().'kecil_'.$temLogo);
                                            }
                                        }
                                        $gambar->saveAs($fullImgSource);
                                        $thumb->create($fullImgSource)
                                            ->resize(200,200)
                                            ->save($fullThumbSource);
                                    }
                                    else
                                    {
                                        Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                                    }
                                }else{
                                  $model->photopegawai = $temLogo;
                                }
                                                              
                            }   
                            else 
                            {
                                ////Jika user Memasukan Photo Dari Webcam
                                if(!empty($temLogo))
                                { 
                                    //                                       
                                    if(!empty($temLogo))
                                    { 
                                        if(file_exists(Params::pathPegawaiDirectory().$temLogo))
                                        {
                                            unlink(Params::pathPegawaiDirectory().$temLogo);
                                        }
                                        if(file_exists(Params::pathIconModulThumbsDirectory().'kecil_'.$temLogo))
                                        {
                                            unlink(Params::pathIconModulThumbsDirectory().'kecil_'.$temLogo);
                                        }                                        
//                                        unlink(Params::pathPegawaiDirectory().$temLogo);
//                                        unlink(Params::pathPegawaiTumbsDirectory().$temLogo);
                                    }
                                }
                                $model->photopegawai=$_POST['KPPegawaiM']['tempPhoto'];
                                $model->update();
                                
                            }
                            if(!empty($_POST['ruangan_id']))
                                $jumlahRuanganPegawai = COUNT($_POST['ruangan_id']);
                            else
                                $jumlahRuanganPegawai = 0;
                            $pegawai_id=$model->pegawai_id;
                            $hapusRuanganPegawai=  RuanganpegawaiM::model()->deleteAll('pegawai_id='.$pegawai_id.''); 
                            for($i=0; $i<$jumlahRuanganPegawai; $i++)
                                {
                                    $modRuanganPegawai = new RuanganpegawaiM;
                                    $modRuanganPegawai->ruangan_id=$_POST['ruangan_id'][$i];
                                    $modRuanganPegawai->pegawai_id=$pegawai_id;
                                    $modRuanganPegawai->save();

                                }
                                $model->update(); // update data 
                                $transaction->commit();

                         Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan !');    
                         $this->redirect(array('update', 'id'=>$model->pegawai_id)); 
                      } 
                    }
                    catch (Exception $e)
                    {
                      $transaction->rollback();
                      Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($e,true));
                    }   
              
		}
                if(!empty($model->tgl_lahirpegawai))
                    $model->tgl_lahirpegawai = date('d M Y',strtotime($model->tgl_lahirpegawai));

                $this->render('update',array(
			'model'=>$model,'modRuanganPegawai'=>$modRuanganPegawai, 'modelgol'=>$modelgol
		));
	}


	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}
/* ============================ Proses delete riwayat ========================================= */
                public function actiondeletePendidikanpegawai($pendidikanpegawai_id)
                {
                    $modPendidikanpegawai = new KPPendidikanpegawaiR;
                    if ($modPendidikanpegawai->deleteByPK($pendidikanpegawai_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
        
                public function actiondeletePegawaidiklat($pegawaidiklat_id)
                {
                    $modPegawaidiklat = new KPPegawaidiklatT;
                    if ($modPegawaidiklat->deleteByPK($pegawaidiklat_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
        
                public function actiondeletePengalamankerja($pengalamankerja_id)
                {
                    $modPengalamankerja = new KPPengalamankerjaR;
                    if ($modPengalamankerja->deleteByPK($pengalamankerja_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
        
                public function actiondeletePegawaijabatan($pegawaijabatan_id)
                {
                    $modPegawaijabatan = new KPPegawaijabatanR;
                    if ($modPegawaijabatan->deleteByPK($pegawaijabatan_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
        
                public function actiondeletePegmutasi($pegmutasi_id)
                {
                    $modPegmutasi = new KPPegmutasiR;
                    if ($modPegmutasi->deleteByPK($pegmutasi_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
        
                public function actiondeletePegawaicuti($pegawaicuti_id)
                {
                    $modPegawaicuti = new KPPegawaicutiT;
                    if ($modPegawaicuti->deleteByPK($pegawaicuti_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
                
                public function actiondeleteIzintugasbelajar($izintugasbelajar_id)
                {
                    $modIzintugasbelajar = new KPIzintugasbelajarR;
                    if ($modIzintugasbelajar->deleteByPK($izintugasbelajar_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
                
                public function actiondeleteHukdisiplin($hukdisiplin_id)
                {
                    $modHukdisiplin = new KPHukdisiplinR;
                    if ($modHukdisiplin->deleteByPK($hukdisiplin_id)) {
                        $this->redirect(array('Pencatatanriwayat'));
                    }
                }
/* =========================== Akhir proses delete riwayat ======================================= */
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPegawaiM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPegawaiM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPegawaiM']))
			$model->attributes=$_GET['KPPegawaiM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
              
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
               //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPegawaiM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPegawaiM']))
			$model->attributes=$_GET['KPPegawaiM'];

		$this->render('informasi',array(
			'model'=>$model,
		));
	}
        
        
        

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPPegawaiM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sapegawai-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                KPPegawaiM::model()->updateByPk($id, array('pegawai_aktif'=>false));
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model = new KPPegawaiM;
            $model->attributes=$_REQUEST['KPPegawaiM'];
            $judulLaporan='Data Pegawai';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                         
        }
        
         
        public function actionRiwayat($id) {

            
            $this->render('riwayat',array(
                'model'=>$this->loadModel($id),
        ));
        }
                
        public function actionPenilaian($id) {
            if (!empty($id)) {
                $modelpegawai = KPPegawaiM::model()->find('pegawai_id = ' . $id . '');
                $modelpegawai->jabatan_id = $modelpegawai->jabatan->jabatan_nama;
                $model = PenilaianpegawaiT::model()->find('pegawai_id = ' . $modelpegawai->pegawai_id . ' ');
                
            } 

            if(empty($model)){
                $model = new PenilaianpegawaiT;
            }

            $this->render('penilaian',array(
                'modelpegawai'=>$modelpegawai,
                'model'=>$model,
            ));
        }
        
        public function actionPrintDetailPenilaian($id) {
        $modelpegawai = PegawaiM::model()->findByPk($id);
        $model = PenilaianpegawaiT::model()->find('pegawai_id = ' . $modelpegawai->pegawai_id . ' ');
        $modelpegawai->attributes = $_REQUEST['KPPegawaiM'];
        if(empty($model)){
            $model = new PenilaianpegawaiT;
        }
        $judulLaporan = 'Penilaian Pegawai';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('PrintPenilaian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('PrintPenilaian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->session['ukuran_kertas'];                  // Ukuran Kertas Pdf
            $posisi = Yii::app()->session['posisi_kertas'];                                      // Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->render('PrintPenilaian', array('model' => $model, 'modelpegawai'=>$modelpegawai, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
         }
        }
        
       public function actionPrintRiwayat($id) {
        $model = PegawaiM::model()->findByPk($id);
        $modelPangkat = KenaikanpangkatT::model()->findAllByAttributes(array('pegawai_id'=>$pegawai));
//        $model = PenggajianpegT::model()->find('pegawai_id = ' . $modelpegawai->pegawai_id . ' ');
        $model->attributes = $_REQUEST['KPPegawaiM'];
//        if(empty($model)){
//            $model = new PenggajianpegT;
//        }
        $judulLaporan = 'Riwayat Pegawai';
        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == 'PRINT') {
            $this->layout = '//layouts/printWindows';
            $this->render('PrintRiwayat', array('model' => $model, 'modelPangkat'=>$modelPangkat, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render('PrintRiwayat', array('model' => $model,  'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->session['ukuran_kertas'];                  // Ukuran Kertas Pdf
            $posisi = Yii::app()->session['posisi_kertas'];                                      // Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->render('PrintRiwayat', array('model' => $model, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
         }
        }

       protected function validasiTabularDiklat($datas, $model){
            $pegawai = 0;
            $details = array();
            foreach ($datas as $i=>$data){
                $data = array_filter($data, 'strlen');
                if (is_array($data)){
                    if (!empty($data['pegawaidiklat_id'])){
                        $details[$i] = KPPegawaidiklatT::model()->findByPk($data['pegawaidiklat_id']);
                        $details[$i]->attributes = $data;
                        $pegawai = $data['pegawai_id'];
                    }else{
                        if(!empty($data['pegawaidiklat_nama']))
                        {
                            $details[$i] = new KPPegawaidiklatT();
                            $details[$i]->attributes = $data;
                            $details[$i]->pegawai_id = $model->pegawai_id;
                        }
                    }
                }
                else{
                    if (empty($data)){
                        
                    }else{
                        $pegawai = $data;
                    }
                }
            }
            
            $rows = array();
            foreach ($details as $i=>$data){
                $rows[$i] = $data;
                $rows[$i]->validate();
            }
            
            return $rows;
        }
        
       protected function validasiTabularPengalamanKerja($datas, $model){
            $pegawai = 0;
            $details = array();
            foreach ($datas as $i=>$data){
                $data = array_filter($data, 'strlen');
                if (is_array($data)){
                    if (!empty($data['pengalamankerja_id'])){
                        $details[$i] = KPPengalamankerjaR::model()->findByPk($data['pengalamankerja_id']);
                        $details[$i]->attributes = $data;
                        $pegawai = $data['pegawai_id'];
                    }else{
                        if (!empty($data['namaperusahaan'])){
                            $details[$i] = new KPPengalamankerjaR();
                            $details[$i]->attributes = $data;
                            $details[$i]->pegawai_id = $model->pegawai_id;
                        }
                    }
                }
            }
            $rows = array();
            foreach ($details as $i=>$data){
                $rows[$i] = $data;
                $rows[$i]->validate();
            }
            return $rows;
        }
        
}
