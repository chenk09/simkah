<?php

class TrxPenggajianPegController extends SBaseController
{
	public function actionIndex(){}
	public function actionAdmin(){
		$format = new CustomFormat;
		$model = new KPPenggajianpegT;
		
		if(isset($_GET["KPPenggajianpegT"]))
		{
			$model->attributes = $_GET["KPPenggajianpegT"];
			$model->periodegaji = $format->formatDateTimeMediumForDB($_GET['KPPenggajianpegT']['periodegaji']);
			$model->kategoripegawai = $_GET['KPPenggajianpegT']['kategoripegawai'];
		}
		
		$this->render('admin',array(
			"model"=>$model
		));		
	}
	
	public function actionCreate()
	{
		$model = new KPPenggajianpegT;
		
		$format = new CustomFormat;
		$bulan 	= $format->formatBulanInaLengkap(date('M'));
		
		$model->tglpenggajian = date('Y-m-d H:i:s');
		$model->periodegaji = $bulan.' '.date('Y');
		$model->nopenggajian = Generator::noPenggajian();
		$model->penerimaanbersih= 0;
		$model->totalpajak = 0;
		$model->totalpotongan = 0;
		$model->totalterima = 0;
		$model->jmlefektifharikerja = 26;
		$model->harikerja = 26;
		$model->cuti = 0;
		$model->alpha = 0;
		$model->ijin = 0;
		$model->mengetahui = 'Santi Susanti';
		$model->menyetujui = 'Dedy Kusmana';
		
		$komponen = new PenggajiankompT();
		$modPegawai = new KPPegawaiM();

		if(isset($_POST["KPPenggajianpegT"]))
		{
			$transaction = Yii::app()->db->beginTransaction();
			$pesan = array();
			try{
				$model->attributes = $_POST["KPPenggajianpegT"];
				$modPegawai->attributes = $_POST["KPPegawaiM"];
				
				$periode		= explode(' ', $_POST['KPPenggajianpegT']['periodegaji']);
				$periode_bulan  = $periode[0];
				$periode_tahun  = $periode[1];
				$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
				$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';
				
				$model->periodegaji = $periode_gaji;
				$model->tglpenggajian = $format->formatDateTimeMediumForDB($_POST['KPPenggajianpegT']['tglpenggajian']);
				$model->penerimaanbersihpertahun = $model->penerimaanbersih * 12;
				
				if(!$model->validate())
				{
					foreach($model->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}else{
					
					$model->save();
					foreach($_POST["PenggajiankompT"] as $key=>$val)
					{
						$komponen = new PenggajiankompT();
						$komponen->attributes = $_POST["PenggajiankompT"][$key];
						$komponen->penggajianpeg_id = $model->penggajianpeg_id;
						
						if(!$komponen->validate())
						{
							foreach($komponen->getErrors() as $key=>$val)
							{
								$pesan[] = $key . " : " . implode(", ", $val);
							}
						}else{
							$komponen->save();
							$this->saveBuktiBayar($model, $_POST['KPPegawaiM'], $komponen->jumlah);							
						}
					}
					
				}
				
				$bulan 	= $format->formatBulanInaLengkap(date('M', strtotime($model->periodegaji)));
				$model->periodegaji = $bulan.' '.date('Y', strtotime($model->periodegaji));
				if(count($pesan) > 0)
				{	
					throw new Exception(implode("<br>", $pesan));
				}else{
					$transaction->commit();
					Yii::app()->user->setFlash('success',"Data berhasil disimpan");
					$this->redirect(array('create'));
				}
				
			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
			}
		}
		
		$this->render('create',array(
			"model"=>$model,
			"modPegawai"=>$modPegawai,
			"komponen"=>$komponen
		));
	}
	
	public function actionUpdate($updateid = null)
	{
		$format = new CustomFormat;
		$model = KPPenggajianpegT::model()->findByPk($updateid);
		$modPegawai = KPPegawaiM::model()->findByPk($model->pegawai_id);
		$bulan 	= $format->formatBulanInaLengkap(date('M', strtotime($model->periodegaji)));
		$model->periodegaji = $bulan.' '.date('Y', strtotime($model->periodegaji));
		
		if(!is_null($model->pengeluaranumum_id))
		{
			Yii::app()->user->setFlash('error',"Data penggajian sudah diproses oleh petugas akutansi");
		}		
		
		if(isset($_POST["KPPenggajianpegT"]))
		{
			$transaction = Yii::app()->db->beginTransaction();
			$pesan = array();
			try{
				$model->attributes = $_POST["KPPenggajianpegT"];
				$modPegawai->attributes = $_POST["KPPegawaiM"];
				
				$periode		= explode(' ', $_POST['KPPenggajianpegT']['periodegaji']);
				$periode_bulan  = $periode[0];
				$periode_tahun  = $periode[1];
				$bulan_angka    = $format->formatAngkaBulan($periode_bulan);
				$periode_gaji 	= $periode_tahun.'-'.$bulan_angka.'-01';
				
				$model->periodegaji = $periode_gaji;
				$model->tglpenggajian = $format->formatDateTimeMediumForDB($_POST['KPPenggajianpegT']['tglpenggajian']);
				$model->penerimaanbersihpertahun = $model->penerimaanbersih * 12;
				
				if(!is_null($model->pengeluaranumum_id))
				{
					$pesan[] = "Pengluaran ID : Sudah dikeluarkan oleh akutansi";
				}
				
				if(!$model->validate())
				{
					foreach($model->getErrors() as $key=>$val)
					{
						$pesan[] = $key . " : " . implode(", ", $val);
					}
				}else{
					
					$model->save();
					foreach($_POST["PenggajiankompT"] as $key=>$val)
					{
						$komponen = PenggajiankompT::model()->findByPk($_POST["PenggajiankompT"][$key]["penggajiankomp_id"]);
						$komponen->attributes = $_POST["PenggajiankompT"][$key];
						
						if(!$komponen->validate())
						{
							foreach($komponen->getErrors() as $key=>$val)
							{
								$pesan[] = $key . " : " . implode(", ", $val);
							}
						}else $komponen->save();
					}
					
				}
				
				$bulan 	= $format->formatBulanInaLengkap(date('M', strtotime($model->periodegaji)));
				$model->periodegaji = $bulan.' '.date('Y', strtotime($model->periodegaji));
				if(count($pesan) > 0)
				{	
					throw new Exception(implode("<br>", $pesan));
				}else{
					$transaction->commit();
					Yii::app()->user->setFlash('success',"Data berhasil disimpan");
					$this->redirect(array('update', "updateid"=>$updateid));
				}
				
			}catch(Exception $exc){
				$transaction->rollback();
				Yii::app()->user->setFlash('error',"Data gagal disimpan ". MyExceptionMessage::getWarning($exc,true));
			}
		}
		
		$this->render('update',array(
			"model"=>$model,
			"modPegawai"=>$modPegawai
		));
		
	}
	
	public function actionView($id){}
	
	public function actionPrint($caraPrint = null){
		$format = new CustomFormat;
		$model = new KPPenggajianpegT;
		
		if(isset($_GET["KPPenggajianpegT"]))
		{
			$model->attributes = $_GET["KPPenggajianpegT"];
			$model->periodegaji = $format->formatDateTimeMediumForDB($_GET['KPPenggajianpegT']['periodegaji']);
		}
		
        $format = new CustomFormat();
		if($caraPrint == 'print')
		{
			$this->layout = '//layouts/printWindows';
			$this->render('cetak', array(
				'model' => $model,
				'caraPrint' => $caraPrint
			));
		}else if($caraPrint == 'excel')
		{
			$this->layout = '//layouts/printExcel';
			$this->render('cetak', array(
				'model' => $model,
				'caraPrint' => $caraPrint
			));
		}else if($_REQUEST['caraPrint'] == 'pdf')
		{
			$ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
			$posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
			$mpdf = new MyPDF('', $ukuranKertasPDF);
			$mpdf->useOddEven = 2;
			$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
			$mpdf->WriteHTML($stylesheet, 1);
			$mpdf->AddPage($posisi, '', '', '', '', 5, 5, 5, 5, 5, 5);
			$mpdf->WriteHTML($this->renderPartial('cetak', array(
				'model' => $model,
				'caraPrint' => $caraPrint,
			), true));
			$mpdf->Output();
		}		
		
	}

	public function actionPrintStruk($penggajianpeg_id = null)
	{
		$model = KPPenggajianpegT::model()->findByPk($penggajianpeg_id);
		$modPegawai = KPPegawaiM::model()->findByPk($model->pegawai_id);
		
		$this->layout = '//layouts/printWindows';
		$this->render('cetak_faktur', array(
			'model' => $model,
			'caraPrint' => $caraPrint
		));		
	}
	
	public function terbilang($x, $style=4, $strcomma=",")
	{
		if ($x < 0) {
			$result = "minus " . trim($this->ctword($x));
		} else {
			$arrnum = explode("$strcomma", $x);
			$arrcount = count($arrnum);
			if($arrcount == 1) {
				$result = trim($this->ctword($x));
			}else if ($arrcount > 1) {
				$result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
			}
		}
		switch ($style){
			case 1: //1=uppercase  dan
				$result = strtoupper($result);
			break;
			case 2: //2= lowercase
				$result = strtolower($result);
			break;
			case 3: //3= uppercase on first letter for each word
				$result = ucwords($result);
			break;
			default: //4= uppercase on first letter
				$result = ucfirst($result);
			break;
		}
		return $result;
    }
	
    public function ctword($x) {
        $x = abs($x);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($x < 12) {
            $temp = " " . $number[$x];
        } else if ($x < 20) {
            $temp = $this->ctword($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->ctword($x - 100);
        } else if ($x < 1000) {
            $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->ctword($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
        }
        return $temp;
    }

    protected function saveBuktiBayar($model, $modPegawai, $jumlah)
    {
    	$modBuktiBayar = new TandabuktibayarT();
    	$modBuktiBayar->ruangan_id = Yii::app()->user->getState('ruangan_id');
    	$modBuktiBayar->shift_id = Yii::app()->user->getState('shift_id');
    	$modBuktiBayar->nourutkasir = Generator::noUrutKasir($modBuktiBayar->ruangan_id);
    	$modBuktiBayar->nobuktibayar = Generator::noBuktiBayar();
    	$modBuktiBayar->tglbuktibayar = $model->tglpenggajian;
    	$modBuktiBayar->carapembayaran = "TUNAI";
    	$modBuktiBayar->darinama_bkm = $modPegawai['nama_pegawai'];
    	$modBuktiBayar->alamat_bkm = "RSUD TASIKMALAYA";
    	$modBuktiBayar->sebagaipembayaran_bkm = "Peminjaman";
    	$modBuktiBayar->jmlpembulatan = 0;
    	$modBuktiBayar->jmlpembayaran = $jumlah;
    	$modBuktiBayar->biayaadministrasi = 0;
    	$modBuktiBayar->biayamaterai = 0;
    	$modBuktiBayar->uangditerima = $jumlah;
    	$modBuktiBayar->uangkembalian = 0;
    	$modBuktiBayar->create_time = date('Y-m-d H:i:s');
    	$modBuktiBayar->create_loginpemakai_id = Yii::app()->user->getState('user_id');
    	$modBuktiBayar->create_ruangan = Yii::app()->user->getState('ruangan_id');
    	$modBuktiBayar->save();

    	$criteria = new CDbCriteria;
    	$criteria->compare('pegawai_id', $model->pegawai_id);
        $criteria->addCondition('sisapinjaman < jumlahpinjaman');
        $pinjaman = PinjamanpegT::model()->find($criteria);
		
        $tandabuktibayar = null;
		$pinjamanpeg_id = $pinjaman['pinjamanpeg_id'];
		$sisapinjaman = $pinjaman['sisapinjaman'] + $jumlah;
		$pinjDetail = PinjampegdetT::model()->findbyAttributes(array('pinjamanpeg_id'=>$pinjamanpeg_id, 'tandabuktibayar_id'=>$tandabuktibayar));
		$pinjampegdet_id = $pinjDetail->pinjampegdet_id;
		if($jumlah>=$pinjDetail->jmlcicilan){
			PinjampegdetT::model()->updateByPk($pinjampegdet_id, array('tandabuktibayar_id'=>$modBuktiBayar->tandabuktibayar_id));	
		}
		PinjamanpegT::model()->updateByPk($pinjamanpeg_id, array('sisapinjaman'=>$sisapinjaman));
}
	
}
