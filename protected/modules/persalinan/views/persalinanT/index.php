<?php $this->renderPartial('/_ringkasDataPasien', array('modPendaftaran' => $modPendaftaran, 'modPasien' => $modPasien)); ?>
<?php $this->renderPartial('_persalinan', array('modPersalinan' => $modPersalinan)); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<fieldset>
<legend>Persalinan</legend>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'pspersalinan-t-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#',
        ));
?>
<style>
    .control-group{
        padding:5px;
    }
    td .control-group:hover{
        background-color: #B5C1D7;
    }
    .additional-text{
        display:inline-block;
        font-size: 11px;
    }
    </style>
<p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary($model); ?>
<br/>
<table class='table-condensed'>
    <tr>
        <td width='5%'>
        <td width="45%">
            <?php echo $form->dropDownListRow($model, 'kegiatanpersalinan_id', CHtml::listData(PSKegiatanpersalinanM::model()->findAll(), 'kegiatanpersalinan_id', 'kegiatanpersalinan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'kelsebababortus_id', CHtml::listData(PSKelsebababortusM::model()->findAll(), 'kelsebababortus_id', 'kelsebababortus_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'sebababortus_id', CHtml::listData(PSSebababortusM::model()->findAll(), 'sebababortus_id', 'sebababortus_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'pegawai_id', CHtml::listData($model->DokterItems, 'pegawai_id', 'nama_pegawai'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'jeniskegiatanpersalinan', PSJeniskegiatanpersalinanM::items(), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
            <?php //echo $form->textFieldRow($model,'tglmulaipersalinan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglmulaipersalinan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglmulaipersalinan',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                            //
                            //'onkeypress' => "js:function(){getUmur(this);}",
                            //'onSelect' => 'js:function(){$(this).close();}',
                            //'yearRange' => "-60:+0",
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model, 'tglmulaipersalinan'); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglselesaipersalinan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglselesaipersalinan',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                            //
                            //'onkeypress' => "js:function(){getUmur(this);}",
                           // 'onSelect' => 'js:function(){getUmur(this);}',
                            //'yearRange' => "-60:+0",
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model, 'tglselesaipersalinan'); ?>
                </div>
            </div>
            <?php //echo $form->textFieldRow($model, 'tglselesaipersalinan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model, 'lamapersalinan_jam', array('class' => 'span3 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'lamapersalinan_jam', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    echo $form->textField($model, 'lamapersalinan_jam', array('class'=>'span1 numbersOnly','onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100));
                    ?> <div class='additional-text'>Jam</div>
                    <?php echo $form->error($model, 'lamapersalinan_jam'); ?>
                </div>
            </div>
            <?php echo $form->dropDownListRow($model, 'carapersalinan', Carapersalinan::items(), array('class' => 'span3 ', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
            <?php echo $form->dropDownListRow($model, 'posisijanin', PosisiJanin::items(), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
            <table>
                <tr>
                    <td width="143px">
                        <div align="right">
                            <?php echo $form->Label($model, 'tglmelahirkan'); ?>
                        </div>    
                    </td>
                    <td width='130px'><?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglmelahirkan',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                    //
                                    //'onkeypress' => "js:function(){getUmur(this);}",
                                    //'onSelect' => 'js:function(){getUmur(this);}',
                                    //'yearRange' => "-60:+0",
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker2', 'onkeypress' => "return $(this).focusNextInputField(event)"
                                ),
                            ));
                            ?></td>
                    <td>
                        <div class="checkbox inline">
                            <?php echo $form->checkBox($model, 'islahirdirs', array('onkeypress' => "return $(this).focusNextInputField(event)")); ?>
                            <?php echo CHtml::activeLabel($model, 'islahirdirs'); ?>
                        </div> 
                        <div class="checkbox inline">
                    </td>
                </tr>
            </table>
        </td>
        <td width='50%'>
            
            <?php //echo $form->textFieldRow($model, 'tglmelahirkan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>

            <?php echo $form->dropDownListRow($model, 'keadaanlahir',Keadaanlahir::items(), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100)); ?>
            <?php //echo $form->textFieldRow($model, 'masagestasi_minggu', array('class' => 'span3 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'masagestasi_minggu', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    echo $form->textField($model, 'masagestasi_minggu', array('class'=>'span1 numbersOnly','onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100));
                    ?> <div class='additional-text'>Minggu</div><br/>
                    <?php echo $form->error($model, 'masagestasi_minggu'); ?>
                </div>
            </div>
            <?php echo $form->dropDownListRow($model, 'paritaske', CHtml::listData(Params::textAngka(), 'id', 'name'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 30)); ?>
            <?php //echo $form->textFieldRow($model, 'jmlkelahiranhidup', array('class' => 'span3 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model, 'jmlkelahiranmati', array('class' => 'span3 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'jmlkelahiranhidup', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    echo $form->textField($model, 'jmlkelahiranhidup', array('class'=>'span1 numbersOnly','onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100));
                    ?> <div class='additional-text'>Orang</div>
                    <?php echo $form->error($model, 'jmlkelahiranhidup'); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'jmlkelahiranmati', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    echo $form->textField($model, 'jmlkelahiranmati', array('class'=>'span1 numbersOnly','onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100));
                    ?> <div class='additional-text'>Orang</div>
                    <?php echo $form->error($model, 'jmlkelahiranmati'); ?>
                </div>
            </div>
            
            <?php echo $form->dropDownListRow($model, 'sebabkematian', Sebabkematian::items(), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 100)); ?>
            <?php //echo $form->textFieldRow($model, 'tglabortus', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglabortus', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglabortus',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            'maxDate' => 'd',
                            
                            //'onkeypress' => "js:function(){getUmur(this);}",
                            //'onSelect' => 'js:function(){getUmur(this);}',
                            //'yearRange' => "-60:+0",
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                    <?php echo $form->error($model, 'tglabortus'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model, 'jmlabortus', array('class' => 'span3 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textAreaRow($model, 'catatan_dokter', array('onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'bidan_id',  CHtml::listData($model->BidanItems, 'pegawai_id', 'nama_pegawai'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($model, 'paramedis_id',  CHtml::listData($model->ParamedisItems, 'pegawai_id', 'nama_pegawai'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
            <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
            <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
        </td></tr>
</table>
<div class="form-actions"> 
    <?php
    echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                    Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
    ?>
    <?php
    echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), Yii::app()->createUrl($this->module->id . '/daftarPasien/index'), array('class' => 'btn btn-danger',
        'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
    ?>
</div> 

<?php $this->endWidget(); ?>
</fieldset>
<?php 
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>