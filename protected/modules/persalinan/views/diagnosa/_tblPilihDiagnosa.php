<div id='tblDiagnosa'>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rjdiagnosa-m-grid',
    'dataProvider'=>$modDiagnosa->searchDiagnosis(),
    'filter'=>$modDiagnosa,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-condensed',
    'columns'=>array(
        array(
            'name'=>'diagnosa_nourut',
            'value'=>'$data->diagnosa_nourut',
            'filter'=>false,
        ),
        'diagnosa_kode',
        'diagnosa_nama',
        'diagnosa_namalainnya',
        'diagnosa_katakunci',
        array(
            'header'=>'Kelompok Diagnosa',
            'type'=>'raw',
            'value'=>'CHtml::dropDownList("kelompokDiagnosa_$data->diagnosa_id","",CHtml::listData(PSKelompokDiagnosaM::model()->findAll(), "kelompokdiagnosa_id", "kelompokdiagnosa_nama"))',
        ),
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPasien",
                            "onClick" => "inputDiagnosa(this,$data->diagnosa_id);return false;"))',
        ),
        /*
        'diagnosa_imunisasi',
        'diagnosa_aktif',
        */
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 
</div>
   
<div id="tblKasuspenyakitDiagnosa" class="hide">
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rjkasuspenyakitdiagnosa-m-grid',
    'dataProvider'=>$modKasuspenyakitDiagnosa->search(),
    'filter'=>$modKasuspenyakitDiagnosa,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-condensed',
    'columns'=>array(
        array(
            'name'=>'diagnosa_nourut',
            'value'=>'$data->diagnosa->diagnosa_nourut',
            'filter'=>false,
        ),
        array(
            'name'=>'diagnosa_kode',
            'value'=>'$data->diagnosa->diagnosa_kode',
        ),
        array(
            'name'=>'diagnosa_nama',
            'value'=>'$data->diagnosa->diagnosa_nama',
        ),
        array(
            'name'=>'diagnosa_namalainnya',
            'value'=>'$data->diagnosa->diagnosa_namalainnya',
        ),
        array(
            'name'=>'diagnosa_katakunci',
            'value'=>'$data->diagnosa->diagnosa_katakunci',
        ),
        array(
            'header'=>'Kelompok Diagnosa',
            'type'=>'raw',
            'value'=>'CHtml::dropDownList("kelompokDiagnosa_$data->diagnosa_id","",CHtml::listData(PSKelompokDiagnosaM::model()->findAll(), "kelompokdiagnosa_id", "kelompokdiagnosa_nama"))',
        ),
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPasien",
                            "onClick" => "inputDiagnosa(this,$data->diagnosa_id);return false;"))',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?> 
</div>