<?php
$this->breadcrumbs=array(
	'Diagnosa',
);

$this->widget('bootstrap.widgets.BootAlert');
$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));

$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/persalinan/anamnesa',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/persalinan/PemeriksaanFisik',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/persalinan/laboratorium',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Radiologi', 'url'=>$this->createUrl('/persalinan/radiologi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/persalinan/rehabMedis',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/persalinan/konsulGizi',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/persalinan/konsulPoli',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Tindakan', 'url'=>$this->createUrl('/persalinan/tindakan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
        array('label'=>'Diagnosis', 'url'=>'', 'active'=>true),
        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/persalinan/bedahSentral',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/persalinan/rujukanKeluar',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Reseptur', 'url'=>$this->createUrl('/persalinan/reseptur',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/persalinan/pemakaianBahan',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id))),
    ),
));

?>


<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-morbiditas-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
    
    <?php echo $form->errorSummary($modMorbiditas[0]); ?>
    
    <table class="table-condensed">
        <tr>
            <td width="50%">
                <?php echo $form->dropDownListRow($modMorbiditas[0],'[0]pegawai_id',CHtml::listData($modMorbiditas[0]->DokterItems, 'pegawai_id', 'nama_pegawai'), array('onkeypress'=>"return $(this).focusNextInputField(event);"));?>
                <?php //echo $form->dropDownListRow($modMorbiditas[0],'[0]kasusdiagnosa',KasusDiagnosa::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
            </td>
            <td width="50%">
                <?php echo $form->labelEx($modMorbiditas[0],'[0]tglmorbiditas', array('class'=>'control-label')) ?>
                <?php $modMorbiditas[0]->tglmorbiditas = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modMorbiditas[0]->tglmorbiditas, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                     'model'=>$modMorbiditas[0],
                                     'attribute'=>'[0]tglmorbiditas',
                                     'mode'=>'datetime',
                                     'options'=> array(
                                     'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                     'maxDate'=>'d',   
                                         ),
                                     'htmlOptions'=>array('readonly'=>true,
                                     'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); ?>
                </div>
            </td>
        </tr>
    </table>
    <div class="control-group ">
        <div class="controls">
            <label for="berdasarKasusPenyakit" class="checkbox">
                <?php echo CHtml::checkBox('berdasarKasusPenyakit', false, array('onclick'=>'daftarDiagnosa(this)')); ?> Berdasarkan Kasus Penyakit
            </label>
        </div>
    </div>
    
    <?php $this->renderPartial('_tblPilihDiagnosa',array('modDiagnosa'=>$modDiagnosa,'modKasuspenyakitDiagnosa'=>$modKasuspenyakitDiagnosa)); ?>
   
    <fieldset>
        <legend>Diagnosa Pasien</legend>
        <table id="tblDiagnosaPasien" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>Tgl Diagnosa</th>
                    <th>Kelompok Diagnosa</th>
                    <th>Kode</th>
                    <th>Nama Diagnosa</th>
                    <th>Nama Lain</th>
                    <th>Kata Kunci</th>
                    <th>Diagnosa Tindakan</th>
                    <th>Sebab Diagnosa</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($listMorbiditas)){
                        $this->renderPartial('_listDiagnosa',array('modMorbiditas'=>$listMorbiditas));
                    }
                
                    if($newInput && !$successSave) {
                        $this->renderPartial('_cekValidDiagnosa',array('modMorbiditas'=>$modMorbiditas));
                    }
                ?>
            </tbody>
        </table>
    </fieldset>
            
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
function inputDiagnosa(obj,idDiagnosa)
{
    var idDiagnosa = idDiagnosa;
    var idKelDiagnosa = $(obj).parent().parent().find('select[name^="kelompokDiagnosa_"]').val();
    var tglDiagnosa = $('#RJPasienMorbiditasT_0_tglmorbiditas').val();
    if(!cekInputDiagnosa(idDiagnosa)) {
        $(obj).parent().parent().css("background-color", "yellow");
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormDiagnosis')?>',
                 'data':{tglDiagnosa:tglDiagnosa, idDiagnosa:idDiagnosa, idKelDiagnosa:idKelDiagnosa},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblDiagnosaPasien tbody').append(data.form);
                        renameInput('Morbiditas','diagnosa');
                        renameInput('Morbiditas','kelompokDiagnosa');
                        renameInput('Morbiditas','diagnosaTindakan');
                        renameInput('Morbiditas','sebabDiagnosa');
                        renameInput('Morbiditas','infeksiNosokomial');
                 } ,
                 'cache':false});
    }
}
             
function cekInputDiagnosa(idDiagnosa)
{
    var sudahAda = false;
    $('#tblDiagnosaPasien').find('input[class$="idDiagnosa"]').each(function(){
        if(this.value == idDiagnosa)
            sudahAda = true;
    });
    return sudahAda;
}

function daftarDiagnosa(obj)
{
    if($(obj).is(':checked')){
        $('#tblDiagnosa').hide();
        $('#tblKasuspenyakitDiagnosa').show();
    } else {
        $('#tblDiagnosa').show();
        $('#tblKasuspenyakitDiagnosa').hide
        ();
    }
    
//    $('#tblDiagnosaPasien tbody tr').detach();
}

function addDiagnosaTindakan(obj,idDiagnosa)
{
    $('#dialogDiagnosaTindakan #tr').val(idDiagnosa);
    $('#dialogDiagnosaTindakan').dialog('open');
    $('#dialogDiagnosaTindakan').find("input[name$='[diagnosaicdix_id]']").each(function(){
        $(this).removeAttr('checked');
    });
}

function addSebabDiagnosa(obj)
{
    $('#dialogSebabDiagnosa').dialog('open');
}

function setDiagnosaTindakan(obj,idDiagnosaTind,namaDiagnosaTind)
{
    var idTr = $('#dialogDiagnosaTindakan #tr').val();
    $('#tr_'+idTr).find('input[name$="[diagnosaTindakan]"]').val(idDiagnosaTind);
    $('#tr_'+idTr+' #diagnosaTindakanNama').html(namaDiagnosaTind);
    $('#dialogDiagnosaTindakan').dialog('close');
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblDiagnosaPasien tr').length;
    var i = -1;
    $('#tblDiagnosaPasien tr').each(function(){
        if($(this).has('input[name$="[diagnosa]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
</script>

<?php 
//========= Dialog buat diagnosa tindakan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogDiagnosaTindakan',
    'options'=>array(
        'title'=>'Diagnosa Tindakan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>700,
        'height'=>400,
        'resizable'=>false,
    ),
));
echo CHtml::hiddenField("tr", '', array('readonly'=>true,'class'=>'span1'));
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rjdiagnosatindakan-m-grid',
    'dataProvider'=>$modDiagnosaicdixM->search(),
    'filter'=>$modDiagnosaicdixM,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-condensed',
    'columns'=>array(
        'diagnosaicdix_nourut',
        'diagnosaicdix_kode',
        'diagnosaicdix_nama',
        'diagnosaicdix_namalainnya',
        'diagnosatindakan_katakunci',
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                            "id" => "selectPasien",
                            "onClick" => "setDiagnosaTindakan(this,$data->diagnosaicdix_id,\'$data->diagnosaicdix_nama\');return false;"))',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end diagnosa tindakan =============================
?>  

<?php 
//========= Dialog buat sebab diagnosa  =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogSebabDiagnosa',
    'options'=>array(
        'title'=>'Sebab Diagnosa',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>500,
        'height'=>400,
        'resizable'=>false,
    ),
));

     echo $form->radioButtonListRow($modMorbiditas[0], 'sebabdiagnosa_id', CHtml::listData($modSebabDiagnosa, 'sebabdiagnosa_id', 'sebabdiagnosa_nama'));

$this->endWidget();
//========= end sebab diagnosa =============================
?> 