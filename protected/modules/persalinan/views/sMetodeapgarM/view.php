<?php
$this->breadcrumbs=array(
	'Psmetodeapgar Ms'=>array('index'),
	$model->metodeapgar_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Metode Apgar ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Metode ApgarM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PSMetodeapgarM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PSMetodeapgarM', 'icon'=>'pencil','url'=>array('update','id'=>$model->metodeapgar_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PSMetodeapgarM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->metodeapgar_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Metode Apgar  ', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'metodeapgar_id',
		'akronim',
		'kriteria',
		'nilai_2',
		'nilai_1',
		'nilai_0',
		'metodeapgar_aktif',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>