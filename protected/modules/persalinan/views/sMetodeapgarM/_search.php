<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'psmetodeapgar-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'metodeapgar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'akronim',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'kriteria',array('class'=>'span3','maxlength'=>100)); ?>

	<?php //echo $form->textAreaRow($model,'nilai_2',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'nilai_1',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php //echo $form->textAreaRow($model,'nilai_0',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->checkBoxRow($model,'metodeapgar_aktif', array('checked'=>'$data->metodeapgar_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
