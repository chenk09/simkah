
<?php
$this->breadcrumbs=array(
	'Gfobat Alkes Ms'=>array('index'),
	$model->obatalkes_id=>array('view','id'=>$model->obatalkes_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Obat Alkes ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFObatAlkesM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','View').' GFObatAlkesM', 'icon'=>'eye-open', 'url'=>array('view','id'=>$model->obatalkes_id))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Obat Alkes', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate',array('model'=>$model,'modUbahHarga'=>$modUbahHarga,'modObatSupplier'=>$modObatSupplier,'obatdetail'=>$obatdetail)); ?>
<?php //$this->widget('TipsMasterData',array('type'=>'update'));?>