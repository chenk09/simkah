<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfobat-alkes-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table>
            <tr>
                <td>
                     <?php echo $form->hiddenField($model,'obatalkes_id',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200,'onkeyup'=>'generateKode(this)')); ?>
                     <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>200,'onkeyup'=>'generateKode(this)')); ?>
                     <?php echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50,'onkeyup'=>'numbersOnly(this)')); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model,'harganetto', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'harganetto',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>TRUE)); ?>
                            <?php echo CHtml::htmlButton('<i class="icon-plus-sign icon-white"></i>', 
                                                            array('class'=>'btn btn-primary','onclick'=>"ubahHargaObat()",
                                                                  'id'=>'edit','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                  'rel'=>'tooltip','title'=>'Klik untuk menambah '.$model->getAttributeLabel('harganetto'))) ?>
                            <?php echo $form->error($model, 'propinsi_id'); ?>
                        </div>
                    </div>
                     
                    <?php echo $form->textFieldRow($model,'hargajual',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>TRUE)); ?>
                     <?php echo $form->textFieldRow($model,'discount',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <div class='control-group'>
                            <?php echo $form->labelEx($model,'tglkadaluarsa', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglkadaluarsa',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                              )); ?>
                         </div>
                     </div>    
 <?php echo $form->textFieldRow($model,'minimalstok',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                     <?php echo $form->dropDownListRow($model,'sumberdana_id',
                                               CHtml::listData($model->SumberDanaItems, 'sumberdana_id', 'sumberdana_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'satuanbesar_id',
                                               CHtml::listData($model->SatuanBesarItems, 'satuanbesar_id', 'satuanbesar_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                   
                     <?php echo $form->dropDownListRow($model,'satuankecil_id',
                                               CHtml::listData($model->SatuanKecilItems, 'satuankecil_id', 'satuankecil_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    
                    
                    
                      <?php echo $form->dropDownListRow($model,'lokasigudang_id',
                                               CHtml::listData($model->lokasiGudangItems, 'lokasigudang_id', 'lokasigudang_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                      <?php echo $form->dropDownListRow($model,'obatalkes_kadarobat',ObatAlkesKadarObat::items(),
                                       array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                       'empty'=>'-- Pilih --',)); ?>
                </td>
                <td>
          
                     <?php echo $form->textFieldRow($model,'kemasanbesar',array('class'=>'span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <div class="control-label"> Kekuatan</div>
                    <div class="controls">
                      <?php echo $form->textField($model,'kekuatan',array('class'=>'span2 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                      <?php echo $form->dropDownList($model,'satuankekuatan',  SatuanKekuatan::items(),
                       array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                       'empty'=>'-- Pilih --',)); ?>
                    </div>

                     <?php echo $form->dropDownListRow($model,'formularium',  Formularium::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'obatalkes_kategori',ObatAlkesKategori::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                     <?php echo $form->dropDownListRow($model,'discountinue',array('1'=>'Ya','0'=>'Tidak'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <div class="control-label">
                        Therapi Obat
                        <?php echo $form->hiddenField($model,'therapiobat_id'); ?>
                    </div>
                    <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'therapiobat',
                                           'value'=>$model->therapiobat->therapiobat_nama,
                                            'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/TherapiObat').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                            'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui )
                                                           {
                                                            $(this).val(ui.item.label);
                                                            return false;
                                                            }',
                                                       'select'=>'js:function( event, ui ) {
                                                           $(\'#GFObatAlkesM_therapiobat_id\').val(ui.item.therapiobat_id);
                                                           $(\'#therapiobat\').val(ui.item.therapiobat_nama);
                                                            return false;
                                                        }',
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>false,
                                                'placeholder'=>'Therapi Obat',
                                                'size'=>13,
                                                'class'=>'span2',
                                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogtherapiobat'),
                                    )); ?>
                    </div>
                    <?php echo $form->dropDownListRow($model,'pbf_id',
                                               CHtml::listData($model->PbfItems, 'pbf_id', 'pbf_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <?php echo $form->dropDownListRow($model,'generik_id',
                                               CHtml::listData($model->generikItems, 'generik_id', 'generik_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <div class="control-label">
                        Jenis Obat Alkes
                        <?php echo $form->hiddenField($model,'jenisobatalkes_id'); ?>
                    </div>
                    <div class="controls">
                    <?php $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'jenisobatalkes',
                                           'value'=>$model->jenisobatalkes->jenisobatalkes_nama,
                                            'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('ActionAutoComplete/JenisObatAlkes').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                            'options'=>array(
                                                       'showAnim'=>'fold',
                                                       'minLength' => 2,
                                                       'focus'=> 'js:function( event, ui )
                                                           {
                                                            $(this).val(ui.item.label);
                                                            return false;
                                                            }',
                                                       'select'=>'js:function( event, ui ) {
                                                           $(\'#GFObatAlkesM_jenisobatalkes_id\').val(ui.item.jenisobatalkes_id);
                                                           $(\'#jenisobatalkes\').val(ui.item.jenisobatalkes_nama);
                                                            return false;
                                                        }',
                                            ),
                                            'htmlOptions'=>array(
                                                'readonly'=>false,
                                                'placeholder'=>'Jenis Obat Alkes',
                                                'size'=>13,
                                                'class'=>'span2',
                                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                                            ),
                                            'tombolDialog'=>array('idDialog'=>'dialogjenisobatalkes'),
                                    )); ?>
                    </div>
                    <?php echo $form->dropDownListRow($model,'obatalkes_golongan',  ObatAlkesGolongan::items(),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
                    <?php echo $form->checkBoxRow($model,'obatalkes_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

	
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="control-group">
                        <?php echo Chtml::label('Supplier','Supplier',array('class'=>'control-label'));?>
                        <div class="controls">
                           <?php 
                              $arrSupplier = array();
                                   foreach($modObatSupplier as $data)
                                     {
                                        $arrSupplier[] = $data['supplier_id'];
                                     }

                                $this->widget('application.extensions.emultiselect.EMultiSelect',
                                             array('sortable'=>true, 'searchable'=>true)
                                        );
                                echo CHtml::dropDownList(
                                'supplier_id[]',
                                $arrSupplier,
                                CHtml::listData(GFSupplierM::model()->findAll('supplier_aktif=TRUE ORDER BY supplier_nama'), 'supplier_id', 'supplier_nama'),
                                array('multiple'=>'multiple','key'=>'obatalkes_id', 'class'=>'multiselect','style'=>'width:500px;height:150px')
                                        );
                          ?>
                        </div>    
                    </div>    
                </td> 
            </tr>
        </table>
        <?php echo $this->renderPartial('_UpdateObatAlkesDetail', array('model'=>$model,'form'=>$form)); ?>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.obatAlkesM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>

<?php $this->endWidget(); ?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'ubahHarga',
    'options'=>array(
        'title'=>'Ubah Harga Obat',
        'autoOpen'=>false,
        'width'=>600,
        'height'=>600,
        'modal'=>'true',
        'hide'=>'explode',
        'resizelable'=>false,
    ),
));

?>
        
 <?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlUbahHarga=  Yii::app()->createUrl($module.'/'.$controller.'/UbahHarga');
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gfobat-alkes-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>$urlUbahHarga,
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modUbahHarga); ?>


            <div class="control-group">
                    <?php echo $form->labelEx($modUbahHarga,'obatalkes_id', array('class'=>'control-label')) ?>
                        <div class="controls">
                              <?php echo CHtml::textField('namaObat','',array('readonly'=>TRUE));?>
                        </div>
                    </div>           
            <?php echo $form->hiddenField($modUbahHarga,'obatalkes_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->dropDownListRow($modUbahHarga,'sumberdana_id',
                                               CHtml::listData($model->SumberDanaItems, 'sumberdana_id', 'sumberdana_nama'),
                                               array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                               'empty'=>'-- Pilih --',)); ?>
            <?php echo $form->hiddenField($modUbahHarga,'loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'harganettoasal',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>TRUE)); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'hargajualasal',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>TRUE)); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'harganettoperubahan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'hargajualperubahan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'alasanperubahan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textFieldRow($modUbahHarga,'disetujuioleh',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
           
    <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit',)); ?>
          <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 'onClick'=>'$(\'#ubahHarga\').dialog(\'close\')')); ?>
               
   				<?php
$content = $this->renderPartial('../tips/tips',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>
<?php $this->endWidget('zii.widgets.jui.CJuiDialog');?>

        

<?php
if($this->checkRoles(Params::DEFAULT_ROLES_SUPERVISOR)=='1'){//Jika User Yang Aktif Sekarang Adalah Supervisor
    $userSupervisor=$this->checkRoles(Params::DEFAULT_ROLES_SUPERVISOR);
}else{
    $userSupervisor='0';//Bukan Supervisor
}    

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);

$urlgetKodeObatAlkes=Yii::app()->createUrl('ActionAjax/GetKodeObatAlkes');
$kodeObat = CHtml::activeId($model,'obatalkes_kode');
$idObatAlkes =  CHtml::activeId($model,'obatalkes_id');//Id Obat alkes dati tabel Obat Alkes     
$idHargaNettoObatAlkes =  CHtml::activeId($model,'harganetto'); 
$idHargaJualObatAlkes =  CHtml::activeId($model,'hargajual');  
$idHargaNettoAsal =  CHtml::activeId($modUbahHarga,'harganettoasal'); //dari ubah harga R
$idHargaJualAsal =  CHtml::activeId($modUbahHarga,'hargajualasal');  //dari ubah harga R
$idObatAlkesUbahHarga =  CHtml::activeId($modUbahHarga,'obatalkes_id');
$idLoginPemakai=  CHtml::activeId($modUbahHarga,'loginpemakai_id');
$idUserYangSekarangLogin = Yii::app()->user->id;
$namaObatAlkes = CHtml::activeId($model,'obatalkes_nama');
$js = <<< JS
function generateKode(obj)
{
   namaObat =obj.value;
   if(namaObat!=''){//Jika nama Obat Tidak Kosong  
       $.post("${urlgetKodeObatAlkes}",{namaObat: namaObat},
            function(data){
                $('#${kodeObat}').val(data.kodeObatBaru);      
        },"json");
   }else{//Jika Nama Obat Kosong
                $('#${kodeObat}').val('');      
   } 
}

function ubahHargaObat()
{
    userSuperVisor = ${userSupervisor};          
    if(userSuperVisor!='0'){ //Jika User Yang aktif Sekarang adalah Supervisor
        
        hargaNettoObatAlkes = $('#${idHargaNettoObatAlkes}').val();
        hargaJualObatAlkes = $('#${idHargaJualObatAlkes}').val();
        idObatAlkes = $('#${idObatAlkes}').val();
        namaObatAlkes = $('#${namaObatAlkes}').val();
        
        $('#${idLoginPemakai}').val(${idUserYangSekarangLogin});
        $('#${idObatAlkesUbahHarga}').val(idObatAlkes);
        $('#${idHargaNettoAsal}').val(hargaNettoObatAlkes);
        $('#${idHargaJualAsal}').val(hargaNettoObatAlkes);

        $('#namaObat').val(namaObatAlkes);
        $('#ubahHarga').dialog('open');
    }else{ //Jika User Yang Aktif Sekarang Bukan Supervisor
    
        $('#loginForm').dialog('open');
    }
}

function cekUser()
{
    username = $('#username').val();
    password = $('#password').val();
    
    hargaNettoObatAlkes = $('#${idHargaNettoObatAlkes}').val();
    hargaJualObatAlkes = $('#${idHargaJualObatAlkes}').val();
    idObatAlkes = $('#${idObatAlkes}').val();
    namaObatAlkes = $('#${namaObatAlkes}').val();    
        
    if(username=='')
    {
        alert('Username Tidak Boleh Kosong');
    }else if(password==''){
        alert('Password Tidak Boleh Kosong');
    }else{
         $.post("${url}/cekOtoritas", {username: username,password:password},
            function(data){
                    if(data.loginpemakai_id!='null'){ //Jika tidak ada ditemukan login pemakai Id
                        $('#${idLoginPemakai}').val(data.loginpemakai_id);
                        $('#${idObatAlkesUbahHarga}').val(idObatAlkes);
                        $('#${idHargaNettoAsal}').val(hargaNettoObatAlkes);
                        $('#${idHargaJualAsal}').val(hargaNettoObatAlkes);
                        $('#namaObat').val(namaObatAlkes);    
                    $('#ubahHarga').dialog('open');
                    }else{//Jika ada loginpemakai_id (username dan password cocok dan mempunyai akses supervisor)
                            alert(data.message);
                    }                       
            },"json");
    }
}

JS;
Yii::app()->clientScript->registerScript('generateKodeObatAlkes',$js,CClientScript::POS_HEAD);

                       
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
// Dialog buat nambah data propinsi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'loginForm',
    'options'=>array(
        'title'=>'User Identify',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>450,
        'minHeight'=>350,
        'resizable'=>false,
    ),
));
?>
    <div class="control-group">
        <?php echo Chtml::label('Username','Username',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo CHtml::textField('username','',array('class'=>'span3'));?>
        </div>    
    </div> 
    <div class="control-group">
        <?php echo Chtml::label('Passowrd','Passowrd',array('class'=>'control-label'));?>
        <div class="controls">
          <?php echo CHtml::textField('password','',array('class'=>'span3'));?>
        </div>    
    </div> 
     <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekUser()')); ?>
          <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'button', 'onClick'=>'$(\'#loginForm\').dialog(\'close\')')); ?>
               
    </div>
<?php
$this->endWidget();
//========= end propinsi dialog =============================
?>
        
<!-- =============================== beginWidget Therapi Obat ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogtherapiobat',
    'options'=>array(
        'title'=>'Pencarian Therapi Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modTherapiobat = new TherapiobatM('search');
$modTherapiobat->unsetAttributes();
if(isset($_GET['TherapiobatM'])) {
    $modTherapiobat->attributes = $_GET['TherapiobatM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'therapiobat-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTherapiobat->search(),
	'filter'=>$modTherapiobat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selecttherapiobat",
                                                    "onClick" => "\$(\"#GFObatAlkesM_therapiobat_id\").val($data->therapiobat_id);
                                                                          \$(\"#therapiobat\").val(\"$data->therapiobat_nama\");
                                                                          \$(\"#dialogtherapiobat\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'therapiobat_nama',
                'therapiobat_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

<!-- =============================== beginWidget Jenis Obat Alkes ============================= -->        
<?php
   $this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogjenisobatalkes',
    'options'=>array(
        'title'=>'Pencarian Jenis Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>400,
        'resizable'=>false,
        ),
    ));
   
$modTherapiobat = new JenisobatalkesM('search');
$modTherapiobat->unsetAttributes();
if(isset($_GET['JenisobatalkesM'])) {
    $modTherapiobat->attributes = $_GET['JenisobatalkesM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'jenisobatalkes-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modTherapiobat->search(),
	'filter'=>$modTherapiobat,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                            array(
                                                    "class"=>"btn-small",
                                                    "id" => "selectjenisobatalkes",
                                                    "onClick" => "\$(\"#GFObatAlkesM_jenisobatalkes_id\").val($data->jenisobatalkes_id);
                                                                          \$(\"#jenisobatalkes\").val(\"$data->jenisobatalkes_nama\");
                                                                          \$(\"#dialogjenisobatalkes\").dialog(\"close\");"
                                             )
                             )',
                        ),
                'jenisobatalkes_nama',
                'jenisobatalkes_namalain',
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<!-- ============================== endWidget TherapiObat ================================= -->

 <?php
$urlgetKodeObatAlkes=Yii::app()->createUrl('ActionAjax/GetKodeObatAlkes');
$kodeObat = CHtml::activeId($model,'obatalkes_kode');
                       
$js = <<< JS
function generateKode(obj)
{
   namaObat =obj.value;
   if(namaObat!=''){//Jika nama Obat Tidak Kosong  
       $.post("${urlgetKodeObatAlkes}",{namaObat: namaObat},
            function(data){
                $('#${kodeObat}').val(data.kodeObatBaru);      
        },"json");
   }else{//Jika Nama Obat Kosong
                $('#${kodeObat}').val('');      
   } 
}

JS;
Yii::app()->clientScript->registerScript('sfdasdasda',$js,CClientScript::POS_HEAD);

                       
$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

