<legend class="rim2">Informasi Daftar Pasien</legend>                         
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
    if(Yii::app()->user->getState('instalasi_id')==Params::INSTALASI_ID_RJ){//Jika Bukan Rawat Jalan

    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchPasienPersalinan(),
//                'filter'=>$model,
    'template'=>"{pager}{summary}\n{items}",

    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran'
                    ),
                    array(
                        'header'=>'No.Pendaftaran / No.Rekam Medik',
                        'type'=>'raw',
                        'value'=>'$data->noPendaftaranRekammedik',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        'value'=>'$data->namaNamaBin'
                    ),
                    array(
                        'header'=>'Instalasi / Ruangan',
                        'type'=>'raw',
                        'value'=>'$data->InsatalasiRuangan2',
                    ),
                    array(
                       'name'=>'Cara Masuk / Transportasi',
                        'type'=>'raw',
                        'value'=>'$data->caraMasukTransportasi',
                    ),
            
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                       'name'=>'Rujukan',
                        'type'=>'raw',
                        'value'=>'(!empty($data->asalrujukan_nama))? $data->asalrujukan_nama : "-"',
                    ),
                    array(
                       'header'=>'Kasus Penyakit / <br/> Kelas Pelayanan',
                        'type'=>'raw',
                        'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
                    ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                       'name'=>'statusperiksa',
                        'type'=>'raw',
                        'value'=>'$data->statusperiksa',
                    ),
                    array(
                        'header'=>'Periksa Kehamilan',
			             'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil'></i>",
                                                'options'=>array('title'=>'Persalinan'),
                                                'url'=>'Yii::app()->createUrl("persalinan/periksaKehamilan/index",array("idPendaftaran"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'header'=>'Imunisasi',
			            'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil-yellow'></i>",
                                                'options'=>array('title'=>'Kelahiran', 'class'=>'kelahiran'),
                                                'url'=>'Yii::app()->createUrl("persalinan/Imunisasi/index",array("idPendaftaran"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'header'=>'Keluarga Berencana',
			            'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil'></i>",
                                                'options'=>array('title'=>'Kelahiran', 'class'=>'kelahiran'),
                                                'url'=>'Yii::app()->createUrl("persalinan/KeluargaBerencana/index",array("idPendaftaran"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'header'=>'Kegiatan Bayi Tabung',
			             'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil'></i>",
                                                'options'=>array('title'=>'Kelahiran', 'class'=>'kelahiran'),
                                                'url'=>'Yii::app()->createUrl("persalinan/kegiatanBayiTabung/index",array("idPendaftaran"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),
                          'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'name'=>'Pemeriksaan Pasien',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/persalinan/anamnesa",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))',
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'header'=>'Tindak Lanjut RI',
			             'type'=>'raw',
                        'value'=>'(!empty($data->pasienpulang_id) ? "Pasien Rawat Inap" : CHtml::link("<i class=\'icon-user\'></i> ".$data->pasienpulang_id, "javascript:tindakLanjutRI(\'$data->pendaftaran_id\');",array("title"=>"Klik Untuk Mendaftarkan ke Rawat Inap"))) ',
                         'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                
                    
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        ));
     
    }else{
        
        $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchPasienPersalinan(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran'
                    ),
                    array(
                       //'name'=>'no_pendaftaran',
                        'header'=>'No.Pendaftaran / No.Rekam Medik',
                        'type'=>'raw',
                        'value'=>'$data->noPendaftaranRekammedik',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        
                        'value'=>'$data->namaNamaBin'
                    ),
                    array(
                        'header'=>'Instalasi / Ruangan',
                        'type'=>'raw',
                        'value'=>'$data->InsatalasiRuangan2',
                    ),
                    array(
                       'name'=>'Cara Masuk / Transportasi',
                        'type'=>'raw',
                        'value'=>'$data->caraMasukTransportasi',
                    ),
            
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                       'name'=>'Rujukan',
                        'type'=>'raw',
                        'value'=>'(!empty($data->asalrujukan_nama))? $data->asalrujukan_nama : "-"',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                       'name'=>'statusperiksa',
                        'type'=>'raw',
                        'value'=>'$data->statusperiksa',
                    ),
                    array(
                        'header'=>'Persalinan',
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil'></i>",
                                                'options'=>array('title'=>'Persalinan'),
                                                'url'=>'Yii::app()->createUrl("persalinan/persalinanT/index",array("id"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),
                    ),
                    array(
                        'header'=>'Kelahiran',
			'class'=>'bootstrap.widgets.BootButtonColumn',
                        'template'=>'{lihat}',
                        'buttons'=>array(
                                        'lihat' => array (
                                                'label'=>"<i class='icon-pencil'></i>",
                                                'options'=>array('title'=>'Kelahiran', 'class'=>'kelahiran'),
                                                'url'=>'Yii::app()->createUrl("persalinan/kelahiranbayiT/index",array("id"=>"$data->pendaftaran_id"))',                            
                                            ),
                            ),
                    ),
                    array(
                        'name'=>'Pemeriksaan Pasien',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/persalinan/anamnesaTPS",array("idPendaftaran"=>$data->pendaftaran_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien"))',
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                       'header'=>'Tindak Lanjut',
                       'type'=>'raw',
                       'value'=>'(!empty($data->pasienpulang_id))? $data->carakeluar : CHtml::link("<i class=icon-pencil></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Menindak Lanjuti Pasien","onclick"=>"{buatSession($data->pendaftaran_id,$data->pasien_id); addPasienPulang($data->pendaftaran_id,$data->pasien_id); $(\'#dialogPasienPulang\').dialog(\'open\');}"))',    
                       'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
//                    array(
//                       'header'=>'Tindak Lanjut',
//                       'type'=>'raw',
//                       'value'=>'(!empty($data->pasienpulang_id))? $data->carakeluar : CHtml::link("<i class=icon-pencil></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Menindak Lanjuti Pasien","onclick"=>"{buatSession($data->pendaftaran_id,$data->pasien_id); addPasienPulang($data->pendaftaran_id,$data->pasien_id); $(\'#dialogPasienPulang\').dialog(\'open\');}"))',    
//                       'htmlOptions'=>array('style'=>'text-align: center; width:40px')
//                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
        
    }
?>
<hr></hr>
<?php
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>
<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                 <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            <td>
                <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        <?php echo CHtml::activecheckBox($model, 'ceklis', array('uncheckValue'=>0,'rel'=>'tooltip' ,'data-original-title'=>'Cek untuk pencarian berdasarkan tanggal')); ?>
                        Tgl Masuk 
                    </label>
                    <div class="controls">
                        <?php   $format = new CustomFormat;
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        ));?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                   <div class="controls"> 
                            <?php    
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                        )); ?>
                    </div>
                </div> 
                 
                 
            </td>
        </tr>
    </table>
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
echo CHtml::hiddenField('pendaftaran_id');
if(isset($_GET['data']))
{
    echo CHtml::hiddenField('jumlahPersalinan', $_GET['data']); 
}
echo CHtml::hiddenField('pasien_id');

?>
 <?php
    echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-refresh icon-white"></i>')), Yii::app()->createUrl($this->module->id . '/asuhankeperawatan/admin'), array('class' => 'btn btn-danger',
        'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
    ?>
	<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
<?php $this->endWidget();?>
</fieldset>  

<?php 
// Dialog untuk pasienpulang_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPasienPulang',
    'options'=>array(
        'title'=>'Pasien Pulang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>
<script>
function addPasienPulang(idPendaftaran,idPasien)
{
    $('#pendaftaran_id').val(idPendaftaran);
    $('#pasien_id').val(idPasien);
    
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/addPasienPulang'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $('#dialogPasienPulang div.divForForm form').submit(addPasienPulang);
                    
                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false}, 
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','minDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    
                    
                }
                else
                {
                    $('#dialogPasienPulang div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogPasienPulang').dialog('close') \",1000);
                }
 
            } ",
    ))
?>;
    return false; 
}

</script>

<?php
$urlSession = Yii::app()->createUrl('actionAjax/buatSessionPendaftaranPasien');
$urlPasienRujukRI = Yii::app()->createUrl('actionAjax/PasienRujukRI');

$jscript = <<< JS
function buatSession(idPendaftaran,idPasien)
{
    $.post("${urlSession}", { idPendaftaran: idPendaftaran,idPasien: idPasien },
        function(data){
            'sukses';
    }, "json");
}


JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>

<?php
$jscript = <<< JS
function tindakLanjutRI(idPendaftaran)
{
    $('#dialogRujukanRI').dialog('open');
    $('#idPendaftaran').val(idPendaftaran);
}

function simpanPasienPulang()
{
    idPendaftaran=$('#idPendaftaran').val();
    alert(idPendaftaran);
    $.post("${urlPasienRujukRI}", { idPendaftaran: idPendaftaran},
        function(data){
        alert(data.pesan);
        $('#dialogRujukanRI').dialog('close')
    }, "json");
}

JS;
Yii::app()->clientScript->registerScript('rujukKerawatInap',$jscript, CClientScript::POS_HEAD);


// ===========================Dialog Rujukan ke RI=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRujukanRI',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Konfirmasi',
                        'autoOpen'=>false,
                        'minWidth'=>500,
                        'minHeight'=>100,
                        'resizable'=>false,
//                        'hide'=>explode,    
                         ),
                    ));
?>
<div align="center">Anda Yakin Akan Melakukan Tindak Lanjut Ke Rawat Inap ?
    <br/>
        <?php echo CHtml::hiddenField('idPendaftaran'); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Ya',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'simpanPasienPulang()')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Tidak',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-danger', 'type'=>'button','onclick'=>'$(\'#dialogRujukanRI\').dialog(\'close\')')); ?>
		
		
</div>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Rujukan ke RI================================

?>
