
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rjkasuspenyakitruangan-m-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#kasuspenyakit',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

                <?php if (isset($modDetails)){
                    echo $form->errorSummary($modDetails);
                } else {
                    echo $form->errorSummary($model);
                } ?>
                    <?php
                        $instalasi_id = Yii::app()->user->instalasi_id;
                        $modInstalasi = InstalasiM::model()->findByPK($instalasi_id);
                        echo CHtml::hiddenField('instalasi_id',$instalasi_id);
                        echo $form->textFieldRow($model,'instalasi_nama',array('value'=>$modInstalasi->instalasi_nama,'readonly'=>true,'class'=>'span2'));
                    ?>
                    <?php
                        $ruanganid = Yii::app()->user->ruangan_id;
                        $modruangan = RuanganM::model()->findByPK($ruanganid);
                        echo CHtml::hiddenField('ruanganid',$ruanganid,array('readonly'=>true));
                        echo $form->textFieldRow($model,'ruangan_nama',array('value'=>$modruangan->ruangan_nama,'readonly'=>true,'class'=>'span2',));
                    ?>
                <div>
                        <?php echo CHtml::label('Kasus Penyakit','',array('class'=>'control-label')); ?>
                        <div class="controls">
                                        <?php echo $form->hiddenField($model,'jeniskasuspenyakit_id', array('readonly'=>true)) ?>

                                        <?php $this->widget('MyJuiAutoComplete', array(
                                                               'name'=>'kasuspenyakit', 
                                                                'source'=>'js: function(request, response) {
                                                                       $.ajax({
                                                                           url: "'.Yii::app()->createUrl('ActionAutoComplete/JenisKasusPenyakit').'",
                                                                           dataType: "json",
                                                                           data: {
                                                                               term: request.term,
                                                                           },
                                                                           success: function (data) {
                                                                                   response(data);
                                                                           }
                                                                       })
                                                                    }',
                                                                'options'=>array(
                                                                           'showAnim'=>'fold',
                                                                           'minLength' => 2,
                                                                           'focus'=> 'js:function( event, ui )
                                                                               {
                                                                                $(this).val(ui.item.label);
                                                                                return false;
                                                                                }',
                                                                           'select'=>'js:function( event, ui ) {
                                                                               $(\'#PSKasuspenyakitruanganM_jeniskasuspenyakit_id\').val(ui.item.value);
                                                                               $(\'#kasuspenyakit\').val("");
                                                                                submitKasuspenyakitruangan();
                                                                                return false;
                                                                            }',
                                                                ),
                                                                'htmlOptions'=>array(
                                                                    'readonly'=>false,
                                                                    'placeholder'=>'Kasus Penyakit',
                                                                    'size'=>13,
                                                                    'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                ),
                                                                'tombolDialog'=>array('idDialog'=>'dialogkasuspenyakit'),
                                                        )); ?>
                        </div>
                </div>
        <table id="tabelKasuspenyakitruangan" class="table table-bordered table-striped table-condensed">
            <thead>
                <tr>
                    <th>Instalasi</th>
                    <th>Ruangan</th>
                    <th>Nama Kasus</th>
                    <th>Nama Lain</th>
                    <th>Hapus <br/>/ Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $ruangan_id = $_GET['id'];
                    $modKasuspenyakitruangan = KasuspenyakitruanganM::model()->findAllByAttributes(array('ruangan_id'=>$ruangan_id));
                    foreach ($modKasuspenyakitruangan as $value)
                    {
                        $hapus = Yii::app()->createUrl('persalinan/kasuspenyakitruanganM/Delete',array('ruangan_id'=>"$value->ruangan_id",'jeniskasuspenyakit_id'=>"$value->jeniskasuspenyakit_id"));
                        $tr .= '<tr>';
                        $tr .= '<td>' .$modInstalasi->instalasi_nama. '</td>';
                        $tr .= '<td>' .$value->ruangan->ruangan_nama. '</td>';
                        $tr .= '<td>' .$value->kasuspenyakit->jeniskasuspenyakit_nama .'</td>';
                        $tr .= '<td>' .$value->kasuspenyakit->jeniskasuspenyakit_namalainnya .'</td>';
                    $tr .= '<td>'.CHtml::link("<i class='icon-trash'></i>",$hapus).'</td>';
                        $tr .= '</tr>';
                    }
                    echo $tr;
                    
                if (count($modDetails) > 0){
                    foreach ($modDetails as $i=>$row){
                        $modruangan = RuanganM::model()->findByPK($row->ruangan_id);
                        $modJeniskasuspenyakit = JeniskasuspenyakitM::model()->findByPK($row->jeniskasuspenyakit_id);
                        $tr = "<tr>";
                        $tr .= "<td>"
                                    .$modruangan->instalasi->instalasi_nama
                                    .CHtml::hiddenField('ruangan_id['.$i.']',$row->ruangan_id,array('readonly'=>true))
                                    .CHtml::hiddenField('jeniskasuspenyakit_id['.$i.']',$row->jeniskasuspenyakit_id,array('readonly'=>true))
                                    ."</td>";
                        $tr .= "<td>".$modruangan->ruangan_nama."</td>";
                        $tr .= "<td>".$modJeniskasuspenyakit->jeniskasuspenyakit_nama."</td>";
                        $tr .= "<td>".$modJeniskasuspenyakit->jeniskasuspenyakit_namalainnya."</td>";
                        $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'remove(this);'))."</td>";
                        $tr .= "</tr>";
                        echo $tr;
                    }
                }
                ?>
            </tbody>
        </table>


	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'id'=>'btn_simpan','onKeypress'=>'return formSubmit(this,event)','onClick'=>'return formSubmit(this,event)')); ?>
                        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                                    Yii::app()->createUrl($this->module->id.'/'.kasuspenyakitruanganM.'/admin'), 
                                    array('class'=>'btn btn-danger',
                                            'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                        <?php
                            $content = $this->renderPartial('persalinan.views.tips.tipsaddedit3',array(),true);
                            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
                        ?>
        </div>

<?php $this->endWidget(); ?>
<!-- ============================== Widget Dialog Jenis Kasus Penyakit =============================== -->
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
        'id'=>'dialogkasuspenyakit',
        'options'=>array(
            'title'=>'Pencarian Kasus Penyakit',
            'autoOpen'=>false,
            'modal'=>true,
            'width'=>900,
            'height'=>600,
            'resizable'=>false,
        ),
    ));
    
    $modJeniskasuspenyakit = new JeniskasuspenyakitM;
    $modJeniskasuspenyakit->unsetAttributes();
    if (isset($_GET['JeniskasuspenyakitM'])) {
        $modJeniskasuspenyakit->attributes = $_GET['JeniskasuspenyakitM'];
    }
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'jeniskasuspenyakit-grid',
        'dataProvider'=>$modJeniskasuspenyakit->search(),
        'filter'=>$modJeniskasuspenyakit,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-bordered table-striped table-condensed',
        'columns'=>array(
            array(
                'header'=>'Pilih',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                                array(
                                        "class"=>"btn-small",
                                        "id" => "selectKasuspenyakit",
                                        "onClick" => "\$(\"#PSKasuspenyakitruanganM_jeniskasuspenyakit_id\").val($data->jeniskasuspenyakit_id);
                                                              \$(\"#kasuspenyakit\").val(\"$data->jeniskasuspenyakit_nama\");
                                                              \$(\"#dialogkasuspenyakit\").dialog(\"close\");
                                                              submitKasuspenyakitruangan();"
                                ))',
            ),
            array(
                'header'=>'Nama Kasus',
                'value'=>'$data->jeniskasuspenyakit_nama',
            ),
            array(
                'header'=>'Nama Lainnya',
                'value'=>'$data->jeniskasuspenyakit_namalainnya',
            ),
            array(
                'header'=>'Urutan',
                'value'=>'$data->jeniskasuspenyakit_urutan',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
$this->endWidget();
?>
<!-- ======================== endWidget dialogkasuspenyakit ===================================== -->
<?php
$urlGetKasuspenyakitruangan = Yii::app()->createUrl('actionAjax/Jeniskasuspenyakitruangan');
?>
        
<?php
$jscript = <<< JS
function submitKasuspenyakitruangan()
{
    instalasi_id = $('#instalasi_id').val();
    ruanganid = $('#ruanganid').val();
    jeniskasuspenyakit_id = $('#PSKasuspenyakitruanganM_jeniskasuspenyakit_id').val();

    if(jeniskasuspenyakit_id==''){
        alert('Silahkan Pilih Jenis Kasus Penyakit Terlebih Dahulu');
    }else{
        $.post("${urlGetKasuspenyakitruangan}", {instalasi_id:instalasi_id, ruanganid: ruanganid, jeniskasuspenyakit_id:jeniskasuspenyakit_id,},
        function(data){
            $('#tabelKasuspenyakitruangan tbody').append(data.tr);
        }, "json");
    }
}
JS;

Yii::app()->clientScript->registerScript('Jeniskasuspenyakitruangan',$jscript, CClientScript::POS_HEAD);
?>

<script type="text/javascript">
  function hapusBaris(obj)
    {
      $(obj).parent().parent('tr').detach();
    }
</script>