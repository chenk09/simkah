<?php

class DaftarPasienController extends SBaseController
{
    
        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','GetRiwayatPasien','DetailTindakan','DetailTerapi','DetailPemakaianBahan'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{           
        $format = new CustomFormat();
                $this->pageTitle = Yii::app()->name." - Daftar Pasien";
                $model = new PSInfokunjunganRDV;
                $model->tglAwal = date("d M Y").' 00:00:00';
                $model->tglAkhir = date('d M Y h:i:s');
                if(isset ($_REQUEST['PSInfokunjunganRDV'])){
                    $model->attributes=$_REQUEST['PSInfokunjunganRDV'];
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSInfokunjunganRDV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSInfokunjunganRDV']['tglAkhir']);
                    $model->ceklis = $_REQUEST['PSInfokunjunganRDV']['ceklis'];
                }
               
                $this->render('index',array('model'=>$model));
	}
        
        public function actionAjaxJumlahPersalinan(){
            if (Yii::app()->request->isAjaxRequest){
                $idPendaftaran = $_POST['idPendaftaran'];
            }
            
        }

        public function actionGetRiwayatPasien($id){
            $this->layout='//layouts/frameDialog';
            $criteria = new CDbCriteria(array(
                    //'condition' => 't.pasien_id = '.$id.' and t.ruangan_id ='.Yii::app()->user->getState('ruangan_id'),
                    'condition' => 't.pasien_id = '.$id,
                    'order'=>'tgl_pendaftaran DESC',
                ));

            $pages = new CPagination(PSPendaftaranT::model()->count($criteria));
            $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            
            $modKunjungan = PSPendaftaranT::model()->with('hasilpemeriksaanlab','anamnesa','pemeriksaanfisik','pasienmasukpenunjang','diagnosa')->
                    findAll($criteria);
            
           
            $this->render('/_periksaDataPasien/_riwayatPasien', array(
                    'pages'=>$pages,
                    'modKunjungan'=>$modKunjungan,
            ));
        }
	
        
        
        public function actionDetailTindakan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = PSPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modTindakan = PSTindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modTindakanSearch = new PSTindakanPelayananT('search');
            $modPasien = PSPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('/_periksaDataPasien/_tindakan', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTindakan'=>$modTindakan,
                        'modTindakanSearch'=>$modTindakanSearch,
                        'modPasien'=>$modPasien));
        }
        
        public function actionDetailTerapi($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = PSPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modTerapi = PSPenjualanresepT::model()->with('reseptur')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modDetailTerapi = new PSPenjualanresepT();
            $modPasien = PSPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('/_periksaDataPasien/_terapi', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modTerapi'=>$modTerapi,
                        'modDetailTerapi'=>$modDetailTerapi,
                        'modPasien'=>$modPasien));
        }
        
        public function actionDetailPemakaianBahan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = PSPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
            $modBahan = PSObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$id));
            $format = new CustomFormat;
            $modPemakaianBahan = new PSObatalkesPasienT;
            $modPasien = PSPasienM::model()->findByPK($modPendaftaran->pasien_id);
            $this->render('/_periksaDataPasien/_pemakaianBahan', 
                    array('modPendaftaran'=>$modPendaftaran, 
                        'modBahan'=>$modBahan,
                        'modPemakaianBahan'=>$modPemakaianBahan,
                        'modPasien'=>$modPasien));
        }
}