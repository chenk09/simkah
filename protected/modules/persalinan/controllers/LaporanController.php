
<?php

class LaporanController extends SBaseController { 
    
    public function actionLaporanSensusHarian() {
        $model = new PSLaporansensuharianpersalinanV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $kunjungan = Kunjungan::items();
        $model->kunjungan = $kunjungan;
        if (isset($_GET['PSLaporansensuharianpersalinanV'])) {
            $model->attributes = $_GET['PSLaporansensuharianpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporansensuharianpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporansensuharianpersalinanV']['tglAkhir']);
        }

        $this->render('sensus/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new PSLaporansensuharianpersalinanV('search');
        $judulLaporan = 'Laporan Sensus Harian Persalinan';
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporansensuharianpersalinanV'])) {
            $model->attributes = $_REQUEST['PSLaporansensuharianpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporansensuharianpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporansensuharianpersalinanV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporansensuharianpersalinanV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['PSLaporansensuharianpersalinanV'])) {
            $model->attributes = $_GET['PSLaporansensuharianpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporansensuharianpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporansensuharianpersalinanV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKunjungan() {
        $model = new PSLaporankunjunganpersalinanV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->kunjungan = Kunjungan::items();
        if (isset($_GET['PSLaporankunjunganpersalinanV'])) {
            $model->attributes = $_GET['PSLaporankunjunganpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporankunjunganpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporankunjunganpersalinanV']['tglAkhir']);
        }

        $this->render('kunjungan/index', array(
            'model' => $model,
        ));

    }

    public function actionPrintLaporanKunjungan() {
        $model = new PSLaporankunjunganpersalinanV('search');
        $judulLaporan = 'Laporan Kunjungan Persalinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Persalinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporankunjunganpersalinanV'])) {
            $model->attributes = $_REQUEST['PSLaporankunjunganpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporankunjunganpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporankunjunganpersalinanV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_printKunjungan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

  public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporankunjunganpersalinanV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Info Kunjungan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporankunjunganpersalinanV'])) {
            $model->attributes = $_GET['PSLaporankunjunganpersalinanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporankunjunganpersalinanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporankunjunganpersalinanV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new PSLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['PSLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new PSLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Persalinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien Persalinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['PSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Persalinan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PSLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new PSLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenisObat =CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['PSLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['PSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/index',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new PSLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Persalinan';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Persalinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['PSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Persalinan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['PSLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new PSLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $tindakan = array('sudah', 'belum');
        $model->tindakansudahbayar_id = $tindakan;
        if (isset($_GET['PSLaporanjasainstalasi'])) {
            $model->attributes = $_GET['PSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new PSLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Persalinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['PSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporanjasainstalasi'])) {
            $model->attributes = $_GET['PSLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new PSLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['PSLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['PSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new PSLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Persalinan';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Persalinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['PSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Persalinan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['PSLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new PSLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['PSLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['PSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanpendapatanruanganV']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new PSLaporanpendapatanruanganV('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Persalinan';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporanpendapatanruanganV'])) {
            $model->attributes = $_REQUEST['PSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporanpendapatanruanganV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['PSLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporanpendapatanruanganV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new PSBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PSBukuregisterpasienV'])) {
            $model->attributes = $_GET['PSBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSBukuregisterpasienV']['tglAkhir']);
        }

        $this->render('bukuRegister/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new PSBukuregisterpasienV('search');
        $judulLaporan = 'Laporan Buku Register Pasien Persalinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Persalinan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSBukuregisterpasienV'])) {
            $model->attributes = $_REQUEST['PSBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSBukuregisterpasienV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSBukuregisterpasienV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Persalinan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSBukuregisterpasienV'])) {
            $model->attributes = $_GET['PSBukuregisterpasienV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSBukuregisterpasienV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSBukuregisterpasienV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new PSLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'), 'asalrujukan_id', 'asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $ruanganasal = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_id');
        $model->ruanganasal_id = $ruanganasal;
        if (isset($_GET['PSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['PSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('caraMasuk/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new PSLaporancaramasukpenunjangV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Persalinan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_REQUEST['PSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new PSLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PSLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['PSLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PSLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PSLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}