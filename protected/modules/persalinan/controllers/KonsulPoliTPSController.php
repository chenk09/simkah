<?php
Yii::import('rawatJalan.controllers.KonsulPoliController');
Yii::import('rawatJalan.models.*');
class KonsulPoliTPSController extends KonsulPoliController
{
        
}
//class KonsulPoliController extends SBaseController
//{
//	public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = PSPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = PSPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
//            
//            $modKonsul = new PSKonsulPoliT;
//            $modKonsul->pasien_id = $modPendaftaran->pasien_id;
//            $modKonsul->pendaftaran_id = $idPendaftaran;
//            $modKonsul->pegawai_id = $modPendaftaran->pegawai_id;
//            $modKonsul->statusperiksa = Params::statusPeriksa(1);
//            $modKonsul->asalpoliklinikkonsul_id = Yii::app()->user->getState('ruangan_id');
//            
//            if(isset($_POST['PSKonsulPoliT'])) {
//                $modKonsul->attributes = $_POST['PSKonsulPoliT'];
//                if($modKonsul->validate()){
//                    $modKonsul->save();
//                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
//                }
//            }
//            
//            $modRiwayatKonsul = PSKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//		
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modKonsul'=>$modKonsul,
//                                        'karcisTindakan'=>$karcisTindakan,
//                                        'modRiwayatKonsul'=>$modRiwayatKonsul));
//	}
//        
//        public function actionAjaxDetailKonsul()
//        {
//            if(Yii::app()->request->isAjaxRequest) {
//            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
//            $modKonsulPoli = PSKonsulPoliT::model()->findByPk($idKonsulAntarPoli);
//            $data['result'] = $this->renderPartial('_viewKonsulPoli', array('modKonsul'=>$modKonsulPoli), true);
//
//            echo json_encode($data);
//             Yii::app()->end();
//            }
//        }
//        
//        public function actionAjaxBatalKonsul()
//        {
//            if(Yii::app()->request->isAjaxRequest) {
//            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
//            $idPendaftaran = $_POST['idPendaftaran'];
//            
//            PSKonsulPoliT::model()->deleteByPk($idKonsulAntarPoli);
//            $modRiwayatKonsul = PSKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//            
//            $data['result'] = $this->renderPartial('_listKonsulPoli', array('modRiwayatKonsul'=>$modRiwayatKonsul), true);
//
//            echo json_encode($data);
//             Yii::app()->end();
//            }
//        }
//
//	// Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}