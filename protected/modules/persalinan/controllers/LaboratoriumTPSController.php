<?php
Yii::import('rawatJalan.controllers.LaboratoriumController');
Yii::import('rawatJalan.models.*');
class LaboratoriumTPSController extends LaboratoriumController
{
        
}
//class LaboratoriumController extends SBaseController
//{
//    protected $statusSaveKirimkeUnitLain = false;
//    protected $statusSavePermintaanPenunjang = false;
//
//    public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = PSPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = PSPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            $modKirimKeUnitLain = new PSPasienKirimKeUnitLainT;
//            $modKirimKeUnitLain->tgl_kirimpasien = date('Y-m-d H:i:s');
//            $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
//            $modJenisPeriksaLab = PSJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
//            $modPeriksaLab = PSPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_urutan'));
//                        
//            if(isset($_POST['PSPasienKirimKeUnitLainT'])) {
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    $modKirimKeUnitLain = $this->savePasienKirimKeUnitLain($modPendaftaran);
//                    if(isset($_POST['permintaanPenunjang'])){
//                        $this->savePermintaanPenunjang($_POST['permintaanPenunjang'],$modKirimKeUnitLain);
//                    } else {
//                        $this->statusSavePermintaanPenunjang = true;
//                    }
//                    
//                    if($this->statusSaveKirimkeUnitLain && $this->statusSavePermintaanPenunjang){
//                        $transaction->commit();
//                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
//                    } else {
//                        $transaction->rollback();
//                        Yii::app()->user->setFlash('error',"Data tidak valid ");
//                    }
//                } catch (Exception $exc) {
//                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
//                }
//            }
//            
//            $modRiwayatKirimKeUnitLain = PSPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
//                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
//                                                                                                'pasienmasukpenunjang_id IS NULL');
//		
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
//                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab,
//                                        'modPeriksaLab'=>$modPeriksaLab,
//                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
//                                        ));
//	}
//
//        protected function savePasienKirimKeUnitLain($modPendaftaran)
//        {
//            $modKirimKeUnitLain = new PSPasienKirimKeUnitLainT;
//            $modKirimKeUnitLain->attributes = $_POST['PSPasienKirimKeUnitLainT'];
//            $modKirimKeUnitLain->pasien_id = $modPendaftaran->pasien_id;
//            $modKirimKeUnitLain->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//            //$modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
//            $modKirimKeUnitLain->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//            $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_LAB;
//            $modKirimKeUnitLain->ruangan_id = Params::RUANGAN_ID_LAB;
//            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
//            if($modKirimKeUnitLain->validate()){
//                $modKirimKeUnitLain->save();
//                $this->statusSaveKirimkeUnitLain = true;
//            }
//            
//            return $modKirimKeUnitLain;
//        }
//        
//        protected function savePermintaanPenunjang($permintaan,$modKirimKeUnitLain)
//        {
//            foreach ($permintaan['inputpemeriksaanlab'] as $i => $value) {
//                $modPermintaan = new PSPermintaanPenunjangT;
//                $modPermintaan->daftartindakan_id = '';     //$permintaan['idDaftarTindakan'][$i];
//                $modPermintaan->pemeriksaanlab_id = $permintaan['inputpemeriksaanlab'][$i];
//                $modPermintaan->pemeriksaanrad_id = '';
//                $modPermintaan->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
//                $modPermintaan->noperminatanpenujang = Generator::noPermintaanPenunjang('PL');
//                $modPermintaan->qtypermintaan = $permintaan['inputqty'][$i];
//                $modPermintaan->tglpermintaankepenunjang = $modKirimKeUnitLain->tgl_kirimpasien; //date('Y-m-d H:i:s');
//                if($modPermintaan->validate()){
//                    $modPermintaan->save();
//                    $this->statusSavePermintaanPenunjang = true;
//                }
//            }
//        }
//        
//        public function actionAjaxBatalKirim()
//        {
//            if(Yii::app()->request->isAjaxRequest) {
//            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeUnitLain'];
//            $idPendaftaran = $_POST['idPendaftaran'];
//            
//            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
//            PasienkirimkeunitlainT::model()->deleteByPk($idPasienKirimKeUnitLain);
//            $modRiwayatKirimKeUnitLain = PSPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
//                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
//                                                                                                'pasienmasukpenunjang_id IS NULL');
//            
//            $data['result'] = $this->renderPartial('_listKirimKeUnitLain', array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain), true);
//
//            echo json_encode($data);
//             Yii::app()->end();
//            }
//        }
//
//
//        // Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}