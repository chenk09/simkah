<?php
Yii::import('rawatJalan.controllers.DiagnosaController');
Yii::import('rawatJalan.models.*');
class DiagnosaTPSController extends DiagnosaController
{
        
}
//class DiagnosaController extends SBaseController
//{
//    protected $successSave;
//    
//    public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran = PSPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
//            $modPasien = PSPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            
//            $modDiagnosa = new PSDiagnosaM('searchDiagnosis');
//            $modDiagnosa->unsetAttributes();  // clear any default values
//            if(isset($_GET['PSDiagnosaM']))
//                $modDiagnosa->attributes=$_GET['PSDiagnosaM'];
//            
//            $modMorbiditas[0] = new PSPasienMorbiditasT;
//            $modMorbiditas[0]->pendaftaran_id = $idPendaftaran;
//            $modMorbiditas[0]->pasien_id = $modPendaftaran->pasien_id;
//            $modMorbiditas[0]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//            $modMorbiditas[0]->kelompokumur_id = $modPasien->kelompokumur_id;
//            $modMorbiditas[0]->golonganumur_id = $modPendaftaran->golonganumur_id;
//            $modMorbiditas[0]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//            $modMorbiditas[0]->pegawai_id = $modPendaftaran->pegawai_id;
//            
//            $modKasuspenyakitDiagnosa = new PSKasusPenyakitDiagnosaM('search');
//            $modKasuspenyakitDiagnosa->unsetAttributes();  // clear any default values
//            $modKasuspenyakitDiagnosa->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//            if(isset($_GET['PSKasusPenyakitDiagnosaM'])){
//                $modKasuspenyakitDiagnosa->attributes=$_GET['PSKasusPenyakitDiagnosaM'];
//                $modKasuspenyakitDiagnosa->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//            }
//            
//            $modDiagnosaicdixM = new PSDiagnosaicdixM('search');
//            $modDiagnosaicdixM->unsetAttributes();  // clear any default values
//            if(isset($_GET['PSDiagnosaicdixM']))
//                $modDiagnosaicdixM->attributes=$_GET['PSDiagnosaicdixM'];
//            $modSebabDiagnosa = PSSebabDiagnosaM::model()->findAll();
//            
//            $newInput = false;
//            if(isset($_POST['Morbiditas'])){
//                //echo "<pre>".print_r($_POST['Morbiditas'],1)."</pre>";exit;
//                $newInput = true;
//                $modMorbiditas = $this->saveDiagnosa($_POST['Morbiditas'], $modPasien, $modPendaftaran);
//                $successSave = $this->successSave;
//            }
//            
//            $listMorbiditas = PSPasienMorbiditasT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//		
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
//                                        'modPasien'=>$modPasien,
//                                        'modDiagnosa'=>$modDiagnosa,
//                                        'modDiagnosaicdixM'=>$modDiagnosaicdixM,
//                                        'modKasuspenyakitDiagnosa'=>$modKasuspenyakitDiagnosa,
//                                        'modSebabDiagnosa'=>$modSebabDiagnosa,
//                                        'modMorbiditas'=>$modMorbiditas,
//                                        'listMorbiditas'=>$listMorbiditas,
//                                        'successSave'=>$successSave,
//                                        'newInput'=>$newInput));
//	}
//        
//        protected function saveDiagnosa($diagnosas,$modPasien,$modPendaftaran)
//        {
//            $valid = true;
//            $golUmur = $this->cekGolonganUmur($modPendaftaran->golonganumur_id);
//            foreach ($diagnosas as $i => $diagnosa) {
//                $morbiditas[$i] = new PSPasienMorbiditasT;
//                $morbiditas[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//                $morbiditas[$i]->pasien_id = $modPendaftaran->pasien_id;
//                $morbiditas[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                $morbiditas[$i]->kelompokumur_id = $modPasien->kelompokumur_id;
//                $morbiditas[$i]->golonganumur_id = $modPendaftaran->golonganumur_id;
//                $morbiditas[$i]->$golUmur = 1;
//                $morbiditas[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//                $morbiditas[$i]->pegawai_id = $modPendaftaran->pegawai_id;
//                $morbiditas[$i]->diagnosa_id = $diagnosa['diagnosa'];
//                $morbiditas[$i]->kelompokdiagnosa_id = $diagnosa['kelompokDiagnosa'];
//                $morbiditas[$i]->diagnosaicdix_id = $diagnosa['diagnosaTindakan'];
//                $morbiditas[$i]->sebabdiagnosa_id = $diagnosa['sebabDiagnosa'];
//                $morbiditas[$i]->infeksinosokomial = '0';//$diagnosa['infeksiNosokomial'];
//                $morbiditas[$i]->tglmorbiditas = $_POST['PSPasienMorbiditasT'][0]['tglmorbiditas'];
//                //$morbiditas[$i]->kasusdiagnosa = $_POST['PSPasienMorbiditasT'][0]['kasusdiagnosa'];
//                $morbiditas[$i]->kasusdiagnosa = $this->getKasusDiagnosa($modPendaftaran->pasien_id, $diagnosa['diagnosa']);
//                $morbiditas[$i]->pegawai_id = $_POST['PSPasienMorbiditasT'][0]['pegawai_id'];
//                $valid = $morbiditas[$i]->validate() && $valid;
//            }
//            if($valid){
//                foreach ($morbiditas as $j => $morbiditasPasien) {
//                    $morbiditasPasien->save();
//                }
//                //echo 'VALID';
//                $this->successSave = true;
//                Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
//                return $morbiditas;
//            } else {
//                //echo 'TIDAK VALID';
//                Yii::app()->user->setFlash('error',"Data tidak valid ");
//                return $morbiditas;
//            }
//        }
//        
//        protected function getKasusDiagnosa($idPasien,$idDiagnosa)
//        {
//            $modMorbiditas = PasienmorbiditasT::model()->findByAttributes(array('pasien_id'=>$idPasien,'diagnosa_id'=>$idDiagnosa));
//            if(!empty($modMorbiditas))
//                return 'KASUS LAMA';
//            else 
//                return 'KASUS BARU';
//        }
//        
//        private function cekGolonganUmur($idGolonganUmur)
//        {
//            switch ($idGolonganUmur) {
//                case 1:return 'umur_0_28hr';
//                case 2:return 'umur_28hr_1thn';
//                case 3:return 'umur_1_4thn';
//                case 4:return 'umur_5_14thn';
//                case 5:return 'umur_15_24thn';
//                case 6:return 'umur_25_44thn';
//                case 7:return 'umur_45_64thn';
//                case 8:return 'umur_65';
//
//                default:
//                    break;
//            }
//            
//        }
//
//        // Uncomment the following methods and override them if needed
//	/*
//	public function filters()
//	{
//		// return the filter configuration for this controller, e.g.:
//		return array(
//			'inlineFilterName',
//			array(
//				'class'=>'path.to.FilterClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//
//	public function actions()
//	{
//		// return external action classes, e.g.:
//		return array(
//			'action1'=>'path.to.ActionClass',
//			'action2'=>array(
//				'class'=>'path.to.AnotherActionClass',
//				'propertyName'=>'propertyValue',
//			),
//		);
//	}
//	*/
//}