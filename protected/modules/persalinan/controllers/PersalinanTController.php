<?php

class PersalinanTController extends SBaseController {

//	public function actionIndex()
//	{
//		$this->render('index');
//	}
// Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }

      public function actions()
      {
      // return external action classes, e.g.:
      return array(
      'action1'=>'path.to.ActionClass',
      'action2'=>array(
      'class'=>'path.to.AnotherActionClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

    public function actionIndex($id) {

        $modPendaftaran=PSPendaftaranT::model()->findByPk($id);
        $modPasien = PSPasienM::model()->findByPk($modPendaftaran->pasien_id);
        $modPersalinan = PSPersalinanT::model()->with(array('pendaftaran','pegawai'))->findAllByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasien_id'=>$modPasien->pasien_id));
        $format = new CustomFormat;

        if (count($modPersalinan) > 0) {
            $model = PSPersalinanT::model()->with(array('pendaftaran','pegawai'))->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasien_id'=>$modPasien->pasien_id));
        }else{
            $model = new PSPersalinanT;
            $model->tglmulaipersalinan = date('d M Y H:i:s');
            $model->tglabortus = date('d M Y H:i:s');
        }


        if (isset($_POST['PSPersalinanT'])) {
            $model->attributes = $_POST['PSPersalinanT'];
            $model->pasien_id = $modPasien->pasien_id;
            $model->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $model->tglselesaipersalinan = $format->formatDateTimeMediumForDB($_POST['PSPersalinanT']['tglselesaipersalinan']);

            if ($model->validate()) {
                if ($model->save()){
                    Yii::app()->user->setFlash('success',"Data Berhasil disimpan ");
                    $this->redirect(Yii::app()->createUrl($this->module->id.'/'.daftarPasien.'/index'));
                }
            } else {
                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
            }
            
        }
        $this->render('index', array('model' => $model, 'modPendaftaran'=>$modPendaftaran, 'modPasien'=>$modPasien, 'modPersalinan'=>$modPersalinan));
    }
    
}