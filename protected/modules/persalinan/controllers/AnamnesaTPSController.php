<?php
Yii::import('rawatJalan.controllers.AnamnesaController');
Yii::import('rawatJalan.models.*');
class AnamnesaTPSController extends AnamnesaController
{
        
}
//class AnamnesaController extends SBaseController
//{
//	/**
//	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
//	 * using two-column layout. See 'protected/views/layouts/column2.php'.
//	 */
//	public $layout='//layouts/column1';
//        public $defaultAction = 'index';
//
//	/**
//	 * @return array action filters
//	 */
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
//
//	
//	public function actionIndex($idPendaftaran)
//	{
//            $modPendaftaran=PSPendaftaranT::model()->findByPk($idPendaftaran);
//            
//            $modPasien = PSPasienM::model()->findByPk($modPendaftaran->pasien_id);
//            
//            $dataPendaftaran = PSPendaftaranT::model()->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
//            //print_r($lastPendaftaran);
//            //echo $modPasien->pasien_id;exit();
//            $i = 1;
//            if (count($dataPendaftaran) > 1){
//                foreach ($dataPendaftaran as $row){
//                    if ($i == 2){
//                        $lastPendaftaran = $row->pendaftaran_id;
//                    }
//                    $i++;
//                }
//            }else{
//                $lastPendaftaran = $idPendaftaran;
//            }
//
//            
//            $cekAnamnesa=PSAnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
//            $modDiagnosa = new PSDiagnosaM;
//           
//            if(COUNT($cekAnamnesa)>0) {  //Jika Pasien Sudah Melakukan Anamnesa Sebelumnya
//                $modAnamnesa=$cekAnamnesa;
//                //$modAnamnesa->riwayatimunisasi = $modPendaftaran->statuspasien;
//            } else {  
//                ////Jika Pasien Belum Pernah melakukan Anamnesa
//                $modAnamnesa=new PSAnamnesaT;
//                $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
//                $modAnamnesa->pendaftaran_id=$modPendaftaran->pendaftaran_id;
//                $modAnamnesa->pasien_id=$modPendaftaran->pasien_id;
//                $modAnamnesa->tglanamnesis=date('Y-m-d H:i:s');
//                //$isPasien = PSPendaftaranT::model()->findByPk($idPendaftaran)->statuspasien;
////                $sql = "SELECT c(diagnosa_id) FROM pasienimunisasi_t WHERE pendaftaran_id = $idPendaftaran";
////                $stoks = Yii::app()->db->createCommand($sql)->queryAll();
//                
//            }
//            
//            if ($modPendaftaran->statuspasien == "PENGUNJUNG LAMA"){
//                $modDiagnosaTerdahulu = PSPasienMorbiditasT::model()->with('diagnosa')->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id, 'pendaftaran_id'=>$lastPendaftaran));
//                
//                $hasilImunisasi = array();
//                $hasilDiagnosaDahulu = array();
//                foreach($modDiagnosaTerdahulu as $row){
//                    if ($row->diagnosa->diagnosa_imunisasi == true)
//                        $hasilImunisasi[] = $row->diagnosa->diagnosa_nama;
//                    else
//                        $hasilDiagnosaDahulu[] = $row->diagnosa->diagnosa_nama;
//                }
//                if (empty($modAnamnesa->riwayatimunisais)){
//                    $modAnamnesa->riwayatimunisasi = implode(', ',$hasilImunisasi);
//                }
//                if (empty($modAnamnesa->riwayatpenyakitterdahulu)){
//                    $modAnamnesa->riwayatpenyakitterdahulu = implode(', ',$hasilDiagnosaDahulu);
//                }
//            }
//            
//            //echo $modAnamnesa->riwayatpenyakitterdahulu;exit();
//            if($_POST['PSAnamnesaT']) {
//                $transaction = Yii::app()->db->beginTransaction();
//                try {
//                    $modAnamnesa->attributes=$_POST['PSAnamnesaT'];
//                    $modAnamnesa->keluhanutama = implode(', ', $_POST['PSAnamnesaT']['keluhanutama']);
//                    $modAnamnesa->keluhantambahan = implode(', ', $_POST['PSAnamnesaT']['keluhantambahan']);
//                    $modAnamnesa->save();
//                    $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
//                    $transaction->commit();
//                    Yii::app()->user->setFlash('success',"Data Anamnesa berhasil disimpan");
//                    $this->redirect($_POST['url']);       
//                } catch (Exception $exc) {
//                    $transaction->rollback();
//                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
//                }
//            }
//                
//            $modAnamnesa->tglanamnesis = Yii::app()->dateFormatter->formatDateTime(
//                                        CDateTimeParser::parse($modAnamnesa->tglanamnesis, 'yyyy-MM-dd hh:mm:ss'));    
//            
//            $modDataDiagnosa = new PSDiagnosaM('search');
//            $modDataDiagnosa->unsetAttributes();
//            if(isset($_GET['PSDiagnosaM']))
//                $modDataDiagnosa->attributes = $_GET['PSDiagnosaM'];
//            
//            $this->render('index',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,
//                        'modAnamnesa'=>$modAnamnesa, 'modDiagnosa'=>$modDiagnosa, 'modDataDiagnosa'=>$modDataDiagnosa,
//		));
//	}
//        
//}
