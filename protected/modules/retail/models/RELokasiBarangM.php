<?php

/**
 * This is the model class for table "lokasigudang_m".
 *
 * The followings are the available columns in table 'lokasigudang_m':
 * @property integer $lokasigudang_id
 * @property string $lokasigudang_nama
 * @property string $lokasigudang_namalain
 * @property boolean $lokasigudang_aktif
 * @property boolean $lokasigudang_farmasi
 */
class RELokasiBarangM extends LokasigudangM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LokasigudangM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('LOWER(lokasigudang_nama)',strtolower($this->lokasigudang_nama),true);
		$criteria->compare('LOWER(lokasigudang_namalain)',strtolower($this->lokasigudang_namalain),true);
		$criteria->compare('lokasigudang_aktif',$this->lokasigudang_aktif);
                $criteria->compare('lokasigudang_farmasi',$this->lokasigudang_farmasi);
                $criteria->addCondition('lokasigudang_farmasi is false');
                $criteria->order=lokasigudang_nama;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
}