<?php
class REProdukposV extends ProdukposV {
        
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public $hargabelibruto, $stok, $hargajualminppn, $marginrp, $hargajualplusppn;
    public $data, $tick, $jumlah;
    
    public function getJenisObatAlkesItems() {
        return JenisobatalkesM::model()->findAll('jenisobatalkes_aktif=TRUE AND jenisobatalkes_farmasi=FALSE ORDER BY jenisobatalkes_nama');
    }

    public function getSubJenisItems() {
        if (!empty($this->category_id)) {
            return SubjenisM::model()->findAll('jenisobatalkes_id=' . $this->category_id . ' order BY subjenis_nama');
        } else {
            return array();
        }
    }

    public function getSatuanBesarItems() {
        return SatuanbesarM::model()->findAll('satuanbesar_aktif=TRUE ORDER BY satuanbesar_nama');
    }

    public function getSatuanKecilItems() {
        return SatuankecilM::model()->findAll('satuankecil_aktif=TRUE ORDER BY satuankecil_nama');
    }
    
    public function getCategoryItems() {
//            $data = Yii::app()->db->createCommand("SELECT category_id, category_name FROM laporanpenjualanprodukpos_v GROUP BY category_id, category_name")->queryAll();
        $data = Yii::app()->db->createCommand("SELECT jenisobatalkes_id AS category_id, jenisobatalkes_nama AS category_name FROM jenisobatalkes_m WHERE jenisobatalkes_farmasi=FALSE")->queryAll();
        return $data;
    }
    
    public function getSubcategoryItems() {
        $data = Yii::app()->db->createCommand("SELECT subjenis_id AS subcategory_id, subjenis_nama AS subcategory_name FROM subjenis_m WHERE subjenis_farmasi=FALSE")->queryAll();
        return $data;
    }
    
    public function searchData()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('brg_id',$this->brg_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('LOWER(category_name)',strtolower($this->category_name),true);
		$criteria->compare('subcategory_id',$this->subcategory_id);
		$criteria->compare('LOWER(subcategory_code)',strtolower($this->subcategory_code),true);
		$criteria->compare('LOWER(subcategory_name)',strtolower($this->subcategory_name),true);
		$criteria->compare('LOWER(stock_code)',strtolower($this->stock_code),true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar_nama)',strtolower($this->satuanbesar_nama),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
		$criteria->compare('LOWER(stock_name)',strtolower($this->stock_name),true);
		$criteria->compare('jmldalamkemasan',$this->jmldalamkemasan);
		$criteria->compare('jmlisi',$this->jmlisi);
		$criteria->compare('LOWER(satuanisi)',strtolower($this->satuanisi),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(barang_barcode)',strtolower($this->barang_barcode),true);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
		$criteria->compare('mintransaksi',$this->mintransaksi);
		$criteria->compare('LOWER(price_name)',strtolower($this->price_name),true);
		$criteria->compare('margin',$this->margin);
		$criteria->compare('gp_persen',$this->gp_persen);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
                $criteria->order='brg_id asc';
                $criteria->limit = -1;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function criteriaLaporan()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                                $criteria->join = 'RIGHT JOIN stokobatalkes_t ON t.brg_id=stokobatalkes_t.obatalkes_id';
                                $criteria->select = 'activedate, stock_code, stock_name, margin, movingavarage,
                                                                ppn_persen, barang_barcode, hargajual, harganetto,
                                                                (movingavarage*(1 + ppn_persen/100)) AS hargabelibruto,
                                                                (harganetto*ppn_persen) AS hargajualplusppn,
                                                                ((harganetto*ppn_persen)/(1 + ppn_persen/100)) AS hargajualminppn,
                                                                SUM (stokobatalkes_t.qtystok_current) AS stok';
                                $criteria->group = 't.activedate, t.stock_code, t.stock_name, t.ppn_persen, t.barang_barcode, t.hargajual, t.brg_id, t.harganetto, t.margin, t.movingavarage';
		$criteria->compare('brg_id',$this->brg_id);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('LOWER(category_name)',strtolower($this->category_name),true);
		$criteria->compare('subcategory_id',$this->subcategory_id);
		$criteria->compare('LOWER(subcategory_code)',strtolower($this->subcategory_code),true);
		$criteria->compare('LOWER(subcategory_name)',strtolower($this->subcategory_name),true);
		$criteria->compare('LOWER(stock_code)',strtolower($this->stock_code),true);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(satuanbesar_nama)',strtolower($this->satuanbesar_nama),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
		$criteria->compare('LOWER(stock_name)',strtolower($this->stock_name),true);
		$criteria->compare('jmldalamkemasan',$this->jmldalamkemasan);
		$criteria->compare('jmlisi',$this->jmlisi);
		$criteria->compare('LOWER(satuanisi)',strtolower($this->satuanisi),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(barang_barcode)',strtolower($this->barang_barcode),true);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
		$criteria->compare('mintransaksi',$this->mintransaksi);
		$criteria->compare('LOWER(price_name)',strtolower($this->price_name),true);
		$criteria->compare('margin',$this->margin);
		$criteria->compare('gp_persen',$this->gp_persen);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
                                $criteria->order='brg_id asc';
                
                                return $criteria;
        }
        
    public function searchLaporan()
    {

            return new CActiveDataProvider($this, array(
                    'criteria'=>$this->criteriaLaporan(),
                                            'pagination'=>array(
                                                'pageSize'=>10,
                                            ),
            ));
    }
        
    public function searchLaporanPrint()
    {

            return new CActiveDataProvider($this, array(
                    'criteria'=>$this->criteriaLaporan(),
                                            'pagination'=>false,
            ));
    }
        
   public function searchProdukgrafik()
   {
            $criteria = $this->criteriaLaporan();
            $criteria->select = 'stock_name as data,
                                            SUM (stokobatalkes_t.qtystok_current) AS jumlah';
            
            return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
            ));
   }
        
    public function relations()
    {
            // NOTE: you may need to adjust the relation name and the related
            // class name for the relations automatically generated below.
            return array(
                'obatalkes'=>array(self::BELONGS_TO, 'ObatalkesM','brg_id'),
            );
    }
    
    public function attributeLabels()
    {
            return array(
                    'brg_id' => 'Brg',
                    'category_id' => 'Category',
                    'category_name' => 'Category Name',
                    'subcategory_id' => 'Subcategory',
                    'subcategory_code' => 'Subcategory Code',
                    'subcategory_name' => 'Subcategory Name',
                    'stock_code' => 'Stock Code',
                    'satuanbesar_id' => 'Satuanbesar',
                    'satuanbesar_nama' => 'Satuanbesar Nama',
                    'satuankecil_id' => 'Satuankecil',
                    'satuankecil_nama' => 'Satuankecil Nama',
                    'stock_name' => 'Stock Name',
                    'jmldalamkemasan' => 'Jmldalamkemasan',
                    'jmlisi' => 'Jmlisi',
                    'satuanisi' => 'Satuanisi',
                    'harganetto' => 'Harganetto',
                    'hargajual' => 'Hargajual',
                    'discount' => 'Discount',
                    'minimalstok' => 'Minimalstok',
                    'barang_barcode' => 'Barcode',
                    'ppn_persen' => 'PPN',
                    'activedate' => 'Active Date',
                    'mintransaksi' => 'Mintransaksi',
                    'price_name' => 'Price Name',
                    'margin' => 'Margin',
                    'gp_persen' => 'Gp Persen',
                    'sumberdana_id' => 'Sumberdana',
            );
    }
    
    public function criteriaTransaksi()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->compare('brg_id',$this->brg_id);
            $criteria->compare('category_id',$this->category_id);
            $criteria->compare('LOWER(category_name)',strtolower($this->category_name),true);
            $criteria->compare('subcategory_id',$this->subcategory_id);
            $criteria->compare('LOWER(subcategory_code)',strtolower($this->subcategory_code),true);
            $criteria->compare('LOWER(subcategory_name)',strtolower($this->subcategory_name),true);
            $criteria->compare('LOWER(stock_code)',strtolower($this->stock_code),true);
            $criteria->compare('satuanbesar_id',$this->satuanbesar_id);
            $criteria->compare('LOWER(satuanbesar_nama)',strtolower($this->satuanbesar_nama),true);
            $criteria->compare('satuankecil_id',$this->satuankecil_id);
            $criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
            $criteria->compare('LOWER(stock_name)',strtolower($this->stock_name),true);
            $criteria->compare('jmldalamkemasan',$this->jmldalamkemasan);
            $criteria->compare('jmlisi',$this->jmlisi);
            $criteria->compare('LOWER(satuanisi)',strtolower($this->satuanisi),true);
            $criteria->compare('harganetto',$this->harganetto);
            $criteria->compare('hargajual',$this->hargajual);
            $criteria->compare('discount',$this->discount);
            $criteria->compare('minimalstok',$this->minimalstok);
            $criteria->compare('LOWER(barang_barcode)',strtolower($this->barang_barcode),true);
            $criteria->compare('ppn_persen',$this->ppn_persen);
            $criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
            $criteria->compare('mintransaksi',$this->mintransaksi);
            $criteria->compare('LOWER(price_name)',strtolower($this->price_name),true);
            $criteria->compare('margin',$this->margin);
            $criteria->compare('gp_persen',$this->gp_persen);
            $criteria->compare('sumberdana_id',$this->sumberdana_id);
            $criteria->order='brg_id asc';
            
            return $criteria;
    }
    public function searchTransaksi()
    {
            return new CActiveDataProvider($this, array(
                    'criteria'=>$this->criteriaTransaksi(),
                    'pagination'=>false,
            ));
    }
    
    public function criteriaDataObat()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->compare('LOWER(stock_code)',strtolower($this->stock_code),true);
		$criteria->compare('LOWER(stock_name)',strtolower($this->stock_name),true);
                $criteria->compare('LOWER(price_name)',strtolower($this->price_name),true);
                $criteria->compare('category_id',$this->category_id);
                $criteria->compare('subcategory_id',$this->subcategory_id);
                $criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('obatalkes_aktif',TRUE);
                return $criteria;
		

        }
    
        public function searchDataObat()
            {
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$this->criteriaDataObat(),
                                                    'pagination'=>false,
                    ));
            }

}