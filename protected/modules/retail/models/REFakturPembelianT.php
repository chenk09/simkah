<?php

class REFakturPembelianT extends FakturpembelianT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FakturpembelianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                                $criteria->with = 'supplier';
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		$criteria->addCondition('tglfaktur BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
                                $criteria->addCondition("supplier.supplier_jenis='Farmasi'");

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function getSupplierItems()
        {
            return SupplierM::model()->findAll("supplier_aktif=TRUE ORDER BY supplier_nama");
        }
}