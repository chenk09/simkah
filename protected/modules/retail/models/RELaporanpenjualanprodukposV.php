<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class RELaporanpenjualanprodukposV extends LaporanpenjualanprodukposV {

                public static function model($className = __CLASS__) {
                    parent::model($className);
                }

                public $tglpenjualanAwal;
                public $tglpenjualanAkhir;
                public $tick;
                public $data;
                public $jumlah;
                public $qty_category, $hargajual_category, $hargasatuan_category, $totalharga_category;
                public $qty_subcategory, $hargajual_subcategory, $hargasatuan_subcategory, $totalharga_subcategory, $sumqty, $totalnetvalue ,$cogs, $vat,$value;
                
	public function attributeLabels()
	{
                            return array(
                                    'brg_id' => 'Brg',
                                    'category_id' => 'Category',
                                    'category_name' => 'Category Name',
                                    'subcategory_id' => 'Sub category',
                                    'subcategory_code' => 'Subcategory Code',
                                    'subcategory_name' => 'Subcategory Name',
                                    'stock_code' => 'Stock Code',
                                    'stock_name' => 'Stock Name',
                                    'satuankecil_nama' => 'Satuankecil Nama',
                                    'barang_barcode' => 'Barang Barcode',
                                    'obatalkes_id' => 'Obatalkes',
                                    'qty_oa' => 'Qty Oa',
                                    'hargasatuan_oa' => 'Hargasatuan Oa',
                                    'hargajual_oa' => 'Hargajual Oa',
                                    'tglpenjualan' => 'Tglpenjualan',
                                    'jenispenjualan' => 'Jenispenjualan',
                                    'totalhargajual' => 'Totalhargajual',
                                    'penjualanresep_id' => 'Penjualanresep',
                                    'obatalkespasien_id' => 'Obatalkespasien',
                                    'noresep' => 'Noresep',
                                    'ruangan_id' => 'Ruangan',
                                    'ruangan_nama' => 'Ruangan Nama',
                                    'create_loginpemakai_id' => 'Create Loginpemakai',
                                    'pegawai_id' => 'Pegawai',
                                    'nama_pegawai' => 'Nama Pegawai',
                                    'nama_pemakai' => 'Nama Pemakai',
                                
                                    'tglpenjualanAwal'=>'Tgl penjualan',
                                    'tglpenjualanAkhir'=>'Sampai dengan',
                            );
	}
/* ============================= By Category ========================================== */
                public function criteriaCategory()
                {
		$criteria=new CDbCriteria;

                                $criteria->select =
                                        'category_id, category_name,
                                        SUM(qty_oa) AS qty_category,
                                        SUM(hargajual_oa) AS hargajual_category,
                                        SUM(hargasatuan_oa) AS hargasatuan_category,
                                        SUM(totalhargajual) AS totalharga_category';
                                $criteria->group = 'category_id, category_name';
		$criteria->compare('category_id',$this->category_id);
		$criteria->addBetweenCondition('tglpenjualan',$this->tglpenjualanAwal,$this->tglpenjualanAkhir);
                
                                return $criteria;
                }
                
	public function searchCategory()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaCategory(),
		));
	}
        
                public function searchCategoryprint()
                {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaCategory(),
                                                'pagination'=>false,
		));
                }
                
                public function searchCategorygrafik(){
                    $criteria = $this->criteriaCategory();
                    $crit = new MyCriteria();
                    $crit->select = 'category_name as data, count(category_id) as jumlah';
                    $crit->group = 'category_name';
                    $crit->mergeWith($criteria);
                    return new CActiveDataProvider($this, array(
                                'criteria'=>$crit,
                        ));
                }
/* ============================= End By Category ========================================== */
                
/* ============================= By Subcategory ========================================== */
                public function criteriaSubcategory()
                {
		$criteria=new CDbCriteria;

                                $criteria->select =
                                        'subcategory_id, subcategory_name,
                                        SUM(qty_oa) AS qty_subcategory,
                                        SUM(hargajual_oa) AS hargajual_subcategory,
                                        SUM(hargasatuan_oa) AS hargasatuan_subcategory,
                                        SUM(totalhargajual) AS totalharga_subcategory';
                                $criteria->group = 'subcategory_id, subcategory_name';
		$criteria->compare('subcategory_id',$this->subcategory_id);
		$criteria->addBetweenCondition('tglpenjualan',$this->tglpenjualanAwal,$this->tglpenjualanAkhir);
                
                                return $criteria;
                }
                
	public function searchSubcategory()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaSubcategory(),
		));
	}
        
                public function searchSubcategoryprint()
                {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaSubcategory(),
                                                'pagination'=>false,
		));
                }
                
                public function searchSubcategorygrafik(){
                    $criteria = $this->criteriaSubcategory();
                    $crit = new MyCriteria();
                    $crit->select = 'subcategory_name as data, count(subcategory_id) as jumlah';
                    $crit->group = 'subcategory_name';
                    $crit->mergeWith($criteria);
                    return new CActiveDataProvider($this, array(
                                'criteria'=>$crit,
                        ));
                }
/* ============================= End By Subcategory ========================================== */
                
/* ============================= By Product ========================================== */
                public function criteriaProduct()
                {
                    
                    $criteria = new CDbCriteria;
                    $criteria->order = 'stock_name';
                    $criteria->select = 'tglpenjualan, brg_id, category_name,subcategory_name,stock_code,stock_name,ppn_persen, hargajual_oa,harganetto,qty_oa,gp_persen,
                                        sum(qty_oa) as qty_oa,
                                        sum(hargajual_oa * qty_oa) as value,
                                        sum(((ppn_persen + 1) * (hargajual_oa * qty_oa) / 10)) as vat,
                                        sum((hargajual_oa * qty_oa) - (((hargajual_oa * qty_oa) * ppn_persen) / 100)) AS totalnetvalue,
                                        sum(harganetto * qty_oa) as cogs,
                                        sum(gp_persen) as gp_persen';
                    $criteria->group = 'tglpenjualan, brg_id, category_name,subcategory_name,stock_code,stock_name,ppn_persen, hargajual_oa,harganetto,qty_oa,gp_persen';
                    $criteria->compare('penjualanresep_id', $this->penjualanresep_id);
                    $criteria->addBetweenCondition('tglpenjualan', $this->tglpenjualanAwal, $this->tglpenjualanAkhir);
                    $criteria->compare('LOWER(noresep)', strtolower($this->noresep), true);
                    $criteria->compare('brg_id', $this->brg_id);
                   
                    $criteria->compare('category_id', $this->category_id);
                    $criteria->compare('LOWER(category_name)', strtolower($this->category_name), true);
                    $criteria->compare('LOWER(stock_code)', strtolower($this->stock_code), true);
                    $criteria->compare('LOWER(stock_name)', strtolower($this->stock_name), true);
          
                    $criteria->compare('qty_oa', $this->qty_oa);
                    $criteria->compare('hargajual_oa',$this->hargajual_oa);
                    $criteria->compare('hargasatuan_oa',$this->hargasatuan_oa);
                    

                    return $criteria;
        
//		$criteria=new CDbCriteria;
//                
//		$criteria->compare('stock_name',$this->stock_name);
//		$criteria->addBetweenCondition('tglpenjualan',$this->tglpenjualanAwal,$this->tglpenjualanAkhir);
//                
//                return $criteria;
                }
                
	public function searchProduct()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaProduct(),
		));
	}
        
                public function searchProductprint()
                {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaProduct(),
                                                'pagination'=>false,
		));
                }
        

        public function searchProductgrafik(){
            $criteria = $this->criteriaProduct();
            $crit = new MyCriteria();
            $crit->select = 'stock_name as data, count(brg_id) as jumlah';
            $crit->group = 'stock_name';
            $crit->mergeWith($criteria);
            return new CActiveDataProvider($this, array(
			'criteria'=>$crit,
		));
        }
/* ============================= End By Category ========================================== */

        public function getStockItems() {
            $data = Yii::app()->db->createCommand("SELECT stock_code, stock_name FROM laporanpenjualanprodukpos_v GROUP BY stock_code, stock_name")->queryAll();
            return $data;
        }

        public function getCategoryItems() {
//            $data = Yii::app()->db->createCommand("SELECT category_id, category_name FROM laporanpenjualanprodukpos_v GROUP BY category_id, category_name")->queryAll();
            $data = Yii::app()->db->createCommand("SELECT jenisobatalkes_id AS category_id, jenisobatalkes_nama AS category_name FROM jenisobatalkes_m WHERE jenisobatalkes_farmasi=FALSE")->queryAll();
            return $data;
        }

        public function getSubcategoryItems() {
            $data = Yii::app()->db->createCommand("SELECT subjenis_id AS subcategory_id, subjenis_nama AS subcategory_name FROM subjenis_m WHERE subjenis_farmasi=FALSE")->queryAll();
            return $data;
        }
        
         public function Criteria()
                {
                    $criteria = new CDbCriteria;
                    
                    $criteria->compare('stock_name',$this->stock_name);
                    $criteria->addBetweenCondition('tglpenjualan',$this->tglpenjualanAwal,$this->tglpenjualanAkhir);
                    return $criteria;
                }
         public function getQty()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(qty_oa)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
        public function getHargajual()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(hargajual_oa * qty_oa)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
        public function getTotalhargajual()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(((hargajual_oa * qty_oa) - ((ppn_persen / 100) * (hargajual_oa * qty_oa ))))';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
        public function getTotalhargasatuan()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM((ppn_persen + 1) * (hargajual_oa * qty_oa) / 10)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
        public function getTotalharganetto()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(harganetto * qty_oa)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
        public function getTotalgp()
        {
            $criteria=$this->Criteria();
            $criteria->select = 'SUM(gp_persen)';
            return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
        }
}

?>
