<?php

/**
 * This is the model class for table "obatalkes_m".
 *
 * The followings are the available columns in table 'obatalkes_m':
 * @property integer $obatalkes_id
 * @property integer $lokasigudang_id
 * @property integer $therapiobat_id
 * @property integer $pbf_id
 * @property integer $generik_id
 * @property integer $satuanbesar_id
 * @property integer $sumberdana_id
 * @property integer $satuankecil_id
 * @property integer $jenisobatalkes_id
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $kemasanbesar
 * @property integer $kekuatan
 * @property string $satuankekuatan
 * @property double $harganetto
 * @property double $hargajual
 * @property double $discount
 * @property string $tglkadaluarsa
 * @property integer $minimalstok
 * @property string $formularium
 * @property boolean $discountinue
 * @property boolean $obatalkes_aktif
 * @property boolean $obatalkes_farmasi
 * @property string $obatalkes_barcode
 * @property double $ppn_persen
 * @property string $activedate
 * @property boolean $mintransaksi
 * @property integer $subjenis_id
 */
class REObatalkesM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatalkesM the static model class
	 */
         
        public $tglkadaluarsa_awal;
        public $tglkadaluarsa_akhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'obatalkes_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sumberdana_id, satuankecil_id, obatalkes_kode, obatalkes_nama, harganetto, hargajual, discount', 'required'),
			array('lokasigudang_id, therapiobat_id, pbf_id, generik_id, satuanbesar_id, sumberdana_id, satuankecil_id, jenisobatalkes_id, kemasanbesar, kekuatan, minimalstok, subjenis_id', 'numerical', 'integerOnly'=>true),
			array('harganetto, hargajual, discount, ppn_persen', 'numerical'),
			array('obatalkes_kode, obatalkes_golongan, obatalkes_kategori, formularium', 'length', 'max'=>50),
			array('obatalkes_nama, obatalkes_barcode', 'length', 'max'=>200),
			array('obatalkes_kadarobat, satuankekuatan', 'length', 'max'=>20),
			array('tglkadaluarsa, discountinue, obatalkes_aktif, obatalkes_farmasi, activedate, mintransaksi', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('obatalkes_id, lokasigudang_id, therapiobat_id, pbf_id, generik_id, satuanbesar_id, sumberdana_id, satuankecil_id, jenisobatalkes_id, obatalkes_kode, obatalkes_nama, obatalkes_golongan, obatalkes_kategori, obatalkes_kadarobat, kemasanbesar, kekuatan, satuankekuatan, harganetto, hargajual, discount, tglkadaluarsa, minimalstok, formularium, discountinue, obatalkes_aktif, obatalkes_farmasi, obatalkes_barcode, ppn_persen, activedate, mintransaksi, subjenis_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'lokasigudang'=>array(self::BELONGS_TO, 'LokasigudangM','lokasigudang_id'),
                    'therapiobat'=>array(self::BELONGS_TO, 'TherapiobatM','therapiobat_id'),
                    'pbf'=>array(self::BELONGS_TO, 'PbfM','pbf_id'),
                    'generik'=>array(self::BELONGS_TO, 'GenerikM','generik_id'),
                    'satuanbesar'=>array(self::BELONGS_TO, 'SatuanbesarM','satuanbesar_id'),
                    'sumberdana'=>array(self::BELONGS_TO, 'SumberdanaM','sumberdana_id'),
                    'satuankecil'=>array(self::BELONGS_TO, 'SatuankecilM','satuankecil_id'),
                    'jenisobatalkes'=>array(self::BELONGS_TO, 'JenisobatalkesM','jenisobatalkes_id'),
                    'detailobat'=>array(self::HAS_MANY,  'ObatalkesdetailM', 'obatalkes_id'),
                    'subjenis'=>array(self::BELONGS_TO, 'SubjenisM','subjenis_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'obatalkes_id' => 'ID',
			'lokasigudang_id' => 'Lokasi Gudang',
			'therapiobat_id' => 'Therapi Obat',
			'pbf_id' => 'Pbf',
			'generik_id' => 'Generik',
			'satuanbesar_id' => 'Satuan Besar',
			'sumberdana_id' => 'Sumber Dana',
			'satuankecil_id' => 'Satuan Kecil',
			'jenisobatalkes_id' => 'Jenis Obat Alkes',
			'obatalkes_kode' => 'Kode Obat Alkes',
			'obatalkes_nama' => 'Nama Obat Alkes',
			'obatalkes_golongan' => 'Golongan',
			'obatalkes_kategori' => 'Kategori',
			'obatalkes_kadarobat' => 'Kadar Obat',
			'kemasanbesar' => 'Kemasan Besar',
			'kekuatan' => 'Kekuatan',
			'satuankekuatan' => 'Satuan Kekuatan',
			'harganetto' => 'Harga Netto',
			'hargajual' => 'Harga Jual',
			'discount' => 'Discount',
			'tglkadaluarsa' => 'Tgl Kadaluarsa',
			'minimalstok' => 'Minimal Stok',
			'formularium' => 'Formularium',
			'discountinue' => 'Discountinue',
			'obatalkes_aktif' => 'Obat Alkes Aktif',
			'obatalkes_farmasi' => 'Obat Alkes Farmasi',
			'obatalkes_barcode' => 'Obat Alkes Barcode',
			'ppn_persen' => 'Ppn',
			'activedate' => 'Activedate',
			'mintransaksi' => 'Minimal Transaksi',
			'subjenis_id' => 'Sub Jenis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with=array('sumberdana', 'satuankecil', 'satuanbesar');
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('therapiobat_id',$this->therapiobat_id);
		$criteria->compare('pbf_id',$this->pbf_id);
		$criteria->compare('generik_id',$this->generik_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('LOWER(sumberdana.sumberdana_nama)',strtolower($this->sumberdana_id),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',$this->obatalkes_aktif);
		$criteria->compare('obatalkes_farmasi',$this->obatalkes_farmasi);
		$criteria->compare('LOWER(obatalkes_barcode)',strtolower($this->obatalkes_barcode),true);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
		$criteria->compare('mintransaksi',$this->mintransaksi);
		$criteria->compare('subjenis_id',$this->subjenis_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('therapiobat_id',$this->therapiobat_id);
		$criteria->compare('pbf_id',$this->pbf_id);
		$criteria->compare('generik_id',$this->generik_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',$this->obatalkes_aktif);
		$criteria->compare('obatalkes_farmasi',$this->obatalkes_farmasi);
		$criteria->compare('LOWER(obatalkes_barcode)',strtolower($this->obatalkes_barcode),true);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
		$criteria->compare('mintransaksi',$this->mintransaksi);
		$criteria->compare('subjenis_id',$this->subjenis_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
         public function getLokasiGudangItems()
        {
            return LokasigudangM::model()->findAll(array('order'=>'lokasigudang_nama'));
        
        }
        
        public function getTherapiObatItems()
        {
            return TherapiobatM::model()->findAll(array('order'=>'therapiobat_nama'));
        }
        
        public function getPbfItems()
        {
            return PbfM::model()->findAll(array('order'=>'pbf_nama'));
        }
        
        public function getGenerikItems()
        {
            return GenerikM::model()->findAll(array('order'=>'generik_nama'));
        }
        
        public function getSatuanBesarItems()
        {
            return SatuanbesarM::model()->findAll(array('order'=>'satuanbesar_nama'));
        }
        
        public function getSatuanKecilItems()
        {
            return SatuankecilM::model()->findAll('satuankecil_aktif=TRUE ORDER BY satuankecil_nama');
        }
        
        public function getJenisObatAlkesItems()
        {
            return JenisobatalkesM::model()->findAll('jenisobatalkes_farmasi=false AND jenisobatalkes_aktif=TRUE ORDER BY jenisobatalkes_nama');
        }
        
        
         public function getKategoriBarangItems() {
            return JenisobatalkesM::model()->findAll('jenisobatalkes_farmasi=FALSE AND jenisobatalkes_aktif=TRUE ORDER BY jenisobatalkes_nama');
        }

         public function getSubKategoriItems() {
        if (!empty($this->jenisobatalkes_id)) {
            return SubjenisM::model()->findAll(' subjenis_farmasi = FALSE AND jenisobatalkes_id=' . $this->category_id . ' order BY subjenis_nama');
        } else {
            return array();
        }
        }
    
         public function getSumberDanaItems()
        {
            return sumberdanaM::model()->findAll('sumberdana_aktif=TRUE ORDER BY sumberdana_nama');
        }
        
        public function showKomposisi($detailObat)
        {
            //$tmp = '<div class="raw">';
            $tmp = '';
            foreach ($detailObat as $obat) {
                $tmp = $tmp.$obat->komposisi.'';
            }
            //$tmp = $tmp.'</div>';
            return $tmp;

        }
        public function searchPrintPriceTag()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('lokasigudang_id',$this->lokasigudang_id);
		$criteria->compare('therapiobat_id',$this->therapiobat_id);
		$criteria->compare('pbf_id',$this->pbf_id);
		$criteria->compare('generik_id',$this->generik_id);
		$criteria->compare('satuanbesar_id',$this->satuanbesar_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('minimalstok',$this->minimalstok);
		$criteria->compare('LOWER(formularium)',strtolower($this->formularium),true);
		$criteria->compare('discountinue',$this->discountinue);
		$criteria->compare('obatalkes_aktif',$this->obatalkes_aktif);
		$criteria->compare('obatalkes_farmasi',$this->obatalkes_farmasi);
		$criteria->compare('LOWER(obatalkes_barcode)',strtolower($this->obatalkes_barcode),true);
		$criteria->compare('ppn_persen',$this->ppn_persen);
		$criteria->compare('LOWER(activedate)',strtolower($this->activedate),true);
		$criteria->compare('mintransaksi',$this->mintransaksi);
		$criteria->compare('subjenis_id',$this->subjenis_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
         public function criteriaDataObat()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
                $criteria->compare('LOWER(obatalkes_namalain)',strtolower($this->obatalkes_namalain),true);
                $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
                $criteria->compare('subjenis_id',$this->subjenis_id);
//		$criteria->compare('obatalkes_id', $this->obatalkes_id);
		
                return $criteria;
		

        }
    
        public function searchDataObat()
            {
                    return new CActiveDataProvider($this, array(
                            'criteria'=>$this->criteriaDataObat(),
                    ));
            }
}