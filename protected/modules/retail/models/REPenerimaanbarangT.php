<?php

class REPenerimaanbarangT extends PenerimaanbarangT
{
        public $supplierCode;
        public $supplierName;
            
        public $tglterimaAwal;
        public $tglterimaAkhir;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return REPenerimaanbarangT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */    
        public function attributeLabels()
        {
                return array(
                        'penerimaanbarang_id' => 'Penerimaanbarang',
                        'supplier_id' => 'Supplier',
                        'permintaanpembelian_id' => 'Permintaanpembelian',
                        'fakturpembelian_id' => 'Fakturpembelian',
                        'noterima' => 'Receive code',
                        'tglterima' => 'Receive date',
                        'tglterimafaktur' => 'Tglterimafaktur',
                        'nosuratjalan' => 'No Surat Jalan',
                        'tglsuratjalan' => 'Tanggal',
                        'gudangpenerima_id' => 'Gudangpenerima',
                        'keteranganterima' => 'Keterangan',
                        'harganetto' => 'Harga Netto',
                        'jmldiscount' => 'Discount',
                        'persendiscount' => 'Persen Discount',
                        'totalpajakppn' => 'RCV PPN',
                        'totalpajakpph' => 'RCV PPH',
                        'totalharga' => 'Total',
                        'create_time' => 'Create Time',
                        'update_time' => 'Update Time',
                        'create_loginpemakai_id' => 'Create Loginpemakai',
                        'update_loginpemakai_id' => 'Update Loginpemakai',
                        'create_ruangan' => 'Create Ruangan',

                        'tglterimaAwal'=>'Receive date',
                        'tglterimaAkhir'=>'Sampai dengan',
                );
        }
        
        public function searchInformasi()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
                
		$criteria->addBetweenCondition('tglterima',$this->tglterimaAwal,$this->tglterimaAkhir);
		$criteria->compare('LOWER(noterima)',strtolower($this->noterima),true);
		$criteria->compare('supplier_id',$this->supplier_id);
                
                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        
        public function searchFakturPembelian()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('LOWER(noterima)',strtolower($this->noterima),true);
		$criteria->compare('LOWER(tglterima)',strtolower($this->tglterima),true);
                $criteria->addCondition('fakturpembelian_id isNull');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
?>
