<?php

class REPermintaanpembelianT extends PermintaanpembelianT
{
        public $supplierCode;
        public $supplierName;
        
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PermintaanpembelianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}


	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'pegawai_id' => 'Pegawai',
			'supplier_id' => 'Supplier',
			'rencanakebfarmasi_id' => 'Rencanakebfarmasi',
			'instalasi_id' => 'Instalasi',
			'syaratbayar_id' => 'Syarat Bayar',
			'ruangan_id' => 'Ruangan',
			'suratpesanan_id' => 'Suratpesanan',
			'tglpermintaanpembelian' => 'PO Date',
			'nopermintaan' => 'PO Code',
			'tglsuratjalan' => 'Tanggal Surat Jalan',
			'nosuratjalan' => 'No Surat Jalan',
			'tglterimabarang' => 'Delivery Date',
			'alamatpengiriman' => 'Alamat Pengiriman',
			'istermasukppn' => 'Istermasukppn',
			'istermasukpph' => 'Istermasukpph',
			'keteranganpermintaan' => 'Keterangan Permintaan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
                        'obatAlkes'=>'Obat Alkes',
		);
	}

}