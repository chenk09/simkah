<?php

class REPermintaandetailT extends PermintaandetailT 
{

        public static function model($className=__CLASS__) {
                return parent::model($className);
        }
                
	public function attributeLabels()
	{
		return array(
			'permintaandetail_id' => 'Permintaandetail',
			'obatalkes_id' => 'Obatalkes',
			'satuankecil_id' => 'Satuankecil',
			'sumberdana_id' => 'Sumberdana',
			'permintaanpembelian_id' => 'Permintaanpembelian',
			'satuanbesar_id' => 'Satuanbesar',
			'stokakhir' => 'Stokakhir',
			'maksimalstok' => 'Maksimalstok',
			'minimalstok' => 'Minimalstok',
			'jmlpermintaan' => 'Jmlpermintaan',
			'persendiscount' => 'Persendiscount',
			'jmldiscount' => 'Jmldiscount',
			'harganettoper' => 'Harganettoper',
			'hargappnper' => 'Hargappnper',
			'hargapphper' => 'Hargapphper',
			'hargasatuanper' => 'Hargasatuanper',
			'tglkadaluarsa' => 'Tglkadaluarsa',
			'jmlkemasan' => 'Jmlkemasan',
		);
	}
        
	public function searchDetailbarang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('permintaanpembelian_id',$this->permintaanpembelian_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>
