<?php

/**
 * This is the model class for table "obatsupplier_m".
 *
 * The followings are the available columns in table 'obatsupplier_m':
 * @property integer $obatalkes_id
 * @property integer $supplier_id
 */
class REBarangsupplierM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ObatsupplierM the static model class
	 */
        public $supplier_nama, $supplier_kode, $supplier_telp, $supplier_fax, $supplier_alamat, $obatalkes_nama;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'obatsupplier_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
//                        array('obatalkes_id,supplier_id', 'application.extensions.uniqueMultiColumnValidator'),
			array('obatalkes_id, supplier_id', 'required'),
			array('obatalkes_id, supplier_id', 'numerical', 'integerOnly'=>true),
                        array('obatalkes_id, supplier_id, belinonppn, disc1, disc2, beliplusdisc, ppn, othercost, totalbeliplusppn', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('obatalkes_id, supplier_kode, obatalkes_nama, supplier_nama, belinonppn, disc1, disc2, beliplusdisc, ppn, othercost, totalbeliplusppn,  supplier_telp, supplier_fax, supplier_alamat, supplier_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'supplier'=>array(self::BELONGS_TO,'SupplierM','supplier_id'),
                    'obatalkes'=>array(self::BELONGS_TO,'ObatalkesM','obatalkes_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'obatalkes_id' => 'Obat Alkes',
			'supplier_id' => 'Supplier',
                        'belinonppn'=>'Beli -PPN',
                        'totalbeliplusppn'=>'Total Beli +PPN',
                        'disc2'=>'Discount 2',
                        'disc1'=>'Discount 1',
                        'ppn'=>'PPN',
                        'belilplusdisc'=>'Beli Plus Discount',
                        'othercost'=>'Other Cost',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with=array('supplier','obatalkes');
                $criteria->compare('obatalkes_id',$this->obatalkes_id,true);
                $criteria->compare('supplier_id',$this->supplier_id, true);
		$criteria->compare('LOWER(obatalkes.obatalkes_nama)',strtolower($this->obatalkes_id),true);
		$criteria->compare('LOWER(supplier.supplier_nama)', strtolower($this->supplier_id),true);
                $criteria->compare('LOWER(supplier.supplier_nama)',strtolower($this->supplier_nama),true);
                $criteria->compare('LOWER(obatalkes.obatalkes_nama)',strtolower($this->obatalkes_nama),true);
                $criteria->compare('LOWER(supplier.supplier_kode)',strtolower($this->supplier_kode),true);
                $criteria->order='supplier.supplier_kode';  
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('supplier_id',$this->supplier_id);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
     public function getBarangItems()
     {
        return REProdukposV::model()->findAll();
     }
}