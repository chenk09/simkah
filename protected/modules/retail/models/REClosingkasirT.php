<?php

/**
 * This is the model class for table "closingkasir_t".
 *
 * The followings are the available columns in table 'closingkasir_t':
 * @property integer $closingkasir_id
 * @property integer $tandabuktibayar_id
 * @property integer $shift_id
 * @property string $tglclosingkasir
 * @property string $closingdari
 * @property string $sampaidengan
 * @property double $nilaiclosingtrans
 * @property double $closingsaldoawal
 * @property double $jmluanglogam
 * @property double $jmluangkertas
 * @property string $keterangan_closing
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class REClosingkasirT extends CActiveRecord
{
                public $tglAwal;
                public $tglAkhir;
                public $tick;
                public $data;
                public $jumlah;
                public $nama_pegawai;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ClosingkasirT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'closingkasir_t';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tglclosingkasir, closingdari, sampaidengan, nilaiclosingtrans, closingsaldoawal, jmluanglogam, jmluangkertas, create_time, update_time, create_loginpemakai_id, create_ruangan', 'required'),
			array(' shift_id', 'numerical', 'integerOnly'=>true),
			array('nilaiclosingtrans, closingsaldoawal, jmluanglogam, jmluangkertas', 'numerical'),
			array('keterangan_closing, update_loginpemakai_id, nama_pegawai', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('closingkasir_id, shift_id, tglclosingkasir, closingdari, nama_pegawai, sampaidengan, nilaiclosingtrans, closingsaldoawal, jmluanglogam, jmluangkertas,keterangan_closing, create_time, update_time, create_loginpemakai_id, update_loginpemakai_id, create_ruangan', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                                    'tandabuktibayar'=>array(self::BELONGS_TO,'TandabuktibayarT','tandabuktibayar_id'),
                                    'shift'=>array(self::BELONGS_TO, 'ShiftM','shift_id'),
                                    'pegawai'=>array(self::BELONGS_TO,'PegawaiM','create_loginpemakai_id'),
                                    'loginpemakai'=>array(self::BELONGS_TO, 'LoginpemakaiK','create_loginpemakai_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'closingkasir_id' => 'ID',
			'shift_id' => 'Shift',
			'tglclosingkasir' => 'Tgl Closing Kasir',
			'closingdari' => 'Closing Dari',
			'sampaidengan' => 'Sampai Dengan',
			'nilaiclosingtrans' => 'Nilai Closing Transaksi',
			'closingsaldoawal' => 'Closing Saldo Awal',
			'jmluanglogam' => 'Jml Uang Logan',
			'jmluangkertas' => 'Jml Uang Kertas',
			'keterangan_closing' => 'Keterangan Closing',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Login Pemakai',
			'update_loginpemakai_id' => 'Update Login Pemakai',
			'create_ruangan' => 'Create Ruangan',
                    
                                                'tglAwal'=>'Tgl Closing kasir',
                                                'tglAkhir'=>'Sampai dengan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('closingkasir_id',$this->closingkasir_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('tglclosing',$this->tglclosingkasir,true);
		$criteria->compare('closingdari',$this->closingdari,true);
		$criteria->compare('sampaidengan',$this->sampaidengan,true);
		$criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
		$criteria->compare('closingsaldoawal',$this->closingsaldoawal);
		$criteria->compare('jmluanglogam',$this->jmluanglogam);
		$criteria->compare('jmluangkertas',$this->jmluangkertas);
		$criteria->compare('LOWER(keterangan_closing)',strtolower($this->keterangan_closing),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->compare('closingkasir_id',$this->closingkasir_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->addBetweenCondition('tglclosingkasir', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('DATE(closingdari)',$this->closingdari,true);
		$criteria->compare('DATE(sampaidengan)',$this->sampaidengan,true);
		$criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
		$criteria->compare('closingsaldoawal',$this->closingsaldoawal);
		$criteria->compare('jmluanglogam',$this->jmluanglogam);
		$criteria->compare('jmluangkertas',$this->jmluangkertas);
		$criteria->compare('LOWER(keterangan_closing)',strtolower($this->keterangan_closing),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                $criteria->order='closingkasir_id asc';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('closingkasir_id',$this->closingkasir_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('DATE(tglclosingkasir)',$this->tglclosingkasir,true);
		$criteria->compare('DATE(closingdari)',$this->closingdari,true);
		$criteria->compare('DATE(sampaidengan)',strtolower($this->sampaidengan),true);
		$criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
		$criteria->compare('closingsaldoawal',$this->closingsaldoawal);
		$criteria->compare('jmluanglogam',$this->jmluanglogam);
		$criteria->compare('jmluangkertas',$this->jmluangkertas);
		$criteria->compare('LOWER(keterangan_closing)',strtolower($this->keterangan_closing),true);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
        public function getShiftItems()
        {
            return ShiftM::model()->findAll('shift_aktif=TRUE ORDER BY shift_nama');
        }

        public function criteriaSalesReport()
        {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->compare('shift_id',$this->shift_id);
            $criteria->addBetweenCondition('tglclosingkasir',$this->tglAwal,$this->tglAkhir);
            return $criteria;
        }
        
        public function searchSalesReport()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaSalesReport(),
                ));
        }
        
        public function searchSalesReportPrint()
        {
                return new CActiveDataProvider($this, array(
                        'criteria'=>$this->criteriaSalesReport(),
                ));
        }
        
        public function searchSalesReportGrafik()
        {
            $criteria = $this->criteriaSalesReport();
            $crit = new CDbCriteria();
            $crit->compare('t.shift_id',$this->shift_id);
            $crit->addBetweenCondition('tglclosingkasir',$this->tglAwal,$this->tglAkhir);
            $crit->with = 'shift';
            $crit->select = 'shift_nama AS data, SUM(tottransaksi) AS jumlah';
            $crit->group = 't.shift_id, t.closingkasir_id, shift.shift_id, shift_namalainnya, shift_jamawal, shift_jamakhir, shift_aktif, shift_nama';
            return new CActiveDataProvider($this, array(
                        'criteria'=>$crit,
//                        'pagination'=>true,
                ));
        }
        
        public function getclosingdarisampaidengan()
        {
            return $this->closingdari.' - '.$this->sampaidengan;
        }
        
         public function getPegawaiItems()
        {
            return PegawaiM::model()->findAll('pegawai_aktif=TRUE ORDER BY nama_pegawai');
        }
}