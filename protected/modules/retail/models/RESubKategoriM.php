<?php

/**
 * This is the model class for table "subjenis_m".
 *
 * The followings are the available columns in table 'subjenis_m':
 * @property integer $subjenis_id
 * @property integer $jenisobatalkes_id
 * @property string $subjenis_kode
 * @property string $subjenis_nama
 * @property string $subjenis_namalainnya
 * @property boolean $subjenis_farmasi
 * @property boolean $subjenis_aktif
 */
class RESubKategoriM extends SubjenisM
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SubjenisM the static model class
	 */
        public $jenisobatalkes_nama;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('jenisobatalkes');
		$criteria->compare('subjenis_id',$this->subjenis_id);
		$criteria->compare('LOWER(jenisobatalkes_nama)',strtolower($this->jenisobatalkes_id),true);
		$criteria->compare('LOWER(subjenis_kode)',strtolower($this->subjenis_kode),true);
		$criteria->compare('LOWER(subjenis_nama)',strtolower($this->subjenis_nama),true);
		$criteria->compare('LOWER(subjenis_namalainnya)',strtolower($this->subjenis_namalainnya),true);
		$criteria->compare('subjenis_farmasi',$this->subjenis_farmasi);
		$criteria->compare('subjenis_aktif',$this->subjenis_aktif);
                $criteria->order = 'subjenis_id';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getJenisobatalkesItems()
        {
            return JenisobatalkesM::model()->findAll();
        }
}