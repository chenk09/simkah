<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class REPermintaanpembalianT extends PermintaanpembelianT {

                public static function model($className=__CLASS__) {
                        return parent::model($className);
                }
                public $tglpermintaanpembelianAwal;
                public $tglpermintaanpembelianAkhir;
	public function attributeLabels()
	{
		return array(
			'permintaanpembelian_id' => 'ID',
			'pegawai_id' => 'Pegawai',
			'supplier_id' => 'Supplier',
			'rencanakebfarmasi_id' => 'Rencanakebfarmasi',
			'instalasi_id' => 'Instalasi',
			'syaratbayar_id' => 'Syarat Bayar',
			'ruangan_id' => 'Ruangan',
			'suratpesanan_id' => 'Suratpesanan',
			'tglpermintaanpembelian' => 'PO date',
			'nopermintaan' => 'No PO',
			'tglsuratjalan' => 'Tanggal Surat Jalan',
			'nosuratjalan' => 'No Surat Jalan',
			'tglterimabarang' => 'Delivery date',
			'alamatpengiriman' => 'Alamat Pengiriman',
			'istermasukppn' => 'Istermasukppn',
			'istermasukpph' => 'Istermasukpph',
			'keteranganpermintaan' => 'Keterangan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
                                                'obatAlkes'=>'Obat Alkes',
                    
                                                'tglpermintaanpembelianAwal'=>'PO date',
                                                'tglpermintaanpembelianAkhir'=>'Sampai dengan',
		);
	}
        
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;


		$criteria->addBetweenCondition('tglpermintaanpembelian',$this->tglpermintaanpembelianAwal,$this->tglpermintaanpembelianAkhir);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(nopermintaan)',strtolower($this->nopermintaan),true);
                $criteria->addCondition('penerimaanbarang_id IS NULL');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

}

?>
