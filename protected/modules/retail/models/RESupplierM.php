<?php

/**
 * This is the model class for table "supplier_m".
 *
 * The followings are the available columns in table 'supplier_m':
 * @property integer $supplier_id
 * @property string $supplier_kode
 * @property string $supplier_nama
 * @property string $supplier_namalain
 * @property string $supplier_alamat
 * @property string $supplier_propinsi
 * @property string $supplier_kabupaten
 * @property string $supplier_telp
 * @property string $supplier_fax
 * @property string $supplier_kodepos
 * @property string $supplier_npwp
 * @property string $supplier_norekening
 * @property string $supplier_namabank
 * @property string $supplier_rekatasnama
 * @property string $supplier_matauang
 * @property string $supplier_website
 * @property string $supplier_email
 * @property string $supplier_logo
 * @property string $supplier_cp
 * @property string $supplier_cp_hp
 * @property string $supplier_cp_email
 * @property string $supplier_cp2
 * @property string $supplier_cp2_hp
 * @property string $supplier_cp2_email
 * @property string $supplier_jenis
 * @property integer $supplier_termin
 * @property boolean $supplier_aktif
 */
class RESupplierM extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SupplierM the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'supplier_m';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('supplier_kode, supplier_nama, supplier_alamat, supplier_jenis', 'required'),
			array('supplier_termin', 'numerical', 'integerOnly'=>true),
			array('supplier_kode', 'length', 'max'=>10),
			array('supplier_nama, supplier_namalain, supplier_propinsi, supplier_kabupaten, supplier_npwp, supplier_norekening, supplier_namabank, supplier_rekatasnama, supplier_cp, supplier_cp_hp, supplier_cp_email, supplier_cp2, supplier_cp2_hp, supplier_cp2_email', 'length', 'max'=>100),
			array('supplier_telp, supplier_fax, supplier_kodepos, supplier_matauang, supplier_website, supplier_email', 'length', 'max'=>50),
			array('supplier_logo', 'length', 'max'=>500),
			array('supplier_jenis', 'length', 'max'=>20),
			array('supplier_aktif', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('supplier_id, supplier_kode, supplier_nama, supplier_namalain, supplier_alamat, supplier_propinsi, supplier_kabupaten, supplier_telp, supplier_fax, supplier_kodepos, supplier_npwp, supplier_norekening, supplier_namabank, supplier_rekatasnama, supplier_matauang, supplier_website, supplier_email, supplier_logo, supplier_cp, supplier_cp_hp, supplier_cp_email, supplier_cp2, supplier_cp2_hp, supplier_cp2_email, supplier_jenis, supplier_termin, supplier_aktif', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'supplier_id' => 'ID',
			'supplier_kode' => 'Kode Supplier',
			'supplier_nama' => 'Nama Supplier',
			'supplier_namalain' => 'Nama Lainnya',
			'supplier_alamat' => 'Alamat',
			'supplier_propinsi' => 'Propinsi',
			'supplier_kabupaten' => 'Kabupaten',
			'supplier_telp' => 'Telp',
			'supplier_fax' => 'Fax',
			'supplier_kodepos' => 'Kode Pos',
			'supplier_npwp' => 'NPWP',
			'supplier_norekening' => 'No Rekening',
			'supplier_namabank' => 'Nama Bank',
			'supplier_rekatasnama' => 'Atas Nama',
			'supplier_matauang' => 'Mata Uang',
			'supplier_website' => 'Website',
			'supplier_email' => 'E-mail',
			'supplier_logo' => 'Logo',
			'supplier_cp' => 'Nama',
			'supplier_cp_hp' => 'Hp',
			'supplier_cp_email' => 'E-mail',
			'supplier_cp2' => 'Nama',
			'supplier_cp2_hp' => 'Hp',
			'supplier_cp2_email' => 'E-mail',
			'supplier_jenis' => 'Jenis Supplier',
			'supplier_termin' => 'Supplier Termin',
			'supplier_aktif' => 'Supplier Aktif',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(supplier_kode)',strtolower($this->supplier_kode),true);
		$criteria->compare('LOWER(supplier_nama)',strtolower($this->supplier_nama),true);
		$criteria->compare('LOWER(supplier_namalain)',strtolower($this->supplier_namalain),true);
		$criteria->compare('LOWER(supplier_alamat)',strtolower($this->supplier_alamat),true);
		$criteria->compare('LOWER(supplier_propinsi)',strtolower($this->supplier_propinsi),true);
		$criteria->compare('LOWER(supplier_kabupaten)',strtolower($this->supplier_kabupaten),true);
		$criteria->compare('LOWER(supplier_telp)',strtolower($this->supplier_telp),true);
		$criteria->compare('LOWER(supplier_fax)',strtolower($this->supplier_fax),true);
		$criteria->compare('LOWER(supplier_kodepos)',strtolower($this->supplier_kodepos),true);
		$criteria->compare('LOWER(supplier_npwp)',strtolower($this->supplier_npwp),true);
		$criteria->compare('LOWER(supplier_norekening)',strtolower($this->supplier_norekening),true);
		$criteria->compare('LOWER(supplier_namabank)',strtolower($this->supplier_namabank),true);
		$criteria->compare('LOWER(supplier_rekatasnama)',strtolower($this->supplier_rekatasnama),true);
		$criteria->compare('LOWER(supplier_matauang)',strtolower($this->supplier_matauang),true);
		$criteria->compare('LOWER(supplier_website)',strtolower($this->supplier_website),true);
		$criteria->compare('LOWER(supplier_email)',strtolower($this->supplier_email),true);
		$criteria->compare('LOWER(supplier_logo)',strtolower($this->supplier_logo),true);
		$criteria->compare('LOWER(supplier_cp)',strtolower($this->supplier_cp),true);
		$criteria->compare('LOWER(supplier_cp_hp)',strtolower($this->supplier_cp_hp),true);
		$criteria->compare('LOWER(supplier_cp_email)',strtolower($this->supplier_cp_email),true);
		$criteria->compare('LOWER(supplier_cp2)',strtolower($this->supplier_cp2),true);
		$criteria->compare('LOWER(supplier_cp2_hp)',strtolower($this->supplier_cp2_hp),true);
		$criteria->compare('LOWER(supplier_cp2_email)',strtolower($this->supplier_cp2_email),true);
		$criteria->compare('LOWER(supplier_jenis)',strtolower($this->supplier_jenis),true);
		$criteria->compare('supplier_termin',$this->supplier_termin);
		$criteria->compare('supplier_aktif',$this->supplier_aktif);
                $criteria->condition='supplier_jenis=:supplier_jenis';
                $criteria->params=array(':supplier_jenis'=>'Retail');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(supplier_kode)',strtolower($this->supplier_kode),true);
		$criteria->compare('LOWER(supplier_nama)',strtolower($this->supplier_nama),true);
		$criteria->compare('LOWER(supplier_namalain)',strtolower($this->supplier_namalain),true);
		$criteria->compare('LOWER(supplier_alamat)',strtolower($this->supplier_alamat),true);
		$criteria->compare('LOWER(supplier_propinsi)',strtolower($this->supplier_propinsi),true);
		$criteria->compare('LOWER(supplier_kabupaten)',strtolower($this->supplier_kabupaten),true);
		$criteria->compare('LOWER(supplier_telp)',strtolower($this->supplier_telp),true);
		$criteria->compare('LOWER(supplier_fax)',strtolower($this->supplier_fax),true);
		$criteria->compare('LOWER(supplier_kodepos)',strtolower($this->supplier_kodepos),true);
		$criteria->compare('LOWER(supplier_npwp)',strtolower($this->supplier_npwp),true);
		$criteria->compare('LOWER(supplier_norekening)',strtolower($this->supplier_norekening),true);
		$criteria->compare('LOWER(supplier_namabank)',strtolower($this->supplier_namabank),true);
		$criteria->compare('LOWER(supplier_rekatasnama)',strtolower($this->supplier_rekatasnama),true);
		$criteria->compare('LOWER(supplier_matauang)',strtolower($this->supplier_matauang),true);
		$criteria->compare('LOWER(supplier_website)',strtolower($this->supplier_website),true);
		$criteria->compare('LOWER(supplier_email)',strtolower($this->supplier_email),true);
		$criteria->compare('LOWER(supplier_logo)',strtolower($this->supplier_logo),true);
		$criteria->compare('LOWER(supplier_cp)',strtolower($this->supplier_cp),true);
		$criteria->compare('LOWER(supplier_cp_hp)',strtolower($this->supplier_cp_hp),true);
		$criteria->compare('LOWER(supplier_cp_email)',strtolower($this->supplier_cp_email),true);
		$criteria->compare('LOWER(supplier_cp2)',strtolower($this->supplier_cp2),true);
		$criteria->compare('LOWER(supplier_cp2_hp)',strtolower($this->supplier_cp2_hp),true);
		$criteria->compare('LOWER(supplier_cp2_email)',strtolower($this->supplier_cp2_email),true);
		$criteria->compare('LOWER(supplier_jenis)',strtolower($this->supplier_jenis),true);
		$criteria->compare('supplier_termin',$this->supplier_termin);
		$criteria->compare('supplier_aktif',$this->supplier_aktif);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
        
          public function beforeSave() {
            $this->supplier_namalain = strtoupper($this->supplier_namalain);
            $this->supplier_nama = ucwords(strtolower($this->supplier_nama));
            return parent::beforeSave();
        }
        
        public function getPropinsiItems()
        {
            return PropinsiM::model()->findAll('propinsi_aktif=TRUE ORDER BY propinsi_nama');
        }
        
        public function getkabupatenItems()
        {
            return KabupatenM::model()->findAll('kabupaten_aktif=TRUE ORDER BY kabupaten_nama');
        }
        
        public function getJenisSupplierItems()
        {
            return LookupM::model()->findAll("lookup_type='jenissupplier' ORDER BY lookup_name");
        }
        
         public function getMataUangItems()
        {
            return LookupM::model()->findAll("lookup_type='matauang' ORDER BY lookup_name");
        }
}