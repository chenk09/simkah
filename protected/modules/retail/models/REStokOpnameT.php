<?php

/**
 * This is the model class for table "stokopname_t".
 *
 * The followings are the available columns in table 'stokopname_t':
 * @property integer $stokopname_id
 * @property integer $ruangan_id
 * @property integer $formuliropname_id
 * @property string $tglstokopname
 * @property string $nostokopname
 * @property boolean $isstokawal
 * @property string $jenisstokopname
 * @property string $keterangan_opname
 * @property double $totalharga
 * @property double $totalnetto
 * @property integer $mengetahui_id
 * @property integer $petugas1_id
 * @property integer $petugas2_id
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $update_loginpemakai_id
 * @property string $create_ruangan
 */
class REStokOpnameT extends StokopnameT
{
      public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'mengetahui'=>array(self::BELONGS_TO, 'PegawaiM', 'mengetahui_id'),
                    'petugas1'=>array(self::BELONGS_TO, 'PegawaiM', 'petugas1_id'),
                    'petugas2'=>array(self::BELONGS_TO, 'PegawaiM', 'petugas2_id'),
		);
	}
        
    public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('mengetahui');
		$criteria->compare('stokopname_id',$this->stokopname_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('formuliropname_id',$this->formuliropname_id);
		$criteria->addBetweenCondition('tglstokopname',$this->tglstokopname, $this->tglAkhir);
		$criteria->compare('LOWER(nostokopname)',strtolower($this->nostokopname),true);
		$criteria->compare('isstokawal',$this->isstokawal);
		$criteria->compare('LOWER(jenisstokopname)',strtolower($this->jenisstokopname),true);
		$criteria->compare('LOWER(keterangan_opname)',strtolower($this->keterangan_opname),true);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('totalnetto',$this->totalnetto);
		$criteria->compare('LOWER(mengetahui.nama_pegawai)',strtolower($this->mengetahui_id),true);
		$criteria->compare('petugas1_id',$this->petugas1_id);
		$criteria->compare('petugas2_id',$this->petugas2_id);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}