<?php

class REStokopnamedetT extends StokopnamedetT{
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchDataObat()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('stokopnamedet_id',$this->stokopnamedet_id);
		$criteria->compare('formstokopname_id',$this->formstokopname_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('stokopname_id',$this->stokopname_id);
		$criteria->compare('obatalkes_id',$this->obatalkes_id);
		$criteria->compare('volume_fisik',$this->volume_fisik);
		$criteria->compare('volume_sistem',$this->volume_sistem);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('jumlahharga',$this->jumlahharga);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('jumlahnetto',$this->jumlahnetto);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
		$criteria->compare('LOWER(kondisibarang)',strtolower($this->kondisibarang),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}

?>
