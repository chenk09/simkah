<?php

class REAntrianbayarposV extends AntrianbayarposV {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->select = 'penjualanresep_id, tglpenjualan, jenispenjualan, totalhargajual, create_loginpemakai_id, nama_pemakai';
        $criteria->group = 'penjualanresep_id, tglpenjualan, jenispenjualan, totalhargajual, create_loginpemakai_id, nama_pemakai';
        $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('brg_id', $this->brg_id);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('LOWER(category_name)', strtolower($this->category_name), true);
        $criteria->compare('subcategory_id', $this->subcategory_id);
        $criteria->compare('LOWER(subcategory_code)', strtolower($this->subcategory_code), true);
        $criteria->compare('LOWER(subcategory_name)', strtolower($this->subcategory_name), true);
        $criteria->compare('LOWER(stock_code)', strtolower($this->stock_code), true);
        $criteria->compare('LOWER(stock_name)', strtolower($this->stock_name), true);
        $criteria->compare('LOWER(satuankecil_nama)', strtolower($this->satuankecil_nama), true);
        $criteria->compare('LOWER(barang_barcode)', strtolower($this->barang_barcode), true);
        $criteria->compare('obatalkes_id', $this->obatalkes_id);
        $criteria->compare('qty_oa', $this->qty_oa);
        $criteria->compare('hargasatuan_oa', $this->hargasatuan_oa);
        $criteria->compare('hargajual_oa', $this->hargajual_oa);
//        $criteria->compare('LOWER(tglpenjualan)', strtolower($this->tglpenjualan), true);
        $criteria->compare('LOWER(jenispenjualan)', strtolower($this->jenispenjualan), true);
        $criteria->compare('totalhargajual', $this->totalhargajual);
        $criteria->compare('penjualanresep_id', $this->penjualanresep_id);
        $criteria->compare('obatalkespasien_id', $this->obatalkespasien_id);
//        $criteria->compare('LOWER(noresep)', strtolower($this->noresep), true);
        $criteria->compare('ruangan_id', $this->ruangan_id);
        $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
//        $criteria->compare('LOWER(pegawai.nama_pegawai)', strtolower($this->create_loginpemakai_id), true);
        $criteria->compare('pegawai_id', $this->pegawai_id);
        $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('LOWER(nama_pemakai)', strtolower($this->nama_pemakai), true);
        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit = -1;

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    protected function beforeValidate() {
        // convert to storage format
        //$this->tglrevisimodul = date ('Y-m-d', strtotime($this->tglrevisimodul));
        $format = new CustomFormat();
        //$this->tglrevisimodul = $format->formatDateMediumForDB($this->tglrevisimodul);
        foreach ($this->metadata->tableSchema->columns as $columnName => $column) {
            if ($column->dbType == 'date') {
                $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
            } else if ($column->dbType == 'timestamp without time zone') {
                $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
            }
        }

        return parent::beforeValidate();
    }

    public function beforeSave() {
        if ($this->tglanamnesis === null || trim($this->tglanamnesis) == '') {
            $this->setAttribute('tglanamnesis', null);
        }
        return parent::beforeSave();
    }

    protected function afterFind() {
        foreach ($this->metadata->tableSchema->columns as $columnName => $column) {

            if (!strlen($this->$columnName))
                continue;

            if ($column->dbType == 'date') {
                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'), 'medium', null);
            } elseif ($column->dbType == 'timestamp without time zone') {
                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
            }
        }
        return true;
    }

}