<?php

class GFObatalkesM extends ObatalkesM
{
    public static function model($className = __CLASS__) 
    {
        return parent::model($className);
    }
    
    public $tglkadaluarsa_awal;
    public $tglkadaluarsa_akhir;
    
    public function criteriaDataObat()
    {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

//		$criteria->compare('obatalkes_id', $this->obatalkes_id);
                                $criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('hargajual',$this->hargajual);
		$criteria->compare('discount',$this->discount);
                                if ($this->tglkadaluarsa == 1){
                                    $criteria->addBetweenCondition('date(tglkadaluarsa)',$this->tglkadaluarsa_awal, $this->tglkadaluarsa_akhir);
                                }
		$criteria->compare('obatalkes_aktif',TRUE);
                
                                return $criteria;
		

    }
    
    public function searchDataObat()
	{
		return new CActiveDataProvider($this, array(
			'criteria'=>$this->criteriaDataObat(),
                                                'pagination'=>false,
		));
	}
        
        
}

?>
