<?php

class FormuliropnameRController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex($id = null)
	{
               //if(!Yii::app()->user->checkAccess(Params::DEFAULT_GUDANG_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $modProduk = new REProdukposV('search');
		if (!empty($id)){
                    $model = FormuliropnameR::model()->findByPK($id);
                    if ($model == 1){
                        $modDetail = FormstokopnameR::model()->findAllByAttributes(array('formuliropname_id'=>$model->formuliropname_id));
                        $modProduk->brg_id = CHtml::listData($modDetail, 'obatalkes_id', 'obatalkes_id');
                    }
                }
                else{
                    $model=new REFormuliropnameR;
                    $model->tglformulir = date('Y-m-d');
                    $model->totalharga = 0;
                    $model->totalvolume = 0;
                    $model->noformulir = Generator::noFormulirOpname();
                }
                
                if (isset($_GET['REProdukposV']))
                {
                    $modProduk->attributes=$_GET['REProdukposV'];
                }
                if(isset($_POST['REFormuliropnameR']))
		{
                        $model->attributes=$_POST['REFormuliropnameR'];
                        $modProduk->unsetAttributes();
                        $modProduk->brg_id = $this->sortPilih($_POST['REProdukposV']);
                        if ($model->validate()){
                            $transaction = Yii::app()->db->beginTransaction();
                            try{
                                $hasil = 0;
                                if($model->save()){
                                    $jumlah = count($modProduk->brg_id);
                                    foreach ($modProduk->brg_id as $data){
                                        $modDetail = new FormstokopnameR();
                                        $modDetail->obatalkes_id = $data;
                                        $modDetail->formuliropname_id = $model->formuliropname_id;
                                        $modDetail->volume_stok = StokobatalkesT::getStokBarang($data);
                                        if ($modDetail->save()){
                                            $hasil++;
                                        }
                                    }
                                }

                                if(($hasil>0)&&($hasil == $jumlah)){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                                    $this->redirect(array('index', 'id'=>$model->formuliropname_id));
                                }
                                else{
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error',"Data Gagal Disimpan ");
                                }
                            }
                            catch(Exception $ex){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                            }
                        }
		}

		$this->render('transaksi',array(
			'model'=>$model, 'modProduk'=>$modProduk,
		));
	}
        
        protected function sortPilih($data){
            $result = array();
            foreach ($data as $i=>$row){
                if ($row['cekList'] == 1){
                    $result[] = $row['brg_id'];
                }
            }
            
            return $result;
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REFormuliropnameR::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfformuliropname-r-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $model= REFormuliropnameR::model()->findByPK($id);
            $modDetails = FormstokopnameR::model()->findAllByAttributes(array('formuliropname_id'=>$id));
//            $model->attributes=$_REQUEST['REFormuliropnameR'];
            $modProduk = new REProdukposV('search');
            $modProduk->brg_id = CHtml::listData($modDetails, 'brg_id', 'brg_id');
            $judulLaporan='Data Formulir Obatalkes Opname';
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT'){
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model, 'modObat'=>$modProduk, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint, 'data'=>$data));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'modObat'=>$modProduk,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'modObat'=>$modProduk,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
