<?php
class HargaJualMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','informasi','PrintPriceTag'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=REProdukBarangM::model()->findByPk($id);
                
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REHargaJualM();
                $modProduk = new REProdukposV();
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                $model->obatalkes_id = 0;

                if (isset($_GET['REHargaJualM'])){
                        $model->unsetAttributes();
                        $model->attributes=$_GET['REHargaJualM'];
                        $model->obatalkes_kode = $_GET['REHargaJualM']['obatalkes_kode'];
                        $model->obatalkes_nama = $_GET['REHargaJualM']['obatalkes_nama'];
                        $model->obatalkes_namalain = $_GET['REHargaJualM']['obatalkes_namalain'];
                        $model->jenisobatalkes_id = $_GET['REHargaJualM']['jenisobatalkes_id'];
                        $model->subjenis_id = $_GET['REHargaJualM']['jenisobatalkes_id'];
                }
                if (isset($_GET['REProdukposV'])) {
                    $modProduk->attributes = $_GET['REProdukposV'];
                }
		if(isset($_POST['REHargaJualM']))
		{
                        $data = $_POST['REHargaJualM'];
                        if (count($data) > 0){
                            $modDetails = $this->validasiTabular($data);
                            $transaction = Yii::app()->db->beginTransaction();
                            try{
                                $jumlah = 0;
                                foreach ($modDetails as $i=>$v){
                                    if ($v->update()){
                                        $jumlah++;
                                    }
                                }
                                
                                    if ($jumlah == count($modDetails)){
                                        $transaction->commit();

                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->refresh();
                                    }
                                    else{
                                        $transaction->rollback();
                                        throw new Exception('Penyimpanan ke Database gagal');
                                    }
                                
                            }
                            catch(Exception $ex){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($ex, true));
                            }
                        }
//                        echo '<pre>';
//                        echo print_r($data);
//                                    exit();
//                        $model->unsetAttributes();
//			$model->attributes=$_POST['KPRegistrasifingerprint'];
//			if($model->save()){
//                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//				$this->redirect(array('create'));
//                        }
		}

		$this->render('create',array(
			'model'=>$model, 'modDetails'=>$modDetails,
                        'modProduk'=>$modProduk,
		));
	}
        
        protected function validasiTabular($data){
            foreach ($data as $i=>$attribute){
                $modDetails[$i] = REHargaJualM::model()->findByPk($i);
                $modDetails[$i]->margin = $attribute['margin'];
                $modDetails[$i]->gp_persen = $attribute['gp'];
                $modDetails[$i]->hargajual = $attribute['hargajual'];
                $modDetails[$i]->harganetto = $attribute['harganetto'];
                $modDetails[$i]->discount = $attribute['discount'];
                $modDetails[$i]->validate();
            }
            
            return $modDetails;
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=REHargaJualM::model()->findByPk($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REHargaJualM']))
		{
			$model->attributes=$_POST['REHargaJualM'];
                        if (Yii::app()->controller->module->id=='gudangFarmasi') {
                        $model->obatalkes_farmasi = TRUE;
                        } else {
                        $model->obatalkes_farmasi = FALSE;
                        }
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->obatalkes_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$model=REHargaJualM::model()->findByPk($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('REHargaJualM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REHargaJualM('search');
                $modProduk = new REProdukposV('search');
		$model->unsetAttributes();  // clear any default values
                $modProduk->unsetAttributes();
		if(isset($_GET['REHargaJualM']))
			$model->attributes=$_GET['REHargaJualM'];
                else if(isset($_GET['REProdukposV']))
                        $modProduk->attributes = $_GET['REProdukposV'];

		$this->render('admin',array(
			'model'=>$model, 'modProduk'=>$modProduk
		));
	}
        
        public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REHargaJualM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REHargaJualM']))
			$model->attributes=$_GET['REHargaJualM'];

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REHargaJualM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadDelete($id)
	{
		$model=REHargaJualM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reharga-jual-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                REProdukposV::model()->updateByPk($id, array('obatalkes_aktif'=>false));
//                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REHargaJualM;
            $model->attributes=$_REQUEST['REHargaJualM'];
            $judulLaporan='Data Produk Barang ';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
}
