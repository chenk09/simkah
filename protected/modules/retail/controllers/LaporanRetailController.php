<?php

class LaporanRetailController extends SBaseController
{
	public $layout='//layouts/column1';
        
        public $defaultAction = 'Index';
	public function actionIndex()
	{
		$this->render('index');
	}
        
                public function actionSalesReport()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new REClosingkasirT;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['REClosingkasirT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['REClosingkasirT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAkhir']);
                        }
                        $this->render('salesReport/index',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintSalesReport()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new REClosingkasirT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Sales Report';

                    //Data Grafik
                    $data['title'] = 'Grafik Sales Report';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['REClosingkasirT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['REClosingkasirT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'salesReport/print';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameSalesReport() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new REClosingkasirT;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Sales Report';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['REClosingkasirT'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['REClosingkasirT'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAkhir']);
                    }
                    $searchdata = $model->searchSalesReportgrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
                public function actionSalesbyCategory()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    if(!Yii::app()->user->checkAccess($this->action->id)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    if (isset($_GET['RELaporanpenjualanprodukposV'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['RELaporanpenjualanprodukposV'];
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $this->render('salesBy/category',array(
                        'model'=>$model,
                    ));
                }
                
                public function actionPrintSalesbyCategory()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Sales By Category';

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Category';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'salesBy/printCategory';

                    $this->printFunctionSalesby($model, $data, $caraPrint, $judulLaporan, $target);
                }       
                
                public function actionFrameSalesbyCategory() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Category';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $searchdata = $model->searchCategorygrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
                public function actionSalesbySubcategory()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    if (isset($_GET['RELaporanpenjualanprodukposV'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['RELaporanpenjualanprodukposV'];
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $this->render('salesBy/subcategory',array(
                        'model'=>$model,
                    ));
                }
                
                public function actionPrintSalesbySubcategory()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Sales By Sub Category';

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Sub Category';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'salesBy/printSubcategory';

                    $this->printFunctionSalesby($model, $data, $caraPrint, $judulLaporan, $target);
                }
                
                public function actionFrameSalesbySubcategory() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Sub Category';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $searchdata = $model->searchSubcategorygrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
                public function actionSalesbyProduct()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    if (isset($_GET['RELaporanpenjualanprodukposV'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['RELaporanpenjualanprodukposV'];
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $this->render('salesBy/product',array(
                        'model'=>$model,
                    ));
                }

                public function actionPrintSalesbyProduct()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Laporan Sales By Product';

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Product';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'salesBy/printProduct';

                    $this->printFunctionSalesby($model, $data, $caraPrint, $judulLaporan, $target);
                }

                public function actionFrameSalesbyProduct() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanpenjualanprodukposV;
                    $model->tglpenjualanAwal = date('Y-m-d 00:00:00');
                    $model->tglpenjualanAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Product';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanpenjualanprodukposV'])) {
                        $model->attributes = $_REQUEST['RELaporanpenjualanprodukposV'];
                        $format = new CustomFormat();
                        $model->tglpenjualanAwal = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAwal']);
                        $model->tglpenjualanAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RELaporanpenjualanprodukposV']['tglpenjualanAkhir']);
                    }
                    $searchdata = $model->searchProductgrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
                
                public function actionProduk()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new REProdukposV;
                    if (isset($_GET['REProdukposV'])) {
                        $model->attributes = $_GET['REProdukposV'];
                    }
                    $this->render('produkposV/index',array(
                        'model'=>$model,
                    ));
                }
                
                public function actionPrintProduk()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new REProdukposV;
                    $judulLaporan = 'Laporan Produk';

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Produk';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['REProdukposV'])) {
                        $model->attributes = $_REQUEST['REProdukposV'];
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'produkposV/print';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }

                public function actionFrameGrafikProduk() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new REProdukposV;

                    //Data Grafik
                    $data['title'] = 'Grafik Laporan Sales By Product';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['REProdukposV'])) {
                        $model->attributes = $_REQUEST['REProdukposV'];
                    }
                    $searchdata = $model->searchProdukgrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
                protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $format = new CustomFormat();
                    if (isset($model->tglAwal) && isset($model->tglAkhir)) {
                        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
                    }

                    if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
                        $this->layout = '//layouts/printWindows';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($caraPrint == 'EXCEL') {
                        $this->layout = '//layouts/printExcel';
                         $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($_REQUEST['caraPrint'] == 'PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('', $ukuranKertasPDF);
                        $mpdf->useOddEven = 2;
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
                        $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
                        $mpdf->Output();
                    }
                }
                
                protected function printFunctionSalesby($model, $data, $caraPrint, $judulLaporan, $target){
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $format = new CustomFormat();
                    $periode = $this->parserTanggal($model->tglpenjualanAwal).' s/d '.$this->parserTanggal($model->tglpenjualanAkhir);

                    if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
                        $this->layout = '//layouts/printWindows';
                        $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($caraPrint == 'EXCEL') {
                        $this->layout = '//layouts/printExcel';
                         $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
                    } else if ($_REQUEST['caraPrint'] == 'PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('', $ukuranKertasPDF);
                        $mpdf->useOddEven = 2;
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
                        $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
                        $mpdf->Output();
                    }
                }
                
                public function actionStock()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                        $model = new RELaporanstokprodukposV;
                        $model->tglAwal = date('Y-m-d 00:00:00');
                        $model->tglAkhir = date('Y-m-d 23:59:59');
                        if (isset($_GET['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                        }
                        $this->render('stock/stock',array(
                            'model'=>$model,
                        ));
                }
                
                public function actionPrintStock()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');
                    $judulLaporan = 'Stock Barang';

                    //Data Grafik
                    $data['title'] = 'Grafik Stock';
                    $data['type'] = $_REQUEST['type'];
                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $caraPrint = $_REQUEST['caraPrint'];
                    $target = 'stock/printStock';

                    $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
                }   
                
                public function actionFrameStock() {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout = '//layouts/frameDialog';

                    $model = new RELaporanstokprodukposV;
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    //Data Grafik
                    $data['title'] = 'Grafik Stock Barang';
                    $data['type'] = $_GET['type'];

                    if (isset($_REQUEST['RELaporanstokprodukposV'])) {
                            $format = new CustomFormat;
                            $model->attributes = $_GET['RELaporanstokprodukposV'];
                            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAwal']);
                            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RELaporanstokprodukposV']['tglAkhir']);
                    }
                    $searchdata = $model->searchGrafik();
                    $this->render('_grafik', array(
                        'model' => $model,
                        'data' => $data,
                        'searchdata'=>$searchdata,
                    ));
                }
    
                protected function parserTanggal($tgl){
                    $tgl = explode(' ', $tgl);
                    $result = array();
                    foreach ($tgl as $row){
                        if (!empty($row)){
                            $result[] = $row;
                        }
                    }
                    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

                }
}