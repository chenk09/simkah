<?php
class ProdukBarangMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view',),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','informasi','PrintPriceTag','GantiKode','PrintPrice'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
                $model=REProdukBarangM::model()->findByPk($id);
                
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REProdukBarangM;
                $modSubjenis = SubjenisM::model()->findByPk($model->subjenis_id);
                $model->activedate = date('Y-m-d H:i:s');
                $model->kekuatan = 1;
                $model->ppn_persen = 10;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REProdukBarangM']))
		{
			$model->attributes=$_POST['REProdukBarangM'];
                        $model->margin = $_POST['margin'];
                        $model->gp_persen = $_POST['gp'];
                        $model->movingavarage = $_POST['hargaratarata'];
                        $model->hargamaks = $_POST['hargamaksimal'];
                        $model->hargamin = $_POST['hargaminimal'];
                        $model->sumberdana_id = 1;
                        if (Yii::app()->controller->module->id=='gudangFarmasi') {
                        $model->obatalkes_farmasi = TRUE;
                        } else {
                        $model->obatalkes_farmasi = FALSE;
                        }
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->obatalkes_id));
                        }
		}

		$this->render('create',array(
			'model'=>$model,
                        'modSubjenis'=>$modSubjenis,
		));
	}
        
         public function actionGantiKode()
        {
            if(Yii::app()->request->isAjaxRequest){
//                $jenisobatalkes_id = $_POST['REProdukBarangM']['jenisobatalkes_id'];
                $subjenisobatalkes_id = $_POST['subjenisobatalkes_id'];
                $modSubjenis = SubjenisM::model()->findByPk($subjenisobatalkes_id);
                $count = count(ObatalkesM::model()->findAllByAttributes(array('subjenis_id'=>$subjenisobatalkes_id)))+1;
//                $countkode = count(REProdukBarangM::model()->findByAttributes(array('subjenis_id'=>$subjenisobatalkes_id)))+1;
                $data['obatalkes_kode'] = $modSubjenis->subjenis_kode."".str_pad($count,4,0000,STR_PAD_LEFT);
                
                echo CJSON::encode($data);
                Yii::app()->end();
            }
        }
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=REProdukBarangM::model()->findByPk($id);
                 $model->activedate = date('Y-m-d H:i:s');
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REProdukBarangM']))
		{
			$model->attributes=$_POST['REProdukBarangM'];
                        $model->margin = $_POST['REProdukBarangM']['margin'];
                        $model->gp_persen = $_POST['REProdukBarangM']['gp_persen'];
                        $model->movingavarage = $_POST['hargaratarata'];
                        $model->hargamaks = $_POST['hargamaksimal'];
                        $model->hargamin = $_POST['hargaminimal'];
                        $model->subjenis_id = $_POST['REProdukBarangM']['subjenis_id'];
                        $model->sumberdana_id = 1;
                        if (Yii::app()->controller->module->id=='gudangFarmasi') {
                        $model->obatalkes_farmasi = TRUE;
                        } else {
                        $model->obatalkes_farmasi = FALSE;
                        }
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('admin','id'=>$model->obatalkes_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$model=REProdukBarangM::model()->findByPk($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('REProdukposV');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REProdukposallV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REProdukposallV']))
			$model->attributes=$_GET['REProdukposallV'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionInformasi()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REProdukposV('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REProdukposV']))
			$model->attributes=$_GET['REProdukposV'];

		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REProdukposV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function loadDelete($id)
	{
		$model=REProdukBarangM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reproduk-barang-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                REProdukposV::model()->updateByPk($id, array('obatalkes_aktif'=>false));
//                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REProdukposV;
            $model->attributes=$_REQUEST['REProdukposV'];
            $judulLaporan='Data Produk Barang ';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
          public function actionPrintPriceTag()
        {
            $model= new REProdukposV('searchPrint');
            $model->attributes=$_REQUEST['REProdukposV'];
//            echo '<pre>';
//            echo print_r($model->attributes);
//            exit();
            $judulLaporan='Print Bandroll Barang ';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='priceTag';
                $this->render('printPriceTag',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('printPriceTag',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('printPriceTag',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionPrintPrice($id,$nama)
	{   
                $this->layout='//layouts/printWindows';
                $model = ObatalkesM::model()->findByPk($id);
//                $model = ObatalkesM::model()->findByAttributes(array('obatalkes_id'=>$id .'and obatalkes_nama ASC'));
		$this->render('printPrice',array('model'=>$model,'nama'=>$nama));
	}
        
}
