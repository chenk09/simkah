<?php

class FakturPembelianController extends SBaseController
{
        public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','search','details','detailsFaktur','retur'),
				'users'=>array('@'),
			),
		
		);
	}
        
        public function actionRetur($idReturPembelian)
        {
            
            $modPenerimaan = REPenerimaanbarangT::model()->findByPk($idReturPembelian);
            $modPenerimaandetail = REPenerimaandetailT::model()->findAll('penerimaanbarang_id='.$idReturPembelian.'');
            $modRetur = new GFReturPembelianT;
            $modRetur->penerimaanbarang_id=$modPenerimaan->penerimaanbarang_id;
            $modRetur->noretur=  Generator::noRetur();
            $modRetur->totalretur=0;
            $modRetur->tglretur=date('Y-m-d H:i:s');
            $modRetur->supplier_id=$modPenerimaan->supplier_id;
            $modReturDetails = new GFReturDetailT;
            $tersimpan=false;
            $totalretur = REPenerimaandetailT::model()->findAllByAttributes(array('penerimaanbarang_id' => $modPenerimaan->penerimaanbarang_id));
            $totretur = COUNT($totalretur);
            $modRetur->totalretur = $totretur;
            if(isset($_POST['GFReturPembelianT'])){

            $transaction = Yii::app()->db->beginTransaction();
            try {     
                $jumlahCekList=0;
                $jumlahSave=0;
                $modRetur = new GFReturPembelianT;
                $modRetur->attributes=$_POST['GFReturPembelianT'];
                $modRetur->ruangan_id=Yii::app()->user->getState('ruangan_id');
                if($modRetur->save()){//Jika retur Berhasi Disimpan
                $jumlahObat=COUNT($_POST['GFReturDetailT']['obatalkes_id']);
                    for($i=0; $i<=$jumlahObat; $i++):
                       if($_POST['checkList'][$i]=='1'){
                            $jumlahCekList++;
                            $modReturDetails = new GFReturDetailT;
                            $modReturDetails->penerimaandetail_id=$_POST['GFReturDetailT']['penerimaandetail_id'][$i];
                            $modReturDetails->obatalkes_id=$_POST['GFReturDetailT']['obatalkes_id'][$i];
                            $modReturDetails->satuanbesar_id=$_POST['GFReturDetailT']['satuanbesar_id'][$i];
                            $modReturDetails->fakturdetail_id=$_POST['GFReturDetailT']['fakturdetail_id'][$i];
                            $modReturDetails->sumberdana_id=$_POST['GFReturDetailT']['sumberdana_id'][$i];
                            $modReturDetails->returpembelian_id=$modRetur->returpembelian_id;
                            $modReturDetails->satuankecil_id=$_POST['GFReturDetailT']['satuankecil_id'][$i];
                            $modReturDetails->jmlretur=$_POST['GFReturDetailT']['jmlretur'][$i];                       
                            $modReturDetails->harganettoretur=$_POST['GFReturDetailT']['harganettoretur'][$i];
                            $modReturDetails->hargappnretur=$_POST['GFReturDetailT']['hargappnretur'][$i];
                            $modReturDetails->hargapphretur=$_POST['GFReturDetailT']['hargapphretur'][$i];
                            $modReturDetails->jmldiscount=$_POST['GFReturDetailT']['jmldiscount'][$i];
                            $modReturDetails->hargasatuanretur=$_POST['GFReturDetailT']['hargasatuanretur'][$i];
                            if($modReturDetails->save()){
                                $jumlahSave++;
//                                REPenerimaandetailT::model()->updateByPk($modReturDetails->penerimaandetail_id,
//                                                                        array('returdetail_id'=>$modReturDetails->returdetail_id));
                                REPenerimaanbarangT::model()->updateByPk($modPenerimaan->penerimaanbarang_id, array('returpembelian_id'=>$modRetur->returpembelian_id));
                                
                                $idStokObatAlkes=REPenerimaanDetailT::model()->findByPk($modReturDetails->penerimaandetail_id)->stokobatalkes_id;
                                $stokObatAlkesIN=REStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_in;
                                $stokCurrent=REStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_current;
                                $stokINBaru=$stokObatAlkesIN - $modReturDetails->jmlretur;
                                $stokCurrentBaru=$stokCurrent - $modReturDetails->jmlretur;
                                REStokObatAlkesT::model()->updateByPk($idStokObatAlkes,array('qtystok_in'=>$stokINBaru,
                                                                                             'qtystok_current'=>$stokCurrentBaru));
                            }
                    }
                    endfor;     
                 }
                 
                 if(($jumlahCekList==$jumlahSave) and ($jumlahCekList>0)){
                     $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                        $tersimpan=true;
                     
                 }else{
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                 }
             }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }    
                    
            }
            
           
             
            
            $this->render('retur',array('modPenerimaan'=>$modPenerimaan,
                                        'modPenerimaandetail'=>$modPenerimaandetail,
                                        'modRetur'=>$modRetur,
                                        'modReturDetails'=>$modReturDetails,
                                        'tersimpan'=>$tersimpan
                                       ));
        }
        
       
        public function actionSearch()
        {
            $modFaktur = new REFakturPembelianT;
            $format = new CustomFormat();
            $modFaktur->tglAwal=date('Y-m-d');
            $modFaktur->tglAkhir=date('Y-m-d');
            
            if(isset($_GET['REFakturPembelianT'])){
                $modFaktur->attributes=$_GET['REFakturPembelianT'];
                $modFaktur->tglAwal  = $format->formatDateMediumForDB($_REQUEST['REFakturPembelianT']['tglAwal']);
                $modFaktur->tglAkhir = $format->formatDateMediumForDB($_REQUEST['REFakturPembelianT']['tglAkhir']);
            }
            
            $this->render('search',array('modFaktur'=>$modFaktur));
        }


        public function actionIndex()
	{
                $modPenerimaanbarang = new REPenerimaanbarangT('search');
                $modFakturPembelian = new REFakturPembelianT;
                $modFakturPembelian->nofaktur=Generator::noFaktur();
                $modFakturPembelian->tglfaktur=date('Y-m-d');
                $modFakturPembelian->tgljatuhtempo=date('Y-m-d');
                $format = new CustomFormat();
                
                if(isset($_POST['REFakturPembelianT'])){
                   $transaction = Yii::app()->db->beginTransaction();
                   try {  
                       $modFakturPembelian = new REFakturPembelianT;
                       $modFakturPembelian->attributes=$_POST['REFakturPembelianT'];
                       $modFakturPembelian->tglfaktur=$format->formatDateMediumForDB($_POST['REFakturPembelianT']['tglfaktur']);
                       $modFakturPembelian->tgljatuhtempo=$format->formatDateMediumForDB($_POST['REFakturPembelianT']['tgljatuhtempo']);
                       $modFakturPembelian->ruangan_id=Yii::app()->user->getState('ruangan_id');
                       if($modFakturPembelian->save()){
                           $idPenerimaanBarang=$_POST['idPenerimaanBarang'];
                            REPenerimaanbarangT::model()->updateByPk($idPenerimaanBarang, array('fakturpembelian_id'=>$modFakturPembelian->fakturpembelian_id,
                                                                                'tglterimafaktur'=>$modFakturPembelian->tglfaktur,
                                                                                'jmldiscount'=>$modFakturPembelian->jmldiscount,
                                                                                'persendiscount'=>$modFakturPembelian->persendiscount,
                                                                                'totalpajakppn'=>$modFakturPembelian->totalpajakppn,
                                                                                'totalpajakpph'=>$modFakturPembelian->totalpajakpph,
                                                                                'totalharga'=>$modFakturPembelian->totalhargabruto,
                                                                                'harganetto'=>$modFakturPembelian->totharganetto,    
                                                                        ));
                           $jumlahPengecekan=0;
                           $jumlahPenerimaan =COUNT($_POST['FakturdetailT']['obatalkes_id']);
                           for($i=0; $i<$jumlahPenerimaan; $i++):
                                   $modFakturDetail= new REFakturDetailT;
                                   $modFakturDetail->satuankecil_id=$_POST['FakturdetailT']['obatalkes_id'][$i];
                                   $modFakturDetail->fakturpembelian_id=$modFakturPembelian->fakturpembelian_id;
                                   $modFakturDetail->obatalkes_id=$_POST['FakturdetailT']['obatalkes_id'][$i];
                                   $modFakturDetail->penerimaandetail_id=$_POST['FakturdetailT']['penerimaandetail_id'][$i];
                                   $modFakturDetail->satuanbesar_id=$_POST['FakturdetailT']['satuanbesar_id'][$i];
                                   $modFakturDetail->sumberdana_id=$_POST['FakturdetailT']['sumberdana_id'][$i];
                                   $modFakturDetail->jmlterima=$_POST['FakturdetailT']['jmlterima'][$i];
                                   $modFakturDetail->harganettofaktur=$_POST['FakturdetailT']['harganettofaktur'][$i];
                                   $modFakturDetail->hargappnfaktur=$_POST['FakturdetailT']['hargappnfaktur'][$i];
                                   $modFakturDetail->hargapphfaktur=$_POST['FakturdetailT']['hargapphfaktur'][$i];
                                   $modFakturDetail->persendiscount=$_POST['FakturdetailT']['persendiscount'][$i];
                                   $modFakturDetail->jmldiscount=$_POST['FakturdetailT']['jmldiscount'][$i];
                                   $modFakturDetail->hargasatuan=($modFakturDetail->harganettofaktur + $modFakturDetail->hargappnfaktur + $modFakturDetail->hargapphfaktur) - $modFakturDetail->jmldiscount;
                                   if($_POST['FakturdetailT']['tglkadaluarsa'][$i]!=''){
                                        $modFakturDetail->tglkadaluarsa=$_POST['FakturdetailT']['tglkadaluarsa'][$i];
                                   }else{
                                        $modFakturDetail->tglkadaluarsa=null;
                                   }
                                   $modFakturDetail->jmlkemasan=$_POST['FakturdetailT']['jmlkemasan'][$i];
                                   if($modFakturDetail->save()){
                                     $jumlahPengecekan++;
                                     REPenerimaanDetailT::model()->updateByPk($modFakturDetail->penerimaandetail_id,
                                             array('persendiscount'=>$modFakturDetail->persendiscount,
                                                    'jmldiscount'=>$modFakturDetail->jmldiscount,
                                                    'hargappnper'=>$modFakturDetail->hargappnfaktur,
                                                    'hargapphper'=>$modFakturDetail->hargapphfaktur,
                                                    'fakturdetail_id'=>$modFakturDetail->fakturdetail_id,
                                                 ));
                                     $idStokObatAlkes=REPenerimaanDetailT::model()->findByPk($modFakturDetail->penerimaandetail_id)->stokobatalkes_id;

                                     REStokObatAlkesT::model()->updateByPk($idStokObatAlkes,array('harganetto_oa'=>$modFakturDetail->harganettofaktur,
                                                                                                  'hargajual_oa'=>$modFakturDetail->harganettofaktur * (Params::persenHargaJual() / 100),
                                                                                                  'discount'=>$modFakturDetail->persendiscount));
                                   }else{
                                       echo "gagal";exit;
                                   }
                           endfor;
                       }
//                       echo $jumlahPengecekan.'=='.$jumlahPenerimaan;exit;
                       if($jumlahPengecekan==$jumlahPenerimaan){
                           $transaction->commit();
                           Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                       }
                   }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
                
                $modFakturPembelian->tglfaktur = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modFakturPembelian->tglfaktur, 'yyyy-MM-dd'),'medium',null);
                    
                $modFakturPembelian->tgljatuhtempo = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modFakturPembelian->tgljatuhtempo, 'yyyy-MM-dd'),'medium',null);
                
		$this->render('index',array('modPenerimaanbarang'=>$modPenerimaanbarang,
                                            'modFakturPembelian'=>$modFakturPembelian));
	}


}