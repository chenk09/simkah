
<?php

class ClosingkasirTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','printStruck','GantiShift','ClosingKasir'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionClosingKasir($id = null)
	{
           
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_PENJUALAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $model=new REClosingkasirT;
            $modTandabukti = new TandabuktibayarT;
            //$modShift = ShiftM::model()->findByPk(1);
            $model->shift_id = Yii::app()->user->getState('shift_id');
//            if($model->shift_id == 1){
//                 $modShift = ShiftM::model()->findByPk(1);
//                 $jamshift = $modShift->shift_jamawal;
//                 $jamshiftakhir = $modShift->shift_jamakhir;
//                 $model->closingdari = date('Y-m-d')." ".$jamshift;
//                 $model->sampaidengan = date('Y-m-d')." ".$jamshiftakhir;
//            }else if($model->shift_id == 2){
//                 $modShift = ShiftM::model()->findByPk(2);
//                 $jamshift = $modShift->shift_jamawal;
//                 $jamshiftakhir = $modShift->shift_jamakhir;
//                 $model->closingdari = date('Y-m-d')." ".$jamshift;
//                 $model->sampaidengan = date('Y-m-d')." ".$jamshiftakhir;
//            }else if($model->shift_id == 3){
//                 $modShift = ShiftM::model()->findByPk(3);
//                 $jamshift = $modShift->shift_jamawal;
//                 $jamshiftakhir = $modShift->shift_jamakhir;
//                 $model->closingdari = date('Y-m-d')." ".$jamshift;
//                 $model->sampaidengan = date('Y-m-d')." ".$jamshiftakhir;
//            }else{
//                
//                $model->closingdari = date('Y-m-d');
//                $model->sampaidengan = date('Y-m-d');
//            }
            $modShift = ShiftM::model()->findByPk($model->shift_id);
            $jamshift = $modShift->shift_jamawal;
            $jamshiftakhir = $modShift->shift_jamakhir;
            $model->closingsaldoawal = 0;
            $model->nilaiclosingtrans = 0;
            $model->closingdari = date('Y-m-d')." ".$jamshift;
            $model->sampaidengan = date('Y-m-d')." ".$jamshiftakhir;
            $model->create_loginpemakai_id = Yii::app()->user->id;
            
//            $model->closingdari = date('Y-m-d')." ".$jamshift;
//            $model->sampaidengan = date('Y-m-d')." ".$jamshiftakhir;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
          if (isset($_GET['id'])) {
            $model = REClosingkasirT::model()->findByPk($_GET['id']);
            $criteria = new CDbCriteria;
            $criteria->compare('closingkasir_id', $model->closingkasir_id);
            $tandabukti = RETandabuktibayarposV::model()->findAll($criteria);
            $modTandaBukti->jmlpembayaran = 0;
            $modTandaBukti->uangditerima = 0;
            $modTandaBukti->uangkembalian = 0;
//            $penerimaan = PenerimaankasT::model()->findAllByAttributes(array('closingkasir_id' => $model->closingkasir_id));
            $data = $this->rowPenerimaan($tandabukti);
            $pengeluaran = RETandabuktibayarposV::model()->findAllByAttributes(array('closingkasir_id' => $model->closingkasir_id));
            $tottransaksi = COUNT($pengeluaran);
            $model->tottransaksi = $tottransaksi;
            $tr = $this->rowPengeluaran($tandabukti, $data['saldo'], $data['tr']);
        }
        else{
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if ((isset($_POST['REClosingkasirT'])) && (is_null($_GET['id']))) {
            $model->attributes = $_POST['REClosingkasirT'];
            $model->closingdari = $_POST["Filter"]["tglAwal"];
            $model->sampaidengan =$_POST["Filter"]["tglAkhir"];
            $model->create_time = date('Y-m-d H:i:s');
            $model->update_time = date('Y-m-d H:i:s');
            $model->tglclosingkasir = date('Y-m-d H:i:s');
            $model->create_ruangan = Params::RUANGAN_ID_IBS;
            $transaction = Yii::app()->db->beginTransaction();
            if ($model->validate()) {
                
                try{
                    if ($model->save()) {
                        if (count($_POST['cekList']) > 0){
                            foreach ($_POST['cekList'] as $i => $row) {
                                if ($i == 'Terima') {
                                    foreach ($row as $j => $data) {
                                        TandabuktibayarT::model()->updateByPk($j, array('closingkasir_id' => $model->closingkasir_id));
                                    }
                                } 
                            }
                        }
                    }
                    if (count($_POST['cekList']) > 0){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        $this->redirect(array('ClosingKasir', 'id' => $model->closingkasir_id));
                    }
                    else{
                        throw new Exception('Detail Transaksi harus diisi');
                    }
                }
                catch (Exception $ex){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data Gagal disimpan. '.$ex->getMessage());
                }
            }
        }
        }
                 if ((isset($_GET['tglAwal'])) && (isset($_GET['tglAkhir']))) {
                    if (Yii::app()->request->isAjaxRequest) {
                        $tglAwal = $_GET['tglAwal'];
                        $tglAkhir = $_GET['tglAkhir'];
                        $tr = $this->createList($tglAwal, $tglAkhir, true);
                        echo $tr;
                        Yii::app()->end();
                    }
                }
                
                if ((isset($_GET['REClosingkasir_T']['shift_id']))) {
                    if (Yii::app()->request->isAjaxRequest) {
                        $sift_id = $_GET['REClosingkasir_T']['shift_id'];
                        $datashift = $this->createListShift($shift_id, true);
                        echo $datashift;
                        Yii::app()->end();
                    }
                }
		$this->render('create',array(
			'model'=>$model,
                        'modTandabukti'=>$modTandabukti,
                        'modShift'=>$modShift,
                        'tr' => $tr,
                        'datashift'=>$datashift,
		));
	}
        
        public function actionGantiShift()
        {
            if(Yii::app()->request->isAjaxRequest){
                $shiftId = $_POST['shift_id'];
                $modShift = ShiftM::model()->findByPk($shiftId);
                
                $data['jamawal'] = date('Y-m-d')." ".$modShift->shift_jamawal;
                $data['jamakhir']= date('Y-m-d')." ".$modShift->shift_jamakhir;
                
                echo CJSON::encode($data);
                Yii::app()->end();
            }
        }
        
        protected function rowPenerimaan($tandabukti,$text=null) {
        $tr = '';
        $saldo = 0;
        if (count($tandabukti) > 0) {
            foreach ($tandabukti as $i => $row) {
                $i++;
                $saldo += $row->uangditerima;
                $tr .= '<tr>';
                $tr .= '<td>'.$i.'</td>';
                $tr .= '<td>' . $row->nobuktibayar . '</td>';
                $tr .= '<td>' . $row->tglbuktibayar . '</td>';
                $tr .= '<td>' . $row->carapembayaran . '</td>';
                if ($text == true){
                    $tr .= '<td>'.$row->jmlpembayaran.'</td>';
                    $tr .= '<td>'.$row->uangditerima.'</td>';
                    $tr .= '<td>'.$row->uangkembali.'</td>';
                }else{
                    $tr .= '<td>' . CHtml::textField('jmlpembayaran', MyFunction::formatNumber($row->jmlpembayaran), array('class' => 'span2 jmlbayar', 'readonly' => true)) . '</td>';
                    $tr .= '<td>' . CHtml::textField('uangditerima', $row->uangditerima, array('class' => 'span2 numbersOnly jmlterima', 'onblur' => 'setAll();')) . '</td>';
                    $tr .= '<td>' . CHtml::textField('uangkembalian', $row->uangkembalian, array('class' => 'span2 numbersOnly jmlkembali', 'onblur' => 'setAll();')) . '</td>';
                   
                    $tr .= '<td>' . CHtml::checkBox('cekList[Terima][' . $row->tandabuktibayar_id . ']', true, array('value'=>$row-tandabuktibayar_id,'class' => 'cek', 'onclick' => 'setAll();')) . '</td>';
                }
                
                $tr .= '</tr>';
            }
        }
        $data['tr'] = $tr;
        $data['saldo'] = $saldo;

        return $data;
    }
    
    protected function rowPengeluaran($pengeluaran, $saldo, $tr, $text=null) {
        if (count($pengeluaran) > 0) {
            foreach ($pengeluaran as $i => $row) {
               $i++;
               $totaltransaksi = count($pengeluaran);
                $saldo += $row->uangkembalian;
                $tr .= '<tr >';
                $tr .= '<td>'.$i.'</td>';
                $tr .= '<td>' . $row->nobuktibayar . '</td>';
                $tr .= '<td>' . $row->tglbuktibayar . '</td>';
                $tr .= '<td>' . $row->carapembayaran . '</td>';
                if ($text == true){
                    $tr .= '<td>'.$row->jmlpembayaran.'</td>';
                    $tr .= '<td>'.$row->uangditerima.'</td>';
                    $tr .= '<td>'.$row->uangkembali.'</td>';
                }else{
                    $tr .= '<td>' . CHtml::textField('jmlpembayaran', MyFunction::formatNumber($row->jmlpembayaran), array('class' => 'inputFormTabel span2 currency jmlbayar', 'readonly' => true)) .'</td>';
                    $tr .= '<td>' . CHtml::textField('uangditerima', MyFunction::formatNumber($row->uangditerima), array('class' => 'inputFormTabel span2 currency jmlterima', 'onblur' => 'setAll();')) . '</td>';
                    $tr .= '<td>' . CHtml::textField('uangkembalian', MyFunction::formatNumber($row->uangkembalian), array('class' => 'inputFormTabel span2 currency jmlkembali', 'onblur' => 'setAll();')) .
                            CHtml::hiddenField('totaltransaksi', $totaltransaksi, array('class' => 'span2 totaltransaksi', 'readonly' => true)).
                            CHtml::hiddenField('cekList[Terima][' . $row->tandabuktibayar_id . ']', true, array('value'=>$row-tandabuktibayar_id,'class' => 'cek', 'onclick' => 'setAll();')).'</td>';
                  
//                    $tr .= '<td>' . CHtml::checkBox('cekList[Terima][' . $row->tandabuktibayar_id . ']', true, array('value'=>$row-tandabuktibayar_id,'class' => 'cek', 'onclick' => 'setAll();')) . '</td>';
                }
//                $tr .= '<td>' .  .'</td>';
                $tr .= '</tr>';
            }
        }
        return $tr;
    }
    
    protected function createList($tglAwal, $tglAkhir, $status=null) {

        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('tglbuktibayar', $tglAwal, $tglAkhir);
        if ($status == true) {
            $criteria->addCondition('closingkasir_id is null');
        }
        $penerimaan = RETandabuktibayarposV::model()->findAll($criteria);
        $data = $this->rowPenerimaan($tandabukti);
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('tglbuktibayar', $tglAwal, $tglAkhir);
        if ($status == true) {
            $criteria->addCondition('closingkasir_id is null');
        }
        $pengeluaran = RETandabuktibayarposV::model()->findAll($criteria);
        $tr = $this->rowPengeluaran($pengeluaran, $data['saldo'], $data['tr']);
        return $tr;
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REClosingkasirT']))
		{
			$model->attributes=$_POST['REClosingkasirT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->closingkasir_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ClosingkasirT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REClosingkasirT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REClosingkasirT']))
			$model->attributes=$_GET['REClosingkasirT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REClosingkasirT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='reclosingkasir-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REClosingkasirT;
            $model->attributes=$_REQUEST['REClosingkasirT'];
            $judulLaporan='Data ClosingkasirT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function monthDifference($start_date, $end_date)  
         {  
            $start_date = strtotime($start_date);
            $end_date = strtotime($end_date);

            // takes remaning seconds to find months  2629743.83 seconds each month  
            $days = floor(($end_date + $start_date) / 86400);

            $diff = abs($end_date - $start_date);

            $years = floor($diff / (365*60*60*24));
            $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
            $days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));


            return $days;  
         }
     
         public function actionPrintStruck($id)
        {
            $model= REClosingkasirT::model()->findByPk($id);
            $struk = COUNT(REClosingkasirT::model()->findAll());
            $lamahari = $this->monthDifference($model->closingdari, $model->sampaidengan);
            
         
            $model->attributes=$_REQUEST['REClosingkasirT'];
            $judulLaporan='Data ClosingkasirT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printKartu';
                $this->render('struckClosingkasir',array('model'=>$model, 'lamahari'=>$lamahari, 'newdate1'=>$newdate1,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'struk'=>$struk));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model, 'lamahari'=>$lamahari, 'newdate1'=>$newdate1,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'struk'=>$struk));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,  'newdate1'=>$newdate1,'lamahari'=>$lamahari, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint,'struk'=>$struk),true));
                $mpdf->Output();
            }                       
        }
}
