<?php

class PenerimaanItemsController extends SBaseController
{
	public function actionIndex()
	{
                $permintaans = array();
                $supplierTermin = '';
                $modPermintaanPembelian = array();
                $modPenerimaanBarang = new REPenerimaanbarangT;
                $modPenerimaanBarang->tglterima = date('Y-m-d H:i:s');
                $modPenerimaanBarang->noterima = Generator::noTerimaBarang();
                if (isset($_GET['frame']) && !empty($_GET['id'])) {
                    $this->layout = '//layouts/frameDialog';
                    
                    $permintaanpembelian_id = $_GET['id'];
                    $modPermintaanPembelian = REPermintaanpembalianT::model()->findByPk($permintaanpembelian_id);
                    $permintaans = REPermintaandetailT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$permintaanpembelian_id));                   
                    $harganetto = 0; $jmldiscount = 0; $persendiscount = 0; $totalpajakppn = 0; $totalharga = 0;
                    foreach($permintaans as $i=>$permintaan){
                        $harganetto = $harganetto + $permintaan->harganettoper;
                        //$jmldiscount = $permintaan->harganettoper;
                        //$persendiscount = $permintaan->persendiscount;
                        $totalpajakppn = $totalpajakppn + $permintaan->hargappnper;
                        $subtot = $permintaan->harganettoper + $permintaan->hargappnper;
                        $totalharga = $totalharga + $subtot;
                    }
                    if(!empty($modPermintaanPembelian->supplier_id)) {
                        $supplier = SupplierM::model()->findByPk($modPermintaanPembelian->supplier_id);
                        $modPenerimaanBarang->supplier_id = $modPermintaanPembelian->supplier_id;
                        $modPenerimaanBarang->supplierCode = $supplier->supplier_kode;
                        $modPenerimaanBarang->supplierName = $supplier->supplier_nama;
                        $supplierTermin = $supplier->supplier_termin;
                    }
                    $modPenerimaanBarang->permintaanpembelian_id = $permintaanpembelian_id;
                    $modPenerimaanBarang->harganetto = $harganetto;
                    $modPenerimaanBarang->jmldiscount = $jmldiscount;
                    $modPenerimaanBarang->persendiscount = $persendiscount;
                    $modPenerimaanBarang->totalpajakppn = $totalpajakppn;
                    $modPenerimaanBarang->totalharga = $totalharga;
                }
                
                if(isset($_POST['REPenerimaanbarangT'])){
                     $modPenerimaanBarang = new REPenerimaanbarangT;
                     $modPenerimaanBarang->attributes = $_POST['REPenerimaanbarangT'];
                     $modPenerimaanBarang->gudangpenerima_id = Yii::app()->user->getState('ruangan_id');
                     $modPenerimaanBarang->harganetto = $_POST['REPenerimaanbarangT']['harganetto'];
                     $modPenerimaanBarang->jmldiscount = $_POST['REPenerimaanbarangT']['jmldiscount'];
                     $modPenerimaanBarang->persendiscount = $_POST['REPenerimaanbarangT']['persendiscount'];
                     $modPenerimaanBarang->totalpajakppn = $_POST['REPenerimaanbarangT']['totalpajakppn'];
                     $modPenerimaanBarang->totalpajakpph = 0;
                     $modPenerimaanBarang->totalharga = $_POST['REPenerimaanbarangT']['totalharga'];
                     //$modPenerimaanBarang->tglsuratjalan=null;
                     //$modPenerimaanBarang->tglterima=date('Y-m-d'); 
                    if ($modPenerimaanBarang->validate()){
                        $transaction = Yii::app()->db->beginTransaction();
                         try {   
                             if($modPenerimaanBarang->save()){
                                 REPermintaanpembelianT::model()->updateByPk($modPenerimaanBarang->permintaanpembelian_id, array('penerimaanbarang_id'=>$modPenerimaanBarang->penerimaanbarang_id));
                                 $jumlahBrg=COUNT($_POST['penerimaan']);
                                 $jumlahCekPenerimaanDetail=0;
                                 for($i=0; $i<$jumlahBrg; $i++) {
                                      $modStokObatAlkes = new StokobatalkesT;
                                      $obatAlkes = ObatalkesM::model()->findByPk($_POST['penerimaan'][$i]['brg_id']);
                                      $jmlKemasan = (!empty($_POST['penerimaan'][$i]['jmldlmkemasan'])) ? $_POST['penerimaan'][$i]['jmldlmkemasan'] : 1;
                                      $modStokObatAlkes->satuankecil_id = $_POST['penerimaan'][$i]['satuankecil_id'];
                                      $modStokObatAlkes->obatalkes_id = $_POST['penerimaan'][$i]['brg_id'];
                                      $modStokObatAlkes->sumberdana_id = $_POST['penerimaan'][$i]['sumberdana_id'];
                                      $modStokObatAlkes->tglstok_in = $modPenerimaanBarang->tglterima; 
                                      $modStokObatAlkes->tglstok_out = null;
                                      $modStokObatAlkes->qtystok_in = $_POST['penerimaan'][$i]['qty'] * $jmlKemasan;
                                      $modStokObatAlkes->qtystok_out = 0;
                                      $modStokObatAlkes->qtystok_current = $modStokObatAlkes->qtystok_in;
                                      $modStokObatAlkes->harganetto_oa = $_POST['penerimaan'][$i]['ponet'] / $jmlKemasan;
                                      //$persen = (!empty($obatAlkes->gp_persen)) ? $obatAlkes->gp_persen : (int)Params::totalPersenHargaJual() ;
                                      $modStokObatAlkes->hargajual_oa = $obatAlkes->hargajual;
                                      $modStokObatAlkes->discount = $_POST['penerimaan'][$i]['discount'];
                                      if(!empty($_POST['penerimaan'][$i]['tglkadaluarsa'])){
                                            $modStokObatAlkes->tglkadaluarsa = $_POST['penerimaan'][$i]['tglkadaluarsa'];
                                      }else{
                                            $modStokObatAlkes->tglkadaluarsa = null;
                                      }
                                      $modStokObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                                      $modStokObatAlkes->sumberdana_id = (!empty($modStokObatAlkes->sumberdana_id)) ? $modStokObatAlkes->sumberdana_id : 1;

                                        if($modStokObatAlkes->save()){ //Jika Stok Obat Alkes Berhasil Disimpan
                                              $modPenerimaanDetail=new REPenerimaandetailT;
                                              $modPenerimaanDetail->stokobatalkes_id = $modStokObatAlkes->stokobatalkes_id;
                                              $modPenerimaanDetail->penerimaanbarang_id = $modPenerimaanBarang->penerimaanbarang_id;
                                              $modPenerimaanDetail->satuankecil_id = $_POST['penerimaan'][$i]['satkecil'];
                                              $modPenerimaanDetail->sumberdana_id = $_POST['penerimaan'][$i]['sumberdana_id'];
                                              $modPenerimaanDetail->obatalkes_id = $_POST['penerimaan'][$i]['brg_id'];
                                              $modPenerimaanDetail->satuanbesar_id = $_POST['penerimaan'][$i]['satbesar'];
                                              $modPenerimaanDetail->jmlpermintaan = (!empty($_POST['penerimaan'][$i]['jmlpermintaan']))? $_POST['penerimaan'][$i]['jmlpermintaan'] : 0;
                                              $modPenerimaanDetail->jmlterima = $_POST['penerimaan'][$i]['qty'];
                                              $modPenerimaanDetail->persendiscount = $_POST['penerimaan'][$i]['discount'];
                                              $modPenerimaanDetail->jmldiscount = $_POST['penerimaan'][$i]['jmldiscount'];
                                              $modPenerimaanDetail->harganettoper = $_POST['penerimaan'][$i]['ponet'];
                                              $modPenerimaanDetail->hargappnper = $_POST['penerimaan'][$i]['povat'];
                                              $modPenerimaanDetail->hargapphper = 0;
                                              $modPenerimaanDetail->hargasatuanper = $_POST['penerimaan'][$i]['prize'];
                                              //$modPenerimaanDetail->hargasatuanper=($_POST['penerimaan'][$i]['ponet'] + $_POST['penerimaan'][$i]['povat'] ) - $_POST['penerimaan'][$i]['jmldiscount'];
                                              if(isset($_POST['penerimaan'][$i]['tglkadaluarsa']) AND $_POST['penerimaan'][$i]['tglkadaluarsa']>0){
                                                    $modPenerimaanDetail->tglkadaluarsa = $_POST['penerimaan'][$i]['tglkadaluarsa'];
                                              }
                                              $modPenerimaanDetail->jmlkemasan=$_POST['penerimaan'][$i]['jmldlmkemasan'];
                                                  if($modPenerimaanDetail->save()){//Jika Penerimaan Detail Berhasil Disimpan
                                                      StokobatalkesT::model()->updateByPk($modPenerimaanDetail->stokobatalkes_id,array('penerimaandetail_id'=>$modPenerimaanDetail->penerimaandetail_id));
                                                      $this->updateMA($modPenerimaanDetail, $modStokObatAlkes);
                                                      $jumlahCekPenerimaanDetail++;
                                                  }
                                          } else {
                                              $jumlahCekPenerimaanDetail--;
                                          }

                                 }

                            }
                            if($jumlahCekPenerimaanDetail==$jumlahBrg){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                                //$this->refresh();
                            }else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan".'<pre>'.print_r($modStokObatAlkes->getErrors(),1).'</pre>');
                            }
                         }catch(Exception $exc){
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                       }    
                   } 
                }
            
		$this->render('index',array('modPenerimaanBarang'=>$modPenerimaanBarang,
                                            'permintaans'=>$permintaans,
                                            'supplierTermin'=>$supplierTermin,
                                            'modPermintaanPembelian'=>$modPermintaanPembelian));
	}
        
        protected function updateMA($modPenerimaanDetail,$modStokObatAlkes)
        {
            $modBarang = ObatalkesM::model()->findByPk($modPenerimaanDetail->obatalkes_id);
            $prevMA = $modBarang->movingavarage;
            $receiveQTY = $modPenerimaanDetail->jmlterima;
            $receiveNET = $modPenerimaanDetail->harganettoper;
            
            $stoks = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$modPenerimaanDetail->obatalkes_id));
            $stokAkhir = 0; $maxStok = 0;
            foreach($stoks as $k=>$stok){
                $stokAkhir = $stokAkhir + $stok->qtystok_in;
                $maxStok = $maxStok + $stok->qtystok_out;
            }
            $stokOnHand = $stokAkhir;
            
            $totalQtyStok = $stokOnHand + $receiveQTY;
            $MA = (($stokOnHand * $prevMA) +  $receiveNET) / $totalQtyStok;
            $newMA = ($prevMA+($modPenerimaanDetail->hargasatuanper-$modPenerimaanDetail->jmldiscount-$modPenerimaanDetail->hargappnper))/2;
            
            ObatalkesM::model()->updateAll(array('movingavarage'=>$MA), 'obatalkes_id = :idBarang', array(':idBarang'=>$modPenerimaanDetail->obatalkes_id));
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}