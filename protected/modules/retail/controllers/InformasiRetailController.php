<?php

class InformasiRetailController extends SBaseController
{
	public function actionIndex()
	{
		$this->render('index');
	}
        
                public function actionInformasireceive()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout='//layouts/column1';
                    
                    $model = new REPenerimaanbarangT('search');
                    $model->tglterimaAwal = date('Y-m-d 00:00:00');
                    $model->tglterimaAkhir = date('Y-m-d H:i:s');
                    if (isset($_GET['REPenerimaanbarangT'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['REPenerimaanbarangT'];
                        $model->tglterimaAwal = $format->formatDateTimeMediumForDB($_GET['REPenerimaanbarangT']['tglterimaAwal']);
                        $model->tglterimaAkhir = $format->formatDateTimeMediumForDB($_GET['REPenerimaanbarangT']['tglterimaAkhir']);
                    }
                    $this->render('penerimaanbarangT/informasi',array(
                        'model'=>$model,
                    ));
                }
        
                public function actionDetailbarangReceive($id)
                {
                    if (isset($_GET['frame'])) {
                        $this->layout = '//layouts/frameDialog';
                    }
                    if ($_GET['caraPrint']=='PRINT') {
                        $this->layout = '//layouts/printWindows';
                    }
                    $model = REPenerimaanbarangT::model()->findByPK($id);
                    $modDetailbarang = new REPenerimaandetailT('search');
                    $modDetailbarang->penerimaanbarang_id = $id;

                    $this->render('penerimaanbarangT/detailBarang',array(
                        'model'=>$model,
                        'modDetailbarang'=>$modDetailbarang,
                        'id'=>$id,
                    ));
                }
                
                public function actionInformasiPO()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout='//layouts/column1';
                    
                    $model = new REPermintaanpembalianT('search');
                    $model->tglpermintaanpembelianAwal = date('Y-m-d 00:00:00');
                    $model->tglpermintaanpembelianAkhir = date('Y-m-d H:i:s');
                    if (isset($_GET['REPermintaanpembalianT'])) {
                        $format = new CustomFormat;
                        $model->attributes = $_GET['REPermintaanpembalianT'];
                        $model->tglpermintaanpembelianAwal = $format->formatDateTimeMediumForDB($_GET['REPermintaanpembalianT']['tglpermintaanpembelianAwal']);
                        $model->tglpermintaanpembelianAkhir = $format->formatDateTimeMediumForDB($_GET['REPermintaanpembalianT']['tglpermintaanpembelianAkhir']);
                    }
                    $this->render('permintaanpembelianT/informasi',array(
                        'model'=>$model,
                    ));
                }
        
                public function actionDetailbarangPO($id)
                {
                    if (isset($_GET['frame'])) {
                        $this->layout = '//layouts/frameDialog';
                    }
                    $model = REPermintaanpembalianT::model()->findByPK($id);
                    $modDetailbarang = new REPermintaandetailT('search');
                    $modDetailbarang->permintaanpembelian_id = $id;

                    $this->render('permintaanpembelianT/detailBarang',array(
                        'model'=>$model,
                        'modDetailbarang'=>$modDetailbarang,
                    ));
                }
                
	public function actionInformasiopnamestok()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REFormuliropnameR('search');
                $model->tglAkhir = date('Y-m-d');
                $model->tglformulir = date('Y-m-d');
//		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REFormuliropnameR'])){
			$model->attributes=$_GET['REFormuliropnameR'];
                        $format = new CustomFormat();
                        $model->tglformulir = $format->formatDateMediumForDB($model->tglformulir);
                        $model->tglAkhir = $format->formatDateMediumForDB($model->tglAkhir);
                }

		$this->render('formuliropnameR/informasi',array(
			'model'=>$model,
		));
	}
        
        
                public function actionPrintformuliropname()
                {
                    $id = $_REQUEST['id'];
                    $model= REFormuliropnameR::model()->findByPK($id);
                    $modDetails = FormstokopnameR::model()->findAllByAttributes(array('formuliropname_id'=>$id));
        //            $model->attributes=$_REQUEST['REFormuliropnameR'];
                    $modObat = new REProdukposV('search');
                    $modObat->brg_id = CHtml::listData($modDetails, 'obatalkes_id', 'obatalkes_id');
                    $judulLaporan='Data Formulir Stock Opname';
                    $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                    $caraPrint=$_REQUEST['caraPrint'];
                    if($caraPrint=='PRINT'){
                        $this->layout='//layouts/printWindows';
                        $this->render('formuliropnameR/Print',array('model'=>$model, 'modObat'=>$modObat, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint, 'data'=>$data));
                    }
                    else if($caraPrint=='EXCEL') {
                        $this->layout='//layouts/printExcel';
                        $this->render('formuliropnameR/Print',array('model'=>$model,'modObat'=>$modObat,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($_REQUEST['caraPrint']=='PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('',$ukuranKertasPDF); 
                        $mpdf->useOddEven = 2;  
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet,1);  
                        $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                        $mpdf->WriteHTML($this->renderPartial('formuliropnameR/Print',array('model'=>$model,'modObat'=>$modObat,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                        $mpdf->Output();
                    }                       
                }
                
                public function actionInformasiClosing()
                {
                     //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                    $this->layout='//layouts/column1';
                    
                    $model = new REClosingkasirT('searchInformasi');
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d H:i:s');
                    $model->unsetAttributes(); 
                    if (isset($_GET['REClosingkasirT'])) {
                        $format = new CustomFormat();
                        $model->attributes = $_GET['REClosingkasirT'];
                  
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['REClosingkasirT']['tglAkhir']);

                        $model->create_loginpemakai_id = $_GET['REClosingkasirT']['create_loginpemakai_id'];
                        $model->shift_id = $_GET['REClosingkasirT']['shift_id'];
                    }
                    $this->render('/closingkasirT/_searchinformasi',array(
                        'model'=>$model,
                    ));
                }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}