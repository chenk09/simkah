<?php
class BarangsupplierMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view',),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','Deleteupdate'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($obatalkes_id, $supplier_id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($obatalkes_id, $supplier_id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
               //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE))
                {
                    throw new CHttpException(401,Yii::t('mds','You are probihited to access this page. Contact Super Administrator'));
                }
                
		$model=new ObatsupplierM;
                $modSupplier = new RESupplierM;
                $modBarang = new ObatalkesM;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ObatsupplierM']))
		{
			$model->attributes=$_POST['ObatsupplierM'];
                        $model->supplier_id = $_POST['idSupplier'];
                        $model->obatalkes_id = $_POST['ObatsupplierM']['obatalkes_id'];
                        $model->belinonppn = $_POST['ObatsupplierM']['belinonppn'];
                        if (count($_POST['ObatsupplierM']) > 0){
//                            echo '<pre>';
//                                print_r($_POST['ObatsupplierM']);
//                              echo '</pre>';
//                            exit;
                             $model->supplier_id = $_POST['idSupplier'];
                             $model->obatalkes_id = $_POST['ObatsupplierM']['obatalkes_id'];
                             $model->belinonppn = $_POST['ObatsupplierM']['belinonppn'];
                            $modDetails = $this->validasiTabularInput($_POST['ObatsupplierM'], $model);
                           
//                            echo "<pre>".print_r($modDetails,1)."</pre>"; exit;
//                            if ($model->validate()){
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    $success = false;
//                                    if($model->save()){
                                        $modDetails = $this->validasiTabularInput($_POST['ObatsupplierM'], $model);
                                        foreach ($modDetails as $i=>$data){
                                            if ($data->obatalkes_id > 0){
                                                if ($data->save()){
                                                    ObatalkesM::model()->updateAll(array('obatalkes_aktif'=>TRUE),'obatalkes_id>=:obatalkes_id',array(':obatalkes_id'=>$data->obatalkes_id));
                                                    $success = true;
                                                }
                                                else{
                                                  $success = false;
                                                }
                                            }
                                        }
                                    if ($success == true){
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->redirect(array('create'/*,'idSupplier'=>$model->supplier_id*/));
                                    }
                                    else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                    }
                                }
                                catch (Exception $ex){
                                     $transaction->rollback();
                                     Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex)); 
                                }
                                
//                            }
                        }
                        else{
                            $model->validate();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                        }
                        
		}
		$this->render('create',array(
			'model'=>$model,
                        'modSupplier'=>$modSupplier,
                        'modBarang'=>$modBarang,
                        'modDetails'=>$modDetails,
                        'modDetail'=>$modDetail,
		));
	}

         protected function validasiTabularInput($datas, $model){ 
            $valid = true;
            for ($i=0;$i<count($datas);$i++){
                $modDetail[$i] = new ObatsupplierM();
                $modDetail[$i]->attributes = $datas[$i];
                $valid = $modDetail[$i]->validate() && $valid;
//                echo "<pre>".print_r($datas[$i],1)."</pre>";
//                echo "<pre>".print_r($modDetail[$i]->getAttributes(),1)."</pre>";
//                exit;
            }
             
//            $this->_valid = $valid;
            return $modDetail;
        }
        
        protected function validasiTabularUpdate($datas){ 
            echo '<pre>';
//            echo print_r($datas);
//            exit();
              $valid = true;
            for ($i=0;$i<count($datas);$i++){
//                $datas[$i]->attributes = $model[$i];
//                echo print_r($datas[$i]->attributes);
                $modDetail[$i] = new ObatsupplierM();
                $modDetail[$i]->attributes = $datas[$i];
//                echo print_r($modDetail[$i]);
//                exit();
//                $modDetail[$i]->attributes = $datas[$i];
                $valid = $modDetail[$i]->validate() && $valid;
            }
            return $modDetail;
        }
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($supplier_id)
	{
            
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		//$model=RESupplierM::model()->findByAttributes(array('supplier_id'=>$supplier_id));

		$model=ObatsupplierM::model()->findAllByAttributes(array('supplier_id'=>$supplier_id));
                $modSupplier =RESupplierM::model()->findByPk($supplier_id);
                
                $modBarang = new ObatalkesM;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ObatsupplierM']))
		{
                        if (count($_POST['ObatsupplierM']) > 0){
                            $modDetails = $this->validasiTabularUpdate($_POST['ObatsupplierM']);
                           
//                            echo "<pre>".print_r($modDetails,1)."</pre>"; exit;
//                            if ($model->validate()){
                                $transaction = Yii::app()->db->beginTransaction();
                                try{
                                    $success = false;
//                                    if($model->save()){
                                        $modDetails = $this->validasiTabularUpdate($_POST['ObatsupplierM']);
                                        foreach ($modDetails as $i=>$data){
                                            ObatsupplierM::model()->deleteAllByAttributes(array('supplier_id'=>$data->supplier_id, 'obatalkes_id'=>$data->obatalkes_id));
                                            if ($data->obatalkes_id > 0){
                                                if ($data->save()){
                                                    ObatalkesM::model()->updateAll(array('obatalkes_aktif'=>TRUE),'obatalkes_id>=:obatalkes_id',array(':obatalkes_id'=>$data->obatalkes_id));
                                                    $success = true;
                                                }
                                                else{
                                                  $success = false;
                                                }
                                            }
                                        }
                                    if ($success == true){
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                        $this->redirect(array('admin'));
                                    }
                                    else{
                                        $transaction->rollback();
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                    }
                                }
                                catch (Exception $ex){
                                     $transaction->rollback();
                                     Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex)); 
                                }
                                
//                            }
                        }
                        else{
                            $model->validate();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                        }
		}
		$this->render('update',array(
			'model'=>$model,
                        'modSupplier'=>$modSupplier,
                        'modBarang'=>$modBarang,
                        'modDetails'=>$modDetails,
                        'modDetail'=>$modDetail,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($obatalkes_id, $supplier_id)
                {
                        $this->loadModel($obatalkes_id, $supplier_id)->delete();
                        if(!isset($_GET['ajax']))
                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                
        public function actionDeleteupdate($obatalkes_id, $supplier_id)
        {
                $this->loadModel($obatalkes_id, $supplier_id)->delete();
                if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('update','supplier_id'=>$supplier_id));
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('REBarangsupplierM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REBarangsupplierM('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REBarangsupplierM']))
			$model->attributes=$_GET['REBarangsupplierM'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($obatalkes_id, $supplier_id)
	{
               
                    $model=REBarangsupplierM::model()->findByAttributes(array('obatalkes_id'=>$obatalkes_id, 'supplier_id'=>$supplier_id));
               
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rebarang-supplier-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                REObatsupplierM::model()->updateByPk($id, array('satuanbesar_aktif'=>false));
//                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REBarangsupplierM;
            $model->attributes=$_REQUEST['REBarangsupplierM'];
            $judulLaporan='Data Barang Supplier';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
