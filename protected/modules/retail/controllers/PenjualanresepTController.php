<?php
class PenjualanresepTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'Create';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','bayar', 'informasi'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id=null,$antrian=null)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_PENJUALAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $modObat = new REProdukposV();
                if (isset($id)){
                    $model = REPenjualanresepT::model()->findByPk($id);
                    if (count($model) == 1){
                        $modDetails = ObatalkespasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
                    }
                    else{
                        unset($model);
                    }
                }else{
                    $modPasien = $this->defaultPasien();
                    $konfigFarmasi = KonfigfarmasiK::model()->find();

                    $model=new REPenjualanresepT;
                    $model->discount = 0;
                    $model->biayaadministrasi = 0;
                    $model->totalhargajual = 0;
                    $model->biayakonseling = 0;
                    $model->tglresep = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenjualan->tglresep, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
                    $model->tglpenjualan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenjualan->tglpenjualan, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
                    $model->jenispenjualan = 'PENJUALAN RETAIL';
//                    $model->noresep = Generator::noResep2('Z24');
                    $model->totaltarifservice = 0;
                    $modObatAlkes = new ObatalkespasienT('retail');
                }
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if((isset($_POST['ObatalkespasienT'])) && ($model->isNewRecord))
		{
                        $model->attributes = $_POST['REPenjualanresepT'];
                        $modDetails = $this->validasiTabular($_POST['ObatalkespasienT']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $modPenjualan = $this->savePenjualanResep($modPasien, $model->attributes, $konfigFarmasi);
                            $total = 0;
                            $jumlah = count($_POST['ObatalkespasienT']);
                            $counter = 1;
                            
                            if ($modPenjualan){
                                unset($modDetails);
                                foreach ($_POST['ObatalkespasienT'] as $i => $obatResep) {
                                    $modObatAlkes = new ObatalkespasienT('retail');
                                    $modDetails[$counter] = new ObatalkespasienT('retail');
                                    $modObatAlkes->attributes =$modPenjualan->attributes;
                                    $modObatAlkes->penjualanresep_id = $modPenjualan->penjualanresep_id;
                                    $modObatAlkes->pendaftaran_id = $modPenjualan->pendaftaran_id;
                                    $modObatAlkes->penjamin_id = $modPenjualan->penjamin_id;
                                    $modObatAlkes->carabayar_id = $modPenjualan->carabayar_id;
                                    $modObatAlkes->pasien_id = $modPenjualan->pasien_id;
                                    $modObatAlkes->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
//                                    $modObatAlkes->pegawai_id = $modPenjualan->pegawai_id;
                                    $modObatAlkes->qty_oa = $obatResep['qty_oa'];
                                    $modObatAlkes->harganetto_oa = $obatResep['harganetto_oa'];
                                    $modObatAlkes->hargasatuan_oa = $obatResep['hargajual_oa']/$obatResep['qty_oa'];
                                    $modObatAlkes->hargajual_oa = $obatResep['hargajual_oa'];
                                    $modObatAlkes->obatalkes_id = $obatResep['obatalkes_id'];
                                    $modObatAlkes->sumberdana_id = $obatResep['sumberdana_id'];
                                    $modObatAlkes->biayaadministrasi = 0;
                                    $modObatAlkes->biayakonseling = $modPenjualan->biayakonseling;
                                    $modObatAlkes->tglpelayanan = $modPenjualan->tglpenjualan;
                                    $modObatAlkes->discount = $obatResep['discount'];
                                    $modObatAlkes->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                                    $modObatAlkes->shift_id = Yii::app()->user->getState('shift_id');
                                    $modObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                                    $modObatAlkes->subsidiasuransi = 0;
                                    $modObatAlkes->subsidipemerintah = 0;
                                    $modObatAlkes->subsidirs = 0;
                                    $modObatAlkes->iurbiaya = 0;
                                    $modDetails[$counter]->attributes = $modObatAlkes->attributes;

                                    if($modDetails[$counter]->validate()){
                                        if ($modDetails[$counter]->save()){
                                            StokobatalkesT::kurangiStok($modDetails[$counter]->qty_oa, $modDetails[$counter]->obatalkes_id);
                                            $total++;
                                        }
                                    }
                                    $counter++;
                                }
                            }
                            
                            
                            if($modPenjualan && ($total == $jumlah)){
                                
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                if (isset($_POST["antrian"])){
                                    $this->redirect(array('create', 'id'=>$modPenjualan->penjualanresep_id));
                                }else{
                                    $this->redirect(array('create', 'id'=>$modPenjualan->penjualanresep_id, 'antrian'=>1));
                                }
//                                $this->refresh();
                            } else {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                            }
                            
                        }
                        catch (Exception $exc){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        }
		}

		$this->render('create',array(
			'model'=>$model, 'konfigFarmasi'=>$konfigFarmasi, 'modObat'=>$modObat, 'modDetails'=>$modDetails, 'modObatAlkes'=>$modObatAlkes,'antrian'=>$antrian
		));
	}
        
        protected function cekStatusBayar($sisaTagihan)
        {
            if($sisaTagihan>0){
                return 'BELUM LUNAS';
            } else {
                return 'LUNAS';
            }
        }
        
        protected function validasiTabular($obat){
            foreach ($obat as $i=>$v){
                $i = $v["no"];
                $modDetails[$i] = new ObatalkespasienT();
                $modDetails[$i]->attributes = $v;
            }
            return $modDetails;

        }
        
        public function actionInformasi(){
             //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $model = new REAntrianbayarposV('search');
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:59');
            if (isset($_GET['REAntrianbayarposV'])){
                $model->attributes = $_GET['REAntrianbayarposV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
            }
            $this->render('informasi', array("model"=>$model));
        }
        
        public function fungsiPembulatan($harga){
            $pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanretail;
            $pembulatan = $harga % $pembulatanHarga;
            
            return $pembulatan;
        }
        
        public function actionBayar($id,$url=null,$frame=null){
            if (!is_null($frame)){
                $this->layout='//layouts/frameDialog';
            }
            
            if(isset($_POST['TandabuktibayarT'])) {

                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modTandaBukti = new TandabuktibayarT;
                    $modTandaBukti->attributes = $_POST['TandabuktibayarT'];
                    if($modTandaBukti->carapembayaran == 'HUTANG'){
                        $modTandaBukti->uangditerima = 0;
                    }
                    $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                    $modTandaBukti->nourutkasir = Generator::noUrutKasir($modTandaBukti->ruangan_id);
                    $modTandaBukti->nobuktibayar = Generator::noBuktiBayar();
                    $modTandaBukti->alamat_bkm = Params::DEFAULT_ALAMAT_BKM;
                    $modTandaBukti->darinama_bkm = Params::DEFAULT_DARINAMA_BKM;
                    $modTandaBukti->sebagaipembayaran_bkm = Params::DEFAULT_SEBAGAIPEMBAYARAN_BKM;
                    
                    if ($modTandaBukti->save()){
                        $pembayaran = $this->savePembayaranPelayanan($modTandaBukti);
                        foreach($_POST['pembayaranAlkes'] as $i=>$item){
                            $model[$i] = new OasudahbayarT;
                            $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                            $model[$i]->obatalkespasien_id = $item['obatalkespasien_id'];
                            $model[$i]->pembayaranpelayanan_id = $pembayaran->pembayaranpelayanan_id;
                            $model[$i]->obatalkes_id = $item['obatalkes_id'];
                            $model[$i]->qty_oa = $item['qty_oa'];
                            $model[$i]->jmlsubsidi_asuransi = 0;
                            $model[$i]->jmlsubsidi_pemerintah = 0;
                            $model[$i]->jmlsubsidi_rs = 0;
                            $model[$i]->jmlbayar_oa = $item['sub_total'];
                            $model[$i]->jmlsisabayar_oa = 0;
                            $model[$i]->hargasatuan = $item['hargajual_oa'];

                            //$model[$i]->oasudahbayar_id = $model[$i]->getOldPrimaryKey() + 1;
                            if($model[$i]->save()){
                                $this->updateObatAlkesPasienT($model[$i]);
                            }
                            
                        }
                    }
                    $modObat = ObatalkespasienT::model()->findByPk($model[0]->obatalkespasien_id);
                    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print', array("caraPrint"=>"PRINT", 'idPenjualan'=>$modObat->penjualanresep_id, 'idTandaBukti'=>$modTandaBukti->tandabuktibayar_id));
                    
                    if($pembayaran){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        if (!empty($frame)){
//                            Yii::app()->user->setFlash('url',"$url");
                            echo '<iframe src='.$url.' style="height:0px;"></iframe>';
//                            echo '<script>parent.location.href="'.Yii::app()->createUrl($this->module->id."/".$this->id.'/create').'";</script>';
                            echo '<script>setTimeout(function(){parent.location.href="'.Yii::app()->createUrl($this->module->id."/".$this->id.'/create').'";},3600);</script>';
                            exit();
                        }else{
                            Yii::app()->user->setFlash('url',"$url");
                            $this->refresh();
                        }
                    } else { 
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }
            
            $criteriaoa = new CDbCriteria;
            $penjualan = PenjualanresepT::model()->findByPk($id);
            if (count($penjualan) == 1){
                $criteriaoa->compare('penjualanresep_id', $id);
                $criteriaoa->addCondition('oasudahbayar_id IS NULL');
                $modObatalkes = ObatalkespasienT::model()->findAll($criteriaoa);
                if (count($modObatalkes) > 0){
                   $modPasien = PasienM::model()->findByPk($modObatalkes[0]->pasien_id);
                } else {
                    $modPasien = new PasienM;
                    $sudahBayar = true;
                }
            }
            
            if($tandaBukti->tandabuktibayar_id != null){
                $modTandaBukti = $tandaBukti;
            } else if($sudahBayar){
                $penjualanResep = PenjualanresepT::model()->findByPk($id);
                $criteriaoa = new CDbCriteria;
                $criteriaoa->compare('penjualanresep_id', $id);
                $criteriaoa->addCondition('oasudahbayar_id IS NOT NULL');
                $modObatalkes = ObatalkesPasienT::model()->findAll($criteriaoa);
                $modOaSudahBayar = OasudahbayarT::model()->findByPk($modObatalkes[0]->oasudahbayar_id);
                $pembayaran = PembayaranpelayananT::model()->findByPk($modOaSudahBayar->pembayaranpelayanan_id);
                $modTandaBukti = TandabuktibayarT::model()->findByPk($pembayaran->tandabuktibayar_id);
                
            } else {
                $modTandaBukti = new TandabuktibayarT;
                $modTandaBukti->tglbuktibayar =  date('d M Y H:i:s');
                $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
                $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
            } 
            
            $this->render('_bayar',array('modPasien'=>$modPasien,
                                        'modObatalkes'=>$modObatalkes,
                                        'modTandaBukti'=>$modTandaBukti,
                                        'tandaBukti'=>$tandaBukti,
                                        'successSave'=>$successSave,
                                        'url'=>$url,
                                        'sudahBayar'=>$sudahBayar));
        }
        
        protected function defaultPasien()
        {
            $pasien = PasienM::model()->findByPk(Params::DEFAULT_PASIEN_ID_RETAIL);
            
            return $pasien;
        }
        
        protected function savePembayaranPelayanan($tandaBukti)
        {
                //$idPendaftaran = $_POST['FAPendaftaranT']['pendaftaran_id'];
                $pembayaran = new PembayaranpelayananT;
                $pembayaran->carabayar_id = Params::DEFAULT_CARABAYAR;
                $pembayaran->penjamin_id = Params::DEFAULT_PENJAMIN;
                $pembayaran->pasien_id = Params::DEFAULT_PASIEN_ID_RETAIL;
                //$pembayaran->pendaftaran_id = $idPendaftaran;
                $pembayaran->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $pembayaran->tglpembayaran = $_POST['TandabuktibayarT']['tglbuktibayar'];
                $pembayaran->ruanganpelakhir_id = Yii::app()->user->getState('ruangan_id');
                $pembayaran->totalbiayaoa = $tandaBukti->jmlpembayaran;
                $pembayaran->totalbiayatindakan = 0;
                
                $pembayaran->totalbiayapelayanan = $tandaBukti->jmlpembayaran;
                $pembayaran->totalsubsidiasuransi = 0;
                $pembayaran->totalsubsidipemerintah = 0;
                $pembayaran->totalsubsidirs = 0;
                $pembayaran->totaliurbiaya = 0;
                
                $pembayaran->totalbayartindakan = $tandaBukti->jmlpembayaran;
                $pembayaran->totaldiscount = 0;
                $pembayaran->totalpembebasan = 0;
                $pembayaran->totalsisatagihan = 0;
                $pembayaran->statusbayar = $this->cekStatusBayar($pembayaran->totalsisatagihan);
                $pembayaran->nopembayaran = Generator::noResep2('Z24');
                $pembayaran->tandabuktibayar_id = $tandaBukti->tandabuktibayar_id;
                
                if($pembayaran->save()){
                    TandabuktibayarT::model()->updateByPk($tandaBukti->tandabuktibayar_id, array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id));
                } 
                else{
                    throw new Exception(MyFunction::exceptionMessage($pembayaran));
                }
                
                return $pembayaran;
        }
        
        protected function savePenjualanResep($modPasien,$penjualanResep, $konfigFarmasi)
        {
            $modPenjualan = new REPenjualanresepT;
            $modPenjualan->attributes = $penjualanResep;
            $modPenjualan->penjamin_id = Params::DEFAULT_PENJAMIN;
            $modPenjualan->carabayar_id = Params::DEFAULT_CARABAYAR;
            $modPenjualan->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $modPenjualan->pasien_id = $modPasien->pasien_id;
            $modPenjualan->tglresep = date('d M Y H:i:s');
            $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
            $modPenjualan->ruanganasal_nama = $ruanganAsal->ruangan_nama;
            $modPenjualan->instalasiasal_nama = $ruanganAsal->instalasi->instalasi_nama;
            $modPenjualan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modPenjualan->pembulatanharga = $konfigFarmasi->pembulatanretail;
            $modPenjualan->subsidiasuransi = 0;
            $modPenjualan->subsidipemerintah = 0;
            $modPenjualan->subsidirs = 0;
            $modPenjualan->iurbiaya = 0;
            $modPenjualan->shift_id = Yii::app()->user->getState('shift_id');
            if($modPenjualan->validate()){
                if ($modPenjualan->save()){
                    
                }
            } else {
                throw new Exception(MyFunction::exceptionMessage($modPenjualan));
            }
            
            return $modPenjualan;
        }
        
        protected function saveOaSudahBayar($modPembayaranPelayanan,$postPembayaranOa,$modTandaBukti)
        {
            foreach($postPembayaranOa as $i=>$item){
                $model[$i] = new OasudahbayarT;
                $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $model[$i]->obatalkespasien_id = $item['obatalkespasien_id'];
                $model[$i]->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
                $model[$i]->obatalkes_id = $item['obatalkes_id'];
                $model[$i]->qty_oa = $item['qty_oa'];
                $model[$i]->jmlsubsidi_asuransi = 0;
                $model[$i]->jmlsubsidi_pemerintah = 0;
                $model[$i]->jmlsubsidi_rs = 0;
                $model[$i]->jmliurbiaya = 0;
                $model[$i]->jmlbayar_oa = $item['sub_total'] / $_POST['totalbayar_oa'] * $modTandaBukti->jmlpembayaran;
                $model[$i]->jmlsisabayar_oa = $item['sub_total'] - $model[$i]->jmlbayar_oa;
                $model[$i]->hargasatuan = 0;
                
                //$model[$i]->oasudahbayar_id = $model[$i]->getOldPrimaryKey() + 1;
                if($model[$i]->validate()){
                    //echo "saveOaSudahBayar valid <br/>";
                    //echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                    $model[$i]->save();
                    $this->updateObatAlkesPasienT($model[$i]);
                } else {
                    
                }
                
                return $model[0];
            }
        }

        protected function updateObatAlkesPasienT($modOa)
        {
            ObatalkespasienT::model()->updateByPk($modOa->obatalkespasien_id, array('subsidiasuransi'=>$modOa->jmlsubsidi_asuransi,
                                                                                    'subsidipemerintah'=>$modOa->jmlsubsidi_pemerintah,
                                                                                    'subsidirs'=>$modOa->jmlsubsidi_rs,
                                                                                    'iurbiaya'=>$modOa->jmliurbiaya,
                                                                                    'oasudahbayar_id'=>$modOa->oasudahbayar_id));
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REPenjualanresepT']))
		{
			$model->attributes=$_POST['REPenjualanresepT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->penjualanresep_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('REPenjualanresepT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new REPenjualanresepT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['REPenjualanresepT']))
			$model->attributes=$_GET['REPenjualanresepT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REPenjualanresepT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='repenjualanresep-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REPenjualanresepT;

            $model->attributes=$_REQUEST['REPenjualanresepT'];
            $judulLaporan='';
            $modObat = ObatalkespasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$_REQUEST['idPenjualan']));
            $modTandaBukti = TandabuktibayarT::model()->findByPk($_REQUEST['idTandaBukti']);
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printKartu';
                $this->render('Print',array('modObat'=>$modObat, 'modTanda'=>$modTandaBukti,'model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
