<?php

class PermintaanPembelianController extends SBaseController
{
	public function actionIndex()
	{
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_GUDANG_RETAIL)){
                throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
//                }

                $modPermintaanPembelian = new REPermintaanpembalianT;
                $modPermintaanPembelian->alamatpengiriman = Yii::app()->user->getState('alamatlokasi_rumahsakit');
                $modPermintaanPembelian->nopermintaan=Generator::noPembelian();
                $modPermintaanPembelian->tglpermintaanpembelian=date('Y-m-d H:i:s');
                $modPermintaanPembelian->tglterimabarang = date('Y-m-d',strtotime('+3 day')).' 00:00:00';
                $format = new CustomFormat();
                $modPermintaanDetail = new REPermintaandetailT;
                
                if(isset($_POST['REPermintaanpembelianT'])) {
                    $modPermintaanPembelian->attributes = $_POST['REPermintaanpembelianT'];
                    $modPermintaanPembelian->pegawai_id = Yii::app()->user->id;
                    $modPermintaanPembelian->instalasi_id=Yii::app()->user->getState('instalasi_id');
                    $modPermintaanPembelian->ruangan_id=Yii::app()->user->getState('ruangan_id');
                    $modPermintaanPembelian->tglpermintaanpembelian=$format->formatDateTimeMediumForDB($_POST['REPermintaanpembelianT']['tglpermintaanpembelian']);
                    $modPermintaanPembelian->tglterimabarang=$format->formatDateTimeMediumForDB($_POST['REPermintaanpembelianT']['tglterimabarang']);
                    if(!empty($modPermintaanPembelian->supplier_id)) {
                        $supplier = SupplierM::model()->findByPk($modPermintaanPembelian->supplier_id);
                        $modPermintaanPembelian->supplierCode = $supplier->supplier_kode;
                        $modPermintaanPembelian->supplierName = $supplier->supplier_nama;
                    }
                    $transaction = Yii::app()->db->beginTransaction();
                    try { 
                        if($modPermintaanPembelian->validate()) {
                                $modPermintaanPembelian->save();
                                $jumlahBrg=COUNT($_POST['permintaan']);
                                $jumlahCek=0;
                                for($i=0; $i<$jumlahBrg; $i++):
                                          $modPermintaanDetail=new REPermintaandetailT;
                                          $modPermintaanDetail->obatalkes_id=$_POST['permintaan'][$i]['brg_id'];
                                          $modPermintaanDetail->satuankecil_id=$_POST['permintaan'][$i]['satkecil'];
                                          $modPermintaanDetail->sumberdana_id=$_POST['permintaan'][$i]['sumberdana_id'];
                                          $modPermintaanDetail->permintaanpembelian_id=$modPermintaanPembelian->permintaanpembelian_id;
                                          $modPermintaanDetail->satuanbesar_id=$_POST['permintaan'][$i]['satbesar'];
                                          $modPermintaanDetail->stokakhir=$_POST['permintaan'][$i]['stokakhir'];
                                          $modPermintaanDetail->maksimalstok=$_POST['permintaan'][$i]['maksimalstok'];
                                          $modPermintaanDetail->minimalstok=$_POST['permintaan'][$i]['minimalstok'];
                                          $modPermintaanDetail->jmlpermintaan=$_POST['permintaan'][$i]['qty'];
                                          $modPermintaanDetail->persendiscount=$_POST['permintaan'][$i]['discount'];
                                          $modPermintaanDetail->jmldiscount=$_POST['permintaan'][$i]['jmldiscount'];
                                          $modPermintaanDetail->harganettoper=$_POST['permintaan'][$i]['ponet'];
                                          $modPermintaanDetail->hargappnper=$_POST['permintaan'][$i]['povat'];
                                          $modPermintaanDetail->hargapphper=0; //$_POST['permintaan'][$i]['hargapphper'];
                                          //$modPermintaanDetail->hargasatuanper=($modPermintaanDetail->harganettoper + $modPermintaanDetail->hargappnper + $modPermintaanDetail->hargapphper ) - $modPermintaanDetail->jmldiscount;
                                          $modPermintaanDetail->hargasatuanper = $_POST['permintaan'][$i]['prize'];
                                          $modPermintaanDetail->jmlkemasan=$_POST['permintaan'][$i]['jmldlmkemasan'];
                                          $modPermintaanDetail->biaya_lainlain=$_POST['permintaan'][$i]['othercost'];

                                          if(!empty($_POST['permintaan'][$i]['tglkadaluarsa'])){
                                            $modPermintaanDetail->tglkadaluarsa=$format->formatDateMediumForDB($_POST['permintaan'][$i]['tglkadaluarsa']);
                                          }else{
                                              $modPermintaanDetail->tglkadaluarsa=null;
                                          }
                                          $modPermintaanDetail->validate();
                                          if($modPermintaanDetail->save()){
                                              $jumlahCek++; 
                                          }
                                endfor;
                                if($jumlahCek==$jumlahBrg){
                                    $transaction->commit();
                                    Yii::app()->user->setFlash('success',"Data Berhasil Disimpan");
                                }else{
                                    $transaction->rollback();
                                    Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                }
                        }
                    } catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    } 
                }
		
                $this->render('index',array('modPermintaanPembelian'=>$modPermintaanPembelian,'modPermintaanDetail'=>$modPermintaanDetail));
	}
        
        public function actionAutocompleteSupplierCode()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $returnVal = array();
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(supplier_kode)', strtolower($_GET['term']), true);
                $criteria->compare('LOWER(supplier_jenis)', 'retail');
                $criteria->order = 'supplier_kode';
                $criteria->limit=10;
                $models = SupplierM::model()->findAll($criteria);
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $returnVal[$i]['label'] = $model->supplier_kode;
                    $returnVal[$i]['value'] = $model->supplier_kode;
                }

                echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
        }
        
        public function actionAutocompleteSupplierName()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $returnVal = array();
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(supplier_nama)', strtolower($_GET['term']), true);
                $criteria->compare('LOWER(supplier_jenis)', 'retail');
                $criteria->order = 'supplier_nama';
                $criteria->limit=10;
                $models = SupplierM::model()->findAll($criteria);
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                    }
                    $returnVal[$i]['label'] = $model->supplier_nama;
                    $returnVal[$i]['value'] = $model->supplier_nama;
                }

                echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
        }
        
        public function actionAutocompleteStockCode()
        {
            /* codingan lama
            if(Yii::app()->request->isAjaxRequest) { 
                $criteria = new CDbCriteria();
                $criteria->compare('LOWER(stock_code)', strtolower($_GET['term']), true);
                $criteria->order = 'stock_code';
                $criteria->limit=10;
                $models = ProdukposV::model()->findAll($criteria);
                foreach($models as $i=>$model)
                {
                    $attributes = $model->attributeNames();
                    foreach($attributes as $j=>$attribute) {

                        $returnVal[$i]['label'] = $model->stock_code;
                        $returnVal[$i]['value'] = $model->stock_code;
                        $returnVal[$i]["$attribute"] = $model->$attribute;
                        $stoks = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$model->brg_id));
                        $stokAkhir = 0; $maxStok = 0;
                        foreach($stoks as $k=>$stok){
                            $stokAkhir = $stokAkhir + $stok->qtystok_in;
                            $maxStok = $maxStok + $stok->qtystok_out;
                        }
                        $returnVal[$i]['maksimalstok'] = $maxStok;
                        $returnVal[$i]['stokakhir'] = $stokAkhir;
                    }
                }

                echo CJSON::encode($returnVal);
            }
            Yii::app()->end();*/
            if(Yii::app()->request->isAjaxRequest) { 
                if(!empty($_GET['supplier_id'])){
                    $returnVal = array();
                    $idSupplier = $_GET['supplier_id'];
                    $criteria = new CDbCriteria();
                    $criteria->with = array('obatalkes');
                    $criteria->compare('LOWER(obatalkes.obatalkes_kode)', strtolower($_GET['term']), true);
                    $criteria->compare('obatalkes.obatalkes_farmasi','false');
                    $criteria->compare('supplier_id',$idSupplier);
                    $criteria->order = 'obatalkes_kode';
                    $criteria->limit=10;
                    $models = ObatsupplierM::model()->findAll($criteria);
                    foreach($models as $i=>$model)
                    {
                        $attributes = $model->attributeNames();
                        foreach($attributes as $j=>$attribute) {
                            $returnVal[$i]["$attribute"] = $model->$attribute;
                            $obatAttributes = $model->obatalkes->attributeNames();
                            foreach($obatAttributes as $o=>$attribute) {
                                $returnVal[$i]["$attribute"] = $model->obatalkes->$attribute;
                            }
                        }
                        $returnVal[$i]['label'] = $model->obatalkes->obatalkes_nama;
                        $returnVal[$i]['value'] = $model->obatalkes->obatalkes_nama;                            
                        $stoks = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$model->obatalkes_id));
                        $stokAkhir = 0; $maxStok = 0;
                        foreach($stoks as $k=>$stok){
                            $stokAkhir = $stokAkhir + $stok->qtystok_in;
                            $maxStok = $maxStok + $stok->qtystok_out;
                        }
                        $returnVal[$i]['maksimalstok'] = $maxStok;
                        $returnVal[$i]['stokakhir'] = $stokAkhir;
                    }

                    $returnVal[0]['error'] = '';
                    echo CJSON::encode($returnVal);
                } else {
                    $returnVal[0]['error'] = 'Supplier masih kosong';
                    echo CJSON::encode($returnVal);
                }
            }
            Yii::app()->end();
        }
        
        public function actionAutocompleteStockName()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                if(!empty($_GET['supplier_id'])){
                    $returnVal = array();
                    $idSupplier = $_GET['supplier_id'];
                    $criteria = new CDbCriteria();
                    $criteria->with = array('obatalkes');
                    $criteria->compare('LOWER(obatalkes.obatalkes_nama)', strtolower($_GET['term']), true);
                    $criteria->compare('obatalkes.obatalkes_farmasi','false');
                    $criteria->compare('supplier_id',$idSupplier);
                    $criteria->order = 'obatalkes_nama';
                    $criteria->limit=10;
                    $models = ObatsupplierM::model()->findAll($criteria);
                    foreach($models as $i=>$model)
                    {
                        $attributes = $model->attributeNames();
                        foreach($attributes as $j=>$attribute) {
                            $returnVal[$i]["$attribute"] = $model->$attribute;
                            $obatAttributes = $model->obatalkes->attributeNames();
                            foreach($obatAttributes as $o=>$attribute) {
                                $returnVal[$i]["$attribute"] = $model->obatalkes->$attribute;
                            }
                        }
                        $returnVal[$i]['label'] = $model->obatalkes->obatalkes_nama;
                        $returnVal[$i]['value'] = $model->obatalkes->obatalkes_nama;                            
                        $stoks = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$model->obatalkes_id));
                        $stokAkhir = 0; $maxStok = 0;
                        foreach($stoks as $k=>$stok){
                            $stokAkhir = $stokAkhir + $stok->qtystok_in;
                            $maxStok = $maxStok + $stok->qtystok_out;
                        }
                        $returnVal[$i]['maksimalstok'] = $maxStok;
                        $returnVal[$i]['stokakhir'] = $stokAkhir;
                    }

                    $returnVal[0]['error'] = '';
                    echo CJSON::encode($returnVal);
                } else {
                    $returnVal[0]['error'] = 'Supplier masih kosong';
                    echo CJSON::encode($returnVal);
                }
            }
            Yii::app()->end();
        }
        
        public function actionAddBarangFromDialog()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                if(!empty($_GET['supplier_id'])){
                    $returnVal = array();
                    $idSupplier = $_GET['supplier_id'];
                    $idBarang = $_GET['obatalkes_id'];
                    $criteria = new CDbCriteria();
                    $criteria->with = array('obatalkes');
                    $criteria->compare('t.obatalkes_id', $idBarang);
                    $criteria->compare('obatalkes.obatalkes_farmasi','false');
                    $criteria->compare('supplier_id',$idSupplier);
                    $criteria->order = 'obatalkes_nama';
                    $criteria->limit=10;
                    $models = ObatsupplierM::model()->findAll($criteria);
                    foreach($models as $i=>$model)
                    {
                        $attributes = $model->attributeNames();
                        foreach($attributes as $j=>$attribute) {
                            $returnVal["$attribute"] = $model->$attribute;
                            $obatAttributes = $model->obatalkes->attributeNames();
                            foreach($obatAttributes as $o=>$attribute) {
                                $returnVal["$attribute"] = $model->obatalkes->$attribute;
                            }
                        }
                        $returnVal['label'] = $model->obatalkes->obatalkes_nama;
                        $returnVal['value'] = $model->obatalkes->obatalkes_nama;                            
                        $stoks = StokobatalkesT::model()->findAllByAttributes(array('obatalkes_id'=>$model->obatalkes_id));
                        $stokAkhir = 0; $maxStok = 0;
                        foreach($stoks as $k=>$stok){
                            $stokAkhir = $stokAkhir + $stok->qtystok_in;
                            $maxStok = $maxStok + $stok->qtystok_out;
                        }
                        $returnVal['maksimalstok'] = $maxStok;
                        $returnVal['stokakhir'] = $stokAkhir;                        
                    }

                    $returnVal['error'] = '';
                    echo CJSON::encode($returnVal);
                } else {
                    $returnVal['error'] = 'Supplier masih kosong';
                    echo CJSON::encode($returnVal);
                }
            }
            Yii::app()->end();
        }
        
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}