
<?php

class StokOpnameTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'informasi', 'details'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','print','stokopname'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','RemoveTemporary','stokopname'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionStokopname($id = null)
	{
               //if(!Yii::app()->user->checkAccess(Params::DEFAULT_GUDANG_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                
		$model=new REStokOpnameT;
                $model->totalharga = 0;
                $model->totalnetto = 0;
                $model->tglstokopname = date('Y-m-d');
                $model->nostokopname = Generator::noStokOpname();
                $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modObat = new REObatalkesM();
                $modObat->obatalkes_id = 0 ;
                $modProduk = new REProdukposV();
                $modProduk->brg_id = 0;
                if (isset($_GET['id'])){
                    $id = $_GET['id'];
                    $modFormulir = FormuliropnameR::model()->find('formuliropname_id ='.$id.' and stokopname_id is null');
                    if (count($modFormulir) == 1){
                        $model->formuliropname_id = $modFormulir->formuliropname_id;
                        $modDetailFormulir = FormstokopnameR::model()->findAll('formuliropname_id = '.$modFormulir->formuliropname_id.' and stokopnamedet_id is null');
                        $modProduk->brg_id = CHtml::listData($modDetailFormulir, 'obatalkes_id', 'obatalkes_id');
                    }
                }
                $model->petugas1_id = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai_id;
                $model->petugas1_nama = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['REStokOpnameT']))
		{
                    $model->attributes=$_POST['REStokOpnameT'];
                    ($model->jenisstokopname == Params::JENIS_STOKOPNAME_PENYESUAIAN) ? $model->isstokawal = FALSE :$model->isstokawal = true ;

                    if (count($_POST['REProdukposV']) > 0){
                        $modProduk->brg_id = $this->sortPilih($_POST['REProdukposV']);
                        $modDetails = $this->validasiTabular($_POST['REProdukposV']);
                    }
                    if ($model->validate()){
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $hasil = 0;
                            if($model->save()){
                                if (count($modDetails) > 0){
                                    $jumlah = count($modDetails);
                                    foreach ($modDetails as $i => $v) {
                                        if (!empty($model->formuliropname_id)){
                                            FormuliropnameR::model()->updateByPk($model->formuliropname_id, array('stokopname_id'=>$model->stokopname_id));
                                            $v->formstokopname_id = FormstokopnameR::model()->find('formuliropname_id = '.$model->formuliropname_id.' and obatalkes_id = '.$v->obatalkes_id)->formstokopname_id;
                                        }
                                        $v->stokopname_id = $model->stokopname_id;
                                        if ($v->save()){
                                            if (!empty($v->formstokopname_id)){
                                                FormstokopnameR::model()->updateByPk($v->formstokopname_id, array('stokopnamedet_id'=>$v->stokopnamedet_id));
                                            }
                                            if ($model->jenisstokopname != Params::JENIS_STOKOPNAME_PENYESUAIAN){
                                                $this->tambahStok($v,$model,$v->volume_fisik);
                                            }
                                            else{
                                                $selisih = $v->volume_fisik - $v->volume_sistem;
                                                if ($selisih > 0){

                                                    if (!$this->tambahStok($v, $model, $selisih)){
                                                        exit();
                                                    }
                                                }
                                                else {
                                                    StokobatalkesT::kurangiStok(abs($selisih), $v->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                                                }
                                            }
                                            $hasil++;
                                        }
//                                       echo print_r($v->getErrors()) ;
                                    }
                                }
                            }

                            if(($hasil>0)&&($hasil == $jumlah)){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                                $this->redirect(array('index'));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data Gagal Disimpan ");
                            }
                        }catch(Exception $ex){
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan.'.MyExceptionMessage::getMessage($ex, true));
                        }
                    }
		}
                
                if(isset($_GET['REProdukposV']))
		{
                      
			$modProduk->attributes=$_GET['REProdukposV'];			
		}

		$this->render('index',array(
			'model'=>$model, 'modObat'=>$modObat, 'modDetails'=>$modDetails, 'modFormulir'=>$modFormulir, 'modProduk'=>$modProduk
		));
	}
        
        protected function tambahStok($data, $model, $selisih){
            $stokObat = new StokobatalkesT();
            $stokObat->obatalkes_id = $data->obatalkes_id;
            $stokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $stokObat->tglstok_in = date('Y-m-d H:i:s');
            $stokObat->qtystok_in = $selisih;
            $stokObat->qtystok_out = 0;
            $stokObat->qtystok_current = $selisih;
            $stokObat->satuankecil_id = $data->satuankecil_id;
            $stokObat->hargajual_oa = $data->hargasatuan;
            $stokObat->harganetto_oa = $data->harganetto;
            $stokObat->sumberdana_id = $data->sumberdana_id;
            
            if ($stokObat->save()){
            
                return true;
            }
//            echo print_r($stokObat->getErrors());
        }
        
        protected function sortPilih($data){
            $result = array();
            foreach ($data as $i=>$row){
                if ($row['cekList'] == 1){
                    $result[] = $row['brg_id'];
                }
            }
            
            return $result;
        }
        
        protected function validasiTabular($data){
            foreach ($data as $i=>$row){
//                echo '<pre>';
                $obat = ProdukposV::model()->findByAttributes(array('brg_id'=>$row['brg_id']));

                $modDetails[$i] = new StokopnamedetT();
//                $modDetails[$i]->attributes = $obat->attributes;
//                $modDetails[$i]->attributes = $row;
                
                $modDetails[$i]->volume_fisik = $row['volume_fisik'];
                $modDetails[$i]->volume_sistem = $row['volume_sistem'];
                $modDetails[$i]->obatalkes_id = $row['brg_id'];
                $modDetails[$i]->kondisibarang = $row['kondisibarang'];
                $modDetails[$i]->hargasatuan = $obat->hargajual;
                $modDetails[$i]->harganetto = $obat->harganetto;
                $modDetails[$i]->jumlahharga = $modDetails[$i]->hargasatuan*$modDetails[$i]->volume_fisik;
                $modDetails[$i]->jumlahnetto = $modDetails[$i]->harganetto*$modDetails[$i]->volume_fisik;
                $modDetails[$i]->satuankecil_id = $obat->satuankecil_id;
                $modDetails[$i]->sumberdana_id = $obat->sumberdana_id;
                
//                echo 
                
                $modDetails[$i]->validate();
//                echo print_r($modDetails[$i]->attributes);
            }
//            exit();
            return $modDetails;
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=REStokOpnameT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
        public function actionInformasi(){
            $model = new REStokOpnameT('search');
            $model->tglstokopname = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
            if (isset($_GET['REStokOpnameT'])){
                $format = new CustomFormat();
                $model->attributes = $_GET['REClosingkasirT'];
                $model->tglstokopname = $format->formatDateTimeMediumForDB($_GET['REStokOpnameT']['tglstokopname']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['REStokOpnameT']['tglAkhir']);

                $model->nostokopname = $_GET['REStokOpnameT']['nostokopname'];
                $model->mengetahui_id = $_GET['REStokOpnameT']['mengetahui_id'];
            }
            $this->render('informasi', array('model'=>$model));
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='restokopname-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new REStokOpnameT;
            $model->attributes=$_REQUEST['REStokOpnameT'];
            $judulLaporan='Data Stock Opname';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionDetails($id){
            $this->layout = '//layouts/frameDialog';
            $model = StokopnameT::model()->findByPk($id);
//            $modDetails = StokopnamedetT::model()->findAllByAttributes(array('stokopname_id'=>$model->stokopname_id));
            $modTable = new REStokopnamedetT('search');
            $modTable->stokopname_id = $model->stokopname_id;
//            $modObat->obatalkes_id = CHtml::listData($modDetails, 'obatalkes_id', 'obatalkes_id');
            $this->render('details', array('model'=>$model, 'modDetails'=>$modDetails, 'modTable'=>$modTable));
        }
}
