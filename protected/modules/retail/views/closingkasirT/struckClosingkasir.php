<?php $this->renderPartial('application.views.headerReport.headerDefaultStruk'); ?>
<table  width="100%" border="1">
    <tr align="center">
        <td rowspan="2" colspan="2"><b>SALES REPORT</b></td>
    </tr>
    <tr></tr>
    <tr>
        <td width="50%">REPORT AREA :</td>
        <td width="50%"># <?=RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama?></td>
    </tr>
    <tr>
        <td width="50%">DATE RANGE:</td>
        <td width="50%"><?php echo DATE($model->closingdari); ?> UNTIL <?php echo DATE($model->sampaidengan); ?></td>
    </tr>
    <tr>
        <td width="50%">DAYS IN RANGE:</td>
        <td width="50%"><?php echo $lamahari; ?> DAYS</td>
    </tr>
    <tr>
        <td width="50%">SALES REPORT:</td>
        <td width="50%">SALES REPORT</td>
    </tr>
    <tr>
        <td width="50%">EXPORT DATE:</td>
        <td width="50%"><?php echo $model->tglclosingkasir; ?></td>
    </tr>
</table>
<br/><br/>
<table border="1" width="100%">
    <thead align="center">
        <tr align="center">
            <th rowspan="2">START</th>
            <th rowspan="2">SHIFT</th>
            <th colspan="2">TOTAL SALES DETAILS</th>
        </tr>
        <tr align="center">
            <th>TRANSAKSI</th>
            <th>VALUE</th>
        </tr>
    </thead>
    <tbody>
        <tr align="center">
            <td width="15%"><?php echo $model->closingdari; ?></td>
            <td width="15%"><?php echo strtolower($model->shift->shift_nama); ?> </td>
            <td width="15%"><?php echo $model->tottransaksi; ?> </td>
            <td width="15%"><?php echo MyFunction::formatNumber($model->nilaiclosingtrans); ?></td>
        </tr>
         <tr align="center">
            <td><?php echo $model->sampaidengan; ?></td>
            <td>TOTAL</td>
            <td><?php echo $model->tottransaksi; ?> </td>
            <td><?php echo MyFunction::formatNumber($model->nilaiclosingtrans); ?></td>
        </tr>
    </tbody>
</table>
<table width="100%">
        <tr align="center">
            <td rowspan="2" width="50%">CREATED BY<br><br><br><br><?php echo $model->pegawai->nama_pegawai; ?></th>
            <td rowspan="2"><br><br><br><br></th>
            <td colspan="2" width="50%">APPROVED BY<br><br><br><br><br></th>
        </tr>
</table>