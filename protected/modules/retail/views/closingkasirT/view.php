<?php
$this->breadcrumbs=array(
	'Closingkasir Ts'=>array('index'),
	$model->closingkasir_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Closing Kasir', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' ClosingkasirT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' ClosingkasirT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' ClosingkasirT', 'icon'=>'pencil','url'=>array('update','id'=>$model->closingkasir_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' ClosingkasirT','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->closingkasir_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Closing Kasir', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'closingkasir_id',
		'tandabuktibayar_id',
		'shift_id',
		'tglclosingkasir',
		'closingdari',
		'sampaidengan',
		'nilaiclosingtrans',
		'closingsaldoawal',
		'jmluanglogam',
		'jmluangkertas',
		'tottransaksi',
		'keterangan_closing',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>