<?php $this->renderPartial('/closingkasirT/informasi',array('model'=>$model)); ?>

<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>
<table>
    <tr>
        <td>
             <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglAwal', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
        <div class="control-group">
             <?php echo $form->labelEx($model,'shift_id', array('class'=>'control-label')) ?>
             <div class="controls">

                <?php echo $form->dropDownList($model,'shift_id', CHtml::listData($model->getShiftItems(), 'shift_id', 'shift_nama'), 
                                          array('empty'=>'-- Pilih --','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
             </div>
          </div>
        </td>
        <td>
          <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglAkhir', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
         <div class="control-group">
             <?php echo CHtml::label('Nama Pegawai','', array('class'=>'control-label')) ?>
             <div class="controls">

                <?php echo $form->dropDownList($model,'create_loginpemakai_id', CHtml::listData($model->getPegawaiItems(), 'pegawai_id', 'nama_pegawai'), 
                                          array('empty'=>'-- Pilih --','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
             </div>
        </div>
        </td>
    </tr>
</table>
    
	<div class="form-actions">
            <?php 
                echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); 
            ?>
             <?php
                         echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/'.InformasiRetail.'/InformasiClosing'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                    ?>
                    <?php  
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                     ?>
	</div>

<?php $this->endWidget(); ?>
