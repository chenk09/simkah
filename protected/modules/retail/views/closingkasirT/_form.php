<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<iframe id="framePrint" src="<?=$url; ?><?php echo Yii::app()->user->getFlash('url');?>" style="height:0px;">
</iframe>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'reclosingkasir-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>

        <table>
            <tr>
                <td>
                     <div class='control-group'>
                        <div class = 'control-label'>Tgl Transaksi</div>
                        <div class="controls">  
                            <?php
                                    $this->widget('MyDateTimePicker', array(
                                        'model'=>$model,
                                        'name' => 'Filter[tglAwal]',
                                        'attribute'=>'closingdari',
                                        'value'=>'Y-m-d',
                                        'language' => '',
                                        'mode' => 'datetime', //use "time","date" or "datetime" (default)
                                        'options' => array(
                                            'dateFormat' => 'yy-mm-dd',
                                            'changeYear' => true,
                                            'changeMonth' => true,
                                            'yearRange' => '-70y:+4y',
                                            'showSecond' => false,
                                            'showDate' => false,
                                            'showMonth' => false,
                                            'timeFormat' => 'hh:mm:ss',
                                        ),
                                        'htmlOptions' => array(
                                            'class'=>'isRequired',
                                            'readonly' => true,
                                            'onkeypress' => 'return $(this).focusNextInputField(event);',
                                            'onchange'=>'ajaxGetList()',
                                        ),
                                    ));
                                    ?>
                        </div>
                     </div>
                    
                     <div class='control-group'>
                        <div class = 'control-label'>Sampai Dengan</div>
                        <div class="controls">  
                            <?php
                                    $this->widget('MyDateTimePicker', array(
                                        'name' => 'Filter[tglAkhir]',
                                        'language' => '',
                                        'model'=>$model,
                                        'attribute'=>'sampaidengan',
                                        'value'=>'Y-m-d',
                                        'mode' => 'datetime', //use "time","date" or "datetime" (default)
                                        'options' => array(
                                            'dateFormat' => 'yy-mm-dd',
                                            'changeYear' => true,
                                            'changeMonth' => true,
                                            'yearRange' => '-70y:+4y',
                                            'showSecond' => false,
                                            'showDate' => false,
                                            'showMonth' => false,
                                            'timeFormat' => 'hh:mm:ss',
                                        ),
                                        'htmlOptions' => array(
                                            'class'=>'isRequired',
                                            'readonly' => true,
                                            'onchange'=>'ajaxGetList()',
                                            'onkeypress' => 'return $(this).focusNextInputField(event);',
                                        ),
                                    ));
                                    ?>
                        </div>
                     </div>
                </td>
                <td>
                    <div class="control-group">
                         <?php echo $form->labelEx($model,'shift_id', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'shift_id', CHtml::listData($model->getShiftItems(), 'shift_id', 'shift_nama'), 
                                                      array( 'disabled'=>'disabled', 'onchange'=>'ajaxGetShift()','empty'=>'-- Pilih --','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
                             <?php echo $form->hiddenField($model, 'shift_id', array('readonly'=>true, 'class'=>'span1')); ?>
                         </div>
                    </div>
                    
                    <div class="control-group">
                         <?php echo CHtml::label('User','', array('class'=>'control-label')) ?>
                         <div class="controls">
                            
                            <?php echo $form->dropDownList($model,'create_loginpemakai_id', CHtml::listData($model->getPegawaiItems(), 'pegawai_id', 'nama_pegawai'), 
                                                      array( 'disabled'=>'disabled','empty'=>'-- Pilih --','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
                             <?php echo $form->hiddenField($model, 'create_loginpemakai_id', array('readonly'=>true, 'class'=>'span1')); ?>
                         </div>
                    </div>
                </td>
            </tr>
        </table>
        <div style='max-height:300px;overflow-y: scroll;'>
         <?php echo CHtml::css('#tableList th{vertical-align:middle;}'); ?>
        <table id="tableList" class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Bukti Bayar</th>
                    <th>Tgl Pembayaran </th>
                    <th>Cara Pembayaran</th>    
                    <th>Jumlah Bayar</th>
                    <th>Uang Diterima</th>
                    <th>Uang Kembali</th>
<!--                    <th>Pilih</th>-->
                </tr>
            </thead>
            <tbody>  
                <?php if (isset($tr)){
                    echo $tr;
                }?>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan ="4"> 
                    </td>
                    <td>
                        <?php echo $form->textField($modTandabukti, 'jmlpembayaran', array('onkeypress' => 'return $(this).focusNextInputField(event);', 'class' => 'inputFormTabel span2 currency', 'readonly' => true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($modTandabukti, 'uangditerima', array('onkeypress' => 'return $(this).focusNextInputField(event);', 'class' => 'inputFormTabel span2 currency', 'readonly' => true)); ?>
                    </td>
                    <td>
                        <?php echo $form->textField($modTandabukti, 'uangkembalian', array('onkeypress' => 'return $(this).focusNextInputField(event);', 'class' => 'inputFormTabel span2 currency', 'readonly' => true)); ?>
                    </td>
                    <td></td>
                </tr>
            </tfoot>
        </table>
        </div><br>
        <legend class="rim">Jumlah Closing</legend>
        <table>
            <tr>
                <td>
                     <div class="control-group">
                         <?php echo CHtml::label('Total Jumlah Bayar','', array('class'=>'control-label')) ?>
                         <div class="controls">
                            <?php echo $form->textField($model,'nilaiclosingtrans', array('onkeyup'=>'setAll()','class'=>'span3 currency inputFormTabel','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
                             <?php echo $form->textField($model,'tottransaksi', array('class'=>'inputFormTabel span2 currency','style'=>'width:40px','onkeypress'=>"return nextFocus(this,event)",)); ?> <i>Transaksi</i>
                         </div>
                    </div>
                    
                     <div class="control-group">
                         <?php echo CHtml::label('Jumlah Saldo Awal','', array('class'=>'control-label')) ?>
                         <div class="controls">
                            <?php echo $form->textField($model,'closingsaldoawal', array('onkeyup'=>'hitungtotalclosing()','class'=>'inputFormTabel span3 currency','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    
                     <div class="control-group">
                         <?php echo CHtml::label('Total Nilai Closing','', array('class'=>'control-label')) ?>
                         <div class="controls">
                             <?php echo CHtml::textField('totalnilaiclosing','', array('onblur'=>'hitungtotalclosing()','class'=>'inputFormTabel span3 currency','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)")); ?>
                             Jml <?php echo $form->textField($model,'jmluanglogam', array('class'=>'inputFormTabel span2 currency','style'=>'width:40px','onkeypress'=>"return nextFocus(this,event)",)); ?> <i>Logam</i>
                             <?php echo $form->textField($model,'jmluangkertas', array('class'=>'inputFormTabel span2 currency','style'=>'width:40px','onkeypress'=>"return nextFocus(this,event)",)); ?> <i>Kertas</i>
                         </div>
                    </div>
                </td>
                <td>
                     <div class="control-group">
                         <?php echo CHtml::label('Keterangan','', array('class'=>'control-label')) ?>
                         <div class="controls">
                            <?php echo $form->textArea($model,'keterangan_closing', array('class'=>'span4','rows'=>5,'cols'=>50,'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                </td>
            </tr>
        </table>
	<div class="form-actions">
                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Closing',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                           Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                           array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'disabled'=>($model->isNewRecord) ? "" : "disabled")); 
                ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.closingkasirT.'/ClosingKasir'), 
                        array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); 
                ?>
                 <?php 
                        if($model->isNewRecord){
                            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'onclick'=>"printBayar($model->closingkasir_id);return false",'disabled'=>true,'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";
                        }else{
                            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                                    array('class'=>'btn btn-primary' ,'onclick'=>'print("PRINT");return false','disabled'=>false, 'type'=>'button'))."&nbsp&nbsp";
                        }
                ?>
                <?php
                    $content = $this->renderPartial('../tips/transaksi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                ?>
	</div>

<?php $this->endWidget(); ?>
<?php 
 
        
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintStruck&id='.$model->closingkasir_id);

$js = <<< JSCRIPT
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>
     
<?php 
    $url =Yii::app()->createUrl($this->route);
    $urlshift = $this->createUrl('GantiShift');
    $jmltottransaksi = CHtml::activeId($model, 'nilaiclosingtrans');
    $jmlbayar = CHtml::activeId($modTandabukti, 'jmlpembayaran');
    $jmlterima = CHtml::activeId($modTandabukti, 'uangditerima');
    $jmlkembali = CHtml::activeId($modTandabukti, 'uangkembalian');
    $uangkertas = CHtml::activeId($model, 'jmluangkertas');
    
    $js = <<< JS
    function dialogOpen(nama){
        $("#dialogKaryawan").attr('data-parent-nama',nama);
        $("#dialogKaryawan").dialog('open');
    }
    
    function setValueDialog(nama){
        var dataParentNama = $('#dialogKaryawan').attr('data-parent-nama');
        $("#"+dataParentNama+"").val(nama);
        $("#dialogKaryawan").dialog("close");    
    }
    
    function ajaxGetList(){
        tglAwal = $('#Filter_tglAwal').val();
        tglAkhir = $('#Filter_tglAkhir').val();
        $.get('${url}', {tglAwal:tglAwal, tglAkhir:tglAkhir},function(data){
            $('#tableList tbody').html(data);
            setAll();
        });
    }
        
    function ajaxGetShift(){
        shift_id = $('#REClosingkasirT_shift_id').val();
        $.post('${urlshift}', {shift_id:shift_id},function(data){
            $('#Filter_tglAwal').val(data.jamawal);
            $('#Filter_tglAkhir').val(data.jamakhir);
            //$('#tableList tbody').html(data);
            //setAll();
        },'json');
    }
    
    function setAll(){
        jmlbayar = 0;
        jmlterima = 0;
        jmlkembali = 0;
        totaltransaksi = 0;
        var totalbayar = parseFloat(unformatNumber($('#REClosingkasirT_nilaiclosingtrans').val()));
        var saldoawal = parseFloat($('#REClosingkasirT_closingsaldoawal').val());
        var totalnilaiclosing =$('#totalnilaiclosing').val();
       
        var jmlnilaiclosing = totalbayar + saldoawal;
      
        
        $('.cek').each(function(){
//            if ($(this).is(':checked')){
                tempbayar = unformatNumber($(this).parents('tr').find('.jmlbayar').val());
                tempterima = unformatNumber($(this).parents('tr').find('.jmlterima').val());
                tempkembali = unformatNumber($(this).parents('tr').find('.jmlkembali').val());
                temptotaltransaksi = $(this).parents('tr').find('.totaltransaksi').val();
                totalbayar = parseFloat(unformatNumber($('#REClosingkasirT_nilaiclosingtrans').val()));
                saldoawal = parseFloat($('#REClosingkasirT_closingsaldoawal').val());
                totalnilaiclosing =$('#totalnilaiclosing').val();

                jmlnilaiclosing = totalbayar + saldoawal;
        
                if (jQuery.isNumeric(tempbayar)){
                    jmlbayar += parseFloat(unformatNumber(tempbayar));
                }
                if (jQuery.isNumeric(tempterima)){
                    jmlterima += parseFloat(unformatNumber(tempterima));
                }
                if (jQuery.isNumeric(tempkembali)){
                    jmlkembali += parseFloat(unformatNumber(tempkembali));
                }
                if (jQuery.isNumeric(totaltransaksi)){
                    totaltransaksi = parseFloat(temptotaltransaksi);
                }
//            }
        });
        
        $('#${jmlbayar}').val(formatUang(jmlbayar));
        $('#${jmlterima}').val(formatUang(jmlterima));
        $('#${jmlkembali}').val(formatUang(jmlkembali));
        $('#REClosingkasirT_nilaiclosingtrans').val(formatUang(jmlbayar));
        $('#REClosingkasirT_tottransaksi').val(totaltransaksi);
        $('#totalnilaiclosing').val(formatUang(jmlbayar));
    }
        
    function hitungtotalclosing(){
        
        var totalbayar = parseFloat(unformatNumber($('#REClosingkasirT_nilaiclosingtrans').val()));
        var saldoawal = parseFloat(unformatNumber($('#REClosingkasirT_closingsaldoawal').val()));
        var totalnilaiclosing =$('#totalnilaiclosing').val();
       
        var jmlnilaiclosing = totalbayar + saldoawal;
      
        $('#totalnilaiclosing').val(formatUang(jmlnilaiclosing));
    }
    
     function cekInput()
     {

        $(".currency").each(function(){this.value = unformatNumber(this.value)});
        return true;
     }
        
    function print(caraPrint){
        window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
    }
    
    
JS;
    Yii::app()->clientScript->registerScript('onheadDialog', $js, CClientScript::POS_HEAD);
    ?>

    <?php 
    Yii::app()->clientScript->registerScript('onsubmitfunction',
        (($model->isNewRecord) ? 'ajaxGetList();' : 'printStruck("PRINT");hitungtotalclosing();').'
        $("form").submit(function(){
            if ($(".cek").length < 1){
                alert("Detail harus diisi");
                return false;
            }
        });
',  CClientScript::POS_READY); 
    ?>
<script>
            
function printStruck(caraPrint)
{
        //window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/printStruck', array('id'=>$model->closingkasir_id, "caraPrint"=>"PRINT")); ?>',"", 'left=100,top=100,width=400,height=400,scrollbars=1');
        $("#framePrint").attr("src",'<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/printStruck', array('id'=>$model->closingkasir_id, "caraPrint"=>"PRINT")); ?>'); 
}
</script>