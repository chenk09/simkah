<legend class="rim2">Informasi Closing Kasir</legend>
<?php
$this->breadcrumbs=array(
	'Closingkasir Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('closingkasir-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>


<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'closingkasir-t-grid',
	'dataProvider'=>$model->searchInformasi(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Total Sales Dari</center>',
                'start'=>'5',
                'end'=>'6',
            ),
        ),
	'columns'=>array(
		array(
                    'header'=>'No Closing',
                    'type'=>'raw',
                    'filter'=>false,
                    'name'=>'closingkasir_id',
                    'value'=>'$data->closingkasir_id',
                ),
                array(
                    'header'=>'Tgl Closing',
                    'type'=>'raw',
                    'filter'=>false,
                    'name'=>'tglclosingkasir',
                    'value'=>'$data->tglclosingkasir',
                ),
                array(
                    'header'=>'User',
                    'type'=>'raw',
                    'name'=>'nama_pegawai',
                    'value'=>'$data->pegawai->nama_pegawai',
                ),
                array(
                    'header'=>'Shift',
                    'type'=>'raw',
                    'name'=>'shift_id',
                    'value'=>'$data->shift->shift_nama',
                ),
                 array(
                    'header'=>'Tgl Closing <br> Sampai Dengan',
                    'type'=>'raw',
                    'value'=>'$data->closingdari."<br>".$data->sampaidengan',
                ),
//                 array(
//                    'header'=>'<center>Total Transaksi</center>',
//                    'type'=>'raw',
//                    'value'=>'$data->tottransaksi',
//                ),
                 array(
                    'header'=>'<center>Value</center>',
                    'type'=>'raw',
                     'value'=>'MyFunction::formatNumber($data->nilaiclosingtrans)',
                ),
                 array(
                    'header'=>'Print Closing',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-print\'></i>",Yii::app()->createUrl(\'retail/closingkasirT/PrintStruck&id=\'.$data->closingkasir_id."&caraPrint=PRINT"),array("rel"=>"tooltip","title"=>"Klik untuk Print Closing Kasir"))',
                  'htmlOptions'=>array('style'=>'text-align: center; width:60px'),
                    ),
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
