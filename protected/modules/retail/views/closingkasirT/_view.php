<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('closingkasir_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->closingkasir_id),array('view','id'=>$data->closingkasir_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tandabuktibayar_id')); ?>:</b>
	<?php echo CHtml::encode($data->tandabuktibayar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift_id')); ?>:</b>
	<?php echo CHtml::encode($data->shift_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglclosingkasir')); ?>:</b>
	<?php echo CHtml::encode($data->tglclosingkasir); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('closingdari')); ?>:</b>
	<?php echo CHtml::encode($data->closingdari); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sampaidengan')); ?>:</b>
	<?php echo CHtml::encode($data->sampaidengan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nilaiclosingtrans')); ?>:</b>
	<?php echo CHtml::encode($data->nilaiclosingtrans); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('closingsaldoawal')); ?>:</b>
	<?php echo CHtml::encode($data->closingsaldoawal); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmluanglogam')); ?>:</b>
	<?php echo CHtml::encode($data->jmluanglogam); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmluangkertas')); ?>:</b>
	<?php echo CHtml::encode($data->jmluangkertas); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tottransaksi')); ?>:</b>
	<?php echo CHtml::encode($data->tottransaksi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keterangan_closing')); ?>:</b>
	<?php echo CHtml::encode($data->keterangan_closing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->update_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_ruangan')); ?>:</b>
	<?php echo CHtml::encode($data->create_ruangan); ?>
	<br />

	*/ ?>

</div>