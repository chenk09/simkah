<legend class="rim2">
   Informasi Stock
</legend>
<?php
$this->breadcrumbs=array(
	'Reinformasipenjualanprodukpos Vs'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reinformasipenjualanprodukpos-v-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
	'id'=>'reinformasipenjualanprodukpos-v-grid',
	'dataProvider'=>$model->search(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeColumns'=>array('stock_code'),
	'columns'=>array(
                array(
                    'header'=>'NO',
                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    
                ),
                array(
                    'header'=>'Category /'."<br>".'Sub Category',
                    'name'=>'category_name',
                    'type'=>'raw',
                    'value'=>'$data->category_name."/"."<br>".$data->subcategory_name',
                ),
               'stock_code',
                array(
                    'header'=>'Stock Name /'."<br>".'Price Name',
                    'name'=>'stock_name',
                    'type'=>'raw',
                    'value'=>'$data->stock_name."/"."<br>".$data->stock_name',
                ),
                array(
                    'header'=>'Harga Jual',
                    'name'=>'hargajual',
                    'type'=>'raw',
                    'value'=>'$data->hargajual',
                ),
                 array(
                    'header'=>'Discount',
                    'name'=>'discount',
                    'footer'=>'<b>Jumlah:</b>',
                    'type'=>'raw',
                    'value'=>'$data->discount',
                     'htmlOptions'=>array(
                        'style'=>'text-align:right;padding-right:10%;'
                    ),
                ),
                array(
                    'header'=>'Stock On Hand',
                    'name'=>'qtystok_current',
                    'footer'=>number_format($model->getTotalscurrent()),
                    'type'=>'raw',
                    'value'=>'$data->qtystok_current',
                ),
                 array(
                    'header'=>'Moving Average',
                    'name'=>'movingavarage',
                    'type'=>'raw',
                    'value'=>'$data->movingavarage',
                     'htmlOptions'=>array(
                        'style'=>'text-align:right;padding-right:10%;'
                    ),
                ),
                 
//             
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>


<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form">
<?php 
$this->renderPartial('_search',array(
	'model'=>$model,
)); 
?>
