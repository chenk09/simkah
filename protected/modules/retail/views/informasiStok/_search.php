<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'reinformasipenjualanprodukpos-v-search',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <?php echo $form->textFieldRow($model,'category_name',array('class'=>'span3','style'=>'width:140px','maxlength'=>50)); ?>

            <?php echo $form->textFieldRow($model,'subcategory_name',array('class'=>'span3','style'=>'width:140px','maxlength'=>100)); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'stock_code',array('class'=>'span3','style'=>'width:140px','maxlength'=>50)); ?>

            <?php echo $form->textFieldRow($model,'stock_name',array('class'=>'span3','style'=>'width:140px','maxlength'=>200)); ?>
        </td>
    </tr>
</table>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
            array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
     <?php
         echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.InformasiStok.'/InformasiStock'), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
    ?>
    <?php  
        $content = $this->renderPartial('../tips/informasi',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
     ?>
</div>

<?php $this->endWidget(); ?>
