<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'reinformasipenjualanprodukpos-v-search',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<!--
	<?php echo $form->textFieldRow($model,'brg_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'category_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'category_name',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'subcategory_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'subcategory_code',array('class'=>'span5','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'subcategory_name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'stock_code',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'stock_name',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'satuankecil_nama',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'barang_barcode',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'obatalkes_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'qty_oa',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hargasatuan_oa',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hargajual_oa',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglpenjualan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jenispenjualan',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'totalhargajual',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'penjualanresep_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'obatalkespasien_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'noresep',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'ruangan_nama',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'nama_pemakai',array('class'=>'span5','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'oasudahbayar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'pembayaranpelayanan_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglpembayaran',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nopembayaran',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'tandabuktibayar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'shift_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nourutkasir',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'nobuktibayar',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'tglbuktibayar',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'carapembayaran',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'dengankartu',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'bankkartu',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nokartu',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'nostrukkartu',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'jmlpembulatan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlpembayaran',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'biayaadministrasi',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'uangditerima',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'uangkembalian',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'keterangan_pembayaran',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update_time',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'create_time',array('class'=>'span5')); ?>
-->
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglAwal', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
        </td>

        <td>
            <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglAkhir', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </td>
    </tr>
</table>
	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
                    <?php
                         echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/'.InformasipenjualanprodukposV.'/admin'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                    ?>
                    <?php  
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                     ?>
	</div>

<?php $this->endWidget(); ?>
