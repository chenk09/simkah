<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php
//$urlData = Yii::app()->createUrl('ActionAjax/GetProdukpos&category_id=1');
//$this->widget('MyJqgrid', 
//              array(
//                    'name'=>'produkpos-v-grid',
//                    'compression'=>'none',
//                    'theme'=>'start',
//                    'useNavBar'=>true,
//                    'useNavBar'=>'false',
//                    'options'=>array(
//                                     'datatype'=>'json',
//                                     'scroll'=>true,
//                                     'url'=>$urlData,
//                                     'search'=>true,
//                                     'searchdata'=>array(
//                                     ),
//                                     'colNames'=>array(
//                                         'No',
//                                         'Category / SubCategory',
//                                         'Stock Code / Barcode',
//                                         'Stock Name / Price Name',
//                                         'Harga Jual + PPN',
//                                         'Discount(%)',
//                                         'PPN(%)',
//                                         'Harga Jual -PPN',
//                                         'Moving Average',
//                                         'Stock Sistem',
//                                     ),
//                                     'colModel'=>array(
//                                                       array('name'=>'No','index'=>'no','width'=>30,'background'=>'#FF0000'),
//                                                       array('name'=>'Category.SubCategory','index'=>'category_name','width'=>150),
//                                                       array('name'=>'Stock_code.barang_code','index'=>'stock_code','width'=>100),
//                                                       array('name'=>'stock_name','index'=>'stock_name','width'=>100),
//                                                       array('name'=>'hargajual','index'=>'hargajual','width'=>100),
//                                                       array('name'=>'discount','index'=>'discount','width'=>100),
//                                                       array('name'=>'ppn','index'=>'ppn_persen','width'=>100),
//                                                       array('name'=>'harganetto','index'=>'harganetto','width'=>100),
//                                                       array('name'=>'ratarata','index'=>'movingavarage','width'=>100),
//                                                       array('name'=>'stocksistem','width'=>50),
//                                                      ),
//                                     'rowNum'=>10,
//                                     'sortname'=>'brg_id',
//                                     'viewrecords'=>true,
//                                     'sortorder'=>"desc",
//                                     'caption'=>"",
//                                     'height'=>270,
//                                    ),
//                   )
//             );
?>
<?php
$this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
    'id'=>'produkpos-v-grid',
    'dataProvider'=>$modProduk->searchTransaksi(),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
                array(
                    'header' => 'No',
                    'type'=>'raw',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
                 array(
                        'header'=>'Category / <br> Sub Category',
                        'type'=>'raw',
                        'value'=>'$data->category_name." / ".$data->subcategory_name.
                        CHtml::activeHiddenField($data, \'[\'.$data->brg_id.\']brg_id\').
                        CHtml::checkBox(\'REProdukposV[\'.$data->brg_id.\'][cekList]\', true, array(\'onclick\'=>\'setUrutan()\', \'class\'=>\'cekList\', \'onclick\'=>\'getTotal();\', \'style\'=>\'display:none;\'));
                        ',
                ),
                array(
                        'header'=>'Stock Code / <br> Barcode ',
                        'value'=>'$data->stock_code." / ".$data->barang_barcode'
                ),
                 array(
                        'header'=>'Stock Name / <br> Price Name',
                        'value'=>'$data->stock_name." / ".$data->stock_name' 
                ),
                array(
                        'header'=>'Harga Jual +PPN',
                        'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][hargajual]", $data->hargajual, array("id"=>"hargajual","class"=>"span1 hargajual numbersOnly margin"))',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'Discount(%)',
                         'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][discount]", $data->discount, array("id"=>"discount","class"=>"span1 numbersOnly margin"))',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'PPN(%)',
                        'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][ppn]", $data->ppn_persen, array("id"=>"ppn","class"=>"span1 numbersOnly margin"))."".$data->ppn_persen',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'Harga Jual -PPN',
                        'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][harganetto]", $data->harganetto, array("id"=>"harganetto","class"=>"span1 numbersOnly margin"))',
                        'type'=>'raw',
                ),
                 array(
                        'header'=>'Moving Average',
                         'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][ratarata]", $data->movingavarage, array("id"=>"hargaratarata","class"=>"span1 numbersOnly margin"))."".$data->movingavarage',
                        'type'=>'raw',
                        
                ),
//                array(
//                        'header'=>'Margin',
//                         'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][jmlmargin]", $data->margin, array("id"=>"marginlama","class"=>"span1 numbersOnly margin"))."".$data->margin',
//                         'type'=>'raw',
//                ),
//                array(
//                        'header'=>'GP %',
//                        'value'=>'$data->gp_persen',
//                        
//                ),
//                array(
//                    'header'=>'Margin',
//                    'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][margin]",$data->margin, array("id"=>"margin","class"=>"span1 numbersOnly margin"))',
//                    'type'=>'raw',
//                ),
//                array(
//                    'header'=>'GP %',
//                    'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][gp]", $data->gp_persen, array("id"=>"gp","class"=>"span1 numbersOnly margin"))',
//                    'type'=>'raw',
//                ),
                array(
                    'header'=>'Stok Sistem',
                    'type'=>'raw',
                'value'=> 'CHtml::textField(\'stok\', StokobatalkesT::getStokBarang($data->brg_id, '.Yii::app()->user->getState('ruangan_id').'), array(\'class\'=>\'stok span1\', \'readonly\'=>true))',
                ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                    getTotal();
                }',
));
?>
<script>
    $("#btncari").click(function() {
        $("#gview_produkpos-v-grid_grid").trigger("reloadGrid");
        return false;
    });
    
//    stock_code = $("#REProdukposV_stock_code").val();
//    barang_barcode = $("#REProdukposV_barang_barcode").val();
//    stock_name = $("#REProdukposV_stock_name").val();
//    price_name = $("#REProdukposV_price_name").val();
//    category_id = $("#REProdukposV_category_id").val();
//    subcategory_id = $("#REProdukposV_subcategory_id").val();
//    $("#btncari").click(function(){
//        $.ajax({
//            url : "<?php echo $urlData ?>",
//            dataType : "json",
//            data : {stock_code:stock_code, barang_barcode:barang_barcode, stock_name:stock_name, price_name:price_name, category_id:category_id, subcategory_id:subcategory_id},
//            success : function (data) {
//                
//            }
//        });
//        return false;
//    });
</script>