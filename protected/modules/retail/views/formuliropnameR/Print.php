
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>11));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $dataProvider = $modObat->searchDataObat();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $dataProvider = $modObat->searchDataObat();
         $template = "\n{items}";
    }
    
$this->widget($table,array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$dataProvider,
//    'filter'=>$model,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        'brg_id',
            array(
                'name'=>'jenisobatalkes_id',
                'value'=>'$data->jenisobatalkes->jenisobatalkes_nama',
                ),
            'obatalkes_kode',
//            array(
//                'header'=>'Nama Obat <br/> Kategori',
//                'type'=>'raw',
//                'value'=>'$data->obatalkes_nama.\'<br/>\'.$data->obatalkes_kategori',
//            ),
//            'obatalkes_golongan',
            array(
                'header'=>'Nama Obat <br/> Kekuatan',
                'type'=>'raw',
                'value'=>'$data->obatalkes_nama.\'<br/>\'.$data->kekuatan',
            ),
            array(
                'header'=>'Golongan<br/>Kategori',
                'type'=>'raw',
                'value'=>'$data->obatalkes_golongan.\'<br/>\'.$data->obatalkes_kategori',
            ),
            array(
                'name'=>'sumberdana_id',
                'value'=>'$data->sumberdana->sumberdana_nama',
            ),
           
            array(
                'header'=>'Harga Satuan',
                'type'=>'raw',
                'value'=>'$data->hargajual'
            ),
            array(
                'header'=>'Harga Netto',
                'type'=>'raw',
                'value'=>'$data->harganetto'
            ),
            'tglkadaluarsa',
            'minimalstok',
            array(
                'header'=>'Stok Sistem',
                'type'=>'raw',
                'value'=> 'StokobatalkesT::getStokBarang($data->obatalkes_id)',
            ),
            array(
                'header'=>'Stok Fisik',
                'value'=>'',
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
?>

<?php if (isset($caraPrint)) { ?>
<table width="100%" style='margin-top:100px;margin-left:auto;margin-right:auto;'>
    <tr>
        <td width="50%">
                <label style='float:left;'>Petugas : <?php echo $data['nama_pegawai']; ?></label>

        </td>
        <td width="50%">
            
                <label style='float:right;'>Tanggal Print : <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?></label>
            
        </td>
    </tr>
</table>
<?php } 