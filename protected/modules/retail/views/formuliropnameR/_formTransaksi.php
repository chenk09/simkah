<?php if ($model->isNewRecord) { ?>
<?php// $this->renderPartial('_obatalkes', array('model' => $modProduk)); ?>
<?php } ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php 
    Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('produkpos-v-grid', {
		data: $(this).serialize()
	});
                getTotal();
	return false;
});
");
?>
<?php 
Yii::app()->clientScript->registerScript('head', '
    $("document").ready(function(){
        getTotal();
    });
    
    function getTotal(){
        var totalStok = 0;
        var totalHarga = 0;
        var totalVolume = 0;
        $(".hargajual").each(function(){
            totalHarga += parseFloat($(this).val());
            totalVolume += parseFloat($(this).parents("tr").find("#stok").val());
        });
        $("#REFormuliropnameR_totalharga").val(totalHarga);
        $("#REFormuliropnameR_totalvolume").val(totalVolume);
    }
',  CClientScript::POS_HEAD); 
?>
<?php $this->renderPartial('_searchTransaksi',array('model'=>$model,'modProduk'=>$modProduk)); ?>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'gfformuliropname-r-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#',
        ));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>

<?php echo $form->errorSummary($model); ?>
<?php $this->renderPartial('_tableTransaksi', array('model' =>$model,'modProduk'=>$modProduk, 'posts'=>$posts, 'pages'=>$pages)); ?>
    <table>
        <tr>
            <td>
                <?php //echo $form->textFieldRow($model, 'tglformulir', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglformulir', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglformulir',
                            'mode' => 'date',
                            'options' => array(
                                'dateFormat' => Params::DATE_TIME_FORMAT,
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model, 'noformulir', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($model, 'totalvolume', array('class' => 'span3', 'readonly' => true, 'onclick'=>'getTotal()')); ?>
                <?php echo $form->textFieldRow($model, 'totalharga', array('class' => 'span3', 'readonly' => true, 'onclick'=>'getTotal()')); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
        <?php
        if ($model->isNewRecord){
            echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                        Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event) getTotal()'));
        }
        else{
            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
        }
        ?>
        <?php
        if (isset(Yii::app()->request->urlReferrer)){
            $back = Yii::app()->request->urlReferrer;
        }else{
            $back = Yii::app()->createUrl($this->module->id . '/' . formuliropnameR . '/index');
        }
        echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), $back, array('class' => 'btn btn-danger',
            'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;'));
			$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
    </div>

<?php $this->endWidget(); ?>
<?php Yii::app()->clientScript->registerScript('ready', '
    $("form#gfformuliropname-r-form").submit(function(){
        getTotal();
    })
', CClientScript::POS_READY); ?>

<?php 

        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
$formulir = $model->formuliropname_id;
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/&id=${formulir}&caraPrint="+caraPrint,"",'location=_new, width=1100px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>