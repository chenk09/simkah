<?php
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
    'stacked'=>false, // whether this is a stacked menu
    'items'=>array(
        array('label'=>'Satuan Besar', 'url'=>$this->createUrl('/retail/satuanBesarM/admin'), 'active'=>true),
        array('label'=>'Satuan Kecil', 'url'=>$this->createUrl('/retail/satuanKecilM/admin'),),
    ),
)); 
?>