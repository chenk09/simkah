<?php
$this->breadcrumbs=array(
	'Penerimaanbarang Ts'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List PenerimaanbarangT', 'url'=>array('index')),
	array('label'=>'Create PenerimaanbarangT', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('penerimaanbarang-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Penerimaanbarang Ts</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'penerimaanbarang-t-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'penerimaanbarang_id',
		'supplier_id',
		'permintaanpembelian_id',
		'fakturpembelian_id',
		'noterima',
		'tglterima',
		/*
		'tglterimafaktur',
		'nosuratjalan',
		'tglsuratjalan',
		'gudangpenerima_id',
		'keteranganterima',
		'harganetto',
		'jmldiscount',
		'persendiscount',
		'totalpajakppn',
		'totalpajakpph',
		'totalharga',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
