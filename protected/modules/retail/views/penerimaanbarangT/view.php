<?php
$this->breadcrumbs=array(
	'Penerimaanbarang Ts'=>array('index'),
	$model->penerimaanbarang_id,
);

$this->menu=array(
	array('label'=>'List PenerimaanbarangT', 'url'=>array('index')),
	array('label'=>'Create PenerimaanbarangT', 'url'=>array('create')),
	array('label'=>'Update PenerimaanbarangT', 'url'=>array('update', 'id'=>$model->penerimaanbarang_id)),
	array('label'=>'Delete PenerimaanbarangT', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->penerimaanbarang_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PenerimaanbarangT', 'url'=>array('admin')),
);
?>

<h1>View PenerimaanbarangT #<?php echo $model->penerimaanbarang_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'penerimaanbarang_id',
		'supplier_id',
		'permintaanpembelian_id',
		'fakturpembelian_id',
		'noterima',
		'tglterima',
		'tglterimafaktur',
		'nosuratjalan',
		'tglsuratjalan',
		'gudangpenerima_id',
		'keteranganterima',
		'harganetto',
		'jmldiscount',
		'persendiscount',
		'totalpajakppn',
		'totalpajakpph',
		'totalharga',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
	),
)); ?>
