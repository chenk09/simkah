<legend class="rim2">Informasi Receive</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'penerimaanbarang-t-grid',
	'dataProvider'=>$model->searchInformasi(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{pager}{summary}\n{items}",
	'columns'=>array(
		'noterima',
		'tglterima',
		'penerimaanbarang_id',
		'supplier.supplier_nama',
		'permintaanpembelian_id',
		'fakturpembelian_id',
//                                'harganetto',
                                'totalharga',
		'keteranganterima',
                                array(
                                        'header'=>'Detail Receive',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("PenerimaandetailT/detailBarang",array("id"=>$data->penerimaanbarang_id,"frame"=>true)),
                                                    array("class"=>"", 
                                                          "target"=>"iframeDetailreceive",
                                                          "onclick"=>"$(\"#dialogDetailreceive\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk melihat Detail Receive",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),
                                array(
                                        'header'=>'Retur',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("SupplierM/detailProduk",array("id"=>$data->supplier_id,"frame"=>true)),
                                                    array("class"=>"", 
                                                          "target"=>"iframeRetur",
                                                          "onclick"=>"$(\"#dialogReturreceive\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk melihat Retur",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),
		/*
		'tglterimafaktur',
		'nosuratjalan',
		'tglsuratjalan',
		'gudangpenerima_id',
		'harganetto',
		'jmldiscount',
		'persendiscount',
		'totalpajakppn',
		'totalpajakpph',
		'totalharga',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
	),
)); ?>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'penerimaanbarang-t-search',
)); ?>
<table>
    <tr>
        <td>
                <div class="control-group">
                   <?php echo $form->label($model,'tglterimaAwal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglterimaAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
                <div class="control-group">
                   <?php echo $form->label($model,'tglterimaAkhir',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglterimaAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'noterima',array('class'=>'span3')); ?>
            <?php echo $form->dropDownListRow($model,'supplier_id',CHtml::listData($model->getSupplierforInformasi(),'supplier_id','supplier_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
        </td>
    </tr>
</table>

	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.supplierM.'/informasi'), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                    <?php
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailreceive',
    'options'=>array(
        'title'=>'Detail Receive',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetailreceive" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>