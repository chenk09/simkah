<?php
$this->breadcrumbs=array(
	'Penerimaanbarang Ts'=>array('index'),
	$model->penerimaanbarang_id=>array('view','id'=>$model->penerimaanbarang_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PenerimaanbarangT', 'url'=>array('index')),
	array('label'=>'Create PenerimaanbarangT', 'url'=>array('create')),
	array('label'=>'View PenerimaanbarangT', 'url'=>array('view', 'id'=>$model->penerimaanbarang_id)),
	array('label'=>'Manage PenerimaanbarangT', 'url'=>array('admin')),
);
?>

<h1>Update PenerimaanbarangT <?php echo $model->penerimaanbarang_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>