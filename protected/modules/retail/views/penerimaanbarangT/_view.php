<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('penerimaanbarang_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->penerimaanbarang_id), array('view', 'id'=>$data->penerimaanbarang_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('supplier_id')); ?>:</b>
	<?php echo CHtml::encode($data->supplier_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('permintaanpembelian_id')); ?>:</b>
	<?php echo CHtml::encode($data->permintaanpembelian_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fakturpembelian_id')); ?>:</b>
	<?php echo CHtml::encode($data->fakturpembelian_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('noterima')); ?>:</b>
	<?php echo CHtml::encode($data->noterima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglterima')); ?>:</b>
	<?php echo CHtml::encode($data->tglterima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglterimafaktur')); ?>:</b>
	<?php echo CHtml::encode($data->tglterimafaktur); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('nosuratjalan')); ?>:</b>
	<?php echo CHtml::encode($data->nosuratjalan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglsuratjalan')); ?>:</b>
	<?php echo CHtml::encode($data->tglsuratjalan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gudangpenerima_id')); ?>:</b>
	<?php echo CHtml::encode($data->gudangpenerima_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keteranganterima')); ?>:</b>
	<?php echo CHtml::encode($data->keteranganterima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harganetto')); ?>:</b>
	<?php echo CHtml::encode($data->harganetto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmldiscount')); ?>:</b>
	<?php echo CHtml::encode($data->jmldiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('persendiscount')); ?>:</b>
	<?php echo CHtml::encode($data->persendiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalpajakppn')); ?>:</b>
	<?php echo CHtml::encode($data->totalpajakppn); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalpajakpph')); ?>:</b>
	<?php echo CHtml::encode($data->totalpajakpph); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalharga')); ?>:</b>
	<?php echo CHtml::encode($data->totalharga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->update_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_ruangan')); ?>:</b>
	<?php echo CHtml::encode($data->create_ruangan); ?>
	<br />

	*/ ?>

</div>