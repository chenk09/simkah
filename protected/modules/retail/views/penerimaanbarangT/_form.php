<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'penerimaanbarang-t-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'supplier_id'); ?>
		<?php echo $form->textField($model,'supplier_id'); ?>
		<?php echo $form->error($model,'supplier_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'permintaanpembelian_id'); ?>
		<?php echo $form->textField($model,'permintaanpembelian_id'); ?>
		<?php echo $form->error($model,'permintaanpembelian_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fakturpembelian_id'); ?>
		<?php echo $form->textField($model,'fakturpembelian_id'); ?>
		<?php echo $form->error($model,'fakturpembelian_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'noterima'); ?>
		<?php echo $form->textField($model,'noterima',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'noterima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tglterima'); ?>
		<?php echo $form->textField($model,'tglterima'); ?>
		<?php echo $form->error($model,'tglterima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tglterimafaktur'); ?>
		<?php echo $form->textField($model,'tglterimafaktur'); ?>
		<?php echo $form->error($model,'tglterimafaktur'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nosuratjalan'); ?>
		<?php echo $form->textField($model,'nosuratjalan',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'nosuratjalan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tglsuratjalan'); ?>
		<?php echo $form->textField($model,'tglsuratjalan'); ?>
		<?php echo $form->error($model,'tglsuratjalan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'gudangpenerima_id'); ?>
		<?php echo $form->textField($model,'gudangpenerima_id'); ?>
		<?php echo $form->error($model,'gudangpenerima_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'keteranganterima'); ?>
		<?php echo $form->textArea($model,'keteranganterima',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'keteranganterima'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'harganetto'); ?>
		<?php echo $form->textField($model,'harganetto'); ?>
		<?php echo $form->error($model,'harganetto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'jmldiscount'); ?>
		<?php echo $form->textField($model,'jmldiscount'); ?>
		<?php echo $form->error($model,'jmldiscount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'persendiscount'); ?>
		<?php echo $form->textField($model,'persendiscount'); ?>
		<?php echo $form->error($model,'persendiscount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'totalpajakppn'); ?>
		<?php echo $form->textField($model,'totalpajakppn'); ?>
		<?php echo $form->error($model,'totalpajakppn'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'totalpajakpph'); ?>
		<?php echo $form->textField($model,'totalpajakpph'); ?>
		<?php echo $form->error($model,'totalpajakpph'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'totalharga'); ?>
		<?php echo $form->textField($model,'totalharga'); ?>
		<?php echo $form->error($model,'totalharga'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_time'); ?>
		<?php echo $form->textField($model,'create_time'); ?>
		<?php echo $form->error($model,'create_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
		<?php echo $form->error($model,'update_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_loginpemakai_id'); ?>
		<?php echo $form->textField($model,'create_loginpemakai_id'); ?>
		<?php echo $form->error($model,'create_loginpemakai_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_loginpemakai_id'); ?>
		<?php echo $form->textField($model,'update_loginpemakai_id'); ?>
		<?php echo $form->error($model,'update_loginpemakai_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_ruangan'); ?>
		<?php echo $form->textField($model,'create_ruangan'); ?>
		<?php echo $form->error($model,'create_ruangan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->