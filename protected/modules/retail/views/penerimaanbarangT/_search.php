<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'penerimaanbarang_id'); ?>
		<?php echo $form->textField($model,'penerimaanbarang_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'supplier_id'); ?>
		<?php echo $form->textField($model,'supplier_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'permintaanpembelian_id'); ?>
		<?php echo $form->textField($model,'permintaanpembelian_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fakturpembelian_id'); ?>
		<?php echo $form->textField($model,'fakturpembelian_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'noterima'); ?>
		<?php echo $form->textField($model,'noterima',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglterima'); ?>
		<?php echo $form->textField($model,'tglterima'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglterimafaktur'); ?>
		<?php echo $form->textField($model,'tglterimafaktur'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nosuratjalan'); ?>
		<?php echo $form->textField($model,'nosuratjalan',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tglsuratjalan'); ?>
		<?php echo $form->textField($model,'tglsuratjalan'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gudangpenerima_id'); ?>
		<?php echo $form->textField($model,'gudangpenerima_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'keteranganterima'); ?>
		<?php echo $form->textArea($model,'keteranganterima',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'harganetto'); ?>
		<?php echo $form->textField($model,'harganetto'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'jmldiscount'); ?>
		<?php echo $form->textField($model,'jmldiscount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'persendiscount'); ?>
		<?php echo $form->textField($model,'persendiscount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'totalpajakppn'); ?>
		<?php echo $form->textField($model,'totalpajakppn'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'totalpajakpph'); ?>
		<?php echo $form->textField($model,'totalpajakpph'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'totalharga'); ?>
		<?php echo $form->textField($model,'totalharga'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_time'); ?>
		<?php echo $form->textField($model,'create_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_time'); ?>
		<?php echo $form->textField($model,'update_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_loginpemakai_id'); ?>
		<?php echo $form->textField($model,'create_loginpemakai_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'update_loginpemakai_id'); ?>
		<?php echo $form->textField($model,'update_loginpemakai_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'create_ruangan'); ?>
		<?php echo $form->textField($model,'create_ruangan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->