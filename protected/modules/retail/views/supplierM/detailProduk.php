
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'resupplier-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
 <legend class="rim">Data Supplier</legend>
        <table>
            <tr>
               
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('Kode Supplier','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($modSupplier,'supplier_kode',array('id'=>'supplier_kode','class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                     <div class="control-group">
                        <?php echo CHtml::label('Nama Supplier','namasupplier',array('class'=>'control-label')) ?>
                        <div class="controls">  
                                <?php echo $form->textField($modSupplier,'supplier_nama',array('id'=>'supplier_kode','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                     <div class="control-group">
                         <?php echo CHtml::label('Telp / Fax','', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($modSupplier,'supplier_telp',array('id'=>'supplier_telp','class'=>'span2', 'placeholder'=>'Telp','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50));?>
                            <?php echo $form->textField($modSupplier,'supplier_fax',array('id'=>'supplier_fax','class'=>'span2', 'placeholder'=>'Fax','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        </div>
                     </div>
                </td>
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('Propinsi','',array('class'=>'control-label')) ?>
                        <div class="controls">  
                                <?php echo $form->textField($modSupplier,'supplier_propinsi',array('id'=>'supplier_kode','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Kabupaten','',array('class'=>'control-label')) ?>
                        <div class="controls">  
                                <?php echo $form->textField($modSupplier,'supplier_kabupaten',array('id'=>'supplier_kode','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Alamat','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textArea($modSupplier,'supplier_alamat',array('id'=>'supplier_alamat','rows'=>6, 'cols'=>30, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
         <?php $this->endWidget(); ?>
 
         <table id="tblObatSupplier" class="table table-bordered table-condensed">
                <thead>
                    <tr>
<!--                        <th>No</th>
-->                        <th>No</th>
                        <th>Stock Code</th>
                        <th>Stock Name</th>
                        <th>Category</th>
                        <th>Sub Category</th>
                    </tr>
                </thead>
                <tbody id="bodyTblFormObatSupplier">
                    <?php 
       
                    foreach ($modDetailProduk as $i=>$row){
                        $i++;
                        echo '<tr>
                                <td>'.$i.'
                                </td>
                                <td>'.$row->obatalkes->obatalkes_kode.' 
                                </td>
                                <td>'.$row->obatalkes->obatalkes_nama.'
                                </td>
                                <td>'.$row->obatalkes->jenisobatalkes->jenisobatalkes_nama.'
                                </td>
                               <td>'.$row->obatalkes->subjenis->subjenis_nama.'
                                </td>
                               </tr>';

                            }
            ?>
                </tbody>
         </table>