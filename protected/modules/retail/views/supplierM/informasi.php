<legend class="rim2">Informasi Supplier</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('supplier-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'supplier-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
         'mergeHeaders'=>array(
            array(
                'name'=>'<center>Contact I</center>',
                'start'=>6, //indeks kolom 3
                'end'=>8, //indeks kolom 4
            ),
             array(
                'name'=>'<center>Contact II</center>',
                'start'=>9, //indeks kolom 3
                'end'=>11, //indeks kolom 4
            ),
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'supplier_id',
		array(
                        'header'=>'ID',
                        'name'=>'supplier_id',
                        'value'=>'$data->supplier_id',
                        'filter'=>false,
                ),
                array(
                        'header'=>'Supplier Code',
                        'name'=>'supplier_kode',
                        'value'=>'$data->supplier_kode',
                        'filter'=>false,
                ),
		array(
                        'header'=>'Supplier Name',
                        'name'=>'supplier_nama',
                        'value'=>'$data->supplier_nama',
                        'filter'=>false,
                ),
		array(
                        'header'=>'Alamat',
                        'name'=>'supplier_alamat',
                        'value'=>'$data->supplier_alamat',
                        'filter'=>false,
                ),
		array(
                        'header'=>'Propinsi / Kabupaten',
                        'name'=>'supplier_propinsi',
                        'value'=>'$data->supplier_propinsi."/".$data->supplier_kabupaten',
                        'filter'=>false,
                ),
		array(
                        'header'=>'Telp Office / Fax Office',
                        'name'=>'supplier_telp',
                        'value'=>'$data->supplier_telp." / ".$data->supplier_fax',
                        'filter'=>false,
                ),
		array(
                        'header'=>'Nama',
                        'name'=>'supplier_cp',
                        'value'=>'$data->supplier_cp',
                        'filter'=>false,
                ),
                array(
                        'header'=>'Phone',
                        'name'=>'supplier_cp_hp',
                        'value'=>'$data->supplier_cp_hp',
                        'filter'=>false,
                ),
                array(
                        'header'=>'E-mail',
                        'name'=>'supplier_cp_email',
                        'value'=>'$data->supplier_cp_email',
                        'filter'=>false,
                ),
                array(
                        'header'=>'Nama',
                        'name'=>'supplier_cp2',
                        'value'=>'$data->supplier_cp2',
                        'filter'=>false,
                ),
                array(
                        'header'=>'Phone',
                        'name'=>'supplier_cp_hp',
                        'value'=>'$data->supplier_cp2_hp',
                        'filter'=>false,
                ),
                array(
                        'header'=>'E-mail',
                        'name'=>'supplier_cp_email',
                        'value'=>'$data->supplier_cp2_email',
                        'filter'=>false,
                ),
                array(
                        'header'=>'Detail Produk',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("SupplierM/detailProduk",array("id"=>$data->supplier_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeDetailProduk",
                                          "onclick"=>"$(\"#dialogDetailProduk\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Detail Produk Barang Supplier",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<legend class="rim">Pencarian Supplier</legend>
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailProduk',
    'options'=>array(
        'title'=>'Detail Produk Barang Supplier',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetailProduk" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>