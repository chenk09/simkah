
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'resupplier-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        
        <table>
            <tr>
                <td>
                    <legend class="rim">Data Supplier</legend>
                    <?php echo $form->hiddenField($model, supplier_jenis,array('value'=>$_GET['modulId'])); ?>
                    <?php echo $form->textFieldRow($model,'supplier_kode',array('class'=>'span3', 'style'=>'width:100px','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_npwp',array('class'=>'span3', 'style'=>'width:140px','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textAreaRow($model,'supplier_alamat',array('rows'=>6, 'cols'=>30, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php echo $form->textFieldRow($model,'supplier_kodepos',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <div class="control-group">
                         <?php echo $form->labelEx($model,'supplier_propinsi', array('class'=>'control-label')) ?>
                        <div class="controls">
                            
                            <?php echo $form->dropDownList($model,'supplier_propinsi', CHtml::listData($model->getPropinsiItems(), 'propinsi_nama', 'propinsi_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                         <?php echo $form->labelEx($model,'supplier_kabupaten', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'supplier_kabupaten', CHtml::listData($model->getKabupatenItems(), 'kabupaten_nama', 'kabupaten_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                        </div>
                    </div>
                     <div class="control-group">
                         <?php echo CHtml::label('Telp / Fax','', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($model,'supplier_telp',array('class'=>'span2', 'placeholder'=>'Telp','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50));?>
                            <?php echo $form->textField($model,'supplier_fax',array('class'=>'span2', 'placeholder'=>'Fax','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        </div>
                    </div>
                </td>
                <td>
                    <legend class ="rim">Rekening Bank</legend>
                    <?php echo $form->textFieldRow($model,'supplier_norekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_namabank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_rekatasnama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('Mata Uang','', array('class'=>'control-label')); ?>
                        <div class="controls">
                        <?php echo $form->dropDownList($model,'supplier_matauang', CHtml::listData($model->getMataUangItems(), 'lookup_name', 'lookup_name'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Termin','', array('class'=>'control-label')); ?>
                        <div class="controls">
                        <?php echo $form->textField($model,'supplier_termin',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?> <i>Hari</i>
                        </div>
                    </div>
                    
                    <?php echo $form->textFieldRow($model,'supplier_website',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_email',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                    <div class="control-group">
                        <?php echo CHtml::label('Logo Supplier','', array('class'=>'control-label')); ?>
                        <div class="controls">
                        <?php echo Chtml::activeFileField($model,'supplier_logo',array('maxlength'=>254,'Hint'=>'Isi Jika Akan Menambahkan Logo')); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <legend class="rim">Contact Person I</legend>
                    <?php echo $form->textFieldRow($model,'supplier_cp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_cp_hp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_cp_email',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <legend class="rim">Contact Person II</legend>
                    <?php echo $form->textFieldRow($model,'supplier_cp2',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_cp2_hp',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($model,'supplier_cp2_email',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </td>
            </tr>
        </table>
                       
            <?php //echo $form->textFieldRow($model,'supplier_jenis',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
            <?php //echo $form->textFieldRow($model,'supplier_termin',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->checkBoxRow($model,'supplier_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
	<div class="form-actions">
            <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.supplierM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            <?php $this->widget('TipsMasterData',array('type'=>'create'));?>
	</div>

<?php $this->endWidget(); ?>
