
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'supplier_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->supplier_id',
                ),
		'supplier_kode',
		'supplier_nama',
		'supplier_namalain',
		'supplier_alamat',
		'supplier_propinsi',
		 array
                (
                    'header'=>'Aktif',
                    'name'=>'supplier_aktif',
                    'type'=>'raw',
                    'value'=>'($data->supplier_aktif==1)? Yii::t("mds","Yes") : Yii::t("mds","No")',
                ), 
 
        ),
    )); 
?>