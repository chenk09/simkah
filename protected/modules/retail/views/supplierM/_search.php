<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'supplier-m-search',
        'type'=>'horizontal',
)); ?>

<table>
   <tr>
       <td>
            <?php echo $form->textFieldRow($model,'supplier_kode',array('class'=>'span3','maxlength'=>10,'style'=>'width:120px')); ?>

            <?php echo $form->textFieldRow($model,'supplier_nama',array('class'=>'span3','maxlength'=>100, 'style'=>'width:170px')); ?>
       </td>
       <td>
            <div class="control-group">
                         <?php echo $form->labelEx($model,'supplier_propinsi', array('class'=>'control-label')) ?>
                        <div class="controls">
                            
                            <?php echo $form->dropDownList($model,'supplier_propinsi', CHtml::listData($model->getPropinsiItems(), 'propinsi_nama', 'propinsi_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                        </div>
                    </div>
                    
                    <div class="control-group">
                         <?php echo $form->labelEx($model,'supplier_kabupaten', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'supplier_kabupaten', CHtml::listData($model->getKabupatenItems(), 'kabupaten_nama', 'kabupaten_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                        </div>
                    </div>

            <?php echo $form->checkBoxRow($model,'supplier_aktif',array('checked'=>'checked')); ?>
       </td>
   </tr>
</table>

	

	

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
           
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.supplierM.'/informasi'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                <?php
                    $content = $this->renderPartial('../tips/informasi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                ?>
	</div>

<?php $this->endWidget(); ?>
