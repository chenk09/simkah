<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('penerimaandetail_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->penerimaandetail_id),array('view','id'=>$data->penerimaandetail_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stokobatalkes_id')); ?>:</b>
	<?php echo CHtml::encode($data->stokobatalkes_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('returdetail_id')); ?>:</b>
	<?php echo CHtml::encode($data->returdetail_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penerimaanbarang_id')); ?>:</b>
	<?php echo CHtml::encode($data->penerimaanbarang_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuankecil_id')); ?>:</b>
	<?php echo CHtml::encode($data->satuankecil_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sumberdana_id')); ?>:</b>
	<?php echo CHtml::encode($data->sumberdana_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fakturdetail_id')); ?>:</b>
	<?php echo CHtml::encode($data->fakturdetail_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('obatalkes_id')); ?>:</b>
	<?php echo CHtml::encode($data->obatalkes_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuanbesar_id')); ?>:</b>
	<?php echo CHtml::encode($data->satuanbesar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlpermintaan')); ?>:</b>
	<?php echo CHtml::encode($data->jmlpermintaan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlterima')); ?>:</b>
	<?php echo CHtml::encode($data->jmlterima); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('persendiscount')); ?>:</b>
	<?php echo CHtml::encode($data->persendiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmldiscount')); ?>:</b>
	<?php echo CHtml::encode($data->jmldiscount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('harganettoper')); ?>:</b>
	<?php echo CHtml::encode($data->harganettoper); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hargappnper')); ?>:</b>
	<?php echo CHtml::encode($data->hargappnper); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hargapphper')); ?>:</b>
	<?php echo CHtml::encode($data->hargapphper); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hargasatuanper')); ?>:</b>
	<?php echo CHtml::encode($data->hargasatuanper); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglkadaluarsa')); ?>:</b>
	<?php echo CHtml::encode($data->tglkadaluarsa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jmlkemasan')); ?>:</b>
	<?php echo CHtml::encode($data->jmlkemasan); ?>
	<br />

	*/ ?>

</div>