<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'penerimaandetail-t-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'penerimaandetail_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'stokobatalkes_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'returdetail_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'penerimaanbarang_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'satuankecil_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'sumberdana_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'fakturdetail_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'obatalkes_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'satuanbesar_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlpermintaan',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlterima',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'persendiscount',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmldiscount',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'harganettoper',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hargappnper',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hargapphper',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'hargasatuanper',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'tglkadaluarsa',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'jmlkemasan',array('class'=>'span5')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
