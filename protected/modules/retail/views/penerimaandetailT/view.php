<?php
$this->breadcrumbs=array(
	'Penerimaandetail Ts'=>array('index'),
	$model->penerimaandetail_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' PenerimaandetailT #'.$model->penerimaandetail_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' PenerimaandetailT', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' PenerimaandetailT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' PenerimaandetailT', 'icon'=>'pencil','url'=>array('update','id'=>$model->penerimaandetail_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' PenerimaandetailT','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->penerimaandetail_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' PenerimaandetailT', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'penerimaandetail_id',
		'stokobatalkes_id',
		'returdetail_id',
		'penerimaanbarang_id',
		'satuankecil_id',
		'sumberdana_id',
		'fakturdetail_id',
		'obatalkes_id',
		'satuanbesar_id',
		'jmlpermintaan',
		'jmlterima',
		'persendiscount',
		'jmldiscount',
		'harganettoper',
		'hargappnper',
		'hargapphper',
		'hargasatuanper',
		'tglkadaluarsa',
		'jmlkemasan',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>