
<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      

$this->widget($table,array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'penerimaandetail_id',
		array(
                        'header'=>'ID',
                        'value'=>'$data->penerimaandetail_id',
                ),
		'stokobatalkes_id',
		'returdetail_id',
		'penerimaanbarang_id',
		'satuankecil_id',
		'sumberdana_id',
		/*
		'fakturdetail_id',
		'obatalkes_id',
		'satuanbesar_id',
		'jmlpermintaan',
		'jmlterima',
		'persendiscount',
		'jmldiscount',
		'harganettoper',
		'hargappnper',
		'hargapphper',
		'hargasatuanper',
		'tglkadaluarsa',
		'jmlkemasan',
		*/
 
        ),
    )); 
?>