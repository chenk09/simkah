<table width="100%" class="">
    <tr>
        <td>
                <table width="100%" border="1px">
                    <tr>
                        <td><?php echo CHtml::activeLabel($model,'noterima'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->noterima; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeLabel($model,'permintaanpembelian_id'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->permintaanpembelian->nopermintaan; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('Supplier kode','supplier_id'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->supplier->supplier_kode; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('Supplier kode','supplier_id'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->supplier->supplier_kode; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activelabel($model,'tglterima'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->tglterima; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activelabel($model,'totalharga'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->totalharga; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('PO Created By',''); ?></td>
                        <td>:</td>
                        <td><?php echo $model->permintaanpembelian->loginpemakai->pegawai->nama_pegawai; ?></td>
                    </tr>
                </table>
        </td>
        <td width="50%">
            
        </td>
    </tr>
</table>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'penerimaanbarang-t-grid',
	'dataProvider'=>$modDetailbarang->searchDetailbarang(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{pager}{summary}\n{items}",
	'columns'=>array(
		////'penerimaandetail_id',
		array(
                                    'name'=>'penerimaandetail_id',
                                    'value'=>'$data->penerimaandetail_id',
                                    'filter'=>false,
                                ),
		'obatalkes.obatalkes_nama',
//		'returdetail_id',
//		'penerimaanbarang_id',
                                array(
                                    'header'=>'Unit',
                                    'value'=>'$data->satuanbesar->satuanbesar_nama',
                                ),
                                'penerimaanbarang.harganetto',
                                'penerimaanbarang.totalpajakppn',
                                'penerimaanbarang.totalharga',
		/*
		'fakturdetail_id',
		'obatalkes_id',
		'satuanbesar_id',
		'jmlpermintaan',
		'jmlterima',
		'persendiscount',
		'jmldiscount',
		'harganettoper',
		'hargappnper',
		'hargapphper',
		'hargasatuanper',
		'tglkadaluarsa',
		'jmlkemasan',
		*/
	),
)); ?>