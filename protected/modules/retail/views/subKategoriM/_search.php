<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'resub-kategori-m-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'subjenis_id',array('class'=>'span5')); ?>

	<div class="control-group">
             <?php echo $form->labelEx($model,'jenisobatalkes_id', array('class'=>'control-label')) ?>
             <div class="controls">

                <?php echo $form->dropDownList($model,'jenisobatalkes_id', CHtml::listData($model->getJenisobatalkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                          array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
             </div>
        </div>

	<?php echo $form->textFieldRow($model,'subjenis_kode',array('class'=>'span2','maxlength'=>10)); ?>

	<?php echo $form->textFieldRow($model,'subjenis_nama',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'subjenis_namalainnya',array('class'=>'span3','maxlength'=>100)); ?>

	<?php echo $form->checkBoxRow($model,'subjenis_aktif', array('checked'=>'checked')); ?>

	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
