<?php
$this->breadcrumbs=array(
	'Subjenis Ms'=>array('index'),
	$model->subjenis_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Sub Kategori Barang Supplier ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' Sub Kategori Barang ', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Sub Kategori Barang', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' Sub Kategori Barang', 'icon'=>'pencil','url'=>array('update','id'=>$model->subjenis_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' Sub Kategori Barang','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->subjenis_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Sub Kategori Barang Supplier', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'subjenis_id',
		'jenisobatalkes_id',
		'subjenis_kode',
		'subjenis_nama',
		'subjenis_namalainnya',
		'subjenis_farmasi',
                // related city displayed as a link
                 array(          
                    'header'=>'Sub Jenis Aktif',
                    'name'=>'subjenis_aktif',
                    'type'=>'raw',
                    'value'=>(($model->subjenis_aktif==1)? Yii::t('mds','Yes') : Yii::t('mds','No')),
                ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>