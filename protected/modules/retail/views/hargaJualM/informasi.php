<legend class="rim2">Informasi Produk Barang </legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gfobat-alkes-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gfobat-alkes-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
                 array(
                        'header'=>'Stock Code',
                        'name'=>'stock_code',
                        'value'=>'$data->stock_code',  
                ),
		 array(
                        'header'=>'Stock Name',
                        'name'=>'stock_name',
                        'value'=>'$data->stock_name',
                ),
                 array(
                        'header'=>'Price Name',
                        'name'=>'stock_name',
                        'value'=>'$data->stock_name', 
                ),
                array(
                        'header'=>'Barcode',
                        'name'=>'barang_barcode',
                        'value'=>'$data->barang_barcode',
                        
                ),
                array(
                        'header'=>'Harga Jual +PPN',
                        'name'=>'hargajual',
                        'value'=>'MyFunction::formatNumber($data->hargajual)'
                        
                ),
                array(
                        'header'=>'Discount',
                        'name'=>'discount',
                        'value'=>'$data->discount',
                        
                ),
                array(
                        'header'=>'PPN',
                        'name'=>'ppn_persen',
                        'value'=>'$data->ppn_persen',
                        
                ),
                array(
                      'header'=>'Harga Jual -PPN',
                      'name'=>'harganetto',
                      'value'=>'MyFunction::formatNumber($data->harganetto)'
                ),
                array(
                        'header'=>'Moving Average',
                        'name'=>'harganetto',
                        'value'=>'MyFunction::formatNumber($data->harganetto)'
                        
                ),
                array(
                        'header'=>'Margin',
                        'name'=>'hargajual',
                        'value'=>'MyFunction::formatNumber($data->hargajual - $data->harganetto)'
                        
                ),
            
                 array(
                        'header'=>'GP %',
                        'name'=>'hargajual',
                        'value'=>'$data->hargajual',
                        
                ),
                
		array(
                    'id'=>'printBandrol',
                    'class'=>'CCheckBoxColumn',
                    'selectableRows' => '50',   
                    'value'=>'$data->brg_id',
                ),
               array(
                    'type'=> 'raw',
                    'value'=> 'CHtml::hiddenField("idObat", $data->brg_id)', 
                ),

		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<legend class="rim">Pencarian Produk Barang</legend>
<?php $this->renderPartial('_searchinformasiproduk',array(
	'model'=>$model,
)); ?>