<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>


<table>
    <tr>
        <td>
            <div class="control-group">
                 <?php echo CHtml::label('Stok Code','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($modProduk,'stock_code',array('style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Barcode','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($modProduk,'barang_barcode',array('style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Stock Name','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($modProduk,'stock_name',array('style'=>'width:180px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Price Name','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($modProduk,'price_name',array('style'=>'width:180px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
        </td>
        <td>
             <div class="control-group">
                 <?php echo CHtml::label('Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                    <?php echo $form->dropDownList($modProduk,'category_id', CHtml::listData($model->getJenisObatAlkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                        'ajax'=>array('type'=>'POST',
                                                      'url'=>Yii::app()->createUrl('ActionDynamic/GetSubkategoriProduk',array('encode'=>false,'namaModel'=>'REProdukposV')),
                                                      'update'=>'#REProdukposV_subcategory_id'))); ?>
                 </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Sub Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                     <?php echo $form->dropDownList($modProduk,'subcategory_id', array(), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", )); ?>
                 </div>
            </div>
        </td>
    </tr>
</table>

	<div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit')); 
                ?>
                
	</div>

<?php $this->endWidget(); ?>
