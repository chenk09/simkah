<?php
$this->breadcrumbs=array(
	'Gfobat Alkes Ms'=>array('index'),
	$model->obatalkes_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Produk Barang ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFObatAlkesM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' GFObatAlkesM', 'icon'=>'pencil','url'=>array('update','id'=>$model->obatalkes_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' GFObatAlkesM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->obatalkes_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Produk Barang', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'obatalkes_id',
		'lokasigudang.lokasigudang_nama',
		'therapiobat.therapiobat_nama',
		'pbf.pbf_nama',
		'generik.generik_nama',
		'satuanbesar.satuanbesar_nama',
		'sumberdana.sumberdana_nama',
		'satuankecil.satuankecil_nama',
		'jenisobatalkes.jenisobatalkes_nama',
		'obatalkes_kode',
		'obatalkes_nama',
		'obatalkes_golongan',
		'obatalkes_kategori',
		'obatalkes_kadarobat',
		'kemasanbesar',
		'kekuatan',
		'satuankekuatan',
		'harganetto',
		'hargajual',
                'ppn_persen',
		'discount',
		'tglkadaluarsa',
		'minimalstok',
		'formularium',
		'discountinue',
                'obatalkes_namalain',
		array(            
                                            'label'=>'Aktif',
                                            'type'=>'raw',
                                            'value'=>(($model->obatalkes_aktif==1)? ''.Yii::t('mds','Yes').'' : ''.Yii::t('mds','No').''),
                                        ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>