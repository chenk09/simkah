<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'reproduk-barang-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>
<legend class="rim">Data Produk Barang</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table>
            <tr>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Stock Code','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_kode',array('empty'=>'-- Pilih --','class'=>'span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Barcode','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_barcode',array('empty'=>'-- Pilih --','class'=>'span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Stock Name','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_nama',array('empty'=>'-- Pilih --','class'=>'span3',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Price Name','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_namalain',array('empty'=>'-- Pilih --','class'=>'span3',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    
                    <div class="control-group">
                         <?php echo CHtml::label('Category Name','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'jenisobatalkes_id', CHtml::listData($model->getJenisObatAlkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetSubkategori',array('encode'=>false,'namaModel'=>'REProdukBarangM')),
                                                              'update'=>'#REProdukBarangM_subjenis_id'))); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Sub Category Name','', array('class'=>'control-label')) ?>
                         <div class="controls">

                             <?php echo $form->dropDownList($model,'subjenis_id', array(), 
                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", )); ?>
                         </div>
                    </div>
                    
                </td>
                <td>
                    <div class="control-group">
                          <div class='control-group'>
                        <div class = 'control-label'>Active Date</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'activedate',
                                'mode' => 'datetime',
                                //                                          'maxDate'=>'d',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Jumlah Isi','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'kekuatan',array('empty'=>'-- Pilih --','style'=>'width:80px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                             <?php echo $form->textField($model,'satuankekuatan',array('placeholder'=>'Satuan','empty'=>'-- Pilih --','style'=>'width:100px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Jml Dlm Kemasan','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'kemasanbesar',array('empty'=>'-- Pilih --','style'=>'width:80px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                     <div class="control-group">
                         <?php echo CHtml::label('Satuan Besar','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'satuanbesar_id', CHtml::listData($model->getSatuanBesarItems(), 'satuanbesar_id', 'satuanbesar_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                   <div class="control-group">
                        <?php echo CHtml::label('Satuan Kecil','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'satuankecil_id', CHtml::listData($model->getSatuanKecilItems(), 'satuankecil_id', 'satuankecil_nama'), 
                                                      array('empty'=>'-- Pilih --','style'=>'width:140px','onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Minimal Transaksi','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'mintransaksi', 
                                                      array('empty'=>'-- Pilih --','1'=>'Ya','0'=>"Tidak")); ?>
                         </div>
                    </div>
                </td>
            </tr>
        </table>
        <legend class="rim">Harga Produk</legend>
        <table>
            <tr>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Netto','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'harganetto',array('onkeyup'=>'HitungHargaNettoPPN();','class'=>'inputFormTabel currency span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <div class="controls">

                            <?php echo CHtml::checkBox('cekharganettoppn',true,
                                    array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'ViewPPN()')) ?>PPN
                            <?php echo $form->textField($model,'ppn_persen',array('onkeyup'=>'hitungHargaJual();','style'=>'width:50px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> %
                         </div>
                    </div>
                        
                                                      
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Jual','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'hargajual',array('onkeyup'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                             
                        <?php echo CHtml::checkBox('cekhargajualppn',true, 
                                array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'HitungHargaNettoPPN()')) ?>+PPN
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Margin','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('margin','',array('onkeyup'=>'HitungHargaNettoPPN();','empty'=>'-- Pilih --','style'=>'width:50px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> GP
                         
                             <?php echo CHtml::textField('gp','',array('onkeyup'=>'HitungHargaNettoPPN();','empty'=>'-- Pilih --','style'=>'width:50px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> %
                         </div>
                    </div> 
                </td>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Maksimal','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargamaksimal','',array('onkeyup'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> 
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Minimal','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargaminimal','',array('onkeyup'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> 
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Rata-rata','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargaratarata','',array('onkeyup'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?> 
                         </div>
                    </div> 
                </td>
            </tr>
        </table>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.produkBarangM.'/create'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
            $content = $this->renderPartial('../tips/transaksiProdukRetail',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
           </div>

<?php $this->endWidget(); ?>


<div id="testForm"></div>

<script type="text/javascript">
$('.currency').each(function(){this.value = formatUang(this.value)});

function hitungHargaJual()
{
    if ($("#cekhargajualppn").is(":checked")) {
    var hargaNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
    var ppn = 10;
    var jmlharganetto = hargaNetto;
    var hargappn = (100 + ppn) / 100;
//    alert(uangDiterima);
    
    var hargaJual = hargaNetto * hargappn ;
//    var totaluangkembalian = formatNumber(uangKembalian);
    
    $('#REProdukBarangM_hargajual').val(formatUang(hargaJual));
    $('#hargamaksimal').val(formatUang(jmlharganetto));
    $('#hargaminimal').val(formatUang(jmlharganetto));
    $('#hargaratarata').val(formatUang(jmlharganetto));
   
//    $('#REProdukBarangM_ppn_persen').val(formatUang(ppn));
    }else{
        $('#REProdukBarangM_hargajual').val(formatUang(jmlharganetto));
    }
     
}

function enableInputKartu()
{
    if($('#pakeKartu').is(':checked'))
        $('#divDenganKartu').show();
    else 
        $('#divDenganKartu').hide();
    if($('#BKTandabuktibayarUangMukaT_dengankartu').val() != ''){
        //alert('isi');
        $('#BKTandabuktibayarUangMukaT_bankkartu').removeAttr('readonly');
        $('#BKTandabuktibayarUangMukaT_nokartu').removeAttr('readonly');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#BKTandabuktibayarUangMukaT_bankkartu').attr('readonly','readonly');
        $('#BKTandabuktibayarUangMukaT_nokartu').attr('readonly','readonly');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').attr('readonly','readonly');
        
        $('#BKTandabuktibayarUangMukaT_bankkartu').val('');
        $('#BKTandabuktibayarUangMukaT_nokartu').val('');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').val('');
    }
}

function hitungJmlBayar()
{
    var jmlNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
    var jmlppn = 10;
    var jmlhargappn = (100 + jmlppn) / 100;
    totHargaJual = jmlNetto * jmlhargappn ;
    
    $('#REProdukBarangM').val(formatUang(totHargaJual));
    hitungHargaJual();
}

function ubahCaraPembayaran(obj)
{
    if(obj.value == 'CICILAN'){
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').removeAttr('readonly');
    } else {
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').attr('readonly', true);
        hitungJmlBayar();
    }
    
    if(obj.value == 'TUNAI'){
        hitungJmlBayar();
    } 
}

function printKasir(idTandaBukti)
{
    if(idTandaBukti!=''){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    }     
}

function cekInput()
{
    if($('#BKTandabuktibayarUangMukaT_sebagaipembayaran_bkm').val() == ''){
        alert('"Sebagai Pembayaran" tidak boleh kosong!');
        $('#BKTandabuktibayarUangMukaT_sebagaipembayaran_bkm').addClass('error');
        return false;
    } 
    
    if($('#BKPendaftaranT_pendaftaran_id').val() == ''){
        alert('Belum input Pasien');
        $('#BKPasienM_no_rekam_medik').focus();
        return false;
    }
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>
<?php
$urlGetPendidikanpegawai = Yii::app()->createUrl('actionAjax/GetPendidikanpegawai');
$urlGetPegawaidiklat = Yii::app()->createUrl('actionAjax/GetPegawaidiklat');
$urlGetPengalamankerja = Yii::app()->createUrl('actionAjax/GetPengalamankerja');
$urlGetPegawaijabatan = Yii::app()->createUrl('actionAjax/GetPegawaijabatan');
$urlGetPegmutasi = Yii::app()->createUrl('actionAjax/GetPegmutasi');
$urlGetPegawaicuti = Yii::app()->createUrl('actionAjax/GetPegawaicuti');
$urlGetIjintugasbelajar = Yii::app()->createUrl('actionAjax/GetIjintugasbelajar');
$urlGetHukdisiplin = Yii::app()->createUrl('actionAjax/GetHukdisiplin');
$js= <<< JS
    

    function HitungHargaNettoPPN() {
    
         if ($("#cekhargajualppn").is(":checked")) {
            var hargaNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
             var hargaratarata = unformatNumber($('#hargaratarata').val());
                var ppn = 10;
                var jmlharganetto = hargaNetto;
                var hargappn = (100 + ppn) / 100;
            //    alert(uangDiterima);

                var hargaJual = hargaNetto * hargappn ;
            //    var totaluangkembalian = formatNumber(uangKembalian);
                var margin = hargaJual - hargaratarata;
                var jmlmargin = margin;
                var gp = hargaJual/margin;
                var jmlgp = gp;
                $('#REProdukBarangM_hargajual').val(formatUang(hargaJual));
                $('#hargamaksimal').val(formatUang(jmlharganetto));
                $('#hargaminimal').val(formatUang(jmlharganetto));
                $('#hargaratarata').val(formatUang(jmlharganetto));
                $('#REProdukBarangM_ppn_persen').val(formatUang(ppn));
                $('#margin').val(formatUang(jmlmargin));
                $('#gp').val(formatUang(jmlgp));
        }else {
            var hargaNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
                var ppn = 0;
                var jmlharganetto = hargaNetto;
                var hargappn = (100 + ppn) / 100;
            //    alert(uangDiterima);

                var hargaJual = hargaNetto ;
            //    var totaluangkembalian = formatNumber(uangKembalian);
            
           $('#REProdukBarangM_hargajual').val(formatUang(hargaJual));
           $('#hargamaksimal').val(formatUang(jmlharganetto));
           $('#hargaminimal').val(formatUang(jmlharganetto));
           $('#hargaratarata').val(formatUang(jmlharganetto));
           $('#REProdukBarangM_ppn_persen').val(formatUang(ppn));
        }
    }
    
    function ViewPPN() {
    
         if ($("#cekharganettoppn").is(":checked")) {
            var hargaNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
                var ppn = 10;
                var jmlharganetto = hargaNetto;
                var hargappn = (100 + ppn) / 100;
            //    alert(uangDiterima);

                var hargaJual = hargaNetto * hargappn ;
            //    var totaluangkembalian = formatNumber(uangKembalian);

                $('#REProdukBarangM_ppn_persen').val(formatUang(ppn));
        }else {
            var hargaNetto = unformatNumber($('#REProdukBarangM_harganetto').val());
                var ppn = 0;
                var jmlharganetto = hargaNetto;
                var hargappn = (100 + ppn) / 100;
            //    alert(uangDiterima);

                var hargaJual = hargaNetto ;
            //    var totaluangkembalian = formatNumber(uangKembalian);
            
           $('#REProdukBarangM_ppn_persen').val(formatUang(ppn));
        }
    }
// ========================== Akhir script Hukuman disiplin ===================================
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>