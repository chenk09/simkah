<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<fieldset>  
    <?php 
    Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('reharga-jual-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

    <legend class="rim">Pencarian Produk</legend>
<?php $this->renderPartial('_search',array(
	'model'=>$model,'modProduk'=>$modProduk,
)); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'reharga-jual-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>

	<?php 
        if (isset($modDetails)){ echo $form->errorSummary($modDetails); }
        ?>
	<?php echo $form->errorSummary($model); ?>
<div style='max-height:300px;overflow-y: scroll;'>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'reharga-jual-m-grid',
	'dataProvider'=>$modProduk->search(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Ubah Margin</center>',
                'start'=>11, //indeks kolom 3
                'end'=>13, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                ),
		 array(
                        'header'=>'Category / <br> Sub Category',
                        'value'=>'$data->category_name." / ".$data->subcategory_name'  
                ),
		 array(
                        'header'=>'Stock Code / <br> Barcode ',
                        'value'=>'$data->stock_code." / ".$data->barang_barcode'
                ),
                 array(
                        'header'=>'Stock Name / <br> Price Name',
                        'value'=>'$data->stock_name." / ".$data->stock_name' 
                ),
                array(
                        'header'=>'Harga Jual +PPN',
                        'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][hargajual]", $data->hargajual, array("onkeyup"=>"HitungHargaTanpaPPN(this);","id"=>"hargajual","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'Discount(%)',
                         'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][discount]", $data->discount, array("onkeyup"=>"HitungDenganDiskon(this);","id"=>"discount","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'PPN(%)',
                        'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][ppn]", $data->ppn_persen, array("id"=>"ppn","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))."".$data->ppn_persen',
                        'type'=>'raw',
                ),
                array(
                        'header'=>'Harga Jual -PPN',
                        'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][harganetto]", $data->harganetto, array("id"=>"harganetto","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))',
                        'type'=>'raw',
                ),
                 array(
                        'header'=>'Moving Average',
                         'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][ratarata]", $data->movingavarage, array("id"=>"hargaratarata","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))."".$data->movingavarage',
                        'type'=>'raw',
                        
                ),
                array(
                        'header'=>'Margin',
                         'value'=>'CHtml::hiddenField("REHargaJualM[$data->brg_id][jmlmargin]", $data->margin, array("id"=>"marginlama","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))."".$data->margin',
                         'type'=>'raw',
                ),
                array(
                        'header'=>'GP %',
                        'value'=>'$data->gp_persen',
                        
                ),
                array(
                    'header'=>'Margin',
                    'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][margin]",$data->margin, array("id"=>"margin","onkeyup"=>"HargaJual(this);","onkeypress"=>"return $(this).focusNextInputField(this,event)","class"=>"span1 numbersOnly margin"))',
                    'type'=>'raw',
                ),
                array(
                    'header'=>'GP %',
                    'value'=>'CHtml::textField("REHargaJualM[$data->brg_id][gp]", $data->gp_persen, array("id"=>"gp","onkeyup"=>"HargaJualGp(this);","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span1 numbersOnly margin"))',
                    'type'=>'raw',
                ),
                
            
	),
        'afterAjaxUpdate'=>'
            function(id, data){
                jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
                $("#reharga-jual-m-grid .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
        }',
)); ?>
            </div>
    
            
	<div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.$this->id.'/create'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	</div>


<?php $this->endWidget(); ?>
        </fieldset>

<?php 
Yii::app()->clientScript->registerScript('onheadfunction','
function slide(data){
    $("."+data).slideToggle();
}

function openDialogini(){
    $("#dialogDetails").dialog("open");
}
    
function setVolume(){
    var value = $("#fisiks").val();
        $("#reharga-jual-m-grid").find(".margin").each(function(){
        $(this).val(value);
        value++;
    });
}
    
function HargaJual(obj){
    parent = $(obj).parents("tr");
    $("#reharga-jual-m-grid").find(".margin").each(function(){
    
        var margin = parseFloat(unformatNumber(parent.find("#margin").val()));
        var harganetto = parseFloat(unformatNumber(parent.find("#harganetto").val()));
        var hargajual = parseFloat(unformatNumber(parent.find("#hargajual").val()));
        var hargaratarata = parseFloat(unformatNumber(parent.find("#hargaratarata").val()));
        var gp = parseFloat(unformatNumber(parent.find("#gp").val()));
        var ppn = parseFloat(unformatNumber(parent.find("#ppn").val()));

        var jmlppn = (100 + ppn) / 100;
        var jmlhargappn = (hargajual * ppn ) / 100;
        var harganettoplusppn = harganetto * jmlppn;
        
        var jmlhargajual = margin + harganetto;
        var jmlgp = (margin / hargajual) * 100;
        var jmlhargamargin = hargajual - harganetto;

        parent.find("#harganetto").val(formatUang(jmlhargajual));
        parent.find("#gp").val(formatUang(jmlgp));
    });
}
    
function HargaJualGp(obj){
    parent = $(obj).parents("tr");
    $("#reharga-jual-m-grid").find(".margin").each(function(){
    
        var margin = parseFloat(unformatNumber(parent.find("#margin").val()));
        var marginlama = parseFloat(unformatNumber(parent.find("#marginlama").val()));
        var harganetto = parseFloat(unformatNumber(parent.find("#harganetto").val()));
        var hargajual = parseFloat(unformatNumber(parent.find("#hargajual").val()));
        var hargaratarata = parseFloat(unformatNumber(parent.find("#hargaratarata").val()));
        var gp = parseFloat(unformatNumber(parent.find("#gp").val()));
        var ppn = parseFloat(unformatNumber(parent.find("#ppn").val()));


        var jmlhargajualgp = marginlama / (gp/100);
        var jmlmargin = hargajual - harganetto;
        var harganettoplusppn = harganetto * jmlppn;
        var jmlppn = (100 + ppn) / 100;

        parent.find("#hargajual").val(formatUang(jmlhargajualgp));
        parent.find("#margin").val(formatUang(jmlmargin));

    });

}
    
function HitungHargaTanpaPPN(obj){
    parent = $(obj).parents("tr");
    $("#reharga-jual-m-grid").find(".margin").each(function(){
    
        var margin = parseFloat(unformatNumber(parent.find("#margin").val()));
        var harganetto = parseFloat(unformatNumber(parent.find("#harganetto").val()));
        var hargajual = parseFloat(unformatNumber(parent.find("#hargajual").val()));
        var hargaratarata = parseFloat(unformatNumber(parent.find("#hargaratarata").val()));
        var gp = parseFloat(unformatNumber(parent.find("#gp").val()));
        var ppn = parseFloat(unformatNumber(parent.find("#ppn").val()));
        var diskon = parseFloat(unformatNumber(parent.find("#diskon").val()));

        var jmlppn = (100 + ppn) / 100;
        var hargatanpappn = (hargajual - diskon) / (1 + ppn /100);
        var harganettoplusppn = harganetto * jmlppn; 
        var jmlmargin = hargajual - harganettoplusppn;
        var jmlhargappn = (hargajual * ppn ) / 100;
        var harganettoplusppn = harganetto * jmlppn;
        var harganettononppn = hargajual - jmlhargappn;

        parent.find("#margin").val(formatUang(jmlmargin));
        var hargagp = (margin / harganetto)*100;
        parent.find("#gp").val(formatUang(hargagp));
        parent.find("#harganetto").val(formatUang(harganettononppn));
    });

}
    
    
function HitungDenganDiskon(obj){
    parent = $(obj).parents("tr");
    $("#reharga-jual-m-grid").find(".margin").each(function(){
    
        var margin = parseFloat(unformatNumber(parent.find("#margin").val()));
        var disc = parseFloat(unformatNumber(parent.find("#discount").val()));
        var harganetto = parseFloat(unformatNumber(parent.find("#harganetto").val()));
        var hargajual = parseFloat(unformatNumber(parent.find("#hargajual").val()));
        

        var jmldisc = disc / 100;
        var jmlharganetto =( harganetto - (harganetto * jmldisc));
        var jmlmargin = hargajual - jmlharganetto;
        var jmlgp = (margin / harganetto) * 100;
        
        parent.find("#margin").val(formatUang(jmlmargin));
        parent.find("#gp").val(formatUang(jmlgp));
    });

}

function cekInput()
{

    $(".margin").each(function(){this.value = unformatNumber(this.value)});
    return true;
}
', CClientScript::POS_HEAD); ?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.numbersOnly',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
