<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'search',
    'type' => 'horizontal',
        ));
?>
<table>
    <tr>
        <td width="50%">
           <div class="control-group ">
                <label class='control-label'>
                    <?php echo CHtml::label('Stock Code','');?>
                    </label>
                <div class="controls">
                      <?php echo $form->textField($modProduk, 'stock_code', array('class' => 'span2', 'maxlength' => 50)); ?>
                </div>
            </div>
            <div class="control-group ">
                <label class='control-label'>
                    <?php echo CHtml::label('Barcode','');?>
                    </label>
                <div class="controls">
                      <?php echo $form->textField($modProduk, 'barang_barcode', array('class' => 'span2', 'maxlength' => 50)); ?>
                </div>
            </div>
            <div class="control-group ">
                <label class='control-label'>
                    <?php echo CHtml::label('Stock Name','');?>
                    </label>
                <div class="controls">
                      <?php echo $form->textField($modProduk, 'stock_name', array('class' => 'span3', 'maxlength' => 50)); ?>
                </div>
            </div>
             <div class="control-group ">
                <label class='control-label'>
                    <?php echo CHtml::label('Price Name','');?>
                    </label>
                <div class="controls">
                      <?php echo $form->textField($modProduk, 'price_name', array('class' => 'span3', 'maxlength' => 50)); ?>
                </div>
            </div>
        </td>
        <td>
            <div class="control-group">
                 <?php echo CHtml::label('Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                    <?php echo $form->dropDownList($modProduk,'category_id', CHtml::listData($modProduk->getJenisObatAlkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                        'ajax'=>array('type'=>'POST',
                                                      'url'=>Yii::app()->createUrl('ActionDynamic/GetSubkategoriProduk',array('encode'=>false,'namaModel'=>'REProdukposV')),
                                                      'update'=>'#REProdukposV_subcategory_id'))); ?>
                 </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Sub Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                     <?php echo $form->dropDownList($modProduk,'subcategory_id', array(), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", )); ?>
                 </div>
            </div>
        </td>
    </tr>
</table>

<div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
</div>

<?php $this->endWidget(); ?>