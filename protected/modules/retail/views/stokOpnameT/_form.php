<?php if (($model->isNewRecord)&&(!isset($modFormulir))) { ?>
<?php $this->renderPartial('_obatalkes', array('model' => $modObat, 'modProduk'=>$modProduk)); ?>
<?php } ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
    Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').slideToggle();
	return false;
});
$('#search').submit(function(){
	$.fn.yiiGridView.update('obatalkes-m-grid', {
		data: $(this).serialize()
	});
                getTotal();
	return false;
});
");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'restokopname-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model,$modDetails); ?>
        <?php $this->renderPartial('_listObat', array('model' => $modObat, 'modDetails'=>$modDetails,'modProduk'=>$modProduk)); ?>
            <table>
                <tr>
                    <td>
                        <?php echo $form->dropDownListRow($model,'jenisstokopname', Jenisstokopname::items(), array('empty'=>'-- Pilih --', 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'tglstokopname', array('class' => 'control-label')) ?>
                            <div class="controls">
                                <?php
                                $this->widget('MyDateTimePicker', array(
                                    'model' => $model,
                                    'attribute' => 'tglstokopname',
                                    'mode' => 'date',
                                    'options' => array(
                                        'dateFormat' => Params::DATE_TIME_FORMAT,
                                    ),
                                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                                    ),
                                ));
                                ?>
                            </div>
                        </div>    
                        <?php echo $form->textFieldRow($model,'nostokopname',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        <?php //echo $form->textFieldRow($model,'mengetahui_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'mengetahui_id', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $form->hiddenField($model, 'mengetahui_id'); ?>
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$model,
                                    'attribute' => 'mengetahui_nama',
                                    'source' => 'js: function(request, response) {
                                                       $.ajax({
                                                           url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                                           dataType: "json",
                                                           data: {
                                                               term: request.term,
                                                           },
                                                           success: function (data) {
                                                                   response(data);
                                                           }
                                                       })
                                                    }',
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'minLength' => 2,
                                        'focus' => 'js:function( event, ui ) {
                                                                                    $(this).val( ui.item.label);
                                                                                    return false;
                                                                                }',
                                        'select' => 'js:function( event, ui ) {
                                                                                    $("#'.Chtml::activeId($model, 'mengetahui_id') . '").val(ui.item.pegawai_id); 
                                                                                    return false;
                                                                                }',
                                    ),
                                    'htmlOptions' => array(
                                        'class'=>'namaPegawai',
                                        'onkeypress' => "return $(this).focusNextInputField(event)",
                                    ),
                                    'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'openDialog("'.Chtml::activeId($model, 'mengetahui_id').'");'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'peg_menyetujui_id'); ?>
                            </div>
                        </div>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'petugas1_id', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $form->hiddenField($model, 'petugas1_id'); ?>
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$model,
                                    'attribute' => 'petugas1_nama',
                                    'source' => 'js: function(request, response) {
                                                       $.ajax({
                                                           url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                                           dataType: "json",
                                                           data: {
                                                               term: request.term,
                                                           },
                                                           success: function (data) {
                                                                   response(data);
                                                           }
                                                       })
                                                    }',
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'minLength' => 2,
                                        'focus' => 'js:function( event, ui ) {
                                                                                    $(this).val( ui.item.label);
                                                                                    return false;
                                                                                }',
                                        'select' => 'js:function( event, ui ) {
                                                                                    $("#'.Chtml::activeId($model, 'petugas1_id') . '").val(ui.item.pegawai_id); 
                                                                                    return false;
                                                                                }',
                                    ),
                                    'htmlOptions' => array(
                                        'class'=>'namaPegawai',
                                        'onkeypress' => "return $(this).focusNextInputField(event)",
                                    ),
                                    'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'openDialog("'.Chtml::activeId($model, 'petugas1_id').'");'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'petugas1_id'); ?>
                            </div>
                        </div>
                        <div class="control-group ">
                            <?php echo $form->labelEx($model, 'petugas2_id', array('class' => 'control-label')); ?>
                            <div class="controls">
                                <?php echo $form->hiddenField($model, 'petugas2_id'); ?>
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$model,
                                    'attribute' => 'petugas2_nama',
                                    'source' => 'js: function(request, response) {
                                                       $.ajax({
                                                           url: "' . Yii::app()->createUrl('ActionAutoComplete/getPegawai') . '",
                                                           dataType: "json",
                                                           data: {
                                                               term: request.term,
                                                           },
                                                           success: function (data) {
                                                                   response(data);
                                                           }
                                                       })
                                                    }',
                                    'options' => array(
                                        'showAnim' => 'fold',
                                        'minLength' => 2,
                                        'focus' => 'js:function( event, ui ) {
                                                                                    $(this).val( ui.item.label);
                                                                                    return false;
                                                                                }',
                                        'select' => 'js:function( event, ui ) {
                                                                                    $("#'.Chtml::activeId($model, 'petugas2_id') . '").val(ui.item.pegawai_id); 
                                                                                    return false;
                                                                                }',
                                    ),
                                    'htmlOptions' => array(
                                        'class'=>'namaPegawai',
                                        'onkeypress' => "return $(this).focusNextInputField(event)",
                                    ),
                                    'tombolDialog' => array('idDialog' => 'dialogPegawai', 'jsFunction'=>'openDialog("'.Chtml::activeId($model, 'petugas2_id').'");'),
                                ));
                                ?>
                                <?php echo $form->error($model, 'petugas2_id'); ?>
                            </div>
                        </div>
                        <?php //echo $form->textFieldRow($model,'petugas1_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php //echo $form->textFieldRow($model,'petugas2_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                    <td>
                        <?php echo $form->textFieldRow($model,'totalharga',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textFieldRow($model,'totalnetto',array('class'=>'span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo $form->textAreaRow($model,'keterangan_opname',array('rows'=>4, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </td>
                </tr>
            </table>
            <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php //echo $form->textFieldRow($model,'formuliropname_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            
            <?php //echo $form->checkBoxRow($model,'isstokawal', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
            
            

            
	<div class="form-actions">
             <?php
        if (isset(Yii::app()->request->urlReferrer)){
            $back = Yii::app()->request->urlReferrer;
        }else{
            $back = Yii::app()->createUrl($this->module->id . '/' . stokOpnameT . '/stokopname');
        }
        ?>
                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                 <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.StokOpnameT.'/stokopname'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                <?php $content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php Yii::app()->clientScript->registerScript('head2', "
function openDialog(obj){
        $('#dialogPegawai').attr('parentClick',obj);
        $('#dialogPegawai').dialog('open');   
    }            
", CClientScript::POS_HEAD); ?>

<?php
//========= Dialog buat cari Bahan Diet =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogPegawai',
    'options' => array(
        'title' => 'Daftar Pegawai',
        'autoOpen' => false,
        'modal' => true,
        'width' => 750,
        'height' => 600,
        'resizable' => false,
    ),
));

$modPegawai = new REPegawaiM('search');
$modPegawai->unsetAttributes();
//$modPegawai->ruangan_id = 0;
if (isset($_GET['REPegawaiM']))
    $modPegawai->attributes = $_GET['REPegawaiM'];

$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawai-m-grid',
    'dataProvider'=>$modPegawai->searchDialog(),
    'filter'=>$modPegawai,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        ////'pegawai_id',
        
            'nama_pegawai',
            'nomorindukpegawai',
                'alamat_pegawai',
        'agama',
            array(
                'name'=>'jeniskelamin',
                'filter'=>  JenisKelamin::items(),
                'value'=>'$data->jeniskelamin',
                ),
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                    "id" => "selectBahan",
                                    "onClick" => "
                                    var parent = $(\"#dialogPegawai\").attr(\"parentclick\");
                                    $(\"#\"+parent+\"\").val($data->pegawai_id);
                                    $(\"#\"+parent+\"\").parents(\".controls\").find(\".namaPegawai\").val(\"$data->nama_pegawai\");
                                    $(\'#dialogPegawai\').dialog(\'close\');
                                    return false;"))',
        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>

<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php 
Yii::app()->clientScript->registerScript('ready', '
    $("form#restokopname-t-form").submit(function(){
        if ($("#'.CHtml::activeId($model, 'jenisstokopname').'").val() == ""){
            alert("Jenis Stok Obat Opname harus diisi");
            return false;
        }
        getTotal();
    })
', CClientScript::POS_READY); ?>

        <?php 
Yii::app()->clientScript->registerScript('heading', '
    function getTotal(){
        var totalNetto = 0;
        var totalHarga = 0;
        $(".fisik").each(function(){
            if ($(this).parents("tr").find(".cekList").is(":checked")){
                totalNetto += ((parseFloat($(this).val()))*(parseFloat($(this).parents("tr").find("#hargaNetto").val())));
                totalHarga += ((parseFloat($(this).val()))*(parseFloat($(this).parents("tr").find("#harga").val())));
            }
        });
        $("#REStokOpnameT_totalnetto").val(totalNetto);
        $("#REStokOpnameT_totalharga").val(totalHarga);
    }
',  CClientScript::POS_HEAD); 
?>
