<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'restokopname-t-search',
        'type'=>'horizontal',
)); ?>
        <table>
            <tr>
                <td>
                   <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglstokopname', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglstokopname',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
        </div>
                    <div class="control-group ">
                <?php echo $form->labelEx($model, 'tglAkhir', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </div>
                </td>
                <td>
                    <?php echo $form->textFieldRow($model,'nostokopname',array('class'=>'span3','maxlength'=>50)); ?>
                    <?php //echo $form->dropDownListRow($model,'ruangan_id',array('class'=>'span3')); ?>
                    <?php echo $form->textFieldRow($model,'mengetahui_id',array('class'=>'span3')); ?>
                </td>
            </tr>
        </table>
	<?php //echo $form->textFieldRow($model,'stokopname_id',array('class'=>'span5')); ?>

	

	<?php //echo $form->textFieldRow($model,'petugas1_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'petugas2_id',array('class'=>'span5')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
                                 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>

<?php $this->endWidget(); ?>
