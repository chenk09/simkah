<table>
    <tr>
        <td>
             <b><?php echo CHtml::encode($model->getAttributeLabel('jenisstokopname')); ?>:</b>
            <?php echo CHtml::encode($model->jenisstokopname); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('nostokopname')); ?>:</b>
            <?php echo CHtml::encode($model->nostokopname); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('tglstokopname')); ?>:</b>
            <?php echo CHtml::encode($model->tglstokopname); ?>
            <br />
            <b><?php echo CHtml::encode($model->getAttributeLabel('mengetahui_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->mengetahui->nama_pegawai)) ? $model->mengetahui->nama_pegawai : " - " )); ?>
            <br />
        </td>
        <td>
            <b><?php echo CHtml::encode($model->getAttributeLabel('petugas1_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->petugas1->nama_pegawai)) ? $model->petugas1->nama_pegawai : " - " )); ?>
            <br />
            <b><?php echo CHtml::encode($model->getAttributeLabel('petugas2_id')); ?>:</b>
            <?php echo CHtml::encode(((!empty($model->petugas2->nama_pegawai)) ? $model->petugas2->nama_pegawai : " - " )); ?>
            <br />
             <b><?php echo CHtml::encode($model->getAttributeLabel('totalharga')); ?>:</b>
            <?php echo CHtml::encode($model->totalharga); ?>
            <br />
        </td>
    </tr>   
</table>
            

<?php 
echo CHtml::css('#obatalkes-m-grid th{vertical-align:middle;}'); 
?>
<!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$modTable->searchDataObat(),
    'mergeHeaders'=>array(
            array(
                'name'=>'<center>Stok</center>',
                'start'=>6, //indeks kolom 3
                'end'=>8, //indeks kolom 4
            ),
            
        ),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            array(
                'header'=>'Category Name',
                'value'=>'$data->obatalkes->jenisobatalkes->jenisobatalkes_nama',
                ),
            array(
                'header'=>'Stock Code',
                'value'=>'$data->obatalkes->obatalkes_kode',
            ),
            array(
                'header'=>'Stock Name <br/> Price Name',
                'type'=>'raw',
                'value'=>'$data->obatalkes->obatalkes_nama.\'<br/>\'.$data->obatalkes->obatalkes_namalain',
            ),
            array(
                'header'=>'Harga Satuan',
                'type'=>'raw',
                'value'=>'$data->obatalkes->hargajual'
            ),
            array(
                'header'=>'Moving Average',
                'type'=>'raw',
                'value'=>'$data->obatalkes->movingavarage'
            ),
            array(
                'header'=>'Minimal Stok',
                'type'=>'raw',
                'value'=>'$data->obatalkes->minimalstok',
            ),
            array(
                'header'=>'Sistem',
                'type'=>'raw',
                'value'=> '$data->volume_sistem',
            ),
            array(
                'header'=>'Fisik',
                'type'=>'raw',
                'value'=> '$data->volume_fisik',
            ),
            'tglkadaluarsa',
            array(
                'header'=>'Kondisi Barang',
                'type'=>'raw',
                'value'=> '$data->kondisibarang',
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null})
                getTotal();
                }',
)); ?> 
 