<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
echo CHtml::css('#isiScroll{max-height:300px;overflow-y:scroll;margin-bottom:10px;}#obatalkes-m-grid th{vertical-align:middle;}'); 
?>
<!-- search-form -->

<div id='isiScroll'>
<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
    'id'=>'obatalkes-m-grid',
    'dataProvider'=>$modProduk->searchDataObat(),
    'mergeHeaders'=>array(
            array(
                'name'=>'<center>Stok</center>',
                'start'=>9, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
            
        ),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
            array(
                'header'=> 'Pilih',
                'type'=>'raw',
                'value'=>'
                    CHtml::activeHiddenField($data, \'[\'.$data->brg_id.\']brg_id\').
                    CHtml::checkBox(\'REProdukposV[\'.$data->brg_id.\'][cekList]\', true, array(\'onclick\'=>\'setUrutan()\', \'class\'=>\'cekList\', \'onclick\'=>\'getTotal();\',\'onkeypress\'=>\'return $(this).focusNextInputField(event)\'));
                    ',
            ),
            array(
                'header'=>'Category Name',
                'name'=>'category_name',
                'value'=>'$data->category_name',
                ),
            array(
                'header'=>'Sub Categori Name',
                'name'=>'subcategory_name',
                'value'=>'$data->subcategory_name',
                ),
            array(
                'header'=>'Stock Code',
                'name'=>'stock_code',
                'value'=>'$data->stock_code',
                ),
            array(
                'header'=>'Stock Name <br/> Price Name',
                'type'=>'raw',
                'value'=>'$data->stock_name.\'<br/>\'.$data->price_name',
            ),
            array(
                'header'=>'Harga Satuan',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))'
            ),
            array(
                'header'=>'Harga Netto',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'hargaNetto\', $data->harganetto, array(\'class\'=>\'span1 netto\', \'readonly\'=>true))'
            ),
            array(
                'header'=>'Moving Average',
                'type'=>'raw',
                'value'=>'CHtml::textField(\'movingAverage\', $data->movingavarage, array(\'class\'=>\'span1 avarage\', \'readonly\'=>true))'
            ),
//            array(
//                'header'=>'Harga Satuan <br/> harga Netto',
//                'type'=>'raw',
//                'value'=>'CHtml::textField(\'harga\', $data->hargajual, array(\'class\'=>\'span1 harga\', \'readonly\'=>true))
//                            .\'<br/>\'.
//                          CHtml::textField(\'hargaNetto\', $data->harganetto, array(\'class\'=>\'span1 netto\', \'readonly\'=>true))'
//            ),
            'minimalstok',
            array(
                'header'=>'Sistem',
                'type'=>'raw',
                'value'=> 'CHtml::textField(\'REProdukposV[\'.$data->brg_id.\'][volume_sistem]\', StokobatalkesT::getStokBarang($data->brg_id, '.Yii::app()->user->getState('ruangan_id').'), array(\'class\'=>\'stok span1\', \'readonly\'=>true))',
            ),
            array(
                'header'=>'<div class="test" style="cursor:pointer;" onclick="openDialogini()"> Fisik <icon class="icon-list"></icon></div> ',
                'type'=>'raw',
                'value'=> 'CHtml::textField(\'REProdukposV[\'.$data->brg_id.\'][volume_fisik]\', 0  , array(\'class\'=>\'fisik span1 numbersOnly\', \'onblur\'=>\'getTotal();\', \'onkeypress\'=>\'return $(this).focusNextInputField(event)\',\'id\'=>\'volume_fisik\'))',
            ),
            'activedate',
            array(
                'header'=>'Kondisi Barang',
                'type'=>'raw',
                'value'=> 'CHtml::dropDownList(\'REProdukposV[\'.$data->brg_id.\'][kondisibarang]\', \'\', Inventarisasikeadaan::items(), array(\'class\'=>\'span2\',\'onkeypress\'=>\'return $(this).focusNextInputField(event)\'))',
            ),
    ),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $("#isiScroll .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null})
                getTotal();
                }',
)); ?> 
    </div>
<?php
// ===========================Dialog Details Tarif=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogDetails',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Volume Fisik',
                        'autoOpen'=>false,
                        'width'=>170,
                        'height'=>140,
                        'resizable'=>false,
                        'scroll'=>false,
                            'modal'=>true
                         ),
                    ));
?>
<div class="awawa" width="100%" height="100%">
    <?php echo CHtml::textField('fisiks', 0, array('class'=>'numbersOnly span2')); ?>
    <?php echo CHtml::button('submit', array('class'=>'btn btn-primary', 'onclick'=>'setVolume();', 'id'=>'submitJumlahVolume')); ?>
</div>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');

Yii::app()->clientScript->registerScript('openDialog','
    function openDialogini(){
        $("#dialogDetails").dialog("open");
    }
    
    function setVolume(){
        var value = $("#fisiks").val();
        $("#obatalkes-m-grid").find(".fisik").each(function(){
            $(this).val(value);
        });
        getTotal();
    }
',  CClientScript::POS_HEAD);
?>
<script>
    $('#obatalkes-m-grid').children('.table').children('tbody').attr('id','tableSd');
</script>
