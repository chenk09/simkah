<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').slideToggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('obatalkes-m-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<legend class="rim">Pencarian Barang</legend>
<?php $this->renderPartial('_searchObat',array(
    'model'=>$model, 'modBarang'=>$modBarang, 'modObat'=>$modObat, 'modProduk'=>$modProduk
)); ?>


