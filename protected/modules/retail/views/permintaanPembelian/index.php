<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>1,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number0',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'purchase-order-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); 
$this->widget('bootstrap.widgets.BootAlert'); 
echo $form->errorSummary(array($modPermintaanPembelian,$modPermintaanDetail));
?>

<fieldset>
    <legend class="rim">Purchase Order</legend>
    <table>
        <tr>
            <td>
                <?php echo $form->textFieldRow($modPermintaanPembelian,'nopermintaan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                <?php //echo $form->textFieldRow($modPermintaanPembelian,'tglpermintaanpembelian',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPermintaanPembelian,'tglpermintaanpembelian', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'tglpermintaanpembelian',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                        )); 
                                 ?>
                    </div>
                </div>

                <?php //echo $form->textFieldRow($modPermintaanPembelian,'supplierCode',array('class'=>'span3')); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPermintaanPembelian,'supplierCode', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$modPermintaanPembelian,
                            'attribute'=>'supplierCode',
                            'sourceUrl'=> Yii::app()->createUrl('retail/permintaanPembelian/autoCompleteSupplierCode'),
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 2,
                               'focus'=> 'js:function( event, ui ) {
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierCode').'").val( ui.item.value );
                                    return false;
                                }',
                               'select'=>'js:function( event, ui ) {
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierName').'").val(ui.item.supplier_nama);
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplier_id').'").val(ui.item.supplier_id);
                                    $("#termin").val(ui.item.supplier_termin);
                                    return false;
                                }',

                            ),
                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly'),
                            //'tombolDialog'=>array('idDialog'=>'dialogDaftarPasien','idTombol'=>'tombolPasienDialog'),
                        )); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modPermintaanPembelian,'supplierName',array('class'=>'span3')); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPermintaanPembelian,'supplierName', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $this->widget('MyJuiAutoComplete',array(
                            'model'=>$modPermintaanPembelian,
                            'attribute'=>'supplierName',
                            'sourceUrl'=> Yii::app()->createUrl('retail/permintaanPembelian/autoCompleteSupplierName'),
                            'options'=>array(
                               'showAnim'=>'fold',
                               'minLength' => 2,
                               'focus'=> 'js:function( event, ui ) {
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierName').'").val( ui.item.value );
                                    return false;
                                }',
                               'select'=>'js:function( event, ui ) {
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierCode').'").val(ui.item.supplier_kode);
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplier_id').'").val(ui.item.supplier_id);
                                    $("#termin").val(ui.item.supplier_termin);
                                    return false;
                                }',

                            ),
                            'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly'),
                            'tombolDialog'=>array('idDialog'=>'dialogSupplier','idTombol'=>'tombolDialogSupplier'),
                        )); ?>
                    </div>
                </div>
                <?php echo $form->hiddenField($modPermintaanPembelian,'supplier_id',array('readonly'=>'true', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>

                <?php //echo $form->textFieldRow($modPermintaanPembelian,'tglterimabarang',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPermintaanPembelian,'tglterimabarang', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modPermintaanPembelian,
                                                'attribute'=>'tglterimabarang',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    //'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2-5'),
                        )); 
                                 ?>
                    </div>
                </div>

                <?php echo $form->dropDownListRow($modPermintaanPembelian,'syaratbayar_id',
                                                                   CHtml::listData($modPermintaanPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                                   array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                                   'empty'=>'-- Pilih --',)); ?>
                <div class="control-group ">
                    <?php echo CHtml::label('Term Of Payments','termin', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('termin','',array('class'=>'span1','readonly'=>true)); ?>
                    </div>
                </div>
            </td>
            <td>
                <?php echo $form->textAreaRow($modPermintaanPembelian,'keteranganpermintaan',array('class'=>'span3')); ?>
                <?php echo $form->textAreaRow($modPermintaanPembelian,'alamatpengiriman',array('class'=>'span3')); ?>
            </td>
        </tr>
    </table>
</fieldset>

<fieldset>
    <legend class="rim">Pemilihan Barang</legend>
    <div class="control-group ">
        <?php echo CHtml::label('Stock Code','stockCode', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $this->widget('MyJuiAutoComplete',array(
                'name'=>'stockCode',
                'value'=>'',
//                'sourceUrl'=> Yii::app()->createUrl('retail/permintaanPembelian/autoCompleteStockCode'),
                'source'=>'js: function(request, response) {
                    $.ajax({
                        url: "'.Yii::app()->createUrl('retail/permintaanPembelian/autoCompleteStockCode').'",
                        dataType: "json",
                        data: {
                            term: request.term,
                            supplier_id: $("#'.CHtml::activeId($modPermintaanPembelian, 'supplier_id').'").val()
                        },
                        success: function (data) {
                                response(data);
                                if(data[0].error!=""){
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierCode').'").focus();
                                    alert(data[0].error);
                                    $("#stockName").val("");
                                }
                        }
                    })
                 }',
                'options'=>array(
                   'showAnim'=>'fold',
                   'minLength' => 2,
                   'focus'=> 'js:function( event, ui ) {
                        $("#stockCode").val( ui.item.value );
                        return false;
                    }',
                   'select'=>'js:function( event, ui ) {
                        $("#stockName").val(ui.item.obatalkes_nama);
                        addBarang(ui.item);
                        return false;
                    }',

                ),
                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly'),
                //'tombolDialog'=>array('idDialog'=>'dialogDaftarPasien','idTombol'=>'tombolPasienDialog'),
            )); ?>
        </div>
    </div>
    <div class="control-group ">
        <?php echo CHtml::label('Stock Name','stockName', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php $this->widget('MyJuiAutoComplete',array(
                'name'=>'stockName',
                'value'=>'',
                'source'=>'js: function(request, response) {
                    $.ajax({
                        url: "'.Yii::app()->createUrl('retail/permintaanPembelian/autoCompleteStockName').'",
                        dataType: "json",
                        data: {
                            term: request.term,
                            supplier_id: $("#'.CHtml::activeId($modPermintaanPembelian, 'supplier_id').'").val()
                        },
                        success: function (data) {
                                response(data);
                                if(data[0].error!=""){
                                    $("#'.CHtml::activeId($modPermintaanPembelian, 'supplierCode').'").focus();
                                    alert(data[0].error);
                                    $("#stockName").val("");
                                }
                        }
                    })
                 }',
                'options'=>array(
                   'showAnim'=>'fold',
                   'minLength' => 2,
                   'focus'=> 'js:function( event, ui ) {
                        $("#stockName").val( ui.item.value );
                        return false;
                    }',
                   'select'=>'js:function( event, ui ) {
                        $("#stockCode").val(ui.item.obatalkes_kode);
                        addBarang(ui.item);
                        return false;
                    }',

                ),
                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly'),
                'tombolDialog'=>array('idDialog'=>'dialogBarang','idTombol'=>'tombolDialogBarang',
                                      'jsFunction'=>'$("#dialogBarang").dialog("open");
                                                    $.fn.yiiGridView.update("daftarbarang-v-grid", {
                                                            data: {"ObatsupplierM[supplier_id]":$("#'.CHtml::activeId($modPermintaanPembelian, 'supplier_id').'").val()}
                                                    });'),
            )); ?>
        </div>
    </div>
    <div class="control-group ">
        <?php echo CHtml::label('Qty','qty', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('qty', '1', array('class'=>'inputFormTabel lebar2 number0')) ?>
        </div>
    </div>
</fieldset>

<table id="tablePO" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.</th>
        <th>Stock Code</th>
        <th>Stock Name</th>
        <th>Qty</th>
        <th>Unit</th>
        <th>Price / Unit</th>
        <th>Other Cost</th>
        <th>PO Net</th>
        <th>PO PPN</th>
        <th>(%) Discount 1</th>
        <th>(%) Discount 2</th>
        <th>PO Total</th>
        <th></th>
    </tr>
    </thead>
    <tbody></tbody>
</table>

<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'id'=>'resetbtn')); ?>								
    <?php   $content = $this->renderPartial('../tips/transaksi',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));?>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">

$('.currency').each(function(){this.value = formatUang(this.value)});
$('#resetbtn').click(function(){
    $('#tablePO').children('tbody').remove();
    $('#tablePO').append('<tbody></tbody>');
});
function addBarang(item)
{
    <?php $units = CHtml::dropDownList('penerimaan[0][unit]', '', CHtml::listData(SatuanbesarM::model()->findAll(array('order'=>'satuanbesar_nama')), 'satuanbesar_id', 'satuanbesar_nama'),array('empty'=>'--','class'=>'inputFormTabel lebar3')); ?>
    <?php $units = trim(preg_replace('/\s+/', ' ', $units )); ?>
    
    var i = $('#tablePO tr').length;
    var inputQty = '<?php echo CHtml::textField('permintaan[0][qty]', '', array('class'=>'inputFormTabel lebar2 number0','onkeyup'=>'changeQty(this)','readonly'=>false)); ?>';
    var inputBrgId = '<?php echo CHtml::hiddenField('permintaan[0][brg_id]', '', array('class'=>'inputFormTabel lebar1','readonly'=>true)); ?>';
    var inputUnit = '<?php echo $units; ?>';
    var inputPrize = '<?php echo CHtml::textField('permintaan[0][prize]', '', array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>';
    var inputPonet = '<?php echo CHtml::textField('permintaan[0][ponet]', '', array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>';
    var inputPovat = '<?php echo CHtml::textField('permintaan[0][povat]', '', array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>';
    var inputPotot = '<?php echo CHtml::textField('permintaan[0][potot]', '', array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>';
    var inputOthercost = '<?php echo CHtml::textField('permintaan[0][othercost]', '', array('class'=>'inputFormTabel lebar3 currency','onkeyup'=>'changeQty(this)','readonly'=>false)); ?>';
    var inputDisc1 = '<?php echo CHtml::textField('permintaan[0][disc1]', '', array('class'=>'inputFormTabel lebar1 number0','onkeyup'=>'changeQty(this)','readonly'=>false)); ?>';
    var inputDisc2 = '<?php echo CHtml::textField('permintaan[0][disc2]', '', array('class'=>'inputFormTabel lebar1 number0','onkeyup'=>'changeQty(this)','readonly'=>false)); ?>';
    
    var inputDisc = '<?php echo CHtml::hiddenField('permintaan[0][discount]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputSatkecil = '<?php echo CHtml::hiddenField('permintaan[0][satkecil]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputSatbesar = '<?php echo CHtml::hiddenField('permintaan[0][satbesar]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputStokakhir = '<?php echo CHtml::hiddenField('permintaan[0][stokakhir]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputMaksimalstok = '<?php echo CHtml::hiddenField('permintaan[0][maksimalstok]', '', array('class'=>'inputFormTabel lebar3 number','readonly'=>true)); ?>';
    var inputMinimalstok = '<?php echo CHtml::hiddenField('permintaan[0][minimalstok]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputJmldlmkemasan = '<?php echo CHtml::hiddenField('permintaan[0][jmldlmkemasan]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputJmldiscount = '<?php echo CHtml::hiddenField('permintaan[0][jmldiscount]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputSumberdana = '<?php echo CHtml::hiddenField('permintaan[0][sumberdana_id]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    var inputPpn = '<?php echo CHtml::hiddenField('penerimaan[0][ppn]', '', array('class'=>'inputFormTabel lebar3','readonly'=>true)); ?>';
    
    var btnHapus = '<?php echo CHtml::link("<span class=\"icon-remove\">&nbsp;</span>",'',array('href'=>'#','onclick'=>'remove(this);return false;','style'=>'text-decoration:none;')); ?>';
    var textNo = '<span id="nomor"></span>';
    
    var brg_id = item.obatalkes_id;
    var qty = unformatNumber($('#qty').val());
    var unit_id = item.satuanbesar_id;
    var ppn = item.ppn;
    var prize = item.belinonppn;
    var othercost = item.othercost; 
    var ponet = (qty*prize)+(qty*othercost);
    var povat = (qty*prize)*(ppn/100);
    var potot = ponet+povat;
    var discount = item.disc1;
    var discount2 = item.disc2;
    var satkecil = item.satuankecil_id;
    var satbesar = item.satuanbesar_id;
    var stokakhir = item.stokakhir;
    var maksimalstok = item.maksimalstok;
    var minimalstok = item.minimalstok;
    var jmldlmkemasan = item.kemasanbesar;
    var jmldiscount = discount/100 * potot;
    var sumberdana = item.sumberdana_id;
    
    $('#tablePO tr').each(function(j){
        $(this).attr('id','tr_'+j);
    });
    
    var tr = '<tr id="tr_'+i+'">'+
                '<td>'+textNo+inputBrgId+'</td>'+
                '<td>'+item.obatalkes_kode+'</td>'+
                '<td>'+item.obatalkes_nama+'</td>'+
                '<td>'+inputQty+'</td>'+
                '<td>'+inputUnit+'</td>'+
                '<td>'+inputPrize+'</td>'+
                '<td>'+inputOthercost+
                       inputDisc+
                       inputSatkecil+
                       inputSatbesar+
                       inputStokakhir+
                       inputMaksimalstok+
                       inputMinimalstok+
                       inputJmldlmkemasan+
                       inputJmldiscount+
                       inputSumberdana+
                       inputPpn+'</td>'+
                '<td>'+inputPonet+'</td>'+
                '<td>'+inputPovat+'</td>'+
                '<td>'+inputDisc1+'</td>'+
                '<td>'+inputDisc2+'</td>'+
                '<td>'+inputPotot+'</td>'+
                '<td>'+btnHapus+'</td>'+
             '</tr>';
         
    $('#tablePO tbody').append(tr);
    $('#tr_'+i+' #nomor').text(i);
    $('#tr_'+i).find('select[name$="[unit]"]').attr('value', unit_id);
    $('#tr_'+i).find('input[name$="[brg_id]"]').attr('value', brg_id);
    $('#tr_'+i).find('input[name$="[prize]"]').attr('value', prize);
    $('#tr_'+i).find('input[name$="[ponet]"]').attr('value', ponet);
    $('#tr_'+i).find('input[name$="[povat]"]').attr('value', povat);
    $('#tr_'+i).find('input[name$="[potot]"]').attr('value', potot);
    $('#tr_'+i).find('input[name$="[qty]"]').attr('value', qty);
    
    $('#tr_'+i).find('input[name$="[jmldlmkemasan]"]').attr('value', jmldlmkemasan);
    $('#tr_'+i).find('input[name$="[minimalstok]"]').attr('value', minimalstok);
    $('#tr_'+i).find('input[name$="[maksimalstok]"]').attr('value', maksimalstok);
    $('#tr_'+i).find('input[name$="[stokakhir]"]').attr('value', stokakhir);
    $('#tr_'+i).find('input[name$="[satbesar]"]').attr('value', satbesar);
    $('#tr_'+i).find('input[name$="[satkecil]"]').attr('value', satkecil);
    $('#tr_'+i).find('input[name$="[discount]"]').attr('value', discount);
    $('#tr_'+i).find('input[name$="[othercost]"]').attr('value', othercost);
    $('#tr_'+i).find('input[name$="[jmldiscount]"]').attr('value', jmldiscount);
    $('#tr_'+i).find('input[name$="[sumberdana_id]"]').attr('value', sumberdana);
    $('#tr_'+i).find('input[name$="[ppn]"]').attr('value', ppn);
    $('#tr_'+i).find('input[name$="[disc1]"]').attr('value', discount);
    $('#tr_'+i).find('input[name$="[disc2]"]').attr('value', discount2);
    
    renameInput('permintaan','unit');
    renameInput('permintaan','brg_id');
    renameInput('permintaan','prize');
    renameInput('permintaan','ponet');
    renameInput('permintaan','povat');
    renameInput('permintaan','potot');
    renameInput('permintaan','qty');
    renameInput('permintaan','othercost');
    renameInput('permintaan','discount');
    renameInput('permintaan','satkecil');
    renameInput('permintaan','satbesar');
    renameInput('permintaan','stokakhir');
    renameInput('permintaan','maksimalstok');
    renameInput('permintaan','minimalstok');
    renameInput('permintaan','jmldlmkemasan');
    renameInput('permintaan','jmldiscount');
    renameInput('penerimaan','sumberdana_id');
    renameInput('penerimaan','ppn');
    renameInput('penerimaan','disc1');
    renameInput('penerimaan','disc2');
    
    $('#stockCode').val('');
    $('#stockName').val('');
    
    $("#tablePO > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
    $('.currency').each(function(){this.value = formatUang(this.value)});
    $("#tablePO > tbody > tr:last .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":1,"symbol":null});
    $('.number').each(function(){this.value = formatNumber(this.value)});
    $("#tablePO > tbody > tr:last .number0").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0,"symbol":null});
    //$('.number0').each(function(){this.value = formatNumber(this.value)});
    
}

function renameInput(modelName,attributeName)
{
    var i = -1;
    $('#tablePO tr').each(function(j){
        $(this).attr('id','tr_'+j);
        $('#tr_'+j+' #nomor').text(j);
        if($(this).has('input[name$="[brg_id]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function remove(obj)
{
    if(confirm('Apakah anda akan menghapus order?')){
        $(obj).parent().parent().remove();
        renameInput('permintaan','unit');
        renameInput('permintaan','brg_id');
        renameInput('permintaan','prize');
        renameInput('permintaan','ponet');
        renameInput('permintaan','povat');
        renameInput('permintaan','potot');
        renameInput('permintaan','qty');
        renameInput('permintaan','othercost');
        renameInput('permintaan','discount');
        renameInput('permintaan','satkecil');
        renameInput('permintaan','satbesar');
        renameInput('permintaan','stokakhir');
        renameInput('permintaan','maksimalstok');
        renameInput('permintaan','minimalstok');
        renameInput('permintaan','jmldlmkemasan');
        renameInput('permintaan','jmldiscount');
        renameInput('penerimaan','sumberdana_id');
        renameInput('penerimaan','ppn');
        renameInput('penerimaan','disc1');
        renameInput('penerimaan','disc2');
    }
}

function changeQty(obj)
{
    var qty = unformatNumber($(obj).parents('tr').find('input[name$="[qty]"]').val());
    
    var inDisc1 = $(obj).parents('tr').find('input[name$="[disc1]"]');
    var inDisc2 = $(obj).parents('tr').find('input[name$="[disc2]"]');
    var inJmldisc = $(obj).parents('tr').find('input[name$="[jmldiscount]"]');
    var inPOnet = $(obj).parents('tr').find('input[name$="[ponet]"]');
    var inPOvat = $(obj).parents('tr').find('input[name$="[povat]"]');
    var inPOtot = $(obj).parents('tr').find('input[name$="[potot]"]');
    var inPrize = $(obj).parents('tr').find('input[name$="[prize]"]');
    var prize = unformatNumber(inPrize.val());
    
    var ppn = $(obj).parents('tr').find('input[name$="[ppn]"]').val();
    var othercost = unformatNumber($(obj).parents('tr').find('input[name$="[othercost]"]').val());
    var ponet = (qty*prize)+(qty*othercost);
    var povat = (qty*prize)*(ppn/100);
    var potot = ponet+povat;
    var disc1 = unformatNumber(inDisc1.val());
    var disc2 = unformatNumber(inDisc2.val());
    var disc = 0;
    var jmlDisc = 0;
    
    disc = (potot*disc1/100);
    jmlDisc = jmlDisc + disc;
    potot = potot - disc;
    disc = (potot*disc2/100);
    jmlDisc = jmlDisc + disc;
    potot = potot - disc;
    
    inJmldisc.attr('value', jmlDisc);
    inPOnet.attr('value', formatUang(ponet));
    inPOvat.attr('value', formatUang(povat));
    inPOtot.attr('value', formatUang(potot));
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}

function addBarangFromDialog(barang,supplier)
{
    if($("#<?php echo CHtml::activeId($modPermintaanPembelian, 'supplierCode') ?>").val()=="") {
        $("#<?php echo CHtml::activeId($modPermintaanPembelian, 'supplierCode') ?>").focus();
        alert('Supplier masih kosong!');
        $("#stockName").val("");
    } else {
        $.ajax({
            url: "<?php echo Yii::app()->createUrl('retail/permintaanPembelian/addBarangFromDialog')?>",
            dataType: "json",
            data: {
                obatalkes_id: barang,
                supplier_id:supplier
            },
            success: function (data) {
                    if(data.error!=""){
                        $("#<?php echo CHtml::activeId($modPermintaanPembelian, 'supplierCode') ?>").focus();
                        alert(data.error);
                        $("#stockName").val("");
                    } else {
                        addBarang(data);
                    }
            }
        })
    }
    
}
</script>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogSupplier',
            'options'=>array(
                'title'=>'Daftar Supplier',
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>600,
            ),
        ));

    $supplier = new SupplierM('search');
    $supplier->unsetAttributes();
    if(isset($_GET['SupplierM']))
        $supplier->attributes = $_GET['SupplierM'];

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'daftarsupplier-v-grid',
            'dataProvider'=>$supplier->search(),
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'filter'=>$supplier,
            'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-check\'></i> ", "javascript:void(0)",array("onclick"=>"\$(\'#REPermintaanpembelianT_supplierCode\').val(\'$data->supplier_kode\');
                                                                                                                     \$(\'#REPermintaanpembelianT_supplierName\').val(\'$data->supplier_nama\');
                                                                                                                     \$(\'#REPermintaanpembelianT_supplier_id\').val(\'$data->supplier_id\');
                                                                                                                     \$(\'#termin\').val(\'$data->supplier_termin\');
                                                                                                                     \$(\'#dialogSupplier\').dialog(\'close\');",
                                                                                                         "rel"=>"tooltip","title"=>"Klik untuk memilih supplier"))',
                  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
                'supplier_kode',
                'supplier_nama',
                'supplier_propinsi',
                'supplier_kabupaten',
                'supplier_telp',
                'supplier_fax',
                'supplier_cp',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogBarang',
            'options'=>array(
                'title'=>'Daftar Barang',
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>600,
            ),
        ));

    $barang = new ObatsupplierM('searchPOS');
    $barang->unsetAttributes();
    if(isset($_GET['ObatsupplierM']))
        $barang->attributes = $_GET['ObatsupplierM'];

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'daftarbarang-v-grid',
            'dataProvider'=>$barang->searchPOS(),
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'filter'=>$barang,
            'columns'=>array(
                array(
                    'filter'=>CHtml::activeHiddenField($barang, 'supplier_id'),
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\'icon-check\'></i> ", "javascript:void(0)",array("onclick"=>"addBarangFromDialog($data->obatalkes_id,$data->supplier_id);
                                                                                                                     \$(\'#dialogBarang\').dialog(\'close\');",
                                                                                                         "rel"=>"tooltip","title"=>"Klik untuk memilih barang"))',
                  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                ),
                'supplier.supplier_nama',
                'obatalkes.obatalkes_nama',
                'belinonppn',
                'disc1',
                'disc2',
                'beliplusdisc',
                'ppn',
                'othercost',
                'totalbeliplusppn',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>