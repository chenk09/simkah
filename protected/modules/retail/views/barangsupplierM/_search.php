<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'gfobat-alkes-m-searchData',
        'type'=>'horizontal',
)); ?>


	<div class="control-group">
             <?php echo CHtml::label('Nama Barang','', array('class'=>'control-label')) ?>
            <div class="controls">
                 <?php echo $form->textField($model,'obatalkes_nama',array('class'=>'span3','style'=>'width:180px')); ?>
            </div>
        </div>
       
        <div class="control-group">
             <?php echo CHtml::label('Nama Supplier','', array('class'=>'control-label')) ?>
            <div class="controls">
                 <?php echo $form->textField($model,'supplier_id',array('class'=>'span3','style'=>'width:180px')); ?>
            </div>
        </div>
       

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
