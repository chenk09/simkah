<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'rebarang-supplier-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>
 <legend class="rim">Data Supplier</legend>
<!--	<p class="help-block"><?php //echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>-->

	<?php if (isset($modDetails)){
                    echo $form->errorSummary($modDetails);
                } else {
                    echo $form->errorSummary($model);
                } ?>
        <table>
            <tr>
               
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('Kode Supplier','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($modSupplier,'supplier_kode',array('id'=>'supplier_kode','class'=>'span3','placeholder'=>'Ketikan Kode Supplier', 'onkeypress'=>"if (event.keyCode == 13){setSupplierKode(this);}return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                            <?php echo $form->hiddenField($modSupplier,'supplier_id',array('id'=>'idSupplier','class'=>'span3','placeholder'=>'Ketikan Kode Supplier', 'onkeypress'=>"if (event.keyCode == 13){setSupplierKode(this);}return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                            <?php //echo $form->hiddenField($model,'obatalkes_id',array('id'=>'idObat','class'=>'span3', 'onkeypress'=>"if (event.keyCode == 13){setSupplierKode(this);}return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                    
                     <div class="control-group">
                        <?php echo CHtml::label('Nama Supplier','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($modSupplier,'supplier_nama',array('id'=>'supplier_kode','class'=>'span3','placeholder'=>'Nama Supplier', 'onkeypress'=>"if (event.keyCode == 13){setSupplierKode(this);}return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                        </div>
                    </div>
                     <div class="control-group">
                         <?php echo CHtml::label('Telp / Fax','', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textField($modSupplier,'supplier_telp',array('id'=>'supplier_telp','class'=>'span2', 'placeholder'=>'Telp','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50));?>
                            <?php echo $form->textField($modSupplier,'supplier_fax',array('id'=>'supplier_fax','class'=>'span2', 'placeholder'=>'Fax','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                        </div>
                     </div>
                </td>
                <td>
                    <div class="control-group">
                        <?php echo CHtml::label('Alamat','',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->textArea($modSupplier,'supplier_alamat',array('id'=>'supplier_alamat','rows'=>6, 'cols'=>30, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <legend class="rim">Pilih Nama Barang</legend>
        <table id="formBarang" class="table table-bordered table-condensed">
            <tr>
                <td>
                    <div class="control-group">
                    <?php echo CHtml::label('Kode Barang','',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <div class="input-append">
                            <?php echo CHtml::hiddenField('idObat'); ?>
                            <?php echo $form->textField($modBarang,'obatalkes_kode',array('id'=>'obatalkes_kode','style'=>'width:120px','placeholder'=>'Barang Code', 'onkeypress'=>"if (event.keyCode == 13){submitObat();}{setKodeObatAlkes(this);}return $(this).focusNextInputField(event)",'maxlength'=>100)); ?>
                            <span class="add-on"><i class="icon-list-alt"></i></a></span>
                        </div>
                    </div>
                </div>
                </td>
                <td>
                   <div class="control-group">
                    <?php echo CHtml::label('Barcode Barang','',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <div class="input-append">
                            <?php echo CHtml::hiddenField('idObat'); ?>
                            <?php echo $form->textField($modBarang,'obatalkes_barcode',array('id'=>'obatalkes_barcode','style'=>'width:120px','placeholder'=>'Barang Barcode', 'onkeypress'=>"if (event.keyCode == 13){submitObat();}{setBarcodeBarang(this);}return $(this).focusNextInputField(event)",'maxlength'=>100)); ?>
                            <span class="add-on"><i class="icon-list-alt"></i></a></span>
                        </div>
                    </div>
                </div>
                </td>
                <td>
                    <div class="control-group">
                    <?php echo CHtml::label('Nama Barang','namabarang',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo CHtml::hiddenField('idObat'); ?>
                            <?php //echo $form->hiddenField($modBarang,'obatalkes_id',array('readonly'=>true,'id'=>'obatalkes_id')) ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'model'=>$modBarang, 
//                                        'name'=>'namapegawai',
                                        'attribute'=>'obatalkes_nama',
                                        'id'=>'obatalkesnama',
                                        'value'=>$namapegawai,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Obatalkes'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#idObat").val( ui.item.obatalkes_id );
                                                $("#namabarang").val( ui.item.obatalkes_nama );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#idObat").val( ui.item.obatalkes_id );
                                                $("#obatalkesnama").val( ui.item.obatalkes_nama);
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('onkeypress'=>"if (event.keyCode == 13){submitObat();}return $(this).focusNextInputField(event)", 'class'=>'span2','placeholder'=>'Stock Name '),
                                        'tombolDialog'=>array('idDialog'=>'dialogObatSupplier','idTombol'=>'tombolObatSupplier'),
                                
                            )); ?>
                    </div>
                </div>
                </td>
            </tr>
        </table>
        
         <table id="tblObatSupplier" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th>Kategori Barang</th>
                        <th>Sub Kategori Barang</th>
                        <th>Kode Barang</th>
                        <th>Nama Barang</th>
                        <th>Satuan</th>
                        <th>Beli -PPN</th>
                        <th>Disc 1(%)</th>
                        <th>Disc 2(%)</th>
                        <th>Beli +Disc</th>
                        <th>PPN(%)</th>
                        <th>Other Cost</th>
                        <th>Total Beli +PPN</th>
                        <th>Batal</th>
                    </tr>
                </thead>
                <tbody id="bodyTblFormObatSupplier">
                    
                      <?php
//                      echo count($model);
//                      exit();
                    $supplier_id = $_POST['supplier_id'];
                    $supplier_id = $_POST['obatalkes_id'];
//                    $supplier = REBarangsupplierM::model()->findAllByAttributes(array('supplier_id'=>$supplier_id));
                    foreach ($model as $i=>$value)
                    {
                        $hapus = Yii::app()->createUrl('retail/barangsupplierM/deleteupdate',array('obatalkes_id'=>"$value->obatalkes_id",'supplier_id'=>"$value->supplier_id"));
                        $tr .= "<tr>";
                        $tr .= "<td>"
                                    .CHtml::activeHiddenField($value, '['.$i.']obatalkes_id',array('id'=>'obatalkles_id','class'=>'barang'))
                                    .CHtml::activeHiddenField($value, '['.$i.']supplier_id',array('id'=>'supplier_id','class'=>'barang')) 
                                    .$value->obatalkes->jenisobatalkes->jenisobatalkes_nama
                                    ."</td>";
                        $tr .= "<td>".$value->obatalkes->subjenis->subjenis_nama."</td>";
                        $tr .= "<td>".$value->obatalkes->obatalkes_kode."</td>";
                        $tr .= "<td>".$value->obatalkes->obatalkes_nama."</td>";
                        $tr .= "<td>".$value->obatalkes->satuanbesar->satuanbesar_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']belinonppn',array('id'=>'belinonppn','onkeyup'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']disc1',array('id'=>'disc1','onblur'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']disc2',array('id'=>'disc2','onblur'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']beliplusdisc',array('id'=>'beliplusdisc','onblur'=>'hitungbeliplusdisc(this);','span1 class'=>'currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']ppn',array('onblur'=>'hitungbeliplusdisc(this);','id'=>'ppn','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']othercost',array('id'=>'othercost','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($value,'['.$i.']totalbeliplusppn',array('onblur'=>'hitungbeliplusdisc(this);','id'=>'totalbeliplusppn','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                    $tr .= '<td>'.CHtml::link("<i class='icon-trash'></i>",$hapus).'</td>';
                        $tr .= "</tr>";
                    }
                    echo $tr;
                    
                if (count($modDetails) > 0){
                    foreach ($modDetails as $i=>$detail){
                        $modBarang = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                        $tr = "<tr>";
                        $tr .= "<td>"
                                    .CHtml::activeHiddenField($detail, '['.$i.']obatalkes_id',array('class'=>'barang'))
                                    .CHtml::activeHiddenField($detail, '['.$i.']supplier_id',array('class'=>'barang')) 
                                    .$modBarang->jenisobatalkes->jenisobatalkes_nama
                                    ."</td>";
                        $tr .= "<td>".$modBarang->subjenis->subjenis_nama."</td>";
                        $tr .= "<td>".$modBarang->obatalkes_kode."</td>";
                        $tr .= "<td>".$modBarang->obatalkes_nama."</td>";
                        $tr .= "<td>".$modBarang->satuanbesar->satuanbesar_nama."</td>";
                         $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']belinonppn',array('id'=>'belinonppn','onkeyup'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']disc1',array('id'=>'disc1','onblur'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']disc2',array('id'=>'disc2','onblur'=>'hitungbeliplusdisc(this);','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']beliplusdisc',array('id'=>'beliplusdisc','onblur'=>'hitungbeliplusdisc(this);','span1 class'=>'currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']ppn',array('onblur'=>'hitungbeliplusdisc(this);','id'=>'ppn','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']othercost',array('id'=>'othercost','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($detail,'['.$i.']totalbeliplusppn',array('onblur'=>'hitungbeliplusdisc(this);','id'=>'totalbeliplusppn','class'=>'span1 currency barang','style'=>'width:60px;'))."</td>";
                        $tr .= "<td>".CHtml::link("<i class='icon-remove'></i>", '#', array('onclick'=>'batal(this);'))."</td>";
                        $tr .= "</tr>";
                        echo $tr;
                    }
                }
                ?>
                  
                </tbody>
        </table> 
        <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.BarangsupplierM.'/create'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php
/**
 * Dialog untuk nama Supplier
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogSupplier',
    'options'=>array(
        'title'=>'Daftar Supplier',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modSupplier = new RESupplierM('search');
$modSupplier->unsetAttributes();
if (isset($_GET['RESupplierM'])) {
    $modSupplier->attributes = $_GET['RESupplierM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'supplier-m-grid',
	'dataProvider'=>$modSupplier->search(),
	'filter'=>$modSupplier,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                        "id" => "selectPegawai",
                                        "href"=>"",
                                        "onClick" => "
                                                      $(\"#supplier_kode\").val(\"$data->supplier_kode\");
                                                      $(\"#idSupplier\").val(\"$data->supplier_id\");
                                                      $(\"#namasupplier\").val(\"$data->supplier_nama\");
                                                      $(\"#supplier_telp\").val(\"$data->supplier_telp\");
                                                      $(\"#supplier_fax\").val(\"$data->supplier_fax\");
                                                      $(\"#supplier_alamat\").val(\"$data->supplier_alamat\");
                                                      $(\"#dialogSupplier\").dialog(\"close\");    
                                                      return false;
                                            "))',
                    ),
                'supplier_kode',
                array(
                  'header'=>'Nama Supplier',
                  'name'=>'supplier_nama',
                  'value'=>'$data->supplier_nama',
                ),
                'supplier_telp',
                'supplier_fax',
                'supplier_alamat',
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>
<?php 
$urlSupplierKode = Yii::app()->createUrl('actionAjax/getSupplierKode');
Yii::app()->clientScript->registerScript('supplier_kode','
    function setSupplierKode(obj){
        var value = $(obj).val();
        $.post("'.$urlSupplierKode.'",{supplier_kode:value},function(hasil){
            $("#idSupplier").val(hasil.supplier_id);
            $("#supplier_telp").val(hasil.supplier_telp);
            $("#namasupplier").val(hasil.supplier_nama);
            $("#supplier_fax").val(hasil.supplier_fax);
            $("#supplier_alamat").val(hasil.supplier_alamat);
            $("#'.CHtml::activeId($modSupplier, 'supplier_nama').'").val(hasil.supplier_nama);
        }, "json");
    }
',  CClientScript::POS_HEAD); 
?>
<?php

$urlKodeBarang = Yii::app()->createUrl('actionAjax/getKodeBarangSupplier');
Yii::app()->clientScript->registerScript('obatalkes_kode','
    function setKodeObatAlkes(obj){
        var value = $(obj).val();
        $.post("'.$urlKodeBarang.'",{obatalkes_kode:value},function(hasil){
            $("#idObat").val(hasil.obatalkes_id);
        }, "json");
    }
',  CClientScript::POS_HEAD); 
?>  
        
<?php

$urlBarcodeBarang = Yii::app()->createUrl('actionAjax/getBarcodeBarangSupplier');
Yii::app()->clientScript->registerScript('obatalkes_barcode','
    function setBarcodeBarang(obj){
        var value = $(obj).val();
        $.post("'.$urlBarcodeBarang.'",{obatalkes_barcode:value},function(hasil){
            $("#idObat").val(hasil.obatalkes_id);
        }, "json");
    }
',  CClientScript::POS_HEAD); 
?>  
<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogObatSupplier',
    'options' => array(
        'title' => 'Pencarian Obat Supplier',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modObat = new REProdukposallV('search');
$modObat->unsetAttributes();
if (isset($_GET['REProdukposallV'])) {
    $modObat->attributes = $_GET['REProdukposallV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'pasien-m-grid',
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modObat->search(),
    'filter' => $modObat,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#idObat\").val(\"$data->brg_id\");
                                                          $(\"#obatalkesnama\").val(\"$data->stock_code - $data->stock_name\");
                                                          $(\"#satuan\").val(\"$data->satuankecil_id\");
                                                          $(\"#dialogObatSupplier\").dialog(\"close\");
                                                          submitObat();
                                                          return false;
                                                "))',
    ),
        array(
           'header'=>'ID',
           'name'=>'brg_id',
           'type' => 'raw',
           'value' => '$data->brg_id',
        ),
        array(
           'header'=>'Kategori Barang',
           'name'=>'category_name',
           'type' => 'raw',
           'value' => '$data->category_name',
        ),
       array(
           'header'=>'Sub Kategori Barang',
           'name'=>'subcategory_name',
           'type' => 'raw',
           'value' => '$data->subcategory_name',
        ),
        array(
           'header'=>'Kode Barang',
           'name'=>'stock_code',
           'type' => 'raw',
           'value' => '$data->stock_code',
        ),
        array(
           'header'=>'Nama Barang',
           'name'=>'stock_name',
           'type' => 'raw',
           'value' => '$data->stock_name',
        ),
         array(
           'header'=>'Satuan Besar',
           'name'=>'satuanbesar_nama',
           'type' => 'raw',
           'value' => '$data->satuanbesar_nama',
        ),
         array(
           'header'=>'Satuan Kecil',
           'name'=>'satuankecil_nama',
           'type' => 'raw',
           'value' => '$data->satuankecil_nama',
        ),
        array(
           'header'=>'Active Date',
            'type' => 'raw',
            'value' => '$data->activedate',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>

        
<?php

$notif = Yii::t('mds','Do You want to cancel?');
$urlGetObatSupplier = Yii::app()->createUrl('actionAjax/getObatSupplier'); //MAsukan Dengan memilih Obat Alkes

$jscript = <<< JS
    function clearAll(){
        $('#tblObatSupplier tbody tr').remove();
    }

   function submitObat(){
        idObat = $('#idObat').val();
        idSupplier = $('#idSupplier').val();
        
        if (!jQuery.isNumeric(idSupplier)){
            alert('Isi Supplier Terlebih Dahulu');
            return false;
        }
        else if (!jQuery.isNumeric(idObat)){
            alert('Isi Barang yang akan dipesan');
            return false;
        }
        else{
            if (cekList(idObat) == true){
                $.post('${urlGetObatSupplier}', {idObat:idObat, idSupplier:idSupplier}, function(data){
                    $('#tblObatSupplier tbody').append(data);
                    $("#tblObatSupplier tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                    clearInput();
                    renameInput();
                }, 'json');
            }
        }
        
    }
    
    function hitungSemua(){
        $('.obat').each(function(){
            qty = parseFloat($(this).parents('tr').find('.qty').val());
            harga = parseFloat($(this).parents('tr').find('.hargaJual').val());
            $(this).parents('tr').find('.subTotal').val(qty*harga);
        })
    }

    function cekList(id){
        x = true;
        $('.barang').each(function(){
            if ($(this).val() == id){
                alert('Barang telah ada di List');
                x = false;
            }
        });
        return x;
    }
    
    
    function clearInput(){
        $('#formBarang').find('input, select').each(function(){
            $(this).val('');
        });
    }
                
    function batal(obj){
        if(!confirm("${notif}")) {
            return false;
        }else{
            $(obj).parents('tr').remove();
            renameInput();
        }
    }
    
    function renameInput(){
        noUrut = 0;
        giliran = 2;
        $('.cancel').each(function(){
            $(this).parents('tr').find('[name*="ObatsupplierM"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('ObatsupplierM[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','ObatsupplierM['+noUrut+']'+data[1]);
                }
            });
            
//            $(this).parents('tr').find('[id*="REBarangsupplierM_no"]').each(function() {
//                var input = $(this).attr('id');
//                var data = input.split('REBarangsupplierM_');
//                $(this).attr('id','REBarangsupplierM_'+data[1]+nourut);
//                $("#REBarangsupplierM_"+data[1]+nourut).val(giliran);
//             });
            noUrut++;
//            giliran++;
        });        
    }

JS;
Yii::app()->clientScript->registerScript('masukanobat', $jscript, CClientScript::POS_HEAD);
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php
$jscript = <<< JS
   
     function hitungbeliplusdisc(obj){
     parent = $(obj).parents("tr");
     $("#rebarang-supplier-m-form").find(".barang").each(function(){
          var belinonppn = parseFloat(unformatNumber(parent.find("#belinonppn").val()));
          var disc1 = parseFloat(unformatNumber(parent.find("#disc1").val()));
          var disc2 = parseFloat(unformatNumber(parent.find("#disc2").val()));
          var ppn = parseFloat(unformatNumber(parent.find("#ppn").val()));
          var beliplusdisc = parseFloat(unformatNumber(parent.find("#beliplusdisc").val()));
          var othercost = parseFloat(unformatNumber(parent.find("#othercost").val()));
          

            var totalppn = 10;
            
            var jmldisc1 = disc1 / 100;
            var jmldisc2 = disc2 / 100;
            var jmlbeli1 = (belinonppn-(belinonppn * jmldisc1));
            var jmlbeli2 = (jmlbeli1 - (jmlbeli1 * jmldisc2));
            var beliplusdisc = jmlbeli2;
            
            var jmlppn = (100 + ppn) / 100;
            var beliplusppn = (beliplusdisc * jmlppn) + othercost;
            
            parent.find("#beliplusdisc").val(formatUang(beliplusdisc));
            parent.find("#ppn").val(formatUang(totalppn));
            parent.find("#totalbeliplusppn").val(formatUang(beliplusppn));
            
        });
    }
    
     function hitungplusppn(obj){
     parent = $(obj).parents("tr");
     $("#rebarang-supplier-m-form").find(".barang").each(function(){
          var belinonppn = parseFloat(unformatNumber(parent.find("#belinonppn").val()));
          var disc1 = parseFloat(unformatNumber(parent.find("#disc1").val()));
          var disc2 = parseFloat(unformatNumber(parent.find("#disc2").val()));
          var ppn = parseFloat(unformatNumber(parent.find("#ppn").val()));
          var beliplusdisc = parseFloat(unformatNumber(parent.find("#beliplusdisc").val()));
          var othercost = parseFloat(unformatNumber(parent.find("#othercost").val()));
          
            var jmlppn = (100 + ppn) / 100;
            var beliplusppn = (beliplusdisc * jmlppn) + othercost;
            
            var jmldisc1 = disc1 / 100;
            var jmldisc2 = disc2 / 100;
            var jmlbeli1 = (belinonppn-(belinonppn * jmldisc1))  ;
            var jmlbeli2 = (jmlbeli1 - (jmlbeli1 * jmldisc2));
            var beliplusdisc = jmlbeli2;
            
          
        parent.find("#totalbeliplusppn").val(formatUang(beliplusppn));
        });
    }
    

    function cekInput()
    {
    
        $(".barang").each(function(){this.value = unformatNumber(this.value)});
        return true;
    }

JS;
Yii::app()->clientScript->registerScript('hargadiscount', $jscript, CClientScript::POS_HEAD);
?>