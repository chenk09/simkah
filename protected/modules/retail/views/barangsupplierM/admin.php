<?php
$this->breadcrumbs=array(
	'Gfobat Alkes Ms'=>array('index'),
	'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Barang Supplier ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' Barang Supplier ', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('gfobat-alkes-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-accordion icon-white"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'gfobat-alkes-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Kode Supplier',
                    'name'=>'supplier_kode',
                    'value'=>'$data->supplier->supplier_kode',
                ),
                 array(
                    'header'=>'Nama Supplier',
                    'name'=>'supplier_nama',
                    'value'=>'$data->supplier->supplier_nama',
                ),
		array(
                    'header'=>'Nama Barang',
                    'name'=>'obatalkes_nama',
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Beli -PPN',
                    'name'=>'belinonppn',
                    'value'=>'$data->belinonppn',
                ),
                array(
                    'header'=>'Disc 1(%)',
                    'name'=>'disc1',
                    'value'=>'$data->disc1',
                ),
                array(
                    'header'=>'Disc 2(%)',
                    'name'=>'disc2',
                    'value'=>'$data->disc2',
                ),
                array(
                    'header'=>'Beli +Disc',
                    'name'=>'beliplusdisc',
                    'value'=>'$data->beliplusdisc',
                ),
                array(
                    'header'=>'ppn(%)',
                    'name'=>'ppn',
                    'value'=>'$data->ppn',
                ),
                array(
                    'header'=>'Other Cost',
                    'name'=>'othercost',
                    'value'=>'$data->othercost',
                ),
                array(
                    'header'=>'Total Beli +PPN',
                    'name'=>'totalbeliplusppn',
                    'value'=>'$data->totalbeliplusppn',
                ),
		array(
                        'header'=>'View',
                        'class'=>'ext.bootstrap.widgets.BootButtonColumn',
                        'template'=>'{view}',
                        'buttons'=>array(
                            'view'=>array(
                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/View",array("obatalkes_id"=>"$data->obatalkes_id","supplier_id"=>"$data->supplier_id"))',
                            ),
                        ),
                    ),
		array(
                        'header'=>'Ubah',
                        'class'=>'ext.bootstrap.widgets.BootButtonColumn',
                        'template'=>'{update}',
                        'buttons'=>array(
                            'update'=>array(
                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/Update",array("supplier_id"=>"$data->supplier_id"))',
                            ),
                        ),
                    ),
		array(
                        'header'=>'Hapus',
                        'class'=>'ext.bootstrap.widgets.BootButtonColumn',
                        'template'=>'{delete}',
                        'buttons'=>array(
                            'delete'=>array(
                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/Delete",array("obatalkes_id"=>"$data->obatalkes_id","supplier_id"=>"$data->supplier_id"))',
                            ),
                        ),
                    ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
 
        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
       $content = $this->renderPartial('../tips/master',array(),true);
        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#gfobat-alkes-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>