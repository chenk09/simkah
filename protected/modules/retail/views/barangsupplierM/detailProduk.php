
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      

?>
<table>
    <tr>

        <td>
            <div class="control-group">
                <?php echo CHtml::label('Kode Supplier','',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($modSupplier,'supplier_kode',array('id'=>'supplier_kode','class'=>'span3','placeholder'=>'Ketikan Kode Supplier', 'onkeypress'=>"if (event.keyCode == 13){setSupplierKode(this);}return $(this).focusNextInputField(event)",  'maxlength'=>100)); ?>
                </div>
            </div>
             <div class="control-group">
            <?php echo CHtml::label('Nama Supplier','namasupplier',array('class'=>'control-label')) ?>
            <div class="controls">  
                    <?php echo CHtml::hiddenField('idSupplier'); ?>
                    <?php $this->widget('MyJuiAutoComplete',array(
                                'model'=>$model, 
//                                        'name'=>'namapegawai',
                                'attribute'=>'supplier_nama',
                                'id'=>'namasupplier',
                                'value'=>$namapegawai,
                                'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Supplier'),
                                'options'=>array(
                                   'showAnim'=>'fold',
                                   'minLength' => 2,
                                   'focus'=> 'js:function( event, ui ) {
                                        $("#idSupplier").val( ui.item.suppllier_id);
                                        $("#namasupplier").val( ui.item.supplier_nama );
                                        return false;
                                    }',
                                   'select'=>'js:function( event, ui ) {
                                        $("#idSupplier").val( ui.item.supplier_id);
                                        $("#supplier_kode").val( ui.item.supplier_kode);
                                        $("#namasupplier").val( ui.item.supplier_nama);
                                        $("#supplier_telp").val( ui.item.supplier_telp);
                                        $("#supplier_fax").val( ui.item.supplier_fax);
                                        $("#supplier_alamat").val( ui.item.supplier_alamat);
                                        return false;
                                    }',

                                ),
                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','placeholder'=>'Ketikan Nama Supplier'),
                                'tombolDialog'=>array('idDialog'=>'dialogSupplier','idTombol'=>'tombolSupplierDialog'),

                    )); ?>
            </div>
        </div>
             <div class="control-group">
                 <?php echo CHtml::label('Telp / Fax','', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($modSupplier,'supplier_telp',array('id'=>'supplier_telp','class'=>'span2', 'placeholder'=>'Telp','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50));?>
                    <?php echo $form->textField($modSupplier,'supplier_fax',array('id'=>'supplier_fax','class'=>'span2', 'placeholder'=>'Fax','onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                </div>
             </div>
        </td>
        <td>
            <div class="control-group">
                <?php echo CHtml::label('Alamat','',array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textArea($modSupplier,'supplier_alamat',array('id'=>'supplier_alamat','rows'=>6, 'cols'=>30, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div>
        </td>
    </tr>
</table>