<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'rjinfostokobatalkesruangan-i-search',
                'type'=>'horizontal',
)); ?>

<legend class="rim">Pencarian</legend>
                <div class="control-group ">
                    <table>
                        <tr>
                            <td>
                                    <?php echo $form->dropDownListRow($model,'jenisobatalkes_id',CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_nama'),array('class'=>'span2','empty'=>'-- Pilih --')); ?>
                                    <?php echo $form->dropDownListRow($model,'obatalkes_golongan',obatalkes_golongan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                                    <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span2')); ?>
                                    <?php echo $form->dropDownListRow($model,'sumberdana_id',CHtml::listData($model->getSumberdanaItems(),'sumberdana_id','sumberdana_nama'),array('class'=>'span2','empty'=>'-- Pilih --')); ?>
                            </td>
                            <td>
                                <div class="control-label">
                                    <?php echo CHtml::activeCheckBox($model, 'filterTanggal'); ?>
                                    <?php echo CHtml::label('Tanggal Transaksi','tglAwal',array('class'=>'')); ?>
                                </div>
        
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglAwal',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'maxDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                                            ?>
                                </div>
                                <?php echo CHtml::label('Sampai dengan','tglAkhir',array('class'=>'control-label')); ?>
                                <div class="controls">
                                    <?php
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglAkhir',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'maxDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); ?>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </div>

	<div class="form-actions">
                    <?php
                        echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
                        echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/'.obatAlkesM.'/informasi'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                 $content = $this->renderPartial('../tips/informasi',array(),true);
                      $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>
