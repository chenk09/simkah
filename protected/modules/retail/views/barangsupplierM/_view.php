<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('obatalkes_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->obatalkes_id),array('view','id'=>$data->obatalkes_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lokasigudang_id')); ?>:</b>
	<?php echo CHtml::encode($data->lokasigudang_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('therapiobat_id')); ?>:</b>
	<?php echo CHtml::encode($data->therapiobat_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pbf_id')); ?>:</b>
	<?php echo CHtml::encode($data->pbf_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('generik_id')); ?>:</b>
	<?php echo CHtml::encode($data->generik_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satuanbesar_id')); ?>:</b>
	<?php echo CHtml::encode($data->satuanbesar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sumberdana_id')); ?>:</b>
	<?php echo CHtml::encode($data->sumberdana_id); ?>
	<br />

</div>