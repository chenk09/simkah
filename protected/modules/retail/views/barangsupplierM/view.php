<?php
$this->breadcrumbs=array(
	'REBarang Supplier Ms'=>array('index'),
	$model->obatalkes_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Barang Supplier ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' GFObatAlkesM', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' GFObatAlkesM', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' GFObatAlkesM', 'icon'=>'pencil','url'=>array('update','id'=>$model->obatalkes_id))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' GFObatAlkesM','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->obatalkes_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Barang Supplier', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
            'supplier.supplier_kode',
            'supplier.supplier_nama',
            'belinonppn',
            'disc1',
            'disc2',
            'beliplusdisc',
            'ppn',
            'othercost',
            'totalbeliplusppn',
                
            ),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>