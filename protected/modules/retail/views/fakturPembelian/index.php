<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'gffakturpembelian-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class="rim2">Faktur Pembelian</legend>
<?php
if (isset($_POST['idPenerimaanForm']))
{   
$idPenerimaan=$_POST['idPenerimaanForm'];
$noPenerimaan=$_POST['noPenerimaanForm'];
$tglPenerimaan=$_POST['tglPenerimaanForm'];

$jscript = <<< JS
    submitPermintaanPembelianDariInformasi('${idPenerimaan}','${noPenerimaan}','${tglPenerimaan}');
JS;
Yii::app()->clientScript->registerScript('submitRencanaDari Informasi',$jscript, CClientScript::POS_HEAD);
}
$Penerimaanbrg = PenerimaanbarangT::model()->findByPK($idPenerimaan);
$supplier_nama =  $Penerimaanbrg->supplier->supplier_nama;
?>
       <table>
           <tr>
               <td>
    <fieldset>
       <legend class="rim">Informasi Receive</legend>
           <table>
               <tr>
                   <td>
                       <div class="control-group ">
                           <?php echo $form->labelEx($modPenerimaanbarang,'noterima', array('class'=>'control-label')) ?>
                           <div class="controls">
                            <?php echo CHtml::hiddenField('idPenerimaanBarang','',array('readonly'=>TRUE));?>
                            <?php echo $form->textField($modPenerimaanbarang,'noterima',array('class'=>'span3')); ?>
                            <?php
//                                    $this->widget('MyJuiAutoComplete',array(
//                                        'model'=>$modPenerimaanbarang,
//                                        'attribute'=>'noterima',
//                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/noPenerimaanBarang'),
//                                        'options'=>array(
//                                           'showAnim'=>'fold',
//                                           'minLength' => 2,
//                                           'select'=>'js:function( event, ui ) {
//                                                      $("#'.CHtml::activeId($modPenerimaanbarang,'penerimaanbarang_id').'").val(ui.item.penerimaanbarang_id);
//                                                      $("#'.CHtml::activeId($modPenerimaanbarang,'tglterima').'").val(ui.item.tglterima);   
//                                                      $("#'.CHtml::activeId($modPenerimaanbarang,'supplier_id').'").val(ui.item.supplier_id);     
//                                                      
//                                            }',
//                                        ),
//                                        'htmlOptions'=>array('onkeypress'=>"submitPenerimaanBarang();return $(this).focusNextInputField(event)",'class'=>'span2 numbersOnly','readonly'=>FALSE), 'tombolDialog'=>array('idDialog'=>'dialogPenerimaanBarang'),
//                            ));
                            ?>

                            <?php //echo CHtml::htmlButton('<i class="icon-search icon-white"></i>',
                              //  array('onclick'=>'$("#dialogPenerimaanBarang").dialog("open");return false;',
                                    //  'class'=>'btn btn-primary',
                                    //  'onkeypress'=>"return $(this).focusNextInputField(event)",
                                      //'rel'=>"tooltip",
                                     // 'title'=>"Klik Untuk Penerimaan barang Lebih Lanjut",
                                  //    'id'=>'buttonpenerimaanBarang')); ?>
                           </div>
                       </div>    
                       <?php echo $form->textFieldRow($modPenerimaanbarang,'tglterima', array('class'=>'span3')) ?>
                       <div class="control-group">
                           <?php echo CHtml::label('Supplier','',array('class'=>'control-label required')); ?>
                           <div class="controls">
                            <?php
                            echo $form->hiddenField($modFakturPembelian,'supplier_id',array('value'=>$Penerimaanbrg->supplier_id,'readonly'=>true));
                            echo CHtml::textField('supplier_nama',$Penerimaanbrg->supplier->supplier_nama,array('readonly'=>true,'class'=>'span3'));
//                            echo $form->dropDownListRow($modFakturPembelian,'supplier_id',
//                                                               CHtml::listData($modFakturPembelian->getSupplierItems(), 'supplier_id', 'supplier_nama'),
//                                                               array('class'=>'span3 isRequired', 'onkeypress'=>"return $(this).focusNextInputField(event)",
//                                                               'empty'=>'-- Pilih --',));
                            ?>
                           </div>
                       </div>
                   </td>
               </tr>
           </table>  
     </fieldset>
</td>
<td>
<table>
    <tr>
       <td>
            <fieldset>
                <legend class="rim">Data Faktur</legend>
                
                    <?php echo $form->textFieldRow($modFakturPembelian,'nofaktur', array('class'=>'span3 isRequiredFaktur')) ?>
                         <div class="control-group ">
                                <?php echo $form->labelEx($modFakturPembelian,'tglfaktur', array('class'=>'control-label')) ?>
                                    <div class="controls">
                                        <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modFakturPembelian,
                                                    'attribute'=>'tglfaktur',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,

                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                                        )); ?>
                                    </div>
                         </div>
                        <div class="control-group ">
                                    <?php echo $form->labelEx($modFakturPembelian,'tgljatuhtempo', array('class'=>'control-label')) ?>
                                        <div class="controls">
                                            <?php $this->widget('MyDateTimePicker',array(
                                                        'model'=>$modFakturPembelian,
                                                        'attribute'=>'tgljatuhtempo',
                                                        'mode'=>'date',
                                                        'options'=> array(
                                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,

                                                        ),
                                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                        ),
                                            )); ?>
                                        </div>
                             </div>
                    <?php echo $form->textFieldRow($modFakturPembelian,'biayamaterai', array('class'=>'span3')) ?>
                    <?php echo $form->textAreaRow($modFakturPembelian,'keteranganfaktur', array('class'=>'span3')) ?>
                    <?php echo $form->dropDownListRow($modFakturPembelian,'syaratbayar_id',
                                                   CHtml::listData($modFakturPembelian->SyaratBayarItems, 'syaratbayar_id', 'syaratbayar_nama'),
                                                   array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event)",
                                                   'empty'=>'-- Pilih --',)); ?>
                   
       </td>
       <td>
            <fieldset>
<!--                <legend class="rim">Informasi Harga</legend>-->
                    <table>
                                                
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="5px"><?php //echo CHtml::checkBox('termasukPPN',false,array('onclick'=>'persenPPN(this)','disabled'=>TRUE,'style'=>'width : 10px'))?></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td width="5px"><?php //echo CHtml::checkBox('termasukPPH',false,array('onclick'=>'persenPPH(this)','disabled'=>TRUE,'style'=>'width : 10px'))?></td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2"><?php //echo $form->textFieldRow($modFakturPembelian,'totalhargabruto', array('class'=>'span2 isRequired','readonly'=>TRUE)) ?></td>
                        </tr>
                    </table>
            </fieldset>
        </td>
    </tr>
</table>
</td>
</tr>
</table>
                    
<table id="tableFaktur" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Stock Code</th>
        <th>Stock Name</th>
        <th>Jumlah <br/> Permintaan</th>
        <th>Jumlah <br/>Diterima</th>
        <th>Diskon (%)</th>
        <th>Diskon (Rp)</th>
        <th>Price / Unit</th>
        <th>PPN</th>
        <th>PPH</th>     
    </tr>
    </thead>
    <tbody></tbody>
    <tfoot>
        <tr>
            <th colspan="5">Total</th>
            <th><?php echo $form->textField($modFakturPembelian,'persendiscount', array('class'=>'span1 isRequired','maxlength'=>2,'onkeyup'=>'numberOnlyNol(this);hitungJumlahHargaDiskon(this);')) ?></th>
            <th><?php echo $form->textField($modFakturPembelian,'jmldiscount', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php echo $form->textField($modFakturPembelian,'totharganetto', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php echo $form->textField($modFakturPembelian,'totalpajakppn', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>
            <th><?php echo $form->textField($modFakturPembelian,'totalpajakpph', array('class'=>'span1 isRequired','readonly'=>TRUE)) ?></th>     
        </tr>
    </tfoot>
</table>
<div class="form-actions">
        <?php 
             echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'cekValidasi()'));?>
    <div style="display: none">     
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
    </div>
       <?php
             echo "&nbsp;".CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-danger', 'type'=>'button'));
        ?>
    <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>            
<?php $this->endWidget(); ?>
<?php 
//========= Dialog buat Permintaan Kebutuhan obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPenerimaanBarang',
    'options'=>array(
        'title'=>'Pencarian Penerimaan Items',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'permintaan-m-grid',
	'dataProvider'=>$modPenerimaanbarang->searchFakturPembelian(),
	'filter'=>$modPenerimaanbarang,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#'.CHtml::activeId($modPenerimaanbarang,'noterima').'\").val(\"$data->noterima\");
                                                  $(\"#'.CHtml::activeId($modPenerimaanbarang,'supplier_id').'\").val(\"$data->supplier_id\");
                                                  $(\"#'.CHtml::activeId($modPenerimaanbarang,'tglterima').'\").val(\"$data->tglterima\");    
                                                  $(\"#idPenerimaanBarang\").val(\"$data->penerimaanbarang_id\");
                                                  submitPermintaanPembelian();
                                                  $(\"#dialogPenerimaanBarang\").dialog(\"close\");    
                                        "))',
                ),
                'noterima',
                array(
            'name' => 'tglterima',
            'filter' =>$this->widget('MyDateTimePicker',array(
                                            'model'=>$modPenerimaanbarang,
                                            'attribute'=>'tglterima',
                                            'mode'=>'date',
                                            'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT,

                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker1',
                                            ),
                                ),true), 
                    ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();

//========= end Permintaan dialog =============================
?>

<?php
$urlGetPenerimaanBarang =  Yii::app()->createUrl('actionAjax/getPenerimaanBarang');
$idSupplier = CHtml::activeId($modFakturPembelian,'supplier_id');
$persenPPN=  Params::persenPPN();
$persenPPH=  Params::persenPPH();
$idPersenDiskon=CHtml::activeId($modFakturPembelian, 'persendiscount');
$idTotalHargaNetto=CHtml::activeId($modFakturPembelian, 'totharganetto');
$idTotalPajakPPN=CHtml::activeId($modFakturPembelian,'totalpajakppn');
$idTotalPajakPPH=CHtml::activeId($modFakturPembelian, 'totalpajakpph');
$idJumlahDiskon=CHtml::activeId($modFakturPembelian, 'jmldiscount');
$idSyaratBayar=CHtml::activeId($modFakturPembelian, 'syaratbayar_id');
$idTotalHargaBruto=CHtml::activeId($modFakturPembelian, 'totalhargabruto');
$jscript = <<< JS

function hitungJumlahHargaDiskon(obj)
{
    besarDiskon =obj.value;
   
    $('.persenDiskon').each(function() {
          $(this).val(besarDiskon);
        });
    hitungSemua();
}

function cekValidasi()
{   
  
  hargaTotalNetto = parseFloat($('#REFakturPembelianT_totharganetto').val());
  
 if ($('.isRequired').val()==''){
    alert ('Harap Isi Semua Data Yang Bertanda *');
  }else if(hargaTotalNetto<1){
     alert('Anda Belum memimlih Obat Yang Akan Diminta');   
  }else{
     $('#btn_simpan').click();
//     alert('simpan');
  }
}

function numberOnly(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(1);
    }
}

function numberOnlyNol(obj)
{
    var d = $(obj).attr('numeric');
    var value = $(obj).val();
    var orignalValue = value;


    if (d == 'decimal') {
    value = value.replace(/\./, "");
    msg = "Only Numeric Values allowed.";
    }

    if (value != '') {
    orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
    $(obj).val(orignalValue);
    }else{
    $(obj).val(0);
    }
}

function submitPermintaanPembelian()
{
    idPenerimaanBarang = $('#idPenerimaanBarang').val();
        if(idPenerimaanBarang==''){
            alert('Silahkan Pilih penerimaan Terlebih Dahulu');
        }else{
            $.post("${urlGetPenerimaanBarang}", { idPenerimaanBarang: idPenerimaanBarang },
            function(data){
                $('#tableFaktur').append(data.tr);
               
                $('#${idSupplier}').val(data.supplier_id);
                hitungSemua();
                
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }

            }, "json");
           
                        
        }   
}

function submitPermintaanPembelianDariInformasi(idPenerimaan,noPenerimaan,tglPenerimaan)
{
    idPenerimaanBarang = idPenerimaan;
        if(idPenerimaanBarang==''){
            alert('Silahkan Pilih penerimaan Terlebih Dahulu');
        }else{
            $.post("${urlGetPenerimaanBarang}", { idPenerimaanBarang: idPenerimaanBarang },
            function(data){
                $('#tableFaktur').append(data.tr);
                $('#idPenerimaanBarang').val(idPenerimaan);
               
                $('#${idSupplier}').val(${supplier_id});
                $('#REPenerimaanbarangT_noterima').val(noPenerimaan);
                $('#buttonpenerimaanBarang').attr('disabled','TRUE');
                $('#REPenerimaanbarangT_tglterima').val(tglPenerimaan);
                $('#REPenerimaanbarangT_noterima').attr('readonly','TRUE');
                $('#REPenerimaanbarangT_tglterima').attr('readonly','TRUE');
                $('#REFakturPembelianT_supplier_id').attr('readonly','TRUE');
                hitungSemua();
                
                
                if(data.isPPN=='1'){ //Jika termasuk PPN
                 $('#termasukPPN').attr('checked','checked');
                }
                
                if(data.isPPH=='1'){ //Jika termasuk PPH
                 $('#termasukPPH').attr('checked','checked');
                }

            }, "json");
           
                        
        }   
}                
                
                
function hitungJumlahDariPersentaseDiskon(obj)
{
    persenDiskon = parseFloat(obj.value);
    jumlahDiterima = parseFloat($(obj).parent().prev().find('input').val());
    hargaProdukNetto = parseFloat($(obj).parent().next().next().find('input').val());
    
        jumlahDiskonProduk = (hargaProdukNetto * (persenDiskon/100)) * jumlahDiterima;
        $(obj).parent().next().find('input').val(jumlahDiskonProduk);

    hitungSemua();
}

function hitungJumlahDariDiskon(obj)
{
    jumlahDiterima = parseFloat($(obj).parent().prev().prev().find('input').val());
    jumlahDiskon = parseFloat(obj.value);
    hargaProdukNetto = parseFloat($(obj).parent().next().find('input').val());
    jumlahHarga = jumlahDiterima * hargaProdukNetto;
    
    if(jumlahDiskon>jumlahHarga){
        alert('Jumlah Diskon Tidak Boleh Melebihi Jumlah Diterima * Harga Netto');
    }else{
         persenDiskon = ((jumlahDiskon * 100)/hargaProdukNetto) / jumlahDiterima;
       $(obj).parent().prev().find('input').val(persenDiskon); 
    }
    hitungSemua();
}


function persenPPN(obj)
{
    isPPN = obj.value;
    if(obj.checked==true){ //Jika tidak termasuk PPN
        $('.ppn').each(function() {
           hargaNettoProduk=parseFloat($(this).parent().prev().find('input').val());
           jumlahPPN=hargaNettoProduk * (parseFloat(${persenPPN})/100);
           $(this).val(jumlahPPN);
        });
    }else{ //Jika Termasuk PPN
       $('.ppn').each(function() {
               $(this).val(0);
            });          
             
    }
}
               
function persenPPH(obj)
{
    isPPH = obj.value;
    if(obj.checked==true){ //Jika  termasuk PPN
           $('.pph').each(function() {
               hargaNettoProduk=parseFloat($(this).parent().prev().prev().find('input').val());
               jumlahPPH=hargaNettoProduk * (parseFloat(${persenPPH})/100);
               $(this).val(jumlahPPH);
            });
    }else{ //Jika tidak Termasuk PPN
           $('.pph').each(function() {
               $(this).val(0);
            });
             
    }
}

function hitungSemua()
{
    totalNetto=0;
    totalPajakPPN=0;
    totalPajakPPH=0;
    totalJumlahDiskon=0;
    totalHargaBruto=0;
    hargaBrutoPerProduk=0;        
    totalDiskonSemua=0;        
    
    $('.terima').each(function(){
     
           nettoPerProduk = parseFloat($(this).parent().next().next().next().find('input').val());
           PPNPreProduk = parseFloat($(this).parent().next().next().next().next().find('input').val());
           PPHPreProduk = parseFloat($(this).parent().next().next().next().next().next().find('input').val());
           jumlahDiterima = parseFloat($(this).val());
           persenDiskonPerProduk = parseFloat($(this).parent().next().find('input').val());

           jumlahDiskonProduk = (nettoPerProduk * (persenDiskonPerProduk/100)) * jumlahDiterima;
           $(this).parent().next().next().find('input').val(jumlahDiskonProduk);
            
           nettoDiterima =  nettoPerProduk * jumlahDiterima;
           diskonPerProduk = parseFloat($(this).parent().next().next().find('input').val());
            
           hargaBrutoPerProduk = jumlahDiterima * nettoPerProduk;
           totalHargaBruto = totalHargaBruto + hargaBrutoPerProduk;  
            
           totalNetto = totalNetto + nettoPerProduk;
           totalPajakPPN = totalPajakPPN + PPNPreProduk;
           totalPajakPPH = totalPajakPPH + PPHPreProduk;
           totalJumlahDiskon = totalJumlahDiskon + diskonPerProduk;
            
   });
            
    totalDiskonSemua=   (totalJumlahDiskon * 100)/totalHargaBruto; 
       
    $('#${idTotalHargaNetto}').val(totalNetto);
    $('#${idTotalPajakPPN}').val(totalPajakPPN);
    $('#${idTotalPajakPPH}').val(totalPajakPPH);
    $('#${idJumlahDiskon}').val(totalJumlahDiskon);
    $('#${idTotalHargaBruto}').val(totalHargaBruto);
    $('#${idPersenDiskon}').val(totalDiskonSemua);    
    
    noUrut = 1;
     $('.noUrut').each(function() {
          $(this).val(noUrut);
          noUrut = noUrut + 1;
     });

}
JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
?>
