<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#permintaanpembelian-t-search').submit(function(){
	$.fn.yiiGridView.update('permintaanpembelian-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Informasi Purchase Order</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'permintaanpembelian-t-grid',
	'dataProvider'=>$model->searchInformasi(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{pager}{summary}\n{items}",
	'columns'=>array(
		'nopermintaan',
		'tglpermintaanpembelian',
                                array(
                                    'header'=>'Nama supplier',
                                    'value'=>'$data->supplier->supplier_nama',
                                ),
                                array(
                                    'header'=>'Delivery date',
                                    'value'=>'$data->tglterimabarang',
                                ),
//                                'harganetto',
                                array(
                                    'header'=>'Term of payment',
                                    'value'=>'$data->supplier->supplier_termin',
                                ),
		'keteranganpermintaan',
                                array(
                                        'header'=>'Detail PO',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("InformasiRetail/detailBarangPO",array("id"=>$data->permintaanpembelian_id,"frame"=>true)),
                                                    array("class"=>"", 
                                                          "target"=>"iframeDetailPO",
                                                          "onclick"=>"$(\"#dialogDetailPO\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk melihat Detail PO",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),
                                array(

                                        'header'=>'Received',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("PenerimaanItems/index",array("id"=>$data->permintaanpembelian_id,"frame"=>true)),
                                                    array("class"=>"", 
                                                          "target"=>"iframeReceived",
                                                          "onclick"=>"$(\"#dialogReceived\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk Received barang",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),/*
                                array(
                                        'header'=>'Retur',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",#,
                                                    array("class"=>"", 
                                                          "target"=>"iframeRetur",
                                                          "onclick"=>"$(\"#dialogReturreceive\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk melihat Retur",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),*/
		/*
		'tglterimafaktur',
		'nosuratjalan',
		'tglsuratjalan',
		'gudangpenerima_id',
		'harganetto',
		'jmldiscount',
		'persendiscount',
		'totalpajakppn',
		'totalpajakpph',
		'totalharga',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
	),
)); ?>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'permintaanpembelian-t-search',
)); ?>
<table>
    <tr>
        <td>
                <div class="control-group">
                   <?php echo $form->label($model,'tglpermintaanpembelianAwal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglpermintaanpembelianAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
                <div class="control-group">
                   <?php echo $form->label($model,'tglpermintaanpembelianAkhir',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglpermintaanpembelianAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'nopermintaan',array('class'=>'span3')); ?>
            <?php echo $form->dropDownListRow($model,'supplier_id',CHtml::listData($model->getSupplierforInformasi(),'supplier_id','supplier_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
        </td>
    </tr>
</table>

	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.informasiRetail.'/InformasiPO'), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                    <?php
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailPO',
    'options'=>array(
        'title'=>'Detail Purchase Order',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetailPO" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogReceived',
    'options'=>array(
        'title'=>'Received',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
        'close'=>'js:function(){
            $.fn.yiiGridView.update("permintaanpembelian-t-grid", {
		data: $("#permintaanpembelian-t-search").serialize()
            });
        }',
    ),
));
?>
<iframe src="" name="iframeReceived" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>