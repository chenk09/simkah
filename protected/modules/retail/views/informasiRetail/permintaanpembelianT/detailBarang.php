<table width="100%" class="">
    <tr>
        <td>
                <table width="100%" border="1px">
                    <tr>
                        <td><?php echo CHtml::activeLabel($model,'nopermintaan'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->nopermintaan; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activeLabel($model,'tglpermintaanpembelian'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->tglpermintaanpembelian; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('Supplier code','supplier_id'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->supplier->supplier_kode; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('Nama supplier',''); ?></td>
                        <td>:</td>
                        <td><?php echo $model->supplier->supplier_nama; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::activelabel($model,'tglterimabarang'); ?></td>
                        <td>:</td>
                        <td><?php echo $model->tglterimabarang; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('Term of payment',''); ?></td>
                        <td>:</td>
                        <td><?php echo $model->supplier->supplier_termin; ?></td>
                    </tr>
                    <tr>
                        <td><?php echo CHtml::label('PO Created By',''); ?></td>
                        <td>:</td>
                        <td><?php echo $model->loginpemakai->pegawai->nama_pegawai; ?></td>
                    </tr>
                </table>
        </td>
        <td width="50%">
            
        </td>
    </tr>
</table>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'penerimaanbarang-t-grid',
	'dataProvider'=>$modDetailbarang->searchDetailbarang(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{pager}{summary}\n{items}",
	'columns'=>array(
		////'penerimaandetail_id',
                                array(
                                    'header'=>'Stok Code',
                                    'value'=>'$data->obatalkes->obatalkes_kode',
                                ),
                                array(
                                    'header'=>'Stok Name',
                                    'value'=>'$data->obatalkes->obatalkes_nama',
                                ),
//		'returdetail_id',
//		'penerimaanbarang_id',
                                array(
                                    'header'=>'Unit',
                                    'value'=>'$data->satuankecil->satuankecil_nama',
                                ),
//                                'harganetto',
                                array(
                                    'header'=>'PO QTY',
                                    'value'=>'$data->jmlpermintaan',
                                ),
                                array(
                                    'header'=>'PRICE / UNIT',
                                    'value'=>'$data->hargasatuanper'
                                ),
                                array(
                                    'header'=>'OTHER COST/ UNIT',
                                    'value'=>'$data->biaya_lainlain',
                                ),
                                array(
                                    'header'=>'PO NET',
                                    'type'=>'raw',
                                    'value'=>'$data->harganettoper',
                                ),
                                array(
                                    'header'=>'PO PPN',
                                    'type'=>'raw',
                                    'value'=>'$data->hargappnper',
                                ),
                                array(
                                    'header'=>'PO TOTAL',
                                    'type'=>'raw',
                                    'value'=>'($data->harganettoper) + ($data->hargappnper)',
                                ),
//                                array(
//                                    'header'=>'RCV PPN',
//                                    'value'=>'$data->harganettoper * $data->jmlterima',
//                                ),
//                                array(
//                                    'header'=>'RCV TOTAL',
//                                    'type'=>'raw',
//                                    'value'=>'($data->harganettoper * $data->jmlpermintaan) * ($data->harganettoper * $data->jmlterima)',
//                                ),
		/*
		'fakturdetail_id',
		'obatalkes_id',
		'satuanbesar_id',
		'jmlpermintaan',
		'jmlterima',
		'persendiscount',
		'jmldiscount',
		'harganettoper',
		'hargappnper',
		'hargapphper',
		'hargasatuanper',
		'tglkadaluarsa',
		'jmlkemasan',
		*/
	),
)); ?>
<?php 
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print()'))."&nbsp&nbsp"; 
        $this->widget('TipsMasterData',array('type'=>'informasi'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createUrl($module.'/'.$controller.'/DetailbarangPO');

$js = <<< JSCRIPT
function print()
{
    window.open("${urlPrint}"+"&id"=${id}"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>