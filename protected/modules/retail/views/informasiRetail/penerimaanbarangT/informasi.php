<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#penerimaanbarang-t-search').submit(function(){
	$.fn.yiiGridView.update('penerimaanbarang-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php
$module = Yii::app()->controller->module->id;
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenFaktur',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/fakturPembelian/index'),
)); ?>
    <?php echo CHtml::hiddenField('idPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<legend class="rim2">Informasi Receive</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'penerimaanbarang-t-grid',
	'dataProvider'=>$model->searchInformasi(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>"{pager}{summary}\n{items}",
	'columns'=>array(
		'noterima',
		'tglterima',
                                array(
                                    'header'=>'Nama supplier',
                                    'value'=>'$data->supplier->supplier_nama',
                                ),
                                array(
                                    'header'=>'Delivery date',
                                    'value'=>'$data->permintaanpembelian->tglterimabarang',
                                ),
//                                'harganetto',
                                array(
                                    'header'=>'Term of payment',
                                    'value'=>'$data->supplier->supplier_termin',
                                ),
		'keteranganterima',
                                array(
                                        'header'=>'Detail Receive',
                                        'type'=>'raw',
                                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("InformasiRetail/detailBarangReceive",array("id"=>$data->penerimaanbarang_id,"frame"=>true)),
                                                    array("class"=>"", 
                                                          "target"=>"iframeDetailreceive",
                                                          "onclick"=>"$(\"#dialogDetailreceive\").dialog(\"open\");",
                                                          "rel"=>"tooltip",
                                                          "title"=>"Klik untuk melihat Detail Receive",
                                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                                ),
                                array( 
                                   'header'=>'Retur',
                                   'type'=>'raw',
                                   'value'=>'(!empty($data->returpembelian_id) ? $data->retur->noretur : CHtml::link("<i class=\'icon-list-alt\'></i> ",Yii::app()->controller->createUrl("FakturPembelian/retur",array("idReturPembelian"=>$data->penerimaanbarang_id)) ,array("title"=>"Klik Untuk Melihat Detail Faktur", "rel"=>"tooltip")))',
                                ),  
                               array(
                                      'header'=>'Faktur Pembelian',//Details Faktur Ini dipakai juga di Informasi Penerimaan Faktur
                                      'type'=>'raw',
                                      'value'=>'(!empty($data->fakturpembelian_id) ? $data->fakturpembelian->nofaktur : CHtml::link("<i class=\'icon-list-alt\'></i> ","javascript:formFaktur(\'$data->penerimaanbarang_id\',\'$data->noterima\',\'$data->tglterima\')" ,array("title"=>"Klik Untuk Memasukan Faktur"))) ',
                                    ),
		/*
		'tglterimafaktur',
		'nosuratjalan',
		'tglsuratjalan',
		'gudangpenerima_id',
		'harganetto',
		'jmldiscount',
		'persendiscount',
		'totalpajakppn',
		'totalpajakpph',
		'totalharga',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
	),
)); ?>
<legend class="rim">Pencarian</legend>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'penerimaanbarang-t-search',
)); ?>

<table>
    <tr>
        <td>
                <div class="control-group">
                   <?php echo $form->label($model,'tglterimaAwal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglterimaAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
                <div class="control-group">
                   <?php echo $form->label($model,'tglterimaAkhir',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglterimaAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                )); 
                        ?>
                    </div>
                </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'noterima',array('class'=>'span3')); ?>
            <?php echo $form->dropDownListRow($model,'supplier_id',CHtml::listData($model->getSupplierforInformasi(),'supplier_id','supplier_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
        </td>
    </tr>
</table>

	<div class="form-actions">
                    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->module->id.'/'.informasiRetail.'/Informasireceive'), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                    <?php
                        $content = $this->renderPartial('../tips/informasi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailreceive',
    'options'=>array(
        'title'=>'Detail Receive',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetailreceive" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
$js = <<< JSCRIPT
function formFaktur(idPenerimaan,noPenerimaan,tglPenerimaan, noterima)
{
    $('#idPenerimaanForm').val(idPenerimaan);
    $('#noPenerimaanForm').val(noPenerimaan);
    $('#tglPenerimaanForm').val(tglPenerimaan);
    $('#form_hiddenFaktur').submit();
}

function formRetur(idPenerimaan,noPenerimaan,tglPenerimaan, noterima)
{
    $('#idPenerimaanForm').val(idPenerimaan);
    $('#noPenerimaanForm').val(noPenerimaan);
    $('#tglPenerimaanForm').val(tglPenerimaan);
    $('#form_hiddenFaktur').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);
?>

<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenRetur',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('target'=>'_new'),
        'action'=>Yii::app()->createUrl($module.'/fakturPembelian/Retur'),
)); ?>
    <?php echo CHtml::hiddenField('idPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglPenerimaanForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>
<?php    
// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogRetur',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Retur',
                        'autoOpen'=>false,
                        'minWidth'=>1100,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="iframeRetur" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

$js = <<< JSCRIPT
function formFaktur(idPenerimaan,noPenerimaan,tglPenerimaan)
{
    $('#idPenerimaanForm').val(idPenerimaan);
    $('#noPenerimaanForm').val(noPenerimaan);
    $('#tglPenerimaanForm').val(tglPenerimaan);
    $('#form_hiddenFaktur').submit();
}
JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);

?>