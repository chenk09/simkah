<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#formuliropname-r-search').submit(function(){
	$.fn.yiiGridView.update('formuliropname-r-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Informasi Formulir Stock Opname</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'formuliropname-r-grid',
	'dataProvider'=>$model->searchInformasi(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		'formuliropname_id',
                'tglformulir',
                'noformulir',
                'totalvolume',
		'totalharga',
                array(
                    'header'=>'Formulir',
                    'type'=>'raw',
                    'value'=>'CHtml::link(\'<icon class="icon-list">\', \'#\', array(\'onclick\'=>\'print("PRINT", \'.$data->formuliropname_id.\');\', \'style\'=>\'\'))',
                ),
                array(
                    'header'=>'Stok Opname',
                    'type'=>'raw',
                    'value'=>'(!empty($data->stokopname_id)) ? "Sudah Stok Opname" : CHtml::link(\'<icon class="icon-list">\', Yii::app()->createUrl(\'retail/StokOpnameT/Index\', array(\'id\'=>$data->formuliropname_id)))',
                ),
//		array(
//                        'name'=>'formuliropname_id',
//                        'value'=>'$data->formuliropname_id',
//                        'filter'=>false,
//                ),
//		'stokopname_id',
//		'tglformulir',


		/*
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'formuliropname-r-search',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'formuliropname_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'stokopname_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'tglformulir',array('class'=>'span5')); ?>
        <div class="control-group ">
            <label class='control-label'>
                <?php
                    echo CHtml::label('Tgl Formulir Stock Opname','tglFormulirStockOpname',array());
                ?>

                </label>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglformulir',
                    'mode' => 'date',
                    'options' => array(
                        'dateFormat' => Params::DATE_TIME_FORMAT,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </div>
        </div>
        <div class="control-group ">
            <label class='control-label'>
                <?php
                    echo CHtml::label('Sampai Dengan','sampaiDengan',array());
                ?>

                </label>
            <div class="controls">
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'date',
                    'options' => array(
                        'dateFormat' => Params::DATE_TIME_FORMAT,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </div>
        </div>

	<?php echo $form->textFieldRow($model,'noformulir',array('class'=>'span3','maxlength'=>50)); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
	</div>

<?php $this->endWidget(); ?>
<?php 

        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/Printformuliropname');
$formulir = $model->formuliropname_id;
$js = <<< JSCRIPT
function print(caraPrint, id)
{
    window.open("${urlPrint}/&id="+id+"&caraPrint="+caraPrint,"",'location=_new, width=1100px');

}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>