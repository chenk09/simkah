<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'reproduk-barang-m',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)','onsubmit'=>'return cekInput();'),
        'focus'=>'#',
)); ?>
<legend class="rim">Data Produk Barang</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
        <table id="obatalkes">
            <tr>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Category Name','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'jenisobatalkes_id', CHtml::listData($model->getJenisObatAlkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                          array('style'=>'width:190px','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)", 
                                                'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetSubkategori',array('encode'=>false,'namaModel'=>'REProdukBarangM')),
                                                              'update'=>'#REProdukBarangM_subjenis_id'))); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Sub Category Name','', array('class'=>'control-label', 'onchange'=>'ajaxGetKode();','onkeypress'=>"return nextFocus(this,event)")) ?>
                         <div class="controls">

                             <?php echo $form->dropDownList($model,'subjenis_id', CHtml::listData($model->getSubJenisItems(), 'subjenis_id', 'subjenis_nama'), 
                                                      array( 'onchange'=>'ajaxGetKode()','empty'=>'-- Pilih --','style'=>'width:190px','onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    
                    <div class="control-group">
                         <?php echo CHtml::label('Stock Code '."<font color='red'> * </font>",'', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_kode',array('style'=>'width:100px','empty'=>'-- Pilih --','class'=>'span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Barcode','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_barcode',array('style'=>'width:100px','empty'=>'-- Pilih --','class'=>'span2',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Stock Name'."<font color='red'> * </font>",'', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_nama',array('maxLength'=>30,'style'=>'width:160px','empty'=>'-- Pilih --','class'=>'span3',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Price Name'."<font color='red'> * </font>",'', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'obatalkes_namalain',array('maxLength'=>30,'style'=>'width:160px','empty'=>'-- Pilih --','class'=>'span3',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    
                </td>
                <td>
                    <div class="control-group">
                          <div class='control-group'>
                        <div class = 'control-label'>Active Date</div>
                        <div class="controls">  
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'activedate',
                                'mode' => 'datetime',
                                //                                          'maxDate'=>'d',
                                'options' => array(
                                    'dateFormat' =>'yy-mm-dd',
                                ),
                                'htmlOptions' => array('readonly' => true,
                                    'onkeypress' => "return $(this).focusNextInputField(event)"),
                            ));
                            ?>
                        </div>
                    </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Jumlah Isi','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'kekuatan',array('empty'=>'-- Pilih --','style'=>'width:80px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                             <?php echo $form->textField($model,'satuankekuatan',array('placeholder'=>'Satuan','empty'=>'-- Pilih --','style'=>'width:100px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Jml Dlm Kemasan','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'kemasanbesar',array('empty'=>'-- Pilih --','style'=>'width:80px',
                                                        'onkeypress'=>"return nextFocus(this,event)")); ?>
                         </div>
                    </div>
                     <div class="control-group">
                         <?php echo CHtml::label('Satuan Besar'."<font color='red'> * </font>",'', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'satuanbesar_id', CHtml::listData($model->getSatuanBesarItems(), 'satuanbesar_id', 'satuanbesar_nama'),
                                                      array('empty'=>'-- Pilih --','style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)",'options'=>array(Params::DEFAULT_IDSATUANBESAR=>array('selected'=>true)))); ?>
                         </div>
                    </div>
                   <div class="control-group">
                        <?php echo CHtml::label('Satuan Kecil'."<font color='red'> * </font>",'', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'satuankecil_id', CHtml::listData($model->getSatuanKecilItems(), 'satuankecil_id', 'satuankecil_nama'),
                                                      array('style'=>'width:100px','onkeypress'=>"return nextFocus(this,event)",'options' => array(Params::DEFAULT_IDSATUANKECIL=>array('selected'=>true)))); ?>
                         </div>
                    </div>
                    <div class="control-group">
                        <?php echo CHtml::label('Minimal Transaksi','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->dropDownList($model,'mintransaksi', 
                                                      array('empty'=>'-- Pilih --','1'=>'Ya','0'=>"Tidak"),array('style'=>'width:80px')); ?>
                         </div>
                    </div>
                </td>
            </tr>
        </table>
        <legend class="rim">Harga Produk</legend>
        <table>
            <tr>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Beli Netto','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'harganetto',array('onkeyup'=>'HitungHargaNettoPPN();','class'=>'inputFormTabel currency span2',
                                                      'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <div class="controls">

                            <?php echo CHtml::checkBox('cekhargajualppn',true,
                                    array('onkeypress'=>"return $(this).focusNextInputField(event)",'onclick'=>'HitungHargaNettoPPN()')) ?>PPN
                            <?php echo $form->textField($model,'ppn_persen',array('onkeyup'=>'HitungHargaNettoPPN();','style'=>'width:50px',
                                                       'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> %
                         </div>
                    </div>
                        
                                                      
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Jual','', array('class'=>'control-label')) ?>
                         <div class="controls">

                            <?php echo $form->textField($model,'hargajual',array('onkeyup'=>'hargamargin();','class'=>'inputFormTabel currency span2',
                                                       'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Margin','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            
                            <?php echo CHtml::textField('margin','',array('readonly'=>true,'class'=>'inputFormTabel currency span2','empty'=>'-- Pilih --','style'=>'width:50px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> GP
                         
                             <?php echo CHtml::textField('gp','',array('readonly'=>false,'empty'=>'-- Pilih --','style'=>'width:50px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(this,event)")); ?> %
                                                       
                         </div>
                    </div> 
                     <fieldset id="fieldsetMargin" class="">
                <legend class="accord1"><?php echo CHtml::checkBox('ubahmargin', true, array('onclick'=>'hitungubahmargin();','class'=>'inputFormTabel currency span2','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Ubah Margin 
                </legend>
                <div id='detail_data_pasien' class="toggle">
                <?php //echo $form->textFieldRow($modPasien,'anakke',array('class'=>'span1','maxlength'=>2, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                   <div class="control-group">
                         <?php echo CHtml::label('Margin','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            
                            <?php echo CHtml::textField('marginbaru','',array('onkeyup'=>'hitungubahmargin();','class'=>'inputFormTabel currency span2','style'=>'width:50px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(this,event)")); ?> GP
                         
                             <?php echo CHtml::textField('ubahgp','',array('class'=>'inputFormTabel currency span2','onkeyup'=>'HargaJualGp();','style'=>'width:50px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> %
                                                       
                         </div>
                    </div> 
                </td>
                <td>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Maksimal','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargamaksimal','',array('onblur'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> 
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Minimal','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargaminimal','',array('onblur'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> 
                         </div>
                    </div>
                    <div class="control-group">
                         <?php echo CHtml::label('Harga Rata-rata','',array('class'=>'control-label')); ?>
                         <div class="controls">

                            <?php echo CHtml::textField('hargaratarata','',array('onblur'=>'hitungHargaJual();','class'=>'inputFormTabel currency span2','style'=>'width:100px',
                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> 
                         </div>
                    </div> 
                </td>
            </tr>
        </table>
           <div class="form-actions">
		                <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.produkBarangM.'/create'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
            $content = $this->renderPartial('../tips/transaksiProdukRetail',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
           </div>

<?php $this->endWidget(); ?>
<div id="testForm"></div>

<?php
$js= <<<JS

function HitungHargaNettoPPN() {

 if ($("#cekhargajualppn").is(":checked")) {
        var hargaNetto = unformatNumber($("#REProdukBarangM_harganetto").val());
        var hargaJual = unformatNumber($("#REProdukBarangM_hargajual").val());
        var hargaratarata = unformatNumber($("#hargaratarata").val());
        var hargamaksimal = unformatNumber($("#hargamaksimal").val());
        var hargaminimal = unformatNumber($("#hargaminimal").val());
        var margin = unformatNumber($("#margin").val());
        var gp = unformatNumber($("#gp").val());
        var ppn = parseFloat($("#REProdukBarangM_ppn_persen").val());

        var jmlharganetto = hargaNetto;
        harganettoplusppn = hargaNetto + hargappn;
        var hargappn = (100 + ppn) / 100;
        var harganettoppn = harganettoppn * hargappn;
        var hargaJual = hargaNetto * hargappn ;
        var hargajualnonppn = harganettoplusppn - harganettoppn;
        var margin = hargajualnonppn - hargaNetto;
        var jmlmargin = margin;
        var gp = (margin / hargajualnonppn) * 100;
        var jmlgp = gp;


        $("#REProdukBarangM_hargajual").val(formatUang(hargaJual));
        $("#hargamaksimal").val(formatUang(jmlharganetto));
        $("#hargaminimal").val(formatUang(jmlharganetto));
        $("#hargaratarata").val(formatUang(jmlharganetto));
        $("#REProdukBarangM_ppn_persen").val(formatUang(ppn));
        $("#margin").val(formatUang(margin));
        $("#gp").val(formatUang(gp));
    }else{
        var hargaNetto = unformatNumber($("#REProdukBarangM_harganetto").val());
        var harganetto = hargaNetto;
        var hargaJual = unformatNumber($("#REProdukBarangM_hargajual").val());
        var ppn = unformatNumber($("#REProdukBarangM_ppn_persen").val());

        var jmlppn = 10;
        var hargappn = (hargaNetto * ppn ) / 100;
        var jmlhargajualnonppn = hargaJual - hargappn;
        var jmlmargin = jmlhargajualnonppn - hargaNetto;
        var jmlgp = (margin / jmlhargajualnonppn) * 100;
        
        $("#REProdukBarangM_ppn_persen").val(formatUang(jmlppn));
        $("#REProdukBarangM_hargajual").val(formatUang(jmlhargajualnonppn));
        $("#margin").val(formatUang(jmlmargin));
        $("#gp").val(formatUang(jmlgp));
    }
}
    

function HargaJualGp(){
    var harganetto = unformatNumber($("#REProdukBarangM_harganetto").val());
    var margin = unformatNumber($("#margin").val());
    var hargajual = unformatNumber($("#REProdukBarangM_hargajual").val());
    var hargaratarata = unformatNumber($("#hargaratarata").val());
    var ppn = unformatNumber($("#REProdukBarangM_ppn_persen").val());
    var gp = $("#ubahgp").val();
    
    var jmlppn = (100 + ppn)/100;
    var jmlgp = gp / 100;
    var harganettoppn = harganetto * jmlppn;
    var jmlhargajualgp = margin / jmlgp;
    var jmlmargin = hargajual - harganetto;

    $("#REProdukBarangM_hargajual").val(formatUang(jmlhargajualgp));
    $("#margin").val(formatUang(jmlmargin));
    $("#gp").val(formatUang(gp));
}
// ============================= Akhir Script Ubah Margin ======================================= //
// ============================= Script Check Input ======================================= //

function cekInput()
{

    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
    
// ============================= Akhir Script Check Input ======================================= //
JS;
Yii::app()->clientScript->registerScript('hargajual',$js,CClientScript::POS_HEAD);
?>

<?php 
Yii::app()->clientScript->registerScript('ubahmargin',"
    $('#ubahmargin').show();    
    $('#ubahmargin').change(function(){
        if ($(this).is(':checked')){
                $('#fieldsetMargin input').not('input[type=checkbox]').removeAttr('disabled');
                $('#fieldsetMargin select').removeAttr('disabled');
        }else{
                $('#fieldsetMargin input').not('input[type=checkbox]').attr('disabled','true');
                $('#fieldsetMargin select').attr('disabled','true');
                $('#fieldsetMargin input').attr('value','');
                $('#fieldsetMargin select').attr('value','');
        }
        $('#detail_data_pasien').slideToggle(500);
    });
");
?>

<?php 
$js2 = <<<JS
function hargamargin()
{
        var hargajual = unformatNumber($('#REProdukBarangM_hargajual').val());
        var jmlhargajual = hargajual;
        var harganetto = unformatNumber($('#REProdukBarangM_harganetto').val());
        var jmlharganetto = harganetto;
        var margin = unformatNumber($('#margin').val());
        var jmlmargin = margin;
        var gp = unformatNumber($('#gp').val());
        var ppn = unformatNumber($('#REProdukBarangM_ppn_persen').val());
        
        var hargappn = (100 + ppn) / 100;
        var jmlhargappn = (harganetto * 10)/100;
        var harganettoppn = harganetto * hargappn;
        var hargajualnonppn = harganettoppn - jmlhargappn;
        var jmlharganettoppn = harganettoppn;
        var jmlgp = (margin / hargajualnonppn) * 100;
        var jumlahmargin = jmlhargajual - jmlharganetto;


        $('#margin').val(formatUang(jumlahmargin));
        $('#gp').val(formatUang(jmlgp));
    }
JS;
Yii::app()->clientScript->registerScript('hargajualppn',$js2,CClientScript::POS_HEAD);
?>

<?php 
$js3 = <<<JS

// ========================== Scrip Ubah Margin ===================================
    
function hitungubahmargin() {
    
     if ($("#ubahmargin").is(":checked")) {
      var marginbaru = unformatNumber($("#marginbaru").val()); 
      var jmlmarginbaru = marginbaru;
      var gpbaru = unformatNumber($("#ubahgp").val());
      var ppn = unformatNumber($("#REProdukBarangM_ppn_persen").val());
      var marginlama = unformatNumber($("#margin").val());
      var gplama = unformatNumber($("#gp").val());
      var harganetto = unformatNumber($("#REProdukBarangM_harganetto").val());
      var hargajual = unformatNumber($("#REProdukBarangM_hargajual").val());
      var jmlhargajual = hargajual;
      
      var jmlppn = (100 + ppn) / 100;
      var harganettoppnpersen = harganetto * jmlppn;
      var margin = hargajual - harganetto;
      var jmlhargajualmargin = marginbaru + harganetto;
      var jmlgp = (marginlama / hargajual) * 100;

      
      $("#REProdukBarangM_hargajual").val(formatUang(jmlhargajualmargin));
      $("#margin").val(formatUang(margin));
      $("#gp").val(formatUang(jmlgp));
      
     }
     
}

JS;
Yii::app()->clientScript->registerScript('hitunghargamargin',$js3,CClientScript::POS_HEAD);
?>

<?php 
   $urlKode = $this->createUrl('GantiKode');
    
    $js = <<< JS
   
   
     function ajaxGetKode(){
        subjenisobatalkes_id = $('#REProdukBarangM_subjenis_id').val();
        $.post('${urlKode}', {subjenisobatalkes_id:subjenisobatalkes_id},function(data){
            $('#REProdukBarangM_obatalkes_kode').val(data.obatalkes_kode);
            //$('#obatalkes tbody').html(data);
            //setAll();
        },'json');
    }
        

JS;
    Yii::app()->clientScript->registerScript('onheadDialog', $js, CClientScript::POS_HEAD);
    ?>