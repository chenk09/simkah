
<legend class="rim2">Informasi Produk Barang </legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('reproduk-barang-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'produk-barang-m-search',
        'type'=>'horizontal',
)); ?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'reproduk-barang-m-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
                 array(
                        'header'=>'Stock Code',
                        'name'=>'stock_code',
                        'value'=>'$data->stock_code',  
                ),
		 array(
                        'header'=>'Stock Name',
                        'name'=>'stock_name',
                        'value'=>'$data->stock_name',
                ),
                 array(
                        'header'=>'Price Name',
                        'name'=>'price_name',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\"icon-print\"></i>", "javascript:printprice(\'$data->brg_id\',\'$data->stock_name\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint Price Tag"))." ".CHtml::link($data->price_name, "javascript:print(\'$data->brg_id\',\'$data->stock_name\');", array("rel"=>"tooltip","title"=>"Klik untuk mengeprint Price Tag"))',
                ),
                array(
                        'header'=>'Barcode',
                        'name'=>'barang_barcode',
                        'value'=>'$data->barang_barcode',
                        
                ),
                array(
                        'header'=>'Harga Jual +PPN',
                        'name'=>'hargajual',
                        'value'=>'$data->hargajual'
                        
                ),
                array(
                        'header'=>'Discount',
                        'name'=>'discount',
                        'value'=>'$data->discount',
                        
                ),
                array(
                        'header'=>'PPN',
                        'name'=>'ppn_persen',
                        'value'=>'$data->ppn_persen',
                        
                ),
                array(
                      'header'=>'Harga Jual -PPN',
                      'name'=>'harganetto',
                      'value'=>'$data->harganetto'
                ),
                array(
                        'header'=>'Moving Average',
                        'name'=>'hargajual',
                        'value'=>'$data->hargajual'
                        
                ),
                array(
                        'header'=>'Margin',
                        'name'=>'margin',
                        'value'=>'$data->margin'
                        
                ),
            
                 array(
                        'header'=>'GP %',
                        'name'=>'gp_persen',
                        'value'=>'$data->gp_persen',
                        
                ),
//                
//		array(
//                    'id'=>'printBandrol',
//                    'class'=>'CCheckBoxColumn',
//                    'selectableRows' => '50',   
//                    'value'=>'$data->brg_id',
//                ),
              array(
                        'header'=>'Price Tag',
                        'value'=>'CHtml::checkBox("REProdukposV[brg_id][]", false, array("value"=>$data->brg_id,"id"=>"pricetag","class"=>"inputFormTabel currency span2","onkeypress"=>"return $(this).focusNextInputField(event)"))',
//                  CHtml::hiddenField("REProdukBarangM[$data->brg_id][brg_id]", $data->brg_id, array("id"=>"brg_id","class"=>"span1 numbersOnly margin"))
                        'type'=>'raw',
                ),

		
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->endWidget();?>
<legend class="rim">Pencarian Produk Barang</legend>
<?php $this->renderPartial('_searchinformasiproduk',array(
	'model'=>$model,
)); ?>

<?php 
Yii::app()->clientScript->registerScript('onheadfunction','
    function slide(data){
        $("."+data).slideToggle();
    }

   function openDialogini(){
        $("#dialogDetails").dialog("open");
    }
    
    function setVolume(){
        var value = $("#fisiks").val();
        $("#reharga-jual-m-grid").find(".margin").each(function(){
            $(this).val(value);
            value++;
        });
        
    }
    
    function PrintPriceTag(obj){
     parent = $(obj).parents("tr");
      if ($("#pricetag").is(":checked")) {
           var idBrg = parent.find("#brg_id").val();
           alert(idBrg);
        }else{
        
        }
        
    }
    
    
', CClientScript::POS_HEAD); 
?>

<?php 
    
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
    $js = <<< JSCRIPT

function printprice(id,nama)
   {    
               window.open('${url}/printPrice/id/'+id+'/nama/'+nama,'printwin','left=100,top=100,width=310,height=200,scrollbars=0');
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('jsprintprice',$js, CClientScript::POS_HEAD);
?>
