<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'search',
        'type'=>'horizontal',
)); ?>


<table>
    <tr>
        <td>
            <div class="control-group">
                 <?php echo CHtml::label('Stok Code','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($model,'stock_code',array('style'=>'width:120px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Barcode','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($model,'barang_barcode',array('style'=>'width:120px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Stock Name','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($model,'stock_name',array('style'=>'width:160px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Price Name','', array('class'=>'control-label')) ?>
                <div class="controls">

                    <?php echo $form->textField($model,'price_name',array('empty'=>'-- Pilih --','style'=>'width:160px','onkeypress'=>"return nextFocus(this,event)")); ?>
                </div>
            </div>
        </td>
        <td>
            <div class="control-group">
                 <?php echo CHtml::label('Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                    <?php echo $form->dropDownList($model,'category_id', CHtml::listData($model->getJenisObatAlkesItems(), 'jenisobatalkes_id', 'jenisobatalkes_nama'), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 
                                        'ajax'=>array('type'=>'POST',
                                                      'url'=>Yii::app()->createUrl('ActionDynamic/GetSubkategoriProduk',array('encode'=>false,'namaModel'=>'REProdukposV')),
                                                      'update'=>'#REProdukposV_subcategory_id'))); ?>
                 </div>
            </div>
            <div class="control-group">
                 <?php echo CHtml::label('Sub Category Name','', array('class'=>'control-label')) ?>
                 <div class="controls">

                     <?php echo $form->dropDownList($model,'subcategory_id', array(), 
                                  array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", )); ?>
                 </div>
            </div>
        </td>
    </tr>
</table>

	<div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit')); 
                ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.produkBarangM.'/informasi'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                <?php  echo CHtml::htmlButton(Yii::t('mds','{icon} Print Tag',array('{icon}'=>'<i class="icon-book icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";  ?>
                <?php
                    $content = $this->renderPartial('../tips/informasi',array(),true);
                    $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                ?>
	</div>

<?php $this->endWidget(); ?>
<?php 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printPriceTag');

$js = <<< JSCRIPT
function print(caraPrint)
{
    //console.log($('#produk-barang-m-search').serialize());
    window.open("${urlPrint}/"+$('#produk-barang-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px,scrollbars=1');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>