
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>11));      

$table = 'ext.bootstrap.widgets.BootGridView';
    $sort = true;
    if (isset($caraPrint)){
        $data = $model->searchPrint();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
    } else{
        $data = $model->searchPrint();
         $template = "{pager}{summary}\n{items}";
    }
  ?>  

<?php $this->widget($table,array(
	'id'=>'gfobat-alkes-m-grid',
	'dataProvider'=>$data,
//	'filter'=>$model,
        'template'=>$template,
        'enableSorting'=>$sort,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
                 array(
                        'header'=>'Category / <br> Sub Category',
                        'name'=>'category_name',
                        'value'=>'$data->category_name." / ".$data->subcategory_name'  
                ),
		 array(
                        'header'=>'Stock Code / <br> Barcode ',
                        'name'=>'stock_code',
                        'value'=>'$data->stock_code." / ".$data->barang_barcode'
                ),
                 array(
                        'header'=>'Stock Name / <br> Price Name',
                        'name'=>'stock_name',
                        'value'=>'$data->stock_name." / ".$data->stock_name' 
                ),
                array(
                        'header'=>'Jml Dlm Kemasan / <br> Minimal Stok',
                        'name'=>'jmldalamkemasan',
                        'value'=>'$data->jmldalamkemasan." / ".$data->minimalstok'
                        
                ),
                array(
                        'header'=>'Satuan Besar / <br> Satuan Kecil',
                        'name'=>'satuanbesar_nama',
                        'value'=>'$data->satuanbesar_nama." / ".$data->satuankecil_nama',
                        
                ),
                array(
                        'header'=>'Harga Jual',
                        'name'=>'hargajual',
                        'value'=>'$data->hargajual',
                        
                ),
                array(
                        'header'=>'Discount',
                        'name'=>'discount',
                        'value'=>'$data->discount',
                        
                ),
                 array(
                        'header'=>'Harga Rata-rata',
                        'name'=>'hargajual',
                        'value'=>'$data->hargajual',
                        
                ),
                array(
                        'header'=>'Margin',
                        'name'=>'margin',
                        'value'=>'$data->margin',
                        
                ),
                array(
                        'header'=>'GP %',
                        'name'=>'gp_persen',
                        'value'=>'$data->gp_persen',
                        
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>