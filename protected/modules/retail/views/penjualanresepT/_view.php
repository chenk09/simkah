<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjualanresep_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->penjualanresep_id),array('view','id'=>$data->penjualanresep_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reseptur_id')); ?>:</b>
	<?php echo CHtml::encode($data->reseptur_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasienadmisi_id')); ?>:</b>
	<?php echo CHtml::encode($data->pasienadmisi_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('penjamin_id')); ?>:</b>
	<?php echo CHtml::encode($data->penjamin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('carabayar_id')); ?>:</b>
	<?php echo CHtml::encode($data->carabayar_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pendaftaran_id')); ?>:</b>
	<?php echo CHtml::encode($data->pendaftaran_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruangan_id')); ?>:</b>
	<?php echo CHtml::encode($data->ruangan_id); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('pegawai_id')); ?>:</b>
	<?php echo CHtml::encode($data->pegawai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kelaspelayanan_id')); ?>:</b>
	<?php echo CHtml::encode($data->kelaspelayanan_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pasien_id')); ?>:</b>
	<?php echo CHtml::encode($data->pasien_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglpenjualan')); ?>:</b>
	<?php echo CHtml::encode($data->tglpenjualan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jenispenjualan')); ?>:</b>
	<?php echo CHtml::encode($data->jenispenjualan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tglresep')); ?>:</b>
	<?php echo CHtml::encode($data->tglresep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('noresep')); ?>:</b>
	<?php echo CHtml::encode($data->noresep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totharganetto')); ?>:</b>
	<?php echo CHtml::encode($data->totharganetto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalhargajual')); ?>:</b>
	<?php echo CHtml::encode($data->totalhargajual); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totaltarifservice')); ?>:</b>
	<?php echo CHtml::encode($data->totaltarifservice); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayaadministrasi')); ?>:</b>
	<?php echo CHtml::encode($data->biayaadministrasi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('biayakonseling')); ?>:</b>
	<?php echo CHtml::encode($data->biayakonseling); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pembulatanharga')); ?>:</b>
	<?php echo CHtml::encode($data->pembulatanharga); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jasadokterresep')); ?>:</b>
	<?php echo CHtml::encode($data->jasadokterresep); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('instalasiasal_nama')); ?>:</b>
	<?php echo CHtml::encode($data->instalasiasal_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ruanganasal_nama')); ?>:</b>
	<?php echo CHtml::encode($data->ruanganasal_nama); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('discount')); ?>:</b>
	<?php echo CHtml::encode($data->discount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subsidiasuransi')); ?>:</b>
	<?php echo CHtml::encode($data->subsidiasuransi); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subsidipemerintah')); ?>:</b>
	<?php echo CHtml::encode($data->subsidipemerintah); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subsidirs')); ?>:</b>
	<?php echo CHtml::encode($data->subsidirs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('iurbiaya')); ?>:</b>
	<?php echo CHtml::encode($data->iurbiaya); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lamapelayanan')); ?>:</b>
	<?php echo CHtml::encode($data->lamapelayanan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_time')); ?>:</b>
	<?php echo CHtml::encode($data->create_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_time')); ?>:</b>
	<?php echo CHtml::encode($data->update_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->create_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_loginpemakai_id')); ?>:</b>
	<?php echo CHtml::encode($data->update_loginpemakai_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_ruangan')); ?>:</b>
	<?php echo CHtml::encode($data->create_ruangan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('returresep_id')); ?>:</b>
	<?php echo CHtml::encode($data->returresep_id); ?>
	<br />

	*/ ?>

</div>