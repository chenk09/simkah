<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<iframe id="framePrint" src="<?php echo Yii::app()->user->getFlash('url');?>" style="height:0px;">
</iframe>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'repenjualanresep-t-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#barang_barcode',
        ));
?>
<?php echo $form->errorSummary($model); ?>
<?php if (isset($modDetails)){echo $form->errorSummary($modDetails); }?>
<!--<p class="help-block"><?php echo Yii::t('mds', 'Fields with <span class="required">*</span> are required.') ?></p>-->
<table>
    <tr>
        <td width="50%">
            <div class="grouping">
                <div class="control-group">
                    <?php echo $form->labelEx($modObat, 'barang_barcode', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <div class="input-append">
                            <?php echo $form->textField($modObat, 'barang_barcode', array('onkeypress' => "if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){setAjax(this);}}else{return $(this).focusNextInputField(event)}", 'class' => 'span2', 'placeholder' => $modObat->getAttributeLabel('barang_barcode'), "id"=>"barang_barcode", 'style'=>'float:left')); ?>
                            <span class="add-on"><i class="icon-list-alt"></i></a></span>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($modObat, 'stock_name', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->hiddenField($modObat, 'brg_id', array('readonly' => true)) ?>
                        <?php
                        $this->widget('MyJuiAutoComplete', array(
                            'model' => $modObat,
                            'attribute' => 'stock_name',
                            'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/getStockBarangRetail'),
                            'options' => array(
                                'showAnim' => 'fold',
                                'minLength' => 2,
                                'focus' => 'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modObat, 'brg_id').'").val( ui.item.brg_id );
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                'select' => 'js:function( event, ui ) {
                                                $("#'.CHtml::activeId($modObat, 'brg_id').'").val( ui.item.brg_id );
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                            ),
                            'htmlOptions' => array('onkeypress' => "if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){submitStok();}}else{return $(this).focusNextInputField(event)}", 'class' => 'span2', 'placeholder' => $modObat->getAttributeLabel('stock_name')),
                            'tombolDialog' => array('idDialog' => 'dialogStock', 'idTombol' => 'tombolStockName'),
                        ));
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <?php echo $form->labelEx($modObat, 'stock_code', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <div class="input-append">
                            <?php echo $form->textField($modObat, 'stock_code', array('onkeypress' => "if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){setAjax(this);}}else{ if (event.keyCode == 13){ $('#antrian').focus()}}", 'class' => 'span2', 'placeholder' => $modObat->getAttributeLabel('barang_code'), "id"=>"stock_code",'style'=>'float:left')); ?>
                            <span class="add-on"><i class="icon-list-alt"></i></a></span>
                        </div>
                    </div>
                </div>
                
                <?php //echo $form->textFieldRow($modObat, 'stock_code', array('onkeypress' => "if (event.keyCode == 13){submitStok();}return $(this).focusNextInputField(event)", 'class' => 'span2', 'placeholder' => $modObat->getAttributeLabel('barang_code'), 'append'=>'<i class="icon-search"></i>')); ?>
                <?php //echo $form->textFieldRow($modObat, 'barang_barcode', array('onkeypress' => "if (event.keyCode == 13){submitStok();}return $(this).focusNextInputField(event)", 'class' => 'span2', 'placeholder' => $modObat->getAttributeLabel('barang_barcode'))); ?>
<!--                <div class="control-group">
                    <?php //echo Chtml::label('Qty', '', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php //echo Chtml::textField('qty', 1, array('class' => 'numbersOnly span1', 'onkeypress'=>'return $(this).focusNextInputField(event)')); ?>
                    </div>
                </div>-->
            </div>
        </td>
        <td width="50%">
            <div class="pull-right" style="-webkit-border-radius:3px;-moz-border-radius:3px;border:1px solid #cccccc; margin-top:-10px;padding:5px;height:100%;margin-right: 100px;">
                <table width="100%" style="margin:auto;">
                    <tbody>
                    <tr>
                        <td width="150">
                            <div class="nama_obat"><center>-- Nama Obat --</center></div>
                        </td>
                        <td rowspan="3" style="text-align:right;">
                            <div class="totalharga" style="color:red;font-size:80px;vertical-align:middle;font-family: digitalFont"><?php echo (!empty($model->totalhargajual)) ? MyFunction::formatNumber($model->totalhargajual-$model->discount+$model->biayaadministrasi) : 0; ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="harga_obat"><center>-- Harga Obat --</center></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="qty_obat"><center>-- Qty/Stok --</center></div>
                        </td>
                    </tr> 
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
</table>



<table id="tblStockBarang" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No</th>
            <th>Stock Code</th>
            <th>Stock Nama</th>
            <th>Harga Jual</th>
            <th>Qty</th>
<!--            <th>Disc (%)</th>
            <th>Disc (Rp)</th>-->
            <th>Sub Total</th>
            <?php if ($model->isNewRecord): ?>
            <th>Batal</th>
            <?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php if (isset($modDetails)) {
          foreach ($modDetails as $i=>$obat){
              if (!$model->isNewRecord){
                  $i++;
              }
            $qty = $obat->qty_oa;
//            echo $obat->obatalkes_id;
//            exit();
            $modBarang = ProdukposV::model()->findByAttributes(array('brg_id'=>$obat->obatalkes_id));
            $criteria = new CDBCriteria();
            $criteria->compare('obatalkes_id',$modBarang->brg_id);
            $criteria->order = 'tglstok_in '.((Params::KONFIG_LIFO) ? "DESC" : "ASC");
            $modStok = StokobatalkesT::model()->find($criteria);
            $data["stok"] = StokobatalkesT::getStokBarang($obat->obatalkes_id);
            $harga = ($obat->hargajual_oa < 1) ? 1 : $obat->hargajual_oa;
            $data["disc"] = ($model->isNewRecord) ? $modStok->discount : $obat->discount*100/$harga;
            $data["sub"] = ($model->isNewRecord) ? $modBarang->hargajual : $obat->hargajual_oa/$qty;
            $data["netto"] =$modBarang->harganetto;
            $data["disc"] = round($data["disc"],2);
            $obat->obatalkes_id = $modBarang->brg_id;
            if (!$model->isNewRecord){
                $modStok->hargajual_oa = $obat->hargajual_oa/$qty;
            }
            else{
                $modStok->hargajual_oa = $modBarang->hargajual;
            }
//            $obat->qty_oa = ($data["stok"] > $qty) ? $qty : 0 ;
            $obat->hargajual_oa = ((!empty($modStok->hargajual_oa)) ? $modStok->hargajual_oa : 0 )*$qty;
            $obat->harganetto_oa = ((!empty($modStok->harganetto_oa)) ? $modStok->harganetto_oa : 0 )*$qty;
            $obat->discount = ($model->isNewRecord) ? $modStok->discount*$modStok->hargajual_oa*$qty/100 : $obat->discount;
            $subtotal += $obat->hargajual_oa;
              ?>
              <tr> 
                <td>
                    <?php echo CHtml::activeTextField($obat,'['.$i.']no',array('readonly'=>true,'class'=>'span1 no','value'=>$i)) ?>
                </td>
                <td><?php 
                    echo Chtml::hiddenField("stok",$data["stok"], array('class'=>'stok span1', 'readonly'=>true));
                    echo CHtml::activeHiddenField($obat, '['.$i.']obatalkes_id', array('class'=>'barang span1', 'readonly'=>true));
                    echo CHtml::hiddenField("hargajual",$data["sub"], array("readonly"=>true, "class"=>"span2 harga"));
                    echo CHtml::hiddenField("harganetto",$data["netto"], array("readonly"=>true, "class"=>"span2 netto"));
                    echo CHtml::activeHiddenField($obat, '['.$i.']sumberdana_id', array('class'=>'span1', 'readonly'=>true));
                    echo CHtml::hiddenField('discount_rp',$data["disc"], array('class'=>'disc numbersOnly span1', 'onkeyup'=>'getDiscount(this);','onblur'=>'getDiscount(this);'));
                    echo CHtml::activeHiddenField($obat,'['.$i.']discount',array('class'=>'disc_rp numbersOnly span1', 'onkeyup'=>'getTotal(this);','onblur'=>'getTotal(this);'));
            //         echo CHtml::activeHiddenField($modDetail, '[]supplier_id', array('class'=>'barang'));
                    echo $modBarang->stock_code; 
                    ?>
                </td>
                <td><?php echo $modBarang->stock_name; ?></td>
                <td><?php echo $data["sub"]; ?></td>
                <td><?php echo CHtml::activeTextField($obat,'['.$i.']qty_oa',array('class'=>'qty numbersOnly span1', 'onkeyup'=>'getSubTotal(this);', 'onblur'=>'getSubTotal(this);', 'onkeypress'=>'return $(this).focusNextInputField(event)')) ?></td>
<!--                <td>-->
                    
<!--                </td>-->
<!--                <td>-->
                    
<!--                </td>-->
                <td>
                    <?php 
                        echo CHtml::activeHiddenField($obat, '['.$i.']hargajual_oa', array('class'=>'sub span2', 'readonly'=>true));
                        echo CHtml::activeHiddenField($obat, '['.$i.']harganetto_oa', array('class'=>' subnetto span2', 'readonly'=>true));
                        ?>
                    <div class="text_sub" style="text-align:right"><?php echo MyFunction::formatNumber($obat->hargajual_oa);?></div>
                </td>
                <?php if ($model->isNewRecord) : ?>
                <td><?php echo Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel')); ?></td>
                <?php endif; ?>
            </tr>      
          <?php }
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <?php 
            $total = ((!empty($model->totalhargajual)) ? $model->totalhargajual : 0);
            $discount = ((!empty($model->discount)) ? $model->discount: 0);
//            $pajak= ((!empty($model->biayaadministrasi)) ? $model->biayaadministrasi: 0);
            $subtotal = ((!empty($subtotal)) ? ((!empty($model->discount)) ? $subtotal-$model->discount:$subtotal ): 0);
            $subtotal = MyFunction::formatNumber($subtotal);
            $total = MyFunction::formatNumber($total);
            $discount = MyFunction::formatNumber($discount);
//            $pajak = MyFunction::formatNumber($pajak);
            ?>
            <td colspan="5" style="text-align: right;">Sub Total</td><td>
                Rp.
                <?php echo $form->hiddenField($model, 'totalhargajual', array('readonly' => true, 'class' => 'span2 subtotal', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->hiddenField($model, 'totharganetto', array('readonly' => true, 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="text_subtotal" style="text-align: right;">
                <?php echo $total; ?>
                </div>
            </td>
            <?php if ($model->isNewRecord) : ?>
            <td rowspan="4"></td>
            <?php endif;?>
        </tr>
        <tr>
            <td colspan="5" style="text-align: right;"><?php echo $model->getAttributeLabel('discount'); ?></td>
            <td>Rp.
            <?php echo $form->hiddenField($model, 'discount', array('class' => 'span2', 'readonly' => true, 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="text_discount" style="text-align: right;"><?php echo $discount; ?></div>
            </td>
        </tr>
<!--        <tr>
            <td colspan="7" style="text-align: right;">Pajak</td>
            <td>Rp.
            <?php echo $form->hiddenField($model, 'biayaadministrasi', array('class' => 'span2', 'readonly' => true, 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="text_pajak" style="text-align: right;"><?php echo $pajak; ?></div>
            </td>
        </tr>-->
        <tr>
            <td colspan="5" style="text-align: right;"><?php echo ($model->isNewRecord) ? CHtml::checkBox("antrian",true, array('onkeypress'=>'return $(this).focusNextInputField(event)')) : "";?> Total Tagihan</td><td>Rp.<?php echo CHtml::hiddenField('subtotal', $subtotal, array('class' => 'span2', 'readonly' => true, 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
                <div class="text_total" style="text-align:right"><?php echo $subtotal;?></div></td>
        </tr>
    </tfoot>
</table> 
<?php //echo $form->textFieldRow($model,'reseptur_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'pasienadmisi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'penjamin_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'carabayar_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'kelaspelayanan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'tglpenjualan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'jenispenjualan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
<?php //echo $form->textFieldRow($model,'tglresep',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'noresep',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
<?php //echo $form->textFieldRow($model,'totharganetto',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'totalhargajual',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'totaltarifservice',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'biayaadministrasi',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'biayakonseling',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'pembulatanharga',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'jasadokterresep',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'instalasiasal_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
<?php //echo $form->textFieldRow($model,'ruanganasal_nama',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
<?php //echo $form->textFieldRow($model,'discount',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'subsidiasuransi',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'subsidipemerintah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'subsidirs',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'iurbiaya',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'lamapelayanan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'create_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<?php //echo $form->textFieldRow($model,'update_time',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"));  ?>
    <?php //echo $form->textFieldRow($model,'update_loginpemakai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'create_ruangan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($model,'returresep_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
<div class="form-actions">
    <?php
    
    if (!$model->isNewRecord){
        $urlDialog = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/bayar', array('id'=>$model->penjualanresep_id,"frame"=>1));
        if (empty($antrian)){
            echo CHtml::Link("<i class=\"icon-shopping-cart icon-white\"></i> Pembayaran",$urlDialog,
                                            array("class"=>"btn btn-primary", 
                                                  "onclick"=>"$(\"#dialogPembayaran\").dialog(\"open\");",
                                                  "rel"=>"tooltip",
                                                  'target'=>'framePembayaran',
                                                  "title"=>"Klik untuk Pembayaran",));
        }
    }else{
        echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')) :
                    Yii::t('mds', '{icon} Save', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onKeypress' => 'return formSubmit(this,event)'));
        
    }
    ?>
    
<?php
echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), Yii::app()->createUrl($this->module->id . '/' . penjualanresepT . '/create'), array('class' => 'btn btn-danger',
    'onclick' => 'if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;','onkeypress' => 'if (event.keyCode == 13){ if(!confirm("' . Yii::t('mds', 'Do You want to cancel?') . '")) return false;}'));
?>
</div>

<?php $this->endWidget(); ?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogStock',
    'options'=>array(
        'title'=>'Daftar Stock Name',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modStock = new REProdukposV();
if (isset($_GET['REProdukposV'])){
    $modStock->attributes = $_GET['REProdukposV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'produkpos-v-grid', 
	'dataProvider'=>$modStock->searchData(),
	'filter'=>$modStock,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                        "id" => "selectPegawai",
                                        "href"=>"",
                                        "onClick" => "
                                                      $(\"#'.CHtml::activeId($modStock, 'brg_id').'\").val($data->brg_id);
                                                      $(\"#'.CHtml::activeId($modStock, 'stock_name').'\").val(\"$data->stock_name\");
                                                      $(\"#dialogStock\").dialog(\"close\"); 
                                                      submitStok();
                                                      return false;
                                            "))',
                    ),
                'stock_code',
                'stock_name',
                'satuanbesar_nama',
                'satuankecil_nama',
                'discount',
                'hargajual'
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>


<?php
if (!$model->isNewRecord){
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPembayaran',
    'options'=>array(
        'title'=>'Pembayaran',
        'autoOpen'=>(!empty($antrian)) ? false : true,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

echo '<iframe name="framePembayaran" width="100%" height="550" src="'.$urlDialog.'"></iframe>';

$this->endWidget();
}
?>

<?php 
$url = Yii::app()->createUrl('actionAjax/getStockBarangRetail');
$urlAjax = Yii::app()->createUrl('actionAjax/getIdBarangStock');
$notif = Yii::t('mds','Do You want to cancel?');
Yii::app()->clientScript->registerScript('submitStokPOS','
    function setAjax(obj){
        objName = $(obj).attr("id");
        objValue = $(obj).val();        
        objValue = jQuery.trim(objValue);

        $.post("'.$urlAjax.'",{objName:objName, objValue:objValue}, function(data){
            if (data != 1){
                $("#'.CHtml::activeId($modObat, 'brg_id').'").val(data.brg_id);
                submitStok();
            }
            else{
                alert("Barang tidak ditemukan");
                clear();
            }
        }, "json");
    }
    function submitStok(){'.(($model->isNewRecord) ? '
        idBarang = $("#'.Chtml::activeId($modObat,'brg_id').'").val();
        qty = 1;
        if (qty > 0){
            $.post("'.$url.'",{idBarang:idBarang,qty:qty},function(data){
                if (cekId(idBarang,data.barang.qty,data.barang.minimal, data.stok)){
//                    if (data.stok != 0){
                        setObatTerakhir(data.barang.nama,data.barang.harga,data.barang.qty, data.stok);
                        $("#tblStockBarang tbody").append(data["tr"]);
                        $("#tblStockBarang tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                        $("#tblStockBarang tbody tr:last .numbersOnly2").maskMoney({"thousands":"", "defaultZero":true,"allowZero":true, "allowDecimal":true,"precision":0,"symbol":null, "decimal":"."});
                        $("#tblStockBarang tbody tr:last .qty").focus();
                        $("#tblStockBarang tbody tr:last .qty").select();
                        setUrutan();
                        getTotal();
//                    }
                    if (data.stok == 0){
                        alert("Stok Habis");
                    }
                    $("#tblStockBarang tbody tr:last .qty").focus();
                    $("#tblStockBarang tbody tr:last .qty").select();
                }
                clear();
            },"json");
        }
        else{
            clear();
            alert("Quantity minimal 1");
        }
    ' : 'clear();').'}
    
    function clear(){
        $(".grouping").find("input").each(function(){
            $(this).val("");
        });
    }
    
    function setUrutan(){
        no = 1;
        $(".no").each(function(){
            $(this).val(no);
            no++;
        });
    }
    
    function batal(obj){
        if (confirm("'.$notif.'")){
            $(obj).parents("tr").remove();
            setUrutan();
            getTotal();
        }
        else{
            return false;
        }
    }
    
    function cekId(obj,qty,minimal,stokdata){
        barang = $(".barang[value="+obj+"]").parents("tr").find(".qty");
        stok = parseFloat($(".barang[value="+obj+"]").parents("tr").find(".stok").val());
        harga = parseFloat($(".barang[value="+obj+"]").parents("tr").find(".harga").val());
        value = barang.val();
        hasil = parseFloat(qty)+parseFloat(value);
        if (barang.length == 1){
            sisa = stok-hasil;
//            if (hasil > stok){
//                alert("Stok Tidak Ada");
//            }else{
                barang.val(hasil);
                $(".barang[value="+obj+"]").parents("tr").find(".sub").val(hasil*harga);
                $(".barang[value="+obj+"]").parents("tr").find(".text_sub").html(hasil*harga);
                $("#tblStockBarang tbody tr:last .qty").focus();
                getTotal();
                if (sisa < minimal){
                    alert("Stok sudah kurang dari stok minimal");
                }
//            }
            return false;
        }
        else{
            if (stokdata > 0){
                sisa = stokdata-qty;
                if (sisa < minimal){
                    alert("Stok sudah kurang dari stok minimal");
                }
            }
            return true;
        }
    }
    
    function getSubTotal(obj){
        parent = $(obj).parents("tr");
        harga = parseFloat(parent.find(".harga").val());
        netto = parseFloat(parent.find(".netto").val());
        qty = parseFloat(parent.find(".qty").val());
        stok = $(obj).parents("tr").find(".stok").val();
        pajak = parseFloat($(obj).parents("tr").find(".ppn").val());
        if (jQuery.isNumeric(qty)){
//            if (qty > stok){
//                parent.find(".qty").val(stok);
//                hasil = stok*harga;
//                totnetto = stok*netto;
//                parent.find(".sub").val(hasil);
//                parent.find(".text_sub").html(hasil);
//                parent.find(".subnetto").val(totnetto);
//                parent.find(".administrasi").val(pajak*hasil/100);
//                getDiscount(obj);
//                alert("Stok Tidak Ada");
//            }
//            else{
                hasil = qty*harga;
                totnetto = qty*netto;
                parent.find(".sub").val(hasil);
                parent.find(".text_sub").html(hasil);
                parent.find(".subnetto").val(totnetto);
                parent.find(".administrasi").val(pajak*hasil/100);
                getDiscount(obj);
//            }
        }
        
    }
    
    function getTotal(obj){
        total = 0;
        totalnetto = 0;
        disc = 0;
        pajak = 0;
        $(".sub").each(function(){
            total += parseFloat($(this).val());
            disc += parseFloat($(this).parents("tr").find(".disc_rp").val());
//            pajak += parseFloat($(this).parents("tr").find(".administrasi").val());
            totalnetto += parseFloat($(this).parents("tr").find(".subnetto").val());
        });
        subtotal = total;
        if (typeof obj != "undefined"){
            qty = parseFloat($(obj).parents("tr").find(".qty").val());
            sub = parseFloat($(obj).parents("tr").find(".sub").val());
            discount = $(obj).val();
            disc_persen = discount/sub*100;
            if (jQuery.isNumeric(discount)){
                if (discount >= sub){
                    disc_persen = 100;
                    disc = disc-discount+sub;
                }else{
                    sub = discount;
                }
                total = total-disc;
                disc_persen = Math.round(disc_persen*100)/100;
                $(obj).parents("tr").find(".disc").val(disc_persen);
                $(obj).parents("tr").find(".disc_rp").val(sub);
            }
        }else{
            total = ((total-disc) > 0) ? total-disc : 0 ;
        }
        
        $(".subtotal").val(subtotal);
        subtotal = formatUang(subtotal);
        $(".text_subtotal").html(subtotal);
        $("#subtotal").val(total);
//        total = total+pajak;
        total = formatUang(total);
        
        $(".text_total").html(total);
        $("#'.CHtml::activeId($model,'totharganetto').'").val(totalnetto);
//        if (jQuery.isNumeric(pajak)){
//        $("#'.CHtml::activeId($model,'biayaadministrasi').'").val(pajak);
//            pajak = formatUang(pajak);
//            $(".text_pajak").html(pajak);
//        }
        if (jQuery.isNumeric(disc)){
            $("#'.CHtml::activeId($model,'discount').'").val(disc);
            disc = formatUang(disc);
            $(".text_discount").html(disc);
        }
        
        $(".totalharga").html(total);
    }
    
    function getDiscount(obj){
        value = $(obj).parents("tr").find(".disc").val();
        parent = $(obj).parents("tr");
        harga = parseFloat(parent.find(".harga").val());
        qty = parseFloat(parent.find(".qty").val());
        disc = harga*qty/100*value;
        if(value > 100){
            disc = harga*qty;
            $(obj).val(100);
        }
        parent.find(".disc_rp").val(Math.round(disc));
        getTotal();
    }
    
    function setObatTerakhir(nama, harga, quantity, stok){
        harga = formatUang(harga);
        $(".nama_obat").html("<center>"+nama+"</center>");
        $(".harga_obat").html("<center>"+harga+"</center>");
        $(".qty_obat").html("<center>"+quantity+"/"+stok+"</center>");
    }
',  CClientScript::POS_HEAD); ?>