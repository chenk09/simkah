<?php
$this->breadcrumbs=array(
	'Pembayaran',
);?>
<iframe id="framePrint" src="<?=$url; ?><?php echo Yii::app()->user->getFlash('url');?>" style="height:0px;">
</iframe>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        //'action'=>$this->createUrl('/billingKasir/pembayaran/prosesBayar'),
        'focus'=>'#'.CHtml::activeId($modTandaBukti, 'uangditerima'),
        'htmlOptions'=>array('onKeyPress'=>'cekInputTindakan(); return disableKeyPress(event);',
                             'onsubmit'=>'return cekInputTindakan();'),
));?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo $form->hiddenField($modPasien,'pasien_id',array('readonly'=>true)); ?>

<fieldset>
    <legend class="rim">Pembayaran Stock</legend>
    <table id="tblBayarOA" class="table table-condensed">
        <thead>
            <tr>
                <th>No</th>
                <th>Stock Code</th>
                <th>Stock Nama</th>
                <th>Harga Jual</th>
                <th>Qty</th>
                <th>Disc (Rp)</th>
                <th>Sub Total</th>
            </tr>
        </thead>
        <tbody>
        <?php 
        $totalPajak = 0;
        foreach($modObatalkes as $i=>$obatAlkes) { 
            $pos = ProdukposV::model()->findByAttributes(array('brg_id'=>$obatAlkes->obatalkes_id));
            ?>
            <tr>
                <td><?php echo ($i+1); ?></td>
                <td><?php echo $pos->stock_code; ?></td>
                <td>
                    <?php echo $obatAlkes->obatalkes->obatalkes_nama ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][obatalkespasien_id]" ,$obatAlkes->obatalkespasien_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][obatalkes_id]" ,$obatAlkes->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][carabayar_id]",$obatAlkes->carabayar_id, array('readonly'=>true)); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][penjamin_id]",$obatAlkes->penjamin_id, array('readonly'=>true)); ?>
                </td>
                <td>
                    <?php $totHargaJualOa = $totHargaJualOa + ($obatAlkes->hargajual_oa/$obatAlkes->qty_oa); ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][hargajual_oa]", $obatAlkes->hargajual_oa/$obatAlkes->qty_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][qty_oa]", $obatAlkes->qty_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                </td>
                <td>
                    <?php $totDisc = $totDisc + $obatAlkes->discount; ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][discount]", $obatAlkes->discount, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php $totSub = $totSub+$obatAlkes->hargajual_oa; ?>
                    <?php $totalPajak += $obatAlkes->biayaadministrasi;?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][sub_total]", $obatAlkes->hargajual_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        <?php } ?>
            <tr class="trfooter">
                <td colspan="3" style="text-align:right">Total</td>
                <td>
                    <?php echo CHtml::textField("totalhargajual_oa", $totHargaJualOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalqty_oa", $totQtyOa, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaldiscount_oa", $totDisc, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbayar_oa", $totSub, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<?php
$pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanretail;
$totTagihan = $totSub-$totDisc+$totalPajak;
$totDeposit = 0;
$totTagihan =  round($totTagihan);
if(empty($modTandaBukti->tandabuktibayar_id)){
    $modTandaBukti->jmlpembayaran = $totTagihan;
    $modTandaBukti->biayaadministrasi = $totalPajak;
    $modTandaBukti->biayamaterai = 0;
    $modTandaBukti->uangkembalian = 0;
//echo $modTandaBukti->jmlpembayaran;
    $pembulatan = $modTandaBukti->jmlpembayaran % $pembulatanHarga;
    if($pembulatan>0){
        $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
        $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan;
        $harusDibayar = $modTandaBukti->jmlpembayaran;
    } else {
        $modTandaBukti->jmlpembulatan = 0;
    }
    $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
}
?>

<fieldset>
    <legend class="rim">Data Pembayaran</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Sub Total','subTotal', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('subTotal',$totSub,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Discount','discount', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('discount',$totDisc,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>

                <div class="control-group ">
                    <label class='control-label'>Pajak</label>
                    <div class="controls">
                        <?php echo $form->textField($modTandaBukti,'biayaadministrasi',array('class'=>'inputFormTabel currency span3', 'readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Tagihan','harusDibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('totTagihan',$totTagihan,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modTandaBukti,'biayaadministrasi',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'jmlpembulatan',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                
                <?php echo $form->hiddenField($modTandaBukti,'biayamaterai',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'jmlpembayaran',array('onkeyup'=>'hitungKembalian();','readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'uangditerima',array('onkeyup'=>'hitungKembalian();','class'=>'inputFormTabel currency span3', 'onblur'=>'hitungKembalian(this)', 'onkeypress'=>"if (event.keyCode == 13){ $('button#submitButton').focus();}",  'onfocus'=>'$(this).select()')); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'uangkembalian',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php //$modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modTandaBukti,'tglbuktibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modTandaBukti,
                                                'attribute'=>'tglbuktibayar',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modTandaBukti,'tglbuktibayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                <?php echo $form->dropDownListRow($modTandaBukti,'carapembayaran',  CaraPembayaran::items('TUNAI'),array('onchange'=>'ubahCaraPembayaran(this)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <div class="control-group ">
                    <?php echo CHtml::label('Menggunakan Kartu','pakeKartu', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::checkBox('pakeKartu', ((!empty($modTandaBukti->dengankartu)) ? true : false),array('onchange'=>"enableInputKartu(this);", 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div id="divDenganKartu">
                    <?php echo $form->dropDownListRow($modTandaBukti,'dengankartu',  DenganKartu::items(),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50, 'disabled'=>'disabled')); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'bankkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nokartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nostrukkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>

                <?php //echo $form->textFieldRow($modTandaBukti,'darinama_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php //echo $form->textAreaRow($modTandaBukti,'alamat_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($modTandaBukti,'sebagaipembayaran_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
        </tr>
    </table>
</fieldset>

    <?php //echo $form->textFieldRow($modTandaBukti,'closingkasir_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'shift_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'bayaruangmuka_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'pembayaranpelayanan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nourutkasir',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nobuktibayar',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'keterangan_pembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
    <div class="form-actions">
            <?php 
            if($sudahBayar){
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary disabled', 'disabled'=>true, 'type'=>'submit', 'onclick'=>'return false;', 'onKeypress'=>'return formSubmit(this,event)', 'id'=>"submitButton"));  
                echo "&nbsp;&nbsp;";
                echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                                    array('class'=>'btn btn-info','onclick'=>"printBayar($modTandaBukti->tandabuktibayar_id);return false",'disabled'=>false)); 
            } else {
                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'id'=>"submitButton"));
                echo "&nbsp;&nbsp;";
                echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                                    array('class'=>'btn btn-info disabled','onclick'=>"return false",'disabled'=>true)); 
            }
            ?>
            <?php //echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>								
<?php  
$content = $this->renderPartial('../tips/tips',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<div id="testForm"></div>

<script type="text/javascript">
$('#pembayaran-form').submit(function(){
    window.parent.$('#dialogPembayaran').dialog('close');
    printBayar(<?php echo $modTandaBukti->tandabuktibayar_id; ?>);
});
//window.parent.$('#dialogPembayaran').dialog('close').fade(3000);
$('.currency').each(function(){this.value = formatUang(this.value)});
function hitungKembalian(obj)
{
    var bayar = $('#TandabuktibayarT_jmlpembayaran').val();
    var jmlBayar = unformatNumber(bayar);
    var terima = $('#TandabuktibayarT_uangditerima').val();
    var uangDiterima = unformatNumber(terima);
    var uangKembalian;
    uangKembalian = uangDiterima - jmlBayar;
    
    if (uangKembalian >= 0){
        $('#TandabuktibayarT_uangditerima').val(formatUang(uangDiterima));
        $('#TandabuktibayarT_uangkembalian').val(formatUang(uangKembalian));
    }else{
        if(typeof obj != "undefined"){
            if(confirm('Uang diterima tidak boleh lebih kecil dari Jumlah Pembayaran')){
                uangDiterima = jmlBayar;
            } else {
                $('#TandabuktibayarT_uangditerima').addClass('error');
                $('#TandabuktibayarT_uangditerima').focus();
                $('#TandabuktibayarT_uangditerima').select();
            }
        }
        $('#TandabuktibayarT_uangditerima').val(formatUang(uangDiterima));
        $('#TandabuktibayarT_uangkembalian').val(0);
    }
}
<?php if(!$sudahBayar){ ?>
function enableInputKartu(obj)
{
    if($(obj).is(':checked')){
        //alert('isi');
        $('#TandabuktibayarT_dengankartu').removeAttr('disabled');
        $('#TandabuktibayarT_bankkartu').removeAttr('readonly');
        $('#TandabuktibayarT_nokartu').removeAttr('readonly');
        $('#TandabuktibayarT_nostrukkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#TandabuktibayarT_dengankartu').attr('disabled','disabled');
        $('#TandabuktibayarT_bankkartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nokartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nostrukkartu').attr('readonly','readonly');
        
        $('#TandabuktibayarT_dengankartu').val('');
        $('#TandabuktibayarT_bankkartu').val('');
        $('#TandabuktibayarT_nokartu').val('');
        $('#TandabuktibayarT_nostrukkartu').val('');
    }
}
<?php } ?>
function hitungJmlBayar()
{
    var biayaAdministrasi = unformatNumber($('#TandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#TandabuktibayarT_biayamaterai').val());
    var deposit = unformatNumber($('#deposit').val());
    var totPembebasan = unformatNumber($('#totPembebasan').val());
    var totDiscountTind = unformatNumber($('#totaldiscount_tindakan').val());
    var totBayar = 0;
    var totTagihan = unformatNumber($('#totTagihan').val());
    var jmlPembulatan = unformatNumber($('#TandabuktibayarT_jmlpembulatan').val());
    
    totBayar = totTagihan + jmlPembulatan + biayaAdministrasi + biayaMaterai - totDiscountTind - totPembebasan - deposit;
    
    $('#TandabuktibayarT_jmlpembayaran').val(formatUang(totBayar));
    hitungKembalian();
}

function ubahCaraPembayaran(obj)
{
    if(obj.value == 'CICILAN'){
        $('#TandabuktibayarT_jmlpembayaran').removeAttr('readonly');
    } else {
        $('#TandabuktibayarT_jmlpembayaran').attr('readonly', true);
        hitungJmlBayar();
    }
    
    if(obj.value == 'TUNAI'){
        hitungJmlBayar();
    } 
}

function printBayar(idTandaBukti)
{
    if(idTandaBukti!=''){ 
        //window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print', array("caraPrint"=>"PRINT", 'idPenjualan'=>$modObatalkes[0]->penjualanresep_id, 'idTandaBukti'=>$modTandaBukti->tandabuktibayar_id)); ?>',"", 'left=100,top=100,width=400,height=400,scrollbars=1');
        $("#framePrint").attr("src",'<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/print', array("caraPrint"=>"PRINT", 'idPenjualan'=>$modObatalkes[0]->penjualanresep_id, 'idTandaBukti'=>$modTandaBukti->tandabuktibayar_id)); ?>');
    }     
}

function cekInputTindakan()
{
       
    if($('#totalbayartindakan').val() <=0 && $('#totalbayar_oa').val()<=0){
        alert('Tidak ada tindakan / Obat Alkes');
        return false;
    } else {
        $('.currency').each(function(){this.value = unformatNumber(this.value)});
        return true;
    }
}
</script>

<?php
//if($successSave){
Yii::app()->clientScript->registerScript('onsubmitsssss',"
$('form#pembayaran-form').submit(function(){
    var bayar = $('#TandabuktibayarT_jmlpembayaran').val();
    var jmlBayar = unformatNumber(bayar);
    var terima = $('#TandabuktibayarT_uangditerima').val();
    var uangDiterima = unformatNumber(terima);
    if (jmlBayar > uangDiterima){
        $('#TandabuktibayarT_uangditerima').addClass('error');
        $('#TandabuktibayarT_uangditerima').focus();
        $('#TandabuktibayarT_uangditerima').select();
        return false;
    }
});
",  CClientScript::POS_READY);
//}
?>