<?php
$this->breadcrumbs=array(
    'Reantrianbayarpos Vs'=>array('index'),
    'Manage',
);

$arrMenu = array();
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>'Informasi Antrian Pembayaran', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' REAntrianbayarposV', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' REAntrianbayarposV', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                
$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('reantrianbayarpos-v-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>



<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'reantrianbayarpos-v-grid',
    'dataProvider'=>$model->searchInformasi(),
//    'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'name'=>'penjualanresep_id',
            'header'=>'No Penjualan',
            'value'=>'$data->penjualanresep_id',
        ),
        'tglpenjualan',
//        'jenispenjualan', 
//        'noresep', 
        //'totalhargajual', 
        array(
            'name'=>'totalhargajual',
            'type'=>'raw',
            'value'=>'MyFunction::formatNumber($data->totalhargajual)',
            'htmlOptions'=>array("style"=>'text-align:right')
        ),
        'nama_pemakai',
        array(
            'header'=>'Bayar',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.$this->id.'/bayar", array("id"=>$data->penjualanresep_id)),
                            array("class"=>"", 
                                  "title"=>"Klik untuk melakukan Pembayaran",))'
        ),
        /*
        'stock_code',
        'stock_name',
        'satuankecil_nama',
        'barang_barcode',
        'obatalkes_id',
        'qty_oa',
        'hargasatuan_oa',
        'hargajual_oa',
        'tglpenjualan',
        'jenispenjualan',
        'totalhargajual',
        'penjualanresep_id',
        ////'obatalkespasien_id',
        array(
                        'name'=>'obatalkespasien_id',
                        'value'=>'$data->obatalkespasien_id',
                        'filter'=>false,
                ),
        'noresep',
        'ruangan_id',
        'ruangan_nama',
        'create_loginpemakai_id',
        'pegawai_id',
        'nama_pegawai',
        'nama_pemakai',
        */
//        array(
//                        'header'=>Yii::t('zii','View'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{view}',
//        ),
//        array(
//                        'header'=>Yii::t('zii','Update'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{update}',
//                        'buttons'=>array(
//                            'update' => array (
//                                          'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)',
//                                        ),
//                         ),
//        ),
//        array(
//                        'header'=>Yii::t('zii','Delete'),
//            'class'=>'bootstrap.widgets.BootButtonColumn',
//                        'template'=>'{remove} {delete}',
//                        'buttons'=>array(
//                                        'remove' => array (
//                                                'label'=>"<i class='icon-remove'></i>",
//                                                'options'=>array('title'=>Yii::t('mds','Remove Temporary')),
//                                                'url'=>'Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/removeTemporary",array("id"=>"$data->obatalkespasien_id"))',
//                                                //'visible'=>'($data->kabupaten_aktif && Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ? TRUE : FALSE',
//                                                'click'=>'function(){return confirm("'.Yii::t("mds","Do You want to remove this item temporary?").'");}',
//                                        ),
//                                        'delete'=> array(
//                                                'visible'=>'Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)',
//                                        ),
//                        )
//        ),
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>

<?php 
//echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<div class="search-form">
<?php 
$this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>

<?php 
 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
        
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#reantrianbayarpos-v-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?> 