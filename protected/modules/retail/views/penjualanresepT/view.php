<?php
$this->breadcrumbs=array(
	'Repenjualanresep Ts'=>array('index'),
	$model->penjualanresep_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' REPenjualanresepT #'.$model->penjualanresep_id, 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                array_push($arrMenu,array('label'=>Yii::t('mds','List').' REPenjualanresepT', 'icon'=>'list', 'url'=>array('index'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' REPenjualanresepT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Update').' REPenjualanresepT', 'icon'=>'pencil','url'=>array('update','id'=>$model->penjualanresep_id))) :  '' ;
                array_push($arrMenu,array('label'=>Yii::t('mds','Delete').' REPenjualanresepT','icon'=>'trash','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->penjualanresep_id),'confirm'=>Yii::t('mds','Are you sure you want to delete this item?')))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' REPenjualanresepT', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'penjualanresep_id',
		'reseptur_id',
		'pasienadmisi_id',
		'penjamin_id',
		'carabayar_id',
		'pendaftaran_id',
		'ruangan_id',
		'pegawai_id',
		'kelaspelayanan_id',
		'pasien_id',
		'tglpenjualan',
		'jenispenjualan',
		'tglresep',
		'noresep',
		'totharganetto',
		'totalhargajual',
		'totaltarifservice',
		'biayaadministrasi',
		'biayakonseling',
		'pembulatanharga',
		'jasadokterresep',
		'instalasiasal_nama',
		'ruanganasal_nama',
		'discount',
		'subsidiasuransi',
		'subsidipemerintah',
		'subsidirs',
		'iurbiaya',
		'lamapelayanan',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		'returresep_id',
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>