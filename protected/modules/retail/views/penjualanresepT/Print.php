<?php 
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
    $template = "{items}";
}
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');   
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
//echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>''));      
?>
<?php echo CHtml::css(
        'body{margin:0;padding:0}
        table.faktur{
            border:1px solid black;
            }
        table.faktur td{
                padding:5px;
            }
        table.faktur td.right{
        text-align:right;}
'
        ); ?>
<?php $data=ProfilrumahsakitM::model()->findByPk(Params::DEFAULT_PROFIL_RUMAH_SAKIT); ?>

<table class='faktur' style="margin:auto;" width="100%">
    <tr>
        <td colspan="5"><center><?php echo $data->nama_rumahsakit ?> #<?=RuanganM::model()->findByPk(Yii::app()->user->getState('ruangan_id'))->ruangan_nama?></center></td>
    </tr>
    <tr>
        <td colspan="5"><center><?php echo $data->alamatlokasi_rumahsakit ?></center></td>
    </tr>
    <tr>
        <td colspan="5"><center>Telp./Fax. <?php echo $data->no_telp_profilrs ?> / <?php echo $data->no_faksimili ?></center></td>
    </tr>
    <tr style="border:1px solid black;border-bottom:none;">
        <td colspan="2">CASHIER</td>
        <td ><?=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;?></td>
        <td class="right">PC:</td>
        <td class="right">#<?=$_SERVER['REMOTE_ADDR'];?></td>
    </tr>
    <tr style="border:1px solid black;border-top:none;">
        <td colspan="2"><center>####</center></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
    <?php 
    if (isset($modObat)){
        $total = 0;
        $totaldisc = 0;
        $jumlah = 0;
        foreach ($modObat as $i=>$v){
            $total +=$v->hargajual_oa;
            $totaldisc +=$v->discount;
            $jumlah+=$v->qty_oa;
            echo '<tr>
                        <td></td>
                        <td></td>';
            
                        
            if ($v->qty_oa > 1){
                echo '  
                        <td colspan="3">'.$v->obatalkes->obatalkes_nama.'</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="right">'.MyFunction::formatNumber($v->qty_oa).'</td>
                        <td>x</td>
                        <td class="right">'.MyFunction::formatNumber($v->hargajual_oa/$v->qty_oa).'</td>
                        <td class="right">'.MyFunction::formatNumber($v->hargajual_oa).'</td>
                    </tr>';
            }
            else{
                echo '
                      <td colspan="2">'.$v->obatalkes->obatalkes_nama.'</td>
                      <td class="right">'.MyFunction::formatNumber($v->hargajual_oa).'</td>
                    </tr>';
            }
            if ((!empty($v->discount))&&($v->discount>0)){
                echo '<tr>
                            <td><center>Diskon</center></td>
                            <td class="right">'.MyFunction::formatNumber($v->qty_oa).'</td>
                            <td>x</td>
                            <td class="right">'.MyFunction::formatNumber($v->discount/$v->qty_oa).'</td>
                            <td class="right">'.MyFunction::formatNumber($v->discount).'</td>
                        </tr>';
            }
        }
    }
    ?>
<!--    <tr style="border-top:1px solid black;">
        <td colspan="2">SUBTOTAL</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($total);?></td>
    </tr>
    <tr>
        <td colspan="2">Total Discount</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($totaldisc);?></td>
    </tr>
    <tr style="border-top:1px solid black;">
        <td colspan="2">TOTAL</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($total-$totaldisc);?></td>
    </tr>
    <tr>
        <td colspan="2">Pembulatan</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($modTanda->jmlpembulatan);?></td>
    </tr>-->
    <tr style="border-top:1px solid black;">
        <td colspan="2">TOTAL</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($total-$totaldisc+$modTanda->jmlpembulatan);?></td>
    </tr>
    <tr>
        <td colspan="2">Cash</td>        
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($modTanda->uangditerima);?></td>
    </tr>
    <tr>
        <td colspan="2">Kembali</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($modTanda->uangkembalian);?></td>
    </tr>
    <tr>
        <td colspan="2">RCV</td>
        <td>:</td>
        <td></td>
        <td class="right"><?php echo MyFunction::formatNumber($jumlah);?> Items</td>
    </tr>
    <tr>
        <td colspan="5"><center>Terimakasih atas kunjungan anda.</center></td>
    </tr>
    <tr>
        <td colspan="5"><center>Barang yang sudah dibeli tidak dapat dikembalikan.</center></td>
    </tr>
    
</table>

