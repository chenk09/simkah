<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
    'id'=>'reantrianbayarpos-v-search',
        'type'=>'horizontal',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pembayaran', 'tglPembayaran', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan', 'sampaiDengan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </td>
        <td>
            <?php //echo $form->textFieldRow($model, 'jenispenjualan', array('class' => 'span3')); ?>
            <?php //echo $form->textFieldRow($model, 'penjualanresep_id', array('class' => 'span3')); ?>
            <div class="control-group ">
                <?php echo CHtml::label('No Penjualan','', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'penjualanresep_id', array('class' => 'span3')); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model, 'nama_pemakai', array('class' => 'span3')); ?>
            <?php
//            echo $form->dropDownListRow($model, 'ruanganpemesan_id', CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
//                'empty' => '-- Pilih --',));
            ?>
        </td>
    </tr>
</table>

    <?php //echo $form->textFieldRow($model,'brg_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'category_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'category_name',array('class'=>'span5','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'subcategory_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'subcategory_code',array('class'=>'span5','maxlength'=>10)); ?>

    <?php //echo $form->textFieldRow($model,'subcategory_name',array('class'=>'span5','maxlength'=>100)); ?>

    <?php //echo $form->textFieldRow($model,'stock_code',array('class'=>'span5','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'stock_name',array('class'=>'span5','maxlength'=>200)); ?>

    <?php //echo $form->textFieldRow($model,'satuankecil_nama',array('class'=>'span5','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'barang_barcode',array('class'=>'span5','maxlength'=>200)); ?>

    <?php //echo $form->textFieldRow($model,'obatalkes_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'qty_oa',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'hargasatuan_oa',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'hargajual_oa',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'tglpenjualan',array('class'=>'span3')); ?>

    <?php //echo $form->textFieldRow($model,'jenispenjualan',array('class'=>'span3','maxlength'=>100)); ?>

    <?php //echo $form->textFieldRow($model,'totalhargajual',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'penjualanresep_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'obatalkespasien_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'noresep',array('class'=>'span3','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'ruangan_nama',array('class'=>'span5','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'create_loginpemakai_id',array('class'=>'span3')); ?>

    <?php //echo $form->textFieldRow($model,'pegawai_id',array('class'=>'span5')); ?>

    <?php //echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span5','maxlength'=>50)); ?>

    <?php //echo $form->textFieldRow($model,'nama_pemakai',array('class'=>'span5','maxlength'=>20)); ?>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php 
             echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->module->id.'/'.penjualanresepT.'/informasi'), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
        ?>
        <?php  
            $content = $this->renderPartial('../tips/informasi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
    </div>

<?php $this->endWidget(); ?>