<?php
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
$data = $model->searchLaporan();
if (isset($caraPrint)){
  $data = $model->searchLaporanPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
}
?>
<?php $this->widget($table,array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
        'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    'activedate',
                    'stock_code',
                    'stock_name',
                    array(
                        'name'=>'ppn_persen',
                        'type'=>'raw',
                        'value'=>'$data->ppn_persen."%"',
                    ),
                    'barang_barcode',
                    array(
                            'header'=>'Moving Average',
                            'name'=>'hargajual',
                            'value'=>'$data->movingavarage'

                    ),
                    array(
                        'header'=>'Harga Beli Bruto',
                        'value'=>'$data->hargabelibruto',
                    ),
                    array(
                        'header'=>'Stok',
                        'value'=>'$data->stok',
                    ),
                    array(
                        'header'=>'Harga Jual + PPN',
                        'value'=>'$data->hargajualplusppn',
                    ),
                    array(
                        'header'=>'Harga Jual - PPN',
                        'value'=>'$data->hargajualminppn',
                    ),
                    array(
                        'header'=>'Margin (RP)',
                        'value'=>'$data->margin',
                    ),
                    array(
                        'header'=>'Margin (%)',
                        'type'=>'raw',
                        'value'=>'(empty($data->stock_code)? 0 : number_format(($data->margin / $data->hargajualminppn),1)."%")',
                    ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>