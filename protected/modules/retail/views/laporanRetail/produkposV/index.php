<legend class="rim2"> Laporan Produk</legend>
<legend class="rim">Pencarian</legend>
<?php
    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/FrameGrafikProduk&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#laporan-search').submit(function(){
            $.fn.yiiGridView.update('laporan-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'laporan-search',
)); ?>
    <table>
        <tr>
            <td>
                <?php echo $form->textFieldRow($model,'stock_code',array('class'=>'span3',)); ?>
                <?php echo $form->textFieldRow($model,'barang_barcode',array('class'=>'span3')); ?>
                <?php echo $form->textFieldRow($model,'stock_name',array('class'=>'span3')); ?>
                <?php echo $form->textFieldRow($model,'price_name',array('class'=>'span3')); ?>
            </td>
            <td>
                <?php echo $form->dropDownListRow($model, 'category_id', CHtml::listData($model->getCategoryItems(),'category_id','category_name'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
                <?php echo $form->dropDownListRow($model, 'subcategory_id', CHtml::listData($model->getSubcategoryItems(),'subcategory_id','subcategory_name'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                
            </td>
        </tr>
    </table>
    <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                            Yii::app()->createUrl($this->module->id.'/'.informasiRetail.'/Informasireceive'), 
                            array('class'=>'btn btn-danger',
                                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Stock</legend>
<?php $this->renderPartial('produkposV/_table',array('model'=>$model)); ?>
<?php $this->renderPartial('_tab'); ?>
<iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
</iframe>  
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintProduk');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>