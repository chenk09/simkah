<?php
$table = 'ext.bootstrap.widgets.BootGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->search();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                        'header'=>'Stock Code',
                        'value'=>'$data->stock_code',
                    ),
                    array(
                        'header'=>'Stock Name',
                        'value'=>'$data->stock_name',
                    ),
                    array(
                        'header'=>'QTY',
                        'value'=>'$data->qtystok_in',
                    ),
                    array(
                        'header'=>'Price',
                        'value'=>'MyFunction::formatNumber($data->hargajual)',
                    ),
                    array(
                        'header'=>'Value',
                        'value'=>'MyFunction::formatNumber($data->hargajual * $data->qtystok_in)',
                    ),
	),
)); ?>