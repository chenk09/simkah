<legend class="rim2">Sales By Product</legend>
<legend class="rim">Pencarian</legend>
<?php
    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/FrameSalesbyProduct&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#laporan-search').submit(function(){
            $.fn.yiiGridView.update('laporan-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'laporan-search',
)); ?>
    <table>
        <tr>
            <td>
                    <div class="control-group">
                       <?php echo $form->label($model,'tglpenjualanAwal',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglpenjualanAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
            <td>
                    <div class="control-group">
                       <?php echo $form->label($model,'tglpenjualanAkhir',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglpenjualanAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <div class="control-group">
                    <?php echo $form->label($model,'stock_name',array('class'=>'control-label')); ?>
                    <div class="controls">
                <?php// echo $form->dropDownListRow($model, 'stock_code', CHtml::listData($model->getStockItems(),'stock_code','stock_name'),array('empty'=>'-- Pilih --')); ?>
                <?php
                    $this->widget('MyJuiAutoComplete', array(
                        'model' => $model,
                        'attribute' => 'stock_name',
                        'value' => '',
                        'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/ProductItems'),
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ) {
                                $(this).val(ui.item.label);
                                return true;
                            }',
                            'select' => 'js:function( event, ui ) {
                                $(this).val(ui.item.label);
                                 return true;
                              }',
                        ),
                        'htmlOptions'=>array(
                            'onkeypress'=>'return $(this).focusNextInputField(event)',
                            'disabled'=>($model->isNewRecord)?'':'disabled', 
                        ),
                        'tombolDialog'=>array('idDialog'=>'dialogStockItems'),

                    ));
                ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                            Yii::app()->createUrl($this->module->id.'/'.informasiRetail.'/Informasireceive'), 
                            array('class'=>'btn btn-danger',
                                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Sales Product</legend>
<?php $this->renderPartial('salesBy/_tableProduct',array('model'=>$model)); ?>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintSalesbyProduct');
?>
<?php $this->renderPartial('_tab'); ?>
<iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
</iframe>
<?php $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); ?>
<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogStockItems',
    'options' => array(
        'title' => 'Stock Detail',
        'autoOpen' => false,
        'modal' => true,
        'width' => 1000,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<?php 
$modStock = new REProdukBarangM; 
$modStock->unsetAttributes();
if (isset($_GET['REProdukBarangM'])){
    $modStock->attributes = $_GET['REProdukBarangM'];
}
?>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'rkdokumenpasienrmlama-v-grid',
    'dataProvider'=>$modStock->search(),
    'filter'=>$modStock,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",
                            array(
                                    "class"=>"btn-small",
                                    "id" => "selectStock",
                                    "onClick" => "$(\"#RELaporanpenjualanprodukposV_stock_name\").val(\"$data->obatalkes_nama\");
                                                          \$(\"#dialogStockItems\").dialog(\"close\");"
                             )
             )',
        ),
        array(
            'name'=>'obatalkes_kode',
            'header'=>'Stock code',
            'value'=>'$data->obatalkes_kode',
        ),
        array(
            'name'=>'obatalkes_nama',
            'header'=>'Stock Name',
            'value'=>'$data->obatalkes_nama',
        ),
        array(
            'name'=>'jenisobatalkes_id',
            'header'=>'kategori',
            'value'=>'$data->jenisobatalkes->jenisobatalkes_nama',
        ),
    ),
)); ?>

<?php $this->endWidget(); ?>