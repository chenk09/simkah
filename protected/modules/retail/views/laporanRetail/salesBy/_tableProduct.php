<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchProductprint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchProduct();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
//     'mergeColumns'=>array('stock_code'),
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                        'type'=>'raw',
                    ),
                    'stock_code',
                    array(
                        'header'=>'Stock Name',
                        'value'=>'$data->stock_name',
                        'footer'=>'<b>Jumlah:</b>',
                        'footerHtmlOptions'=>array('colspan'=>3, 'style'=>'text-align:right;'),
                        'type'=>'raw',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
                    array(
                        'name'=>'qty_oa',
                        'type'=>'raw',
                        'header'=>'Qty',
                        'value'=>'MyFunction::formatnumber($data->qty_oa)',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(qty_oa)',
                    ),
                    array(
                        'name'=>'value',
                        'type'=>'raw',
                        'header'=>'Value',
                        'value'=>'MyFunction::formatnumber($data->hargajual_oa * $data->qty_oa)',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(value)',
                    ),
//                    array(
//                        'header'=>'Vat',
//                        'value'=>'number_format((($data->ppn_persen + 1) * ($data->hargajual_oa * $data->qty_oa) / 10),0,"",",")',
//                        'type'=>'raw',
//                        'footer'=>number_format($model->getTotalhargasatuan()),
//                    ),
                    array(
                        'name'=>'vat',
                        'type'=>'raw',
                        'header'=>'Vat',
                        'value'=>'MyFunction::formatnumber((($data->ppn_persen + 1) * ($data->hargajual_oa * $data->qty_oa) / 10))',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(vat)',
                    ),
//                    array(
//                        'header'=>'Net Value',
//                        'value'=>'number_format((($data->hargajual_oa * $data->qty_oa) - (($data->ppn_persen / 100) * ($data->hargajual_oa * $data->qty_oa))),0,"",",")',
//                        'type'=>'raw',
//                        'footer'=>number_format($model->getTotalhargajual()),
//                    ),
                    array(
                        'name'=>'totalnetvalue',
                        'type'=>'raw',
                        'header'=>'Net Value',
                        'value'=>'MyFunction::formatnumber(($data->hargajual_oa * $data->qty_oa) - (($data->ppn_persen / 100) * ($data->hargajual_oa * $data->qty_oa )))',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(totalnetvalue)',
                    ),
//                    array(
//                        'header'=>'COGS',
//                        'value'=>'number_format($data->harganetto * $data->qty_oa,0,"",",")',
//                        'type'=>'raw',
//                        'footer'=>number_format($model->getTotalharganetto()),
//                    ),
                     array(
                        'name'=>'cogs',
                        'type'=>'raw',
                        'header'=>'COGS',
                        'value'=>'MyFunction::formatnumber($data->harganetto * $data->qty_oa)',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(cogs)',
                    ),
//                     array(
//                        'header'=>'PPN %',
//                        'value'=>'number_format($data->ppn_persen,0,"",",")',
//                        'type'=>'raw',
//                    ),
//                    array(
//                        'header'=>'GP %',
//                        'value'=>'number_format($data->gp_persen,0,"",",")',
//                        'type'=>'raw',
//                        'footer'=>number_format($model->getTotalgp()),
//                    ),
             array(
                        'name'=>'gp_persen',
                        'type'=>'raw',
                        'header'=>'GP %',
                        'value'=>'MyFunction::formatnumber($data->gp_persen)."%"',
                        'htmlOptions'=>array('style'=>'text-align:right;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                        'footer'=>'sum(gp_persen)',
                    ),
	),
)); ?>