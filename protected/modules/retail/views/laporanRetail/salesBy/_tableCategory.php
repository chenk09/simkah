<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchCategoryprint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchCategory();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                        'header'=>'Category Name',
                        'value'=>'$data->category_name',
                        'footer'=>'<b>Total:</b>',
                        'footerHtmlOptions'=>array('colspan'=>2,'style'=>'text-align:right;'),
                        'type'=>'raw',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
//                    array(
//                        'header'=>'QTY',
//                        'value'=>'$data->qty_category',
//                    ),
                    array(
                        'name'=>'qty_category',
                        'type'=>'raw',
                        'header'=>'QTY',
                        'value'=>'MyFunction::formatnumber($data->qty_category)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(qty_category)',
                    ),
//                    array(
//                        'header'=>'Value',
//                        'value'=>'number_format($data->hargajual_category,0,"",",")',
//                    ),
                    array(
                        'name'=>'hargajual_category',
                        'type'=>'raw',
                        'header'=>'Value',
                        'value'=>'MyFunction::formatnumber($data->hargajual_category)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(hargajual_category)',
                    ),
//                    array(
//                        'header'=>'Vat',
//                        'type'=>'raw',
//                        'value'=>'(($data->hargasatuan_category=="")? "0" :$data->hargasatuan_category)',
//                    ),
                     array(
                        'name'=>'hargasatuan_category',
                        'type'=>'raw',
                        'header'=>'Vat',
                        'value'=>'MyFunction::formatnumber($data->hargasatuan_category)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(hargasatuan_category)',
                    ),
//                    array(
//                        'header'=>'Net Value',
//                        'value'=>'number_format($data->totalharga_category,0,"",",")',
//                    ),
                    array(
                        'name'=>'totalharga_category',
                        'type'=>'raw',
                        'header'=>'Net Value',
                        'value'=>'MyFunction::formatnumber($data->totalharga_category)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(totalharga_category)',
                    ),
	),
)); ?>