<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchSubcategoryprint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchSubcategory();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                    ),
                    array(
                        'header'=>'Sub Category Name',
                        'value'=>'$data->subcategory_name',
                        'footer'=>'<b>Total:</b>',
                        'footerHtmlOptions'=>array('colspan'=>2,'style'=>'text-align:right;'),
                        'type'=>'raw',
                         'htmlOptions'=>array(
                            'style'=>'text-align:right;padding-right:10%;'
                        ),
                    ),
//                    array(
//                        'header'=>'QTY',
//                        'value'=>'$data->qty_subcategory',
//                    ),
                    array(
                        'name'=>'qty_subcategory',
                        'type'=>'raw',
                        'header'=>'QTY',
                        'value'=>'MyFunction::formatnumber($data->qty_subcategory)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(qty_subcategory)',
                    ),
//                    array(
//                        'header'=>'Value',
//                        'value'=>'number_format($data->hargajual_subcategory,0,"",",")',
//                    ),
                    array(
                        'name'=>'hargajual_subcategory',
                        'type'=>'raw',
                        'header'=>'Value',
                        'value'=>'MyFunction::formatnumber($data->hargajual_subcategory)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(hargajual_subcategory)',
                    ),
//                    array(
//                        'header'=>'Vat',
//                        'value'=>'(($data->hargasatuan_subcategory=="")? "0" :$data->hargasatuan_subcategory)',
//                    ),
                    array(
                        'name'=>'hargasatuan_subcategory',
                        'type'=>'raw',
                        'header'=>'Vat',
                        'value'=>'MyFunction::formatnumber($data->hargasatuan_subcategory)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(hargasatuan_subcategory)',
                    ),
//                    array(
//                        'header'=>'Net Value',
//                        'value'=>'number_format($data->totalharga_subcategory,0,"",",")',
//                    ),
                    array(
                        'name'=>'totalharga_subcategory',
                        'type'=>'raw',
                        'header'=>'Net Value',
                        'value'=>'MyFunction::formatnumber($data->totalharga_subcategory)',
                        'htmlOptions'=>array('style'=>'text-align:center;'),
                        'footerHtmlOptions'=>array('style'=>'text-align:center;'),
                        'footer'=>'sum(totalharga_subcategory)',
                    ),
	),
)); ?>