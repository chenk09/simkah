<?php
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$template = "{pager}{summary}\n{items}";
if (isset($caraPrint)){
  $data = $model->searchSalesReportPrint();
  $template = "{items}";
  if ($caraPrint=='EXCEL') {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
} else{
  $data = $model->searchSalesReport();
}
?>
<?php $this->widget($table, array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
                'template'=>$template,
                'mergeHeaders'=>array(
                    array(
                        'name'=>'<center>Total Sales Detail</center>',
                        'start'=>'2',
                        'end'=>'3',
                    ),
                ),
	'columns'=>array(
                    array(
                        'header'=>'Tgl closing - sampai dengan',
                        'type'=>'raw',
                        'value'=>'$data->closingdarisampaidengan',
                    ),
                    array(
                        'header'=>'Shift',
                        'value'=>'$data->shift->shift_nama',
                    ),
                    array(
                        'name'=>'total',
                        'header'=>'Total transaksi',
                        'value'=>'number_format($data->tottransaksi,0,"",",")'
                    ),
                    array(
                        'header'=>'Value',
                        'value'=>'number_format($data->nilaiclosingtrans,0,"",",")',
                    ),
	),
)); ?>