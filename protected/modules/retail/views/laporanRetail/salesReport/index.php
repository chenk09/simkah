<legend class="rim2">Sales Report</legend>
<legend class="rim">Pencarian</legend>
<?php
    $url = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/FrameSalesReport&id=1');
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#laporan-search').submit(function(){
            $.fn.yiiGridView.update('laporan-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
                'type'=>'horizontal',
                'id'=>'laporan-search',
)); ?>
    <table>
        <tr>
            <td>
                    <div class="control-group">
                       <?php echo $form->label($model,'tglAwal',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAwal',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
            <td>
                    <div class="control-group">
                       <?php echo $form->label($model,'tglAkhir',array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tglAkhir',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                    )); 
                            ?>
                        </div>
                    </div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <?php echo $form->checkBoxListRow($model, 'shift_id', CHtml::listData($model->getShiftItems(),'shift_id','shift_nama'),array('style'=>'float:none;margin-left:10px;')); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit')); ?>

                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                            Yii::app()->createUrl($this->module->id.'/'.informasiRetail.'/Informasireceive'), 
                            array('class'=>'btn btn-danger',
                                  'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>
<?php
$this->endWidget();
?>

<legend class="rim">Tabel Sales Report</legend>
<?php $this->renderPartial('salesReport/_table',array('model'=>$model)); ?>
<?php 
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintSalesReport');
?>
<?php $this->renderPartial('_tab'); ?>
<iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
</iframe>
<script>
    $('document').ready(function(){
        $('.checkbox').children('input').attr('checked','checked');
    });
</script>
<?php $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); ?>