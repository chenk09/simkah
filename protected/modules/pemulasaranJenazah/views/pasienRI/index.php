<fieldset>
    <legend class="rim2">Infromasi Pasien Rawat Inap</legend>
</fieldset>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchRILagi(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'header'=>'Tgl Admisi / Masuk Kamar',
                        'type'=>'raw',
                        'value'=>'$data->tglAdmisiMasukKamar'
                    ),
//                    'ruangan_nama',
                    array(
                       'name'=>'caramasuk_nama',
                        'type'=>'raw',
                        'value'=>'$data->caramasuk_nama',
                    ),
                    array(
                       'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Bin',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'name'=>'umur',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                       'name'=>'kamarruangan_nokamar',
                        'type'=>'raw',
                        'value'=>'(!empty($data->kamarruangan_nokamar))? $data->kamarruangan_nokamar : CHtml::link("<i class=icon-home></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Memasukan Pasien Ke kamar","onclick"=>"{buatSessionMasukKamar($data->masukkamar_id,$data->kelaspelayanan_id,$data->pendaftaran_id); addMasukKamar(); $(\'#dialogMasukKamar\').dialog(\'open\');}"))',    
                    ),
                    
                    array(
                        'header'=>'Masuk Kamar Jenazah',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",Yii::app()->controller->createUrl("masukPenunjangJenazah/index",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RI)),
                                    array("class"=>"", 
                                          "target"=>"iframeMasukKamar",
                                          "onclick"=>"$(\"#dialogMasukKamar\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk masuk kamar",
                                    ))'
                    )
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr/>

<?php echo $this->renderPartial('_searchPasienRI', array('model'=>$model)); ?>

<?php 
// Dialog untuk masuk kamar jenazah =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogMasukKamar',
    'options'=>array(
        'title'=>'Masuk Kamar Jenazah',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>950,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeMasukKamar" width="100%" height="450">
</iframe>

<?php
$this->endWidget();
//========= end masuk kamar jenazah =============================
?>
