<fieldset>
    <legend class="rim2">Informasi Pasien Meninggal</legend>
</fieldset>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->search(),
//        'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    'tgl_pendaftaran',
                    'no_pendaftaran',
                    'no_rekam_medik',
                    'nama_pasien',
                    'nama_bin',
                    'caramasuk_nama',
                    'instalasi_nama',
                    'alamat_pasien',
                    'carabayar_nama',
                    'penjamin_nama',
                    'kondisipulang',
                    array(
                        'header'=>'Masuk Kamar Jenazah',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",Yii::app()->controller->createUrl("masukKamarJenazah/index",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RD)),
                                    array("class"=>"", 
                                          "target"=>"iframeMasukKamar",
                                          "onclick"=>"$(\"#dialogMasukKamar\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk masuk kamar",
                                    ))'
                    )
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr/>

<?php $this->renderPartial('_searchPasienMeninggal',array('model'=>$model)); ?>

<?php 
// Dialog untuk masuk kamar jenazah =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogMasukKamar',
    'options'=>array(
        'title'=>'Masuk Kamar Jenazah',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>950,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeMasukKamar" width="100%" height="450">
</iframe>

<?php
$this->endWidget();
//========= end masuk kamar jenazah =============================
?>