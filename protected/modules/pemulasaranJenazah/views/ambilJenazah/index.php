<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class="rim2">Transaksi Pengambilan Jenazah</legend>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'ambiljenazah-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php //echo $form->errorSummary(array($model,$modPenyBarangs[0])); ?>
            
    <table>
        <tr>
            <td width="50%">
                <?php echo $form->hiddenField($model,'pendaftaran_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->hiddenField($model,'pasien_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo CHtml::activeLabel($model, 'no_pendaftaran', array('class' => 'control-label')); ?>
                    <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $model,
                                'attribute' => 'no_pendaftaran',
                                'value' => '',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/NoPendaftaranJenazah'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);

                                            return false;
                                        }',
                                    'select' => 'js:function( event, ui ) {
                                              $("#' . CHtml::activeId($model, 'pasien_id') . '").val(ui.item.pasien_id);
                                              $("#' . CHtml::activeId($model, 'pendaftaran_id') . '").val(ui.item.pendaftaran_id);
                                              $("#' . CHtml::activeId($model, 'no_rekam_medik') . '").val(ui.item.no_rekam_medik);
                                              setRuanganMeninggal(ui.item.instalasiasal_id,ui.item.ruanganasal_id);
                                          }',
                                ),
                                'htmlOptions'=>array('class'=>'span2'),
                            ));
                        ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'no_rekam_medik',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php
                            $this->widget('MyJuiAutoComplete', array(
                                'model' => $model,
                                'attribute' => 'no_rekam_medik',
                                'value' => '',
                                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/PasienJenazah'),
                                'options' => array(
                                    'showAnim' => 'fold',
                                    'minLength' => 2,
                                    'focus' => 'js:function( event, ui ) {
                                            $(this).val( ui.item.label);

                                            return false;
                                        }',
                                    'select' => 'js:function( event, ui ) {
                                              $("#' . CHtml::activeId($model, 'pasien_id') . '").val(ui.item.pasien_id);
                                              $("#' . CHtml::activeId($model, 'pendaftaran_id') . '").val(ui.item.pendaftaran_id);
                                              $("#' . CHtml::activeId($model, 'no_pendaftaran') . '").val(ui.item.no_pendaftaran);
                                              setRuanganMeninggal(ui.item.instalasiasal_id,ui.item.ruanganasal_id);
                                          }',
                                ),
                                'htmlOptions'=>array('class'=>'span2'),
                            ));
                        ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglmeninggal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglmeninggal',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>false,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($model,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'tglpengambilan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpengambilan',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                                                            'model'=>$model,
                                                            'attribute'=>'tglpengambilan',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                                'minDate' => 'd',
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>false,'class'=>'dtPicker2-5'),
                                    )); 
                        ?>
                    </div>
                </div>
                <?php echo $form->textAreaRow($model,'keterangan_pengambilan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($model,'ruanganmeninggal_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'ruanganmeninggal_id',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('instalasi', $instalasi, CHtml::listData($modInstalasi, 'instalasi_id', 'instalasi_nama'),
                                                        array('empty' =>'-- Instalasi --',
                                                              'ajax'=>array('type'=>'POST',
                                                                            'url'=>  CController::createUrl('dynamicRuangan'),
                                                                            'update'=>'#'.CHtml::activeId($model, 'ruanganmeninggal_id'),),'class'=>'span2')); ?>
                        <?php echo CHtml::activeDropDownList($model, 'ruanganmeninggal_id',  CHtml::listData(RuanganM::model()->getRuanganByInstalasi($instalasi),'ruangan_id','ruangan_nama'),array('empty' =>'-- Ruangan --','class'=>'span2')); ?>
                    </div>
                </div>
            </td>
            <td width="50%">
                <?php echo $form->textFieldRow($model,'nama_pengambiljenazah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->dropDownListRow($model,'hubungan_pengjenazah', HubunganKeluarga::items(),array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'empty'=>'-- Pilih --')); ?>
                <?php echo $form->textFieldRow($model,'noidentitas_pengjenazah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textAreaRow($model,'alamat_pengjenazah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model,'notelepon_pengjenazah',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            </td>
        </tr>
    </table>
    
    <table id="tblPenyBarang" class="table table-bordered">
        <thead>
            <tr>
                <th>No Urut <span class="required">*</span></th>
                <th>Jenis Barang</th>
                <th>Nama Barang <span class="required">*</span></th>
                <th>Keadaan Barang</th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $this->renderPartial('_formPenyBarang',array('modPenyBarang'=>$modPenyBarang,'modPenyBarangs'=>$modPenyBarangs,'removeButton'=>false)); ?>
        </tbody>
    </table>
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                         Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
									         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
			<?php 
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
var trPenyBarang=new String(<?php echo CJSON::encode($this->renderPartial('_formPenyBarang',array('modPenyBarang'=>$modPenyBarang,'removeButton'=>true),true));?>);
var trPenyBarangFirst=new String(<?php echo CJSON::encode($this->renderPartial('_formPenyBarang',array('modPenyBarang'=>$modPenyBarang,'removeButton'=>false),true));?>);

function addPenyBarang(obj)
{
    $(obj).parents('table').children('tbody').append(trPenyBarang.replace());
    <?php 
        $attributes = $modPenyBarang->attributeNames();
        foreach($attributes as $i=>$attribute){
            echo "renameInput('".get_class($modPenyBarang)."','$attribute');";
        }
    ?>
    buatNoUrut();
}

function buatNoUrut()
{
    var i = 0;
    $('input[name$="[no_urutbrg]"]').each(function(){
        i++; $(this).val(i);
    });
}

function batalPenyBarang(obj)
{
    if(confirm('Apakah anda yakin akan membatalkan?')){
        $(obj).parents('tr').detach();
        buatNoUrut();
    }
}

function renameInput(modelName,attributeName)
{
    var i = -1;
    $('#tblPenyBarang tr').each(function(){
        if($(this).has('input[name$="[no_urutbrg]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function setRuanganMeninggal(instalasiasalId,ruanganasalId)
{
    $("#instalasi").val(instalasiasalId);
    $("#instalasi").change();
    alert('Otomatis mengambil dari instalasi/ruangan/unit pasien terakhir diperiksa');$("#<?php echo CHtml::activeId($model, 'ruanganmeninggal_id') ?>").val(ruanganasalId);
}
</script>
    