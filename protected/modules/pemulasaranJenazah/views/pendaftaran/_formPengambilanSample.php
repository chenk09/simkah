<fieldset class="">
    <legend  class="accord1">
        <?php echo CHtml::checkBox('pakeSample', $model->pakeSample, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Sample Laboratorium
    </legend>
    <div id="divSample" class="control-group toggle <?php echo ($model->pakeSample) ? '':'hide'; ?>">
        <?php echo $form->dropDownListRow($modPengambilanSample,'samplelab_id', CHtml::listData($modPengambilanSample->getSampleLabItems(), 'samplelab_id', 'samplelab_nama') ,array('disabled'=>true,'empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
        <?php echo $form->textFieldRow($modPengambilanSample,'no_pengambilansample', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modPengambilanSample,'jmlpengambilansample', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modPengambilanSample,'tempatsimpansample', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textAreaRow($modPengambilanSample,'keterangansample', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
    </div>
</fieldset>
<?php
$enableInputPengambilanSample = ($model->pakeSample) ? 1 : 0;
$noPengambilanSample = $modPengambilanSample->no_pengambilansample;
$js = <<< JS
if(${enableInputPengambilanSample}) {
    $('#divSample input').removeAttr('disabled');
    $('#divSample select').removeAttr('disabled');
}
else {
    $('#divSample input').attr('disabled','true');
    $('#divSample select').attr('disabled','true');
}

$('#pakeSample').change(function(){
        if ($(this).is(':checked')){
                $('#divSample input').removeAttr('disabled');
                $('#divSample select').removeAttr('disabled');
                $('#PJPengambilanSampleT_no_pengambilansample').val('${noPengambilanSample}')
        }else{
                $('#divSample input').attr('disabled','true');
                $('#divSample select').attr('disabled','true');
                $('#divSample input').attr('value','');
                $('#divSample select').attr('value','');
        }
        $('#divSample').slideToggle(500);
    });
JS;
Yii::app()->clientScript->registerScript('sample',$js,CClientScript::POS_READY);
?>