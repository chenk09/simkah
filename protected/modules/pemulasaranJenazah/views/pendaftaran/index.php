
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pendaftaran-jenazah-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#isPasienLama',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php 
    if(KonfigsystemK::model()->find()->isantrian==TRUE){ 
        $modLoket = LoketM::model()->findAllByAttributes(array('loket_aktif'=>true));
        $loket = (isset($_POST['loket'])) ? $_POST['loket'] : '';
        echo CHtml::dropDownList('loket', $loket, CHtml::listData($modLoket, 'loket_id', 'loket_nama'), array('empty'=>'-- Pilih Loket --','class'=>'span2'));
        echo CHtml::link(Yii::t('mds', 'Antrian {icon}', array('{icon}'=>'<i class="icon-chevron-right icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"$('#dialogAntrian').parent().css({position:'fixed'}).end().dialog('open');return false;")); 
    }
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>  
        <legend class="rim2">Pendaftaran Jenazah</legend>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        
	<?php echo $form->errorSummary(array($model,$modPasien,$modPenanggungJawab,$modRujukan,$modTindakanPelayan)); ?>
        <table class="table-condensed">
            <tr>
                <td width="50%">
                    
                </td>
                <td width="50%">
                   <div class="control-group ">
                        <?php echo $form->labelEx($model,'tgl_pendaftaran', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$model,
                                                    'attribute'=>'tgl_pendaftaran',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                            <?php echo $form->error($model, 'tgl_pendaftaran'); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <?php echo $this->renderPartial('_formPasien', array('model'=>$model,'form'=>$form,'modPasien'=>$modPasien,'format'=>$format)); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <fieldset>
                        <legend class="rim">Data Kunjungan</legend>
                    <table>
                        <tr>
                            <td width="50%">
                                <?php echo $form->textFieldRow($model,'no_urutantri', array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <table>
                                    <tr>
                                        <td width="143px">
                                            <div align="right">
                                                    <?php echo $form->labelEx($model,'ruangan_id');?>
                                            </div>    
                                        </td>
                                        <td><?php echo $form->dropDownList($model,'ruangan_id', CHtml::listData($model->getRuanganItems(Params::INSTALASI_ID_JZ), 'ruangan_id', 'ruangan_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'ajax'=>array('type'=>'POST',
                                                                                      'url'=>Yii::app()->createUrl('ActionDynamic/GetKasusPenyakit',array('encode'=>false,'namaModel'=>'PJPendaftaranT')),
                                                                                      'update'=>'#PJPendaftaranT_jeniskasuspenyakit_id'),
                                                                        'onchange'=>"listDokterRuangan(this.value);listKarcis(this.value)",
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?></td>
                                        <td>

                                                    <div class="checkbox inline">
                                                        <?php echo $form->checkBox($model,'kunjunganrumah', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                                                        <?php echo CHtml::activeLabel($model, 'kunjunganrumah'); ?>
                                                    </div>

                                        </td>
                                    </tr>
                                </table>

                                <?php echo $form->dropDownListRow($model,'jeniskasuspenyakit_id', CHtml::listData($model->getJenisKasusPenyakitItems($model->ruangan_id), 'jeniskasuspenyakit_id', 'jeniskasuspenyakit_nama') ,
                                                                  array('empty'=>'-- Pilih --',
                                                                        'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php echo $form->dropDownListRow($model,'kelaspelayanan_id', CHtml::listData($model->getKelasPelayananItems(), 'kelaspelayanan_id', 'kelaspelayanan_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>"listKarcis(this.value)")); ?>

                                <?php echo $form->dropDownListRow($model,'pegawai_id', CHtml::listData($model->getDokterItems($model->ruangan_id), 'pegawai_id', 'nama_pegawai') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                                <?php //echo $form->dropDownListRow($model,'statusmasuk', StatusMasuk::items(), array('onchange'=>'enableRujukan(this);','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>


                                <?php echo $form->dropDownListRow($model,'carabayar_id', CHtml::listData($model->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                            'ajax' => array('type'=>'POST',
                                                                'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'PJPendaftaranT')), 
                                                                'update'=>'#PJPendaftaranT_penjamin_id'  //selector to update
                                                            ),
                                    )); ?>

                                <?php echo $form->dropDownListRow($model,'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>

                            </td>
                            <td width="50%">
                                <?php echo $this->renderPartial('_formRujukan', array('model'=>$model,'form'=>$form,'modRujukan'=>$modRujukan)); ?>

                                <?php echo $this->renderPartial('_formAsuransi', array('model'=>$model,'form'=>$form)); ?>

                                <?php echo $this->renderPartial('_formPenanggungJawab', array('model'=>$model,'form'=>$form,'modPenanggungJawab'=>$modPenanggungJawab)); ?>


                                <fieldset id="fieldsetKarcis" class="">
                                    <legend>
                                        <?php echo CHtml::checkBox('karcisTindakan', $model->adaKarcis, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Karcis Poliklinik
                                        <?php echo CHtml::checkBox('bayarKarcisLangsung', '', array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                                        Bayar Langsung
                                    </legend>
                                <?php echo $this->renderPartial('_formKarcis', array('model'=>$model,'form'=>$form)); ?>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
            </tr>
        </table>
    </fieldset>

	<div class="form-actions">
		<?php 
                    if($model->isNewRecord)
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                    else 
                      echo CHtml::htmlButton(Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')), array('disabled'=>true,'class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)'));
                ?>
		<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('Pendaftaran/Index',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
                <?php if((!$model->isNewRecord) AND ((KonfigsystemK::model()->find()->printkartulsng==TRUE) OR (KonfigsystemK::model()->find()->printkartulsng==TRUE)))
                        {
                          
                ?>
                      <!--      <script>
                                print(<?php //echo $model->pendaftaran_id ?>);
                            </script>
                <?php //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('$model->pendaftaran_id');return false",'disabled'=>FALSE  )); 
                     //  }else{
                    //    echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','disabled'=>TRUE  )); 
                       } 
                ?> 
				<?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>

	</div>

<?php $this->endWidget(); ?>

<?php Yii::app()->clientScript->registerScript('status_masuk',"
    $(document).ready(function(){
              
        $(':input').keypress(function(e) {
            if(e.keyCode == 13) {
                $(this).focusNextInputField(); 
            } 
        });
    });
    "); ?>

<?php
$urlPrintLembarPoli = Yii::app()->createUrl('print/lembarPoliRJ',array('idPendaftaran'=>''));
$urlPrintKartuPasien = Yii::app()->createUrl('print/kartuPasien',array('idPendaftaran'=>''));
$urlListDokterRuangan = Yii::app()->createUrl('actionDynamic/listDokterRuangan');
$urlListKarcis =Yii::app()->createUrl('actionAjax/listKarcis');

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKunjunganPasien=KonfigsystemK::model()->find()->printkunjunganlsng;
if(!empty($cekKunjunganPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKunjunganPasien=$cekKunjunganPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKunjunganPasien=0;
}

//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= AWAL  Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan
$cekKartuPasien=KonfigsystemK::model()->find()->printkartulsng;;
if(!empty($cekKartuPasien)){ //Jika Kunjungan Pasien diisi TRUE
    $statusKartuPasien=$cekKartuPasien;
}else{ //JIka Print Kunjungan Diset FALSE
    $statusKartuPasien=0;
}
//================= Akhir Ngecek Ke Konfigurasi System Status Print Kartu Kunjungan

//================= Awal Ngecek Status Pasien
$statusPasien=$model->statuspasien;


$statusPasienBaru=Params::statusPasien('baru');

//================= Akhir Ngecek Status Pasien

$jscript = <<< JS
function print(idPendaftaran)
{

        if(${statusKunjunganPasien}==1){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('${urlPrintLembarPoli}'+idPendaftaran,'printwin','left=100,top=100,width=400,height=400');
        }     
        
        if((${statusKartuPasien}==1) && ('${statusPasien}'=='${statusPasienBaru}')){ //Jika di Konfig Systen diset TRUE untuk Print Kartu Pasien
                    window.open('${urlPrintKartuPasien}'+idPendaftaran,'printwi','left=100,top=100,width=400,height=280');
        }
}

function listKarcis(obj)
{
     kelasPelayanan=$('#PJPendaftaranT_kelaspelayanan_id').val();
     ruangan=$('#PJPendaftaranT_ruangan_id').val();
            
     if(kelasPelayanan!='' && ruangan!=''){
             $.post("${urlListKarcis}", { kelasPelayanan: kelasPelayanan, ruangan:ruangan },
                function(data){
                    $('#tblFormKarcis').append(data.form);
             }, "json");
     }      
       
}
             
function changeBackground(obj,idTindakan,tarifSatuan,idKarcis)
{
        banyakRow=$('#tblFormKarcis tr').length;
        for(i=1; i<=banyakRow; i++){
        $('#tblFormKarcis tr').css("background-color", "#FFFFFF");          
        } 
             
        $(obj).parent().parent().css("backgroundColor", "#00FF00");     
        $('#TindakanPelayananT_idTindakan').val(idTindakan);  
        $('#TindakanPelayananT_tarifSatuan').val(tarifSatuan);     
        $('#TindakanPelayananT_idKarcis').val(idKarcis);     
     
}             
            
function listDokterRuangan(idRuangan)
{
    $.post("${urlListDokterRuangan}", { idRuangan: idRuangan },
        function(data){
            $('#PJPendaftaranT_pegawai_id').html(data.listDokter);
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);

$js = <<< JS
$('.numbersOnly').keyup(function() {
var d = $(this).attr('numeric');
var value = $(this).val();
var orignalValue = value;
value = value.replace(/[0-9]*/g, "");
var msg = "Only Integer Values allowed.";

if (d == 'decimal') {
value = value.replace(/\./, "");
msg = "Only Numeric Values allowed.";
}

if (value != '') {
orignalValue = orignalValue.replace(/([^0-9].*)/g, "")
$(this).val(orignalValue);
}
});
JS;
Yii::app()->clientScript->registerScript('numberOnly',$js,CClientScript::POS_READY);
?>

<?php 
    if(KonfigsystemK::model()->find()->isantrian==TRUE){ 
        $this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogAntrian',
            'options'=>array(
                'title'=>'Antrian',
                'autoOpen'=>true,
                'resizable'=>false,
                'width'=>170,
                'height'=>120,
                'position'=>'right',
            ),
        ));

            echo '<iframe src="'.$this->createUrl('TampilAntrian/PanggilAntrian').'" width="100%" height="70px" scrolling="no"></iframe>';

        $this->endWidget('zii.widgets.jui.CJuiDialog');
        
    }
?>