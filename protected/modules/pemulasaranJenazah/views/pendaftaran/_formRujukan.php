<fieldset class="">
    <legend  class="accord1">
        <?php echo CHtml::checkBox('isRujukan', $model->isRujukan, array('onchange'=>'toggleRujukan();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
        Rujukan
    </legend>
    <div id="divRujukan" class="toggle <?php echo ($model->isRujukan) ? '':'hide' ?> toggle" >
        <?php echo $form->dropDownListRow($modRujukan,'asalrujukan_id', CHtml::listData($modRujukan->getAsalRujukanItems(), 'asalrujukan_id', 'asalrujukan_nama'), array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modRujukan,'no_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        <?php echo $form->textFieldRow($modRujukan,'nama_perujuk', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        
        <div class="control-group ">
            <?php echo $form->labelEx($modRujukan,'tanggal_rujukan', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modRujukan,
                                        'attribute'=>'tanggal_rujukan',
                                        'mode'=>'date',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); ?>
                <?php echo $form->error($modRujukan, 'tanggal_rujukan'); ?>
            </div>
        </div>
        <?php echo $form->textFieldRow($modRujukan,'diagnosa_rujukan', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?> 
    </div>
</fieldset>
    
<?php
$js = <<< JS
function toggleRujukan()
{
    $('#divRujukan').slideToggle(500);
}
JS;
Yii::app()->clientScript->registerScript('ceklistRujukan',$js,CClientScript::POS_HEAD);
?>