<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'gfobat-alkes-m-searchData',
        'type'=>'horizontal',
)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'lokasigudangNama',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'therapiobatNama',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'pbf_id',array('class'=>'span5')); ?>

	<?php// echo $form->textFieldRow($model,'generik_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'satuanbesarNama',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'sumberdana_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'satuankecil_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'jenisobatalkes_id',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span5','maxlength'=>200)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_golongan',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kategori',array('class'=>'span5','maxlength'=>50)); ?>

	<?php //echo $form->textFieldRow($model,'obatalkes_kadarobat',array('class'=>'span5','maxlength'=>20)); ?>

	<?php //echo $form->textFieldRow($model,'kemasanbesar',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'kekuatan',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'satuankekuatan',array('class'=>'span5','maxlength'=>20)); ?>

	<?php //echo $form->textFieldRow($model,'harganetto',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'hargajual',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'discount',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'tglkadaluarsa',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'minimalstok',array('class'=>'span5')); ?>

	<?php //echo $form->textFieldRow($model,'formularium',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->checkBoxRow($model,'discountinue',array('checked'=>'discountinue')); ?>

	<?php echo $form->checkBoxRow($model,'obatalkes_aktif',array('checked'=>'obatalkes_aktif')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
