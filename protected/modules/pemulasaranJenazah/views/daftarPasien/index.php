<fieldset>
    <legend class="rim2">Informasi Pelumasan Jenazah</legend>
</fieldset>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->search(),
//        'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    'tgl_pendaftaran',
                    'no_pendaftaran',
                    'no_rekam_medik',
                    'nama_pasien',
                    'nama_bin',
                    'caramasuk_nama',
                    'instalasiasal_nama',
                    'alamat_pasien',
                    'carabayar_nama',
                    'penjamin_nama',
                    array(
                        'header'=>'Tindakan & Pelayanan',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",Yii::app()->controller->createUrl("TindakanPelayanan/Index",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RD)),
                                    array("class"=>"", 
                                          "target"=>"",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk Tindakan & Pelayanan",
                                    ))'
                    ),
                    array(
                        'header'=>'Ambil Jenazah',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",Yii::app()->controller->createUrl("AmbilJenazah/Index",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RD)),
                                    array("class"=>"", 
                                          "target"=>"",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk Ambil Jenazah",
                                    ))'
                    ),
                    array(
                        'header'=>'Pemakaian Mobil Jenazah',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>",Yii::app()->controller->createUrl("PemakaianMobil/Pemakaian",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RD)),
                                    array("class"=>"", 
                                          "target"=>"",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk Memakai Mobil Jenazah",
                                    ))'
                    ),
                    array(
                        'header'=>'Surat Ket. Meninggal',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-file\"></i>",Yii::app()->controller->createUrl("suratKeterangan/SuratKematian",array("idPendaftaran"=>$data->pendaftaran_id,"idInstalasi"=>PARAMS::INSTALASI_ID_RD)),
                                    array("class"=>"", 
                                          "target"=>"iframeCetakSurat",
                                          "onclick"=>"$(\"#dialogCetakSurat\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk masuk kamar",
                                    ))'
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr/>

<?php $this->renderPartial('_searchDaftarPasien',array('model'=>$model)); ?>

<?php 
// Dialog untuk masuk kamar jenazah =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogCetakSurat',
    'options'=>array(
        'title'=>'Print Surat Keterangan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>950,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeCetakSurat" width="100%" height="500">
</iframe>

<?php
$this->endWidget();
//========= end masuk kamar jenazah =============================
?>