<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'panggilAntrian-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
)); ?>
        <div align="left">
            <table>
                <tr>
                    <td>
                        <?php echo $dropDownList ?>
                        <?php //$this->widget('bootstrap.widgets.BootButton', array('type'=>'danger','icon'=>'white volume-up','htmlOptions'=>array('onclick'=>'mainkan();', 'rel'=>"tooltip", 'title'=>"Panggil Antrian"))); ?>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle"><font color="#FF0000" size="6px"><b><?php echo $noAntrianUtuh ?></b></font></td>
                </tr>
                <tr>
                    <td>
                    <?php echo CHtml::htmlButton('Pasien Selanjutnya',array(
                                                    'id'=>'pasienLama','onclick'=>'panggilAntrian()','class'=>'btn btn-info'
                        )); ?>
                    </td>
                </tr>
            </table>
        </div>

<div id="player">
    <div id="jquery_jplayer_antrian" class="jp-jplayer"></div>
    <?php 
       if(!empty($forms))
       {
           echo $forms;
       }
    ?>
    <div id="jquery_jplayer_loket" class="jp-jplayer"></div>
    <div id="jquery_jplayer_loket_pilihan" class="jp-jplayer"></div>
</div>




<?php $this->endWidget(); ?>

<script type="text/javascript">
mainkan();
    
function panggilAntrian()
{
    var idLoket = window.parent.$('#loket').val();
    if(idLoket != ''){
        $('#carabayar_loket').val(idLoket);
        submit();
    } else {
        alert('Pilih Loket dahulu!');
    }
}

function submit()
{
    document.forms["panggilAntrian-form"].submit();
}

function mainkan()
{
    $("#jquery_jplayer_antrian").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/mp3/antrian.mp3",
				ogg: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/ogg/antrian.ogg"
			}).jPlayer("play");
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_0").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, ogg",
		wmode: "window",
		autoPlay : true
	});
}

    <?php 
        for ($f = 0; $f< $jumlah; $f++) 
        {
            $g = $f + 1;
            if($f == $jumlah - 1){$loket = 'loket';}else{$loket = $g;}
            echo  '$("#jquery_jplayer_'.$f.'").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "'.Yii::app()->request->baseUrl.'/data/sounds/mp3/'.$noAntrianBaru[$f].'.mp3",
				ogg: "'.Yii::app()->request->baseUrl.'/data/sounds/ogg/'.$noAntrianBaru[$f].'.ogg"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
                                        
					$("#jquery_jplayer_'.$loket.'").jPlayer("play", 0);
				});
			}
		},
		swfPath: "'.Yii::app()->request->baseUrl.'/js/js.sound",
		supplied: "mp3, ogg",
		cssSelectorAncestor: "#jp_container_2",
		wmode: "window"
	});';
        }
    ?>


	
	
	$("#jquery_jplayer_loket").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/mp3/loket2.mp3",
				ogg: "<?php echo Yii::app()->request->baseUrl;?>/data/sounds/ogg/loket2.ogg"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_loket_pilihan").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, ogg",
		cssSelectorAncestor: "#jp_container_7",
		wmode: "window"
	});

	$("#jquery_jplayer_loket_pilihan").jPlayer({
		ready: function () {
			$(this).jPlayer("setMedia", {
				mp3: "<?php echo $formLoketMp3 ?>",
				ogg: "<?php echo $formLoketOgg ?>"
			});
		},
		play: function() { // To avoid both jPlayers playing together.
			$(this).jPlayer("pauseOthers");
		},
		repeat: function(event) { // Override the default jPlayer repeat event handler
			if(event.jPlayer.options.loop) {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerRepeat", function() {
					$(this).jPlayer("play");
				});
			} else {
				$(this).unbind(".jPlayerRepeat").unbind(".jPlayerNext");
				$(this).bind($.jPlayer.event.ended + ".jPlayer.jPlayerNext", function() {
					$("#jquery_jplayer_9").jPlayer("play", 0);
				});
			}
		},
		swfPath: "<?php echo Yii::app()->request->baseUrl;?>/js/js.sound",
		supplied: "mp3, ogg",
		cssSelectorAncestor: "#jp_container_8",
		wmode: "window"
	});
</script>
