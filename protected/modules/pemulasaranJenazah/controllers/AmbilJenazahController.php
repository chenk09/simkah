<?php

class AmbilJenazahController extends SBaseController
{
        protected $successSaveBarang;
        protected $validBarang = true;

        public function actionIndex($idPendaftaran='')
	{
            if(!empty($idPendaftaran)){
                $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                $model = new PJAmbiljenazahT;
                $model->pendaftaran_id = $idPendaftaran;
                $model->pasien_id = $modPendaftaran->pasien_id;
                $model->no_pendaftaran = $modPendaftaran->no_pendaftaran;
                $model->no_rekam_medik = $modPasien->no_rekam_medik;
            } else {
                $model = new PJAmbiljenazahT;
            }
            
            $model->tglmeninggal = date('d M Y H:i:s');
            $model->tglpengambilan = date('d M Y H:i:s');
            $modInstalasi = InstalasiM::model()->findAllByAttributes(array('instalasi_aktif'=>true),array('order'=>'instalasi_nama'));
            $instalasi = '';
            
            $modPenyBarang = new PJPenyerahanbarangpasienT;
            $modPenyBarang->no_urutbrg = 1;
            
            if(isset($_POST['PJAmbiljenazahT'])){
                $instalasi = $_POST['instalasi'];
                $format = new CustomFormat;
                $model->attributes = $_POST['PJAmbiljenazahT'];
                $model->tglpengambilan = $format->formatDateTimeMediumForDB($_POST['PJAmbiljenazahT']['tglpengambilan']);
                $model->tglmeninggal = $format->formatDateTimeMediumForDB($_POST['PJAmbiljenazahT']['tglmeninggal']);
                $model->ruangan_id = Yii::app()->user->ruangan_id;

                if($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $model->save();
                        $modPenyBarangs = $this->savePenyerahanBarang($model, $_POST['PJPenyerahanbarangpasienT']);
                        if($this->validBarang){
                            foreach($modPenyBarangs as $i=>$penyBarang){
                                $penyBarang->save();
                            }
                            $this->updatePasien($model);
                            $transaction->commit();
                            Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                            $this->refresh();
                        } else {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data Gagal disimpan.".'<pre>'.print_r($modPenyBarangs[0]->getErrors(),1).'</pre>');
                        }
                    } catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                    }
                } 
            }
            
            $this->render('index', array('model'=>$model,
                                         'modInstalasi'=>$modInstalasi,
                                         'instalasi'=>$instalasi,
                                         'modPenyBarang'=>$modPenyBarang,
                                         'modPenyBarangs'=>$modPenyBarangs));
	}
        
        protected function savePenyerahanBarang($modAmbilJenazah,$post)
        {
            $valid;
            foreach($post as $i=>$item) {
                $modPenyBarangs[$i] = new PJPenyerahanbarangpasienT;
                $modPenyBarangs[$i]->attributes = $item;
                $modPenyBarangs[$i]->ambiljenazah_id = $modAmbilJenazah->ambiljenazah_id;
                $valid = $modPenyBarangs[$i]->validate();
                $this->validBarang = $this->validBarang && $valid;
            }
            
            return $modPenyBarangs;
        }
        
        protected function updatePasien($modAmbilJenazah)
        {
            PasienM::model()->updateByPk($modAmbilJenazah->pasien_id, array('tgl_meninggal'=>$modAmbilJenazah->tglmeninggal));
        }
                
        public function actionDynamicRuangan()
        {
            $data = RuanganM::model()->findAll('instalasi_id=:instalasi_id AND ruangan_aktif = TRUE order by ruangan_nama', 
                  array(':instalasi_id'=>(int) $_POST['instalasi']));

            $data=CHtml::listData($data,'ruangan_id','ruangan_nama');

            if(empty($data))
            {
                echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Ruangan --'),true);
            }
            else
            {
                echo CHtml::tag('option',array('value'=>''),CHtml::encode('-- Ruangan --'),true);
                foreach($data as $value=>$name)
                {
                    echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                }
            }
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}