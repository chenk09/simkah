<?php

class PasienRDController extends SBaseController
{
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Pasien Rawat Darurat";
            $format = new CustomFormat();
            $model = new PJInfoKunjunganRDV;
            $model->tglAwal = date("Y-m-d").' 00:00:00';
            $model->tglAkhir = date('Y-m-d h:i:s');
            if(isset ($_GET['PJInfoKunjunganRDV'])){
                $model->attributes=$_GET['PJInfoKunjunganRDV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJInfoKunjunganRDV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJInfoKunjunganRDV']['tglAkhir']);
                $model->ceklis = $_GET['PJInfoKunjunganRDV']['ceklis'];
            }

            $this->render('index',array('model'=>$model));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}