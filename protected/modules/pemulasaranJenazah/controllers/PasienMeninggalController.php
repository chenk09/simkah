<?php

class PasienMeninggalController extends SBaseController
{
	public function actionIndex()
	{
            $this->pageTitle = Yii::app()->name." - Pasien Meninggal";
            $format = new CustomFormat();
            $model = new PJDaftarpasienmeninggalV;
            $model->tglAwal = date("Y-m-d").' 00:00:00';
            $model->tglAkhir = date('Y-m-d h:i:s');
            if(isset ($_GET['PJDaftarpasienmeninggalV'])){
                $model->attributes=$_GET['PJDaftarpasienmeninggalV'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJDaftarpasienmeninggalV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJDaftarpasienmeninggalV']['tglAkhir']);
                $model->ceklis = $_GET['PJDaftarpasienmeninggalV']['ceklis'];
            }

            $this->render('index',array('model'=>$model));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}