<?php

class MasukPenunjangJenazahController extends SBaseController
{
        protected $statusSaveKirimkeUnitLain;
        protected $successSave;
        
        public function actionIndex($idPendaftaran='',$idInstalasi='')
	{
            $this->layout = '//layouts/frameDialog';
            $modelPulang = new PasienpulangT;
            $modelPulang->tglpasienpulang = date('Y-m-d H:i:s');
            $modelPulang->tgl_meninggal = date('Y-m-d H:i:s');
            $modelPulang->carakeluar = CaraKeluar::item('MENINGGAL');
            $modelPulang->kondisipulang = Params::KODE_MENINGGAL_1;
            $modelPulang->tglpasienpulang = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modelPulang->tglpasienpulang, 'yyyy-MM-dd hh:mm:ss'));
            $modelPulang->satuanlamarawat = SATUANLAMARAWAT_RD;
            $modelPulang->ruanganakhir_id = Yii::app()->user->getState('ruangan_id');
            if(!empty($idPendaftaran)){
                $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                $modMasukKamar = array();
            } 
            if($idInstalasi == Params::INSTALASI_ID_RI){
                $modPasienRIV = PasienrawatinapV::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                $modMasukKamar = MasukkamarT::model()->findByPk($modPasienRIV->masukkamar_id);
                $modMasukKamar->tglkeluarkamar = (!empty($modMasukKamar->tglkeluarkamar)) ? $modMasukKamar->tglkeluarkamar : date('d M Y');
                $modMasukKamar->lamadirawat_kamar = $this->hitungLamaDirawat($modMasukKamar->tglmasukkamar,$modMasukKamar->tglkeluarkamar);
                $modelPulang->satuanlamarawat = SATUANLAMARAWAT_RI;
            }
            
            $format = new CustomFormat();
            $date1 = $format->formatDateTimeMediumForDB($modPendaftaran->tgl_pendaftaran);
            $date2 = date('Y-m-d H:i:s');
            $diff = abs(strtotime($date2) - strtotime($date1));
            $hours   = floor(($diff)/3600); 

            $modelPulang->lamarawat = $hours;
            
            if(isset($_POST['PasienpulangT'])) {
                $modelPulang->attributes = $_POST['PasienpulangT'];
                $modelPulang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                $modelPulang->pasien_id = $modPendaftaran->pasien_id;
                if($modelPulang->validate()){
                    $modelPulang->save();
                    PasienM::model()->updateByPk($modPasien->pasien_id, array('tgl_meninggal'=>$modelPulang->tgl_meninggal));
                    $this->savePasienPenunjang($modPendaftaran, $modPasien);
                    if($this->successSave)
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                    else 
                        Yii::app()->user->setFlash('error',"Data Gagal disimpan");
                }
            }
            
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modelPulang'=>$modelPulang,
                                        'idInstalasi'=>$idInstalasi,
                                        'modMasukKamar'=>$modMasukKamar));
	}
        
        public function savePasienPenunjang($attrPendaftaran,$attrPasien){
            $format = new CustomFormat;
            $modPasienPenunjang = new PJPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $attrPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $attrPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $attrPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $attrPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $attrPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_JZ;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang(Yii::app()->user->getState('nopendaftaran_jenazah'));
            $modPasienPenunjang->tglmasukpenunjang = $format->formatDateTimeMediumForDB($attrPendaftaran->tgl_pendaftaran);
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($attrPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $attrPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $attrPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $attrPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
    
        protected function hitungLamaDirawat($dateMasuk='',$dateKeluar='')
        {
            $dateMasuk = (!empty($dateMasuk)) ? date('Y-m-d', strtotime($dateMasuk)) : date('Y-m-d');
            $dateKeluar = (!empty($dateKeluar)) ? date('Y-m-d', strtotime($dateKeluar)) : date('Y-m-d');
            $tgl1 = $dateMasuk;  
            $tgl2 = $dateKeluar; 

            $pecah1 = explode("-", $tgl1);
            $date1 = $pecah1[2];
            $month1 = $pecah1[1];
            $year1 = $pecah1[0];

            $pecah2 = explode("-", $tgl2);
            $date2 = $pecah2[2];
            $month2 = $pecah2[1];
            $year2 =  $pecah2[0];

            $jd1 = GregorianToJD($month1, $date1, $year1);
            $jd2 = GregorianToJD($month2, $date2, $year2);

            // hitung selisih hari kedua tanggal
            $selisihHari = ($jd2 - $jd1);
            
            return $selisihHari;
        }
        
        /*
        protected function savePasienKirimKeUnitLain($modPendaftaran)
        {
            $modKirimKeUnitLain = new PJPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->attributes = $_POST['PJPasienKirimKeUnitLainT'];
            $modKirimKeUnitLain->pasien_id = $modPendaftaran->pasien_id;
            $modKirimKeUnitLain->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            //$modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            $modKirimKeUnitLain->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_JZ;
            $modKirimKeUnitLain->ruangan_id = Params::RUANGAN_ID_JZ;
            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
            if($modKirimKeUnitLain->validate()){
                $modKirimKeUnitLain->save();
                $this->statusSaveKirimkeUnitLain = true;
            }
            
            return $modKirimKeUnitLain;
        }
         * 
         */

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}