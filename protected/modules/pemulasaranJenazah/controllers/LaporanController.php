<?php

class LaporanController extends SBaseController {
    
    public function actionLaporanSensusHarian() {
        $model = new PJLaporansensuspenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $kunjungan = Kunjungan::items();
        $model->kunjungan = $kunjungan;
        if (isset($_GET['PJLaporansensuspenunjangV'])) {
            $model->attributes = $_GET['PJLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporansensuspenunjangV']['tglAkhir']);
        }

        $this->render('sensus/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new PJLaporansensuspenunjangV('search');
        $judulLaporan = 'Laporan Sensus Harian Pemulasaran Jenazah';
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporansensuspenunjangV'])) {
            $model->attributes = $_REQUEST['PJLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporansensuspenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporansensuspenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['PJLaporansensuspenunjangV'])) {
            $model->attributes = $_GET['PJLaporansensuspenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporansensuspenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporansensuspenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKunjungan() {
        $model = new PJLaporanpasienpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->kunjungan = Kunjungan::items();
        if (isset($_GET['PJLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['PJLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpasienpenunjangV']['tglAkhir']);
        }

        $this->render('kunjungan/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new PJLaporanpasienpenunjangV('search');
        $judulLaporan = 'Laporan Kunjungan Pemulasaran Jenazah';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Pemulasaran Jenazah';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporanpasienpenunjangV'])) {
            $model->attributes = $_REQUEST['PJLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpasienpenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporanpasienpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['PJLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['PJLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpasienpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new PJLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['PJLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new PJLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Pemulasaran Jenazah';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien Pemulasaran Jenazah';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['PJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pemulasaran Jenazah';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['PJLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new PJLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenisObat =CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['PJLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['PJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/index',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new PJLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Pemulasaran Jenazah';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Pemulasaran Jenazah';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['PJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Pemulasaran Jenazah';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['PJLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new PJLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $tindakan = array('sudah', 'belum');
        $model->tindakansudahbayar_id = $tindakan;
        if (isset($_GET['PJLaporanjasainstalasi'])) {
            $model->attributes = $_GET['PJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new PJLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Pemulasaran Jenazah';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['PJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporanjasainstalasi'])) {
            $model->attributes = $_GET['PJLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new PJLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['PJLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['PJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new PJLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Pemulasaran Jenazah';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Pemulasaran Jenazah';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['PJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Pemulasaran Jenazah';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['PJLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new PJLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['PJLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['PJLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpendapatanruanganV']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new PJLaporanpendapatanruanganV('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Pemulasaran Jenazah';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporanpendapatanruanganV'])) {
            $model->attributes = $_REQUEST['PJLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporanpendapatanruanganV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['PJLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporanpendapatanruanganV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new PJBukuregisterpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['PJBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['PJBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJBukuregisterpenunjangV']['tglAkhir']);
        }

        $this->render('bukuRegister/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new PJBukuregisterpenunjangV('search');
        $judulLaporan = 'Laporan Buku Register Pasien Pemulasaran Jenazah';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Pemulasaran Jenazah';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJBukuregisterpenunjangV'])) {
            $model->attributes = $_REQUEST['PJBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new LKBukuregisterpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Pemulasaran Jenazah';
        $data['type'] = $_GET['type'];
        if (isset($_GET['LKBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['LKBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LKBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new PJLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'), 'asalrujukan_id', 'asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $ruanganasal = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_id');
        $model->ruanganasal_id = $ruanganasal;
        if (isset($_GET['PJLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['PJLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('caraMasuk/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new PJLaporancaramasukpenunjangV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Pemulasaran Jenazah';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['PJLaporancaramasukpenunjangV'])) {
            $model->attributes = $_REQUEST['PJLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['PJLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new PJLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['PJLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['PJLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['PJLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['PJLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}