<?php

class PendaftaranController extends SBaseController
{
        public $successSave = false;
        public $successSaveAdmisi = false; //variabel untuk validasi admisi
        public $successSaveRujukan = true; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
        public $successSaveTindakanKomponen= true;
        public $successSaveMasukKamar=true;
        
//        public function actions()
//        {
//                return array(
//                        'Index'=>array(
//                            'class'=>'PendaftaranJenazahAction',
//                        ),
//                );
//        }
        
	public function actionIndex()
	{
            //Yii::app()->getModule('pendaftaranPenjadwalan');
            $this->pageTitle = Yii::app()->name." - Pendaftaran Jenazah";
            $model = new PJPendaftaranT;
            $model->kelaspelayanan_id = Params::kelasPelayanan('rawat_jalan');
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = '00 Thn 00 Bln 00 Hr';
            if(!empty($_POST['pasien_id'])){//Jika pasien Lama
                $modPasien = PJPasienM::model()->findByPk($_POST['pasien_id']);
                $modPasien->ispasienluar=FALSE;
                $model->isPasienLama = true;
                $model->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
                $model->noRekamMedik = $modPasien->no_rekam_medik;
            } else { //Pasien Baru
                $modPasien = new PJPasienM;
                $modPasien->tanggal_lahir = date('d M Y');
                $modPasien->ispasienluar=FALSE;
            }
            
            $modPenanggungJawab = new PJPenanggungJawabM;
            $modRujukan = new PJRujukanT;
            $modTindakanPelayan= New PJTindakanPelayananT;
            if(isset($_POST['PJPendaftaranT']))
            {
//                echo '<pre>';
//                echo print_r($_POST['PJPendaftaranT']);
//                echo print_r($_POST['PJPasienM']);
//                echo print_r($_POST['PJPenanggungJawabM']);
//                echo print_r($_POST['PJRujukanT']);
//                echo '</pre>';
                $model->attributes = $_POST['PJPendaftaranT'];
                $modPasien->attributes = $_POST['PJPasienM'];
                $modPenanggungJawab->attributes = $_POST['PJPenanggungJawabM'];
                $modRujukan->attributes = $_POST['PJRujukanT'];
                
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if(!isset($_POST['isPasienLama'])) {
                        $modPasien = $this->savePasien($_POST['PJPasienM']);
                    } else {
                        $model->isPasienLama = true;
                        if(!empty($_POST['noRekamMedik'])) {
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = PJPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['PJPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    if(isset($_POST['isRujukan'])) {
                        $model->isRujukan = true;
                        $model->statusmasuk = 'RUJUKAN';
                        $modRujukan = $this->saveRujukan($_POST['PJRujukanT']);
                    } else {
                        $model->statusmasuk = 'NON RUJUKAN';
                    }
                    
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab->save();
                    }
                    
                    if(count($modPasien)>0) {
                        $model = $this->savePendaftaran($model,$modPasien,$modRujukan,$modPenanggungJawab);
                        $modPenunjang = $this->savePasienPenunjang($model, $modPasien);
                    } else {
                        $this->successSave = false;
                        $modPasien = new PJPasienM;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar');
                        Yii::app()->user->setFlash('danger',"Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar");
                    }
                    $this->saveUbahCaraBayar($model);
                    
                    if(isset($_POST['TindakanPelayananT']) && !empty ($_POST['TindakanPelayananT']['idTindakan']))
                    {
                        $this->saveTindakanPelayanan($modPasien,$model);
                    }
                    
//                    if($this->successSaveRujukan !== ''|| $this->successSaveRujukan !== '' )
//                    {
//                        if($this->successSaveRujukan !== '' && $this->successSaveRujukan == '' )
//                        {
//                            
//                        }
//                    }
                    if($this->successSave && $this->successSaveRujukan && $this->successSavePJ && $this->successSaveTindakanKomponen) {
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        $model->isNewRecord = TRUE;
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                    
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            $model->isRujukan = (isset($_POST['isRujukan'])) ? true : false;
            //$model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
            
            $modCaraBayar = CarabayarM::model()->findAllByAttributes(array('carabayar_aktif'=>true),array('order'=>'carabayar_nama'));
               
            $this->render('index',array('model'=>$model,
                                             'modPasien'=>$modPasien,
                                             'modPenanggungJawab'=>$modPenanggungJawab,
                                             'modRujukan'=>$modRujukan,
                                             'modCaraBayar'=>$modCaraBayar,
                                             'modTindakanPelayan'=>$modTindakanPelayan,
                                            ));
	}
        
        public function savePendaftaran($model,$modPasien,$modRujukan,$modPenanggungJawab)
        {
            $modelNew = new PJPendaftaranT;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            //$modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranRJ());
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Yii::app()->user->getState('nopendaftaran_jenazah'));
            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            
            if($modelNew->validate()) {
                // form inputs are valid, do something here
                $modelNew->save();
                $this->successSave = true;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'),'medium',null);
            }
            
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
                    
            return $modelNew;
        }
        
        public function savePasienPenunjang($model,$modPasien){
            $format = new CustomFormat;
            $modPasienPenunjang = new PJPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $model->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $model->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $model->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $model->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_JZ;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang(Yii::app()->user->getState('nopendaftaran_jenazah'));
            $modPasienPenunjang->tglmasukpenunjang = $format->formatDateTimeMediumForDB($model->tgl_pendaftaran);
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($model->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $model->kunjungan;
            $modPasienPenunjang->statusperiksa = $model->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $model->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        public function saveTindakanPelayanan($modPasien,$model)
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New PJTindakanPelayananT;
            $modTindakanPelayan->penjamin_id=$model->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->kelaspelayanan_id=$model->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id=Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id=$model->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id=$_POST['TindakanPelayananT']['idTindakan'];
            $modTindakanPelayan->carabayar_id=$model->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$model->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan=$_POST['TindakanPelayananT']['tarifSatuan'];
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$model->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            
            if(isset($_POST['TindakanPelayananT']['idKarcis'])){
                $modTindakanPelayan->karcis_id=$_POST['TindakanPelayananT']['idKarcis'];
            }

                $tarifTindakan= TariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                foreach($tarifTindakan AS $dataTarif):
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                            $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                            $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                            $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                        }
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                            $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                        }
                endforeach;
             if($modTindakanPelayan->save()){ 
                    $tindakanKomponen= TariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                    $jumlahKomponen=COUNT($tindakanKomponen);

                    foreach ($tindakanKomponen AS $tampilKomponen):
                            $modTindakanKomponen=new TindakanKomponenT;
                            $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                            $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                            $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                            $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                            $modTindakanKomponen->tarifcyto_tindakankomp=0;
                            $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                            $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                            $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                            $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                            if($modTindakanKomponen->save()){
                                $cekTindakanKomponen++;
                            }
                    endforeach;
                    if($cekTindakanKomponen!=$jumlahKomponen){
                           $this->successSaveTindakanKomponen=false;
                        }
            } 
            
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new PJRujukanT;
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d') : $attrRujukan['tanggal_rujukan'] ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modRujukan->tanggal_rujukan = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modRujukan->tanggal_rujukan, 'yyyy-MM-dd'),'medium',null);
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function savePasien($attrPasien)
        {
            $modPasien = new PJPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedik(Yii::app()->user->getState('mr_jenazah'));
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = Params::statusRM(1);
            $modPasien->ispasienluar = FALSE;
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function saveUbahCaraBayar($model) 
        {
            $modUbahCaraBayar = new PJUbahCaraBayarR;
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
            
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}