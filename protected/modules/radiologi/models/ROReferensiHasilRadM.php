<?php

class ROReferensiHasilRadM extends ReferensihasilradM
{
    public $refhasilrad_id;
    public $pemeriksaanrad_nama,$pemeriksaanrad_namalainnya,$nama_pegawai,$pemeriksaanrad_jenis;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    public function criteriaSearch(){
        $criteria = new CDbCriteria();
        $criteria->compare('refhasilrad_id',$this->refhasilrad_id);
        $criteria->compare('pemeriksaanrad_id',$this->pemeriksaanrad_id);
        $criteria->compare('LOWER(refhasilrad_kode)',strtolower($this->refhasilrad_kode),true);
        $criteria->compare('LOWER(refhasilrad_hasil)',strtolower($this->refhasilrad_hasil),true);
        $criteria->compare('LOWER(refhasilrad_kesan)',strtolower($this->refhasilrad_kesan),true);
        $criteria->compare('LOWER(refhasilrad_kesimpulan)',strtolower($this->refhasilrad_kesimpulan),true);
        $criteria->compare('LOWER(refhasilrad_keterangan)',strtolower($this->refhasilrad_keterangan),true);
        $criteria->compare('refhasilrad_aktif',TRUE);
        $criteria->compare('pegawai_id',$this->pegawai_id);
        return $criteria;
    }
    public function searchDialog(){
        $criteria = $this->criteriaSearch();
//        $criteria->addCondition('pegawai_id = '.Yii::app()->user->getState('pegawai_id'));
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
        ));
    }
    public function searchReferensi()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            
            $criteria->with=array('pemeriksaanrad','pegawai');
            $criteria->compare('t.refhasilrad_id',$this->refhasilrad_id);
            $criteria->compare('t.pemeriksaanrad_id',$this->pemeriksaanrad_id);
            $criteria->compare('LOWER(t.refhasilrad_kode)',strtolower($this->refhasilrad_kode),true);
            $criteria->compare('LOWER(t.refhasilrad_hasil)',strtolower($this->refhasilrad_hasil),true);
            $criteria->compare('LOWER(t.refhasilrad_kesan)',strtolower($this->refhasilrad_kesan),true);
            $criteria->compare('LOWER(t.refhasilrad_kesimpulan)',strtolower($this->refhasilrad_kesimpulan),true);
            $criteria->compare('LOWER(t.refhasilrad_keterangan)',strtolower($this->refhasilrad_keterangan),true);
            $criteria->compare('LOWER(pemeriksaanrad.pemeriksaanrad_nama)',strtolower($this->pemeriksaanrad_nama),true);
            $criteria->compare('LOWER(pemeriksaanrad.pemeriksaanrad_namalainnya)',strtolower($this->pemeriksaanrad_namalainnya),true);
            $criteria->compare('LOWER(pegawai.nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('t.refhasilrad_aktif',isset($this->refhasilrad_aktif)?$this->refhasilrad_aktif:true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public function searchReferensiPrint()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            
            $criteria->with=array('pemeriksaanrad','pegawai');
            $criteria->compare('t.refhasilrad_id',$this->refhasilrad_id);
            $criteria->compare('t.pemeriksaanrad_id',$this->pemeriksaanrad_id);
            $criteria->compare('LOWER(t.refhasilrad_kode)',strtolower($this->refhasilrad_kode),true);
            $criteria->compare('LOWER(t.refhasilrad_hasil)',strtolower($this->refhasilrad_hasil),true);
            $criteria->compare('LOWER(t.refhasilrad_kesan)',strtolower($this->refhasilrad_kesan),true);
            $criteria->compare('LOWER(t.refhasilrad_kesimpulan)',strtolower($this->refhasilrad_kesimpulan),true);
            $criteria->compare('LOWER(t.refhasilrad_keterangan)',strtolower($this->refhasilrad_keterangan),true);
            $criteria->compare('LOWER(pemeriksaanrad.pemeriksaanrad_nama)',strtolower($this->pemeriksaanrad_nama),true);
            $criteria->compare('LOWER(pemeriksaanrad.pemeriksaanrad_namalainnya)',strtolower($this->pemeriksaanrad_namalainnya),true);
            $criteria->compare('LOWER(pegawai.nama_pegawai)',strtolower($this->nama_pegawai),true);
            $criteria->compare('t.refhasilrad_aktif',isset($this->refhasilrad_aktif)?$this->refhasilrad_aktif:true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->limit = -1;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public function getPemeriksaanRad($pemeriksaanrad_jenis)
    {
        return PemeriksaanradM::model()->findAllByAttributes(array('pemeriksaanrad_jenis'=>$pemeriksaanrad_jenis,'pemeriksaanrad_aktif' => TRUE),array('order'=>'pemeriksaanrad_nama asc'));
    }
        
}
?>