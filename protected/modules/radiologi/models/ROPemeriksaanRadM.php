<?php

class ROPemeriksaanRadM extends PemeriksaanradM
{
    public $refhasilrad_id;
    public $isChecked = false; //status jika di pilih
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return AgamaM the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function getDaftarTindakanNama($daftartindakanId = null) {
        return DaftartindakanM::model()->findByPk($daftartindakanId);
    }
}
?>