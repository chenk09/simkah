<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ROPasienMasukPenunjangV extends PasienmasukpenunjangV
{
    // public $statusperiksahasil;
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return KelompokmenuK the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchRAD()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
            // $criteria->compare('LOWER(hasilpemeriksaanlab_t.statusperiksahasil)',strtolower($this->statusperiksahasil),true);
            $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
//            $criteria->compare('LOWER(nama_bin )',strtolower($this->nama),true);
//            $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
            $batal = "BATAL PERIKSA";
            $criteria->addCondition('statusperiksa <> \''.$batal.'\'');
            $criteria->addBetweenCondition('t.tglmasukpenunjang', $this->tglAwal, $this->tglAkhir);


//                $criteria->with=array('pasien','jeniskasuspenyakit','pendaftaran','jeniskasuspenyakit','pegawai','kelaspelayanan','ruangan','pasienadmisi','ruanganasal');
            $criteria->order = "t.tglmasukpenunjang DESC"; //tgl masuk penunjang = tgl pendaftaran rad
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
        public function getNamaLengkapDokter($pegawai_id)
        {
            $dokter = DokterV::model()->findByAttributes(array('pegawai_id'=>$pegawai_id));
            return $dokter->gelardepan." ".$dokter->nama_pegawai.", ".$dokter->gelarbelakang_nama;
        }
        
        public function getNamaPegawai($pegawai_id)
        {
            $dokter = PegawaiM::model()->findByAttributes(
                array('pegawai_id'=>$pegawai_id)
            );
            return $dokter->nama_pegawai;
        }
}
?>
