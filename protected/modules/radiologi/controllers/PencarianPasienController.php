<?php

class PencarianPasienController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new ROInfoKunjunganRJV;
                $modRI = new ROInfoKunjunganRIV;
                $modRD = new ROInfoKunjunganRDV;
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
//                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRJ->tglAkhir =date('Y-m-d H:i:s');
//                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRI->tglAkhir = date('Y-m-d H:i:s');
//                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
//                $modRD->tglAkhir =date('Y-m-d H:i:s');
                
                $modRJ->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRJ->tglAkhir = date('d M Y H:i:s');
                $modRI->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRI->tglAkhir = date('d M Y H:i:s');
                $modRD->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modRD->tglAkhir = date('d M Y H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['ROInfoKunjunganRJV'];
                            if(!empty($_POST['ROInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['ROInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['ROInfoKunjunganRIV'];
                            if(!empty($_POST['ROInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['ROInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['ROInfoKunjunganRDV'];
                            if(!empty($_POST['ROInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['ROInfoKunjunganRDV
                                ']['tglAwal']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                                 
                ));
        }
}