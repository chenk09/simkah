<?php

class PendaftaranPeriksaController extends SBaseController
{
        public $successSave = false;
        public $successSaveTindakan = true;
        
	public function actionIndex()
	{
            
            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeunitLain'];
            $modPendaftaran = new ROPendaftaranT;
            $modPasien = new ROPasienM;
            $modPeriksaRad = ROPemeriksaanRadM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true));
            $modPermintaan = ROPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            if(isset($_POST['permintaanPenunjang'])) {
                $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeunitLain'];
                $idPendaftaran = $_POST['ROPendaftaranT']['pendaftaran_id'];
                $modPendaftaran = ROPendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = ROPasienM::model()->findByPk($modPendaftaran->pasien_id);
                //$this->redirect('/radiologi/RujukanPenunjang');
//               echo "<pre>".print_r($_POST['permintaanPenunjang'],1)."</pre>";
                $transaction = Yii::app()->db->beginTransaction();
                try {
                        $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeunitLain'];
                        $this->savePasienPenunjang($modPendaftaran);
                    
                    if ($this->successSave && $this->successSaveTindakan){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect(array('PendaftaranPeriksa/index','id'=>1));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $modRiwayatKirimKeUnitLain = ROPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                    'ruangan_id'=>Params::RUANGAN_ID_RAD,),
                                                                                                    'pasienmasukpenunjang_id IS NULL');
            $modRiwayatPenunjang = ROPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                             'ruangan_id'=>Params::RUANGAN_ID_RAD,));
            
            $this->render('index',array('modPermintaan'=>$modPermintaan,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modPeriksaRad'=>$modPeriksaRad,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRiwayatPenunjang'=>$modRiwayatPenunjang));
	}
        
        /**
         * Fungsi untuk menyimpan data ke model ROPasienMasukPenunjangT
         * @param type $modPendaftaran
         * @param type $modPasien
         * @return ROPasienMasukPenunjangT 
         */
        protected function savePasienPenunjang($modPendaftaran){
            
            $modPasienPenunjang = new ROPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_RAD;   //$modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RO');
            $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d H:i:s');    //$modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            
            
//                        echo "<pre>";
//                        echo print_r($modPasienPenunjang->getAttributes());
//                        echo "</pre>";
                        
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
                $this->saveTindakanPelayanan($modPendaftaran, $modPasienPenunjang);
                $this->updatePasienKirimKeUnitLain($modPasienPenunjang);
            } else {
                $this->successSave = false;
            }
            
            return $modPasienPenunjang;
        }
        
        /**
         * Fungsi untuk menyimpan data ke model TindakanpelayananT
         * @param type $modPendaftaran
         * @param type $modPasienPenunjang
         * @return TindakanpelayananT 
         */
        protected function saveTindakanPelayanan($modPendaftaran,$modPasienPenunjang)
        {
            $valid=true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPendaftaran->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = Params::INSTALASI_ID_RAD; //$modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;

                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $valid = $modTindakans[$i]->validate() && $valid;
                        
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                }
            }
            
            if($valid){
                foreach($modTindakans as $i=>$tindakan){
                    $tindakan->save();
                    $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                }
                if($statusSaveKomponen && $statusSaveHasilPeriksa) {
                    $this->successSaveTindakan = $this->successSaveTindakan && true;
                } else {
                    $this->successSaveTindakan = false;
                }
            } else {
                $this->successSaveTindakan = false;
            }
            
            return $modTindakans;
        }

        public function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        /**
         *
         * @param type $tindakan
         * @return type 
         */
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }
        
        /**
         *
         * @param type $tindakan
         * @param type $idPemeriksaanRad
         * @return type 
         */
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad)
        {
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $valid = $modHasil->validate() && $valid;
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
            
            return $valid;
        }
        
        /**
         *
         * @param type $modPasienPenunjang 
         */
        protected function updatePasienKirimKeUnitLain($modPasienPenunjang) {
            ROPasienKirimKeUnitLainT::model()->updateByPk($idPasienKirimKeUnitLain, 
                    array('pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id));
        }
}