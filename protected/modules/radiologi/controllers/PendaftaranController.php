<?php

class PendaftaranController extends SBaseController
{
        public $defaultAction = 'radiologi';
        public $successSave = false;
        public $successSaveTindakan = true;
        public $successSaveRujukan = true; //variabel untuk validasi data opsional (rujukan)
        public $successSavePJ = true; //variabel untuk validasi data opsional (penanggung jawab)
//        public $successSavePJ = '';

        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('radiologi'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
        public function savePasien($attrPasien)
        {
            $modPasien = new ROPasienM;
            $modPasien->attributes = $attrPasien;
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            $modPasien->no_rekam_medik = Generator::noRekamMedikPenunjang(Params::singkatanNoPendaftaranRad());
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->profilrs_id = Params::DEFAULT_PROFIL_RUMAH_SAKIT;
            $modPasien->statusrekammedis = 'AKTIF';
            $modPasien->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modPasien->ispasienluar = 1;
            
            if($modPasien->validate()) {
                // form inputs are valid, do something here
                $modPasien->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasien->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasien->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            return $modPasien;
        }
        
        public function updatePasien($modPasien,$attrPasien)
        {
            $modPasienupdate = $modPasien;
            $modPasienupdate->attributes = $attrPasien;
            $modPasien->ispasienluar = 1;
            $modPasienupdate->kelompokumur_id = Generator::kelompokUmur($modPasienupdate->tanggal_lahir);
            
            if($modPasienupdate->validate()) {
                // form inputs are valid, do something here
                $modPasienupdate->save();
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $modPasienupdate->tanggal_lahir = Yii::app()->dateFormatter->formatDateTime(
                                                                CDateTimeParser::parse($modPasienupdate->tanggal_lahir, 'yyyy-MM-dd'),'medium',null);
            }
            
            return $modPasienupdate;
        }
        
        public function savePenanggungJawab($attrPenanggungJawab)
        {
            $modPenanggungJawab = new ROPenanggungJawabM;
            $modPenanggungJawab->attributes = $attrPenanggungJawab;
            if($modPenanggungJawab->validate()) {
                // form inputs are valid, do something here
                $modPenanggungJawab->save();
                $this->successSavePJ = TRUE;
            } else {
                // mengembalikan format tanggal contoh 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                //if($modPenanggungJawab->tgllahir_pj != null)
                //$modPenanggungJawab->tgllahir_pj = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenanggungJawab->tgllahir_pj, 'yyyy-MM-dd'),'medium',null);
                $this->successSavePJ = FALSE;
            }

            return $modPenanggungJawab;
        }
        
        public function saveRujukan($attrRujukan)
        {
            $modRujukan = new RORujukanT;
            $format = new CustomFormat();
            $modRujukan->attributes = $attrRujukan;
            $modRujukan->tanggal_rujukan = ($attrRujukan['tanggal_rujukan'] == '') ? date('Y-m-d H:i:s') : $format->formatDateTimeMediumForDB($attrRujukan['tanggal_rujukan']) ;
            if($modRujukan->validate()) {
                // form inputs are valid, do something here
                $modRujukan->save();
                $this->successSaveRujukan = TRUE;
            } else {
                // mengembalikan format tanggal 2012-04-10 ke 10 Apr 2012 untuk ditampilkan di form
                $this->successSaveRujukan = FALSE;
            }
            return $modRujukan;
        }
        
        public function saveUbahCaraBayar($model) 
        {
            $modUbahCaraBayar = new ROUbahCaraBayarR;
            $modUbahCaraBayar->pendaftaran_id = $model->pendaftaran_id;
            $modUbahCaraBayar->carabayar_id = $model->carabayar_id;
            $modUbahCaraBayar->penjamin_id = $model->penjamin_id;
            $modUbahCaraBayar->tglubahcarabayar = date('Y-m-d');
            $modUbahCaraBayar->alasanperubahan = 'x';
            if($modUbahCaraBayar->validate())
            {
                // form inputs are valid, do something here
                $modUbahCaraBayar->save();
            }
            
        }
        
        /**
         * Fungsi untuk menyimpan data ke model ROPendaftaranMP
         * @param type $model array object 
         * @param type $modPasien
         * @param type $modRujukan
         * @param type $modPenanggungJawab
         * @return ROPendaftaranMP 
         */
        protected function savePendaftaranMP($model, $modPasien, $modRujukan, $modPenanggungJawab){
            $modelNew = new ROPendaftaranMp;
            $modelNew->attributes = $model->attributes;
            $modelNew->pasien_id = $modPasien->pasien_id;
            $modelNew->penanggungjawab_id = $modPenanggungJawab->penanggungjawab_id;
            $modelNew->rujukan_id = $modRujukan->rujukan_id;
            $modelNew->ruangan_id = Params::RUANGAN_ID_RAD;
            $modelNew->instalasi_id = Generator::getInstalasiIdFromRuanganId($modelNew->ruangan_id);
            $modelNew->no_pendaftaran = Generator::noPendaftaran(Params::singkatanNoPendaftaranRad());
            $modelNew->no_urutantri = Generator::noAntrian($modelNew->no_pendaftaran, $modelNew->ruangan_id);
            $modelNew->golonganumur_id = Generator::golonganUmur($modPasien->tanggal_lahir);
            $modelNew->umur = Generator::hitungUmur($modPasien->tanggal_lahir);
            $modelNew->statuspasien = Generator::getStatusPasien($modPasien);
            $modelNew->statusperiksa = Params::statusPeriksa(1);
            $modelNew->kunjungan = Generator::getKunjungan($modPasien, $modelNew->ruangan_id);
            $modelNew->shift_id = Yii::app()->user->getState('shift_id');
            $modelNew->kelompokumur_id = $modPasien->kelompokumur_id;
            $modelNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelNew->statusmasuk = Params::statusMasuk('rujukan');
            $modelNew->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);
            
            if ($modelNew->validate()){
                $modelNew->Save();
                $this->successSave = true;
            }else{
                $modelNew->tgl_pendaftaran = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modelNew->tgl_pendaftaran, 'yyyy-MM-dd'), 'medium', null);
            }
            $modelNew->noRekamMedik = $modPasien->no_rekam_medik;
            
            return $modelNew;
        }
        /**
         * Fungsi action untuk proses pendaftaran penunjang radiologi
         */
        public function actionRadiologi() {
            
            $this->pageTitle = Yii::app()->name." - Pendaftaran Pasien Radiologi";
            $model=new ROPendaftaranMp;
            $model->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $model->ruangan_id = Params::RUANGAN_ID_RAD;
            $model->tgl_pendaftaran = date('d M Y H:i:s');
            $model->umur = "00 Thn 00 Bln 00 Hr";
            $modPasien = new ROPasienM;
            $modPasien->tanggal_lahir = date('d M Y');
            $modPenanggungJawab = new ROPenanggungJawabM;
            $modRujukan = new RORujukanT;
            $modPasienPenunjang = new ROPasienMasukPenunjangT;  
            $modPeriksaRad = PemeriksaanradM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true));
            
            if (isset($_POST['ROPendaftaranMp']) && isset($_POST['permintaanPenunjang'])){
                $model->attributes = $_POST['ROPendaftaranMp'];
                $modPasien->attributes = $_POST['ROPasienM'];
                $modPenanggungJawab->attributes = $_POST['ROPenanggungJawabM'];
                $modRujukan->attributes = $_POST['RORujukanT'];
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    if(!isset($_POST['isPasienLama'])){
                        $modPasien = $this->savePasien($_POST['ROPasienM']);
                    }
                    else{
                        $model->isPasienLama = true;
                        if (!empty($_POST['noRekamMedik'])){
                            $model->noRekamMedik = $_POST['noRekamMedik'];
                            $modPasien = ROPasienM::model()->findByAttributes(array('no_rekam_medik'=>$model->noRekamMedik));
                            if(!empty($modPasien) && isset($_POST['isUpdatePasien'])) {
                                $modPasien = $this->updatePasien($modPasien, $_POST['ROPasienM']);
                            }
                        }else{
                             $model->addError('noRekamMedik', 'no rekam medik belum dipilih');
                             Yii::app()->user->setFlash('danger',"Data pasien masih kosong, Anda belum memilih no rekam medik.");
                        }
                    }
                    
                    if (count($modPasien) != 1){
                        $modPasien = New ROPasienM;
                        $this->successSave = false;
                        $model->addError('noRekamMedik', 'Pasien tidak ditemukan. Masukkan No Rekam Medik dengan benar.');
                        Yii::app()->user->setFlash('warning',"Data Pasien tidak ditemukan isi Nomor Rekam Medik dengan benar");
                    }
                    
                    $modRujukan = $this->saveRujukan($_POST['RORujukanT']);
                    
                    if(isset($_POST['adaPenanggungJawab'])) {
                        $model->adaPenanggungJawab = true;
                        $modPenanggungJawab = $this->savePenanggungJawab($_POST['ROPenanggungJawabM']);
                    }
                    
                    $model = $this->savePendaftaranMP($model,$modPasien,$modRujukan,$modPenanggungJawab);
                    
                    $modPasienPenunjang = $this->savePasienPenunjang($model,$modPasien);
                    
                    $this->saveTindakanPelayanan($modPasien, $model, $modPasienPenunjang);
                                        
                    $this->saveUbahCaraBayar($model);
                    
                    if ($this->successSave && $this->successSaveRujukan && $this->successSaveTindakan){
                        $transaction->commit();
                        //$transaction->rollback();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        $model->isNewRecord = true;
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }
                catch(Exception $exc){
                    $transaction->rollback();
                    $model->isNewRecord = true;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $arrInputPemeriksaan = array();
            if(isset($_POST['permintaanPenunjang'])) {
                foreach($_POST['permintaanPenunjang'] as $i=>$item) {
                    //echo $i." => ".$item['inputpemeriksaanrad']."<br/>";
                    $arrInputPemeriksaan[$i] = $item;
                }
            }
            
            $model->isRujukan = true;
            $model->isPasienLama = (isset($_POST['isPasienLama'])) ? true : false;
            $model->pakeAsuransi = (isset($_POST['pakeAsuransi'])) ? true : false;
            $model->adaPenanggungJawab = (isset($_POST['adaPenanggungJawab'])) ? true : false;
            $model->adaKarcis = (isset($_POST['karcisTindakan'])) ? true : false;
                       
            $this->render('pendaftaran', array('model'=>$model, 
                                               'modPasien'=>$modPasien, 
                                               'modPenanggungJawab'=>$modPenanggungJawab,
                                               'modRujukan'=>$modRujukan,
                                               'modPasienPenunjang'=>$modPasienPenunjang,
                                               'modPeriksaRad'=>$modPeriksaRad,
                                               'arrInputPemeriksaan'=>$arrInputPemeriksaan,
            ));
        }
        
        /**
         * Fungsi untuk menyimpan data ke model ROPasienMasukPenunjangT
         * @param type $modPendaftaran
         * @param type $modPasien
         * @return ROPasienMasukPenunjangT 
         */
        protected function savePasienPenunjang($modPendaftaran,$modPasien){
            
            $modPasienPenunjang = new ROPasienMasukPenunjangT;
            $modPasienPenunjang->pasien_id = $modPasien->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modPasienPenunjang->ruangan_id = $modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = Generator::noMasukPenunjang('RO');
            $modPasienPenunjang->tglmasukpenunjang = $modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  Generator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $modPasienPenunjang->tglmasukpenunjang = Yii::app()->dateFormatter->formatDateTime(
                        CDateTimeParser::parse($modPasienPenunjang->tglmasukpenunjang, 'yyyy-MM-dd'), 'medium', null);
            }
            
            return $modPasienPenunjang;
        }
        
        
        protected function saveTindakanPelayanan($modPasien,$modPendaftaran,$modPasienPenunjang)
        {
            $valid=true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                    if(isset($item['cbPemeriksaan'])){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');

                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $valid = $modTindakans[$i]->validate() && $valid;
                        
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                    }
                }
            }
            
            if($valid){
                foreach($modTindakans as $i=>$tindakan){
                    $tindakan->save();
                    $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                }
                if($statusSaveKomponen && $statusSaveHasilPeriksa) {
                    $this->successSaveTindakan = $this->successSaveTindakan && true;
                } else {
                    $this->successSaveTindakan = false;
                }
            } else {
                $this->successSaveTindakan = false;
            }
            
            return $modTindakans;
        }

        public function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }
        
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad)
        {
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;

                $valid = $modHasil->validate() && $valid;
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
            
            return $valid;
        }


        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
       
        
}
