<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class DaftarPasienController extends SBaseController
{
        public $successPengambilanSample = false;
        public $successKirimSample = false;
        public $pathView = 'radiologi.views.daftarPasien.';

//    MENGGUNAKAN SRBAC
//    public function accessRules()
//    {
//        return array(
//            array('allow',  // allow all users to perform 'index' and 'view' actions
//                'actions'=>array(),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow authenticated user to perform 'create' and 'update' actions
//                'actions'=>array('index','hasilPemeriksaan','print','ubahPasien'),
//                'users'=>array('@'),
//            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions'=>array('BatalPeriksaPasienLuar'),
//                'users'=>array('@'),
//            ),
//            array('deny',  // deny all users
//                'users'=>array('*'),
//            ),
//        );
//    }       
        
	public function actionIndex()
	{
                $modPasienMasukPenunjang = new ROPasienMasukPenunjangV;
                $format = new CustomFormat();
//                $modPasienMasukPenunjang->tglAwal = date('d M Y').' 00:00:00';
                $modPasienMasukPenunjang->tglAwal = date('d M Y', strtotime('-5 days')).' 00:00:00';
                $modPasienMasukPenunjang->tglAkhir = date('d M Y H:i:s');
                if(isset($_GET['ROPasienMasukPenunjangV'])){
                    $modPasienMasukPenunjang->attributes = $_GET['ROPasienMasukPenunjangV'];
                    $modPasienMasukPenunjang->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROPasienMasukPenunjangV']['tglAwal']);
                    $modPasienMasukPenunjang->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROPasienMasukPenunjangV']['tglAkhir']);
                }
		$this->render('index',array('modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
	}
              
        
        public function actionHasilPemeriksaan($idPendaftaran,$idPasien,$idPasienMasukPenunjang)
        {
			$enableprint = true;
            $modPasienMasukPenunjang = ROPasienMasukPenunjangV::model()->findByAttributes(
                array(
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
                )
            );
            $modPendaftaran = ROPendaftaranMp::model()->findByPk($idPendaftaran);
            $modRujukan = RORujukanT::model()->findByPk($modPendaftaran->rujukan_id);
            
            if(isset($_POST['RORujukanT']))
            { // Update Dokter Perujuk pada RujukanT
                $modRujukan->rujukandari_id = $_POST['RORujukanT']['rujukandari_id'];
                $modRujukanDari = RujukandariM::model()->findByPk($modRujukanT->rujukandari_id);
                $modRujukan->nama_perujuk = $modRujukanDari->namaperujuk;
                $modRujukan->update();
            }
            
            if(isset($_POST['ROHasilPemeriksaanRadT']))
            {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $this->saveHasilPemeriksaan($_POST['ROHasilPemeriksaanRadT']);
                
                    //Update dokter pemeriksa (pegawai_id) pada pasien masuk penunjang
                    ROPasienMasukPenunjangT::model()->updateByPk(
                        $idPasienMasukPenunjang,
                        array(
                            'pegawai_id'=>$_POST['ROPasienMasukPenunjangT']['pegawai_id']
                        )
                    );
                        
					
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
//                    $this->redirect(array(
////                        'index', 
//						'hasilPemeriksaan',
//                        'idPendaftaran'=>$idPendaftaran,
//                        'idPasien'=>$idPasien,
//                        'idPasienMasukPenunjang'=>$idPasienMasukPenunjang
//                    ));
//                    $this->redirect($this->createUrl("/radiologi/lihatHasil/HasilPeriksa", array('idPendaftaran'=>$idPendaftaran,'idPasien'=>$idPasien,'idPasienMasukPenunjang'=>$idPasienMasukPenunjang,'caraPrint'=>'PRINT')));
					$this->redirect(array('hasilPemeriksaan','idPendaftaran'=>$idPendaftaran,'idPasien'=>$idPasien,'idPasienMasukPenunjang'=>$idPasienMasukPenunjang,'sukses'=>1));
                } catch(Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $modHasilpemeriksaanRad = ROHasilPemeriksaanRadT::model()->with('pemeriksaanrad')->findAllByAttributes(
                array(
                    'pendaftaran_id'=>$idPendaftaran,
                    'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang,
                    'pasien_id'=>$idPasien
                )
            );
            
            
            if(empty($modHasilpemeriksaanRad))
            {
                $this->redirect(
                    $this->createUrl('/radiologi/inputPemeriksaan',
                        array(
                            'idPendaftaran'=>$idPendaftaran,
                            'idPasien'=>$idPasien,
                            'idPasienMasukPenunjang'=>$idPasienMasukPenunjang
                        )
                    )
                );
            }
            
			// Kondisi Print FALSE (Jika pasien berasal dari pendaftaran luar yang belum bayar) ATAU (jika pasien rawat jalan yang carabayarnya umum yang belum bayar)
			if(empty($modPendaftaran->pembayaranpelayanan_id)){
				if(($modPendaftaran->pasien->ispasienluar) || (($modPendaftaran->instalasi_id == Params::INSTALASI_ID_RJ)&&($modPendaftaran->carabayar_id == Params::DEFAULT_CARABAYAR))){
					$enableprint = false;
				}
			}
			
            $this->render('hasilPemeriksaan',
                array(
                    'modHasilpemeriksaanRad'=>$modHasilpemeriksaanRad,
                    'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    'modPendaftaran'=>$modPendaftaran,
                    'modRujukan'=>$modRujukan,
					'enableprint'=>$enableprint
                )
            );

        }
	
        protected function saveHasilPemeriksaan($arrHasil)
        {
            $format = new CustomFormat();
            $tglpegambilanhasilrad = $format->formatDateTimeMediumForDB($arrHasil[0]['tglpegambilanhasilrad']);
            if(trim($tglpegambilanhasilrad)=='') $tglpegambilanhasilrad = null;
            
            foreach($arrHasil as $i => $hasil) {
                ROHasilPemeriksaanRadT::model()->updateByPk($hasil['hasilpemeriksaanrad_id'], 
                                                            array('hasilexpertise'=>$hasil['hasilexpertise'],
                                                                  'kesan_hasilrad'=>$hasil['kesan_hasilrad'],
                                                                  'kesimpulan_hasilrad'=>$hasil['kesimpulan_hasilrad'],
                                                                  'tglpegambilanhasilrad'=>$tglpegambilanhasilrad,));
            }
        }
        
        public function actionUbahPasien($id,$idpendaftaran)
	{
            Yii::import('laboratorium.models.LKPasienM');
            Yii::import('laboratorium.models.LKPendaftaranT');
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $model = LKPasienM::model()->findByPk($id);
                $modPendaftaran = LKPendaftaranT::model()->findByPk($idpendaftaran);     
                $format = new CustomFormat();
                $temLogo=$model->photopasien;
                $model->update_time = date ('Y-m-d');
                $model->thn = substr($modPendaftaran->umur,0,2);
                $model->bln = substr($modPendaftaran->umur,7,2);
                $model->hr = substr($modPendaftaran->umur,14,2);
                $model->update_loginpemakai_id = Yii::app()->user->id;
                $model->tgl_rekam_medik = $format->formatDateMediumForUser($model->tgl_rekam_medik);
                $model->tgl_rekam_medik = date('d M Y',strtotime($model->tgl_rekam_medik));
                if(isset($_POST['LKPasienM'])) {                   
                    $random=rand(0000000,9999999);
                    $model->attributes = $_POST['LKPasienM'];
                    $model->umur = $_POST['LKPasienM']['thn']." Thn ".$_POST['LKPasienM']['bln']." Bln ".$_POST['LKPasienM']['hr']." Hr ";
                    $model->tanggal_lahir = $format->formatDateMediumForDB($model->tanggal_lahir);
                    $model->kelompokumur_id = Generator::kelompokUmur($model->tanggal_lahir);
                    $model->photopasien = CUploadedFile::getInstance($model, 'photopasien');
                    $gambar=$model->photopasien;
                    $model->tgl_rekam_medik  = $format->formatDateMediumForDB($model->tgl_rekam_medik);
                    if(!empty($model->photopasien)) { //if user input the photo of patient
                        $model->photopasien =$random.$model->photopasien;

                         Yii::import("ext.EPhpThumb.EPhpThumb");

                         $thumb=new EPhpThumb();
                         $thumb->init(); //this is needed

                         $fullImgName =$model->photopasien;   
                         $fullImgSource = Params::pathPasienDirectory().$fullImgName;
                         $fullThumbSource = Params::pathPasienTumbsDirectory().'kecil_'.$fullImgName;

                         if($model->save()) {
                            if(!empty($temLogo)) { 
                               if(file_exists(Params::pathPasienDirectory().$temLogo))
                                    unlink(Params::pathPasienDirectory().$temLogo);
                               if(file_exists(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo))
                                    unlink(Params::pathPasienTumbsDirectory().'kecil_'.$temLogo);
                            }
                            $gambar->saveAs($fullImgSource);
                            $thumb->create($fullImgSource)
                                 ->resize(200,200)
                                 ->save($fullThumbSource);
                            LKPendaftaranT::model()->updateByPk($idpendaftaran,  array('umur'=>$model->umur));
//                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//                            $this->redirect(array('cariPasien'));
                          } else {
                              LKPendaftaranT::model()->updateByPk($idpendaftaran,  array('umur'=>$model->umur));
                               Yii::app()->user->setFlash('error', 'Data <strong>Gagal!</strong>  disimpan.');
                          }
                    } else { //if user not input the photo
                       $model->photopasien=$temLogo;
                       if($model->save()) {
                            LKPendaftaranT::model()->updateByPk($idpendaftaran,  array('umur'=>$model->umur));
//                            $model->updateByPk($id, array('tgl_rekam_medik'=>$model->tgl_rekam_medik));
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//                            $this->redirect(array('cariPasien'));
                       }
                    }

                }
		$this->render('laboratorium.views.daftarPasien.ubahPasien',array('model'=>$model));
	}
        
    public function actionBatalPemeriksaan()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $transaction = Yii::app()->db->beginTransaction();
            $pesan = 'success';
            $status = 'ok';

            try{
                $idPendaftaran = $_POST['idPendaftaran'];
                $idPenunjang = $_POST['idPenunjang'];

                /*
                 * cek data pendaftaran &  pasien masuk penunjang
                 */
                $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByAttributes(
                    array(
                        'pasienmasukpenunjang_id'=>$idPenunjang,
                        'ruangan_id'=>PARAMS::RUANGAN_ID_RAD
                    )
                );
                
                if(count($pasienMasukPenunjang) > 0){
                    $pendaftaran = PendaftaranT::model()->findByPk($pasienMasukPenunjang->pendaftaran_id);
                    $pendaftaran_id = $pasienMasukPenunjang->pendaftaran_id;
                    $pasien_id = $pasienMasukPenunjang->pasien_id;
                    $pasienmasukpenunjang_id = $pasienMasukPenunjang->pasienmasukpenunjang_id;
                    $pasienkirimkeunitlain_id = $pasienMasukPenunjang->pasienkirimkeunitlain_id;
                }else{
                    $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                    $pendaftaran_id = $pendaftaran->pendaftaran_id;
                    $pasien_id = $pendaftaran->pasien_id;
                    $pasienmasukpenunjang_id = null;
                    $pasienkirimkeunitlain_id = null;
                }
//                echo $pasienMasukPenunjang->pendaftaran_id.'<br/>';
//                echo $pasienMasukPenunjang->pasien_id.'<br/>';
//                echo $pasienMasukPenunjang->pasienmasukpenunjang_id;exit;
                /** end cek data pendaftaran & pasien masuk penunjang **/

                                   


                if(empty($pasienMasukPenunjang->pasienkirimkeunitlain_id))
                {
                    $model = new PasienbatalperiksaR();
                    $model->pendaftaran_id = $pendaftaran_id;
                    $model->pasien_id = $pasien_id;
                    $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                    $model->tglbatal = date('Y-m-d');
                    $model->keterangan_batal = "Batal Radiologi";
                    $model->create_time = date('Y-m-d H:i:s');
                    $model->update_time = null;
                    $model->create_loginpemakai_id = Yii::app()->user->id;
                    $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                    if(!$model->save())
                    {
                        $pesan = 'exist';
                        $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                    }

                    if(empty($pendaftaran->pembayaranpelayanan_id)){
                        $attributes = array(
                            'pasienbatalperiksa_id' => $model->pasienbatalperiksa_id,
                            'update_time' => date('Y-m-d H:i:s'),
                            'update_loginpemakai_id' => Yii::app()->user->id
                        );
                        $pendaftaran = PendaftaranT::model()->updateByPk($idPendaftaran, $attributes);

                        $attributes = array(
                            'pasienkirimkeunitlain_id' => $pasienMasukPenunjang->pasienkirimkeunitlain_id
                        );
                        $Perminataan_penunjang = PermintaankepenunjangT::model()->deleteAllByAttributes($attributes);
                        $pesan = 'success';
                        $status = 'ok';
                        $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                    }else{
                        $pesan = 'exist';
                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                        </b> sudah melakukan pembayaran pemeriksaan </div>";

                    }  
                }else{
                     /*
                     * cek data tindakan_pelayanan
                     */
                    $attributes = array(
                        'pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id
                    );
//                        echo $pasienMasukPenunjang->pasienmasukpenunjang_id;exit;
                    $tindakan = TindakanpelayananT::model()->findAllByAttributes($attributes);
                    if(count($tindakan) > 0)
                    {
                        if(empty($tindakan->tindakansudahbayar_id)){
                            $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                    if(empty($pendaftaran->pembayaranpelayanan_id)){
                                            $model = new PasienbatalperiksaR();
                                            $model->pendaftaran_id = $pendaftaran_id;
                                            $model->pasien_id = $pasien_id;
                                            $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                            $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                            $model->tglbatal = date('Y-m-d');
                                            $model->keterangan_batal = "Batal Radiologi";
                                            $model->create_time = date('Y-m-d H:i:s');
                                            $model->update_time = null;
                                            $model->create_loginpemakai_id = Yii::app()->user->id;
                                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                            if(!$model->save())
                                            {
                                                $status = 'not';
                                                $pesan = 'exist';
                                                $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                            }
                                            $attributes = array(
                                                'statusperiksa' => 'BATAL PERIKSA',
                                                'update_time' => date('Y-m-d H:i:s'),
                                                'update_loginpemakai_id' => Yii::app()->user->id
                                            );
                                            $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);

                                            $pesan = 'success';
                                            $status = 'ok';
                                            $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                    }else{
                                        $pesan = 'exist';
                                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                        </b> sudah melakukan pembayaran pemeriksaan </div>";
                                    }                      
                        }else{
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                        }
                    }else{
                        if(empty($tindakan->tindakansudahbayar_id)){
                            $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                        $model = new PasienbatalperiksaR();
                                        $model->pendaftaran_id = $pendaftaran_id;
                                        $model->pasien_id = $pasien_id;
                                        $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                        $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                        $model->tglbatal = date('Y-m-d');
                                        $model->keterangan_batal = "Batal Radiologi";
                                        $model->create_time = date('Y-m-d H:i:s');
                                        $model->update_time = null;
                                        $model->create_loginpemakai_id = Yii::app()->user->id;
                                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                        if(!$model->save())
                                        {
                                            $status = 'ok';
                                            $pesan = 'exist';
                                            $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                        }

                                        if(empty($pendaftaran->pembayaranpelayanan_id)){
                                            $attributes = array(
                                                'statusperiksa' => 'BATAL PERIKSA',
                                                'update_time' => date('Y-m-d H:i:s'),
                                                'update_loginpemakai_id' => Yii::app()->user->id
                                            );
                                            $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);

                                             $pesan = 'success';
                                             $status = 'ok';
                                             $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                    }else{
                                        $pesan = 'exist';
                                        $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                        </b> sudah melakukan pembayaran pemeriksaan </div>";
                                    }                      
                        }else{
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                        }
                    }
                }

                /*
                 * kondisi_commit
                 */
                if($status == 'ok')
                {
                    $transaction->commit();
                }else{
                    $transaction->rollback();
                }   
            }catch(Exception $ex){
                print_r($ex);
                $status = 'not';
                $transaction->rollback();
            }

            $data['pesan'] = $pesan;
            $data['status'] = $status;
            $data['keterangan'] = $keterangan;

            echo json_encode($data);
            Yii::app()->end();
        }
    }

//Jangan di hapus copy dari lab
        public function actionBatalPeriksaPasienLuar()
        {
            if(Yii::app()->request->isAjaxRequest)
            {
                $transaction = Yii::app()->db->beginTransaction();
                $pesan = 'success';
                $status = 'ok';

                try{
                    $idPendaftaran = $_POST['idPendaftaran'];
                    $idPenunjang = $_POST['idPenunjang'];

                    /*
                     * cek data pendaftaran &  pasien masuk penunjang
                     */
                    $pasienMasukPenunjang = PasienmasukpenunjangT::model()->findByAttributes(
                        array(
                            'pasienmasukpenunjang_id'=>$idPenunjang,
                            'ruangan_id'=>PARAMS::RUANGAN_ID_RAD
                        )
                    );

                    if(count($pasienMasukPenunjang) > 0){
                        // echo "A";exit();
                        $pendaftaran = PendaftaranT::model()->findByPk($pasienMasukPenunjang->pendaftaran_id);
                        $pendaftaran_id = $pasienMasukPenunjang->pendaftaran_id;
                        $pasien_id = $pasienMasukPenunjang->pasien_id;
                        $pasienmasukpenunjang_id = $pasienMasukPenunjang->pasienmasukpenunjang_id;
                        $pasienkirimkeunitlain_id = $pasienMasukPenunjang->pasienkirimkeunitlain_id;
                    }else{
                        // echo "B";exit();
                        $pendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                        $pendaftaran_id = $pendaftaran->pendaftaran_id;
                        $pasien_id = $pendaftaran->pasien_id;
                        $pasienmasukpenunjang_id = null;
                        $pasienkirimkeunitlain_id = $idKirimUnit;
                    }
    //                echo $pasienMasukPenunjang->pendaftaran_id.'<br/>';
    //                echo $pasienMasukPenunjang->pasien_id.'<br/>';
    //                echo $pasienMasukPenunjang->pasienmasukpenunjang_id;exit;
                    /** end cek data pendaftaran & pasien masuk penunjang **/
                    // print_r($pasienMasukPenunjang->pasienkirimkeunitlain_id);
                    // exit();

                    
               

                    if(empty($pasienMasukPenunjang->pasienkirimkeunitlain_id))
                    {
                        // echo "kosong";exit();
                        $model = new PasienbatalperiksaR();
                        $model->pendaftaran_id = $pendaftaran_id;
                        $model->pasien_id = $pasien_id;
                        $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                        $model->tglbatal = date('Y-m-d');
                        $model->keterangan_batal = "Batal Radiologi";
                        $model->create_time = date('Y-m-d H:i:s');
                        $model->update_time = null;
                        $model->create_loginpemakai_id = Yii::app()->user->id;
                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                        if(!$model->save())
                        {
                            // echo "D";exit();
                            $status = 'not';
                            $pesan = 'exist';
                            $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                        }

                        if(empty($pendaftaran->pembayaranpelayanan_id)){
                            // echo "E";exit();
                            $attributes = array(
                                'pasienbatalperiksa_id' => $model->pasienbatalperiksa_id,
                                'update_time' => date('Y-m-d H:i:s'),
                                'update_loginpemakai_id' => Yii::app()->user->id
                            );
                            $pendaftaran = PendaftaranT::model()->updateByPk($idPendaftaran, $attributes);

                            $attributes = array('pasienkirimkeunitlain_id' => $pasienMasukPenunjang->pasienkirimkeunitlain_id);
                            $Perminataan_penunjang = PermintaankepenunjangT::model()->deleteAllByAttributes($attributes);
                            $pesan = 'success';
                            $status = 'ok';
                            $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                        }else{
                            // echo "F";exit();
                            $pesan = 'exist';
                            $status ='ok';
                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                            </b> sudah melakukan pembayaran pemeriksaan </div>";

                        }  
                    }else
                    {
                        // echo "G";exit();
                         /*
                         * cek data tindakan_pelayanan
                         */
                        $tindakan = TindakanpelayananT::model()->findAllByAttributes(array('pasienmasukpenunjang_id' => $pasienMasukPenunjang->pasienmasukpenunjang_id));
    
                        // echo (count($tindakan));exit();

                        if(count($tindakan) > 0)
                        {
                            // echo "H";exit();
                            if(empty($tindakan->tindakansudahbayar_id)){
                                // echo "I";exit();
                                $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                            if(empty($pendaftaran->pembayaranpelayanan_id)){
                                                // echo "J";exit();
                                                $model = new PasienbatalperiksaR();
                                                $model->pendaftaran_id = $pendaftaran_id;
                                                $model->pasien_id = $pasien_id;
                                                $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                                $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                                $model->tglbatal = date('Y-m-d');
                                                $model->keterangan_batal = "Batal Radiologi";
                                                $model->create_time = date('Y-m-d H:i:s');
                                                $model->update_time = null;
                                                $model->create_loginpemakai_id = Yii::app()->user->id;
                                                $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                                if(!$model->save()){
                                                    // echo "K";exit();
                                                    $status = 'not';
                                                    $pesan = 'exist';
                                                    $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                                } 

                                                $attributes = array(
                                                    'statusperiksa' => 'BATAL PERIKSA',
                                                    'update_time' => date('Y-m-d H:i:s'),
                                                    'update_loginpemakai_id' => Yii::app()->user->id
                                                );

                                                $penunjang = PasienmasukpenunjangT::model()->updateByPk($pasienMasukPenunjang->pasienmasukpenunjang_id, $attributes);


                                                if($penunjang){
                                                    $pesan = 'success';
                                                    $status = 'ok';
                                                    $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";

                                                    $modHasilRad = ROHasilPemeriksaanRadT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPenunjang,'pendaftaran_id'=>$idPendaftaran));

                                                    if(count($modHasilRad) > 0){
                                                        $s = 0;
                                                        foreach ($modHasilRad as $i => $val) {
                                                            if (empty($val['hasilexpertise']) and empty($val['kesan_hasilrad']) and empty($val['kesimpulan_hasilrad']) and empty($val['tglpegambilanhasilrad']) ) {
                                                                $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                                                                if (!$sukses_pembalik) {
                                                                    $s++;
                                                                }
                                                            }else{
                                                                $s++;
                                                            }
                                                        }
                                                        if ($s==0) {
                                                            $pesan = 'success';
                                                            $status = 'ok';
                                                            $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                                        }else{
                                                            $pesan = 'exist';
                                                            $status = 'ok';
                                                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                                            </b> sudah melakukan pemeriksaan </div>";
                                                        }

                                                    }
                                


                                                }
                                            
                                            }else{
                                                // echo "L";exit();
                                                $pesan = 'exist';
                                                $status = 'ok';
                                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                                            }                     
                            }else{
                                // echo "M";exit();
                                $pesan = 'exist';
                                $status = 'ok';
                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                            }
                        }else{
                            // echo "N";exit();
                            if(empty($tindakan->tindakansudahbayar_id)){
                                // echo "O";exit();
                                $findHasil = HasilpemeriksaanlabT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$pasienMasukPenunjang->pasienmasukpenunjang_id));
                                            $model = new PasienbatalperiksaR();
                                            $model->pendaftaran_id = $pendaftaran_id;
                                            $model->pasien_id = $pasien_id;
                                            $model->pasienmasukpenunjang_id = $pasienmasukpenunjang_id;
                                            $model->pasienkirimkeunitlain_id = $pasienkirimkeunitlain_id;
                                            $model->tglbatal = date('Y-m-d');
                                            $model->keterangan_batal = "Batal Radiologi";
                                            $model->create_time = date('Y-m-d H:i:s');
                                            $model->update_time = null;
                                            $model->create_loginpemakai_id = Yii::app()->user->id;
                                            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

                                            if(!$model->save())
                                            {
                                                // echo "P";exit();
                                                $status = 'not';
                                                $pesan = 'exist';
                                                $keterangan = "<div class='flash-success'>Data gagal disimpan</div>";
                                            }

                                            if(empty($pendaftaran->pembayaranpelayanan_id)){
                                                // echo "Q";exit();
                                                $attributes = array(
                                                    'statusperiksa' => 'BATAL PERIKSA',
                                                    'update_time' => date('Y-m-d H:i:s'),
                                                    'update_loginpemakai_id' => Yii::app()->user->id
                                                );
                                                if($penunjang){
                                                    $pesan = 'success';
                                                    $status = 'ok';
                                                    $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";

                                                    $modHasilRad = ROHasilPemeriksaanRadT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPenunjang,'pendaftaran_id'=>$idPendaftaran));

                                                    if(count($modHasilRad) > 0){
                                                        $s = 0;
                                                        foreach ($modHasilRad as $i => $val) {
                                                            if (empty($val['hasilexpertise']) and empty($val['kesan_hasilrad']) and empty($val['kesimpulan_hasilrad']) and empty($val['tglpegambilanhasilrad']) ) {
                                                                $sukses_pembalik =  TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                                                                if (!$sukses_pembalik) {
                                                                    $s++;
                                                                }
                                                            }else{
                                                                $s++;
                                                            }
                                                        }
                                                        if ($s==0) {
                                                            $pesan = 'success';
                                                            $status = 'ok';
                                                            $keterangan = "<div class='flash-success'>Data berhasil disimpan</div>";
                                                        }else{
                                                            $pesan = 'exist';
                                                            $status = 'ok';
                                                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                                            </b> sudah melakukan pemeriksaan </div>";
                                                        }

                                                    }
                                


                                                }
                                        }else{
                                            // echo "R";exit();
                                            $pesan = 'exist';
                                            $status = 'ok';
                                            $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                            </b> sudah melakukan pembayaran pemeriksaan </div>";
                                        }                      
                            }else{
                                // echo "S";exit();
                                $pesan = 'exist';
                                $status = 'ok';
                                $keterangan = "<div class='flash-success'>Pasien <b> ".$pendaftaran->pasien->nama_pasien." 
                                                </b> sudah melakukan pembayaran pemeriksaan </div>";
                            }
                        }
                    }

                    

                    /*
                     * kondisi_commit
                     */
                    if($pesan == 'success')
                    {
                        $transaction->commit();
                    }else{
                        $transaction->rollback();
                    }   
                }catch(Exception $ex){
                    print_r($ex);
                    $status = 'not';
                    $transaction->rollback();
                }

                $data['pesan'] = $pesan;
                $data['status'] = $status;
                $data['keterangan'] = $keterangan;

                echo json_encode($data);
                Yii::app()->end();
            }
        }    
        
        public function actionStatusPeriksa()
        {
            if(Yii::app()->request->isAjaxRequest){
                $status = (isset($_POST['statusperiksa']) ? $_POST['statusperiksa'] : null);
                $pendaftaran_id = (isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null);
                $pasienmasukpenunjang_id = (isset($_POST['pasienmasukpenunjang_id']) ? $_POST['pasienmasukpenunjang_id'] : null);
                
                $statuspasien='';
                $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                if(!empty($modPendaftaran->pasienadmisi_id)){
                    $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                    if(!empty($modPasienAdmisi->pasienpulang_id)){
                        $statuspasien = 'SUDAH PULANG';
                    }
                }else{
                   $statuspasien = $modPendaftaran->statusperiksa;
                }
                
                $data['pendaftaran_id'] = $pendaftaran_id;
                $data['pasienmasukpenunjang_id'] = $pasienmasukpenunjang_id;
                $data['statuspasien'] = $statuspasien;
                
                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        public function actionPrint($id_pendaftaran,$id_pasienpenunjang = "", $labelOnly = null, $caraPrint){
            $judulLaporan = 'Informasi Pendaftaran Laboratorium';
            $modPendaftaran = PendaftaranT::model()->findByPk($id_pendaftaran);            
            $modPasien = ROPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $labelOnly = false;
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_RAD)
            { 
                $modRincian = RORincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                                    ), array('order'=>'pemeriksaanlab_urutan'));
            }else{ 
                $modRincian = RORincianpemeriksaanlabradV::model()->findAllByAttributes(array(
                                    'pendaftaran_id' => $id_pendaftaran,
                                    ));
            } 
            $modPasienMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($id_pasienpenunjang);
            
            
            if(!empty($id_pasienpenunjang) && Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_RAD)
            {
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran,
//                    'pasienmasukpenunjang_id'=>$id_pasienpenunjang
                ));
            }else{
                $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array(
                    'pendaftaran_id' => $id_pendaftaran
                ));
            }
            
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modPasien'=>$modPasien,
                                            'modPendaftaran'=>$modPendaftaran, 
                                            'modRincian'=>$modRincian,
                                            'modTindakanPelayanan'=>$modTindakanPelayanan,
                                            'labelOnly'=>$labelOnly,
                                            'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                    ));
            }
           
        }
}