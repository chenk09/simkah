<?php
Yii::import('laboratorium.models.LBLaporanpemeriksaanpenunjangV'); 
Yii::import('laboratorium.models.LBLaporandetailpemeriksaanpenunjangV'); //untuk LaporanPemeriksaanPenunjang
class LaporanController extends SBaseController {
    public $pathViewLab = 'laboratorium.views.laporan.';
    public function actionLaporanSensusHarian() {
        $model = new ROLaporansensusradiologiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenis = CHtml::listData(PemeriksaanradM::model()->findAll('pemeriksaanrad_aktif = true'), 'pemeriksaanrad_id', 'pemeriksaanrad_id');
        $model->pemeriksaanrad_id = $jenis;
        $kunjungan = Kunjungan::items();
        $model->kunjungan = $kunjungan;
        $model->pilihan = $_GET['filter'];
        if (isset($_GET['ROLaporansensusradiologiV'])) {
            $model->attributes = $_GET['ROLaporansensusradiologiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporansensusradiologiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporansensusradiologiV']['tglAkhir']);
        }

        $this->render('sensus/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new ROLaporansensusradiologiV('search');
        $judulLaporan = 'Laporan Sensus Harian Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        $model->pilihan = $_GET['filter'];
        if (isset($_REQUEST['ROLaporansensusradiologiV'])) {
            $model->attributes = $_REQUEST['ROLaporansensusradiologiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporansensusradiologiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporansensusradiologiV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporansensusradiologiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->pilihan = $_GET['filter'];
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['ROLaporansensusradiologiV'])) {
            $model->attributes = $_GET['ROLaporansensusradiologiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporansensusradiologiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporansensusradiologiV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanKunjungan() {
        $model = new ROLaporanpasienpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->kunjungan = Kunjungan::items();
        
        if (isset($_GET['ROLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['ROLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpasienpenunjangV']['tglAkhir']);
        }

        $this->render('kunjungan/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new ROLaporanpasienpenunjangV('search');
        $judulLaporan = 'Laporan Kunjungan Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Radiologi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporanpasienpenunjangV'])) {
            $model->attributes = $_REQUEST['ROLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpasienpenunjangV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporanpasienpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['ROLaporanpasienpenunjangV'])) {
            $model->attributes = $_GET['ROLaporanpasienpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpasienpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpasienpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new ROLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['ROLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['ROLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new ROLaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien Radiologi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['ROLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Radiologi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporan10besarpenyakit'])) {
            $model->attributes = $_GET['ROLaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new ROLaporanpemakaiobatalkesV;
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $jenisObat =CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'),'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenisObat;
        if(isset($_GET['ROLaporanpemakaiobatalkesV']))
        {
            $model->attributes = $_GET['ROLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        $this->render('pemakaiObatAlkes/index',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaiObatAlkes() {
        $model = new ROLaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Info Pemakai Obat Alkes Radiologi';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Radiologi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_REQUEST['ROLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Radiologi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['ROLaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi() {
        $model = new ROLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $tindakan = array('sudah', 'belum');
        $model->tindakansudahbayar_id = $tindakan;
        if (isset($_GET['ROLaporanjasainstalasi'])) {
            $model->attributes = $_GET['ROLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanjasainstalasi']['tglAkhir']);
        }

        $this->render('jasaInstalasi/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanJasaInstalasi() {
        $model = new ROLaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporanjasainstalasi'])) {
            $model->attributes = $_REQUEST['ROLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanjasainstalasi']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporanjasainstalasi'])) {
            $model->attributes = $_GET['ROLaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new ROLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s'); ; 
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['ROLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['ROLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new ROLaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Radiologi';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Radiologi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['ROLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Radiologi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporanbiayapelayanan'])) {
            $model->attributes = $_GET['ROLaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new ROLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['ROLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['ROLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpendapatanruanganV']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/index', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new ROLaporanpendapatanruanganV('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Radiologi';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporanpendapatanruanganV'])) {
            $model->attributes = $_REQUEST['ROLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporanpendapatanruanganV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporanpendapatanruanganV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporanpendapatanruanganV'])) {
            $model->attributes = $_GET['ROLaporanpendapatanruanganV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporanpendapatanruanganV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporanpendapatanruanganV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new ROBukuregisterpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['ROBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['ROBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROBukuregisterpenunjangV']['tglAkhir']);
        }

        $this->render('bukuRegister/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanBukuRegister() {
        $model = new ROBukuregisterpenunjangV('search');
        $judulLaporan = 'Laporan Buku Register Pasien Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Radiologi';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROBukuregisterpenunjangV'])) {
            $model->attributes = $_REQUEST['ROBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROBukuregisterpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Radiologi';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROBukuregisterpenunjangV'])) {
            $model->attributes = $_GET['ROBukuregisterpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROBukuregisterpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROBukuregisterpenunjangV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanCaraMasukPasien() {
        $model = new ROLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $asalrujukan = CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'), 'asalrujukan_id', 'asalrujukan_id');
        $model->asalrujukan_id = $asalrujukan;
        $ruanganasal = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_id');
        $model->ruanganasal_id = $ruanganasal;
        if (isset($_GET['ROLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['ROLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('caraMasuk/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanCaraMasukPasien() {
        $model = new ROLaporancaramasukpenunjangV('search');
        $judulLaporan = 'Laporan Cara Masuk Pasien Radiologi';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['ROLaporancaramasukpenunjangV'])) {
            $model->attributes = $_REQUEST['ROLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['ROLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'caraMasuk/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanCaraMasukPasien() {
        $this->layout = '//layouts/frameDialog';
        $model = new ROLaporancaramasukpenunjangV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Cara Masuk Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['ROLaporancaramasukpenunjangV'])) {
            $model->attributes = $_GET['ROLaporancaramasukpenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['ROLaporancaramasukpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ROLaporancaramasukpenunjangV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /**
     * copy dari Laboratorium
     */
    public function actionLaporanPemeriksaanPenunjang() {
        $judulLaporan = 'Laporan Jenis Pemeriksaan Radiologi';
        $model = new LBLaporanpemeriksaanpenunjangV('searchTableLaporan');
          $modDetail = new LBLaporandetailpemeriksaanpenunjangV('searchTableLaporan');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        
         $modDetail->tglAwal = date('d M Y 00:00:00');
        $modDetail->tglAkhir = date('Y-m-d 23:59:59');
     if($_GET['filter_tab'] == "rekap"){
                if (isset($_GET['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_GET['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                    
                }
            } else
            if($_GET['filter_tab'] == "detail"){
                if (isset($_GET['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_GET['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporandetailpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporandetailpemeriksaanpenunjangV']['tglAkhir']);
                          if($_GET['LBLaporandetailpemeriksaanpenunjangV']['pilihDokter'] == "RUJUKANLUAR"){
                            $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['nama_perujuk'];
                        }else{
                            $modDetail->namapegawai = $_GET['LBLaporandetailpemeriksaanpenunjangV']['namapegawai'];
                        }   
                    $modDetail->penjamin_id = $_GET['LBLaporandetailpemeriksaanpenunjangV']['penjamin_id'];
                    $modDetail->carabayar_id = $_GET['LBLaporandetailpemeriksaanpenunjangV']['carabayar_id'];

                    
                }
            }
        
        $this->render($this->pathViewLab.'pemeriksaanPenunjang/admin', array(
            'model' => $model,
              'modDetail'=>$modDetail,
            'judulLaporan' => $judulLaporan,
        ));
    }

   public function actionPrintLaporanPemeriksaanPenunjang() {
        $model = new LBLaporanpemeriksaanpenunjangV('searchPrintLaporan');
        $modDetail = new LBLaporandetailpemeriksaanpenunjangV('searchPrintLaporan');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jenis Pemeriksaan Laboratorium';
        $data['type'] = $_REQUEST['type'];
        
//        if (isset($_REQUEST['filter_tab'])){
            if($_REQUEST['filter_tab'] == "rekap"){
                $judulLaporan = 'Laporan Jenis Pemeriksaan Laboratorium - REKAP';
                if (isset($_REQUEST['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_REQUEST['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                }
            } else
            if($_REQUEST['filter_tab'] == "detail"){
                $judulLaporan = 'Laporan Jenis Pemeriksaan Laboratorium - DETAIL';
                if (isset($_REQUEST['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_REQUEST['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporandetailpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LBLaporandetailpemeriksaanpenunjangV']['tglAkhir']);
                }
            }
//        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = $this->pathViewLab.'pemeriksaanPenunjang/_print';
        
        $this->printFunctionPemeriksaan($model, $modDetail, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPemeriksaanPenunjang() {
        $this->layout = '//layouts/frameDialog';
        $model = new LBLaporanpemeriksaanpenunjangV('search');
        $modDetail = new LBLaporandetailpemeriksaanpenunjangV('search');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jenis Pemeriksaan Laboratorium';
        $data['type'] = $_GET['type'];
        
//        if (isset($_GET['filter_tab'])){
            if($_GET['filter_tab'] == "rekap"){
                if (isset($_GET['LBLaporanpemeriksaanpenunjangV'])) {
                    $model->attributes = $_GET['LBLaporanpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporanpemeriksaanpenunjangV']['tglAkhir']);
                }
            } else
            if($_GET['filter_tab'] == "detail"){
                if (isset($_GET['LBLaporandetailpemeriksaanpenunjangV'])) {
                    $modDetail->attributes = $_GET['LBLaporandetailpemeriksaanpenunjangV'];
                    $format = new CustomFormat();
                    $modDetail->tglAwal = $format->formatDateTimeMediumForDB($_GET['LBLaporandetailpemeriksaanpenunjangV']['tglAwal']);
                    $modDetail->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LBLaporandetailpemeriksaanpenunjangV']['tglAkhir']);
                }
            }
//        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'modDetail'=>$modDetail,
            'data' => $data,
        ));
    }
    /* end copy */
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function printFunctionPemeriksaan($model, $modDetail, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
         if($_GET['filter_tab'] == "rekap"){
            $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        }else if($_GET['filter_tab'] == "detail"){
            $periode = $this->parserTanggal($modDetail->tglAwal).' s/d '.$this->parserTanggal($modDetail->tglAkhir);
        } 
//        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modDetail'=>$modDetail, 'periode'=>$periode,'periodeDetail'=>$periodeDetail, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modDetail'=>$modDetail,'periode'=>$periode, 'periodeDetail'=>$periodeDetail, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modDetail'=>$modDetail, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint),true));
            $mpdf->Output();
        }
    }
    
    
//    protected function parserTanggal($tgl){
//        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($tgl, 'yyyy-MM-dd hh:mm:ss'));
//    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

}