<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class InputPemeriksaanController extends SBaseController
{
        public $successSave = false;
        public $successSaveTindakan = true;
        public $successUpdateTindakan = true;
        public $successUpdateTindakanKomponen = true;
        public $successUpdateHasilPemeriksaan = true;
        public $successSaveBiayaAdministrasi = false;
        
	public function actionIndex($idPendaftaran,$idPasien,$idPasienMasukPenunjang)
	{
            $modPendaftaran = ROPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = ROPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modPeriksaRad = ROPemeriksaanRadM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true));
            
            if(isset($_POST['permintaanPenunjang'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPasienPenunjang = ROPasienMasukPenunjangT::model()->findByPk($idPasienMasukPenunjang);
                    $this->saveTindakanPelayanan($modPendaftaran, $modPasienPenunjang);
                    
                    if ($this->successSaveTindakan){
                        //set null pembayaran supaya muncul di informasi belum bayar
                        PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                            array('pembayaranpelayanan_id'=>null)
                        );
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect($this->createUrl('DaftarPasien/index',array('modulId'=>Yii::app()->session['modulId'])));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',array('modPeriksaRad'=>$modPeriksaRad,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,));
	}
        /**
         * actionUpdate untuk update pemeriksaan yang di input pada pendaftaran
         * @author ichan | ihsan F Rahman <ichan90@yahoo.co.id>
         * Modify by @author Miranitha Fasha Line : 84 
         * @param type $idPendaftaran
         * @param type $idPasienMasukPenunjang
         */
	public function actionUpdate($idPendaftaran,$idPasienMasukPenunjang)
	{
            $modPendaftaran = ROPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = ROPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modHasilPemeriksaan = ROHasilPemeriksaanRadT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran, 'pasien_id'=>$modPendaftaran->pasien_id, 'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modPeriksaRad = ROPemeriksaanRadM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true), array('order'=>'pemeriksaanrad_jenis, pemeriksaanrad_urutan ASC'));$modTindakanPelayan= TindakanpelayananT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran, 'pasien_id'=>$modPendaftaran->pasien_id, 'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang, 'daftartindakan_id'=>2003));
            $rujukanM = RujukanT::model()->findByPk($modPendaftaran->rujukan_id);
            
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa menambahkan tindakan transaksi. ');
                        window.top.location.href='".Yii::app()->createUrl('radiologi/DaftarPasien/index')."';
                    </script>";
            }
            
            foreach($modHasilPemeriksaan as $i => $dataHasil){
                foreach($modPeriksaRad as $j => $dataPeriksa){
                    if($modHasilPemeriksaan[$i]->pemeriksaanrad_id == $modPeriksaRad[$j]->pemeriksaanrad_id){
                        $dataPeriksa->isChecked = true; //menandai pemeriksaan yang telah di pilih
                    }
                }
            }
            $modMasukPenunjang = PasienmasukpenunjangT::model()->findByPk($idPasienMasukPenunjang);
            $modKirimKeUnitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                'pendaftaran_id'=>$idPendaftaran,
                'pasienkirimkeunitlain_id'=>$modMasukPenunjang->pasienkirimkeunitlain_id,
            ));
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            if(isset($_POST['permintaanPenunjang'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    //*** START dipakai untuk merubah dokter perujuk dari ruangan jika tidak sesuai dengan blangko rad
                    if(isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) && !empty($_POST['PasienkirimkeunitlainT']['pegawai_id'])){
                        $modKirimKeUnitLain->pegawai_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
                        $modKirimKeUnitLain->save();
                    }                  
                    //*** END -------------------------------------------------------
                    
                    //*** START dipakai untuk merubah dokter perujuk dari Luar jika tidak sesuai atau akan dirubah
                    if(isset($_POST['RujukanT']['pegawai_id']) && !empty($_POST['RujukanT']['pegawai_id']))
                    {
                        $rujukanM->nama_perujuk = $_POST['RujukanT']['pegawai_id'];
                        $rujukandari_id = $_POST['RujukanT']['pegawai_id'];
                        $rujukandariM = RujukandariM::model()->findByAttributes(
                                array(
                                    'namaperujuk'=>$_POST['RujukanT']['pegawai_id'],
                                )
                            );

                        $rujukanM->rujukandari_id = $rujukandariM->rujukandari_id;
                        $rujukanM->nama_perujuk = $rujukandariM->namaperujuk;

//                        echo "<pre>";
//                        echo $rujukandari_id;
//                        echo print_r($rujukanM->getAttributes());exit;
                        $rujukanM->save();
                    }
                    //*** END ----------------------------------------------------------------------------------
                    
                    $modPasienPenunjang = ROPasienMasukPenunjangT::model()->findByPk($idPasienMasukPenunjang);
                    $this->hapusPemeriksaanUnchecked($modPendaftaran, $modPasienPenunjang, $_POST['permintaanPenunjang']);
                    $this->updateTindakanPelayanan($modPasien, $modPendaftaran, $modPasienPenunjang,$_POST['RekeningakuntansiV']);
                    $this->saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienPenunjang, $_POST['biayaAdministrasi'],$_POST['RekeningakuntansiV']);
					//$this->saveBiayaAdministrasi($modTindakanPelayan, $_POST['biayaAdministrasi']);
                    $pegawai_id = $_POST['ROPasienMasukPenunjangT']['pegawai_id'];
                    if(empty($pegawai_id)){
                        $pegawai_id = Params::DEFAULT_DOKTER_RAD;
                    }
                    PasienmasukpenunjangT::model()->updateByPk($idPasienMasukPenunjang,array('pegawai_id'=>$pegawai_id));
                    if ($this->successUpdateTindakan && $this->successUpdateTindakanKomponen && $this->successUpdateHasilPemeriksaan){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect($this->createUrl('DaftarPasien/index',array('modulId'=>Yii::app()->session['modulId'])));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        if(!$this->successUpdateTindakan)
                            Yii::app()->user->setFlash('error',"Data tindakan gagal disimpan ");
                        if(!$this->successUpdateTindakanKomponen)
                            Yii::app()->user->setFlash('error',"Data tindakan komponen gagal disimpan ");
                        if(!$this->successUpdateHasilPemeriksaan)
                            Yii::app()->user->setFlash('error',"Data Hasil Pemeriksaan gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('update',array(
                    'modPeriksaRad'=>$modPeriksaRad,
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                    'modTindakanPelayan'=>$modTindakanPelayan,
                    'modRekenings'=>$modRekenings,
                    'rujukanM'=>$rujukanM,
            ));
	}
        
        protected function updateTindakanPelayanan($modPasien, $modPendaftaran,$modPasienPenunjangRad,$postRekenings=array())
        {
            $validTRad = true;
//            echo "<pre>"; print_r($_POST['permintaanPenunjang']); exit;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
//                    if(isset($item['cbPemeriksaan'])){ //cek
                        $modTindakans[$i] = TindakanpelayananT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasien_id'=>$modPasien->pasien_id, 'daftartindakan_id'=>$item['idDaftarTindakan']));
                        if(empty($modTindakans[$i]->tindakanpelayanan_id)){ //jika belum ada di database tindakanpelayanan_t
                            $modTindakans[$i] = new TindakanpelayananT;
                            $modTindakans[$i]->attributes=$item;
                            $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                            $modTindakans[$i]->pasien_id = $modPasien->pasien_id;
                            $modTindakans[$i]->kelaspelayanan_id = Params::KELASPELAYANAN_ID_TANPA_KELAS;;
                            $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                            $modTindakans[$i]->instalasi_id = $modPendaftaran->instalasi_id;
                            $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                            $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                            $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjangRad->pasienmasukpenunjang_id;
                            $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                            $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                            $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                            $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                            $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                            $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                            $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                            $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;
                            $modTindakans[$i]->ruangan_id = Params::RUANGAN_ID_RAD;
                            $modTindakans[$i]->discount_tindakan = 0;
                            $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                            $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                            $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                            $modTindakans[$i]->iurbiaya_tindakan = 0;
                        }
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $validTRad = $modTindakans[$i]->validate() && true;
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                    
//                    }
                }
            }
			
			
            
            if($validTRad){
                
                foreach($modTindakans as $i=>$tindakan){
                    
                    if($tindakan->save()){
                        if(isset($postRekenings)){
                            $modJurnalRekening = TindakanController::saveJurnalRekening($tindakan->ruangan_id);
                            //update jurnalrekening_id
                            $tindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                            $tindakan->save();
                            $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $tindakan, 'tm');
                        }
                        $this->successUpdateTindakan = $this->successUpdateTindakan && true;
                    }else
                        $this->successUpdateTindakan = false;
                    
                    $statusSaveKomponen = $this->updateTindakanKomponen($tindakan);
                    
                    $statusSaveHasilPeriksa = $this->updateHasilPemeriksaan($tindakan,$idPemeriksaanRad[$i]);
                    
                }
                
            }else{
                $this->successUpdateTindakan = false;
            } 
           
//            return $modTindakans;
            return $this->successUpdateTindakan;
        }
		
		public function saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienPenunjang, $tarifKarcisLab, $postRekenings = array())
        {
            $cekTindakanKomponen=0;
            $cekTindakan = TindakanpelayananT::model()->findByAttributes(array(
                'pasien_id'=>$modPasien->pasien_id,
                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id,
                'pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id,
                'daftartindakan_id' => Params::TARIF_ADMINISTRASI_LAB_ID,
                ));
            if(!$cekTindakan){
                $modTindakanPelayan= New TindakanpelayananT;
            }
            $modTindakanPelayan->penjamin_id=$modPendaftaran->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=$modPendaftaran->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_LAB_ID;
            $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$modPendaftaran->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = $modPendaftaran->ruangan_id;
             
            $tarifTindakan= ROTariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save()){ 
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening($modTindakanPelayan->ruangan_id);
                        //update jurnalrekening_id
                        $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                    }
                   $tindakanKomponen= ROTariftindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new TindakankomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
			} 
           
        }
		
		/* public function updateBiayaAdministrasi($modTindakanPelayan, $tarifKarcisLab)
		{
			TindakanpelayananT::model()->updateByPk($modTindakanPelayan->tindakanpelayanan_id, array('tarif_satuan'=>$tarifKarcisLab, 'tarif_tindakan'=>$tarifKarcisLab));
		} */
        
        protected function updateTindakanKomponen($tindakan) 
        {   
            
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = TindakankomponenT::model()->findByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id, 'komponentarif_id'=>$tarif->komponentarif_id));
                if(empty($modTindakanKomponen->tindakankomponen_id)){ //Jika belum ada di database
                    $modTindakanKomponen = new TindakankomponenT;
                    $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                    $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                    $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                    $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                    if($tindakan->cyto_tindakan){
                        $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                    } else {
                        $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                    }
                    $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                    $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                    $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                    $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                }   
                $valid = $modTindakanKomponen->validate() && $valid;
                
                if($valid){
                    $modTindakanKomponen->save();
                    $this->successUpdateTindakanKomponen = $this->successUpdateTindakanKomponen && true;
                }else{
                    $this->successUpdateTindakanKomponen = false;
                }
            }
            
//            return $valid;
            return $this->successUpdateTindakanKomponen;
        }
        
        protected function updateHasilPemeriksaan($tindakan,$idPemeriksaanRad) // Untuk Radiologi
        {
            
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = ROHasilPemeriksaanRadT::model()->findByAttributes(array('pendaftaran_id'=>$tindakan->pendaftaran_id, 'pasien_id'=>$tindakan->pasien_id, 'tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id, 'pemeriksaanrad_id'=>$idPemeriksaanRad));
                if(empty($modHasil->hasilpemeriksaanrad_id)){ //jika hasil belum ada di database
                    $modHasil = new ROHasilPemeriksaanRadT;
                    $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                    $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                    $modHasil->pasien_id = $tindakan->pasien_id;
                    $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                    $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                    $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                    $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                }
                $valid = $modHasil->validate() && $valid;
                
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                    $this->successUpdateHasilPemeriksaan = $this->successUpdateHasilPemeriksaan && true;
                }else{
                    $this->successUpdateHasilPemeriksaan = false;
                }
            }
             
//            return $valid;
            return $this->successUpdateHasilPemeriksaan;
        }
        
        //=== end by ichan
        
        
        
        /**
         * Fungsi untuk menyimpan data ke model TindakanpelayananT
         * @param type $modPendaftaran
         * @param type $modPasienPenunjang
         * @return TindakanpelayananT 
         */
        protected function saveTindakanPelayanan($modPendaftaran,$modPasienPenunjang)
        {
            $valid=true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPendaftaran->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = Params::KELASPELAYANAN_ID_TANPA_KELAS;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = Params::INSTALASI_ID_RAD; //$modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $modPendaftaran->pegawai_id;

                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $valid = $modTindakans[$i]->validate() && $valid;
                        
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                }
            }
            
            if($valid){
                foreach($modTindakans as $i=>$tindakan){
                    $tindakan->save();
                    $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                }
                if($statusSaveKomponen && $statusSaveHasilPeriksa) {
                    $this->successSaveTindakan = $this->successSaveTindakan && true;
                } else {
                    $this->successSaveTindakan = false;
                }
            } else {
                $this->successSaveTindakan = false;
            }
            
            return $modTindakans;
        }

        public function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        /**
         *
         * @param type $tindakan
         * @return type 
         */
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }
        
        /**
         *
         * @param type $tindakan
         * @param type $idPemeriksaanRad
         * @return type 
         */
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad)
        {
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $valid = $modHasil->validate() && $valid;
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
            
            return $valid;
        }
        
        /**
         * hapusPemeriksaanUnchecked untuk memncari dan menghapus pemeriksaan yang di uncheck
         */
        protected function hapusPemeriksaanUnchecked($modPendaftaran, $modPasienMasukPenunjang, $postUpdate){
            $konfigSys = KonfigsystemK::model()->find();
            $modTindakanPelayanan = TindakanpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));

            foreach($modTindakanPelayanan as $i => $mod){
                $adaDipost = false;
                foreach($postUpdate AS $i => $val){ // bandingkan dengan daftartindakan_id yang di post
                    if($mod->daftartindakan_id == $val['idDaftarTindakan']){
                        $adaDipost = true;
                    }
                }
                if(!$adaDipost){ //jika tidak ada di post
                    //update tindakan pelayanan 
                    $mod->hasilpemeriksaanrad_id = null;
                    $mod->save();
                    //DINONAKTIFKAN KARENA PENGHAPUSAN DI ATUR DI RELASI TABEL : CASCADE ON DELETE
//                    if($konfigSys->isdeljurnaltransaksi == true){
//                        $updateTindakan = TindakanpelayananT::model()->findByPk($mod->tindakanpelayanan_id);
//                        if(isset($updateTindakan->jurnalrekening_id)){
//                            $jurnalrekId = $updateTindakan->jurnalrekening_id;
//                            $updateTindakan->jurnalrekening_id = null;
//                            $updateTindakan->save();
//                            // hapus jurnaldetail_t dan jurnalrekening_t
//                            if($jurnalrekId){
//                                JurnaldetailT::model()->deleteAllByAttributes(array('jurnalrekening_id'=>$jurnalrekId));
//                                JurnalrekeningT::model()->deleteByPk($jurnalrekId);
//                            }
//                        }
//                    }
                    
                    if($mod->delete()){
                        //hapus detail pemeriksaan
                        $deleteHasilPemeriksaan = ROHasilPemeriksaanRadT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id, 'tindakanpelayanan_id'=>$mod->tindakanpelayanan_id));
                    }
                }
            }
        }
}