
<?php

class ReferensiHasilRadMController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            $modReferensiHasil = new ROReferensiHasilRadM;

            // Uncomment the following line if AJAX validation is needed
            // $this->performAjaxValidation($model);

            if(isset($_POST['ROReferensiHasilRadM']))
            {
                    $modReferensiHasil->attributes = $_POST['ROReferensiHasilRadM'];
                    $modReferensiHasil->pemeriksaanrad_id = $_POST['ROReferensiHasilRadM']['pemeriksaanrad_id'];
                    $validReferensi = $modReferensiHasil->validate();
                    if($validReferensi){                        
                        $modReferensiHasil->save();
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        $this->redirect(array('admin','id'=>$modReferensiHasil->refhasilrad_id));
                    }
            }

            $this->render('create',array(
                    'modReferensiHasil'=>$modReferensiHasil,
            ));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$modReferensiHasil= $this->loadModel($id);
                
                if(!empty($modReferensiHasil)){
                    $modReferensiHasil->pemeriksaanrad_id = $modReferensiHasil->pemeriksaanrad_id;
                    $modReferensiHasil->pegawai_id = $modReferensiHasil->pegawai_id;
                    $modReferensiHasil->refhasilrad_kode = $modReferensiHasil->refhasilrad_kode;
                }else{
                    $modReferensiHasil = new ROReferensiHasilRadM;
                }

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ROReferensiHasilRadM']))
		{
                    $modReferensiHasil->attributes = $_POST['ROReferensiHasilRadM'];
                    $validReferensi = $modReferensiHasil->validate();
                    if($validReferensi){
                        $modReferensiHasil->save();
                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                        $this->redirect(array('admin','id'=>$modReferensiHasil->refhasilrad_id));
                    }
		}

		$this->render('update',array(
                        'modReferensiHasil'=>$modReferensiHasil,
		));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ROReferensiHasilRadM');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $model=new ROReferensiHasilRadM('searchReferensi');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['ROReferensiHasilRadM']))
                    $model->attributes=$_GET['ROReferensiHasilRadM'];

            $this->render('admin',array(
                    'model'=>$model,
            ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model = ROReferensiHasilRadM::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='roreferensihasilrad-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDelete()
	{
		//if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		if(Yii::app()->request->isPostRequest)
		{
                    $id = $_POST['id'];
                    $this->loadModel($id)->delete();
                    if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        'div'=>"<div class='flash-success'>Data berhasil dihapus.</div>",
                        ));
                    exit;
                }
	                    
                    // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
                    if(!isset($_GET['ajax']))
                        $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
                    throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

     /**
     *Mengubah status aktif
     * @param type $id 
     */
    public function actionRemoveTemporary()
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                    SAPropinsiM::model()->updateByPk($id, array('propinsi_aktif'=>false));
//                    $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
              
        
        $id = $_POST['id'];   
        if(isset($_POST['id']))
        {
           $update = ROReferensiHasilRadM::model()->updateByPk($id,array('refhasilrad_aktif'=>false));
           if($update)
            {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
             }
        } else {
                if (Yii::app()->request->isAjaxRequest)
                {
                    echo CJSON::encode(array(
                        'status'=>'proses_form', 
                        ));
                    exit;               
                }
        }

    }
        
    public function actionPrint()
    {
        $model=new ROReferensiHasilRadM('searchReferensiPrint');
                    $model->unsetAttributes();  // clear any default values
                    if(isset($_GET['ROReferensiHasilRadM']))
                            $model->attributes=$_GET['ROReferensiHasilRadM'];
        $judulLaporan='Data Referensi Hasil Radiologi';
        $caraPrint=$_REQUEST['caraPrint'];
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
            $mpdf->Output();
        }                       
    }
    
    /**
     * untuk load data pemeriksaan radiologi
     * @param type $encode
     * @param type $namaModel
     * @param type $att
     */
    public function actionGetPemeriksaan($encode=false,$namaModel='',$attr='')
    {
        if(Yii::app()->request->isAjaxRequest) {
            if($namaModel !=='' && $attr == ''){
                $pemeriksaanrad_jenis = $_POST["$namaModel"]['pemeriksaanrad_jenis'];
            }
             elseif ($namaModel == '' && $attr !== '') {
                $pemeriksaanrad_jenis = $_POST["$attr"];
            }
            elseif ($namaModel !== '' && $attr !== '') {
                $pemeriksaanrad_jenis = $_POST["$namaModel"]["$attr"];
            }
                        
            $pemeriksaanrad = PemeriksaanradM::model()->findAll("pemeriksaanrad_jenis ILIKE '%$pemeriksaanrad_jenis%'");
            $pemeriksaanrad = CHtml::listData($pemeriksaanrad,'pemeriksaanrad_id','pemeriksaanrad_nama');
            
            if($encode){
                echo CJSON::encode($pemeriksaanrad);
            } else {
                if(empty($pemeriksaanrad)){
                    echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Pilih --'),true);
                }else{
                    echo CHtml::tag('option', array('value'=>''),CHtml::encode('-- Pilih --'),true);
                    foreach($pemeriksaanrad as $value=>$name)
                    {
                        echo CHtml::tag('option', array('value'=>$value),CHtml::encode($name),true);
                    }
                }
            }
        }
        Yii::app()->end();
    }
}
