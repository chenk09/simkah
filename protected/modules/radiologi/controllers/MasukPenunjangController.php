<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class MasukPenunjangController extends SBaseController
{
        public $successSave = false;
        public $successSaveTindakan = true;
        public $successSaveBiayaAdministrasi = false;
        
	public function actionIndex($idPasienKirimKeUnitLain,$idPendaftaran)
	{
            //$modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            $modPendaftaran = ROPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = ROPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modPeriksaRad = ROPemeriksaanRadM::model()->findAllByAttributes(array('pemeriksaanrad_aktif'=>true), array('order'=>'pemeriksaanrad_jenis, pemeriksaanrad_urutan ASC'));
            $modPermintaan = ROPermintaanKePenunjangT::model()->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            
            
            if(!empty($modPendaftaran->pasienadmisi_id)){
                $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                if(!empty($modPasienAdmisi->pasienpulang_id)){
                    $status = "SUDAH PULANG";
                }
            }else{
                $status = $modPendaftaran->statusperiksa;
            }
            
            if($status == "SUDAH PULANG"){
                echo "<script>
                        alert('Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan pasien. ');
                        window.top.location.href='".Yii::app()->createUrl('radiologi/RujukanPenunjang/index')."';
                    </script>";
            }
            
            $modKirimKeUnitLain = PasienkirimkeunitlainT::model()->findByPk($idPasienKirimKeUnitLain);
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            if(isset($_POST['permintaanPenunjang'])) {
                //$this->redirect('/radiologi/RujukanPenunjang');
                //echo "<pre>".print_r($_POST['permintaanPenunjang'],1)."</pre>";
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPasienPenunjang = $this->savePasienPenunjang($modPendaftaran,$idPasienKirimKeUnitLain,$_POST['RekeningakuntansiV']);
                    $this->saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienPenunjang, $_POST['biayaAdministrasi'],$_POST['RekeningakuntansiV']);
                    //*** START dipakai untuk merubah dokter perujuk dari ruangan jika tidak sesuai dengan blangko lab
                    if(isset($_POST['PasienkirimkeunitlainT']['pegawai_id']) && !empty($_POST['PasienkirimkeunitlainT']['pegawai_id'])){
                        $modKirimKeUnitLain->pegawai_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
                        $modKirimKeUnitLain->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modKirimKeUnitLain->save();
                    }
                    //*** END -------------------------------------------------------
                    if ($this->successSave && $this->successSaveTindakan){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect(array('DaftarPasien/index','modulId'=>Yii::app()->session['modulId']));
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $modRiwayatKirimKeUnitLain = ROPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                    'ruangan_id'=>Params::RUANGAN_ID_RAD,),
                                                                                                    'pasienmasukpenunjang_id IS NULL');
            $modRiwayatPenunjang = ROPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                             'ruangan_id'=>Params::RUANGAN_ID_RAD,));
            
            $this->render('index',array('modPermintaan'=>$modPermintaan,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modPeriksaRad'=>$modPeriksaRad,
                                        'modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modRiwayatPenunjang'=>$modRiwayatPenunjang,
                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                ));
	}
        
        /**
         * Fungsi untuk menyimpan data ke model ROPasienMasukPenunjangT
         * @param type $modPendaftaran
         * @param type $modPasien
         * @return ROPasienMasukPenunjangT 
         */
        protected function savePasienPenunjang($modPendaftaran,$idPasienKirimKeUnitLain, $postRekenings = array()){
            
            $modPasienPenunjang = new ROPasienMasukPenunjangT;
            $modPasienPenunjang->pasienkirimkeunitlain_id = $idPasienKirimKeUnitLain;
            $modPasienPenunjang->pasien_id = $modPendaftaran->pasien_id;
            $modPasienPenunjang->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modPasienPenunjang->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//            $modPasienPenunjang->pegawai_id = $modPendaftaran->pegawai_id;
            $modPasienPenunjang->pegawai_id = $_POST['ROPasienMasukPenunjangT']['pegawai_id'];
            $modPasienPenunjang->kelaspelayanan_id =  Params::KELASPELAYANAN_ID_TANPA_KELAS;
            $modPasienPenunjang->ruangan_id = Params::RUANGAN_ID_RAD;   //$modPendaftaran->ruangan_id;
            $modPasienPenunjang->no_masukpenunjang = KeyGenerator::noMasukPenunjang('RO');
            $modPasienPenunjang->tglmasukpenunjang = date('Y-m-d H:i:s');    //$modPendaftaran->tgl_pendaftaran;
            $modPasienPenunjang->no_urutperiksa =  KeyGenerator::noAntrianPenunjang($modPendaftaran->no_pendaftaran, $modPasienPenunjang->ruangan_id);
            $modPasienPenunjang->kunjungan = $modPendaftaran->kunjungan;
            $modPasienPenunjang->statusperiksa = $modPendaftaran->statusperiksa;
            $modPasienPenunjang->ruanganasal_id = $modPendaftaran->ruangan_id;
            
            if ($modPasienPenunjang->validate()){
                $modPasienPenunjang->Save();
                $this->successSave = true;
                $this->saveTindakanPelayanan($modPendaftaran, $modPasienPenunjang, $postRekenings);
                $this->updatePasienKirimKeUnitLain($modPasienPenunjang,$idPasienKirimKeUnitLain);
            } else {
                $this->successSave = false;
            }
            
            return $modPasienPenunjang;
        }
        
        /**
         * Fungsi untuk menyimpan data ke model TindakanpelayananT
         * @param type $modPendaftaran
         * @param type $modPasienPenunjang
         * @return TindakanpelayananT 
         */
        protected function saveTindakanPelayanan($modPendaftaran,$modPasienPenunjang,$postRekenings = array())
        {
            $valid=true;
            foreach($_POST['permintaanPenunjang'] as $i=>$item)
            {
                if(!empty($item)){
                        $modTindakans[$i] = new TindakanpelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $modPendaftaran->penjamin_id;
                        $modTindakans[$i]->pasien_id = $modPendaftaran->pasien_id;
                        $modTindakans[$i]->kelaspelayanan_id = $modPasienPenunjang->kelaspelayanan_id;
                        $modTindakans[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modTindakans[$i]->instalasi_id = Params::INSTALASI_ID_RAD; //$modPendaftaran->instalasi_id;
                        $modTindakans[$i]->pendaftaran_id = $modPendaftaran->pendaftaran_id;
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->pasienmasukpenunjang_id = $modPasienPenunjang->pasienmasukpenunjang_id;
                        $modTindakans[$i]->daftartindakan_id = $item['idDaftarTindakan'];
                        $modTindakans[$i]->carabayar_id = $modPendaftaran->carabayar_id;
                        $modTindakans[$i]->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
                        $modTindakans[$i]->tgl_tindakan = date('Y-m-d H:i:s');
                        $modTindakans[$i]->tarif_satuan = $item['inputtarifpemeriksaanrad'];
                        $modTindakans[$i]->tarif_tindakan = $item['inputtarifpemeriksaanrad'] * $item['inputqty'];
                        $modTindakans[$i]->satuantindakan = $item['satuan'];
                        $modTindakans[$i]->qty_tindakan = $item['inputqty'];
                        $modTindakans[$i]->cyto_tindakan = $item['cyto'];
                        $modTindakans[$i]->tarifcyto_tindakan = ($item['cyto']) ? (($item['persencyto'] / 100) * $modTindakans[$i]->tarif_tindakan) : 0;
                        $modTindakans[$i]->kelastanggungan_id = $modPendaftaran->kelastanggungan_id;
                        $modTindakans[$i]->dokterpemeriksa1_id = $_POST['ROPasienMasukPenunjangT']['pegawai_id'];
                        $modTindakans[$i]->dokterpemeriksa2_id = $_POST['PasienkirimkeunitlainT']['pegawai_id'];
//                        echo $_POST['ROPasienMasukPenunjangT']['pegawai_id']."-".$_POST['PasienkirimkeunitlainT']['pegawai_id'];exit;
                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        $modTindakans[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $valid = $modTindakans[$i]->validate() && $valid;
                        
                        $idPemeriksaanRad[$i] = $item['inputpemeriksaanrad'];
                }
            }
            
            if($valid){
                foreach($modTindakans as $i=>$tindakan){
                    $tindakan->save();
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $tindakan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $tindakan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $tindakan, 'tm');
                    }
                    $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                    $statusSaveHasilPeriksa = $this->saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad[$i]);
                }
                if($statusSaveKomponen && $statusSaveHasilPeriksa) {
                    $this->successSaveTindakan = $this->successSaveTindakan && true;
                } else {
                    $this->successSaveTindakan = false;
                }
            } else {
                $this->successSaveTindakan = false;
            }
            
            return $modTindakans;
        }
		
		public function saveBiayaAdministrasi($modPasien, $modPendaftaran, $modPasienPenunjang, $tarifKarcisLab, $postRekenings = array())
        {
            $cekTindakanKomponen=0;
            $modTindakanPelayan= New TindakanpelayananT;
            $modTindakanPelayan->penjamin_id=$modPendaftaran->penjamin_id;
            $modTindakanPelayan->pasien_id=$modPasien->pasien_id;
            $modTindakanPelayan->pasienmasukpenunjang_id=$modPasienPenunjang->pasienmasukpenunjang_id;
            $modTindakanPelayan->kelaspelayanan_id=$modPasienPenunjang->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RAD;
            $modTindakanPelayan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modTindakanPelayan->shift_id =Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = Params::TARIF_ADMINISTRASI_RAD_ID;
            $modTindakanPelayan->carabayar_id = $modPendaftaran->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id=$modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan=date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $tarifKarcisLab;
            $modTindakanPelayan->qty_tindakan=1;
            $modTindakanPelayan->tarif_tindakan=$modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan=Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan=0;
            $modTindakanPelayan->tarifcyto_tindakan=0;
            $modTindakanPelayan->dokterpemeriksa1_id=$modPendaftaran->pegawai_id;
            $modTindakanPelayan->discount_tindakan=0;
            $modTindakanPelayan->subsidiasuransi_tindakan=0;
            $modTindakanPelayan->subsidipemerintah_tindakan=0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan=0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');
             
            $tarifTindakan= ROTariftindakanM::model()->findAllByAttributes(array('daftartindakan_id'=>$modTindakanPelayan->daftartindakan_id, 'kelaspelayanan_id'=>$modTindakanPelayan->kelaspelayanan_id));
            foreach($tarifTindakan AS $dataTarif):
                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                        $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                        $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                        $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                    }
                     if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                        $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                    }
            endforeach;
            
            if($modTindakanPelayan->save()){ 
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening($modTindakanPelayan->ruangan_id);
                        //update jurnalrekening_id
                        $modTindakanPelayan->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modTindakanPelayan->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modTindakanPelayan, 'tm');
                    }
                
                   $tindakanKomponen= ROTariftindakanM::model()->findAll('daftartindakan_id='.$modTindakanPelayan->daftartindakan_id.' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                   $jumlahKomponen=COUNT($tindakanKomponen);

                   foreach ($tindakanKomponen AS $tampilKomponen):
                           $modTindakanKomponen=new TindakankomponenT;
                           $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                           $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                           $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                           $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                           $modTindakanKomponen->tarifcyto_tindakankomp=0;
                           $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                           $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                           $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                           $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                           if($modTindakanKomponen->save()){
                               $cekTindakanKomponen++;
                           }
                   endforeach;
                   if($cekTindakanKomponen!=$jumlahKomponen){
                          $this->successSaveBiayaAdministrasi=false;
                    }else
                        $this->successSaveBiayaAdministrasi=true;
           } 
           
        }

        public function traceTindakan($modTindakans)
        {
            foreach ($modTindakans as $key => $modTindakan) {
                $echo .= "<pre>".print_r($modTindakan->attributes,1)."</pre>";
            }
            return $echo;
        }
        
        /**
         *
         * @param type $tindakan
         * @return type 
         */
        protected function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            $criteria->compare('daftartindakan_id', $tindakan->daftartindakan_id);
            $criteria->compare('kelaspelayanan_id', $tindakan->kelaspelayanan_id);
            $modTarifs = TariftindakanM::model()->findAll($criteria);
            foreach ($modTarifs as $i => $tarif) {
                $modTindakanKomponen = new TindakankomponenT;
                $modTindakanKomponen->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $modTindakanKomponen->komponentarif_id = $tarif->komponentarif_id;
                $modTindakanKomponen->tarif_kompsatuan = $tarif->harga_tariftindakan;
                $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $tindakan->qty_tindakan;
                if($tindakan->cyto_tindakan){
                    $modTindakanKomponen->tarifcyto_tindakankomp = $tarif->harga_tariftindakan * ($tarif->persencyto_tind/100);
                } else {
                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                }
                $modTindakanKomponen->subsidiasuransikomp = $tindakan->subsidiasuransi_tindakan;
                $modTindakanKomponen->subsidipemerintahkomp = $tindakan->subsidipemerintah_tindakan;
                $modTindakanKomponen->subsidirumahsakitkomp = $tindakan->subsisidirumahsakit_tindakan;
                $modTindakanKomponen->iurbiayakomp = $tindakan->iurbiaya_tindakan;
                $valid = $modTindakanKomponen->validate() && $valid;
                if($valid)
                    $modTindakanKomponen->save();
            }
            
            return $valid;
        }
        
        /**
         *
         * @param type $tindakan
         * @param type $idPemeriksaanRad
         * @return type 
         */
        protected function saveHasilPemeriksaanRad($tindakan,$idPemeriksaanRad)
        {
            $valid = true;
            for($j=0;$j<$tindakan->qty_tindakan;$j++){
                $modHasil = new ROHasilPemeriksaanRadT;
                $modHasil->pendaftaran_id = $tindakan->pendaftaran_id;
                $modHasil->pasienadmisi_id = $tindakan->pasienadmisi_id;
                $modHasil->pasien_id = $tindakan->pasien_id;
                $modHasil->pasienmasukpenunjang_id = $tindakan->pasienmasukpenunjang_id;
                $modHasil->tglpemeriksaanrad = $tindakan->tgl_tindakan;
                $modHasil->pemeriksaanrad_id = $idPemeriksaanRad;
                $modHasil->tindakanpelayanan_id = $tindakan->tindakanpelayanan_id;
                $valid = $modHasil->validate() && $valid;
                if($valid){
                    $modHasil->save();
                    TindakanpelayananT::model()->updateByPk($tindakan->tindakanpelayanan_id, array('hasilpemeriksaanrad_id'=>$modHasil->hasilpemeriksaanrad_id));
                }
            }
            
            return $valid;
        }
        
        /**
         *
         * @param type $modPasienPenunjang 
         */
        protected function updatePasienKirimKeUnitLain($modPasienPenunjang,$idPasienKirimKeUnitLain) {
            if(!empty($_POST['permintaanPenunjang'])){
                foreach($_POST['permintaanPenunjang'] as $i => $item) {
                    PasienkirimkeunitlainT::model()->updateByPk($idPasienKirimKeUnitLain, 
                                                                array('pasienmasukpenunjang_id'=>$modPasienPenunjang->pasienmasukpenunjang_id));
                }
            }
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}