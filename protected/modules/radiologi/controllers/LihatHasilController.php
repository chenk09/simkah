<?php

class LihatHasilController extends SBaseController
{
	public function actionHasilPeriksa($idPendaftaran,$idPasien,$idPasienMasukPenunjang,$caraPrint='')
	{   
            $this->layout = '//layouts/printKartu';
			$enableprint = true;
            $modPasienMasukPenunjang = ROPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $pemeriksa = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$modPasienMasukPenunjang->pegawai_id));
            $detailHasil = HasilpemeriksaanradT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                    'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                ));
                $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                $modPasienMasukPenunjang->namaperujuk = $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                $modPasienMasukPenunjang->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
            }
			// Kondisi Print FALSE (Jika pasien berasal dari pendaftaran luar dengan carabayar membayar yang belum bayar) ATAU (jika pasien rawat jalan yang carabayarnya umum yang belum bayar)
//			if(empty($modPendaftaran->pembayaranpelayanan_id)){
//				if((($modPendaftaran->pasien->ispasienluar)&&($modPendaftaran->carabayar_id==Params::DEFAULT_CARABAYAR)) || (($modPendaftaran->instalasi_id==Params::INSTALASI_ID_RJ)&&($modPendaftaran->carabayar_id == Params::DEFAULT_CARABAYAR))){
//					$enableprint = false;
//				}
//			}
			
			//EHJ-3417
			// Kondisi Print FALSE (Jika pasien berasal dari pendaftaran luar dengan carabayar membayar yang belum bayar) 
			if(empty($modPendaftaran->pembayaranpelayanan_id)){
				if(($modPendaftaran->pasien->ispasienluar)&&($modPendaftaran->carabayar_id==Params::DEFAULT_CARABAYAR)){
					$enableprint = false;
				}
			}
			
            $this->render('hasilPemeriksaan',array('detailHasil'=>$detailHasil,
                                               'masukpenunjang'=>$modPasienMasukPenunjang,
                                               'pemeriksa'=>$pemeriksa,
                                               'caraPrint'=>$caraPrint,
												'enableprint'=>$enableprint,
                                                ));
	}
	public function actionHasilPeriksaPrint($idPendaftaran,$idPasien,$idPasienMasukPenunjang,$caraPrint='',$i=0)
	{   
            $this->layout = '//layouts/printWindows';
            $modPasienMasukPenunjang = ROPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $pemeriksa = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$modPasienMasukPenunjang->pegawai_id));
            $detailHasil = HasilpemeriksaanradT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            if(substr($modPendaftaran->no_pendaftaran, 0, 2) != 'LB'){
                $pasienKirimKeunitLain = PasienkirimkeunitlainT::model()->findByAttributes(array(
                    'pasienmasukpenunjang_id'=>$modPasienMasukPenunjang->pasienmasukpenunjang_id,
                ));
                $modPegawai = PegawaiM::model()->findByPk($pasienKirimKeunitLain->pegawai_id);
                $modPasienMasukPenunjang->namaperujuk = $modPegawai->nama_pegawai;// . ', ' . $modPegawai->gelarbelakang->gelarbelakang_nama;
                $modPasienMasukPenunjang->alamatlengkapperujuk = 'RS Jasa Kartini Tasikmalaya';
            }
            $this->render('hasilPrint',array('detailHasil'=>$detailHasil,
                                               'masukpenunjang'=>$modPasienMasukPenunjang,
                                               'pemeriksa'=>$pemeriksa,
                                               'caraPrint'=>$caraPrint,
                                                'i'=>$i,
                                                ));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
}