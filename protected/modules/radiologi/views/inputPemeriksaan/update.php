<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0
    )
));
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-radiologi-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatFormNumber();'),
        'focus'=>'#',
)); ?>
<h3>Pilih Pemeriksaan</h3>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<table class="table-condensed">
    <tr>
        <td>
            <div class="box">
                <?php

                    $jenisPeriksa = '';
                    foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                        $ceklist = $pemeriksaan->isChecked;
                        if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                            echo ($jenisPeriksa!='') ? "</div>" : "";
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                            echo "<div class='boxtindakan' style='width:290px;'>";
                            echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        } else {
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        }
                    } echo "</div>";
//                }
                ?>              
                
            </div>
        </td>
    </tr>
</table>
<div class="control-group">
    <?php echo CHtml::label('Dokter Pemeriksa','Dokter Pemeriksa',array('class'=>'control-label')); ?>
    <div class="controls">
        <?php
            $modPasienMasukPenunjang->pegawai_id = Params::DEFAULT_DOKTER_RAD;
            echo CHtml::dropDownList('ROPasienMasukPenunjangT[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3'));
        ?>
    </div>
</div>
<table id="tblFormPemeriksaanRad" class="table table-condensed">
    <thead>
        <tr>
            <th>Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>Satuan*</th>
            <th>Cyto</th>
            <th>Tarif Cyto</th>
        </tr>
    </thead>
    <?php
        $urlLoadForm = Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadMasuk');
        foreach ($modPeriksaRad as  $itemPeriksa) {
            if($itemPeriksa->isChecked){
                $idPemeriksaanRad=$itemPeriksa->pemeriksaanrad_id;
                $model->kelaspelayanan_id = Params::KELASPELAYANAN_ID_TANPA_KELAS;
            ?>
                <script>
                var idPemeriksaanrad = <?php echo $idPemeriksaanRad ?>;
                var idKelasPelayanan = <?php echo $model->kelaspelayanan_id ?>;
                $.post("<?php echo $urlLoadForm; ?> ", { idPemeriksaanrad: idPemeriksaanrad,idKelasPelayanan:idKelasPelayanan },
                function(data){
                    $('#tblFormPemeriksaanRad').append(data.form);
                    renameInput('permintaanPenunjang','inputpemeriksaanrad');
                    renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                    renameInput('permintaanPenunjang','inputqty');
                    renameInput('permintaanPenunjang','satuan');
                    renameInput('permintaanPenunjang','cyto');
                    renameInput('permintaanPenunjang','persencyto');
                    renameInput('permintaanPenunjang','tarifcyto');
                    renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                    renameInput('permintaanPenunjang','pemeriksaanNama');
                    renameInput('permintaanPenunjang','idDaftarTindakan');
                    hitungTotal();
                }, "json");

                </script>
    <?php 
            }
        } ?>
</table>

<table class="table table-bordered table-condensed">
    <tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', $modTindakanPelayan->tarif_satuan,array('class'=>'span2 currency', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
    <?php if(count($modKirimKeUnitLain) > 0) {?> 
    <tr>
        <td colspan="2">
            <div class="control-group ">
                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Perujuk :
                </label>
                <div class="controls"></div>
            </div> 
        <?php
            echo $form->dropDownListRow(
                $modKirimKeUnitLain,
                'pegawai_id',
                CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                array(
                    'empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);"
                )
            );
        ?>
        </td>
    </tr>
    <?php }else{ ?>
    <tr>
        <td colspan="2">
            <div class="control-group ">
                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Perujuk
                </label>
                <div class="controls">
                <?php
                    echo CHtml::dropDownList('RujukanT[pegawai_id]',
                        $modRujukan->rujukandari_id,
                        CHtml::listData(RujukandariM::model()->findAll(),'namaperujuk','namaperujuk'),
                        array('empty'=>'--Pilih--',
                            'class'=>'span3',
                     'readonly'=>false, 
                        'onkeypress'=>"return $(this).focusNextInputField(event);"));
                ?>
                </div>
            </div>
        </td>
    </tr>    
    <?php }?>
    
</table>
<table>
    <tr>
        <td>
            <br>
                <?php
                //FORM REKENING
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                        array(
                            'form'=>$form,
                            'modRekenings'=>$modRekenings,
                        )
                    );
                ?>
        </td>
    </tr>
</table>
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php 
            $id_pendaftaran = (isset($_GET['idPendaftaran']) ? $_GET['idPendaftaran'] :''); 
            $id_pasienpenunjang = (isset($_GET['idPasienMasukPenunjang']) ? $_GET['idPasienMasukPenunjang'] :''); 
//            if(!empty($id_pendaftaran) && !$model->isNewRecord){ 
            echo CHtml::link(Yii::t('mds', '{icon} Print Label', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\')')); 
            echo CHtml::link(Yii::t('mds', '{icon} Print Daftar Tindakan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('id'=>'btnPrint','class'=>'btn btn-info', 'onclick'=>'printTindakan(\'PRINT\')')); 
            // AUTOPRINT >>> echo "<script>setTimeout(function(){print(\"PRINT\");},500);</script>";
//            }
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('daftarPasien/index',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
</div>
        
<?php $this->endWidget(); ?>
<?php 
$caraPrint = 'PRINT';
$labelOnly = 1;
$urlPrint=  Yii::app()->createAbsoluteUrl('laboratorium/pendaftaranPasienLuar/print', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); //$this->module->id.
$urlPrintTindakan=  Yii::app()->createAbsoluteUrl('laboratorium/pendaftaranPasienLuar/printTindakan', array('id_pendaftaran'=>$id_pendaftaran,'id_pasienpenunjang'=>$id_pasienpenunjang, 'labelOnly'=>$labelOnly)); 
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
function printTindakan(caraPrint)
{
    window.open("${urlPrintTindakan}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
JSCRIPT;
    
Yii::app()->clientScript->registerScript('printLabelTindakan',$js,  CClientScript::POS_HEAD);
?>
<script type="text/javascript">
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = <?php echo Params::KELASPELAYANAN_ID_TANPA_KELAS; ?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadMasuk')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                         var daftartindakanId = $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).find('input[name$="[idDaftarTindakan]"]').val();;
                         getDataRekening(daftartindakanId,idKelasPelayanan,1,'tm');
                         hitungTotal();
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
        }else{
            $(obj).attr('checked', 'checked');
        }            
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanrad)
{
    var daftartindakan_id = $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).find('input[name$="[idDaftarTindakan]"]').val();
    removeRekeningTindakan(daftartindakan_id);
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}

function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parent().parent().find('.qty').val();
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parent().parent().find('.cyto').val();
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
	total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}
function unformatFormNumber(){
    $('#masukpenunjang-radiologi-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
}
function formatFormNumber(){
    $('#masukpenunjang-radiologi-form').find('.currency').each(function(){
        $(this).val(formatNumber($(this).val()));
    });
}
setTimeout(function(){formatFormNumber()},1000);
</script>

