<?php $data=ProfilrumahsakitM::model()->findByPk(Params::DEFAULT_PROFIL_RUMAH_SAKIT);?>
<table style="width:100%;">
    <tr>
        <td>
            <?php echo $this->renderPartial('application.views.headerReport.headerDefaultSurat'); ?>
        </td>
    </tr>
    <tr>
        <td>
            <center>
                <h3>HASIL PEMERIKSAAN RADIOLOGI</h3>
            </center><br/>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%">
                <tr>
                    <td>Tgl. Masuk</td><td width="45%">: <?php echo $modPasienMasukPenunjang->tglmasukpenunjang; ?></td>
                    <td>Tgl. Pemeriksaan</td><td>: <?php echo $detailHasil[0]->tglpemeriksaanrad; ?></td>
                </tr>
                <tr>
                    <td>No. Rekam Medis</td><td width="45%">: <?php echo $modPasienMasukPenunjang->no_rekam_medik; ?></td>
                    <td>No. Pendaftaran</td><td>: <?php echo $modPasienMasukPenunjang->no_pendaftaran; ?></td>
                </tr>
                <tr>
                    <td>Nama Pasien</td><td width="45%">: <?php echo $modPasienMasukPenunjang->nama_pasien; ?></td>
                    <td>Cara Bayar</td><td>: <?php echo $modPasienMasukPenunjang->carabayar_nama; ?></td>
                </tr>
                <tr>
                    <td>Jenis Kelamin</td><td width="45%">: <?php echo $modPasienMasukPenunjang->jeniskelamin; ?></td>
                    <td>Penjamin</td><td>: <?php echo $modPasienMasukPenunjang->penjamin_nama; ?></td>
                </tr>
                <tr>
                    <td>Umur</td><td width="45%">: <?php echo $modPasienMasukPenunjang->umur; ?></td>
                    <td>Pengirim</td><td>: <?php echo $modPasienMasukPenunjang->nama_dokterasal; ?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table>
                <tbody>
                    <tr>
                        <td>
                                <?php foreach($detailHasil as $i=>$detail): ?>
                            <table style="border:1px solid #000;">
                                <tr style="border-bottom: 1px solid #000;">
                                    <th colspan="3">Pemeriksaan : <?php echo $detail->pemeriksaanrad->pemeriksaanrad_nama; ?></th>
                                </tr>
                                <tr>
                                    <td width="150px;">Hasil Expertise</td>
                                    <td width="2px;">:</td>
                                    <td><?php echo $detail->hasilexpertise; ?></td>
                                </tr>
                                <tr>
                                    <td>Kesan</td>
                                    <td>:</td>
                                    <td><?php echo $detail->kesan_hasilrad; ?></td>
                                </tr>
                                <tr>
                                    <td>Kesimpulan</td>
                                    <td>:</td>
                                    <td><?php echo $detail->kesimpulan_hasilrad; ?></td>
                                </tr>
                            </table>
                                <?php endforeach; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $data->kabupaten->kabupaten_nama.', '.Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d'), 'yyyy-MM-dd'),'long',null); ?> 
        </td>
    </tr>
    <tr>
        <td>
            <table style="width:100%;">
                <tr>
                    <td style="width:50%;">
                                    <?php// echo $data->kabupaten->kabupaten_nama.', '.Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d'), 'yyyy-MM-dd'),'long',null); ?> 
                                    <br/><br/><br/><br/><br/> Printed By <?php echo LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai; ?>
                                    &nbsp;
                                    <?php echo date('Y/m/d H:i:s'); ?>
                                
                </td>
                <td style="width:50%;" align="center">
                    
                    Pemeriksa <?php echo '<br/><br/><br/><br/><br/>'.PendaftaranT::model()->findByPK($detailHasil[0]->pendaftaran_id)->pegawai->nama_pegawai; ?>
                </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
