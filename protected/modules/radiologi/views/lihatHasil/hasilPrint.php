<style>
    .watermark
    {
       background-image: url(http://localhost/ehospitaljk/images/watermark_print.png);
       background-position: center 350px;
       background-size: 300px; /* CSS3 only, but not really necessary if you make a large enough image */
       position: absolute;
       background-repeat: no-repeat;
       width: 100%;
       margin: 0;
       z-index: 1000;
    }
    
</style>
<?php // if (isset($caraPrint)){ ?>
<style>
    th {
        border: 1px solid;        
        background-color: transparent;
    }
    .grid tr{
        border: 1px solid;
        background-color: transparent;
    }
    th{
        text-align: center;
        font-size: 14px;
    }
    table{
        width: 100%;
    }
	p{
        line-height: 10px;
    }
    .title td{
        font-size: 16px;
        text-align: center;
        font-weight: bold;
        padding: 5px;
        background: #309C5C;
    }
</style>
<style>
    body {
    margin: 0;
    padding: 5px;
}
@page { 
    padding: 10px;
    margin-top: 2cm;
    margin-bottom: 1.5cm;
}
@media print {  
    @page {  
      size:208mm 314mm;  
    }  
}
p{
    /**font-size: 14pt;**/
   padding: 5px; font-size: 11pt; line-height:3px;
}
</style>
            
<?php if($modHasilPeriksa->printhasillab == '1') {echo '<div class="watermark">';}  ?>
<div style="height:0cm;">
    &nbsp;
</div>
<table style="width:100%;font-family: arial;">
     <tr ><?php $format=new CustomFormat(); $tgl = explode(" ",$masukpenunjang->tglmasukpenunjang); $tgl_periksa = $format->formatDateMediumForDB($tgl[0]." ".$tgl[1]." ".$tgl[2]);?>
    <td width="50%" style="border:none;"><center><?php echo "Tasikmalaya, ".$format->formatDateINA(date($tgl_periksa)); ?></center></td>
        <td width="15%" style="border:none; font-size: 8pt;">Penanggungjawab</td>
        <td width="35%" style="border:none; font-size: 8pt;">: <?php echo $pemeriksa->gelardepan." ".$pemeriksa->nama_pegawai.", ".$pemeriksa->gelarbelakang->gelarbelakang_nama; ?></td>
    </tr><br>
    <tr> 
        <td style="border:none; font-size: 8pt;" ></td>
        <td style="border:none; font-size: 8pt;">Izin</td>
        <td style="border:none; font-size: 8pt;">: <?php echo ProfilrumahsakitM::model()->findByPk(Yii::app()->user->getState('profilrs_id'))->noizinradiologi; ?></td>
    </tr>
    <tr>
</table><br><br>
<table style="font-family: arial;font-size: 10pt;" class="grid">
    <tr>
        <td width="10%" style="padding-left:10px;">No. Rad</td>
        <td width="40%" style="border-right:1px solid;">: <?php echo $masukpenunjang->no_pendaftaran; ?></td>
        <td width="15%">No. Pemeriksaan</td>
        <td>: <?php echo $masukpenunjang->no_masukpenunjang; ?></td>
    </tr>
    <tr>
        <td style="white-space: nowrap; padding-left:10px;">Nama</td>
        <td style="white-space: nowrap; border-right:1px solid;">: <?php echo $masukpenunjang->namadepan." ".$masukpenunjang->nama_pasien; ?></td>
        <td width="10%" style="padding-left:10px;">Dokter</td>
        <td>: <?php echo $masukpenunjang->namaperujuk; ?></td>
    </tr>
    <tr>
        <td style="padding-left:10px;">Umur </td>
        <td style="border-right:1px solid;">: <?php echo $masukpenunjang->umur."; ".$masukpenunjang->jeniskelamin; ?></td>
        <td style="padding-left:10px;">Alamat</td>
        <td>: <?php echo $masukpenunjang->alamatlengkapperujuk; ?></td>
    </tr>
    <tr>
        <td style="padding-left:10px;">Alamat</td>
        <td style="border-right:1px solid;">: <?php echo $masukpenunjang->alamat_pasien ?></td>
        <td style="padding-left:10px;">No. Telp</td>
        <td>: <?php echo $masukpenunjang->notelpperujuk; ?></td>
    </tr>
</table>
<div style="font-family:arial;font-size:12pt;">
    <b>
    <?php
        $modRuangan = RuanganM::model()->findByPk($masukpenunjang->ruanganasal_id);
        $modKamarruangan = KamarruanganM::model()->findByPk($modRuangan->ruangan_id);
        
        if($modRuangan->instalasi->instalasi_id == 4){
            echo $masukpenunjang->no_rekam_medik . '/' . $modRuangan->instalasi->instalasi_nama . '/'. $masukpenunjang->ruanganasal_nama . '/' . $modKamarruangan->kamarruangan_nokamar . '/' .$masukpenunjang->kelaspelayanan_nama;
        }else{
            echo $masukpenunjang->no_rekam_medik . '/' . $masukpenunjang->ruanganasal_nama . '/' . $masukpenunjang->kelaspelayanan_nama;
        }
    ?>
    <?php
        //echo $masukpenunjang->no_rekam_medik . '/' . $masukpenunjang->ruanganasal_nama . '/' . $masukpenunjang->kelaspelayanan_nama;
    ?>
    </b>
</div>
<br>
<table border="1" width="100%" cellpadding="0" cellspacing="0" class="title">
    <tr>
        <td width="50%">BAGIAN RADIOLOGI</td>
        <td><?php echo $pemeriksa->gelardepan." ".$pemeriksa->nama_pegawai.", ".$pemeriksa->gelarbelakang->gelarbelakang_nama; ?></td>
    </tr>
</table>
<table style="border:1px solid #000; margin:4px auto;"  width="100%">
    <tr style="border-bottom: 1px solid #000;">
        <td style="font-family: Arial; font-size: 14pt;font-weight: bold;"><?php echo $detailHasil[$i]->pemeriksaanrad->pemeriksaanrad_nama; ?></td>
    </tr>
    <tr>
        <td class="isi_hasil" style="padding: 10px; font-size: 14pt; line-height:21px;">
            <?php echo (strlen($detailHasil[$i]->hasilexpertise) > 0 ? $detailHasil[$i]->hasilexpertise : ' - '); ?>
        </td>
    </tr>
</table>
<br>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="page-break-inside: avoid;">
    <tr>
        <td align="left" width="50%">&nbsp;</td>
        <td align="center">PEMERIKSA</td>
    </tr>
    <tr>
        <td align="left">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            Printed By : <?=$masukpenunjang->getNamaPegawai(Yii::app()->user->getState('pegawai_id'))?> <?=date('d/m/Y H:i:s')?>
        </td>
        <td align="center">
            <?=$masukpenunjang->getNamaLengkapDokter($masukpenunjang->pegawai_id)?>
        </td>
    </tr>
</table>