<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
    if(!empty($_GET['succes']))
    {
?>

<div class="alert alert-block alert-success">
    <a class="close" data-dismiss="alert">×</a>
        <?php
            if($_GET['succes'] == 2)
            {
        ?>
            Pemeriksaan Pasien berhasil di batalkan
        <?php            
            }
            if($_GET['succes'] == 1){
        ?>
            Pasein Berhasil Di Rujuk
        <?php            
            }
        ?>
</div>

<?php     
    }
?>
<?php
if(isset($_GET['pendaftaran_id']) && isset($_GET['pasien_id'])){
    $pasienmasukpenunjang_id = $_GET['pasienmasukpenunjang_id'];
    $pendaftaran_id = $_GET['pendaftaran_id'];
    $pasien_id = $_GET['pasien_id'];
    $modDetailhasil = HasilpemeriksaanradT::model()->findAllByAttributes(array(
        'pendaftaran_id'=>$pendaftaran_id,
        'pasienmasukpenunjang_id'=>$pasienmasukpenunjang_id,
        'pasien_id'=>$pasien_id,
    ));
    $i = count($modDetailhasil)-1;
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/lihatHasil/HasilPeriksaPrint', array("idPendaftaran"=>$pendaftaran_id,"idPasien"=>$pasien_id,"idPasienMasukPenunjang"=>$pasienmasukpenunjang_id));
$js = <<< JSCRIPT
        
function print(caraPrint)
{
    if(caraPrint == 'PRINT'){
    var jumlah = ${i};
    jumlah++;
    var i = 0;
        for(var i=0;i < jumlah;i++){
            var konfirm = confirm("Apakah Anda Akan Mencetak Pemeriksaan Ke-"+(i+1)+" dari "+jumlah+" pemeriksaan ?");
            if(konfirm)
                window.open("${urlPrint}&i="+i+"&caraPrint="+caraPrint,"",'location=_new, width=1024px');
        }
    }
}
print('PRINT');               
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,  CClientScript::POS_HEAD);
}
?>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari cari', "
$('#daftarPasien-form').submit(function(){
        $('#daftarPasien-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
    <legend class="rim2">Informasi Daftar Pasien </legend>
<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$modPasienMasukPenunjang->searchRAD(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
//            'tgl_pendaftaran', menggunakan tgl masuk penunjang agar urutan pendaftaran sesuai jika dari RI,RJ,RD
            array(
                'header'=>'Tgl. Masuk Penunjang',
                'name'=>'tglmasukpenunjang',
            ),
            array(
                'header'=>'Tgl. Pendaftaran',
                'name'=>'tgl_pendaftaran',
                // 'value'=>'$data->pendaftaran->tgl_pendaftaran'
            ),            
            'ruanganasal_nama',
//            'nama_perujuk',
            array(
                'header'=>'Perujuk RS',
                'value'=>'$this->grid->owner->renderPartial("_dokterPerujuk",array(pasienmasukpenunjang_id=>$data->pasienmasukpenunjang_id),true)',
            ),
            array(
                'header'=>'Perujuk Luar',
                'value'=>'$data->namaperujuk',
            ),
            array(
                'name'=>'no_pendaftaran',
                'header'=>'No. Pendaftaran / No. Pemeriksaan Radiologi',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=\"icon-pencil-blue\"></i>$data->no_pendaftaran","",array("onclick"=>"statusPeriksa(\"$data->statusperiksa\",$data->pendaftaran_id,$data->pasienmasukpenunjang_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Pemeriksaan"))."<br/>".$data->no_masukpenunjang',
                'htmlOptions'=>array('width'=>'100px'),
            ),
            'no_rekam_medik',
            array(
                'header'=>'Nama Pasien / Alias',
                'type'=>'raw',
                'value'=> '((substr($data->no_rekam_medik,0,-6)) == "LB" || (substr($data->no_rekam_medik,0,-6)) == "RD" ? CHtml::link("<i class=\"icon-pencil-blue\"></i> ".$data->nama_pasien.\' / \'.$data->nama_bin, Yii::app()->createUrl("'.Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/ubahPasien",array("id"=>"$data->pasien_id","idpendaftaran"=>$data->pendaftaran_id,"modulId"=>"'.Yii::app()->session['modulId'].'")), array("rel"=>"tooltip","title"=>"Klik untuk mengubah data pasien")) : $data->nama_pasien.\' / \'.$data->nama_bin )',
            ),
//            array(
//                'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
//                'type'=>'raw',
//                'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
//            ),
//            'jeniskasuspenyakit_nama',
            array(
                'header'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->jeniskelamin',
            ),
            'umur',
            'golonganumur_nama',
            'alamat_pasien',
            // 'carabayar_nama',
           
            array(
                'header'=>'Cara Bayar / Penjamin',
                'value'=>'$data->caraBayarPenjamin',
            ),
            // 'statusperiksa',
            array(
                'header'=>'Status Periksa',
                'type'=>'raw',
                // 'value'=>'(empty($data->pasienbatalperiksa_id)) ? $data->statusperiksahasil :  "DIBATALKAN" ',
                'value'=>'($data->statusperiksa == "SUDAH PERIKSA") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>". $data->statusperiksahasil,Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/CancelPemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik untuk membatalkan pemeriksaan", "onclick"=>"return confirm(\"Apakah anda akan membatalkan pemeriksaan ini?\");")) : ((empty($data->pasienbatalperiksa_id)) ? $data->statusperiksa : "DIBATALKAN")',
            ),
            array(
                'header'=>'Dokter Periksa',
                'type'=>'raw',
//               RADIOLOGI TIDAK ADA APPROVE ?? >> 'value'=>'($data->statusperiksa == "SEDANG PERIKSA") ? CHtml::link("<i class=\"icon-pencil-blue\"></i>". $data->getNamaLengkapDokter($data->pegawai_id),Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/ApprovePemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik untuk approve pemeriksaan", "onclick"=>"return confirm(\"Apakah anda akan meng-approve pemeriksaan ini?\");")) : $data->getNamaLengkapDokter($data->pegawai_id)',
                'value'=>'$data->getNamaLengkapDokter($data->pegawai_id)',
            ),                        
             array(
                'name'=>'masukanHasil',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=icon-pencil-brown></i>",Yii::app()->controller->createUrl("/'.$module.'/'.$controller.'/hasilPemeriksaan",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),array("rel"=>"tooltip","title"=>"Klik Untuk Memasukkan hasil"))',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
             array(
                'name'=>'lihatHasil',
                'type'=>'raw',
                'value'=>'CHtml::link("<i class=icon-file-silver></i>",Yii::app()->controller->createUrl("lihatHasil/HasilPeriksa",array("idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id,"idPasienMasukPenunjang"=>$data->pasienmasukpenunjang_id)),
                            array("rel"=>"tooltip",
                                  "title"=>"Klik Untuk Melihat Hasil",
                                  "target"=>"iframeLihatHasil",
                                  "onclick"=>"$(\"#dialogLihatHasil\").dialog(\"open\");",
                            ))',    
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Print Label',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->createAbsoluteUrl("laboratorium/pendaftaranPasienLuar/print",array("id_pendaftaran"=>$data->pendaftaran_id,"id_pasienpenunjang"=>$data->pasienmasukpenunjang_id,"labelOnly"=>1,"caraPrint"=>"PRINT")),
                            array("class"=>"", 
                                  "target"=>"iframePrintLabel",
                                  "onclick"=>"$(\"#dialogDetailPrintLabel\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat hasil pemeriksaan", 
                            ))','htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
            array(
               'header'=>'Batal Periksa',
               'type'=>'raw',
               'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", "javascript:batalperiksa($data->pasienmasukpenunjang_id,$data->pendaftaran_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk membatalkan Pemeriksaan"))',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),

        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
    
<?php 
// Dialog untuk Lihat Hasil =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogLihatHasil',
    'options'=>array(
        'title'=>'Hasil Pemeriksaan Radiologi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>950,
        'minHeight'=>450,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeLihatHasil" width="100%" height="500">
</iframe>

<?php
$this->endWidget();
//========= end Lihat Hasil =============================
?>
    
<?php
 //CHtml::link($text, $url, $htmlOptions)
$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
        'id'=>'daftarPasien-form',
        'type'=>'horizontal',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),

)); ?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
              <?php echo CHtml::label('Tgl Pendaftaran','tglPendaftaran', array('class'=>'control-label')) ?>
                  <div class="controls">  
                   
                     <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAwal',
                                         'mode'=>'datetime',
//                                          'maxDate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                      
                   </div> 
                     <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>
                   <div class="controls">  
                    <?php $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPasienMasukPenunjang,
                                         'attribute'=>'tglAkhir',
                                         'mode'=>'datetime',
//                                         'maxdate'=>'d',
                                         'options'=> array(
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                         'htmlOptions'=>array('readonly'=>true,
                                         'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                    )); ?>
                   </div> 
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 
            </td>
            <td>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
                 <?php echo $form->textFieldRow($modPasienMasukPenunjang,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)", 'maxlength'=>50)); ?>
            </td>
            
        </tr>
    </table>
    <div class="form-actions">
<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));
     ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?><?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>
	 
</fieldset>  
<?php $this->endWidget();?>
</fieldset>


<script type="text/javascript">
{
//    function batalperiksa2(idPenunjang,idPendaftaran)//jangan di
//    {
//        var con = confirm('anda yakin akan membatalkan pemeriksaan radiologi pasien ini? ');
//        if(con){
//            $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . 'BatalPemeriksaan')?>',{idPendaftaran:idPendaftaran,idPenunjang:idPenunjang},
//            // $.post('<?php echo Yii::app()->createUrl('radiologi/daftarPasien/BatalPeriksaPasienLuar')?>',{idPendaftaran:idPendaftaran,idPenunjang:idPenunjang},          
//                      function(data){
//                          if(data.status == 'ok' && data.pesan != 'exist'){
//                              window.location = "<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/index&succes=2')?>";
//                          }else{
//                              if(data.pesan == 'exist')
//                              {
//                                  $('#dialogKonfirm div.divForForm').html(data.keterangan);
//                                  $('#dialogKonfirm').dialog('open');
// //                                 alert(data.keterangan);
//                              }
//                          }
//                      },'json'
//                  );
//        }         
//    }

    function batalperiksa(idPenunjang,idPendaftaran)
    {
       
       var con = confirm('anda yakin akan membatalkan pemeriksaan radiologi pasien ini? ');
       if(con){
           $.post('<?php echo Yii::app()->createUrl('radiologi/daftarPasien/BatalPeriksaPasienLuar')?>',{idPendaftaran:idPendaftaran,idPenunjang:idPenunjang},
                     function(data){
                         if(data.status == 'ok' && data.pesan != 'exist'){
                             window.location = "<?php echo Yii::app()->createUrl('radiologi/daftarPasien/index&succes=2')?>";
                         }else{
                             if(data.pesan == 'exist' && data.status == 'ok')
                             {
                                 $('#dialogKonfirm div.divForForm').html(data.keterangan);
                                 $('#dialogKonfirm').dialog('open');
    //                                 alert(data.keterangan);
                             }
                         }
                     },'json'
                 );
       }     
    }
}
function statusPeriksa(statusperiksa,pendaftaran_id,pasienmasukpenunjang_id){
//    alert(status);
    var statusperiksa = statusperiksa;
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = pasienmasukpenunjang_id;
    
    $.post("<?php echo Yii::app()->createUrl('radiologi/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa menambahkan tindakan transaksi. ");
            }else{
                window.location.href= "<?php echo Yii::app()->controller->createUrl('inputPemeriksaan/update',array()); ?>&idPendaftaran="+data.pendaftaran_id+"&idPasienMasukPenunjang="+data.pasienmasukpenunjang_id,"";
            }
    },"json");
    return false; 
}
</script>
<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKonfirm',
    'options'=>array(
        'title'=>'',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>500,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>

<?php 
// Dialog buat lihat penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailPrintLabel',
    'options'=>array(
        'title'=>'Print Label Pasien Radiologi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframePrintLabel" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end lihat penjualan resep dialog =============================
?>
