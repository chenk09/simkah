<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'hasilpmeriksaan-radiologi-form',
	'enableAjaxValidation'=>false,
        'enableClientValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#kolHasil_0',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

?>
<?php 
if(isset($_GET['sukses'])){
	Yii::app()->user->setFlash('success', "Data pasien berhasil disimpan !");
}
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); 
$this->renderPartial('/_ringkasDataPasien',array('modPasienMasukPenunjang'=>$modPasienMasukPenunjang));
?>

<div class="control-group ">
    <?php echo $form->labelEx($modHasilpemeriksaanRad[0],'tglpegambilanhasilrad', array('class'=>'control-label inline')) ?>
    <div class="controls">
        <?php   
                $this->widget('MyDateTimePicker',array(
                                'model'=>$modHasilpemeriksaanRad[0],
                                'attribute'=>'[0]tglpegambilanhasilrad',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'minDate' => 'd',
                                    //
                                ),
                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                ),
        )); ?>

    </div>
</div>

<table width="50%"  id="tblFormHasilPemeriksaanRad" class="table table-bordered table-condensed">
<!--    <thead>
        <tr>
            <th>Pemeriksaan</th>
            <th>Hasil Expertise</th>
            <th>Kesan</th>
            <th>Kesimpulan</th>
            <th>&nbsp;</th>
        </tr>
    </thead>-->
<?php foreach($modHasilpemeriksaanRad as $i=>$hasil): ?>
    <tr>
        <td colspan="3"><div style="text-align: center; font-size: 11pt;"><?php echo $hasil->pemeriksaanrad->pemeriksaanrad_jenis; ?> : <?php echo $hasil->pemeriksaanrad->pemeriksaanrad_nama; ?></div></td>
    </tr>
    <tr>
        <td style="font-size:10pt; ">
            <?php echo CHtml::css('ul.redactor_toolbar{z-index:10;}'); ?>
            <?php // echo $hasil->pemeriksaanrad->pemeriksaanrad_jenis; ?> 
            Hasil Expertise<br/>
            <b><?php // echo $hasil->pemeriksaanrad->pemeriksaanrad_nama; ?></b> <br/>
            <?php echo CHtml::activeHiddenField($hasil, "[$i]hasilpemeriksaanrad_id", array('readonly'=>true)); ?>
            <?php // echo $hasil->tglpemeriksaanrad; ?>
        </td>
        <td id="kolHasil_<?php echo $i;?>" style="text-align:center;">
            <?php // echo CHtml::activeTextArea($hasil, "[$i]hasilexpertise", array('rows'=>3, 'style'=>'width:750px; font-size:11pt;', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$hasil,'attribute'=>'['.$i.']hasilexpertise','name'=>'ROHasilPemeriksaanRadT_'.$i.'_hasilexpertise','toolbar'=>'mini','height'=>'300px')) ?>
        </td>
        <td style="text-align:center; vertical-align: middle;"><?php echo CHtml::button('Referensi', array('onclick'=>"$('#row').val(".$i."); $('#dialogReferensi').dialog('open');return false;",'class'=>'btn btn-info','disabled'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
    </tr>
    <!--
    CATATAN:
    KHUSUS UNTUK RSJK, TIDAK MENAMPILKAN KESAN DAN KESIMPULAN
    <tr>
        <td style="font-size:10pt; ">Kesan</td>
        <td id="kolKesan_<?php //echo $i;?>" style="text-align:center;">: 
            <?php // echo CHtml::activeTextArea($hasil, "[$i]kesan_hasilrad", array('rows'=>3, 'style'=>'width:750px; font-size:11pt;', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php //$this->widget('ext.redactorjs.Redactor',array('model'=>$hasil,'attribute'=>'['.$i.']kesan_hasilrad','name'=>'ROHasilPemeriksaanRadT_'.$i.'_kesan_hasilrad','toolbar'=>'mini','height'=>'300px')) ?>
        </td>
    </tr>
    <tr>
        <td style="font-size:10pt; ">Kesimpulan</td>
        <td id="kolKesimpulan_<?php //echo $i;?>" style="text-align:center;">: 
            <?php // echo CHtml::activeTextArea($hasil, "[$i]kesimpulan_hasilrad", array('rows'=>3, 'style'=>'width:750px; font-size:11pt;', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php //$this->widget('ext.redactorjs.Redactor',array('model'=>$hasil,'attribute'=>'['.$i.']kesimpulan_hasilrad','name'=>'ROHasilPemeriksaanRadT_'.$i.'_kesimpulan_hasilrad','toolbar'=>'mini','height'=>'300px')) ?>
        </td>
    </tr>
    -->
<?php endforeach;?>
</table>
<table>
    <tr>
        <?php if($modRujukan->rujukandari_id){ ?>
        <td>
            <div class="control-group">
                <?php echo CHtml::label('Dokter Perujuk','Dokter Perujuk',array('class'=>'control-label')); ?>
                <div class="controls">
                        <?php
//                           echo CHtml::dropDownList('ROPasienKirimKeUnitLainT[pegawai_id]',$modPasienKirimKeUnitLain->pegawai_id,CHtml::listData(DokterV::model()->findAll(),'pegawai_id','nama_pegawai'),array('empty'=>'--Pilih--','style'=>'width:160px;', 'readonly'=>false));
                           echo CHtml::activeHiddenField($modRujukan,'asalrujukan_id', array('class'=>'span1'));
                           echo CHtml::activeHiddenField($modRujukan,'rujukandari_id', array('class'=>'span1'));
//                           echo CHtml::dropDownList('RORujukanT[rujukandari_id]',$modPasienMasukPenunjang->rujukandari_id,CHtml::listData(RujukandariM::model()->findAllByAttributes(array('asalrujukan_id'=>$modPasienMasukPenunjang->asalrujukan_id)),'rujukandari_id','namaperujuk'),array('empty'=>'--Pilih--','class'=>'span3'));
                           echo $form->dropDownList($modRujukan,'rujukandari_id',CHtml::listData(RujukandariM::model()->findAll(),'rujukandari_id','namaperujuk'),array('empty'=>'--Pilih--','class'=>'span3', 'onchange'=>'konfirmUbahDokterPerujuk(this);'));
//                           echo CHtml::textField('RORujukanT[rujukandari_id]',$modPasienMasukPenunjang->namaperujuk,array('empty'=>'--Pilih--','class'=>'span3', 'readonly'=>true));

                        ?>
                </div>
            </div>
        </td>
        <?php } ?>
        <td>
            <div class="control-group">
                <?php echo CHtml::label('Dokter Pemeriksa','Dokter Pemeriksa',array('class'=>'control-label')); ?>
                <div class="controls">
                        <?php
                            echo CHtml::activeHiddenField($modPasienMasukPenunjang,'pegawai_id', array('class'=>'span1'));
                           echo CHtml::dropDownList('ROPasienMasukPenunjangT[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>  Yii::app()->user->getState('ruangan_id'))),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3', 'readonly'=>false, 'onchange'=>'konfirmUbahDokterPemeriksa(this);'));
//                           echo $form->dropDownList($modPasienMasukPenunjang,'pegawai_id',CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>  Yii::app()->user->getState('ruangan_id'))),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3', 'readonly'=>false));
                        ?>
                </div>
            </div>
        </td>
    </tr>
</table>
    <div class='form-actions'>
		<?php
		if(isset($_GET['status'])){
			 echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                          array('class'=>'btn btn-primary','disabled'=>true)); 
		}else{
			 echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                          array('class'=>'btn btn-primary', 'type'=>'submit', 
                                          'onKeypress'=>'return formSubmit(this,event)',
                                          'id'=>'btn_simpan')); 
		}
		?>
    	<?php // echo CHtml::link(Yii::t('mds', '{icon} Batal', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl(''), array('class'=>'btn btn-danger')); ?>
	<?php echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('index',array('modulId'=>Yii::app()->session['modulId'])), array('class'=>'btn btn-danger')); ?>
	<?php	
//		echo ($enableprint)?
//		CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'onclick'=>'print(\'PRINT\');'))." &nbsp&nbsp ":
//		CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info', 'disabled'=>true))." &nbsp&nbsp ";
	?>
		<?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')),
              array('class'=>'btn btn-info','onclick'=>"printPemeriksaan(".$modPasienMasukPenunjang->pendaftaran_id.",
                    ".$modPasienMasukPenunjang->pasienmasukpenunjang_id.",".$modPasienMasukPenunjang->pasien_id.");return false")); ?>
    </div>
<?php $this->endWidget(); ?>

<?php
$urlPrintPemeriksaan = Yii::app()->createUrl('radiologi/lihatHasil/HasilPeriksa/',array());
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/lihatHasil'.'/HasilPeriksaPrint', array("idPendaftaran"=>$modPasienMasukPenunjang->pendaftaran_id,"idPasien"=>$modPasienMasukPenunjang->pasien_id,"idPasienMasukPenunjang"=>$modPasienMasukPenunjang->pasienmasukpenunjang_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    if(caraPrint == 'PRINT'){
    var jumlah = ${i};
    jumlah++;
    var i = 0;
        for(var i=0;i < jumlah;i++){
            var konfirm = confirm("Apakah Anda Akan Mencetak Pemeriksaan Ke-"+(i+1)+" dari "+jumlah+" pemeriksaan ?");
            if(konfirm)
                window.open("${urlPrint}&i="+i+"&caraPrint="+caraPrint,"",'location=_new, width=1024px');
        }
    }
}
function printPemeriksaan(idPendaftaran,idPasienMasukPenunjang,idPasien)
{
	window.open('${urlPrintPemeriksaan}&idPendaftaran='+idPendaftaran+'&idPasienMasukPenunjang='+idPasienMasukPenunjang+'&idPasien='+idPasien,'printwin','left=100,top=100,width=800,height=590, scrollbars=1');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,  CClientScript::POS_HEAD);
?>

<script>
	
$(document).ready(function(){
	<?php if(isset($_GET['sukses'])){ ?>
			printPemeriksaan("<?= $modPasienMasukPenunjang->pendaftaran_id ?>","<?= $modPasienMasukPenunjang->pasienmasukpenunjang_id ?>","<?= $modPasienMasukPenunjang->pasien_id ?>")
	<?php } ?>
});

function ambilReferensi(idPemeriksaanRad,row)
{
    $.post("<?php echo Yii::app()->createUrl('ActionAjax/GetReferensiHasilRad');?>",{idPemeriksaanRad: idPemeriksaanRad},
        function(data){
           //menambahkan nilai ke elemen yang di hide oleh widget redactor.js
           $('#ROHasilPemeriksaanRadT_'+row+'_hasilexpertise').val(data.refhasilrad_hasil); 
           $('#ROHasilPemeriksaanRadT_'+row+'_kesan_hasilrad').val(data.refhasilrad_kesan); 
           $('#ROHasilPemeriksaanRadT_'+row+'_kesimpulan_hasilrad').val(data.refhasilrad_kesimpulan); 
           //menambahkan nilai referensi ke masukan hasil pemeriksaan
           $('#kolHasil_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_hasil); 
           $('#kolKesan_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_kesan); 
           $('#kolKesimpulan_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_kesimpulan); 
    },"json");
}
//load referensi berdasarkan refhasilrad_id dan pegawai_id
function ambilReferensiDokter(refhasilradId, pegawaiId,row)
{
    $.post("<?php echo Yii::app()->createUrl('ActionAjax/GetReferensiHasilRadDokter');?>",{refhasilradId : refhasilradId, pegawaiId : pegawaiId},
        function(data){
           //menambahkan nilai ke elemen yang di hide oleh widget redactor.js
           $('#ROHasilPemeriksaanRadT_'+row+'_hasilexpertise').val(data.refhasilrad_hasil); 
           $('#ROHasilPemeriksaanRadT_'+row+'_kesan_hasilrad').val(data.refhasilrad_kesan); 
           $('#ROHasilPemeriksaanRadT_'+row+'_kesimpulan_hasilrad').val(data.refhasilrad_kesimpulan); 
           //menambahkan nilai referensi ke masukan hasil pemeriksaan
           $('#kolHasil_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_hasil); 
           $('#kolKesan_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_kesan); 
           $('#kolKesimpulan_'+row ).find('iframe').contents().find('#page').html(data.refhasilrad_kesimpulan); 
    },"json");
}

function konfirmUbahDokterPerujuk(obj){
    var sblm = $('#RORujukanT_rujukandari_id').val();
    if(!confirm('Apakah anda yakin akan merubah Dokter Perujuk ?')){
        obj.value = sblm;
    };
}
function konfirmUbahDokterPemeriksa(obj){
    var sblm = $('#ROPasienMasukPenunjangV_pegawai_id').val();
    if(!confirm('Apakah anda yakin akan merubah Dokter Pemeriksa ?')){
        obj.value = sblm;
    };
}
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogReferensi',
    'options'=>array(
        'title'=>'Pencarian Referensi Dokter: '.PegawaiM::model()->findByPk($modPasienMasukPenunjang->pegawai_id)->NamaLengkap,
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));
echo CHtml::hiddenField('row',null,array('readonly'=>true)); //baris tujuan masukan referensi
$modReferensiDialog = new ROReferensiHasilRadM('searchDialog');
$modReferensiDialog->unsetAttributes();
$format = new CustomFormat();
if (isset($_GET['ROReferensiHasilRadM'])){
    $modReferensiDialog->attributes = $_GET['ROReferensiHasilRadM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'obatAlkesDialog-m-grid',
    'dataProvider'=>$modReferensiDialog->searchDialog(),
    'filter'=>$modReferensiDialog,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectObat",
                "onClick"=>"
                            var row = $(\"#row\").val();
                            ambilReferensiDokter($data->refhasilrad_id, $data->pegawai_id, row);
                            $(\"#dialogReferensi\").dialog(\"close\");
                            return false;
                ",
               ))'
        ),
        'refhasilrad_kode',
        'refhasilrad_hasil',
        /*
        'refhasilrad_kesan',
        'refhasilrad_kesimpulan',
        'refhasilrad_keterangan',
        */
        
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>