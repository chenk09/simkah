<?php
//$arrMenu = array();
//                array_push($arrMenu,array('label'=>Yii::t('mds','Search Patient'), 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
//$this->menu=$arrMenu;
//$this->widget('bootstrap.widgets.BootAlert');
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");?>
<fieldset>
    <legend class="rim2">Informasi Pasien Kunjungan</legend>
    <table class="table-condensed">
    <tr>
        <td><?php echo CHtml::radioButton('instalasi', $cekRJ, array('value'=>'RJ','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Jalan</td>
        <td><?php echo CHtml::radioButton('instalasi', $cekRI, array('value'=>'RI','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Inap</td>
        <td><?php echo CHtml::radioButton('instalasi', $cekRD, array('value'=>'RD','onClick'=>'submitForm(this)','class'=>'ceklis')) ?> Rawat Darurat</td>
    </tr>
</table>
</fieldset>
<?php 
     if($cekRJ == TRUE)
     {
       echo $this->renderPartial('_formCariRJ', array('model'=>$modRJ,'form'=>$form),true); 
     }
     elseif($cekRI == TRUE)
     {
       
       echo $this->renderPartial('_formCariRI', array('model'=>$modRI,'form'=>$form),true); 
     }
     elseif($cekRD == TRUE)
     {
       echo $this->renderPartial('_formCariRD', array('model'=>$modRD,'form'=>$form),true); 
     }
?>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                    array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
             <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        $this->createUrl('pencarianPasien/index'), array('class'=>'btn btn-danger')); ?>
			<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
		
    </div>



<?php $this->endWidget(); ?>

<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$url=  Yii::app()->createAbsoluteUrl($module.'/'.$controller);
$cetak = Yii::app()->createUrl('pendaftaranPenjadwalan/pencarianPasien/printKartu',array('id'=>''));
$js = <<< JSCRIPT
function submitForm(obj)
{
    $('#patokanInstalasi').val(obj.value);
    $(obj).closest('form').submit();
}
// ==* Fungsi Print *== //

function print(id,umur)
   {    
               window.open('${url}/printKartu/id/'+id+'/umur/'+umur,'printwin','left=100,top=100,width=355,height=255,scrollbars=0');
   }

JSCRIPT;

Yii::app()->clientScript->registerScript('jsPencarianPasien',$js, CClientScript::POS_HEAD);
?>
<script>
function statusPeriksa(statusperiksa,pendaftaran_id){
//    alert(status);
    var statusperiksa = statusperiksa;
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('radiologi/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan. ");
            }else{
                window.location.href= "<?php echo Yii::app()->createUrl('radiologi/pendaftaranPasienKunjungan',array()); ?>&idPendaftaran="+data.pendaftaran_id,"";
            }
    },"json");
    return false; 
}
</script>