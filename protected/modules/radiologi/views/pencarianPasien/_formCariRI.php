<legend class="rim">Pasien Rawat Inap</legend>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRI(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran'
                    ),
                    array(
                       'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'value'=>'$data->instalasi_nama',
                    ),
                    array(
                       'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                    ),
                    array(
                       'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                       'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    array(
                       'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                    ),
                    array(
                       'name'=>'penjamin_nama',
                        'type'=>'raw',
                        'value'=>'$data->penjamin_nama',
                    ),
                    array(
                        'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
                        'type'=>'raw',
                        'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
                    ),
                    array(
                       'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                       'header'=>'Pemeriksaan',
                        'type'=>'raw',
                        'value'=>'CHtml::link("<i class=\"icon-user\"></i>","",array("onclick"=>"statusPeriksa(\"$data->statusperiksa\",$data->pendaftaran_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Periksa Ke Radiologi"))',
//                        'value'=>'CHtml::Link("<i class=\"icon-user\"></i>",Yii::app()->controller->createUrl("pendaftaranPasienKunjungan/",array("idPendaftaran"=>$data->pendaftaran_id)),
//                                    array("class"=>"", 
//                                          "id" => "selectPasien",
//                                          "rel"=>"tooltip",
//                                          "title"=>"Klik untuk periksa ke Radiologi",
//                                          "target"=>"",
//                                    ))',                
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px')
//                        'value'=>'CHtml::link("<i class=\"icon-user\"></i>", "javascript:daftarPasienKunjungan(\'$data->pendaftaran_id\');", array("rel"=>"tooltip","title"=>"Klik untuk periksa ke Radiologi"))',
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr></hr>

<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?>