<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pemeriksaan-radiologi-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php  

    if(!empty($_GET['id'])){
?>
    <div class="mds-form-message error">
                    <?php Yii::app()->user->setFlash('success',"Data berhasil disimpan"); ?>
                </div>
<? } ?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td>
            <div class="box">
                <?php
                    $jenisPeriksa = '';
                    foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                        $ceklist = (!empty($arrInputPemeriksaan)) ? cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan):false;
                        if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                            echo ($jenisPeriksa!='') ? "</div>" : "";
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                            echo "<div class='boxtindakan'>";
                            echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        } else {
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        }
                    } echo "</div>";
//                }
                ?>              
                
            </div>
        </td>
    </tr>
</table>
    <table id="tblFormPemeriksaanRad" class="table table-condensed">
        <thead>
            <tr>
                <th>Pemeriksaan</th>
                <th>Tarif</th>
                <th>Qty</th>
                <th>Satuan*</th>
                <th>Cyto</th>
                <th>Tarif Cyto</th>
            </tr>
        </thead>
        <tbody>
            
        </tbody>
    </table>


    <table class="table table-bordered table-condensed">
        <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
    </table>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>
        
<?php $this->endWidget(); ?>

<script type="text/javascript">
setTimeout(function(){
    hitungTotal();
}, 1000);
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = $('#idKelasPelayanan').val();
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadMasuk')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                 } ,
                 'cache':false});
        setTimeout(function(){hitungTotal();},500);
//        hitungTotal();
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }
        else
            $(obj).attr('checked', 'checked');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}
function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = unformatNumber($(this).parent().parent().find('.qty').val());
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = unformatNumber($(this).parent().parent().find('.cyto').val());
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
 
    $('#periksaTotal').val(formatNumber(total));    
}
</script>


<?php
function cekPilihan($pemeriksaanrad_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanrad_id == $item->pemeriksaanrad_id) $cek = true;
        }
    }
    return $cek;
}
?>
    <?php } ?>