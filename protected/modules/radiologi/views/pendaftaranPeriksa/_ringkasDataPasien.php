<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<fieldset>
      <legend class="rim2">Pendaftaran Pemeriksaan Pasien Radiologi</legend>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'tgl_pendaftaran', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?></td>
            <td>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'model'=>$modPasien,
                                    'attribute'=>'no_rekam_medik',
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('laboratorium/ActionAutoComplete/daftarPasien').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                return false;
                                            }',
                                    ),
                                )); 
                ?>
            </td>
            <td rowspan="4">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'no_pendaftaran', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_pasien', array('readonly'=>true)); ?></td>
        </tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'jeniskasuspenyakit_nama', array('readonly'=>true)); ?></td>
        
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_bin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'kelaspelayanan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'kelaspelayanan_id', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'umur', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'carabayar_id', array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'jeniskelamin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'penjamin_id', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::Label('Dokter Pemeriksa', 'Dokter Pemeriksa',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'pegawai_id', array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset>
<?php echo CHtml::activeHiddenField($modPendaftaran, 'pendaftaran_id',array('readonly'=>true)); ?>
<?php echo CHtml::hiddenField('idPasienKirimKeunitLain', '',array('readonly'=>true)); ?>
<?php echo CHtml::hiddenField('idKelasPelayanan', '',array('readonly'=>true)); ?>
<hr/>

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#idKelasPelayanan').val(data.kelaspelayanan_id);
    $('#idPasienKirimKeunitLain').val(data.pasienkirimkeunitlain_id);
    $('#<?php echo CHtml::activeId($modPasien, 'nama_pasien') ?>').val(data.nama_pasien);
    $('#<?php echo CHtml::activeId($modPasien, 'nama_bin') ?>').val(data.nama_bin);
    $('#<?php echo CHtml::activeId($modPasien, 'jeniskelamin') ?>').val(data.jeniskelamin);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pendaftaran_id') ?>').val(data.pendaftaran_id);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'tgl_pendaftaran') ?>').val(data.tgl_pendaftaran);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'umur') ?>').val(data.umur);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'no_pendaftaran') ?>').val(data.no_pendaftaran);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'jeniskasuspenyakit_nama') ?>').val(data.jeniskasuspenyakit_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'carabayar_id') ?>').val(data.carabayar_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'kelaspelayanan_id') ?>').val(data.kelaspelayanan_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'penjamin_id') ?>').val(data.penjamin_nama);
    $('#<?php echo CHtml::activeId($modPendaftaran, 'pegawai_id') ?>').val(data.nama_pegawai);
}
</script>

<?php

$js = <<< JS
$('#cekRiwayatPasien').change(function(){
        $('#divRiwayatPasien').slideToggle(500);
});
JS;
Yii::app()->clientScript->registerScript('JSriwayatPasien',$js,CClientScript::POS_READY);
?>
