<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-radiologi-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td>
            <div class="box">
                <?php
                foreach ($modRiwayatKirimKeUnitLain as $i => $riwayat) {
                    $modPermintaan = PermintaankepenunjangT::model()->with('daftartindakan','pemeriksaanrad')->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$riwayat->pasienkirimkeunitlain_id));
                    foreach($modPermintaan as $j => $permintaan){
                        $modTarif = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$riwayat->kelaspelayanan_id,
                                                                                    'daftartindakan_id'=>$permintaan->pemeriksaanrad->daftartindakan_id,
                                                                                    'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
                        $tarif[$i][$j] = (!empty($modTarif->harga_tariftindakan)) ? $modTarif->harga_tariftindakan : 0 ;
                        $persencyto[$i][$j] = (!empty($modTarif->persencyto_tind)) ? $modTarif->persencyto_tind : 0 ;
                    }
                    $arrInputPemeriksaan[$i] = $modPermintaan;
                }

//                if(!empty($arrInputPemeriksaan)) {
                    $jenisPeriksa = '';
                    foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                        $ceklist = (!empty($arrInputPemeriksaan)) ? cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan):false;
                        if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                            echo ($jenisPeriksa!='') ? "</div>" : "";
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                            echo "<div class='boxtindakan' style='width:292px;'>";
                            echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        } else {
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        }
                    } echo "</div>";
//                }
                ?>              
                
            </div>
        </td>
        </tr>
</table>
<div class="control-group">
    <?php echo CHtml::label('Dokter Pemeriksa','Dokter Pemeriksa',array('class'=>'control-label')); ?>
    <div class="controls">
        <?php
            $modPasienMasukPenunjang->pegawai_id = Params::DEFAULT_DOKTER_RAD;
            echo CHtml::dropDownList('ROPasienMasukPenunjang[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3'));
        ?>
    </div>
</div>
<table id="tblFormPemeriksaanRad" class="table table-condensed">
    <thead>
        <tr>
            <th>Pemeriksaan</th>
            <th>Tarif</th>
            <th>Qty</th>
            <th>Satuan*</th>
            <th>Cyto</th>
            <th>Tarif Cyto</th>
        </tr>
    </thead>
<?php
if(!empty($arrInputPemeriksaan)) {
    foreach ($arrInputPemeriksaan as $i => $itemPeriksa) {
        foreach ($itemPeriksa as $j => $item) {
?>
        <tr id="periksarad_<?php echo $item->pemeriksaanrad_id; ?>">
            <td>
                <?php echo CHtml::hiddenField("permintaanPenunjang[$item->pemeriksaanrad_id][idPasienKirimKeUnitLain]", $item->pasienkirimkeunitlain_id,array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
                <?php echo $item->pemeriksaanrad->pemeriksaanrad_jenis; ?><br/>
                <?php echo CHtml::hiddenField("permintaanPenunjang[$item->pemeriksaanrad_id][idDaftarTindakan]", $item->pemeriksaanrad->daftartindakan_id,array('class'=>'inputFormTabel','readonly'=>true)); ?>
                <?php echo $item->pemeriksaanrad->pemeriksaanrad_nama; ?>
                <?php echo CHtml::hiddenField("permintaanPenunjang[$item->pemeriksaanrad_id][inputpemeriksaanrad]", $item->pemeriksaanrad_id,array('class'=>'inputFormTabel','readonly'=>true)); ?>
            </td>
            <td>
                <?php echo CHtml::textField("permintaanPenunjang[$item->pemeriksaanrad_id][inputtarifpemeriksaanrad]", $tarif[$i][$j],array('class'=>'inputFormTabel lebar3 currency tarif','readonly'=>true)); ?>
            </td>
            <td><?php echo CHtml::textField("permintaanPenunjang[$item->pemeriksaanrad_id][inputqty]", $item->qtypermintaan,array('class'=>'inputFormTabel lebar1 number qty', 'onkeyup'=>'hitungCyto(this); hitungTotal();')); ?></td>
            <td><?php echo CHtml::dropDownList("permintaanPenunjang[$item->pemeriksaanrad_id][satuan]", '', SatuanTindakan::items(),array('class'=>'inputFormTabel lebar3',)); ?></td>
            <td>
                <?php echo CHtml::dropDownList("permintaanPenunjang[$item->pemeriksaanrad_id][cyto]", '0',array('1'=>'Ya','0'=>'Tidak'),array('class'=>'inputFormTabel lebar2-5 isCyto','onclick'=>'cyto_tarif(this,'.$idPemeriksaanRad.')','onchange'=>'hitungCyto(this); hitungTotal();')); ?>
                <?php echo CHtml::hiddenField("permintaanPenunjang[$item->pemeriksaanrad_id][persencyto]", $persencyto[$i][$j],array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
            </td>
            <td><?php echo CHtml::textField("permintaanPenunjang[$item->pemeriksaanrad_id][tarifcyto]", '',array('class'=>'inputFormTabel lebar2-5 cyto cyto_tarif_'.$idPemeriksaanRad,'readonly'=>true, 'style'=>'display:none;', 'onchange'=>'hitungTotal();')); ?></td>
        </tr>
<?php } } }?>
</table>
<table class="table table-bordered table-condensed">
    <tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', 0,array('class'=>'span2', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
	<tr>
        <td colspan="2">
            <div class="control-group ">
                <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                Dokter Perujuk :
                </label>
                <div class="controls ">
					<?php echo $form->dropDownList($modRiwayatKirimKeUnitLain,'pegawai_id', CHtml::listData($modRiwayatKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'), array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </div>
            </div> 
        </td>
    </tr>
</table>
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/PencarianPasien/Index&modulId=9'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
</div>
        
    
<?php $this->endWidget(); ?>

<script type="text/javascript">
    setTimeout(function(){
    hitungTotal();
}, 1000);
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = <?php echo Params::kelasPelayanan('tanapa_kelas'); ?><?php // echo $modPendaftaran->kelaspelayanan_id ?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadMasuk')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','idPasienKirimKeUnitLain');
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                 } ,
                 'cache':false});
        setTimeout(function(){hitungTotal();},500);
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }
        else
            $(obj).attr('checked', 'checked');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}
function cyto_tarif(test, x){
    if(test.value == 1){
        $('.cyto_tarif_'+ x).show();
    }else{
        $('.cyto_tarif_'+ x).hide();
    }
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parents('tr').find('.persenCyto').val());
    tarif = unformatNumber($(obj).parents('tr').find('.tarif').val());
    qty = unformatNumber($(obj).parents('tr').find('.qty').val());
    isCyto = unformatNumber($(obj).parents('tr').find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parents('tr').find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parents('tr').find('.cyto').val(0);
}
function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = $(this).parents('tr').find('.qty').val();
            isCyto = $(this).parents('tr').find('.isCyto').val();
            if(isCyto == 1){
                cyto = $(this).parents('tr').find('.cyto').val();
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
	total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}
</script>


<?php
function cekPilihan($pemeriksaanrad_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanrad_id == $item->pemeriksaanrad_id) $cek = true;
        }
    }
    return $cek;
}
?>
