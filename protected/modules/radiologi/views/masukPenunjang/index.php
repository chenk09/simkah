<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'masukpenunjang-radiologi-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatFormNumber();'),
        'focus'=>'#',
)); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<table class="table-condensed">
    <tr>
        <td>
            <div class="box">
                <?php
                foreach ($modRiwayatKirimKeUnitLain as $i => $riwayat)
                {
                    $modPermintaan = PermintaankepenunjangT::model()->with('daftartindakan','pemeriksaanrad')->findAllByAttributes(array('pasienkirimkeunitlain_id'=>$riwayat->pasienkirimkeunitlain_id));
                    $arrInputPemeriksaan[$i] = $modPermintaan;
                    
                    foreach($modPermintaan as $j => $permintaan)
                    {
                        $modTarif = TariftindakanM::model()->findByAttributes(
							array(
								'kelaspelayanan_id'=>Params::kelasPelayanan('tanapa_kelas'),
								'daftartindakan_id'=>$permintaan->pemeriksaanrad->daftartindakan_id,
								'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL
							)
						);
                        $tarif[$i][$j] = (!empty($modTarif->harga_tariftindakan)) ? $modTarif->harga_tariftindakan : 0 ;
                        $persencyto[$i][$j] = (!empty($modTarif->persencyto_tind)) ? $modTarif->persencyto_tind : 0 ;
                    }
                }

//                if(!empty($arrInputPemeriksaan)) {
                    $jenisPeriksa = '';
                    foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                        $ceklist = (!empty($arrInputPemeriksaan)) ? cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan):false;
                        if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                            echo ($jenisPeriksa!='') ? "</div>" : "";
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                            echo "<div class='boxtindakan' style='width:292px;'>";
                            echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        } else {
                            $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                                echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                                         'onclick' => "inputperiksa(this)"));
                            echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                        }
                    } echo "</div>";
//                }
                ?>              
                
            </div>
        </td>
    </tr>
</table>
<div class="control-group">
    <?php echo CHtml::label('Dokter Pemeriksa','Dokter Pemeriksa',array('class'=>'control-label')); ?>
    <div class="controls">
        <?php
            $modPasienMasukPenunjang->pegawai_id = Params::DEFAULT_DOKTER_RAD;
            echo CHtml::dropDownList('ROPasienMasukPenunjangT[pegawai_id]',$modPasienMasukPenunjang->pegawai_id,CHtml::listData(DokterV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')),'pegawai_id','NamaLengkap'),array('empty'=>'--Pilih--','class'=>'span3'));
        ?>
    </div>
</div>
    <table id="tblFormPemeriksaanRad" class="table table-condensed">
        <thead>
            <tr>
                <th>Pemeriksaan</th>
                <th>Tarif</th>
                <th>Qty</th>
                <th>Satuan*</th>
                <th>Cyto</th>
                <th>Tarif Cyto</th>
            </tr>
        </thead>
    <?php
    if(!empty($arrInputPemeriksaan)) {
        foreach ($arrInputPemeriksaan as $i => $itemPeriksa) {
            foreach ($itemPeriksa as $j => $item) {
    ?>
            <tr id="periksarad_<?php echo $item->pemeriksaanrad_id; ?>">
                <td>
                    <?php echo CHtml::hiddenField("permintaanPenunjang[$j][idPasienKirimKeUnitLain]", $item->pasienkirimkeunitlain_id,array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
                    <?php echo $item->pemeriksaanrad->pemeriksaanrad_jenis; ?><br/>
                    <?php echo CHtml::hiddenField("permintaanPenunjang[$j][idDaftarTindakan]", $item->pemeriksaanrad->daftartindakan_id,array('class'=>'inputFormTabel','readonly'=>true)); ?>
                    <?php echo $item->pemeriksaanrad->pemeriksaanrad_nama; ?>
                    <?php echo CHtml::hiddenField("permintaanPenunjang[$j][inputpemeriksaanrad]", $item->pemeriksaanrad_id,array('class'=>'inputFormTabel','readonly'=>true)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("permintaanPenunjang[$j][inputtarifpemeriksaanrad]", $tarif[$i][$j],array('class'=>'inputFormTabel lebar3 currency tarif','readonly'=>true)); ?>
                </td>
                <td><?php echo CHtml::textField("permintaanPenunjang[$j][inputqty]", $item->qtypermintaan,array('class'=>'inputFormTabel lebar1 number qty', 'onkeyup'=>'hitungCyto(this); hitungTotal();')); ?></td>
                <td><?php echo CHtml::dropDownList("permintaanPenunjang[$j][satuan]", '', SatuanTindakan::items(),array('class'=>'inputFormTabel lebar3',)); ?></td>
                <td>
                    <?php echo CHtml::dropDownList("permintaanPenunjang[$j][cyto]", '0',array('1'=>'Ya','0'=>'Tidak'),array('class'=>'inputFormTabel lebar2-5 isCyto','onchange'=>'hitungCyto(this); hitungTotal();')); ?>
                    <?php echo CHtml::hiddenField("permintaanPenunjang[$j][persencyto]", $persencyto[$i][$j],array('class'=>'inputFormTabel lebar2 persenCyto','readonly'=>true)); ?>
                </td>
                <td><?php echo CHtml::textField("permintaanPenunjang[$j][tarifcyto]", '0',array('class'=>'inputFormTabel lebar2-5 cyto','readonly'=>true, 'onmkeyup'=>'hitungTotal();')); ?></td>
            </tr>
    <?php } } }?>
    </table>
    <table class="table table-bordered table-condensed">
        <tr><td width="70%" style="text-align: right;">Biaya Administrasi</td><td><?php echo CHtml::textField('biayaAdministrasi', 0,array('class'=>'span2 currency', 'style'=>'text-align:right;', 'onkeyup'=>'hitungTotal();'));?></td></tr>
        <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
        <?php if(count($modKirimKeUnitLain) > 0) :?> 
        <?php //echo $modKirimKeUnitLain->pasienkirimkeunitlain_id;?>
        <tr>
            <td colspan="2">
                <div class="control-group ">
                    <label class="control-label required" for="PasienkirimkeunitlainT_pegawai_id">
                    Perujuk :
                    </label>
                    <div class="controls">
                    </div>
                </div> 
            <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                        array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
        <?php endif; ?>
    </table>
    <table>
        <tr>
            <td>
                <br>
                <?php
                //FORM REKENING
                    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
                        array(
                            'form'=>$form,
                            'modRekenings'=>$modRekenings,
                        )
                    );
                ?>
            </td>
        </tr>
    </table>
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle"></i>')), 
                                                                    Yii::app()->createUrl($this->module->id.'/RujukanPenunjang/index&modulId=9'), 
                                                                     array('class'=>'btn btn-danger')); ?>
    </div>
        
<?php $this->endWidget(); ?>

<script type="text/javascript">
setTimeout(function(){
    hitungTotal();
}, 1000);
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = <?php echo Params::KELASPELAYANAN_ID_TANPA_KELAS;?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadMasuk')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                         renameInput('permintaanPenunjang','jenisPemeriksaanNama');
                         renameInput('permintaanPenunjang','pemeriksaanNama');
                         renameInput('permintaanPenunjang','idDaftarTindakan');
                         var daftartindakanId = $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).find('input[name$="[idDaftarTindakan]"]').val();
                         getDataRekening(daftartindakanId,idKelasPelayanan,1,'tm');
                 } ,
                 'cache':false});
        setTimeout(function(){hitungTotal();},500);
//        hitungTotal();
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }
        else
            $(obj).attr('checked', 'checked');
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

function batalPeriksa(idPemeriksaanrad)
{
    var daftartindakan_id = $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).find('input[name$="[idDaftarTindakan]"]').val();
    removeRekeningTindakan(daftartindakan_id);
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}
function hitungCyto(obj){
    persen = unformatNumber($(obj).parent().parent().find('.persenCyto').val());
    tarif = unformatNumber($(obj).parent().parent().find('.tarif').val());
    qty = unformatNumber($(obj).parent().parent().find('.qty').val());
    isCyto = unformatNumber($(obj).parent().parent().find('.isCyto').val());
    cyto = (tarif * qty)*persen/100;
//        alert(cyto);
    $(obj).parent().parent().find('.cyto').val(cyto);
    if(isCyto == 0)
        $(obj).parent().parent().find('.cyto').val(0);
}
function hitungTotal(){
    var total = 0;
    $('.tarif').each(
        function(){
            qty = unformatNumber($(this).parent().parent().find('.qty').val());
            isCyto = $(this).parent().parent().find('.isCyto').val();
            if(isCyto == 1){
                cyto = unformatNumber($(this).parent().parent().find('.cyto').val());
            }else{
                cyto = 0;
            }
            
            total += (unformatNumber(this.value) * qty) + unformatNumber(cyto);
        }
    );
	total += unformatNumber($('#biayaAdministrasi').val());
    $('#periksaTotal').val(formatNumber(total));    
}
//load rekening
function loadRekening(){
    $('#tblFormPemeriksaanRad').find('input[name$="[idDaftarTindakan]"]').each(function(){
        var daftartindakanId = $(this).val();
        var kelaspelayananId = <?php echo Params::kelasPelayanan('tanapa_kelas');?>;
        getDataRekening(daftartindakanId,kelaspelayananId,1,'tm');
    });
}
setTimeout(function(){
    loadRekening(); //rekening tindakan
    var kelaspelayananId = <?php echo Params::kelasPelayanan('tanapa_kelas');?>;
    getDataRekening(2003,kelaspelayananId,1,'tm'); //rekening biaya administrasi
},1000);
function unformatFormNumber(){
    $('#masukpenunjang-radiologi-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
}
function formatFormNumber(){
    $('#masukpenunjang-radiologi-form').find('.currency').each(function(){
        $(this).val(formatNumber($(this).val()));
    });
}
setTimeout(function(){formatFormNumber()},1000);
</script>


<?php
function cekPilihan($pemeriksaanrad_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $items) {
        foreach($items as $j=>$item){
            if($pemeriksaanrad_id == $item->pemeriksaanrad_id) $cek = true;
        }
    }
    return $cek;
}
?>
