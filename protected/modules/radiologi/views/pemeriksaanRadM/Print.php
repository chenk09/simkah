
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));      

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		////'pemeriksaanrad_id',
        array(
                        'name'=>'pemeriksaanrad_id',
                        'value'=>'$data->pemeriksaanrad_id',
                        'filter'=>false,
                ),
        array(
                    'name'=>'daftartindakan_nama',
                    'value'=>'$data->daftartindakan->daftartindakan_nama',

            ),
        'pemeriksaanrad_jenis',
        'pemeriksaanrad_nama',
        'pemeriksaanrad_namalainnya',
        array(
            'header'=>'<center>Status</center>',
            'value'=>'($data->pemeriksaanrad_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
            'htmlOptions'=>array('style'=>'text-align:center;'),
             ),
 
        ),
    )); 
?>