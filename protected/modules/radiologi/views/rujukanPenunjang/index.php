<?php
    if(!empty($_GET['succes']))
    {
?>

<div class="alert alert-block alert-success">
    <a class="close" data-dismiss="alert">×</a>
        <?php
            if($_GET['succes'] == 2)
            {
        ?>
            Pemeriksaan Pasien berhasil di batalkan
        <?php            
            }
            if($_GET['succes'] == 1){
        ?>
            Pasein Berhasil Di Rujuk
        <?php            
            }
        ?>
</div>

<?php     
    }
?>

<fieldset>
    <legend class="rim2">Informasi Pasien Rujukan</legend>

    <?php 
$this->widget('bootstrap.widgets.BootAlert'); 
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasienpenunjangrujukan-m-grid',
	'dataProvider'=>$dataProvider,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'tgl_pendaftaran',
                'tgl_kirimpasien',
                array(
                    'header'=>'Instalasi / Ruangan Asal',
                    'value'=>'$data->InstalasiNamaRuanganNama',
                ),
                'no_pendaftaran',
                'no_rekam_medik',
                array(
                    'header'=>'Nama Pasien',
                    'value'=>'$data->NamaPasienNamaBin',
                ),
                array(
                    'header'=>'Cara Bayar / Penjamin',
                    'value'=>'$data->CaraBayarPenjaminNama',
                ),
                array(
                    'header'=>'Kasus Penyakit / <br> Kelas Pelayanan',
                    'type'=>'raw',
                    'value'=>'"$data->jeniskasuspenyakit_nama"."<br/>"."$data->kelaspelayanan_nama"',
                ),
                'jeniskasuspenyakit_nama',
//                'pendaftaran.umur',
                'alamat_pasien',
//                'pemeriksaanrad_nama',
                array(
                    'header'=>'Periksa',
                    'type'=>'raw',
                    'value'=>'CHtml::link("<i class=\"icon-user\"></i>","",array("onclick"=>"statusPeriksa($data->pendaftaran_id,$data->pasienkirimkeunitlain_id);return false;","href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Periksa Pasien"))',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px')
//                    'value'=>'CHtml::Link("<i class=\"icon-user\"></i>",Yii::app()->controller->createUrl("masukPenunjang/",array("idPasienKirimKeUnitLain"=>$data->pasienkirimkeunitlain_id,"idPendaftaran"=>$data->pendaftaran_id)),
//                                    array("class"=>"icon-user", 
//                                          "id" => "selectPasien",
//                                          "rel"=>"tooltip",
//                                          "title"=>"Klik untuk periksa pasien",
////                                          "target"=>"blank",
//                                    ))',
                ),
            array(
               'header'=>'Batal Rujukan',
               'type'=>'raw',
               'value'=>'CHtml::link("<i class=\'icon-remove\'></i>", "javascript:batalperiksa($data->pendaftaran_id,$data->pasienkirimkeunitlain_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk membatalkan rujukan"))',
               'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->renderPartial('_formSearch',array()); 
?>
</fieldset>
<script type="text/javascript">
{
   function batalperiksa(idPendaftaran,idKirimUnit)
   {
       var con = confirm('anda yakin akan membatalkan rujukan radiologi pasien ini? ');
       if(con){
           $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/' . 'batalRujuk')?>',{idPendaftaran:idPendaftaran,idKirimUnit:idKirimUnit},
                     function(data){
                         if(data.status == 'ok'){
                            window.location = "<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/index&succes=2')?>";
                            $('#dialogKonfirm div.divForForm').html(data.keterangan);
                            $('#dialogKonfirm').dialog('open');
                         }
                     },'json'
                 );
       }         
   }

}

function statusPeriksa(pendaftaran_id,pasienkirimkeunitlain_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    var pasienkirimkeunitlain_id = pasienkirimkeunitlain_id;
    $.post("<?php echo Yii::app()->createUrl('radiologi/daftarPasien/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id:pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id,pasienkirimkeunitlain_id:pasienkirimkeunitlain_id},
        function(data){
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan pemeriksaan. ");
            }else{
                window.location.href= "<?php echo Yii::app()->createUrl('radiologi/masukPenunjang',array()); ?>&idPasienKirimKeUnitLain="+pasienkirimkeunitlain_id+"&idPendaftaran="+data.pendaftaran_id,"";
            }
    },"json");
    return false; 
}
</script>
<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKonfirm',
    'options'=>array(
        'title'=>'',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>500,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>