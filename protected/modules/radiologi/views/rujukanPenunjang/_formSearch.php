<legend class="rim">Pencarian</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'search-penunjangrujukan-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); 

Yii::app()->clientScript->registerScript('search', "
$('#search-penunjangrujukan-form').submit(function(){
	$.fn.yiiGridView.update('pasienpenunjangrujukan-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert');
?>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <label for="noPendaftaran" class="control-label">No Pendaftaran </label>
                <div class="controls">
                    <input type="text" value="" maxlength="20" id="noPendaftaran" name="noPendaftaran" onkeypress="return $(this).focusNextInputField(event)" empty="-- Pilih --">
                </div>
            </div>    
            <div class="control-group ">
                <label for="noRekamMedik" class="control-label">No Rekam Medik </label>
                <div class="controls">
                    <input type="text" value="" maxlength="10" id="noRekamMedik" name="noRekamMedik" onkeypress="return $(this).focusNextInputField(event)" empty="-- Pilih --">
                </div>
            </div>    
            <div class="control-group ">
                <label for="namaPasien" class="control-label">Nama Pasien </label>
                <div class="controls">
                    <input type="text" value="" maxlength="50" id="namaPasien" name="namaPasien" onkeypress="return $(this).focusNextInputField(event)" empty="-- Pilih --">
                </div>
            </div> 
        </td>
        <td>
            <div class="control-group ">
                <label for="namaPasien" class="control-label">
                    <?php echo CHtml::checkBox('cbTglMasuk', true, array('uncheckValue'=>0)); ?>
                    Tgl Masuk 
                </label>
                <div class="controls">
                    <?php   $format = new CustomFormat;
                            $this->widget('MyDateTimePicker',array(
                                            'name'=>'tglAwal',
//                                            'value'=> date('d M Y').' 00:00:00',
                                            'value'=> date('d M Y', strtotime('-5 days')).' 00:00:00',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    ));?> </div></div>
						<div class="control-group ">
                    <label for="namaPasien" class="control-label">
                       Sampai dengan
                      </label>
                    <div class="controls">
                            <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'name'=>'tglAkhir',
                                            'value'=> date('d M Y H:i:s'),
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                    )); ?>
                </div>
            </div>
        </td>
    </tr>
</table>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit','name'=>'submitSearch')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('rujukanPenunjang/index'), array('class'=>'btn btn-danger')); ?>		
<?php 
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));  ?>	
	
    </div>

<?php $this->endWidget(); ?>
