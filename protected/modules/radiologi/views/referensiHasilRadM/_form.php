
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'roreferensihasilrad-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#'.CHtml::activeId($modReferensiHasil,'pemeriksaanrad_id'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($modReferensiHasil)); ?>
        <table class="table-condensed">
            <tr>
                <td>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modReferensiHasil,'pemeriksaanrad_jenis', array('class'=>'control-label')) ?>
                        <div class="controls">
                        <?php echo $form->dropDownList($modReferensiHasil,'pemeriksaanrad_jenis', CHtml::listData(PemeriksaanradM::model()->findAll(),'pemeriksaanrad_jenis','pemeriksaanrad_jenis'), 
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return nextFocus(this,event,'".CHtml::activeId($modReferensiHasil, 'pemeriksaanrad_id')."'", 
                                                                'ajax'=>array('type'=>'POST',
                                                                              'url'=>Yii::app()->createUrl('radiologi/referensiHasilRadM/GetPemeriksaan',array('encode'=>false,'namaModel'=>'ROReferensiHasilRadM')),
                                                                              'update'=>'#ROReferensiHasilRadM_pemeriksaanrad_id',),
                                                                'onchange'=>"clearPemeriksaan();",)); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group ">                    
                        <?php echo $form->dropDownListRow($modReferensiHasil,'pemeriksaanrad_id', CHtml::listData($modReferensiHasil->getPemeriksaanRad($modReferensiHasil->pemeriksaanrad_jenis),'pemeriksaanrad_id','pemeriksaanrad_nama'),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'onChange'=>'cekPemeriksaan();')); ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group ">                    
                        <?php echo $form->dropDownListRow($modReferensiHasil,'pegawai_id', CHtml::listData(PegawaiM::model()->findAll(array('order'=>'nama_pegawai asc')),'pegawai_id','nama_pegawai'),array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    </div>
                </td>
            </tr>
        </table>
        <fieldset>
            <legend>Referensi Hasil</legend>
            <?php $modReferensiHasil = new ROReferensiHasilRadM; ?>
            <table class="table-condensed">
                <tr>
                    <td><?php echo CHtml::css('ul.redactor_toolbar{z-index:10;}'); ?>
                        <?php echo $form->textFieldRow($modReferensiHasil,'refhasilrad_kode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                        <div class="control-label">Hasil</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_hasil','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                        <td>
                            <div class="control-label">Kesimpulan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_kesimpulan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                        
                        </td>
                </tr>
                <tr>
                    <td>
                        <div class="control-label">Kesan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_kesan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                        <td>
                            <div class="control-label">Keterangan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_keterangan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </fieldset>
        
        <div class="form-actions">
            <?php echo CHtml::htmlButton($modReferensiHasil->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/pemeriksaanRadM/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            <?php
                $content = $this->renderPartial('../tips/tipsaddedit3a',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
            ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
    function namaLain(nama)
    {
        document.getElementById('ROPemeriksaanRadM_pemeriksaanrad_namalainnya').value = nama.value.toUpperCase();
    }
    function clearPemeriksaan()
    {
        $('#ROReferensiHasilRadM_pemeriksaanrad_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
    }

    function cekPemeriksaan(){
        var pemeriksaanrad_jenis = $('#ROReferensiHasilRadM_pemeriksaanrad_jenis').val();
        if(pemeriksaanrad_jenis == ''){
            alert('Pilih Jenis Pemeriksaan Terlebih dahulu');
            $('#ROReferensiHasilRadM_pemeriksaanrad_id').val('');
        }
    }
</script>