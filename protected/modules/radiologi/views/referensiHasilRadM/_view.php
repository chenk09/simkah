<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->refhasilrad_id),array('view','id'=>$data->refhasilrad_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pemeriksaanrad_id')); ?>:</b>
	<?php echo $data->pemeriksaanrad->pemeriksaanrad_namalainnya; ?>
	<br />

	<b><?php echo "Nama Lainnya" ?>:</b>
	<?php echo $data->pemeriksaanrad->pemeriksaanrad_namalainnya; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_kode')); ?>:</b>
	<?php echo $data->refhasilrad_kode; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_hasil')); ?>:</b>
	<?php echo $data->refhasilrad_hasil; ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_kesan')); ?>:</b>
	<?php echo $data->refhasilrad_kesan; ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_kesimpulan')); ?>:</b>
	<?php echo $data->refhasilrad_kesimpulan; ?>
	<br />
        
	<b><?php echo CHtml::encode($data->getAttributeLabel('refhasilrad_keterangan')); ?>:</b>
	<?php echo $data->refhasilrad_keterangan; ?>
	<br />


</div>