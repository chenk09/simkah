<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'roreferensihasilrad-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#'.CHtml::activeId($modReferensiHasil,'pemeriksaanrad_id'),
)); ?>

	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
	<?php echo $form->errorSummary(array($modReferensiHasil)); ?>
        <table class="table-condensed">
            <tr>
                <td>
                    <div class="control-group">                    
                        <?php echo $form->hiddenField($modReferensiHasil,'pemeriksaanrad_jenis',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-label">Jenis Pemeriksaan</div>
                        <div class="controls">
                            <?php echo CHtml::textField('pemeriksaanrad_jenis',$modReferensiHasil->pemeriksaanrad->pemeriksaanrad_jenis,array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$modReferensiHasil->pemeriksaanrad->pemeriksaanrad_jenis)); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group">                    
                        <?php echo $form->hiddenField($modReferensiHasil,'pemeriksaanrad_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-label">Nama Pemeriksaan</div>
                        <div class="controls">
                            <?php echo CHtml::textField('pemeriksaanrad_nama',$modReferensiHasil->pemeriksaanrad->pemeriksaanrad_nama,array('class'=>'span3', 'readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$modReferensiHasil->pemeriksaanrad->pemeriksaanrad_nama)); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group ">                    
                        <?php echo $form->hiddenField($modReferensiHasil,'pegawai_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <div class="control-label">Dokter</div>
                        <div class="controls">
                            <?php echo CHtml::textField('nama_pegawai',$modReferensiHasil->pegawai->nama_pegawai,array('class'=>'span3','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event);",'value'=>$modReferensiHasil->pemeriksaanrad->pemeriksaanrad_nama)); ?>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <?php echo $form->checkBoxRow($modReferensiHasil,'refhasilrad_aktif', array('onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                </td>
            </tr>
        </table>
        <fieldset>
            <legend>Referensi Hasil</legend>
            <table class="table-condensed">
                <tr>
                    <td><?php echo CHtml::css('ul.redactor_toolbar{z-index:10;}'); ?>
                        <?php echo $form->textFieldRow($modReferensiHasil,'refhasilrad_kode',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>10)); ?>
                        <div class="control-label">Hasil</div>
                        <div class="controls">
                        <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_hasil','toolbar'=>'mini','height'=>'100px')) ?>
                        </div>
                    <td>
                        <div class="control-label">Kesimpulan</div>
                        <div class="controls">
                        <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_kesimpulan','toolbar'=>'mini','height'=>'100px')) ?>
                        </div>

                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="control-label">Kesan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_kesan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                        <td>
                            <div class="control-label">Keterangan</div>
                    <div class="controls">
                    <?php $this->widget('ext.redactorjs.Redactor',array('model'=>$modReferensiHasil,'attribute'=>'refhasilrad_keterangan','toolbar'=>'mini','height'=>'100px')) ?>
                    </div>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </fieldset>
        
        <div class="form-actions">
            <?php echo CHtml::htmlButton($modReferensiHasil->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/pemeriksaanRadM/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
            <?php
                $content = $this->renderPartial('../tips/tipsaddedit3a',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
            ?>
	</div>

<?php $this->endWidget(); ?>