
<?php
$this->breadcrumbs=array(
	'Referensi Hasil Radiologi'=>array('index'),
	$modReferensiHasil->refhasilrad_id=>array('view','id'=>$modReferensiHasil->refhasilrad_id),
	'Update',
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','Update').'Referensi Hasil Radiologi', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Referensi Hasil Radiologi', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php echo $this->renderPartial('_formUpdate', array('modReferensiHasil'=>$modReferensiHasil)); ?>

