<?php
$this->breadcrumbs=array(
	'Referensi Hasil Radiologi'=>array('index'),
	$model->refhasilrad_id,
);

$arrMenu = array();
                array_push($arrMenu,array('label'=>Yii::t('mds','View').' Referensi Hasil Radiologi', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) ;
                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' Referensi Hasil Radiologi', 'icon'=>'folder-open', 'url'=>array('admin'))) :  '' ;

$this->menu=$arrMenu;

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php $this->widget('ext.bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		array(
                'name'=>'ID',
                'value'=>$model->refhasilrad_id,
            ),
            array(
                'name'=>'Nama Pemeriksaan',
                'value'=>$model->pemeriksaanrad->pemeriksaanrad_nama,
            ),
            array(
                'name'=>'Nama Lainnya',
                'value'=>$model->pemeriksaanrad->pemeriksaanrad_namalainnya,

            ),
            array(
                'name'=>'Nama Dokter',
                'value'=>$model->pegawai->nama_pegawai,
            ),
            array(
                'name'=>'Status',
                'value'=>($model->refhasilrad_aktif == 1 ) ? "Aktif" : "Tidak Aktif",
                'htmlOptions'=>array('style'=>'text-align:center;'),
             ),
	),
)); ?>

<?php $this->widget('TipsMasterData',array('type'=>'view'));?>