
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan));      

$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'sajenis-kelas-m-grid',
        'enableSorting'=>false,
	'dataProvider'=>$model->searchReferensiPrint(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'ID',
                'name'=>'refhasilrad_id',
                'value'=>'$data->refhasilrad_id',
            ),
            array(
                'header'=>'Nama Pemeriksaan',
                'name'=>'pemeriksaanrad_nama',
                'value'=>'$data->pemeriksaanrad->pemeriksaanrad_nama',
            ),
            array(
                'header'=>'Nama Lainnya',
                'name'=>'pemeriksaanrad_namalainnya',
                'value'=>'$data->pemeriksaanrad->pemeriksaanrad_namalainnya',

            ),
            array(
                'header'=>'Nama Dokter',
                'name'=>'nama_pegawai',
                'value'=>'$data->pegawai->nama_pegawai',
            ),
            array(
                'header'=>'<center>Status</center>',
                'value'=>'($data->refhasilrad_aktif == 1 ) ? "Aktif" : "Tidak Aktif"',
                'htmlOptions'=>array('style'=>'text-align:center;'),
             ),
 
        ),
    )); 
?>