<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'roreferensihasilrad-m-search',
        'type'=>'horizontal',
)); ?>
	<?php echo $form->dropDownListRow($model,'pemeriksaanrad_id',CHtml::listData(PemeriksaanradM::model()->findAll(array('order'=>'pemeriksaanrad_nama asc')), 'pemeriksaanrad_id','pemeriksaanrad_nama'),array('class'=>'span3','empty'=>'--Pilih--')); ?>
	<?php echo $form->dropDownListRow($model,'pegawai_id',CHtml::listData(PegawaiM::model()->findAll(array('order'=>'nama_pegawai asc')), 'pegawai_id','nama_pegawai'),array('class'=>'span3','empty'=>'--Pilih--')); ?>

	<?php echo $form->checkBoxRow($model,'refhasilrad_aktif',array('checked'=>'checked')); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
