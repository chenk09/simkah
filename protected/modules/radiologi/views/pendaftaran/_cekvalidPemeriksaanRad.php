<?php //echo "<pre>".print_r($arrInputPemeriksaan,1)."</pre>"; ?>
<fieldset>
    <legend>Pemeriksaan </legend>
    <table class="table table-bordered table-condensed" id="tblFormPemeriksaanRad">
        <thead>
            <tr>
                <th>
                    <?php echo CHtml::link('<i class="icon-zoom-in icon-white"></i>', '#',
                            array('onclick'=>"$('#dialogPemeriksaanRad').dialog('open');return false;",
                                  'rel'=>"tooltip",'data-original-title'=>"Klik untuk pilih pemeriksaan",
                                  'class'=>'btn btn-primary')) ?>
                </th>
                <th>Pemeriksaan</th>
                <th>Tarif</th>
                <th>Qty</th>
                <th>Satuan*</th>
                <th>Cyto</th>
                <th>Tarif Cyto</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($arrInputPemeriksaan as $i => $item) { 
                    if(!empty($item['cbPemeriksaan'])) {
            ?>
                        <tr id="periksarad_<?php echo $item['inputpemeriksaanrad']; ?>">
                            <td>
                                <?php echo CHtml::checkBox("permintaanPenunjang[$i][cbPemeriksaan]", true, array('value'=>$item['inputpemeriksaanrad'])); ?>
                                <?php echo CHtml::hiddenField("permintaanPenunjang[$i][idDaftarTindakan]", $item['idDaftarTindakan'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                            </td>
                            <td>
                                <?php echo $item['jenisPemeriksaanNama']; 
                                      echo CHtml::hiddenField("permintaanPenunjang[$i][jenisPemeriksaanNama]", $item['jenisPemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                <br/>
                                <?php echo $item['pemeriksaanNama']; 
                                      echo CHtml::hiddenField("permintaanPenunjang[$i][pemeriksaanNama]", $item['pemeriksaanNama'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                                <?php echo CHtml::hiddenField("permintaanPenunjang[$i][inputpemeriksaanrad]", $item['inputpemeriksaanrad'],array('class'=>'inputFormTabel','readonly'=>true)); ?>
                            </td>
                            <td>
                                <?php //echo $tarif; ?>
                                <?php echo CHtml::textField("permintaanPenunjang[$i][inputtarifpemeriksaanrad]", $item['inputtarifpemeriksaanrad'],array('class'=>'inputFormTabel lebar2-5 currency','readonly'=>true)); ?>
                            </td>
                            <td><?php echo CHtml::textField("permintaanPenunjang[$i][inputqty]", $item['inputqty'],array('class'=>'inputFormTabel lebar1')); ?></td>
                            <td><?php echo CHtml::dropDownList("permintaanPenunjang[$i][satuan]", $item['satuan'], SatuanTindakan::items(),array('class'=>'inputFormTabel lebar3','empty'=>'-- Pilih --')); ?></td>
                            <td>
                                <?php echo CHtml::dropDownList("permintaanPenunjang[$i][cyto]", $item['cyto'],array('1'=>'Ya','0'=>'Tidak'),array('class'=>'inputFormTabel lebar2-5')); ?>
                                <?php echo CHtml::hiddenField("permintaanPenunjang[$i][persencyto]", $item['persencyto'],array('class'=>'inputFormTabel lebar2','readonly'=>true)); ?>
                            </td>
                            <td><?php echo CHtml::textField("permintaanPenunjang[$i][tarifcyto]", $item['tarifcyto'],array('class'=>'inputFormTabel lebar2-5 currency','readonly'=>true)); ?></td>
                        </tr>
            <?php } }?>
        </tbody>
    </table>
</fieldset>

<?php 
//========= Dialog buat Input Pemeriksaan Radiologi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPemeriksaanRad',
    'options'=>array(
        'title'=>'Pilih Pemeriksaan Radiologi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'resizable'=>false,
//        'position'=>'top',
    ),
)); ?>
    <div class="box">
        <?php 
            $jenisPeriksa = '';
            foreach($modPeriksaRad as $i=>$pemeriksaan){ 
                $ceklist = cekPilihan($pemeriksaan->pemeriksaanrad_id,$arrInputPemeriksaan);
                if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
                    echo ($jenisPeriksa!='') ? "</div>" : "";
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo "<div class='boxtindakan'>";
                    echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputperiksa(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                } else {
                    $jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
                    echo CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
                                                                             'onclick' => "inputperiksa(this)"));
                    echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span><br/>";
                }
            } echo "</div>";
        ?>               
    </div>

<?php $this->endWidget();
//========= end Input Pemeriksaan Radiologi dialog =============================

function cekPilihan($pemeriksaanrad_id,$arrInputPemeriksaan)
{
    $cek = false;
    foreach ($arrInputPemeriksaan as $i => $item) {
        if($pemeriksaanrad_id == $item['inputpemeriksaanrad']) $cek = true;
    }
    return $cek;
}
?>

<script>
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRadPendRad')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         $('#tblFormPemeriksaanRad').append(data.form);
                         renameInput('permintaanPenunjang','inputpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputtarifpemeriksaanrad');
                         renameInput('permintaanPenunjang','inputqty');
                         renameInput('permintaanPenunjang','satuan');
                         renameInput('permintaanPenunjang','cyto');
                         renameInput('permintaanPenunjang','persencyto');
                         renameInput('permintaanPenunjang','tarifcyto');
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?'))
            batalPeriksa(obj.value);
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
}



function renameInput(modelName,attributeName)
{
    var trLength = $('#tblFormPemeriksaanRad tr').length;
    var i = -1;
    $('#tblFormPemeriksaanRad tr').each(function(){
        if($(this).has('input[name$="[inputpemeriksaanrad]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
</script>