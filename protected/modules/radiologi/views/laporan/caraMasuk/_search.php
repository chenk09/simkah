<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
     <style>

   label.checkbox, label.radio{
            width:150px;
            display:inline-block;
        }

    </style>
   <legend class="rim">Berdasarkan Kunjungan</legend>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php //echo CHtml::hiddenField('src', ''); ?>
                   <table width="200" border="0">
  <tr>
    <td><div class = 'control-label'>Tanggal Kunjungan</div>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
//                                          'maxDate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> </td>
    <td style="padding:0px 100px 0 0;"><?php echo CHtml::label(' Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                    <div class="controls">  
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
//                                         'maxdate'=>'d',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                        ?>
                    </div> </td>
  </tr>
</table>
                   
            
                    <div id='searching'>
                    <fieldset>
                        <legend class="rim3">Berdasarkan Rujukan</legend>
                           <div style='margin-left:0px;'>
                                <?php echo CHtml::checkBox('checkAllRujukan',true, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                    'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked'))."Pilih Semua"; ?>
                          </div><br>
                          <div id="rujukan">
                                <?php echo $form->checkBoxList($model, 'asalrujukan_id', CHtml::listData(AsalrujukanM::model()->findAll('asalrujukan_aktif = true'),'asalrujukan_id','asalrujukan_nama')); ?>
                          </div>
                    </fieldset>
                        <br/>
                    <fieldset>
                        <legend class="rim3">Berdasarkan Asal Instalasi </legend>
                        <div style='margin-left:0px;'>
                            <?php echo CHtml::checkBox('checkAllInstalasi',true, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked'))."Pilih Semua"; ?>
                      </div><br>
                      <div id="instalasi">
                                <?php echo $form->checkBoxList($model, 'ruanganasal_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_id','ruangan_nama')); ?>
                      </div>
                    </fieldset>
                        <br/>
                    <fieldset>
                        <legend class="rim3">Opsi Grafik</legend>
                        <?php $model->pilihan = 'instalasi'; ?>
                                <?php echo $form->radioButtonList($model, 'pilihan', $model->pilihanGrafik()); ?>
                    </fieldset>
                    </div>
          
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')),
                array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('laporan/LaporanCaraMasukPasien'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>
<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#content4").find("input[type=\'checkbox\']").attr("checked", "checked");
',  CClientScript::POS_READY);
?>

<script>
function checkAll() {
    if ($("#checkAllInstalasi").is(":checked")) {
        $('#instalasi input[name*="ruanganasal_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#instalasi input[name*="ruanganasal_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    
    if ($("#checkAllRujukan").is(":checked")) {
        $('#rujukan input[name*="asalrujukan_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#rujukan input[name*="asalrujukan_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
}   
</script>

<?php //Yii::app()->clientScript->registerScript('onclickButton','
//  var tampilGrafik = "<div class=\"tampilGrafik\" style=\"display:inline-block\"> <i class=\"icon-arrow-right icon-white\"></i> Grafik</div>";
//  $(".accordion-heading a.accordion-toggle").click(function(){
//            $(this).parents(".accordion").find("div.tampilGrafik").remove();
//            $(this).parents(".accordion-group").has(".accordion-body.in").length ? "" : $(this).append(tampilGrafik);
//            
//            
//  });
//',  CClientScript::POS_READY);
?>
