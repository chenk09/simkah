<?php

class RITindakanPelayananT  extends TindakanpelayananT
{
        public $dokterpemeriksa1Nama; //untuk default pemeriksa
         /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TindakanpelayananT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getJumlahTarif()
        {
            return $this->tarif_tindakan * $this->qty_tindakan + $this->tarifcyto_tindakan;
        }

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }

        public function getTipePaketItems($carabayar_id='')
        {
            if(!empty($carabayar_id))
                return TipepaketM::model()->findAllByAttributes(array('tipepaket_aktif'=>true,'carabayar_id'=>$carabayar_id));
            else
                return TipepaketM::model()->findAllByAttributes(array('tipepaket_aktif'=>true));
        }

        public function searchDetailTindakan($data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

                $criteria=new CDbCriteria;

		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('tipepaket_id',$this->tipepaket_id);
		$criteria->compare('instalasi_id',$this->instalasi_id);
                $criteria->compare('ruangan_id',$this->ruangan_id);
                $criteria->compare('rencanaoperasi_id',$this->rencanaoperasi_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
//                $criteria->condition = 'pendaftaran_id = '.$data; << FATAL ERROR di riwayat pasien > detail tindakan
                // $criteria->compare('pendaftaran_id',$data);
        $criteria->addCondition('pendaftaran_id ='.$data);
//		$criteria->compare('carabayar_id',$this->carabayar_id);
//		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
//		$criteria->compare('LOWER(tgl_tindakan)',strtolower($this->tgl_tindakan),true);
//		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
//		$criteria->compare('LOWER(satuantindakan)',strtolower($this->satuantindakan),true);
//		$criteria->compare('LOWER(qty_tindakan)',strtolower($this->qty_tindakan),true);
//		$criteria->compare('cyto_tindakan',$this->cyto_tindakan);
//		$criteria->compare('tarifcyto_tindakan',$this->tarifcyto_tindakan);
//		$criteria->compare('LOWER(dokterpemeriksa1_id)',strtolower($this->dokterpemeriksa1_id),true);
//		$criteria->compare('LOWER(dokterpemeriksa2_id)',strtolower($this->dokterpemeriksa2_id),true);
//		$criteria->compare('LOWER(dokterpendamping_id)',strtolower($this->dokterpendamping_id),true);
//		$criteria->compare('LOWER(dokteranastesi_id)',strtolower($this->dokteranastesi_id),true);
//		$criteria->compare('LOWER(dokterdelegasi_id)',strtolower($this->dokterdelegasi_id),true);
//		$criteria->compare('LOWER(bidan_id)',strtolower($this->bidan_id),true);
//		$criteria->compare('LOWER(suster_id)',strtolower($this->suster_id),true);
//		$criteria->compare('LOWER(perawat_id)',strtolower($this->perawat_id),true);
//		$criteria->compare('kelastanggungan_id',$this->kelastanggungan_id);
//		$criteria->compare('discount_tindakan',$this->discount_tindakan);
//		$criteria->compare('subsidiasuransi_tindakan',$this->subsidiasuransi_tindakan);
//		$criteria->compare('subsidipemerintah_tindakan',$this->subsidipemerintah_tindakan);
//		$criteria->compare('subsisidirumahsakit_tindakan',$this->subsisidirumahsakit_tindakan);
//		$criteria->compare('iurbiaya_tindakan',$this->iurbiaya_tindakan);
//		$criteria->compare('LOWER(tm)',strtolower($this->tm),true);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
//		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
//		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
//		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
}