<?php
class RIDetailhasilpemeriksaanlabT extends DetailhasilpemeriksaanlabT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchDetailHasilLab($data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('detailhasilpemeriksaanlab_id',$this->detailhasilpemeriksaanlab_id);
		$criteria->compare('hasilpemeriksaanlab_id',$this->hasilpemeriksaanlab_id);
		$criteria->compare('pemeriksaanlab_id',$this->pemeriksaanlab_id);
                $criteria->condition = 'hasilpemeriksaanlab_id ='.$data;
                $criteria->order = 'pemeriksaanlabdet_id';
//		$criteria->compare('LOWER(hasilpemeriksaan)',strtolower($this->hasilpemeriksaan),true);
//		$criteria->compare('LOWER(nilairujukan)',strtolower($this->nilairujukan),true);
//		$criteria->compare('LOWER(hasilpemeriksaan_satuan)',strtolower($this->hasilpemeriksaan_satuan),true);
//		$criteria->compare('LOWER(hasilpemeriksaan_metode)',strtolower($this->hasilpemeriksaan_metode),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}