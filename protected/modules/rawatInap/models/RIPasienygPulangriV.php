<?php

class RIPasienygPulangriV  extends PasienygpulangriV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PasienygpulangriV the static model class
	 */
	public $ceklis = false;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRI()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.
                $criteria=new CDbCriteria;
				$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
      			$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
				$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
				$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
				$criteria->compare('LOWER(keterangan_kamar)',strtolower($this->keterangan_kamar),true);
                // if($this->ceklis=TRUE){
                //     $criteria->addBetweenCondition('tglpasienpulang',$this->tglAwal,$this->tglAkhir,true);
                // }
                if($this->ceklis == TRUE)
                {
                    $criteria->addCondition('tglpasienpulang BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
                }
                $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('carabayar_id',$this->carabayar_id);


                return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }

}