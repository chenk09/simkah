<?php
class RIPasienapachescoreT extends PasienapachescoreT {
    public $diagnosa_id;
    public $paramedis_nama;
    public $pegawai_nama;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function attributeLabels()
	{
		return array(
			'pasienapachescore_id' => 'Pasien Apache Score',
			'pasien_id' => 'Pasien',
			'apachescore_id' => 'Apache Score',
			'pasienadmisi_id' => 'Pasien Admisi',
			'ruangan_id' => 'Ruangan',
			'pegawai_id' => 'Pegawai',
			'pendaftaran_id' => 'Pendaftaran',
			'tglscoring' => 'Tanggal Scoring',
			'gagalginjalakut' => 'Gagal Ginjal Akut',
			'point_nama' => 'Point Nama',
			'point_nilai' => 'Point Nilai',
			'point_score' => 'Point Score',
			'paramedis_id' => 'Paramedis',
			'catatanapachescore' => 'Catatan Apache Score',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
			'paramedis_nama' => 'Nama Paramedis',
			'pegawai_nama' => 'Nama Dokter',
		);
	}
        
        public function searchDetailHasil($data)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasienapachescore_id',$this->pasienapachescore_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('apachescore_id',$this->apachescore_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('pendaftaran_id',$data);
                $criteria->with = array('apachescore');
//		$criteria->compare('LOWER(tglscoring)',strtolower($this->tglscoring),true);
//		$criteria->compare('gagalginjalakut',$this->gagalginjalakut);
//		$criteria->compare('LOWER(point_nama)',strtolower($this->point_nama),true);
//		$criteria->compare('point_nilai',$this->point_nilai);
//		$criteria->compare('point_score',$this->point_score);
//		$criteria->compare('LOWER(paramedis_id)',strtolower($this->paramedis_id),true);
//		$criteria->compare('LOWER(catatanapachescore)',strtolower($this->catatanapachescore),true);
//		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
//		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
//		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
//		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
//		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}