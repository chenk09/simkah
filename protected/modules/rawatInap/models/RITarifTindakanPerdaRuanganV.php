<?php
class RITarifTindakanPerdaRuanganV  extends TariftindakanperdaruanganV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TariftindakanperdaruanganV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
       public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                //if ((!empty($this->kelaspelayanan_id)) || (!empty($this->daftartindakan_id)) || (!empty($this->kategoritindakan_id))){
                    $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                    $criteria->compare('LOWER(daftartindakan_nama)',  strtolower($this->daftartindakan_id),true);
                    $criteria->compare('kategoritindakan_id',  $this->kategoritindakan_id);
                    $criteria->compare('ruangan_id',$this->ruangan_id);
//                }
//                else{
//                    $criteria->compare('ruangan_id', 0);
//                }
		
                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getKategoriindakanItems()
        {
//            return TariftindakanperdaruanganV::model()->findAll('kategoritindakan_id is not null');
            return Yii::app()->db->CreateCommand('SELECT kategoritindakan_id, kategoritindakan_nama FROM tariftindakanperdaruangan_v WHERE kategoritindakan_id is not null')->queryAll();
        }
        
        public function getDaftartindakanItems()
        {
            return Yii::app()->db->CreateCommand('SELECT daftartindakan_id, daftartindakan_nama FROM tariftindakanperdaruangan_v');
        }
        
        public function getKelaspelayananItems()
        {
            return Yii::app()->db->CreateCommand('SELECT kelaspelayanan_id, kelaspelayanan_nama FROM tariftindakanperdaruangan_v');
        }

	
}

