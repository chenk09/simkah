<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery.tiler.js'); ?>
<?php
$this->breadcrumbs=array(
	'Radiologi',
);

$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modAdmisi'=>$modAdmisi));

echo '<legend class="rim">RADIOLOGI</legend><hr>';
$this->renderPartial('/_tabulasi',array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi));
//$this->widget('bootstrap.widgets.BootMenu', array(
//    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
//    'stacked'=>false, // whether this is a stacked menu
//    'items'=>array(
//        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rawatInap/anamnesaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rawatInap/pemeriksaanFisikTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rawatInap/laboratoriumTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Radiologi', 'url'=>'','linkOptions'=>array(),'active'=>true),
//        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rawatInap/rehabMedisTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rawatInap/konsulGiziTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rawatInap/konsulPoliTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rawatInap/tindakanTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
//        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rawatInap/diagnosaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rawatInap/bedahSentralTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rawatInap/rujukanKeluarTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rawatInap/resepturTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rawatInap/pemakaianBahanTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//    ),
//));
?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpasien-radiologi-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); ?>

    <?php $this->renderPartial('_listKirimKeUnitLain',array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain)) ?>

<div class="formInputTab">
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modKirimKeUnitLain, $modPasienMasukPenunjang); ?>
    <table>
        <tr>
            <td style="background-color: #E5ECF9;">
                <div id="formPeriksaLab">
                    <?php 
                        $jenisPeriksa = '';
                        foreach($modPeriksaRad as $i=>$pemeriksaan){
							if($pemeriksaan->pemeriksaanrad_jenis != 'Paket Pemeriksaan GCU')
							{
								$ceklist = false;
								if($jenisPeriksa != $pemeriksaan->pemeriksaanrad_jenis){
									echo ($jenisPeriksa!='') ? "</div>" : "";
									$jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
									echo "<div class='boxtindakan' style='width:30%;'>";
									echo "<h6>$pemeriksaan->pemeriksaanrad_jenis</h6>";
									echo '<label class="checkbox inline">'.CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
																							 'onclick' => "inputperiksa(this); checkIni(this);"));
									echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span></label><br/>";
								} else {
									$jenisPeriksa = $pemeriksaan->pemeriksaanrad_jenis;
									echo'<label class="checkbox inline">'. CHtml::checkBox("pemeriksaanRad[]", $ceklist, array('value'=>$pemeriksaan->pemeriksaanrad_id,
																							 'onclick' => "inputperiksa(this); checkIni(this);"));
									echo "<span>".$pemeriksaan->pemeriksaanrad_nama."</span></label><br/>";
								}
							}							
                        } echo "</div>";
                    ?>               
            </div>
            </td>
        </tr>
    </table>
    
    <table class="table-condensed">
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo $form->labelEx($modKirimKeUnitLain,'tgl_kirimpasien', array('class'=>'control-label')) ?>
                    <?php $modKirimKeUnitLain->tgl_kirimpasien = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKirimKeUnitLain->tgl_kirimpasien, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKirimKeUnitLain,
                                                    'attribute'=>'tgl_kirimpasien',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKirimKeUnitLain,'pegawai_id', CHtml::listData($modKirimKeUnitLain->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                                array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modKirimKeUnitLain,'catatandokterpengirim',array('onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
            </td>
            <td width="50%">
                <table id="tblFormPemeriksaanRad" class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th>Jenis Pemeriksaan</th>
                            <th>Pemeriksaan</th>
                            <!--<th>Tarif</th>-->
                            <th>Qty</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr id="trPeriksaRadKosong"><td colspan="4"></td></tr>
                    </tbody>
                </table>
                <table class="table table-bordered table-condensed">
                    <tr><td width="70%" style="text-align: right;">Total Biaya Pemeriksaan</td><td><?php echo CHtml::textField('periksaTotal', '',array('class'=>'span2', 'style'=>'text-align:right;', 'disabled'=>'disabled'));?></td></tr>
                </table>
            </td>
        </tr>
    </table>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
									<?php           $content = $this->renderPartial('../tips/tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content)); ?>
    </div>
    
</div>

<?php $this->endWidget(); ?>

<script>
$('#formPeriksaLab').tile({widths : [ 260 ]});
function inputperiksa(obj)
{
    if($(obj).is(':checked')) {
        var idPemeriksaanrad = obj.value;
        var idKelasPelayanan = <?php echo Params::kelasPelayanan('tanapa_kelas'); ?>;
        jQuery.ajax({'url':'<?php echo Yii::app()->createUrl('ActionAjax/loadFormPemeriksaanRad')?>',
                 'data':{idPemeriksaanrad:idPemeriksaanrad, idKelasPelayanan:idKelasPelayanan},
                 'type':'post',
                 'dataType':'json',
                 'success':function(data) {
                         if($.trim(data.form)=='')
                         {
                            $(obj).removeAttr('checked');
                            alert ('Pemeriksaan belum memiliki tarif');
                         } 
                         $('#tblFormPemeriksaanRad #trPeriksaRadKosong').detach();
                         $('#tblFormPemeriksaanRad > tbody').append(data.form);
                         $("#tblFormPemeriksaanRad > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0});
//                         $('.currency').each(function(){this.value = formatNumber(this.value)});
                         $("#tblFormPemeriksaanRad > tbody > tr:last .number").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":".","precision":0,"symbol":null});
                         $('.number').each(function(){this.value = formatNumber(this.value)});
//                         $(".currency").parent().detach(); // Hapus kolom tarif
                        hitungTotal();
                 } ,
                 'cache':false});
    } else {
        if(confirm('Apakah anda akan membatalkan pemeriksaan ini?')){
            batalPeriksa(obj.value);
            hitungTotal();
        }
        else
            $(obj).attr('checked', 'checked');
    }
}

function batalPeriksa(idPemeriksaanrad)
{
    $('#tblFormPemeriksaanRad #periksarad_'+idPemeriksaanrad).detach();
    if($('#tblFormPemeriksaanRad tr').length == 1)
        $('#tblFormPemeriksaanRad').append('<tr id="trPeriksaRadKosong"><td colspan="4"></td></tr>');
}

function batalKirim(idPasienKirimKeUnitLain,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan kirim pasien ke Radiologi?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKirim') ?>', {idPasienKirimKeUnitLain: idPasienKirimKeUnitLain, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListPemeriksaanRad').html(data.result);
        }, 'json');
    }
}

function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
function hitungTotal(){
    var total = 0;
    $('.currency').each(
        function(){
            qty = $(this).parents('tr').find('.number').val();
            total += unformatNumber(this.value) * qty;
        }
    );
 
    $('#periksaTotal').val(formatNumber(total));    
}
</script>
