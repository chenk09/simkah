<?php
$this->breadcrumbs=array(
	'Pemakaian Bahan',
);

$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modAdmisi'=>$modAdmisi));

echo '<legend class="rim">PEMAKAIAN BAHAN</legend><hr>';
$this->renderPartial('/_tabulasi',array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi));
//$this->widget('bootstrap.widgets.BootMenu', array(
//    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
//    'stacked'=>false, // whether this is a stacked menu
//    'items'=>array(
//        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rawatInap/anamnesaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rawatInap/pemeriksaanFisikTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rawatInap/laboratoriumTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rawatInap/radiologiTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rawatInap/rehabMedisTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rawatInap/konsulGiziTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsul Poliklinik', 'url'=>$this->createUrl('/rawatInap/konsulPoliTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rawatInap/tindakanTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
//        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rawatInap/diagnosaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rawatInap/bedahSentralTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rawatInap/rujukanKeluarTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rawatInap/resepturTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Pemakaian Bahan', 'url'=>'','linkOptions'=>array(),'active'=>true),
//    ),
//));
?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjpemakaian-bahan-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInput();'),
)); ?>
<?php $this->renderPartial('_listObatAlkesPasien',array('modViewBmhp'=>$modViewBmhp)); ?>
<?php $this->renderPartial('_formPemakaianBahan',array()); ?>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>
<?php $this->endWidget(); ?>