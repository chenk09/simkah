<legend class="rim2">Informasi Pasien Ruang Lain</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchRI(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'header'=>'Tgl Admisi/ Tgl Masuk',
                       'name'=>'tgladmisi',
                       'type'=>'raw',
                       'value'=>'$data->TglAdmisiMasukKamar'
                    ),
                    array(
                       'header'=>'Cara Masuk',
                       'name'=>'caramasuk_nama',
                       'type'=>'raw',
                       'value'=>'$data->caramasuk_nama',
                    ),
                    array(
                        'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Alias',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'header'=>'Jenis Kelamin',
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'header'=>'Umur',
                        'name'=>'umur',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                        'header'=>'Kelas Pelayanan',
                        'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
                    array(
                        'header'=>'Jenis Kasus Penyakit',
                        'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                        'header'=>'Ruangan Asal',
                        'name'=>'ruanganasal_nama',
                        'type'=>'raw',
                        'value'=>'$data->ruanganasal_nama',
                    ),
                    array(
                       'header'=>'Masuk Kamar / Batal',
                       'type'=>'raw',
                       'value'=>'$data->masukkamar_id ? ( $data->cekTindakanDanObat()->ada ? CHtml::link("Sedang Diperiksa", "#",array("title"=>"Silahkan batalkan dulu ".$data->cekTindakanDanObat()->msg."!")) : CHtml::link("<i class=icon-remove-sign></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Batal Pindah Kamar","onclick"=>"{batalPindahKamar($data->pindahkamar_id,$data->masukkamar_id);}"))) : CHtml::link("<i class=icon-home></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Memasukan Pasien Ke kamar","onclick"=>"{buatSessionMasukKamar($data->kelaspelayanan_id,$data->pendaftaran_id); addMasukKamar(); $(\'#dialogMasukKamar\').dialog(\'open\');}"))',    
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr></hr>

<?php echo $this->renderPartial('_formPencarian', array('model'=>$model)); ?>

<?php 
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogMasukKamar',
    'options'=>array(
        'title'=>'Masuk Kamar Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>
<script>
function addMasukKamar()
{
    
    <?php 
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/insertMasukKamar'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogMasukKamar div.divForForm').html(data.div);
                    $('#dialogMasukKamar div.divForForm form').submit(addMasukKamar);
                    
                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false}, 
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy','minDate'  : 'd','timeText':'Waktu','hourText':'Jam',
                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    
                    jQuery('#MasukkamarT_jammasukkamar').timepicker(jQuery.extend({showMonthAfterYear:false}, 
                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy',
                   'timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
                    
                }
                else
                {
                    $('#dialogMasukKamar div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogMasukKamar').dialog('close') \",1000);
                }
 
            } ",
    ))
?>;
    return false; 
}
</script>

<?php
$urlSessionMasukKamar = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionMasukKamar ');
$jscript = <<< JS
function buatSessionMasukKamar(idKelasPelayanan, idPendaftaran)
{
    $.post("${urlSessionMasukKamar}", { idKelasPelayanan: idKelasPelayanan,idPendaftaran: idPendaftaran },
        function(data){
            'sukses';
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsBatalPindah',$jscript, CClientScript::POS_BEGIN);
?>
<?php
$url = Yii::app()->createUrl('ActionAjaxRIRD/batalPindahKamar');
$mds = Yii::t('mds','Anda yakin akan membatalkan pindah kamar?');
$jscript = <<< JS
function batalPindahKamar(idPindahKamar,idMasukKamar)
{
    if(confirm("${mds}"))
    {
        $.post("${url}", { idPindahKamar: idPindahKamar, idMasukKamar: idMasukKamar },
            function(data){
                if(data.status == 'true')
                {
                    $('#dialogSuksesBatalPindah').dialog('open');
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    $('#dialogBatalPindah div.divForForm').html(data.div);
                    setTimeout("$('#dialogSuksesBatalPindah').dialog('close') ",1000);
                }
                else
                {
                    $('#dialogGagalBatalPindah').dialog('open');
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    $('#dialogBatalPindah div.divForForm').html(data.div);
                    setTimeout("$('#dialogSuksesBatalPindah').dialog('close') ",1000);
                }
        }, "json");
    }
}
JS;
Yii::app()->clientScript->registerScript('jsBatalPindah',$jscript, CClientScript::POS_BEGIN);
?>
