<fieldset>
	<legend class="rim2">Informasi Kamar</legend>
    <legend class="rim">Pencarian Kamar</legend>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
    <?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
        'id'=>'informasiKamar-t-form',
        'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
            'focus'=>'#',
    )); ?>
    
    <table>
        <tr>
            <td width="200px" height="80px">
                 <?php echo $form->dropDownListRow($modKamarRuangan,'kelaspelayanan_id',  CHtml::listData($modKamarRuangan->KelasPelayananRuanganItems, 'kelaspelayanan_id', 'kelaspelayanan.kelaspelayanan_nama'),array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')",'empty'=>'-- Pilih --',
                                                    'ajax'=>array('type'=>'POST',
                                                              'url'=>Yii::app()->createUrl('ActionDynamic/GetRuanganNoKamarRuangan',array('encode'=>false,'namaModel'=>'RIKamarRuanganM')),
                                                              'update'=>'#RIKamarRuanganM_kamarruangan_nokamar',),
                                                )); ?>
                <?php echo $form->dropDownListRow($modKamarRuangan,'kamarruangan_nokamar', array(),array('class'=>'span3', 'onkeypress'=>"return nextFocus(this,event,'','')",'empty'=>'-- Pilih --')); ?>
               
                   
           </td>
           <td rowspan="2">
               <table>
                   <tr>
                       <td>
                           <table>
                               <tr>
                                   <td>
                                       <fieldset>
                                           <legend>Foto Ruangan <?php echo $modRuangan->ruangan_nama;?></legend>
                                             <img src="<?php echo Params::urlRuanganTumbsDirectory().'kecil_'.$modRuangan->ruangan_image ?>" />
                                       </fieldset>
                                   </td>
                                   <td>
                                       <fieldset>
                                        <legend>Fasilitas</legend>
                                           <?php echo $modRuangan->ruangan_fasilitas ?>
                                       </fieldset>
                                   </td>
                               </tr>
                           </table>
                          
                       <td>
                               
                       </td>
                   </tr>
                   <tr>
                       <td>
                           <fieldset>
                              
                       </td>
                   </tr>
               </table>
           </td>
        </tr>
        <tr>
            <td>
               
            </td> 
        </tr>
    </table>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan'));?>
												 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
<?php
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</fieldset>

        <?php echo $formKasur ?>
<?php $this->endWidget(); ?>

<?php
$idKelasPelayanan=  CHtml::activeId($modKamarRuangan,'kelaspelayanan_id');
$idNoKamar=  CHtml::activeId($modKamarRuangan,'kamarruangan_nokamar');
$jscript = <<< JS
function cekValidasi()
{
    idKelas=$('#${idKelasPelayanan}').val();
    idKamar=$('#${idNoKamar}').val();
    
    if(idKelas==''){
        alert('Anda Belum Memilih Kelas Pelayanan');
    }else if(idKamar==''){
        alert('Anda Belum Memilih Kamar');
    }else{
        $('#btn_simpan').click();
    }
}
JS;
Yii::app()->clientScript->registerScript('faktur',$jscript, CClientScript::POS_HEAD);
?>

