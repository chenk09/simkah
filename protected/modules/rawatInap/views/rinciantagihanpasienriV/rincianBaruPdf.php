<style>
    .info{
        font-size: 11px;
        font-family: tahoma;
    }
    
    .grid{
        border-collapse: collapse;
        font-size: 11px;
        font-family: tahoma;
        border:1px solid #000;
    }
    .grid td,th{
        padding: 5px;
    } 
</style>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
    <tr>
        <td width="20%">No. RM / Reg</td>
        <td width="30%"> :
            <?php echo CHtml::encode($modPendaftaran->pasien->no_rekam_medik); ?> / 
            <?php echo CHtml::encode($modPendaftaran->no_pendaftaran); ?>            
        </td>
        <td width="20%">Nama PJP</td>
        <td>:
            <?php
                if(strlen($modPendaftaran->penanggungjawab_id) > 0)
                {
                    echo CHtml::encode($modPendaftaran->penanggungJawab->nama_pj);
                }else{
                    echo CHtml::encode($modPendaftaran->pasien->nama_pasien);
                }
            ?>        
        </td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>: <?php echo CHtml::encode($modPendaftaran->pasien->nama_pasien); ?></td>
        <td>Alamat PJP</td>
        <td>: <?php echo CHtml::encode($modPendaftaran->pasien->alamat_pasien); ?></td>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>:
            <?php echo CHtml::encode($modPendaftaran->pasien->jeniskelamin); ?>
        </td>
        <td>Umur</td>
        <td>: 
            <?php echo CHtml::encode($modPendaftaran->umur); ?>
        </td>
    </tr>
    <tr>
        <td>Unit Pelayanan</td>
        <td>: 
            <?php echo CHtml::encode($modPendaftaran->instalasi->instalasi_nama); ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Dokter Pemeriksa</td>
        <td>: 
            <?php echo CHtml::encode($modPendaftaran->dokter->nama_pegawai); ?>
        </td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>Tgl Perawatan</td>
        <td>: <?php echo CHtml::encode($modRincian[0]->tgl_tindakan); ?></td>
        <td>Perusahaan Penjamin</td>
        <td>: <?php echo CHtml::encode($modRincian[0]->penjamin_nama); ?></td>
    </tr>
</table>
<br>
<div align="center" style="text-align: center;">
    <center><?php echo strtoupper($data['judulPrint']); ?></center>
</div>
    <?php
        $totalbiayaadminfarmasi = 0;
        $row = array();
        foreach($modRincian as $i=>$val)
        {
            $ruangan_id = $val->ruangan_id;
            $row[$ruangan_id]['nama'] = $val->ruangan_nama;
            $row[$ruangan_id]['ruangan_id'] = $val->ruangan_id;
            $row[$ruangan_id]['pendaftaran_id'] = $val->pendaftaran_id;
            $row[$ruangan_id]['kategori'][$i]['nama_pegawai'] = $val->nama_pegawai;
            $row[$ruangan_id]['kategori'][$i]['tindakanpelayanan_id'] = $val->tindakanpelayanan_id;
            $row[$ruangan_id]['kategori'][$i]['daftartindakan_id'] = $val->daftartindakan_id;
            $row[$ruangan_id]['kategori'][$i]['daftartindakan_nama'] = $val->daftartindakan_nama;
            $row[$ruangan_id]['kategori'][$i]['kelas'] = $val->kelaspelayanan_nama;
            $row[$ruangan_id]['kategori'][$i]['harga'] = (isset($val->tarif_medis) ? ($val->tarif_satuan - $val->tarif_medis) : $val->tarif_satuan);
            $row[$ruangan_id]['kategori'][$i]['qty'] = $val->qty_tindakan;
            $row[$ruangan_id]['kategori'][$i]['total'] = ($row[$ruangan_id]['kategori'][$i]['harga'] * $row[$ruangan_id]['kategori'][$i]['qty']);
            $harga = TindakanpelayananT::model()->findAllByPk($val->tindakanpelayanan_id);
            $row[$ruangan_id]['kategori'][$i]['harga_dokter'] = (isset($val->tarif_medis) ? $val->tarif_medis : 0);
            $row[$ruangan_id]['kategori'][$i]['total_dokter'] = (isset($val->tarif_medis) ? ($val->qty_tindakan * $val->tarif_medis) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsidiasuransi_tindakan'] = (isset($val->subsidiasuransi_tindakan) ? ($val->subsidiasuransi_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsidipemerintah_tindakan'] = (isset($val->subsidipemerintah_tindakan) ? ($val->subsidipemerintah_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['subsisidirumahsakit_tindakan'] = (isset($val->subsisidirumahsakit_tindakan) ? ($val->subsisidirumahsakit_tindakan) : 0);
            $row[$ruangan_id]['kategori'][$i]['iurbiaya_tindakan'] = (isset($val->iurbiaya_tindakan) ? ($val->iurbiaya_tindakan) : 0);
            //Total biaya racik dll
            $totalbiayaadminfarmasi += ($val->biayaadministrasi + $val->biayaservice + $val->biayakonseling) ;        }
    ?>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
    <thead>
        <tr>
            <th width="3%">&nbsp;</th>
            <th>&nbsp;</th>
            <th width="15%">Kelas</th>
            <th width="15%">Harga (Rp)</th>
            <th width="15%">Banyak</th>
            <th width="15%">Total (Rp)</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $icek = 0; //dipakai untuk mengatur tampilan pertama kolom tabel sesuai ukurannya dengan yg bawah
            $cols = '';
            $total_biaya = 0;
            foreach($row as $key=>$values)
            {
                $icek++;
                $modRuangan = RuanganM::model()->findByPK($key); 
                if($modRuangan->instalasi_id == 4){
                    $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array(
                        'pendaftaran_id'=>$values['pendaftaran_id'],
                    ));
                    $modKamarRuangan = KamarruanganM::model()->findByPk($modPasienAdmisi->kamarruangan_id);
                    if($icek==1){
                        $cols .= '<tr style="page-break-inside: avoid;">';
                    }else{
                        $cols .= '</tbody>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
                                <tbody>
                                <tr style="page-break-inside: avoid;">';
                    }
                    
                    $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .' Kamar ' . $modKamarRuangan->kamarruangan_nokamar . ' Bed ' . $modKamarRuangan->kamarruangan_nobed .'</b></td>';
                    $cols .= '</tr>';
                }else{
                    if($icek==1){
                        $cols .= '<tr style="page-break-inside: avoid;">';
                    }else{
                        $cols .= '</tbody>
                                </table>
                                <table width="100%" cellpadding="0" cellspacing="0" border="0" class="grid">
                                <tbody>
                                <tr style="page-break-inside: avoid;">';
                    }
                    
                    $cols .= '<td colspan=6><b>'.strtoupper($values['nama']) .'</b></td>';
                    $cols .= '</tr>';
                }
                $col = '';
                $total = 0;
                $tampilAdminFarmasi = true;
                $tempAdminFarmasi = 0;
                $subsidiAsuransi = 0;
                $subsidiPemerintah = 0;
                $subsidiRumahSakit = 0;
                $iurBiaya = 0;
                foreach($values['kategori'] as $val)
                {
                    //menentukan harga obat harga dilihat dari view tabel
//                    if($values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ || $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_DEPO){
//                       $modObat = ObatalkesM::model()->findByAttributes(array('obatalkes_id'=>$val['daftartindakan_id'])); 
//                       $val['harga'] = $modObat->hjaresep;
//                       $val['total'] = $val['qty'] * $val['harga'];
//                    }
                    if($values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_DEPO || $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ || $values['ruangan_id'] == Params::RUANGAN_ID_LAB)
                        {
                            if($values['ruangan_id'] != Params::RUANGAN_ID_LAB){
                                if($totalbiayaadminfarmasi > 0 && $tampilAdminFarmasi == true){
                                    $col .= '<tr>';
                                    $col .= '<td></td>';
                                    $col .= '<td>Biaya Racik, dll</td>';
                                    $col .= '<td>'. $val['kelas'] .'</td>';
                                    $col .= '<td style="text-align:right;">'.MyFunction::formatNumber($totalbiayaadminfarmasi).'</td>';
                                    $col .= '<td style="text-align:right;">1</td>';
                                    $col .= '<td style="text-align:right;">'.MyFunction::formatNumber($totalbiayaadminfarmasi).'</td>';
                                    $col .= '</tr>';
                                    $tampilAdminFarmasi = false;
                                    $tempAdminFarmasi = $totalbiayaadminfarmasi;
                                }
                            }
                        }else{
                            $tempAdminFarmasi = 0;
                        }
                    $col .= '<tr>';
                    $col .= '<td>&nbsp;</td>';
                    $col .= '<td>'. $val['daftartindakan_nama'] .'</td>';
                    $col .= '<td>'. $val['kelas'] .'</td>';
                    $col .= '<td align="right" class="uang">'. number_format($val['harga'] + $val['harga_dokter'],0,'','.') .'</td>'; //merge tarif dokter
                    $col .= '<td align="right" class="uang">'. $val['qty'] .'</td>';
                    $col .= '<td align="right" class="uang">'. number_format($val['total'] + $val['total_dokter'],0,'','.') .'</td>';
                    $col .= '</tr>';
                    
                    if($values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_DEPO || $values['ruangan_id'] == Params::RUANGAN_ID_APOTEK_RJ || $values['ruangan_id'] == Params::RUANGAN_ID_LAB)
                    {
                        
                    }else{
                        if(strlen($val['nama_pegawai']) > 0)
                        {
//                            $col .= '<tr>';
//                            $col .= '<td>&nbsp;</td>';
//                            $col .= '<td>'. $val['nama_pegawai'] .'</td>';
//                            $col .= '<td>'. $val['kelas'] .'</td>';
//                            $col .= '<td class="uang" align="right">'. number_format($val['harga_dokter'],0,'','.') .'</td>';
//                            $col .= '<td class="uang" align="right">'. $val['qty'] .'</td>';
//                            $col .= '<td class="uang" align="right">'. number_format($val['total_dokter'],0,'','.') .'</td>';
//                            $col .= '</tr>'; 
                            if(strtoupper($values['nama']) != 'PENDAFTARAN'){
//                                $col .= '<tr>';
//                                $col .= '<td>&nbsp;</td>';
//                                $col .= '<td>'. $val['nama_pegawai'] .'</td>';
//                                $col .= '<td></td>';
//                                $col .= '<td></td>';
//                                $col .= '<td></td>';
//                                $col .= '<td></td>';
//                                $col .= '</tr>';
                            }
                                
                        }
                    }
                    $total += ($val['qty'] * $val['harga']) + ($val['qty'] * $val['harga_dokter']);//$val['total'] + $val['total_dokter'];
                    $subsidiAsuransi += $val['subsidiasuransi_tindakan'];
                    $subsidiPemerintah += $val['subsidipemerintah_tindakan'];
                    $subsidiRumahSakit += $val['subsisidirumahsakit_tindakan'];
                    $iurBiaya += $val['iurbiaya_tindakan'];
                }
                $subsidiAsuransi = 0;
                $subsidiPemerintah = 0;
                $subsidiRumahSakit = 0;
                foreach ($modRincian as $key => $datarinciansaja) {
                    $subsidiAsuransi += $datarinciansaja->subsidiasuransi_tindakan;
                    $subsidiPemerintah += $datarinciansaja->subsidipemerintah_tindakan;
                    $subsidiRumahSakit += $datarinciansaja->subsisidirumahsakit_tindakan;
                }
                $total = round($total + $tempAdminFarmasi);
                $col .= '<tr>';
                $col .= '<td colspan=5 class="total">Total Biaya :</td>';
                $col .= '<td class="total" align="right">'. number_format($total,0,'','.') .'</td>';
                $col .= '</tr>';
                $cols .= $col;
                $total_biaya += $total;
            }
            echo($cols);
            if($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit > 0){
                $iurBiaya = $total_biaya - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit);  //karena $iurBiaya yang diambil di tindakanpelayanan_t sering tidak sama dengan total biaya - subsidi 
            }else{
                $iurBiaya = 0;
            }
    ?>
    </tbody>
</table>
<br>
<table width="100%" cellpadding="0" cellspacing="0" border="0" class="info" style="page-break-inside: avoid;">
    <tr>
        <td width="50%" align="center" valign="top">
            <table width="100%">
                <tr>
                    <td width="50%" align="center">
                        <div>Tasikmalaya, <?php echo Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-mm-dd hh:mm:ss')); ?></div>
                        <div>Petugas RSJK</div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div>
                            <?php echo $data['nama_pegawai']; ?>
                        </div>
                    </td>
                </tr>
            </table>            
        </td>
        <td align="center" valign="top">
            <table width="100%" style="text-align: right;">
                <tr>
                    <td><b>Total Biaya</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($total_biaya,0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Subsidi Asuransi</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($subsidiAsuransi,0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Subsidi Pemerintah</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($subsidiPemerintah,0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Subsidi Rumah Sakit</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($subsidiRumahSakit,0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Iur Biaya</b></td>
                    <td width="3%">:</td>
                    <td class="totalSeluruh"><?php echo number_format($iurBiaya,0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Deposit</b></td>
                    <td>:</td>
                    <td class="totalSeluruh"><?php echo number_format($data['uang_cicilan'],0,'','.'); ?></td>
                </tr>
                <tr>
                    <td><b>Tanggungan Pasien</b></td>
                    <td>:</td>
                    <td class="totalSeluruh">
                        <?php 
                            $kembalian = ($total_biaya - $subsidiAsuransi - $subsidiPemerintah - $subsidiRumahSakit);
                            if($data['uang_cicilan'] > 0){
                                if($data['uang_cicilan'] < $total_biaya)
                                {
                                    $kembalian = $kembalian - $data['uang_cicilan'];
                                }                                            
                            }
                            echo number_format($kembalian,0,'','.');
                        ?>
                    </td>
                </tr>
            </table>
            <div id="cetakan_jum" style="margin-top: 40px;font-size: 11px"></div>
        </td>
    </tr>
</table>
<script type="text/javascript">

    function insertCetakan()
    {
        var params = {jenis_cetakan:'<?=Params::jenisCetakan($data['jenis_cetakan'])?>',pendaftaran_id:'<?=$modPendaftaran->pendaftaran_id?>'};
        
        $.post("<?php echo Yii::app()->createUrl('ActionAjax/updateJumlahCetakan');?>", {id:params},
            function(data)
            {
                if(data.status == 'not')
                {
                    console.log('insert cetakan data error');
                }else{
                    $('#cetakan_jum').text('Cetakan Ke ' + data.jumlah);
                }
            }, "json"
        );
    }
    insertCetakan();

</script>