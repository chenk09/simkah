<?php
$this->breadcrumbs=array(
	'Konsul Poli',
);

$this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,'modAdmisi'=>$modAdmisi));

echo '<legend class="rim">KONSUL POLIKLINIK</legend><hr>';
$this->renderPartial('/_tabulasi',array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi));
//$this->widget('bootstrap.widgets.BootMenu', array(
//    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
//    'stacked'=>false, // whether this is a stacked menu
//    'items'=>array(
//        array('label'=>'Anamnesis', 'url'=>$this->createUrl('/rawatInap/anamnesaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Periksa Fisik', 'url'=>$this->createUrl('/rawatInap/PemeriksaanFisikTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
//        array('label'=>'Laboratorium', 'url'=>$this->createUrl('/rawatInap/laboratoriumTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Radiologi', 'url'=>$this->createUrl('/rawatInap/radiologiTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Rehab Medis', 'url'=>$this->createUrl('/rawatInap/rehabMedisTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsultasi Gizi', 'url'=>$this->createUrl('/rawatInap/konsulGiziTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Konsul Poliklinik', 'url'=>'','linkOptions'=>array(),'active'=>true),
//        array('label'=>'Tindakan', 'url'=>$this->createUrl('/rawatInap/tindakanTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id)),'linkOptions'=>array('onclick'=>'return palidasiForm(this);')),
//        array('label'=>'Diagnosis', 'url'=>$this->createUrl('/rawatInap/diagnosaTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Bedah Sentral', 'url'=>$this->createUrl('/rawatInap/bedahSentralTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Rujukan Ke Luar', 'url'=>$this->createUrl('/rawatInap/rujukanKeluarTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Reseptur', 'url'=>$this->createUrl('/rawatInap/resepturTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//        array('label'=>'Pemakaian Bahan', 'url'=>$this->createUrl('/rawatInap/pemakaianBahanTRI',array('idPendaftaran'=>$modPendaftaran->pendaftaran_id,'idAdmisi'=>$modAdmisi->pasienadmisi_id))),
//    ),
//));
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'rjkonsul-poli-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
)); ?>

    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary($modKonsul); ?>
    <table>
        <tr>
            <td width="50%">
                <div class="control-group ">
                    <?php echo $form->labelEx($modKonsul,'tglkonsulpoli', array('class'=>'control-label')) ?>
                    <?php $modKonsul->tglkonsulpoli = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modKonsul->tglkonsulpoli, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modKonsul,
                                                    'attribute'=>'tglkonsulpoli',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->dropDownListRow($modKonsul,'ruangan_id', CHtml::listData($modKonsul->getRuanganInstalasiItems($modPendaftaran->instalasi_id,true), 'ruangan_id', 'ruangan_nama'),
                                                    array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->dropDownListRow($modKonsul,'pegawai_id', CHtml::listData($modKonsul->getDokterItems(), 'pegawai_id', 'nama_pegawai'),
                                                    array('empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td width="50%">
                <?php //echo $form->dropDownListRow($modKonsul,'asalpoliklinikkonsul_id', CHtml::listData($modKonsul->getRuanganInstalasiItems(''), 'ruangan_id', 'ruangan_nama'),
                                                //array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modKonsul,'catatan_dokter_konsul',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
<!--        <tr>
            <td colspan="2">
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th colspan="2">Karcis Tindakan</th>
                        </tr>
                    </thead>
                    <?php //foreach ($karcisTindakan as $i => $karcis) { ?>
                    <tr>
                        <td width="15px;">
                            <?php //echo CHtml::checkBox('karcis[]', false, array()); ?>
                        </td>
                        <td>
                            <?php //echo $karcis->daftartindakan_nama; ?>
                        </td>
                    </tr>
                    <?php //} ?>
                </table>
            </td>
        </tr>-->
    </table>
    
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
									<?php           $content = $this->renderPartial('../tips/tips',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content)); ?>
    </div>

<?php $this->endWidget(); ?>

<?php $this->renderPartial('_listKonsulPoli',array('modRiwayatKonsul'=>$modRiwayatKonsul)); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogDetailKonsul',
    'options'=>array(
        'title'=>'Detail Konsul',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>800,
        'resizable'=>false,
        'position'=>'top',
    ),
));

    echo '<div id="contentDetailKonsul">dialog content here</div>';

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>
<script type="text/javascript">
function viewDetailKonsul(idKonsulAntarPoli)
{
    $.post('<?php echo $this->createUrl('ajaxDetailKonsul') ?>', {idKonsulAntarPoli: idKonsulAntarPoli}, function(data){
        $('#contentDetailKonsul').html(data.result);
    }, 'json');
    $('#dialogDetailKonsul').dialog('open');
}
function batalKonsul(idKonsulAntarPoli,idPendaftaran)
{
    if(confirm('Apakah anda akan membatalkan konsul ini?')){
        $.post('<?php echo $this->createUrl('ajaxBatalKonsul') ?>', {idKonsulAntarPoli: idKonsulAntarPoli, idPendaftaran:idPendaftaran}, function(data){
            $('#tblListKonsul').html(data.result);
        }, 'json');
    }
}
</script>