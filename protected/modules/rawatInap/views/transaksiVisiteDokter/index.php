<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
  'id'=>'pasienVisite-form',
  'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#namaDokter',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); 
$this->widget('bootstrap.widgets.BootAlert'); ?>
<legend class="rim2">Transaksi Visite dokter</legend>
<!-- <legend class="rim">Berdasarkan tanggal</legend> -->
<table>
    <tr>
        <td width="120px">
  
    <?php echo CHtml::label('Tanggal Visite *','Tanggal Visite *', array()); ?></td>
        <td><?php   
                    $this->widget('MyDateTimePicker',array(
                                    'name'=>'tangalVisite',
                                    'value'=>Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-MM-dd hh:mm:ss')),
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_TIME_FORMAT,
                                        'maxDate' => 'd',
                                        
                                    ),
                                    'htmlOptions'=>array('readonly'=>true,
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'class'=>'isRequired'
                                        ),
                            )); ?>
        </td>
    </tr>
    <tr>
        <td width="120px"><?php echo CHtml::label('Nama Dokter','Nama Dokter', array()); ?></td>
        <td>
        <?php $this->widget('MyJuiAutoComplete',array(
                        'name'=>'namaDokter',    
                        'value'=>'',
                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/GetDokterJenisKelamin'),
                        'options'=>array(
                           'showAnim'=>'fold',
                           'minLength' => 2,
                           'focus'=> 'js:function( event, ui ) {
                                $(this).val( ui.item.label);
                                
                                return false;
                            }',
                            'select'=>'js:function( event, ui ) {
                             samakanDokter(ui.item.pegawai_id);

                                      }'
     
                        ),
                        'tombolDialog'=>array('idDialog'=>'dialogDokter'),
                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)"),
                 )); ?>
        </td>
    </tr>
    <tr>
        <td width="120px"><?php echo CHtml::label('Jenis Visite','Jenis Visite', array()); ?></td>
        <td>
        <?php $this->widget('MyJuiAutoComplete',array(
                        'name'=>'jenisVisite',    
                        'value'=>'',
                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/GetDaftarTindakanVisite'),
                        'options'=>array(
                           'showAnim'=>'fold',
                           'minLength' => 2,
                           'focus'=> 'js:function( event, ui ) {
                                $(this).val( ui.item.label);
                                
                                return false;
                            }',
                            'select'=>'js:function( event, ui ) {
                             samakanVisite(ui.item.daftartindakan_id);
                                      }'
     
                        ),
                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)"),
                 )); ?>
        </td>
    </tr>
</table>
<?php
     $this->widget('ext.bootstrap.widgets.BootGridView', array(
  'id'=>'daftarPasien-grid',
  'dataProvider'=>$model->searchRIVisiteDokter(),
                'template'=>"\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
  'columns'=>array(
                    array(
                       'header'=>'Tgl Admisi / Masuk Kamar',
                        'type'=>'raw',
                        'value'=>'$data->tglAdmisiMasukKamar'
                    ),
//                    'ruangan_nama',
//                    array(
//                       'name'=>'caramasuk_nama',
//                        'type'=>'raw',
//                        'value'=>'$data->caramasuk_nama',
//                    ),
                    array(
                       'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Alias',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
//                    array(
//                        'name'=>'umur',
//                        'value'=>'$data->umur',
//                    ),
//                    array(
//                       'name'=>'Dokter',
//                        'type'=>'raw',
//                        'value'=>'$data->nama_pegawai',
//                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                   array(
                        'header'=>'Dokter *',
                        'type'=>'raw',
                        'value'=>'CHtml::dropDownList("RITindakanPelayananT[pegawai_id][]","",CHtml::listdata(DokterV::model()->findAll(\'ruangan_id='.Yii::app()->user->getState('ruangan_id').'\'),"pegawai_id","nama_pegawai"),array("style"=>"width:100px;font-size:11px;","empty"=>"- Pilih -","onkeypress"=>"return $(this).focusNextInputField(event)",
                                                       "class"=>"idDokter"))
                                                       .CHtml::hiddenField("RITindakanPelayananT[penjamin_id][]",$data->penjamin_id,array("readonly"=>TRUE,"class"=>"span1"))                                   
                                                       .CHtml::hiddenField("RITindakanPelayananT[pasienadmisi_id][]",$data->pasienadmisi_id,array("readonly"=>TRUE,"class"=>"span1"))
                                                       .CHtml::hiddenField("RITindakanPelayananT[kelaspelayanan_id][]",$data->kelaspelayanan_id,array("readonly"=>TRUE,"class"=>"span1"))
                                                       .CHtml::hiddenField("RITindakanPelayananT[pasien_id][]",$data->pasien_id,array("readonly"=>TRUE,"class"=>"span1"))                                   
                                                       .CHtml::hiddenField("RITindakanPelayananT[pendaftaran_id][]",$data->pendaftaran_id,array("readonly"=>TRUE,"class"=>"span1"))                                   
                                                       .CHtml::hiddenField("RITindakanPelayananT[carabayar_id][]",$data->carabayar_id,array("readonly"=>TRUE,"class"=>"span1"))                                   
                                                       .CHtml::hiddenField("RITindakanPelayananT[jeniskasuspenyakit_id][]",$data->jeniskasuspenyakit_id,array("readonly"=>TRUE,"class"=>"span1"))                                   
                        ',),
                    array(
                        'header'=>'Visite Dokter *',
                        'type'=>'raw',
                        'value'=>'CHtml::dropDownList("RITindakanPelayananT[daftartindakan_id][]","",CHtml::listdata(RIDaftarTindakanM::model()->findAll(\'daftartindakan_visite=TRUE\'),"daftartindakan_id","daftartindakan_nama"),array("style"=>"width:100px;font-size:11px;","empty"=>"- Pilih -","onkeypress"=>"return $(this).focusNextInputField(event)",
                                                       "class"=>"idVisite"))',),
                    
                     array(
                       'header'=>'Pilih',
                       'type'=>'raw',
                       'value'=>'CHtml::checkBox("RITindakanPelayananT[ceklist][]",false,array("class"=>"ceklist","onclick"=>"dipilih(this)","onkeypress"=>"return $(this).focusNextInputField(event)"))
                                .CHtml::hiddenField("RITindakanPelayananT[dipilih][]","Tidak",array("readonly"=>TRUE,"class"=>"span1 dipilih"))                                   
                                ',
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<div class="form-actions">
    <div style="display: none;">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan')); ?>
    </div>    
       <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'validasi()')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
        Yii::app()->createUrl($this->module->id.'/'.caraBayarM.'/admin'), 
        array('class'=>'btn btn-danger',
        'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    <?php
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
  </div>

<?php
$this->endWidget();
//$js = <<< JS
//
//
//
//
//JS;
//Yii::app()->clientScript->registerScript('JSriwayatPasien',$js,CClientScript::POS_READY);

$js = <<< JS

   
function samakanDokter(idPegawai){   
$('.idDokter').each(function(){
        $(this).val(idPegawai);
    });   
}

function samakanVisite(idVisite){   
$('.idVisite').each(function(){
    
        $(this).val(idVisite);
        
    });   
} 

    function dipilih(obj){
    
        if($(obj).is(':checked')){
            $(obj).parent().find('input').val('Ya');
        }else{
            $(obj).parent().find('input').val('Tidak');
        }
    }
    function validasi()
    {
        jumlahCeklist=0;
        validasiDokter='Ya';
        validasiVisite='Ya';
        
        $('.isRequired').each(function(){

            if($(this).val()==''){
                alert('Harap Isi Semua Yang Bertanda *')
                $(this).focus();
            }

        }); 
        
          $('.ceklist').each(function(){
            
            if($(this).is(':checked'))
               {
                  jumlahCeklist = jumlahCeklist +1;  
                  
                  if($(this).parent().prev().find('select').val()==''){
                        $(this).parent().prev().find('select').focus();
                                                validasiVisite='Tidak';

                    }
                  
                   if($(this).parent().prev().prev().find('select').val()==''){
                        $(this).parent().prev().prev().find('select').focus();
                        validasiDokter='Tidak';
                  }

               } 
          });
          
      if(jumlahCeklist==0){
        alert('Anda Belum Memilih Pasien');
      }else if(validasiDokter=='Tidak'){
        alert('Harap Isi Semua Data Dokter Yang Diperlukan');
      }else if (validasiVisite=='Tidak'){
        alert('Harap Isi Semua Data Visite Yang Diperlukan');
      }else{
        $('#btn_simpan').click();
//        alert('simpan');
      }    
         
    }
JS;
Yii::app()->clientScript->registerScript('sasfsddfsgfhgdfgsgsdg',$js,CClientScript::POS_HEAD);
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDokter = new DokterV('search');
$modDokter->unsetAttributes();
$modDokter->ruangan_id = Yii::app()->user->getState('ruangan_id');
if(isset($_GET['DokterV'])){
    $modDokter->attributes = $_GET['DokterV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengajukan-m-grid',
    'dataProvider'=>$modDokter->search(),
    'filter'=>$modDokter,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"samakanDokter($data->pegawai_id);
                            $(\"#namaDokter\").val(\"$data->nama_pegawai\");
                            $(\"#dialogDokter\").dialog(\"close\");
                            return false;"
                ))'
        ),
        
        'gelardepan',
         array(
            'name'=>'nama_pegawai',
            'header'=>'Nama Dokter',
         ),
        'gelarbelakang_nama',
        'jeniskelamin',
        'notelp_pegawai',
        'nomobile_pegawai',
        array(
            'name'=>'nomorindukpegawai',
            'header'=>'NIK',
         ),
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>