<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>


<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'pasienpulang-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
));
$this->widget('bootstrap.widgets.BootAlert');

?>
<?php
//    if(empty($modTariftindakan->harga_tariftindakan)){
//        echo "<script>
//                    alert('Maaf, Harga Tarif Kamar Rawat Inap Belum Ada. Silahkan Hubungi Bagian Administrasi');
//                    window.location.href(".Yii::app()->createUrl('/PasienRawatInap/index').");
//                </script>";
//    }else{
//        echo "<script>
//                    alert('Harga Tarif Kamar Rawat Inap Ada');
//                </script>";
//    }
?>
<?php if($status == true){ ?>

<?php }else{ ?>
<fieldset>
    <legend class="rim2">Pasien Pulang</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'tgl_pendaftaran', array('readonly'=>true)); ?></td>
            
            <td> <div class="control-label"> <?php echo CHtml::activeLabel($modPasienRIV, 'no_rekam_medik',array('class'=>'no_rek')); ?> </div></td>
            <td>
                <?php $this->widget('MyJuiAutoComplete',array(
                        'model'=>$modPasienRIV,
                        'attribute'=>'no_rekam_medik',
                        'value'=>'',
                        'source'=>'js: function(request, response) {
                               $.ajax({
                                   url: "'.Yii::app()->createUrl('ActionAutoComplete/PasienRawatInap').'",
                                   dataType: "json",
                                   data: {
                                       term: request.term,
                                   },
                                   success: function (data) {
                                           response(data);
                                   }
                               })
                            }',
                        'options'=>array(
                           'showAnim'=>'fold',
                           'minLength' => 2,
                           'focus'=> 'js:function( event, ui ) {
                                $(this).val( ui.item.label);
                                return false;
                            }',
                            'select'=>'js:function( event, ui ) {
                                  $("#'.CHtml::activeId($modPasienRIV,'tgl_pendaftaran').'").val(ui.item.tgl_pendaftaran);
                                  $("#'.CHtml::activeId($modPasienRIV,'no_pendaftaran').'").val(ui.item.no_pendaftaran);   
                                  $("#'.CHtml::activeId($modPasienRIV,'umur').'").val(ui.item.umur);     
                                  $("#'.CHtml::activeId($modPasienRIV,'jeniskasuspenyakit_nama').'").val(ui.item.jeniskasuspenyakit_nama);
                                  $("#'.CHtml::activeId($modPasienRIV,'no_pendaftaran').'").val(ui.item.no_pendaftaran);   
                                  $("#'.CHtml::activeId($modPasienRIV,'nama_pasien').'").val(ui.item.nama_pasien);     
                                  $("#'.CHtml::activeId($modPasienRIV,'jeniskelamin').'").val(ui.item.jeniskelamin);  
                                  $("#'.CHtml::activeId($modPasienRIV,'no_pendaftaran').'").val(ui.item.no_pendaftaran);  
                                  $("#'.CHtml::activeId($modPasienRIV,'nama_bin').'").val(ui.item.nama_bin);   
                                  $("#'.CHtml::activeId($modelPulang,'pendaftaran_id').'").val(ui.item.pendaftaran_id);     
                                  $("#'.CHtml::activeId($modelPulang,'pasien_id').'").val(ui.item.pasien_id);    
                                  $("#'.CHtml::activeId($modelPulang,'pasienadmisi_id').'").val(ui.item.pasienadmisi_id);
                                  $("#'.CHtml::activeId($modMasukKamar,'masukkamar_id').'").val(ui.item.masukkamar_id); 
                                  $("#'.CHtml::activeId($modMasukKamar,'tglmasukkamar').'").val(ui.item.tglmasukkamar); 
                                      }'
                        ),
                            'htmlOptions'=>array(
                                'readonly'=>false,
                                'placeholder'=>'No Rekam Medik',
                                'size'=>20,
                                'class'=>'span3',
                                'onkeypress'=>"return $(this).focusNextInputField(event);",
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogDaftarPasien','idTombol'=>'tombolPasienDialog'),
                 )); ?>
            </td>
        </tr>
        <tr>
            <td><label class="control-label">No Pendaftaran</label></td>
            <td>
                <?php echo CHtml::activeTextField($modPasienRIV, 'no_pendaftaran', array('readonly'=>true, 'class'=>'span2')); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'jeniskelamin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'umur', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'nama_pasien', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'jeniskasuspenyakit_nama',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'jeniskasuspenyakit_nama', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasienRIV, 'nama_alias',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasienRIV, 'nama_bin', array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset>
        
    <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

    <?php echo $form->errorSummary(array($modelPulang)); ?>
        <table>
            <tr>
                <td width="50%">
                    <?php //echo $form->textFieldRow($modelPulang,'pasienadmisi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modelPulang,'tglpasienpulang', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modelPulang,
                                                    'attribute'=>'tglpasienpulang',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                            )); ?>
                            <?php echo $form->error($modelPulang, 'tglpasienpulang'); ?> 
                        </div>
                    </div>
                    <?php echo $form->hiddenField($modelPulang,'pasienadmisi_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                    <?php echo $form->hiddenField($modelPulang,'pendaftaran_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                    <?php echo $form->hiddenField($modelPulang,'pasien_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);",'readonly'=>true)); ?>
                     
                        <?php echo CHtml::label('Cara Pulang <font color=red>*</font>', 'carakeluar', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($modelPulang,'carakeluar', CaraKeluar::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'carakeluar(this.value)')); ?>
                            <?php echo $form->error($modelPulang,'carakeluar',array('errorCssClass'=>'error')); ?>
                        </div>
                        </div>
                    
                    <?php echo $form->dropDownListRow($modelPulang,'kondisipulang', KondisiPulang::items(),array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",'onchange'=>'pasienmeninggal(this.value)')); ?>
                    <?php echo $form->textFieldRow($modelPulang,'penerimapasien',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    
                    <?php echo $form->textFieldRow($modMasukKamar,'tglmasukkamar',array('class'=>'span3','readonly'=>true)) ?>
                    <?php echo $form->hiddenField($modMasukKamar,'masukkamar_id',
                                  array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'class'=>'span2','readonly'=>TRUE)); ?>
                    <div class="control-group ">
                        <?php //echo $form->labelEx($modMasukKamar,'tglkeluarkamar', array('class'=>'control-label')) ?>
                        <?php echo CHtml::label('Tgl Pulang Kamar', 'tglkeluarkamar', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modMasukKamar,
                                                    'attribute'=>'tglkeluarkamar',
                                                    'mode'=>'datetime',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                         'class'=>'dtPicker3',
                                                                         'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                         ),
                            )); ?>
                            <?php echo $form->error($modMasukKamar, 'tglkeluarkamar'); ?>   
                        </div>
                    </div>

                    <div class="control-group ">
                        <?php //echo $form->labelEx($modMasukKamar,'jamkeluarkamar', array('class'=>'control-label')); ?>
                        <?php echo CHtml::label('Jam Pulang Kamar', 'jamkeluarkamar', array('class'=>'control-label')); ?>
                        <div class="controls">
                            <?php $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modMasukKamar,
                                                    'attribute'=>'jamkeluarkamar',
                                                    'mode'=>'time',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,
                                                                         'class'=>'dtPicker3',
                                                                         'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                                         ),
                            )); ?>
                            <?php echo $form->error($modMasukKamar, 'jamkeluarkamar'); ?>
                        </div>
                    </div>
                     <div class="control-group ">
                            <?php echo $form->labelEx($modMasukKamar,'lamadirawat_kamar', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php echo $form->textField($modMasukKamar,'lamadirawat_kamar',array('class'=>'span1', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?> Hari
                            </div>
                        </div>
                   
                </td>
                <td width="50%">
                    <div class="control-group ">
                            <?php echo $form->labelEx($modelPulang,'keterangankeluar', array('class'=>'control-label')) ?>
                            <div class="controls">
                                <?php echo $form->textArea($modelPulang,'keterangankeluar',array('class'=>'span3', 'cols'=>50, 'rows'=>3)); ?>
                            </div>
                        </div>
                   <fieldset class="">
                        <legend>
                            <?php echo CHtml::checkBox('isDead', $modelPulang->isDead, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            Pasien Meninggal
                        </legend>
                        <div class="control-group ">
                                <?php echo $form->labelEx($modelPulang,'tgl_meninggal', array('class'=>'control-label')) ?>
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modelPulang,
                                                            'attribute'=>'tgl_meninggal',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','disabled'=>true),
                                    )); ?>

                                </div>
                            </div>
                    </fieldset> 
                    <fieldset class="">
                        <legend>
                            <?php echo CHtml::checkBox('isKontrol', $modelPulang->isKontrol, array('onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                            Rencana Kontrol Pasien
                        </legend>
                        <div class="control-group ">
                                <?php echo $form->labelEx($modPendaftaran,'tglrenkontrol', array('class'=>'control-label')) ?>
                                <div class="controls">
                                    <?php   
                                            $this->widget('MyDateTimePicker',array(
                                                            'model'=>$modPendaftaran,
                                                            'attribute'=>'tglrenkontrol',
                                                            'mode'=>'datetime',
                                                            'options'=> array(
                                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                            ),
                                                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3','disabled'=>true),
                                    )); ?>

                                </div>
                            </div>
                    </fieldset> 
                </td>
            </tr>
        </table>
        
        
        
        <?php
           // echo $this->renderPartial('_formUpdateMasukKamar',array('form'=>$form,'modMasukKamar'=>$modMasukKamar));
            echo $this->renderPartial('_formRujukanKeluar',array('form'=>$form,'modelPulang'=>$modelPulang,'modRujukanKeluar'=>$modRujukanKeluar)) 
        ?>

        
    <div class="form-actions">
                 <?php echo CHtml::htmlButton($modelPulang->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                       Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                        array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
    
                <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('pasienRawatInap/TindakLanjutDrTransaksi'), array('class'=>'btn btn-danger')); ?>
     
 <?php 
           $content = $this->renderPartial('../tips/transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content));
        ?>
    </div>
<?php } ?>
<?php $this->endWidget(); ?>
<?php
if($tersimpan=='Ya'){
?>
<script>
parent.location.reload();
</script>
<?php
}
?>
<script>
    function carakeluar(value)
    {
        if(value == "<?php echo Params::KODE_RUJUKAN ?>")
        {
            $('#pakeRujukan').attr('checked',true);
            $('#divRujukan input').removeAttr('disabled');
            $('#divRujukan select').removeAttr('disabled');
            $('#divRujukan').slideToggle(500);
            
        }
        else
        {
            $('#pakeRujukan').removeAttr('checked');
            $('#divRujukan input').attr('disabled','true');
            $('#divRujukan select').attr('disabled','true');
            $('#divRujukan input').attr('value','');
            $('#divRujukan select').attr('value','');
            $('#divRujukan').hide(500);
            
        }
    }
    function pasienmeninggal(value)
    {
        if(value == "<?php echo Params::KODE_MENINGGAL_1 ?>" || value == "<?php echo Params::KODE_MENINGGAL_2 ?>")
        {
             $('#isDead').attr('checked',true);
             $('#PasienpulangT_tgl_meninggal').removeAttr('disabled');
        }
        else
        {
            $('#isDead').removeAttr('checked');
            $('#PasienpulangT_tgl_meninggal').attr('disabled','true');
        }
    }
    $('#isDead').change(function(){
        if ($(this).is(':checked')){
                $('#RIPasienPulangT_tgl_meninggal').removeAttr('disabled');
                $('#RIPasienPulangT_kondisipulang').val('<?php echo Params::KODE_MENINGGAL_1 ?>');
                $('#RIPasienPulangT_tgl_meninggal').val('<?php echo Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-MM-dd HH:ii:ss')); ?>');
        }else{
                $('#RIPasienPulangT_tgl_meninggal').attr('disabled','true');
                $('#RIPasienPulangT_kondisipulang').val('');
                $('#RIPasienPulangT_tgl_meninggal').val('');
                
        }
    });
    function konfirmasi()
    {
        if(confirm('<?php echo Yii::t('mds','Do You want to cancel?') ?>'))
        {
            $('#dialogPasienPulang').dialog('close');
        }
        else
        {   
            $('#RIPasienPulangT_carakeluar').focus();
            return false;
        }
    }
    $('#isKontrol').change(function(){
        if ($(this).is(':checked')){
                $('#RIPendaftaranT_tglrenkontrol').removeAttr('disabled');
                $('#RIPendaftaranT_tglrenkontrol').val('<?php echo Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d H:i:s'), 'yyyy-MM-dd HH:ii:ss')); ?>');
        }else{
                $('#RIPendaftaranT_tglrenkontrol').attr('disabled','true');
                $('#RIPendaftaranT_tglrenkontrol').val('');
                
        }
    });
</script>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
            'id'=>'dialogDaftarPasien',
            'options'=>array(
                'title'=>'Daftar Pasien',
                'autoOpen'=>false,
                'resizable'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>600,
            ),
        ));
    
    $this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'daftarpasien-v-grid',
    'dataProvider'=>$modPasienRIV->searchRI(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'filter'=>$modPasienRIV,
    'columns'=>array(   
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPendaftaran",
                                    "onClick" => "
                                        $(\"#dialogDaftarPasien\").dialog(\"close\");

                                        $(\"#RIInfopasienmasukkamarV_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                        $(\"#RIInfopasienmasukkamarV_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                        $(\"#RIInfopasienmasukkamarV_umur\").val(\"$data->umur\");

                                        $(\"#RIInfopasienmasukkamarV_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");

                                        $(\"#RIInfopasienmasukkamarV_jeniskelamin\").val(\"$data->jeniskelamin\");
                                        $(\"#RIInfopasienmasukkamarV_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                        $(\"#RIInfopasienmasukkamarV_nama_pasien\").val(\"$data->nama_pasien\"); 
                                        $(\"#RIInfopasienmasukkamarV_nama_bin\").val(\"$data->nama_bin\");
                                        $(\"#RIMasukKamarT_tglmasukkamar\").val(\"$data->tglmasukkamar\");
                                        $(\"#RIMasukKamarT_masukkamar_id\").val(\"$data->masukkamar_id \");
                                        $(\"#RIPasienPulangT_pendaftaran_id\").val(\"$data->pendaftaran_id \");
                                        $(\"#RIPasienPulangT_pasien_id\").val(\"$data->pasien_id \");
                                        $(\"#RIPasienPulangT_pasienadmisi_id\").val(\"$data->pasienadmisi_id \");

                                    "))',
                    
                ),
                    
                'no_rekam_medik',   
                'tgl_pendaftaran',
                'no_pendaftaran',
                'nama_pasien',
                array(
                    'header'=>'Alias',
                    'type'=>'raw',
                    'value'=>'"$data->nama_bin"',
                ),
                array(
                    'header'=>'Penjamin'.' /<br/>'.'Cara Bayar',
                    'type'=>'raw',
                    'value'=>'"$data->penjamin_nama"."<br/>"."$data->carabayar_nama"',
                ),
                array(
                    'header'=>'Nama Dokter',
                    'type'=>'raw',
                    'name'=>'nama_pegawai',
                    'value'=>'"$data->nama_pegawai"',
                ),
                // 'ruangan_nama',
        'jeniskasuspenyakit_nama',
        // 'statusperiksa',
                
    ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 

$this->endWidget('zii.widgets.jui.CJuiDialog');
?>