<style>
    .row-red td{
        background-color: indianred !important;
        color: #ffffff;
    }
</style>

<legend class="rim2">Informasi Pasien Rawat Inap</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai

Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasien-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
//echo Yii::app()->user->getState('ruangan_id');
?>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'daftarPasien-grid',
	'dataProvider'=>$model->searchRI(),
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'rowCssClassExpression'=>'($data->statusfarmasi == "Sudah" ? "row-red" : "row-white" )',
	'columns'=>array(
                    array(
                       'header'=>'Tgl Admisi / Masuk Kamar',
                        'type'=>'raw',
                        'value'=>'$data->tglAdmisiMasukKamar'
                    ),
//                    'ruangan_nama',
                    array(
                       'name'=>'caramasuk_nama',
                        'type'=>'raw',
                        'value'=>'$data->caramasuk_nama',
                    ),
                    array(
                       'header'=>'No RM / No Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->noRmNoPend',
                    ),
                    array(
                        'header'=>'Nama Pasien / Alias',
                        'value'=>'$data->namaPasienNamaBin'
                    ),
                    array(
                        'name'=>'jeniskelamin',
                        'value'=>'$data->jeniskelamin',
                    ),
                    array(
                        'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'CHtml::hiddenField("RIInfokunjunganriV[$data->pendaftaran_id][idPendaftaran]", $data->pendaftaran_id, array("id"=>"idPendaftaran","onkeypress"=>"return $(this).focusNextInputField(event)","class"=>"span3"))."".$data->umur',
                    ),
                    array(
                       'name'=>'Dokter',
                        'type'=>'raw',
                        'value'=>'$data->nama_pegawai',
                    ),
                    array(
                        'header'=>'Cara Bayar / Penjamin',
                        'value'=>'$data->caraBayarPenjamin',
                    ),
                    array(
                       'name'=>'kelaspelayanan_nama',
                        'type'=>'raw',
                        'value'=>'$data->kelaspelayanan_nama',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                        'header'=>'No.Kamar <br> No.Bed',
                       'name'=>'kamarruangan_nokamar',
                        'type'=>'raw',
                        'value'=>'(!empty($data->kamarruangan_nokamar))? "Kmr : ".$data->kamarruangan_nokamar."<br>"."Bed : ".$data->kamarruangan_nobed.CHtml::link("<i class=icon-pencil></i>","",array("href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Memasukan Pasien Ke kamar","onclick"=>"{buatSessionMasukKamar($data->masukkamar_id,$data->kelaspelayanan_id,$data->pendaftaran_id); addMasukKamar(); $(\'#dialogMasukKamar\').dialog(\'open\');}return false;")) : CHtml::link("<i class=icon-home></i>","",array("href"=>"","rel"=>"tooltip","title"=>"Klik Untuk Memasukan Pasien Ke kamar","onclick"=>"{buatSessionMasukKamar($data->masukkamar_id,$data->kelaspelayanan_id,$data->pendaftaran_id); addMasukKamar(); $(\'#dialogMasukKamar\').dialog(\'open\');}return false;"))',
                    ),
                    array(
                       'header'=>'Pindah Kamar',
                       'type'=>'raw',
                       'value'=>'((!empty($data->pasienpulang_id)) ? $data->carakeluar : CHtml::link("<i class=\'icon-share\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/PindahKamarPasienRI",array("idPendaftaran"=>$data->pendaftaran_id)) ,array("title"=>"Klik Untuk Pindah Kamar","target"=>"iframePindahKamar", "onclick"=>"$(\"#dialogPindahKamar\").dialog(\"open\");", "rel"=>"tooltip")))',
                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                        'name'=>'Periksa Pasien',
                        'type'=>'raw',
//                        'value'=>'((!empty($data->pasienpulang_id)) ? "-" : CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatInap/anamnesaTRI",array("idPendaftaran"=>$data->pendaftaran_id,"idAdmisi"=>$data->pasienadmisi_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien")))',
                        'value'=>'(CHtml::link("<i class=\'icon-list-alt\'></i> ", Yii::app()->controller->createUrl("/rawatInap/anamnesaTRI",array("idPendaftaran"=>$data->pendaftaran_id,"idAdmisi"=>$data->pasienadmisi_id)),array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Pemeriksaan Pasien")))',
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),

//                    array(
//                       'header'=>'Tindak Lanjut',
//                       'type'=>'raw',
//                       'value'=>'((!empty($data->pasienpulang_id)) ? $data->carakeluar : CHtml::link("<i class=\'icon-share\'></i> ",Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/TindakLanjutDariPasienRI",array("idPendaftaran"=>$data->pendaftaran_id)) ,array("title"=>"Klik Untuk Tindak lanjut Pasien","target"=>"iframeTindakLanjut", "onclick"=>"$(\"#dialogTindakLanjut\").dialog(\"open\");", "rel"=>"tooltip"))."<br>".CHtml::link("<i class=\'icon-remove\'></i>", "javascript:cekHakAkses($data->pendaftaran_id)",array("id"=>"$data->no_pendaftaran","rel"=>"tooltip","title"=>"Klik untuk Batal Rawat Inap")))',
//                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//                    ),
                    array(
                       'header'=>'Pindahan Dari',
                       'type'=>'raw',
                       'value'=>'($data->PindahanDari->pindahkamar_id == "") ?  "Bukan Pindahan" :
                                "Rg:".$data->PindahanDari->ruangan->ruangan_nama." Kmr:".$data->PindahanDari->kamarruangan->kamarruangan_nokamar." Bed:".$data->PindahanDari->kamarruangan->kamarruangan_nobed."<br>".
                                ($data->TindakanDanObat["ada"] ? CHtml::link("Sedang Diperiksa", "#",array("title"=>"Silahkan batalkan dulu ".$data->TindakanDanObat["msg"]."!")) : CHtml::link("<i class=icon-remove-sign></i>","#",array("rel"=>"tooltip","title"=>"Klik Untuk Batal Pindah Kamar","onclick"=>"batalPindahKamar(".$data->PindahanDari->pindahkamar_id.",".$data->PindahanDari->masukkamar_id.");")))
                            ',
                    ),
                    array(
                       'header'=>'Pulangkan Pasien <br/> Rencana Pulang',
                       'type'=>'raw',
                       'value'=>'((!empty($data->pasienpulang_id)) ? $data->carakeluar : CHtml::link("<i class=\'icon-share\'></i> ",
                                 Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/TindakLanjutDariPasienRI",
                                 array("idPendaftaran"=>$data->pendaftaran_id)) ,
                                 array("title"=>"Klik Untuk Pemulangan Pasien","target"=>"iframeTindakLanjut",
                                 "onclick"=>"$(\"#dialogTindakLanjut\").dialog(\"open\");", "rel"=>"tooltip")))." /"."<br/>".

                                 ((!empty($data->pasienadmisi->rencanapulang)) ? CHtml::link("<i class=\'icon-edit\'></i> Rencana Pulang ".$data->pasienadmisi->rencanapulang,
                                 Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/RencanaPulangPasienRI",
                                 array("idPasienadmisi"=>$data->pasienadmisi_id)) ,
                                 array("title"=>"Klik Untuk Rencana Pulang Pasien","target"=>"iframeRencanaPulang",
                                "onclick"=>"$(\"#dialogRencanaPulang\").dialog(\"open\");", "rel"=>"tooltip")) : CHtml::link("<i class=\'icon-time\'></i> ",
                                 Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/RencanaPulangPasienRI",
                                 array("idPasienadmisi"=>$data->pasienadmisi_id)) ,
                                 array("title"=>"Klik Untuk Rencana Pulang Pasien","target"=>"iframeRencanaPulang",
                                "onclick"=>"$(\"#dialogRencanaPulang\").dialog(\"open\");", "rel"=>"tooltip")))',
                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
                    array(
                       'header'=>'Batal Rawat',
                       'type'=>'raw',
                       'value'=>'CHtml::link("<i class=\'icon-remove\'></i>",
                                 Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/batalRawatInap",
                                 array("idPendaftaran"=>$data->pendaftaran_id)),
                                 array("title"=>"Klik untuk Batal Rawat Inap", "target"=>"iframeBatalRawatInap",
                                 "onclick"=>"$(\"#dialogBatalRawatInap\").dialog(\"open\");", "rel"=>"tooltip"))',
                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
                    ),
//                    array(
//                       'header'=>'Rencana Pulang',
//                       'type'=>'raw',
//                       'value'=>'((!empty($data->rencanapulang)) ? $data->rencanapulang : CHtml::link("<i class=\'icon-time\'></i> ",
//                           Yii::app()->controller->createUrl("'.Yii::app()->controller->id.'/RencanaPulangPasienRI",array("idPasienadmisi"=>$data->pasienadmisi_id)) ,
//                               array("title"=>"Klik Untuk Rencana Pulang Pasien","target"=>"iframeRencanaPulang", "onclick"=>"$(\"#dialogRencanaPulang\").dialog(\"open\");", "rel"=>"tooltip")))',
//                       'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//                    ),
                    "statusFarmasi"
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));


?>
<hr>
<div style='display:none'>
 <?php $this->widget('MyDateTimePicker',array(
//                                        'model'=>$modMasukKamar,
                                        'name'=>'jammasukkamar',
                                        'mode'=>'time',
                                        'options'=> array(
                                             'dateFormat'=>Params::TIME_FORMAT,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,
                                                             'class'=>'dtPicker3',
                                                             'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                             ),
                )); ?>
    </div>
<?php echo $this->renderPartial('_formPencarian', array('model'=>$model)); ?>


<?php
// Dialog untuk pasienpulang_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPasienPulang',
    'options'=>array(
        'title'=>'Pasien Pulang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>600,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>

<?php
// Dialog untuk masukkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogMasukKamar',
    'options'=>array(
        'title'=>'Masuk Kamar Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>200,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end masukkamar_t dialog =============================
?>

<?php
// Dialog untuk pindahkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogPindahKamar',
    'options'=>array(
        'title'=>'Pindah Kamar Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1100,
        'minHeight'=>700,
        'height'=>530,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePindahKamar" width="100%" height="480">
</iframe>
<?php $this->endWidget(); ?>

<?php
// Dialog untuk batal Rawat Inap Revisi (Jang wahyu) =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogBatalRawatInap',
    'options'=>array(
        'title'=>'Pembatalan Pasien Rawat Inap',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>800,
        'minHeight'=>500,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeBatalRawatInap" width="100%" height="550">
</iframe>
<?php $this->endWidget(); ?>

<?php
// Dialog untuk pindahkamar_t =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogTindakLanjut',
    'options'=>array(
        'title'=>'Transaksi Pasien Pulang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1100,
        'minHeight'=>700,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeTindakLanjut" width="100%" height="900">
</iframe>

<?php
$this->endWidget();
//========= end pasienpulang_t dialog =============================
?>


<?php
// Dialog untuk rencana pulang =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogRencanaPulang',
    'options'=>array(
        'title'=>'Rencana Pulang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>300,
        'resizable'=>true,
    ),
));
?>

<iframe src="" name="iframeRencanaPulang" width="100%" height="300px;">
</iframe>

<?php
$this->endWidget();
//========= end rencanapulang dialog =============================
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>250,
        'resizable'=>false,
    ),
));?>
<div class="alert alert-block alert-error" id="alertDiv" style="display : none;">
    Kesalahan dalam Pengisian Usename atau Password
</div>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>
    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'dialogAlasan',
    'options'=>array(
        'title'=>'Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>250,
        'resizable'=>false,
    ),
));
?>
<div id="divFormDataPasien"></div>


<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formAlasan')); ?>
<table>
    <tr>
        <td><?php echo CHtml::label('Alasan','Alasan', array('class'=>'')) ?></td>
        <td>
            <?php echo CHtml::textArea('Alasan', '', array()); ?>
            <?php echo CHtml::hiddenField('idOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('namaOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienPulang', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPendaftaran', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienAdmisi', '', array('readonly'=>TRUE)); ?>

        </td>
    </tr>
</table>

    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'simpanAlasan();return false;')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>    </div>
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'konfirmasiDialog',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<div align="center">
    User Tidak Memiliki Akses Untuk Proses Ini,<br/>
    Yakin Akan Melakukan Ke Proses Selanjutnya ?
</div>
<div class="form-actions" align="center">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#loginDialog').dialog('open');$('#konfirmasiDialog').dialog('close');")); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} No',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>"$('#konfirmasiDialog').dialog('close');")); ?>    </div>

<?php $this->endWidget();?>
<script type="text/javascript">
function batal(){
    $('#loginDialog').dialog('close');
    $('#loginDialog #username').val('');
    $('#loginDialog #password').val('');
    $('#alertDiv').hide();
    $('#idPasien').val('');
    $('#idPendaftaran').val('');

    $('#dialogAlasan').dialog('close');
    $('#dialogAlasan #idOtoritas').val('');
    $('#dialogAlasan #namaOtoritas').val('');
    $('#dialogAlasan #idPasienPulang').val('');
    $('#dialogAlasan #idPendaftaran').val('');
    $('#dialogAlasan #idPasienAdmisi').val('');

    $.fn.yiiGridView.update('daftarpasien-v-grid', {
        data: $('#daftarPasienPulang-form').serialize()
    });
}
function cekHakAkses(idPendaftaran)
{
//       $('#dialogAlasan #idPasienPulang').val(idPasienPulang);
//       $('#dialogAlasan #idPendaftaran').val(idPendaftaran);
//       $('#idPasien').val(idPasien);
//       $('#idPendaftaran').val(idPendaftaran);

       $('#konfirmasiDialog').dialog('open');

    $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/CekHakAkses');?>', {idPendaftaran:idPendaftaran, idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){

        if(data.cekAkses==true){
            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
        } else {
            $('#konfirmasiDialog').dialog('open');
        }
        $('#dialogAlasan #idPasienPulang').val(data.pendaftaran.pasienpulang_id);
       $('#dialogAlasan #idPendaftaran').val(data.pendaftaran.pendaftaran_id);
       $('#idPasien').val(data.pendaftaran.pasien_id);
       $('#idPendaftaran').val(data.pendaftaran.pendaftaran_id);
       $('#dialogAlasan #idPasienAdmisi').val(data.pendaftaran.pasienadmisi_id);
    }, 'json');
}

function cekLogin()
{
    idPasien = $('#idPasien').val();
    idPendaftaran = $('#idPendaftaran').val();
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLoginPembatalRawatInap');?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
              $.post('<?php echo Yii::app()->createUrl('rawatJalan/ActionAjax/dataPasien');?>', {idPasien:idPasien ,idPendaftaran:idPendaftaran}, function(dataPasien){

              $('#divFormDataPasien').html(dataPasien.form);

             }, 'json');

            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
            $('#loginDialog').dialog('close');
        }else{
    $('#alertDiv').show();
        }
    }, 'json');
}

function simpanAlasan()
{
    alasan =$('#dialogAlasan #Alasan').val();
    if(alasan==''){
        alert('Anda Belum Mengisi Alasan Pembatalan');
    }else{
        $.post('<?php echo Yii::app()->createUrl('rawatInap/pasienRawatInap/BatalRawatInap');?>', $('#formAlasan').serialize(), function(data){
//            if(data.error != '')
//                alert(data.error);
//            $('#'+data.cssError).addClass('error');
            if(data.status=='success'){
                batal();
                alert('Data Berhasil Disimpan');
                location.reload();
            }else{
                alert(data.status);
            }
        }, 'json');
   }
}




</script>
<script>
        function addMasukKamar()
{

    <?php
            echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjaxRIRD/addMasukKamarRI'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogMasukKamar div.divForForm').html(data.div);
                    $('#dialogMasukKamar div.divForForm form').submit(addMasukKamar);

//                    jQuery('.dtPicker3').datetimepicker(jQuery.extend({showMonthAfterYear:false},
//                    jQuery.datepicker.regional['id'], {'dateFormat':'dd M yy hh:mm:ss','maxDate'  : 'd','timeText':'Waktu','hourText':'Jam',
//                         'minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih   Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));
//
                    jQuery('#MasukkamarT_jammasukkamar').timepicker(jQuery.extend({showMonthAfterYear:false},
                    jQuery.datepicker.regional['id'], {
                   'timeText':'Waktu','hourText':'Jam','minuteText':'Menit','secondText':'Detik','showSecond':true,'timeOnlyTitle':'Pilih Waktu','timeFormat':'hh:mm:ss','changeYear':true,'changeMonth':true,'showAnim':'fold'}));

                }
                else
                {
                    $('#dialogMasukKamar div.divForForm').html(data.div);
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    setTimeout(\"$('#dialogMasukKamar').dialog('close') \",1000);
                }

            } ",
    ))
?>;
    return false;
}
</script>
<?php
$urlSessionMasukKamar = Yii::app()->createUrl('ActionAjaxRIRD/buatSessionMasukKamar ');
$jscript = <<< JS
function buatSessionMasukKamar(idMasukKamar,idKelasPelayanan, idPendaftaran)
{
    $.post("${urlSessionMasukKamar}", { idMasukKamar: idMasukKamar,idKelasPelayanan: idKelasPelayanan,idPendaftaran: idPendaftaran },
        function(data){
            'sukses';
    }, "json");
}
JS;
Yii::app()->clientScript->registerScript('jsPendaftaran',$jscript, CClientScript::POS_BEGIN);
?>
<?php
$url = Yii::app()->createUrl('ActionAjaxRIRD/batalPindahKamar');
$mds = Yii::t('mds','Anda yakin akan membatalkan pindah kamar?');
$jscript = <<< JS
function batalPindahKamar(idPindahKamar,idMasukKamar)
{
    if(confirm("${mds}"))
    {
        $.post("${url}", { idPindahKamar: idPindahKamar, idMasukKamar: idMasukKamar },
            function(data){
                if(data.status == 'true')
                {
                    $('#dialogSuksesBatalPindah').dialog('open');
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    $('#dialogBatalPindah div.divForForm').html(data.div);
                    setTimeout("$('#dialogSuksesBatalPindah').dialog('close') ",1000);
                }
                else
                {
                    $('#dialogGagalBatalPindah').dialog('open');
                    $.fn.yiiGridView.update('daftarPasien-grid');
                    $('#dialogBatalPindah div.divForForm').html(data.div);
                    setTimeout("$('#dialogSuksesBatalPindah').dialog('close') ",1000);
                }
        }, "json");
    }
}
JS;
Yii::app()->clientScript->registerScript('jsBatalPindah',$jscript, CClientScript::POS_BEGIN);
?>
