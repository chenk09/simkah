<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'batalrawatinap-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
        'focus'=>'#',
));
$this->widget('bootstrap.widgets.BootAlert'); 
echo $form->errorSummary(array($modPasienBatalRawat)); ?>
<?php
    $modTindakan = TindakanpelayananT::model()->findAllByAttributes(array('pasienadmisi_id'=>$modAdmisi->pasienadmisi_id,'ruangan_id'=>  Yii::app()->user->getState('ruangan_id')));
    $modObat = ObatalkespasienT::model()->findAllByAttributes(array('pasienadmisi_id'=>$modAdmisi->pasienadmisi_id));
    $apache = PasienapachescoreT::model()->findAllByAttributes(array('pasienadmisi_id'=>$modAdmisi->pasienadmisi_id));
    if((count($modTindakan) > 0) || (count($modObat) > 0) || (count($apache) > 0)){ ?>
<div class='alert alert-block alert-warning'>
 <a class='close' data-dismiss='alert'>×</a>
    Pasien Tidak dapat di batalkan karena sudah menginputkan tindakan pelayanan
</div>
<?php if(count($modTindakan) > 0){ ?>
<table class='table table-striped table-bordered table-condensed'>
    <legend class="rim"> Tabel Tindakan </legend>
    <thead>
        <tr>
            <th>
                No.
            </th>
            <th>
                Ruangan
            </th>
            <th>
                Nama Tindakan
            </th>
            <th>
                Qty
            </th>
            <th>
                Tarif Tindakan
            </th>
            <th>
                Tarif Cyto
            </th> 
            <th>
                Status Bayar
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($modTindakan as $i=>$tindakan){
                echo "<tr>";
                echo "<td>".($i+1)."</td>";
                echo "<td>".($tindakan->ruangan->ruangan_nama)."</td>";
                echo "<td>".($tindakan->daftartindakan->daftartindakan_nama)."</td>";
                echo "<td>".($tindakan->qty_tindakan)."</td>";
                echo "<td>".($tindakan->tarif_tindakan)."</td>";
                echo "<td>".($tindakan->tarifcyto_tindakan)."</td>";
                echo "<td>".(!empty($tindakan->tindakansudahbayar_id) ? "Lunas" : "Belum Lunas")."</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
<?php } ?>
    
<?php if(count($modObat) > 0){ ?>
<table class='table table-striped table-bordered table-condensed'>
    <legend class="rim"> Tabel Obat Alkes </legend>
    <thead>
        <tr>
            <th>
                No.
            </th>
            <th>
                Ruangan
            </th>
            <th>
                Nama Obat
            </th>
            <th>
                Qty
            </th>
            <th>
                Harga Obat
            </th>
            <th>
                Tarif Cyto
            </th> 
            <th>
                Status Bayar
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($modObat as $key=>$obat){
                echo "<tr>";
                echo "<td>".($key+1)."</td>";
                echo "<td>".($obat->ruangan->ruangan_nama)."</td>";
                echo "<td>".($obat->obatalkes->obatalkes_nama)."</td>";
                echo "<td>".($obat->qty_oa)."</td>";
                echo "<td>".($obat->hargajual_oa)."</td>";
                echo "<td>".($obat->tarifcyto)."</td>";
                echo "<td>".(!empty($obat->oasudahbayar_id) ? "Lunas" : "Belum Lunas")."</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
<?php } ?>
    
<?php if(count($apache) > 0){ ?>
<table class='table table-striped table-bordered table-condensed'>
    <legend class="rim"> Tabel Apache </legend>
    <thead>
        <tr>
            <th>
                No.
            </th>
            <th>
                Tgl. Scoring
            </th>
            <th>
                Gagal Ginjal <br/> Akut
            </th>
            <th>
                Point Nama
            </th>
            <th>
                Point Nilai
            </th>
            <th>
                Point Score
            </th> 
            <th>
                Catatan
            </th> 
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach($apache as $keys=>$apaches){
                echo "<tr>";
                echo "<td>".($keys+1)."</td>";
                echo "<td>".($apaches->tglscoring)."</td>";
                echo "<td>".(($apaches->gagalginjalakut == true) ? "Ya":"Tidak")."</td>";                
                echo "<td>".($apaches->point_nama)."</td>";
                echo "<td>".($apaches->point_nilai)."</td>";
                echo "<td>".($apaches->point_score)."</td>";
                echo "<td>".(!empty($apaches->catatanscore) ? $apaches->catatanscore: "-")."</td>";
                echo "</tr>";
            }
        ?>
    </tbody>
<?php } ?>    
<?php
    }else{
?>
<fieldset>
    <legend class="rim2">Data Pasien</legend>
    <table class="table table-condensed">
        <?php 
            echo CHtml::hiddenField('pendaftaran_id', $modPendaftaran->pendaftaran_id); 
            
        ?>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'tgl_pendaftaran', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?> </td>
            <td><?php echo CHtml::activeTextField($modPasien, 'no_rekam_medik')?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran', array('class'=>'control-label'))?></td>
            <td>
                <?php echo CHtml::activeTextField($modPendaftaran, 'no_pendaftaran', array('readonly'=>true, 'class'=>'span2')); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'jeniskelamin', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'umur', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_pasien', array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php //echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_nama',array('class'=>'control-label')); 
                      echo CHtml::label('Jenis Kasus Penyakit','jeniskasuspenyakit_nama', array('class'=>'control-label') ) ; ?></td>
            <td><?php echo CHtml::activeTextField($modPendaftaran, 'jeniskasuspenyakit_nama', array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::activeTextField($modPasien, 'nama_bin', array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend class="rim">Alasan Batal Di Rawat</legend>
        <p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
        <br>
        <div class="control-group ">
            <?php echo $form->labelEx($modPasienBatalRawat,'tglbatalrawat', array('class'=>'control-label')) ?>
            <div class="controls">
                <?php 
                $this->widget('MyDateTimePicker',array(
                                'model'=>$modPasienBatalRawat,
                                'attribute'=>'tglbatalrawat',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'timeFormat'=>  Params::TIME_FORMAT,
                                ),
                                'htmlOptions'=>array('readonly'=>true,
                                                 'class'=>'dtPicker3',
                                                 'onkeypress'=>"return $(this).focusNextInputField(event);",
                                                 ),
                )); ?>
            </div>
        </div>
        <?php echo $form->textAreaRow($modPasienBatalRawat, 'alasanpembatalan'); ?>
        <?php echo $form->textAreaRow($modPasienBatalRawat, 'keteranganpembatalan'); ?>
        

    <div class="form-actions">
         <?php echo CHtml::htmlButton($modPindahKamar->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                               Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                                array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                array('class'=>'btn btn-danger','onclick'=>'konfirmasi()')); 
           $content = $this->renderPartial('../tips/transaksi',array(),true);
			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
    </div>   
</fieldset>
<?php } ?>
<?php
$this->endWidget();
if($tersimpan=='Ya'){
?>
<script>
parent.location.reload();
</script>
<?php
}
?>
<script>
    function konfirmasi()
    {
        if(confirm('<?php echo Yii::t('mds','Do You want to cancel?') ?>'))
        {
            $('#dialogPasienPulang').dialog('close');
        }
        else
        {   
            $('#RIPasienPulangT_carakeluar').focus();
            return false;
        }
    }
</script>