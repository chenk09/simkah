<legend class="rim2">Informasi Pasien Pulang</legend>
<?php
 $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
 $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
 
Yii::app()->clientScript->registerScript('cari wew', "
$('#daftarPasienPulang-form').submit(function(){
	$.fn.yiiGridView.update('daftarPasienPulang-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
//echo Yii::app()->user->getState('ruangan_id');
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'daftarPasienPulang-grid',
	'dataProvider'=>$modPasienYangPulang->searchRI(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
//                'tglpasienpulang',
                array(
                    'header'=>'Tgl Pasien Pulang',
                    'value'=>'$data->tglpasienpulang',
                ),
                array(
                    'header'=>'Cara/ Kondisi Pulang',
                    'type'=>'raw',
                    'value'=>'$data->CaradanKondisiPulang'
                ),
                array(
                    'header'=>'Lama Dirawat / Nama Kamar',
                    'value'=>'$data->lamadirawat_kamar',
                ),
//                'lamadirawat_kamar',
                array(
                    'header'=>'Tgl Admisi',
                    'value'=>'$data->tgladmisi',
                ),
//                'tgladmisi',
                array(
                    'header'=>'No Rekam Medik / <BR> No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->NoRMdanNoPendaftaran'
                ),
            
                array(
                    'header'=>'Nama / Bin',
                    'type'=>'raw',
                    'value'=>'$data->NamadanNamaBIN'
                ),    
//                'umur',
//                 array(
//                       'header'=>'Cara Bayar/ Penjamin',
//                        'type'=>'raw',
//                        'value'=>'$data->CaraBayardanPenjamin'
//                    ),
                array(
                    'header'=>'Kelas Pelayanan/ No Masuk Kamar',
                    'type'=>'raw',
                    'value'=>'$data->KelasPelayanandanNoMasukKamar'
                ),   
                array(
                    'header'=>'Nama Jenis Kasus Penyakit',
                    'value'=>'$data->jeniskasuspenyakit_nama',
                ),
//                'jeniskasuspenyakit_nama',

                array(
                       'header'=>'Batal Pulang',
                       'type'=>'raw',
                       'value'=>'CHtml::link("<i class=\'icon-list-alt\'></i> ","javascript:cekHakAkses($data->pasienpulang_id,$data->pasienadmisi_id,$data->pasien_id,$data->pendaftaran_id)" ,array("title"=>"Klik Untuk Membatalkan Kepulangan"))',
                    ),
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
echo CHtml::hiddenField('idPasien','',array('readonly'=>TRUE));
echo CHtml::hiddenField('idPendaftaran','',array('readonly'=>TRUE));
?>

<?php echo $this->renderPartial('_formPencarian', array('modPasienYangPulang'=>$modPasienYangPulang)); ?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>250,
        'resizable'=>false,
    ),
));?>
<div class="alert alert-block alert-error" id="alertDiv" style="display : none;">
    Kesalahan dalam Pengisian Usename atau Password
</div>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
         <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>
    </div> 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAlasan',
    'options'=>array(
        'title'=>'Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>900,
        'resizable'=>false,
    ),
));
?>
<div id="divFormDataPasien"></div>


<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formAlasan')); ?>
<table>
    <tr>
        <td><?php echo CHtml::label('Alasan','Alasan', array('class'=>'')) ?></td>
        <td>
            <?php echo CHtml::textArea('Alasan', '', array()); ?>
            <?php echo CHtml::hiddenField('idOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('namaOtoritas', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienPulang', '', array('readonly'=>TRUE)); ?>
            <?php echo CHtml::hiddenField('idPasienAdmisi', '', array('readonly'=>TRUE)); ?>
        </td>
    </tr>
</table>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'simpanAlasan();return false;')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>'batal();return false;')); ?>    </div> 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'konfirmasiDialog',
    'options'=>array(
        'title'=>'Konfirmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<div align="center">
    User Tidak Memiliki Akses Untuk Proses Ini,<br/>
    Yakin Akan Melakukan Ke Proses Selanjutnya ?
</div>
<div class="form-actions" align="center">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Yes',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>"$('#loginDialog').dialog('open');$('#konfirmasiDialog').dialog('close');")); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} No',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                            array('class'=>'btn btn-danger', 'type'=>'button', 'onclick'=>"$('#konfirmasiDialog').dialog('close');")); ?>    </div> 

<?php $this->endWidget();?>


<script type="text/javascript">
    
function batal(){
    $('#loginDialog').dialog('close');
    $('#loginDialog #username').val('');
    $('#loginDialog #password').val('');
    $('#alertDiv').hide(); 
    $('#idPasien').val('');
    $('#idPendaftaran').val('');
     
    $('#dialogAlasan').dialog('close');
    $('#dialogAlasan #idOtoritas').val('');
    $('#dialogAlasan #namaOtoritas').val('');
    $('#dialogAlasan #idPasienPulang').val('');
    $('#dialogAlasan #idPasienAdmisi').val('');
    
    $.fn.yiiGridView.update('daftarPasienPulang-grid', {
        data: $('#daftarPasienPulang-form').serialize()
    });
}    
function cekHakAkses(idPasienPulang,idPasienAdmisi,idPasien,idPendaftaran)
{
       $('#dialogAlasan #idPasienPulang').val(idPasienPulang);
       $('#dialogAlasan #idPasienAdmisi').val(idPasienAdmisi);
       $('#idPasien').val(idPasien);
       $('#idPendaftaran').val(idPendaftaran);
       
       $('#konfirmasiDialog').dialog('open');

    $.post('<?php echo Yii::app()->createUrl('rawatInap/ActionAjax/CekHakAkses');?>', {idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){
        
        if(data.cekAkses==true){
            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
        } else {
            $('#konfirmasiDialog').dialog('open');
        }
    }, 'json');
}

function cekLogin()
{
    idPasien = $('#idPasien').val();    
    idPendaftaran = $('#idPendaftaran').val();    
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLoginPembatalPulang');?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
              $.post('<?php echo Yii::app()->createUrl('rawatInap/ActionAjax/dataPasien');?>', {idPasien:idPasien ,idPendaftaran:idPendaftaran}, function(dataPasien){
                  
              $('#divFormDataPasien').html(dataPasien.form);

             }, 'json');
                 
            $('#dialogAlasan').dialog('open');
            $('#dialogAlasan #idOtoritas').val(data.userid);
            $('#dialogAlasan #namaOtoritas').val(data.username);
            $('#loginDialog').dialog('close');
        }else{
    $('#alertDiv').show(); 
        }
    }, 'json');
}

function simpanAlasan()
{
    alasan =$('#dialogAlasan #Alasan').val();
    if(alasan==''){
        alert('Anda Belum Mengisi Alasan Pembatalan');
    }else{
        $.post('<?php echo Yii::app()->createUrl('rawatInap/InformasiPasienPulang/batalPulang');?>', $('#formAlasan').serialize(), function(data){
//            if(data.error != '')
//                alert(data.error);
//            $('#'+data.cssError).addClass('error');
            if(data.status=='success'){
                batal();
                alert('Data Berhasil Disimpan');
            }else{
                alert(data.status);
            }
        }, 'json');
   }     
}




</script>