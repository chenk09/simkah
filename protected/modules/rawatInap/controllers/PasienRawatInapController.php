<?php
Yii::import('rawatJalan.controllers.TindakanController');//untuk load rekening
class PasienRawatInapController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $successSave;
        public $successUpdateMasukKamar= false;
        public $successPasienPulang= false;
        public $successUpdatePendaftaran= false;
        public $successUpdatePasienAdmisi= false;
        public $successRujukanKeluar= true;
        public $successPaseinM= true;
        public $successSaveTindakanKomponen = true;
        public $successSaveTindakan;


//	FILTER MANGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','pindahKamarDariTransaksi','pindahKamarPasienRI',
//                                    'tindakLanjutDariPasienRI','tindakLanjutDrTransaksi','BatalRawatInap','RencanaPulangPasienRI'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	public function actionIndex()
	{
        $this->pageTitle = Yii::app()->name." - Pasien Rawat Inap";
        $format = new CustomFormat();
        $model = new RIInfopasienmasukkamarV;
        $model->tglAwal = date("d M Y").' 00:00:00';
        $model->tglAkhir = date('d M Y h:i:s');
        $model->ceklis = 0;
        if(isset ($_REQUEST['RIInfopasienmasukkamarV'])){
            $model->attributes=$_REQUEST['RIInfopasienmasukkamarV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RIInfopasienmasukkamarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RIInfopasienmasukkamarV']['tglAkhir']);
            $model->ceklis = $_REQUEST['RIInfopasienmasukkamarV']['ceklis'];
        }
        $this->render('index',array('model'=>$model));
	}

        public function actionTindakLanjutDariPasienRI($idPendaftaran)
	{
                $this->layout='//layouts/frameDialog';

                $modelPulang = new RIPasienPulangT;
                $modRujukanKeluar = new RIPasienDirujukKeluarT;
                $modPendaftaran = RIPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                $modPasienRIV = RIInfopasienmasukkamarV::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                $modTariftindakan = TariftindakanM::model()->findByAttributes(array('kelaspelayanan_id'=>$modPasienRIV->kelaspelayanan_id));
                $modMasukKamar = RIMasukKamarT::model()->findByPk($modPasienRIV->masukkamar_id);
                $modPasienKirimUnit = PasienkirimkeunitlainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienmasukpenunjang_id'=>null));
                $modelPulang->pendaftaran_id=$modPasienRIV->pendaftaran_id;
                $modelPulang->pasien_id=$modPasienRIV->pasien_id;
                $modelPulang->pasienadmisi_id=$modPasienRIV->pasienadmisi_id;
                $modMasukKamar->tglkeluarkamar = date('Y-m-d H:i:s');
                $modMasukKamar->jamkeluarkamar = date('H:i:s');
                $modelPulang->tglpasienpulang = date('Y-m-d H:i:s');
                $modRujukanKeluar->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
                $modRujukanKeluar->tgldirujuk=date('Y-m-d H:i:s');
                $tersimpan='Tidak';
                $status = false;

                $format = new CustomFormat();
                //Hitung lama rawat
                $modMasukKamar->tglmasukkamar = $format->formatDateTimeMediumForDB($modMasukKamar->tglmasukkamar);
                $selisihHari = MyFunction::hitungHari($modMasukKamar->tglmasukkamar);
                $modMasukKamar->lamadirawat_kamar = $selisihHari;

                $ruangan = array();
                if($modPendaftaran->statusfarmasi == false){
        			$ruangan[] = "+ Ruangan : APOTEK FARMASI > Status pasien belum dikonfirmasi";
        		}

                if(count($modPasienKirimUnit) > 0){
                    foreach($modPasienKirimUnit as $key=>$kirimPasien){
                        if($kirimPasien->ruangan_id != Params::RUANGAN_ID_GIZI){
                            $ruangan[] = "+ Ruangan : ". $kirimPasien->ruangan->ruangan_nama . " Tgl Rujuk Pasien : ".$kirimPasien->tgl_kirimpasien;
                        }
                    }
                }

                if(count($ruangan) > 0){
                    $status = true;
                    Yii::app()->user->setFlash('error',"Pasien Belum Ditindak Lanjut Di : <br> " . implode("<br>", $ruangan));
                }

                if(empty($modPasienRIV->kamarruangan_nokamar)){
//                    echo "kamarruangan tidak  ada";
//                              alert('Silahkan Isi No. Kamar Terlebih Dahulu');
                    echo "<script>
                                window.top.location.href='".Yii::app()->createUrl('rawatInap/PasienRawatInap/index')."';
                            </script>";
                }else{
//                    echo "kamarruangan ada";
                }
                if(isset($_POST['RIPasienPulangT']))
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                          $modMasukKamar = RIMasukKamarT::model()->findByPk($_POST['RIMasukKamarT']['masukkamar_id']);
                          $this->updateMasukKamar($modMasukKamar,$_POST['RIMasukKamarT']);
                          if(!isset($modTariftindakan->harga_tariftindakan)){
                            echo "<script>
                                        alert('Maaf, Harga Tarif Kamar Rawat Inap Belum Ada. Silahkan Hubungi Bagian Administrasi');
                                        window.location.href('".Yii::app()->createUrl('/PasienRawatInap/index')."');
                                    </script>";
                            }else{
//                                echo "<script>
//                                            alert('Harga Tarif Kamar Rawat Inap Ada');
//                                        </script>";
                                $modelPulang = $this->savePasienPulang($modMasukKamar,$modelPulang,$_POST['RIPasienPulangT'],$_POST['RIPasienPulangT']['pasienadmisi_id']);
                            }


                          $modPendaftaran = RIPendaftaranT::model()->findByPk($modelPulang->pendaftaran_id);
                          $this->updatePendaftaran($modPendaftaran,$modelPulang);

                          $modPasienAdmisi = RIPasienAdmisiT::model()->findByPk($modelPulang->pasienadmisi_id);
                          $this->updatePasienAdmisi($modPasienAdmisi, $modelPulang);

                          $konfigSystem = KonfigsystemK::model()->findAll();
                          if($konfigSystem->akomodasiotomatis == true){
                              if(PasienRawatInapController::cekAkomodasiHariIni($modPendaftaran, $modAdmisi, $modMasukKamar)){
                                    $this->saveAkomodasi($modPendaftaran, $modPasienAdmisi, $modMasukKamar->lamadirawat_kamar);
                              }
                          }

                            if($_POST['pakeRujukan']=='1')//Jika Pake Rujukan
                            {
                                $this->successRujukanKeluar=false;
                                $modelPulang->pakeRujukan = true;
                                $modRujukanKeluar = $this->saveRujukanKeluar($modRujukanKeluar,$modelPulang,$_POST['RIPasienDirujukKeluarT']);
                            }

                            if($_POST['isDead']=='1')//Jika Pasien Meninggal
                            {
                                $modelPulang->isDead;
                                $this->successPaseinM=false;
                                $modPasien = RIPasienM::model()->findByPk($modelPulang->pasien_id);
                                $modPasien->tgl_meninggal = $_POST['RIPasienPulangT']['tgl_meninggal'];

                                if($modPasien->save()){
                                    $this->successPaseinM=true;
                                }else{
                                    $this->successPaseinM=false;
                                }
                            }

                         if($this->successUpdateMasukKamar && $this->successPasienPulang
                            && $this->successUpdatePendaftaran && $this->successUpdatePasienAdmisi
                            && $this->successRujukanKeluar && $this->successPaseinM){
                             $transaction->commit();
//                             //echo "berhasil9";exit;
                             Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                              $tersimpan='Ya';
                            } else {
                                    if($this->successUpdateMasukKamar==false){
                                       Yii::app()->user->setFlash('error',"Data Masuk Kamar gagal disimpan");
                                    }else if($this->successPasienPulang==false){
                                       Yii::app()->user->setFlash('error',"Data Pasien Pulang gagal disimpan");
                                    }else if($this->successUpdatePendaftaran==false){
                                       Yii::app()->user->setFlash('error',"Data pendaftaran gagal disimpan");
                                    }else if($this->successUpdatePasienAdmisi==false){
                                       Yii::app()->user->setFlash('error',"Data Pasien Admisi gagal disimpan");
                                    }else if($this->successRujukanKeluar==false){
                                       Yii::app()->user->setFlash('error',"Data Rujukan Keluar gagal disimpan");
                                    }else if($this->successPaseinM==false){
                                       Yii::app()->user->setFlash('error',"Data Pasien disimpan");
                                    }
                            }


                    }
                    catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }

                 $modMasukKamar->tglmasukkamar = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modMasukKamar->tglmasukkamar, 'yyyy-MM-dd hh:mm:ss'));
                 $modMasukKamar->tglkeluarkamar = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse(date('Y-m-d',strtotime($modMasukKamar->tglkeluarkamar)), 'yyyy-MM-dd'),'medium',false);
                 $modelPulang->tglpasienpulang = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modelPulang->tglpasienpulang, 'yyyy-MM-dd hh:mm:ss'));
                  $modRujukanKeluar->tgldirujuk = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modRujukanKeluar->tgldirujuk, 'yyyy-MM-dd hh:mm:ss'));

                 $this->render('formTindakLanjutDariPasienRI',array(
                                            'modelPulang'=>$modelPulang,
                                            'modRujukanKeluar'=>$modRujukanKeluar,
                                            'modPasienRIV'=>$modPasienRIV,
                                            'modMasukKamar'=>$modMasukKamar,
                                            'modTariftindakan'=>$modTariftindakan,
                                            'tersimpan'=>$tersimpan,
                                            'modPendaftaran'=>$modPendaftaran,
                                            'status'=>$status));
	}

        public function actionTindakLanjutDrTransaksi()
	{
             $modelPulang = new RIPasienPulangT;
             $modRujukanKeluar = new RIPasienDirujukKeluarT;
             // $modPasienRIV = new RIPasienRawatInapV;
             //$modInfoPasien = new RIInfopasienmasukkamarV;
             $modPasienRIV = new RIInfopasienmasukkamarV;
             $modMasukKamar = new RIMasukKamarT;
             $modMasukKamar->tglkeluarkamar = date('Y-m-d');
             $modMasukKamar->jamkeluarkamar = date('H:i:s');
             $modelPulang->tglpasienpulang = date('Y-m-d H:i:s');
             $modRujukanKeluar->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
             $tersimpan='Tidak';
             $modPendaftaran = new RIPendaftaranT;

             $modPasienRIV->unsetAttributes();
             if(isset($_GET['RIInfopasienmasukkamarV'])){
               $modPasienRIV->attributes = $_GET['RIInfopasienmasukkamarV'];
             }

                if(isset($_POST['RIPasienPulangT']))
                {
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                          $modMasukKamar = RIMasukKamarT::model()->findByPk($_POST['RIMasukKamarT']['masukkamar_id']);
                          $this->updateMasukKamar($modMasukKamar,$_POST['RIMasukKamarT']);

                            $modelPulang = $this->savePasienPulang(
                                $modMasukKamar, $modelPulang, $_POST['RIPasienPulangT'], $_POST['RIPasienPulangT']['pasienadmisi_id']
                            );

                          $modPendaftaran = RIPendaftaranT::model()->findByPk($modelPulang->pendaftaran_id);
                          $this->updatePendaftaran($modPendaftaran,$modelPulang);

                          $modPasienAdmisi = RIPasienAdmisiT::model()->findByPk($modelPulang->pasienadmisi_id);
                          $this->updatePasienAdmisi($modPasienAdmisi, $modelPulang);


                            if($_POST['pakeRujukan']=='1')//Jika Pake Rujukan
                            {
                                $this->successRujukanKeluar=false;
                                $modelPulang->pakeRujukan = true;
                                $modRujukanKeluar = $this->saveRujukanKeluar($modRujukanKeluar,$modelPulang,$_POST['RIPasienDirujukKeluarT']);
                            }

                            if($_POST['isDead']=='1')//Jika Pasien Meninggal
                            {
                                $modelPulang->isDead;
                                $this->successPaseinM=false;
                                $modPasien = RIPasienM::model()->findByPk($modelPulang->pasien_id);
                                $modPasien->tgl_meninggal = $modelPulang->tgl_meninggal;
                                if($modPasien->save()){
                                    $this->successPaseinM=true;
                                }else{
                                    $this->successPaseinM=false;
                                }
                            }

                         if($this->successUpdateMasukKamar && $this->successPasienPulang
                            && $this->successUpdatePendaftaran && $this->successUpdatePasienAdmisi
                            && $this->successRujukanKeluar && $this->successPaseinM){
                             $transaction->commit();
                             Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                             $tersimpan='Ya';
                             $this->refresh();
                            }else{
                                if($this->successUpdateMasukKamar==false){
                                   Yii::app()->user->setFlash('error',"Data Masuk Kamar gagal disimpan");
                                }else if($this->successPasienPulang==false){
                                   Yii::app()->user->setFlash('error',"Data Pasien Pulang gagal disimpan");
                                }else if($this->successUpdatePendaftaran==false){
                                   Yii::app()->user->setFlash('error',"Data pendaftaran gagal disimpan");
                                }else if($this->successUpdatePasienAdmisi==false){
                                   Yii::app()->user->setFlash('error',"Data Pasien Admisi gagal disimpan");
                                }else if($this->successRujukanKeluar==false){
                                   Yii::app()->user->setFlash('error',"Data Rujukan Keluar gagal disimpan");
                                }else if($this->successPaseinM==false){
                                   Yii::app()->user->setFlash('error',"Data Pasien disimpan");
                                }


                            }


                    }
                    catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }


                 $modMasukKamar->tglmasukkamar = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modMasukKamar->tglmasukkamar, 'yyyy-MM-dd hh:mm:ss'));
                 $modMasukKamar->tglkeluarkamar = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modMasukKamar->tglkeluarkamar, 'yyyy-MM-dd'),'medium',false);
                 $modelPulang->tglpasienpulang = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modelPulang->tglpasienpulang, 'yyyy-MM-dd hh:mm:ss'));
                 $modRujukanKeluar->tgldirujuk = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modRujukanKeluar->tgldirujuk, 'yyyy-MM-dd hh:mm:ss'));
		$this->render('formTindakLanjutDariPasienRI',array(
                                            'modelPulang'=>$modelPulang,
                                            'modRujukanKeluar'=>$modRujukanKeluar,
                                            'modPasienRIV'=>$modPasienRIV,
                                            'modMasukKamar'=>$modMasukKamar,
                                            'tersimpan'=>$tersimpan,
                                            'modPendaftaran'=>$modPendaftaran));
	}

        protected function saveRujukanKeluar($modRujukanKeluar,$modelPulang,$attrRujukanKeluar)
        {
            $format = new CustomFormat();
            $modRujukanKeluarNew = new RIPasienDirujukKeluarT;
            $modRujukanKeluarNew->attributes = $attrRujukanKeluar;
            $modMasukKamar->tgldirujuk = $format->formatDateTimeMediumForDB(trim($attrRujukanKeluar['tgldirujuk']));
            $modRujukanKeluarNew->pendaftaran_id = $modelPulang->pendaftaran_id;
            $modRujukanKeluarNew->pasien_id = $modelPulang->pasien_id;
            $modRujukanKeluarNew->create_time = date( 'Y-m-d H:i:s');
            $modRujukanKeluarNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modRujukanKeluarNew->create_loginpemakai_id = Yii::app()->user->id;
            if($modRujukanKeluarNew->save()){
                $this->successRujukanKeluar = true;
            }else{
                $this->successRujukanKeluar = false;
            }
            return $modRujukanKeluarNew;
        }

        protected function updateMasukKamar($modMasukKamar,$attrMasukKamar)
        {
            $format = new CustomFormat();
            $modMasukKamar->attributes = $attrMasukKamar;
            $modMasukKamar->tglmasukkamar = $format->formatDateTimeMediumForDB(trim($attrMasukKamar['tglmasukkamar']));
            $modMasukKamar->tglkeluarkamar  = $format->formatDateTimeMediumForDB(trim($attrMasukKamar['tglkeluarkamar']).' '.$attrMasukKamar['jamkeluarkamar']);
            if($modMasukKamar->save()){
                $this->successUpdateMasukKamar= true;
            }else{
                $this->successUpdateMasukKamar= false;
            }
        }

        protected function updatePendaftaran($modPendaftaran,$modelPulang)
        {
            if($_POST['RIPendaftaranT']['tglrenkontrol'] != null ){
                $format = new CustomFormat();
                $tglrenkontrol = $format->formatDateTimeMediumForDB($_POST['RIPendaftaranT']['tglrenkontrol']);
            }else{
                $tglrenkontrol = null;
            }
            $daftar = PendaftaranT::model()->updateByPk(
                $modelPulang->pendaftaran_id,
                array(
                    'kirim_farmasi'=>'t',
                    'tglselesaiperiksa'=>date('Y-m-d H:i:s'),
                    'pasienpulang_id'=>$modelPulang->pasienpulang_id,
                    'tglrenkontrol'=>$tglrenkontrol,
                    'statusperiksa'=>Params::statusPeriksa(4),
                )
            );
/*
			$_model = new LogKirimFarmasi;
			$_model->pendaftaran_id = $modelPulang->pendaftaran_id;
			$_model->komentar = 'KIRIM KE FARMASI';
			$_model->save();
*/


//            $modPendaftaran->tglselesaiperiksa = date( 'Y-m-d H:i:s');
//            $modPendaftaran->pasienpulang_id = $modelPulang->pasienpulang_id;
            if($daftar){
                $this->successUpdatePendaftaran = true;
                return $modPendaftaran;
            }else{
                $this->successUpdatePendaftaran = false;
            }

        }

         protected function updatePasienAdmisi($modPasienAdmisi,$modelPulang)
        {

            $modPasienAdmisi->pasienpulang_id = $modelPulang->pasienpulang_id;
            $modPasienAdmisi->tglpulang = $modelPulang->tglpasienpulang;
            $admisi = PasienadmisiT::model()->updateByPk($modPasienAdmisi->pasienadmisi_id, array("tglpulang"=>$modPasienAdmisi->tglpulang, "pasienpulang_id"=>$modPasienAdmisi->pasienpulang_id));
            if($admisi){
                $this->successUpdatePasienAdmisi = true;
            }else{
                $this->successUpdatePasienAdmisi = false;
            }

            return $modPasienAdmisi;
        }

        protected function savePasienPulang($modMasukKamar,$modPasienPulang,$attrPasienPulang,$idPasienAdmisi='')
        {
            $format = new CustomFormat();
            $modelPulangNew = new RIPasienPulangT;
            $modelPulangNew->attributes = $attrPasienPulang;
            $modelPulangNew->tglpasienpulang = $format->formatDateTimeMediumForDB(trim($attrPasienPulang['tglpasienpulang']));
            $modelPulangNew->tgl_meninggal = $format->formatDateTimeMediumForDB(trim($attrPasienPulang['tgl_meninggal']));
            $modelPulangNew->lamarawat=$modMasukKamar->lamadirawat_kamar;
            $modelPulangNew->satuanlamarawat =Params::SATUANLAMARAWAT_RI;
            $modelPulangNew->ruanganakhir_id = Yii::app()->user->getState('ruangan_id');
            $modelPulangNew->create_time = date( 'Y-m-d H:i:s');
            $modelPulangNew->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modelPulangNew->create_loginpemakai_id = Yii::app()->user->id;
            $modelPulangNew->pasienadmisi_id =$idPasienAdmisi;

            $masukKamar = MasukkamarT::model()->findByAttributes(
                array(
                    'pasienadmisi_id'=>$idPasienAdmisi,
                    'pindahkamar_id'=>null
                )
            );

           if($modelPulangNew->validate()){
               if($modelPulangNew->save()){
//                   ini digunakan untuk mengupdate masukkamar ruangan_id=>menjadi null dan kamarruangan_m  status menjadi true
                $kamarruangan_status = false;
                $keterangan_kamar = 'RENCANA PULANG';

                $ukamarruangan = KamarruanganM::model()->updateByPk(
                    $masukKamar->kamarruangan_id,
                    array(
                        'kamarruangan_status'=>$kamarruangan_status,
                        'keterangan_kamar'=>$keterangan_kamar
                    )
                );
// EHJ-3441     $umasukkamar = MasukkamarT::model()->updateByPk($masukKamar->masukkamar_id, array('kamarruangan_id'=>null));
                if($ukamarruangan){
                    $this->successPasienPulang = true;
                }
            }else{
                $this->successPasienPulang = false;
            }
        }

            return $modelPulangNew;
        }

        public function actionPindahKamarDariTransaksi()
	{

            $format = new CustomFormat;
            $modPindahKamar = new RIPindahkamarT;
            $modPasienRIV = new RIPasienRawatInapV;
            $modMasukKamar = new RIMasukKamarT;

            $modPindahKamar->tglpindahkamar = date('Y-m-d H:i:s');
            $modPindahKamar->jampindahkamar = date('H:i:s');
            $tersimpan = 'Tidak';

            $modPasienRIV->unsetAttributes();
             if(isset($_GET['RIPasienrawatinapV'])){
               $modPasienRIV->attributes = $_GET['RIPasienrawatinapV'];
             }

            if(isset($_POST['RIPindahkamarT']))
            {
                if($_POST['RIPindahkamarT']['pendaftaran_id'] == '')
                {
                    Yii::app()->user->setFlash('error',"Pendaftaran masih kosong coba cek lagi");
                    $this->refresh();
                }else{
                    $modPindahKamar->attributes = $_POST['RIPindahkamarT'];
                    $idPendaftaran = ((isset($_POST['RIPindahkamarT']['pendaftaran_id'])) ? $_POST['RIPindahkamarT']['pendaftaran_id'] : null);
                    $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);

                    $modPasienRIV = RIPasienRawatInapV::model()->findByAttributes(
                        array(
                            'pasienadmisi_id'=>$modPendaftaran->pasienadmisi_id
                        )
                    );

                    /* PASIEN MASUK KAMAR LAMA*/
                    $modMasukKamar = RIMasukKamarT::model()->findByPk(
                        $modPindahKamar->masukkamar_id
                    );

                    /* PASIEN ADMISI*/
                    $modPasienAdmisi = RIPasienAdmisiT::model()->findByPK(
                        $modPindahKamar->pasienadmisi_id
                    );

                    /* END PASIEN ADMISI*/

                    $modPindahKamar->pasien_id = $modPasienRIV->pasien_id;
                    $modPindahKamar->pendaftaran_id = $modPasienRIV->pendaftaran_id;
                    $modPindahKamar->pasienadmisi_id = $modPasienRIV->pasienadmisi_id;
                    $modPindahKamar->masukkamar_id = $modPasienRIV->masukkamar_id;
                    $modPindahKamar->shift_id = Yii::app()->user->getState('shift_id');
                    $modPindahKamar->nopindahkamar = Generator::noMasukKamar($modPindahKamar->ruangan_id);
                    $modPindahKamar->carabayar_id = $modPasienAdmisi->carabayar_id;
                    $modPindahKamar->penjamin_id = $modPasienAdmisi->penjamin_id;
                    $modPindahKamar->pegawai_id = $modPasienAdmisi->pegawai_id;


                    /* PROSES SIMPAN DAN UPDATE */
                    $transaction = Yii::app()->db->beginTransaction();
                    $is_simpan = false;
                    $errors = array();
                    $pesan = array(
                        'status'=>'success',
                        'text'=>'Data Berhasil Disimpan'
                    );
                    try {
                        /* simpan_pindah_kamar */
                        $modPindahKamar->save();
                        KamarruanganM::model()->updateByPk(
                            $modPasienAdmisi->kamarruangan_id, array('kamarruangan_status'=>true,'keterangan_kamar'=>'OPEN')
                        );

                        /* update_masuk_kamar lama*/
                        $modMasukKamar->pindahkamar_id = $modPindahKamar->pindahkamar_id;
                        if($modMasukKamar->save())
                        {
                            /* update_pasien_admisi */
                            $is_simpan = true;
                            $modPasienAdmisi->ruangan_id = $modPindahKamar->ruangan_id;
                            $modPasienAdmisi->kelaspelayanan_id = $modPindahKamar->kelaspelayanan_id;
                            $modPasienAdmisi->kamarruangan_id = $modPindahKamar->kamarruangan_id;
                            if($_POST['RIPindahkamarT']['langsungMasukKamar'] == true){
                                if($modPasienAdmisi->save())
                                {
                                    /* simpan_masuk_kamar_new */
                                    $is_simpan = true;
                                    $mod_masuk_kamar = new RIMasukKamarT();
                                    $mod_masuk_kamar->attributes = $modPindahKamar->attributes; //mengambil nilai ruangan_id,
                                    $mod_masuk_kamar->pindahkamar_id = null; //karena record baru asumsi belum pernah pindah
                                    $mod_masuk_kamar->masukkamar_id = null; //record baru
                                    $mod_masuk_kamar->nomasukkamar = Generator::noMasukKamar(Yii::app()->user->getState('ruangan_id'));
                                    $mod_masuk_kamar->tglmasukkamar = $modPindahKamar->tglpindahkamar;
                                    $mod_masuk_kamar->jammasukkamar = $modPindahKamar->jampindahkamar;
                                    $mod_masuk_kamar->kelaspelayanan_id = empty($modPindahKamar->kelaspelayanan_id) ?  $modMasukKamar->kelaspelayanan_id : $modPindahKamar->kelaspelayanan_id;
                                    $mod_masuk_kamar->create_time = date('Y-m-d H:i:s');
                                    $mod_masuk_kamar->create_loginpemakai_id = Yii::app()->user->id;
                                    $mod_masuk_kamar->create_ruangan = Yii::app()->user->getState('ruangan_id');
                                    if($_POST['RIPindahkamarT']['langsungMasukKamar'] == true){
                                        $mod_masuk_kamar->kamarruangan_id = $modPindahKamar->kamarruangan_id;
                                    }else{
                                        $mod_masuk_kamar->kamarruangan_id = null; //sebagai tanda sudah / belum masuk kamar
                                    }
                                    if($mod_masuk_kamar->save())
                                    {
                                        $is_simpan = true;

                                        /* update_kamar_ruangan */
                                        KamarruanganM::model()->updateByPk(
                                            $modPindahKamar->kamarruangan_id, array('kamarruangan_status'=>false,'keterangan_kamar'=>'IN USE')
                                        );
                                    }else{
                                        $is_simpan = false;
                                        $pesan = array(
                                            'status'=>'error',
                                            'text'=>'Data Masuk Kamar Gagal Disimpan'
                                        );
                                        $errors[] = $pesan;
                                    }
                                }else{
                                    $is_simpan = false;
                                    $pesan = array(
                                        'status'=>'error',
                                        'text'=>'Data Admisi Gagal Disimpan'
                                    );
                                    $errors[] = $pesan;
                                }
                            }
                        }else{
                            $is_simpan = false;
                            $pesan = array(
                                'status'=>'error',
                                'text'=>'Data Masuk Kamar Gagal Disimpan'
                            );
                            $errors[] = $pesan;
                        }

                        if($is_simpan)
                        {
                            $tersimpan = 'Ya';
                            $transaction->commit();
                            Yii::app()->user->setFlash($pesan['status'],$pesan['text']);
                        }else{
                            foreach($errors as $val)
                            {
                                Yii::app()->user->setFlash($val['status'],$val['text']);
                            }
                            $transaction->rollback();
                        }

                    } catch (Exception $exc)
                    {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan" . MyExceptionMessage::getMessage($exc,true));
                    }
                }
             }
             $modPindahKamar->tglpindahkamar = date('d M Y'); //untuk di form saja
             $this->render(
                'formPindahKamar',
                 array(
                    'modPindahKamar'=>$modPindahKamar,
                    'modPasienRIV'=>$modPasienRIV,
                    'tersimpan'=>$tersimpan,
                    'modMasukKamar'=>$modMasukKamar
                 )
             );
	}

        public function actionPindahKamarPasienRI($idPendaftaran)
	{
            $insert_notifikasi = new MyFunction();
            $this->layout='//layouts/frameDialog';
            $format = new CustomFormat();
            $modPindahKamar = new RIPindahkamarT;
            $modPasienAdmisi = new RIPasienAdmisiT;
            $modPasienPulang = new RIPasienPulangT;

            $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
            $modPasienRIV = RIPasienRawatInapV::model()->findByAttributes(
                array('pasienadmisi_id'=>$modPendaftaran->pasienadmisi_id)
            );
            $modMasukKamar = RIMasukKamarT::model()->findByPk(
                $modPasienRIV->masukkamar_id
            );

            $modPindahKamar->pasien_id=$modPasienRIV->pasien_id;
            $modPindahKamar->pendaftaran_id=$modPasienRIV->pendaftaran_id;
            $modPindahKamar->pasienadmisi_id=$modPasienRIV->pasienadmisi_id;
            $modPindahKamar->masukkamar_id=$modPasienRIV->masukkamar_id;
            $modPindahKamar->kamarruangan_id = $modPasienRIV->kamarruangan_id;
            $modPindahKamar->pegawai_id = $modPendaftaran->pegawai_id;
            $modPindahKamar->carabayar_id = $modPendaftaran->carabayar_id;
            $modPindahKamar->ruangan_id = $modPendaftaran->ruangan_id;
            $modPindahKamar->penjamin_id = $modPendaftaran->penjamin_id;
            $modPindahKamar->kelaspelayanan_id = $modPasienRIV->kelaspelayanan_id;
            $modPindahKamar->jampindahkamar=date('H:i:s');
            $modPindahKamar->shift_id = Yii::app()->user->getState('shift_id');
            $modPindahKamar->nopindahkamar = Generator::noMasukKamar($modPindahKamar->ruangan_id);
            $modPindahKamar->tglpindahkamar = date('Y-m-d H:i:s');

            $tersimpan = 'Tidak';
            if(isset($_POST['RIPindahkamarT']))
            {
                if($_POST['RIPindahkamarT']['pendaftaran_id'] == '')
                {
                    Yii::app()->user->setFlash('error',"Pendaftaran masih kosong coba cek lagi");
                    $this->refresh();
                }else{
                    $modPindahKamar->attributes = $_POST['RIPindahkamarT'];
                    $modPindahKamar->tglpindahkamar = $format->formatDateMediumForDB($_POST['RIPindahkamarT']['tglpindahkamar'])." ".$modPindahKamar->jampindahkamar;
                    $idPendaftaran = ((isset($_POST['RIPindahkamarT']['pendaftaran_id'])) ? $_POST['RIPindahkamarT']['pendaftaran_id'] : null);
                    $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);

                    $modPasienRIV = RIPasienRawatInapV::model()->findByAttributes(
                        array(
                            'pasienadmisi_id'=>$modPendaftaran->pasienadmisi_id
                        )
                    );

                    /* PASIEN MASUK KAMAR LAMA*/
                    $modMasukKamar = RIMasukKamarT::model()->findByPk(
                        $modPindahKamar->masukkamar_id
                    );

                    /* PASIEN ADMISI*/
                    $modPasienAdmisi = RIPasienAdmisiT::model()->findByPK(
                        $modPindahKamar->pasienadmisi_id
                    );

                    /* END PASIEN ADMISI*/

                    $modPindahKamar->pasien_id = $modPasienRIV->pasien_id;
                    $modPindahKamar->pendaftaran_id = $modPasienRIV->pendaftaran_id;
                    $modPindahKamar->pasienadmisi_id = $modPasienRIV->pasienadmisi_id;
                    $modPindahKamar->shift_id = Yii::app()->user->getState('shift_id');
                    $modPindahKamar->nopindahkamar = Generator::noMasukKamar($modPindahKamar->ruangan_id);
                    $modPindahKamar->carabayar_id = $modPasienAdmisi->carabayar_id;
                    $modPindahKamar->penjamin_id = $modPasienAdmisi->penjamin_id;
                    $modPindahKamar->pegawai_id = $modPasienAdmisi->pegawai_id;


                    /* PROSES SIMPAN DAN UPDATE */
                    $transaction = Yii::app()->db->beginTransaction();
                    $is_simpan = false;
                    $errors = array();
                    $pesan = array(
                        'status'=>'success',
                        'text'=>'Data Berhasil Disimpan'
                    );
                    try {
                        /* simpan_pindah_kamar */
                        $modPindahKamar->masukkamar_id = null; //ini di isi masukkamar baru nanti
                        if($modPindahKamar->save()){
                            $modMasukKamar->pindahkamar_id = $modPindahKamar->pindahkamar_id;
                        }else{
                            $modMasukKamar->pindahkamar_id = null;
                        }

                        KamarruanganM::model()->updateByPk(
                            $modPasienAdmisi->kamarruangan_id, array('kamarruangan_status'=>true,'keterangan_kamar'=>'OPEN')
                        );

                        /* update_masuk_kamar lama*/
                        if($modMasukKamar->save())
                        {
                            /* update_pasien_admisi */
                            $is_simpan = true;
                            $modPasienAdmisi->ruangan_id = $modPindahKamar->ruangan_id;
                            $modPasienAdmisi->kelaspelayanan_id = $modPindahKamar->kelaspelayanan_id;
                            $modPasienAdmisi->kamarruangan_id = $modPindahKamar->kamarruangan_id;
                            if($modPasienAdmisi->save())
                            {
                                /* simpan_masuk_kamar_new */
                                $is_simpan = true;
                                $mod_masuk_kamar = new RIMasukKamarT();
                                $mod_masuk_kamar->attributes = $modPindahKamar->attributes; //mengambil nilai ruangan_id,
                                $mod_masuk_kamar->pindahkamar_id = null; //karena record baru asumsi belum pernah pindah
                                $mod_masuk_kamar->masukkamar_id = null; //record baru
                                $mod_masuk_kamar->nomasukkamar = Generator::noMasukKamar(Yii::app()->user->getState('ruangan_id'));
                                $mod_masuk_kamar->tglmasukkamar = $modPindahKamar->tglpindahkamar;
                                $mod_masuk_kamar->jammasukkamar = $modPindahKamar->jampindahkamar;
                                $mod_masuk_kamar->kelaspelayanan_id = empty($modPindahKamar->kelaspelayanan_id) ?  $modMasukKamar->kelaspelayanan_id : $modPindahKamar->kelaspelayanan_id;
                                $mod_masuk_kamar->create_time = date('Y-m-d H:i:s');
                                $mod_masuk_kamar->create_loginpemakai_id = Yii::app()->user->id;
                                $mod_masuk_kamar->create_ruangan = Yii::app()->user->getState('ruangan_id');
                                if($_POST['RIPindahkamarT']['langsungMasukKamar'] == true){
                                    $mod_masuk_kamar->kamarruangan_id = $modPindahKamar->kamarruangan_id;
                                }else{
                                    $mod_masuk_kamar->kamarruangan_id = null; //sebagai tanda sudah / belum masuk kamar
                                }


                                if($mod_masuk_kamar->save())
                                {
                                    $is_simpan = true;
                                    //update masukkamar_id (baru) pada pindahkamar_t
                                    $modPindahKamar->updateByPk($modPindahKamar->pindahkamar_id, array('masukkamar_id'=>$mod_masuk_kamar->masukkamar_id));
                                    if($_POST['RIPindahkamarT']['langsungMasukKamar'] == true){
                                        /* update_kamar_ruangan */
                                        KamarruanganM::model()->updateByPk(
                                            $modPindahKamar->kamarruangan_id, array('kamarruangan_status'=>false,'keterangan_kamar'=>'IN USE')
                                        );
                                    }
                                }else{
                                    $is_simpan = false;
                                    $pesan = array(
                                        'status'=>'error',
                                        'text'=>'Data Masuk Kamar Gagal Disimpan'
                                    );
                                    $errors[] = $pesan;
                                }

                            }else{
                                $is_simpan = false;
                                $pesan = array(
                                    'status'=>'error',
                                    'text'=>'Data Admisi Gagal Disimpan'
                                );
                                $errors[] = $pesan;
                            }
                        }else{
                            $is_simpan = false;
                            $pesan = array(
                                'status'=>'error',
                                'text'=>'Data Masuk Kamar Gagal Disimpan'
                            );
                            $errors[] = $pesan;
                        }

                        if($is_simpan)
                        {
                            //INSERT NOTIFIKASI PINDAH
                            $pasienPindah = PasienM::model()->findByPk($modPasienAdmisi->pasien_id);
                            $ruanganAsal = RuanganM::model()->findByPk($modMasukKamar->ruangan_id);
                            $kamarAsal = KamarruanganM::model()->findByPk($modMasukKamar->kamarruangan_id);
                            $ruanganTujuan = RuanganM::model()->findByPk($modPindahKamar->ruangan_id);
                            $kamarTujuan = KamarruanganM::model()->findByPk($modPindahKamar->kamarruangan_id);
                            $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                            $params['create_time'] = date( 'Y-m-d H:i:s');
                            $params['create_loginpemakai_id'] = Yii::app()->user->id;
                            $params['instalasi_id'] = 4;
                            $params['modul_id'] = 7;
                            $params['isinotifikasi'] = $pasienPindah->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $pasienPindah->nama_pasien."<br>".
                                                "Dari:".$ruanganAsal->ruangan_nama."/".$kamarAsal->kamarruangan_nokamar."/".$kamarAsal->kamarruangan_nobed." ".
                                                "Ke:".$ruanganTujuan->ruangan_nama."/".$kamarTujuan->kamarruangan_nokamar."/".$kamarTujuan->kamarruangan_nobed." ".
                                                ($_POST['RIPindahkamarT']['langsungMasukKamar'] == true ? "(Langsung Masuk)" : "(Belum Masuk)");
                            $params['create_ruangan'] = Yii::app()->user->getState('ruangan_id');
                            $params['judulnotifikasi'] = 'Pasien Pindah Kamar';
                            $nofitikasi = $insert_notifikasi->insertNotifikasi($params);

                            $tersimpan = 'Ya';
                            $transaction->commit();
                            Yii::app()->user->setFlash($pesan['status'],$pesan['text']);
                        }else{
                            foreach($errors as $val)
                            {
                                Yii::app()->user->setFlash($val['status'],$val['text']);
                            }
                            $transaction->rollback();
                        }

                    } catch (Exception $exc)
                    {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan" . MyExceptionMessage::getMessage($exc,true));
                    }
                }
             }
             $modPindahKamar->tglpindahkamar = date('d M Y'); //untuk di form saja
            $this->render(
                'formPindahKamar',
                array(
                    'modPindahKamar'=>$modPindahKamar,
                    'modPasienRIV'=>$modPasienRIV,
                    'modMasukKamar'=>$modMasukKamar,
                    'modTindakan'=>$modTindakan,
                    'tersimpan'=>$tersimpan,
                    'is_grid'=>true,
                )
            );
	}
        //KENAPA FUNGSI InsertPindahKamarUpdateMasukKamar INI TIDAK DIPAKAI / TIDAK DIHAPUS YA ???
        //BY: ICHAN
        protected function InsertPindahKamarUpdateMasukKamar($modMasukKamar, $modPindahKamar)
        {
                    $format = new CustomFormat;
                    $modPasienAdmisi = RIPasienAdmisiT::model()->findByPK($modPindahKamar->pasienadmisi_id);
                    $modPindahKamar->kamarruangan_id = $modPasienAdmisi->kamarruangan_id;
                    $modPindahKamar->carabayar_id = $modPasienAdmisi->carabayar_id;
                    $modPindahKamar->penjamin_id = $modPasienAdmisi->penjamin_id;
                    $modPindahKamar->pegawai_id = $modPasienAdmisi->pegawai_id;
                    $modPindahKamar->kelaspelayanan_id = $modPasienAdmisi->kelaspelayanan_id;

                    $result = array();
                    $modPindahKamar->masukkamar_id=null;
                    if($modPasienAdmisi->kamarruangan_id == null){
                        echo "<script>
                                    alert('No Ruangan Kamar Kosong , Silahkan Isi No Ruangan Kamar');
                                    window.top.location.href='".Yii::app()->createUrl('rawatInap/PasienRawatInap/index')."';
                                </script>";
                    }else{
                         //untuk update kamar ruangan menjadi true apabila dipakai dan menjadi false jika tidak dipakai
                        $kamar1 = KamarruanganM::model()->updateByPk($modPasienAdmisi->kamarruangan_id, array('kamarruangan_status'=>true,'keterangan_kamar'=>'OPEN'));
                        $kamar2 = KamarruanganM::model()->updateByPk($modPindahKamar->kamarruangan_id, array('kamarruangan_status'=>false,'keterangan_kamar'=>'IN USE'));
                        //=========jw endit======
                        $modTindakan = null;
                        if($modPindahKamar->save() && $kamar1 && $kamar2 ){
                            $modMasukKamar->pindahkamar_id = $modPindahKamar->pindahkamar_id;
                            $modMasukKamar->lamadirawat_kamar = MyFunction::hitungHari($modMasukKamar->tglmasukkamar);
                            if($modMasukKamar->save()){
                               $this->successSave=true;
                               $modPendaftaran = PendaftaranT::model()->findByPk($modPasienAdmisi->pendaftaran_id);
                               $modTindakan = $this->saveAkomodasi($modPendaftaran, $modPasienAdmisi, $modMasukKamar->lamadirawat_kamar);
                            }else{
//                                echo '<pre>';
                                print_r($modMasukKamar->getErrors());
                                $this->successSave=false;
                            }

                        }else{
                            $this->successSave=false;
                        }
                        $result['modPindahKamar'] = $modPindahKamar;
                        $result['modMasukKamar'] = $modMasukKamar;
                        $result['modTindakan'] = $modTindakan;
                    }

                   return $result;

        }


        public function cekAkomodasiHariIni($modPendaftaran, $modPasienAdmisi, $modMasukKamar){
            $akomodasi = PasienRawatInapController::tindakanAkomodasi($modMasukKamar->kelaspelayanan_id);
            $tipePaket = PasienRawatInapController::tipePaketAkomodasi($modPendaftaran, $modPasienAdmisi, $akomodasi->daftartindakan_id);
            $criteria = new CdbCriteria();
            $criteria->addCondition('pendaftaran_id = '.$modPasienAdmisi->pendaftaran_id);
            $criteria->addCondition('pasienadmisi_id = '.$modPasienAdmisi->pasienadmisi_id);
            $criteria->addCondition('ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
            $criteria->addCondition('kelaspelayanan_id = '.$modMasukKamar->kelaspelayanan_id);
            $criteria->addCondition('tipepaket_id = '.$tipePaket);
            $criteria->addBetweenCondition('tgl_tindakan',date('Y-m-d')." 00:00:00",date('Y-m-d')." 23:59:59");
            $modAkomodasi = TindakanpelayananT::model()->findAll($criteria);
            if(count($modAkomodasi) == 0){
                return true;
            }else{
                return false;
            }
        }

        public function saveAkomodasi($modPendaftaran, $modPasienAdmisi, $lamaRawat)
        {
            $cekTindakanKomponen=0;
            $modMasukKamar = BKInfopasienmasukkamarV::model()->findByAttributes(array(
                'pendaftaran_id'=>$modPasienAdmisi->pendaftaran_id,
                'pasienadmisi_id'=>$modPasienAdmisi->pasienadmisi_id,
                'ruangan_id'=>Yii::app()->user->getState('ruangan_id'),
            ));
            if($modMasukKamar){ //REPLACE kelaspelayanan_id
                $modPasienAdmisi->kelaspelayanan_id = $modMasukKamar->kelaspelayanan_id;
            }
            $akomodasi = PasienRawatInapController::tindakanAkomodasi($modPasienAdmisi->kelaspelayanan_id);
            $modTindakanPelayan = New TindakanpelayananT;
            $modTindakanPelayan->penjamin_id = $modPasienAdmisi->penjamin_id;
            $modTindakanPelayan->pasien_id = $modPasienAdmisi->pasien_id;
            $modTindakanPelayan->pasienadmisi_id = $modPasienAdmisi->pasienadmisi_id;
            $modTindakanPelayan->kelaspelayanan_id = $modPasienAdmisi->kelaspelayanan_id;
            $modTindakanPelayan->instalasi_id = Params::INSTALASI_ID_RJ;
            $modTindakanPelayan->pendaftaran_id = $modPasienAdmisi->pendaftaran_id;
            $modTindakanPelayan->shift_id = Yii::app()->user->getState('shift_id');
            $modTindakanPelayan->daftartindakan_id = $akomodasi->daftartindakan_id;
            $modTindakanPelayan->carabayar_id = $modPasienAdmisi->carabayar_id;
            $modTindakanPelayan->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
            $modTindakanPelayan->tgl_tindakan = date('Y-m-d H:i:s');
            $modTindakanPelayan->tarif_satuan = $akomodasi->harga_tariftindakan;
            $modTindakanPelayan->qty_tindakan = $lamaRawat;
            $modTindakanPelayan->tarif_tindakan = $modTindakanPelayan->tarif_satuan * $modTindakanPelayan->qty_tindakan;
            $modTindakanPelayan->satuantindakan = Params::SATUAN_TINDAKAN_PENDAFTARAN;
            $modTindakanPelayan->cyto_tindakan = 0;
            $modTindakanPelayan->tarifcyto_tindakan = 0;
            $modTindakanPelayan->dokterpemeriksa1_id = NULL;
            $modTindakanPelayan->discount_tindakan = 0;
            $modTindakanPelayan->subsidiasuransi_tindakan = 0;
            $modTindakanPelayan->subsidipemerintah_tindakan = 0;
            $modTindakanPelayan->subsisidirumahsakit_tindakan = 0;
            $modTindakanPelayan->iurbiaya_tindakan=0;
            $modTindakanPelayan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modTindakanPelayan->tipepaket_id = PasienRawatInapController::tipePaketAkomodasi($modPendaftaran, $modPasienAdmisi, $modTindakanPelayan->daftartindakan_id);
            $modTindakanPelayan->create_time = date('Y-m-d H:i:s');
            $modTindakanPelayan->create_loginpemakai_id = Yii::app()->user->id;
            $modTindakanPelayan->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if(empty($modTindakanPelayan->daftartindakan_id)){
                $kelas = KelaspelayananM::model()->findByPk($modTindakanPelayan->kelaspelayanan_id);
                    echo "<script>
                        alert('Tarif Akomodasi Berdasarkan Kelas ".$kelas->kelaspelayanan_nama." tidak ada ,silahkan hub administrator');
                        window.top.location.href='".Yii::app()->createUrl('rawatInap/PasienRawatInap/index')."';
                    </script>";
            }else{
                $criteria = new CDbCriteria;

                $criteria->addCondition('daftartindakan_id ='.$modTindakanPelayan->daftartindakan_id);
                $criteria->addCondition('kelaspelayanan_id ='.$modTindakanPelayan->kelaspelayanan_id);
                $tarifTindakan= TariftindakanM::model()->findAll($criteria);
                $result = array();
                if ($tarifTindakan > 0) {
                    foreach($tarifTindakan AS $dataTarif):
                         if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                                $modTindakanPelayan->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                            }
                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                                $modTindakanPelayan->tarif_medis=$dataTarif['harga_tariftindakan'];
                            }
                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                                $modTindakanPelayan->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                            }
                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                                $modTindakanPelayan->tarif_bhp=$dataTarif['harga_tariftindakan'];
                            }
                    endforeach;
                }
            }

             if($modTindakanPelayan->save()){
                 $this->successSaveTindakan = true;
                    $tindakanKomponen= $tarifTindakan; //TariftindakanM::model()->findAll('daftartindakan_id='.$_POST['TindakanPelayananT']['idTindakan'].' AND kelaspelayanan_id='.$modTindakanPelayan->kelaspelayanan_id.'');
                    $jumlahKomponen=COUNT($tindakanKomponen);
                    if ($jumlahKomponen > 0){
                        foreach ($tindakanKomponen AS $tampilKomponen):
                                $modTindakanKomponen=new RITindakanKomponenT;
                                $modTindakanKomponen->tindakanpelayanan_id= $modTindakanPelayan->tindakanpelayanan_id;
                                $modTindakanKomponen->komponentarif_id=$tampilKomponen['komponentarif_id'];
                                $modTindakanKomponen->tarif_kompsatuan=$tampilKomponen['harga_tariftindakan'];
                                $modTindakanKomponen->tarif_tindakankomp=$tampilKomponen['harga_tariftindakan']*$modTindakanPelayan->qty_tindakan;
                                $modTindakanKomponen->tarifcyto_tindakankomp=0;
                                $modTindakanKomponen->subsidiasuransikomp=$modTindakanPelayan->subsidiasuransi_tindakan;
                                $modTindakanKomponen->subsidipemerintahkomp=$modTindakanPelayan->subsidipemerintah_tindakan;
                                $modTindakanKomponen->subsidirumahsakitkomp=$modTindakanPelayan->subsisidirumahsakit_tindakan;
                                $modTindakanKomponen->iurbiayakomp=$modTindakanPelayan->iurbiaya_tindakan;

                                if($modTindakanKomponen->save()){
                                    $cekTindakanKomponen++;
                                }
                        endforeach;
                    }else{
                       echo "<script>
                                    alert('Tarif Akomodasi tidak ditemukan, silahkan input terlebih dahulu tarifnya');
                                    window.top.location.href='".Yii::app()->createUrl('rawatInap/PasienRawatInap/index')."';
                                </script>";
                    }
                    if($cekTindakanKomponen!=$jumlahKomponen){
                           $this->successSaveTindakanKomponen=false;
                    }
            } else {
                if (empty($akomodasi->daftartindakan_id)){
                    $modTindakanPelayan->addError('error', 'Daftar Tindakan akomodasi untuk Ruangan dan Kelas Pelayanan ini tidak ditemukan');
                }
                $this->successSaveTindakan = false;
            }
            return $modTindakanPelayan;
        }

        public function tindakanAkomodasi($kelaspelayanan_id)
        {
            $criteria = new CDbCriteria;
            $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('daftartindakan_akomodasi', true);
            $criteria->compare('kelaspelayanan_id', $kelaspelayanan_id);
            $daftarTindakan = TariftindakanperdaruanganV::model()->find($criteria);

            return $daftarTindakan;
        }

        public function tipePaketAkomodasi($modPendaftaran, $modPasienAdmisi, $idTindakan)
        {
            $criteria = new CDbCriteria;
            $criteria->with = array('tipepaket');
            $criteria->compare('daftartindakan_id', $idTindakan);
            $criteria->compare('tipepaket.carabayar_id', $modPasienAdmisi->carabayar_id);
            $criteria->compare('tipepaket.penjamin_id', $modPasienAdmisi->penjamin_id);
            $criteria->compare('tipepaket.kelaspelayanan_id', $modPasienAdmisi->kelaspelayanan_id);
            $modPaket = PaketpelayananM::model()->find($criteria);
            $paket = Params::TIPEPAKET_NONPAKET;
            if (isset($modPaket->paket_id)){
                $paket = $modPaket->tipepaket_id;
            }

            return $paket;
        }

        // Untuk Melakukan Transaksi Bata Rawat Inap => Added, March, 11 2013 //
//        batal rawat inap pertama

//            public function actionBatalRawatInap()
//        {
//             if(Yii::app()->request->isAjaxRequest) {
//                $idOtoritas = $_POST['idOtoritas'];
//                $namaOtoritas = $_POST['namaOtoritas'];
//                $idPasienPulang=$_POST['idPasienPulang'];
//                $alasanPembatalan=$_POST['Alasan'];
//                $idPendaftaran = $_POST['idPendaftaran'];
//                $idKamarruangan = $_POST['idKamarruangan'];
//                $idPasienAdmisi = $_POST['idPasienAdmisi'];
//
//
//                $modPasienBatalPulang = new PasienbatalpulangT;
//                $modPasienadmisi = PasienadmisiT::model()->findByPk($idPasienAdmisi);
//                $modPasienBatalPulang->namauser_otorisasi=$namaOtoritas;
//                $modPasienBatalPulang->iduser_otorisasi=$idOtoritas;
//                $modPasienBatalPulang->pasienpulang_id=$idPasienPulang;
//                $modPasienBatalPulang->tglpembatalan=date('Y-m-d H:i:s');
//                $modPasienBatalPulang->alasanpembatalan=$alasanPembatalan;
//
//                 $transaction = Yii::app()->db->beginTransaction();
//                 try{
//                    if($modPasienBatalPulang->save()){
//
//                        $pulang         = PasienpulangT::model()->updateByPk($idPasienPulang,array('pasienbatalpulang_id'=>$modPasienBatalPulang->pasienbatalpulang_id));
//                        $pasienadmisi   = PasienadmisiT::model()->findByPk($idPasienAdmisi);
////                        if($pasienadmisi->kamarruangan_id){
////                            $updateKamarAdmisi = PasienadmisiT::model()->updateByPk('pasienadmisi_id', array('kamarruangan_id'=>null));
////                        }
//                        $masukKamar     = MasukkamarT::model()->findByAttributes(array('pasienadmisi_id'=>$idPasienAdmisi));
//                        if($masukKamar->kamarruangan_id){
//                            $updateKamarRuangan = KamarruanganM::model()->updateByPk($masukKamar->kamarruangan_id, array('kamarruangan_status'=>true));
//                            if($updateKamarRuangan){
//                                $deleteMasukKamar = MasukkamarT::model()->deleteByPk($masukKamar->masukkamar_id);
//                            }
//
//                        }else{
//                            $deleteMasukKamar = MasukkamarT::model()->deleteByPk($masukKamar->masukkamar_id);
//                        }
//
//                        $deletePaseinAdmisi = PasienadmisiT::model()->deleteByPk($idPasienAdmisi);
//                        if($deletePaseinAdmisi){
//                            $pendaftaran    = PendaftaranT::model()->updateByPk($idPendaftaran,array('pasienpulang_id'=>null,'pasienadmisi_id'=>null, 'alihstatus'=>false));
//                        }
////                        $kamarruangan   = KamarruanganM::model()->updateByPk($modPasienadmisi->kamarruangan_id,array('kamarruangan_status'=>true));
////                        if(empty($idPasienPulang)){
////                            $transaksi = $pendaftaran && $pasienadmisi && $kamarruangan;
////                        }else{
////                            $transaksi = $pulang && $pendaftaran && $pasienadmisi && $kamarruangan;
////                        }
//
//                        $transaksi = $deleteMasukKamar && $deletePaseinAdmisi;
//                        if ($transaksi){
//                                $data['status'] = 'success';
//                                $transaction->commit();
//                                $this->refresh();
//                        }
//                        else{
//                            throw new Exception("Update Data Gagal");
//                        }
//                    }
//                    else{
//                        Throw new Exception("Pasien Batal Rawat Inap Gagal Disimpan");
//                    }
//                 }catch(Exception $ex){
//                     $transaction->rollback();
//                     $data['status'] = $ex;
//                 }
//
//                echo json_encode($data);
//                Yii::app()->end();
//                }
//        }

        // End Batal Rawat Inap //

        /**
         * digunakan untuk membatalkan pasien rawat inap
         * tabel yang digunakan
         * pendaftaran_t; pasien_m; pasienadmisi_t; jeniskasuspenyakit_m, pasienbatalrawat_r
         * @param type $idPendaftaran type = integer
         * @author jang wahyu 04-05-2013
         */
        public function actionBatalRawatInap($idPendaftaran)
        {
             $this->layout='//layouts/frameDialog';

             $modPasienBatalRawat = new PasienbatalrawatR;

             $modPendaftaran    = PendaftaranT::model()->findByPk($idPendaftaran);
             $modPasien         = PasienM::model()->findByPk($modPendaftaran->pasien_id);
             $modAdmisi         = PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id));
             $jenisPenyakit     = JeniskasuspenyakitM::model()->findByPk($modPendaftaran->jeniskasuspenyakit_id);
//             digunakan untuk merefresh jika data berhasil di simpan
             $tersimpan='Tidak';

             $modPendaftaran->jeniskasuspenyakit_nama   = $jenisPenyakit->jeniskasuspenyakit_nama;
             $modPasienBatalRawat->pasienadmisi_id      = $modAdmisi->pasienadmisi_id;
             $modPasienBatalRawat->create_time          = date('Y-m-d H:i:s');
             $modPasienBatalRawat->update_time          = date('Y-m-d H:i:s');
             $modPasienBatalRawat->create_ruangan       = Yii::app()->user->getState('ruangan_id');
             $modPasienBatalRawat->create_loginpemakai_id   = Yii::app()->user->id;
             $modPasienBatalRawat->update_loginpemakai_id   = Yii::app()->user->id;

             if(!empty($_REQUEST['PasienbatalrawatR'])){

                 $format = new CustomFormat();
                 $modPasienBatalRawat->attributes = $_REQUEST['PasienbatalrawatR'];
                 $modPasienBatalRawat->tglbatalrawat = $format->formatDateTimeMediumForDB($modPasienBatalRawat->tglbatalrawat);
                 $pendaftaran_id = $_POST['pendaftaran_id'];
                 $cek = PasienbatalrawatR::model()->findByAttributes(array('pasienadmisi_id'=>$modPasienBatalRawat->pasienadmisi_id));
                 $kamarRuangan = PasienadmisiT::model()->findByPk($modPasienBatalRawat->pasienadmisi_id);

                 if(!empty($cek->update_time) || !empty($cek->update_loginpemakaian_id)){
                     $modPasienBatalRawat->update_time              = date('Y-m-d H:i:s');
                     $modPasienBatalRawat->update_loginpemakai_id   = date('Y-m-d H:i:s');
                 }

                 if($modPasienBatalRawat->validate()){
                     $admisi_id = $modPasienBatalRawat->pasienadmisi_id;;
                     $transaction = Yii::app()->db->beginTransaction();
                     try {
                         if($modPasienBatalRawat->save()){
//                          update null terlebih dahulu kamarruangan_id di pasienadmisi
                            PasienadmisiT::model()->updateByPk($admisi_id, array('bookingkamar_id'=>null, 'kamarruangan_id'=>null, 'pendaftaran_id'=>null));
                            $modTindakan = TindakanpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));


                            foreach ($modTindakan as $i => $val) {
                              TindakanController::jurnalPembalikTindakan($val['tindakanpelayanan_id']);
                            }


                            TindakanpelayananT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));

                            $bookingKamar = BookingkamarT::model()->findByAttributes(array('pasienadmisi_id'=>$admisi_id));

                            $keterangan_kamar = 'OPEN';
                            $kamarruangan_status = true;
                            if($bookingKamar){
                              BookingkamarT::model()->updateByPk($bookingKamar->bookingkamar_id, array('pasienadmisi_id'=>null));
                              $keterangan_kamar = 'BOOKING';
                              $kamarruangan_status = false;
                            }

                            $masukKamar = MasukkamarT::model()->findByAttributes(array('pasienadmisi_id'=>$admisi_id));
                            if($masukKamar){
                               MasukkamarT::model()->deleteByPk($masukKamar->masukkamar_id);
                               KamarruanganM::model()->updateByPk($kamarRuangan->kamarruangan_id, array('kamarruangan_status'=>$kamarruangan_status,'keterangan_kamar'=>$keterangan_kamar));
                            }
                            $pendaftaran = PendaftaranT::model()->updateByPk($pendaftaran_id, array('pasienadmisi_id'=>null,'alihstatus'=>false));
                            $deleteAdmisi = PasienadmisiT::model()->deleteByPk($admisi_id);
                            if($deleteAdmisi){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                $tersimpan='Ya';
                            }
                         }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");
                         }
                     } catch (Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan", MyExceptionMessage::getMessage($exc,false));
                     }

                 }else{
                     Yii::app()->user->setFlash('error',"Data gagal disimpan");
                 }
             }

             $this->render('formBatalRawatInap', array('modAdmisi'=>$modAdmisi, 'modPendaftaran'=>$modPendaftaran, 'modPasien'=>$modPasien, 'modPasienBatalRawat'=>$modPasienBatalRawat, 'tersimpan'=>$tersimpan));
        }

        public function actionRencanaPulangPasienRI($idPasienadmisi)
	{
             $this->layout='//layouts/frameDialog';
             $insert_notifikasi = new MyFunction();
             $format = new CustomFormat;
             $model = new RIPasienAdmisiT;
             $model->rencanapulang = date('Y-m-d H:i:s');
             $tersimpan = 'Tidak';

             $modelAdmisi = RIPasienAdmisiT::model()->findByPk($idPasienadmisi);
             $modPasien = RIPasienM::model()->findByPk($modelAdmisi->pasien_id);
             $modPendaftaran = RIPendaftaranT::model()->findByPk($modelAdmisi->pendaftaran_id);

             if(isset($_POST['RIPasienAdmisiT'])){
                    $rencanapulang = $format->formatDateTimeMediumForDB($_POST['RIPasienAdmisiT']['rencanapulang']);
                    $idPasien = $_POST['RIPasienAdmisiT']['pasienadmisi_id'];
                    $transaction = Yii::app()->db->beginTransaction();
                  try {
                        $update = RIPasienAdmisiT::model()->updateByPk($idPasien,array('rencanapulang'=>$rencanapulang));

                        if($update){
                            $kamarUpdate = KamarruanganM::model()->updateByPk($modelAdmisi->kamarruangan_id,array('keterangan_kamar'=>'RENCANA PULANG'));
                            if($kamarUpdate){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                $tersimpan='Ya';
                            }else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                                $tersimpan='Tidak';
                            }
                        }else{
                           $transaction->rollback();
                           Yii::app()->user->setFlash('error',"Data gagal disimpan");
                        }

                        $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                        $params['create_time'] = date( 'Y-m-d H:i:s');
                        $params['create_loginpemakai_id'] = Yii::app()->user->id;
                        $params['instalasi_id'] = Yii::app()->user->getState('instalasi_id');
                        $params['modul_id'] = Yii::app()->id->module_id;
                        $params['isinotifikasi'] = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien;
                        $params['create_ruangan'] = $modelAdmisi->ruangan_id;
                        $params['judulnotifikasi'] = ($modelAdmisi->rencanapulang != null ? 'Rencana Pulang Pasien' : 'Rencana Pulang Pasien' );
                        $nofitikasi = $insert_notifikasi->insertNotifikasi($params);

                   } catch (Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan", MyExceptionMessage::getMessage($exc,false));
                   }
             }

             $model->rencanapulang = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($model->rencanapulang, 'yyyy-MM-dd hh:mm:ss'));

             $this->render('formRencanaPulang',array(
                                'modelAdmisi'=>$modelAdmisi,
                                'modPasien'=>$modPasien,
                                'modPendaftaran'=>$modPendaftaran,
                                'model'=>$model,
                                'tersimpan'=>$tersimpan,
                          ));
	}

}
