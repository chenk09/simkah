<?php
//Yii::import('rawatJalan.controllers.KonsulPoliController');
//Yii::import('rawatJalan.models.*');
//class KonsulPoliTRIController extends KonsulPoliController
//{
//        
//}
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class KonsulPoliTRIController extends SBaseController
{
	public function actionIndex($idPendaftaran,$idAdmisi)
	{
            $modAdmisi = (!empty($idAdmisi)) ? PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienadmisi_id'=>$idAdmisi)) : array();
            $modPendaftaran = RIPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RIPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $karcisTindakan = DaftartindakanM::model()->findAllByAttributes(array('daftartindakan_karcis'=>true));
            
            $modKonsul = new RIKonsulPoliT;
            $modelPendaftaran = new RIPendaftaranT;
            $modKonsul->pasien_id = $modPendaftaran->pasien_id;
            $modKonsul->pendaftaran_id = $idPendaftaran;
            $modKonsul->pegawai_id = $modPendaftaran->pegawai_id;
            $modKonsul->statusperiksa = Params::statusPeriksa(1);
            $modKonsul->asalpoliklinikkonsul_id = Yii::app()->user->getState('ruangan_id');
            
            if(isset($_POST['RIKonsulPoliT'])) {
                $modKonsul->attributes = $_POST['RIKonsulPoliT'];
                
//                $modelPendaftaran->pasienpulang_id = $modPendaftaran->pasienpulang_id;
//                $modelPendaftaran->ruangan_id = $_POST['RIKonsulPoliT']['ruangan_id'];
//                $modelPendaftaran->pegawai_id = $_POST['RIKonsulPoliT']['pegawai_id'];
//                $modelPendaftaran->pasien_id = $modPendaftaran->pasien_id;
//                $modelPendaftaran->statusperiksa = Params::statusPeriksa(1);
//                $modelPendaftaran->penjamin_id = $modPendaftaran->penjamin_id;
//                $modelPendaftaran->shift_id = $modPendaftaran->shift_id;
//                $modelPendaftaran->instalasi_id = Yii::app()->user->getState('instalasi_id');
//                $modelPendaftaran->jeniskasuspenyakit_id = $modPendaftaran->jeniskasuspenyakit_id;
//                $modelPendaftaran->nopendaftaran_aktif = $modPendaftaran->nopendaftaran_aktif;
//                $modelPendaftaran->create_ruangan = $modPendaftaran->create_ruangan;
//                $modelPendaftaran->tglselesaiperiksa = $modPendaftaran->tglselesaiperiksa;
//                $modelPendaftaran->statusmasuk = $modPendaftaran->statusmasuk;
//                $modelPendaftaran->kunjungan = $modPendaftaran->kunjungan;
//                $modelPendaftaran->statuspasien = $modPendaftaran->statuspasien;
//                $modelPendaftaran->statusperiksa = $modPendaftaran->statusperiksa;
//                $modelPendaftaran->keadaanmasuk = $modPendaftaran->keadaanmasuk;
//                $modelPendaftaran->transportasi = $modPendaftaran->transportasi;
//                $modelPendaftaran->no_urutantri = $modPendaftaran->no_urutantri;
//                $modelPendaftaran->tgl_pendaftaran = $modPendaftaran->tgl_pendaftaran;
//                $modelPendaftaran->no_pendaftaran = $modPendaftaran->no_pendaftaran;
//                $modelPendaftaran->golonganumur_id = $modPendaftaran->golonganumur_id;
//                $modelPendaftaran->kelompokumur_id = $modPendaftaran->kelompokumur_id;
//                $modelPendaftaran->pasienadmisi_id = $idAdmisi;
//                $modelPendaftaran->carabayar_id = $modPendaftaran->carabayar_id;
//                $modelPendaftaran->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
//                $modelPendaftaran->pembayaranpelayanan_id = $modPendaftaran->pembayaranpelayanan_id;
//                $modelPendaftaran->create_time = $modPendaftaran->create_time;
//                $modelPendaftaran->update_time = $modPendaftaran->update_time;
//                $modelPendaftaran->caramasuk_id = $modPendaftaran->caramasuk_id;
//                $modelPendaftaran->umur = $modPendaftaran->umur;
//                $modelPendaftaran->kunjunganrumah = $modPendaftaran->kunjunganrumah;
//                $modelPendaftaran->byphone = $modPendaftaran->byphone;
//                $modelPendaftaran->alihstatus = $modPendaftaran->alihstatus;
//                $modelPendaftaran->create_loginpemakai_id = $modPendaftaran->create_loginpemakai_id;
//                $modelPendaftaran->update_loginpemakai_id = $modPendaftaran->update_loginpemakai_id;
//                $modelPendaftaran->pasienbatalperiksa_id = $modPendaftaran->pasienbatalperiksa_id;
//                $modelPendaftaran->penanggungjawab_id = $modPendaftaran->penanggungjawab_id;
//                $modelPendaftaran->persalinan_id = $modPendaftaran->persalinan_id;
//                $modelPendaftaran->pengirimanrm_id = $modPendaftaran->pengirimanrm_id;
//                $modelPendaftaran->peminjamanrm_id = $modPendaftaran->peminjamanrm_id;
//                $modelPendaftaran->antrian_id = $modPendaftaran->antrian_id;
//                $modelPendaftaran->rujukan_id = $modPendaftaran->rujukan_id;
//                $modelPendaftaran->karcis_id = $modPendaftaran->karcis_id;
                if($modKonsul->validate()){
                    if($modKonsul->save()){
                        
//                        echo '<pre>';
//                        echo print_r($modelPendaftaran->attributes);
//                        echo print_r($modelPendaftaran->getErrors());
//                        exit();
//                        $modelPendaftaran->save();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
                    }
                }
            }
            
            $modRiwayatKonsul = RIKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
		
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modKonsul'=>$modKonsul,
                                        'karcisTindakan'=>$karcisTindakan,
                                        'modRiwayatKonsul'=>$modRiwayatKonsul,
                                        'modAdmisi'=>$modAdmisi,
                                        'modelPendaftaran'=>$modelPendaftaran,));
	}
        
        public function actionAjaxDetailKonsul()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
            $modKonsulPoli = RIKonsulPoliT::model()->findByPk($idKonsulAntarPoli);
            $data['result'] = $this->renderPartial('_viewKonsulPoli', array('modKonsul'=>$modKonsulPoli), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
        
        public function actionAjaxBatalKonsul()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idKonsulAntarPoli = $_POST['idKonsulAntarPoli'];
            $idPendaftaran = $_POST['idPendaftaran'];
            
            RIKonsulPoliT::model()->deleteByPk($idKonsulAntarPoli);
            $modRiwayatKonsul = RIKonsulPoliT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            
            $data['result'] = $this->renderPartial('_listKonsulPoli', array('modRiwayatKonsul'=>$modRiwayatKonsul), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}