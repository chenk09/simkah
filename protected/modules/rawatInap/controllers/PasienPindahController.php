<?php

class PasienPindahController extends SBaseController
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
        
	public function actionIndex()
	{
                $this->pageTitle = Yii::app()->name." - Pasien Pindah";
                $format = new CustomFormat();
                $model = new RIPasienriyangpindahV;
                $model->tglAwal = date("d M Y").' 00:00:00';
                $model->tglAkhir = date('d M Y h:i:s');
                
                if(isset ($_REQUEST['RIPasienriyangpindahV'])){
                    $model->attributes=$_REQUEST['RIPasienriyangpindahV'];
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RIPasienriyangpindahV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RIPasienriyangpindahV']['tglAkhir']);
                    $model->ceklis = $_REQUEST['RIPasienriyangpindahV']['ceklis'];
               }
		$this->render('index',array('model'=>$model));
	}
}