<?php

class InformasiPasienPulangController extends SBaseController
{
	public function actionIndex()
	{
            $format = new CustomFormat();
            $modPasienYangPulang = new RIPasienygPulangriV;
            $modPasienYangPulang->tglAwal=date('d M Y').' 00:00:00';
            $modPasienYangPulang->tglAkhir=date('d M Y H:i:s');
            $modPasienYangPulang->ceklis=0;
            if(isset($_GET['RIPasienygPulangriV'])){
                $modPasienYangPulang->attributes=$_GET['RIPasienygPulangriV'];
                $modPasienYangPulang->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIPasienygPulangriV']['tglAwal']);
                $modPasienYangPulang->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIPasienygPulangriV']['tglAkhir']);
                $modPasienYangPulang->ceklis = $_GET['RIPasienygPulangriV']['ceklis'];
            }
            $this->render('index',array('modPasienYangPulang'=>$modPasienYangPulang));
	}
        
//        public function actionBatalPulang()
//        {
//            if(Yii::app()->request->isAjaxRequest){
//            $idOtoritas = $_POST['idOtoritas'];
//            $namaOtoritas = $_POST['namaOtoritas'];
//            $idPasienPulang=$_POST['idPasienPulang'];
//            $idPasienAdmisi=$_POST['idPasienAdmisi'];
//            $alasanPembatalan=$_POST['Alasan'];
//            
//            
//            $modPasienBatalPulang = new RIPasienBatalPulangT;    
//            $modPasienBatalPulang->namauser_otorisasi=$namaOtoritas;
//            $modPasienBatalPulang->iduser_otorisasi=$idOtoritas;
//            $modPasienBatalPulang->pasienpulang_id=$idPasienPulang;
//            $modPasienBatalPulang->tglpembatalan=date('Y-m-d H:i:s');
//             $modPasienBatalPulang->alasanpembatalan=$alasanPembatalan;
//             $transaction = Yii::app()->db->beginTransaction();
//             try{
//                if($modPasienBatalPulang->save()){
//                    $pulang = RIPasienPulangT::model()->updateByPk($idPasienPulang,array('pasienbatalpulang_id'=>$modPasienBatalPulang->pasienbatalpulang_id));
//                    $admisi = RIPasienAdmisiT::model()->updateByPk($idPasienAdmisi, array('pasienpulang_id'=>null));  
//                    if ($pulang && $admisi){
//                        $data['status'] = 'success';
//                        $transaction->commit();
//                    }
//                    else{
//                        throw new Exception("Update Data Gagal");
//                    }
//                }
//                else{
//                    Throw new Exception("Pasien Batal Pulang Gagal Disimpan");
//                }
//             }catch(Exception $ex){
//                 $transaction->rollback();
//                 $data['status'] = $ex;
//             }
//
//            echo json_encode($data);
//            Yii::app()->end();
//            }
//        }
        public function actionBatalPulang($idPendaftaran)
        {
            $this->layout='//layouts/frameDialog';
            
             $modPendaftaran    = PendaftaranT::model()->findByPk($idPendaftaran); 
             $modPasien         = PasienM::model()->findByPk($modPendaftaran->pasien_id);
             
             $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran), array('order'=>'tgladmisi DESC', 'limit'=>1));
             
             $modPasienPulang   = PasienpulangT::model()->findByAttributes(array('pasienadmisi_id'=>$modPasienAdmisi->pasienadmisi_id));  
             
             $modPasienBatalPulang  = new PasienbatalpulangT;
             $modPasienBatalPulang->create_time             = date('Y-m-d H:i:s');
             $modPasienBatalPulang->update_time             = date('Y-m-d H:i:s');
             $modPasienBatalPulang->namauser_otorisasi      = Yii::app()->user->name;;
             $modPasienBatalPulang->iduser_otorisasi        = Yii::app()->user->id;
             $modPasienBatalPulang->create_loginpemakai_id  = Yii::app()->user->id;    
             $modPasienBatalPulang->update_loginpemakai_id  = Yii::app()->user->id;
             $modPasienBatalPulang->create_ruangan          = Yii::app()->user->getState('ruangan_id');
             $modPasienBatalPulang->pasienpulang_id         = $modPasienPulang->pasienpulang_id;
             
             $jenisPenyakit         = JeniskasuspenyakitM::model()->findByPk($modPendaftaran->jeniskasuspenyakit_id);
             $modPendaftaran->jeniskasuspenyakit_nama   = $jenisPenyakit->jeniskasuspenyakit_nama;
//             digunakan untuk merefresh jika data berhasil di simpan
             $tersimpan='Tidak';
             
             if(!empty($_POST['PasienbatalpulangT'])){
                 $format = new CustomFormat();
                 $modPasienBatalPulang->attributes = $_POST['PasienbatalpulangT'];
                 $modPasienBatalPulang->tglpembatalan = $format->formatDateTimeMediumForDB($modPasienBatalPulang->tglpembatalan);
                 
                 if($modPasienBatalPulang->validate()){
                     $transaction = Yii::app()->db->beginTransaction();
                     try {
                         if($modPasienBatalPulang->save()){
                             $idPasienPulang = $modPasienBatalPulang->pasienpulang_id;
                             $idPasienAdmisi = $_POST['pasienadmisi_id'];
                             $pasienPulang = RIPasienPulangT::model()->updateByPk($idPasienPulang,array('pasienbatalpulang_id'=>$modPasienBatalPulang->pasienbatalpulang_id));
                             $pasienAdmisi = RIPasienAdmisiT::model()->updateByPk($idPasienAdmisi, array('pasienpulang_id'=>null));  
                             if($pasienAdmisi && $pasienPulang){
                                $transaction->commit();
                                PendaftaranT::model()->updateByPk($idPendaftaran,
                                    array(
										'statusfarmasi' => false,
                                        'statusperiksa'=>Params::statusPeriksa(6),
                                    )
                                );
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                $tersimpan='Ya';
                             } else {
                                 $transaction->rollback();
                                 Yii::app()->user->setFlash('error',"Data gagal disimpan");
                             }
                         }
                         else{
                             $transaction->rollback();
                             Yii::app()->user->setFlash('error',"Data gagal disimpan");
                         }
                     } catch (Exception $ex){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan", MyExceptionMessage::getMessage($exc,true));
                     }
                 }                 
                 
             }
             
             $this->render('_formBatalPulang', array('modPendaftaran'=>$modPendaftaran, 
                                                     'modPasien'=>$modPasien, 
                                                     'modPasienBatalPulang'=>$modPasienBatalPulang, 
                                                     'modPasienAdmisi'=>$modPasienAdmisi,
                                                     'tersimpan'=>$tersimpan));
        }
        

	
}