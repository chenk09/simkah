<?php

class InformasiTarifController extends SBaseController
{
        
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','detailsTarif'),
				'users'=>array('@'),
			),
		);
	}
        
	public function actionIndex()
	{
               // $idInstalasi=Yii::app()->user->getState('instalasi_id');
                $idRuangan=Yii::app()->user->getState('ruangan_id');
                $modTarifTindakanRuanganV = new RITarifTindakanPerdaRuanganV;
                $modTarifTindakanRuanganV->instalasi_id=$idInstalasi;
                $modTarifTindakanRuanganV->ruangan_id=$idRuangan;
                
                if(isset($_GET['RITarifTindakanPerdaRuanganV'])){
                    $modTarifTindakanRuanganV->attributes=$_GET['RITarifTindakanPerdaRuanganV'];
                   
                }
		$this->render('index',array('modTarifTindakanRuanganV'=>$modTarifTindakanRuanganV));
	}
        
       public function actionDetailsTarif($idKelasPelayanan,$idDaftarTindakan, $idKategoriTindakan){
            
            $this->layout='//layouts/frameDialog';
            $modTarifTindakanform = RITarifTindakanPerdaRuanganV;
            if($idKelasPelayanan!=''){
            $modTarifTindakan= RITariftindakanM::model()->with('komponentarif')->findAll('kelaspelayanan_id='.$idKelasPelayanan.' AND 
                                                               daftartindakan_id='.$idDaftarTindakan.'
                                                               AND t.komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL.'');
            }else{ 
                $modTarifTindakan=RITariftindakanM::model()->with('komponentarif')->findAll('daftartindakan_id='.$idDaftarTindakan.'
                                                               AND t.komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL.'
                                                               AND kelaspelayanan_id isNull');
            }
            $modTarif = TariftindakanperdaruanganV::model()->find('daftartindakan_id = '.$idDaftarTindakan.' and kelaspelayanan_id = '.$idKelasPelayanan.' and kategoritindakan_id = '.$idKategoriTindakan);
            $jumlahTarifTindakan=COUNT($modTarifTindakan);
            
            $this->render('detailsTarif',array(
                                                'modTarif'=>$modTarif,
                                                'modTarifTindakan'=>$modTarifTindakan,
                                                'modTarifTindakanform'=>$modTarifTindakanform,
                                                'jumlahTarifTindakan'=>$jumlahTarifTindakan));
            
            
        }

}