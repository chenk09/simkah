<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ActionAjaxController extends SBaseController
{
       public function actionCekHakAkses()
        {
            if(!Yii::app()->user->checkAccess('Pembatalan Pulang')){
                //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
                $data['cekAkses'] = false;
            } else {
                //echo 'punya hak akses';
                $data['cekAkses'] = true;
                $data['userid'] = Yii::app()->user->id;
                $data['username'] = Yii::app()->user->name;
            }
            
            echo CJSON::encode($data);
            Yii::app()->end();
        }
        
     public function actiondataPasien()
        {
            $idPasien=$_POST['idPasien'];
            $idPendaftaran=$_POST['idPendaftaran'];
            $modPasien = RIPasienM::model()->findByPk($idPasien);
            $modPendaftaran = RIPendaftaranT::model()->findByPk($idPendaftaran);
            $form=$this->renderPartial('/_ringkasDataPasien', array('modPasien'=>$modPasien,
                                                                           'modPendaftaran'=>$modPendaftaran,
                                                                               ), true);
            $data['form']=$form;
                       echo CJSON::encode($data);

        }  

    public function actionLoadFormDiagnosis()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            $idDiagnosa = $_POST['idDiagnosa'];
            $idKelDiagnosa = $_POST['idKelDiagnosa'];
            $tglDiagnosa = $_POST['tglDiagnosa'];
            
            $modDiagnosaicdixM = DiagnosaicdixM::model()->findAll();
            $modSebabDiagnosa = SebabdiagnosaM::model()->findAll();
            $modDiagnosa = DiagnosaM::model()->findByPk($idDiagnosa);
            
            echo CJSON::encode(array(
                'status'=>'create_form', 
                'form'=>$this->renderPartial('/diagnosaTRI/_formLoadDiagnosis', array('modDiagnosa'=>$modDiagnosa,
                'idKelDiagnosa'=>$idKelDiagnosa,
                'modDiagnosaicdixM'=>$modDiagnosaicdixM,
                'modSebabDiagnosa'=>$modSebabDiagnosa,
               'tglDiagnosa'=>$tglDiagnosa), true)));
            exit;               
        }
    }  

    public function actionloadDataPasien()
    {
        if(Yii::app()->request->isAjaxRequest){
            $data = RIInfokunjunganriV::model()->findByAttributes(array('no_rekam_medik'=>$_POST['no_rekam_medik']));
            $post = array(
                'tgl_pendaftaran'=>$data->tgl_pendaftaran,
                'no_pendaftaran'=>$data->no_pendaftaran,
                'umur'=>$data->umur,
                'jeniskasuspenyakit_nama'=>$data->jeniskasuspenyakit_nama,
                'instalasi_nama' => $data->instalasi_nama,
                'ruangan_nama'=>$data->ruangan_nama,
                'pendaftaran_id'=>$data->pendaftaran_id,
                'pasien_id'=>$data->pasien_id,
                'jeniskelamin'=>$data->jeniskelamin,
                'statusperkawinan'=>$data->statusperkawinan,
                'nama_pasien'=>$data->nama_pasien,
                'nama_bin'=>$data->nama_bin,
            );
            echo CJSON::encode($post);
            Yii::app()->end();
        }
    }

    public function actionLoadTindakanKomponenPasien()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $idPendaftaran = $_POST['idPendaftaran'];
                
                $tindakans = TindakanpelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,));
                foreach($tindakans as $i=>$tindakan){
                    $returnVal[$tindakan->tindakanpelayanan_id]['daftartindakan_id'] = $tindakan->daftartindakan_id;
                    $returnVal[$tindakan->tindakanpelayanan_id]['daftartindakan_nama'] = $tindakan->daftartindakan->daftartindakan_nama;
                    $komponens = TindakankomponenT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                    foreach($komponens as $j=>$komponen){
                        $tindKomponenId = $komponen->tindakankomponen_id;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tindakankomponen_id'] = $tindKomponenId;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['komponentarif_id'] = $komponen->komponentarif_id;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['komponentarif_nama'] = $komponen->komponentarif->komponentarif_nama;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarif_kompsatuan'] = $komponen->tarif_kompsatuan;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarif_tindakankomp'] = $komponen->tarif_tindakankomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['tarifcyto_tindakankomp'] = $komponen->tarifcyto_tindakankomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidiasuransikomp'] = $komponen->subsidiasuransikomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidipemerintahkomp'] = $komponen->subsidipemerintahkomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['subsidirumahsakitkomp'] = $komponen->subsidirumahsakitkomp;
                        $returnVal[$tindakan->tindakanpelayanan_id][$tindKomponenId]['iurbiayakomp'] = $komponen->iurbiayakomp;
                    }
                }
                
                $form = $this->renderPartial('_formPembebasanTarif', array('data'=>$returnVal), true);
                $returnVal['tabelPembebasanTarif'] = $form;
                
                echo CJSON::encode($returnVal);
            }
        }
}

?>
