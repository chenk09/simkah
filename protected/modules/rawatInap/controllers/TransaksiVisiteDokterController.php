<?php

class TransaksiVisiteDokterController extends SBaseController
{
        public $succesSave=false;
        
	public function actionIndex()
	{
            $format = new CustomFormat();
            $model = new RIInfopasienmasukkamarV;
            // $model = new RIPasienrawatinapV;
                
            if(isset($_POST['tangalVisite']))
            {
               $jumlahPasien=count($_POST['RITindakanPelayananT']['pasien_id']);
               $jumlahCeklist=0;
               $jumlahTersimpan=0;
//               echo '<pre>'.print_r($_POST['RITindakanPelayananT'],1).'</pre>';
//               echo $jumlahPasien;exit;
               $transaction = Yii::app()->db->beginTransaction();
                try {
                   for($i=0; $i<$jumlahPasien; $i++){
                         if($_POST['RITindakanPelayananT']['dipilih'][$i]=='Ya'){//Jika Diceklist   
                            $jumlahCeklist++; 
                            $modTindakans = new RITindakanPelayananT;
                            $modTindakans->penjamin_id = $_POST['RITindakanPelayananT']['penjamin_id'][$i];
                            $modTindakans->pasienadmisi_id = $_POST['RITindakanPelayananT']['pasienadmisi_id'][$i];
                            $modTindakans->pasien_id = $_POST['RITindakanPelayananT']['pasien_id'][$i];
                            $modTindakans->kelaspelayanan_id = $_POST['RITindakanPelayananT']['kelaspelayanan_id'][$i];
                            $modTindakans->instalasi_id = Yii::app()->user->getState('instalasi_id');
                            $modTindakans->pendaftaran_id = $_POST['RITindakanPelayananT']['pendaftaran_id'][$i];
                            $modTindakans->shift_id = Yii::app()->user->getState('shift_id');
                            $modTindakans->daftartindakan_id = $_POST['RITindakanPelayananT']['daftartindakan_id'][$i];
                            $modTindakans->carabayar_id = $_POST['RITindakanPelayananT']['carabayar_id'][$i];
                            $modTindakans->jeniskasuspenyakit_id = $_POST['RITindakanPelayananT']['jeniskasuspenyakit_id'][$i];
                            $modTindakans->tgl_tindakan = $format->formatDateTimeMediumForDB(trim($_POST['tangalVisite']));
                            $modTindakans->dokterpemeriksa1_id = $_POST['RITindakanPelayananT']['pegawai_id'][$i];
                            $modTindakans->ruangan_id = Yii::app()->user->getState('ruangan_id');

                            $modTindakans->satuantindakan=Params::SATUAN_TINDAKAN_VISITE;//'KALI';
                            $modTindakans->qty_tindakan=1;
                            $modTindakans->cyto_tindakan=0;
                            $modTindakans->tarifcyto_tindakan = 0;
                            $modTindakans->discount_tindakan = 0;
                            $modTindakans->subsidiasuransi_tindakan = 0;
                            $modTindakans->subsidipemerintah_tindakan = 0;
                            $modTindakans->subsisidirumahsakit_tindakan = 0;
                            $modTindakans->iurbiaya_tindakan = 0;

                            $tarifTindakan= RITarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['RITindakanPelayananT']['daftartindakan_id'][$i].' AND kelaspelayanan_id='.$_POST['RITindakanPelayananT']['kelaspelayanan_id'][$i].'');
                            foreach($tarifTindakan AS $dataTarif):
                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                                    $modTindakans->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                                }
                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                                    $modTindakans->tarif_medis=$dataTarif['harga_tariftindakan'];
                                }
                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                                    $modTindakans->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                                }
                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                                    $modTindakans->tarif_bhp=$dataTarif['harga_tariftindakan'];
                                }

                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_TOTAL){
                                    $modTindakans->tarif_satuan=$dataTarif['harga_tariftindakan'];
                                    $modTindakans->tarif_tindakan=$dataTarif['harga_tariftindakan'];
                                }

                            endforeach;

                            if($modTindakans->save()){
                                $jumlahTersimpan++;
                                $criteria = new CDbCriteria;
                                $criteria->compare('daftartindakan_id', $_POST['RITindakanPelayananT']['daftartindakan_id'][$i]);
                                $criteria->compare('kelaspelayanan_id', $_POST['RITindakanPelayananT']['kelaspelayanan_id'][$i]);
                                //HARUSNYA KOMPONEN TARIF MASUK?? >> $criteria->addCondition('komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL);
                                $modTarifs = RITarifTindakanM::model()->findAll($criteria);
                                //$modTarifs = RITarifTindakanM::model()->findAll('daftartindakan_id='.$_POST['RITindakanPelayananT']['daftartindakan_id'][$i].' AND kelaspelayanan_id='.$_POST['RITindakanPelayananT']['kelaspelayanan_id'][$i].' AND komponentarif_id!='.Params::KOMPONENTARIF_ID_TOTAL.'');
                                foreach ($modTarifs as $tarif):
                                    $modTindakanKomponen = new RITindakanKomponenT;
                                    $modTindakanKomponen->tindakanpelayanan_id = $modTindakans->tindakanpelayanan_id;
                                    $modTindakanKomponen->komponentarif_id = $tarif['komponentarif_id'];
                                    $modTindakanKomponen->tarif_kompsatuan = $tarif['harga_tariftindakan'];
                                    $modTindakanKomponen->tarif_tindakankomp = $modTindakanKomponen->tarif_kompsatuan * $modTindakans->qty_tindakan;
                                    $modTindakanKomponen->tarifcyto_tindakankomp = 0;
                                    $modTindakanKomponen->subsidiasuransikomp = $modTindakans->subsidiasuransi_tindakan;
                                    $modTindakanKomponen->subsidipemerintahkomp = $modTindakans->subsidipemerintah_tindakan;
                                    $modTindakanKomponen->subsidirumahsakitkomp = $modTindakans->subsisidirumahsakit_tindakan;
                                    $modTindakanKomponen->iurbiayakomp = $modTindakans->iurbiaya_tindakan;
                                    $modTindakanKomponen->save();
                                endforeach;
                            }
                          }
                    }
                    
                    if($jumlahCeklist==$jumlahTersimpan){
                       $transaction->commit();
                       Yii::app()->user->setFlash('success',"Data Berhasil disimpan ");
                       $this->redirect(Yii::app()->createUrl($this->route));
                    } else {
                       $transaction->rollback();
                       Yii::app()->user->setFlash('error',"Data gagal disimpan ".'<pre>'.print_r($modTindakans->getErrors(),1).'</pre>');
                    }

                }catch(Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                } 
            }
           $this->render('index',array('model'=>$model));

	}
        
        public function saveTindakan()
        {
            $format = new CustomFormat();
            $post = $_POST['RITindakanPelayanan'];
            $valid=true; //echo $_POST['RJTindakanPelayananT'][0]['tipepaket_id'];exit;
            foreach($post as $i=>$item)
            {
                
                if(!empty($item)){
                    if($item['ceklist']=='1'){
                        $modTindakans[$i] = new RITindakanPelayananT;
                        $modTindakans[$i]->attributes=$item;
                        $modTindakans[$i]->penjamin_id = $item['penjamin_id'];
                        $modTindakans[$i]->pasienadmisi_id = $item['pasienadmisi_id'];
                        $modTindakans[$i]->pasien_id = $item['pasien_id'];
                        $modTindakans[$i]->kelaspelayanan_id = $item['kelaspelayanan_id'];
                        $modTindakans[$i]->instalasi_id = Yii::app()->user->getState('instalasi_id');
                        $modTindakans[$i]->pendaftaran_id = $item['pendaftaran_id'];
                        $modTindakans[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modTindakans[$i]->daftartindakan_id = $items['daftartindakan_id'];
                        $modTindakans[$i]->carabayar_id = $item['carabayar_id'];
                        $modTindakans[$i]->jeniskasuspenyakit_id = $item['jeniskasuspenyakit_id'];
                        $modTindakans[$i]->tgl_tindakan = $format->formatDateTimeMediumForDB(trim($_POST['tangalVisite']));
                        $modTindakans[$i]->dokterpemeriksa1_id = $item['pegawai_id'];
                        
                        $modTindakans[$i]->tarifcyto_tindakan = 0;
                        $modTindakans[$i]->discount_tindakan = 0;
                        $modTindakans[$i]->subsidiasuransi_tindakan = 0;
                        $modTindakans[$i]->subsidipemerintah_tindakan = 0;
                        $modTindakans[$i]->subsisidirumahsakit_tindakan = 0;
                        $modTindakans[$i]->iurbiaya_tindakan = 0;
                        
                        $tarifTindakan= RITarifTindakanM::model()->findAll('daftartindakan_id='.$item['daftartindakan_id'].' AND kelaspelayanan_id='.$modTindakans[$i]->kelaspelayanan_id.'');
                        foreach($tarifTindakan AS $dataTarif):
                             if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_RS){
                                    $modTindakans[$i]->tarif_rsakomodasi=$dataTarif['harga_tariftindakan'];
                                }
                                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_MEDIS){
                                    $modTindakans[$i]->tarif_medis=$dataTarif['harga_tariftindakan'];
                                }
                                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_PARAMEDIS){
                                    $modTindakans[$i]->tarif_paramedis=$dataTarif['harga_tariftindakan'];
                                }
                                 if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_BHP){
                                    $modTindakans[$i]->tarif_bhp=$dataTarif['harga_tariftindakan'];
                                }

                                if($dataTarif['komponentarif_id']==Params::KOMPONENTARIF_ID_TOTAL){
                                    $modTindakans[$i]->tarif_tindakan=$dataTarif['harga_tariftindakan'];
                                }

                        endforeach;
                        
//                        echo "pengecekan".$modTindakans[$i]->pasien_id;exit;
                        $valid = $modTindakans[$i]->validate() && $valid;
                    }   
                }
            }

            $transaction = Yii::app()->db->beginTransaction();
            try {
                if($valid){
                    foreach($modTindakans as $i=>$tindakan){
                        $tindakan->save();
                        $statusSaveKomponen = $this->saveTindakanKomponen($tindakan);
                       
                    }
                    if($statusSaveKomponen) {
                        $transaction->commit();
                        $this->succesSave = true;
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid  1");
                    }
                } else {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data tidak valid 2");
                }
            } catch (Exception $exc) {
                $transaction->rollback();
                Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
            }
            
            return $modTindakans;
        }
        
        public function saveTindakanKomponen($tindakan)
        {   
            $valid = true;
            $criteria = new CDbCriteria();
            $criteria->addCondition('komponentarif_id !='.Params::KOMPONENTARIF_ID_TOTAL);
            
            
            return $valid;
        }

	
}