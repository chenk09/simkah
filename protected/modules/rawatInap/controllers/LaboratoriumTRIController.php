<?php
//Yii::import('rawatJalan.controllers.LaboratoriumController');
//Yii::import('rawatJalan.models.*');
//class LaboratoriumTRIController extends LaboratoriumController
//{
//        
//}
class LaboratoriumTRIController extends SBaseController
{
        protected $statusSaveKirimkeUnitLain = false;
        protected $statusSavePermintaanPenunjang = false;

        public function actionIndex($idPendaftaran,$idAdmisi)
	{
            $modAdmisi = (!empty($idAdmisi)) ? PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienadmisi_id'=>$idAdmisi)) : array();
            $modPendaftaran = RIPendaftaranT::model()->with('kasuspenyakit')->findByPk($idPendaftaran);
            $modPasien = RIPasienM::model()->findByPk($modPendaftaran->pasien_id);
            $modKirimKeUnitLain = new RIPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->tgl_kirimpasien = date('Y-m-d H:i:s');
            // $modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
            $modJenisPeriksaLab = RIJenisPemeriksaanLabM::model()->findAllByAttributes(array('jenispemeriksaanlab_aktif'=>true),array('order'=>'jenispemeriksaanlab_urutan'));
            $modPeriksaLab = RIPemeriksaanLabM::model()->findAllByAttributes(array('pemeriksaanlab_aktif'=>true),array('order'=>'pemeriksaanlab_id, pemeriksaanlab_urutan'));
                        
            if(isset($_POST['RIPasienKirimKeUnitLainT'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modKirimKeUnitLain = $this->savePasienKirimKeUnitLain($modAdmisi);
                    if(isset($_POST['permintaanPenunjang'])){
                        $this->savePermintaanPenunjang($_POST['permintaanPenunjang'],$modKirimKeUnitLain);
                    } else {
                        $this->statusSavePermintaanPenunjang = true;
                    }
                    
                    if($this->statusSaveKirimkeUnitLain && $this->statusSavePermintaanPenunjang){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil disimpan");
                        $this->refresh();
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data tidak valid ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $modRiwayatKirimKeUnitLain = RIPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
                                                                                                'pasienmasukpenunjang_id IS NULL');
		
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modKirimKeUnitLain'=>$modKirimKeUnitLain,
                                        'modJenisPeriksaLab'=>$modJenisPeriksaLab,
                                        'modPeriksaLab'=>$modPeriksaLab,
                                        'modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain,
                                        'modAdmisi'=>$modAdmisi,
                                        ));
	}

        protected function savePasienKirimKeUnitLain($modAdmisi)
        {
            $modKirimKeUnitLain = new RIPasienKirimKeUnitLainT;
            $modKirimKeUnitLain->attributes = $_POST['RIPasienKirimKeUnitLainT'];
            $modKirimKeUnitLain->pasien_id = $modAdmisi->pasien_id;
            $modKirimKeUnitLain->pendaftaran_id = $modAdmisi->pendaftaran_id;
            //$modKirimKeUnitLain->pegawai_id = $modPendaftaran->pegawai_id;
//            $modKirimKeUnitLain->kelaspelayanan_id = $modAdmisi->kelaspelayanan_id;
            $modKirimKeUnitLain->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas'); //Semua pasien lab & rad tanpa kelas
            $modKirimKeUnitLain->instalasi_id = Params::INSTALASI_ID_LAB;
            $modKirimKeUnitLain->ruangan_id = Params::RUANGAN_ID_LAB;
            $modKirimKeUnitLain->nourut = Generator::noUrutPasienKirimKeUnitLain($modKirimKeUnitLain->ruangan_id);
            if($modKirimKeUnitLain->validate()){
                $modKirimKeUnitLain->save();
                $this->statusSaveKirimkeUnitLain = true;
            }
            
            return $modKirimKeUnitLain;
        }
        
        protected function savePermintaanPenunjang($permintaan,$modKirimKeUnitLain)
        {
            foreach ($permintaan['inputpemeriksaanlab'] as $i => $value) {
                $modPermintaan = new RIPermintaanPenunjangT;
                $modPermintaan->daftartindakan_id = '';     //$permintaan['idDaftarTindakan'][$i];
                $modPermintaan->pemeriksaanlab_id = $permintaan['inputpemeriksaanlab'][$i];
                $modPermintaan->pemeriksaanrad_id = '';
                $modPermintaan->pasienkirimkeunitlain_id = $modKirimKeUnitLain->pasienkirimkeunitlain_id;
                $modPermintaan->noperminatanpenujang = Generator::noPermintaanPenunjang('PL');
                $modPermintaan->qtypermintaan = $permintaan['inputqty'][$i];
                $modPermintaan->tglpermintaankepenunjang = $modKirimKeUnitLain->tgl_kirimpasien; //date('Y-m-d H:i:s');
                if($modPermintaan->validate()){
                    $modPermintaan->save();
                    $this->statusSavePermintaanPenunjang = true;
                }
            }
        }
        
        public function actionAjaxBatalKirim()
        {
            if(Yii::app()->request->isAjaxRequest) {
            $idPasienKirimKeUnitLain = $_POST['idPasienKirimKeUnitLain'];
            $idPendaftaran = $_POST['idPendaftaran'];
            
            PermintaankepenunjangT::model()->deleteAllByAttributes(array('pasienkirimkeunitlain_id'=>$idPasienKirimKeUnitLain));
            PasienkirimkeunitlainT::model()->deleteByPk($idPasienKirimKeUnitLain);
            $modRiwayatKirimKeUnitLain = RIPasienKirimKeUnitLainT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran,
                                                                                                      'ruangan_id'=>Params::RUANGAN_ID_LAB),
                                                                                                'pasienmasukpenunjang_id IS NULL');
            
            $data['result'] = $this->renderPartial('_listKirimKeUnitLain', array('modRiwayatKirimKeUnitLain'=>$modRiwayatKirimKeUnitLain), true);

            echo json_encode($data);
             Yii::app()->end();
            }
        }
}
