
<?php

class RinciantagihanpasienriVController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
        
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'rincian'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print','rincianKasirBaruPrint'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}


	public function actionIndex()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model = new RIInfopasienmasukkamarV('search');
                $model->tglAwal = date('d M Y H:i:s');
                $model->tglAkhir = date('d M Y H:i:s');
//		$model->unsetAttributes();  // clear any default values
//                $model->ceklis = true;
		if(isset($_GET['RIInfopasienmasukkamarV'])){
			$model->attributes=$_GET['RIInfopasienmasukkamarV'];
//                        $model->ceklis = true;
                        $format = new CustomFormat;
                        $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render('index',array(
			'model'=>$model,
		));
	}
        
        public function actionRincian($id){
            Yii::import('billingKasir.models.BKPendaftaranT');
            Yii::import('billingKasir.models.BKRinciantagihanpasienV');
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya Sementara';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $criteria = new CDbCriteria();
            $criteria->addCondition('pendaftaran_id = '.$id);
            $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
            $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
            $criteria->limit = -1; //unlimited
            $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria);
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $this->render('rawatJalan.views.rinciantagihanpasienV.rincianSementara', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
        }
        
        public function actionRincianOld($id){
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Tagihan Pasien';
            $modPendaftaran = PendaftaranT::model()->findByPk($id);
            $modAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
//            $modPendaftaran->tgl_admisi = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPendaftaran->tgl_admisi, 'yyyy-MM-dd hh:mm:ss'));
            $modRincian = RIRinciantagihanpasienriV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//            $modRincian->pendaftaran_id = $id;
            $this->render('rincian', array('modPendaftaran'=>$modPendaftaran, 'modAdmisi'=>$modAdmisi, 'modRincian'=>$modRincian, 'data'=>$data));
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=RIRinciantagihanpasienriV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        public function actionRincianKasirBaruPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya  BELUM LUNAS';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $criteria->order = 'ruangan_id';
                $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $modPendaftaran = PendaftaranT::model()->findByPk($id);

                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                );

                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'kwitansi';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF(
                        '',
                        $ukuranKertasPDF, //format A4 Or
                        11, //Font SIZE
                        '', //default font family
                        3, //15 margin_left
                        3, //15 margin right
                        25, //16 margin top
                        10, // margin bottom
                        0, // 9 margin header
                        0, // 9 margin footer
                        'P' // L - landscape, P - portrait
                        );    
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    //$mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianBaruPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rjrinciantagihanpasien-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            $modAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
            $modRincian = RIRinciantagihanpasienriV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $data['judulLaporan']='Data Rincian Tagihan Pasien';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('rincian', array('modAdmisi'=>$modAdmisi, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('rincian',array('modAdmisi'=>$modAdmisi, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('rincian',array('modAdmisi'=>$modAdmisi, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
