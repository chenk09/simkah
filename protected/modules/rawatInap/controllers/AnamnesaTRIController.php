<?php
Yii::import('rawatInap.controllers.PasienRawatInapController'); //Untuk menggunakan function saveAkomodasi()
//Yii::import('rawatJalan.controllers.AnamnesaController');
//Yii::import('rawatJalan.models.*');
//class AnamnesaTRIController extends AnamnesaController
//{
//        public $pathView = 'rawatInap.views.anamnesa.';
//        
//}

class AnamnesaTRIController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        //untuk saveAkomodasi
        public $successSaveTindakanKomponen = true;
        public $successSaveTindakan;

	/**
	 * @return array action filters
	 */
//	FILTER SUDAH MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}
//
//	/**
//	 * Specifies the access control rules.
//	 * This method is used by the 'accessControl' filter.
//	 * @return array access control rules
//	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

	
	public function actionIndex($idPendaftaran,$idAdmisi)
	{
            $format = new CustomFormat();
            $modAdmisi = (!empty($idAdmisi)) ? PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienadmisi_id'=>$idAdmisi)) : array();
            $modPendaftaran=RIPendaftaranT::model()->findByPk($idPendaftaran);
            
            $modPasien = RIPasienM::model()->findByPk($modPendaftaran->pasien_id);
            
            $dataPendaftaran = RIPendaftaranT::model()->findByAttributes(array('pasien_id'=>$modPasien->pasien_id), array('order'=>'tgl_pendaftaran DESC'));
            $modPasienRIV = RIInfopasienmasukkamarV::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $modMasukKamar = RIMasukKamarT::model()->findByPk($modPasienRIV->masukkamar_id);
            $selisihHari = MyFunction::hitungHari($format->formatDateTimeMediumForDB($modMasukKamar->tglmasukkamar));
            $konfigSystem = KonfigsystemK::model()->find();
            if($konfigSystem->akomodasiotomatis == true){
                if(PasienRawatInapController::cekAkomodasiHariIni($modPendaftaran, $modAdmisi, $modMasukKamar)){
                    $transaction_ako = Yii::app()->db->beginTransaction();
                    try {
                        if(PasienRawatInapController::saveAkomodasi($modPendaftaran, $modAdmisi, $selisihHari)){
                            $transaction_ako->commit();
                            Yii::app()->user->setFlash('success',"Akomodasi berhasil disimpan");
                        }else{
                            $transaction_ako->rollback();
                            Yii::app()->user->setFlash('error',"Akomodasi gagal disimpan");
                        }
                    }catch (Exception $exc) {
                        $transaction_ako->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
            } 
            //print_r($lastPendaftaran);
            //echo $modPasien->pasien_id;exit();
            //EHJ-2969
//            $i = 1;
//            if (count($dataPendaftaran) > 1){
//                foreach ($dataPendaftaran as $row){
//                    if ($i == 2){
//                        $lastPendaftaran = $row->pendaftaran_id;
//                    }
//                    $i++;
//                }
//            }else{
//                $lastPendaftaran = $idPendaftaran;
//            }

            
            $cekAnamnesa=RIAnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran));
            $modDiagnosa = new RIDiagnosaM;
           
            if(COUNT($cekAnamnesa)>0) {  //Jika Pasien Sudah Melakukan Anamnesa Sebelumnya
                $modAnamnesa=$cekAnamnesa;
                //$modAnamnesa->riwayatimunisasi = $modPendaftaran->statuspasien;
            } else {  
                ////Jika Pasien Belum Pernah melakukan Anamnesa
                $modAnamnesa=new RIAnamnesaT;
                $modAnamnesa->pegawai_id=$modPendaftaran->pegawai_id;
                $modAnamnesa->pendaftaran_id=$modPendaftaran->pendaftaran_id;
                $modAnamnesa->pasien_id=$modPendaftaran->pasien_id;
                $modAnamnesa->tglanamnesis=date('Y-m-d H:i:s');
                //$isPasien = RIPendaftaranT::model()->findByPk($idPendaftaran)->statuspasien;
//                $sql = "SELECT c(diagnosa_id) FROM pasienimunisasi_t WHERE pendaftaran_id = $idPendaftaran";
//                $stoks = Yii::app()->db->createCommand($sql)->queryAll();
                
            }
            
            if ($modPendaftaran->statuspasien == "PENGUNJUNG LAMA"){
                $modDiagnosaTerdahulu = RIPasienMorbiditasT::model()->with('diagnosa')->findAllByAttributes(array('pasien_id'=>$modPasien->pasien_id, 'pendaftaran_id'=>$dataPendaftaran->pendaftaran_id));
                
                $hasilImunisasi = array();
                $hasilDiagnosaDahulu = array();
                foreach($modDiagnosaTerdahulu as $row){
                    if ($row->diagnosa->diagnosa_imunisasi == true)
                        $hasilImunisasi[] = $row->diagnosa->diagnosa_nama;
                    else
                        $hasilDiagnosaDahulu[] = $row->diagnosa->diagnosa_nama;
                }
                if (empty($modAnamnesa->riwayatimunisais)){
                    $modAnamnesa->riwayatimunisasi = implode(', ',$hasilImunisasi);
                }
                if (empty($modAnamnesa->riwayatpenyakitterdahulu)){
                    $modAnamnesa->riwayatpenyakitterdahulu = implode(', ',$hasilDiagnosaDahulu);
                }
            }
            
            //echo $modAnamnesa->riwayatpenyakitterdahulu;exit();
            if(isset($_POST['RIAnamnesaT'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modAnamnesa->attributes=$_POST['RIAnamnesaT'];
                    $modAnamnesa->keluhanutama = (isset($_POST['RIAnamnesaT']['keluhanutama'])>0) ? implode(', ', $_POST['RIAnamnesaT']['keluhanutama']) : '';
                    $modAnamnesa->keluhantambahan = (isset($_POST['RIAnamnesaT']['keluhantambahan'])> 0) ? implode(', ', $_POST['RIAnamnesaT']['keluhantambahan']) : '';
                    $modAnamnesa->save();
                    $updateStatusPeriksa=PendaftaranT::model()->updateByPk($idPendaftaran,array('statusperiksa'=>Params::statusPeriksa(2)));
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data Anamnesa berhasil disimpan");
                    $this->refresh();
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
                
            $modAnamnesa->tglanamnesis = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($modAnamnesa->tglanamnesis, 'yyyy-MM-dd hh:mm:ss'));    
            
            $modDataDiagnosa = new RIDiagnosaM('search');
            $modDataDiagnosa->unsetAttributes();
            if(isset($_GET['RIDiagnosaM']))
                $modDataDiagnosa->attributes = $_GET['RIDiagnosaM'];
            
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,
                        'modAnamnesa'=>$modAnamnesa, 'modDiagnosa'=>$modDiagnosa, 'modDataDiagnosa'=>$modDataDiagnosa,
                        'modAdmisi'=>$modAdmisi,
		));
	}
        
        
}
