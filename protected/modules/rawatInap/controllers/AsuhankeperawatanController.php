<?php

class AsuhankeperawatanController extends SBaseController
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column1';
        public $defaultAction = 'index';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('index','view','dialogDiagnosaKeperawatan', 'getRiwayatPasien', 'detailDiagnosaKeperawatan', 'detailRencanaKeperawatan', 'detailEvaluasiKeperawatan', 'detailPlanningKeperawatan'),
                'users'=>array('@'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array('create','update','print'),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete','RemoveTemporary'),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=new RIAsuhankeperawatanT;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['RIAsuhankeperawatanT']))
        {
            
            if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('view','id'=>$model->asuhankeperawatan_id));
                        }
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['RIAsuhankeperawatanT']))
        {
            $model->attributes=$_POST['RIAsuhankeperawatanT'];
            if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                $this->redirect(array('view','id'=>$model->asuhankeperawatan_id));
                        }
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        if(Yii::app()->request->isPostRequest)
        {
            // we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $this->loadModel($id)->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if(!isset($_GET['ajax']))
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
        }
        else
            throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=new RIAsuhankeperawatanT;
        $modPasien = new RIInfopasienmasukkamarV();
        // $modPasien = new RIInfokunjunganriV();
        $modAnamnesa = new RIAnamnesaT();
        $modPeriksaFisik = new RIPemeriksaanFisikT();
        $modPasienRIV = new RIPasienRawatInapV();
        $model->tglaskep = date('Y-m-d H:i:s');
        
        $modPasienRIV->unsetAttributes();  
        if(isset($_GET['RIPasienrawatinapV'])){
            $modPasienRIV->attributes = $_GET['RIPasienrawatinapV'];
        }
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['RIAsuhankeperawatanT']))
        {
            $model->attributes=$_POST['RIAsuhankeperawatanT'];
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $model->shift_id = Yii::app()->user->getState('shift_id');
            $model->create_time = date('Y-m-d H:i:s');
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');

            $transaction = Yii::app()->db->beginTransaction();
            try{
                $success = $successIntervensi = $successPlanning = $successImplementasi = true;
                $jumlahAskep = $jumlahIntervensi = $jumlahPlanning = $jumlahImplementasi = 0;
                $jumlah = count($_POST['AsuhankeperawatanT']['diagnosakeperawatan_id']);
                $data = AsuhankeperawatanT::model()->findAllByAttributes(array('pendaftaran_id'=>$model->pendaftaran_id));
                if (count($data) > 0){
                    foreach($data as $data){
                        RIIntervensiaskepT::model()->deleteAllByAttributes(array('asuhankeperawatan_id'=>$data->asuhankeperawatan_id));
                        RIImplementasiaskepT::model()->deleteAllByAttributes(array('asuhankeperawatan_id'=>$data->asuhankeperawatan_id));
                        RIPlaningaskepT::model()->deleteAllByAttributes(array('asuhankeperawatan_id'=>$data->asuhankeperawatan_id));
                        RIAsuhankeperawatanT::model()->deleteByPk($data->asuhankeperawatan_id);
                    }
                }

                for($i =0; $jumlah > $i; $i++){
                    $modAsuhanKeperawatan[$i] = new RIAsuhankeperawatanT();
                    $modAsuhanKeperawatan[$i]->attributes = $model->attributes;
                    $modAsuhanKeperawatan[$i]->diagnosakeperawatan_id = $_POST['AsuhankeperawatanT']['diagnosakeperawatan_id'][$i];
                    $modAsuhanKeperawatan[$i]->diagnosa_id = $_POST['AsuhankeperawatanT']['diagnosa_id'][$i];
                    $modAsuhanKeperawatan[$i]->tglassesment = $model->tglaskep;
                    $modAsuhanKeperawatan[$i]->evaluasi_objektif = $_POST['AsuhankeperawatanT']['evaluasi_objektif'][$i];
                    $modAsuhanKeperawatan[$i]->evaluasi_subjektif = $_POST['AsuhankeperawatanT']['evaluasi_subjektif'][$i];
                    $modAsuhanKeperawatan[$i]->evaluasi_assesment = $_POST['AsuhankeperawatanT']['evaluasi_assesment'][$i];
                    $modAsuhanKeperawatan[$i]->askep_tujuan = $_POST['AsuhankeperawatanT']['askep_tujuan'][$i];
                    $modAsuhanKeperawatan[$i]->askep_kriteriahasil = $_POST['AsuhankeperawatanT']['askep_kriteriahasil'][$i];

                    if ($modAsuhanKeperawatan[$i]->validate()){
                        if ($modAsuhanKeperawatan[$i]->save()){
                            $success = true;
                            $jumlahAskep++;
                            $jumlahDipilihRencanaIntervensi = count($_POST['rencana_intervensi'][$i]);
                            for($b = 0; $jumlahDipilihRencanaIntervensi > $b; $b++){
                                $modRencana = RIRencanakeperawatanM::model()->findByPk($_POST['rencana_intervensi'][$i][$b]);
                                $modIntervensiAskep[$i][$b] = new RIIntervensiaskepT;
                                $modIntervensiAskep[$i][$b]->rencanakeperawatan_id = $_POST['rencana_intervensi'][$i][$b];
                                $modIntervensiAskep[$i][$b]->asuhankeperawatan_id = $modAsuhanKeperawatan[$i]->asuhankeperawatan_id;
                                $modIntervensiAskep[$i][$b]->tglmulaiintervensi = $modAsuhanKeperawatan[$i]->tglaskep;
                                $modIntervensiAskep[$i][$b]->intervensi_kode = $modRencana->rencana_kode;
                                $modIntervensiAskep[$i][$b]->intervensi_nama = $modRencana->rencana_intervensi;
                                $modIntervensiAskep[$i][$b]->intervensi_rasionalisasi = $modRencana->rencana_rasionalisasi;
                                $modIntervensiAskep[$i][$b]->iskolaborasi = $modRencana->iskolaborasiintervensi;
                                $modIntervensiAskep[$i][$b]->lama_waktu_jam = 0;
                                if ($modIntervensiAskep[$i][$b]->validate()){
                                    if ($modIntervensiAskep[$i][$b]->save()){
                                        $successIntervensi = true;
                                        $jumlahIntervensi++;
                                    }
                                }
                            }

                            $jumlahDipilihAmbilIntervensi = count($_POST['ambil_intervensi'][$i]);
                            for($b = 0; $jumlahDipilihAmbilIntervensi > $b; $b++){
                                $modRencana = RIRencanakeperawatanM::model()->findByPk($_POST['ambil_intervensi'][$i][$b]);
                                $modPlanning[$i][$b] = new RIPlaningaskepT();
                                $modPlanning[$i][$b]->asuhankeperawatan_id = $modAsuhanKeperawatan[$i]->asuhankeperawatan_id;
                                if ($modRencana->iskolaborasiintervensi == true){
                                    $modPlanning[$i][$b]->kolaborasilanjutan = $modRencana->rencana_intervensi ;
                                }
                                else{
                                    $modPlanning[$i][$b]->intervensilanjutan = $modRencana->rencana_intervensi;
                                }

                                if ($modPlanning[$i][$b]->validate()){
                                    if ($modPlanning[$i][$b]->save()){
                                        $successPlanning = true;
                                        $jumlahPlanning++;
                                    }
                                }
                            }

                            $jumlahDipilihImplementasi = count($_POST['rencana_implementasi'][$i]);
                            for($b = 0; $jumlahDipilihImplementasi > $b; $b++){
                                $modelImplementasi = ImplementasikeperawatanM::model()->findByPk($_POST['rencana_implementasi'][$i][$b]);
                                $modImplementasi[$i][$b] = new RIImplementasiaskepT();
                                $modImplementasi[$i][$b]->asuhankeperawatan_id = $modAsuhanKeperawatan[$i]->asuhankeperawatan_id;
                                $modImplementasi[$i][$b]->implementasikeperawatan_id = $_POST['rencana_implementasi'][$i][$b];
                                $modImplementasi[$i][$b]->tglmulaiimplementasi = $modAsuhanKeperawatan[$i]->tglaskep;
                                $modImplementasi[$i][$b]->implementasi_nama = $modelImplementasi->implementasi_nama;
                                $modImplementasi[$i][$b]->iskolaborasi = $modelImplementasi->iskolaborasiimplementasi;
                                if ($modImplementasi[$i][$b]->validate()){
                                    if ($modImplementasi[$i][$b]->save()){
                                        $successImplementasi = true;
                                        $jumlahImplementasi++;
                                    }
                                }
                            }
                        }
                    }
                }

                if ($jumlahImplementasi > 0 && $jumlahDipilihImplementasi > 0 && $jumlahImplementasi != $jumlahDipilihImplementasi)
                    $successImplementasi = false;
                if ($jumlahPlanning > 0 && $jumlahDipilihAmbilIntervensi > 0 && $jumlahPlanning != $jumlahDipilihAmbilIntervensi)
                    $successPlanning = false;
                if ($jumlahIntervensi > 0 && $jumlahDipilihRencanaIntervensi > 0 && $jumlahIntervensi != $jumlahDipilihRencanaIntervensi)
                    $successIntervensi = false;
                if ($jumlahDipilihImplementasi == 0)
                    $successImplementasi = true;
                if ($jumlahDipilihAmbilIntervensi == 0)
                    $successPlanning = true;
                if ($jumlahDipilihRencanaIntervensi == 0)
                    $successIntervensi = true;
                
                if ($success && $successPlanning && $successImplementasi && $successIntervensi){
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data Asuhan Keperawatan berhasil disimpan");
                    $this->refresh();
                }else{
                    echo $successIntervensi;
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan");
                }
            } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
            }
            
        } 

        $this->render('index',array('modPasien'=> $modPasien, 'modPasienRIV'=>$modPasienRIV,
                                    'modAnamnesa'=> $modAnamnesa,
                                    'modPeriksaFisik'=> $modPeriksaFisik,
                                    'model'=>$model,
        ));
    }
    
    public function actionRiwayatAsuhan(){
        $this->layout = '//layouts/frameDialog';
        $this->render('_riwayatAsuhanKeperawatan', 
            array('modPendaftaran'=>$modPendaftaran,                         
                  'modDiagnosaKeperawatanSearch'=>$modDiagnosaKeperawatanSearch,
                  ));
    }
    
    public function actionDetailDiagnosaKeperawatan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            $modDiagnosaKeperawatanSearch = new RIAsuhankeperawatanT('search');
            $this->render('_diagnosa', 
                    array('modPendaftaran'=>$modPendaftaran,                         
                        'modDiagnosaKeperawatanSearch'=>$modDiagnosaKeperawatanSearch,
                        ));
    }
    
    public function actionDetailRencanaKeperawatan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            //$modDiagnosaKeperawatanSearch = RIAsuhankeperawatanT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            $modDiagnosaKeperawatanSearch = new RIAsuhankeperawatanT('search');
            $this->render('_rencana', 
                    array('modPendaftaran'=>$modPendaftaran,                         
                        'modDiagnosaKeperawatanSearch'=>$modDiagnosaKeperawatanSearch,
                        ));
    }
    
    public function actionDetailEvaluasiKeperawatan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            //$modDiagnosaKeperawatanSearch = RIAsuhankeperawatanT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            $modDiagnosaKeperawatanSearch = new RIAsuhankeperawatanT('search');
            $this->render('_evaluasi', 
                    array('modPendaftaran'=>$modPendaftaran,                         
                        'modDiagnosaKeperawatanSearch'=>$modDiagnosaKeperawatanSearch,
                        ));
    }
    
    public function actionDetailPlanningKeperawatan($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            //$modDiagnosaKeperawatanSearch = RIAsuhankeperawatanT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
            $modDiagnosaKeperawatanSearch = new RIAsuhankeperawatanT('search');
            $this->render('_planning', 
                    array('modPendaftaran'=>$modPendaftaran,                         
                        'modDiagnosaKeperawatanSearch'=>$modDiagnosaKeperawatanSearch,
                        ));
    }
    
    public function actionGetRiwayatPasien($id){
            $this->layout='//layouts/frameDialog';
            $modPendaftaran = RIPendaftaranT::model()->findByPk($id);
            $noRekamMedik = RIPasienM::model()->findByPk($modPendaftaran->pasien_id)->no_rekam_medik;
            
            $criteria = new CDbCriteria(array(
                        'condition' => "no_rekam_medik ='" . $noRekamMedik . "' and ruangan_id =" . Yii::app()->user->getState('ruangan_id'),
                        'order' => 'tgl_pendaftaran DESC',
                        ));

            $pages = new CPagination(InfokunjunganrjV::model()->count($criteria));
            $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
            $pages->applyLimit($criteria);
            $modKunjungan = InfokunjunganrjV::model()->findAll($criteria);
            $tr = '';
            foreach($modKunjungan as $row){
                $modPendaftaran = PendaftaranT::model()->findByPk($row->pendaftaran_id);
                $modAnamnesa = AnamnesaT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id, 'pasien_id'=>$modPendaftaran->pasien_id));
                $diagnosa = PasienmorbiditasT::model()->with('diagnosa')->findByAttributes(array('pasien_id'=>$row->pasien_id, 'kelompokdiagnosa_id'=>PARAMS::KELOMPOKDIAGNOSA_UTAMA));
                $asuhan = AsuhankeperawatanT::model()->findByAttributes(array('pasien_id'=>$row->pasien_id, 'pendaftaran_id'=>$row->pendaftaran_id, 'ruangan_id'=>  Yii::app()->user->getState('ruangan_id')), array('order'=>'tglaskep DESC'));
                $tr .= "<tr>
                            <td>".$row->tgl_pendaftaran.'<br/>'.$row->no_pendaftaran."</td>
                            <td>".$modAnamnesa->keluhanutama."</td>
                            <td>".$modAnamnesa->riwayatpenyakitterdahulu."</td>
                            <td>".$modPendaftaran->pemeriksaanfisik->tekanandarah."</td>
                            <td>".$modPendaftaran->pemeriksaanfisik->detaknadi."</td>
                            <td>".$modPendaftaran->pemeriksaanfisik->suhutubuh."</td>
                            <td>".$modPendaftaran->pemeriksaanfisik->tinggibadan_cm."<br/>".$modPendaftaran->pemeriksaanfisik->beratbadan_kg."</td>
                            <td>".$diagnosa->diagnosa->diagnosa_nama."</td>
                            <td>".$asuhan->tglaskep."</td>
                            <td>".CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("Asuhankeperawatan/detailDiagnosaKeperawatan",
                                array("id"=>$row->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailData2","rel"=>"tooltip","title"=>"Klik untuk Detail Diagnosa Keperawatan", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailData2').text(text);window.parent.$('#dialogDetailData2').dialog('open');", "dialog-text"=>"Detail Diagnosa Keperawatan"))."</td>
                            <td>".CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("Asuhankeperawatan/detailRencanaKeperawatan",
                                array("id"=>$row->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailData2","rel"=>"tooltip","title"=>"Klik untuk Detail Rencana Keperawatan", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailData2').text(text);window.parent.$('#dialogDetailData2').dialog('open');", "dialog-text"=>"Detail Rencana Keperawatan"))."</td>
                            <td>".CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("Asuhankeperawatan/detailEvaluasiKeperawatan",
                                array("id"=>$row->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailData2","rel"=>"tooltip","title"=>"Klik untuk Detail Evaluasi Keperawatan", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailData2').text(text);window.parent.$('#dialogDetailData2').dialog('open');", "dialog-text"=>"Detail Evaluasi Keperawatan"))."</td>
                            <td>".CHtml::link("<i class='icon-list-alt'></i> ",  Yii::app()->controller->createUrl("Asuhankeperawatan/detailPlanningKeperawatan",
                                array("id"=>$row->pendaftaran_id)),array("id"=>"$modKunjungan->no_pendaftaran","target"=>"detailData2","rel"=>"tooltip","title"=>"Klik untuk Detail Planning Keperawatan", "onclick"=>"var text = $(this).attr('dialog-text'); window.parent.$('#ui-dialog-title-dialogDetailData2').text(text);window.parent.$('#dialogDetailData2').dialog('open');", "dialog-text"=>"Detail Planning Keperawatan"))."</td>                                    
                         </tr>";
            }
            
            $this->render('_riwayatAsuhanKeperawatan', 
                    array('tr'=>$tr,        
                        'pages'=>$pages
                        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
        $model=new RIAsuhankeperawatanT('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['RIAsuhankeperawatanT']))
            $model->attributes=$_GET['RIAsuhankeperawatanT'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id)
    {
        $model=RIAsuhankeperawatanT::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='rjasuhankeperawatan-t-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
    {
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
        
        public function actionPrint()
        {
            $model= new RIAsuhankeperawatanT;
            $model->attributes=$_REQUEST['RIAsuhankeperawatanT'];
            $judulLaporan='Data RIAsuhankeperawatanT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->session['ukuran_kertas'];                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->session['posisi_kertas'];                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}