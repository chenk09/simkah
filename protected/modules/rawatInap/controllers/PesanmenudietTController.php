
<?php

class PesanmenudietTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';
                protected $pathView = 'gizi.views.pesanmenudietT.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'indexPegawai', 'informasi', 'detailPesanMenuDiet'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionIndex()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPesanmenudietT;
                $model->tglpesanmenu = date('Y-m-d H:i:s');
                $model->nopesanmenu = Generator::noPesanMenuDiet();
                $pegawai_nama = PegawaiM::model()->findByPK(LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai_id)->pegawai_nama;
                $model->nama_pemesan = $pegawai_nama;
                $model->jenispesanmenu = Params::JENISPESANMENU_PASIEN;
                $model->instalasi_id = Yii::app()->user->instalasi_id;
                $model->ruangan_id = Yii::app()->user->ruangan_id;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZPesanmenudietT']))
		{
                    $model->attributes=$_POST['GZPesanmenudietT'];
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            foreach($_POST['PesanmenudetailT'] as $i=>$v){
                                if ($v['checkList'] == 1){
                                    foreach($v['menudiet_id'] as $j=>$x){
                                        if (!empty($x)){
                                            $modDetail = new GZPesanmenudetailT();
                                            $modDetail->attributes = $v;
                                            $modDetail->pesanmenudiet_id = $model->pesanmenudiet_id;
                                            $modDetail->jeniswaktu_id = $j;
                                            $modDetail->menudiet_id = $x;
                                            if ($modDetail->save()){

                                            }
                                            else{
                                                $success=false;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            $success = false;
                        }
                        if ($success == TRUE){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch(Exception $ex){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
		}

		$this->render($this->pathView.'index',array(
			'model'=>$model,
		));
	}
	public function actionIndexPegawai()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPesanmenudietT;
                $model->tglpesanmenu = date('Y-m-d H:i:s');
                $model->nopesanmenu = Generator::noPesanMenuDiet();
                $pegawai_nama = PegawaiM::model()->findByPK(LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai_id)->pegawai_nama;
                $model->nama_pemesan = $pegawai_nama;
                $model->instalasi_id = Yii::app()->user->instalasi_id;
                $model->ruangan_id = Yii::app()->user->ruangan_id;
//                $model->jenispesanmenu = Params::JENISPESANMENU_PASIEN;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZPesanmenudietT']))
		{    
                    $model->attributes=$_POST['GZPesanmenudietT'];
                    $model->jenispesanmenu = $_POST['jenisPesan'];
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        $jumlah = count($_POST['PesanmenupegawaiT']);  
                        $ruangan = array();
                        $tempRuangan = array();
                        for ($i = 0; $i < $jumlah; $i++) {       
                            foreach ($_POST['PesanmenupegawaiT'][$i] as $x=>$v){
                                if (in_array($x, $ruangan)){
                                    array_push($tempRuangan[$x], $v);
                                }else{
                                    $ruangan[] = $x;
                                    $tempRuangan[$x] = array($v);
                                }
                            }
                        }
                        foreach($tempRuangan as $i=>$baris){
                            $models = new GZPesanmenudietT();
                            $models->attributes = $model->attributes;
                            $models->nopesanmenu = Generator::noPesanMenuDiet();
                            $models->totalpesan_org = count($baris);
                            $models->ruangan_id = $i;
                            if ($models->save()){
                                foreach ($baris as $row){
                                    foreach ($row['menudiet_id'] as $j=>$v){
                                        if ($row['checkList'] == 1){
                                            if (!empty($v)){
                                                $modDetail = new PesanmenupegawaiT();
                                                $modDetail->attributes = $row;
                                                $modDetail->pesanmenudiet_id = $models->pesanmenudiet_id;
                                                $modDetail->jeniswaktu_id = $j;
                                                $modDetail->menudiet_id = $v;
                                                if ($modDetail->save()){

                                                }
                                                else{
                                                    $success=false;
                                                }
                                            }
                                        }
                                    }
                                }                             
                            }
                            else{
                                 $success = FALSE;
                            }
                        }
                        if ($success == TRUE){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch(Exception $ex){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
		}

		$this->render($this->pathView.'indexPegawai',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GZPesanmenudietT']))
		{
			$model->attributes=$_POST['GZPesanmenudietT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->pesanmenudiet_id));
                        }
		}

		$this->render($this->pathView.'update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
//	public function actionIndex()
//	{
//		$dataProvider=new CActiveDataProvider('GZPesanmenudietT');
//		$this->render($this->pathView.'index',array(
//			'dataProvider'=>$dataProvider,
//		));
//	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPesanmenudietT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GZPesanmenudietT']))
			$model->attributes=$_GET['GZPesanmenudietT'];

		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GZPesanmenudietT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gzpesanmenudiet-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new GZPesanmenudietT;
            $model->attributes=$_REQUEST['GZPesanmenudietT'];
            $judulLaporan='Data GZPesanmenudietT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionInformasi()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new GZPesanmenudietT('search');
//		$model->unsetAttributes();  // clear any default values
                $model->tglAwal = date('Y-m-d H:i:s');
                $model->tglAkhir = date('Y-m-d H:i:s');
		if(isset($_GET['GZPesanmenudietT'])){
                    $model->attributes=$_GET['GZPesanmenudietT'];
                    $format = new CustomFormat();
                    $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
                }

		$this->render($this->pathView.'informasi',array(
			'model'=>$model,
		));
	}
        
        public function actionDetailPesanMenuDiet($id){
            $this->layout ='//layout/frameDialog';
            $modPesan = PesanmenudietT::model()->findByPk($id);
            if ($modPesan->jenispesanmenu == Params::JENISPESANMENU_PASIEN){
                $criteria = new CDbCriteria();
                $criteria->select = 'pasienadmisi_id, pendaftaran_id, pasien_id,  pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt';
                $criteria->group = 'pasienadmisi_id, pendaftaran_id, pasien_id, pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt';
                $criteria->compare('pesanmenudiet_id', $id);
                $modDetailPesan = PesanmenudetailT::model()->findAll($criteria);
            }
            else{
                $criteria = new CDbCriteria();
                $criteria->select = 'pegawai_id,  pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt';
                $criteria->group = 'pegawai_id, pesanmenudiet_id, jml_pesan_porsi, satuanjml_urt';
                $criteria->compare('pesanmenudiet_id', $id);
                $modDetailPesan = PesanmenupegawaiT::model()->findAll($criteria);
            }
            $this->render($this->pathView.'detailInformasi', array(
                'modPesan'=>$modPesan,
                'modDetailPesan'=>$modDetailPesan,
            ));
        }

}
