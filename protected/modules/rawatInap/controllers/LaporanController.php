<?php

class LaporanController extends SBaseController {
    
    public function actionLaporanKunjungan() {
        $model = new RIInfokunjunganriV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['RIInfokunjunganriV'])) {
            $model->attributes = $_GET['RIInfokunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIInfokunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIInfokunjunganriV']['tglAkhir']);
        }


        $this->render('kunjungan/adminKunjungan', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjungan() {
        $model = new RIInfokunjunganriV('search');
        $judulLaporan = 'Laporan Info Kunjungan Pasien Rawat Inap';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Info Kunjungan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RIInfokunjunganriV'])) {
            $model->attributes = $_REQUEST['RIInfokunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RIInfokunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RIInfokunjunganriV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjungan/_printKunjungan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjungan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RIInfokunjunganriV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Info Kunjungan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RIInfokunjunganriV'])) {
            $model->attributes = $_GET['RIInfokunjunganriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIInfokunjunganriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIInfokunjunganriV']['tglAkhir']);
            $model->pilihanx = $format->formatDateTimeMediumForDB($_GET['RIInfokunjunganriV']['pilihanx']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanSensusHarian() {
        $model = new RILaporansensusharian('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['RILaporansensusharian'])) {
            $model->attributes = $_GET['RILaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporansensusharian']['tglAkhir']);
        }

        $this->render('sensus/adminSensus', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanSensusHarian() {
        $model = new RILaporansensusharian('search');
        $judulLaporan = 'Laporan Sensus Harian Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporansensusharian'])) {
            $model->attributes = $_REQUEST['RILaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporansensusharian']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'sensus/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikSensusHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporansensusharian('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RILaporansensusharian'])) {
            $model->attributes = $_GET['RILaporansensusharian'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporansensusharian']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporansensusharian']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanTindakLanjut() {
        $model = new RILaporantindaklanjutri('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        $temp = array();
        foreach (CaraKeluar::items() as $i=>$data){
            $temp[] = strtoupper($data);
        }
        $model->carakeluar = $temp;
        
        if (isset($_GET['RILaporantindaklanjutri'])) {
            $model->attributes = $_GET['RILaporantindaklanjutri'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporantindaklanjutri']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporantindaklanjutri']['tglAkhir']);
        }

        $this->render('tindakLanjut/adminTindakLanjut', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanTindakLanjut() {
        $model = new RILaporantindaklanjutri('search');
        $judulLaporan = 'Laporan Tindak Lanjut Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporantindaklanjutri'])) {
            $model->attributes = $_REQUEST['RILaporantindaklanjutri'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporantindaklanjutri']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporantindaklanjutri']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'tindakLanjut/_printTindakLanjut';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanTindakLanjut() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporantindaklanjutri('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik 
        $data['title'] = 'Grafik Laporan Tindak Lanjut Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporantindaklanjutri'])) {
            $model->attributes = $_GET['RILaporantindaklanjutri'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporantindaklanjutri']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporantindaklanjutri']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPasienMeninggal() {
        $model = new RILaporanpasienmeninggalriV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        $caramasuk = CHtml::listData(CaramasukM::model()->findAll('caramasuk_aktif = true'), 'caramasuk_id', 'caramasuk_id');
        $model->caramasuk_id = $caramasuk;
        if (isset($_GET['RILaporanpasienmeninggalriV'])) {
            $model->attributes = $_GET['RILaporanpasienmeninggalriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpasienmeninggalriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpasienmeninggalriV']['tglAkhir']);
        }

        $this->render('pasienMeninggal/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPasienMeninggal() {
        $model = new RILaporanpasienmeninggalriV('search');
        $judulLaporan = 'Laporan Pasien Meninggal';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporanpasienmeninggalriV'])) {
            $model->attributes = $_REQUEST['RILaporanpasienmeninggalriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanpasienmeninggalriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanpasienmeninggalriV']['tglAkhir']);
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pasienMeninggal/_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPasienMeninggal() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporanpasienmeninggalriV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pasien Meninggal';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['RILaporanpasienmeninggalriV'])) {
            $model->attributes = $_GET['RILaporanpasienmeninggalriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpasienmeninggalriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpasienmeninggalriV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBukuRegister() {
        $model = new RIBukuregisterriV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['RIBukuregisterriV']))
        {
            $model->attributes = $_GET['RIBukuregisterriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAkhir']);
        }
        
        $this->render('bukuRegister/adminBukuRegister', array(
            'model'=>$model
        ));
    }
    
    public function actionPrintLaporanBukuRegister() {
        $model = new RIBukuregisterriV('search');
        $judulLaporan = 'Laporan Buku Register Rawat Inap';
        
        // Data untuk Grafik
        $data['title'] = 'Grafik Laporan Buku Register Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['RIBukuregisterriV'])) {
            $model->attributes = $_REQUEST['RIBukuregisterriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'bukuRegister/_printBukuRegister';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikBukuRegister() {
        $this->layout = '//layouts/frameDialog';
        $model = new RIBukuregisterriV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        // Data untuk Grafik
        $data['title'] = 'Grafik Laporan Buku Register Pasien Rawat Inap';
        $data['type'] = $_REQUEST['type'];
        
        if (isset($_REQUEST['RIBukuregisterriV'])) {
            $model->attributes = $_GET['RIBukuregisterriV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RIBukuregisterriV']['tglAkhir']);
        }
        
        $this->render('_grafik',array(
            'model'=>$model,
            'data'=>$data,
        ));
    }
    
    public function actionLaporan10BesarPenyakit() {
        $model = new RILaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        $model->jumlahTampil = 10;

        if (isset($_GET['RILaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RILaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporan10besarpenyakit']['tglAkhir']);
        }

        $this->render('10Besar/admin10BesarPenyakit', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporan10BesarPenyakit() {
        $model = new RILaporan10besarpenyakit('search');
        $judulLaporan = 'Laporan 10 Besar Penyakit Pasien Rawat Inap';

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporan10besarpenyakit'])) {
            $model->attributes = $_REQUEST['RILaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporan10besarpenyakit']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '10Besar/_print10Besar';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafik10BesarPenyakit() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporan10besarpenyakit('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan 10 Besar Penyakit';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporan10besarpenyakit'])) {
            $model->attributes = $_GET['RILaporan10besarpenyakit'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporan10besarpenyakit']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporan10besarpenyakit']['tglAkhir']);
        }
               
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanBiayaPelayanan() {
        $model = new RILaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE'),'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RILaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RILaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanbiayapelayanan']['tglAkhir']);
        }

        $this->render('biayaPelayanan/adminBiayaPelayanan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanBiayaPelayanan() {
        $model = new RILaporanbiayapelayanan('search');
        $judulLaporan = 'Laporan Biaya Pelayanan Rawat Inap';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Biaya Pelayanan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporanbiayapelayanan'])) {
            $model->attributes = $_REQUEST['RILaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanbiayapelayanan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'biayaPelayanan/_printBiayaPelayanan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanBiayaPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporanbiayapelayanan('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Biaya Pelayanan Rawat Inap';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporanbiayapelayanan'])) {
            $model->attributes = $_GET['RILaporanbiayapelayanan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanbiayapelayanan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanbiayapelayanan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPendapatanRuangan() {
        $model = new RILaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        $penjamin = CHtml::listData($model->getPenjaminItems(), 'penjamin_id', 'penjamin_id');
        $model->penjamin_id = $penjamin;
        $kelas = CHtml::listData(KelaspelayananM::model()->findAll(), 'kelaspelayanan_id', 'kelaspelayanan_id');
        $model->kelaspelayanan_id = $kelas;
        if (isset($_GET['RILaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RILaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpendapatanruangan']['tglAkhir']);
        }

        $this->render('pendapatanRuangan/adminPendapatanRuangan', array(
            'model' => $model, 'filter'=>$filter
        ));
    }

    public function actionPrintLaporanPendapatanRuangan() {
        $model = new RILaporanpendapatanruangan('search');
        $judulLaporan = 'Laporan Grafik Pendapatan Ruangan Rawat Inap';

        //Data Grafik        
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['RILaporanpendapatanruangan'])) {
            $model->attributes = $_REQUEST['RILaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['RILaporanpendapatanruangan']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pendapatanRuangan/_printPendapatanRuangan';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPendapatanRuangan() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporanpendapatanruangan('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Ruangan Rawat Inap';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporanpendapatanruangan'])) {
            $model->attributes = $_GET['RILaporanpendapatanruangan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpendapatanruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpendapatanruangan']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanPemakaiObatAlkes()
    {
        $model = new RILaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['RILaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['RILaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $this->render('pemakaiObatAlkes/adminPemakaiObatAlkes',array(
            'model'=>$model,
        ));
    }
    
    public function actionPrintLaporanPemakaiObatAlkes()
    {
        $model = new RILaporanpemakaiobatalkesV('search');
        $judulLaporan = 'Laporan Pemakai Obat Alkes Rawat Inap';
        $model->tglAwal = date('d M Y  H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['RILaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['RILaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pemakaiObatAlkes/_printPemakaiObatAlkes';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPemakaiObatAlkes() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporanpemakaiobatalkesV('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pemakai Obat Alkes Rawat Inap';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporanpemakaiobatalkesV'])) {
            $model->attributes = $_GET['RILaporanpemakaiobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanpemakaiobatalkesV']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanJasaInstalasi()
    {
        $model = new RILaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['RILaporanjasainstalasi'])) {
            $model->attributes = $_GET['RILaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAkhir']);
        }
        $this->render('jasaInstalasi/adminJasaInstalasi',array(
            'model'=>$model,
        ));
    }
    
    public function actionPrintLaporanJasaInstalasi()
    {
        $model = new RILaporanjasainstalasi('search');
        $judulLaporan = 'Laporan Jasa Instalasi Rawat Inap';
        $model->tglAwal = date('d M Y H:i;s');
        $model->tglAkhir = date('d M Y H:i:s');
        
        if (isset($_GET['RILaporanjasainstalasi'])) {
            $model->attributes = $_GET['RILaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAkhir']);
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaInstalasi/_printJasaInstalasi';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanJasaInstalasi() {
        $this->layout = '//layouts/frameDialog';
        $model = new RILaporanjasainstalasi('search');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Instalasi Rawat Inap';
        $data['type'] = $_GET['type'];
        if (isset($_GET['RILaporanjasainstalasi'])) {
            $model->attributes = $_GET['RILaporanjasainstalasi'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporanjasainstalasi']['tglAkhir']);
        }
                
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
   protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

//    public function actionLaporanKunjungan()
//    {
//        $model = new RILaporankunjunganriV('search');
//        $model->tglAwal = date('Y-m-d H:i:s');
//        $model->tglAkhir = date('Y-m-d H:i:s');
//
//        if (isset($_GET['RILaporankunjunganriV'])) {
//            $model->attributes = $_GET['RILaporankunjunganriV'];
//            $format = new CustomFormat();
//            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['RILaporankunjunganriV']['tglAwal']);
//            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['RILaporankunjunganriV']['tglAkhir']);
//        }
//
//
//        $this->render('kunjungan/adminKunjungan', array(
//            'model' => $model,
//        ));
//    }
    
}
?>
