<?php

class InformasiKamarController extends SBaseController
{
	public function actionIndex()
	{
                $modKamarRuangan = new RIKamarRuanganM;
                $trInformasiHarga='';
                $jumlahTempatTidur=0;
                $dataTempatTidur=array();
                $fotoKamar='';
                $formKasur='';
                $noKamar='';
                $kelasPelayanan='';
                
                $idRuangan=Yii::app()->user->getState('ruangan_id');
                $modRuangan=RIRuanganM::model()->findByPk($idRuangan);
                
                if(isset ($_POST['RIKamarRuanganM']['kelaspelayanan_id'])){
                    $idKelasPelayanan=$_POST['RIKamarRuanganM']['kelaspelayanan_id'];
                }
                
                if(isset ($_POST['RIKamarRuanganM']['kamarruangan_nokamar'])){
                    $noKamar=$_POST['RIKamarRuanganM']['kamarruangan_nokamar'];
                }
                
                if($idKelasPelayanan!=''){
                    $sqlKelas=" AND kelaspelayanan_id=".$idKelasPelayanan."";
                }
                
                if($noKamar!=''){
                    $sqlKamar=" AND kamarruangan_nokamar='".$noKamar."'";
                }
                
                    $sqlKamar="SELECT DISTINCT(kamarruangan_nokamar),kelaspelayanan_id,ruangan_id
                               FROM kamarruangan_m
                               WHERE ruangan_id=".$idRuangan."".$sqlKelas."".$sqlKamar."
                               ORDER BY kamarruangan_nokamar ASC";
//                    $dataNoKamar= Yii::app()->db->createCommand($sqlKamar)->query();
                    
                    $criteria = new CDbCriteria();
                    
                    $criteria->select='kamarruangan_nokamar,kelaspelayanan_id,ruangan_id';
                    $criteria->compare('ruangan_id',$idRuangan);
                    $criteria->compare('kelaspelayanan_id',$idKelasPelayanan);
                    $criteria->compare('LOWER(kamarruangan_nokamar)',strtolower($noKamar));
                    
                    $criteria->group='kamarruangan_nokamar,kelaspelayanan_id,ruangan_id';
                    $criteria->order = 'kamarruangan_nokamar ASC';
                    $dataNoKamar= KamarruanganM::model()->findAll($criteria);
                    
                    foreach ($dataNoKamar AS $i=>$tampilDataNoKamar){
                        $trInformasiHarga='';
                        $trTotal='';
                        $idKelasPelayanan=$tampilDataNoKamar->kelaspelayanan_id;
                        $noKomar=$tampilDataNoKamar->kamarruangan_nokamar;
                        $idRuangan=$tampilDataNoKamar->ruangan_id;
//==============================Awal Mencari Tarif Kamar    
/*                        $sql="SELECT 
                              tariftindakan_m.daftartindakan_id, 
                              daftartindakan_m.daftartindakan_nama, 
                              daftartindakan_m.daftartindakan_akomodasi, 
                              komponentarif_m.komponentarif_nama, 
                              kamarruangan_m.kamarruangan_nokamar,
                              kamarruangan_m.kelaspelayanan_id,
                              tariftindakan_m.harga_tariftindakan,
                              komponentarif_m.komponentarif_id,
                              kamarruangan_m.kamarruangan_jmlbed,
                              kamarruangan_m.kamarruangan_image
                            FROM 
                              public.daftartindakan_m, 
                              public.tindakanruangan_m, 
                              public.tariftindakan_m, 
                              public.komponentarif_m, 
                              public.kamarruangan_m
                            WHERE 
                              tindakanruangan_m.daftartindakan_id = daftartindakan_m.daftartindakan_id AND
                              tindakanruangan_m.ruangan_id = kamarruangan_m.ruangan_id AND
                              tariftindakan_m.daftartindakan_id = daftartindakan_m.daftartindakan_id AND
                              komponentarif_m.komponentarif_id = tariftindakan_m.komponentarif_id AND
                              daftartindakan_m.daftartindakan_akomodasi=TRUE AND
                              kamarruangan_m.kelaspelayanan_id=".$idKelasPelayanan." AND
                              kamarruangan_m.kamarruangan_nokamar='".$noKomar."' AND   
                              kamarruangan_m.ruangan_id=".$idRuangan."    
                              GROUP BY kamarruangan_m.kelaspelayanan_id,kamarruangan_m.kamarruangan_nokamar,
                                       komponentarif_m.komponentarif_nama,daftartindakan_m.daftartindakan_akomodasi,
                                       daftartindakan_m.daftartindakan_nama, komponentarif_m.komponentarif_id,
                                       tariftindakan_m.daftartindakan_id,daftartindakan_m.daftartindakan_id,
                                       harga_tariftindakan,kamarruangan_jmlbed,kamarruangan_m.kamarruangan_image
                              ORDER BY komponentarif_m.komponentarif_id ASC";
                        $dataTarif= Yii::app()->db->createCommand($sql)->query();
 * 
 */                     $criteria2 = new CDbCriteria();
                    
                        $criteria2->select='kamarruangan_nokamar,kelaspelayanan_id,ruangan_id,kamarruangan_image,kamarruangan_jmlbed,harga_tariftindakan';
                        $criteria2->compare('ruangan_id',$idRuangan);
                        $criteria2->compare('kelaspelayanan_id',$idKelasPelayanan);
                        $criteria2->compare('LOWER(kamarruangan_nokamar)',strtolower($noKamar));

                        $criteria2->group = 'kamarruangan_nokamar,kelaspelayanan_id,ruangan_id,kamarruangan_image,kamarruangan_jmlbed,harga_tariftindakan';
                        $criteria2->order = 'kamarruangan_nokamar ASC';
                        $dataTarif= RIInformasitarifakomodasiV::model()->findAll(
                                'kelaspelayanan_id = '.$idKelasPelayanan.' AND ruangan_id ='.$idRuangan.' 
                                    AND kamarruangan_nokamar =\''.$noKomar.'\'');
//                        $dataTarif= RIInformasitarifakomodasiV::model()->findAll($criteria2);
                        foreach($dataTarif AS $key=>$tampiltarif){
//                            if($tampiltarif['komponentarif_id']!=Params::KOMPONENTARIF_ID_TOTAL){
//                            $trInformasiHarga .="<tr>
//                                                    <td width=\"200px\">".$tampiltarif['komponentarif_nama']."</td>
//                                                    <td>".$tampiltarif['harga_tariftindakan']."</td>    
//                                                  </tr>";
//                            }else{
                                 $trTotal .="<tr>
                                                    <td>Total Tarif</td>
                                                    <td>".$tampiltarif->harga_tariftindakan."</td>    
                                                  </tr>";
//                            }
                        $fotoKamar=$tampiltarif->kamarruangan_image;    
                        $jumlahTempatTidur= $tampiltarif->kamarruangan_jmlbed;
                        $kelasPelayanan=  RIKelasPelayananM::model()->findByPk($tampiltarif->kelaspelayanan_id)->kelaspelayanan_nama;
                        $noKamar=$tampiltarif->kamarruangan_nokamar;
                        if($fotoKamar!=''){
                            $fotoTampil=$fotoKamar;
                        }else{
                             $fotoTampil='no_photo.jpeg';
                        }
                            
                        }
//                        $trInformasiHarga ='<table>'.$trInformasiHarga.$trTotal.'</table>';
                        $trInformasiHarga ='<table>'.$trTotal.'</table>';
                        
                        $criteria3 = new CDbCriteria();
                    
                        $criteria3->select='kamarruangan_status,kamarruangan_id,kamarruangan_nokamar,kelaspelayanan_id,ruangan_id,kamarruangan_nobed';
                        $criteria3->compare('ruangan_id',$idRuangan);
                        $criteria3->compare('kelaspelayanan_id',$idKelasPelayanan);
                        $criteria3->compare('LOWER(kamarruangan_nokamar)',strtolower($noKamar));

                        $criteria3->group='kamarruangan_status,kamarruangan_id,kamarruangan_nokamar,kelaspelayanan_id,ruangan_id,kamarruangan_nobed';
                        $criteria3->order = 'kamarruangan_nobed ASC';

//                        $dataTempatTidur=RIKamarRuanganM::model()->findAll('ruangan_id='.$idRuangan.' 
//                                                                    AND kelaspelayanan_id='.$idKelasPelayanan.'
//                                                                    AND kamarruangan_nokamar=\''.$noKomar.'\' 
//                                                                    ORDER BY kamarruangan_nobed ASC');
                        $dataTempatTidur=RIKamarRuanganM::model()->findAll($criteria3);
                
                
                     $col = 3;
                     $cnt =0;
                     $batas   = 999999999;
                     $formKasur .='<div class="boxInformasi">
                                    <fieldset>
                                    <legend>Data No. Kamar :'.$tampilDataNoKamar->kamarruangan_nokamar.'</legend>
                                      <table align="center" cellspacing="0" cellpadding="0">
                                      <tr>
                                        <td colspan='.$col.'>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <fieldset>
                                                            <legend>Informasi Harga</legend>
                                                            '.$trInformasiHarga.'
                                                        </fieldset>
                                                   </td>
                                                   <td>
                                                        <fieldset>
                                                            <legend>Informasi Kamar</legend>
                                                            Jumlah Tempat Tidur :'.$jumlahTempatTidur.'<br/>
                                                            Kelas Pelayanan :'.$kelasPelayanan.'<br/>
    
                                                        </fieldset>
                                                   </td>
                                                   <td>
                                                        <fieldset>
                                                            <legend>Foto Kamar</legend>
                                                        <img src="'.Params::urlKamarRuanganTumbsDirectory().'kecil_'.$fotoTampil.'">
                                                          </fieldset>  
                                                    </td>
                                               </tr>
                                            </table>   
                                        </td>
                                     </tr>   
                                        ';
                     foreach ($dataTempatTidur as $keys=>$tampilTempatTidur){
                           if ($cnt >= $col) 
                            {
                               $formKasur .='<tr>';
                               $cnt = 0;
                            }
                            $cnt++;
                            if($tampilTempatTidur->kamarruangan_status == false){//Jika Terisi
                                $modMasukKamar=  RIMasukKamarT::model()->find('kamarruangan_id='.$tampilTempatTidur->kamarruangan_id.' AND
                                                                               tglkeluarkamar isNUll AND
                                                                               jamkeluarkamar isNULL ORDER BY
                                                                               tglmasukkamar DESC');
//                                echo $modMasukKamar->pasienadmisi_id;
                                $modPasienAdmisi= RIPasienAdmisiT::model()->findByPk($modMasukKamar->pasienadmisi_id);
                                $modPasien=RIPasienM::model()->findByPk($modPasienAdmisi->pasien_id);
//                                echo $modPasienAdmisi->pasien_id;exit;
//                                if(count($modPasien) > 0){
                                    $modPendaftaran=RIPendaftaranT::model()->findByPk($modPasienAdmisi->pendaftaran_id);
                                    $noPendaftaran = $modPendaftaran->no_pendaftaran;
//                                }else{
//                                    $modPendaftaran=RIPendaftaranT::model()->findAll();
//                                    $noPendaftaran = $modPendaftaran->no_pendaftaran;
//                                }
                                $formKasur .='<td>
                                            <div class="boxrepeat ranjangIsi">
                                                <table cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>NO RM : <b>'.$modPasien->no_rekam_medik.'</b><br/>
                                                        No Pendaftaran : <b>'.$noPendaftaran.'</b><br/>
                                                        No. Kasur :<b>'.$tampilTempatTidur->kamarruangan_nobed.'</b><br/>
                                                        Nama : <b>'.$modPasien->nama_pasien.'</b><br/>
                                                        Status : <b>Terisi<b><br/>
                                                        Jenis Kelamin : <b>'.$modPasien->jeniskelamin.'</b></td>
                                                    </tr>
                                                </table>    
                                            </div>                
                                         </td>';
                            }else{
                                $formKasur .='<td>
                                            <div class="boxrepeat ranjangKosong">
                                            No. Kasur : 
                                                '.$tampilTempatTidur->kamarruangan_nobed.'<br/>'.
                                                    CHtml::htmlButton(Yii::t('mds','{icon} Kosong',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')),
                                                            array('class'=>'btn btn-danger', 'type'=>'button','id'=>'btn_simpan')).'
                                            <br/><br/><br/><br/><br/>
                                            </div>                
                                         </td>';
                            }
                     }
                        $formKasur .='</tr></table>
                            </fieldset>
                            </div>
                           <hr style="color: #00F"> '; 
                    }
//==============================Akhir Mencari Tarif Kamar                
                

                $this->render('index',array('modKamarRuangan'=>$modKamarRuangan,
                                            'modRuangan'=>$modRuangan,
                                            'trInformasiHarga'=>$trInformasiHarga,
                                            'jumlahTempatTidur'=>$jumlahTempatTidur,
                                            'dataTempatTidur'=>$dataTempatTidur,
                                            'formKasur'=>$formKasur,
                                            'fotoKamar'=>$fotoKamar,
                                            'idRuangan'=>$idRuangan,
                                            'noKamar'=>$noKamar,
                                            'kelasPelayanan'=>$kelasPelayanan));
                
	}

	
}