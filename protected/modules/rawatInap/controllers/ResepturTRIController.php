<?php
//Yii::import('rawatJalan.controllers.ResepturController');
//Yii::import('rawatJalan.models.*');
//class ResepturTRIController extends ResepturController
//{
//        
//}
class ResepturTRIController extends SBaseController
{
        protected $successSave = false;

        public function actionIndex($idPendaftaran = null,$idAdmisi = null)
	{
            $modAdmisi = (!empty($idAdmisi)) ? PasienadmisiT::model()->findByAttributes(array('pendaftaran_id'=>$idPendaftaran,'pasienadmisi_id'=>$idAdmisi)) : array();
            $modPendaftaran=RIPendaftaranT::model()->findByPk($idPendaftaran);
            $modPasien = RIPasienM::model()->findByPk($modPendaftaran->pasien_id);
            
            $modReseptur = new RIResepturT;
            $instalasi_id = Yii::app()->user->getState('instalasi_id');
//            $modReseptur->noresep = Generator::noResep($instalasi_id);
            $modReseptur->pegawai_id = $modPendaftaran->pegawai_id;
            $modReseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
            
            if(isset($_POST['RIResepturT'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $this->saveReseptur($_POST, $modPendaftaran);
                    
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Resep berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    //echo '<pre>'.print_r($_POST,1).'</pre>';
                }
            }
		
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modReseptur'=>$modReseptur,
                                        'modAdmisi'=>$modAdmisi));
	}
        
        protected function saveReseptur($post,$modPendaftaran)
        {
            $reseptur = new RIResepturT;
            $reseptur->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $reseptur->tglreseptur = $post['RIResepturT']['tglreseptur'];
            $reseptur->noresep = $post['RIResepturT']['noresep'];
            $reseptur->pegawai_id = $post['RIResepturT']['pegawai_id'];
            $reseptur->ruangan_id = $post['RIResepturT']['ruangan_id'];
            $reseptur->ruanganreseptur_id = Yii::app()->user->getState('ruangan_id');
            $reseptur->pasien_id = $modPendaftaran->pasien_id;
            if($reseptur->validate()){
                $reseptur->save();
                $this->saveDetailReseptur($post, $reseptur);
            } else {
                $this->successSave = false;
            }
        }
        
        protected function saveDetailReseptur($post,$reseptur)
        {
            $valid = true;
            for ($i = 0; $i < count($post['obat']); $i++) {
                $detail = new RIResepturDetailT;
                $detail->reseptur_id = $reseptur->reseptur_id;
                $detail->obatalkes_id = $post['obat'][$i];
                $detail->sumberdana_id = $post['sumberdana'][$i];
                $detail->satuankecil_id = $post['satuankecil'][$i];
                $detail->racikan_id = ($post['isRacikan'][$i]) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                $detail->r = 'R/';
                $detail->rke = $post['Rke'][$i];
                $detail->qty_reseptur = $post['qty'][$i];
                $detail->signa_reseptur = $post['signa'][$i];
                $detail->etiket = $post['etiket'][$i];
                $detail->kekuatan_reseptur = $post['kekuatan'][$i];
                $detail->satuankekuatan = $post['satuankekuatan'][$i];
                $detail->hargasatuan_reseptur = $post['hargasatuan'][$i];
                $detail->harganetto_reseptur = $post['harganetto'][$i];
                $detail->hargajual_reseptur = $post['hargajual'][$i] * $post['qty'][$i];
                
                $detail->permintaan_reseptur = $post['jmlpermintaan'][$i];
                $detail->jmlkemasan_reseptur = $post['jmlkemasan'][$i];
                $valid = $detail->validate() && $valid;
                if($valid){
                    $detail->save();
                }
            }
            
            $this->successSave = ($valid) ? true : false;
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}