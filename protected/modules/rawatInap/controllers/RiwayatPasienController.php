<?php
//untuk actionDetailHasilLab
Yii::import('application.modules.laboratorium.models.LKPasienMasukPenunjangV');
//untuk actionDetailHasilRab
Yii::import('application.modules.radiologi.models.ROPasienMasukPenunjangV');
class RiwayatPasienController extends SBaseController
{
    public $pathView = "rawatInap.views.riwayatPasien."; //agar bisa di extend
    public function actionGetRiwayatPasien($id)
    {
        $this->layout='//layouts/frameDialog';
        $criteria = new CDbCriteria(array(
                //'condition' => 't.pasien_id = '.$id.' and t.ruangan_id ='.Yii::app()->user->getState('ruangan_id'),
                'condition' => 't.pasien_id = '.$id,
                'order'=>'tgl_pendaftaran DESC',
            ));

        $pages = new CPagination(RIPendaftaranT::model()->count($criteria));
        $pages->pageSize = Params::JUMLAH_PERHALAMAN; //Yii::app()->params['postsPerPage'];
        $pages->applyLimit($criteria);

        $modKunjungan = RIPendaftaranT::model()->with('hasilpemeriksaanlab','anamnesa','pemeriksaanfisik','pasienmasukpenunjang','diagnosa')->
                findAll($criteria);


        $this->render($this->pathView.'_riwayatPasien', array(
                'pages'=>$pages,
                'modKunjungan'=>$modKunjungan,
        ));
    }
    
    public function actionDetailPemakaianBahan($id)
    {
        $this->layout='//layouts/frameDialog';
        $modPendaftaran = RIPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
        $modBahan = RIObatalkesPasienT::model()->with('obatalkes')->findAllByAttributes(array('pendaftaran_id'=>$id));
        $format = new CustomFormat;
        $modPemakaianBahan = new RIObatalkesPasienT;
        $modPasien = RIPasienM::model()->findByPK($modPendaftaran->pasien_id);
        $this->render($this->pathView.'_pemakaianBahan', 
                array('modPendaftaran'=>$modPendaftaran, 
                    'modBahan'=>$modBahan,
                    'modPemakaianBahan'=>$modPemakaianBahan,
                    'modPasien'=>$modPasien));
    }
    
    public function actionDetailTerapi($id)
    {
        $this->layout='//layouts/frameDialog';
        $modPendaftaran = RIPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
        $modTerapi = RIPenjualanresepT::model()->with('reseptur')->findAllByAttributes(array('pendaftaran_id'=>$id));
        $format = new CustomFormat;
        $modDetailTerapi = new RIPenjualanresepT();
        $modPasien = RIPasienM::model()->findByPK($modPendaftaran->pasien_id);
        $this->render($this->pathView.'_terapi', 
                array('modPendaftaran'=>$modPendaftaran, 
                    'modTerapi'=>$modTerapi,
                    'modDetailTerapi'=>$modDetailTerapi,
                    'modPasien'=>$modPasien));
    }
    
    public function actionDetailTindakan($id)
    {
        $this->layout='//layouts/frameDialog';
        $modPendaftaran = RIPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
        $modTindakan = RITindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
        $format = new CustomFormat;
        $modTindakanSearch = new RITindakanPelayananT('search');
        $modPasien = RIPasienM::model()->findByPK($modPendaftaran->pasien_id);
        $this->render($this->pathView.'_tindakan', 
                array('modPendaftaran'=>$modPendaftaran, 
                    'modTindakan'=>$modTindakan,
                    'modTindakanSearch'=>$modTindakanSearch,
                    'modPasien'=>$modPasien));
    }
    /**
     * actionDetailHasilLab = mnampilkan hasil lab sesuai dengan yang dilab
     * @param type $idPendaftaran
     * @param type $idPasien
     * @param type $idPasienMasukPenunjang
     */
    public function actionDetailHasilLab($idPendaftaran, $idPasien, $idPasienMasukPenunjang)
    {
        $this->layout = '//layouts/frameDialog';

        $cek_penunjang = LKPasienMasukPenunjangV::model()->findAllByAttributes(
            array('pendaftaran_id'=>$idPendaftaran)
        );

        $data_rad = array();
        if(count($cek_penunjang) > 1)
        {
            $masukpenunjangRad = LKPasienMasukPenunjangV::model()->findByAttributes(
                array(
                    'pendaftaran_id'=>$idPendaftaran,                        
                    'ruangan_id'=>Params::RUANGAN_ID_RAD
                )
            );
            if(count($masukpenunjangRad) > 0){
                $modHasilPeriksaRad = HasilpemeriksaanradV::model()->findAllByAttributes(
                    array(
                        'pasienmasukpenunjang_id'=>$masukpenunjangRad->pasienmasukpenunjang_id
                    ),
                    array(
                        'order'=>'pemeriksaanrad_urutan'
                    )
                );
            }
            $modHasilPeriksaRad = array();
            foreach($modHasilPeriksaRad as $i=>$val)
            {
                $data_rad[] = array(
                    'pemeriksaan'=>$val['pemeriksaanrad_nama'],
//                        'hasil'=>'Hasil Pemeriksaan ' . $val['pemeriksaanrad_nama'] . ' terlampir',
                    'hasil'=>'Hasil terlampir'
                );
            }

        }

        $masukpenunjang = LKPasienMasukPenunjangV::model()->findByAttributes(
            array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang)
        );

        $pemeriksa = PegawaiM::model()->findByPk($masukpenunjang->pegawai_id);

        $modHasilPeriksa = HasilpemeriksaanlabV::model()->findByAttributes(
            array(
                'pasienmasukpenunjang_id'=>$idPasienMasukPenunjang
            )
        );
        $query = "
            SELECT * FROM detailhasilpemeriksaanlab_t 
            JOIN pemeriksaanlab_m ON detailhasilpemeriksaanlab_t.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id 
            JOIN pemeriksaanlabdet_m ON detailhasilpemeriksaanlab_t.pemeriksaanlabdet_id = pemeriksaanlabdet_m.pemeriksaanlabdet_id 
            JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
            JOIN nilairujukan_m ON nilairujukan_m.nilairujukan_id = pemeriksaanlabdet_m.nilairujukan_id
            WHERE detailhasilpemeriksaanlab_t.hasilpemeriksaanlab_id = '". $modHasilPeriksa->hasilpemeriksaanlab_id ."'
            ORDER BY jenispemeriksaanlab_m.jenispemeriksaanlab_urutan, pemeriksaanlab_urutan, pemeriksaanlabdet_nourut
        ";
        $detailHasil = Yii::app()->db->createCommand($query)->queryAll();

        $data = array();
        $kelompokDet = null;
        $idx = 0;
        $temp = '';

        foreach ($detailHasil as $i => $detail)
        {
            $id_jenisPeriksa = $detail['jenispemeriksaanlab_id'];
            $jenisPeriksa = $detail['jenispemeriksaanlab_nama'];
            $kelompokDet = $detail['kelompokdet'];
            if($id_jenisPeriksa == '72')
            {
                $query = "
                    SELECT jenispemeriksaanlab_m.* FROM pemeriksaanlabdet_m
                    JOIN pemeriksaanlab_m ON pemeriksaanlabdet_m.pemeriksaanlab_id = pemeriksaanlab_m.pemeriksaanlab_id
                    JOIN jenispemeriksaanlab_m ON jenispemeriksaanlab_m.jenispemeriksaanlab_id = pemeriksaanlab_m.jenispemeriksaanlab_id
                    WHERE nilairujukan_id = ". $detail['nilairujukan_id'] ." AND pemeriksaanlab_m.jenispemeriksaanlab_id <> ". $id_jenisPeriksa ."
                ";
                $rec = Yii::app()->db->createCommand($query)->queryRow();
                $id_jenisPeriksa = $rec['jenispemeriksaanlab_id'];
                $jenisPeriksa = $rec['jenispemeriksaanlab_nama'];
            }

            if($temp != $kelompokDet)
            {
                $idx = 0;
            }

            $data[$id_jenisPeriksa]['tittle'] = $jenisPeriksa;
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['id'] = $id_jenisPeriksa;
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['nama'] = $jenisPeriksa;
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['kelompok'] = $kelompokDet;                
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['kelompok'] = $kelompokDet;
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan_det'] = $detail['pemeriksaanlab_nama'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['namapemeriksaan'] = $detail['namapemeriksaandet'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['id_pemeriksaan'] = $detail['nilairujukan_id'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['normal'] = $detail['nilairujukan_nama'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['metode'] = $detail['nilairujukan_metode'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['hasil'] = $detail['hasilpemeriksaan'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['nilairujukan'] = $detail['nilairujukan'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['satuan'] = $detail['hasilpemeriksaan_satuan'];
            $data[$id_jenisPeriksa]['grid'][$kelompokDet]['pemeriksaan'][$idx]['keterangan'] = $detail['nilairujukan_keterangan'];
            $temp = $kelompokDet;
            $idx++;
        }

        $this->render($this->pathView.'detailHasilLab',
            array(
               'modHasilPeriksa'=>$modHasilPeriksa,
               'masukpenunjang'=>$masukpenunjang,
               'pemeriksa'=>$pemeriksa,
               'data'=>$data,
               'data_rad'=>$data_rad
            )
        );
    }
    
    /**
     * actionDetailHasilRad = menampilkan hasil radiologi sesuai dengan rad
     * @param type $idPendaftaran
     * @param type $idPasien
     * @param type $idPasienMasukPenunjang
     * @param type $caraPrint
     */
    public function actionDetailHasilRad($idPendaftaran,$idPasien,$idPasienMasukPenunjang,$caraPrint='')
    {   
        $this->layout = '//layouts/frameDialog';
        $modPasienMasukPenunjang = ROPasienMasukPenunjangV::model()->findByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));
        $pemeriksa = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$modPasienMasukPenunjang->pegawai_id));
        $detailHasil = HasilpemeriksaanradT::model()->findAllByAttributes(array('pasienmasukpenunjang_id'=>$idPasienMasukPenunjang));

        $this->render($this->pathView.'detailHasilRad',array('detailHasil'=>$detailHasil,
                                           'masukpenunjang'=>$modPasienMasukPenunjang,
                                           'pemeriksa'=>$pemeriksa,
                                           'caraPrint'=>$caraPrint,
                                            ));
    }
    /**
     * Menampilkan riwayat hasil operasi
     * @param type $id
     */
    public function actionDetailHasilOperasi($id){
        $this->layout='//layouts/frameDialog';

        $modPasienMasukPenunjang = RIPasienMasukPenunjangT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));
        $modPendaftaran = PendaftaranT::model()->findByPk($id);
        $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
        $detailHasil = new RITindakanPelayananT('searchDetailTindakan');
        $detailHasil->unsetAttributes();
        $detailHasil->pendaftaran_id = $id;
        $detailHasil->ruangan_id = Params::RUANGAN_ID_IBS;

        $this->render($this->pathView.'detailHasilOperasi',array('detailHasil'=>$detailHasil,
                                           'modPasienMasukPenunjang'=>$modPasienMasukPenunjang,
                                           'modPendaftaran'=>$modPendaftaran,
                                           'modPasien'=>$modPasien,
                                            ));
    }
    /**
     * menampilkan riwayat konsul gizi
     * disesuaikan dengan gizi/controllers/DaftarPasienController
     * @param type $id
     */
    public function actionDetailKonsulGizi($id){
        Yii::import('gizi.models.GZPendaftaranT');
        Yii::import('gizi.models.GZTindakanPelayananT');
        Yii::import('gizi.models.GZTindakanPelayananT');
        Yii::import('gizi.models.GZPasienM');
        $this->layout='//layouts/frameDialog';
        $modPendaftaran = GZPendaftaranT::model()->with('carabayar','penjamin')->findByPk($id);
        $modTindakan = GZTindakanPelayananT::model()->with('daftartindakan')->findAllByAttributes(array('pendaftaran_id'=>$id));
        $format = new CustomFormat;
        $modTindakanSearch = new GZTindakanPelayananT('search');
        $modPasien = GZPasienM::model()->findByPK($modPendaftaran->pasien_id);
        $this->render('gizi.views/_periksaDataPasien/_konsultasiGizi', 
                array('modPendaftaran'=>$modPendaftaran, 
                    'modTindakan'=>$modTindakan,
                    'modTindakanSearch'=>$modTindakanSearch,
                    'modPasien'=>$modPasien));
    }
}
?>
