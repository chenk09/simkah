
<?php

class PenilaianpegawaiTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
                public $defaultAction = 'admin';
                protected $pathView = 'kepegawaian.views.penilaianpegawaiT.';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary','detailPenilaian','informasi','Detail'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
                public function actionDetail($id)
                {
                    $modScoringdetail = ScoringdetailT::model()->findByAttributes(array('personalscoring_id'=>$id));
                    $this->render($this->pathView.'detail',array(
                        'model'=>$this->loadModel($id),
                        'modScoringdetail'=>$modScoringdetail,
                    ));
                }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPenilaianpegawaiT;
                $model->tglpenilaian = date('Y-m-d H:i:s');
                $model->periodepenilaian = date('Y-m-d');
                $model->sampaidengan = date('Y-m-d');
                $model->kejujuran = 0;
                $model->ketaatan = 0;
                $model->jumlahpenilaian = 0;
                $model->nilairatapenilaian = 0;
                $model->performanceindex = 0;
                $model->kesetiaan = 0;
                $model->prestasikerja = 0;
                $model->tanggungjawab = 0;
                $model->kerjasama = 0;
                $model->prakarsa = 0;
                $model->kepemimpinan = 0;
                $modPegawai = new KPRegistrasifingerprint();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KPPenilaianpegawaiT']))
		{
			$model->attributes=$_POST['KPPenilaianpegawaiT'];
                        $model->pegawai_id = $_POST['KPRegistrasifingerprint']['pegawai_id'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->refresh();
//				$this->redirect(array('view','id'=>$model->penilaianpegawai_id));
                        }
		}

		$this->render($this->pathView.'create',array(
			'model'=>$model, 'modPegawai'=>$modPegawai,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KPPenilaianpegawaiT']))
		{
			$model->attributes=$_POST['KPPenilaianpegawaiT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->refresh();
//				$this->redirect(array('create'));
                        }
		}

		$this->render($this->pathView.'update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        //if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KPPenilaianpegawaiT');
		$this->render($this->pathView.'index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new KPPenilaianpegawaiT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KPPenilaianpegawaiT']))
			$model->attributes=$_GET['KPPenilaianpegawaiT'];

		$this->render($this->pathView.'admin',array(
			'model'=>$model,
		));
	}
        
                public function actionInformasi()
                {
                    $model = new KPPenilaianpegawaiT('seach');
                    $format = new CustomFormat;
                    $model->unsetAttributes();
                    $model->periodepenilaian = date('Y-m-d 00:00:00');
                    $model->sampaidengan = date('Y-m-d 23:59:59');
                    if (isset($_GET['KPPenilaianpegawaiT'])) {
                        if ($_GET['KPPenilaianpegawaiT']['tglpenilaian'] > '0') {
                            $model->tglpenilaian = $format->formatDateTimeMediumForDB($_REQUEST['KPPenilaianpegawaiT']['tglpenilaian']);
                        }
                        if ($_GET['KPPenilaianpegawaiT']['periodepenilaian'] > '0') {
                            $model->periodepenilaian = $format->formatDateTimeMediumForDB($_REQUEST['KPPenilaianpegawaiT']['periodepenilaian']);
                        }
                        if ($_GET['KPPenilaianpegawaiT']['sampaidengan'] > '0') {
                        $model->sampaidengan = $format->formatDateTimeMediumForDB($_REQUEST['KPPenilaianpegawaiT']['sampaidengan']);
                        }
                        $model->nama_pegawai = $_REQUEST['KPPenilaianpegawaiT']['nama_pegawai'];
                    }
                    $this->render($this->pathView.'informasi',array(
                            'model'=>$model,
                    ));
                }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=KPPenilaianpegawaiT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kppenilaianpegawai-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new KPPenilaianpegawaiT;
            $model->attributes=$_REQUEST['KPPenilaianpegawaiT'];
            $judulLaporan='Data KPPenilaianpegawaiT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionDetailPenilaian($idPegawai) {
                    
                   $idPegawai = 24;
                   $this->render($this->pathView.'view',array(
			'model'=>$this->loadModel($idPegawai),
		));
                   
                }
}
