<?php

class PersonalscoringTController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','Informasi','detail'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
	public function actionDetail($id)
	{
		$this->render('detail',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PersonalscoringT;
                                $modIndexing = new IndexingM('search');
                                $modScoringdetail = new ScoringdetailT('search');
                                $modPegawai = new PegawaiM;
                                $modLoginpemakai = PegawaiM::model()->findByPK(Yii::app()->user->id);
                                $format = new CustomFormat;
                                $transaction = Yii::app()->db->beginTransaction();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['savescoringdetail']))
		{
                    $model->attributes = $_POST['PersonalscoringT'];
                    $modPegawai->attributes = $_POST['PegawaiM'];
                    $modPegawai->pegawai_id = $_POST['PegawaiM']['pegawai_id'];
                    $model->pegawai_id = $_POST['PegawaiM']['pegawai_id'];
                    $model->create_time = date('Y-m-d H:i:s');
                    $model->create_loginpemakai_id = Yii::app()->user->id;
                    $model->create_ruangan = Yii::app()->user->ruangan_id;
                    $model->tglscoring = $format->formatDateTimeMediumForDB($_POST['PersonalscoringT']['tglscoring']);
                    $model->periodescoring = $format->formatDateTimeMediumForDB($_POST['PersonalscoringT']['periodescoring']);
                    $model->sampaidengan = $format->formatDateTimeMediumForDB($_POST['PersonalscoringT']['sampaidengan']);
                    try{
                        if ($model->save()) {
                            $jumlah = 0;
                            foreach ($_POST['ScoringdetailT'] as $data => $value) {
                                $modScoringdetail = new ScoringdetailT;
                                $modScoringdetail->personalscoring_id = $model->personalscoring_id;
                                $modScoringdetail->attributes = $value;
                                if ($modScoringdetail->save()){
                                    $jumlah++;
                                }
                            }
                            if (($jumlah>0) && ($jumlah == count($_POST['ScoringdetailT']))){
                                $transaction->commit();
                                echo Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->refresh();;
                            }else{
                                throw new Exception("Transaksi gagal");
                            }
                            
                        }
                    }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data Gagal disimpan. ".MyExceptionMessage::getMessage($exc,true));
                    }
		}

		$this->render('create',array(
			'model'=>$model,
                                                'modIndexing'=>$modIndexing,
                                                'modScoringdetail'=>$modScoringdetail,
                                                'modPegawai'=>$modPegawai,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['PersonalscoringT']))
		{
			$model->attributes=$_POST['PersonalscoringT'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->personalscoring_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PersonalscoringT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
        
                public function actionInformasi()
                {
                    $model = new PersonalscoringT;
                    $format = new CustomFormat;
                    $model->unsetAttributes();
                    $model->periodescoring = date('Y-m-d 00:00:00');
                    $model->sampaidengan = date('Y-m-d 23:59:59');
                    
                    if (isset($_GET['PersonalscoringT'])) {
                        if ($_GET['PersonalscoringT']['tglscoring'] > '0') {
                            $model->tglscoring = $format->formatDateTimeMediumForDB($_REQUEST['PersonalscoringT']['tglscoring']);
                        }
                        if ($_GET['PersonalscoringT']['periodescoring'] > '0') {
                            $model->periodescoring = $format->formatDateTimeMediumForDB($_REQUEST['PersonalscoringT']['periodescoring']);
                        }
                        if ($_GET['PersonalscoringT']['sampaidengan'] > '0') {
                            $model->sampaidengan = $format->formatDateTimeMediumForDB($_REQUEST['PersonalscoringT']['sampaidengan']);
                        }
                    }
                    $this->render('informasi',array(
                        'model'=>$model,
                    ));
                }

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PersonalscoringT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PersonalscoringT']))
			$model->attributes=$_GET['PersonalscoringT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=PersonalscoringT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='personalscoring-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
