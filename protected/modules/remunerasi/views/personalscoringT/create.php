<?php
$this->breadcrumbs=array(
	'Personalscoring Ts'=>array('index'),
	'Create',
);

//$this->menu=array(
//	array('label'=>'List PersonalscoringT', 'url'=>array('index')),
//	array('label'=>'Manage PersonalscoringT', 'url'=>array('admin')),
//);
?>

<!--<h1>Create PersonalscoringT</h1>-->


<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'id'=>'personalscoring-t-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'htmlOptions'=>array('enctype'=>'multipart/form-data'),
                'focus'=>'#',
)); ?>
                <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
	<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

	<?php echo $form->errorSummary($model); ?>
    <table class="table">
        <tr>
            <!-- ====================== kolom ke-1 ============================================== -->
            <td>
                <?php echo $form->textFieldRow($modPegawai,'nomorindukpegawai',array('readonly'=>true,'id'=>'NIP')); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Nama pegawai','namapegawai',array('class'=>'control-label')) ?>
                    <div class="controls">
                            <?php echo $form->hiddenField($modPegawai,'pegawai_id',array('readonly'=>true,'id'=>'pegawai_id')) ?>
                            <?php $this->widget('MyJuiAutoComplete',array(
                                        'model'=>$modPegawai, 
//                                        'name'=>'namapegawai',
                                        'attribute'=>'nama_pegawai',
                                        'value'=>$namapegawai,
                                        'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/Pegawairiwayat'),
                                        'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 4,
                                           'focus'=> 'js:function( event, ui ) {
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#namapegawai").val( ui.item.nama_pegawai );
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                $("#pegawai_id").val( ui.item.value );
                                                $("#NIP").val( ui.item.nomorindukpegawai);
                                                $("#tempatlahir_pegawai").val( ui.item.tempatlahir_pegawai);
                                                $("#tgl_lahirpegawai").val( ui.item.tgl_lahirpegawai);
                                                $("#namapegawai").val( ui.item.nama_pegawai);
                                                $("#jeniskelamin").val( ui.item.jeniskelamin);
                                                $("#statusperkawinan").val( ui.item.statusperkawinan);
                                                $("#jabatan").val( ui.item.jabatan_nama);
                                                $("#pangkat").val( ui.item.pangkat_nama);
                                                $("#pendidikan").val( ui.item.pendidikan_nama);
                                                $("#kategoripegawaiasal").val( ui.item.kategoripegawaiasal);
                                                $("#kelompokpegawai").val( ui.item.kelompokpegawai);
                                                $("#alamat_pegawai").val( ui.item.alamat_pegawai);
                                                $("#gajipokok").val( ui.item.gajipokok);
                                                totalbobot();
                                                totalscore();
                                                if(ui.item.photopegawai != null){
                                                    $("#photo_pasien").attr(\'src\',\''.Params::urlPegawaiTumbsDirectory().'kecil_\'+ui.item.photopegawai);
                                                } else {
                                                    $("#photo_pasien").attr(\'src\',\'http://localhost/simrs/data/images/pasien/no_photo.jpeg\');
                                                }
                                                return false;
                                            }',

                                        ),
                                        'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2 '),
                                        'tombolDialog'=>array('idDialog'=>'dialogPegawai','idTombol'=>'tombolPasienDialog'),
                            )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPegawai,'tempatlahir_pegawai',array('readonly'=>true,'id'=>'tempatlahir_pegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai, 'tgl_lahirpegawai',array('readonly'=>true,'id'=>'tgl_lahirpegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai, 'jeniskelamin',array('readonly'=>true,'id'=>'jeniskelamin')); ?>
                <?php echo $form->textFieldRow($modPegawai,'statusperkawinan',array('readonly'=>true,'id'=>'statusperkawinan')); ?>
            </td>
            <!-- =========================== kolom ke 2 ====================================== -->
            <td>
<!--                <div class="control-group">
                    <?php //echo $form->labelEx($model, 'jabatan_id',array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php //echo $form->textFieldRow($model,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                    </div>
                </div>-->
                <?php echo $form->textFieldRow($modPegawai,'jabatan_id',array('readonly'=>true,'id'=>'jabatan')); ?>
                <?php echo $form->textFieldRow($modPegawai,'pangkat_id',array('readonly'=>true,'id'=>'pangkat')); ?>
                <?php echo $form->textFieldRow($modPegawai,'pendidikan_id',array('readonly'=>true,'id'=>'pendidikan')); ?>
                <?php echo $form->textFieldRow($modPegawai,'gajipokok',array('readonly'=>true,'id'=>'gajipokok')); ?>
                <?php echo $form->textFieldRow($modPegawai,'kategoripegawai',array('readonly'=>true,'id'=>'kategoripegawai')); ?>
                <?php echo $form->textFieldRow($modPegawai,'kategoripegawaiasal',array('readonly'=>true,'id'=>'kategoripegawaiasal')); ?>
                <?php echo $form->textFieldRow($modPegawai,'kelompokpegawai_id',array('readonly'=>true,'id'=>'kelompokpegawai')); ?>
                <?php // echo $form->textFieldRow($modPegawai,'pendidikan_id',array('readonly'=>true,'id'=>'pendidikan')); ?>
                <?php //echo $form->textAreaRow($model,'alamat_pegawai',array('readonly'=>true,'id'=>'alamat_pegawai')); ?>
            </td>
            <td>
                <?php 
                    if(!empty($modPegawai->photopegawai)){
                        echo CHtml::image(Params::urlPegawaiTumbsDirectory().'kecil_'.$modPegawai->photopegawai, 'photo pasien', array('id'=>'photo_pasien','width'=>150));
                    } else {
                        echo CHtml::image(Params::urlPegawaiDirectory().'no_photo.jpeg', 'Photo Pegawai', array('id'=>'photo_pasien','width'=>150));
                    }
                ?> 
            </td>
        </tr>
    </table>
        <div class="control-group">
                <?php echo $form->labelEx($model,'tglscoring',array('class'=>'control-label')); ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'tglscoring',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                )); 
                        ?>
            </div>
        </div>
        <div class="control-group">
                <?php echo $form->labelEx($model,'periodescoring',array('class'=>'control-label')); ?>
            <div class="controls">
                <div style="float:left;">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'periodescoring',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2',),
                )); 
                        ?>
                </div>
                <?php echo $form->labelEx($model,'sampaidengan',array('class'=>'control-label','style'=>'margin-right:25px;')); ?>
                <div>
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$model,
                                        'attribute'=>'sampaidengan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker2'),
                )); 
                        ?>
                </div>
            </div>
        </div>
<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'scoringdetail-t-grid',
	'dataProvider'=>$modIndexing->search(),
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
	'columns'=>array(
                                array(
                                    'header'=>'No',
                                    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                                ),
                                array(
                                    'header'=>'Kelompok',
                                    'value'=>'$data->kelrem->kelrem_nama',
                                    'footer'=>'<b>Total</b>:',
                                ),
                                array(
                                    'header'=>'Objek',
                                    'value'=>'$data->indexing_nama',
                                ),
                                array(
                                    'header'=>'Index',
                                    'value'=>'$data->indexing_nilai',
                                    'footer'=>$modIndexing->getTotalindex(),
                                ),
                                array(
                                    'header'=>'Bobot',
                                    'type'=>'raw',
                                    'value'=>'CHtml::textField(\'ScoringdetailT[][ratebobot_personal]\',$data->kelrem->kelrem_rate,array(\'class\'=>\'span1 bobot\', \'id\'=>\'ratebobot_personal\', \'onkeyup\'=>\'scoring(this);\', \'onfocus\'=>\'renameinput(this);\'))',
                                    'footer'=>$modIndexing->getTotalbobot(),
                                ),
                                array(
                                    'header'=>'Score',
                                    'type'=>'raw',
                                    'value'=>'CHtml::textField(\'ScoringdetailT[][score_personal]\',\' \',array(\'class\'=>\'span1 score\', \'id\'=>\'score_personal\', \'readonly\'=>true,))',
                                    'footer'=>$modIndexing->getTotalscore(),
                                ),
                                array(
                                    'type'=>'raw',
                                    'value'=>'CHtml::hiddenField(\'kelrem_id\',$data->kelrem_id,array(\'class\'=>\'span1\', \'id\'=>\'kelrem_id\', \'value\'=>$data->kelrem_id))',
                                    'htmlOptions'=>array('style'=>'display:none;'),
                                    'footer'=>'<b class="hide"></b>',
                                ),
                                array(
                                    'type'=>'raw',
                                    'value'=>'CHtml::hiddenField(\'ScoringdetailT[][indexing_id]\',$data->indexing_id,array(\'class\'=>\'span1\', \'id\'=>\'indexing_id\', \'value\'=>$data->indexing_id))',
                                    'htmlOptions'=>array('style'=>'display:none;'),
                                    'footer'=>'<b class="hide"></b>',
                                ),
                                array(
                                    'type'=>'raw',
                                    'value'=>'CHtml::hiddenField(\'ScoringdetailT[][kelrem_id]\',$data->kelrem_id,array(\'class\'=>\'span1\', \'id\'=>\'kelrem_id\', \'value\'=>$data->kelrem_id))',
                                    'htmlOptions'=>array('style'=>'display:none;'),
                                    'footer'=>'<b class="hide"></b>',
                                ),
                                array(
                                    'type'=>'raw',
                                    'value'=>'CHtml::hiddenField(\'indexing_nilai\',$data->indexing_nilai,array(\'class\'=>\'span1\', \'id\'=>\'indexing_nilai\', \'value\'=>$data->indexing_nilai))',
                                    'htmlOptions'=>array('style'=>'display:none;'),
                                    'footer'=>'<b class="hide"></b>',
                                ),
//		'ratebobot_personal',
//		'score_personal',
		/*
		'score_personal',
		*/
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>

	<div class="form-actions">
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                 Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                                array('class'=>'btn btn-primary', 'type'=>'submit', 
                                                    'onKeypress'=>'return formSubmit(this,event)',
                                                    'id'=>'btn_simpan',
                                                    'name'=>'savescoringdetail',
//                                                    'onclick'=>'do_upload()',
                                                   )); ?>
                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                        Yii::app()->createUrl($this->module->id.'/'.pegawaiM.'/admin'), 
                        array('class'=>'btn btn-danger',
                              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
	<?php
                        $content = $this->renderPartial('../tips/transaksi',array(),true);
                        $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
        </div>

<?php $this->endWidget(); ?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#scoringdetail-t-grid_c6").hide();
        $("#scoringdetail-t-grid_c7").hide();
        $("#scoringdetail-t-grid_c8").hide();
        $("#scoringdetail-t-grid_c9").hide();
        $(".hide").parents("td").hide();
        autoscoring();
        totalbobot();
        totalscore();
    
    });
    
    function scoring(obj) {
        bobot = $(obj).val();
        nilaiindexing = $(obj).parents("tr").children("td").children("#indexing_nilai").val();
        scorepersonal = nilaiindexing * bobot;
        $(obj).parents("tr").children("td").children("#score_personal").val(scorepersonal);
        totalbobot();
        totalscore();
    }
    
    function autoscoring() {
        $(".bobot").each(function() {
            renameinput(this);
            bobot = $(this).val();
            nilaiindexing = $(this).parents("tr").children("td").children("#indexing_nilai").val();
            kelrem_id =  $(this).parents("tr").children("td").children("#kelrem_id").val();
            gajipokok = $("#gajipokok").val();
            if (kelrem_id == 1) {
                scorepersonal = gajipokok/nilaiindexing * bobot;
            } else {
                scorepersonal = nilaiindexing * bobot;
            }
            $(this).parents("tr").children("td").children("#score_personal").val(scorepersonal);
        });
    }
    
    function totalbobot() {
        var totalbobot = 0;
        $(".bobot").each(function() {
            totalbobot += parseFloat($(this).val());
        });
        $("#totalbobot").val(totalbobot);
    }
    
    function totalscore() {
        var totalscore = 0;
        $(".score").each(function() {
            totalscore += parseFloat($(this).val());
        });
        $("#totalscore").val(totalscore);
    }

    
        index = 1;
    function renameinput(obj) {
        $(obj).parents("tr").find('[name*="ScoringdetailT"]').each(function() {
            var input = $(this).attr('name');
            var data = input.split('ScoringdetailT[]');
            $(this).attr('name','ScoringdetailT['+index+']'+data[1]);
        });
        index++;
    };
JS;
Yii::app()->clientScript->registerScript('validasi', $js, CClientScript::POS_HEAD);
?>
<?php
/**
 * Dialog untuk nama Pegawai
 */
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPegawai',
    'options'=>array(
        'title'=>'Daftar Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM;
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pegawai-m-grid',
	'dataProvider'=>$modPegawai->search(),
//	'filter'=>$modRencanaKebFarmasi,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                        "id" => "selectPegawai",
                                        "href"=>"",
                                        "onClick" => "
                                                      $(\"#NIP\").val(\"$data->nomorindukpegawai\");
                                                      $(\"#pegawai_id\").val(\"$data->pegawai_id\");
                                                      $(\"#PegawaiM_nama_pegawai\").val(\"$data->nama_pegawai\");
                                                      $(\"#'.CHtml::activeId($model, 'nama_pegawai').'\").val(\"$data->nama_pegawai\");
                                                      $(\"#tempatlahir_pegawai\").val(\"$data->tempatlahir_pegawai\");
                                                      $(\"#tgl_lahirpegawai\").val(\"$data->tgl_lahirpegawai\");
                                                      $(\"#jabatan\").val(\"".$data->jabatan->jabatan_nama."\");
                                                      $(\"#pangkat\").val(\"".$data->pangkat->pangkat_nama."\");
                                                      $(\"#kategoripegawai\").val(\"$data->kategoripegawai\");
                                                      $(\"#kategoripegawaiasal\").val(\"$data->kategoripegawaiasal\");
                                                      $(\"#kelompokpegawai\").val(\"".$data->kelompokpegawai->kelompokpegawai_nama."\");
                                                      $(\"#pendidikan\").val(\"".$data->pendidikan->pendidikan_nama."\");
                                                      $(\"#jeniskelamin\").val(\"$data->jeniskelamin\");
                                                      $(\"#statusperkawinan\").val(\"$data->statusperkawinan\");
                                                      $(\"#alamat_pegawai\").val(\"$data->alamat_pegawai\");
                                                      $(\"#gajipokok\").val(\"$data->gajipokok\");
                                                      totalbobot();
                                                      totalscore();
                                                      if(\"$data->photopegawai\" != \"\"){
                                                            $(\"#photo_pasien\").attr(\'src\',\"'.Params::urlPegawaiTumbsDirectory().'kecil_$data->photopegawai\");
                                                      } else {
                                                            $(\"#photo_pasien\").attr(\'src\',\"http://localhost/simrs/data/images/pasien/no_photo.jpeg\");
                                                      }
                                                      $(\"#dialogPegawai\").dialog(\"close\");    
                                                      return false;
                                            "))',
                    ),
                'nomorindukpegawai',
                'nama_pegawai',
                'tempatlahir_pegawai',
                'tgl_lahirpegawai',
                'jeniskelamin',
                'statusperkawinan',
                'jabatan.jabatan_nama',
                'alamat_pegawai',
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
?>