<legend class="rim2">Informasi Personal Scoring</legend>
<?php
$this->breadcrumbs=array(
	'Personalscoring Ts'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('#search-t-form').submit(function(){
	$.fn.yiiGridView.update('personalscoring-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'personalscoring-t-grid',
                'itemsCssClass'=>'table table-bordered table-striped table-condensed',
	'dataProvider'=>$model->searchInformasi(),
	'columns'=>array(
//		'personalscoring_id',
		'pegawai.nama_pegawai',
//		'penilaianpegawai_id',
		'tglscoring',
		'periodescoring',
		'sampaidengan',
		/*
		'gajipokok',
		'jabatan',
		'pendidikan',
		'totalscore',
		'create_time',
		'update_time',
		'create_loginpemakai_id',
		'update_loginpemakai_id',
		'create_ruangan',
		*/
		array(
                                    'header'=>'Detail',
                                    'type'=>'raw',
                                    'value'=>'CHtml::link(\'<i class="icon-eye-open"></i>\',Yii::app()->createUrl(\'remunerasi/PersonalscoringT/detail&id=\'.$data->personalscoring_id))',
                                ),
	),
)); ?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
                'type'=>'horizontal',
                'id'=>'search-t-form',
	'method'=>'get',
)); ?>
<fieldset>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($model,'tglscoring',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                      <?php   $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglscoring',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                        )); ?>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($model,'periodescoring',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                      <?php   $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'periodescoring',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                        )); ?>
                                    </div>
                                </div>
                                <div class="control-group ">
                                    <?php echo $form->labelEx($model,'sampaidengan',array('class'=>'control-label')); ?>
                                    <div class="controls">
                                      <?php   $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'sampaidengan',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3'),
                                        )); ?>
                                    </div>
                                </div>
            </td>
            <td>
		<?php echo $form->textFieldRow($model,'nama_pegawai'); ?>
                                <?php echo $form->dropDownListRow($model,'jabatan',CHtml::listData($model->getJabatanItems(),'jabatan_id','jabatan_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
		<?php echo $form->dropDownListRow($model,'pendidikan',CHtml::listData($model->getPendidikanItems(),'pendidikan_id','pendidikan_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
            </td>
    </tr>
</table>
</fieldset>

                                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>&nbsp;
                                <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),Yii::app()->createUrl($this->module->id.'/'.personalscoringT.'/informasi'), array('class'=>'btn btn-danger','onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
                                <?php 
                                   $content = $this->renderPartial('../tips/informasi',array(),true);
                                   $this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
                                ?>

<?php $this->endWidget(); ?>