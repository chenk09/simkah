 <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'sainstalasi-m-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#SAInstalasiM_instalasi_nama',
        'htmlOptions'=>array('enctype'=>'multipart/form-data','onKeyPress'=>'return disableKeyPress(event)'),
)); ?>




    <div class="control-group">
        <?php // echo CHtml::label('Aktifasi Instalasi','',array('class'=>'control-label')); ?>
        <div class="controls">
             <?php 
                    $instalasifalse = array();
                    $modInstalasifalse = InstalasiM::model()->findAll('instalasi_aktif=TRUE');
                     foreach($modInstalasifalse as $tampilInstalasi){
                        $instalasifalse[] = $tampilInstalasi['instalasi_id'];
                    } 
                   $this->widget('application.extensions.emultiselectforinstalasi.EMultiSelectforinstalasi',array('sortable'=>true, 'searchable'=>true));
                    echo CHtml::listBox('instalasi_nonaktif[]',$instalasifalse,CHtml::listData(InstalasiM::model()->findAll(),'instalasi_id', 'instalasi_nama'),array('multiple'=>'multiple','key'=>'instalasi_id', 'class'=>'multiselect','style'=>'width:500px;height:150px'));
              ?>
        </div>
    </div>
    <div class="form-actions">
                            <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                                                 Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                            array('class'=>'btn btn-primary', 'type'=>'submit','id'=>'btn_simpan','name'=>'submitInstalasi')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                    Yii::app()->createUrl($this->module->id.'/'.instalasiM.'/admin'), 
                    array('class'=>'btn btn-danger',
                          'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
    </div>

<?php $this->endWidget(); ?>