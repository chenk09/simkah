<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'saindexing-m-search',
        'type'=>'horizontal',
)); ?>
		<?php //echo $form->textFieldRow($model,'indexing_id'); ?>
		<?php echo $form->textFieldRow($model,'kelrem_id'); ?>
		<?php //echo $form->textFieldRow($model,'indexing_urutan'); ?>
		<?php echo $form->textFieldRow($model,'indexing_nama',array('size'=>60,'maxlength'=>100)); ?>
		<?php //echo $form->textFieldRow($model,'indexing_singk',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->textFieldRow($model,'indexing_nilai'); ?>
		<?php echo $form->checkBoxRow($model,'indexing_aktif',array('checked'=>true)); ?>

	<div class="form-actions">
		                <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	</div>

<?php $this->endWidget(); ?>
