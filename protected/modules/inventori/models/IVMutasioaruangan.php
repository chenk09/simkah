<?php
class IVMutasioaruangan extends MutasioaruanganT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    public $tick;
    public $data;
    public $jumlah;
    public $obatalkes_nama;
    public $harganettosatuan;
    public $hargajualsatuan;
    public $jummutasi;
    public $totalharga;
    public $ruangantujuan_nama;
    public $satuankecil_nama;
    public $sumberdana_nama;
    public $noPemesanan;
    public $noTerima;
    public $statusTerima;
    
    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $ruanganAsal = $this->ruanganasal_id;
        $ruanganTujuan = $this->ruangantujuan_id;
        //Filter Ruangan Asal dan Tujuan
        if(Yii::app()->user->getState('ruangan_id') == Params::RUANGAN_ID_GUDANG_FARMASI){
            $criteria->compare('ruanganasal_id', $ruanganAsal);
            //Gudang bisa akses informasi dari dan ke ruangan mana saja
        }else{
            if(empty($this->ruanganasal_id)){
                $ruanganAsal = Yii::app()->user->getState('ruangan_id');
            }else{
                if($this->ruanganasal_id != Yii::app()->user->getState('ruangan_id')){
                    $ruanganTujuan = Yii::app()->user->getState('ruangan_id');
                }else{
                    $ruanganAsal = Yii::app()->user->getState('ruangan_id');
                }
            }
        }
        if(!empty($this->tglAwal) && !empty($this->tglAkhir))
            $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
            $criteria->compare('t.ruanganasal_id', $ruanganAsal);
            $criteria->compare('t.ruangantujuan_id', $ruanganTujuan);
        
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchInformasiMutasiMasuk() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('pesanobatalkes', 'terimamutasi');
        $ruanganAsal = $this->ruanganasal_id;
        $ruanganTujuan = Yii::app()->user->getState('ruangan_id');
        if(!empty($this->tglAwal) && !empty($this->tglAkhir))
            $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
        $criteria->compare('LOWER(terimamutasi.noterimamutasi)', strtolower($this->noTerima), true);
        $criteria->compare('LOWER(pesanobatalkes.nopemesanan)', strtolower($this->noPemesanan), true);
        $criteria->compare('t.ruanganasal_id', $ruanganAsal);
        $criteria->compare('t.ruangantujuan_id', $ruanganTujuan);
        if($this->statusTerima > 0)
            $criteria->addCondition('t.terimamutasi_id >= '.$this->statusTerima);
        else if($this->statusTerima == 0 && $this->statusTerima != NULL)
            $criteria->addCondition('t.terimamutasi_id IS NULL');
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchInformasiMutasiKeluar() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        $criteria->with = array('pesanobatalkes');
        $ruanganAsal = Yii::app()->user->getState('ruangan_id');
        $ruanganTujuan = $this->ruangantujuan_id;
        if(!empty($this->tglAwal) && !empty($this->tglAkhir))
            $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
        $criteria->compare('LOWER(pesanobatalkes.nopemesanan)', strtolower($this->noPemesanan), true);
        $criteria->compare('t.ruanganasal_id', $ruanganAsal);
        $criteria->compare('t.ruangantujuan_id', $ruanganTujuan);
        if($this->statusTerima > 0)
            $criteria->addCondition('t.terimamutasi_id >= '.$this->statusTerima);
        else if($this->statusTerima == 0 && $this->statusTerima != NULL)
            $criteria->addCondition('t.terimamutasi_id IS NULL');
        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchLaporan() {
        
        $criteria = new CDbCriteria;
        
        $criteria->join = 'JOIN mutasioadetail_t md ON md.mutasioaruangan_id=t.mutasioaruangan_id JOIN obatalkes_m o ON o.obatalkes_id=md.obatalkes_id JOIN ruangan_m r ON r.ruangan_id=t.ruangantujuan_id';
        $criteria->select = 't.nomutasioa as nomutasioa, o.obatalkes_nama as obatalkes_nama, t.terimamutasi_id, t.tglmutasioa, 
                             md.harganetto as harganettosatuan, md.hargajualsatuan as hargajualsatuan, md.jmlmutasi as jummutasi, md.totalharga as totalharga, r.ruangan_nama as ruangantujuan';
        
        $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
        $criteria->compare('ruanganasal_id', Yii::app()->user->getState('ruangan_id'));
        

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function searchLaporanPrint() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

       $criteria = new CDbCriteria;
        
        $criteria->join = 'JOIN mutasioadetail_t md ON md.mutasioaruangan_id=t.mutasioaruangan_id JOIN obatalkes_m o ON o.obatalkes_id=md.obatalkes_id JOIN ruangan_m r ON r.ruangan_id=t.ruangantujuan_id';
        $criteria->select = 't.nomutasioa as nomutasioa, o.obatalkes_nama as obatalkes_nama, t.terimamutasi_id, t.tglmutasioa, 
                             md.harganetto as harganettosatuan, md.hargajualsatuan as hargajualsatuan, md.jmlmutasi as jummutasi, md.totalharga as totalharga, r.ruangan_nama as ruangantujuan';
        $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
        $criteria->compare('ruanganasal_id', Yii::app()->user->getState('ruangan_id'));
        

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria, 'pagination'=>false,
                ));
    }
    /**
     * searchLaporanMutasiIntern untuk grid di:
     * 1. laporan mutasi obat alkes intern
     * 2. Print Laporannya
     * @return \CActiveDataProvider
     */
    public function searchLaporanMutasiIntern() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $format = new CustomFormat();
       $criteria = new CDbCriteria;
        
        $criteria->join = 'JOIN mutasioadetail_t md ON md.mutasioaruangan_id=t.mutasioaruangan_id 
            JOIN obatalkes_m o ON o.obatalkes_id=md.obatalkes_id 
            JOIN sumberdana_m sd ON md.sumberdana_id=sd.sumberdana_id 
            JOIN satuankecil_m s ON md.satuankecil_id=s.satuankecil_id 
            JOIN ruangan_m r ON r.ruangan_id=t.ruangantujuan_id';
        $criteria->select = 't.mutasioaruangan_id, t.nomutasioa as nomutasioa, o.obatalkes_nama as obatalkes_nama, t.terimamutasi_id, t.tglmutasioa, 
                             md.harganetto as harganettosatuan, md.hargajualsatuan as hargajualsatuan, md.jmlmutasi as jummutasi, md.totalharga as totalharga, r.ruangan_nama as ruangantujuan_nama, s.satuankecil_nama, sd.sumberdana_nama';
        $this->tglAwal = $format->formatDateTimeMediumForDB($this->tglAwal);
        $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);
        $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nomutasioa)', strtolower($this->nomutasioa), true);
        $criteria->compare('ruangantujuan_id', $this->ruangantujuan_id);
        $criteria->compare('ruanganasal_id', Yii::app()->user->getState('ruangan_id'));

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria, 'pagination'=>false,
                ));
    }    
     
    public function searchGrafik() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
        
        $criteria->join = "JOIN ruangan_m r on r.ruangan_id=t.ruangantujuan_id";
        $criteria->select = 'r.ruangan_nama as data, count(t.nomutasioa) as jumlah, t.nomutasioa';
        $criteria->group = 't.nomutasioa, r.ruangan_nama';
        $criteria->addBetweenCondition('date(tglmutasioa)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('ruanganasal_id', Yii::app()->user->getState('ruangan_id'));
        

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function getRuanganItems()
        {
            return RuanganM::model()->findAllByAttributes(array('ruangan_aktif'=>true), array('order'=>'ruangan_nama ASC')) ;
        }
        
        public function getNamaModel(){
            return __CLASS__;
        }

}