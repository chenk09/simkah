<?php

class IVPesanobatalkes extends PesanobatalkesT {

    public $ceklis;    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    /**
     * searchInformasi digunakan untuk query informasi pemesanan obat penerimaan dan pemesanan ke luar
     * @return \CActiveDataProvider
     */
    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.
        $instalasi_akses = Yii::app()->user->getState('instalasi_id');
        $ruangan_akses = Yii::app()->user->getState('ruangan_id');
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('date(tglpemesanan)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(nopemesanan)', strtolower($this->nopemesanan), true);
        if($instalasi_akses == Params::ID_INSTALASI_FARMASI){
            $criteria->compare('ruanganpemesan_id', $this->ruanganpemesan_id);
            if(!empty($this->ruangan_id))
                $criteria->compare('ruangan_id', $this->ruangan_id);
            if($this->ruanganpemesan_id != $ruangan_akses)
                $criteria->compare('ruangan_id', $ruangan_akses);
        }else{
            $criteria->compare('ruanganpemesan_id', $ruangan_akses);
            $criteria->compare('ruangan_id', $this->ruangan_id);
        }
        $criteria->order='tglpemesanan DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function getRuanganItems()
        {
            return RuanganM::model()->findAllByAttributes(array('ruangan_aktif'=>true), array('order'=>'ruangan_nama')) ;
        }
        
        public function getNamaModel(){
            return __CLASS__;
        }

}