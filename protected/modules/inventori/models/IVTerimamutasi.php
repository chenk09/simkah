<?php

class IVTerimamutasi extends TerimamutasiT {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchInformasi() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->addBetweenCondition('date(tglterima)',$this->tglAwal,$this->tglAkhir);
        $criteria->addCondition('mutasioaruangan_id is not null');
        $criteria->compare('LOWER(noterimamutasi)', strtolower($this->noterimamutasi), true);
        $criteria->compare('ruanganpenerima_id', Yii::app()->user->getState('ruangan_id'));
        $criteria->compare('ruanganasal_id', $this->ruanganasal_id);
        $criteria->order='tglterima DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    
    public function getRuanganItems()
        {
            return RuanganM::model()->findAll('ruangan_aktif=TRUE ORDER BY ruangan_nama') ;
        }
}