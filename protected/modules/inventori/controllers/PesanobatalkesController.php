<?php

class PesanobatalkesController extends SBaseController {
    protected $dateFormat = 'd M Y';
    /**
     * actionIndex digunakan di setiap transaksi pemesanan obat alkes
     * @param type $idPesanObat
     */
    public function actionIndex($idPesanObat=null) {
        $insert_notifikasi = new MyFunction();
        $format = new CustomFormat();
        $model = new IVPesanobatalkes;
        if(!empty($idPesanObat)){
            $model = IVPesanobatalkes::model()->findByPk($idPesanObat);
            $modDetails = PesanoadetailT::model()->findAllByAttributes(array('pesanobatalkes_id'=>$idPesanObat));
        }
        $model->instalasi_id = Params::ID_INSTALASI_FARMASI;
        $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $model->nopemesanan = Generator::noPemesanan($instalasi_id);
        $model->tglpemesanan = date('d M Y H:i:s');
//        $model->tglmintadikirim = date('Y-m-d H:i:s');
        $model->statuspesan = Params::DEFAULT_STATUS_PESAN;
        $model->instalasipemesan_id = Yii::app()->user->getState('instalasi_id');
        $model->ruanganpemesan_id = Yii::app()->user->getState('ruangan_id');
        
        if(isset($_POST['IVPesanobatalkes']) && (empty($idPesanObat)))
        {
            $model->attributes=$_POST['IVPesanobatalkes'];
            $model->tglmintadikirim = (empty($model->tglmintadikirim)) ? date('Y-m-d H:i:s') : $format->formatDateTimeMediumForDB($model->tglmintadikirim);
            //nilai attribute yang tidak boleh di ubah di form
            $model->tglpemesanan = date('Y-m-d H:i:s');
            $model->instalasipemesan_id = Yii::app()->user->getState('instalasi_id');
            $model->ruanganpemesan_id = Yii::app()->user->getState('ruangan_id');
            if (count($_POST['PesanoadetailT']) > 0){
                $modDetails = $this->validasiTabular($model, $_POST['PesanoadetailT']);
                if ($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){
                            $modDetails = $this->validasiTabular($model, $_POST['PesanoadetailT']);                            
                            foreach ($modDetails as $i=>$data){
                                if ($data->jmlpesan > 0){
                                    if ($data->save()){

                                    }
                                    else{
                                        $success = false;
                                    }
                                }
                            }
                            
                            $params['tglnotifikasi'] = date( 'Y-m-d H:i:s');
                            $params['create_time'] = date( 'Y-m-d H:i:s');
                            $params['create_loginpemakai_id'] = Yii::app()->user->id;
//                            $params['instalasi_id'] = 9;
                            $params['instalasi_id'] = $model->instalasi_id;
                            $params['modul_id'] = 16;
                            $ruangan = RuanganM::model()->findByPk($model->ruanganpemesan_id);
                            $params['isinotifikasi'] = $model->statuspesan . '-' . $ruangan->ruangan_nama;
                            $params['create_ruangan'] = $model->ruangan_id;
                            $params['judulnotifikasi'] = 'Pesan Obat Alkes';
                            $nofitikasi = $insert_notifikasi->insertNotifikasi($params);
                        }
                        else{
                            $success = false;
                        }
                        if ($success == true){
                           $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                            $this->redirect(array('index', 'idPesanObat'=>$model->pesanobatalkes_id));
                        }
                        else{
                            $model->isNewRecord = true;
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch (Exception $ex){
                        $model->isNewRecord = true;
                         $transaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
                }
            }else{
                $model->validate();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
        }

        $this->render('inventori.views.pesanobatalkes.index', array(
            'model' => $model, 'modDetails' => $modDetails, 'idPesanObat'=>$idPesanObat,
        ));
    }
    
    protected function validasiTabular($model, $data){
        $valid = true;
        foreach ($data as $i=>$row){
            $modDetails[$i] = new PesanoadetailT();
            $modDetails[$i]->attributes = $row;
            $modDetails[$i]->pesanobatalkes_id = $model->pesanobatalkes_id;
            $valid = $modDetails[$i]->validate() && $valid;
        }

        return $modDetails;
    }
    /**
     * actionInformasi menampilkan setiap informasi pemesanan obat
     */
    public function actionInformasi() {
        $model = new IVPesanobatalkes('searchInformasi');
        $model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $format = new CustomFormat();
        if (isset($_GET['IVPesanobatalkes'])) {
            $model->attributes = $_GET['IVPesanobatalkes'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['IVPesanobatalkes']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['IVPesanobatalkes']['tglAkhir']);
        }
        $this->render('inventori.views.pesanobatalkes.informasi', array(
            'model' => $model,
        ));
    }
    
    /**
     * actionDetails melihat detail transaksi pemesanan obat
     * @param type $id
     */
    public function actionDetails($id) {
            $this->layout = '//layouts/frameDialog';
        $modDetails = IVPesanoadetail::model()->findAll('pesanobatalkes_id=' . $id . '');
        $model = IVPesanobatalkes::model()->findByPk($id);

        $this->render('inventori.views.pesanobatalkes.details', array('model' => $model,
            'modDetails' => $modDetails));
    }
    
     public function actionPrint($idPesanObat,$caraPrint){
            $judulLaporan = 'Inventory Pemesanan Obat';
            $model = IVPesanobatalkes::model()->findByPk($idPesanObat);
            $modDetails = PesanoadetailT::model()->findAllByAttributes(array('pesanobatalkes_id'=>$idPesanObat));
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('inventori.views.pesanobatalkes.Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'idPesanObat'=>$idPesanObat,
                                            'modDetails'=>$modDetails,
                                            'model'=>$model));
            }
              
    }

}