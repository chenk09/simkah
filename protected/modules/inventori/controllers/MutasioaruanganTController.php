
<?php

class MutasioaruanganTController extends SBaseController {

    /**
     * actionIndex digunakan untuk setiap mutasi transaksi di semua modul
     * @param type $idMutasiRuangan
     */
    public function actionIndex($idMutasiRuangan=null){
        $model = new IVMutasioaruangan;
        $model->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
        $model->totalhargajual = 0;
        $model->totalharganettomutasi = 0;
        $model->nomutasioa = Generator::noMutasi();
        $model->tglmutasioa = date('d M Y H:i:s');
    
        if (isset($_GET['id'])){
            $id = $_GET['id'];
            $modPesan = PesanobatalkesT::model()->find('pesanobatalkes_id = '.$id.' and mutasioaruangan_id is null');
            if ($modPesan){
                $modDetailPesan = PesanoadetailT::model()->findAll('pesanobatalkes_id = '.$id);
                $model->pesanobatalkes_id = $id;
                $model->ruangantujuan_id = $modPesan->ruanganpemesan_id;
                $model->instalasi_id = $modPesan->ruanganpemesan->instalasi_id;
                $info = array();
                foreach ($modDetailPesan as $i=>$row){
                    $modObat = ObatalkesM::model()->findByPk($row->obatalkes_id);
                    //===========cek jumlah stok dengan jumlah pesanan============
                     $sql = "SELECT SUM(qtystok_current) as stok FROM stokobatalkes_t WHERE obatalkes_id=".$row->obatalkes_id."AND ruangan_id=".$model->ruanganasal_id;
//                    $stokobat = StokobatalkesT::model()->findByAttributes(array('obatalkes_id'=>$row->obatalkes_id));
                    $stokobat = StokobatalkesT::model()->findBySql($sql);
                    $jmlobat = $stokobat->qtystok_current;
                    $stokmin = $modObat->minimalstok;
                    //============================================================
                    
                    $format = new CustomFormat();
                    $modDetails[$i] = new MutasioadetailT();
                    $modDetails[$i]->attributes = $row->attributes;
                    $modDetails[$i]->jmlkemasan = $modObat->kemasanbesar;
                    $modDetails[$i]->tglkadaluarsa = trim($modObat->tglkadaluarsa);
//                    $modDetails[$i]->hargajualsatuan = $modObat->hargajual;
                    $modDetails[$i]->hargajualsatuan = $modObat->hpp;
                    $modDetails[$i]->harganetto = $modObat->harganetto;
//                    $modDetails[$i]->persendiscount = $modObat->discount;
                    $modDetails[$i]->persendiscount = $modObat->diskonJual;
                    
                    //===========membuat pesan peringatan stok sama dengan dan kurang dari sama dengan stok obat 
                    if(!empty($jmlobat)){
                        if($jmlobat == $stokmin){
                            $info[] = 'Obat Dengan Nama '.$modObat->obatalkes_nama.' sudah mencapai stok minimal';
                        }if($jmlobat<$stokmin) {
                            $info[] = 'Obat Dengan Nama '.$modObat->obatalkes_nama.' kurang dari stok minimal';
                        }
                    }
                    //================================================================================
                    
//                    $modDetails[$i]->jmlpesan = $row->jmlpesan;
                    $modDetails[$i]->stok = StokobatalkesT::getStokBarang($row->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                    if ($row->jmlpesan > $modDetails[$i]->stok){
                        if ($modDetails[$i]->stok < 0){
                            $modDetails[$i]->jmlmutasi = 0;
                        }else{
                            $modDetails[$i]->jmlmutasi = $modDetails[$i]->stok;
                        }
                    }
                    else{
                        $modDetails[$i]->jmlmutasi = $modDetails[$i]->jmlpesan;
                    }
                    $modDetails[$i]->subTotal = $modDetails[$i]->hargajualsatuan*$modDetails[$i]->jmlmutasi;
                    $model->totalhargajual += $modDetails[$i]->subTotal;
                    $model->totalharganettomutasi += $modDetails[$i]->harganetto;
                }
            }
        }
        if(!empty ($idMutasiRuangan)){
            $modDetails = MutasioadetailT::model()->findAllByAttributes(array('mutasioaruangan_id'=>$idMutasiRuangan));
            $model = MutasioaruanganT::model()->findByPk($idMutasiRuangan);
        }
        
        if (isset($_POST['IVMutasioaruangan']) && empty ($idMutasiBarang)) {
            $model->attributes = $_POST['IVMutasioaruangan'];
            $model->create_time = date('Y-m-d H:i:s');
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if (count($_POST['MutasioadetailT']) > 0){
                if ($model->validate()){
                    $transaction = Yii::app()->db->beginTransaction();
                    try{
                        $success = true;
                        if($model->save()){           
                            if (!empty($model->pesanobatalkes_id)){
                                PesanobatalkesT::model()->updateByPk($model->pesanobatalkes_id, array('mutasioaruangan_id'=>$model->mutasioaruangan_id));
//                                $modTerima = $this->insertTerimaMutasi($model);
//                                if ($modTerima->save()){
//                                    MutasioaruanganT::model()->updateByPk($model->mutasioaruangan_id, array('terimamutasi_id'=>$modTerima->terimamutasi_id));
                                    $modDetails = $this->validasiTabular($model, $_POST['MutasioadetailT']);                
                                    foreach ($modDetails as $i=>$data){
                                        if ($data->jmlmutasi > 0){
                                            if ($data->save()){
                                                /*HARUS MELALUI FORM PENERIMAAN MUTASI
                                                $modDetailTerima = $this->insertDetailTerima($data, $modTerima);
                                                if ($modDetailTerima->save()){
                                                    OPERASI STOK HANYA DILAKUKAN JIKA BARANG SUDAH DI KONFIRMASI TERIMA
                                                    $stokObat = $this->tambahStok($modDetailTerima, $modTerima, $model->ruangantujuan_id);
                                                    if ($stokObat->save()){
                                                        OPERASI STOK HANYA DILAKUKAN JIKA BARANG SUDAH DI KONFIRMASI TERIMA
                                                        $stok = StokobatalkesT::validasiStok($modDetailTerima->jmlterima, $modDetailTerima->obatalkes_id, $model->ruanganasal_id) ;
                                                        if (StokobatalkesT::validasiStok($modDetailTerima->jmlterima, $modDetailTerima->obatalkes_id, $model->ruanganasal_id) == Params::konfigGudangFarmasi('stokminus')){
                                                        if($stok){   
                                                            StokobatalkesT::kurangiStok($modDetailTerima->jmlterima, $modDetailTerima->obatalkes_id, $model->ruanganasal_id);
                                                        }
                                                        else{
                                                            $success = false;
                                                        }
                                                    }else{
                                                        $success = false;
                                                    }
                                                HARUS MELALUI FORM PENERIMAAN MUTASI
                                                }
                                                else{
                                                    $success = false;
                                                }
                                                StokobatalkesT::kurangiStok($data->jmlmutasi, $data->obatalkes_id, $model->ruanganasal_id);
                                                 * 
                                                 */
                                            }
                                            else{
                                                $success = false;
                                            }
                                        }
                                    }
//                                HARUS MELALUI FORM PENERIMAAN MUTASI
//                                }
//                                else{
//                                    $success = false;
//                                }
                            }
                            else{
                                $modDetails = $this->validasiTabular($model, $_POST['MutasioadetailT']);
                                foreach ($modDetails as $i=>$data){
                                    $data->jmlpesan = 0;
                                    $data->totalharga = $data->jmlmutasi*$data->hargajualsatuan;
                                    if ($data->jmlmutasi > 0){
                                        if ($data->save()){
    //                                        HARUS MELALUI KONFIRMASI TERIMA
    //                                        StokobatalkesT::kurangiStok($data->jmlmutasi, $data->obatalkes_id, $model->ruanganasal_id);
                                        }
                                        else{
                                            $success = false;
                                        }
                                    }
                                }
                            }
                        }
                        else{
                            $success = false;
                        }
                        if ($success == true){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
//                            if (!empty($model->pesanobatalkes_id)){
//                                $this->redirect(Yii::app()->createUrl('gudangFarmasi/pesanobatalkes/informasi'));
//                            }
//                            else {
                                $this->redirect(array('index', 'idMutasiRuangan'=>$model->mutasioaruangan_id));
//                            }
                        }
                        else{
                            $model->isNewRecord = true;
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        }
                    }
                    catch (Exception $ex){
                        $model->isNewRecord = true;
                         $transaction->rollback();
                         Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                    }
                }
            }else{
                $model->validate();
                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
            }
        }
        
        $this->render('inventori.views.mutasioaruanganT.index', array(
            'model' => $model, 'modDetails'=>$modDetails, 'modPesan'=>$modPesan, 'info'=>$info
        ));
    }   


    public function actionDetails($id) {
        $this->layout = '//layouts/frameDialog';
        $modDetailMutasi = IVMutasioadetail::model()->findAll('mutasioaruangan_id=' . $id . '');
        $modMutasi = IVMutasioaruangan::model()->findByPk($id);

        $this->render('inventori.views.mutasioaruanganT.details', array('modDetailMutasi' => $modDetailMutasi,
            'modMutasi' => $modMutasi));
    }

    protected function validasiTabular($model, $data){
        $format = new CustomFormat();
        $valid = true;
        foreach ($data as $i=>$row){
            $modDetails[$i] = new MutasioadetailT();
            $modDetails[$i]->attributes = $row;
            $modDetails[$i]->stok = StokobatalkesT::getStokBarang($row['obatalkes_id'], Yii::app()->user->getState('ruangan_id'));
            $modDetails[$i]->mutasioaruangan_id = $model->mutasioaruangan_id;
            $modDetails[$i]->tglkadaluarsa = date('Y-m-d H:i:s', strtotime($row['tglkadaluarsa']));
            $modDetails[$i]->totalharga = $row['subTotal'];
            $valid = $modDetails[$i]->validate() && $valid;
        }
        return $modDetails;
    }
    /**
     * actionInformasi = menampilkan informasi mutasi masuk dan keluar
     */
    public function actionInformasi() {
        $model = new IVMutasioaruangan('searchInformasi');
        $model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y');
        $model->tglAkhir = date('d M Y');
        $model->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
        $format = new CustomFormat();
        if (isset($_GET['IVMutasioaruangan'])) {
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
        }

        $this->render('inventori.views.mutasioaruanganT.informasi', array(
            'model' => $model,
        ));
    }
    
    /**
     * actionInformasiMutasiMasuk = menampilkan informasi mutasi masuk saja dan penerimaan mutasi
     */
    public function actionInformasiMutasiMasuk() {
        $model = new IVMutasioaruangan('searchInformasiMutasiMasuk');
        $model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y');
        $model->tglAkhir = date('d M Y');
        $model->ruangantujuan_id = Yii::app()->user->getState('ruangan_id');
        $format = new CustomFormat();
        if (isset($_GET['IVMutasioaruangan'])) {
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
            $model->noPemesanan = $_REQUEST['IVMutasioaruangan']['noPemesanan'];
            $model->noTerima = $_REQUEST['IVMutasioaruangan']['noTerima'];
            $model->statusTerima = $_REQUEST['IVMutasioaruangan']['statusTerima'];
        }

        $this->render('inventori.views.mutasioaruanganT.informasiMutasiMasuk', array(
            'model' => $model,
        ));
    }
    /**
     * actionInformasiMutasiKeluar = menampilkan informasi mutasi keluar saja
     */
    public function actionInformasiMutasiKeluar() {
        $model = new IVMutasioaruangan('searchInformasiMutasiKeluar');
        $model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y  H:i:s');
        $model->ruanganasal_id = Yii::app()->user->getState('ruangan_id');
        $format = new CustomFormat();
        if (isset($_GET['IVMutasioaruangan'])) {
            $model->attributes = $_GET['IVMutasioaruangan'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['IVMutasioaruangan']['tglAkhir']);
            $model->noPemesanan = $_REQUEST['IVMutasioaruangan']['noPemesanan'];
            $model->statusTerima = $_REQUEST['IVMutasioaruangan']['statusTerima'];
        }

        $this->render('inventori.views.mutasioaruanganT.informasiMutasiKeluar', array(
            'model' => $model,
        ));
    }
    
    public function actionPrint($idMutasi,$caraPrint){
            $judulLaporan = 'Mutasi Obal Alkes';
            $modDetailMutasi = IVMutasioadetail::model()->findAll('mutasioaruangan_id=' . $idMutasi . '');
            $modMutasi = IVMutasioaruangan::model()->findByPk($idMutasi);
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('inventori.views.mutasioaruanganT.Print',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'modDetailMutasi' => $modDetailMutasi,
                                           'modMutasi' => $modMutasi));
            }
              
        }
   
   public function actionPrintMutasiObat($idMutasiRuangan,$caraPrint){
            $judulLaporan = 'Mutasi Obat Alkes Ruangan';
            $modDetails = MutasioadetailT::model()->findAllByAttributes(array('mutasioaruangan_id'=>$idMutasiRuangan));
            $model = MutasioaruanganT::model()->findByPk($idMutasiRuangan);
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('inventori.views.mutasioaruanganT.PrintMutasiObat',array('judulLaporan'=>$judulLaporan,
                                            'caraPrint'=>$caraPrint,
                                            'idMutasiRuangan'=>$idMutasiRuangan,
                                            'modDetails'=>$modDetails,
                                            'model'=>$model));
            }
              
    }
    
}
