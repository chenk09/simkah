<?php

class TerimamutasiController extends SBaseController {

    protected $_mutasi = 'mutasioaruanganT';
    
    public function actionIndex($id) {
        $modMutasi = MutasioaruanganT::model()->findByPk($id);
        if (count($modMutasi) == 1){
            $model = new IVTerimamutasi;
            $format = new CustomFormat();
            $model->totalhargajual = 0;
            $model->totalharganetto = 0;
            $model->noterimamutasi = Generator::noTerimaMutasi();
            $model->tglterima = date('Y-m-d H:i:s');
            $model->ruanganasal_id = $modMutasi->ruanganasal_id;
            $model->ruanganpenerima_id = Yii::app()->user->getState('ruangan_id');
            $model->mutasioaruangan_id = $id;
            $modDetailMutasi = MutasioadetailT::model()->findAll('mutasioaruangan_id = '.$id);
            foreach ($modDetailMutasi as $i=>$detail){
                $format = new CustomFormat();
                $modDetails[$i] = new TerimamutasidetailT();
                $modDetails[$i]->attributes = $detail->attributes;
                $modDetails[$i]->jmlmutasi = $detail->jmlmutasi;
                $modDetails[$i]->satuankecil_id = $detail->satuankecil_id;
                $modDetails[$i]->sumberdana_id = $detail->sumberdana_id;
                $modDetails[$i]->obatalkes_id = $detail->obatalkes_id;
                $modDetails[$i]->harganettoterima = $detail->harganetto;
                $modDetails[$i]->stok = StokobatalkesT::getStokBarang($detail->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                $modDetails[$i]->jmlterima = $detail->jmlmutasi;
                $modDetails[$i]->totalhargaterima = $detail->hargajualsatuan*$detail->jmlmutasi;
                $modDetails[$i]->hargajualterim = $detail->hargajualsatuan;
//                $modDetails[$i]->tglkadaluarsa = date('Y-m-d',strtotime($modDetails[$i]->tglkadaluarsa));
                $model->totalhargajual += $modDetails[$i]->totalhargaterima;
                $model->totalharganetto += $detail->harganetto;
            }
            
            if (isset($_POST['IVTerimamutasi'])) {
                $model->attributes = $_POST['IVTerimamutasi'];
                if (count($_POST['TerimamutasidetailT']) > 0){
//                    $modDetails = $this->validasiTabular($model, $_POST['TerimamutasidetailT']);
                    if ($model->validate()){
                        $transaction = Yii::app()->db->beginTransaction();
                        try{
                            $success = true;
                            if($model->save()){
                                MutasioaruanganT::model()->updateByPk($id, array('terimamutasi_id'=>$model->terimamutasi_id));
                                
                                $modDetails = $this->validasiTabular($model, $_POST['TerimamutasidetailT']);
                                foreach ($modDetails as $i=>$data){
                                    if ($data->jmlterima > 0){
                                        if ($data->save()){
                                            $obat = $this->tambahStok($data);
                                            if ($obat->save()){
//                                                Params::konfigGudangFarmasi('stokminus')
//                                                if(StokobatalkesT::validasiStok($data->jmlterima, $data->obatalkes_id, $model->ruanganasal_id) == true)
//                                                {
                                                    StokobatalkesT::kurangiStok($data->jmlterima, $data->obatalkes_id, $model->ruanganasal_id);
                                                    TerimamutasidetailT::model()->updateByPk($data->terimamutasidetail_id, array('stokobatalkes_id'=>$obat->stokobatalkes_id));
                                                    
//                                                }
//                                                else{
//                                                    $success = false;
//                                                }
                                            }else{
                                                $success = false;
                                            }
                                        }
                                        else{
                                            $success = false;
                                        }
                                    }
                                }
                            }
                            else{
                                $success = false;
                            }
                            
                            if ($success == true){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->redirect(Yii::app()->createUrl(Yii::app()->controller->module->id.'/'.$this->_mutasi.'/informasi'));
                            }
                            else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            }
                        }
                        catch (Exception $ex){
                             $transaction->rollback();
                             Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($ex,true));
                        }
                    }
                }else{
                    $model->validate();
                    Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data detail barang harus diisi.');
                }
                
            }
        }else{
            throw new CHttpException(401, Yii::t('mds', 'You are prohibited to access this page. Contact Super Administrator'));
        }
        

        $this->render('inventori.views.terimamutasi.index', array(
            'model' => $model, 'modMutasi' => $modMutasi, 'modDetails' => $modDetails
        ));
    }
    
    protected function tambahStok($data){
        $modStokObat = new StokobatalkesT();
        $modStokObat->attributes = $data->attributes;
        $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modStokObat->tglstok_in = date('Y-m-d H:i:s');
        $modStokObat->qtystok_in = $data->jmlterima;
        $modStokObat->qtystok_out = 0;
        $modStokObat->qtystok_current = $data->jmlterima;
        $modStokObat->hargajual_oa = $data->hargajualterim;
        $modStokObat->harganetto_oa = $data->harganettoterima;
        $modStokObat->discount = $data->persendiscount;
        
        $modObat = ObatalkesM::model()->findByPk($data->obatalkes_id);
        if(!empty($modObat->obatalkes_id)){
            $modStokObat->hjaresep = $modObat->hjaresep;
            $modStokObat->hjanonresep = $modObat->hjanonresep;
            $modStokObat->marginresep = $modObat->marginresep;
            $modStokObat->marginnonresep = $modObat->marginnonresep;
            $modStokObat->hpp = $modObat->hpp;
        }
        $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modStokObat->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modStokObat->tglstok_in = date('Y-m-d H:i:s');
        $modStokObat->tglstok_out = null;
        $modStokObat->create_time = date('Y-m-d H:i:s');
        $modStokObat->update_time = null;
        $modStokObat->create_loginpemakai_id = Yii::app()->user->id;
        return $modStokObat;
    }
    
    protected function validasiTabular($model, $data){
        $format = new CustomFormat();
        $valid = true;
        foreach ($data as $i=>$row){
            $modDetails[$i] = new TerimamutasidetailT();
            $modDetails[$i]->attributes = $row;                  
            $modDetails[$i]->terimamutasi_id = $model->terimamutasi_id;
            $modDetails[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//            $modDetails[$i]->tglkadaluarsa = date('Y-m-d', strtotime($row['tglkadaluarsa']));
            
            $valid = $modDetails[$i]->validate() && $valid;
        }

        return $modDetails;
    }

    public function actionInformasi() {
        $model = new IVTerimamutasi('search');
        $model->unsetAttributes();  // clear any default values
        $model->tglAwal = date('d M Y');
        $model->tglAkhir = date('d M Y');
        $format = new CustomFormat();
        if (isset($_GET['IVTerimamutasi'])) {
            $model->attributes = $_GET['IVTerimamutasi'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['IVTerimamutasi']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['IVTerimamutasi']['tglAkhir']);
        }

        $this->render('inventori.views.terimamutasi.informasi', array(
            'model' => $model,
        ));
    }

    public function actionDetails($id) {
        $this->layout = '//layouts/frameDialog';
        $modDetailTerimaMutasi = IVTerimamutasidetail::model()->findAll('terimamutasi_id=' . $id . '');
        $modTerimaMutasi = IVTerimamutasi::model()->findByPk($id);

        $this->render('inventori.views.terimamutasi.details', array('modDetailTerimaMutasi' => $modDetailTerimaMutasi,
            'modTerimaMutasi' => $modTerimaMutasi));
    }

}