<table id="tableObatAlkes" class="table table-bordered table-condensed">
    
    <tbody>
       <?php
       if (isset($modDetails)){
            echo "<thead><tr>
                    <th>No.</th>
                    <th>Jenis / Kategori / Nama Obat</th>
                    <th>Asal Barang</th>
                    <th>Jumlah Pesan (Satuan Kecil)</th>
                    <th>Harga Netto</th>
                    <th>Harga Jual</th>
                    <th>Subtotal</th>
                    ";
            echo "</tr></thead>";
            foreach ($modDetails as $i=>$detail){
                $modObatAlkes = ObatalkesM::model()->findByPk($detail->obatalkes_id);
//                if(empty($modObatAlkes->hargajual)){
//                echo "<tr>
//                        <td>".($i+1)."</td>
//                        <td>".CHtml::activeHiddenField($detail,'['.$i.']satuankecil_id').
//                              CHtml::activeHiddenField($detail,'['.$i.']sumberdana_id'). 
//                              CHtml::activeHiddenField($detail,'['.$i.']obatalkes_id',array('class'=>'obat')).
////                              CHtml::activehiddenField($detail,'stokobat',array('class'=>'obat')).
//                            $modObatAlkes->jenisobatalkes->jenisobatalkes_nama." / ".$modObatAlkes->obatalkes_kategori." / ".$modObatAlkes->obatalkes_nama."</td>
//                        <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
//                        <td>".CHtml::activetextField($detail,'['.$i.']jmlpesan',array('readonly'=>true,'class'=>'span1 numbersOnly qty','onblur'=>'hitungSemua();', 'onkeypress' => "return $(this).focusNextInputField(event)",))
//                        ." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
//                        <td>".CHtml::activetextField($detail,'['.$i.']harganetto',array('class'=>'span1 numbersOnly netto','readonly'=>TRUE))."</td>
//                        <td>".CHtml::activetextField($detail,'['.$i.']hargajual',array('class'=>'span1 numbersOnly hargaJual','readonly'=>TRUE))."</td>
//                        <td>".CHtml::activetextField($detail,'['.$i.']subtotal',array('class'=>'span1 subTotal','readonly'=>TRUE))."</td>
//                      </tr>";
////                        <td>".$modObatAlkes->satuankecil->satuankecil_nama." / ".$modObatAlkes->satuanbesar->satuanbesar_nama."</td>
////                        <td>".CHtml::activeTextField($detail,'['.$i.']stok',array('class'=>'span1', 'readonly'=>true, 'onkeypress' => "return $(this).focusNextInputField(event)",))."</td>
////                        <td>".Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel'))."</td>
//                }else{
                    $id=$detail->obatalkes_id;
                    $pesanobat = PesanobatalkesT::model()->findByPk($detail->pesanobatalkes_id);          
                    $StokObat = StokobatalkesT::getStokBarang($id, $pesanobat->ruangan_id);
                    $subtotal = $detail->jmlpesan*$modObatAlkes->hjaresep;
                echo 
                "<tr>
                    <td>".($i+1)."</td>
                    <td>".$modObatAlkes->jenisobatalkes->jenisobatalkes_nama."/".$modObatAlkes->obatalkes_kategori."/".$modObatAlkes->obatalkes_nama."</td>
                    <td>".$modObatAlkes->sumberdana->sumberdana_nama."</td>
                    <td>".$detail->jmlpesan." ".$modObatAlkes->satuankecil->satuankecil_nama."</td>
                    <td>".$modObatAlkes->harganetto."</td>
                    <td>".$modObatAlkes->hargajual."</td>
                    <td>".$subtotal."</td>
                </tr>"    ;
//                }

//                    (($model->isNewRecord) ? "<td>".'-'."</td>" : '').
            }
        }else{
            echo "<thead><tr>
                    <th>Kategori /<br/> Nama Obat</th>
                    <th>Asal Barang</th>
                    <th>Satuan Kecil/<br/>Satuan Besar</th>   
                    <th>Jumlah Stok</th>
                    <th>Jumlah Pesan</th>
                    <th>Harga Netto</th>
                    <th>Harga Jual</th>
                    <th>Sub Total</th>";
            echo (($model->isNewRecord) ? '<th>Batal</th>' : '');
            echo "</tr></thead>";
        }
        ?>
    </tbody>
</table>

<?php
$notif = Yii::t('mds','Do You want to cancel?');
$urlGetObatAlkes = Yii::app()->createUrl('actionAjax/getPesanObatAlkes'); //MAsukan Dengan memilih Obat Alkes
$hargaNettoPenawaran = CHtml::activeId($modPermintaanPenawaran, 'harganettopenawaran');
$obatAlkes = CHtml::activeId($modPermintaanPenawaran, 'obatAlkes');
$ruangan_id = CHtml::activeId($model, 'ruangan_id');
//$noPerencanaan = CHtml::activeId($modRencanaKebFarmasi, 'noperencnaan');
//$tglPerencanaan = CHtml::activeId($modRencanaKebFarmasi, 'tglperencanaan');

$jscript = <<< JS
    function clearAll(){
        $('#tableObatAlkes tbody tr').remove();
    }
    function cekRuangan(){
        instalasiTujuan= $('#IVPesanobatalkes_instalasi_id');
        ruanganTujuan = $('#IVPesanobatalkes_ruangan_id');
        
        if(!jQuery.isNumeric(instalasiTujuan.val())){
            alert('Silahkan Pilih Instalasi Tujuan Terlebih Dahulu');
            instalasiTujuan.focus();
        }else if(!jQuery.isNumeric(ruanganTujuan.val())){
            alert('Silahkan Pilih Ruangan Tujuan Terlebih Dahulu');
            ruanganTujuan.focus();
        }
    }
    function submitObat()
    {
        idRuangan = $('#${ruangan_id}').val();
        idObat = $('#idObatAlkes').val();
        qtyObat = $('#qtyObat').val();
        satuan = $('#satuan').val();
        stok = $('#stok').val();
        min = $('#min').val();
        ruanganpemesan= $('#IVPesanobatalkes_instalasipemesan_id').val();
        ruangan = $('#IVPesanobatalkes_ruanganpemesan_id').val();
        
        
        if(!jQuery.isNumeric(ruanganpemesan)){
            alert('Silahkan PIlih Instalasi Terlebih Dahulu');
        }else if(!jQuery.isNumeric(ruangan)){
            alert('Silahkan PIlih Ruangan Terlebih Dahulu');
        }else{
        if(!jQuery.isNumeric(idRuangan)){
            alert('Silahkan Pilih Ruangan Terlebih Dahulu');
            return false;
        }
        else if(!jQuery.isNumeric(idObat)){
            alert('Silahkan Pilih Obat Terlebih Dahulu');
            return false;
        }else{
            if (cekList(idObat) == true){
                $.post("${urlGetObatAlkes}", {idRuangan:idRuangan,satuan:satuan, idObat: idObat, qtyObat:qtyObat },
                function(data){
//                      if(data.stok = min){
//                        alert('Obat Alkes Sudah Mencapai Stok Minimal');
//                    }else if(stok == 0 || stok < 1){
//                        alert('Jumlah Pemesanan Lebih dari Stok');
//                    }
                    if(data.stok == 0)
                    {
                        alert('Stok Obat Alkes di Ruangan Tujuan Tidak Ada');
                        return false;
                    }else{
                        $('#tableObatAlkes tbody').append(data.tr);
                        $("#tableObatAlkes tbody tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
                        clear();
                        hitungSemua();
                        renameInput();
                    }
                }, "json");
            }
        }   
      }
    }
    
    function hitungSemua(){
        $('.obat').each(function(){
            qty = parseFloat($(this).parents('tr').find('.qty').val());
            harga = parseFloat($(this).parents('tr').find('.hargaJual').val());
            $(this).parents('tr').find('.subTotal').val(qty*harga);
        })
    }

    function cekList(id){
        x = true;
        $('.obat').each(function(){
            if ($(this).val() == id){
                alert('Obat dan Alat Kesehatan telah ada di List');
                clear();
                x = false;
            }
        });
        return x;
    }
    
    function clear(){
        $('#formObat').find('input, select').each(function(){
            $(this).val('');
        });
        $('#qtyObat').val(1);
    }
    
    function batal(obj){
        if(!confirm("${notif}")) {
            return false;
        }else{
            $(obj).parents('tr').remove();
            renameInput();
        }
    }
    
    function renameInput(){
        noUrut = 0;
        $('.obat').each(function(){
            $(this).parents('tr').find('[name*="PesanoadetailT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('PesanoadetailT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','PesanoadetailT['+noUrut+']'+data[1]);
                }
            });
            noUrut++;
        });        
    }

JS;
Yii::app()->clientScript->registerScript('masukanobat', $jscript, CClientScript::POS_HEAD);
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php 
Yii::app()->clientScript->registerScript('onready','
    $("form").submit(function(){
        obat = false;
        idRuangan = $("#'.CHtml::activeId($model, 'ruangan_id').'").val();
            
        $(".qty").each(function(){
            if ($(this).val() > 0){
                obat = true
            }
        });
        
        if(!jQuery.isNumeric(idRuangan)){
            alert("'.CHtml::encode($model->getAttributeLabel('ruangan_id')).' harus diisi");
            return false;
        }
        
        
        if ($(".obat").length < 1){
            alert("Detail Obat Harus Diisi");
            return false;
        }
        else if (obat == false){
            alert("'.CHtml::encode($model->getAttributeLabel('qty_pesan')).' harus memiliki value yang lebih dari 0");
            return false;
        }
    });
',CClientScript::POS_READY);?>
