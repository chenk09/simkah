<legend class="rim2">Informasi Pesan Obat alkes</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('form#ivpesanobatalkes-search').submit(function(){
	$.fn.yiiGridView.update('ivpesanobatalkes-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action = $this->getAction()->getId();
$currentUrl = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
$instalasi_akses = Yii::app()->user->getState('instalasi_id');
$ruangan_akses = Yii::app()->user->getState('ruangan_id');

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'ivpesanobatalkes-grid',
    'dataProvider' => $model->searchInformasi(),
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        ////'pesanobatalkes_id',
//        array(
//            'name' => 'pesanobatalkes_id',
//            'value' => '$data->pesanobatalkes_id',
//            'filter' => false,
//        ),
        //'mutasioaruangan_id',
        //'ruangan_id',
        'nopemesanan',
        'tglpemesanan',
        'tglmintadikirim',
        array(
            'name' => 'ruanganpemesan_id',
            'value' => '$data->ruanganpemesan->ruangan_nama',
            'filter' => false,
        ),
        array(
            'name' => 'ruangan_id',
            'value' => '$data->ruangantujuan->ruangan_nama',
            'filter' => false,
        ),
        'statuspesan',
        array(
            'header' => 'Details',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.$controller.'/details",array("id"=>$data->pesanobatalkes_id)),
                            array("class"=>"", 
                                  "target"=>"pesan",
                                  "onclick"=>"$(\"#dialogPesanObatAlkes\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Pemesanan Obat Alkes",
                            ))',
        ),
        
        array(
            'header' => 'Mutasi',
            'type' => 'raw',
            'value' => '
                        (empty($data->mutasioaruangan_id)) ?
                          ('.$instalasi_akses.' == Params::ID_INSTALASI_FARMASI && '.$ruangan_akses.' == $data->ruangan_id) ?
                            CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("/inventori/mutasioaruanganT/index",array("id"=>$data->pesanobatalkes_id)),array("class"=>"", "title"=>"Klik untuk mutasi obat"))
                            : "Belum Dimutasi" 
                        : "Telah Dimutasi"',
            
        ),
        
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?> 

<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'ivpesanobatalkes-search',
    'type' => 'horizontal',
        ));
?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pesan Obat Alkes', 'tglPesanObatAlkes', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan', 'sampaiDengan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </td>
        <td>
            <?php echo $form->textFieldRow($model, 'nopemesanan', array('class' => 'numberOnly')); ?>
            <?php if ($instalasi_akses == Params::ID_INSTALASI_FARMASI) { ?>
            <div class="control-group">
                <label class="control-label">Ruang Pemesanan</label>
                <div class="controls">
                    <?php echo $form->dropDownList($model, 'ruanganpemesan_id', CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
                        'empty' => '-- Pilih --',));
                    ?>
                </div>
            </div>
            <?php }?>
            <div class="control-group">
                <label class="control-label">Pesan Ke</label>
                <div class="controls">
            <?php
            echo $form->dropDownList($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAllByAttributes(array('instalasi_id'=> Params::ID_INSTALASI_FARMASI, 'ruangan_aktif'=>true),array('order'=>'ruangan_nama')), 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
                'empty' => '-- Pilih --',));
            ?>
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-actions"> 
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
	            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						 <?php  
$content = $this->renderPartial('inventori.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div> 

<?php $this->endWidget(); ?>

<?php


$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'form_hiddenPemesanan',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'action' => Yii::app()->createAbsoluteUrl($module . '/mutasioaruanganT/index'),
        ));
?>
<?php echo CHtml::hiddenField('idPemesananForm', '', array('readonly' => true)); ?>
<?php echo CHtml::hiddenField('noPemesananForm', '', array('readonly' => true)); ?>
<?php echo CHtml::hiddenField('tglPemesananForm', '', array('readonly' => true)); ?>
<?php echo CHtml::hiddenField('currentUrl', $currentUrl, array('readonly' => true)); ?>
<?php $this->endWidget(); ?>

<?php
$js = <<< JSCRIPT
function formMutasi(idPesan,noPesan,tglPesan)
{
    $('#idPemesananForm').val(idPesan);
    $('#noPemesananForm').val(noPesan);
    $('#tglPemesananForm').val(tglPesan);
    $('#form_hiddenPemesanan').submit();
}


JSCRIPT;
Yii::app()->clientScript->registerScript('javascript', $js, CClientScript::POS_HEAD);

// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'dialogPesanObatAlkes',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => 'Details Pemesanan Obat Dan Alat Kesehatan',
        'autoOpen' => false,
        'minWidth' => 900,
        'minHeight' => 100,
        'resizable' => false,
    ),
));
?>
<iframe src="" name="pesan" width="100%" height="500">
</iframe>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================
?>
