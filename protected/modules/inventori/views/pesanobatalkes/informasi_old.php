<legend class="rim2">Informasi Pesan Obat Alkes</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('form#ivpesanobatalkes-search').submit(function(){
	$.fn.yiiGridView.update('ivpesanobatalkes-grid', {
		data: $(this).serialize()
	});
	return false;
});
", CClientScript::POS_READY);
?>

<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action = $this->getAction()->getId();
$currentUrl = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
?>

<?php

$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'ivpesanobatalkes-grid',
    'dataProvider' => $model->searchInformasi(),
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        ////'pesanobatalkes_id',
        //'mutasioaruangan_id',
        //'ruangan_id',
        'tglpemesanan',
        'nopemesanan',
        'statuspesan',
        'tglmintadikirim',
        /*
          'tglmintadikirim',
          'keterangan_pesan',
          'ruanganpemesan_id',
          'create_time',
          'update_time',
          'create_loginpemakai_id',
          'update_loginpemakai_id',
          'create_ruangan',
         */
        array(
            'header' => 'Details',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.$controller.'/details",array("id"=>$data->pesanobatalkes_id)),
                            array("class"=>"", 
                                  "target"=>"pesan",
                                  "onclick"=>"$(\"#dialogPesanObatAlkes\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Pemesanan Obat Alkes",
                            ))',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?> 

<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'ivpesanobatalkes-search',
    'type' => 'horizontal',
        ));
?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pesan', 'tglAwalPesan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan', 'sampaiDengan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </td>
        <td>
            <?php echo $form->textFieldRow($model, 'nopemesanan', array('class' => 'numberOnly')); ?>
            <?php
//            echo $form->dropDownListRow($model, 'ruanganpemesan_id', CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
//                'empty' => '-- Pilih --',));
            ?>
        </td>
    </tr>
</table>
<div class="form-actions"> 
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
	            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						 <?php  
$content = $this->renderPartial('inventori.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div> 

<?php $this->endWidget(); ?>

<?php

// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'dialogPesanObatAlkes',
    // additional javascript options for the dialog plugin
    'options' => array(
        'title' => 'Details Pemesanan Obat Dan Alat Kesehatan',
        'autoOpen' => false,
        'minWidth' => 900,
        'minHeight' => 100,
        'resizable' => false,
    ),
));
?>
<iframe src="" name="pesan" width="100%" height="500">
</iframe>
<?php
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================
?>
