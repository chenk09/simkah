<table width="100%">
    <tr>
        <td width="50%">
            
             <b><?php echo CHtml::encode($model->getAttributeLabel('nopemesanan')); ?>:</b>
            <?php echo CHtml::encode($model->nopemesanan); ?>
        </td>
        <td></td>
        <td>
            <b><?php echo CHtml::encode($model->getAttributeLabel('ruanganpemesan_id')); ?>:</b>
            <?php echo CHtml::encode($model->ruanganpemesan->ruangan_nama); ?>
        </td>
    </tr>
    <tr>
        <td>
            <b><?php echo CHtml::encode($model->getAttributeLabel('tglpemesanan')); ?>:</b>
            <?php echo CHtml::encode($model->tglpemesanan); ?>
        </td>
        <td></td>
         <td>
             <b><?php echo CHtml::encode($model->getAttributeLabel('ruangan_id')); ?>:</b>
            <?php echo CHtml::encode($model->ruangantujuan->ruangan_nama); ?>
        </td>
    </tr>
    <tr>
        <td>
        <?php // echo CHtml::encode($model->getAttributeLabel('create_time')); ?>
        <?php // echo CHtml::encode($model->create_time); ?>
        <b><?php echo CHtml::encode($model->getAttributeLabel('tglmintadikirim')); ?>:</b>
            <?php echo CHtml::encode($model->tglmintadikirim); ?>
        </td>
        <td></td>
        <td></td>
    </tr>
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Satuan Kecil</th>
        <th>Jumlah <br/> Permintaan</th>
        <th>Harga Netto</th>
        <th>Harga Jual</th>
        <th>SubTotal</th>
    
    </thead>
    <?php
    $no=1;
        foreach($modDetails AS $tampilData):
            $subTotal = $tampilData['jmlpesan']*$tampilData->obatalkes['hargajual'];
    echo "<tr>
            <td>".$no."</td>
            <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
            <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
            <td>".$tampilData->satuankecil['satuankecil_nama']."</td>   
            <td>".$tampilData['jmlpesan']."</td>
            <td>".$tampilData->obatalkes['harganetto']."</td>     
            <td>".$tampilData->obatalkes['hargajual']."</td>     
            
            <td>".$subTotal."</td>     
            
                      
         </tr>";  
        $no++;
       
        $totalSubTotal=$totalSubTotal+$subTotal;
        
        endforeach;
    echo "<tr>
            <td colspan='7' style='text-align:right;'> Total</td>
           
            <td>".$totalSubTotal."</td>
         </tr>";    
    ?>
    
</table>
<?php
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/Print', array('idPesanObat'=>$model->pesanobatalkes_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>    