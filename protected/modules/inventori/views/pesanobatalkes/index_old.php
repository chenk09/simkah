<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php
$this->widget('bootstrap.widgets.BootAlert');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'ivpesanobatalkes-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#',
        ));

?>
<?php ?>

<fieldset>
    <legend class="rim2">Transaksi Pesan Obat Alkes</legend>
    <?php echo $form->errorSummary($model);$idpesanobat = $_GET['idPesanObat'] ;
            if(empty($idpesanobat)){ ?>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                   <?php echo $form->labelEx($model, 'ruangan_id', array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php
                        echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                            'ajax' => array('type' => 'POST',
                                'url' => Yii::app()->createUrl('ActionDynamic/getRuanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                                'update' => '#' . CHtml::activeId($model, 'ruangan_id') . ''),));
                        ?>
                        <?php echo $form->dropDownList($model, 'ruangan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true and instalasi_id ='.$model->instalasi_id), 'ruangan_id', 'ruangan_nama'), array('onchange'=>'clearAll();','class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'ruangan_id'); ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($model,'tglpemesanan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model, 'nopemesanan', array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50, 'readonly'=>true)); ?>

                <?php echo $form->dropDownListRow($model, 'statuspesan', IVStatusPesan::items(), array('class' => 'span3 isRequired', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 30, 'empty' => '-- Pilih --')); ?>
<!--                <div class="control-group ">
                   <?php echo $form->labelEx($model, 'ruanganpemesan_id', array('class'=>'control-label')); ?>
                    <div class="controls">
                    
                        <?php
                        echo $form->dropDownList($model, 'instalasipemesan_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'readonly'=>true,'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                            'ajax' => array('type' => 'POST',
                                'url' => Yii::app()->createUrl('ActionDynamic/ruanganPemesanDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                                'update' => '#' . CHtml::activeId($model, 'ruanganpemesan_id') . ''),));
                        ?>
                       
                        <?php echo $form->dropDownList($model, 'ruanganpemesan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'readonly'=>true,'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50)); ?>
                        <?php echo $form->error($model, 'ruanganpemesan_id'); ?>
                    </div>
                </div>-->
                <?php //echo $form->textFieldRow($model,'tglmintadikirim',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->dropDownListRow($model,'ruanganpemesan_id', array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            

                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglpemesanan', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglpemesanan',
                            'mode' => 'date',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                'minDate'=>'d',
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>
                
                <?php //echo $form->textFieldRow($model,'tglmintadikirim',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->dropDownListRow($model,'ruanganpemesan_id', array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td>
                <?php //echo $form->dropDownListRow($model, 'statuspesan', IVStatusPesan::items(), array('class' => 'span3 isRequired', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 30, 'empty' => '-- Pilih --')); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglmintadikirim', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglmintadikirim',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                'minDate'=>'d',
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>
                <?php echo $form->textAreaRow($model, 'keterangan_pesan', array('rows' => 3, 'class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
    </table>
</fieldset>
    <?php 
    if (isset($modDetails)){
        echo $form->errorSummary($modDetails); 
    }
    ?>
<?php $this->renderPartial('inventori.views.pesanobatalkes._formObat', array('model'=>$model)); }?>


<?php $this->renderPartial('inventori.views.pesanobatalkes._tableDetailObat', array('model'=>$model, 'modDetails'=>$modDetails)); ?>

<div class="form-actions">
    <?php
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $action = $this->getAction()->getId();
    $currentAction = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
    //$urlRencana=Yii::app()->createUrl($module.'/RencanaKebutuhan/search');
    if(empty($_GET['idPesanObat'])){
    echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'onkeypress' => "cekValidasi()",));
    ?>

    <div style="display: none">     
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
    </div>

    <?php
    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                           $currentAction, 
                            array('class'=>'btn btn-danger',));  
    ?>
							 <?php  
$content = $this->renderPartial('inventori.views.tips.transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
} else{
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/printpesanobat', array('idPesanObat'=>$idpesanobat));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printpesanobat',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}}
?>

</div>
<?php $this->endWidget(); 
?>

