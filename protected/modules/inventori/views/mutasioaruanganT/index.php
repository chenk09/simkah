
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php
$this->widget('bootstrap.widgets.BootAlert');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'mutasibarang-m-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)', 'onSubmit'=>'unformatNumberForm(); return konfirmasi()'),
    'focus' => '#',
        ));

?> 
  
<?php echo $form->errorSummary($model);$idMutasiRuangan = $_GET['idMutasiRuangan'] ;
            if(empty($idMutasiRuangan)){ ?>
<fieldset>
<legend class="rim2">Data Mutasi Barang</legend>  

    <table>
        <tr>
            <td>
                    <?php echo $form->textFieldRow($model, 'nomutasioa', array('readonly'=>TRUE,'class' => 'span3')) ?>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'ruangantujuan_id', array('class'=>'control-label', )); ?>
                        <div class="controls">
                            <?php
                            echo $form->dropDownList($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array('autofocus'=>true, 'empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50,
                                'ajax' => array('type' => 'POST',
                                    'url' => Yii::app()->createUrl('ActionDynamic/ruanganDariInstalasi', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                                    'update' => '#' . CHtml::activeId($model, 'ruangantujuan_id') . ''),'disabled'=>(isset($modPesan)) ? 'disabled' : ''));
                            ?>
                            <?php echo $form->dropDownList($model, 'ruangantujuan_id', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_id', 'ruangan_nama'), array('empty' => '-- Pilih --', 'class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event);", 'maxlength' => 50, 'disabled'=>(isset($modPesan)) ? 'disabled' : '')); ?>
                            <?php echo $form->error($model, 'ruangantujuan_id'); ?>
                        </div>
                    </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($model, 'tglmutasioa', array('class' => 'control-label')) ?>
                        <div class="controls">
                            <?php
                            $this->widget('MyDateTimePicker', array(
                                'model' => $model,
                                'attribute' => 'tglmutasioa',
                                'mode' => 'datetime',
                                'options' => array(
                                    'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                    'maxDate'=>'d',
                                ),
                                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                                ),
                            ));
                            ?>
                            <?php echo $form->error($model, 'tglmutasioa'); ?>
                        </div>
                    </div>
                    <?php echo $form->textAreaRow($model, 'keteranganmutasi', array('class' => 'span3 isRequired', 'onkeypress' => "return $(this).focusNextInputField(event)")) ?>          
            </td>
            <td>
                <?php if (isset($modPesan)){
                   $this->renderPartial('inventori.views.mutasioaruanganT._dataPesan', array('modPesan'=>$modPesan));
                }?>
            </td>
        </tr>
    </table>
</fieldset> 
<?php 
    if (isset($modDetails)){
        echo $form->errorSummary($modDetails); 
    }
    ?>
<?php if (!isset($modPesan)){ ?>
<?php $this->renderPartial('inventori.views.mutasioaruanganT._formObat', array('model'=>$model)); ?>
<?php } ?>
<?php
if (!empty($info)){
    
    echo "<div style='background:#FBE3E4;color:#8a1f11;border-color:#FBC2C4;margin:3px 0;padding:2px 0 0 0'>";
    echo MyFunction::infoMessage($info)."</div>";
} }
?>
<?php $this->renderPartial('inventori.views.mutasioaruanganT._tableDetailObat', array('model'=>$model, 'form'=>$form, 'modDetails'=>$modDetails, 'modPesan'=>$modPesan)); ?>





<div class="form-actions">
    <?php
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $action = $this->getAction()->getId();
    $currentAction = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
    //$urlRencana=Yii::app()->createUrl($module.'/RencanaKebutuhan/search');
    if(empty($idMutasiRuangan)){
    echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit','id'=>'submit'));
    ?>

    <div style="display: none">     
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
    </div>

    <?php if (isset($modPesan)){//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
                    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                           Yii::app()->request->urlReferrer, 
                            array('class'=>'btn btn-danger',));
               }else{
                    echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
                           $currentAction, 
                            array('class'=>'btn btn-danger',));   
                }  
         ?>
<?php  
$content = $this->renderPartial('inventori.views.tips.transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
} else{
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/printMutasiObat', array('idMutasiRuangan'=>$idMutasiRuangan));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('printmutasiobat',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}}
?>
</div>

<?php $this->endWidget(); ?>

<script>
    //Format dan unformat number pada form
    //Created by: ichan include: accounting.js
    function formatNumberForm(){
        $("form").find(".integer").each(function(){
            $(this).val(formatNumber($(this).val()));
        });
    }
    formatNumberForm();
    function unformatNumberForm(){
        $("form").find(".integer").each(function(){
            $(this).val(unformatNumber($(this).val()));
        });
    }
</script>
