<style type="text/css">
    .hidden{
        display: none;
    }
</style>
<table width="100%">
    <tr>
        <td>
             <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('nomutasioa')); ?>:</b>
            <?php echo CHtml::encode($modMutasi->nomutasioa); ?>
            <br />
             <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('tglmutasioa')); ?>:</b>
            <?php echo CHtml::encode($modMutasi->tglmutasioa); ?>
             <br/>
            <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('keteranganmutasi')); ?>:</b> 
            <?php echo CHtml::encode($modMutasi->keteranganmutasi); ?>
             
        </td>
        <td>
            <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('ruanganasal_id')); ?>:</b>
            <?php echo CHtml::encode($modMutasi->ruanganasal->ruangan_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('ruangantujuan_id')); ?>:</b>
            <?php echo CHtml::encode($modMutasi->ruangantujuan->ruangan_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modMutasi->getAttributeLabel('create_time')); ?>:</b>
            <?php echo CHtml::encode($modMutasi->create_time); ?>
            <br />
        </td>
    </tr>   
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Satuan Kecil</th>
        <?php if (!empty($modMutasi->pesanobatalkes_id)) { ?>
        <th>Jumlah <br/> Pesan</th>
        <?php } ?>
        <th>Jumlah <br/> Mutasi</th>
        <th class="hidden">Harga Netto</th>
        <th class="hidden">Harga Jual</th>
        <th class="hidden">SubTotal</th>
    
    </thead>
    <?php
    $no=1;
        foreach($modDetailMutasi AS $tampilData):
            $subTotal = $tampilData['jmlmutasi']*$tampilData['hargajualsatuan'];
    echo "<tr>
            <td>".$no."</td>
            <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
            <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
            <td>".$tampilData->satuankecil['satuankecil_nama']."</td>";
                if (!empty($modMutasi->pesanobatalkes_id)) {
            echo "<td>".$tampilData['jmlpesan']."</td>";
                }
            echo "<td>".MyFunction::formatNumber($tampilData['jmlmutasi'])."</td>
            <td class='hidden'>".MyFunction::formatNumber($tampilData['harganetto'])."</td>     
            <td class='hidden'>".MyFunction::formatNumber($tampilData['hargajualsatuan'])."</td>     
            
            <td class='hidden'>".MyFunction::formatNumber($subTotal)."</td>     
            
                      
         </tr>";  
        $no++;
       
        $totalSubTotal=$totalSubTotal+$subTotal;
        
        endforeach;
    echo "<tr class='hidden'>
            <td colspan='";
    if (!empty($modMutasi->pesanobatalkes_id)) {
        echo '8';
    }
    else{
        echo '7';
    }
    echo "' style='text-align:right;'> Total</td>
           
            <td>".MyFunction::formatNumber($totalSubTotal)."</td>
         </tr>";    
    ?>
    
</table>
<?php
    if(isset($caraPrint)){
?>
        <table width="100%">
            <tr>
                <td></td>
                <td></td>
            </tr>    
            <tr>
                <td align="center">Penerima<br><br><br><br><br></td>
                <td align="center">Pengirim<br><br><br><br><br></td>
            </tr>
            <tr>
                <td align="center">(<?php for ($i=0; $i < 30; $i++) { 
                    echo "&nbsp;";
                } ?>)</td>
                <td align="center"><?php echo "( ".Yii::app()->user->name." )"; ?></td>
            </tr>    
        </table>
<?php
}
if (!isset($caraPrint)){
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/print', array('idMutasi'=>$modMutasi->mutasioaruangan_id));
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')')); 
}
?>  