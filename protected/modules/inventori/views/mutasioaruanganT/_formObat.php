<fieldset id='formObat'>
    <legend class="rim">Nama Obat dan Alat Kesehatan</legend>

    <div class='control-group'>
        <label class ='control-label'>Obat Alkes</label>
        <?php echo CHtml::hiddenField('idObatAlkes'); ?>
        <div class='controls'>
            <?php
            $this->widget('MyJuiAutoComplete', array(
//                    'model' => $modDetail,
                'name' => 'namaObatAlkes',
                'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/ObatAlkes'),
                'options' => array(
                    'showAnim' => 'fold',
                    'minLength' => 2,
                    'focus' => 'js:function( event, ui ) {
                                                        $(this).val("");
                                                        return false;
                                                    }',
                    'select' => 'js:function( event, ui ) {
                                                      $(this).val(ui.item.value);
                                                      $("#idObatAlkes").val(ui.item.obatalkes_id);
                                                      $("#satuan").val(ui.item.satuankecil_id);
                                                      $("#minimal").val(ui.item.minimalstok);
                                                      return false;
                                                    }',
                ),
                'htmlOptions' => array('onkeypress' => "return $(this).focusNextInputField(event)",
                    'class' => 'span3 isRequired',
                ),
                'tombolDialog' => array('idDialog' => 'dialogObatAlkes'),
            ));
            ?>
        </div>
    </div>
    <div class='control-group'>
        <?php echo CHtml::label('Qty', 'Qty', array('class' => 'control-label')); ?>
        <div class='controls'>
            <?php echo CHtml::textField('qtyObat', '1', array('class' => 'span1 numbersOnly', 'onkeypress' => "return $(this).focusNextInputField(event)",)); ?>
            <?php echo CHtml::dropDownList('satuan', '', CHtml::listData(SatuankecilM::model()->findAll('satuankecil_aktif = true'), 'satuankecil_id', 'satuankecil_nama'), array('class' => 'span2', 'onkeypress' => "return $(this).focusNextInputField(event)", 'empty'=>'-- Pilih --')); ?>
            <?php
        echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>', array('onclick' => 'submitObat();return false;',
            'class' => 'btn btn-primary',
            'onkeypress' => "submitObat();return $('#namaObatAlkes').focus()",
            'rel' => "tooltip",
            'id' => 'tambahObat',
            'title' => "Klik Untuk Menambahkan Obat",));
        ?>
        </div>
    </div>
</fieldset>   

<?php
//========= Dialog buat cari data obatAlkes =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogObatAlkes',
    'options' => array(
        'title' => 'Pencarian Obat Alkes',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 600,
        'resizable' => false,
    ),
));

$modObatAlkes = new IVObatalkes('search');
$modObatAlkes->unsetAttributes();
if (isset($_GET['IVObatalkes'])) {
    $modObatAlkes->attributes = $_GET['IVObatalkes'];
}
$this->widget('ext.bootstrap.widgets.BootGridView', array(
    'id' => 'pasien-m-grid',
    //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
    'dataProvider' => $modObatAlkes->searchDialog(),
    'filter' => $modObatAlkes,
    'template' => "{pager}{summary}\n{items}",
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Pilih',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "$(\"#idObatAlkes\").val(\"$data->obatalkes_id\");
                                                          $(\"#namaObatAlkes\").val(\"$data->obatalkes_kode - $data->obatalkes_nama\");
                                                          $(\"#satuan\").val(\"$data->satuankecil_id\");
                                                           $(\"#minimal\").val(\"$data->minimalstok\");
                                                          $(\"#dialogObatAlkes\").dialog(\"close\");    
                                                "))',
        ),
        'obatalkes_kategori',
        'obatalkes_golongan',
        'obatalkes_kode',
        'obatalkes_nama',
        array('name' => 'sumberdanaNama',
            'type' => 'raw',
            'value' => '$data->sumberdana->sumberdana_nama',
        ),
        array('name'=>'satuankecil_id',
            'value'=>'$data->satuankecil->satuankecil_nama',
            ),
        'obatalkes_kadarobat',
        'kemasanbesar',
        'kekuatan',
        'tglkadaluarsa',
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));

$this->endWidget();
//========= end obatAlkes dialog =============================
?>

