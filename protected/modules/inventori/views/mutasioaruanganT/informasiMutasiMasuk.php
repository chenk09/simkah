<legend class="rim2">Informasi Penerimaan Obat Alkes</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('form#mutasimasuk-search').submit(function(){
	$.fn.yiiGridView.update('ivmutasioaruangan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array( 
    'id'=>'ivmutasioaruangan-grid', 
    'dataProvider'=>$model->searchInformasiMutasiMasuk(), 
    //'filter'=>$model, 
        'template'=>"{pager}{summary}\n{items}", 
        'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array( 
        array(
            'name'=>'terimamutasi.noterimamutasi',
            'type'=>'raw',
            'value'=>'(empty($data->terimamutasi->noterimamutasi)) ? "<center><b> - </b></center>" : $data->terimamutasi->noterimamutasi'
        ),
        'nomutasioa',
        array(
            'name'=>'pesanobatalkes.nopemesanan',
            'type'=>'raw',
            'value'=>'(empty($data->pesanobatalkes->nopemesanan)) ? "<center><b> - </b></center>" : $data->pesanobatalkes->nopemesanan'
        ),
        'tglmutasioa',
        array(
            'header'=>'Total Harga Netto',
            'type'=>'raw',
            'value'=>'"Rp. ".MyFunction::formatNumber($data->totalharganettomutasi)',
        ),
         array(
          'header'=>'Total Harga Jual',
          'type'=>'raw',
          'value'=>'"Rp. ".MyFunction::formatNumber($data->totalhargajual)', 
        ),
        array(
            'header'=>'Ruangan Asal',
            'value'=>'$data->ruanganasal->ruangan_nama',
        ),
        
        array(
            'header' => 'Detail Mutasi',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.$this->_mutasi.'/details",array("id"=>$data->mutasioaruangan_id)),
                            array("class"=>"", 
                                  "target"=>"mutasi",
                                  "onclick"=>"$(\"#dialogMutasiObatAlkes\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Mutasi Obat Alkes",
                            ))',
        ),
        array(
            'header' => 'Terima Obat Alkes <br>/ Detail Terima',
            'type' => 'raw',
            'value'=>'
                    ((empty($data->terimamutasi_id))
                           ? (($data->ruangantujuan_id == Yii::app()->user->getState("ruangan_id")) ?
                                CHtml::Link("<i class=\"icon-pencil-brown\"></i>",Yii::app()->controller->createUrl("Terima'.$this->_mutasi.'/index", array("id"=>$data->mutasioaruangan_id)),
                                array("class"=>"", 
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk terima Obat Alkes",
                                              )) : "-") 
                                      : CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("Terima'.$this->_mutasi.'/details",array("id"=>$data->terimamutasi_id)),
                                        array("class"=>"", 
                                              "target"=>"terimamutasi",
                                              "onclick"=>"$(\"#dialogTerimaObatAlkes\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk melihat detail Penerimaan Obat Alkes",
                                        )))
                ',
        ),
        array(
            'header' => 'Status Terima',
            'type' => 'raw',
            'value'=>'(empty($data->terimamutasi_id)) ? "Belum Diterima" : "Sudah Diterima"',
        ),
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 



<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'mutasimasuk-search',
    'type' => 'horizontal',
        ));
?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Mutasi', 'tglMutasi', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan', 'sampaiDengan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
            <?php echo $form->textFieldRow($model, 'noPemesanan', array('class' => 'span3')); ?>
            <?php echo $form->textFieldRow($model, 'nomutasioa', array('class' => 'span3')); ?>
        </td>
        <td>
            <div class="control-group ">
                <?php echo Chtml::label('No. Terima Mutasi', 'noTerima', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->textField($model, 'noTerima', array('class' => 'span3')); ?>
                </div>
            </div>
            <?php echo $form->dropDownListRow($model, 'ruanganasal_id', CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
                'empty' => '-- Pilih --',)); ?>
            <?php echo $form->dropDownListRow($model, 'statusTerima', array(1=>"Sudah Diterima", 0=>"Belum Diterima"), array('class'=>'span3', "empty"=>"-- Pilih --")); ?>
        </td>
    </tr>
</table>
<div class="form-actions"> 
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
    <?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                Yii::app()->createUrl($this->route), 
                                array('class'=>'btn btn-danger',
                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
						 <?php  
$content = $this->renderPartial('inventori.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div> 

<?php $this->endWidget(); ?>

<?php 
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action=$this->getAction()->getId();
$currentUrl=  Yii::app()->createUrl($module.'/'.$controller.'/'.$action);

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'form_hiddenMutasi',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'action'=>Yii::app()->createAbsoluteUrl($module.'/Terimamutasi/index'),
)); ?>
    <?php echo CHtml::hiddenField('idMutasiForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('noMutasiForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('tglMutasiForm','',array('readonly'=>true));?>
    <?php echo CHtml::hiddenField('currentUrl',$currentUrl,array('readonly'=>true));?>
<?php $this->endWidget(); ?>

<?php
$js = <<< JSCRIPT
function formMutasi(idMutasi,noMutasi,tglMutasi)
{
    $('#idMutasiForm').val(idMutasi);
    $('#noMutasiForm').val(noMutasi);
    $('#tglMutasiForm').val(tglMutasi);
    $('#form_hiddenMutasi').submit();
}


JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);                        

// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogMutasiObatAlkes',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Mutasi Obat Dan Alat Kesehatan',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="mutasi" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogTerimaObatAlkes',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Penerimaan Obat Dan Alat Kesehatan',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="terimamutasi" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

?>

