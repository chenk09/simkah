<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modMutasi, 'nomutasioa', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    echo CHtml::activeTextField($modMutasi, 'nomutasioa', array('readonly'=>true))
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modMutasi, 'tglmutasioa', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    echo CHtml::activeTextField($modMutasi, 'tglmutasioa', array('readonly'=>true))
                    ?>
                </div>
            </div>
            
            <div class="control-group ">
                <?php echo CHtml::activeLabel($modMutasi, 'ruanganasal_id', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php
                    echo CHtml::activeTextField($modMutasi, 'ruanganasal_id', array('readonly'=>true, 'value'=>$modMutasi->ruanganasal->ruangan_nama))
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>