<?php if(Yii::app()->controller->module->id == 'gizi' || Yii::app()->controller->module->id == 'rehabMedis' || Yii::app()->controller->module->id =='bedahSentral'){
    echo "<legend class='rim2'>Informasi Penerimaan Obat Alkes</legend>";
}else { ?>
<legend class="rim2">Informasi Terima Mutasi Obat Alkes</legend>
<?php } ?>
<?php
Yii::app()->clientScript->registerScript('search', "
$('form#ivpesanobatalkes-search').submit(function(){
	$.fn.yiiGridView.update('ivterimamutasi-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$action = $this->getAction()->getId();
$currentUrl = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
?>

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array( 
    'id'=>'ivterimamutasi-grid', 
    'dataProvider'=>$model->searchInformasi(),
    //'filter'=>$model, 
        'template'=>"{pager}{summary}\n{items}", 
        'itemsCssClass'=>'table table-striped table-bordered table-condensed', 
    'columns'=>array( 
        ////'terimamutasi_id',
        
        'tglterima',
        'noterimamutasi',
        'totalharganetto',
        'totalhargajual',
        'ruanganasal.ruangan_nama',
        /*
        'keterangan_terima',
        'ruanganpenerima_id',
        
        'create_time',
        'update_time',
        'create_loginpemakai_id',
        'update_loginpemakai_id',
        'create_ruangan',
        */
        array(
            'header' => 'Details',
            'type' => 'raw',
            'value' => 'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("'.$controller.'/details",array("id"=>$data->terimamutasi_id)),
                            array("class"=>"", 
                                  "target"=>"mutasi",
                                  "onclick"=>"$(\"#dialogTerimaMutasiBarang\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat details Penerimaan Barang",
                            ))',
        ),
        
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 

<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
    'id' => 'ivpesanobatalkes-search',
    'type' => 'horizontal',
        ));
?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Terima Mutasi', 'tglTerimaMutasi', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan', 'sampaiDengan', array('class' => 'control-label')) ?>
                <div class="controls">
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'date',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </div>
            </div> 
        </td>
        <td>
            <?php echo $form->textFieldRow($model, 'noterimamutasi', array('class' => 'numberOnly')); ?>
            <?php
            echo $form->dropDownListRow($model, 'ruanganasal_id', CHtml::listData($model->RuanganItems, 'ruangan_id', 'ruangan_nama'), array('class' => 'span3', 'onkeypress' => "return $(this).focusNextInputField(event)",
                'empty' => '-- Pilih --',));
            ?>
        </td>
    </tr>
</table>
<div class="form-actions"> 
    <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit')); ?>
	            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						 <?php  
$content = $this->renderPartial('inventori.views.tips.informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div> 

<?php $this->endWidget(); ?>



<?php


JSCRIPT;
Yii::app()->clientScript->registerScript('javascript',$js,CClientScript::POS_HEAD);                        

// ===========================Dialog Details=========================================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
                    'id'=>'dialogTerimaMutasiBarang',
                        // additional javascript options for the dialog plugin
                        'options'=>array(
                        'title'=>'Details Penerimaan Barang',
                        'autoOpen'=>false,
                        'minWidth'=>900,
                        'minHeight'=>100,
                        'resizable'=>false,
                         ),
                    ));
?>
<iframe src="" name="mutasi" width="100%" height="500">
</iframe>
<?php    
$this->endWidget('zii.widgets.jui.CJuiDialog');
//===============================Akhir Dialog Details================================

?>
