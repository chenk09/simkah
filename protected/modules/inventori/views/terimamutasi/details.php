<table>
    <tr>
        <td>
             <b><?php echo CHtml::encode($modTerimaMutasi->getAttributeLabel('noterimamutasi')); ?>:</b>
            <?php echo CHtml::encode($modTerimaMutasi->noterimamutasi); ?>
            <br />
             <b><?php echo CHtml::encode($modTerimaMutasi->getAttributeLabel('tglterima')); ?>:</b>
            <?php echo CHtml::encode($modTerimaMutasi->tglterima); ?>
            <br />
        </td>
        <td>
             <b><?php echo CHtml::encode($modTerimaMutasi->getAttributeLabel('ruanganasal_id')); ?>:</b>
            <?php echo CHtml::encode($modTerimaMutasi->ruanganasal->ruangan_nama); ?>
            <br />
             <b><?php echo CHtml::encode($modTerimaMutasi->getAttributeLabel('create_time')); ?>:</b>
            <?php echo CHtml::encode($modTerimaMutasi->create_time); ?>
            <br />
        </td>
    </tr>   
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    
        <th>No.Urut</th>
        <th>Asal Barang</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Satuan Kecil</th>
        <th>Jumlah <br/> Terima</th>
        <th>Harga Netto</th>
        <th>Harga Jual</th>
        <th>SubTotal</th>
    
    </thead>
    <?php
    $no=1;
        foreach($modDetailTerimaMutasi AS $tampilData):
            $subTotal = $tampilData['jmlterima']*$tampilData['hargajualterim'];
    echo "<tr>
            <td>".$no."</td>
            <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
            <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
            <td>".$tampilData->satuankecil['satuankecil_nama']."</td>   
            <td>".MyFunction::formatNumber($tampilData['jmlterima'])."</td>
            <td>".MyFunction::formatNumber($tampilData['harganettoterima'])."</td>     
            <td>".MyFunction::formatNumber($tampilData['hargajualterim'])."</td>     
            
            <td>".MyFunction::formatNumber($subTotal)."</td>     
            
                      
         </tr>";  
        $no++;
       
        $totalSubTotal=$totalSubTotal+$subTotal;
        
        endforeach;
    echo "<tr>
            <td colspan='7' style='text-align:right;'> Total</td>
           
            <td>".MyFunction::formatNumber($totalSubTotal)."</td>
         </tr>";    
    ?>
    
</table>