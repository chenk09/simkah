<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No.Urut</th>
            <th>Asal Barang</th>
            <th>Kategori/<br/>Nama Obat</th>
            <th>stok</th>
            <th>Jumlah <br/>Mutasi</th>
            <th>Jumlah <br/>Terima</th>
            <th>Jumlah Diskon (%)</th>
            <th>Harga Netto</th>
            <th>Harga Satuan</th>
            <th>Sub Total</th>
            <th>Batal</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if (isset($modDetails)){
            foreach ($modDetails as $i=>$detail){
                $format = new CustomFormat();
                $modObatAlkes = ObatalkesM::model()->findByPk($detail->obatalkes_id);
                echo "<tr>
                    <td>".($i+1)."</td>
                    <td>".$modObatAlkes->sumberdana->sumberdana_nama.
                          CHtml::activeHiddenField($detail,'['.$i.']satuankecil_id',array('value'=>$modObatAlkes->satuankecil_id)).
                          CHtml::activeHiddenField($detail,'['.$i.']sumberdana_id',array('value'=>$modObatAlkes->sumberdana_id)). 
                          CHtml::activeHiddenField($detail,'['.$i.']obatalkes_id',array('value'=>$modObatAlkes->obatalkes_id, 'class'=>'obat')). 
                          CHtml::activeHiddenField($detail,'['.$i.']jmlkemasan',array('value'=>$tampilDetail->obatalkes->kemasanbesar)). 
                          CHtml::activeHiddenField($detail,'['.$i.']tglkadaluarsa',array('value'=>$format->formatDateMediumForDB($modObatAlkes->tglkadaluarsa))). 
//                          CHtml::activeHiddenField($detail,'['.$i.']persendiscount',array('value'=>$modObatAlkes->discount,'class'=>'span1 persenDiskon')).
                   "<td>".$modObatAlkes->obatalkes_kategori."/<br/>".$modObatAlkes->obatalkes_nama."</td>
                    <td>".CHtml::activeTextField($detail, '['.$i.']stok', array('value'=>$stok, 'class'=>'maxStok span1', 'readonly'=>true))."</td>";
                    echo "<td>".CHtml::activeTextField($detail, '['.$i.']jmlmutasi',array('value'=>$jumlahMutasi,'class'=>'span1 numbersOnly jmlMutasi', 'onkeypress' => "return $(this).focusNextInputField(event)", 'readonly'=>true))."</td>";
                    echo "<td>".CHtml::activeTextField($detail, '['.$i.']jmlterima',array('value'=>$jumlahMutasi,'class'=>'span1 numbersOnly qty', 'onkeyup'=>'hitungJumlahMutasi(this);', 'onkeypress' => "return $(this).focusNextInputField(event)", 'onblur'=>'hitungSemua();'))."</td>
                    <td>".CHtml::activeTextField($detail,'['.$i.']persendiscount', array('value'=>$modObatAlkes->discount,'class'=>'span1 numbersOnly jumlahDiscount','readonly'=>TRUE))."</td>
                    <td>".CHtml::activeTextField($detail,'['.$i.']harganettoterima',array('value'=>$modObatAlkes->harganetto,'class'=>'span1 hargaNetto','readonly'=>true))."</td>
                    <td>".CHtml::activeTextField($detail,'['.$i.']hargajualterim',array('readonly'=>TRUE,'class'=>'span1 hargaJual'))."</td>
                    <td>".CHtml::activeTextField($detail,'['.$i.']totalhargaterima',array('readonly'=>TRUE,'class'=>'span2 subTotal'))."</td> 
                    <td>".Chtml::link('<icon class="icon-remove"></icon>', '', array('onclick'=>'batal(this);', 'style'=>'cursor:pointer;', 'class'=>'cancel'))."</td>
                  </tr>";
            }
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="9">
                Total Harga
            </td>
            <td>
                <?php echo $form->textField($model, 'totalhargajual', array('class'=>'span2', 'readonly'=>true)); ?>
                <?php echo $form->hiddenField($model, 'totalharganetto', array('class'=>'span1', 'readonly'=>true)); ?>
            </td>
            <td></td>
        </tr>
    </tfoot>
</table>

<?php
$notif = Yii::t('mds','Do You want to cancel?');
$urlGetObatAlkes = Yii::app()->createUrl('actionAjax/getObatAlkesDariMutasi'); //MAsukan Dengan memilih Obat Alkes
$hargaNettoPenawaran = CHtml::activeId($modPermintaanPenawaran, 'harganettopenawaran');
$obatAlkes = CHtml::activeId($modPermintaanPenawaran, 'obatAlkes');
$ruangan_id = CHtml::activeId($model, 'ruangan_id');
$totalHarga = CHtml::activeId($model, 'totalhargajual');
$totalHargaNetto = CHtml::activeId($model, 'totalharganettomutasi');
//$noPerencanaan = CHtml::activeId($modRencanaKebFarmasi, 'noperencnaan');
//$tglPerencanaan = CHtml::activeId($modRencanaKebFarmasi, 'tglperencanaan');

$jscript = <<< JS
    
    function hitungSemua(){
        $('.obat').each(function(){
            qty = parseFloat($(this).parents('tr').find('.qty').val());
            harga = parseFloat($(this).parents('tr').find('.hargaJual').val());
            maxStok = $(this).parents('tr').find('.jmlMutasi').val();
            jmlPesan = $(this).parents('tr').find('.jmlPesan').val();
                if (jQuery.isNumeric(jmlPesan)){
                    if (qty > jmlPesan){
                        alert('Jumlah Mutasi tidak boleh lebih dari Jumlah Pesan');
                        $(this).parents('tr').find('.qty').val(jmlPesan);
                        hitungSemua();
                        return false;
                    }
                    
                }
                if (qty > maxStok){
                    alert('Jumlah Mutasi tidak boleh lebih dari Jumlah Mutasi');
                    $(this).parents('tr').find('.qty').val(maxStok);
                    hitungSemua();
                    return false;
                }
            $(this).parents('tr').find('.subTotal').val(qty*harga);
        })
        subTotal = 0;
        nettoMutasi = 0;
        $('.subTotal').each(function(){
            subTotal+=parseFloat($(this).val());
            nettoMutasi+=parseFloat($(this).parents('tr').find('.hargaNetto').val());
        });
        $('#${totalHarga}').val(subTotal);
        $('#${totalHargaNetto}').val(nettoMutasi);
    }
    function hitungJumlahMutasi(obj){
        hitungSemua();
    }
    function batal(obj){
        if(!confirm("${notif}")) {
            return false;
        }else{
            $(obj).parents('tr').remove();
            renameInput();
            
        }
        hitungSemua();
    }
    
    function renameInput(){
        noUrut = 0;
        $('.obat').each(function(){
            $(this).parents('tr').find('[name*="MutasioadetailT"]').each(function(){
                var nama = $(this).attr('name');
                data = nama.split('MutasioadetailT[]');
                if (typeof data[1] === "undefined"){}else{
                    $(this).attr('name','MutasioadetailT['+noUrut+']'+data[1]);
                }
            });
            noUrut++;
        });        
    }
JS;
Yii::app()->clientScript->registerScript('masukanobat', $jscript, CClientScript::POS_HEAD);
?>
<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => ',',
        'thousands' => '',
        'precision' => 0,
    )
));
?>

<?php 
Yii::app()->clientScript->registerScript('onready','
    $("form").submit(function(){
        obat = false;
        idRuangan = $("#'.CHtml::activeId($model, 'ruangantujuan_id').'").val();
            
        $(".qty").each(function(){
            if ($(this).val() > 0){
                obat = true
            }
        });
        
//        if(!jQuery.isNumeric(idRuangan)){
//            alert("'.CHtml::encode($model->getAttributeLabel('ruangantujuan_id')).' harus diisi");
//            return false;
//        }
        
        
        if ($(".obat").length < 1){
            alert("Detail Obat Harus Diisi");
            return false;
        }
        else if (obat == false){
            alert("'.CHtml::encode($model->getAttributeLabel('jmlmutasi')).' harus memiliki value yang lebih dari 0");
            return false;
        }
    });
',CClientScript::POS_READY);?>
