<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/form.js'); ?>
<?php
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
    'id' => 'ivterimamutasi-form',
    'enableAjaxValidation' => false,
    'type' => 'horizontal',
    'htmlOptions' => array('onKeyPress' => 'return disableKeyPress(event)'),
    'focus' => '#',
        ));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>




<fieldset>
    <legend class="rim2">Data Terima Mutasi Barang</legend>
    <?php echo $form->errorSummary($model); ?>
    <table>
        <tr><td>
                <?php echo $form->textFieldRow($model, 'noterimamutasi', array('readonly'=>true,'class' => 'span3 isRequired')) ?>
                <?php
                echo $form->dropDownListRow($model, 'ruanganasal_id', CHtml::listData(RuanganM::model()->findAll(), 'ruangan_id', 'ruangan_nama'), array('class' => 'span3 isRequired', 'onkeypress' => "return $(this).focusNextInputField(event)",
                    'empty' => '-- Pilih --',
                    'disabled'=>'disabled'));
                ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($model, 'tglterima', array('class' => 'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglterima',
                            'mode' => 'date',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                                'minDate'=>'d',
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                        ?>
                    </div>
                </div>

                <?php echo $form->textAreaRow($model, 'keterangan_terima', array('class' => 'span3 isRequired')) ?>
            </td>
            <td>
                <?php
                if (isset($modMutasi)) {
                    echo $this->renderPartial('inventori.views.terimamutasi._dataMutasi', array('modMutasi' => $modMutasi));
                }
                ?>    
            </td>
        </tr>

        </td>
        </tr>
    </table>
</fieldset> 

        <?php
        if (isset($modDetails)) {
            echo $form->errorSummary($modDetails);
        }
        ?>
    </fieldset> 
    <?php $this->renderPartial('inventori.views.terimamutasi._tableDetailObat', array('modMutasi' => $modMutasi, 'form' => $form, 'model' => $model, 'modDetails' => $modDetails)); ?>
    <div class="form-actions">
        <?php
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $action = $this->getAction()->getId();
        $currentAction = Yii::app()->createUrl($module . '/' . $controller . '/' . $action);
        //$urlRencana=Yii::app()->createUrl($module.'/RencanaKebutuhan/search');
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 's'));
        ?>

        <div style="display: none">     
            <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Create', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
        </div>

        <?php
        if (isset($modMutasi)) {//Jika Masuk ke Halaman ini memalui Informasi Rencana kebutuhan
            echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), Yii::app()->request->urlReferrer, array('class' => 'btn btn-danger',));
        } else {
            echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}' => '<i class="icon-ban-circle icon-white"></i>')), $currentAction, array('class' => 'btn btn-danger',));
        }
        ?>
        <?php
        $content = $this->renderPartial('inventori.views.tips.tips', array(), true);
        $this->widget('TipsMasterData', array('type' => 'transaksi', 'content' => $content));
        ?>

    </div>

    <?php $this->endWidget(); ?>

    <?php
    $urlGetMutasi = Yii::app()->createUrl('actionAjax/getMutasiObatAlkesDariTerimaMutasi');
    $idMutasiRuanganOA = CHtml::activeId($modMutasi, 'mutasioaruangan_id');
    $idRuanganAsal = CHtml::activeId($model, 'ruanganasal_id');
    $noMutasiOA = CHtml::activeId($modMutasi, 'nomutasioa');
    $tglMutasiOA = CHtml::activeId($modMutasi, 'tglmutasioa');
    ?>