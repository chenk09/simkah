<fieldset id="frm_data_dokter">
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <label class="control-label" for="FADokterV_nama_pegawai">Nama</label>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete',
                                array(
                                    'model'=>$modPegawai,
                                    'attribute'=>'nama_pegawai',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ListKaryawan'),
                                    'options'=>array(
                                        'class'=>'span3',
                                        'showAnim'=>'fold',
                                  
                                        'minLength' => 2,
                                        'select'=>'js:function( event, ui ){
                                            inputDataDokter(ui.item.pegawai_id);
                                            return false;
                                        }',
                                    ),
                                    'htmlOptions'=>array(
                                        'placeholder'=>'Ketikan Nama Karyawan',
//                                        'onblur'=>'cariDataPasien(this.value);',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'class'=>'span3'
                                    ),
                                    'tombolDialog'=>array(
                                        'idDialog'=>'dialogDokter'
                                    ),
                                )
                            ); 
                        ?>                    
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPegawai,'jeniskelamin',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($modPegawai,'nomorindukpegawai',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
            </td>
            <td>
                <?php echo $form->textAreaRow($modPegawai,'alamat_pegawai',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>


<!-- Dialog pencarian karyawan -->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Karyawan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>700,
        'height'=>450,
        'resizable'=>false,
    ),
));

$modPeg = new FAPegawaiM('searchByFilterKaryawan');
$modPeg->unsetAttributes();
if(isset($_GET['FAPegawaiM'])){
    $modPeg->attributes = $_GET['FAPegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'cari-dokter-m-grid',
    'dataProvider'=>$modPeg->searchByFilterKaryawan(),
    'filter'=>$modPeg,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectDokter",
                "onClick"=>"inputDataDokter(\"$data->pegawai_id\");return false;"
                )
            )'
        ),
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai'
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>

<script type="text/javascript">

function inputDataDokter(id_dokter)
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai'); ?>', {idPegawai:id_dokter},
        function(data)
        {
            $("#FAPenjualanResepT_pasienpegawai_id").val(data.pegawai_id);
            $("#dialogDokter").dialog('close');
            for(x in data)
            {
                $("#frm_data_dokter").find("input[name$='["+ x +"]']").val(data[x]);
            }
        },
        'json'
    );
}

</script>
    