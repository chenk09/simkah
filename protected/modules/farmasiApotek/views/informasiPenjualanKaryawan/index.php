<legend class="rim2">Informasi Penjualan Karyawan</legend>
<?php
$this->widget('bootstrap.widgets.BootAlert');

Yii::app()->clientScript->registerScript('cariPasien', "
$('#search').submit(function(){
	$.fn.yiiGridView.update('informasipenjualankaryawan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'search',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'method'=>'get',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'informasipenjualankaryawan-grid',
	'dataProvider'=>$modInfoPenjualan->searchInfoJualKaryawan(),
//        'filter'=>$modInfo,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Tgl Penjualan /<br/> Tgl Resep',
                'type'=>'raw',
                'value'=>'$data->tglpenjualan." / ".$data->tglresep',
            ),
            array(
                'header'=>'No Resep',
                'type'=>'raw',
                'value'=>'$data->noresep',
            ),
            array(
                'header'=>'Jenis Penjualan',
                'type'=>'raw',
                'value'=>'$data->jenispenjualan',
            ),
            array(
                'header'=>'Nama Pasien',
                'type'=>'raw',
                'value'=>'"$data->nama_pasien"."<br>".$data->getNamaPegawai($data->pasienpegawai_id)',
            ),
            array(
                'header'=>'Umur / <br> Jenis Kelamin',
                'type'=>'raw',
                'value'=>'"$data->umur"."<br/>"."$data->jeniskelamin"',
            ),
//            array(
//                'header'=>'Alamat',
//                'type'=>'raw',
//                'value'=>'$data->alamat_pasien',
//            ),
//            array(
//                'header'=>'Dokter',
//                'type'=>'raw',
//                'value'=>'($data->jenispenjualan == "PENJUALAN BEBAS" OR $data->nama_pegawai == "Eli Hismiati") ? "-" : $data->nama_pegawai',
//            ),
            array(
                'header'=>'Ubah',
                'type'=>'raw', 
//                'value'=>'(($data->nomorResepSudahBayar) ? "<center>-</center>" : CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl((($data->jenispenjualan == "PENJUALAN RESEP") ? "penjualanResep/jualResep" : (($data->jenispenjualan == "PENJUALAN BEBAS") ? "PenjualanBebas/JualBebas" : "PenjualanResepLuar/JualResep" )),array("idPenjualan"=>$data->penjualanresep_id, "idPendaftaran"=>$data->pendaftaran_id,"idPasien"=>$data->pasien_id)),
//                            array("class"=>"", 
//                                  "rel"=>"tooltip",
//                                  "title"=>"Klik untuk lihat ubah penjualan",
//                            )))',
                'value'=>'(!empty($data->nomorResepSudahBayar) ? "<center>-</center>" : CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl(("PenjualanKaryawan/updateJualResepKaryawan"),array("idPenjualan"=>$data->penjualanresep_id)),
                            array("class"=>"", 
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk lihat ubah penjualan",
                            )))',
            ),
            array(
                'header'=>'Detail Penjualan',
                'type'=>'raw', 
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("penjualanDokter/printFakturPenjualanDokter",array("id"=>$data->penjualanresep_id , "idPasien"=>$data->pasien_id)),
                            array("class"=>"", 
                                  "target"=>"iframePenjualanResep",
                                  "onclick"=>"$(\"#dialogDetailPenjualan\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk lihat detail penjualan",
                            ))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
            array(
                'header'=>'Batal / Retur Penjualan',
                'type'=>'raw', 
                'value'=>'(!empty($data->returresep_id)) ?  "Sudah Diretur" : 
                    (!empty($data->nomorResepSudahBayar) ? 
                         "Sudah Lunas": 
                         CHtml::Link("<i class=\"icon-remove\"></i>","",
                            array("class"=>"", 
                                  "href"=>"",
                                  "onclick"=>"batalPenjualanResep(".$data->penjualanresep_id.");return false;",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Batal Penjualan Resep",
                            ))."<br><br>".CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiPenjualanResep/returPenjualan",array("idPenjualanResep"=>$data->penjualanresep_id)),
                            array("class"=>"", 
                                  "target"=>"iframeReturPenjualan",
                                  "onclick"=>"cekHakRetur(this);return false;",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Retur Penjualan",
                            )))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
//            array(
//                'header'=>'Print Struk',
//                'type'=>'raw', 
//                'value'=>'CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("penjualanDokter/PrintFakturPenjualanDokter",array("id"=>$data->penjualanresep_id, "caraPrint"=>"PRINT")),
//                            array("class"=>"", 
//                                  "target"=>"iframeReturResep",
//                                  "onclick"=>"$(\"#dialogPrintPenjualan\").dialog(\"open\");",
//                                  "rel"=>"tooltip",
//                                  "title"=>"Klik untuk prin struk ",
//                            ))',
////                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiPenjualanResep/printStruk",array("id"=>$data->penjualanresep_id,"idPasien"=>$data->pasien_id)),
////                            array("class"=>"", 
////                                  "target"=>"iframeReturResep",
////                                  "onclick"=>"$(\"#dialogPrintPenjualan\").dialog(\"open\");",
////                                  "rel"=>"tooltip",
////                                  "title"=>"Klik untuk prin struk ",
////                            ))',
//                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
//            ),
            array(
                'header'=>'Copy Resep',
                'type'=>'raw', 
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("informasiPenjualanResep/CopyResep",array("idPenjualanResep"=>$data->penjualanresep_id,"idPasien"=>$data->pasien_id)),
                            array("class"=>"", 
                                  "target"=>"iframeCopyResep",
                                  "onclick"=>"$(\"#dialogCopyResep\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Copy Resep ",
                            ))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Tgl Penjualan','tglawal',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modInfoPenjualan,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                    //
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label(' Sampai dengan','tgl_akhir', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modInfoPenjualan,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group">
                    <?php echo CHtml::label('No Resep','no_resep',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo $form->textField($modInfoPenjualan,'noresep',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
            </td>
            <td>
<!--                <div class="control-group">
                    <?php //echo CHtml::label('Jenis Penjualan','jenispenjualan',array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php //echo $form->dropDownList($modInfoPenjualan,'jenispenjualan', Params::jenisPenjualan(),array('empty'=>'-- Pilih --')); ?>
                        <?php //echo $form->textField($modInfoPenjualan,'jenispenjualan',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>-->
                <?php // echo $form->textFieldRow($modInfoPenjualan,'no_rekam_medik',array('class'=>'span3 numbersOnly','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php // echo $form->textFieldRow($modInfoPenjualan,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
            <?php  
                $this->widget('TipsMasterData',array()); 
            ?>
    </div>

<?php $this->endWidget(); ?>

<?php 
// Dialog buat lihat penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetailPenjualan',
    'options'=>array(
        'title'=>'Detail Penjualan Resep',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframePenjualanResep" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end lihat penjualan resep dialog =============================
?>

<?php 
// Dialog buat Copy Resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogCopyResep',
    'options'=>array(
        'title'=>'Copy Resep',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframeCopyResep" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end Copy Resep dialog =============================
?>

<script type ='text/javascript'>
function cekHakRetur(obj)
{
    iframe = $(obj).attr("target");
    urlIframe = $(obj).attr("href");
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekHakRetur');?>', {idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){
        if(data.cekAkses){
            $("iframe[name*='"+iframe+"']").attr("src",urlIframe);
            $("#dialogReturPenjualan").dialog("open");
        } else {
            $('#loginDialog').dialog('open');    
        }
    }, 'json');
}
</script>
<?php 
// Dialog buat lihat Retur Penjualan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogReturPenjualan',
    'options'=>array(
        'title'=>'Retur Penjualan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
        'before'
    ),
));
?>
<iframe src="" name="iframeReturPenjualan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end lihat Retur Penjualan Dialog =============================
?>

<?php 
// Dialog buat retur penjualan resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPrintPenjualan',
    'options'=>array(
        'title'=>'Print Struk',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>700,
        'resizable'=>false,
        'close'=>"js:function(){ $.fn.yiiGridView.update('infopenjualanapotik-grid', {
                        data: $('#caripasien-form').serialize()
                    }); }",
    ),
));
?>
<iframe src="" name="iframeReturResep" width="100%" height="700" >
</iframe>
<?php
$this->endWidget();
//========= end retur penjualan resep dialog =============================
?>

<?php 
$urlBatalPenjualan = Yii::app()->createUrl($this->module->id.'/'.$this->id.'/batalPenjualanResep');
$notif = "Anda Yakin Membatalkan Penjualan Resep ini ?";
$jsFunctionOnHead = "
    function batalPenjualanResep(value){
        if (confirm('".$notif."')){
            $.post('$urlBatalPenjualan',{value:value},function(data){
                alert(data.info);
                if (data.status == 'success'){
                    $.fn.yiiGridView.update('informasipenjualankaryawan-grid', {
                        data: $('#search').serialize()
                    });
                }
            }, 'JSON');
        }
    }
";
Yii::app()->clientScript->registerScript('jsFunctionOnHead',$jsFunctionOnHead, CClientScript::POS_HEAD);
?>