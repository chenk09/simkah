<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>

<fieldset>
    <table>
        <tr>
            <td width ="50%">
                <?php echo $form->textFieldRow($model,'tglproduksiobt',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($model,'noproduksiobt',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>20)); ?>
            </td>
            <td>
                <?php echo $form->textAreaRow($model,'keternganprdobat',array('rows'=>3, 'cols'=>50, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <?php 
                    $criteria = new CDbCriteria();
                    $criteria->addInCondition(kelompokpegawai_id, array(1,3));
                    $criteria->order = nama_pegawai;
                    
                    echo $form->dropDownListRow($model,'pegawai_id',CHtml::listData(PegawaiM::model()->findAll($criteria), 'pegawai_id', 'NamaLengkap'),array('onchange'=>'setValue();','empty'=>'-- Pilih Dokter --','class'=>'span3 pegawai_id', 'onkeypress'=>"return $(this).focusNextInputField(event);")); 
                    echo CHtml::hiddenField('dokter_id','',array());
                ?>
                <div class="control-group">
                    <label class="control-label">Nama Obat</label>
                    <div class="controls">
                        <?php // echo CHtml::dropDownList('obatProduksi',null,  CHtml::listData(ObatalkesfarmasiV::model()->findAll(), 'obatalkes_id', 'obatalkes_nama'),array('empty'=>'-- Pilih Obat --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                        <?php echo CHtml::hiddenField('obatProduksiId');?>
                        <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            'name'=>'obatProduksi',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatAlkesProduksi').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                                   pegawai_id: $("#pegawai_id").val(),
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   // 'readonly'=>false,
                                                   'minLength' => 2,
                                                   'select'=>'js:function( event, ui ) {
                                                        $(this).val( ui.item.value);
                                                        $("#obatProduksiId").val(ui.item.obatalkes_id);
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array('class'=>'span3 obatProduksi','disabled'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)", 'placeholder'=>'Ketik Nama Obat'),
                                            'tombolDialog'=>array('idDialog'=>'dialogObatproduksiM'), //dialogObatproduksiM akak di switch otomastis jika berdasarkanSupplier di ceklis
                                        )); 
                        ?>
                    </div>
<!--                     <div class="controls"> 
                        <?php echo CHtml::checkBox('berdasarkanDokter', false,array('onclick'=>'setValue();', 'title'=>'Filter Obat Berdasarkan Dokter', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                        <?php echo $form->label($model,'Cek jika ingin menampilakan obat produksi berdasarkan dokter'); ?>
                    </div> -->
                </div>
            </td>
        </tr>
    </table>
</fieldset>   
	
<?php 
//========= Dialog buat cari data ObatalkesM=========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatalkesM',
    'options'=>array(
        'title'=>'Pencarian Obat Alkes',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkes = new ObatalkesM('search');
$modObatAlkes->unsetAttributes();
if(isset($_GET['ObatalkesM'])) {
    $modObatAlkes->attributes = $_GET['ObatalkesM'];
}
echo CHtml::hiddenField('forRow',null,array('readonly'=>true));
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'ObatalkesM-m-grid',
	'dataProvider'=>$modObatAlkes->search(),
	'filter'=>$modObatAlkes,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectObat",
                                    "onClick" => "
                                                var row = $(\"#forRow\").val();
                                                if(row.length > 0){ //klik perbaris
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_obatalkes_id\").val(\"$data->obatalkes_id\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_obatalkes_nama\").val(\"$data->obatalkes_nama\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_obatalkes_kode\").val(\"$data->obatalkes_kode\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_hargasatuan\").val(\"$data->hjaresep\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_harganetto\").val(\"$data->harganetto\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_hpp\").val(\"$data->hpp\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_satuankecil_id\").val(\"$data->satuankecil_id\");
                                                    $(\"#FAProduksiobatdetT_\"+row+\"_satuankecil_nama\").val(\"$data->SatuankecilNama\");
                                                }else{
                                                  $(\"#obatProduksiId\").val(\"$data->obatalkes_id\");
                                                  $(\"#obatProduksi\").val(\"$data->obatalkes_nama\");
                                                }
                                                //setTindakanAuto($data->obatalkes_id);
                                                $(\"#dialogObatalkesM\").dialog(\"close\"); 
                                                return false;
                                        "))',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkes, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes_nama',
                ),
                array(
                    'header'=>'Asal Barang',
                    'value'=>'$data->sumberdana->sumberdana_nama',
                ),
                array(
                    'header'=>'Kadar Obat',
                    'value'=>'$data->obatalkes_kadarobat',
                ),
                array(
                    'header'=>'Kemasan Besar',
                    'value'=>'$data->kemasanbesar',
                ),
                array(
                    'header'=>'Kekuatan',
                    'value'=>'$data->kekuatan',
                ),
                array(
                    'header'=>'Tgl Kadaluarsa',
                    'value'=>'$data->tglkadaluarsa',
                ),
            ),
            'afterAjaxUpdate' => 'function(id, data){
            jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
        ));
$this->endWidget();
//========= end ObatalkesM dialog =============================
?>

<?php 
//========= Dialog buat cari data ObatsupplierM =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogObatproduksiM',
    'options'=>array(
        'title'=>'Pencarian Obat Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatAlkesProduksi = new ObatalkesproduksiM('searchDialog');
$modObatAlkesProduksi->unsetAttributes();
$modObatAlkesProduksi->pegawai_id = $var['pegawai_id'];
if(isset($_GET['ObatalkesproduksiM'])) {
    $modObatAlkesProduksi->attributes = $_GET['ObatalkesproduksiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
        'id'=>'ObatproduksiM-m-grid',
        'dataProvider'=>$modObatAlkesProduksi->searchDialog(),
        'filter'=>$modObatAlkesProduksi,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#obatProduksiId\").val(\"$data->obatalkesproduksi_id\");
                                                  $(\"#obatProduksi\").val(\"".$data->obatalkes->obatalkes_nama."\");
                                                  $(\"#dokter_id\").val(\"".$data->pegawai_id."\");
                                                  submitObat();
                                                  $(\"#dialogObatproduksiM\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'Dokter / Pegawai',
                    'filter'=>CHtml::activeHiddenField($modObatAlkesProduksi, 'nama_pegawai'),
                    'value'=>'$data->pegawai->NamaLengkap',
                ),
                array(
                    'header'=>'Kategori Obat',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Golongan Obat',
                    'value'=>'$data->obatalkes->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kode Obat',
                    'filter'=>  CHtml::activeTextField($modObatAlkesProduksi, 'obatalkes_kode'),
                    'value'=>'$data->obatalkes->obatalkes_kode',
                ),
                array(
                    'header'=>'Nama Obat Alkes',
                    'filter'=>  CHtml::activeTextField($modObatAlkesProduksi, 'obatalkes_nama'),
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                
  ),
        'afterAjaxUpdate' => 'function(id, data){
        $("#ObatproduksiM_pegawai_id").val($("#pegawai_id").val());
        jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
$this->endWidget();
//========= end ObatsupplierM dialog =============================
?>
<script>
    // the subviews rendered with placeholders
    var trBahan=new String(<?php echo CJSON::encode($this->renderPartial('_rowDetailProduksi',array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form,'removeButton'=>true),true));?>);
    var trBahanFirst=new String(<?php echo CJSON::encode($this->renderPartial('_rowDetailProduksi',array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form,'removeButton'=>false),true));?>);
    
    function get_qty_a(obj){
        var qty = $(obj).val();
        // alert (qty);
        $(obj).parents("tr").find('input[name$="[qtyproduksi]"]').val(Math.ceil(qty));
    }
    // function pilihDokter(obj){}    
    function pilihDokter(obj){
        $('#obatProduksiId').val("");
        $('#obatProduksi').val("");
        if($(obj).val()){
            $("#obatProduksi").parent().find("a").removeAttr("onclick");
            $("#obatProduksi").parent().find("a").attr("onclick",'$(\"#dialogObatproduksiM\").dialog(\"open\");return false;');
        }
        // }else{
        //     // alert (id);
        //     $("#obatProduksi").parent().find("a").removeAttr("onclick");
        //     $("#obatProduksi").parent().find("a").attr("onclick",'$(\"#dialogObatalkesM\").dialog(\"open\");return false;');
        // }
    }
    
    function addRowBahan(obj)
    {
        $(obj).parents('table').children('tbody').append(trBahan.replace());
        <?php 
        $attributes = $modDetail->attributeNames(); 
            foreach($attributes as $i=>$attribute){
                echo "renameInput('FAProduksiobatdetT','$attribute');";
            }
        ?>
        renameInput('FAProduksiobatdetT','obatalkes_kode');
        renameInput('FAProduksiobatdetT','obatalkes_nama');
        renameInput('FAProduksiobatdetT','dosis');
        renameInput('FAProduksiobatdetT','kemasan');
        renameInput('FAProduksiobatdetT','kekuatan');
        renameInput('FAProduksiobatdetT','qtyproduksi');
        renameInput('FAProduksiobatdetT','satuankecil_nama');
        
        $(obj).parents('tr').find('input[name$="[obatalkes_nama]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                    $(this).val("");
                                                                                    return false;
                                                                                },'select':function( event, ui ) {
//                                                                                    setTindakan(this, ui.item);
                                                                                    $(this).val(ui.item.value);
                                                                                    $(this).parents("tr").find("input[name$=\"[obatalkes_id]\"]").val(ui.item.obatalkes_id);
                                                                                    $(this).parents("tr").find("input[name$=\"[obatalkes_kode]\"]").val(ui.item.obatalkes_kode);
                                                                                    $(this).parents("tr").find("input[name$=\"[hargasatuan]\"]").val(ui.item.hjaresep);
                                                                                    $(this).parents("tr").find("input[name$=\"[harganetto]\"]").val(ui.item.harganetto);
                                                                                    $(this).parents("tr").find("input[name$=\"[hpp]\"]").val(ui.item.hpp);
                                                                                    $(this).parents("tr").find("input[name$=\"[satuankecil_id]\"]").val(ui.item.satuankecil_id);
                                                                                    $(this).parents("tr").find("input[name$=\"[satuankecil_nama]\"]").val(ui.item.satuankecil_nama);
                                                                                    return false;
                                                                                },'source':function(request, response) {
                                                                                                $.ajax({
                                                                                                    url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/ObatAlkesProduksi');?>",
                                                                                                    dataType: "json",
                                                                                                    data: {
                                                                                                        term: request.term,
                                                                                                    },
                                                                                                    success: function (data) {
                                                                                                        response(data);
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                });   
                                                                                
        $(obj).parents('table').find('.integer').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        $(obj).parents('table').find('.float').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
    }
 
    function batalBahan(obj)
    {
        if(confirm('Apakah anda yakin akan membatalkan bahan ini ?')){
            $(obj).parents('tr').next('tr').detach();
            $(obj).parents('tr').detach();
            
            <?php 
            $attributes = $modDetail->attributeNames(); 
                foreach($attributes as $i=>$attribute){
                    echo "renameInput('FAProduksiobatdetT','$attribute');";
                }
            ?>
        renameInput('FAProduksiobatdetT','obatalkes_kode');
        renameInput('FAProduksiobatdetT','obatalkes_nama');
        renameInput('FAProduksiobatdetT','dosis');
        renameInput('FAProduksiobatdetT','kemasan');
        renameInput('FAProduksiobatdetT','kekuatan');
        renameInput('FAProduksiobatdetT','qtyproduksi');
        renameInput('FAProduksiobatdetT','satuankecil_nama');

        }
    }
    
    function renameInput(modelName,attributeName)
    {
        var trLength = $('#tblDetailProduksi tr').length;
        var i = -1;
        $('#tblDetailProduksi tr').each(function(){
            if($(this).has('input[name$="[obatalkes_id]"]').length){
                i++;
            }
            $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
            $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
            $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
            $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
            $(this).find('input[id="row"]').attr('value',i);
            $(this).find('input[id="row"]').val(i);
//            $(this).find('input[name$="[obatalkes_nama]"]').addClass('ui-autocomplete-input');
            $(this).find('input[name$="[obatalkes_nama]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                    $(this).val("");
                                                                                    return false;
                                                                                },'select':function( event, ui ) {
//                                                                                    setTindakan(this, ui.item);
                                                                                    $(this).val(ui.item.value);
                                                                                    $(this).parents("tr").find("input[name$=\"[obatalkes_id]\"]").val(ui.item.obatalkes_id);
                                                                                    $(this).parents("tr").find("input[name$=\"[obatalkes_kode]\"]").val(ui.item.obatalkes_kode);
                                                                                    $(this).parents("tr").find("input[name$=\"[hargasatuan]\"]").val(ui.item.hjaresep);
                                                                                    $(this).parents("tr").find("input[name$=\"[harganetto]\"]").val(ui.item.harganetto);
                                                                                    $(this).parents("tr").find("input[name$=\"[hpp]\"]").val(ui.item.hpp);
                                                                                    $(this).parents("tr").find("input[name$=\"[satuankecil_id]\"]").val(ui.item.satuankecil_id);
                                                                                    $(this).parents("tr").find("input[name$=\"[satuankecil_nama]\"]").val(ui.item.satuankecil_nama);
                                                                                    return false;
                                                                                },'source':function(request, response) {
                                                                                                $.ajax({
                                                                                                    url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/ObatAlkesProduksi');?>",
                                                                                                    dataType: "json",
                                                                                                    data: {
                                                                                                        term: request.term,
                                                                                                    },
                                                                                                    success: function (data) {
                                                                                                        response(data);
                                                                                                    }
                                                                                                })
                                                                                            }
                                                                                });   
        });
        clear();
    }
    
    function clear(){
        urut = 1;
            $(".noUrut").each(function(){
                    $(this).val(urut);
                     urut++;
                });
    }
    
    function setDialog(obj){
        var row = $(obj).parents('tr').find('#row').val();
        $('#dialogObatalkesM #forRow').val(row);
        $('#dialogObatalkesM').dialog('open');
    }    
    
    function hitungQty(obj){
        var dosis = unformatNumber(parseFloat($(obj).parents('tr').find('input[name$="[dosis]"]').val()));
        var kemasan = unformatNumber(parseFloat($(obj).parents('tr').find('input[name$="[kemasan]"]').val()));
        var kekuatan = unformatNumber(parseFloat($(obj).parents('tr').find('input[name$="[kekuatan]"]').val()));
        if(kekuatan > 0)
            var qty = dosis * kemasan / kekuatan;
        else
            var qty = 0;
        $(obj).parents('tr').find('input[name$="[qtyproduksi]"]').val(formatDesimal(parseFloat(qty)));
    }
</script>


<?php
$urlGetObatProduksi = Yii::app()->createUrl('actionAjax/getObatProduksi');
$inputObat = CHtml::activeId($model, 'obatProduksi');
$urlGetDokter = Yii::app()->createUrl('actionAjax/getDokter');
$urlHalamanIni = Yii::app()->createUrl($this->route);
$pegawai_id = CHtml::activeId($model, 'pegawai_id');
$obatProduksi = CHtml::activeId($model, 'obatProduksi');

$jscript = <<< JS
function setValue()
{
    id = $('#${pegawai_id}').val();
    pegawai_id = $('#pegawai_id').val();
    $('#${obatProduksi}').val('');
    $('#obatProduksiId').val('');
    if(id == ""){
        $("#berdasarkanDokter").removeAttr("checked");
        alert("Anda belum memilih Dokter !");
    }
    
    if($("#berdasarkanDokter").is(":checked")){
        $("#pegawai_id").val(id);
        $("#obatProduksi").removeAttr("onclick");
        $("#obatProduksi").attr("onclick",'$(\"#dialogObatproduksiM\").dialog(\"open\");return false;');
        $.post("${urlGetDokter}", {pegawai_id:id},
        function(data){
           $('#pegawai_id').val(data.pegawai_id);
//         ERROR INPUT number >>  $(".numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":",","thousands":"","precision":0,"symbol":null});
           $.get('${urlHalamanIni}', {pegawai_id:data.pegawai_id}, function(datas){
                $.fn.yiiGridView.update('ObatproduksiM-m-grid', {
                    url: document.URL+'&ObatproduksiM%5Bpegawai_id%5D='+data.pegawai_id,
                }); 
            });

        }, "json");
        
    }else{
        id = "";
        $("#pegawai_id").val(id);
        //switch to dialogObatAlkesM
        $("#${inputObat}").parent().find("a").removeAttr("onclick");
        $("#${inputObat}").parent().find("a").attr("onclick",'$(\"#dialogObatAlkesM\").dialog(\"open\");return false;');
    }
    
    
}


function submitObat()
{
    obatProduksiId = $('#obatProduksiId').val();
    obatProduksi = $('#obatProduksi').val();
    pegawai_id = $('#FAProduksiobatT_pegawai_id').val();
    dokter_id = $('#dokter_id').val();

    if(pegawai_id =='')
    {
        alert('Silahkan Pilih Dokter Terlebih Dahulu');
    }else{
            $.post("${urlGetObatProduksi}", { obatProduksiId: obatProduksiId, obatProduksi: obatProduksi, pegawai_id:pegawai_id, dokter_id:dokter_id},
            function(data){
                $('#tblDetailProduksi tbody').append(data.tr);
                renameInput('FAProduksiobatdetT','obatalkes_kode');
                renameInput('FAProduksiobatdetT','obatalkes_nama');
                renameInput('FAProduksiobatdetT','dosis');
                renameInput('FAProduksiobatdetT','kemasan');
                renameInput('FAProduksiobatdetT','kekuatan');
                renameInput('FAProduksiobatdetT','qtyproduksi');
                renameInput('FAProduksiobatdetT','satuankecil_nama');
                $("#tblDetailProduksi tbody").find('.integer').maskMoney({"defaultZero":true, "allowZero":true, "decimal":",", "thousands":".", "symbol":null, "precision":0});
                $("#tblDetailProduksi tbody").find('.float').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
                clear();
                
            }, "json");
    }   
}

function remove(obj) {
    if(confirm('Apakah anda yakin akan membatalkan bahan ini ?')){
        $(obj).parents('tr').remove();
    }
}

function clear(){
    
    urut = 1;
    $(".noUrut").each(function(){
        $("#ObatsupplierM_obatAlkes").val("");
        $("#GFObatSupplierM_supplier_id").val();
            $(this).val(urut);
             urut++;
    });
}
JS;
Yii::app()->clientScript->registerScript('masukanobat',$jscript, CClientScript::POS_HEAD);
?>
