<fieldset>
<legend class="rim">Detail Bahan</legend>
<table class="table table-striped table-bordered table-condensed" id="tblDetailProduksi">
    <thead>
        <tr>
            <th>No.</th>
            <th>Kode Bahan</th>
            <th>Nama Bahan</th>
            <th>Dosis</th>
            <th>Kemasan</th>
            <th>Kekuatan</th>
            <th>Qty</th>
            <th>Satuan</th>
            <th>Tindakan</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if(count($dataDetails) > 0){
            foreach($dataDetails AS $row => $data){
//              MENGEMBALIKAN YANG DI POST TABEL
                $modDetail->attributes = $data;
                $modDetail->obatalkes_kode = $data['obatalkes_kode'];
                $modDetail->obatalkes_nama = $data['obatalkes_nama'];
                $modDetail->dosis = $data['dosis'];
                $modDetail->kemasan = $data['kemasan'];
                $modDetail->kekuatan = $data['kekuatan'];
                $modDetail->satuankecil_nama = $data['satuankecil_nama'];
                echo $this->renderPartial('_rowDetailProduksi', array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form, 'row'=>$row)); 
            }
        }else{
            echo $this->renderPartial('_rowDetailProduksi', array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form, 'row'=>0)); 
        }
        ?>
    </tbody>
</table>
</fieldset>
