<style>
    .integer, .float{
        text-align: right;
    }
</style>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.integer',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.float',
    'currency'=>'PHP',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));
?>
<legend class="rim2">Transaksi Produksi Obat</legend>
<?php
$this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'faproduksiobat-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'return cekInput();'),
        'focus'=>'#FAProduksiobatT_noproduksiobt',
)); ?>
<?php echo $form->errorSummary(array($model)); ?>
<?php echo isset($modObatalkesM) ? $form->errorSummary(array($modObatalkesM)) : ""; ?>
<?php echo $this->renderPartial('_form', array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form)); ?>
<?php echo $this->renderPartial('_formDetailProduksi', array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form, 'dataDetails'=>$dataDetails)); ?>
<?php echo $this->renderPartial('_formObatalkes', array('model'=>$model,'modDetail'=>$modDetail,'modObatalkesM'=>$modObatalkesM,'form'=>$form)); ?>

<div class="form-actions">
<?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                     Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)')); ?>
<?php echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), 
        Yii::app()->createUrl($this->route), 
        array('class'=>'btn btn-danger',
              'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
</div>   
<?php $this->endWidget(); ?>
<?php $this->widget('TipsMasterData',array('type'=>'create'));?>

<script>
    function cekInput()    {
        $('.float').each(function(){this.value = unformatNumber(this.value)});
        $('.integer').each(function(){this.value = unformatNumber(this.value)});
        return true;
    }
    function formatInputLoad(){
        $('.float').each(function(){this.value = formatNumber(this.value)});
        $('.integer').each(function(){this.value = formatNumber(this.value)});
    }
    formatInputLoad();
</script>