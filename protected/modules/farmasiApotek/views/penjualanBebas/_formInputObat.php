<table>   
    <tr>
        <td>
            <fieldset id="formNonRacikan" class="table-bordered">
                <legend class="table-bordered radio">
                    <?php echo CHtml::radioButton('pilihNonRacik', true, array('onclick'=>'enableNonRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Non Racikan
                </legend>
                
                <div class="control-group ">
                    <label class="control-label" for="namaObat">Nama Obat</label>
                    <div class="controls">
                        <div class="row-fluid">
                            <div class="span5">
                                <?php
                                $this->widget('MyJuiAutoComplete', array(
                                            'name'=>'namaObatNonRacik',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatReseptur').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                                   idSumberDana: $("#idSumberDana").val(),
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                        'minLength' => 2,
//                                                   'focus'=> 'js:function( event, ui ) {
//                                                        $(this).val( ui.item.label);
//                                                        return false;
//                                                    }',
                                                   'select'=>'js:function( event, ui ) {
                                                       $(this).val( ui.item.label);
                                                        $("#idObat").val(ui.item.obatalkes_id); 
                                                        $("#hargaSatuan").val(((ui.item.harganetto < 1) ? ui.item.hargajual : ui.item.harganetto)); 
                                                        $("#hargaNetto").val(ui.item.harganetto); 
                                                        $("#hargaNetto_a").val(ui.item.harganetto); 
                                                        $("#hargaJual").val(ui.item.hargajual); 
                                                        $("#satuanKekuatan").val(ui.item.satuankekuatan);
                                                        $("#kekuatan").val(ui.item.kekuatan);
                                                        $("#jmlKemasan").val(ui.item.kemasanbesar); 
                                                        $("#namaObat").val(ui.item.obatalkes_kode+" - "+ui.item.obatalkes_nama);
                                                        $("#discountObat").val(ui.item.diskonJual);
                                                        $("#jmlStok").val(ui.item.minimalstok);
                                                        $("#idSumberDana").val(ui.item.sumberdana_id);
                                                        $("#kadaluarsa").val(ui.item.kadaluarsa);
                                                        $("#namaSumberDana").val(ui.item.sumberdana_nama);
                                                        $("#idSatuanKecil").val(ui.item.satuankecil_id);
                                                        $("#idJenisObat").val(ui.item.jenisobatalkes_id);
                                                        $("#kategoriObat").val(ui.item.obatalkes_kategori);
                                                        $("#stokObat").val(getStokObat(ui.item.obatalkes_id));
                                                        $("#jnskelompok").val(ui.item.jnskelompok);
                                                        $("#isRacikan").val("0");
                                                        
                                                        $("#hjaresep").val(ui.item.hjaresep);
                                                        $("#hjanonresep").val(ui.item.hjanonresep);
                                                        $("#hpp").val(ui.item.hpp);
                                                        
                                                        return false;
                                                    }',
                                    ),
                                    'tombolDialog' => array('idDialog' => 'dialogObat'),
                                    'htmlOptions' => array('onkeypress' => "return $(this).focusNextInputField(event)", 'class'=>'span3', ),
                                ));
                                ?>
                            </div>
                            <div class="span6">
                                <?php
                                $satuanObat = EtiketSatuan::items();
                                echo CHtml::dropDownList("satuan_obat", "", $satuanObat, array('empty' => '-- Pilih --', 'style' => 'width:100px', 'onkeypress' => "return $(this).focusNextInputField(event)"));
                                echo CHtml::dropDownList("caramakan_obat", "", array("Sebelum Makan" => "Sebelum Makan", "Sesudah Makan" => "Sesudah Makan","Selagi Makan" => "Selagi Makan"), array('style' => 'margin-left:10px;width:150px', 'empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)"));
                                ?>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Signa</label>
                    <div class="controls">
                        <?php echo CHtml::textField('signa','',array('placeholder'=>'-- Aturan Pakai --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="qty">Qty</label>
                    <div class="controls">
                        <?php echo CHtml::textField('qtyNonRacik', '', array('readonly'=>false,'onblur'=>'$("#qty").val(this.value);','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="keterangan_non_racik">Keterangan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('keterangan_non_racik', '', array('readonly' => false, 'onkeypress' => "return $(this).focusNextInputField(event)", 'class' => 'span3')) ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'addDataResep();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"addDataResep(); return $('#namaObatNonRacik').focus()",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik untuk menambahkan resep",)); ?>
                    </div>
                </div>
            </fieldset>
            <fieldset id="obatTemp">
            <?php echo CHtml::hiddenField('idObat', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hargaSatuan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hargaNetto', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hargaNetto_a', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hargaJual', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('kekuatan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('satuanKekuatan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('jmlPermintaan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('jmlKemasan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('qty', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('discountObat', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('namaObat', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('idSumberDana', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('namaSumberDana', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('idSatuanKecil', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('stokObat', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('jmlStok', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('isRacikan', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('kadaluarsa', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('kategoriObat', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('jnskelompok', '', array('readonly'=>true)) ?>
            <?php //echo CHtml::hiddenField('idJenisObat', $jenisobatalkes->jenisobatalkes_id, array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('biayaadministrasi', $konfigFarmasi->administrasi, array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('biayaKemasan', $nonRacikan->biayakemasan, array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('tarifService', $nonRacikan->tarifservice, array('readonly'=>true)) ?>
            
            <?php echo CHtml::hiddenField('hjaresep', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hjanonresep', '', array('readonly'=>true)) ?>
            <?php echo CHtml::hiddenField('hpp', '', array('readonly'=>true)) ?>
            </fieldset>
        </td>
        <td>
            <fieldset id="formRacikan" class="table-bordered">
                <legend class="table-bordered radio">
                    <?php echo CHtml::radioButton('pilihRacik', false, array('onclick'=>'enableRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Racikan  &nbsp;
                </legend>
                
                <div class="control-group ">
                    <label class="control-label" for="racikanKe">R ke</label>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('racikanKe', '', Params::listAngka20(),array('disabled'=>true,'class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="namaObatRacik">Nama Obat</label>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete', array(
                                            'name'=>'namaObatRacik',
                                            'source'=>'js: function(request, response) {
                                                           $.ajax({
                                                               url: "'.Yii::app()->createUrl('ActionAutoComplete/ObatReseptur').'",
                                                               dataType: "json",
                                                               data: {
                                                                   term: request.term,
                                                                   idSumberDana: $("#idSumberDana").val(),
                                                               },
                                                               success: function (data) {
                                                                       response(data);
                                                               }
                                                           })
                                                        }',
                                             'options'=>array(
                                                   'showAnim'=>'fold',
                                                   'minLength' => 2,
//                                                   'focus'=> 'js:function( event, ui ) {
//                                                        $(this).val( ui.item.label);
//                                                        return false;
//                                                    }',
                                                   'select'=>'js:function( event, ui ) {
                                                       $(this).val( ui.item.label);
                                                        $("#idObat").val(ui.item.obatalkes_id);  
                                                        $("#hargaSatuan").val(((ui.item.harganetto < 1) ? ui.item.hargajual : ui.item.harganetto)); 
                                                        $("#hargaNetto").val(ui.item.harganetto); 
                                                        $("#hargaNetto_a").val(ui.item.harganetto); 
                                                        $("#hargaJual").val(ui.item.hargajual); 
                                                        $("#discountObat").val(ui.item.diskonJual); 
                                                        $("#satuanKekuatan").val(ui.item.satuankekuatan);
                                                        $("#kekuatan").val(ui.item.kekuatan);
                                                        $("#jmlKemasan").val(ui.item.kemasanbesar); 
                                                        $("#namaObat").val(ui.item.obatalkes_kode+" - "+ui.item.obatalkes_nama);
                                                        $("#jmlStok").val(ui.item.minimalstok);
                                                        $("#kekuatanObat").val(ui.item.kekuatan);
                                                        $("#satuanKekuatanObat").html(ui.item.satuankekuatan); 
                                                        $("#idSumberDana").val(ui.item.sumberdana_id);
                                                        $("#namaSumberDana").val(ui.item.sumberdana_nama);
                                                        $("#kadaluarsa").val(ui.item.kadaluarsa);
                                                        $("#idSatuanKecil").val(ui.item.satuankecil_id);
                                                        $("#stokObat").val(getStokObat(ui.item.obatalkes_id));
                                                        $("#idJenisObat").val(ui.item.jenisobatalkes_id);
                                                        $("#kategoriObat").val(ui.item.obatalkes_kategori);
                                                        $("#jnskelompok").val(ui.item.jnskelompok);
                                                        $("#isRacikan").val("1");
                                                        
                                                        $("#hjaresep").val(ui.item.hjaresep);
                                                        $("#hjanonresep").val(ui.item.hjanonresep);
                                                        $("#hpp").val(ui.item.hpp);
                                                        
                                                        return false;
                                                    }',
                                            ),
                                            'htmlOptions'=>array('disabled'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                            'tombolDialog'=>array('idDialog'=>'dialogObat'),
                                        )); 
                        ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="permintaan">Permintaan Dosis</label>
                    <div class="controls">
                        <?php echo CHtml::textField('permintaan', '', array('disabled'=>true,'onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="jmlKemasan">Jml Kemasan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('jmlKemasanObat', '', array('disabled'=>true,'onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="kekuatanObat">Kekuatan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('kekuatanObat', '', array('disabled'=>true,'onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                        <span id="satuanKekuatanObat"></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Signa</label>
                    <div class="controls">
                        <?php echo CHtml::textField('signa_racik','',array('disabled'=>true, 'placeholder'=>'-- Aturan Pakai --','class'=>'span2', 'style'=>'width:80px', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="qty">Qty</label>
                    <div class="controls">
                        <?php echo CHtml::textField('qtyRacik', '', array('disabled'=>true, 'onkeyup'=>'$("#qty").val($(this).val());','readonly'=>false,'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="keterangan_racik">Keterangan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('keterangan_racik', '', array('disabled'=>true,  'onkeypress' => "return $(this).focusNextInputField(event)", 'class' => 'span3')) ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'addDataResep();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"addDataResep(); return $('#racikanKe').focus()",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik untuk menambahkan resep",
                                      'disabled'=>true,)); ?>
                    </div>
                </div>
                <div style='border:1px solid #cccccc; border-radius:2px;padding:10px; width: 42%;float:right;margin-top:-80px;'>
                <font style='font-size:9pt'>Keterangan : <br>
                <font style='font-size:8pt'>Qty=Permintaan*Jml Kemasan/Kekuatan</font>
                    
            </fieldset>
        </td>
    </tr>
</table>

<script type="text/javascript">
function hitungQtyRacikan()
{
    var permintaan = $('#permintaan').val();
    var jmlKemasan = $('#jmlKemasanObat').val();
    var kekuatan = $('#kekuatanObat').val();
    var qty = permintaan * jmlKemasan / kekuatan;
    qty = Math.ceil(qty);
    if (jQuery.isNumeric(permintaan)){
        $('#jmlPermintaan').val(permintaan);
    }
    if (jQuery.isNumeric(kekuatan)){
        $('#kekuatan').val(kekuatan);
    }
    if (jQuery.isNumeric(jmlKemasan)){
        $('#jmlKemasan').val(jmlKemasan);
    }
    if (jQuery.isNumeric(qty)){
        $('#qty').val(qty);
    }
    if (jQuery.isNumeric(qty)){
        $('#qtyRacik').val(qty);
    }
}

function getStokObat(idObatalkes)
{
    var stok = 0;
    $.post('<?php echo Yii::app()->createUrl('ActionAutoComplete/GetStokObat'); ?>', {idObatalkes:idObatalkes}, function(data){
        $('#stokObat').val(data.stok);
    }, 'json');
    
    return stok;
}
    
function addDataResep()
{
    var isHrgGlobal = parseInt(<?php echo $konfigFarmasi->hargajualglobal; ?>);
    
    var R = 'R/';
    var qty = Math.ceil($('#qty').val());
    var stokObat = $('#stokObat').val();
    var idSumberDana = $('#idSumberDana').val();
    var kadaluarsa = $('#kadaluarsa').val();
    var discountObat = $('#discountObat').val();
    var idJenisObat = $('#idJenisObat').val();
    var kategoriObat = $('#kategoriObat').val();
    var jenis_kelompok = $('#jnskelompok').val();
    //var namaSumberDana = $('#namaSumberDana').val();
    var idObat = $('#idObat').val();
    var namaObat = $('#namaObat').val();
    var temp = 0; var RkeMax = 0;
    var idSatuanKecil = $('#idSatuanKecil').val();
    var hjaResep = $('#hjaresep').val(); 
    var hjaNonresep = $('#hjanonresep').val(); 
    var hargaSatuan = $('#hjanonresep').val(); //hja non-resep karena penjualan tanpa resep
    var hargaNetto = parseFloat($('#hpp').val());
    var hargaNetto_a = parseFloat($('#hargaNetto_a').val());
    hargaSatuan = parseInt(hargaSatuan);
    var hargaJual = $('#hargaJual').val();
    var signa = $('#signa').val();
    if($('#signa').val() == ""){
        signa = $('#signa_racik').val();
    }
    var etiketSatuan ="";
    var etiketCaraMakan ="";
    var etiketKeterangan = $("#keterangan_racik").val();
    if ($("#pilihNonRacik").is(":checked")){
        etiketSatuan = $("#satuan_obat").val();
        etiketCaraMakan = $("#caramakan_obat").val();
        etiketKeterangan =$("#keterangan_non_racik").val();
    }
    var kekuatan = $('#kekuatan').val();
    var satuanKekuatan = $('#satuanKekuatan').val();
    var jmlPermintaan = $('#jmlPermintaan').val();
    var jmlKemasan = $('#jmlKemasanObat').val();
    var i = $('#tblDaftarResep tr').length;
    var isRacikan = $('#isRacikan').val();
    var biayaKemasan = $('#biayaKemasan').val();
    var biayaAdministrasi = 0;
//    var biayaService = $('#tarifService').val();
    var biayaService = 0;
     // Hanya Ada di Aplikasi Tiar Medika //
//    var biayaService = ((idJenisObat == 3 || kategoriObat == "GENERIK") ? 0 : parseFloat(biayaService));
     // End Hanya Ada di Aplikasi Tiar Medika //
    var jasaDokterResep = 0;
    var jasaDokterResep = hargaJual * parseFloat(<?php echo MyFunction::calculate_string($konfigFarmasi->formulajasadokter) ?>) * qty;
    
    if(idObat==''){
        alert('Obat Masih Kosong');
        return false;
    }
    

    if(!jQuery.isNumeric(qty)){
        alert('Quantity tidak boleh Kosong !');
        return false;
    }else{
        if(qty == 0){
            alert('Quantity tidak boleh nol !');
            return false;
        }
    }
    
    if (jQuery.isNumeric(kadaluarsa)){
        if (kadaluarsa == 1){
            var obat_param = idObat + "-" + namaObat + "-" + kategoriObat;
            var params={
                instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                modul_id:<?php echo Yii::app()->session['modulId'];?>,
                create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                judulnotifikasi:"OBAT SUDAH KADALUARSA",
                isinotifikasi:obat_param
            };
            insert_notifikasi(params);
            alert("Obat Sudah Kadaluarsa, silahkan cek tanggal kadaluarsa !");
//            return false;
        }
    }
    
    if (jQuery.isNumeric(stokObat)){
        if (stokObat < 1){
            clearInputan();
            
            var obat_param = idObat + "-" + namaObat + "-" + kategoriObat;
            var params={
                instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                modul_id:<?php echo Yii::app()->session['modulId'];?>,
                create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                judulnotifikasi:"STOK OBAT KOSONG",
                isinotifikasi:obat_param
            };
            insert_notifikasi(params);
            
            alert("Stok Obat Kosong !");
            return false;
        }else{
            if(stokObat == jmlStok)
            {
                var obat_param = idObat + "-" + namaObat + "-" + kategoriObat;
                var params={
                    instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                    modul_id:<?php echo Yii::app()->session['modulId'];?>,
                    create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                    judulnotifikasi:"STOK OBAT MINIMUM",
                    isinotifikasi:obat_param
                };
                insert_notifikasi(params);

                alert("Stok Obat Minimum !");                
            }        
        }
    }
    
    if (jQuery.isNumeric(idObat)){
        if ($('#formNonRacikan #pilihNonRacik').is(':checked')){
            parent = $("#tblDaftarResep tbody").find('input[name$="[isRacikan]"][value="'+isRacikan+'"]').parents("tr").find('.obat[value="'+idObat+'"]');
        } else if($('#formRacikan #pilihRacik').is(':checked')) {
            rKe = $('#racikanKe').val();
            parent = $("#tblDaftarResep tbody").find('input[name$="[rke]"][value="'+rKe+'"]').parents("tr").find('input[name$="[isRacikan]"][value="'+isRacikan+'"]').parents("tr").find('.obat[value="'+idObat+'"]');
        }
        
        jumlahObat = parent.length;
        if (jumlahObat > 0){
            qtyObat = parseFloat(parent.parents("tr").find('input[name$="[qty]"]').val());
            qtyObat += parseFloat(qty);
            if (jQuery.isNumeric(qtyObat)){
                parent.parents("tr").find('input[name$="[qty]"]').val(qtyObat);
            }
            clearInputan();
            hitungTotalSemua();
            return false;
        }
    }
    
    
    var ceklist = '<?php echo CHtml::checkBox("penjualanResep[0][pilihObat]", true, array('style'=>'display:none;','readonly'=>true,'onchange'=>'hitungTotalSemua();','uncheckValue'=>'0')) ?>';
    var detail = '<?php echo CHtml::hiddenField("penjualanResep[0][detailreseptur_id]", 1, array('readonly'=>true)) ?>';
    var inputR = '<?php echo CHtml::hiddenField('penjualanResep[0][r]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) ?>';
    var inputRke = '<?php echo CHtml::textField('penjualanResep[0][rke]', '', array('readonly'=>true,'style'=>'width:15px;')) ?>';
    var inputObat = '<?php echo CHtml::hiddenField('penjualanResep[0][obatalkes_id]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1 obat')) ?>';
    var inputPilihHarga = <?php echo json_encode(CHtml::dropDownList('pilihHarga', 1, array(0=>'HJA Resep', 1 =>'HJA Non Resep'), array('readonly'=>false,'class'=>'inputFormTabel span2','style'=>'width:100px;','onchange'=>'ubahHarga(this);hitungTotalSemua();', 'onkeypress'=>"return $(this).focusNextInputField(event)"))) ?>;
    var inputHjaResep = '<?php echo CHtml::hiddenField('hjaResep', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')) ?>';
    var inputHjaNonresep = '<?php echo CHtml::hiddenField('hjaNonresep', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')) ?>';
    var inputHargaSatuan = '<?php echo CHtml::textField('penjualanResep[0][hargasatuan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).CHtml::hiddenField('penjualanResep[0][hargasatuanjual_oa]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>';
    var inputHargaNetto = '<?php echo CHtml::hiddenField('penjualanResep[0][harganetto_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1')) ?>';
    var inputHargaNetto_a = '<?php echo CHtml::hiddenField('penjualanResep[0][harganetto]', '', array('readonly'=>true,'class'=>'inputFormTabel span1')) ?>';
    var inputHargaJual = '<?php echo CHtml::textField('penjualanResep[0][hargajual_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')) ?>';
    var inputKekuatan = '<?php echo CHtml::hiddenField('penjualanResep[0][kekuatan_oa]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>';
    var inputSatuanKekuatan = '<?php echo CHtml::hiddenField('penjualanResep[0][satuankekuatan_oa]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>';
    var inputJmlPermintaan = '<?php echo CHtml::hiddenField('penjualanResep[0][permintaan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1')) ?>';
    var inputJmlKemasan = '<?php echo CHtml::hiddenField('penjualanResep[0][jmlkemasan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1')) ?>';
    var inputSatuan = <?php echo json_encode(CHtml::dropDownList('penjualanResep[0][satuankecil_id]', '', CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"))) ?>;
    var inputSigna = '<?php echo CHtml::textField('penjualanResep[0][signa_reseptur]', '', array('placeholder'=>'-- Aturan Pakai --','class'=>'inputFormTabel span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>';
        inputSigna += '<?php echo CHtml::hiddenField('penjualanResep[0][etiket_caramakan]', '', array()) ?>';
        inputSigna += '<?php echo CHtml::hiddenField('penjualanResep[0][etiket_satuan]', '', array()) ?>';
        inputSigna += '<?php echo CHtml::hiddenField('penjualanResep[0][etiket_keterangan]', '', array()) ?>';
    var inputQty = '<?php echo CHtml::textField('penjualanResep[0][qty]', '', array('onkeyup'=>'validasiQty(this);hitungTotal(this);','onblur'=>'pembulatanKeAtas(this);hitungTotal(this);','class'=>'inputFormTabel span1 numbersOnly', 'onfocus'=>'$(this).select();', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>';
    var inputDiscount = '<?php echo CHtml::textField('penjualanResep[0][disc]', 0, array('onkeyup'=>'hitungTotalSemua();','onblur'=>'hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly','onfocus'=>'$(this).select();')) ?>';
    var inputSumberDana = <?php echo json_encode(CHtml::dropDownList('penjualanResep[0][sumberdana_id]', '', CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"))) ?>;
    var inputEtiket = <?php echo json_encode(CHtml::dropDownList('penjualanResep[0][etiket]', Params::DEFAULT_ETIKET, Etiket::items(),array('class'=>'inputFormTabel span2', 'onkeypress'=>"return $(this).focusNextInputField(event)"))); ?>;
    var inputIsRacikan = '<?php echo CHtml::hiddenField('penjualanResep[0][isRacikan]', '', array()) ?>';
    var inputRacikanId = '<?php echo CHtml::hiddenField('penjualanResep[0][racikan_id]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>';
    var inputBiayaKemasan = '<?php echo CHtml::hiddenField('penjualanResep[0][biayakemasan]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) ?>';
    var inputBiayaAdministrasi = '<?php echo CHtml::hiddenField('penjualanResep[0][biayaadministrasi]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) ?>';
    var inputBiayaService = '<?php echo CHtml::hiddenField('penjualanResep[0][biayaservice]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) ?>';
    var inputJasaDokterResep = '<?php echo CHtml::hiddenField('penjualanResep[0][jasadokterresep]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) ?>';
    var inputStok = '<?php echo CHtml::textField('penjualanResep[0][stok]', '', array('readonly'=>true,'class'=>'inputFormTabel span1')) ?>';
    var iconRemove = '<a onclick="removeObat(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a>';
    
    $('#tblDaftarResep').find('input[name$="[rke]"]').each(function(){
        temp = this.value; if(temp='') temp=0;
        if(temp<this.value) temp = this.value;
        RkeMax = temp;
    });
    
    if($('#formNonRacikan #pilihNonRacik').is(':checked'))
        RkeMax++;
    if($('#formRacikan #pilihRacik').is(':checked')) {
        RkeMax = $('#racikanKe').val();
        if(adaRmax(RkeMax))
            R = '';
    }
    
    var row = '<tr id="tr_'+i+'">'+
                '<td>'+ceklist+detail+'</td>'+
                '<td>'+inputR+R+inputIsRacikan+'</td>'+
                '<td>'+inputRke + inputBiayaKemasan + inputBiayaService + inputJasaDokterResep + inputBiayaAdministrasi +'</td>'+
                '<td>'+inputRacikanId + inputKekuatan + inputSatuanKekuatan + namaObat + inputObat +'</td>'+
                '<td>'+inputSumberDana+'</td>'+
                '<td>'+inputSatuan+'</td>'+
                '<td>'+inputQty + inputJmlPermintaan + inputJmlKemasan +'</td>'+
                '<td>'+inputStok +'</td>'+
                '<td>'+inputPilihHarga+inputHargaSatuan+inputHargaNetto + inputHargaNetto_a+inputHjaResep + inputHjaNonresep+'</td>'+
                '<td>'+inputDiscount+'</td>'+
                '<td>'+inputHargaJual +'</td>'+
                '<td>'+inputSigna+'</td>'+
                '<td>'+inputEtiket+'</td>'+
                '<td>'+iconRemove+'</td>'+
               '</tr>';
    //if($('#idObat').val() == '' || $('#namaObat').val() == ''){alert('Maaf Anda Belum Mengisi Nama Obat.');$('#namaObat').focus();return;}
    if($("#formRacikan #pilihRacik").is(":checked")){
        hitungTarifService();
    }
    $('#tblDaftarResep').append(row);
    $('#tr_'+i).find('select[name$="[satuankecil_id]"]').attr('value', idSatuanKecil);
    $('#tr_'+i).find('input[name$="[qty]"]').attr('value', qty);
    $('#tr_'+i).find('input[name$="[rke]"]').attr('value', RkeMax);
    $('#tr_'+i).find('select[name$="[sumberdana_id]"]').attr('value', idSumberDana);
    $('#tr_'+i).find('input[name$="[obatalkes_id]"]').attr('value', idObat);
    $('#tr_'+i).find('input[name$="[disc]"]').attr('value', discountObat);
    $('#tr_'+i).find('input[name$="hjaResep"]').attr('value', hjaResep);
    $('#tr_'+i).find('input[name$="hjaNonresep"]').attr('value', hjaNonresep);
    $('#tr_'+i).find('input[name$="[hargasatuan_reseptur]"]').attr('value', hargaSatuan);
    $('#tr_'+i).find('input[name$="[etiket_caramakan]"]').attr('value', etiketCaraMakan);    
    $('#tr_'+i).find('input[name$="[etiket_satuan]"]').attr('value', etiketSatuan);    
    $('#tr_'+i).find('input[name$="[etiket_keterangan]"]').attr('value', etiketKeterangan);   
    $('#tr_'+i).find('input[name$="[hargasatuanjual_oa]"]').attr('value', hargaSatuan);
    $('#tr_'+i).find('input[name$="[harganetto_reseptur]"]').attr('value', hargaNetto);
    $('#tr_'+i).find('input[name$="[harganetto]"]').attr('value', hargaNetto_a);
    $('#tr_'+i).find('input[name$="[hargajual_reseptur]"]').attr('value', hargaJual);
    $('#tr_'+i).find('input[name$="[signa_reseptur]"]').attr('value', signa);
    $('#tr_'+i).find('input[name$="[kekuatan_oa]"]').attr('value', kekuatan);
    $('#tr_'+i).find('input[name$="[satuankekuatan_oa]"]').attr('value', satuanKekuatan);
    $('#tr_'+i).find('input[name$="[permintaan_reseptur]"]').attr('value', jmlPermintaan);
    $('#tr_'+i).find('input[name$="[jmlkemasan_reseptur]"]').attr('value', jmlKemasan);
    $('#tr_'+i).find('input[name$="[isRacikan]"]').attr('value', isRacikan);
    $('#tr_'+i).find('input[name$="[r]"]').attr('value', R);
    $('#tr_'+i).find('input[name$="[biayakemasan]"]').attr('value', biayaKemasan);
    $('#tr_'+i).find('input[name$="[biayaservice]"]').attr('value', biayaService);
    $('#tr_'+i).find('input[name$="[jasadokterresep]"]').attr('value', jasaDokterResep);
    $('#tr_'+i).find('input[name$="[stok]"]').attr('value', stokObat);
//    $("#tblDaftarResep > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
//    $('.currency').each(function(){this.value = formatUang(this.value)});

    biayaAdministrasi = 0
    $('#tr_'+i).find('input[name$="[biayaadministrasi]"]').attr('value', biayaAdministrasi);
    clearInputan();
    
    renameInput('penjualanResep', 'detailreseptur_id');
    renameInput('penjualanResep', 'r');
    renameInput('penjualanResep', 'rke');
    renameInput('penjualanResep', 'disc');
    renameInput('penjualanResep', 'obatalkes_id');
    renameInput('penjualanResep', 'hargasatuan_reseptur');
    renameInput('penjualanResep', 'hargasatuanjual_oa');
    renameInput('penjualanResep', 'harganetto_reseptur');
    renameInput('penjualanResep', 'hargajual_reseptur');
    renameInput('penjualanResep', 'kekuatan_oa');
    renameInput('penjualanResep', 'permintaan_reseptur');
    renameInput('penjualanResep', 'jmlkemasan_reseptur');
    renameInput('penjualanResep', 'satuankekuatan_oa');
    renameInput('penjualanResep', 'satuankecil_id');
    renameInput('penjualanResep', 'etiket_satuan');
    renameInput('penjualanResep', 'etiket_caramakan');
    renameInput('penjualanResep', 'etiket_keterangan');
    renameInput('penjualanResep', 'signa_reseptur');
    renameInput('penjualanResep', 'sumberdana_id');
    renameInput('penjualanResep', 'etiket');
    renameInput('penjualanResep', 'isRacikan');
    renameInput('penjualanResep', 'racikan_id');
    renameInput('penjualanResep', 'biayakemasan');
    renameInput('penjualanResep', 'biayaservice');
    renameInput('penjualanResep', 'biayaadministrasi');
    renameInput('penjualanResep', 'jasadokterresep');
    renameInput('penjualanResep', 'stok');
    renameInput('penjualanResep', 'qty');
/*
    $("#tblDaftarResep > tbody > tr:last .currency").maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
    $('.currency').each(function(){this.value = formatUang(this.value)});
    
    $("#tblDaftarResep > tbody > tr:last .numbersOnly").maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":1,"symbol":null});
    $('.numbersOnly').each(function(){this.value = formatNumber(this.value)});
*/

    $('#tblDaftarResep tbody tr:last').find('.numbersOnly').maskMoney({"defaultZero":true,"allowZero":true,"decimal":".","thousands":"","precision":0,"symbol":null,'allowDecimal':true,});
    getDataRekeningObatFarmasi(idObat,"",qty,"ap",jenis_kelompok);
    setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
        updateRekeningObatApotek(idObat, formatDesimal(hargaSatuan*qty),formatDesimal(hargaNetto_a*qty))
    },1500);
    hitungTotalSemua();
}
function removeObat(obj)
{
    var xxx = $(obj).parents("tr").find("td[id$='nama_obat']").text();
    if(confirm('Apakah anda akan menghapus obat ' + xxx + ' ?'))
    {
        $(obj).parent().parent().remove();
        kurangiTarifService(obj);
        var obatalkes_id = $(obj).parents("tr").find("input[name$='[obatalkes_id]']").val();
        removeRekeningObat(obatalkes_id);
    }
    
    hitungTotalSemua();
}
function adaRmax(Rke)
{
    var ada = false;
    $('#tblDaftarResep').find('input[name$="[rke]"]').each(function(){
       if(Rke == this.value)
           ada = true;
    });
    
    return ada;
}

function enableRacikan()
{
    $('#formRacikan input[type="text"]').removeAttr('disabled');
    $('#formRacikan select').removeAttr('disabled');
    $('#formRacikan button').removeAttr('disabled');
    $('#formNonRacikan input[type="text"]').attr('disabled','disabled');
    $('#formNonRacikan select').attr('disabled','disabled');
    $('#formNonRacikan button').attr('disabled','disabled');
    $('#formNonRacikan :radio').attr('checked',false);
    $('#formNonRacikan').find("input[type=text], textarea").val("");
    $('#obatTemp').find("input, textarea").val("");
    $('#racikanKe').focus();
    $('#biayaKemasan').val('<?php echo $racikan->biayakemasan; ?>');
    $('#tarifService').val('<?php echo $racikan->tarifservice; ?>');
    $('#biayaadministrasi').val('<?php echo $konfigFarmasi->admracikan; ?>');
}

function enableNonRacikan()
{
    $('#formNonRacikan input[type="text"]').removeAttr('disabled');
    $('#formNonRacikan select').removeAttr('disabled');
    $('#formNonRacikan button').removeAttr('disabled');
    $('#formRacikan input[type="text"]').attr('disabled','disabled');
    $('#formRacikan select').attr('disabled','disabled');
    $('#formRacikan button').attr('disabled','disabled');
    $('#formRacikan :radio').attr('checked',false);
    $('#formRacikan').find("input[type=text], textarea").val("");
    $('#obatTemp').find("input, textarea").val("");
    $('#biayaKemasan').val('<?php echo $nonRacikan->biayakemasan; ?>');
    $('#tarifService').val('<?php echo $nonRacikan->tarifservice; ?>');
}

function clearRacikan()
{
    $('#formRacikan input[type="text"][id!="jmlKemasanObat"]').val('');
    $('#satuanKekuatanObat').html('');
    $('#racikanKe').focus();
}

function clearNonRacikan()
{
    $('#formNonRacikan input[type="text"]').val('');
        $('#formNonRacikan select').val('');
    $('#satuanKekuatanObat').html('');
    $('#racikanKe').focus();
}

function clearInputan()
{
    $('#idObat').val('');
    $('#hargaSatuan').val('');
    $('#hargaNetto').val('');
    $('#hargaNetto_a').val('');
    $('#hargaJual').val('');
    $('#kekuatan').val('');
    $('#satuanKekuatan').val('');
    $('#jmlPermintaan').val('');
    $('#jmlKemasan').val('');
    $('#qty').val('');
    $('#namaObat').val('');
    $('#idSumberDana').val('');
    $('#kadaluarsa').val('');
    $('#namaSumberDana').val('');
    $('#idSatuanKecil').val('');
    clearRacikan(); clearNonRacikan();
}

function renameInput(modelName,attributeName)
{
    var i = -1;
    $('#tblDaftarResep tr').each(function(){
        if($(this).has('input[name$="[obatalkes_id]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}
function cekInput()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    $('.number').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
function validasiQty(obj){
    var stok = 0;
    stok = unformatNumber($(obj).parent().parent().find('input[name$="[stok]"]').val());
    if((unformatNumber($(obj).val())) > stok){
        alert("Qty tidak boleh lebih dari stok "+stok+"!");
        $(obj).val(1);
    }
}
function ubahHarga(obj){
    var hargaBaru = 0;
    if($(obj).val() == 0){
        hargaBaru = $(obj).parent().find('#hjaResep').val();
        $(obj).parent().find('input[name$="[hargasatuan_reseptur]"]').val(hargaBaru);
    }else if($(obj).val() == 1){
        hargaBaru = $(obj).parent().find('#hjaNonresep').val();
        $(obj).parent().find('input[name$="[hargasatuan_reseptur]"]').val(hargaBaru);
    }
}
</script>


<?php
$this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '.',
        'thousands' => '',
        'precision' =>0,
        'allowDecimal'=>true,
    )
));
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogObat',
    'options'=>array(
        'title'=>'Pencarian Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatDialog = new ObatalkesM('searchObatFarmasi');
$modObatDialog->unsetAttributes();
$format = new CustomFormat();
if (isset($_GET['ObatalkesM']))
    $modObatDialog->attributes = $_GET['ObatalkesM'];
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'obatAlkesDialog-m-grid',
    'dataProvider'=>$modObatDialog->searchObatFarmasi(),
    'filter'=>$modObatDialog,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectObat",
                "onClick"=>"
                            if ($(\"#formNonRacikan #pilihNonRacik\").is(\":checked\")){
                                $(\"#namaObatNonRacik\").val(\"$data->obatalkes_kode - $data->obatalkes_nama\");
                                $(\"#isRacikan\").val(\"0\");
                            }
                            else if ($(\"#formRacikan #pilihRacik\").is(\":checked\")){
                                $(\"#namaObatRacik\").val(\"$data->obatalkes_kode - $data->obatalkes_nama\");
                                $(\"#isRacikan\").val(\"1\");
                            }
                            $(\"#idObat\").val(\"$data->obatalkes_id\");
                            $(\"#jenisObatAlkes_id\").val(\"$data->jenisobatalkes_id\");
                            $(\"#satuanKekuatan\").val(\"$data->satuankekuatan\");
                            $(\"#kekuatan\").val(\"$data->kekuatan\");
                            $(\"#jmlKemasan\").val(\"$data->kemasanbesar\");
                            $(\"#namaObat\").val(\"$data->obatalkes_kode - $data->obatalkes_nama\");
                            $(\"#jmlStok\").val(\"$data->minimalstok\");
                            $(\"#kekuatanObat\").val(\"$data->kekuatan\");
                            $(\"#satuanKekuatanObat\").html(\"$data->satuankekuatan\"); 
                            $(\"#idSumberDana\").val(\"$data->sumberdana_id\");
                            $(\"#discountObat\").val(\"$data->diskonJual\");
                            $(\"#idSatuanKecil\").val(\"$data->satuankecil_id\");
                            $(\"#kategoriObat\").val(\"$data->obatalkes_kategori\");
                            $(\"#kadaluarsa\").val(\"$data->kadaluarsa\");
                            //$(\"#isRacikan\").val(1);
                            $(\"#hjaresep\").val(\"$data->hjaresep\");
                            $(\"#hjanonresep\").val(\"$data->hjanonresep\");
                            $(\"#hpp\").val(\"$data->hpp\");
                            $(\"#jnskelompok\").val(\"$data->jnskelompok\");
                            
                            $(\"#idNonRacikan\").val(\"1\");
                            $(\"#stokObat\").val(\"$data->stokObatRuangan\");
                            $(\"#hargaNetto\").val(\"$data->harganetto\");
                            $(\"#hargaNetto_a\").val(\"$data->harganetto\");
                            $(\"#hargaJual\").val(\"$data->hargajual\");
                            $(\"#idSumberDana\").val(\"$data->sumberdana_id\");
                            $(\"#namaSumberDana\").val(\"$data->namaSumberDana\");

                            $(\"#dialogObat\").dialog(\"close\");
                            return false;
                ",
               ))'
        ),

        'obatalkes_kode',
        'obatalkes_nama',
        array(
            'name'=>'tglkadaluarsa',
            'filter'=>'',
        ),
        
        array(
          'name'=>'satuankecil.satuankecil_nama',
            'header'=>'Satuan Kecil',
        ),
        array(
            'name'=>'satuanbesar.satuanbesar_nama',
            'header'=>'Satuan Besar',
        ),
        array(
            'header'=>'HJA Resep',
            'type'=>'raw',
            'value'=>'number_format($data->hjaresep, 2, ",", ".")',
            'filter'=>'',
            'htmlOptions'=>array('style'=>'text-align:right;'),
        ),
        array(
            'header'=>'HJA Non Resep',
            'value'=>'number_format($data->hjanonresep, 2, ",", ".")',
            'filter'=>'',
            'htmlOptions'=>array('style'=>'text-align:right;'),
        ),
        
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>