<?php // $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<!--<div style="height: 3cm;"></div>-->
<?php $format = new CustomFormat();?>
<style>
    th, td, div{
        font-family: Arial;
        font-size: 7pt;
    }
    .tandatangan{
        vertical-align: bottom;
        text-align: center;
    }
</style>
<!--<table width="84%"><tr><td>-->
<table width="100%">
    <tr>
        <td align="center"><b>Rumah Sakit Jasa Kartini</b></td>
	</tr>
	<tr>
        <td style="border-bottom:1px solid #000000;" align="center">Kota Tasikmalaya</td>
	</tr>
</table>
<br>
<table width="100%">
    <tr>
        <td width="80">No. Resep</td>
        <td width="5">:</td>
        <td><?php echo $modPenjualan->noresep;?>
    </tr>
	<tr>
        <td>Nama</td>
		<td>:</td>
        <td><?php echo $pasien->no_rekam_medik; ?> - <?php echo $pasien->namadepan." ".$pasien->nama_pasien;?> </td>
    </tr>
	<tr>
        <td>Alamat</td>
		<td>:</td>
        <td>
			<?php echo $pasien->alamat_pasien;?>
			<?php echo (!empty($pasien->no_telepon_pasien)) ? " / No. Telp: ".$pasien->no_telepon_pasien." /".$pasien->no_mobile_pasien : ""; ?>		
		</td>
    </tr>
	<tr>
        <td>Tgl. Resep</td>
		<td>:</td>
        <td><?php echo $modPenjualan->tglresep;?></td>
    </tr>
	<tr>
        <td>Tgl. Penjualan</td>
		<td>:</td>
        <td><?php echo $modPenjualan->tglpenjualan;?></td>
    </tr>	
</table>
<!--
<table width="100%">
    <tr>
        <td width="20%">No. Resep</td>
        <td width="30%">: <?php echo $modPenjualan->noresep;?>
        <td width="20%">Nama</td>
        <td width="30%">: <?php echo $pasien->no_rekam_medik; ?> - <?php echo $pasien->namadepan." ".$pasien->nama_pasien;?> </td>
    </tr>
    <tr>
        <td>Tgl. Resep</td>
        <td>: <?php echo $modPenjualan->tglresep;?></td>
        <td>Alamat</td>
        <td rowspan="2">:
		<?php echo $pasien->alamat_pasien;?>
        <?php echo (!empty($pasien->no_telepon_pasien)) ? "<br>No. Telp: ".$pasien->no_telepon_pasien." /".$pasien->no_mobile_pasien : ""; ?>
        </td>
    </tr>
    <tr>
        <td>Tgl. Penjualan </td>
        <td>: <?php echo $modPenjualan->tglpenjualan;?></td>
    </tr>
</table>
-->
<table width="100%">
    <thead style='border:1px solid;'>
		<th style='text-align: center; width: 20px;'>No.</th>
		<th width='20px' style='text-align: left;'>Kode</th>
		<th style='text-align: left;'>Nama</th>
		<th width='50px' style='text-align: right;'>Harga</th>
		<th width='30px' style='text-align: center;'>Qty</th>
		<th width='80px' style='text-align: right;'>Subtotal</th>
    </thead>
    <tbody>
    <?php
    $no=1;
    $subTotal = 0;
    $totalSubTotal = 0;
    $biayaAdmin = 0;
    if (count($obatAlkes) > 0){
        foreach($obatAlkes AS $tampilData):
        $subTotal = ($tampilData->hargasatuan_oa * $tampilData->qty_oa); 
        $totalSubTotal += $subTotal;
        echo "<tr>
            <td valign='top' style='text-align:center;'>".$no."</td>
            <td valign='top'>".$tampilData->obatalkes->obatalkes_kode."</td>
            <td valign='top'>".$tampilData->obatalkes->obatalkes_nama."</td>
            <td valign='top' style='text-align: right;'>".$format->formatNumber($tampilData->hargasatuan_oa)."</td>
            <td valign='top' style='text-align: center;'>".$format->formatNumber($tampilData->qty_oa)."</td>
            <td valign='top' style='text-align: right;'>".$format->formatNumber($subTotal)."</td>
         </tr>";  
        $no++;
        endforeach;
    }
    $biayaAdmin = $modPenjualan->totaltarifservice + $modPenjualan->biayaadministrasi + $modPenjualan->biayakonseling;//INI TIDAK DI TAGIHKAN KE PASIEN KARENA SUDAH MASUK KE HARGA OBAT >>> +$reseptur->jasadokterresep
    ?>
    </tbody>
    <tfoot>
        <tr style='border-top:1px solid;'>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Total</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($modPenjualan->totalhargajual);?></td>
        </tr>
        <tr>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Biaya Administrasi, dll.</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($biayaAdmin);?></td>
        </tr>
        <tr>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Total Tagihan</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($modPenjualan->totalhargajual + $biayaAdmin);?></td>
        </tr>
    </tfoot>
</table>

<table style="width:100%;">
    <tr><td class="tandatangan">Penerima</td>
        <td class="tandatangan">Hormat Kami,</td>
    </tr>
    <tr>
        <td class="tandatangan" style="height: 50px;">.........................</td>
        <td class="tandatangan" ><?php echo Yii::app()->user->getState('nama_pegawai'); ?>
    </td></tr>
</table>
            <div style="font-size: 9pt;">Print Date: <?php echo $modPenjualan->ruangan->ruangan_nama;?>,<?php // echo Yii::app()->user->getState('nama_pegawai'); ?>
    <?php echo date('d M Y H:i:s'); ?></div>
<!--</td></tr></table>-->
<?php 
if(!isset($_GET['caraPrint'])){
    echo CHtml::link(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),Yii::app()->createUrl($this->route)."&id=".$_GET['id']."&idPasien=".$_GET['idPasien']."&caraPrint=PRINT",array('class'=>'btn btn-primary', 'type'=>'button'))."&nbsp&nbsp";
}
?>