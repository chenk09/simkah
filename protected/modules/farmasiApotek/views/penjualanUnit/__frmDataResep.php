<fieldset id="frm_data_resep">
    <legend class="rim">Data Resep</legend>
    <table >
        <tr>
            <?php
                echo $form->hiddenField($modPenjualan,'penjualanresep_id',array('readonly'=>true));
                echo $form->hiddenField($modPenjualan,'pegawai_id',array('readonly'=>true, 'class'=>'req'));
            ?>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'tglresep', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenjualan,
                                        'attribute'=>'tglresep',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                            'yearRange'=> "-60:+0",
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPenjualan,'noresep',array('readonly'=>TRUE)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'tglpenjualan', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenjualan,
                                        'attribute'=>'tglpenjualan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                            'yearRange'=> "-60:+0",
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPenjualan,'jenispenjualan',array('readonly'=>true)); ?>
            </td>
            <td style="padding-right:160px;">
                <?php
                    echo $form->dropDownListRow($modPenjualan,'pasienprofilrs_id',
                        CHtml::listData($modPenjualan->getProfilRs(), 'profilrs_id', 'nama_rumahsakit'),
                        array(
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)",
                            'ajax' => array(
                                'type'=>'POST',
                                'url'=> Yii::app()->createUrl('ActionDynamic/GetInstansiByProfilRs',array('encode'=>false,'namaModel'=>'FAPenjualanResepT')), 
                                'update'=>'#FAPenjualanResepT_pasieninstalasiunit_id'
                            ),
                        )
                    );
                    echo $form->dropDownListRow($modPenjualan,'pasieninstalasiunit_id',
                        CHtml::listData(InstalasiM::model()->findAll(), 'instalasi_id', 'instalasi_nama'),
                        array('empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",)
                    );
                ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'lamapelayanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPenjualan,'lamapelayanan',array('class'=>'inputFormTabel span2 numbersOnly','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Menit
                    </div> 
                </div>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'discount', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPenjualan,'discount',array('class'=>'span2 currency','readonly'=>true,'onkeyup'=>'hitungDiskonSemua();', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Rupiah
                    </div> 
                </div>
            </td>
        </tr>
    </table>
</fieldset>