<tr>
    <td>
        <?php
            $checkbox = false; //menampilkan checkbox = true atau hide = false
            if($checkbox)
                echo CHtml::checkBox("penjualanResep[99][detailreseptur_id]", true, array('onchange'=>'hitungTotalSemua();','uncheckValue'=>'0','value'=>1));
            else
                echo CHtml::checkBox("penjualanResep[99][detailreseptur_id]", true, array('onchange'=>'hitungTotalSemua();','uncheckValue'=>'0','value'=>1, 'style'=>'display:none;'));
            echo CHtml::hiddenField('penjualanResep[99][r]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));    
            echo CHtml::hiddenField('penjualanResep[99][obatalkes_id]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1 obat'));
            echo CHtml::hiddenField('penjualanResep[99][harganetto_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1'));
            echo CHtml::hiddenField('penjualanResep[99][harganetto]', '', array('readonly'=>true,'class'=>'inputFormTabel span1'));
            echo CHtml::hiddenField('penjualanResep[99][isRacikan]', '', array());
            echo CHtml::hiddenField('penjualanResep[99][racikan_id]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
            echo CHtml::hiddenField('penjualanResep[99][biayakemasan]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));        
            echo CHtml::hiddenField('penjualanResep[99][biayaservice]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
            echo CHtml::hiddenField('penjualanResep[99][biayaadministrasi]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
            echo CHtml::hiddenField('penjualanResep[99][jasadokterresep]', '', array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
            echo CHtml::hiddenField('penjualanResep[99][kekuatan_oa]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
            echo CHtml::hiddenField('penjualanResep[99][satuankekuatan_oa]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
            echo CHtml::hiddenField('penjualanResep[99][permintaan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1'));
            echo CHtml::hiddenField('penjualanResep[99][jmlkemasan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel span1'));
            echo CHtml::hiddenField('penjualanResep[99][hargasatuan_reseptur_a]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargasatuanobat'));
        ?>
    </td>
    <td name="penjualanResep[99][is_recipe]">&nbsp;</td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][rke]', '', array('readonly'=>true,'style'=>'width:15px;'));
        ?>
    </td>
    <td name="penjualanResep[99][nama_obat_alkes]"></td>
    <td>
        <?php
            echo CHtml::dropDownList('penjualanResep[99][sumberdana_id]', '', CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::dropDownList('penjualanResep[99][satuankecil_id]', '', CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][qty]', '', array('onkeyup'=>'validasiQty(this);hitungTotalSemua();','onblur'=>'pembulatanKeAtas(this);hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly qty', 'onfocus'=>'$(this).select();', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?><?php echo CHtml::hiddenField('penjualanResep[99][qty_a]', '', array('onkeyup'=>'hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly qty2', 'onfocus'=>'$(this).select();'));        
        ?>
    </td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][stok]', '', array('readonly'=>true,'class'=>'inputFormTabel span1'));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::hiddenField('penjualanResep[99][harganetto_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency'));
            echo CHtml::hiddenField('penjualanResep[99][harganetto]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency'));
            echo CHtml::textField('penjualanResep[99][hargasatuan_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargareseptur'));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][disc]', 0, array('onkeyup'=>'hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly','onfocus'=>'$(this).select();'));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][hargajual_reseptur]', '', array('readonly'=>true,'class'=>'inputFormTabel currency lebar2'));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::textField('penjualanResep[99][signa_reseptur]', '', array('placeholder'=>'-- Aturan Pakai --','class'=>'inputFormTabel span2','onkeypress'=>"return $(this).focusNextInputField(event)"));
        ?>
    </td>
    <td>
        <?php
            echo CHtml::dropDownList('penjualanResep[99][etiket]', Params::DEFAULT_ETIKET, Etiket::items(),array('class'=>'inputFormTabel span2','onkeypress'=>"return $(this).focusNextInputField(event)"));
        ?>        
    </td>
    <td>
        <a onclick="removeObat(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a>
    </td>
</tr>