<?php 
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js');
$form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
    array(
        'id'=>'caripasien-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#FAPenjualanResepT_pasienprofilrs_id',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
    )
);

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.numbersOnly',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
        'allowDecimal'=>true,
    )
));

?>

<fieldset>
    <legend class="rim2">Transaksi Penjualan Unit</legend>
    <?php
        $this->renderPartial('__frmDataResep',array('form'=>$form, 'modPenjualan'=>$modPenjualan));
    ?>
    <?php
        $this->renderPartial('__gridInputObat',
            array(
                'form'=>$form,
                'konfigFarmasi'=>$konfigFarmasi,
                'nonRacikan'=>$nonRacikan,
                'modPenjualan'=>$modPenjualan,
                'obatAlkes'=>$obatAlkes,
                'modRekenings'=>$modRekenings,
            )
        );
    ?>
</fieldset>

<?php $this->endWidget(); ?>

<script type="text/javascript">
function hitungTotalSemua()
{
    var qty = 0;
    var hargaNetto = 0;
    var hargaSatuan = 0;
    var disc = 0;
    var subTotal = 0;
    var totHargaJual = 0;
    var totHargaNetto = 0;
    var totJasaDokter = 0;
    var totadministrasi = 0;
    var totDiskon = 0;
    var jasaDokter = 0;
    var administrasi = parseFloat(<?php echo ($konfigFarmasi->administrasi) ?>);
    $('#tblDaftarResep').find('input[name$="[qty]"]').each(function(){
        qty = parseFloat($(this).parents('tr').find('input[name$="[qty]"]').val());
        hargaNetto = parseFloat($(this).parents('tr').find('input[name$="[harganetto_reseptur]"]').val());
        hargaSatuan = parseFloat($(this).parents('tr').find('input[name$="[hargasatuan_reseptur]"]').val());
        disc = parseFloat($(this).parents('tr').find('input[name$="[disc]"]').val());
        if ($(this).parents("tr").find('input[name$="[detailreseptur_id]"]').is(':checked')){
            totDiskon += ((hargaSatuan * disc / 100)* qty);
            subTotal = (hargaSatuan - (hargaSatuan * disc / 100)) * qty;
            jasaDokter = (qty * hargaSatuan * parseFloat(<?php echo ($konfigFarmasi->formulajasadokter == 0) ? $konfigFarmasi->formulajasadokter : MyFunction::calculate_string($konfigFarmasi->formulajasadokter) ?>));        
            if (jQuery.isNumeric(subTotal)){
                $(this).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(Math.ceil(subTotal));
            }
            totHargaNetto += hargaNetto;
            totHargaJual += subTotal;
            totadministrasi += administrasi;
            totJasaDokter += jasaDokter;
        }
        var obatalkes_id = $(this).parents("tr").find("input[name$='[obatalkes_id]']").val();
        var qty = $(this).parents("tr").find("input[name$='[qty]']").val();
        var saldo = $(this).parents("tr").find("input[name$='[hargajual_reseptur]']").val();
        var saldo2 = $(this).parents("tr").find("input[name$='[harganetto]']").val();
        saldoTindakan = (saldo2*qty);
        updateRekeningObatApotek(obatalkes_id, formatDesimal(saldo), formatDesimal(saldoTindakan));
    });
    
    if (jQuery.isNumeric(totHargaNetto)){
        $('#FAPenjualanResepT_totharganetto').val(Math.ceil(totHargaNetto));
    }
    if (jQuery.isNumeric(totHargaJual)){
        $('#FAPenjualanResepT_totalhargajual').val(Math.ceil(totHargaJual));
        $('.totalhargajual').val(Math.ceil(totHargaJual));
    }
    if (jQuery.isNumeric(totJasaDokter)){
        $('#FAPenjualanResepT_jasadokterresep').val(Math.ceil(totJasaDokter));
    }
    if (jQuery.isNumeric(totadministrasi)){
        $('#FAPenjualanResepT_biayaadministrasi').val(Math.ceil(totadministrasi));
    }
    if (jQuery.isNumeric(totDiskon)){
        $('#FAPenjualanResepT_discount').val(Math.ceil(totDiskon));
    }
}

function hitungDiskonSemua()
{
    var diskon = parseFloat($('#FAPenjualanResepT_discount').val());
    var totalharga = parseFloat($('.totalhargajual').val());
    
    diskon = parseFloat(totalharga - (totalharga * (diskon/100)));
    
    $('#FAPenjualanResepT_totalhargajual').val(Math.ceil(diskon));
}

$('#resetbtn').click(function(){
    window.location = '<?php echo Yii::app()->createAbsoluteUrl(Yii::app()->controller->module->id.'/'.Yii::app()->controller->id.'/index') ?>';
});
function pembulatanKeAtas(obj){
    $(obj).val(Math.ceil(obj.value));
}
function loadTarifService(jmlkemasan){
    var jml = <?php echo count($racikanDetail);?>;
    <?php
        $js_racikan = json_encode($racikanDetail);
        echo "var racikanDet = ".$js_racikan.";\n"; //convert php array to js array
    ?>
    var jmlkemasan = parseFloat(jmlkemasan);
    var tarifService = 0;
    for(var i = 0;i<jml;i++){
        var qtymin = parseFloat(racikanDet[i]['qtymin']);
        var qtymaks = parseFloat(racikanDet[i]['qtymaks']) + 1;
        if((jmlkemasan >= qtymin) && (jmlkemasan < qtymaks)){
            tarifService = racikanDet[i]['tarifservice'];
        }
    }
    return parseFloat(tarifService);
}

<?php $urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintFakturPenjualanUnit'); ?>
 <?php $idPenjualan = $modPenjualan->penjualanresep_id;?>

function print(caraPrint)
{
    var idPenjualan = $('#idPenjualan').val();
    window.open("<?php echo $urlPrint;?>&id="+idPenjualan+"&caraPrint="+caraPrint+"","",'location=_new, width=980px');
}

function hitungTarifService(){
    var racikanke = $('#formRacikan').find('#racikanKe').val();
    var jmlkemasan = $('#formRacikan').find('#jmlKemasanObat').val();
    var totalTarifService = parseFloat($("#FAPenjualanResepT_totaltarifservice").val());
    //cari racikan yang berbeda di tabel
    var rkebeda = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value!="'+racikanke+'"]');
    var rkesama = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value="'+racikanke+'"]');
    var jmlrkebeda = rkebeda.length;
    var jmlrkesama = rkesama.length;
    if(jmlrkesama === 0 && jmlrkebeda === 0){ //jika tidak ada yang sama dan tidak ada yang beda
        totalTarifService = loadTarifService(jmlkemasan);
    }else if(jmlrkesama === 0 && jmlrkebeda > 0){ //jika tidak ada yang sama tapi ada yang beda
        totalTarifService += loadTarifService(jmlkemasan);
    }
    $("#FAPenjualanResepT_totaltarifservice").val(Math.ceil(totalTarifService));
}
function kurangiTarifService(obj){
    var totalTarifService = $("#FAPenjualanResepT_totaltarifservice").val();
    var rkecari = $(obj).parents('tr').find('input[name$="[rke]"]').val();
    var jmlkemasan = $(obj).parents('tr').find('input[name$="[jmlkemasan_reseptur]"]').val();
    var rkesama = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value='+rkecari+']');
    var jmlrkesama = rkesama.length;
    if(jmlrkesama === 0){
        totalTarifService -= loadTarifService(jmlkemasan);
        $("#FAPenjualanResepT_totaltarifservice").val(Math.ceil(totalTarifService));
    }
}
//dieksekusi saat load halaman
hitungTotalSemua();
</script>