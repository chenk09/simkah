<?php 
$table = 'ext.bootstrap.widgets.BootGroupGridView';
$data = $model->searchLaporan();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchLaporanPrint();  
  $template = "{items}";
  if ($caraPrint == "EXCEL") {
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'laporan-grid',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
//                'mergeColumns'=>array('noresep','tglresep','totalhargajual','jumalhresep'),
	'columns'=>array(
                    array(
                        'header'=>'No',
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
			'footer'=>'&nbsp;'
		    ),
                    array(
                        'header'=>'No Resep',
                        'name'=>'noresep',
                        'value'=>'$data->noresep',
                        'footer'=>'<b>Total</b>'
                    ),
                    array(
                        'header'=>'Tgl Resep',
                        'name'=>'tglresep',
                        'value'=>'$data->tglresep',
			 'footer'=>'&nbsp;'
                    ),
                    array(
                        'header'=>'Total Harga',
                        'name'=>'totalhargajual',
                        'value'=>'"Rp. ".number_format($data->totalhargajual,0,"",",")',
                        'footer'=>"Rp. ".''.number_format($model->getTotalhargajual(),0,"",",").'',
                    ),
                    array(
                        'header'=>'Total Item (R)',
                        'value'=>'FALaporanLembarresepLuarV::getItemR($data->noresep)',
                        'footer'=>number_format($model->getTotalR(),0,"",","),
                    ),
                    array(
                        'header'=>'Qty Item',
                        'value'=>'FALaporanLembarresepLuarV::getQty($data->noresep)',
                        'footer'=>$model->getTotalQty(),
                    ),
                    array(
                        'header'=>'Jumlah Resep',
                        'name'=>'jumalhresep',
                        'type'=>'raw',
                        'value'=>'1',
                        'footer'=>$model->getTotalJumlahresep(),
                    )
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>
