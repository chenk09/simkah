<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/LapDetailPendapatanProdusen/Print.php
* Updated by    : Hardi
* Date          : 24-04-2014
* Issue         : EHJ-1082
* Deskripsi     : Print
**/
?>
<style>
    #laporanprint-grid th, #laporanprint-grid td{text-align:center;vertical-align:center;}
    #headercolumn {border-bottom:1px solid #DDDDDD;}
    #childcolumn {border-left:1px solid #DDDDDD;}
</style>
<?php 
if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>$judulLaporan, 'periode'=>$periode, 'colspan'=>10));  

$produsen = !empty($model->produsen_id) ? "Produsen ".$model->produsen : "Seluruh Produsen";
$jenisobatalkes = isset($model->jenisobatalkes_nama) ? $model->jenisobatalkes_nama : "Seluruh Kelompok Obat";

$judul1 = "Laporan Detail Pendapatan ".$produsen;
$judul2 = "Kelompok Obat: ".$jenisobatalkes;

echo "<h4>".$judul1."</h4>";
echo "<h5>".$judul2."</h5>";

?>
<?php $this->renderPartial('kinerjapenjualanOA/LapDetailPendapatanProdusen/_table',array('modKinerja'=> $modKinerja)); ?>