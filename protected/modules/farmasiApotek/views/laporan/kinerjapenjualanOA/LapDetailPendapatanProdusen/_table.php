<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/_table.php
* Updated by    : Hardi
* Date          : 24-04-2014
* Issue         : EHJ-1082
* Deskripsi     : Table
**/
?>
<div id="lapbarangdokter-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <!-- <th id="lapbarangdokter-m-grid_c0" width="10px" rowspan="2"> No </th> -->
		        <th id="lapbarangdokter-m-grid_c1" rowspan="2"> Kode </th>
		        <th id="lapbarangdokter-m-grid_c2" rowspan="2"> Nama </th>
		        <th id="lapbarangdokter-m-grid_c3" colspan="5"> Penjualan </th>
		        <th id="lapbarangdokter-m-grid_c4" colspan="5"> Retur Penjualan </th>
		        <th id="lapbarangdokter-m-grid_c5" colspan="5"> Jumlah </th>
		    </tr>
		    <tr>
		        <th id="lapbarangdokter-m-grid_c6"> QTY </th>
		        <th id="lapbarangdokter-m-grid_c7"> Bruto </th>
		        <th id="lapbarangdokter-m-grid_c8"> Diskon </th>
				<th id="lapbarangdokter-m-grid_c9"> Ppn </th>
		        <th id="lapbarangdokter-m-grid_c10"> Netto </th>

		        <th id="lapbarangdokter-m-grid_c11"> QTY </th>
		        <th id="lapbarangdokter-m-grid_c12"> Bruto </th>
		        <th id="lapbarangdokter-m-grid_c13"> Diskon </th>
				<th id="lapbarangdokter-m-grid_c14"> Ppn </th>
		        <th id="lapbarangdokter-m-grid_c15"> Netto </th>

		        <th id="lapbarangdokter-m-grid_c16"> QTY </th>
		        <th id="lapbarangdokter-m-grid_c17"> Bruto </th>
		        <th id="lapbarangdokter-m-grid_c18"> Diskon </th>
				<th id="lapbarangdokter-m-grid_c19"> Ppn </th>
		        <th id="lapbarangdokter-m-grid_c20"> Netto </th>
		    </tr>
		</thead>
		<tbody>
		<?php
			$penjualanResep[-1] = '';
			$obatalkes[-1] 		= '';
			$returresep[-1]		= '';
			$pindahbaris = true;
			$jumlah = count($modKinerja);
			$i = 1;
			$penjualan_bruto = 0;
			$penjualan_diskon = 0;
			$penjualan_ppn = 0;
			$penjualan_netto = 0;

			$retur_bruto = 0;
			$retur_diskon = 0;
			$retur_ppn = 0;
			$retur_netto = 0;
			foreach ($modKinerja as $key => $mod) {
				$penjualanResep[$key] 	= $mod['penjualanresep_id'];
				$obatalkes[$key] 		= $mod['obatalkes_id'];
				$returresep[$key]		= $mod['returresep_id'];
				$qty[$key] 		= $mod['qty_oa']; 
				$bruto[$key] 	= $mod['hargajual_oa']; 
				$diskon[$key] 	= $mod['discount']; 
				$ppn[$key] 		= $mod['ppn_persen']; 
				$netto[$key] 	= $mod['hargajual_oa']; 
				if(empty($returresep[$key]) && ($obatalkes[$key-1]!=$obatalkes[$key] || $penjualanResep[$key-1]!=$penjualanResep[$key]))
				{
					$penjualan_bruto += $bruto[$key];
					$penjualan_diskon += $diskon[$key];
					$penjualan_ppn += $ppn[$key];
					$penjualan_netto += $netto[$key];

					if($key>0){
						if($pindahbaris){
							echo "<td>0</td>";
							echo "<td>0</td>";
							echo "<td>0</td>";
							echo "<td>0</td>";
							echo "<td>0</td>";
							echo "<td>".$qty[$key-1]."</td>";
							echo "<td>".number_format($bruto[$key-1],2)."</td>";
							echo "<td>".number_format($diskon[$key-1],2)."</td>";
							echo "<td>".number_format($ppn[$key-1],2)."</td>";
							echo "<td>".number_format($netto[$key-1],2)."</td>";
							echo "</tr>";
						}
						$pindahbaris = true;
					}
					echo "<tr>";
					// echo "<td></td>";
					echo "<td>".$mod['obatalkes_kode']."</td>";
					echo "<td>".$mod['obatalkes_nama']."</td>";
					echo "<td>".$qty[$key]."</td>";
					echo "<td>".number_format($bruto[$key],2)."</td>";
					echo "<td>".number_format($diskon[$key],2)."</td>";
					echo "<td>".number_format($ppn[$key],2)."</td>";
					echo "<td>".number_format($netto[$key],2)."</td>";
				}else{
					echo "<td>".$qty[$key]."</td>";
					echo "<td>".number_format($bruto[$key],2)."</td>";
					echo "<td>".number_format($diskon[$key],2)."</td>";
					echo "<td>".number_format($ppn[$key],2)."</td>";
					echo "<td>".number_format($netto[$key],2)."</td>";

					$jumlah_qty = $qty[$key-1] - $qty[$key];
					$jumlah_bruto 	= $bruto[$key-1] - $bruto[$key];
					$jumlah_diskon 	= $diskon[$key-1] - $diskon[$key];
					$jumlah_ppn 	= $ppn[$key-1] - $ppn[$key];
					$jumlah_netto 	= $netto[$key-1] - $netto[$key];
					echo "<td>".$jumlah_qty."</td>";
					echo "<td>".number_format($jumlah_bruto,2)."</td>";
					echo "<td>".number_format($jumlah_diskon,2)."</td>";
					echo "<td>".number_format($jumlah_ppn,2)."</td>";
					echo "<td>".number_format($jumlah_netto,2)."</td>";
					echo "</tr>";

					$pindahbaris = false;

					$retur_bruto += $bruto[$key];
					$retur_diskon += $diskon[$key];
					$retur_ppn += $ppn[$key];
					$retur_netto += $netto[$key];
				}

				$i++;
			}

			if($pindahbaris && $i>1){
				echo "<td>0</td>";
				echo "<td>0</td>";
				echo "<td>0</td>";
				echo "<td>0</td>";
				echo "<td>0</td>";
				echo "<td>".$qty[$key]."</td>";
				echo "<td>".number_format($bruto[$key],2)."</td>";
				echo "<td>".number_format($diskon[$key],2)."</td>";
				echo "<td>".number_format($ppn[$key],2)."</td>";
				echo "<td>".number_format($netto[$key],2)."</td>";
				echo "</tr>";
			}

			echo "<tr>";
			echo "<td colspan=2>Jumlah</td>";
			echo "<td></td>";
			echo "<td>".number_format($penjualan_bruto,2)."</td>";
			echo "<td>".number_format($penjualan_diskon,2)."</td>";
			echo "<td>".number_format($penjualan_ppn,2)."</td>";
			echo "<td>".number_format($penjualan_netto,2)."</td>";
			echo "<td></td>";
			echo "<td>".number_format($retur_bruto,2)."</td>";
			echo "<td>".number_format($retur_diskon,2)."</td>";
			echo "<td>".number_format($retur_ppn,2)."</td>";
			echo "<td>".number_format($retur_netto,2)."</td>";
			echo "<td></td>";
			echo "<td>".number_format($penjualan_bruto-$retur_bruto,2)."</td>";
			echo "<td>".number_format($penjualan_diskon-$retur_diskon,2)."</td>";
			echo "<td>".number_format($penjualan_ppn-$retur_ppn,2)."</td>";
			echo "<td>".number_format($penjualan_netto-$retur_netto,2)."</td>";
			echo "</tr>";
		?>
		</tbody>
	</table>
</div>