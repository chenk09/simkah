<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/index.php
* Updated by    : Hardi
* Date          : 22-04-2014
* Issue         : EHJ-1083
* Deskripsi     : view Index yang berisi tab laporan
**/
?>

<?php
    $this->breadcrumbs=array(
        'falaporan Ms'=>array('index'),
        'Manage',
    );
?>
<legend class="rim2">Laporan Kinerja Penjualan Obat Alkes</legend>
<br><br>
<?php $this->renderPartial('kinerjapenjualanOA/_tabMenu',array()); ?>
<?php $this->renderPartial('kinerjapenjualanOA/_jsFunctions',array()); ?>
<div>
    <iframe id="frame" src="" width='100%' frameborder="0" style="overflow-y:scroll" ></iframe>
</div>