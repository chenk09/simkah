<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/_tabMenu.php
* Updated by    : Hardi
* Date          : 22-04-2014
* Issue         : EHJ-1083
* Deskripsi     : view Index yang berisi tab laporan
**/
?>
<?php
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', 
    'stacked'=>false, 
    'items'=>array(
    	
        array('label'=>'Seluruh Barang Per Produsen', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'tab-default','onclick'=>'setTab(this);', 'tab'=>'farmasiApotek/laporan/seluruhbarangprodusen')),
        array('label'=>'Seluruh Barang Per Dokter', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'farmasiApotek/laporan/seluruhbarangdokter')),
        array('label'=>'Detail Pendapatan Dokter', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'farmasiApotek/laporan/DetailPendapatanDokter')),
        array('label'=>'Detail Pendapatan Produsen', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'farmasiApotek/laporan/DetailPendapatanProdusen')),
 
    ),
));
?>