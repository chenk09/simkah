<?php
/**
* modules/farmasiApotek/views/laporan/LapSeluruhBarangProdusen/_table.php
* Updated by    : Hardi
* Date          : 23-04-2014
* Issue         : EHJ-1081
* Deskripsi     : Table
**/
?>
<legend class="rim">Laporan Seluruh Barang Per Produsen</legend>
<div id="lapbarangdokter-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <th id="lapbarangdokter-m-grid_c0" width="10px"> No </th>
		        <th id="lapbarangdokter-m-grid_c1"> Kode </th>
		        <th id="lapbarangdokter-m-grid_c2"> Nama </th>
		        <th id="lapbarangdokter-m-grid_c3"> Satuan </th>
		        <th id="lapbarangdokter-m-grid_c4"> QTY </th>
		        <th id="lapbarangdokter-m-grid_c5"> Bruto </th>
		        <th id="lapbarangdokter-m-grid_c6"> Diskon </th>
		        <th id="lapbarangdokter-m-grid_c7"> Ppn </th>
		        <th id="lapbarangdokter-m-grid_c8"> Netto </th>
		        <th id="lapbarangdokter-m-grid_c9"> Status </th>
		    </tr>
		</thead>
		<tbody>
			<?php
				$format = new CustomFormat();
				$i=1;
				$namaProdusen[-1] 	= '';
				$tglpenjualan[-1] 	= '';
				$kelompok[-1] 		= '';
				$jml_row = count($modKinerja);
				$baris_tanggal 	= false;
				$baris_kelompok = false;
				$spasi = "&nbsp;&nbsp;&nbsp;";
				$totBruto = 0;
				$totDiskon = 0;
				$totPpn = 0;
				$totNetto = 0;

				$totBrutoTanggal = 0;
				$totDiskonTanggal = 0;
				$totPpnTanggal = 0;
				$totNettoTanggal = 0;

				$totBrutoProdusen = 0;
				$totDiskonProdusen = 0;
				$totPpnProdusen = 0;
				$totNettoProdusen = 0;

				foreach ($modKinerja as $key => $mod) {
					$namaProdusen[$key] = $mod['produsen'];
					$tglpenjualan[$key] = substr($mod['tglpenjualan'], 0,10);
					$baris_tanggal 		= false;
					$kelompok[$key] 	= $mod['jenisobatalkes_nama'];
					$baris_kelompok 	= false;
					//========= baris untuk produsen ===============
					if($namaProdusen[$key] != $namaProdusen[$key-1])
					{
						if($key>0){
							echo "<tr>";
							echo "<td colspan=5><i>".$spasi.$spasi.$spasi.$spasi."Total Kelompok ".$kelompok[$key-1]."</i></td>";
							echo "<td>".number_format($totBruto,2)."</td>";
							echo "<td>".number_format($totDiskon,2)."</td>";
							echo "<td>".number_format($totPpn,2)."</td>";
							echo "<td>".number_format($totNetto,2)."</td>";
							echo "</tr>";
							$totBruto = 0;
							$totDiskon = 0;
							$totPpn = 0;
							$totNetto = 0;

							echo "<tr>";
							echo "<td colspan=5><i>".$spasi.$spasi."Total Tanggal ".$format->formatDateINA($tglpenjualan[$key-1])."</i></td>";
							echo "<td>".number_format($totBrutoTanggal,2)."</td>";
							echo "<td>".number_format($totDiskonTanggal,2)."</td>";
							echo "<td>".number_format($totPpnTanggal,2)."</td>";
							echo "<td>".number_format($totNettoTanggal,2)."</td>";
							echo "</tr>";
							$totBrutoTanggal = 0;
							$totDiskonTanggal = 0;
							$totPpnTanggal = 0;
							$totNettoTanggal = 0;

							echo "<tr>";
							echo "<td colspan=5><i>".$spasi.$spasi."Total Produsen ".$namaProdusen[$key-1]."</i></td>";
							echo "<td>".number_format($totBrutoProdusen,2)."</td>";
							echo "<td>".number_format($totDiskonProdusen,2)."</td>";
							echo "<td>".number_format($totPpnProdusen,2)."</td>";
							echo "<td>".number_format($totNettoProdusen,2)."</td>";
							echo "</tr>";
							$totBrutoProdusen = 0;
							$totDiskonProdusen = 0;
							$totPpnProdusen = 0;
							$totNettoProdusen = 0;
						}					

						echo "<tr>";
						echo "<td colspan=10><b>Produsen : ".$mod['produsen']."</b></td>";
						echo "</tr>";	
						if($key>0){
							echo "<tr>";
							echo "<td colspan=10>".$spasi."Tanggal : <i>".$format->formatDateINA($tglpenjualan[$key])."</i></td>";
							echo "</tr>";
							$baris_tanggal = true;

							echo "<tr>";
							echo "<td colspan=10>".$spasi.$spasi."Kelompok : <i>".$kelompok[$key]."</i></td>";
							echo "</tr>";
							$baris_kelompok = true;
						}
					}
					//========= baris untuk tanggal ===============
					if($tglpenjualan[$key] != $tglpenjualan[$key-1] && $baris_tanggal == false)
					{
						if($key>0){
							echo "<tr>";
							echo "<td colspan=5><i>".$spasi.$spasi.$spasi.$spasi."Total Kelompok ".$kelompok[$key-1]."</i></td>";
							echo "<td>".number_format($totBruto,2)."</td>";
							echo "<td>".number_format($totDiskon,2)."</td>";
							echo "<td>".number_format($totPpn,2)."</td>";
							echo "<td>".number_format($totNetto,2)."</td>";
							echo "</tr>";
							$totBruto = 0;
							$totDiskon = 0;
							$totPpn = 0;
							$totNetto = 0;

							echo "<tr>";
							echo "<td colspan=5><i>".$spasi.$spasi."Total Tanggal ".$format->formatDateINA($tglpenjualan[$key-1])."</i></td>";
							echo "<td>".number_format($totBrutoTanggal,2)."</td>";
							echo "<td>".number_format($totDiskonTanggal,2)."</td>";
							echo "<td>".number_format($totPpnTanggal,2)."</td>";
							echo "<td>".number_format($totNettoTanggal,2)."</td>";
							echo "</tr>";
							$totBrutoTanggal = 0;
							$totDiskonTanggal = 0;
							$totPpnTanggal = 0;
							$totNettoTanggal = 0;
						}

						echo "<tr>";
						echo "<td colspan=10>".$spasi."Tanggal : <i>".$format->formatDateINA($tglpenjualan[$key])."</i></td>";
						echo "</tr>";	

						echo "<tr>";
						echo "<td colspan=10>".$spasi.$spasi."Kelompok : <i>".$kelompok[$key]."</i></td>";
						echo "</tr>";
						$baris_kelompok = true;
					}

					//========= baris untuk Kelompok ===============
					if($kelompok[$key] != $kelompok[$key-1] && $baris_kelompok == false)
					{
						echo "<tr>";
						echo "<td colspan=10>".$spasi.$spasi."Kelompok : <i>".$kelompok[$key]."</i></td>";
						echo "</tr>";	
					}

					echo "<tr>";
					echo "<td>".$i."</td>";
					echo "<td>".$mod['obatalkes_kode']."</td>";
					echo "<td>".$mod['obatalkes_nama']."</td>";
					echo "<td></td>";
					echo "<td>".$mod['qty_oa']."</td>";
					echo "<td>".number_format($mod['hargajual_oa'],2)."</td>";
					echo "<td>".number_format($mod['discount'],2)."</td>";
					echo "<td>".number_format($mod['ppn_persen'],2)."</td>";
					echo "<td>".number_format($mod['hargajual_oa'],2)."</td>";
					echo "<td></td>";
					echo "</tr>";

					$totBruto 	+= $mod['hargajual_oa']; 
					$totDiskon 	+= $mod['discount'];  
					$totPpn 	+= $mod['ppn_persen']; 
					$totNetto 	+= $mod['hargajual_oa'];

					$totBrutoTanggal += $mod['hargajual_oa']; 
					$totDiskonTanggal += $mod['discount'];  
					$totPpnTanggal += $mod['ppn_persen']; 
					$totNettoTanggal += $mod['hargajual_oa'];

					$totBrutoProdusen += $mod['hargajual_oa']; 
					$totDiskonProdusen += $mod['discount'];  
					$totPpnProdusen += $mod['ppn_persen']; 
					$totNettoProdusen += $mod['hargajual_oa'];

					if($i==$jml_row){
						echo "<tr>";
						echo "<td colspan=5><i>".$spasi.$spasi.$spasi.$spasi."Total Kelompok ".$kelompok[$key]."</i></td>";
						echo "<td>".number_format($totBruto,2)."</td>";
						echo "<td>".number_format($totDiskon,2)."</td>";
						echo "<td>".number_format($totPpn,2)."</td>";
						echo "<td>".number_format($totNetto,2)."</td>";
						echo "</tr>";
						$totBruto = 0;
						$totDiskon = 0;
						$totPpn = 0;
						$totNetto = 0;

						echo "<tr>";
						echo "<td colspan=5><i>".$spasi.$spasi."Total Tanggal ".$format->formatDateINA($tglpenjualan[$key])."</i></td>";
						echo "<td>".number_format($totBrutoTanggal,2)."</td>";
						echo "<td>".number_format($totDiskonTanggal,2)."</td>";
						echo "<td>".number_format($totPpnTanggal,2)."</td>";
						echo "<td>".number_format($totNettoTanggal,2)."</td>";
						echo "</tr>";
						$totBrutoTanggal = 0;
						$totDiskonTanggal = 0;
						$totPpnTanggal = 0;
						$totNettoTanggal = 0;

						echo "<tr>";
						echo "<td colspan=5><i>".$spasi.$spasi."Total Produsen ".$namaProdusen[$key]."</i></td>";
						echo "<td>".number_format($totBrutoProdusen,2)."</td>";
						echo "<td>".number_format($totDiskonProdusen,2)."</td>";
						echo "<td>".number_format($totPpnProdusen,2)."</td>";
						echo "<td>".number_format($totNettoProdusen,2)."</td>";
						echo "</tr>";
						$totBrutoProdusen = 0;
						$totDiskonProdusen = 0;
						$totPpnProdusen = 0;
						$totNettoProdusen = 0;
					}

					$i++;
				}
			?>
		</tbody>
	</table>
</div>