<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/_table.php
* Updated by    : Hardi
* Date          : 22-04-2014
* Issue         : EHJ-1083
* Deskripsi     : Table
**/
?>
<legend class="rim">Laporan Seluruh Barang Per Dokter</legend>
<div id="lapbarangdokter-m-grid" class="grid-view">
	<table class="table table-striped table-bordered table-condensed">
		<thead>
		    <tr>
		        <th id="lapbarangdokter-m-grid_c0" width="10px"> No </th>
		        <th id="lapbarangdokter-m-grid_c1"> Kode </th>
		        <th id="lapbarangdokter-m-grid_c2"> Nama </th>
		        <th id="lapbarangdokter-m-grid_c3"> Satuan </th>
		        <th id="lapbarangdokter-m-grid_c4"> QTY </th>
		        <th id="lapbarangdokter-m-grid_c5"> Bruto </th>
		        <th id="lapbarangdokter-m-grid_c6"> Diskon </th>
		        <th id="lapbarangdokter-m-grid_c7"> Ppn </th>
		        <th id="lapbarangdokter-m-grid_c8"> Netto </th>
		        <th id="lapbarangdokter-m-grid_c9"> Status </th>
		    </tr>
		</thead>
		<tbody>
				<?php 
					$i=1;
					$namaDokter[-1] = '';
					$kelompokAlkes[-1] = '';
					$jml_row = count($modKinerja);
					$totBruto = 0;
					$totDiskon = 0;
					$totPpn = 0;
					$totNetto = 0;

					$totBrutoDokter = 0;
					$totDiskonDokter = 0;
					$totPpnDokter = 0;
					$totNettoDokter = 0;
					foreach ($modKinerja as $key => $mod) {
						$namaDokter[$key] 		= $mod['dokter'];
						$kelompokAlkes[$key] 	= $mod['jenisobatalkes_nama'];
						$total_aktif = true;
						if($namaDokter[$key]!=$namaDokter[$key-1])
						{
							if($key>0){
								echo "<tr>";
								echo "<td colspan=5><b>Total Kelompok ".$kelompokAlkes[$key-1]."</b></td>";
								echo "<td>".number_format($totBruto,2)."</td>";
								echo "<td>".number_format($totDiskon,2)."</td>";
								echo "<td>".number_format($totPpn,2)."</td>";
								echo "<td>".number_format($totNetto,2)."</td>";
								echo "</tr>";
								$totBruto = 0;
								$totDiskon = 0;
								$totPpn = 0;
								$totNetto = 0;

								echo "<tr>";
								echo "<td colspan=5><b>Total Dokter :".$namaDokter[$key-1]."</b></td>";
								echo "<td>".number_format($totBrutoDokter,2)."</td>";
								echo "<td>".number_format($totDiskonDokter,2)."</td>";
								echo "<td>".number_format($totPpnDokter,2)."</td>";
								echo "<td>".number_format($totNettoDokter,2)."</td>";
								echo "</tr>";
								$totBrutoDokter = 0;
								$totDiskonDokter = 0;
								$totPpnDokter = 0;
								$totNettoDokter = 0;

								$total_aktif = false;
							}		
							echo "<tr>";
							echo "<td colspan=5><b>Dokter : ".$mod['dokter']."</b></td>";
							echo "</tr>";					
						}

						if($kelompokAlkes[$key]!=$kelompokAlkes[$key-1])
						{
							if(($key>0) and ($total_aktif)){
								echo "<tr>";
								echo "<td colspan=5><b>Total Kelompok ".$kelompokAlkes[$key-1]."</b></td>";
								echo "<td>".number_format($totBruto,2)."</td>";
								echo "<td>".number_format($totDiskon,2)."</td>";
								echo "<td>".number_format($totPpn,2)."</td>";
								echo "<td>".number_format($totNetto,2)."</td>";
								echo "</tr>";
								$totBruto = 0;
								$totDiskon = 0;
								$totPpn = 0;
								$totNetto = 0;
							}

							echo "<tr>";
							echo "<td colspan=5>&nbsp;&nbsp;&nbsp;<b>Kelompok : ".$mod['jenisobatalkes_nama']."</b></td>";
							echo "</tr>";							
						}

						echo "<tr>";
						echo "<td>".$i."</td>";
						echo "<td>".$mod['obatalkes_kode']."</td>";
						echo "<td>".$mod['obatalkes_nama']."</td>";
						echo "<td></td>";
						echo "<td>".$mod['qty_oa']."</td>";
						echo "<td>".number_format($mod['hargajual_oa'],2)."</td>";
						echo "<td>".number_format($mod['discount'],2)."</td>";
						echo "<td>".number_format($mod['ppn_persen'],2)."</td>";
						echo "<td>".number_format($mod['hargajual_oa'],2)."</td>";
						echo "<td></td>";
						echo "</tr>";

						$totBruto 	+= $mod['hargajual_oa']; 
						$totDiskon 	+= $mod['discount'];  
						$totPpn 	+= $mod['ppn_persen']; 
						$totNetto 	+= $mod['hargajual_oa']; 

						$totBrutoDokter += $mod['hargajual_oa'];
						$totDiskonDokter += $mod['discount'];  
						$totPpnDokter += $mod['ppn_persen']; 
						$totNettoDokter += $mod['hargajual_oa']; 
						if($i==$jml_row){
							echo "<tr>";
							echo "<td colspan=5><b>Total Kelompok ".$kelompokAlkes[$key-1]."</b></td>";
							echo "<td>".number_format($totBruto,2)."</td>";
							echo "<td>".number_format($totDiskon,2)."</td>";
							echo "<td>".number_format($totPpn,2)."</td>";
							echo "<td>".number_format($totNetto,2)."</td>";
							echo "</tr>";
							$totBruto = 0;
							$totDiskon = 0;
							$totPpn = 0;
							$totNetto = 0;

							echo "<tr>";
							echo "<td colspan=5><b>Total Dokter : ".$namaDokter[$key-1]."</b></td>";
							echo "<td>".number_format($totBrutoDokter,2)."</td>";
							echo "<td>".number_format($totDiskonDokter,2)."</td>";
							echo "<td>".number_format($totPpnDokter,2)."</td>";
							echo "<td>".number_format($totNettoDokter,2)."</td>";
							echo "</tr>";
							$totBrutoDokter = 0;
							$totDiskonDokter = 0;
							$totPpnDokter = 0;
							$totNettoDokter = 0;
						}

						$i++;
					}
				?>
		</tbody>
	</table>
</div>