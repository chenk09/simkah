<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/admin.php
* Updated by    : Hardi
* Date          : 22-04-2014
* Issue         : EHJ-1083
* Deskripsi     : Admin
**/
?>
<?php
	Yii::app()->clientScript->registerScript('search', "
	$('.search-button').click(function(){
	    $('.search-form').toggle();
	    return false;
	});
	$('#searchLaporan').submit(function(){
	    $('#Grafik').attr('src','').css('height','0px');
	    $.fn.yiiGridView.update('tableLaporan', {
	            data: $(this).serialize()
	    });
	    return false;
	});
	");
?>
<div class="search-form">
    <?php
        $this->renderPartial('kinerjapenjualanOA/LapSeluruhBarangDokter/_search',
            array(
                'model'=>$model,
            )
        );
    ?>
</div>
<?php
    $this->renderPartial('kinerjapenjualanOA/LapSeluruhBarangDokter/_table',
        array(
            'modKinerja'=>$modKinerja,
        )
    );
?>
<?php 
	$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
	$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
	$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintSeluruhBarangDokter');
	$this->renderPartial('_footerLaporanlembar', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>