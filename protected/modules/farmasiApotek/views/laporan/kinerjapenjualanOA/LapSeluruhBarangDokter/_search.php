<?php
/**
* modules/farmasiApotek/views/laporan/kinerjapenjualanOA/_search.php
* Updated by    : Hardi
* Date          : 22-04-2014
* Issue         : EHJ-1083
* Deskripsi     : Search
**/
?>

<script type="text/javascript">
    function reseting()
    {
        setTimeout(function(){
            $.fn.yiiGridView.update('lapbarangdokter-m-grid', {
                    data: $('#lapbarangdokter-m-search').serialize()
            });
        },1000);

    }   
</script>

<fieldset>
    <legend class="rim">Pencarian</legend>
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
            'id'=>'laporan-search',
            'type'=>'horizontal',
        )
    );
    ?>
    <table width="100%" >
        <tr>
            <td>
                <?php echo CHtml::label('Periode Penjualan', 'Periode Penjualan', array('class' => 'control-label')) ?>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                        'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div>
            </td>
            <td>
                <?php echo CHtml::label('Sampai dengan', 'Sampai dengan', array('class' => 'control-label')) ?>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                        'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Dokter','pegawai_id', array('class'=>'control-label')); ?> 
                    <div class="controls">
                        <?php echo $form->dropDownList($model, 'pegawai_id', CHtml::listData(PegawaiM::model()->findAll(array('order'=>'nama_pegawai')), 'pegawai_id', 'nama_pegawai'),array('class'=>'span3', 'empty'=>'-- Pilih Nama Dokter --')); ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Nama Kelompok Obat','jnskelompok', array('class'=>'control-label')); ?> 
                    <div class="controls">
                        <?php echo $form->dropDownList($model, 'jnskelompok', CHtml::listData(LookupM::model()->findAllbyAttributes(array('lookup_type'=>'jnskelompok')), 'lookup_value', 'lookup_name'),array('class'=>'span3', 'empty'=>'-- Pilih Kelompok Obat --')); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div class="form-actions">
        <?php 
            echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
            echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('onClick'=>'reseting()', 'class'=>'btn btn-danger', 'type'=>'reset'));
         ?>
    </div>
    <?php $this->endWidget(); ?>
</fieldset>
<?php
Yii::app()->clientScript->registerScript('search', "
$('#lapbarangdokter-m-search').submit(function(){
    $.fn.yiiGridView.update('lapbarangdokter-m-grid', {
        data: $(this).serialize()
    });
    return false;
});
");
?>