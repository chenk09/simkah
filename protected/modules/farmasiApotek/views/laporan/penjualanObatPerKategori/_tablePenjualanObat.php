<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->searchTabelKategoriDua();
$template = "{summary}\n{items}\n{pager}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->printTabelKategoriDua();
  $template = "{items}";
  if ($caraPrint == "EXCEL") {
      echo $caraPrint;
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'footerHtmlOptions'=>array('colspan'=>14,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total (Rp.)',
            ),
	    'nama_pasien','no_rekam_medik','no_pendaftaran','tgl_pendaftaran','nama_pegawai',
            array(
                'header'=>'No Resep',
                'type'=>'raw',
                'value'=>'$data->noresep',
            ),
            array(
                'header'=>'Tanggal Penjualan',
                'type'=>'raw',
                'value'=>'$data->tglpenjualan',
            ),
            array(
                'header'=>'Jenis Penjualan',
                'type'=>'raw',
                'value'=>'$data->jenispenjualan',
            ),
            array(
                'header'=>'Nama Obat Alkes',
                'type'=>'raw',
                'value'=>'$data->obatalkes_nama',
            ),
	    'obatalkes_golongan',
            array(
                'header'=>'Satuan Kecil',
                'type'=>'raw',
                'value'=>'$data->satuankecil_nama',
            ),
            array(
                'header'=>'Qty',
                'type'=>'raw',
                'value'=>'$data->qty_oa',
            ),
            array(
                'header'=>'Harga Satuan',
                'type'=>'raw',
                'name'=>'hargasatuan_oa',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->hargasatuan_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
            
            ),
            array(
                'header'=>'Total',
                'name'=>'hargajual_oa',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->hargajual_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:left;'),
                'footerHtmlOptions'=>array('style'=>'text-align:left;font-weight:bold;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('hargajual_oa',true))
            ),
/*
            array(
                'header'=>'Status Bayar',
                'name'=>'status_bayar',
                'value'=>'(empty($data->oasudahbayar_id)) ? "Belum" : "Sudah"',
                'footerHtmlOptions'=>array('style'=>'color:white;'),
                'footer'=>'-'
            ),
*/
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>
