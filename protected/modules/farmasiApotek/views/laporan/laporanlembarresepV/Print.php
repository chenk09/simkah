<?php 

if($caraPrint=='EXCEL')
{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$judulLaporan.'-'.date("Y/m/d").'.xls"');
    header('Cache-Control: max-age=0');     
}
echo $this->renderPartial('application.views.headerReport.headerLaporanTransaksi',array('judulLaporan'=>'Laporan Kunjungan Resep', 'periode'=>'Periode : '.$periode, 'colspan'=>7)); 

if ($caraPrint != 'GRAFIK')
$this->renderPartial('laporanlembarresepV/_table', array('model'=>$model, 'caraPrint'=>$caraPrint, 'tglAwal'=>$tglAwal, 'tglAkhir'=>$tglAkhir)); 

if ($caraPrint == 'GRAFIK')
echo $this->renderPartial('_grafik', array('model'=>$model, 'data'=>$data, 'caraPrint'=>$caraPrint, 'searchdata'=>$model->searchGrafikLembarResep()), true); 


?>