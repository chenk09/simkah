<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
    ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
        .nav-tabs > .active a:hover{cursor:pointer;}
    </style>
    <style>
        #penjamin, #ruangan, #statusBayar{
            width:250px;
        }
        #penjamin label.checkbox, #ruangan label.checkbox, #statusBayar label.checkbox{
            width: 150px;
            display:inline-block;
        }

    </style>
    <legend class="rim">Berdasarkan Tanggal Penjualan</legend>
    <table>
        <tr>
            <td>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                <div class = 'control-label'>Tanggal Penjualan</div>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAwal',
                        'mode' => 'datetime',
//                                          'maxDate'=>'d',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div> 
            </td>
            <td style="padding:0px 130px 0 0px;"> <?php echo CHtml::label('Sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
//                                         'maxdate'=>'d',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div> </td>
        </tr>
    </table>
    <fieldset>
        <legend class="rim">Berdasarkan Cara Bayar </legend>
        <table width="216" border="0">
            <tr>
                <td width="51">   
                    <div id='searching'>
                        <table>
                            <tr>
                                <td>
                                    <?php echo $form->dropDownListRow($model, 'jenispenjualan', Jenispenjualan::items(), array('empty' => '-- Pilih --')); ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <?php
//                                    echo CHtml::hiddenField('filter', 'carabayar', array('disabled' => 'disabled')) . 

                                    echo $form->dropDownListRow($model, 'carabayar_id', CHtml::listData(CarabayarM::model()->findAll('carabayar_aktif = true'), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                        'ajax' => array('type' => 'POST',
                                            'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                                            'update' => '#penjamin', //selector to update
                                        ),
                                    ));
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="control-group ">
                                        <?php echo $form->labelEx($model, 'penjamin_id', array('class' => 'control-label')); ?>
                                        <div class="controls">
                                            <div id="penjamin">
                                                <?php echo $form->checkBoxList($model, 'penjamin_id', CHtml::listData($model->getPenjaminItems($model->carabayar_id), 'penjamin_id', 'penjamin_nama'), array('value' => 'pengunjung', 'empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)")); ?>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="51">   
                    <div id='searching'>
                        <table>
                            <tr>
                                <td>
                                    <div class="control-group ">
                                        <?php // echo $form->labelEx($model, 'oasudahbayar_id', array('class'=>'control-label')); ?>
                                        <label class="control-label"> Status Bayar </label>
                                        <div class="controls">
                                            <div id="statusBayar">
                                                <?php echo $form->checkBoxList($model, 'oasudahbayar_id', array(true => 'Sudah Bayar', false => 'Belum Bayar')); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
//                                    echo CHtml::hiddenField('filter', 'carabayar', array('disabled' => 'disabled')) .
                                    echo $form->dropDownListRow($model, 'instalasiasal_nama', CHtml::listData(InstalasiM::model()->findAll('instalasi_id in (2,3,4)'), 'instalasi_nama', 'instalasi_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
                                        'ajax' => array('type' => 'POST',
                                            'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganAsalNamaForCheckBox', array('encode' => false, 'namaModel' => '' . $model->getNamaModel() . '')),
                                            'update' => '#ruangan', //selector to update
                                        ),
                                    ));
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="control-group ">
                                        <?php echo $form->labelEx($model, 'ruanganasal_nama', array('class' => 'control-label')); ?>
                                        <div class="controls">
                                            <!--<div id="ruangan"><?php //echo $form->checkBoxList($model, 'ruanganasal_nama', CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'), 'ruangan_nama', 'ruangan_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)"));   ?></div>-->
                                            <div id="ruangan">-</div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </fieldset>


    <div class="form-actions">
        <?php
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
        <?php
 echo CHtml::htmlButton(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                                                                        array('class'=>'btn btn-danger','onclick'=>'konfirmasi()','onKeypress'=>'return formSubmit(this,event)'));?> 
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
?>


<?php
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai

Yii::app()->clientScript->registerScript('reloadPage', '
    function konfirmasi(){
        window.location.href="' . Yii::app()->createUrl($module . '/' . $controller . '/LaporanPenjualanObat', array('modulId' => Yii::app()->session['modulId'])) . '";
    }', CClientScript::POS_HEAD);
?>