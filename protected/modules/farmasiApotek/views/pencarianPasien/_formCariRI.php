
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRI(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                    array(
                       'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran'
                    ),
                    array(
                       'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'value'=>'$data->instalasi_nama',
                    ),
                    array(
                       'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                    ),
                    array(
                       'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                       'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    array(
                       'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                    ),
                    array(
                       'name'=>'penjamin_nama',
                        'type'=>'raw',
                        'value'=>'$data->penjamin_nama',
                    ),
                    array(
                       'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                    ),
                    array(
                       'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'$data->umur',
                    ),
                    array(
                       'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                        'header'=>'Penjualan Resep',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("penjualanResep/JualResep",array("idPendaftaran"=>$data->pendaftaran_id, "tipe"=>"informasi")))',
//                        'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("penjualanResep/JualResep",array("idPendaftaran"=>$data->pendaftaran_id,"frame"=>true)),
//                                    array("class"=>"", 
//                                          "target"=>"iframePenjualanResep",
//                                          "onclick"=>"$(\"#dialogPenjualanResep\").dialog(\"open\");",
//                                          "rel"=>"tooltip",
//                                          "title"=>"Klik untuk menjual resep",
//                                    ))',
                    ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr></hr>

<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?>