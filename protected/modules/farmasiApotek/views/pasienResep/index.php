<legend class="rim2">Informasi Pesien Resep</legend>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#FAResepturT_noresep',
        'method'=>'get',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

$this->widget('bootstrap.widgets.BootAlert');

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");?>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$modReseptur->searchPasien(),
//        'filter'=>$modReseptur,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            'noresep',
            'tglreseptur',
            'pendaftaran.no_pendaftaran',
            'pasien.no_rekam_medik',
            'pasien.nama_pasien',
            'pasien.nama_bin',
            'pendaftaran.carabayar.carabayar_nama',
            'pendaftaran.penjamin.penjamin_nama',
            'pendaftaran.kasuspenyakit.jeniskasuspenyakit_nama',
            'pendaftaran.umur',
            'ruanganreseptur.instalasi.instalasi_nama',
            'ruanganreseptur.ruangan_nama',
            array(
                'header'=>'Penjualan Resep',
                'type'=>'raw', 
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("penjualanResep/reseptur",array("idReseptur"=>$data->reseptur_id)),
                            array("class"=>"", 
                                  "target"=>"iframePenjualanResep",
                                  "onclick"=>"$(\"#dialogPenjualanResep\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk menjual resep",
                            ))',
  'htmlOptions'=>array('style'=>'text-align: center; width:40px'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>

<fieldset>
    <legend class="rim"><?php echo  Yii::t('mds','Search Patient') ?></legend>
    <table class="table-condensed">
        <tr>
            <td>
                <?php echo $form->textFieldRow($modReseptur,'noRekamMedik',array('class'=>'span3 numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($modReseptur,'namaPasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($modReseptur,'namaBin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($modReseptur,'tglreseptur', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modReseptur,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                    //
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label(' Sampai dengan','tgl_akhir', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modReseptur,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <?php echo $form->textFieldRow($modReseptur,'noresep',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>
      
    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
			 <?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">
    
</script>

<?php 
// Dialog buat nambah data propinsi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPenjualanResep',
    'options'=>array(
        'title'=>'Penjualan Resep',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
        'close'=>"js:function(){ $.fn.yiiGridView.update('pencarianpasien-grid', {
                        data: $('#caripasien-form').serialize()
                    }); }",
    ),
));
?>
<iframe src="" name="iframePenjualanResep" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end propinsi dialog =============================
?>