<legend class="rim2">Informasi Kartu Stok Obat Alkes</legend>
<?php
Yii::app()->clientScript->registerScript('search', "
$('#rjinfostokobatalkesruangan-i-search').submit(function(){
$('#informasistokobatalkesruangan-i-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('informasistokobatalkesruangan-i-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'informasistokobatalkesruangan-i-grid',
	'dataProvider'=>$model->searchInformasi(),
	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
         'mergeHeaders'=>array(
             array(
                'name'=>'<center>Qty</center>',
                'start'=>8, //indeks kolom 3
                'end'=>10, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Sub Total Harga</center>',
                'start'=>11, //indeks kolom 3
                'end'=>13, //indeks kolom 4
            ),
        ),
	'columns'=>array(
                        array(
                            'name'=>'tglstok_in',
                            'header'=>'Tanggal Transaksi',
                            'value'=>'$data->tglstok_in',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Jenis Obat Alkes / Golongan',
                            'value'=>'$data->jenisobatalkes_nama." / ".$data->obatalkes_golongan',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
//                        array(
//                            'header'=>'Golongan',
//                            'value'=>'$data->obatalkes_golongan',
//                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                        ),
                        array(
                            'header'=>'Kode',
                            'value'=>'$data->obatalkes_kode',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Nama',
                            'value'=>'$data->obatalkes_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Asal Barang',
                            'value'=>'$data->sumberdana_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Satuan',
                            'value'=>'$data->satuankecil_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Harga',
                            'value'=>'"Rp. ".MyFunction::formatNumber($data->harganetto_oa)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Tanggal Kadaluarsa',
                            'value'=>'(empty($data->tglkadaluarsa)) ? "-" : $data->tglkadaluarsa',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'In',
                            'value'=>'MyFunction::formatNumber($data->qtystok_in)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Out',
                            'value'=>'MyFunction::formatNumber($data->qtystok_out)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Current',
                            'value'=>'MyFunction::formatNumber($data->qtystok_current)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
            // Sub Total Harga In Out CUrrent //
                        array(
                            'header'=>'In',
                            'value'=>'"Rp. ".MyFunction::formatNumber($data->qtystok_in * $data->harganetto_oa)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Out',
                            'value'=>'"Rp. ".MyFunction::formatNumber($data->qtystok_out * $data->harganetto_oa)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Current',
                            'value'=>'"Rp. ".MyFunction::formatNumber($data->qtystok_current * $data->harganetto_oa)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
            
            // end Sub Total Harga In Out Current //
//                        array(
//                            'header'=>'Diskon',
//                            'value'=>'$data->discount',
//                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                        ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<?php $this->renderPartial($this->pathView.'_searchinformasi',array('model'=>$model)); ?>