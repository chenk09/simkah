<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'stokobatalkes-search',
                'type'=>'horizontal',
)); ?>

<legend class="rim">Pencarian</legend>
                <div class="control-group ">
                    <table>
                        <tr>
                            <td>
                                <?php echo $form->dropDownListRow($model,'jenisobatalkes_id',CHtml::listData($model->getJenisobatalkesItems(),'jenisobatalkes_id','jenisobatalkes_nama'),array('class'=>'span2','empty'=>'-- Pilih --')); ?>
                                <?php echo $form->dropDownListRow($model,'obatalkes_golongan',obatalkes_golongan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2')); ?>
                                <?php echo $form->dropDownListRow($model,'sumberdana_id',CHtml::listData($model->getSumberdanaItems(),'sumberdana_id','sumberdana_nama'),array('class'=>'span2','empty'=>'-- Pilih --')); ?>
                            </td>
                            <td>
                                <?php echo $form->textFieldRow($model,'obatalkes_kode',array('class'=>'span3')); ?>
                                <?php echo $form->textFieldRow($model,'obatalkes_nama',array('class'=>'span3')); ?>
                            </td>
                        </tr>
                    </table>
                    </div>

	<div class="form-actions">
                    <?php
                        echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit'));
                        echo CHtml::link(Yii::t('mds','{icon} Cancel',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                                                Yii::app()->createUrl($this->route), 
                                                array('class'=>'btn btn-danger',
                                                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;'));
                 $content = $this->renderPartial('farmasiApotek.views.'.'tips/informasi',array(),true);
                      $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
                    ?>
	</div>

<?php $this->endWidget(); ?>
