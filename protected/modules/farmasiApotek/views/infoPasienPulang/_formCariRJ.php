
<?php
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRJ(),
//                'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'mergeHeaders'=>array(
            array(
                'name'=>'<center>Penjamin</center>',
                'start'=>5,
                'end'=>6,
            ),
        ),
	'columns'=>array(
            array(
                'header'=>'Tgl Pendaftaran',
                'name'=>'tgl_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->tgl_pendaftaran',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'name'=>'no_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->no_pendaftaran',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'name'=>'no_rekam_medik',
                'type'=>'raw',
                'value'=>'$data->no_rekam_medik',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'Nama',
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->nama_pasien',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'Alias',
                'name'=>'nama_bin',
                'type'=>'raw',
                'value'=>'$data->nama_bin',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
            ),
            array(
                'header'=>'Cara Bayar',
                'name'=>'carabayar_nama',
                'type'=>'raw',
                'value'=>'$data->carabayar_nama',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
            ),
            array(
                'header'=>'Penjamin',
                'name'=>'penjamin_nama',
                'type'=>'raw',
                'value'=>'$data->penjamin_nama',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
            ),
            array(
                'header'=>'Penanggung',
                'name'=>'nama_pj',
                'type'=>'raw',
                'value'=>'isset($data->nama_pj) ? CHtml::Link($data->nama_pj,Yii::app()->controller->createUrl("DaftarPasien/informasiPenanggung",array("id"=>$data->no_pendaftaran,"frame"=>true)),array("class"=>"", "target"=>"iframeInformasiPenanggung", "onclick"=>"$(\"#dialogInformasiPenanggung\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik untuk melihat Informasi Penanggung Jawab",)) : "-"',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'Jenis Kasus Penyakit',
                'name'=>'jeniskasuspenyakit_nama',
                'type'=>'raw',
                'value'=>'$data->jeniskasuspenyakit_nama',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'name'=>'umur',
                'type'=>'raw',
                'value'=>'$data->umur',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'Alamat',
                'name'=>'alamat_pasien',
                'type'=>'raw',
                'value'=>'$data->alamat_pasien',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                        'header'=>'Rincian Tagihan Farmasi',
                        'type'=>'raw',
//                        'value'=>'($data->getStatusFarmasi($data->pendaftaran_id)>0) ? CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
//                                array("class"=>"", 
//                                      "target"=>"iframeRincianTagihan",
//                                      "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
//                                      "rel"=>"tooltip",
//                                      "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
//                                      "htmlOptions"=>array("style"=>"text-align: center; width:40px")
//                                )) : "Belum ada <br/>Transaksi <br/>Apotek"',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->createUrl("billingKasir/RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianTagihan",
                                          "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
            array(
                'header'=>'Status<br/>Farmasi',
                'type'=>'raw',
                'value'=>'FAInformasikasirrawatjalanV::getFarmasiStatus($data->pendaftaran_id)',
//                'value'=>'CHtml::link("<i class=icon-list-alt></i>","",array("href"=>"#","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Status Farmasi","onclick"=>"ubahStatusFarmasi($data->pendaftaran_id);"))',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>

<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
// Dialog untuk ubah status periksa =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogUbahStatusFarmasi',
    'options'=>array(
        'title'=>'Ubah Status Farmasi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>600,
        'minHeight'=>500,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end ubah status periksa dialog =============================
?>
<script>
function ubahStatusFarmasi(idpendaftaran){
    var idpendaftaran = idpendaftaran;
    var status = "RJ";
    var answer = confirm('Yakin Akan Verifikasi Status Farmasi Pasien?');
    if (answer){
          $.post('<?php echo Yii::app()->createUrl('ActionAjaxRIRD/UbahStatusFarmasi');?>', {idpendaftaran:idpendaftaran,status:status}, function(data){
//                   if(data.pesan == 'Berhasil'){
//                       alert('Status Farmasi berhasil diubah');
//                   }else{
//                       alert('Status Farmasi gagal diubah');
//                   }
                     setTimeout("$.fn.yiiGridView.update('daftarpasien-v-grid');",1000);
        }, 'json');
    }else{
        preventDefault();
    }
}
</script>