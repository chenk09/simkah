
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRI(),
//                'filter'=>$model,
                'template'=>"{pager}{summary}\n{items}",

                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Tgl Admisi / <br/>Tgl Pulang Awal',
                'name'=>'tgl_pulang',
                'type'=>'raw',
                'value'=>'$data->combineTglPendaftaran'
            ),
            array(
                'header'=>'Nama Instalasi',
                'name'=>'instalasi_nama',
                'type'=>'raw',
                'value'=>'$data->instalasi_nama',
            ),
            array(
                'header'=>'No Pendaftaran',
                'name'=>'no_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->no_pendaftaran',
            ),
            array(
                'name'=>'no_rekam_medik',
                'type'=>'raw',
                'value'=>'$data->no_rekam_medik',
            ),
            array(
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->nama_pasien',
            ),
            array(
                'name'=>'nama_bin',
                'type'=>'raw',
                'value'=>'$data->nama_bin',
            ),
            array(
                'header'=>'Cara Bayar',
                'name'=>'carabayar_nama',
                'type'=>'raw',
                'value'=>'$data->carabayar_nama',
            ),
            array(
                'header'=>'Nama Penjamin',
                'name'=>'penjamin_nama',
                'type'=>'raw',
                'value'=>'$data->penjamin_nama',
            ),
            array(
                'header'=>'Nama Jenis Kasus Penyakit',
                'name'=>'jeniskasuspenyakit_nama',
                'type'=>'raw',
                'value'=>'$data->jeniskasuspenyakit_nama',
            ),
            array(
                'name'=>'umur',
                'type'=>'raw',
                'value'=>'$data->umur',
            ),
            array(
                'name'=>'alamat_pasien',
                'type'=>'raw',
                'value'=>'$data->alamat_pasien',
            ),
            array(
                        'header'=>'Rincian Tagihan Farmasi',
                        'type'=>'raw',
//                        'value'=>'($data->getStatusFarmasi($data->pendaftaran_id)>0) ? CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
//                                array("class"=>"", 
//                                      "target"=>"iframeRincianTagihan",
//                                      "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
//                                      "rel"=>"tooltip",
//                                      "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
//                                      "htmlOptions"=>array("style"=>"text-align: center; width:40px")
//                                )) : "Belum ada <br/>Transaksi <br/>Apotek"',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->createUrl("billingKasir/RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianTagihan",
                                          "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
            array(
                'header'=>'Status<br/>Farmasi',
                'type'=>'raw',
                'value'=>'FAInformasikasirinappulangV::getFarmasiStatus($data->pendaftaran_id)',
//                'value'=>'CHtml::link("<i class=icon-list-alt></i>","",array("href"=>"#","rel"=>"tooltip","title"=>"Klik Untuk Mengubah Status Farmasi","onclick"=>"ubahStatusFarmasi($data->pendaftaran_id);"))',
            ),
                    
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
     

?>
<hr></hr>

<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<script>
function ubahStatusFarmasi(idpendaftaran){
    var idpendaftaran = idpendaftaran;
    var status = "RI";
    var answer = confirm('Yakin Akan Verifikasi Status Farmasi Pasien?');
    if (answer){
          $.post('<?php echo Yii::app()->createUrl('ActionAjaxRIRD/UbahStatusFarmasi');?>', {idpendaftaran:idpendaftaran,status:status}, function(data){
//                   if(data.pesan == 'Berhasil'){
//                       alert('Status Farmasi berhasil diubah');
//                   }else{
//                       alert('Status Farmasi gagal diubah');
//                   }
                   setTimeout("$.fn.yiiGridView.update('daftarpasien-v-grid');",1000);
        }, 'json');
    }else{
        preventDefault();
    }
}
</script>