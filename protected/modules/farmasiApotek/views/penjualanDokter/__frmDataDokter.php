<fieldset id="frm_data_dokter">
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <label class="control-label" for="FADokterV_nama_pegawai">Nama</label>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete',
                                array(
                                    'model'=>$modDokter,
                                    'attribute'=>'nama_pegawai',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ListDokter'),
                                    'options'=>array(
                                        'class'=>'span3',
                                        'showAnim'=>'fold',
                                        'minLength' => 2,
                                        'select'=>'js:function( event, ui ){
                                            inputDataDokter(ui.item.pegawai_id);
                                            return false;
                                        }',
                                    ),
                                    'htmlOptions'=>array(
                                        'placeholder'=>'Ketikan Nama Dokter',
                                        //'onblur'=>'cariDataPasien(this.value);',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'class'=>'span3'
                                    ),
                                    'tombolDialog'=>array(
                                        'idDialog'=>'dialogDokter'
                                    ),
                                )
                            ); 
                        ?>                    
                    </div>
                </div>
                <?php echo $form->textFieldRow($modDokter,'jeniskelamin',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($modDokter,'nomorindukpegawai',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
            </td>
            <td>
                <?php echo $form->textAreaRow($modDokter,'alamat_pegawai',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>


<!-- Dialog pencarian dokter -->
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>700,
        'height'=>450,
        'resizable'=>false,
    ),
));

$modPegawai = new FAPegawaiM('searchByFilterDokter');
$modPegawai->unsetAttributes();
if(isset($_GET['FAPegawaiM'])){
    $modPegawai->attributes = $_GET['FAPegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'cari-dokter-m-grid',
    'dataProvider'=>$modPegawai->searchByFilterDokter(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectDokter",
                "onClick"=>"inputDataDokter(\"$data->pegawai_id\");return false;"
                )
            )'
        ),
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai'
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>

<script type="text/javascript">

function inputDataDokter(id_dokter)
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/getDataPegawai'); ?>', {idPegawai:id_dokter},
        function(data)
        {
            $("#FAPenjualanResepT_pasienpegawai_id").val(data.pegawai_id);
            $("#dialogDokter").dialog('close');
            for(x in data)
            {
                $("#frm_data_dokter").find("input[name$='["+ x +"]']").val(data[x]);
            }
        },
        'json'
    );
}

</script>
    