<?php // $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<!--<div style="height: 3cm;"></div>-->
<?php $format = new CustomFormat();?>
<style>
    th, td, div{
        font-family: Arial;
        font-size: 11pt;
    }
    .tandatangan{
        vertical-align: bottom;
        text-align: center;
    }
</style>
<table width="100%" style='margin-left:auto; margin-right:auto;font-size:12px; '>   
<tr>
        <td width="13%">No. Resep</td>
        <td>: <?php echo $modPenjualan->noresep;?>
        <td width="10%">Nama</td>
        <td>: <?php echo $modDokter->nomorindukpegawai ?> - <?php echo $modDokter->gelardepan." ".$modDokter->nama_pegawai.", ".$modDokter->gelarbelakang_nama;?> </td>
    </tr>
    <tr>
        <td>Tgl. Resep</td>
        <td>: <?php echo $modPenjualan->tglresep;?></td>
        <td>Alamat</td>
        <td>: RS. Jasakartini Tasikmalaya
        </td>
    </tr>
    <tr>
        <td>Tgl. Penjualan </td>
        <td>: <?php echo $modPenjualan->tglpenjualan;?></td>
        <td></td>
        <td>
            <?php echo (!empty($modDokter->alamat_pegawai)) ? "/".$modDokter->alamat_pegawai : "";?>
        </td>
    </tr>
</table>
<table class='table table-striped table-bordered table-condensed'>
    <thead style='border:1px solid;'>
        <th style='text-align: center; width: 20px;'>No.</th>
        <th style='text-align: center;'>Kode</th>
        <th style='text-align: center;'>Nama</th>
        <th style='text-align: center;'>Harga Satuan</th>
        <th style='text-align: center;'>Qty</th>
        <th style='text-align: center;'>Subtotal</th>
    </thead>
    <tbody>
    <?php
    $no=1;
    $subTotal = 0;
    $totalSubTotal = 0;
    $biayaAdmin = 0;
    if (count($obatAlkes) > 0){
        foreach($obatAlkes AS $tampilData):
        $subTotal = ($tampilData->hargasatuan_oa * $tampilData->qty_oa); 
        $totalSubTotal += $subTotal;
        echo "<tr style='border:1px solid;''>
            <td style='text-align:center;'>".$no."</td>
            <td>".$tampilData->obatalkes->obatalkes_kode."</td>
            <td>".$tampilData->obatalkes->obatalkes_nama."</td>
            <td style='text-align: right;'>".$format->formatNumber($tampilData->hargasatuan_oa)."</td>
            <td style='text-align: center;'>".$format->formatNumber($tampilData->qty_oa)."</td>
            <td style='text-align: right;'>".$format->formatNumber($subTotal)."</td>
         </tr>";  
        $no++;
        endforeach;
    }
    $biayaAdmin = $modPenjualan->totaltarifservice + $modPenjualan->biayaadministrasi + $modPenjualan->biayakonseling;// + $modPenjualan->jasadokterresep << dihide karena sudah termasuk di harga obat
    ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Total</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($modPenjualan->totalhargajual);?></td>
        </tr>
        <tr>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Biaya Administrasi, dll.</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($biayaAdmin);?></td>
        </tr>
        <tr>
            <td colspan ="5" style='text-align: right; font-weight: bold;'>Total Tagihan</td>
            <td style='text-align: right;'>Rp. <?php echo $format->formatNumber($modPenjualan->totalhargajual + $biayaAdmin);?></td>
        </tr>
    </tfoot>
</table>


<table style="width:100%;">
    <tr><td class="tandatangan">Penerima</td>
        <td class="tandatangan">Hormat Kami,</td>
    </tr>
    <tr>
        <td class="tandatangan" style="height: 50px;">.........................</td>
        <td class="tandatangan" ><?php echo Yii::app()->user->getState('nama_pegawai'); ?>
    </td></tr>
</table>

<?php if (isset($caraPrint)) { ?>

            <div style="font-size: 9pt;">Print Date: <?php echo $modPenjualan->ruangan->ruangan_nama;?>,<?php // echo Yii::app()->user->getState('nama_pegawai'); ?>
    <?php echo date('d M Y H:i:s'); ?></div></br></br>

<?php } else { 

     echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; ?>
    <?php //$this->widget('TipsMasterData',array('type'=>'admin'));
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintFakturPenjualanDokter');
 $idPenjualan = $modPenjualan->penjualanresep_id;
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/&id=${idPenjualan}&caraPrint="+caraPrint,"",'location=_new, width=1100px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);         
 } ?>
</td>

</tr>

</table>