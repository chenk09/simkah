<table>   
    <tr>
        <td>
            <fieldset id="formNonRacikan" class="table-bordered">
                <legend class="table-bordered radio">
                    <?php echo CHtml::radioButton('pilihRacik', true, array('onclick'=>'enableFormAlkes();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Non Racikan
                </legend>
                
                <div class="control-group ">
                    <label class="control-label" for="namaObat">Nama Obat</label>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete',
                                array(
                                    'name'=>'namaObatNonRacik',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatReseptur'),
                                    'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'select'=>'js:function( event, ui){
                                               $(this).val(ui.item.obatalkes_nama);
                                               $("#idObat").val(ui.item.obatalkes_id);
                                               $("#hjaresep").val(ui.item.hjaresep);
                                               $("#jnskelompok").val(ui.item.jnskelompok);
                                               $("#qtyNonRacik").val(1);
                                               $("#isRacikan").val("0");
                                               return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'req',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogObat'),
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Signa</label>
                    <div class="controls">
                        <?php echo CHtml::hiddenField('idPenjualan', '', array('readonly'=>true)); ?>
                        <?php echo CHtml::textField('signa','',array('placeholder'=>'-- Aturan Pakai --','class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="qty">Qty</label>
                    <div class="controls">
                        <?php
                            echo CHtml::textField('qtyNonRacik', '',
                                array(
                                    'readonly'=>false,
                                    'onblur'=>'$("#qty").val(this.value);',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                    'class'=>'req span1 numbersOnly')
                            )
                        ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'addDataResep();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"addDataResep();return $('#namaObatNonRacik').focus();",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik untuk menambahkan resep",)); ?>
                    </div>
                </div>
            </fieldset>           
        </td>
        <td>
            <fieldset id="formRacikan" class="table-bordered">
                <legend class="table-bordered radio">
                    <?php echo CHtml::radioButton('pilihRacik', false, array('onclick'=>'enableFormAlkes();','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    Racikan  &nbsp;
                </legend>
                
                <div class="control-group ">
                    <label class="control-label" for="racikanKe">R ke</label>
                    <div class="controls">
                        <?php echo CHtml::dropDownList('racikanKe', '', Params::listAngka20(),array('class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="namaObatRacik">Nama Obat</label>
                    <div class="controls">
                        <?php 
                            $this->widget('MyJuiAutoComplete',
                                array(
                                    'name'=>'namaObatRacik',
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/ObatReseptur'),
                                    'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'select'=>'js:function( event, ui){
                                               $(this).val(ui.item.obatalkes_nama);
                                               $("#idObat").val(ui.item.obatalkes_id);
                                               $("#hjaresep").val(ui.item.hjaresep);
                                               $("#jnskelompok").val(ui.item.jnskelompok);
                                               $("#qtyNonRacik").val(1);
                                               $("#isRacikan").val("1");
                                               return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'req',
                                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogObat'),
                                )
                            ); 
                        ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="permintaan">Permintaan Dosis</label>
                    <div class="controls">
                        <?php echo CHtml::textField('permintaan', '', array('onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="jmlKemasan">Jml Kemasan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('jmlKemasanObat', '', array('onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
                
                <div class="control-group ">
                    <label class="control-label" for="kekuatanObat">Kekuatan</label>
                    <div class="controls">
                        <?php echo CHtml::textField('kekuatanObat', '', array('onblur'=>'hitungQtyRacikan();','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                        <span id="satuanKekuatanObat"></span>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Signa</label>
                    <div class="controls">
                        <?php echo CHtml::textField('signa_racik','',array('placeholder'=>'-- Aturan Pakai --','class'=>'span2', 'style'=>'width:80px', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label" for="qty">Qty</label>
                    <div class="controls">
                        <?php echo CHtml::textField('qtyRacik', '', array('onkeyup'=>'$("#qty").val($(this).val());','onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'inputFormTabel span1 numbersOnly')) ?>
                        <?php echo CHtml::htmlButton('<i class="icon-plus icon-white"></i>',
                                array('onclick'=>'addDataResep();return false;',
                                      'class'=>'btn btn-primary',
                                      'onkeypress'=>"addDataResep();return $('#racikanKe').focus();",
                                      'rel'=>"tooltip",
                                      'title'=>"Klik untuk menambahkan resep",
                                    )
                        ); ?>
                    </div>
                </div>
                <div style='border:1px solid #cccccc; border-radius:2px;padding:10px; width: 42%;float:right;margin-top:-70px;'>
                <font style='font-size:9pt'>Keterangan : <br>
                <font style='font-size:8pt'>Qty=Permintaan*Jml Kemasan/Kekuatan</font>
                <?php echo CHtml::hiddenField('idObat', '', array('class'=>'req', 'readonly'=>true)) ?>
                <?php echo CHtml::hiddenField('hjaresep', '', array('class'=>'req', 'readonly'=>true)) ?>
                <?php echo CHtml::hiddenField('isRacikan', '', array('class'=>'req', 'readonly'=>true)) ?>
                <?php echo CHtml::hiddenField('jnskelompok', '', array('class'=>'req', 'readonly'=>true)) ?>
            </fieldset>
        </td>
    </tr>
</table>
<?php  
if(isset($_GET['sukses'])){
    $sukses = $_GET['sukses'];
}
if($sukses > 0) 
    Yii::app()->user->setFlash('success',"Transaksi berhasil disimpan !");

?>
<div style="width: 100%;overflow: auto;">
<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Recipe</th>
            <th>R ke</th>
            <th>Nama Obat</th>
            <th>Asal Barang</th>
            <th>Satuan Kecil</th>
            <th>Qty</th>
            <th>Stok</th>
            <th>Harga</th>
            <th>Discount (%)</th>
            <th>Sub Total</th>
            <th>Signa</th>
            <th>Etiket</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
         <?php
            if((isset($obatAlkes))){
                if (count($obatAlkes) > 0){ 
                    foreach ($obatAlkes as $key => $value) {
                        $stok = StokobatalkesT::getStokBarang($value->obatalkes_id,Yii::app()->user->getState('ruangan_id'));
//                        echo $stok;
        ?>
        <tr>
            <td>
                <?php
                    echo CHtml::checkBox('penjualanResep['.$key.'][detailreseptur_id]', true, array('onchange'=>'hitungTotalSemua();','value'=>1));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][r]', $value->r, array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));    
                    echo CHtml::hiddenField('penjualanResep['.$key.'][obatalkespasien_id]', $value->obatalkespasien_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][obatalkes_id]', $value->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1 obat'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][harganetto_reseptur]', $value->harganetto_oa, array('readonly'=>true,'class'=>'inputFormTabel span1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][isRacikan]', $value->r, array());
                    echo CHtml::hiddenField('penjualanResep['.$key.'][racikan_id]', $value->racikan_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][biayakemasan]', $value->biayakemasan, array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));        
                    echo CHtml::hiddenField('penjualanResep['.$key.'][biayaservice]', $value->biayaservice, array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][biayaadministrasi]', $value->biayaadministrasi, array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][jasadokterresep]', $value->jasadokterresep, array('readonly'=>true, 'class'=>'inputFormTabel lebar2'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][kekuatan_oa]', $value->kekuatan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][satuankekuatan_oa]', $value->satuankekuatan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][permintaan_reseptur]', $value->permintaan_oa, array('readonly'=>true,'class'=>'inputFormTabel span1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][jmlkemasan_reseptur]', $value->jmlkemasan_oa, array('readonly'=>true,'class'=>'inputFormTabel span1'));
                    echo CHtml::hiddenField('penjualanResep['.$key.'][hargasatuan_reseptur_a]', $value->hargasatuan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargasatuanobat'));
                ?>
            </td>
            <td name="penjualanResep[<?php echo $key ?>][is_recipe]"><?php echo $value->r ; ?></td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][rke]', $value->rke, array('readonly'=>true,'style'=>'width:15px;'));
                ?>
            </td>
            <td name="penjualanResep[<?php echo $key ?>][nama_obat_alkes]"> <?php echo $value->obatalkes->obatalkes_nama; ?></td>
            <td>
                <?php
                    echo CHtml::dropDownList('penjualanResep['.$key.'][sumberdana_id]', $value->sumberdana_id, CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::dropDownList('penjualanResep['.$key.'][satuankecil_id]', $value->satuankecil_id, CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][qty]', $value->qty_oa, array('onkeyup'=>'hitungTotalSemua();','onblur'=>'pembulatanKeAtas(this);hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly qty', 'onfocus'=>'$(this).select();', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?><?php echo CHtml::hiddenField('penjualanResep['.$key.'][qty_a]', $value->qty_oa, array('onkeyup'=>'hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly qty2', 'onfocus'=>'$(this).select();'));        
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][stok]', $stok, array('readonly'=>true,'class'=>'inputFormTabel span1'));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][harganetto_reseptur]', $value->harganetto_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargareseptur'));
                    echo CHtml::textField('penjualanResep['.$key.'][hargasatuan_reseptur]', $value->hargasatuan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargareseptur'));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][disc]', $value->discount, array('onkeyup'=>'hitungTotalSemua();','class'=>'inputFormTabel span1 numbersOnly','onfocus'=>'$(this).select();'));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][hargajual_reseptur]', $value->hargajual_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar2'));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::textField('penjualanResep['.$key.'][signa_reseptur]', $value->signa_oa, array('placeholder'=>'-- Aturan Pakai --','class'=>'inputFormTabel span2', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                ?>
            </td>
            <td>
                <?php
                    echo CHtml::dropDownList('penjualanResep['.$key.'][etiket]', (empty($value->etiket)) ? Params::DEFAULT_ETIKET : $value->etiket, Etiket::items(),array('class'=>'inputFormTabel span2', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                ?>        
            </td>
            <td>&nbsp;</td>
        </tr>
    <?php 
                }
            }
        } 
    ?>
    </tbody>
</table>
</div>

<table id="frm_administrasi">
    <tr>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'biayaadministrasi',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'tambahRekeningAdmin();')); ?>
            <?php echo $form->textFieldRow($modPenjualan,'biayakonseling',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'tambahRekeningKonseling();')); ?>            
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'totharganetto',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totalhargajual',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>            
            <?php echo $form->hiddenField($modPenjualan,'totalhargajual',array('value'=>0,'class'=>'inputFormTabel lebar3 currency totalhargajual','readonly'=>true)); ?>            
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'jasadokterresep',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totaltarifservice',array('value'=>0,'class'=>'inputFormTabel lebar3 currency','readonly'=>false,'onblur'=>'tambahRekeningService();')); ?>
            <?php echo $form->hiddenField($modPenjualan,'pasien_id',array()); ?>
        </td>
    </tr>
</table>
<br>
<?php
//FORM REKENING
    $this->renderPartial('farmasiApotek.views.penjualanResep.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
    <?php 
        $disableSave = false;
        $disableSave = (!empty($_GET['id'])) ? true : ($sukses > 0) ? true : false;; 
    ?>
    <?php $disablePrint = ($disableSave) ? false : true; ?>
    <div class="form-actions">
        <?php
            echo CHtml::htmlButton(Yii::t('mds','{icon} Save',
                    array('{icon}'=>'<i class="icon-ok icon-white"></i>')
                ),
                array('class'=>'btn btn-primary', 'type'=>'submit','onClick'=>'submitForm();return false;','onKeypress'=>'return formSubmit(this,event)')
            );
            echo CHtml::htmlButton(
                Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                array('class'=>'btn btn-danger', 'type'=>'reset', 'id'=>'resetbtn')
            );
            echo CHtml::htmlButton(Yii::t('mds','{icon} Print',
            array('{icon}'=>'<i class="icon-print icon-white"></i>')),
            array('class'=>'btn btn-primary', 'disabled'=>$disablePrint,'id'=>'print_obat','type'=>'button','onclick'=>'print(\'caraPrint\')'))."&nbsp&nbsp";
        ?>
    </div>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Data Transaksi Penjualan Obat Dokter',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>550,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Transaksi</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Nama Pegawai</td>
                    <td width="250" tag="nama_pegawai"></td>
                    <td width="150">NIP</td>
                    <td tag="nip"></td>
                </tr>
                <tr>
                    <td width="150">Jenis Kelamin</td>
                    <td width="250" tag="jeniskelamin"></td>
                    <td width="150">Alamat Pegawai</td>
                    <td tag="alamat_pegawai"></td>
                </tr>
                <tr>
                    <td width="150">Tgl Resep</td>
                    <td width="250" tag="tgl_resep"></td>
                    <td width="150">No Resep</td>
                    <td tag="noresep"></td>
                </tr>
                <tr>
                    <td>Tgl Penjualan</td>
                    <td tag="tgl_penjualan"></td>
                    <td>Jenis Penjualan</td>
                    <td tag="jenis_penjualan"></td>
                </tr>
                <tr>
                    <td>Lama Pelayanan</td>
                    <td tag="lama_pelayanan"></td> 
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Obat Alkes</legend>
            <table id="info_tindakan_temp" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="50">Asal Barang</th>
                        <th width="50">Satuan Kecil</th>
                        <th width="50">Recipe</th>
                        <th width="50">R ke</th>
                        <th width="300">Kode / Nama Obat</th>
                        <th width="50">Qty</th>
                        <th width="150">Harga</th>
                        <th width="150">Discount</th>                        
                        <th width="150">Signa</th>
                        <th width="150">Etiket</th>
                        <th>Total</th>
                    </tr>                    
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Pembayaran</legend>
            <table id="info_pembayaran_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Total Harga Jual</td>
                    <td width="250" tag="total_hargajual" class="curency"></td>
                    <td width="150">Biaya Konseling</td>
                    <td width="250" tag="biaya_konseling" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tanggungan</td>
                    <td width="250" tag="total_tanggungan" class="curency"></td>
                    <td width="150">Jasa Dokter Resep</td>
                    <td width="250" tag="jasa_dokter" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tarif Service</td>
                    <td width="250" tag="tarif_service" class="curency"></td>
                    <td width="150">Biaya Administrasi</td>
                    <td width="250" tag="biaya_adm" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tagihan</td>
                    <td width="250" tag="total_tagihan" class="curency"></td>
                    <td width="150">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                </tr>
            </table>
        </fieldset>        
    </div>  
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses(this);return false;',
                        'disabled'=>FALSE                        
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
<script type="text/javascript">
$('.currency').each(function(){this.value = formatDesimal(this.value)});    
$("#formRacikan").attr("disabled", true);
$("#formRacikan").find(".btn").attr("disabled", true);
var trUraian = new String(<?php echo CJSON::encode($this->renderPartial('__frmInputObat',array('form'=>$form),true));?>);

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#caripasien-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
    $('#caripasien-form').submit();
}    

function submitForm()
{
    if(cekInputTindakan())
    {
        confirmPembayaran();
    }
}

function cekInputTindakan()
{
    tgl_penjualan = $('#FAPenjualanResepT_tglpenjualan').val();
    if(tgl_penjualan==''){
        alert('Silahkan isi Tgl Penjualan terlebih dahulu');
        $('#FAPenjualanResepT_tglpenjualan').focus();
        return false;
    }else{
        return true;
    }
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    var nama_pegawai = $('#caripasien-form').find('input[name$="[nama_pegawai]"]').val();
    var jeniskelamin = $('#caripasien-form').find('input[name$="[jeniskelamin]"]').val();
    var nip = $('#caripasien-form').find('input[name$="[nomorindukpegawai]"]').val();
    var alamat_pegawai = $('#caripasien-form').find('input[name$="[alamat_pegawai]"]').val();
    var tgl_resep = $('#caripasien-form').find('input[name$="[tglresep]"]').val();
    var tgl_penjualan = $('#caripasien-form').find('input[name$="[tglpenjualan]"]').val();
    var noresep = $('#caripasien-form').find('input[name$="[noresep]"]').val();
    var lama_pelayanan = $('#caripasien-form').find('input[name$="[lamapelayanan]"]').val();
     if(lama_pelayanan == ''){
        lama_pelayanan = '-';
    }else{
        lama_pelayanan = lama_pelayanan +' '+'Menit';
    }
    var jenis_penjualan = $('#caripasien-form').find('input[name$="[jenispenjualan]"]').val();
    
    var total_harganetto = unformatNumber($('#caripasien-form').find('input[name$="[totharganetto]"]').val());
    var total_hargajual = unformatNumber($('#caripasien-form').find('input[name$="[totalhargajual]"]').val());
    var jasa_dokter = unformatNumber($('#caripasien-form').find('input[name$="[jasadokterresep]"]').val());
    var biaya_adm = unformatNumber($('#caripasien-form').find('input[name$="[biayaadministrasi]"]').val());
    var biaya_konseling = unformatNumber($('#caripasien-form').find('input[name$="[biayakonseling]"]').val());
    var tarif_service = unformatNumber($('#caripasien-form').find('input[name$="[totaltarifservice]"]').val());
    
    total_tagihan = total_hargajual + biaya_adm + biaya_konseling + tarif_service;
    var data_pembayaran = {
        'total_harganetto':total_harganetto,
        'total_hargajual':total_hargajual,
        'jasa_dokter':jasa_dokter,
        'biaya_adm':biaya_adm,
        'biaya_konseling':biaya_konseling,
        'tarif_service':tarif_service,
        'total_tagihan':total_tagihan
    }
    $('#content_confirm').find('#info_pembayaran_temp td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]);
                }
            }
        }
    );    
    
    
    var data_pasien = {
        'nama_pegawai':nama_pegawai,
        'nip':nip,
        'jeniskelamin':jeniskelamin,
        'alamat_pegawai':alamat_pegawai,
        'tgl_resep':tgl_resep,
        'tgl_penjualan':tgl_penjualan,
        'lama_pelayanan':lama_pelayanan,
        'jenis_penjualan':jenis_penjualan,
        'noresep':noresep
    }
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]);
                }
            }
        }
    );
    var tr_info = "";
    var total_hrg_obat = 0;
    $('#tblDaftarResep').find('tbody tr').each(
        function()
        {
            if($(this).attr('class') != 'trfooter')
            {
                var recipe = $(this).find('td[name$="[is_recipe]"]').text();
                var r_ke = $(this).find('td input[name$="[rke]"]').val();
                var nama_obat = $(this).find('td[name$="[nama_obat_alkes]"]').text();
                var qty = $(this).find('td input[name$="[qty]"]').val();
                var harga_satuan = $(this).find('td input[name$="[hargasatuan_reseptur]"]').val();
                var discount = $(this).find('td input[name$="[disc]"]').val();
                var signa = $(this).find('td input[name$="[signa_reseptur]"]').val();
                var etiket = $(this).find('select[name$="[etiket]"] option:selected').text();
                var sumberdana = $(this).find('select[name$="[sumberdana_id]"] option:selected').text();
                var satuankecil = $(this).find('select[name$="[satuankecil_id]"] option:selected').text();
                                
                total = harga_satuan * qty;
                tr_info += '<tr>';
                tr_info += '<td>'+ sumberdana +'</td><td>'+ satuankecil +'</td><td>'+ recipe +'</td><td>'+ r_ke + '</td><td>'+ nama_obat +'</td><td>'+ qty +'</td><td class="curency">'+ harga_satuan +'</td><td class="curency">'+ discount +'</td><td>'+ signa +'</td><td>'+ etiket +'</td><td class="curency">'+ total +'</td>'
                tr_info += '</tr>';
                total_hrg_obat += parseFloat(total);
            }

        }
    );
    
    $('#info_tindakan_temp').find('tbody').empty();
    tr_info += '<tr class="trfooter">';
    tr_info += '<td colspan="10">Total</td><td class="curency">'+ total_hrg_obat +'</td>';
    tr_info += '</tr>';
    $('#info_tindakan_temp').find('tbody').append(tr_info);
    
    $('#content_confirm').find('.curency').each(
        function()
        {
//            var result = formatNumber(parseInt($(this).text()));
//          JANGAN DIBULATKAN >>  var result = formatMoney($(this).text());
            var result = accounting.formatMoney($(this).text(),'',2,',','.');
            $(this).text(result);            
        }
    );
}

function clearFormInputan()
{
    $('#formNonRacikan').find('input').each(
        function(){
            $(this).val('');
        }
    );

    $('#formRacikan').find('input[id!="jmlKemasanObat"]').each(
        function(){
            $(this).val('');
        }
    );
}

function hitungQtyRacikan()
{
    var permintaan = $('#permintaan').val();
    var jmlKemasan = $('#jmlKemasanObat').val();
    var kekuatan = $('#kekuatanObat').val();
    var qty = permintaan * jmlKemasan / kekuatan;
    qty = Math.ceil(qty);
    if (jQuery.isNumeric(permintaan)){
        $('#jmlPermintaan').val(permintaan);
    }
    if (jQuery.isNumeric(kekuatan)){
        $('#kekuatan').val(kekuatan);
    }
    if (jQuery.isNumeric(jmlKemasan)){
        $('#jmlKemasan').val(jmlKemasan);
    }
    if (jQuery.isNumeric(qty)){
        $('#qty').val(qty);
    }
    if (jQuery.isNumeric(qty)){
        $('#qtyRacik').val(qty);
    }
}

function addDataResep(id_obat)
{
    var id_obat = $('#idObat').val();
    var hargaSatuan = $('#hjaresep').val();
    var isRacikan = $('#isRacikan').val();
    var jenis_kelompok = $('#jnskelompok').val();
    var signa = $('#signa').val();
    if($('#signa').val() == ""){
        signa = $('#signa_racik').val();
    }
    var racikan = $("#tblDaftarResep tbody").find('input[name$="[isRacikan]"][value="1"').length;
    var nonRacikan = $("#tblDaftarResep tbody").find('input[name$="[isRacikan]"][value="0"').length;
    
    var kosong = "";    
    var is_valid = true;
    if($("#pilihRacik").is(":checked"))
    {
        var jumlahKosong = $("#formNonRacikan").find(".req[value="+ kosong +"]");
    }else{
        var jumlahKosong = $("#formRacikan").find(".req[value="+ kosong +"]");
        
    }
    
    if($('#FADokterV_nama_pegawai').val().length > 0)
    {
        if(jumlahKosong.length == 0)
        {
            $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/GetObatReseptur'); ?>', {id_obat:id_obat},
                function(data)
                {
                    if (jQuery.isNumeric(data.kadaluarsa)){
                        if (data.kadaluarsa == 1)
                        {
                            var obat_param = data.obatalkes_id + "-" + data.obatalkes_nama + "-" + data.obatalkes_kategori;
                            var params={
                                instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                                modul_id:<?php echo Yii::app()->session['modulId'];?>,
                                create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                                judulnotifikasi:"OBAT SUDAH KADALUARSA",
                                isinotifikasi:obat_param
                            };
                            insert_notifikasi(params);
                            alert("Obat Sudah Kadaluarsa, silahkan cek tanggal kadaluarsa !");
//                            return false;
                        }
                    }
                    
                    if (jQuery.isNumeric(data.stokObat))
                    {
                        if (data.stokObat < 1)
                        {
                            var obat_param = data.obatalkes_id + "-" + data.obatalkes_nama + "-" + data.obatalkes_kategori;
                            var params={
                                instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                                modul_id:<?php echo Yii::app()->session['modulId'];?>,
                                create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                                judulnotifikasi:"STOK OBAT KOSONG",
                                isinotifikasi:obat_param
                            };
                            insert_notifikasi(params);

                            alert("Stok Obat Kosong");
                            return false;
                        }else{
                            if(data.stokObat == data.jmlStok)
                            {
                                var obat_param = data.obatalkes_id + "-" + data.obatalkes_nama + "-" + data.obatalkes_kategori;
                                var params={
                                    instalasi_id:<?php echo Yii::app()->user->getState('instalasi_id');?>,
                                    modul_id:<?php echo Yii::app()->session['modulId'];?>,
                                    create_ruangan:<?php echo Yii::app()->user->getState('ruangan_id');?>,
                                    judulnotifikasi:"STOK OBAT MINIMUM",
                                    isinotifikasi:obat_param
                                };
                                insert_notifikasi(params);

                                alert("Stok Obat Minimum !");                
                            }
                        }
                    }
                    if($("#formRacikan #pilihRacik").is(":checked")){
                        hitungTarifService();
                    }
                    $('#tblDaftarResep').children('tbody').append(trUraian.replace());
                    if(isRacikan == 1)
                    {
                        var qty = Math.ceil($('#qtyRacik').val());
                        $('#tblDaftarResep').find('td[name$="[99][is_recipe]"]').text('');
                        $('#tblDaftarResep').find('input[name$="[99][qty]"]').attr('value', Math.ceil($('#qtyRacik').val()));
                        $('#tblDaftarResep').find('input[name$="[99][qty_a]"]').attr('value', Math.ceil($('#qtyRacik').val()));                        
                    }else{
                        var qty = Math.ceil($('#qtyNonRacik').val());
                        $('#tblDaftarResep').find('td[name$="[99][is_recipe]"]').text('R/');
                        $('#tblDaftarResep').find('input[name$="[99][qty]"]').attr('value', Math.ceil($('#qtyNonRacik').val()));
                        $('#tblDaftarResep').find('input[name$="[99][qty_a]"]').attr('value', Math.ceil($('#qtyNonRacik').val()));
                    }
                    
                    
                    $('#tblDaftarResep').find('td[name$="[99][nama_obat_alkes]"]').text(data.obatalkes_nama);
                    
                    $('#tblDaftarResep').find('select[name$="[99][satuankecil_id]"]').attr('value', data.satuankecil_id);
                    $('#tblDaftarResep').find('select[name$="[99][sumberdana_id]"]').attr('value', data.sumberdana_id);
                    $('#tblDaftarResep').find('input[name$="[99][obatalkes_id]"]').attr('value', data.obatalkes_id);
                    $('#tblDaftarResep').find('input[name$="[99][hjaResep]"]').attr('value', data.hjaresep);
                    $('#tblDaftarResep').find('input[name$="[99][hjaNonresep]"]').attr('value', data.hjanonresep);
                    $('#tblDaftarResep').find('input[name$="[99][hargasatuan_reseptur]"]').attr('value', data.hjaresep);
                    $('#tblDaftarResep').find('input[name$="[99][hargasatuanjual_oa]"]').attr('value', data.hjanonresep);
//                    $('#tblDaftarResep').find('input[name$="[99][hargasatuan_reseptur_a]"]').attr('value', data.hjanonresep);
                    $('#tblDaftarResep').find('input[name$="[99][harganetto_reseptur]"]').attr('value', data.harganetto);
//                    $('#tblDaftarResep').find('input[name$="[99][harganetto_reseptur]"]').attr('value', data.hpp);
                    $('#tblDaftarResep').find('input[name$="[99][hargajual_reseptur]"]').attr('value', data.hjanonresep);    
                    $('#tblDaftarResep').find('input[name$="[99][signa_reseptur]"]').attr('value', signa);
                    $('#tblDaftarResep').find('input[name$="[99][kekuatan_oa]"]').attr('value', $('#kekuatanObat').val());
                    $('#tblDaftarResep').find('input[name$="[99][satuankekuatan_oa]"]').attr('value', data.satuanKekuatan);
                    $('#tblDaftarResep').find('input[name$="[99][permintaan_reseptur]"]').attr('value', $('#permintaan').val());
                    $('#tblDaftarResep').find('input[name$="[99][jmlkemasan_reseptur]"]').attr('value', $('#jmlKemasanObat').val());
                    $('#tblDaftarResep').find('input[name$="[99][isRacikan]"]').attr('value', data.isRacikan);
                    $('#tblDaftarResep').find('input[name$="[99][r]"]').attr('value', 'R');
                    $('#tblDaftarResep').find('input[name$="[99][biayakemasan]"]').attr('value', data.biayaKemasan);
                    $('#tblDaftarResep').find('input[name$="[99][biayaservice]"]').attr('value', data.biayaService);
                    $('#tblDaftarResep').find('input[name$="[99][biayaadministrasi]"]').attr('value', data.biayaAdministrasi);
                    $('#tblDaftarResep').find('input[name$="[99][disc]"]').attr('value', data.diskonJual);
                    $('#tblDaftarResep').find('input[name$="[99][jasadokterresep]"]').attr('value', data.jasaDokterResep);
                    $('#tblDaftarResep').find('input[name$="[99][stok]"]').attr('value', data.stokObat);
                    if(isRacikan == 1)
                        $('#tblDaftarResep').find('input[name$="[99][racikan_id]"]').attr('value', <?php echo Params::DEFAULT_RACIKAN_ID; ?>);
                    else
                        $('#tblDaftarResep').find('input[name$="[99][racikan_id]"]').attr('value', <?php echo Params::DEFAULT_NON_RACIKAN_ID; ?>);
                    
                    renameInput('penjualanResep', 'detailreseptur_id');
                    renameInput('penjualanResep', 'r');
                    renameInput('penjualanResep', 'rke');
                    renameInput('penjualanResep', 'obatalkes_id');
                    renameInput('penjualanResep', 'hjaResep');
                    renameInput('penjualanResep', 'hjaNonresep');
                    renameInput('penjualanResep', 'hargasatuan_reseptur');
                    renameInput('penjualanResep', 'hargasatuan_reseptur_a');
                    renameInput('penjualanResep', 'hargasatuanjual_oa');
                    renameInput('penjualanResep', 'harganetto_reseptur');
                    renameInput('penjualanResep', 'qty');
                    renameInput('penjualanResep', 'qty_a');
                    renameInput('penjualanResep', 'disc');
                    renameInput('penjualanResep', 'hargajual_reseptur');
                    renameInput('penjualanResep', 'kekuatan_oa');
                    renameInput('penjualanResep', 'permintaan_reseptur');
                    renameInput('penjualanResep', 'jmlkemasan_reseptur');
                    renameInput('penjualanResep', 'satuankekuatan_oa');
                    renameInput('penjualanResep', 'satuankecil_id');
                    renameInput('penjualanResep', 'signa_reseptur');
                    renameInput('penjualanResep', 'sumberdana_id');
                    renameInput('penjualanResep', 'etiket');
                    renameInput('penjualanResep', 'isRacikan');
                    renameInput('penjualanResep', 'racikan_id');
                    renameInput('penjualanResep', 'biayakemasan');
                    renameInput('penjualanResep', 'biayaservice');
                    renameInput('penjualanResep', 'biayaadministrasi');
                    renameInput('penjualanResep', 'jasadokterresep');
                    renameInput('penjualanResep', 'stok');
                    clearFormInputan();
                    getDataRekeningObatFarmasi(id_obat,"",qty,"ap",jenis_kelompok);
                    hitungTotalSemua();
                },
                'json'
            );
        }else{
            alert("Isikan item obat terlebih dahulu");
        }
    }else{
        alert("Isi Data dokter terlebih dahulu");
    }

}
function removeObat(obj)
{
    var xxx = $(obj).parents("tr").find("td[id$='nama_obat']").text();
    if(confirm('Apakah anda akan menghapus obat ' + xxx + ' ?')){
        $(obj).parents('tr').remove();
        kurangiTarifService(obj);
        var obatalkes_id = $(obj).parents("tr").find("input[name$='[obatalkes_id]']").val();
        removeRekeningObat(obatalkes_id);
    }
    hitungTotalSemua();
}
function enableFormAlkes()
{
    clearFormInputan();
    if($("#pilihRacik").is(":checked"))
    {
        $("#formNonRacikan").attr("disabled", false);
        $("#formNonRacikan").find(".btn").attr("disabled", false);
        
        $("#formRacikan").attr("disabled", true);        
        $("#formRacikan").find(".btn").attr("disabled", true);
    }else{
        $("#formNonRacikan").attr("disabled", true);
        $("#formNonRacikan").find(".btn").attr("disabled", true);
        
        $("#formRacikan").attr("disabled", false);
        $("#formRacikan").find(".btn").attr("disabled", false);
    }
    
}

function renameInput(modelName,attributeName)
{
    var i = -1;
    $('#tblDaftarResep tr').each(function(){
        if($(this).has('input[name$="[obatalkes_id]"]').length){
            i++;
        }
        $(this).find('input[name$="[99][rke]"]').val(i+1);
        $(this).find('td[name$="[99][is_recipe]"]').attr('name','penjualanResep['+i+'][is_recipe]');
        $(this).find('td[name$="[99][nama_obat_alkes]"]').attr('name','penjualanResep['+i+'][nama_obat_alkes]');
        
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

if($('#FAPenjualanResepT_penjualanresep_id').val().length == 0){
    $("#caripasien-form").submit(
        function(){
            if($('#FADokterV_nama_pegawai').val().length > 0)
            {
                if($("#tblDaftarResep > tbody > tr").length == 0)
                {
                    alert('Data Obat masih kosong');
                }else{
                    $('.currency').each(
                        function(){
                            this.value = unformatNumber(this.value)
                        }
                    );
                    $.post('<?php echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/simpanDataPembelian'); ?>', $('#caripasien-form').serialize(),
                        function(data)
                        {
                            if(data.status == 'ok')
                            {
                                $('#tblDaftarResep').children('tbody').empty();
                                $('#frm_data_dokter').find('input').each(
                                    function(){
                                        $(this).val('');
                                    }
                                );
                                $('#frm_administrasi').find('input').each(
                                    function(){
                                        $(this).val('');
                                    }
                                );
                                $('#frm_data_resep').find('input').each(
                                    function(){
                                        for(x in data.pesan)
                                        {
                                            $('#FAPenjualanResepT_' + x).val(data.pesan[x]);
                                        }
                                    }
                                );
                                clearFormInputan();
                                alert('Data berhasil disimpan !');
                                $('#dlgConfirmasi').dialog('close');
                                // $('#print_obat').disabled(false);
                                document.getElementById("print_obat").disabled = false; 
                                $('#idPenjualan').val(data.pesan.idPenjualan);
                                // print(data.pesan.idPenjualan); //auto print faktur
                            }else{
                                alert('Data gagal disimpan !');
                            }
                        },
                        'json'
                    );

                }
            }else{
                alert('Informasi dokter masih kosong, coba cek lagi !');
            }

            return false;
        }
    );
}
function ubahHarga(obj){
    var hargaBaru = 0;
    if($(obj).val() == 0){
        hargaBaru = $(obj).parent().find('.hjaResep').val();
        $(obj).parent().find('input[name$="[hargasatuan_reseptur]"]').val(hargaBaru);
    }else if($(obj).val() == 1){
        hargaBaru = $(obj).parent().find('.hjaNonresep').val();
        $(obj).parent().find('input[name$="[hargasatuan_reseptur]"]').val(hargaBaru);
    }
}
function validasiQty(obj){
    var stok = 0;
    stok = unformatNumber($(obj).parent().parent().find('input[name$="[stok]"]').val());
    if((unformatNumber($(obj).val())) > stok){
        alert("Qty tidak boleh lebih dari stok "+stok+"!");
        $(obj).val(1);
    }
}

// untuk tambah rekening biaya konseling
function tambahRekeningKonseling(){
    var biayaKonseling = unformatNumber($('#FAPenjualanResepT_biayakonseling').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_KONSELING_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_KONSELING ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaKonseling > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaKonseling),jenis_biaya)
        },1500);
    }
}

// untuk tambah rekening biaya administrasi
function tambahRekeningAdmin(){
    var biayaAdmin = unformatNumber($('#FAPenjualanResepT_biayaadministrasi').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_ADMINISTRASI_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_ADMINISTRASI ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaAdmin > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaAdmin),jenis_biaya)
        },1500);
    }
}

// untuk tambah rekening biaya service
function tambahRekeningService(){
    var biayaService = unformatNumber($('#FAPenjualanResepT_totaltarifservice').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_SERVICE_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_SERVICE ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaService > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaService),jenis_biaya)
        },1500);
    }
}
</script>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogObat',
    'options'=>array(
        'title'=>'Pencarian Obat',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modObatDialog = new ObatalkesM('searchObatFarmasi');
$modObatDialog->unsetAttributes();
$format = new CustomFormat();
if (isset($_GET['ObatalkesM']))
    $modObatDialog->attributes = $_GET['ObatalkesM'];
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'obatAlkesDialog-m-grid',
    'dataProvider'=>$modObatDialog->searchObatFarmasi(),
    'filter'=>$modObatDialog,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectObat",
                "onClick"=>"
                            if ($(\"#formNonRacikan #pilihRacik\").is(\":checked\")){
                                $(\"#namaObatNonRacik\").val(\"$data->obatalkes_nama\");
                                $(\"#isRacikan\").val(\"0\");
                                $(\"#qtyNonRacik\").val(\"1\");
                            }
                            else if ($(\"#formRacikan #pilihRacik\").is(\":checked\")){
                                $(\"#namaObatRacik\").val(\"$data->obatalkes_nama\");
                                $(\"#isRacikan\").val(\"1\");
                            }
                            $(\"#idObat\").val(\"$data->obatalkes_id\");
                            $(\"#hjaresep\").val(\"$data->hjaresep\");
                            $(\"#jnskelompok\").val(\"$data->jnskelompok\");

                            $(\"#dialogObat\").dialog(\"close\");
                            return false;
                ",
               ))'
        ),

        'obatalkes_kode',
        'obatalkes_nama',
        array(
            'name'=>'tglkadaluarsa',
            'filter'=>'',
        ),
        
        array(
          'name'=>'satuankecil.satuankecil_nama',
            'header'=>'Satuan Kecil',
        ),
        array(
            'name'=>'satuanbesar.satuanbesar_nama',
            'header'=>'Satuan Besar',
        ),
        array(
            'header'=>'HJA Resep',
            'type'=>'raw',
            'value'=>'number_format($data->hjaresep, 2, ",", ".")',
            'filter'=>'',
            'htmlOptions'=>array('style'=>'text-align:right;'),
        ),
        array(
            'header'=>'HJA Non Resep',
            'value'=>'number_format($data->hjanonresep, 2, ",", ".")',
            'filter'=>'',
            'htmlOptions'=>array('style'=>'text-align:right;'),
        ),
        
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>