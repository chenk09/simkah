<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'',
        'thousands'=>'',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
$this->widget('bootstrap.widgets.BootAlert');

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'returpenjualan-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#FAReturresepT_alasanretur',
        'method'=>'post',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 
            'onsubmit'=>'return unformatSemuaNumber(); return validasiForm();'),
//            'onsubmit'=>'return unformatSemuaNumber(); return validasiForm();'),
//        'onClick'=>'cekHakRetur();return false;'
));
?>
<?php echo $form->errorSummary($modRetur);?>
<fieldset>
    <legend class="rim">Retur Penjualan</legend>
    <table class="table-condensed">
        <tr>
            <td>
                <?php echo $form->textFieldRow($infoJualObat[0],'tglpenjualan',array('class'=>'span3','readonly'=>true)); ?>
                <?php echo $form->textFieldRow($infoJualObat[0],'tglresep',array('class'=>'span3','readonly'=>true)); ?>
                <?php echo $form->textFieldRow($infoJualObat[0],'noresep',array('class'=>'span3','style'=>'width:275px','readonly'=>true)); ?>
                 <?php echo $form->textFieldRow($infoJualObat[0],'pendaftaran_id',array('class'=>'span3','style'=>'width:275px','readonly'=>true)); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($infoJualObat[0],'no_rekam_medik',array('class'=>'span3','readonly'=>true)); ?>
                <?php echo $form->textFieldRow($infoJualObat[0],'nama_pasien',array('class'=>'span3','readonly'=>true)); ?>
                <div class="control-group ">
                    <label for="FAInformasipenjualanapotikV_jeniskelamin" class="control-label">Jeniskelamin</label>
                    <div class="controls">
                        <?php //echo $form->textField($infoJualObat[0],'umur',array('class'=>'span2','readonly'=>true)); ?> 
                        <?php echo $form->textField($infoJualObat[0],'jeniskelamin',array('class'=>'span2','readonly'=>true)); ?>
                    </div>
                </div>
                <div class="control-group ">
                    <label class="control-label">Cara Bayar/Penjamin</label>
                    <div class="controls">
                        <?php echo $form->textField($infoJualObat[0],'carabayar_nama',array('class'=>'span2', 'style'=>'width:80px;','readonly'=>true)); ?>
                        <?php echo $form->textField($infoJualObat[0],'penjamin_nama',array('class'=>'span2','readonly'=>true)); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset>

<table>
    <tr>
        <td>
            <?php echo $form->hiddenField($modRetur,'pasien_id',array('value'=>$infoJualObat[0]->pasien_id,'class'=>'span3','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <div class="control-group ">
                <?php //echo $form->textFieldRow($modRetur,'tglretur',array('class'=>'span3','readonly'=>true)); ?>
                <?php echo $form->labelEx($modRetur,'tglretur', array('class'=>'control-label required')) ?>
                <div class="controls">  
                 <?php $this->widget('MyDateTimePicker',array(
                                     'model'=>$modRetur,
                                     'attribute'=>'tglretur',
                                     'mode'=>'datetime',
                                     'options'=> array(
                                         'maxDate'=>'d',
                                         'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                     'htmlOptions'=>array('readonly'=>true,
                                     'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); ?>
                </div> 
            </div>
            <?php echo $form->textAreaRow($modRetur,'alasanretur',array('class'=>'span3 required','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($modRetur,'noreturresep',array('class'=>'span3','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </td>
        <td>
            <?php echo $form->textAreaRow($modRetur,'keteranganretur',array('class'=>'span3','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->dropDownListRow($modRetur,'pegretur_id', CHtml::listData(PegawairuanganV::model()->findAllByAttributes(array('ruangan_id'=>Yii::app()->user->getState('ruangan_id')), array('order'=>'nama_pegawai')), 'pegawai_id', 'nama_pegawai'),array('empty'=>'-- Pilih --','class'=>'span3 required','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->dropDownListRow($modRetur,'mengetahui_id', CHtml::listData(DokterpegawaiV::model()->findAll(array('order'=>'nama_pegawai')), 'pegawai_id', 'namaLengkap'),array('empty'=>'-- Pilih --','class'=>'span3','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </td>
    </tr>
</table>
<div id="divTabelRetur">
        <?php echo $form->errorSummary($modReturDetail); ?>
        <?php $this->renderPartial('_tblReturPenjualan',array('infoJualObat'=>$infoJualObat,'modPenjualanResep'=>$modPenjualanResep, 'modObatAlkesPasien'=>$modObatAlkesPasien,'modReturDetail'=>$modReturDetail,'modRetur'=>$modRetur)); ?>
    </div>
<?php
//FORM REKENING
//    $this->renderPartial('rawatJalan.views.tindakan.rekening._formRekening',
//        array(
//            'form'=>$form,
//            'modRekenings'=>$modRekenings,
//        )
//    );
//FORM REKENING
    $this->renderPartial('farmasiApotek.views.informasiPenjualanResep.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
<div class="form-actions">
    
            <?php 
				if($modRetur->isNewRecord)
				{
					if($jumlah_return < 2)
					{
						echo CHtml::htmlButton(
							Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
							array(
								'class'=>'btn btn-primary',
								'type'=>'submit',
								'onClick'=>'submitForm();return false;',
								'onKeypress'=>'return formSubmit(this,event)'
							)
						); 
					}
				}else{
					echo "<script>setTimeout('print(\"PRINT\");',1000);</script>";
					echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'disabled'=>false,'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";                 
				}
				echo CHtml::link(
					Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
					Yii::app()->createUrl('farmasiApotek/'.$this->id.'/returPenjualan',
						array(
							'idPenjualanResep'=>$modRetur->penjualanresep_id
						)
					),
					array(
						'class'=>'btn btn-danger',
						'type'=>'reset'
					)
				);
            ?>
    
		<?php 
//           $content = $this->renderPartial('../tips/tips',array(),true);
//			$this->widget('TipsMasterData',array('type'=>'admin','content'=>$content));
        ?>
			
</div>
<!-- DIKOMEN KARENA MUNCUL GARIS MERAH PADAHAL TDK ADA ERROR -> <div id="errorMessage" class="errorSummary"></div> -->

<?php $this->endWidget(); ?>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Data Retur Penjualan',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>850,
                'height'=>550,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Transaksi</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Tgl. Penjualan</td>
                    <td width="250" tag="tgl_penjualan"></td>
                    <td width="150">No Rekam Medik</td>
                    <td tag="no_rekam_medik"></td>
                </tr>
                <tr>
                    <td>Tgl. Resep</td>
                    <td tag="tgl_resep"></td>
                    <td>Nama Pasien</td>
                    <td tag="nama_pasien"></td>
                </tr>
                <tr>
                    <td width="150">No. Resep</td>
                    <td width="250" tag="noresep"></td>
                    <td width="150">Jenis Kelamin</td>
                    <td tag="jeniskelamin"></td>
                </tr>
                <tr>
                    <td>Pendaftaran</td>
                    <td tag="no_pendaftaran"></td>
                    <td>Cara Bayar / Penjamin</td>
                    <td tag="bayar_penjamin"></td>
                </tr>
                <tr>
                    <td>Tgl. Retur</td>
                    <td tag="tgl_retur"></td>
                    <td>Keterangan Retur</td>
                    <td tag="ket_retur"></td>
                </tr>
                <tr>
                    <td>Alasan Retur</td>
                    <td tag="alasan_retur"></td>
                    <td>Pegawai Retur</td>
                    <td tag="peg_retur"></td>
                </tr>
                <tr>
                    <td>No. Retur</td>
                    <td tag="noretur"></td>
                    <td>Mengetahui</td>
                    <td tag="meng_retur"></td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Obat Alkes</legend>
            <table id="info_tindakan_temp" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="50">Obat</th>
                        <th width="50">Harga Satuan</th>
                        <th width="50">Qty Jual</th>
                        <th width="50">Qty Retur</th>
                        <th width="300">Satuan</th>
                        <th width="50">Kondisi Obat</th>
                        <th width="150">Sub Total Retur</th>
                    </tr>                    
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Total Pembayaran Retur</legend>
            <table id="info_pembayaran_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Total</td>
                    <td width="250" tag="total" class="curency"></td>
                    <td width="150">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                </tr>
                <tr>
                    <td width="150">Biaya Administrasi + Tarif Service + Konseling + Jasa Dokter</td>
                    <td width="250" tag="biaya_adm" class="curency"></td>
                    <td width="150">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                </tr>
                <tr>
                    <td width="150">Total Retur</td>
                    <td width="250" tag="total_retur" class="curency"></td>
                    <td width="150">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                </tr>
            </table>
        </fieldset>        
    </div>  
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses(this);return false;',
                        'disabled'=>FALSE
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
<script type="text/javascript">
//$('.currency').each(function(){this.value = formatDesimal(this.value)});

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#returpenjualan-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
    $('#returpenjualan-form').submit();
}   
function submitForm()
{
    if(cekInputTindakan())
    {
        confirmPembayaran();
    }
}

function cekInputTindakan()
{
    jumlahPilihan = $(".isRetur:checked").length;
    if (jumlahPilihan < 1){
        alert('Tidak ada Obat Alkes yang akan diretur !');
        return false;
    }
    
    var kosong = "";
    var Req = $('#returpenjualan-form').find('.required[value='+kosong+']');
    var jumReq = Req.length;
    if(jumReq > 0){
        alert("Silahkan isi yang bertanda * !");
        return false;
    }else{
        return true;
    }
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    var no_pendaftaran = $('#returpenjualan-form').find('input[name$="[no_pendaftaran]"]').val();
    var no_rekam_medik = $('#returpenjualan-form').find('input[name$="[no_rekam_medik]"]').val();
    var instalasi_nama = $('#returpenjualan-form').find('input[name$="[instalasi_nama]"]').val();
    var nama_pasien = $('#returpenjualan-form').find('input[name$="[nama_pasien]"]').val();
    var ruangan_nama = $('#returpenjualan-form').find('input[name$="[ruangan_nama]"]').val();
    var carabayar_nama = $('#returpenjualan-form').find('input[name$="[carabayar_nama]"]').val();
    var penjamin_nama = $('#returpenjualan-form').find('input[name$="[penjamin_nama]"]').val();
    var jeniskelamin = $('#returpenjualan-form').find('input[name$="[jeniskelamin]"]').val();
    bayar_penjamin = carabayar_nama+'/'+penjamin_nama;
    
    var tgl_retur = $('#returpenjualan-form').find('input[name$="[tglretur]"]').val();
    var alasan_retur = $('#returpenjualan-form').find('input[name$="[alasanretur]"]').val();
    var noretur = $('#returpenjualan-form').find('input[name$="[noreturresep]"]').val();
    var ket_retur = $('#returpenjualan-form').find('input[name$="[keteranganretur]"]').val();
    var peg_retur = $('#returpenjualan-form').find('select[name$="[pegretur_id]"] option:selected').text();
    var meng_retur = $('#returpenjualan-form').find('select[name$="[mengetahui_id]"] option:selected').text();
    var tgl_resep = $('#returpenjualan-form').find('input[name$="[tglresep]"]').val();
    var tgl_penjualan = $('#returpenjualan-form').find('input[name$="[tglpenjualan]"]').val();
    var noresep = $('#returpenjualan-form').find('input[name$="[noresep]"]').val();
    
    var total = unformatNumber($('#returpenjualan-form').find('#total').val());
    var biaya_adm = unformatNumber($('#returpenjualan-form').find('#biayaAdministrasi').val());
    var total_retur = unformatNumber($('#returpenjualan-form').find('input[name$="[totalretur]"]').val());
    
    var data_pembayaran = {
        'total':total,
        'biaya_adm':biaya_adm,
        'total_retur':total_retur
    }
    $('#content_confirm').find('#info_pembayaran_temp td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]);
                }
            }
        }
    );    
        
    var data_pasien = {
        'no_pendaftaran':no_pendaftaran,
        'no_rekam_medik':no_rekam_medik,
        'instalasi_nama':instalasi_nama,
        'nama_pasien':nama_pasien,
        'ruangan_nama':ruangan_nama,
        'tgl_resep':tgl_resep,
        'tgl_penjualan':tgl_penjualan,
        'noresep':noresep,
        'bayar_penjamin':bayar_penjamin,
        'tgl_retur':tgl_retur,
        'alasan_retur':alasan_retur,
        'noretur':noretur,
        'ket_retur':ket_retur,
        'peg_retur':peg_retur,
        'meng_retur':meng_retur,
        'jeniskelamin':jeniskelamin
    }
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]);
                }
            }
        }
    );
    var tr_info = "";
    var total_hrg_obat = 0;
    var jml = 1;
    $('#tblReturResepDetail').find('tbody tr').each(
        function()
        {
            if($(this).attr('class') != 'trfooter')
            {
                var nama_obat = $(this).find('td input[name$="[obatalkes_id]"]').parent().text();
                var qty_oa = $(this).find('#qty_oa').val();
                var qty_retur = $(this).find('td input[name$="[qty_retur]"]').val();
                var harga_satuan = $(this).find('td input[name$="[hargasatuan]"]').val();
                var kondisi_brg = $(this).find('td input[name$="[kondisibrg]"]').val();
                var satuan = $(this).find('td input[name$="[satuankecil_id]"]').text();
                var subtotal = $(this).find('#subtotal').val();
                                
                total = harga_satuan * qty_retur;
                tr_info += '<tr>';
                tr_info += '<td>'+ nama_obat+'-'+jml+'</td><td class="curency">'+ harga_satuan +'</td><td>'+ qty_oa +'</td><td>'+ qty_retur + '</td><td>'+ satuan +'</td><td>'+ kondisi_brg +'</td><td class="curency">'+ subtotal +'</td>';
                tr_info += '</tr>';
                total_hrg_obat += parseFloat(total);
            }
            jml++;
        }
        
    );
    
    $('#info_tindakan_temp').find('tbody').empty();
//    tr_info += '<tr class="trfooter">';
//    tr_info += '<td colspan="6">Total</td><td class="curency">'+ total_hrg_obat +'</td>';
//    tr_info += '</tr>';
    $('#info_tindakan_temp').find('tbody').append(tr_info);
    
    $('#content_confirm').find('.curency').each(
        function()
        {
//            var result = formatNumber(parseInt($(this).text()));
//          JANGAN DIBULATKAN >>  var result = formatMoney($(this).text());
            var result = accounting.formatMoney($(this).text(),'',2,',','.');
            $(this).text(result);            
        }
    );
}
function cekLogin()
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLogin',array('task'=>'Retur'));?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
            alert(data.error);
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
            //alert(data.status);
            $('#<?php echo CHtml::activeId($modRetur, 'pegretur_id'); ?>').val(data.userid);
            $('#loginDialog').dialog('close');
            return true;
        }else{
            alert(data.status);
        }
    }, 'json');
}

function unformatSemuaNumber(){
    $('#tblReturResepDetail tbody').find('tr').each(
            function(){
                $(this).find('.harga').val(unformatNumber($(this).find('.harga').val()));
                $(this).find('.qty').val(unformatNumber($(this).find('.qty').val()));
                $(this).find('.subtotal').val(unformatNumber($(this).find('.subtotal').val()));                
            }
        );
    $('#tblReturResepDetail tfoot').find('tr').each(
            function(){
                $(this).find('.total').val(unformatNumber($(this).find('.total').val()));
                $(this).find('.totalAdmin').val(unformatNumber($(this).find('.totalAdmin').val()));
                $(this).find('.totalRetur').val(unformatNumber($(this).find('.totalRetur').val()));
            }
        );
}
function validasiForm(){
    var kosong = "";
    var Req = $('#returpenjualan-form').find('.required[value='+kosong+']');
    var jumReq = Req.length;
    if(jumReq > 0){
        alert("Silahkan isi yang bertanda * !");
        return false;
    }else{
        return true;
    }
    
}
function loadDataRekening(){
    $("#tblReturResepDetail > tbody > tr").each(function(){
        var obatalkes_id = $(this).find("input[name$='[obatalkes_id]']").val();
        var qty = $(this).find("input[name$='[qty_retur]']").val();
        getDataRekening(obatalkes_id, "", qty, "rr");
    });
    
}
</script>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), '#', array('class'=>'btn btn-danger','onclick'=>"$('#loginDialog').dialog('close');return false",'disabled'=>false)); ?>
    </div> 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>

<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/informasiPenjualanResep/printStrukRetur&id='.$modRetur->returresep_id);
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>