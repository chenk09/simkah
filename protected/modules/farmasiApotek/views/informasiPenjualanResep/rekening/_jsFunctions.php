<?php
/**
 * KHUSUS UNTUK MENYIMPAN FUNGSI-FUNGSI JAVASCRIPT
 */
?>
<?php 
    $modNewRekenings = array();
    $modNewRekenings[0] = new RekeningakuntansiV;
?>
<script type="text/javascript">
    
    //rekening dihapus jika biaya di ganti
    function removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya){
        $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[jenis_biaya]"]').val() == jenis_biaya){
                $(this).detach();
            }
        });
    }
    
    function updateRekeningObatApotek(obatalkes_id,saldo,saldoTindakan){
        $("#tblInputRekening > tbody > tr").each(function(){
            var daftartindakan_id = $(this).find('input[name$="[daftartindakan_id]"]').val()
                if(daftartindakan_id == <?php echo PARAMS::DEFAULT_OBAT_BEBAS_JURNAL; ?> || 
                        daftartindakan_id == <?php echo PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_RUANGAN; ?> ||
                        daftartindakan_id == <?php echo PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA; ?> || 
                        daftartindakan_id == <?php echo PARAMS::DEFAULT_OBAT_BEBAS_JURNAL_OA2 ; ?> ||
                        daftartindakan_id == <?php echo PARAMS::DEFAULT_BEBAN_BAHAN_AL ; ?> ||
                        daftartindakan_id == <?php echo PARAMS::DEFAULT_BEBAN_BAHAN_GM ; ?>
                ){
                    saldoTotal = saldoTindakan;
                }else{
                    saldoTotal = saldo;
                }
            if($(this).find('input[name$="[obatalkes_id]"]').val() == obatalkes_id){
                if($(this).find('input[name$="[saldonormal]"]').val() == "D"){
                    $(this).find('input[name$="[saldodebit]"]').val(saldoTotal);
                }else{
                    $(this).find('input[name$="[saldokredit]"]').val(saldoTotal);
                }
            }
        });
    }
    // untuk update rekening biaya Administrasi, Konseling, Service
    function updateRekeningBiayaFarmasi(obatalkes_id, saldo,jenis_biaya){
        $("#tblInputRekening > tbody > tr").each(function(){
            if($(this).find('input[name$="[jenis_biaya]"]').val() == jenis_biaya){
                if($(this).find('input[name$="[saldonormal]"]').val() == "D"){
                    $(this).find('input[name$="[saldodebit]"]').val(saldo);
                }else{
                    $(this).find('input[name$="[saldokredit]"]').val(saldo);
                }
            }
        });
    }
    
    //getDataRekening untuk load data rekening berdasarkan pelayanan yang ditambahkan
    function getDataRekeningBiayaFarmasi(daftartindakan_id, kelaspelayanan_id, qty, jnspelayanan, jenis_biaya){
        $("#formJurnalRekening").addClass("srbacLoading");
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningBiayaFarmasi');?>', {daftartindakan_id:daftartindakan_id, kelaspelayanan_id:kelaspelayanan_id, qty:qty, jnspelayanan:jnspelayanan, jenis_biaya:jenis_biaya},
            function(data){
                if(data == null){
                    $("#formJurnalRekening").removeClass("srbacLoading");  
                }else{
                    $("#tblInputRekening > tbody").append(data.replace());
                    $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                    $("#tblInputRekening > tbody").find('.uncurrency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
                    //uncurrency agar tidak duakali maskMoney krn bisa error input
                    $("#tblInputRekening > tbody").find('.uncurrency').addClass('currency');
                    $("#tblInputRekening > tbody").find('.currency').removeClass('uncurrency');
//                    setTimeout(function(){renameRowRekening()},1000);
                    renameRowRekening();
                }
              $("#formJurnalRekening").removeClass("srbacLoading");  
            }, "json");
            return false;
    }
    
    //getDataRekening untuk load data rekening berdasarkan pelayanan yang ditambahkan
    function getDataRekeningObatFarmasi(daftartindakan_id, kelaspelayanan_id, qty, jnspelayanan, jenis_kelompok){
        $("#formJurnalRekening").addClass("srbacLoading");
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/GetDataRekeningObatFarmasi');?>', {daftartindakan_id:daftartindakan_id, kelaspelayanan_id:kelaspelayanan_id, qty:qty, jnspelayanan:jnspelayanan,jenis_kelompok:jenis_kelompok},
            function(data){
                if(data == null){
                    $("#formJurnalRekening").removeClass("srbacLoading");  
                }else{
                    $("#tblInputRekening > tbody").append(data.replace());
                    $("#tblInputRekening > tbody").find('input[name$="[rekDebitKredit]"]').autocomplete({'showAnim':'fold','minLength':2,'focus':function( event, ui ) {
                                                                                        $(this).val("");
                                                                                        return false;
                                                                                    },'select':function( event, ui ) {
                                                                                        $(this).val(ui.item.value);
                                                                                        var data = {
                                                                                            rincianobyek_id:ui.item.rincianobyek_id,
                                                                                            obyek_id:ui.item.obyek_id,
                                                                                            jenis_id:ui.item.jenis_id,
                                                                                            kelompok_id:ui.item.kelompok_id,
                                                                                            struktur_id:ui.item.struktur_id,
                                                                                            nmrincianobyek:ui.item.nmrincianobyek,
                                                                                            kdstruktur:ui.item.kdstruktur,
                                                                                            kdkelompok:ui.item.kdkelompok,
                                                                                            kdjenis:ui.item.kdjenis,
                                                                                            kdobyek:ui.item.kdobyek,
                                                                                            kdrincianobyek:ui.item.kdrincianobyek,
                                                                                            saldodebit:ui.item.saldodebit,
                                                                                            saldokredit:ui.item.saldokredit,
                                                                                            status:"debit"
                                                                                        };
                                                                                        var row = $(this).parents("tr").find("#row").val();
                                                                                        editDataRekeningFromGrid(data, row);
                                                                                        return false;
                                                                                    },'source':function(request, response) {
                                                                                                    $.ajax({
                                                                                                        url: "<?php echo Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null));?>",
                                                                                                        dataType: "json",
                                                                                                        data: {
                                                                                                            term: request.term,
                                                                                                        },
                                                                                                        success: function (data) {
                                                                                                            response(data);
                                                                                                        }
                                                                                                    })
                                                                                                }
                                                                                    });  
                    $("#tblInputRekening > tbody").find('.uncurrency').maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
                    //uncurrency agar tidak duakali maskMoney krn bisa error input
                    $("#tblInputRekening > tbody").find('.uncurrency').addClass('currency');
                    $("#tblInputRekening > tbody").find('.currency').removeClass('uncurrency');
//                    setTimeout(function(){renameRowRekening()},1000);
                    renameRowRekening();
                }
              $("#formJurnalRekening").removeClass("srbacLoading");  
            }, "json");
            return false;
    }
</script>
<?php 
Yii::app()->clientScript->registerScript('renameOnload', "renameRowRekening();", CClientScript::POS_READY);
?>