<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No</th>
            <th>Uraian</th>
            <th>Qty</th>
            <th>Qty Retur</th>
            <th>Harga Satuan</th>
            <th>Diskon</th>
            <th>Sub Total</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $totQty = 0; $totHargaSatuan = 0; $toHarga = $totHargaDiskon = 0; $toHargaRet = 0;
        $totBiayaAdministrasi = 0;
        $totalpersenhargajual = ($modPenjualanResep->jenispenjualan != "PENJUALAN BEBAS") ? Params::konfigFarmasi('totalpersenhargajual') :  Params::konfigFarmasi('persjualbebas');
        $persenhargajual = Params::konfigFarmasi('persehargajual');
        $biayaLainLain = 0;
        $totalSubtotal = 0;
        if (count($modReturDetails) > 0){
        foreach ($modReturDetails as $i => $detail) { 
            $obatpasien = ObatalkespasienT::model()->findByAttributes(array('obatalkespasien_id'=>$detail->obatalkespasien_id));
            $detailqty = $detail->qty_retur;
            $totQty = $totQty + $detail->qty_retur;
            $totalpersen = ($totalpersenhargajual > 0) ? $totalpersenhargajual / 100 : 0;
            $totalppnharga = ($persenhargajual > 0) ? (100 + $persenhargajual) / 100 : 0 ;
            $totHargaSatuan = $totHargaSatuan + $detail->hargasatuan;
            $diskon = $obatpasien->discount*$detail->hargasatuan;
            $diskon = ($diskon > 0) ? $diskon/100 : 0;
            $biayaservice = $obatpasien->biayaservice;
            $biayaLainLain += ($obatpasien->biayaadministrasi+$obatpasien->biayakonseling+$obatpasien->biayaservice); //$obatpasien->jasadokterresep+ << SDH TERMASUK DLM HARGA OBAT
            $toHarga = $toHarga + ($detail->qty_retur * ($detail->hargasatuan-$diskon));
            $subTotalHargaRetur = $detail->qty_retur * ($detail->hargasatuan - $diskon);
            $toHargaRet = ceil((($detail->hargasatuan / $totalpersen / $totalppnharga) * $detail->qty_retur) - $diskon);
            $totHargaDiskon += $diskon;
            $totalSubtotal += $subTotalHargaRetur;
        ?>
        <tr>
            <td><?php echo ($i+1); ?></td>
            <td>
                <?php // echo CHtml::activeHiddenField($modReturDetails[$i], "obatalkes_id",array("readonly"=>true,"class"=>"span1 number")); ?>
                <?php echo CHtml::activeHiddenField($detail, "[$i]obatalkespasien_id",array("readonly"=>true,"class"=>"span1")); ?>
                <?php echo $obatpasien->obatalkes->obatalkes_nama; ?>
                <?php  echo CHtml::hiddenField("returdetail[$i][sumberdana_id]",$obatpasien->sumberdana_id,array("readonly"=>true,"class"=>"span1 number")); ?>
                <?php  echo CHtml::activeHiddenField($detail,"[$i]satuankecil_id",array("readonly"=>true,"class"=>"span1 number")); ?></td>
<!--            <td style="text-align: right;"><?php //echo $obatpasien->qty_oa; ?></td>-->
            <td style="text-align: right;"><?php echo CHtml::TextField('qty',MyFunction::formatNumber($detail->qty_retur),array("class"=>"inputFormTabel number lebar2 qty2",'disabled'=>'disabled', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td style="text-align: right;"><?php echo CHtml::activeTextField($detail,"[$i]qty_retur",array("class"=>"inputFormTabel number lebar2",'onkeyup'=>'hitungQtyRetur(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td style="text-align: right;"><?php /*echo MyFunction::formatNumber($detail->hargasatuan_oa);*/ echo CHtml::activeTextField($detail,"[$i]hargasatuan", array("readonly"=>true,"class"=>"inputFormTabel currency lebar2", 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td style="text-align: right;"><?php echo CHtml::textField('diskon',$diskon,array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
            <td style="text-align: right;"><?php /*echo MyFunction::formatNumber($detail->hargajual_oa);*/ echo CHtml::textField("subtotalhargaretur",$subTotalHargaRetur,array("readonly"=>true,"class"=>"inputFormTabel currency lebar2",'onblur'=>'hitungQtyRetur(this);')); ?></td>
        </tr>
        <?php } 
        }else{
            echo '<tr><td colspan=8><i>Data Obat Alkes tidak ditemukan</></td></tr>';
        }
        $toHarga = $toHarga+$biayaLainLain;
        $toHargaRet = $toHargaRet + $biayaLainLain;
        $totHargaRetur = $toHarga - $totBiayaAdministrasi;
        $totBiayaAdministrasi = ceil($toHarga-(($toHarga/($totalpersenhargajual/100))/(($persenhargajual+100)/100))+$biayaLainLain-$obatpasien->biayaservice);
        $toHargaRet = $toHargaRet ;
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2" style="text-align: right;"><b>Total</b></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalQty",MyFunction::formatNumber($totQty,1),array("readonly"=>true,"class"=>"inputFormTabel number lebar2")); //echo MyFunction::formatNumber($totQty,1); ?></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalQtyRetur",MyFunction::formatNumber($totQty,1),array("readonly"=>true,"class"=>"inputFormTabel number lebar2")); ?></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalHargaSatuan",$totHargaSatuan,array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalHargaDiskon",$totHargaDiskon,array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalHargaReturOa",$totalSubtotal /*$toHarga*/,array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: right;"><b>Biaya Administrasi, dll.</b></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totBiayaAdministrasi",($biayaLainLain),array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: right;"><b>Total Harga Retur</b></td>
            <td style="text-align: right;"><?php echo CHtml::textField("totalHargaRetur",($totalSubtotal + $biayaLainLain),array("readonly"=>true,"class"=>"inputFormTabel currency lebar2")); ?></td>
        </tr>
    </tfoot>
</table>
<?php
$js = <<< JS
function hitungQtyRetur(obj)
{
    var totQty = 0; 
    var subTotalRetur = 0;
    var totHargaretur = 0;
    var totalpersenhargajual = ${totalpersenhargajual};
    var ppnbarang = ${persenhargajual};
    var biayaservice = ${biayaservice};
    var detailqty = ${detailqty};
    var totalbiayaadministrasi = 0;
    var biayaLainLain = ${biayaLainLain};

    $(obj).parents('table').find('input[name$="[qty_retur]"]').each(function(){
        qty = unformatNumber(this.value);
        qty2 =  unformatNumber($(this).parents('tr').find('.qty2').val());
        hargaSatuan = unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val());
        hargatotal = unformatNumber($(this).parents('tr').find('input[name$="[subtotalhargaretur]"]').val());
        diskon = unformatNumber($(this).parents('tr').find('input[name$="diskon"]').val());
        totQty = totQty + qty;
        subTotalRetur = qty * (hargaSatuan-diskon);
        totHargaretur += subTotalRetur;
        if (jQuery.isNumeric(qty)){
                    if (qty > qty2){
                        alert('Jumlah Qty Retur tidak boleh lebih dari Jumlah Qty');
                        $(this).parents('tr').find('input[name$="[qty_retur]"]').val(qty2);
                        return false;
                        totQty = totQty + detailqty;
                    }
                    hitungQtyRetur();
                }
        $(this).parents('tr').find('input[name$="subtotalhargaretur"]').val(unformatNumber((subTotalRetur)));
        
    });
    
    totHargaretur = totHargaretur+biayaLainLain;
    totalretur = (qty * (hargaSatuan /(totalpersenhargajual/100)/((ppnbarang+100)/100)) + biayaLainLain);
//    totalbiayaadministrasi2 = (totHargaretur > 0 ) ? totHargaretur-((totHargaretur/(totalpersenhargajual/100))/((ppnbarang+100)/100))+biayaLainLain-biayaservice : 0;
    totHargaRetur2 = totHargaretur - totalretur;
    totalbiayaadministrasi = (totHargaretur > 0 ) ? totHargaretur-((totHargaretur/(totalpersenhargajual/100))/((ppnbarang+100)/100))+biayaLainLain-biayaservice: 0;
      
    if (jQuery.isNumeric(totalbiayaadministrasi))
        $("#totBiayaAdministrasi").val(Math.ceil(totalbiayaadministrasi));
    if (jQuery.isNumeric(totQty))
        $('#totalQtyRetur').val(formatNumber(totQty));
    if (jQuery.isNumeric(totHargaretur))
        $('#totalHargaReturOa').val(unformatNumber(totHargaretur));
    if (jQuery.isNumeric(totHargaRetur2))
        $('#totalHargaRetur').val(Math.ceil(totHargaRetur2));
}

JS;

Yii::app()->clientScript->registerScript('nyobain javascript kk', $js, CClientScript::POS_HEAD);
?>
