<?php
Yii::app()->clientScript->registerScript('cariPasien', "
$('#cari-pasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});");
?>
<fieldset>
    <legend class="rim"><?php echo  Yii::t('mds','Search Patient') ?></legend>
	<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
		'id'=>'cari-pasien-form',
		'enableAjaxValidation'=>false,
		'type'=>'horizontal',
		'focus'=>'#',
		'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
	));	?>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));?>
                    <?php echo $form->labelEx($model,'tglAwal', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAwal',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
								'class'=>'dtPicker3',
								'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        ));?>
                    </div>
                </div>
                <div class="control-group">
                    <?php $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($model,'tglAkhir', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
							'attribute'=>'tglAkhir',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array(
								'class'=>'dtPicker3',
								'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
    <div class="form-actions">
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
		<?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
</fieldset>
<?php $this->endWidget(); ?>
