<?php $this->widget('bootstrap.widgets.BootMenu', array(
	'type'=>'tabs',
	'stacked'=>false,
	'items'=>array(
		array(
			'label'=>'Pasien Rawat Jalan',
			'url'=>array('index'),
			'active'=>($status == 'RJ' ? true : false)
		),
		array(
			'label'=>'Pasien Rawat Inap',
			'url'=>array('rawatInap'),
			'active'=>($status == 'RI' ? true : false)
		),
		array(
			'label'=>'Pasien Rawat Darurat',
			'url'=>array('rawatDarurat'),
			'active'=>($status == 'RD' ? true : false)
		),
	),
));?>