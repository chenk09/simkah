<?php
$this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$data,
	'template'=>"{pager}{summary}\n{items}",
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
			array(
				'header'=>'Tgl Pendaftaran',
				'name'=>'tgl_pendaftaran',
				'type'=>'raw',
				'value'=>'$data->tgl_pendaftaran'
			),
			array(
				'header'=>'Nama Instalasi',
				'name'=>'instalasi_nama',
				'type'=>'raw',
				'value'=>'$data->instalasi_nama',
			),
			array(
				'name'=>'no_pendaftaran',
				'type'=>'raw',
				'value'=>'$data->no_pendaftaran',
			),
			array(
				'name'=>'no_rekam_medik',
				'type'=>'raw',
				'value'=>'$data->no_rekam_medik',
			),
			array(
				'name'=>'nama_pasien',
				'type'=>'raw',
				'value'=>'$data->nama_pasien',
			),
			array(
				'name'=>'nama_bin',
				'type'=>'raw',
				'value'=>'$data->nama_bin',
			),
			array(
				'header'=>'Cara Bayar',
				'name'=>'carabayar_nama',
				'type'=>'raw',
				'value'=>'$data->carabayar_nama',
			),
			array(
				'header'=>'Nama Penjamin',
				'name'=>'penjamin_nama',
				'type'=>'raw',
				'value'=>'$data->penjamin_nama',
			),
			array(
				'header'=>'Nama Jenis Kasus Penyakit',
				'name'=>'jeniskasuspenyakit_nama',
				'type'=>'raw',
				'value'=>'$data->jeniskasuspenyakit_nama',
			),
			array(
				'name'=>'umur',
				'type'=>'raw',
				'value'=>'$data->umur',
			),
			array(
				'name'=>'alamat_pasien',
				'type'=>'raw',
				'value'=>'$data->alamat_pasien',
			),
			array(
				'header'=>'Total Tagihan',
				'type'=>'raw',
				'htmlOptions'=>array(
					"style"=>"text-align:center;"
				),
				'value'=>array($this,'totalTagihan')
			),
			array(
				'header'=>'Rincian',
				'type'=>'raw',
				'htmlOptions'=>array(
					"style"=>"text-align:center;"
				),
				'value'=>array($this,'rincianTagihan')
			),
			array(
				'header'=>'Konfirmasi',
				'type'=>'raw',
				'htmlOptions'=>array(
					"style"=>"text-align:center;"
				),
				'value'=>array($this,'approveFarmasi')
			),
		),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" ></iframe>
<?php $this->endWidget();?>
<?php Yii::app()->clientScript->registerScript('ajax_set_status', "
function ubahStatusFarmasi(idpendaftaran, status){
    var idpendaftaran = idpendaftaran;
    var answer = confirm('Yakin Akan Verifikasi Status Farmasi Pasien?');
    if (answer){
		$.post('". Yii::app()->createUrl('ActionAjaxRIRD/UbahStatusFarmasi') ."', {idpendaftaran:idpendaftaran,status:status}, function(data){
			setTimeout(function(){
				$.fn.yiiGridView.update('pencarianpasien-grid');
			}, 500);
		},'json');
    }
	return false;
}", CClientScript::POS_BEGIN);?>