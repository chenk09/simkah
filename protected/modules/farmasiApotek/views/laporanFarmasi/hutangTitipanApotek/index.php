<?

Yii::app()->clientScript->registerScript('search', "
$('#search-laporan').submit(function(){
        $('#laporan-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<legend class="rim2">Laporan Hutang Titipan Apotik</legend>
<?php $this->renderPartial('hutangTitipanApotek/_search',array('model'=>$model)); ?>
<fieldset>
    <?php $this->renderPartial('hutangTitipanApotek/_table',array('model'=>$model)); ?>
    <?php // $this->renderPartial('_tab'); ?>
<!--    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);"></iframe>-->
</fieldset>
<?php        
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printHutangTitipanApotik');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
