<?php 
//    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $table = 'ext.bootstrap.widgets.BootExcelGridView';
    $sort = true;
	$data = $model->searchLaporan();
    if (isset($caraPrint)){
        $template = "{items}";
        $sort = false;
//        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
        echo "<style>
                .tableRincian thead, th{
                    border: 1px #000 solid;
                }
                .tableRincian{
                    width:100%;
                }
            </style>";
        $itemsCssClass = 'tableRincian';
    } else{
         $template = "{pager}{summary}\n{items}";
         $itemsCssClass = 'table table-striped table-bordered table-condensed';
    }
    
      $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array( 
    'id'=>'laporan-grid',
    'dataProvider'=>$data, 
    'template'=>$template, 
    'itemsCssClass'=>$itemsCssClass,
   // 'mergeColumns'=>array('nama_pegawai'),
//    'extraRowColumns'=> array('nama_pegawai'),
    'columns'=>array( 
        array(
            'header' => 'No.',
            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
        ),
        array(
            'header'=>'<center>Tgl Pendaftaran</center>',
            'name'=>'tgl_pendaftaran',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
             'value'=>'$data->tgl_pendaftaran',
            'footerHtmlOptions'=>array('colspan'=>9,'style'=>'text-align:right;font-weight:bold;'),
            'footer'=>'Jumlah Total',
        ),
        array(
            'header'=>'<center>No Registrasi</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
            'value'=>'$data->no_pendaftaran',
        ),
        array(
            'header'=>'<center>Unit Pelayanan</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
            'value'=>'$data->instalasiasal_nama." ". $data->ruanganasal_nama',
        ),
        array(
            'header'=>'<center>No. RM</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
            'value'=>'$data->no_rekam_medik',
        ),
        array(
            'header'=>'<center>Nama Pasien</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
            'value'=>'$data->nama_pasien',
        ),
        array(
            'header'=>'<center>Obat</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
			'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Obat",false))',
        ),
        array(
            'header'=>'<center>Alkes</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
			'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Alkes",false))',
         
        ),
        array(
            'header'=>'<center>Gas</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:center;'),
			'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Gas",false))',
        ),
        array(
            'header'=>'<center>Bruto</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;'),
            'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Bruto",false))',
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
			'footer'=>MyFunction::formatNumber($model->getSumJenisObat("Bruto",true)),
        ),
        array(
            'header'=>'<center>KSO</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;'),
			'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Bruto",false) * $data->kso)',
			'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
            'footer'=>(MyFunction::formatNumber(($model->getSumJenisObat("Kso",true)))),
        ),
        array(
            'header'=>'<center>Netto</center>',
            'type'=>'raw',
            'htmlOptions'=>array('style'=>'text-align:right;'),
			'value'=>'MyFunction::formatNumber($data->getSumJenisObat("Netto",false))',
            'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
			'footer'=>(MyFunction::formatNumber($model->getSumJenisObat("Netto",true))),
            
        ),
    ), 
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}', 
)); ?> 