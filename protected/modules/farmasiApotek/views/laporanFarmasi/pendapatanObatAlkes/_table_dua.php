<?php

$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchTablePendapatanObatAlkes();
$template = "{pager}{summary}\n{items}";
$sort = true;

if (isset($model->print) && $model->print == true) {
    $sort = false;
    $data = $model->searchTablePendapatanObatAlkes(false);
    $template = "{items}";
    if ($model->typePrint == "excel") {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="laporanPendAlkesV-' . date("Y/m/d") . '.xls"');
        header('Cache-Control: max-age=0');
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
}
?>
<?php

$this->widget($table, array(
    'id' => 'tableLaporan',
    'dataProvider' => $data,
    'enableSorting' => $sort,
    'template' => $template,
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'mergeHeaders' => array(),
    'columns' => array(
        array(
                'name'=>'tglpenjualan',
            'header' => 'Tanggal Penjualan',
            ),
        array(
                'name'=>'obatalkes_kode',
            'header' => 'Kode Obat Alkes',
            ),
        array(
                'name'=>'obatalkes_nama',
            'header' => 'Nama Obat Alkes',
            ),
        array(
                'name'=>'noresep',
            'header' => 'No Resep',
            ),
        array(
                'name'=>'jenispenjualan',
            'header' => 'Jenis Penjualan',
                'footerHtmlOptions'=>array('colspan'=>14,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total (Rp.)',
            ),
        array(
            'name' => 'harga_satuan',
            'value' => 'MyFunction::formatNumber($data->harga_satuan)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
		array(
            'name' => 'harga_satuan_jual',
			'header' => 'HJA Jual',
            'value' => 'MyFunction::formatNumber($data->harga_satuan_jual)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        'ppn_persen',
        array(
            'name' => 'hpp',
            'value' => 'MyFunction::formatNumber($data->hpp)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        'qty_jual',
        array(
            'name' => 'bruto_jual',
            'value' => 'MyFunction::formatNumber($data->bruto_jual)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'name' => 'netto_jual',
            'value' => 'MyFunction::formatNumber($data->netto_jual)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
        'discount_jual',
        'qty_retur',
        array(
            'name' => 'total_ret_bruto',
            'value' => 'MyFunction::formatNumber($data->total_ret_bruto)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer'=>'sum(total_ret_bruto)',
        ),
        array(
            'header' => 'Total HJA',
            'value' => 'MyFunction::formatNumber($data->harga_satuan_jual * $data->qty_jual)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer'=>array('harga_satuan_jual', 'qty_jual'),
        ),
        array(
            'name' => 'total_ret_hpp',
            'value' => 'MyFunction::formatNumber($data->total_ret_hpp)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer'=>'sum(total_ret_hpp)',
        ),
        array(
            'name' => 'total_bruto',
            'value' => 'MyFunction::formatNumber($data->total_bruto)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer'=>'sum(total_bruto)'
        ),
        array(
            'name' => 'total_netto',
            'value' => 'MyFunction::formatNumber($data->total_netto)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
             'footer'=>'sum(total_netto)'
        ),
        array(
            'name' => 'total_hpp',
            'value' => 'MyFunction::formatNumber($data->total_hpp)',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer'=>'sum(total_hpp)'
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?>
