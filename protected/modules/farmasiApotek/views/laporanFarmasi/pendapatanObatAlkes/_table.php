<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchTablePendapatanObatAlkes();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
    $data = $model->searchTablePendapatanObatAlkes(false);  
    $template = "{items}";
    if ($caraPrint == "EXCEL") {
        echo $caraPrint;
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'mergeHeaders'=>array(
        array(
            'name'=>'<center>Obat Alkes</center>',
            'start'=>0,
            'end'=>4,
        ),  
        array(
            'name'=>'<center>Penjualan</center>',
            'start'=>5,
            'end'=>9,
        ),  
        array(
            'name'=>'<center>Retur Penjualan</center>',
            'start'=>10,
            'end'=>14,
        ),  
        array(
            'name'=>'<center>Jumlah</center>',
            'start'=>15,
            'end'=>20,
        ),  
    ),
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'footerHtmlOptions'=>array('colspan'=>6,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total (Rp.)',
            ),
//            array(
//                'header'=>'Tgl. Penjualan',
//                'type'=>'raw',
//                'value'=>'$data->tglpenjualan',
//            ),
            array(
                'header'=>'Jenis Obat Alkes',
                'type'=>'raw',
                'value'=>'$data->jenisobatalkes_nama',
            ),
            array(
                'header'=>'Kode Obat',
                'type'=>'raw',
                'value'=>'$data->obatalkes_kode',
            ),
            array(
                'header'=>'Nama Obat',
                'type'=>'raw',
                'value'=>'$data->obatalkes_nama',
            ),
            array(
                'header'=>'Golongan',
                'type'=>'raw',
                'value'=>'empty($data->obatalkes_golongan) ? "<center>-</center>":$data->obatalkes_golongan',
            ),
            //Penjualan
            array(
                'header'=>'Total (Qty)',
                'name'=>'qty_oa',
                'value'=>'MyFunction::formatNumber($data->qty_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
                'header'=>'Bruto',
                'name'=>'hargajual_oa',
                'value'=>'MyFunction::formatNumber($data->hargajual_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'name'=>'discount',
                'value'=>'MyFunction::formatNumber($data->discount)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->ppn_persen)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Netto',
                'name'=>'harganetto_oa',
                'value'=>'MyFunction::formatNumber($data->harganetto_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('harganetto_oa',true))
            ),
            //Retur
            array(
                'header'=>'Total (Qty)',
                'value'=>'MyFunction::formatNumber($data->getOaRetur("qty_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Bruto',
                'value'=>'MyFunction::formatNumber($data->getOaRetur("hargajual_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaRetur('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'value'=>'MyFunction::formatNumber($data->getOaRetur("discount"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaRetur('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->getOaRetur("ppn_persen"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Netto',
                'value'=>'MyFunction::formatNumber($data->getOaRetur("harganetto_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaRetur('harganetto_oa',true))
            ),
            //Total
            array(
                'header'=>'Total (Qty)',
                'value'=>'MyFunction::formatNumber($data->getOaTotal("qty_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Bruto',
                'value'=>'MyFunction::formatNumber($data->getOaTotal("hargajual_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaTotal('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'value'=>'MyFunction::formatNumber($data->getOaTotal("discount"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaTotal('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->getOaTotal("ppn_persen"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Netto',
                'value'=>'MyFunction::formatNumber($data->getOaTotal("harganetto_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaTotal('harganetto_oa',true))
            ),
            array(
                'header'=>'HPP',
                'value'=>'MyFunction::formatNumber($data->hpp)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('hpp',true))
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>