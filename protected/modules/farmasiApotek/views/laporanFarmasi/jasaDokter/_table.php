<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchTableJasaDokter();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchTableJasaDokter(false);  
  $template = "{items}";
  if ($caraPrint == "EXCEL") {
      echo $caraPrint;
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            array(
                'header'=>'Nama Dokter',
                'type'=>'raw',
                'value'=>'PegawaiM::model()->findByPk($data->pegawai_id)->NamaLengkap',
                'footerHtmlOptions'=>array('colspan'=>2,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total Jasa Dokter (Rp.)',
            ),
            array(
                'header'=>'Jasa Dokter (Rp.)',
                'name'=>'jasadokterresep',
                'value'=>'MyFunction::formatNumber($data->jasadokterresep)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'sum(jasadokterresep)'
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>
<script>
function hitungTotal(){
    var total = 0;
    $('#tableLaporan table tbody').find('tr').each(function(){
        var subTotal = parseFloat(unformatNumber($('.subTotal').html()));
        total += subTotal;
    });
    $('#tableTotal tr').find('.total').html(formatNumber(Math.ceil(total)));
}
//hitungTotal();
</script>