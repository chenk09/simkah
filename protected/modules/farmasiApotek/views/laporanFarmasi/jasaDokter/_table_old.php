<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
/**
 * css untuk membuat text head berada d tengah
 */
echo CHtml::css('.table thead tr th{
    vertical-align:middle;
}'); ?>
<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$mergeColumns = array('obatalkes_nama');
$data = $model->searchPrintJasaDokter();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchPrintJasaDokter();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView'; ?>
<style>
    .tableRincian thead, th{
        border: 1px #000 solid;
    }
</style>
  <?php  $itemsCssClass = 'table tableRincian';
}else{
  $itemsCssClass = 'table table-striped table-bordered table-condensed';
}
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
//    'mergeColumns'=>$mergeColumns,
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>$itemsCssClass,
	'columns'=>array(
            
            array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            array(
                'header'=>'No Faktur',
                'name'=>'NoFaktur',
                'type'=>'raw',
                'value'=>'(!empty($data->NoFaktur) ? $data->NoFaktur : " - ")'
            ),
            array(
                'header'=>'Tgl Faktur',
                'name'=>'TglFaktur',
                'type'=>'raw',
                'value'=>'(!empty($data->TglFaktur) ? $data->TglFaktur : " - ")'
            ),
            array(
                'header'=>'Tgl Resep',
                'name'=>'tglresep',
            ),
            array(
                'header'=>'No Resep',
                'name'=>'noresep',
            ),
            array(
                'header'=>'Nama Dokter',
                'type'=>'raw',
                'value'=>'$data->pegawai->nama_pegawai',
            ),
//            array(
//                'name'=>'Tgl Penjualan',
//                'type'=>'raw',
//                'value'=>'$data->tglpenjualan',
//            ),            
//            array(
//                'name'=>'Biaya Administrasi',
//                'type'=>'raw',
//                'value'=>'$data->biayaadministrasi  ',
//            ),
//            array(
//                'name'=>'Biaya Konseling',
//                'type'=>'raw',
//                'value'=>'$data->biayakonseling  ',
//            ),
            array(
                'name'=>'Jasa Resep',
                'type'=>'raw',
                'value'=>'MyFunction::formatNumber($data->jasadokterresep)  ',
                'htmlOptions'=>array('class'=>'subTotal','style'=>'text-align:right;'),
            ),
            
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<table id="tableTotal" width="100%">
    <tr>
        <td width="75%"></td>
        <td><b>Total</b></td>
        <td class="total">0</td>
    </tr>
</table>
<script>
function hitungTotal(){
    var total = 0;
    $('#tableLaporan table tbody').find('tr').each(function(){
        var subTotal = parseFloat(unformatNumber($('.subTotal').html()));
        total += subTotal;
    });
    $('#tableTotal tr').find('.total').html(formatNumber(Math.ceil(total)));
}
hitungTotal();
</script>