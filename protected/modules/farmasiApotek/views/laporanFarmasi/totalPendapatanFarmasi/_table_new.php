<?php
// $data = $model->searchTablePendapatanObatAlkes();
$data = $model->searchGroupByJenisObat();
//$table = 'zii.widgets.grid.CGridView';
$template = "{pager}{summary}\n{items}";
$sort = true;

 $table = 'ext.bootstrap.widgets.ArrayGroupGridView';
// $data = $model->searchGroupByJenisObat();
// $template = "{pager}{summary}\n{items}";
// $sort = true;

if (isset($model->print) && $model->print == true) {
    $sort = false;
    //  $data = $model->searchGroupByJenisObat(false);
    $template = "{items}";
    if ($model->typePrint == "excel") {
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename="laporanTotalPendAlkes-'.date("Y/m/d").'.xls"');
//        header('Cache-Control: max-age=0');
         $table = 'ext.bootstrap.widgets.ArrayExcelGridView';
    }
}
?>

<?php

$this->widget($table, array(
    'id' => 'tableLaporan',
    'dataProvider' => $data,
    'enableSorting' => $sort,
    'template' => $template,
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'name' => 'jenisobatalkes_nama',
            'header' => 'Jenis Obat Alkes',
            'footerHtmlOptions' => array('colspan' => 2, 'style' => 'text-align:right;'),
            'footer' => 'Total (Rp.)',
        ),
//        array(
//            'name' => 'qty_jual',
//            'header' => 'Qty Jual',
//        ),
//		array(
//            'name' => 'hargasatuan_jual',
//            'header' => 'HJA Satuan',
//            'value' => 'MyFunction::formatNumber($data[hargasatuan_jual])',
//            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
//            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//        ),
        array(
            'name' => 'bruto_jual',
            'header' => 'Bruto Jual',
            'value' => 'MyFunction::formatNumber($data[bruto_jual])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
        ),
//        array(
//            'name' => 'netto_jual',
//            'header' => 'Netto Jual',
//            'value' => 'MyFunction::formatNumber($data[netto_jual])',
//            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
//            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//        ),
//        array(
//            'name' => 'hpp_jual',
//            'header' => 'HPP Jual',
//            'value' => 'MyFunction::formatNumber($data[hpp_jual])',
//            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
//            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//        ),
//        array(
//            'header' => 'Discount Jual',
//            'name' => 'discount_jual',
//            'value' => 'MyFunction::formatNumber($data[discount_jual])',
//            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
//            'htmlOptions' => array('style' => 'text-align:right;'),
//            'footerHtmlOptions' => array('style' => 'text-align:right;'),
//        ),
//        array(
//            'header' => 'Qty Retur',
//            'name' => 'qty_retur',
//            'footerHtmlOptions' => array('colspan' => 8, 'style' => 'text-align:right;'),
//            'footer' => 'Total (Rp.)',
//        ),
        array(
            'header' => 'Total Ret Bruto',
            'name' => 'total_ret_bruto',
            'value' => 'MyFunction::formatNumber($data[total_ret_bruto])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
                        'footer' => 'sum(total_ret_bruto)',
        ),
        array(
            'header' => 'Total Ret Netto',
            'name' => 'total_ret_netto',
            'value' => 'MyFunction::formatNumber($data[total_ret_netto])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'sum(total_ret_netto)',
        ),
        array(
            'header' => 'Total Ret HPP',
            'name' => 'total_ret_hpp',
            'value' => 'MyFunction::formatNumber($data[total_ret_hpp])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
             'footer' => 'sum(total_ret_hpp)',
        ),
        array(
            'header' => 'Total Bruto',
            'name' => 'total_bruto',
            'value' => 'MyFunction::formatNumber($data[total_bruto])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'sum(total_bruto)',
        ),
        array(
            'header' => 'Total Netto',
            'name' => 'total_netto',
            'value' => 'MyFunction::formatNumber($data[total_netto])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'sum(total_netto)',
        ),
        array(
            'header' => 'Total HPP',
            'name' => 'total_hpp',
            'value' => 'MyFunction::formatNumber($data[total_hpp])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
            'footer' => 'sum(total_hpp)',
        ),
        array(
            'header' => 'Total HJA',
			'name' => 'total_hja',
            'value' => 'MyFunction::formatNumber($data[total_hja])',
            'headerHtmlOptions' => array('style' => 'vertical-align:middle;text-align:center;'),
            'htmlOptions' => array('style' => 'text-align:right;'),
            'footerHtmlOptions' => array('style' => 'text-align:right;'),
                        'footer' => 'sum(total_hja)',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?>
