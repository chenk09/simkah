<?php
$print_url =  Yii::app()->createAbsoluteUrl('farmasiApotek/laporanFarmasi/laporanPendapatanTotalFarmasi');
Yii::app()->clientScript->registerScript('search', "
$('.tombol').click(function(){
    var type = $(this).attr('type_print');
    window.open('${print_url}&' + $('.search-form form').serialize() + '&print=true&type_print=' + type,'','location=_new, width=900px, scrollbars=yes');
    return false;
});
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableLaporan', {
      data: $(this).serialize()
    });
    return false;
});
");
?><legend class="rim2">Laporan Total Pendapatan Farmasi </legend>
<div class="search-form">
  <?php $this->renderPartial('totalPendapatanFarmasi/_search',array(
    'model'=>$model,
  ));?>
</div>
<fieldset>
    <legend class="rim">Tabel Total Pendapatan Farmasi</legend>
    <?php $this->renderPartial('totalPendapatanFarmasi/_table_new', array('model'=>$model)); ?>
</fieldset>
<?php $this->widget('bootstrap.widgets.BootButtonGroup', array(
   'type'=>'primary',
   'buttons'=>array(
       array('label'=>'Print', 'icon'=>'icon-print icon-white', 'url'=>'javascript:void(0)', 'htmlOptions'=>array(
         'type_print'=>'print',
         'class'=>'tombol'
       )),
       array('label'=>'', 'items'=>array(
           array('label'=>'Print', 'icon'=>'icon-book', 'url'=>'javascript:void(0)', 'itemOptions'=>array(
             'type_print'=>'print',
             'class'=>'tombol'
           )),
           array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>'javascript:void(0)', 'itemOptions'=>array(
             'type_print'=>'excel',
             'class'=>'tombol'
           )),
       )),
   ),
 ));?>
