<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchTableJasaRacikan();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchTableJasaRacikan(false);  
  $template = "{items}";
  if ($caraPrint == "EXCEL") {
      echo $caraPrint;
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
  }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            array(
                'header'=>'Tgl Penjualan',
                'name'=>'tglpenjualan',
            ),
            array(
                'header'=>'No Resep',
                'name'=>'noresep',
            ),
            array(
                'header'=>'Nama',
                'type'=>'raw',
                'value'=>'"Total Biaya Racik"',
                'footerHtmlOptions'=>array('colspan'=>4,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total (Rp.)',
            ),
            array(
                'header'=>'Total (Rp.)',
                'name'=>'totaltarifservice',
                'value'=>'MyFunction::formatNumber($data->totaltarifservice)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'sum(totaltarifservice)'
            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>