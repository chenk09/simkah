<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableLaporan', {
      data: $(this).serialize()
    });
    return false;
});
");
?><legend class="rim2">Laporan Total Pendapatan Farmasi </legend>
<div class="search-form">
  <?php $this->renderPartial('totalPendapatanFarmasi/_search',array(
    'model'=>$model,
  ));?>
</div>
<fieldset>
    <legend class="rim">Tabel Total Pendapatan Farmasi</legend>
    <?php $this->renderPartial('totalPendapatanFarmasi/_table_new', array('model'=>$model)); ?>
</fieldset>
