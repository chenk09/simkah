<?php
  // $data = $model->searchTablePendapatanObatAlkes();
  $data = $model->searchGroupByJenisObat();
  $table = 'zii.widgets.grid.CGridView';
  $template = "{pager}{summary}\n{items}";
  $sort = true;
?>

<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	  'columns'=>array(
      'jenisobatalkes_nama',
      'qty_jual',
      'bruto_jual',
      'netto_jual',
      'hpp_jual',
      'discount_jual',
      'qty_retur',
      'total_ret_bruto',
      'total_ret_netto',
      'total_ret_hpp',
      'total_bruto',
      'total_netto',
      'total_hpp'
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));?>
