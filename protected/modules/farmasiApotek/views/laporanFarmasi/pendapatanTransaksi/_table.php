<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$data = $model->searchTablePendapatanTransaksi();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
    $data = $model->searchTablePendapatanTransaksi(false);  
    $template = "{items}";
    if ($caraPrint == "EXCEL") {
        echo $caraPrint;
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
}
?>
<?php 
$this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'mergeHeaders'=>array(  
        array(
            'name'=>'<center>Penjualan</center>',
            'start'=>2,
            'end'=>7,
        ),
        array(
            'name'=>'<center>Retur Penjualan</center>',
            'start'=>8,
            'end'=>11,
        ),
        array(
            'name'=>'<center>Total</center>',
            'start'=>12,
            'end'=>17,
        ),        

    ),

	'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
            ),
            array(
                'header'=>'No. Resep',
                'type'=>'raw',
                'value'=>'$data->noresep',
            ),
            array(
                'header'=>'Tgl. Penjualan',
                'type'=>'raw',
                'value'=>'$data->tglpenjualan',
            ),
            array(
                'header'=>'Jenis Penjualan',
                'type'=>'raw',
                'value'=>'$data->jenispenjualan',
                'footerHtmlOptions'=>array('colspan'=>4,'style'=>'text-align:right;font-weight:bold;'),
                'footer'=>'Total (Rp.)',
            ),
            array(
                'header'=>'Bruto',
                'name'=>'hargajual_oa',
                'value'=>'MyFunction::formatNumber($data->hargajual_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'name'=>'discount',
                'value'=>'MyFunction::formatNumber($data->discount)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->ppn_persen)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'%'
            ),
            array(
                'header'=>'Netto',
                'name'=>'harganetto_oa',
                'value'=>'MyFunction::formatNumber($data->harganetto_oa)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getOaJual('harganetto_oa',true))
            ),
            // array(
            //     'header'=>'Total',
            //     'name'=>'hargajual_oa',
            //     'value'=>'MyFunction::formatNumber($data->hargajual_oa)',
            //     'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            //     'htmlOptions'=>array('style'=>'text-align:right;'),
            //     'footerHtmlOptions'=>array('style'=>'text-align:right;'),
            //     'footer'=>'sum(hargajual_oa)'
            // ),
            array(
                'header'=>'Bruto',
                'value'=>'MyFunction::formatNumber($data->gettRRetur("hargajual_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRRetur('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'value'=>'MyFunction::formatNumber($data->gettRRetur("discount"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRRetur('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->gettRRetur("ppn_persen"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Netto',
                'value'=>'MyFunction::formatNumber($data->gettRRetur("harganetto_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRRetur('harganetto_oa',true))
            ),
           //Total
            array(
                'header'=>'Bruto',
                'value'=>'MyFunction::formatNumber($data->gettRTotal("hargajual_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRTotal('hargajual_oa',true))
            ),
            array(
                'header'=>'Discount',
                'value'=>'MyFunction::formatNumber($data->gettRTotal("discount"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRTotal('discount',true))
            ),
            array(
                'header'=>'PPn (%)',
                'value'=>'MyFunction::formatNumber($data->gettRTotal("ppn_persen"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>'-'
            ),
            array(
                'header'=>'Netto',
                'value'=>'MyFunction::formatNumber($data->gettRTotal("harganetto_oa"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->gettRTotal('harganetto_oa',true))
            ),
            array(
                'header'=>'HPP',
                'value'=>'MyFunction::formatNumber($data->getTrTotal("hpp"))',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                'footer'=>  MyFunction::formatNumber($model->getTrTotal('hpp',true))
            ),


	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>

