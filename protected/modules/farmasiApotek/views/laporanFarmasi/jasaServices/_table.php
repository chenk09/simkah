<?php 
/**
 * css untuk membuat text head berada d tengah
 */
echo CHtml::css('.table thead tr th{
    vertical-align:middle;
}'); ?>
<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
$mergeColumns = array('obatalkes_nama');
$data = $model->searchTabelServices();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchPrintServices();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
//    'mergeColumns'=>$mergeColumns,
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            
            array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
            ),
            array(
                'name'=>'Tanggal Pendaftaran',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->tgl_pendaftaran',
            ),
            array(
                'name'=>'No Pendaftaran',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->no_pendaftaran',
            ),
            array(
                'name'=>'No Rekam Medik',
                'type'=>'raw',
                'value'=>'$data->pasien->no_rekam_medik',
            ),
            array(
                'name'=>'Jenis Kelamin',
                'type'=>'raw',
                'value'=>'$data->pasien->jeniskelamin',
            ),
            array(
                'name'=>'Nama/ Bin',
                'type'=>'raw',
                'value'=>'$data->pasien->NamaNamaBIN',
            ),
            array(
                'name'=>'Ruangan Asal',
                'type'=>'raw',
                'value'=>'$data->ruanganasal_nama',
            ),
            array(
                'name'=>'Tgl Resep',
                'type'=>'raw',
                'value'=>'$data->tglresep',
            ),
            array(
                'name'=>'No Resep',
                'type'=>'raw',
                'value'=>'$data->noresep',
            ),
            array(
                'name'=>'Nama Dokter',
                'type'=>'raw',
                'value'=>'$data->pegawai->nama_pegawai',
            ),
            array(
                'name'=>'Tgl Penjualan',
                'type'=>'raw',
                'value'=>'$data->tglpenjualan',
            ),            
            array(
                'name'=>'Biaya Administrasi',
                'type'=>'raw',
                'value'=>'$data->biayaadministrasi  ',
            ),
            array(
                'name'=>'Biaya Konseling',
                'type'=>'raw',
                'value'=>'$data->biayakonseling  ',
            ),
            array(
                'name'=>'Total Tarif Service',
                'type'=>'raw',
                'value'=>'$data->totaltarifservice  ',
            ),
            
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>