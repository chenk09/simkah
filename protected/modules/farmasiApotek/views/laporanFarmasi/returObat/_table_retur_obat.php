<?php 
	$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
	$data = $model->searchReturFarmasi();
	$template = "{pager}{summary}\n{items}";
	$sort = true;
	
	if(isset($caraPrint))
	{
		$sort = false;
		$data = $model->searchReturFarmasiPrint();  
		$template = "{items}";
		
		if($caraPrint == "EXCEL"){
			echo $caraPrint;
			$table = 'ext.bootstrap.widgets.BootExcelGridView';
		} 
	}
?>

<?php 
$this->widget($table,array(
	'id'=>'laporan-grid',
	'dataProvider'=>$data,
	'enableSorting'=>$sort,
	'template'=>$template,
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
		array(
			'name'=>'noreturresep',
			'type'=>'raw',
			'footerHtmlOptions'=>array('colspan'=>7,'style'=>'text-align:right;font-weight:bold;'),
			'footer'=>'Total',
		),
		'tglretur',
		'jenispenjualan',
		'alasanretur',
		'obatalkes_nama',
		'qty_retur',
		array(
			'name'=>'hargasatuan',
			'value'=>'MyFunction::formatNumber($data->hargasatuan)',
			'htmlOptions'=>array(
				'style'=>'text-align:right;'
			),
		),		
		array(
			'name'=>'totalretur',
			'value'=>'MyFunction::formatNumber($data->hargasatuan*$data->qty_retur)',
			'htmlOptions'=>array(
				'style'=>'text-align:right;'
			),
			'headerHtmlOptions'=>array('style'=>'vertical-align:middle;'),
			'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
			'footer'=>array('qty_retur', 'hargasatuan')
		),
	),
	'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>