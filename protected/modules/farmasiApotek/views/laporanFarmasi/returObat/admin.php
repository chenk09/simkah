<?php

//$this->breadcrumbs=array(
//    'Ppinfo Kunjungan Rjvs'=>array('index'),
//    'Manage',
//);
//
//$this->widget('bootstrap.widgets.BootAlert');

$url = Yii::app()->createUrl('farmasiApotek/laporanFarmasi/frameGrafikReturObat&id=1');
Yii::app()->clientScript->registerScript('searchTable', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('#search-laporan').submit(function(){
	$('#Grafik').attr('src','').css('height','0px');
	$('#laporan-grid').addClass('srbacLoading');
	$.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

/*
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('#search-laporan').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $.fn.yiiGridView.update('laporan-grid', {
		data: $(this).serialize()
    });
    return false;
});
");
*/

?>
<legend class="rim2">Laporan Retur Obat</legend>
<div class="search-form">
<?php
$this->renderPartial('returObat/_search',array(
    'model'=>$model,
));
?>
</div><!-- search-form -->
<fieldset>
    <legend class="rim">Tabel Retur</legend>
    <?php //$model = new FALaporanreturobatV; ?>
    <?php //$this->renderPartial('returObat/_tableReturObat', array('model'=>$model)); ?>
	
    <?php $this->renderPartial('returObat/_table_retur_obat', array('model'=>$model)); ?>
    <?php //$this->renderPartial('_tab'); ?>
	<!--
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);"></iframe>        
	-->
</fieldset>

<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanReturObat');
    $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); ?>