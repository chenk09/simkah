<?php 
    // echo $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>7));      
?>

<?php
echo CHtml::css('.control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    #wrapper{
        margin-top:42mm
    }
    table{
        font-size:11px;
    }
    @media print {
        @page {
            
        }
    }
');
?>
<!-- size: 103mm 210mm; -->
<div id="wrapper">
    <table width="100%" style="margin:0px" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="70mm"></td>
                        <td width=""><?php echo (empty($modDetailPenjualan[0]->nama_pegawai )) ? " - " :  $modDetailPenjualan[0]->nama_pegawai  ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td><?php echo CHtml::encode($modelPenjualanResep->tglresep); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <Td> <?php echo CHtml::encode($modPasien->nama_pasien); ?></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <br>
    <table>
        <?php
        if (count($modDetailPenjualan) > 0) {
            $no = 1;
            foreach($modDetailPenjualan AS $tampilData):
        ?>
        <br>
        <tr>
            <td width="5%">R/<?=$no?></td>
            <td width="40%"><?=$tampilData->obatalkes_nama?></td>
            <td width="55%">no <?=$this->ConvertRomawi($tampilData->copy_resep_qty)?></td>
        </tr>
        <tr>
            <td></td>
            <td colspan="2"> 
                <?php
                    if (preg_match('!(\d+)\s*x\s*(\d+)!i', $tampilData->signa_oa, $m)){
                        echo 'S '.$this->ConvertRomawi($m[1]).' dd '.$this->ConvertRomawi($m[2]);
                    }
                ?>
                <?//= str_replace('x',' dd ',$tampilData->signa_oa)?>
            </td>
        </tr>
        <tr>
            <td colspan="2"><hr style="border: 1; border-color:#000;"></td>
            <td>
                <?php echo  $this->ConvertRomawi($tampilData->qty_oa).' '.$tampilData->copy_resep_det;?>
            </td>
        </tr>
        <?php
            endforeach;
        }else{
            echo '<tr><td colspan=12>'.Yii::t('zii','No results found.').'</td></tr>';
        }
        
        ?>
    </table>
</div>
<?php
        //echo $this->renderPartial('application.views.headerReport.footer2Default',array('colspan'=>7)); 
?>