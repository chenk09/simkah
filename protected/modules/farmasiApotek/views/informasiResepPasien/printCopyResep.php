
<style>
    @page { 
        height: 210mm; 
        width: 138mm; 
        size: auto;  
        margin: 5mm; 
    }
    @media print {
        #page{
            /* margin:0px; */
            /* padding:0px; */
            height: 210mm; 
            width: 138mm; 
            size: auto;  
            margin: 5mm; 
            /* padding-bottom:50mm; */
        }
    }
    div#header{
        position:fixed;
        top:0px;
        width:100%;
    }
    .control-label{
        float:left; 
        text-align: right; 
        width:120px;
        color:black;
        padding-right:10px;
    }
    table{
        font-size:11px;
    }
    *{
        margin:0px;
        padding:0px;
    }
    hr{
        border: 1px solid #000;
    }
    .p-kecil {
        font-size:10pt;
    }
    .jarak {
        display: inline-block;
        width: 20mm;
    }
    div.content{
        margin-top:30mm;
    }
    .kecil{
        font-size:8pt;
        padding: 3px;
    }
</style>
<div id="page">
    <div id="header">
        <?php $data=ProfilrumahsakitM::model()->findByPk(Params::DEFAULT_PROFIL_RUMAH_SAKIT); ?>
        <table width="<?php echo ((isset($width)) ? $width : "100%")?>">
            <tr style="display: none;">
                <td width="20%" height="50%">
                    <img src="<?php echo Params::urlProfilRSDirectory().$data->path_logorumahsakit ?>" style="max-width: 90px; width:80px; margin-left: 15px;"/>
                </td>
                <td>
                    <span class="p-kecil">INSTALASI FARMASI</span>
                    <div>
                        <h3><FONT FACE="Liberation Serif" SIZE=6 color="black">
                        <?php echo $data->nama_rumahsakit ?> </FONT></h3>
                    </div>
                    <span class="p-kecil">
                        <?php echo $data->alamatlokasi_rumahsakit ?><br>
                        Tlp. <?php echo $data->no_telp_profilrs.' Email : '.$data->email ?>
                        <br>
                    </span>
                    <span class="p-kecil jarak">APOTEKER</span><span class="p-kecil"> : Lina Mellina, S.Farm., Apt</span><br>
                    <span class="p-kecil jarak">STRA</span><span class="p-kecil"> : 19910423/STRA_UNJANI/2014/238192</span><br>
                    <span class="p-kecil jarak">SIPA</span><span class="p-kecil"> : 19910423/SIPA_32.78/2019/2.475</span>
                </td>
                <td width="15%">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" HEIGHT=2 style="border-bottom: 1px solid #000000; ">&nbsp;</td>
            </tr>  
            <tr>
                <td colspan="3" HEIGHT=2 style="border-bottom: 1px solid #000000; text-align:center">SALINAN RESEP <br></td>
            </tr> 
        </table>
        <br>
        <table width="100%" style="margin:0px" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                            <td width="210mm">Oleh Dokter</td>
                            <td width=""> : <?php echo $modDetailPenjualan[0]->nama_pegawai  ?></td>
                        </tr>
                        <tr>
                            <td>Diberikan Tanggal</td>
                            <td> : <?php echo CHtml::encode($modelPenjualanResep->tglresep); ?></td>
                        </tr>
                        <tr>
                            <td>Untuk</td>
                            <Td> : <?php echo CHtml::encode($modPasien->nama_pasien); ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div class="content">
        <table width="100%" cellspacing="0px" cellpadding="0">
            <style>
                
            </style>
            <?php
            $jm = count($modDetailPenjualan);
            if (count($modDetailPenjualan) > 0)
            {
                $no = 1;
                foreach($modDetailPenjualan AS $tampilData):
            ?>
            <tr>
                <td class="kecil" style="width:5%">R/</td>
                <td class="kecil" style="width:35%"><?=$tampilData->obatalkes_nama?></td>
                <td class="kecil" style="width:50%">no <?=$this->ConvertRomawi($tampilData->qty_oa)?></td>
                <td class="kecil" style="width:10%">&nbsp;</td>
            </tr>
            <tr>
                <td class="kecil"></td>
                <td class="kecil" colspan="3"> 
                    <?php
                        if (preg_match('!(\d+)\s*x\s*(\d+)!i', $tampilData->signa_oa, $m)){
                            echo 'S &nbsp; &nbsp; &nbsp;'.$m[1].' dd '.$this->ConvertRomawi($m[2]);
                        }
                    ?>
                    <?= str_replace('x',' dd ',$tampilData->signa_oa)?>
                </td>
            </tr>
            <tr>
                <td class="kecil" colspan="3"><hr style="border-bottom:0px; margin: 0em;"></td>
                <td class="kecil"></td>
            </tr>
            <?php
                $no++;
                endforeach;
            }else{
                echo '<tr><td colspan="4"><br><br><br><br><br>'.Yii::t('zii','No results found.').'</td></tr>';
            }
            ?>
        </table>
    </div>
</div>