<?php
    $urlAddItems = $this->createUrl('/farmasiApotek/mappingDiagnosaObat/mappingObat', array(
        'id'=>$model->diagnosa_id
    ));
    $urlRemoveItems = $this->createUrl('/farmasiApotek/mappingDiagnosaObat/removeMappingObat', array(
        'id'=>$model->diagnosa_id
    ));
?>
<legend class="rim2">Mapping Diagnosa ke Obat</legend>
<div class="row-fluid" style="margin-top: 15px;">
    <div class="span2" style="font-weight: bold;">Kode Diagnosa</div>
    <div class="span10"><?=$model->diagnosa_kode?></div>
</div>
<div class="row-fluid">
    <div class="span2" style="font-weight: bold;">Nama Diagnosa</div>
    <div class="span10"><?=$model->diagnosa_nama?></div>
</div>
<hr>
<div class="row-fluid">
    <div class="span12">
        <table width="100%" class="table-bordered">
            <thead>
            <tr>
                <th width="45%">Master Obat</th>
                <th width="10%">&nbsp;</th>
                <th width="45%">Yang sudah dimapping</th>
            </tr>
            </thead>
            <tr>
                <td>
                    <?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
                        'id'=>'master-obat-grid',
                        'dataProvider'=>$obats->search(),
                        'filter'=>$obats,
                        'template'=>"{pager}{summary}\n{items}",
                        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                        'columns'=>array(
                            array(
                                'header'=>'#',
                                'type'=>'raw',
                                'htmlOptions'=>array(
                                    'style'=>'text-align:center'
                                ),
                                'value'=>function($data){
                                    $vName = 'pilih_' . $data->obatalkes_id;
                                    $vLink = CHtml::checkBox($vName, false, array(
                                        'value'=>$data->obatalkes_id,
                                        'class'=>'pilih_obat'
                                    ));
                                    return $vLink;
                                },
                            ),
                            'obatalkes_kode',
                            'obatalkes_nama',
                        ),
                        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                    )); ?>
                </td>
                <td>
                    <div style="margin: 2px">
                        <?=CHtml::link('<i class="icon-plus icon-white"></i>', array(),array(
                            'class'=>'label label-success',
                            'onClick'=>'return addItems()',
                            'style'=>'display: block;text-align:center',
                        ))?>
                    </div>
                    <div style="margin: 2px">
                        <?=CHtml::link('<i class="icon-remove icon-white"></i>', array(), array(
                            'class'=>'label label-important',
                            'onClick'=>'return removeItems()',
                            'style'=>'display: block;text-align:center',
                        ))?>
                    </div>
                </td>
                <td>
                    <?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
                        'id'=>'map-obat-grid',
                        'dataProvider'=>$mapobats->search(),
                        'filter'=>$mapobats,
                        'template'=>"{pager}{summary}\n{items}",
                        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                        'columns'=>array(
                            array(
                                'header'=>'#',
                                'type'=>'raw',
                                'htmlOptions'=>array(
                                    'style'=>'text-align:center'
                                ),
                                'value'=>function($data){
                                    $vName = 'pilih_' . $data->mapping_diag_obat_det_id;
                                    $vLink = CHtml::checkBox($vName, false, array(
                                        'value'=>$data->mapping_diag_obat_det_id,
                                        'class'=>'pilih_obat'
                                    ));
                                    return $vLink;
                                },
                            ),
                            array(
                                'name'=>'obatalkes_kode',
                                'value'=>function($data){
                                    $obatalkes_kode = $data->obats->obatalkes_kode;
                                    return $obatalkes_kode;
                                },
                            ),
                            array(
                                'name'=>'obatalkes_nama',
                                'value'=>function($data){
                                    $obatalkes_kode = $data->obats->obatalkes_nama;
                                    return $obatalkes_kode;
                                },
                            ),
                            array(
                                'header'=>'#',
                                'type'=>'raw',
                                'htmlOptions'=>array(
                                    'style'=>'text-align:center'
                                ),
                                'value'=>function($data){
                                    $vLink = CHtml::link('<i class="icon-remove"></i>', '#', array(
                                        'data-attr'=>json_encode($data->attributes),
                                        'onclick'=>'return removeItem(this);'
                                    ));
                                    return $vLink;
                                },
                            ),
                        ),
                        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                    )); ?>
                </td>
            </tr>
        </table>
    </div>
</div>

<?=CHtml::link('<< Kembali', array('/farmasiApotek/mappingDiagnosaObat/index'), array(
    'class'=>'btn btn-info'
))?>

<script type="text/javascript">
    function addItems(){
        var vJob = $("#master-obat-grid").find(".pilih_obat").is(':checked');
        if(vJob)
        {
            var vObat = {};
            $.each($("#master-obat-grid").find(".pilih_obat"), function (index, value){
                if($(this).is(':checked'))
                {
                    var xObat = $(this).val();
                    vObat[xObat] = xObat;
                }
            });

            var vUri = '<?=$urlAddItems?>';
            var vData = {
                'MapDiagnosaObat':'ok',
                'obats':vObat
            };
            postingDokumen(vData, vUri);
        }else{
            swal({
                html: 'silakan pilih obat yang akan dimapping',
                type: 'error'
            });
        }
        return false;
    }

    function removeItems(){
        var vJob = $("#map-obat-grid").find(".pilih_obat").is(':checked');
        if(vJob)
        {
            var vObat = {};
            $.each($("#map-obat-grid").find(".pilih_obat"), function (index, value){
                if($(this).is(':checked'))
                {
                    var xObat = $(this).val();
                    vObat[xObat] = xObat;
                }
            });
            var vUri = '<?=$urlRemoveItems?>';
            var vData = {
                'MapDiagnosaObat':'ok',
                'obats':vObat
            };
            postingDokumen(vData, vUri);
        }else{
            swal({
                html: 'silakan pilih obat yang akan dihapus',
                type: 'error'
            });
        }
        return false;
    }

    function removeItem(obj)
    {
        var attr = $(obj).data('attr');
        var id = attr.mapping_diag_obat_det_id

        var vObat = {};
        vObat[id] = id;

        var vUri = '<?=$urlRemoveItems?>';
        var vData = {
            'MapDiagnosaObat':'ok',
            'obats':vObat
        };
        postingDokumen(vData, vUri);
        return false;
    }

    function postingDokumen(vData, vUri)
    {
        swal.queue([{
            confirmButtonText: 'Lanjutkan',
            showLoaderOnConfirm: true,
            allowOutsideClick: false,
            showCancelButton: true,
            cancelButtonText: 'Batal',
            text:'Konfirmasi proses dokumen',
            preConfirm: function (){
                return new Promise(function (resolve){
                    $.ajax({
                        type: "POST",
                        data:vData,
                        dataType: 'json',
                        url: vUri,
                        success: function(response, statusText, xhr, $form)
                        {
                            swal.close();
                            if(response.status == 'not')
                            {
                                swal({
                                    html: response.msg,
                                    type: 'error'
                                });
                            }else{
                                location.reload();
                            }
                        },
                        error: function(response, statusText, xhr, $form)
                        {
                            swal.close();
                        }
                    });
                })
            }
        }])
    }

</script>