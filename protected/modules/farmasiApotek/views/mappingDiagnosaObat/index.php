<?php
$this->breadcrumbs = array(
	'Mapping Diagnosa Obat',
);?>
<legend class="rim2">Mapping Diagnosa ke Obat</legend>
<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
    'id'=>'fadiagnosaobat-m-grid',
    'dataProvider'=>$model->searchByMapDiagnosa(),
    'filter'=>$model,
    'template'=>"<div><div class='pull-right'>{pager}</div><div class='pull-left'>{summary}</div></div><div>{items}</div><div><div class='pull-right'>{pager}</div></div>",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        'diagnosa_kode',
        'diagnosa_nama',
        'diagnosa_namalainnya',
        array(
            'header'=>'Status',
            'type'=>'raw',
            'name'=>'status_mapping_obat',
            'filter'=>array('Belum', 'Sudah'),
            'htmlOptions'=>array(
                'style'=>'width:105px;'
            ),
            'value'=>function($data){
                $vBtn = CHtml::tag('span', array(
                    'class'=>'label'
                ), 'Belum Dimapping');
                if(!is_null($data->status_mapping_obat))
                {
                    $vBtn = CHtml::tag('span', array(
                        'class'=>'label label-success'
                    ), 'Sudah Dimapping');
                }
                return $vBtn;
            },
        ),
        array(
            'header'=>'#',
            'type'=>'raw',
            'htmlOptions'=>array(
                'style'=>'width:100px;'
            ),
            'value'=>function($data){
                $vBtn = CHtml::link('Mapping Obat', array('/farmasiApotek/mappingDiagnosaObat/mappingObat', 'id'=>$data->diagnosa_id), array(
                    'class'=>'btn'
                ));
                return $vBtn;
            },
        ),
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>