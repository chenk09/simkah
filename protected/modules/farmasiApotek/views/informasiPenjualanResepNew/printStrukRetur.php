<?php // $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<!--<div style="height: 3cm;"></div>-->
<style>
    table td{
        font-family: tahoma;
        font-size: 7pt;
    }
	table th{
        font-family: tahoma;
        font-size: 7pt;
    }
    .tandatangan{
        vertical-align: bottom;
        text-align: center;
    }
</style>
<div style="margin:5px;">
	<table width="100%" border="0">
		<tr>
			<td valign='top' width="10">No. RM</td>
			<td valign='top'>: <?php echo implode(".", str_split($modPenjualan->pasien->no_rekam_medik, 2)); ?> - <?php echo $modPenjualan->pendaftaran->no_pendaftaran;?></td>
		</tr>
		<tr>
			<td valign='top'>Nama</td>
			<td valign='top'>: <?php echo $modPenjualan->pasien->nama_pasien;?> </td>
		</tr>
		<tr>
			<td valign='top'>Umur</td>
			<td valign='top'>: <?php echo $modPenjualan->pendaftaran->umur;?> 
		</tr>	
		<tr>
			<td valign='top' width="80">No. Resep</td>
			<td valign='top'>: <?php echo $modPenjualan->noresep;?>
		</tr>
		<tr>
			<td valign='top'>Ruangan Asal</td>
			<td valign='top'>: <?php echo $modPenjualan->ruangan->ruangan_nama?> <?php if($modPenjualan->ruangan_id != $modRetur->ruangan_id) echo "/".RuanganM::model()->findByPk($modRetur->ruangan_id)->ruangan_nama; ?></td>
		</tr>		
	</table>
	<!--
	<table width="100%" border="0">
		<tr>
			<td valign='top' width="155">
				<table border="0">
					<tr>
						<td valign='top'>No. Resep</td>
						<td valign='top'>: <?php echo $modPenjualan->noresep;?>
					</tr>
					<tr>
						<td valign='top'>No. Daftar</td>
						<td valign='top'>: <?php echo $modPenjualan->pendaftaran->no_pendaftaran;?></td>
					</tr>
					<tr>
						<td valign='top'>Ruangan Asal</td>
						<td valign='top'>: <?php echo $modPenjualan->ruangan->ruangan_nama?> <?php if($modPenjualan->ruangan_id != $modRetur->ruangan_id) echo "/".RuanganM::model()->findByPk($modRetur->ruangan_id)->ruangan_nama; ?></td>
					</tr>
					<tr>
						<td valign='top' width="40">No. RM</td>
						<td valign='top'>: <?php echo implode(".", str_split($modPenjualan->pasien->no_rekam_medik, 2)); ?></td>
					</tr>
					<tr>
						<td valign='top'>Nama</td>
						<td valign='top'>:<?php echo $modPenjualan->pasien->nama_pasien;?> </td>
					</tr>
					<tr>
						<td valign='top'>Umur</td>
						<td valign='top'>: <?php echo $modPenjualan->pendaftaran->umur;?> 
					</tr>					
				</table>			
			</td>
			<td valign='top'>
				<table border="0">
					<tr>
						<td valign='top' width="40">No. RM</td>
						<td valign='top'>: <?php echo implode(".", str_split($modPenjualan->pasien->no_rekam_medik, 2)); ?></td>
					</tr>
					<tr>
						<td valign='top'>Nama</td>
						<td valign='top'>:<?php echo $modPenjualan->pasien->nama_pasien;?> </td>
					</tr>
					<tr>
						<td valign='top'>Umur</td>
						<td valign='top'>: <?php echo $modPenjualan->pendaftaran->umur;?> 
					</tr>
				</table>			
			</td>
			
		</tr>
	</table>
	-->
	<div>&nbsp;</div>
	<table width="100%">
		<thead style='border-top:1px solid; border-bottom:1px solid;'>
			<th width="30" style='text-align:left;'>No.</th>
			<th width="50" style='text-align:left;'>Kode</th>
			<th style='text-align:left;'>Nama</th>
			<!--<th style='text-align: center;'>Qty Jual</th>-->
			<th width="50" style='text-align:left;'>Qty Retur</th>
			<!--<th style='text-align: center;'>Qty Setelah Retur</th>
			<th style='text-align: center;'>Kondisi Obat</th>-->
		</thead>
		<?php
		$no=1;
		$totalSubTotal = 0;
		if (count($modReturDetail) > 0){
			foreach($modReturDetail AS $tampilData):
			echo "<tr ". ($no == (count($modReturDetail)) ? "style='border-bottom:1px solid;'" : "") .">
				<td valign='top'>".$no."</td>
				<td valign='top'>".$tampilData->obatpasien->obatalkes->obatalkes_kode."</td>
				<td>".$tampilData->obatpasien->obatalkes->obatalkes_nama."</td>
				<td style='text-align:right;'>".$tampilData->qty_retur."&nbsp;</td>
			 </tr>";
			$no++;
			endforeach;
		}
		?>
	</table>
	<div>&nbsp;</div>
	<table width="100%">
		<tr>
			<td valign="top" width="50%">Alasan Retur :<br><?php echo $modRetur->alasanretur; ?></td>
			<td valign="top">Keterangan Retur :<br><?php echo $modRetur->keteranganretur; ?></td>
		</tr>
	</table>
	<div>&nbsp;</div>
	<table style="width:100%;">
		<tr>
			<td class="tandatangan">Penerima</td>
			<td class="tandatangan">Hormat Kami,</td>
		</tr>
		<tr>
			<td class="tandatangan" style="height:50px;">.........................</td>
			<td class="tandatangan"><?php echo $modPegawaiRetur->gelardepan." ".$modPegawaiRetur->nama_pegawai."., ".$modPegawaiRetur->gelarbelakang->gelarbelakang_nama; ?></td>
		</tr>
	</table>
	<div style="font-size:8pt;">Print Date: <?php echo Yii::app()->user->getState('nama_pegawai'); ?>
		<?php echo date('d M Y H:i:s'); ?>
	</div>
</div>