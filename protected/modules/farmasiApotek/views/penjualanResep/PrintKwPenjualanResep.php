<?php // $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<!--<div style="height: 3cm;"></div>-->
<style>
    table td{
        font-family: tahoma;
        font-size: 7pt;
    }
	table th{
        font-family: tahoma;
        font-size: 7pt;
    }
    .tandatangan{
        vertical-align: bottom;
        text-align: center;
    }
</style>
<div style="margin:5px;">
	<table width="100%" border="0">
		<tr>
			<td valign='top' width="10">No. RM</td>
			<td valign='top'>: <?php echo implode(".", str_split($modPenjualan->pasien->no_rekam_medik, 2)); ?> - <?php echo $modPenjualan->pendaftaran->no_pendaftaran;?></td>
		</tr>
		<tr>
			<td valign='top'>Nama</td>
			<td valign='top'>: <?php echo $modPenjualan->pasien->nama_pasien;?> </td>
		</tr>
		<tr>
			<td valign='top'>Umur</td>
			<td valign='top'>: <?php echo $modPenjualan->pendaftaran->umur;?> 
		</tr>	
		<tr>
			<td valign='top' width="80">No. Resep</td>
			<td valign='top'>: <?php echo $modPenjualan->noresep;?>
		</tr>
	</table>
	<div>&nbsp;</div>
	<table width="100%">
		<thead style='border-top:1px solid; border-bottom:1px solid;'>
			<th style='text-align: center;width: 20px;'>No.</th>
			<th style='text-align: left;'>Kode</th>
			<th style='text-align: left;'>Nama</th>
			<th style='text-align: left;'>Qty</th>
		</thead>
		<?php
		$no=1;
		$totalSubTotal = 0;
		if (count($obatAlkes) > 0)
		{
			foreach($obatAlkes AS $tampilData):
			echo "<tr ". ($no == (count($obatAlkes)) ? "style='border-bottom:1px solid;'" : "") .">
				<td style='text-align:center;'>".$no."</td>
				<td>".$tampilData->obatalkes->obatalkes_kode."</td>
				<td>".$tampilData->obatalkes->obatalkes_nama."</td>
				<td>".$tampilData->qty_oa."</td>
			 </tr>";  
			$no++;
			endforeach;
		}
		?>
	</table>
	<table style="width:100%;">
		<tr><td class="tandatangan">Penerima</td>
			<td class="tandatangan">Hormat Kami,</td>
		</tr>
		<tr>
			<td class="tandatangan" style="height: 50px;">.........................</td>
			<td class="tandatangan" ><?php echo Yii::app()->user->getState('nama_pegawai'); ?>
		</td></tr>
	</table>	
	<div style="font-size:8pt;">Print Date: <?php echo $modPenjualan->ruangan->ruangan_nama;?>,<?php echo date('d M Y H:i:s'); ?></div>	
</div>