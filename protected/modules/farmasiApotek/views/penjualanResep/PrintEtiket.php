<?php

function potongNama($data) {
    $nama = explode(" ", ucwords($data));
    if (count($nama) > 1) {
        return $nama[0] . ' ' . $nama[1][0];
    }
    return $nama[0];
}

$format = new CustomFormat();
$data = ProfilrumahsakitM::model()->findByPk(Params::DEFAULT_PROFIL_RUMAH_SAKIT);
?>
<?php
$gabungan = array();
$racikan = array();
foreach ($obatAlkes as $obat) {
    if ($obat->r == "R/") {
        $gabungan[$obat->obatalkespasien_id] = $obat;
    } else {
        $racikan[0]['data'] = $obat;
        $racikan[0]['children'][] = $obat;
    }
}
?>
<style type="text/css">
    @page {
        margin: 10px 0 10px 0;
    }
</style>
<?php 

function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}

 ?>

<?php foreach ($gabungan as $obat) { ?>
    <table cellpadding="0" cellspacing="0" style="margin-bottom: 10px; margin-top: 10px; margin-left: 10px; margin-right: 0px;">
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td>
                            <table width="100%">
                                <TR>
                                    <TD VALIGN=MIDDLE colspan="6">
                                        <B><FONT FACE="Arial" style="font-size: 13px;" color="black">Instalasi Farmasi <?php echo $data->nama_rumahsakit ?></FONT></B>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD VALIGN=MIDDLE colspan="5">
                                        <FONT FACE="Arial" style="font-size: 9px;"color="black"><?php echo $data->alamatlokasi_rumahsakit ?> 46113 Telp. <?php echo $data->no_telp_profilrs ?></FONT>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD colspan="7" HEIGHT=2 style="border-bottom: 2px solid #000000; text-align: center; margin-right: 0px;"></TD>
                                </TR>
                                <TR>
                                    <TD colspan="3" VALIGN=MIDDLE >
                                        <FONT FACE="Arial" style="font-size: 12px;"  color="black"> No. Resep : <?php echo $modPenjualan->noresep; ?>
                                        </font>
                                    </TD>
                                    <TD colspan="2"  align="center"  VALIGN=MIDDLE>
                                        <FONT FACE="Arial" style="font-size: 12px;"  color="black">
                                            <?php echo Yii::app()->dateFormatter->format("d-M-y",$modPenjualan->tglresep);?>
                                        </font>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD colspan="3" VALIGN=MIDDLE>
                                        <FONT FACE="Arial" style="font-size: 12px;"  color="black"><?php echo $pasien->namadepan; ?> <?php custom_echo($pasien->nama_pasien,25); ?>/<?php echo $pasien->no_rekam_medik; ?><?php echo (!empty($pasien->pendaftaran_id) ? '/' . date('d-m-Y', strtotime($format->formatDateMediumForDB($pasien->tanggal_lahir))) : ""); ?>
                                        </font>
                                    </TD>
                                    <TD colspan="2" align="center"  VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black">

                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" align="center" VALIGN=MIDDLE>
                                        <FONT FACE="Arial" style="font-size: 12px;" color="black">
                                        <B>Sehari <?php echo $obat->signa_oa; ?> <?php echo $obat->etiket_satuan; ?> <?php echo $obat->etiket_caramakan; ?> </b>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" align="center"  VALIGN=MIDDLE>
                                        <FONT FACE="Arial" style="font-size: 12px;"  color="black">
                                        <B><?php echo $obat->etiket_keterangan; ?> </B>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" VALIGN=MIDDLE>
                                        <FONT FACE="Arial" style="font-size: 12px;" color="black">
                                        <?php echo $obat->obatalkes->obatalkes_nama . '(' . $obat->qty_oa . ')'; ?>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="6" HEIGHT=2 style="border-bottom: 2px solid #000000"></TD>
                                </TR>
                                <TR>
                                    <TD colspan="5" align="center" style="font-size: 12px;"  VALIGN=MIDDLE>
                                        <FONT FACE="Arial" color="black">
                                        Semoga Lekas Sembuh Terima Kasih
                                        </font>
                                    </TD>
                                </TR> 
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <?php //echo $modPenjualan->tglresep; ?>
<?php }    ?>

<?php
foreach ($racikan as $obatRacikan) {
    $obat = $obatRacikan['data'];
    ?>
    <table cellpadding="0" cellspacing="0" style="margin-bottom: 10px; margin-top: 10px; margin-left: 10px; margin-right: 15px;">
        <tr>
            <td colspan="3">
                <table>
                    <tr>
                        <td>
                            <table width="100%">
                                <TR>
                                    <TD VALIGN=MIDDLE colspan="6">
                                        <B><FONT FACE="Arial" style="font-size: 13px;" color="black">Instalasi Farmasi <?php echo $data->nama_rumahsakit ?></FONT></B>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD VALIGN=MIDDLE colspan="5">
                                        <FONT FACE="Arial" style="font-size: 9px;" color="black"><?php echo $data->alamatlokasi_rumahsakit ?> 46113 Telp. <?php echo $data->no_telp_profilrs ?></FONT>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD colspan="7" style="font-size: 12px;"  HEIGHT=2 style="border-bottom: 2px solid #000000; text-align: center; margin-right: 0px;"></TD>
                                </TR>
                                <TR>
                                    <TD colspan="3" VALIGN=MIDDLE >
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black"> No. Resep : <?php echo $modPenjualan->noresep; ?>
                                        </font>
                                    </TD>
                                    <TD colspan="2"  align="center"  VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black">
                                        <?php echo date('d-m-Y', strtotime($modPenjualan->tglresep)); ?>
                                        </font>
                                    </TD>
                                </TR>
                                <TR>
                                    <TD colspan="3" VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black"><?php echo $pasien->namadepan; ?> <?php custom_echo($pasien->nama_pasien,25); ?>/<?php echo $pasien->no_rekam_medik; ?><?php echo (!empty($pasien->pendaftaran_id) ? '/' . date('d-m-Y', strtotime($format->formatDateMediumForDB($pasien->tanggal_lahir))) : ""); ?>
                                        </font>
                                    </TD>
                                    <TD colspan="2"  style="font-size: 12px;" align="center"  VALIGN=MIDDLE>
                                        <FONT FACE="Arial" color="black">

                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" align="center" VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black">
                                        <B>Sehari <?php echo $obat->signa_oa; ?> <?php echo $obat->etiket_satuan; ?> <?php echo $obat->etiket_caramakan; ?> </b>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" align="center"  VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black">
                                        <B><?php echo $obat->etiket_keterangan; ?> </B>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;"  FACE="Arial" color="black">
                                        <B>Racikan</B>
                                        </font>
                                    </TD>
                                </TR>  
                                <TR>
                                    <TD colspan="5" VALIGN=MIDDLE>
                                        <FONT style="font-size: 12px;" FACE="Arial" color="black">
                                        <?php foreach ($obatRacikan['children'] as $key => $dataObat) { ?>
                                            <?php echo $dataObat->obatalkes->obatalkes_nama . (($key != count($obatRacikan['children']) - 1) ? ', ' : ''); ?>
                                        <?php } ?>
                                        </font>
                                    </TD>
                                </TR> 
                                <TR>
                                    <TD colspan="6" HEIGHT=2 style="border-bottom: 2px solid #000000"></TD>
                                </TR>
                                <TR>
                                    <TD colspan="5" style="font-size: 12px;" align="center"  VALIGN=MIDDLE>
                                        <FONT FACE="Arial" color="black">
                                        Semoga Lekas Sembuh Terima Kasih
                                        </font>
                                    </TD>
                                </TR> 
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
<?php } ?>