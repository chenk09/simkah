<?php $this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency'
));?>
<?php $this->widget('application.extensions.moneymask.MMask', array(
    'element' => '.numbersOnly',
    'config' => array(
        'defaultZero' => true,
        'allowZero' => true,
        'decimal' => '.',
        'thousands' => '',
        'precision' =>0,
    )
));?>
<?php $this->renderPartial('/_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien)); ?>
<iframe id="framePrint" src="<?php echo $url; ?>" style="height:0px;"></iframe>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
    $ruangan_id = Yii::app()->user->getState('ruangan_id');
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
        'id'=>'penjualanresep-form',
        'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
    ));
?>
<fieldset>
    <legend class="rim">Data Resep</legend>
    <table>
        <tr>
            <td>

                <?php echo $form->hiddenField($modPenjualan,'reseptur_id',array('readonly'=>true)); ?>
                <?php echo $form->textFieldRow($modReseptur,'tglreseptur',array('readonly'=>true)); ?>
                <?php echo $form->textFieldRow($modReseptur,'noresep',array('readonly'=>true)); ?>

                <!--DITAMBAHKAN UNTUK INFORMASI DOKTER CREATED:JANG WAHYU 23-04-2013-->
                <div class="control-group">
                    <?php echo $form->labelEx($modReseptur,'pegawai_id', array('class'=>'control-label')); ?>
                    <div class="controls">
                        <?php echo CHtml::activeHiddenField($modReseptur,'pegawai_id'); ?>
                        <?php //echo CHtml::hiddenField('ygmengajukan_id'); ?>
                        <div style="float:left;">
                            <?php
                            $this->widget('MyJuiAutoComplete',array(
                                'model'=>$modReseptur,
                                'attribute'=>'dokter',
                                'sourceUrl'=>  Yii::app()->createUrl('ActionAutoComplete/ListDokter'),
                                'options'=>array(
                                    'showAnim'=>'fold',
                                    'minLength'=>2,
                                    'select'=>'js:function( event, ui ) {
                                        $("#FAResepturT_pegawai_id").val(ui.item.pegawai_id);
                                    }',
                                ),
                                'tombolDialog'=>array('idDialog'=>'dialogDokter'),
                                'htmlOptions'=>array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','style'=>'float:left;')
                            ));
                            ?>
                        </div>
                    </div>
                </div>
                <!--AKHIR PENAMBAHAN-->

                <?php echo $form->textFieldRow($modPenjualan,'discount',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true,'onkeyup'=>'hitungDiskon();')); ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'tglpenjualan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$modPenjualan,
                            'attribute'=>'tglpenjualan',
                            'mode'=>'datetime',
                            'options'=> array(
                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'maxDate' => 'd',
                                'yearRange'=> "-60:+0",
                            ),
                            'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPenjualan,'jenispenjualan',array('readonly'=>true)); ?>
                <div class="row-fluid">
                    <div class="control-group">
                        <label class="control-label"><?php echo $form->labelEx($modPenjualan,'noresep'); ?></label>
                        <div class="controls">
                            <?php echo $form->textField($modReseptur,'noresep_depan',array(
                                'readonly'=>true,
                                'class'=>'span2',
                            )); ?>
                            <?php echo $form->textField($modReseptur,'noresep_belakang',array(
                                'readonly'=>(!$modPenjualan->isNewRecord),
                                'class'=>'span2',
                                'onkeypress'=>"return $(this).focusNextInputField(event)"
                            )); ?>
                        </div>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'lamapelayanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPenjualan,'lamapelayanan',array('class'=>'inputFormTabel lebar3','readonly'=>false)); ?> Menit
                    </div>
                </div>

            </td>
        </tr>
    </table>
</fieldset>

<table id="tableObat" class="table table-condensed table-bordered">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Recipe</th>
        <th>R ke</th>
        <th>Nama Obat</th>
        <th>Asal Barang</th>
        <th>Satuan Kecil</th>
        <th>Qty</th>
        <th>Harga</th>
        <th>Discount</th>
        <th>Sub Total</th>
        <th>Etiket</th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php
    if((isset($obatAlkes)))
    {
        if (count($obatAlkes) > 0)
        {
            $tr = '';
            foreach ($obatAlkes as $key => $value)
            {
                $satuan = SatuankecilM::model()->findByPk($value->satuankecil_id);
                $tr .= '<tr id="tr_'.$key.'">
                    <td rowspan="2">' . ($key + 1) .CHtml::hiddenField('penjualanResep['.$key.'][detailreseptur_id]', true, array(
                        'readonly'=>true, 'class'=>'inputFormTabel lebar2'
                    )).'</td>
                    <td rowspan="2">'.$value->r.'</td>
                    <td rowspan="2">'.$value->rke.'</td>'.
                    '<td rowspan="2">'.$value->obatalkes->obatalkes_nama.'</td>'.
                    '<td rowspan="2">'.$value->sumberdana_id.'</td>'.
                    '<td rowspan="2">'.$satuan->satuankecil_nama.'</td>'.
                    '<td style="text-align: right">'.number_format($value->qty_oa).'</td>'.
                    '<td style="text-align: right">'.number_format($value->hargasatuan_oa).'</td>'.
                    '<td style="text-align: right">'.number_format($value->discount).'</td>'.
                    '<td style="text-align: right">'.number_format($value->hargajual_oa).'</td>'.
                    '<td>'.$value->etiket.'</td>'.
                    '<td>-</td>
                   </tr><tr><td colspan="6" style="border-left:1px solid #ddd;font-style: italic;">'.$value->signa_oa.'</td></tr>';
            }
        }
        echo $tr;
    }else {
        ?>
        <?php
        $tempRke = '';
        $totHargaNetto = 0;
        $totHargaSatuan = 0;
        $totHargaJual = 0;
        $totDiscount = 0; $totTarifService = 0; $totJasaDokter = 0; $tarifService = 0;
        foreach ($modDetailResep as $i => $detailResep)
        {
            $totHargaSatuan = $totHargaSatuan + $detailResep->hargasatuan_reseptur * $detailResep->qty_reseptur;
            $totHargaNetto = $totHargaNetto + $detailResep->harganetto_reseptur * $detailResep->qty_reseptur;
            $totHargaJual = $totHargaJual + $detailResep->hargajual_reseptur;
            $qtyStok = StokobatalkesT::getStokBarang($detailResep->obatalkes_id, $ruangan_id);
            $vClassDuplicat = '';
            if($qtyStok == 0)
            {
                $vClassDuplicat = 'label-important';
            }

            //$totHargaJual = $totHargaJual + $detailResep->hargajual_reseptur * $detailResep->qty_reseptur;
            ?>
            <tr class="<?=$detailResep->obatalkes_id?> <?=$vClassDuplicat?>">
                <td rowspan="2">
                    <?php echo CHtml::checkBox("penjualanResep[$i][detailreseptur_id]", true, array('uncheckValue'=>'0','value'=>$detailResep->resepturdetail_id,'class'=>'cek')) ?>
                    <?php //echo $detailResep->resepturdetail_id ?>
                </td>
                <td rowspan="2">
                    <?php
                        if($tempRke != $detailResep->rke)
                        {
                            $r = $detailResep->r;
                            if($detailResep->racikan_id == Params::DEFAULT_RACIKAN_ID){
                                if($detailResep->obatalkes->obatalkes_kategori == 'GENERIK'){
                                    $tarifService = 0 ;
                                }else{
                                    $tarifService = $racikan->tarifservice;
                                }
                                $tarifService = $tarifService;
                                $biayaKemasan = $racikan->biayakemasan;
                                $totTarifService = $totTarifService + $tarifService;
                            } else if($detailResep->racikan_id == Params::DEFAULT_NON_RACIKAN_ID){
                                if($detailResep->obatalkes->obatalkes_kategori == 'GENERIK'){
                                    $tarifService = 0 ;
                                }else{
                                    $tarifService = $nonRacikan->tarifservice;
                                }
                                $tarifService = $tarifService;
                                $biayaKemasan = $nonRacikan->biayakemasan;
                                $totTarifService = $totTarifService + $tarifService;
                            }
                            echo CHtml::hiddenField("penjualanResep[$i][biayakemasan]", $biayaKemasan, array('readonly'=>true,'class'=>'inputFormTabel lebar2'));
                        } else {
                            $r = $detailResep->r;
                            $tarifService = 0;
                            echo CHtml::hiddenField("penjualanResep[$i][biayakemasan]", 0, array('readonly'=>true,'class'=>'inputFormTabel lebar2'));
                        }
                        $jasaDokter = $detailResep->hargajual_reseptur * MyFunction::calculate_string($konfigFarmasi->formulajasadokter);
                        $totJasaDokter = $totJasaDokter + $jasaDokter;
                    ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][jasadokterresep]", $jasaDokter, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][biayaservice]", $tarifService, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][r]", $r, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo $r; ?>
                </td>
                <td rowspan="2">
                    <?php echo $detailResep->rke; ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][rke]", $detailResep->rke, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                </td>
                <td rowspan="2">
                    <?php echo CHtml::hiddenField("penjualanResep[$i][racikan_id]", $detailResep->racikan_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][kekuatan_oa]", $detailResep->kekuatan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][satuankekuatan_oa]", $detailResep->satuankekuatan, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                    <?php echo $detailResep->obatalkes->obatalkes_nama; ?> ( Stock = <?php echo $qtyStok; ?> )
                    <?php echo CHtml::hiddenField("penjualanResep[$i][obatalkes_id]", $detailResep->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                </td>
                <td rowspan="2">
                    <?php echo $detailResep->sumberdana->sumberdana_nama; ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][sumberdana_id]", $detailResep->sumberdana_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                </td>
                <td rowspan="2">
                    <?php echo $detailResep->satuankecil->satuankecil_nama; ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][satuankecil_id]", $detailResep->satuankecil_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) ?>
                </td>
                <td>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][permintaan_reseptur]", $detailResep->permintaan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][jmlkemasan_reseptur]", $detailResep->jmlkemasan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][qty]", $detailResep->qty_reseptur, array('readonly'=>true, 'id'=>'defaultqty','class'=>'inputFormTabel numbersOnly lebar2 qty2')) ?>
                    <?php echo CHtml::textField("penjualanResep[$i][qty]", $detailResep->qty_reseptur, array(
                        'onkeyup'=>'hitungTotal(this);',
                        'class'=>'inputFormTabel numbersOnly lebar2 qty'
                    )) ?>
                </td>
                <td>
                    <?php echo CHtml::textField("penjualanResep[$i][hargasatuan_reseptur]", $detailResep->hargasatuan_reseptur, array('readonly'=>true,'class'=>'currency inputFormTabel lebar2 ')) ?>
                </td>
                <td>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][permintaan_reseptur]", $detailResep->permintaan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][jmlkemasan_reseptur]", $detailResep->jmlkemasan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][discount]", 0, array('readonly'=>true, 'id'=>'defaultdiscount','class'=>'inputFormTabel lebar2 discount')) ?>
                    <?php echo CHtml::textField("penjualanResep[$i][discount]",'',array('readonly'=>true, 'class'=>'inputFormTabel numbersOnly lebar2 disc')) ?>
                </td>
                <td>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][hargasatuan_reseptur]", $detailResep->hargasatuan_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2 harganetto')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][harganetto_reseptur]", $detailResep->harganetto_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2 ')) ?>
                    <?php echo CHtml::textField("penjualanResep[$i][hargajual_reseptur]", $detailResep->hargajual_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargareseptur')) ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][hargajual]", $detailResep->hargajual_reseptur, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargasatuanobat')) ?>
                </td>
                <td>
                    <?php echo $detailResep->etiket; ?>
                    <?php echo CHtml::hiddenField("penjualanResep[$i][etiket]", $detailResep->etiket, array('readonly'=>true)) ?>
                </td>
                <td rowspan="2"><a onclick="removeObat(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a></td>
            </tr>
            <tr class="<?=$detailResep->obatalkes_id?>">
                <td colspan="5" style="border-left:1px solid #ddd">
                    <?php echo CHtml::textArea("penjualanResep[$i][signa_reseptur]", $detailResep->signa_reseptur, array(
                        'style'=>'width:100%'
                    )) ?>
                </td>
            </tr>
            <?php
            $tempRke = $detailResep->rke;

//            echo $totHargaNetto.' | '.$totHargaJual.'<br/>';
            $totDiscount = $totDiscount + $detailResep->obatalkes->discount;
        }
//        $modPenjualan->totharganetto = Yii::app()->numberFormatter->formatCurrency($totHargaNetto,'');
        $modPenjualan->totharganetto = $totHargaNetto;
        $modPenjualan->totalhargajual = $totHargaJual;
        $modPenjualan->totaltarifservice = $totTarifService;
        $modPenjualan->jasadokterresep = $totHargaJual * (($konfigFarmasi->formulajasadokter == 0 ) ? 0 : MyFunction::calculate_string($konfigFarmasi->formulajasadokter));
//        echo $totDiscount;
//        echo $totTarifService;
    }
    ?>
    </tbody>
</table>

<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Takaran Pembelian','', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo CHtml::dropDownList('takaranPembelian', '', Params::listTakaran(),array('disabled'=>false,'onChange'=>'get_qty();','class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    <?php echo CHtml::hiddenField('takaran', '',array('disabled'=>false,'onkeyup'=>'get_qty();','class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'biayaadministrasi',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'biayakonseling',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'totharganetto',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totalhargajual',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'jasadokterresep',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totaltarifservice',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
        </td>
    </tr>
</table>

<div class="form-actions">
    <?php
    if((!isset($obatAlkes)))
    {
        if(!$modPenjualan->isNewRecord){
            echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'disabled'=>true));
        } else {
            echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)'));
        }
    }else{
        echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array(
            '{icon}'=>'<i class="icon-print icon-white"></i>')
        ),array(
            'class'=>'btn btn-primary',
            'disabled'=>false,
            'type'=>'button',
            'onclick'=>'print(\'PRINT\')'
        ))."&nbsp&nbsp";
        echo CHtml::htmlButton("<i class=\"icon-print icon-white\"></i> Print Resep",array(
            'class'=>'btn btn-primary',
            'disabled'=>false,
            'type'=>'button',
            'onclick'=>'printCopyResep(\'PRINT\')'
        ))."&nbsp&nbsp";
    }
    ?>
</div>
<?php $this->endWidget(); ?>
<?php

$url =Yii::app()->createUrl($this->route);
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintReseptur&id='.$modReseptur->reseptur_id);
$urlPrintResep =  Yii::app()->createAbsoluteUrl($this->module->id.'/informasiResepPasien/printResep&idPenjualanResep='.$modReseptur->penjualanresep_id);

//$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintKwitansiPenjualan&id='.$modPenjualan->penjualanresep_id);
$urlGetTakaran =  Yii::app()->createUrl('actionAjax/getTakaran');
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
function printCopyResep(caraPrint)
{
    window.open("${urlPrintResep}&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
//function get_qty(obj){
//     takaran = $('#takaranPembelian').val();
//     jmltakaran = parseFloat($('#takaran').val());
//     var qty=new Array();
//    test = 1;
//     $('#tableObat tbody .qty2').each(function(){
//        idObat = $(this).parents("tr").find("input[name$='obatalkes_id']").val();
//        qty[test] = $(this).val();
//    });
//        $.post('${urlGetTakaran}', {takaran:takaran, jmltakaran:jmltakaran, qty:qty},function(data){
//             $(".qty").each(function(){
//                a = $(this).val(data.qty);
//            });
//        },'json');
//        $('#takaran').val(takaran);
//        
//}

function get_qty(obj){
     takaran = $('#takaranPembelian').val();
     $('#takaran').val(takaran);
     jmltakaran = parseInt($('#takaran').val());

      
    $('#tableObat tbody .qty').each(function(){
      qtyawal = parseFloat($(this).parents("tr").find(".qty2").val());
        qty2 = parseFloat($(this).parents("tr").find(".qty").val());
    qty = 0;
        if(qtyawal < qty2){
            qtyawal = qty2;
        }else{
            qtyawal = qtyawal;
        }
        if(jmltakaran == 1)
        {
           qty = (qtyawal/1).toFixed(2);    
        }else if(jmltakaran == 2){
             qty = parseFloat(qtyawal/2).toFixed(2);      
        }else if(jmltakaran == 3){
             qty = parseFloat(qtyawal/3).toFixed(2);    
        }
        $(this).val(qty);
    });  
    hitungQty();
}

function hitungQty(obj)
{
    jmltakaran = parseInt($('#takaran').val());
    $('#tableObat tbody .hargareseptur').each(function(){
    hargareseptur = 0;
        hargaobat = parseFloat($(this).parents("tr").find(".hargasatuanobat").val());
        qty = parseFloat($(this).parents("tr").find(".qty").val());
        qty2 = parseFloat($(this).parents("tr").find(".qty2").val());
          if(jmltakaran == 1)
        {
           hargareseptur = Math.ceil(((hargaobat) / 1));
        }else if(jmltakaran == 2){
             hargareseptur = Math.ceil(((hargaobat) / 2));         
        }else if(jmltakaran == 3){
             hargareseptur = Math.ceil(((hargaobat) / 3));       
        }
        $(this).val(hargareseptur);
//        hargareseptur = hargareseptur ; 
        $('#FAPenjualanResepT_totalhargajual').val(hargareseptur);
        $('.tothargaj').val(hargareseptur);
        
    });  
}

function hitungDiskon(){
    diskon = parseFloat($('#FAPenjualanResepT_discount').val());
    if (!jQuery.isNumeric(diskon)){
        diskon = 0;
    }else{
         diskon = diskon;
    }
    
    tothargajual = parseFloat($('.tothargaj').val());
    totalhargajual = parseFloat($('.tothargaj').val());
    
    if(tothargajual == 0){
        diskonhj = diskonhj = Math.ceil(totalhargajual - (totalhargajual * (diskon / 100)));
    }else{
        diskonhj = Math.ceil(tothargajual - (tothargajual * (diskon / 100)));
    }
        
    
    $('#FAPenjualanResepT_totalhargajual').val(diskonhj);
}


JSCRIPT;

Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>

<?php
Yii::app()->clientScript->registerScript('onsubmitfunction','
        $("form").submit(function(){
            if ($(".cek").length < 1){
              alert("Detail harus diisi");
                return false;
            }
        });
',  CClientScript::POS_READY);
?>

<script type="text/javascript">
    function hitungTotal(obj)
    {
        $(".currency").maskMoney('unmasked');
        var administrasi = parseFloat(<?php echo $konfigFarmasi->administrasi; ?>);
        var qty = parseFloat(obj.value);

        var hargaSatuan = $(obj).parents('tr').find('input[name$="[hargasatuan_reseptur]"]').val();
        hargaSatuan = parseFloat(unformatNumber(hargaSatuan));
        var vSubTotal = (qty * hargaSatuan);
        $(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(vSubTotal);

        var totHargaJual = 0;
        $(obj).parents('table').find('input[name$="[hargajual_reseptur]"]').each(function(){
            var vHasil = unformatNumber(this.value)
            totHargaJual = totHargaJual + parseFloat(vHasil);
        });
        $('#FAPenjualanResepT_totalhargajual').val(totHargaJual);

        var totHargaNetto = 0;
        $(obj).parents('table').find('input[name$="[harganetto_reseptur]"]').each(function(){
            var qty = $(this).closest('tr').find('.qty').val();
            var vHarga = 0;
            if(qty > 0)
            {
                qty = parseFloat(qty);
                vHarga = (qty * parseFloat(this.value));
            }
            totHargaNetto = totHargaNetto + parseFloat(vHarga);
        });
        $('#FAPenjualanResepT_totharganetto').val(totHargaNetto);

        $('.tothargaj').val(totHargaJual);
        $(".currency").maskMoney("mask");
    }

    function diskonObat(obj)
    {
        var disc = parseFloat(obj.value);
        var qty =  parseFloat($(obj).parents('tr').find('.qty').val());
        var hargaSatuan = parseFloat($(obj).parents('tr').find('.hargasatuanobat').val());
        totalharga = Math.ceil(hargaSatuan-(hargaSatuan * (disc/100)));
//    totalharga = Math.ceil((hargaSatuan * qty )- (hargaSatuan * (disc / 100)));
        $(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(totalharga);
        var tothargadisc = 0;
        $(obj).parents('table').find('input[name$="[hargajual_reseptur]"]').each(function(){
            tothargadisc = tothargadisc + parseFloat(this.value);
        });
        $('#FAPenjualanResepT_totalhargajual').val(tothargadisc);
        $('.tothargaj').val(tothargadisc);
    }

</script>

<script>

    function PrintReseptur(caraPrint)
    {
        //window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/PrintReseptur', array('id'=>$modReseptur->reseptur_id, "caraPrint"=>"PRINT")); ?>',"", 'left=100,top=100,width=400,height=400,scrollbars=1');
        $("#framePrint").attr("src",'<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/PrintReseptur', array('id'=>$modReseptur->reseptur_id, "caraPrint"=>"PRINT")); ?>');
    }
</script>
<?php
//if($successSave){
//Yii::app()->clientScript->registerScript('tutupDialog',"
//window.parent.setTimeout(\"$('#dialogPenjualanResep').dialog('close')\",1500);
//window.parent.$.fn.yiiGridView.update('pencarianpasien-grid', {
//		data: $('#caripasien-form').serialize()
//});
//",  CClientScript::POS_READY);
//} 
//===============Dialog buat pegawai
$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM('search');
$modPegawai->unsetAttributes();
if(isset($_GET['PegawaiM'])){
    $modPegawai->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengajukan-m-grid',
    'dataProvider'=>$modPegawai->search(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"$(\"#FAResepturT_pegawai_id\").val(\"$data->pegawai_id\");
                            $(\"#'.CHtml::activeId($modReseptur,'dokter').'\").val(\"$data->nama_pegawai\");
                            $(\"#dialogDokter\").dialog(\"close\");
                            return false;"
                ))'
        ),

        'gelardepan',
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai',
        'jeniswaktukerja',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
$this->endWidget();
?>

<script type="text/javascript">
    $(function() {
        setTimeout(function () {
            konversiFormat()
        }, 100)
    });
    function konversiFormat()
    {
        $(".currency").maskMoney('unmasked');
        $(".currency").maskMoney("mask");
    }

    function removeObat(obj)
    {
        if(confirm('Apakah anda akan menghapus obat?'))
        {
            var vIdTr = $(obj).closest('tr').attr('class');
            $(obj).parent().parent().remove();
            var idObat = $(obj).closest('tr').find('input[name="obat[]"]').val();
            $('.' + vIdTr).remove();
        }
    }
    $(document).on('submit', "#penjualanresep-form", function (event){
        $(".currency").maskMoney('unmasked');
        $('.currency').each(function(){
            this.value = unformatNumber(this.value)
        });
        return true;
    })
</script>
