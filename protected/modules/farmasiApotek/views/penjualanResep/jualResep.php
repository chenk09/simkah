<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0
    )
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.numbersOnly',
    'config'=>array(
        'defaultZero'=>false,
        'allowZero'=>false,
        'precision'=>0,
        'allowDecimal'=>false,
        'decimal'=>false,
    )
));
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>1,
    )
));

$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number0',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'penjualanresep-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#FAPendaftaranT_instalasi_id',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)',
            'onsubmit'=>'return cekInput();'
        ),
));?>
<?php  
if(isset($_GET['sukses'])){
    $sukses = $_GET['sukses'];
}
if($sukses > 0) {
    echo "<script>print('PRINT');</script>";
    
}
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php echo $form->errorSummary($modPenjualan); ?>
<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien,)); ?>

<fieldset>
    <legend class="rim">Data Resep</legend>
    <table >
        <tr>
            
        <?php
                echo $form->hiddenField($modPenjualan,'penjualanresep_id',array('readonly'=>true));
            ?>
            <td>
                <?php //echo $form->textFieldRow($modReseptur,'tglreseptur',array('readonly'=>false)); ?>
                <div class="control-group ">
                    <?php echo $form->labelEx($modReseptur,'tglreseptur', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modReseptur,
                                        'attribute'=>'tglreseptur',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                            'yearRange'=> "-60:+0",
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'style'=>'width:128px;','onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                    </div>
                </div>
                <?php // echo $form->textFieldRow($modReseptur,'noresep',array('readonly'=>TRUE)); ?>
                <div class="control-group">
                    <label class="control-label"><?php echo $form->labelEx($modReseptur,'noresep'); ?></label>
                    <div class="controls">
                        <?php echo $form->textField($modReseptur,'noresep_depan',array('readonly'=>true, 'style'=>'width:170px;')); ?><br>
                        <?php echo $form->textField($modReseptur,'noresep_belakang',array('readonly'=>(!$modReseptur->isNewRecord), 'style'=>'width:180px;', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>
<!--DITAMBAHKAN UNTUK INFORMASI DOKTER CREATED:JANG WAHYU 23-04-2013-->
                <div class="control-group">  
                <?php echo $form->labelEx($modReseptur,'pegawai_id', array('class'=>'control-label')); ?> 
                <div class="controls">
                    <?php echo CHtml::activeHiddenField($modReseptur,'pegawai_id'); ?>
                    <?php //echo CHtml::hiddenField('ygmengajukan_id'); ?>
                        <div style="float:left;">
                            <?php
                                $this->widget('MyJuiAutoComplete',array(
                                    'model'=>$modReseptur,
                                    'attribute'=>'dokter',
                                    'sourceUrl'=>  Yii::app()->createUrl('ActionAutoComplete/ListDokter'),
                                    'options'=>array(
                                        'showAnim'=>'fold',
                                        'minLength'=>2,
                                        'select'=>'js:function( event, ui ) {
                                                $("#FAResepturT_pegawai_id").val(ui.item.pegawai_id);
                                                    }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogDokter'),
                                    'htmlOptions'=>array('value'=>$modReseptur->getDokter(),'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span3','style'=>'float:left;')
                                ));
                            ?>
                        </div>
                </div>          
                </div>
<!--AKHIR PENAMBAHAN-->

                <?php 
                    echo $form->textFieldRow($modPenjualan,'discount',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true,'onkeyup'=>'hitungDiskonSemua();', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                ?>
            </td>
            <td style="padding-right:160px;">
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'tglpenjualan', array('class'=>'control-label')) ?>
                    <div class="controls">
                    <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modPenjualan,
                                        'attribute'=>'tglpenjualan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                            'yearRange'=> "-60:+0",
                                        ),
                                        'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'style'=>'width:128px;', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                    )); ?>
                    </div>
                </div>
                <?php echo $form->textFieldRow($modPenjualan,'jenispenjualan',array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                
                <div class="control-group ">
                    <?php echo $form->labelEx($modPenjualan,'lamapelayanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->textField($modPenjualan,'lamapelayanan',array('class'=>'inputFormTabel lebar3 numbersOnly','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?> Menit
                    </div> 
                </div>
                 
            </td>
        </tr>
    </table>
</fieldset>

<?php $this->renderPartial('_formInputObat',array('form'=>$form,'racikan'=>$racikan, 'racikanDetail'=>$racikanDetail,'nonRacikan'=>$nonRacikan,'konfigFarmasi'=>$konfigFarmasi, 'tanggungan'=>$tanggungan)); ?>

<?php if (isset($obatAlkes)) {
    echo  $form->errorSummary($obatAlkes);
}?>
<table id="tblDaftarResep" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Recipe</th>
            <th>R ke</th>
            <th>Kode / Nama Obat</th>
            <th>Asal Barang</th>
            <th>Satuan Kecil</th>
            <th>Qty</th>
            <th>Stok</th>
            <th>Harga</th>
            <th>Discount (%)</th>
            <th>Sub Total</th>
            <th>Signa</th>
            <th>Etiket</th>
            <?php echo (($modPenjualan->isNewRecord) ? '<th>Batal</th>' : ''); ?>
            <!--<th>&nbsp;</th>-->
        </tr>
    </thead>
    <tbody>
        <?php
        if((isset($obatAlkes))){
            if (count($obatAlkes) > 0){
                $tr = '';
                foreach ($obatAlkes as $key => $value) {
                    $tr .= '<tr id="tr_'.$key.'">'
                    .'<td>'.(($modPenjualan->isNewRecord == false) ? CHtml::checkBox('penjualanResep['.$key.'][pilihObat]', true, array('class'=>'inputFormTabel lebar2')). CHtml::hiddenField('penjualanResep['.$key.'][obatalkespasien_id]', $value->obatalkespasien_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')) : '').'</td>
                    <td>'.CHtml::hiddenField('penjualanResep['.$key.'][r]', $value->r, array('readonly'=>true, 'class'=>'inputFormTabel lebar2')).
                            $value->r.
                          CHtml::hiddenField('penjualanResep['.$key.'][isRacikan]', ((empty($value->r)) ? 0 : 1), array()).
                          '</td>
                    <td>'.CHtml::textField('penjualanResep['.$key.'][rke]', $value->rke, array('readonly'=>true,'style'=>'width:15px;')).
                           CHtml::hiddenField('penjualanResep['.$key.'][biayakemasan]', $value->biayakemasan, array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) . 
                           CHtml::hiddenField('penjualanResep['.$key.'][biayaservice]', $value->biayaservice, array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) . 
                           CHtml::hiddenField('penjualanResep['.$key.'][jasadokterresep]', $value->jasadokterresep, array('readonly'=>true, 'class'=>'inputFormTabel lebar2')) .'</td>'.
                    '<td>'.CHtml::hiddenField('penjualanResep['.$key.'][racikan_id]', $value->racikan_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) . 
                           CHtml::hiddenField('penjualanResep['.$key.'][kekuatan_oa]', $value->kekuatan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) .
                           CHtml::hiddenField('penjualanResep['.$key.'][satuankekuatan_oa]', $value->satuankekuatan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar1')) . 
                           $value->obatalkes->obatalkes_kode." - ".$value->obatalkes->obatalkes_nama.
                           CHtml::hiddenField('penjualanResep['.$key.'][obatalkes_id]', $value->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar1 obat')) .'</td>'.
                    '<td>'.CHtml::dropDownList('penjualanResep['.$key.'][sumberdana_id]', $value->sumberdana_id, CHtml::listData(SumberdanaM::model()->findAll(), 'sumberdana_id', 'sumberdana_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3')).'</td>'.
                    '<td>'.CHtml::dropDownList('penjualanResep['.$key.'][satuankecil_id]', $value->satuankecil_id, CHtml::listData(SatuankecilM::model()->findAll(), 'satuankecil_id', 'satuankecil_nama'),array('empty'=>'-- Pilih --','class'=>'inputFormTabel lebar3')).'</td>'.
                    
                    '<td>'.CHtml::textField('penjualanResep['.$key.'][qty]', $value->qty_oa, array('onblur'=>'pembulatanKeAtas(this); hitungTotal(this);','onkeyup'=>'get_qty_a(this);','class'=>'inputFormTabel span1 numbersOnly qty')) . 
                    
                           CHtml::hiddenField('penjualanResep['.$key.'][qty_a]', $value->qty_oa, array('class'=>'inputFormTabel span1 numbersOnly qty2')).
                           CHtml::hiddenField('penjualanResep['.$key.'][permintaan_reseptur]', $value->permintaan_oa, array('readonly'=>true,'class'=>'inputFormTabel span1')) . 
                           CHtml::hiddenField('penjualanResep['.$key.'][jmlkemasan_reseptur]', $value->jmlkemasan_oa, array('readonly'=>true,'class'=>'inputFormTabel span1')) .'</td>'.
                    '<td>'.CHtml::textField('penjualanResep['.$key.'][stok]', StokobatalkesT::getStokBarang($value->obatalkes_id,Yii::app()->user->getState('ruangan_id')), array('readonly'=>true,'class'=>'inputFormTabel span1')) .'</td>'.
                    '<td>'.CHtml::textField('penjualanResep['.$key.'][hargasatuan_reseptur]', $value->hargasatuan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargareseptur')).'</td>'.
                           CHtml::hiddenField('penjualanResep['.$key.'][hargasatuanjual_oa]', $value->hargasatuanjual_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2')).CHtml::hiddenField('penjualanResep['.$key.'][hargasatuan_reseptur_a]', $value->hargasatuan_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hargasatuanobat')).'</td>'.
                           CHtml::hiddenField('penjualanResep['.$key.'][hpp]', $value->hpp, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency hpp')).'</td>'.
//                           CHtml::hiddenField('penjualanResep['.$key.'][disc]', 0, array('class'=>'inputFormTabel span1 numbersOnly','onkeyup'=>'hitungTotalSemua();','onblur'=>'hitungTotalSemua();',)).
                    '<td>'.CHtml::textField('penjualanResep['.$key.'][disc]', $value->discount, array('class'=>'inputFormTabel span1 numbersOnly','onkeyup'=>'hitungTotalSemua();','onblur'=>'hitungTotalSemua();',)).'</td>'.
                    '<td>'.CHtml::hiddenField('penjualanResep['.$key.'][harganetto_reseptur]', $value->harganetto_oa, array('readonly'=>true,'class'=>'inputFormTabel span1')).
                           CHtml::textField('penjualanResep['.$key.'][hargajual_reseptur]', $value->hargajual_oa, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).
                           CHtml::hiddenField('penjualanResep['.$key.'][subsidirs]', $value->subsidirs, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).
                           CHtml::hiddenField('penjualanResep['.$key.'][subsidipemerintah]', $value->subsidipemerintah, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).
                           CHtml::hiddenField('penjualanResep['.$key.'][subsidiasuransi]', $value->subsidiasuransi, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).
                           CHtml::hiddenField('penjualanResep['.$key.'][iurbiaya]', $value->iurbiaya, array('readonly'=>true,'class'=>'inputFormTabel lebar2 currency')).
                            '</td>'.
                    '<td>'.CHtml::textField('penjualanResep['.$key.'][signa_reseptur]', $value->signa_oa, array('placeholder'=>'-- Aturan Pakai --','class'=>'inputFormTabel span2')).'</td>'.
                    '<td>'.CHtml::dropDownList('penjualanResep['.$key.'][etiket]', (empty($value->etiket)) ? Params::DEFAULT_ETIKET : $value->etiket, Etiket::items(),array('class'=>'inputFormTabel span2')).'</td>'.
                    (($modPenjualan->isNewRecord) ? '<td><a onclick="removeObat(this);return false;" rel="tooltip" href="javascript:void(0);" data-original-title="Klik untuk menghapus Obat"><i class="icon-remove"></i></a></td>' : '').
                    '</tr>';
                }
            }
            echo $tr;
        }
        ?>
    </tbody>
</table>
<table>
    <tr>
        <td>
            <div class="control-group ">
                    <?php echo CHtml::label('Takaran Pembelian','', array('class'=>'control-label')) ?>
                    <div class="controls">
                     <?php echo CHtml::dropDownList('takaranPembelian', '', Params::listTakaran(),array('disabled'=>false,'onChange'=>'get_qty();','class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                     <?php echo CHtml::hiddenField('takaran', '',array('disabled'=>false,'onkeyup'=>'get_qty();','class'=>'inputFormTabel span1','onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                    </div>
                </div>
        </td>
    </tr>
    <tr>
        <?php
            //Menentukan Tanggungan Penjamin / Subsidi
            $subsidi = array();
            $subsidi['makstanggpel'] = 0;
            $subsidi['subsidirumahsakitoa'] = 0;
            $subsidi['subsidipemerintahoa'] = 0;
            $subsidi['subsidiasuransioa'] = 0;
            $subsidi['iurbiayaoa'] = 0;
            if(isset($tanggungan)){
                if(!empty($tanggungan->tanggunganpenjamin_id)){
                    if($tanggungan->subsidirumahsakitoa > 0){
                        $subsidi['subsidirumahsakitoa'] = $tanggungan->subsidirumahsakitoa;
                    }
                    if($tanggungan->subsidipemerintahoa > 0){
                        $subsidi['subsidipemerintahoa']  = $tanggungan->subsidipemerintahoa;
                    }
                    if($tanggungan->subsidiasuransioa > 0){
                        $subsidi['subsidiasuransioa'] = $tanggungan->subsidiasuransioa;
                    }
                    if($tanggungan->iurbiayaoa > 0){
                        $subsidi['iurbiayaoa'] = $tanggungan->iurbiayaoa;
                    }
                    if($tanggungan->makstanggpel > 0){
                        $subsidi['makstanggpel'] = $tanggungan->makstanggpel;
                    }
                }
            }
            ?>
        <?php echo CHtml::hiddenField('subsidirspersen', $subsidi['subsidirumahsakitoa'], array('readonly'=>true)) ?>
        <?php echo CHtml::hiddenField('subsidipemerintahpersen', $subsidi['subsidipemerintahoa'], array('readonly'=>true)) ?>
        <?php echo CHtml::hiddenField('subsidiasuransipersen', $subsidi['subsidiasuransioa'], array('readonly'=>true)) ?>
        <?php echo CHtml::hiddenField('iurbiayapersen', $subsidi['iurbiayaoa'], array('readonly'=>true)) ?>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'biayaadministrasi',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'tambahRekeningAdmin();')); ?>
            <?php echo $form->textFieldRow($modPenjualan,'biayakonseling',array('class'=>'inputFormTabel lebar3 currency','readonly'=>false, 'onkeypress'=>"return $(this).focusNextInputField(event)",'onblur'=>'tambahRekeningKonseling();')); ?>            
            <div class="control-group">
                <label class="control-label">Maks. Tanggungan</label>
                <div class="controls">
                    <?php echo CHtml::textField('makstanggpel', $subsidi['makstanggpel'], array('readonly'=>true, 'class'=>'inputFormTabel lebar3 currency', 'onkeypress'=>"return $(this).focusNextInputField(event)")) ?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'totharganetto',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totalhargajual',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>            
            <?php echo $form->hiddenField($modPenjualan,'totalhargajual',array('class'=>'inputFormTabel lebar3 currency totalhargajual','readonly'=>true)); ?>            
            <div class="control-group">
                <label class="control-label">Total Tanggungan</label>
                <div class="controls">
                    <?php echo CHtml::textField('totalTanggungan', 0,array('class'=>'inputFormTabel lebar3 currency','readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")) ;?>
                </div>
            </div>
        </td>
        <td>
            <?php echo $form->textFieldRow($modPenjualan,'jasadokterresep',array('class'=>'inputFormTabel lebar3 currency','readonly'=>true)); ?>
            <?php echo $form->textFieldRow($modPenjualan,'totaltarifservice',array('class'=>'inputFormTabel lebar3 currency','readonly'=>false,'onblur'=>'tambahRekeningService();')); ?>
        </td>
    </tr>
</table>
<br>
<?php
//FORM REKENING
    $this->renderPartial('farmasiApotek.views.penjualanResep.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
    <?php 
        $disableSave = false;
        $disableSave = (!empty($_GET['id'])) ? true : ($sukses > 0) ? true : false;; 
    ?>
    <?php $disablePrint = ($disableSave) ? false : true; ?>
    <div class="form-actions">
            
            <?php 
//                if($modPenjualan->isNewRecord){
//                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
//                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onClick'=>'submitForm();return false;',
//                        'onKeypress'=>'return formSubmit(this,event)', 'disabled'=>$disableSave));
                
                echo CHtml::htmlButton(
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array(
                            'class'=>'btn btn-primary', 
                            'type'=>'submit', 
                            'onClick'=>'submitForm();return false;'
                        )
                    );
//                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'return cekDokter();','onKeypress'=>'return formSubmit(this,event)', 'disabled'=>$disableSave));
//            }else{
//                echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
//                                    array('class'=>'btn btn-primary', 'type'=>'submit', 'disabled'=>true,'onKeypress'=>'return formSubmit(this,event)'));
//            }
                 ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'id'=>'resetbtn')); ?>								
            <?php  
//                $content = $this->renderPartial('../tips/transaksi',array(),true);
//                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
            <?php
//                if($modPenjualan->isNewRecord){
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'disabled'=>$disablePrint,'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";                 
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Print Etiket',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'disabled'=>$disablePrint,'type'=>'button','onclick'=>'printEtiket(\'PRINT\')'))."&nbsp&nbsp";                 
//                }else{
//                    echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'disabled'=>false,'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp";                 
//                }
//                
//                 DI HIDE SEMENTARA BY: ICHAN >>>    echo CHtml::htmlButton(Yii::t('mds','{icon} Copy Resep',array('{icon}'=>'<i class="icon-share icon-white"></i>')),
//                                    array('class'=>'btn btn-primary', 'type'=>'button', 'onclick'=>'if(confirm("Apakah anda hanya akan meng-copy resep saja ?")){copyResep();}','disabled'=>$disableSave));
            ?>
    </div>
<?php $this->endWidget(); ?>
<?php
$urlPrint=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintKwPenjualanResep&id='.$modPenjualan->penjualanresep_id);
$urlPrintEtiket=  Yii::app()->createAbsoluteUrl($this->module->id.'/'.$this->id.'/PrintEtiket&id='.$modPenjualan->penjualanresep_id);
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}
function printEtiket(caraPrint)
{
    window.open("${urlPrintEtiket}&caraPrint="+caraPrint,"",'location=_new, width=980px');
}

JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Pegawai',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPegawai = new PegawaiM('searchByDokter');
$modPegawai->unsetAttributes();
if(isset($_GET['PegawaiM'])){
    $modPegawai->attributes = $_GET['PegawaiM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
    'id'=>'pegawaiYangMengajukan-m-grid',
    'dataProvider'=>$modPegawai->searchByDokter(),
    'filter'=>$modPegawai,
    'template'=>"{pager}{summary}\n{items}",
    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'columns'=>array(
        array(
            'header'=>'Pilih',
            'type'=>'raw',
            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn_small",
                "id"=>"selectPegawai",
                "onClick"=>"$(\"#FAResepturT_pegawai_id\").val(\"$data->pegawai_id\");
                            $(\"#'.CHtml::activeId($modReseptur,'dokter').'\").val(\"$data->nama_pegawai\");
                            $(\"#dialogDokter\").dialog(\"close\");
                            return false;"
                ))'
        ),
        
        'gelardepan',
        'nama_pegawai',
        'jeniskelamin',
        'nomorindukpegawai',
        'jeniswaktukerja',
    ),
    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));
        
$this->endWidget();
?>
<?php 
// Dialog buat Copy Resep =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogCopyResep',
    'options'=>array(
        'title'=>'Copy Resep',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>false,
    ),
));
?>
<iframe src="" name="iframeCopyResep" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
//========= end Copy Resep dialog =============================
?>
<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Data Transaksi Penjualan Obat Resep RS',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>550,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Pasien</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Tgl Resep</td>
                    <td width="250" tag="tgl_resep"></td>
                    <td width="150">Tgl Penjualan</td>
                    <td tag="tgl_penjualan"></td>
                </tr>
                <tr>
                    <td>No Resep</td>
                    <td tag="noresep"></td>
                    <td>Jenis Penjualan</td>
                    <td tag="jenis_penjualan"></td>
                </tr>
                <tr>
                    <td width="150">No. Pendaftaran</td>
                    <td width="250" tag="no_pendaftaran"></td>
                    <td width="150">Nama Pasien</td>
                    <td tag="nama_pasien"></td>
                </tr>
                <tr>
                    <td>Instalasi</td>
                    <td tag="instalasi_nama"></td>
                    <td>No. Rekam Medis</td>
                    <td tag="no_rekam_medik"></td>
                </tr>
                <tr>
                    <td>Ruangan</td>
                    <td tag="ruangan_nama"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Obat Alkes</legend>
            <table id="info_tindakan_temp" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="50">Asal Barang</th>
                        <th width="50">Satuan Kecil</th>
                        <th width="50">Recipe</th>
                        <th width="50">R ke</th>
                        <th width="300">Kode / Nama Obat</th>
                        <th width="50">Qty</th>
                        <th width="150">Harga</th>
                        <th width="150">Discount</th>                        
                        <th width="150">Signa</th>
                        <th width="150">Etiket</th>
                        <th>Total</th>
                    </tr>                    
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Pembayaran</legend>
            <table id="info_pembayaran_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Total Harga Jual</td>
                    <td width="250" tag="total_hargajual" class="curency"></td>
                    <td width="150">Biaya Konseling</td>
                    <td width="250" tag="biaya_konseling" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tanggungan</td>
                    <td width="250" tag="total_tanggungan" class="curency"></td>
                    <td width="150">Jasa Dokter Resep</td>
                    <td width="250" tag="jasa_dokter" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tarif Service</td>
                    <td width="250" tag="tarif_service" class="curency"></td>
                    <td width="150">Biaya Administrasi</td>
                    <td width="250" tag="biaya_adm" class="curency"></td>
                </tr>
                <tr>
                    <td width="150">Total Tagihan</td>
                    <td width="250" tag="total_tagihan" class="curency"></td>
                    <td width="150">&nbsp;</td>
                    <td width="250">&nbsp;</td>
                </tr>
            </table>
        </fieldset>        
    </div>  
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses(this);return false;',
                        'disabled'=>FALSE
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
<script type="text/javascript">
$('.currency').each(function(){this.value = formatDesimal(this.value)});

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#penjualanresep-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
    $('#penjualanresep-form').submit();
}    
function cekDokter(){
    dokter = $('#FAResepturT_dokter').val();
    if(dokter==''){
        alert('Silahkan isi nama dokter terlebih dahulu');
        $('#FAResepturT_dokter').focus();
        return false;
    }else{
        return true
    }
}

function submitForm()
{
    if(cekInputTindakan())
    {
        confirmPembayaran();
    }
}

function cekInputTindakan()
{
    dokter = $('#FAResepturT_dokter').val();
    if(dokter==''){
        alert('Silahkan isi nama dokter terlebih dahulu');
        $('#FAResepturT_dokter').focus();
        return false;
    }else{
        return true;
    }
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    var no_pendaftaran = $('#penjualanresep-form').find('input[name$="[no_pendaftaran]"]').val();
    var no_rekam_medik = $('#penjualanresep-form').find('input[name$="[no_rekam_medik]"]').val();
    var instalasi_nama = $('#penjualanresep-form').find('input[name$="[instalasi_nama]"]').val();
    var nama_pasien = $('#penjualanresep-form').find('input[name$="[nama_pasien]"]').val();
    var ruangan_nama = $('#penjualanresep-form').find('input[name$="[ruangan_nama]"]').val();
    var tgl_resep = $('#penjualanresep-form').find('input[name$="[tglreseptur]"]').val();
    var tgl_penjualan = $('#penjualanresep-form').find('input[name$="[tglpenjualan]"]').val();
    var noresep_depan = $('#penjualanresep-form').find('input[name$="[noresep_depan]"]').val();
    var noresep_belakang = $('#penjualanresep-form').find('input[name$="[noresep_belakang]"]').val();
    var lama_pelayanan = $('#penjualanresep-form').find('input[name$="[lamapelayanan]"]').val();
    var dokter = $('#penjualanresep-form').find('input[name$="[dokter]"]').val();
    var noresep = noresep_depan+''+noresep_belakang;
    var jenis_penjualan = $('#penjualanresep-form').find('input[name$="[jenispenjualan]"]').val();
    if(lama_pelayanan == ''){
        lama_pelayanan = '-';
    }else{
        lama_pelayanan = lama_pelayanan +' '+'Menit';
    }
    
    var jenis_penjualan = $('#penjualanresep-form').find('input[name$="[jenispenjualan]"]').val();    
    var total_harganetto = unformatNumber($('#penjualanresep-form').find('input[name$="[totharganetto]"]').val());
    var total_hargajual = unformatNumber($('#penjualanresep-form').find('input[name$="[totalhargajual]"]').val());
    
    var jasa_dokter = unformatNumber($('#FAPenjualanResepT_jasadokterresep').val());
    var biaya_adm = unformatNumber($('#FAPenjualanResepT_biayaadministrasi').val());
    var biaya_konseling = unformatNumber($('#FAPenjualanResepT_biayakonseling').val());
    var total_tanggungan = unformatNumber($('#totalTanggungan').val());
    var tarif_service = unformatNumber($('#FAPenjualanResepT_totaltarifservice').val());
    
    total_tagihan = total_hargajual + biaya_adm + biaya_konseling + tarif_service;
    var data_pembayaran = {
        'jenis_penjualan':jenis_penjualan,
        'total_harganetto':total_harganetto,
        'total_hargajual':total_hargajual,
        'jasa_dokter':jasa_dokter,
        'biaya_adm':biaya_adm,
        'total_tanggungan':total_tanggungan,
        'biaya_konseling':biaya_konseling,
        'total_tagihan':total_tagihan,
        'tarif_service':tarif_service
    }
    $('#content_confirm').find('#info_pembayaran_temp td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]);
                }
            }
        }
    );    
        
    var data_pasien = {
        'no_pendaftaran':no_pendaftaran,
        'no_rekam_medik':no_rekam_medik,
        'instalasi_nama':instalasi_nama,
        'nama_pasien':nama_pasien,
        'ruangan_nama':ruangan_nama,
        'tgl_resep':tgl_resep,
        'tgl_penjualan':tgl_penjualan,
        'noresep':noresep,
        'jenis_penjualan':jenis_penjualan
    }
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]);
                }
            }
        }
    );
    var tr_info = "";
    var total_hrg_obat = 0;
    $('#tblDaftarResep').find('tbody tr').each(
        function()
        {
            if($(this).attr('class') != 'trfooter')
            {
                var recipe = $(this).find('td input[name$="[r]"]').parent().text();
                var r_ke = $(this).find('td input[name$="[rke]"]').val();
                var nama_obat = $(this).find('td input[name$="[obatalkes_id]"]').parent().text();
                var qty = $(this).find('td input[name$="[qty]"]').val();
                var harga_satuan = $(this).find('td input[name$="[hargasatuan_reseptur]"]').val();
                var discount = $(this).find('td input[name$="[disc]"]').val();
                var signa = $(this).find('td input[name$="[signa_reseptur]"]').val();
                var etiket = $(this).find('select[name$="[etiket]"] option:selected').text();
                var sumberdana = $(this).find('select[name$="[sumberdana_id]"] option:selected').text();
                var satuankecil = $(this).find('select[name$="[satuankecil_id]"] option:selected').text();
                                
                total = harga_satuan * qty;
                tr_info += '<tr>';
                tr_info += '<td>'+ sumberdana +'</td><td>'+ satuankecil +'</td><td>'+ recipe +'</td><td>'+ r_ke + '</td><td>'+ nama_obat +'</td><td>'+ qty +'</td><td class="curency">'+ harga_satuan +'</td><td class="curency">'+ discount +'</td><td>'+ signa +'</td><td>'+ etiket +'</td><td class="curency">'+ total +'</td>'
                tr_info += '</tr>';
                total_hrg_obat += parseFloat(total);
            }

        }
    );
    
    $('#info_tindakan_temp').find('tbody').empty();
    tr_info += '<tr class="trfooter">';
    tr_info += '<td colspan="10">Total</td><td class="curency">'+ total_hrg_obat +'</td>';
    tr_info += '</tr>';
    $('#info_tindakan_temp').find('tbody').append(tr_info);
    
    $('#content_confirm').find('.curency').each(
        function()
        {
//            var result = formatNumber(parseInt($(this).text()));
//          JANGAN DIBULATKAN >>  var result = formatMoney($(this).text());
            var result = accounting.formatMoney($(this).text(),'',2,',','.');
            $(this).text(result);            
        }
    );
}

function hitungTotal(obj)
{
        
    var qty = parseFloat(obj.value);
    var hargaSatuan =  parseFloat($(obj).parents('tr').find('.hargareseptur').val());
    var hpp =  parseFloat($(obj).parents('tr').find('.hpp').val());
    var hargaJual = parseFloat($(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val());
    var disc = parseFloat($(obj).parents('tr').find('input[name$="[disc]"]').val());
    var persenJasaDokter = parseFloat($(obj).parents('tr').find('input[name$="[persenJasaDokter]"]').val());
    var instalasiId = $('#FAPendaftaranT_instalasi_id').val();
    
    var subTotal = 0;
    var totHargaJual = 0;
    var totHargaNetto = 0;
    var totTarifService = 0;
    var totJasaDokter = 0;
    var totadministrasi = 0;
    var totDiskon = 0;
    var jumlahDiskon = 0;
    var jasaDokter = 0;
//    administrasi = parseFloat(<?php //echo ($konfigFarmasi->admracikan) ?>);
//    totadministrasi += parseFloat(administrasi);
//    jumlahDiskon = (disc/100)*hargaSatuan;
    jumlahDiskon =((hargaSatuan * qty) * (disc/100));
    subTotal = parseFloat((hargaSatuan*qty)-jumlahDiskon);
    jasaDokter = ((hpp*qty)*(persenJasaDokter / 100));
    
    if ($.isNumeric(subTotal)){
        $(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(subTotal);
    }
    if ($.isNumeric(jasaDokter)){
        $(obj).parents('tr').find('input[name$="[jasadokterresep]"]').val(jasaDokter);
    }
    
    $('#tblDaftarResep tbody .obat').each(
        function()
        {
            hargaNetto = parseFloat($(this).parents('tr').find('input[name$="[harganetto_reseptur]"]').val());

            var isRacikan = parseFloat($(this).parents('tr').find('input[name$="[isRacikan]"]').val());
            tarifServices = 0;
            if(isRacikan == 0)
            {
                tarifServices = parseFloat($(this).parents('tr').find('input[name$="[biayaservice]"]').val());
            }

            discObat = parseFloat($(this).parents('tr').find('input[name$="[disc]"]').val());
            biayaadministrasi = parseFloat($(this).parents('tr').find('input[name$="[biayaadministrasi]"]').val());
            hargaJual = parseFloat($(this).parents('tr').find('input[name$="[hargajual_reseptur]"]').val());
            hargaSatuan = parseFloat($(this).parents('tr').find('input[name$="[hargasatuan_reseptur]"]').val());
            hpp = parseFloat($(this).parents('tr').find('input[name$="[hpp]"]').val());
            qtyObat = parseFloat($(this).parents('tr').find('input[name$="[qty]"]').val());
            jasaDokter = parseFloat($(this).parents('tr').find('input[name$="[jasadokterresep]"]').val());
//          DIAMBIL DARI MASTER obatalkes_m >>>  jasaDokter = (qty * hargaSatuan * parseFloat(<?php // echo ($konfigFarmasi->formulajasadokter == 0) ? $konfigFarmasi->formulajasadokter : MyFunction::calculate_string($konfigFarmasi->formulajasadokter) ?>));        
            if(instalasiId == <?php echo Params::INSTALASI_ID_RJ; ?>){
                totAdmin = (hargaJual * parseFloat(<?php echo ($konfigFarmasi->rj_persjual / 100 )?>));
            }else if(instalasiId == <?php echo Params::INSTALASI_ID_RI; ?>){
                totAdmin = (hargaJual * parseFloat(<?php echo ($konfigFarmasi->ri_persjual / 100 )?>));
            }else if(instalasiId == <?php echo Params::INSTALASI_ID_RD; ?>){
                totAdmin = (hargaJual * parseFloat(<?php echo ($konfigFarmasi->rd_persjual / 100 )?>));
            }
            
            if ($(this).parents("tr").find('input[name$="[pilihObat]"]').is(':checked')){
                totDiskon += ((hargaSatuan * qtyObat) * (discObat/100));
                totHargaNetto += (hargaNetto * qtyObat);
                totTarifService += tarifServices;
                totadministrasi += biayaadministrasi + totAdmin;
                totHargaJual += hargaJual;
                totJasaDokter += jasaDokter;
            }

        }
    );
    
    if (jQuery.isNumeric(totHargaNetto))
    {
        $('#FAPenjualanResepT_totharganetto').val(totHargaNetto);
    }
    
    if (jQuery.isNumeric(totHargaJual))
    {
        $('#FAPenjualanResepT_totalhargajual').val(totHargaJual);
        $('.totalhargajual').val(totHargaJual);
    }
    
//    if ($.isNumeric(totTarifService))
//    {
//       $('#FAPenjualanResepT_totaltarifservice').val(totTarifService);
//    }
    
    if ($.isNumeric(totJasaDokter))
    {
        $('#FAPenjualanResepT_jasadokterresep').val(totJasaDokter);
    }
    
    if (jQuery.isNumeric(totadministrasi))
    {
        $('#FAPenjualanResepT_biayaadministrasi').val(totadministrasi);
    }
    
    if (jQuery.isNumeric(totDiskon))
    {
        $('#FAPenjualanResepT_discount').val(totDiskon);
    }
    
}

function hitungTotalSemua()
{
    $('#FAPenjualanResepT_totharganetto').val(0);
    $('#FAPenjualanResepT_totalhargajual').val(0);
    $('.tothargajual').val(0);
//    $('#FAPenjualanResepT_totaltarifservice').val(0);
    $('#FAPenjualanResepT_jasadokterresep').val(0);
    $('#FAPenjualanResepT_biayaadministrasi').val(0);
//    $('#FAPenjualanResepT_discount').val(0);
    $('#tblDaftarResep').find('input[name$="[qty]"]').each(
        function()
        {
            hitungTotal(this);
            //Hitung tanggungan
            hitungTanggungan(this); 
        }
    );
    //Hitung Total Seluruh Tanggungan
    var subsidiRs = 0;
    var subsidiPemerintah = 0;
    var subsidiAsuransi = 0;
    var iurBiaya = 0;
    var totTanggungan = 0;
    $('#tblDaftarResep tbody .obat').each(
            function(){
                subsidiRs = parseFloat($(this).parents('tr').find('input[name$="[subsidirs]"]').val());
                subsidiPemerintah = parseFloat($(this).parents('tr').find('input[name$="[subsidipemerintah]"]').val());
                subsidiAsuransi = parseFloat($(this).parents('tr').find('input[name$="[subsidiasuransi]"]').val());
                iurBiaya = parseFloat($(this).parents('tr').find('input[name$="[iurbiaya]"]').val());
                totTanggungan += (subsidiRs + subsidiPemerintah + subsidiAsuransi + iurBiaya);
                
                var qty = parseFloat($(this).parents('tr').find('input[name$="[qty]"]').val());
                var obatalkes_id = $(this).parents("tr").find("input[name$='[obatalkes_id]']").val();
                var saldo = $(this).parents("tr").find("input[name$='[hargajual_reseptur]']").val();
                var saldo2 = $(this).parents("tr").find("input[name$='[harganetto_reseptur]']").val();
                saldoTindakan = (saldo2*qty);
                updateRekeningObatApotek(obatalkes_id, formatDesimal(saldo), formatDesimal(saldoTindakan));
            }
        
    );
    if (jQuery.isNumeric(totTanggungan)){
        $('#totalTanggungan').val(Math.ceil(totTanggungan));
    }
    var idx = 0;
    $('#tblDaftarResep tbody .obat').each(
        function()
        {
            var isRacikan = parseFloat($(this).parents('tr').find('input[name$="[isRacikan]"]').val());
            if(isRacikan == 1)
            {
                idx++;
            }
        }
    );
       
//    if(idx > 0)
//    {
//        var tarifService = $('#FAPenjualanResepT_totaltarifservice').val();
//        $.post('<?php //echo Yii::app()->createUrl('farmasiApotek/ActionAutoComplete/GetBiayaRacik'); ?>', {idx:idx},
//            function(data)
//            {
//                var totTarifService = parseInt(tarifService) + parseInt(data.value);
//                $('#FAPenjualanResepT_totaltarifservice').val(totTarifService);
//            },
//        'json');
//    }    
}

$('#resetbtn').click(
    function()
    {
        reset();
        window.location = '<?php echo Yii::app()->createAbsoluteUrl('farmasiApotek/PenjualanResep/JualResep') ?>';
    }
);

function reset()
{
    $('#penjualanresep-form').find(':input').each(function() {
        $(this).val('');
    });
}

function diskonObat(obj)
{
    var disc = parseFloat(obj.value);
    var hargaSatuan = parseFloat($(this).parents('tr').find('.hargareseptur').val());
    var hpp = parseFloat($(this).parents('tr').find('.hpp').val());
    var qty = parseFloat($(this).parents('tr').find('.qty').val());
    var totalharga = Math.ceil((hargaSatuan * qty) - ((hargaSatuan * qty) * (disc / 100))).toFixed(2);
    $(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(totalharga);
    var tothargadisc = 0;
    $(obj).parents('table').find('input[name$="[hargajual_reseptur]"]').each(function(){
        tothargadisc = tothargadisc + parseFloat(this.value);
    });
    $('#FAPenjualanResepT_totalhargajual').val(Math.ceil(tothargadisc));
}
function get_qty_a(obj){
    var qty = $(obj).val();
    $(obj).parents("tr").find("input[name$='[qty_a]']").val(Math.ceil(qty));
    hitungTotalSemua();
    var obatalkes_id = $(obj).parents("tr").find("input[name$='[obatalkes_id]']").val();
    var saldo = $(obj).parents("tr").find("input[name$='[hargajual_reseptur]']").val();
    var saldo2 = $(obj).parents("tr").find("input[name$='[harganetto_reseptur]']").val();
    saldoTindakan = (saldo2*qty);
    updateRekeningObatApotek(obatalkes_id, formatDesimal(saldo), formatDesimal(saldoTindakan));
}
function get_qty()
{
    var takaran = $('#takaranPembelian').val();
    $('#takaran').val(takaran);
    var jmltakaran = parseInt($('#takaran').val());

    $('#tblDaftarResep tbody .qty').each(function(){
        var qty = 0;
        var qtyawal = parseFloat($(this).parents("tr").find(".qty2").val());
        if(jmltakaran == 1)
        {
           qty = qtyawal;    
        }else if(jmltakaran == 2){
             qty = Math.ceil(parseFloat(qtyawal/2));      
        }else if(jmltakaran == 3){
             qty = Math.ceil(parseFloat(qtyawal/3));    
        }
        $(this).val(qty);
    });  
    //hitungQty(); <<Perhitungan salah jika qty obat di bulatkan
    hitungTotalSemua();
}
//hitung Qty tidak digunakan karena hasil perhitungan salah jika qty obat di bulatkan
//ini tidak perlu karena ada sudah ada hitungTotalSemua()
function hitungQty(obj)
{
    var jmltakaran = parseInt($('#takaran').val());
    var hargareseptur = 0;
    var subTotal = 0;    
    $('#tblDaftarResep tbody .hargareseptur').each(function(){
        var hargaobat = parseFloat($(this).parents("tr").find(".hargasatuanobat").val());
        var hargaobatreseptur = parseFloat($(this).parents("tr").find(".hargareseptur").val());
        var harganetto = parseFloat($(obj).parents('tr').find('input[name$="[hargasatuan_reseptur]"]').val());
        var hargajual = parseFloat($(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val());
        var qty = parseFloat($(this).parents("tr").find(".qty").val());
        var qty2 = parseFloat($(this).parents("tr").find(".qty2").val());
        var disc = parseFloat($(this).parents('tr').find('input[name$="[disc]"]').val());
        var jumlahDiskon = (disc/100) * harganetto;
        
        total = Math.ceil(qty*harganetto);
        subtotal = qty * harganetto; 
        
//           hargareseptur = Math.ceil(((hargaobat) / 1));
        hargareseptur = Math.ceil(((hargaobat*qty2) / jmltakaran));
        
        
//        $(this).val(hargareseptur);
//        subtotal = Math.ceil(hargareseptur);

        $(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val(hargareseptur * qty);
        var totHargaJual = 0;
        $(obj).parents('table').find('input[name$="[hargajual_reseptur]"]').each(function(){
            totHargaJual = totHargaJual + parseFloat(this.value);
        });
        
        /*
        $('#tblDaftarResep tbody .obat').each(function(){
            var disc = parseFloat($(this).parents('tr').find('input[name$="[disc]"]').val());
            subTotal += ((hargareseptur * qty) - ((hargareseptur * qty)*(disc/100)));
        });
        */
        $('.totalhargajual').val(Math.ceil(parseFloat(subTotal)));
//        parseFloat($(this).parents("tr").find('input[name$="[hargajual_reseptur]"]').val(parseFloat(((hargareseptur * qty) - ((hargareseptur * qty)*(disc/100)))).toFixed(2)));
        parseFloat($(this).parents("tr").find('input[name$="[hargajual_reseptur]"]').val(parseFloat((hargareseptur - (hargareseptur*(disc/100)))).toFixed(2)));
        
        var disc = parseFloat($(this).parents('tr').find('input[name$="[disc]"]').val());
        subTotal += (hargareseptur - (hargareseptur*(disc/100)));
        
    });
    $('#FAPenjualanResepT_totalhargajual').val(Math.ceil(parseFloat(subTotal)));    
    
}

function hitungDiskonSemua(){
    var diskon = parseFloat($('#FAPenjualanResepT_discount').val());
    var totalharga = parseFloat($('.totalhargajual').val());
    
    diskon = parseFloat(totalharga - (totalharga * (diskon/100)));
    
    $('#FAPenjualanResepT_totalhargajual').val(Math.ceil(diskon));
}

function pembulatanKeAtas(obj){
    // if(obj.value == 0){
    //     alert("Quantity tidak boleh nol !");
    //     $(obj).val(obj.defaultValue)
    // }
    
    $(obj).val(Math.ceil(obj.value));
}

function loadTarifService(jmlkemasan){
    var jml = <?php echo count($racikanDetail);?>;
    <?php
        $js_racikan = json_encode($racikanDetail);
        echo "var racikanDet = ".$js_racikan.";\n"; //convert php array to js array
    ?>
    var jmlkemasan = parseFloat(jmlkemasan);
    var tarifService = 0;
    for(var i = 0;i<jml;i++){
        var qtymin = parseFloat(racikanDet[i]['qtymin']);
        var qtymaks = parseFloat(racikanDet[i]['qtymaks']) + 1;
        if((jmlkemasan >= qtymin) && (jmlkemasan < qtymaks)){
            tarifService = racikanDet[i]['tarifservice'];
        }
    }
    return parseFloat(tarifService);
}

function hitungTarifService(){
    var racikanke = $('#formRacikan').find('#racikanKe').val();
    var jmlkemasan = $('#formRacikan').find('#jmlKemasanObat').val();
    var totalTarifService = parseFloat($("#FAPenjualanResepT_totaltarifservice").val());
    //cari racikan yang berbeda di tabel
    var rkebeda = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value!="'+racikanke+'"]');
    var rkesama = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value="'+racikanke+'"]');
    var jmlrkebeda = rkebeda.length;
    var jmlrkesama = rkesama.length;
    if(jmlrkesama === 0 && jmlrkebeda === 0){ //jika tidak ada yang sama dan tidak ada yang beda
        totalTarifService = loadTarifService(jmlkemasan);
    }else if(jmlrkesama === 0 && jmlrkebeda > 0){ //jika tidak ada yang sama tapi ada yang beda
        totalTarifService += loadTarifService(jmlkemasan);
    }
    $("#FAPenjualanResepT_totaltarifservice").val(Math.ceil(totalTarifService));
}
function kurangiTarifService(obj){
    var totalTarifService = $("#FAPenjualanResepT_totaltarifservice").val();
    var rkecari = $(obj).parents('tr').find('input[name$="[rke]"]').val();
    var jmlkemasan = $(obj).parents('tr').find('input[name$="[jmlkemasan_reseptur]"]').val();
    var rkesama = $('#tblDaftarResep tbody').find('input[name$="[rke]"][value='+rkecari+']');
    var jmlrkesama = rkesama.length;
    if(jmlrkesama === 0){
        totalTarifService -= loadTarifService(jmlkemasan);
        $("#FAPenjualanResepT_totaltarifservice").val(Math.ceil(totalTarifService));
    }
}
function hitungTanggungan(obj){
    var maksTanggungan = parseFloat($('#makstanggpel').val());
    var persSubsidiRs = parseFloat($('#subsidirspersen').val());
    var persSubsidiPemerintah = parseFloat($('#subsidipemerintahpersen').val());
    var persSubsidiAsuransi = parseFloat($('#subsidiasuransipersen').val());
    var persIurBiaya = parseFloat($('#iurbiayaoa').val());
    var hargaJual = parseFloat($(obj).parents('tr').find('input[name$="[hargajual_reseptur]"]').val());
    var totalSekarang = 0;
    var tanggunganRata = 0;
    var subsidiRs = 0;
    var subsidiPemerintah = 0;
    var subsidiAsuransi = 0;
    var iurBiaya = 0;
    var rataKe ="";
    if(maksTanggungan > 0){
        totalSekarang = parseFloat($('#FAPenjualanResepT_totalhargajual').val());
        tanggunganRata = (hargaJual / totalSekarang) * maksTanggungan;
        //  PERHITUNGAN TANGGUNGANRATA DITENTUKAN BERDASARKAN CARABAYAR_M << SEMENTARA BELUM BY: IHSAN
         
        subsidiRs = parseFloat(tanggunganRata);
        subsidiPemerintah = 0;
        subsidiAsuransi = 0;
        alert(totalSekarang+"-"+subsidiRs +"-"+subsidiPemerintah +"-"+ subsidiAsuransi);
    }else{
        subsidiRs = parseFloat(hargaJual * persSubsidiRs / 100);
        subsidiPemerintah = parseFloat(hargaJual * persSubsidiPemerintah / 100);
        subsidiAsuransi = parseFloat(hargaJual * persSubsidiAsuransi / 100);
        iurBiaya = parseFloat(hargaJual * persIurBiaya / 100);
    }
    if($.isNumeric(subsidiRs)){
        $(obj).parents('tr').find('input[name$="[subsidirs]"]').val(Math.ceil(subsidiRs));
    }
    if($.isNumeric(subsidiPemerintah)){
        $(obj).parents('tr').find('input[name$="[subsidipemerintah]"]').val(Math.ceil(subsidiPemerintah));
    }
    if($.isNumeric(subsidiAsuransi)){
        $(obj).parents('tr').find('input[name$="[subsidiasuransi]"]').val(Math.ceil(subsidiAsuransi));
    }
    if($.isNumeric(iurBiaya)){
        $(obj).parents('tr').find('input[name$="[iurbiaya]"]').val(Math.ceil(subsidiAsuransi));
    }
}

function copyResep(){
    if(cekDokter()){
            $.ajax({
                type:'POST',
                url:'<?php echo $this->createUrl('copyresep'); ?>',
                data: $('form').serialize(),
                success:function(data){
                    $("#dialogCopyResep").html(data);
                    $("#dialogCopyResep").dialog('open');
                }
            });
    }
}

// untuk tambah rekening biaya konseling
function tambahRekeningKonseling(){
    var biayaKonseling = unformatNumber($('#FAPenjualanResepT_biayakonseling').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_KONSELING_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_KONSELING ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaKonseling > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaKonseling),jenis_biaya)
        },1500);
    }
}

// untuk tambah rekening biaya administrasi
function tambahRekeningAdmin(){
    var biayaAdmin = unformatNumber($('#FAPenjualanResepT_biayaadministrasi').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_ADMINISTRASI_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_ADMINISTRASI ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaAdmin > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaAdmin),jenis_biaya)
        },1500);
    }
}

// untuk tambah rekening biaya service
function tambahRekeningService(){
    var biayaService = unformatNumber($('#FAPenjualanResepT_totaltarifservice').val());
    var idObat = $('#tblDaftarResep').parent().parent().find('input[name$="[obatalkes_id]"]').val();
    var daftartindakan_id = '<?php echo PARAMS::DEFAULT_BIAYA_SERVICE_FARMASI; ?>';
    var jenis_biaya = '<?php echo PARAMS::BIAYA_SERVICE ?>';
    var qty = 1;
    removeRekeningBiayaFarmasi(daftartindakan_id,jenis_biaya);
    if(biayaService > 0){        
        getDataRekeningBiayaFarmasi(idObat,"",qty,"ap",jenis_biaya);
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya            
            updateRekeningBiayaFarmasi(idObat, formatDesimal(biayaService),jenis_biaya)
        },1500);
    }
}
//dieksekusi saat load halaman
hitungTotalSemua();
</script>
