<?php $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<table class="table table-condensed table-striped">
    <tr>
        <td>No. Resep </td>
        <td>:</td>
        <td><?php echo $reseptur->noresep;?>
        <td>Tgl. Pendaftaran </td>
        <td>:</td>
        <td><?php echo $daftar->tgl_pendaftaran; ?></td>
    </tr>
    <tr>
        <td>No. Pendaftaran</td>
        <td>:</td>
        <td><?php echo $daftar->no_pendaftaran;?></td>
         <td>No. RM</td>
        <td>:</td>
        <td><?php echo $pasien->no_rekam_medik; ?></td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>:</td>
        <td><?php echo $pasien->nama_pasien;?>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $pasien->jeniskelamin;?></td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>:</td>
        <td><?php echo $daftar->umur;?> 
        <td>Cara Bayar / Penjamin</td>
        <td>:</td>
        <td><?php echo $daftar->carabayar->carabayar_nama; ?> / <?php echo $daftar->penjamin->penjamin_nama; ?></td>
    </tr>
</table><br/>
 <table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    
        <th style='text-align: center;'>No.</th>
        <th style='text-align: center;'>Uraian</th>
        <th style='text-align: center;'>Total Discount</th>
        <th style='text-align: center;'>SubTotal</th>
    
    </thead>
    
    <tbody>
        <?php
            $obatAlkes = ObatalkespasienT::model()->findAllByAttributes(array('pendaftaran_id'=>$daftar->pendaftaran_id));
            $total = 0;
            $diskon = 0;
            $service = 0;
            $konseling = 0;
            $administrasi = 0;
            
            foreach ($obatAlkes as $key => $value) {
                $subtotal = $value->hargajual_oa;
                $dis = $value->harganetto_oa*($value->discount/100);
                $serv = $value->biayaservice;
                $kons = $value->biayakonseling;
                $adm = $value->biayaadministrasi;
                
                $total = $total+$subtotal;
                $diskon = $diskon+$dis;
                $service = $service+$serv;
                $konseling = $konseling+$kons;
                $administrasi = $administrasi+$adm;
            }
        ?>
        <tr>
            <td><?php echo $key; ?></td>
            <td>Obat Alkes</td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($diskon); ?></td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($total + $administrasi + $konseling + $service); ?></td>
        </tr>
        
        </tr>
    </tbody>
</table>
<br>
<table>
    <tr><td><?php echo Yii::app()->user->getState('kabupaten_nama').', '.date('d-M-Y'); ?></td></tr>
    <tr><td>Yang membuat,</td></tr>
    <tr><td>Petugas I</td><td></td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td><?php echo Yii::app()->user->getState('nama_pegawai'); ?></td><td></tr>
</table>