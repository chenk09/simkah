<table>
    <tr>
        <td colspan="3">
            <table>
                <tr>
                    <td>
                        <?php echo $this->renderPartial('application.views.headerReport.headerDefaultStruk'); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<table style="margin-left:10px">
    <tr>
        <td>No. Nota </td>
        <td>:</td>
        <td><?php echo $modPenjualan->noresep;?>
    </tr>
    <tr>
        <td>Tgl. Pendaftaran </td>
        <td>:</td>
        <td><?php echo $daftar->tgl_pendaftaran;?>
    </tr>
    <tr>
        <td>No. Pendaftaran</td>
        <td>:</td>
        <td><?php echo $daftar->no_pendaftaran;?>   
    </tr>
    <tr>
        <td>No. RM</td>
        <td>:</td>
        <td><?php echo $pasien->no_rekam_medik; ?>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>:</td>
        <td><?php echo $pasien->nama_pasien;?>
    </tr>
    <tr>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $pasien->jeniskelamin;?>
    </tr>
    <tr>
        <td>Umur</td>
        <td>:</td>
        <td><?php echo $daftar->umur;?> 
    </tr>
    <tr>
        <td>Cara Bayar / Penjamin</td>
        <td>:</td>
        <td><?php echo $daftar->carabayar->carabayar_nama."&nbsp;/&nbsp;".$daftar->penjamin->penjamin_nama ;?> 
    </tr>
</table><br/>
<table border="1" style="margin-left:10px">
    <thead>
        <tr>
            <th>No.</th><th>Nama Obat</th><th>Qty</th><th>Harga Satuan</th><th>Discount</th><th>Sub Total</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $total = 0;
        foreach($obatAlkes as $i=>$rincian) { 
            $diskon = $rincian->qty_oa* $rincian->hargasatuan_oa * (($rincian->discount > 0) ? $rincian->discount/100 : 0);
            $subTotal = ($rincian->qty_oa * $rincian->hargasatuan_oa)-$diskon;
            $total = $total + $subTotal;
        ?>
        <tr>
            <td><?php echo $i+1; ?></td>
            <td><?php echo $rincian->obatalkes->obatalkes_nama; ?></td>
            <td style="text-align: right;"><?php echo $rincian->qty_oa; ?></td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($rincian->hargasatuan_oa); ?></td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($diskon); ?></td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber($subTotal); ?></td>
        </tr>
        <?php } ?>
<!--        <tr>
            <td style="text-align: right;" colspan="5">Biaya Administrasi</td>
            <td style="text-align: right;"><?php //echo MyFunction::formatNumber($modPenjualan->biayaadministrasi); ?></td>
        </tr>
         <tr>
            <td style="text-align: right;" colspan="5">Biaya Konseling</td>
            <td style="text-align: right;"><?php //echo MyFunction::formatNumber($modPenjualan->biayakonseling); ?></td>
        </tr>
         <tr>
            <td style="text-align: right;" colspan="5">Total Tarif Service</td>
            <td style="text-align: right;"><?php //echo MyFunction::formatNumber($modPenjualan->totaltarifservice); ?></td>
        </tr>
         <tr>
            <td style="text-align: right;" colspan="5">Jasa Dokter Resep</td>
            <td style="text-align: right;"><?php //echo MyFunction::formatNumber($modPenjualan->jasadokterresep); ?></td>
        </tr>-->
<!--        <tr>
            <td style="text-align: right;" colspan="5">Discount</td>
            <td style="text-align: right;"><?php //($modPenjualan->discount == 0) ? MyFunction::formatNumber($modPenjualan->discount) :  MyFunction::formatNumber($total * ($modPenjualan->discount/100)) ?></td>
        </tr>-->
        <tr>
            <td style="text-align: right;" colspan="5">TOTAL</td>
            <td style="text-align: right;"><?php echo MyFunction::formatNumber((($total - ($total*($modPenjualan->discount / 100))) + $modPenjualan->biayaadministrasi + $modPenjualan->biayakonseling + $modPenjualan->totaltarifservice)); ?></td>
        </tr>
    </tbody>
</table>
