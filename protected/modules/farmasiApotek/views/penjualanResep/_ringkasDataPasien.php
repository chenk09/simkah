<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim2">Transaksi Penjualan Resep</legend>
    <table>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::hiddenField('FAPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo CHtml::textField('FAPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td><div class=" control-label"><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'no_rek')); ?></div></td>
            <td>
                <?php
                if(!empty($modPendaftaran->instalasi_id)){
                    echo CHtml::textField('FAPendaftaranT[instalasi_id]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));
                }else{
                    echo CHtml::dropDownList('FAPendaftaranT[instalasi_id]', NULL, CHtml::listData($modPendaftaran->InstalasiResepturItems, 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'refreshDialogPendaftaran();', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                }
                ?>
            </td>
            
            <td rowspan="6">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
               <div class="control-label">   <label class="no_rek" for="noRekamMedik">No Rekam Medik</label></div>
            </td>
            <td>
                <?php //echo CHtml::textField('FAPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php if (!empty($modPasien->no_rekam_medik)) { 
                    echo CHtml::textField('FAPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true));
                    }else{
                        $this->widget('MyJuiAutoComplete', array(
                            'name'=>'FAPasienM[no_rekam_medik]',
                            'value'=>$modPasien->no_rekam_medik,
                            'source'=>'js: function(request, response) {
                                           $.ajax({
                                               url: "'.Yii::app()->createUrl('farmasiApotek/ActionAutoComplete/daftarPasien').'",
                                               dataType: "json",
                                               data: {
                                                   term: request.term,
                                                   instalasiId: $("#FAPendaftaranT_instalasi_id").val(),
                                               },
                                               success: function (data) {
                                                       response(data);
                                               }
                                           })
                                        }',
                             'options'=>array(
                                   'showAnim'=>'fold',
                                   'minLength' => 2,
                                   'focus'=> 'js:function( event, ui ) {
                                        $(this).val(ui.item.value);
                                        
                                        return false;
                                    }',
                                   'select'=>'js:function( event, ui ) {
                                        statusPeriksa(ui.item.pendaftaran_id,ui.item);
                                        isiDataPasien(ui.item);
                                        
                                        clearObat();
                                        return false;
                                    }',
                            ),
                            'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                            'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span2', 'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                        )); 
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
               <div class="control-label">   <label class="no_rek" for="namaPasien">Nama Pasien</label></div>
            </td>
            <td>
                <?php //echo CHtml::textField('FAPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                    if (!empty($modPasien->pasien_id)) { 
                        echo CHtml::textField('FAPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true));
                    }else{
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'FAPasienM[nama_pasien]',
                                    'value'=>$modPasien->nama_pasien,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('farmasiApotek/ActionAutoComplete/namaPasien').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                           instalasiId: $("#FAPendaftaranT_instalasi_id").val(),
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                               statusPeriksa(ui.item.pendaftaran_id,ui.item);
                                                isiDataPasien(ui.item);
                                                $("#FAPasienM_no_rekam_medik").val(ui.item.no_rekam_medik);
                                                clearObat();
                                                return false;
                                            }',
                                    ),
                                    'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span3', 'placeholder'=>'Ketik Nama Pasien','style'=>'width:200px;','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[jeniskasuspenyakit_nama]',((isset($modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama)) ? $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama : ''), array('readonly'=>true)); ?>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
        </tr>
        <tr>
            <!--<td><?php//DIGANTI DROPDOWN UNTUK FILTER PASIEN >>> echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>-->
<!--            <td>
                <?php // echo CHtml::textField('FAPendaftaranT[instalasi_nama]',  ((isset($modPendaftaran->instalasi->instalasi_nama)) ? $modPendaftaran->instalasi->instalasi_nama :''), array('readonly'=>true)); ?>
            </td>-->
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')) ?>
            </td>
            <td>
                <?php echo CHtml::textField('FAPendaftaranT[carabayar_nama]', (isset($modPendaftaran->carabayar->carabayar_nama) ? $modPendaftaran->carabayar->carabayar_nama : ""), array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('FAPendaftaranT[carabayar_id]', $modPendaftaran->carabayar_id, array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('FAPendaftaranT[instalasi_nama]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));   ?>
            </td>
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[ruangan_nama]', ((isset($modPendaftaran->ruangan->ruangan_nama)) ? $modPendaftaran->ruangan->ruangan_nama : ''), array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?></td>
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')) ?>
            </td>
            <td>
                <?php echo CHtml::textField('FAPendaftaranT[penjamin_nama]', (isset($modPendaftaran->penjamin->penjamin_nama) ? $modPendaftaran->penjamin->penjamin_nama : ""), array('readonly'=>true,  'data-percentage_farmasi' => (isset($modPendaftaran->penjamin->percentage_farmasi) ? $modPendaftaran->penjamin->percentage_farmasi : 0)));   ?>
                <?php echo CHtml::hiddenField('FAPendaftaranT[penjamin_id]', $modPendaftaran->penjamin_id, array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('FAPendaftaranT[kelaspelayanan_id]',((isset($modPendaftaran->kelaspelayanan_id)) ? $modPendaftaran->kelaspelayanan_id : ''), array('readonly'=>true)); ?></td>
            </td>
        </tr>
    </table>
</fieldset> 

<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
$format = new CustomFormat();
$modDialogPasien = new FAPasienM('searchPasienRumahsakitV');
$modDialogPasien->unsetAttributes();
if(isset($_GET['FAPasienM'])) {
    $modDialogPasien->attributes = $_GET['FAPasienM'];
    $modDialogPasien->idInstalasi = $_GET['FAPasienM']['idInstalasi'];
    $modDialogPasien->no_pendaftaran = $_GET['FAPasienM']['no_pendaftaran'];
    $modDialogPasien->tgl_pendaftaran_cari = $_GET['FAPasienM']['tgl_pendaftaran_cari'];
    $modDialogPasien->instalasi_nama = $_GET['FAPasienM']['instalasi_nama'];
    $modDialogPasien->carabayar_nama = $_GET['FAPasienM']['carabayar_nama'];
    $modDialogPasien->ruangan_nama = $_GET['FAPasienM']['ruangan_nama'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-t-grid',
	'dataProvider'=>$modDialogPasien->searchPasienRumahsakitV(),
	'filter'=>$modDialogPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPendaftaran",
                                    "onClick" => "
                                        $(\"#dialogPasien\").dialog(\"close\");
                                            statusPeriksa($data->pendaftaran_id);
                                            $(\"#FAPendaftaranT_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                            $(\"#FAPendaftaranT_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                            $(\"#FAPendaftaranT_umur\").val(\"$data->umur\");
                                            $(\"#FAPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");
                                            $(\"#FAPendaftaranT_instalasi_id\").val(\"$data->instalasi_id\");
                                            $(\"#FAPendaftaranT_instalasi_nama\").val(\"$data->instalasi_nama\");
                                            $(\"#FAPendaftaranT_ruangan_nama\").val(\"$data->ruangan_nama\");
                                            $(\"#FAPendaftaranT_pendaftaran_id\").val(\"$data->pendaftaran_id\");
                                            $(\"#FAPendaftaranT_carabayar_id\").val(\"$data->carabayar_id\");
                                            $(\"#FAPendaftaranT_penjamin_id\").val(\"$data->penjamin_id\");
                                            $(\"#FAPendaftaranT_kelaspelayanan_id\").val(\"$data->kelaspelayanan_id\");

                                            $(\"#FAPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                            $(\"#FAPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                            $(\"#FAPasienM_nama_pasien\").val(\"$data->nama_pasien\");
                                            $(\"#FAPasienM_nama_bin\").val(\"$data->nama_bin\");
                                            $(\"#FAPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                            $(\"#FAPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");

                                            $(\"#makstanggpel\").val(\"$data->Makstanggpel\");
                                            $(\"#subsidirspersen\").val(\"$data->Subsidirumahsakitoa\");
                                            $(\"#subsidipemerintahpersen\").val(\"$data->Subsidipemerintahoa\");
                                            $(\"#subsidiasuransipersen\").val(\"$data->Subsidiasuransioa\");
                                            $(\"#iurbiayapersen\").val(\"$data->Iurbiayaoa\");
                                            $(\"#FAPendaftaranT_penjamin_nama\").data(\"percentage_farmasi\",\"$data->percentage_farmasi\");
                                        
                                    "))',
                ),
                array(
                    'name'=>'no_rekam_medik',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'nama_pasien',
                    'type'=>'raw',
                ),
                'jeniskelamin',
                'no_pendaftaran',
                array(
                    'name'=>'tgl_pendaftaran',
                    'filter'=> 
                    CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran_cari', array('placeholder'=>'contoh: 15 Jan 2013')),
//                    POSISI DATE PICKER BUGS
//                    $this->widget('MyDateTimePicker',array(
//                            'model'=>$modDialogPasien,
//                            'attribute'=>'tgl_pendaftaran_cari',
//                            'mode'=>'date',
//                            'options'=> array(
//                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                            ),
//                            'htmlOptions'=>array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)"
//                            ),
//                    )),
                ),
                array(
                    'name'=>'instalasi_nama',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'ruangan_nama',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'carabayar_nama',
                    'type'=>'raw',
                ),
                
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pendaftaran dialog =============================
?>


<script type="text/javascript">
    function statusPeriksa(pendaftaran_id){
//    alert(status);
    var statusperiksa = '';
    var pendaftaran_id = pendaftaran_id;
    var pasienmasukpenunjang_id = '';
    
    $.post("<?php echo Yii::app()->createUrl('farmasiApotek/penjualanResep/statusPeriksa')?>", {statusperiksa:statusperiksa,pendaftaran_id: pendaftaran_id,pasienmasukpenunjang_id:pasienmasukpenunjang_id},
        function(data){
            if(data.pasienadmisi_id == null){
                var pasienadmisi_id = '';
            }else{
                var pasienadmisi_id = data.pasienadmisi_id;
            }
            if(data.pasien_id == null){
                var pasien_id = '';
            }else{
                var pasien_id = data.pasien_id;
            }
            if(data.statuspasien == 'SUDAH PULANG') {
                alert("Maaf, status pasien SUDAH PULANG tidak bisa melanjutkan transaksi pembelian obat. ");
                location.reload();
            }
    },"json");
    return false; 
}    
function isiDataPasien(data)
{
    $("#FAPendaftaranT_tgl_pendaftaran").val(data.tgl_pendaftaran);
    $("#FAPendaftaranT_no_pendaftaran").val(data.no_pendaftaran);
    $("#FAPendaftaranT_umur").val(data.umur);
    $("#FAPendaftaranT_jeniskasuspenyakit_nama").val(data.jeniskasuspenyakit);
    $("#FAPendaftaranT_instalasi_id").val(data.instalasi_id);
    $("#FAPendaftaranT_instalasi_nama").val(data.namainstalasi);
    $("#FAPendaftaranT_ruangan_nama").val(data.namaruangan);
    $("#FAPendaftaranT_pendaftaran_id").val(data.pendaftaran_id);
    $("#FAPendaftaranT_carabayar_id").val(data.carabayar_id);
    $("#FAPendaftaranT_penjamin_id").val(data.penjamin_id);
    $("#FAPendaftaranT_kelaspelayanan_id").val(data.kelaspelayanan_id);
    
    $("#FAPasienM_jeniskelamin").val(data.jeniskelamin);
    $("#FAPasienM_nama_pasien").val(data.namapasien);
    $("#FAPasienM_nama_bin").val(data.namabin);
    $("#FAPendaftaranT_carabayar_nama").val(data.carabayar_nama);
    $("#FAPendaftaranT_penjamin_nama").val(data.penjamin_nama);
    
    $("#makstanggpel").val(data.makstanggpel);
    $("#subsidirspersen").val(data.subsidirumahsakitoa);
    $("#subsidipemerintahpersen").val(data.subsidipemerintahoa);
    $("#subsidiasuransipersen").val(data.subsidiasuransioa);
    $("#iurbiayapersen").val(data.iurbiayaoa);
    $("#FAPendaftaranT_penjamin_nama").data('percentage_farmasi', data.percentage_farmasi);
    
//    $("#FAResepturT_pegawai_id").val(data.pegawai_id);
//    $("#FAResepturT_dokter").val(data.pegawai_nama);
    
}
function refreshDialogPendaftaran(){
    var instalasiId = $("#FAPendaftaranT_instalasi_id").val();
    var instalasiNama = $("#FAPendaftaranT_instalasi_id option:selected").text();
    $.fn.yiiGridView.update('pendaftaran-t-grid', {
        data: {
            "FAPasienM[idInstalasi]":instalasiId,
            "FAPasienM[instalasi_nama]":instalasiNama,
        }
    });
}
function cekInstalasi(){
    var instalasiId = $("#FAPendaftaranT_instalasi_id").val();
    if(instalasiId.length > 0){
        return true;
    }else{
        alert("Silahkan pilih instalasi ! ");
        $("#FAPendaftaranT_instalasi_id").focus();
        return false;
    }
}
</script>