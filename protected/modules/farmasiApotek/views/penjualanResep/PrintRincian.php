<?php $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<table class="table table-condensed table-striped">
    <tr>
        <td>No. Resep </td>
        <td>:</td>
        <td><?php echo $modPenjualan->noresep;?>
        <td>Tgl. Pendaftaran </td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->tgl_pendaftaran;?></td>
    </tr>
    <tr>
        <td>No. Pendaftaran</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->no_pendaftaran;?></td>
         <td>No. RM</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->pasien->no_rekam_medik; ?></td>
    </tr>
    <tr>
        <td>Nama Pasien</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->pasien->nama_pasien;?>
        <td>Jenis Kelamin</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->pasien->jeniskelamin;?></td>
    </tr>
    <tr>
        <td>Umur</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->umur;?> 
        <td>Cara Bayar / Penjamin</td>
        <td>:</td>
        <td><?php echo $modPenjualan->pendaftaran->carabayar->carabayar_nama; ?> / <?php echo $modPenjualan->pendaftaran->penjamin->penjamin_nama; ?></td>
    </tr>
</table><br/>
 <table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    
        <th style='text-align: center;'>No.</th>
        <th style='text-align: center;'>Uraian</th>
        <th style='text-align: center;'>Total Discount</th>
        <th style='text-align: center;'>SubTotal</th>
    
    </thead>
    <?php
    $no=1;
    $totalSubTotal = 0;
    if (count($obatAlkes) > 0){
        
        foreach($obatAlkes AS $tampilData):
            $subTotal = ($tampilData->subtotal - $tampilData->diskon );
//            $subTotal = (($tampilData['qty_oa']*$tampilData['hargasatuan_oa']) - (($tampilData['qty_oa']*$tampilData['hargasatuan_oa'])*($tampilData['discount']/100)));
//            $discount = (($subTotal*($tampilData['discount']/100)));
//            if($tampilData['oasudahbayar_id'] != null){
//                echo $status = 'Sudah Lunas';
//            }else{
//                echo $status = 'Belum Lunas';
//            }
    echo "<tr>
            <td style='text-align:center;'>".$no."</td>
            <td>"."Obat Alkes"."</td>
            <td style='text-align: right;'>".MyFunction::formatNumber($tampilData->diskon)."</td>
            <td style='text-align: right;'>".MyFunction::formatNumber($subTotal + $modPenjualan->biayaadministrasi+$modPenjualan->biayakonseling+$modPenjualan->totaltarifservice)."</td>
               
         </tr>";  
        $no++;
       
        $totalSubTotal+=$subTotal;
        
        endforeach;
    }
    echo "<tr>
            <td colspan='3' style='text-align:right;'> Total</td>
            <td style='text-align: right;'>".MyFunction::formatNumber((($totalSubTotal + $modPenjualan->biayaadministrasi+$modPenjualan->biayakonseling+$modPenjualan->totaltarifservice)))."</td>
         </tr>";    
    ?>
    
</table>
<table>
    <tr><td><?php echo Yii::app()->user->getState('kabupaten_nama').', '.date('d-M-Y'); ?></td></tr>
    <tr><td>Yang membuat,</td></tr>
    <tr><td>Petugas I</td><td></td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td>&nbsp;</td></tr>
    <tr><td><?php echo Yii::app()->user->getState('nama_pegawai'); ?></td><td></tr>
</table>