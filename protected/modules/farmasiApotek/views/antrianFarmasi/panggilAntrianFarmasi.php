<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/socket.io.js'); ?>
<?php
Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
    $.fn.yiiGridView.update('informasi-all', {
        data: $(this).serialize()
    });
    return false;
});
");
?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'caripasien-form',
    'enableAjaxValidation'=>false,
    'type'=>'horizontal',
    'focus'=>'#',
    'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));
?>
<table>
    <tr>
        <td>
            <fieldset>
                <legend class="rim2">Informasi Antrian Farmasi <?php echo $modLokasi->lokasiantrian_nama; ?></legend>
            <?php

            $this->widget('ext.bootstrap.widgets.BootGridView', array(
                'id' => 'informasi-all',
                'dataProvider' => $model->search(),
//                'filter' => $modInformasi,
                'template' => "{pager}{summary}\n{items}",
                'itemsCssClass' => 'table table-striped table-bordered table-condensed',
                'columns' => array(
                    array(
                        'header'=>'No',
                        'type'=>'raw',
                        'value'=>'$row+1',
                    ),
                    array(
                        'header'=>'No. Antrian Farmasi',
                        'type'=>'raw',
                        'value'=>'$data->nourutantrian',
                    ),
                    array(
                        'header'=>'Tgl Pelayanan',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran',
                    ),
                    array(
                        'header'=>'No. Pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                    ),
                    array(
                        'header'=>'No. RM',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'header'=>'Nama Pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    array(
                        'header'=>'Alamat',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                    ),
                    array(
                        'header'=>'Ruangan Perawatan',
                        'type'=>'raw',
                        'value'=>'$data->ruangan_nama',
                    ),
                    array(
                        'header'=>'Jenis Antrian',
                        'type'=>'raw',
                        'value'=>'$data->jenisantrian_nama',
                    ),
                    array(
                        'header'=>'Antrian',
                        'type'=>'raw',
                        'value'=>'$data->lokasiantrian_nama',
                    ),
                    array(
                        'header'=>'Panggil Antrian',
                        'type'=>'raw',
                        'value'=>'($data->is_batalantrian)? "Batal Antrian" : CHtml::htmlButton(Yii::t("mds","{icon}",array("{icon}"=>"<i class=\'icon-volume-up icon-white\'></i>")),array("class"=>"btn btn-primary antrian","onclick"=>"panggilAntrian(\"$data->antrianpasienfarmasi_id\",\"$data->tglpemanggilanpasien\",\"$data->nourutantrian\");","rel"=>"tooltip","title"=>"Klik untuk memanggil<br>pasien antrian ini"))',
                    ),
                    array(
                        'header'=>'Batal Antrian',
                        'type'=>'raw',
                        'value'=>'($data->is_batalantrian)? "Batal Antrian" : CHtml::htmlButton(Yii::t("mds","{icon}",array("{icon}"=>"<i class=\'icon-remove icon-white\'></i>")),array("class"=>"btn btn-primary antrian","onclick"=>"batallAntrian(\"$data->antrianpasienfarmasi_id\",\"$data->nourutantrian\");","rel"=>"tooltip","title"=>"Klik untuk batal<br> antrian ini"))',
                    ),
                ),
                'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
            ));
            ?>
            </fieldset>
        </td>
    </tr>
</table>

<fieldset>
    <legend class="rim">Kriteria Pencarian</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Tanggal Pelayanan','Tanggal Pelayanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAwal',
                            'mode'=>'date',
                            'options'=> array(
//                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'dateFormat'=>'yy-mm-dd',
                                'maxDate' => 'd',
                                //
                            ),
                            'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        )); ?>
                        
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Jenis Antrian','Jenis Antrian', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php echo $form->dropDownList($model,'jenisantrian_id',CHtml::listData(JenisantrianM::model()->findAll('jenisantrian_id in (6,7)'),'jenisantrian_id','jenisantrian_nama'),array('empty'=>'-- Pilih --',)); ?>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Sampai Dengan','Sampai Dengan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                        $this->widget('MyDateTimePicker',array(
                            'model'=>$model,
                            'attribute'=>'tglAkhir',
                            'mode'=>'date',
                            'options'=> array(
//                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                'dateFormat'=>'yy-mm-dd',
                                'maxDate' => 'd',
                            ),
                            'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        )); ?>
                        
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset>
<div class="form-actions">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onKeypress'=>'return formSubmit(this,event)')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
</div>
<?php $this->endWidget(); ?>
<script type="text/javascript">
    
    var chatServer='<?php echo Yii::app()->user->getState("nodejs_host") ?>';
    if (chatServer == ''){
     chatServer='http://localhost';
    }
    var chatPort='<?php echo Yii::app()->user->getState("nodejs_port") ?>';
    socket = io.connect(chatServer+':'+chatPort);
    
    function batallAntrian(antrianpasienfarmasi_id,nourutantrian){
        if(confirm("Apakah akan membatalkan antrian "+nourutantrian+" ?")){
            sudah = 1;
        }else{
            sudah = 0;
        }
        if(sudah==0){return false;}
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl('farmasiApotek/AntrianFarmasi/BatalAntrian'); ?>',
            data: {antrianpasienfarmasi_id:antrianpasienfarmasi_id},
            dataType: "json",
            success:function(data){
                if(data.pesan !== "OK"){
                    alert(data.pesan);
                }else{
                    $.fn.yiiGridView.update('informasi-all', {
                        data: $('#caripasien-form').serialize()
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }
    
    function panggilAntrian(antrianpasienfarmasi_id,tglpemanggilanpasien,nourutantrian){
        sudah = 1;
        if(tglpemanggilanpasien !== ""){
            if(confirm("Antrian sudah dipaggil. Apakah akan mengulang antrian "+nourutantrian+"?")){
                sudah = 1;
            }else{
                sudah = 0;
            }
        }
        if(sudah==0){return false;}
        $('.antrian').attr("disabled",true);
        $.ajax({
            type:'POST',
            url:'<?php echo Yii::app()->createUrl('farmasiApotek/AntrianFarmasi/PanggilAntrian'); ?>',
            data: {antrianpasienfarmasi_id:antrianpasienfarmasi_id},
            dataType: "json",
            success:function(data){
                if(data.pesan !== "OK"){
                    alert(data.pesan);
                }else{
                    socket.emit('send',{
                        conversationID:'antrianFarmasi',
                        antrianpasienfarmasi_id:data.model.antrianpasienfarmasi_id,
                        lokasiantrian_id:data.model.lokasiantrian_id,
                        jenisantrian_id:data.model.jenisantrian_id,
                        nourutantrian:data.model.nourutantrian,
                    });
                    $.fn.yiiGridView.update('informasi-all', {
                        data: $('#caripasien-form').serialize()
                    });
                    setTimeout(function(){
                        $('.antrian').removeAttr("disabled");
                    },5000); //5 detik tombol baru aktif
                }
            },
            error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
        });
    }
    
</script>