<head>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/js/daterangepicker-master/daterangepicker.css" />
<style>
    button.btn-tiket {
        width: auto;
        height:100px;
        /*background:	url("images/antrian/button a tanpa text.png") no-repeat;*/
        background-color: #1a92c2; background-image: -webkit-gradient(linear, left top, left bottom, from(#1a92c2), to(#1a92c2));
        background-image: -webkit-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -moz-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -ms-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: -o-linear-gradient(top, #1a92c2, #1a92c2);
        background-image: linear-gradient(to bottom, #1a92c2, #1a92c2);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#ffaf46, endColorstr=#1a92c2);
        background-size: 100% 100%;
        border:none;
        /*margin: 256px -15px 0px 28px;*/
        vertical-align: top;
        font-family: Arial, Helvetica, sans-serif;
        color:white;
        font-size:38px;
        letter-spacing:0px;
        font-weight: bold;
        text-shadow: 2px 2px 6px #000000;
        padding: 0px 100px;
        display: inline-block;
        border-radius: 20px;
    }
    button.btn-tiket:hover{
        /*background:	url("images/antrian/button a tanpa text (hover).png") no-repeat;*/
        background-size: 100% 100%;
        background-color: #778899; background-image: -webkit-gradient(linear, left top, left bottom, from(#1ab0ec), to(#778899));
        background-image: -webkit-linear-gradient(top, #778899, #778899);
        background-image: -moz-linear-gradient(top, #778899, #778899);
        background-image: -ms-linear-gradient(top, #778899, #778899);
        background-image: -o-linear-gradient(top, #778899, #778899);
        background-image: linear-gradient(to bottom, #778899, #778899);filter:progid:DXImageTransform.Microsoft.gradient(GradientType=0,startColorstr=#1ab0ec, endColorstr=#778899);
    }
    button.disabled{
        background:url("images/process-working.gif") no-repeat !important;
        background-position: top center !important;
    }
</style>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/daterangepicker-master/moment.min.js'); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/daterangepicker-master/daterangepicker.js'); ?>
</head>

<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'penjualanresep-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#FAPendaftaranT_instalasi_id',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)',
            'onsubmit'=>'return cekInput();'
        ),
));?>

<fieldset>
    <legend class="rim2">Pengambilan Antrian <?php echo $modLokasi->lokasiantrian_nama; ?></legend>
    <table>
        <tr>
            <td><?php echo CHtml::label('No. Pendaftaran','No. Pendaftaran',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::hiddenField('pendaftaran_id','', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php
                $this->widget('MyJuiAutoComplete', array(
                    'name'=>'no_pendaftaran',
                    'value'=>'',
                    'source'=>'js: function(request, response) {
                                   $.ajax({
                                       url: "'.Yii::app()->createUrl('farmasiApotek/ActionAutoComplete/daftarPasien').'",
                                       dataType: "json",
                                       data: {
                                           term: request.term,
                                           instalasiId: $("#FAPendaftaranT_instalasi_id").val(),
                                       },
                                       success: function (data) {
                                               response(data);
                                       }
                                   })
                                }',
                     'options'=>array(
                           'showAnim'=>'fold',
                           'minLength' => 2,
                           'focus'=> 'js:function( event, ui ) {
                                $(this).val(ui.item.value);

                                return false;
                            }',
                           'select'=>'js:function( event, ui ) {
                                statusPeriksa(ui.item.pendaftaran_id,ui.item);
                                isiDataPasien(ui.item);

                                clearObat();
                                return false;
                            }',
                    ),
                    'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                    'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span3', 'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                ));
                ?>
            </td>
            <td><?php echo CHtml::label('Cara Bayar','Cara Bayar',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('carabayar_nama', '', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('No. Rekam Medik','No. Rekam Medik',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('no_rekam_medik', '', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td><?php echo CHtml::label('Penjamin','Penjamin',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('penjamin_nama', '', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::label('Nama Pasien','Nama Pasien',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('nama_pasien', '', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
            <td><?php echo CHtml::label('Ruangan Perawatan','Ruangan Perawatan',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('ruangan_nama', '', array('readonly'=>true, 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend class="rim2">Jenis Antrian</legend>
    <table width="100%">
        <tr>
            <td style="text-align: center"><button id="btn-antrian-b" class="btn-tiket" type="button" name="yt0" onclick="SimpanAntrianFarmasi(this,6)">NON RACIKAN</button></td>
            <td><button id="btn-antrian-b" class="btn-tiket" type="button" name="yt0" onclick="SimpanAntrianFarmasi(this,7)">RACIKAN</button></td>
        </tr>            
    </table>
</fieldset>
<iframe id="print_win" src="" style="display: none;"></iframe>
<?php $this->endWidget(); ?>

<script type="text/javascript">
    
    function resetKunjungan(){
        $("#pendaftaran_id").val('');
        $("#no_pendaftaran").val('');
        $("#no_rekam_medik").val('');
        $("#nama_pasien").val('');
        $("#carabayar_nama").val('');
        $("#penjamin_nama").val('');
        $("#ruangan_nama").val('');
    }
    
    function SimpanAntrianFarmasi(obj,jenisantrian_id){
        
        var pendaftaran_id = $("#pendaftaran_id").val();
        var lokasiantrian_id = '<?php echo $modLokasi->lokasiantrian_id ?>';
        
        if(pendaftaran_id==""){
            alert("Pilih dahulu kunjungan pasien");
            return false;
        }
        
        $("button").attr("disabled");
        if (!$(obj).hasClass("disabled")) {
            $("button").attr("disabled");
            $("button").addClass("disabled");
            $.ajax({
                type: 'POST',
                url: '<?php echo $this->createUrl('SimpanAntrianFarmasi'); ?>',
                data: {lokasiantrian_id:lokasiantrian_id,jenisantrian_id:jenisantrian_id,pendaftaran_id:pendaftaran_id}, //
                dataType: "json",
                success: function (data) {
                    var delaytombol = parseInt(data.delaytombol) * parseInt(1000);
                    print(data.model.antrianpasienfarmasi_id);
                    setTimeout(function () {
                            $("button").removeAttr("disabled");
                            $("button").removeClass("disabled");
                    }, delaytombol);
                    resetKunjungan();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                }
            });
        }
    }
    function print(antrianpasienfarmasi_id) {
        $("#print_win").attr('src', "<?php echo $this->createUrl('Print') ?>&antrianpasienfarmasi_id=" + antrianpasienfarmasi_id);
    }
    
    $(function () {
        $('input[name="InfokunjunganrjAntrianfarmasiV[tgl_pendaftaran]"]').daterangepicker({
            "maxDate": "<?php echo date('m/d/Y') ?>",
            "showDropdowns": true,
            "singleDatePicker":true,
        });
    });
</script>

<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
$format = new CustomFormat();
$modDialogPasien = new InfokunjunganrjAntrianfarmasiV('searchDialog');
$modDialogPasien->unsetAttributes();
$modDialogPasien->tgl_pendaftaran = date('m/d/Y');
if(isset($_GET['InfokunjunganrjAntrianfarmasiV'])) {
    $modDialogPasien->attributes = $_GET['InfokunjunganrjAntrianfarmasiV'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-t-grid',
	'dataProvider'=>$modDialogPasien->searchDialog(),
	'filter'=>$modDialogPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                    "id" => "selectPendaftaran",
                                    "onClick" => "
                                        $(\"#dialogPasien\").dialog(\"close\");
                                        $(\"#pendaftaran_id\").val(\"$data->pendaftaran_id\");
                                        $(\"#no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                        $(\"#no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                        $(\"#nama_pasien\").val(\"$data->nama_pasien\");
                                        $(\"#carabayar_nama\").val(\"$data->carabayar_nama\");
                                        $(\"#penjamin_nama\").val(\"$data->penjamin_nama\");
                                        $(\"#ruangan_nama\").val(\"$data->ruangan_nama\");
                                    "))',
                ),
                array(
                    'name'=>'no_rekam_medik',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'nama_pasien',
                    'type'=>'raw',
                ),
                'no_pendaftaran',
                array(
                    'name'=>'tgl_pendaftaran',
                    'filter'=> 
                    CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran', array('readonly'=>true)),
                ),
                array(
                    'name'=>'instalasi_nama',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'ruangan_nama',
                    'type'=>'raw',
                ),
                array(
                    'name'=>'carabayar_nama',
                    'type'=>'raw',
                ),
                
                
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $(\'input[name="InfokunjunganrjAntrianfarmasiV[tgl_pendaftaran]"]\').daterangepicker({
                "maxDate": "'.date('m/d/Y').'",
                "showDropdowns": true,
                "singleDatePicker":true,
            });
        }',
));

$this->endWidget();
//========= end pendaftaran dialog =============================
?>