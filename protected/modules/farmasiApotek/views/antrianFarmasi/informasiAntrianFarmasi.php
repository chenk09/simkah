
<table>
    <tr>
        <td>
            <fieldset>
            <legend class="rim2">Antrian Non Racikan <?php echo $modLokasi->lokasiantrian_nama; ?></legend>
            <?php
            $modInfoNonRacikan->unsetAttributes();
            $modInfoNonRacikan->lokasiantrian_id = $modLokasi->lokasiantrian_id;
            if (isset($_GET['AntrianfarmasiNonracikanV'])) {
                $modInfoNonRacikan->attributes = $_GET['AntrianfarmasiNonracikanV'];
            }
            $this->widget('ext.bootstrap.widgets.BootGridView', array(
                'id' => 'informasi-non-racikan',
                'dataProvider' => $modInfoNonRacikan->search(),
//                    'filter' => $modDialogPasien,
                'template' => "{pager}{summary}\n{items}",
                'itemsCssClass' => 'table table-striped table-bordered table-condensed',
                'columns' => array(
                    array(
                        'header'=>'No',
                        'type'=>'raw',
                        'value'=>'$row+1',
                    ),
                    array(
                        'header'=>'No. Antrian',
                        'type'=>'raw',
                        'value'=>'$data->getNomorUrut()',
                    ),
                    array(
                        'header'=>'Status',
                        'type'=>'raw',
                        'value'=>'$data->status',
                    ),
                ),
                'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
            ));
            ?>
            </fieldset>
        </td>
        <td>
            <fieldset>
                <legend class="rim2">Antrian Racikan <?php echo $modLokasi->lokasiantrian_nama; ?></legend>
            <?php
            $modInfoRacikan->unsetAttributes();
            $modInfoRacikan->lokasiantrian_id = $modLokasi->lokasiantrian_id;
            if (isset($_GET['AntrianfarmasiRacikanV'])) {
                $modInfoRacikan->attributes = $_GET['AntrianfarmasiRacikanV'];
            }
            $this->widget('ext.bootstrap.widgets.BootGridView', array(
                'id' => 'informasi-racikan',
                'dataProvider' => $modInfoRacikan->search(),
//                'filter' => $modInfoRacikan,
                'template' => "{pager}{summary}\n{items}",
                'itemsCssClass' => 'table table-striped table-bordered table-condensed',
                'columns' => array(
                    array(
                        'header'=>'No',
                        'type'=>'raw',
                        'value'=>'$row+1',
                    ),
                    array(
                        'header'=>'No. Antrian',
                        'type'=>'raw',
                        'value'=>'$data->getNomorUrut()',
                    ),
                    array(
                        'header'=>'Status',
                        'type'=>'raw',
                        'value'=>'$data->status',
                    ),
                ),
                'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
            ));
            ?>
            </fieldset>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <fieldset>
                <legend class="rim2">Informasi Antrian Farmasi</legend>
            <?php
            $modInformasi->unsetAttributes();
            if (isset($_GET['AntrianfarmasiInformasiV'])) {
                $modInformasi->attributes = $_GET['AntrianfarmasiInformasiV'];
            }
            $this->widget('ext.bootstrap.widgets.BootGridView', array(
                'id' => 'informasi-all',
                'dataProvider' => $modInformasi->search(),
//                'filter' => $modInformasi,
                'template' => "{pager}{summary}\n{items}",
                'itemsCssClass' => 'table table-striped table-bordered table-condensed',
                'columns' => array(
                    array(
                        'header'=>'No',
                        'type'=>'raw',
                        'value'=>'$row+1',
                    ),
                    array(
                        'header'=>'Jenis Antrian',
                        'type'=>'raw',
                        'value'=>'$data->jenisantrian_nama',
                    ),
                    array(
                        'header'=>'Jumlah',
                        'type'=>'raw',
                        'value'=>'$data->jumlah',
                    ),
                    array(
                        'header'=>'Terlayani',
                        'type'=>'raw',
                        'value'=>'$data->is_dilayani',
                    ),
                    array(
                        'header'=>'Belum Terlayani',
                        'type'=>'raw',
                        'value'=>'$data->is_belumdilayani',
                    ),
                ),
                'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
            ));
            ?>
            </fieldset>
        </td>
    </tr>
</table>

<script type="text/javascript">
setInterval(   // fungsi untuk menjalankan suatu fungsi berdasarkan waktu
    function(){
        $.fn.yiiGridView.update('informasi-non-racikan', {   // fungsi untuk me-update data pada Cgridview yang memiliki id=category_grid
            data: $(this).serialize()
        });
        $.fn.yiiGridView.update('informasi-racikan', {   // fungsi untuk me-update data pada Cgridview yang memiliki id=category_grid
            data: $(this).serialize()
        });
        $.fn.yiiGridView.update('informasi-all', {   // fungsi untuk me-update data pada Cgridview yang memiliki id=category_grid
            data: $(this).serialize()
        });
        return false;
    }, 
 10000  // fungsi di eksekusi setiap 10 detik sekali
);
</script>