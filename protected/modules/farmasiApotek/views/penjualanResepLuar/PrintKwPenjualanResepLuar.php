<?php // $this->renderPartial('application.views.headerReport.headerDefault',array('colspan'=>10)); ?>
<?php $format = new CustomFormat(); //Format Uang?>
<div style="height: 3cm;"></div>
<table width="100%">
    <tr>
        <td width="20%">No. Resep</td>
        <td width="30%">: <?php echo $modPenjualan->noresep;?>
        <td width="20%">Nama Pasien</td>
        <td width="30%">: <?php echo $modPenjualan->pasien->nama_pasien;?>
    </tr>
    <tr>
        <td>Tgl. Resep</td>
        <td>: <?php echo $modPenjualan->tglresep;?></td>
        <td>Alamat Pasien</td>
        <td>: <?php echo $modPenjualan->pendaftaran->pasien->alamat_pasien;?>
            <?php // echo ", ".$modPenjualan->pendaftaran->pasien->kelurahan->kelurahan_nama;?>
            <?php echo ", ".$modPenjualan->pendaftaran->pasien->kecamatan->kecamatan_nama;?>
            <?php // echo ", ".$modPenjualan->pendaftaran->pasien->kabupaten->kabupaten_nama;?>
        </td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td><td></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td><td></td>
    </tr>
    <tr>
        <td></td><td></td>
        <td></td><td></td>
    </tr>
</table><br/>
<table width="100%">
    <thead style='border:1px solid;'>
        <th style='text-align: center;'>No.</th>
        <th style='text-align: center;'>Kode</th>
        <th style='text-align: center;'>Nama</th>
        <th style='text-align: center;'>Qty</th>
        <th style='text-align: center;'>Harga</th>
        <th style='text-align: center;'>Subtotal</th>
        <th style='text-align: center;'>Aturan Pakai</th>
    </thead>
    <?php
    $no=1;
    $subtotal = 0;
    $total = 0;
    if (count($obatAlkes) > 0){
        foreach($obatAlkes AS $tampilData):
            $subtotal = $tampilData->qty_oa * $tampilData->hargasatuan_oa;
            $total = $total + $subtotal;
            echo "<tr style='border:1px solid;'>
                <td style='text-align:center;'>".$no."</td>
                <td>".$tampilData->obatalkes->obatalkes_kode."</td>
                <td>".$tampilData->obatalkes->obatalkes_nama."</td>
                <td style='text-align: right;'>".$tampilData->qty_oa."</td>
                <td style='text-align: right;'>".$format->formatNumber($tampilData->hargasatuan_oa)."</td>
                <td style='text-align: right;'>".$format->formatNumber($subtotal)."</td>
                <td>".$tampilData->signa_oa."</td>
             </tr>";  
            $no++;
        endforeach;
    }
    ?>
</table><br>
<table style="float: left; width: 50%;">
    <tr style="border:1px solid;"><th style="background-color: #000; color: #FFF;">Keterangan</th></tr>
    <tr style="border:1px solid;"><td style="height: 2cm;">&nbsp;</td></tr>
</table>
<table style="float: right; width: 30%;">
    <tr>
        <td>Total</td>
        <td style="text-align: right;"><?php echo $format->formatNumber($total)?></td>
    </tr>
    <tr>
        <td>Ppn</td>
        <td style="text-align: right;">0</td>
    </tr>
    <tr>
        <td>Total Transaksi</td>
        <td style="text-align: right;"><?php echo $format->formatNumber($total)?></td>
    </tr>
    <tr>
        <td>Bayar</td>
        <td style="text-align: right;"><?php echo $format->formatNumber($total)?></td>
    </tr>
    <tr>
        <td>Biaya Administrasi</td>
        <td style="text-align: right;"><?php echo $format->formatNumber($modPenjualan->biayaadministrasi);?></td>
    </tr>
</table><br>
<table style="width:100%;">
    <tr><td style="width:50%;text-align: center;">Penerima</td><td style="text-align: center;">Hormat Kami,</td></tr>
    <tr><td style="text-align: center; height: 3cm;">.........................</td><td style="text-align: center;"><?php echo Yii::app()->user->getState('nama_pegawai'); ?></td></tr>
</table>
<div style="font-size: 9pt;">Dicetak Oleh: <?php echo Yii::app()->user->getState('nama_pegawai'); ?>
    <?php echo date('d M Y H:i:s'); ?></div>
