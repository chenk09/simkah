<?php //echo '<pre>'.print_r($_SESSION,1).'</pre>'; ?>
<fieldset id='fieldsetPasien'>
    <legend class="accord1" style="width:450px;">
       <?php echo CHtml::checkBox('isUpdatePasien', '', array('rel'=>'tooltip','title'=>'Pilih untuk update data pasien','onclick'=>'updateInputPasien(this)', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>  Data Pasien <i class="icon-pencil"></i>
    </legend>
    <table class="table-condensed hide" id="tblFormPasien">
        <tr>
            <td width="50%">
                <div class="control-group">
                    <label class="control-label">
                        No. Pasien Apotek
                    </label>
                    <div class="controls">
                        <?php echo $form->hiddenField($modPasien, 'pasien_id', array('readonly'=>true));?>
                        <?php 
                        $this->widget('MyJuiAutoComplete',array(
                                    'name'=>'noPasienApotek',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'sourceUrl'=> Yii::app()->createUrl('ActionAutoComplete/PasienLamaApotek'),
                                    'options'=>array(
                                       'showAnim'=>'fold',
                                       'minLength' => 4,
                                       'focus'=> 'js:function( event, ui ) {
                                            $("#noPasienApotek").val( ui.item.value );
                                            return false;
                                        }',
                                       'select'=>'js:function( event, ui ) {
                                            $("#'.CHtml::activeId($modPasien,'pasien_id').'").val(ui.item.pasien_id);
                                            $("#'.CHtml::activeId($modPasien,'jenisidentitas').'").val(ui.item.jenisidentitas);
                                            $("#'.CHtml::activeId($modPasien,'no_identitas_pasien').'").val(ui.item.no_identitas_pasien);
                                            $("#'.CHtml::activeId($modPasien,'namadepan').'").val(ui.item.namadepan);
                                            $("#'.CHtml::activeId($modPasien,'nama_pasien').'").val(ui.item.nama_pasien);
                                            $("#'.CHtml::activeId($modPasien,'nama_bin').'").val(ui.item.nama_bin);
                                            $("#'.CHtml::activeId($modPasien,'tempat_lahir').'").val(ui.item.tempat_lahir);
                                            $("#'.CHtml::activeId($modPasien,'tanggal_lahir').'").val(ui.item.tanggal_lahir);
                                            $("#'.CHtml::activeId($modPasien,'kelompokumur_id').'").val(ui.item.kelompokumur_id);
                                            $("#'.CHtml::activeId($modPasien,'jeniskelamin').'").val(ui.item.jeniskelamin);
                                            setJenisKelaminPasien(ui.item.jeniskelamin);
                                            setRhesusPasien(ui.item.rhesus);
                                            var kelurahan = String.trim(\'ui.item.kelurahan_id\');
                                            if(kelurahan.length == 0){
                                                kelurahan = 0
                                            }
                                            loadDaerahPasien(ui.item.propinsi_id, ui.item.kabupaten_id, ui.item.kecamatan_id, kelurahan);
                                            $("#'.CHtml::activeId($modPasien,'statusperkawinan').'").val(ui.item.statusperkawinan);
                                            $("#'.CHtml::activeId($modPasien,'golongandarah').'").val(ui.item.golongandarah);
                                            $("#'.CHtml::activeId($modPasien,'rhesus').'").val(ui.item.rhesus);
                                            $("#'.CHtml::activeId($modPasien,'alamat_pasien').'").val(ui.item.alamat_pasien);
                                            $("#'.CHtml::activeId($modPasien,'rt').'").val(ui.item.rt);
                                            $("#'.CHtml::activeId($modPasien,'rw').'").val(ui.item.rw);
                                            $("#'.CHtml::activeId($modPasien,'propinsi_id').'").val(ui.item.propinsi_id);
                                            $("#'.CHtml::activeId($modPasien,'kabupaten_id').'").val(ui.item.kabupaten_id);
                                            $("#'.CHtml::activeId($modPasien,'kecamatan_id').'").val(ui.item.kecamatan_id);
                                            $("#'.CHtml::activeId($modPasien,'kelurahan_id').'").val(ui.item.kelurahan_id);
                                            $("#'.CHtml::activeId($modPasien,'no_telepon_pasien').'").val(ui.item.no_telepon_pasien);
                                            $("#'.CHtml::activeId($modPasien,'no_mobile_pasien').'").val(ui.item.no_mobile_pasien);
                                            $("#'.CHtml::activeId($modPasien,'suku_id').'").val(ui.item.suku_id);
                                            $("#'.CHtml::activeId($modPasien,'alamatemail').'").val(ui.item.alamatemail);
                                            $("#'.CHtml::activeId($modPasien,'anakke').'").val(ui.item.anakke);
                                            $("#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'").val(ui.item.jumlah_bersaudara);
                                            $("#'.CHtml::activeId($modPasien,'pendidikan_id').'").val(ui.item.pendidikan_id);
                                            $("#'.CHtml::activeId($modPasien,'pekerjaan_id').'").val(ui.item.pekerjaan_id);
                                            $("#'.CHtml::activeId($modPasien,'agama').'").val(ui.item.agama);
                                            $("#'.CHtml::activeId($modPasien,'warga_negara').'").val(ui.item.warga_negara);
                                            loadUmur(ui.item.tanggal_lahir);
                                            return false;
                                        }',

                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                                    'htmlOptions'=>array('readonly'=>true,'onkeypress'=>"return $(this).focusNextInputField(event)"),
                        )); ?>
                    </div>
                </div>
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'no_identitas_pasien', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                            echo $form->dropDownList($modPasien,'jenisidentitas', JenisIdentitas::items(),  
                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
                                                                )); ?>   
                            <?php echo $form->textField($modPasien,'no_identitas_pasien', array('placeholder'=>'No Identitas','onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>            
                            <?php echo $form->error($modPasien, 'jenisidentitas'); ?><?php echo $form->error($modPasien, 'no_identitas'); ?>
                        </div>
                    </div>
                    
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'nama_pasien', array('class'=>'control-label')) ?>
                        <div class="controls inline">

                            <?php 
//                            echo $form->dropDownList($modPasien,'namadepan', NamaDepan::items(),  
//                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2'
//                                                                )); ?>   
                            <?php echo $form->textField($modPasien,'nama_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span3')); ?>            

                            <?php // echo $form->error($modPasien, 'namadepan'); ?><?php echo $form->error($modPasien, 'nama_pasien'); ?>
                        </div>
                    </div>

                    <?php // echo $form->textFieldRow($modPasien,'nama_bin', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php // echo $form->textFieldRow($modPasien,'tempat_lahir', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    
                    <div class="control-group ">
                        <?php echo $form->labelEx($modPasien,'tanggal_lahir', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php   
                                    $this->widget('MyDateTimePicker',array(
                                                    'model'=>$modPasien,
                                                    'attribute'=>'tanggal_lahir',
                                                    'mode'=>'date',
                                                    'options'=> array(
                                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                        'maxDate' => 'd',
                                                        //
                                                        'onkeypress'=>"js:function(){getUmur(this);}",
                                                        'onSelect'=>'js:function(){getUmur(this);}',
                                                        'yearRange'=> "-150:+0",
                                                    ),
                                                    'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                    ),
                            )); ?>
                            <?php echo $form->error($modPasien, 'tanggal_lahir'); ?>
                        </div>
                    </div>

                    <?php echo $form->radioButtonListInlineRow($modPasien, 'jeniskelamin', JenisKelamin::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    <?php // echo $form->dropDownListRow($modPasien,'statusperkawinan', StatusPerkawinan::items(),array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                    <div class="control-group ">
                        <?php // echo $form->labelEx($modPasien,'golongandarah', array('class'=>'control-label')) ?>

                        <div class="controls">

                            <?php 
//                            echo $form->dropDownList($modPasien,'golongandarah', GolonganDarah::items(),  
//                                                          array('empty'=>'-- Pilih --', 'onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span2')); ?>   
                            <div class="radio inline">
                                <div class="form-inline">
                                <?php // echo $form->radioButtonList($modPasien,'rhesus',Rhesus::items(), array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>            
                                </div>
                           </div>
                            <?php // echo $form->error($modPasien, 'golongandarah'); ?>
                            <?php // echo $form->error($modPasien, 'rhesus'); ?>
                        </div>
                    </div>
            </td>
            <td width="50%">
             <?php echo $form->textAreaRow($modPasien,'alamat_pasien', array('onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <div class="control-group ">
                <?php echo $form->labelEx($modPasien,'rt', array('class'=>'control-label inline')) ?>

                <div class="controls">
                    <?php echo $form->textField($modPasien,'rt', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>   / 
                    <?php echo $form->textField($modPasien,'rw', array('onkeypress'=>"return $(this).focusNextInputField(event)", 'class'=>'span1','maxlength'=>3)); ?>            
                    <?php echo $form->error($modPasien, 'rt'); ?>
                    <?php echo $form->error($modPasien, 'rw'); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($modPasien,'no_telepon_pasien', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modPasien,'no_telepon_pasien', array('class'=>'numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. Telepon yang dapat dihubungi')); ?>
                    <?php echo $form->error($modPasien, 'no_telepon_pasien'); ?>
                </div>
            </div>
            <div class="control-group ">
                <?php echo $form->labelEx($modPasien,'no_mobile_pasien', array('class'=>'control-label')) ?>
                <div class="controls">
                    <?php echo $form->textField($modPasien,'no_mobile_pasien', array('class'=>'numberOnly','onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'No. HP yang dapat dihubungi')); ?>
                    <?php echo $form->error($modPasien, 'no_mobile_pasien'); ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($modPasien,'alamatemail', arraY('onkeypress'=>"return $(this).focusNextInputField(event)",'placeholder'=>'Alamat E-mail')); ?>

            </td>
        </tr>
    </table>
</fieldset>


<?php
$urlGetTglLahir = Yii::app()->createUrl('ActionAjax/GetTglLahir');
$urlGetUmur = Yii::app()->createUrl('ActionAjax/GetUmur');
$urlGetDaerah = Yii::app()->createUrl('ActionAjax/getListDaerahPasien');
$idTagUmur = CHtml::activeId($model,'umur');
$js = <<< JS
function previewFoto(obj)
{
    var pathFile = $(obj).val();
    $('#imgFotoPasien').attr('src','file://'+pathFile);
    $('#imgFotoPasien').load();
}

function enableInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien input').removeAttr('checked');
        $('#fieldsetPasien #isUpdatePasien').hide();
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');

        $('#noRekamMedik').val('');
        $('#fieldsetPasien input').not(':radio').val('');
        $('#fieldsetPasien select').val('');
        $('#fieldsetPasien textarea').val('');
        $('#fieldsetPasien button').val('');
        $('#fieldsetDetailPasien input').val('');
        $('#fieldsetDetailPasien select').val('');
        $('#tombolPasienDialog').addClass('hide');
    }
    else {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').show();
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('readonly');
        $('#detail_data_pasien').slideDown(500);
        $('#cex_detaildatapasien').attr('checked','checked');
        $('#tombolPasienDialog').removeClass('hide');
    }
}

function updateInputPasien(obj)
{
    if(!obj.checked) {
        $('#fieldsetPasien input').attr('disabled','true');
        $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
        $('#fieldsetPasien select').attr('disabled','true');
        $('#fieldsetPasien textarea').attr('disabled','true');
        $('#fieldsetPasien button').attr('disabled','true');
        $('#fieldsetDetailPasien input').attr('disabled','true');
        $('#fieldsetDetailPasien select').attr('disabled','true');
        $('#controlNoRekamMedik button').removeAttr('disabled');
        $('#noRekamMedik').removeAttr('readonly');
        //$('#detail_data_pasien').slideDown(500);
        //$('#cex_detaildatapasien').attr('checked','checked');
        $('#tblFormPasien').slideUp(500);
    }
    else {
        $('#fieldsetPasien input').removeAttr('disabled');
        $('#fieldsetPasien select').removeAttr('disabled');
        $('#fieldsetPasien textarea').removeAttr('disabled');
        $('#fieldsetPasien button').removeAttr('disabled');
        $('#fieldsetDetailPasien input').removeAttr('disabled');
        $('#fieldsetDetailPasien select').removeAttr('disabled');
        $('#controlNoRekamMedik button').attr('disabled','true');
        $('#noRekamMedik').attr('readonly','true');
        //$('#detail_data_pasien').slideUp(500);
        //$('#cex_detaildatapasien').removeAttr('checked','checked');
        $('#tblFormPasien').slideDown(500);
    }
}

function getTglLahir(obj)
{
    var str = obj.value;
    obj.value = str.replace(/_/gi, "0");
    $.post("${urlGetTglLahir}",{umur: obj.value},
        function(data){
           $('#FAPasienM_tanggal_lahir').val(data.tglLahir); 
    },"json");
}

function getUmur(obj)
{
    //alert(obj.value);
    if(obj.value == '')
        obj.value = 0;
    $.post("${urlGetUmur}",{tglLahir: obj.value},
        function(data){

           $('#PPPendaftaranRj_umur').val(data.umur); 
           $('#PPPendaftaranMp_umur').val(data.umur); 
           $('#PPPendaftaranRd_umur').val(data.umur); 

           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function loadUmur(tglLahir)
{
    $.post("${urlGetUmur}",{tglLahir: tglLahir},
        function(data){
           $("#${idTagUmur}").val(data.umur);
    },"json");
}

function setJenisKelaminPasien(jenisKelamin)
{
    $('input[name="FAPasienM[jeniskelamin]"]').each(function(){
            if(this.value == jenisKelamin)
                $(this).attr('checked',true);
        }
    );
}

function setRhesusPasien(rhesus)
{
    $('input[name="FAPasienM[rhesus]"]').each(function(){
            if(this.value == rhesus)
                $(this).attr('checked',true);
        }
    );
}

function loadDaerahPasien(idProp,idKab,idKec,idKel)
{
    $.post("${urlGetDaerah}", { idProp: idProp, idKab: idKab, idKec: idKec, idKel: idKel },
        function(data){
            $('#FAPasienM_propinsi_id').html(data.listPropinsi);
            $('#FAPasienM_kabupaten_id').html(data.listKabupaten);
            $('#FAPasienM_kecamatan_id').html(data.listKecamatan);
            $('#FAPasienM_kelurahan_id').html(data.listKelurahan);
    }, "json");
}

function clearKecamatan()
{
    $('#FAPasienM_kecamatan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}

function clearKelurahan()
{
    $('#FAPasienM_kelurahan_id').find('option').remove().end().append('<option value="">-- Pilih --</option>').val('');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_HEAD);

$enableInputPasien = ($model->isPasienLama) ? 1 : 1;
$js = <<< JS
if(${enableInputPasien}) {
    $('#fieldsetPasien input').attr('disabled','true');
    $('#fieldsetPasien #isUpdatePasien').show();
    $('#fieldsetPasien #isUpdatePasien').removeAttr('disabled');
    $('#fieldsetPasien select').attr('disabled','true');
    $('#fieldsetPasien textarea').attr('disabled','true');
    $('#fieldsetDetailPasien input').attr('disabled','true');
    $('#fieldsetDetailPasien select').attr('disabled','true');
    $('#noRekamMedik').removeAttr('readonly');
    $('#controlNoRekamMedik button').removeAttr('disabled');
    $('#fieldsetPasien button').attr('disabled','true');
}
else {
    $('#fieldsetPasien input').removeAttr('disabled');
    $('#fieldsetPasien #isUpdatePasien').hide();
    $('#fieldsetPasien select').removeAttr('disabled');
    $('#fieldsetPasien textarea').removeAttr('disabled');
    $('#fieldsetDetailPasien input').removeAttr('disabled');
    $('#fieldsetDetailPasien select').removeAttr('disabled');
    $('#noRekamMedik').attr('readonly','true');
    $('#controlNoRekamMedik button').attr('disabled','true');
    $('#fieldsetPasien button').removeAttr('disabled');
}
JS;
Yii::app()->clientScript->registerScript('formPasien',$js,CClientScript::POS_READY);
?>

<?php 
// Dialog buat nambah data propinsi =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddPropinsi',
    'options'=>array(
        'title'=>'Menambah data Propinsi',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>450,
        'minHeight'=>350,
        'resizable'=>false,
    ),
));

echo '<div class="divForForm"></div>';


$this->endWidget();
//========= end propinsi dialog =============================

// Dialog buat nambah data kabupaten =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKabupaten',
    'options'=>array(
        'title'=>'Menambah data Kabupaten',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKabupaten"></div>';


$this->endWidget();
//========= end kabupaten dialog =============================

// Dialog buat nambah data kecamatan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKecamatan',
    'options'=>array(
        'title'=>'Menambah data Kecamatan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKecamatan"></div>';


$this->endWidget();
//========= end kecamatan dialog =============================

// Dialog buat nambah data kelurahan =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogAddKelurahan',
    'options'=>array(
        'title'=>'Menambah data Kelurahan',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>450,
        'height'=>440,
        'resizable'=>false,
    ),
));

echo '<div class="divForFormKelurahan"></div>';


$this->endWidget();
//========= end kelurahan dialog =============================
?>
<script type="text/javascript">
// here is the magic
function addPropinsi()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addPropinsi'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#dialogAddPropinsi div.divForForm form').submit(addPropinsi);
                }
                else
                {
                    $('#dialogAddPropinsi div.divForForm').html(data.div);
                    $('#FAPasienM_propinsi_id').html(data.propinsi);
                    setTimeout(\"$('#dialogAddPropinsi').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKabupaten()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKabupaten'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#dialogAddKabupaten div.divForFormKabupaten form').submit(addKabupaten);
                }
                else
                {
                    $('#dialogAddKabupaten div.divForFormKabupaten').html(data.div);
                    $('#FAPasienM_kabupaten_id').html(data.kabupaten);
                    setTimeout(\"$('#dialogAddKabupaten').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKecamatan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKecamatan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#dialogAddKecamatan div.divForFormKecamatan form').submit(addKecamatan);
                }
                else
                {
                    $('#dialogAddKecamatan div.divForFormKecamatan').html(data.div);
                    $('#FAPasienM_kecamatan_id').html(data.kecamatan);
                    setTimeout(\"$('#dialogAddKecamatan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function addKelurahan()
{
    <?php echo CHtml::ajax(array(
            'url'=>Yii::app()->createUrl('ActionAjax/addKelurahan'),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
            {
                if (data.status == 'create_form')
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#dialogAddKelurahan div.divForFormKelurahan form').submit(addKelurahan);
                }
                else
                {
                    $('#dialogAddKelurahan div.divForFormKelurahan').html(data.div);
                    $('#FAPasienM_kelurahan_id').html(data.kelurahan);
                    setTimeout(\"$('#dialogAddKelurahan').dialog('close') \",1000);
                }
 
            } ",
    ))?>;
    return false; 
}

function cariDataPasien(noRekammedik)
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/cariPasien');?>', {norekammedik: noRekammedik}, function(data){
        isiDataPasien(data);
    }, 'json');
}

function isiDataPasien(data)
{
    $("#<?php echo CHtml::activeId($modPasien,'pasien_id');?>").val(data.pasien_id);
    $("#<?php echo CHtml::activeId($modPasien,'jenisidentitas');?>").val(data.jenisidentitas);
    $("#<?php echo CHtml::activeId($modPasien,'no_identitas_pasien');?>").val(data.no_identitas_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'namadepan');?>").val(data.namadepan);
    $("#<?php echo CHtml::activeId($modPasien,'nama_pasien');?>").val(data.nama_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'nama_bin');?>").val(data.nama_bin);
    $("#<?php echo CHtml::activeId($modPasien,'tempat_lahir');?>").val(data.tempat_lahir);
    $("#<?php echo CHtml::activeId($modPasien,'tanggal_lahir');?>").val(data.tanggal_lahir);
    $("#<?php echo CHtml::activeId($modPasien,'kelompokumur_id');?>").val(data.kelompokumur_id);
    $("#<?php echo CHtml::activeId($modPasien,'jeniskelamin');?>").val(data.jeniskelamin);
    setJenisKelaminPasien(data.jeniskelamin);
    setRhesusPasien(data.rhesus);
    loadDaerahPasien(data.propinsi_id, data.kabupaten_id, data.kecamatan_id, data.kelurahan_id);
    $("#<?php echo CHtml::activeId($modPasien,'statusperkawinan');?>").val(data.statusperkawinan);
    $("#<?php echo CHtml::activeId($modPasien,'golongandarah');?>").val(data.golongandarah);
    $("#<?php echo CHtml::activeId($modPasien,'rhesus');?>").val(data.rhesus);
    $("#<?php echo CHtml::activeId($modPasien,'alamat_pasien');?>").val(data.alamat_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'rt');?>").val(data.rt);
    $("#<?php echo CHtml::activeId($modPasien,'rw');?>").val(data.rw);
    $("#<?php echo CHtml::activeId($modPasien,'propinsi_id');?>").val(data.propinsi_id);
    $("#<?php echo CHtml::activeId($modPasien,'kabupaten_id');?>").val(data.kabupaten_id);
    $("#<?php echo CHtml::activeId($modPasien,'kecamatan_id');?>").val(data.kecamatan_id);
    $("#<?php echo CHtml::activeId($modPasien,'kelurahan_id');?>").val(data.kelurahan_id);
    $("#<?php echo CHtml::activeId($modPasien,'no_telepon_pasien');?>").val(data.no_telepon_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'no_mobile_pasien');?>").val(data.no_mobile_pasien);
    $("#<?php echo CHtml::activeId($modPasien,'suku_id');?>").val(data.suku_id);
    $("#<?php echo CHtml::activeId($modPasien,'alamatemail');?>").val(data.alamatemail);
    $("#<?php echo CHtml::activeId($modPasien,'anakke');?>").val(data.anakke);
    $("#<?php echo CHtml::activeId($modPasien,'jumlah_bersaudara');?>").val(data.jumlah_bersaudara);
    $("#<?php echo CHtml::activeId($modPasien,'pendidikan_id');?>").val(data.pendidikan_id);
    $("#<?php echo CHtml::activeId($modPasien,'pekerjaan_id');?>").val(data.pekerjaan_id);
    $("#<?php echo CHtml::activeId($modPasien,'agama');?>").val(data.agama);
    $("#<?php echo CHtml::activeId($modPasien,'warga_negara');?>").val(data.warga_negara);
    loadUmur(data.tanggal_lahir);
    return false;
}
</script>

<?php Yii::app()->clientScript->registerScript('detail_data_pasien',"
    $('#detail_data_pasien').show();
    $('#cex_detaildatapasien').change(function(){
        if ($(this).is(':checked')){
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').removeAttr('disabled');
                $('#fieldsetDetailPasien select').removeAttr('disabled');
        }else{
                $('#fieldsetDetailPasien input').not('input[type=checkbox]').attr('disabled','true');
                $('#fieldsetDetailPasien select').attr('disabled','true');
                $('#fieldsetDetailPasien input').attr('value','');
                $('#fieldsetDetailPasien select').attr('value','');
        }
        $('#detail_data_pasien').slideToggle(500);
    });
");
?>
<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modDataPasien = new FAPasienM('searchPasienApotek');
$modDataPasien->unsetAttributes();
if(isset($_GET['FAPasienM'])) {
    $modDataPasien->attributes = $_GET['FAPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
        //'ajaxUrl'=>Yii::app()->createUrl('actionAjax/CariDataPasien'),
	'dataProvider'=>$modDataPasien->searchPasienApotek(),
	'filter'=>$modDataPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                        array(
                            'header'=>'Pilih',
                            'type'=>'raw',
                            'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","#",array("class"=>"btn-small", 
                                            "id" => "selectPasien",
                                            "onClick" => "
                                                $(\"#dialogPasien\").dialog(\"close\");
                                                $(\"#noPasienApotek\").val(\"$data->no_rekam_medik\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                setJenisKelaminPasien(\"$data->jeniskelamin\");
                                                setRhesusPasien(\"$data->rhesus\");
                                                var kelurahan = String.trim(\'$data->kelurahan_id\');
                                                if(kelurahan.length == 0){
                                                    kelurahan = 0
                                                }
                                                loadDaerahPasien($data->propinsi_id,$data->kabupaten_id,$data->kecamatan_id,kelurahan);
                                                $(\"#'.CHtml::activeId($modPasien,'pasien_id').'\").val(\"$data->pasien_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jenisidentitas').'\").val(\"$data->jenisidentitas\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_identitas_pasien').'\").val(\"$data->no_identitas_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'namadepan').'\").val(\"$data->namadepan\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_pasien').'\").val(\"$data->nama_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'nama_bin').'\").val(\"$data->nama_bin\");
                                                $(\"#'.CHtml::activeId($modPasien,'tempat_lahir').'\").val(\"$data->tempat_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'tanggal_lahir').'\").val(\"$data->tanggal_lahir\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelompokumur_id').'\").val(\"$data->kelompokumur_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'jeniskelamin').'\").val(\"$data->jeniskelamin\");
                                                $(\"#'.CHtml::activeId($modPasien,'statusperkawinan').'\").val(\"$data->statusperkawinan\");
                                                $(\"#'.CHtml::activeId($modPasien,'golongandarah').'\").val(\"$data->golongandarah\");
                                                $(\"#'.CHtml::activeId($modPasien,'rhesus').'\").val(\"$data->rhesus\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamat_pasien').'\").val(\"$data->alamat_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'rt').'\").val(\"$data->rt\");
                                                $(\"#'.CHtml::activeId($modPasien,'rw').'\").val(\"$data->rw\");
                                                $(\"#'.CHtml::activeId($modPasien,'propinsi_id').'\").val(\"$data->propinsi_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kabupaten_id').'\").val(\"$data->kabupaten_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kecamatan_id').'\").val(\"$data->kecamatan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'kelurahan_id').'\").val(\"$data->kelurahan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_telepon_pasien').'\").val(\"$data->no_telepon_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'no_mobile_pasien').'\").val(\"$data->no_mobile_pasien\");
                                                $(\"#'.CHtml::activeId($modPasien,'suku_id').'\").val(\"$data->suku_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'alamatemail').'\").val(\"$data->alamatemail\");
                                                $(\"#'.CHtml::activeId($modPasien,'anakke').'\").val(\"$data->anakke\");
                                                $(\"#'.CHtml::activeId($modPasien,'jumlah_bersaudara').'\").val(\"$data->jumlah_bersaudara\");
                                                $(\"#'.CHtml::activeId($modPasien,'pendidikan_id').'\").val(\"$data->pendidikan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'pekerjaan_id').'\").val(\"$data->pekerjaan_id\");
                                                $(\"#'.CHtml::activeId($modPasien,'agama').'\").val(\"$data->agama\");
                                                $(\"#'.CHtml::activeId($modPasien,'warga_negara').'\").val(\"$data->warga_negara\");
                                                loadUmur(\"$data->tanggal_lahir\");
                                            "))',
                        ),
//                'no_rekam_medik',
                array(
                        'name'=>'no_rekam_medik',
                        'header'=>'No. Rekam Medik',
                        'value'=>'$data->no_rekam_medik',
                    ),
                'nama_pasien',
//                'nama_bin',
                array(
                        'name'=>'nama_bin',
                        'header'=>'Alias',
                        'value'=>'$data->nama_bin',
                    ),
                'alamat_pasien',
                'rw',
                'rt',
                array(
                    'name'=>'propinsiNama',
                    'value'=>'$data->propinsi->propinsi_nama',
                ),
                array(
                    'name'=>'kabupatenNama',
                    'value'=>'$data->kabupaten->kabupaten_nama',
                ),
                array(
                    'name'=>'kecamatanNama',
                    'value'=>'$data->kecamatan->kecamatan_nama',
                ),
                array(
                    'name'=>'kelurahanNama',
                    'value'=>'$data->kelurahan->kelurahan_nama',
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pasien dialog =============================
?>
