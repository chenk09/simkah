<?php
class InfoPasienSiapPulangController extends SBaseController{
	public $layout='//layouts/column1';
	public $defaultAction = 'index';

	public function actionIndex(){
		$format = new CustomFormat();
		$model = new FAInformasikasirrawatjalanV;

		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));

		// $model->kirim_farmasi = 't';
		$model->statusfarmasi = 'f';

		if(isset($_GET["FAInformasikasirrawatjalanV"])){
			$model->attributes	= $_GET["FAInformasikasirrawatjalanV"];
			$model->tglAwal		= $_GET['FAInformasikasirrawatjalanV']['tglAwal'];
			$model->tglAkhir	= $_GET['FAInformasikasirrawatjalanV']['tglAkhir'];
		}

		$this->render('index',array(
			"model"=>$model
		));
	}

	public function actionRawatInap(){
		$format = new CustomFormat();
		$model = new FAInformasiinapsiappulangV;
		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));

		// $model->kirim_farmasi = 't';
		$model->statusfarmasi = 'f';

		if(isset($_GET["FAInformasiinapsiappulangV"])){
			$model->attributes 	= $_GET["FAInformasiinapsiappulangV"];
			$model->tglAwal		= $_GET['FAInformasiinapsiappulangV']['tglAwal'];
			$model->tglAkhir 	= $_GET['FAInformasiinapsiappulangV']['tglAkhir'];
		}

		$this->render('rawat_inap',array(
			"model"=>$model
		));
	}

	public function actionRawatDarurat(){
		$format = new CustomFormat();
		$model = new FAInfoKunjunganRDV;

		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));

		// $model->kirim_farmasi = 't';
		$model->statusfarmasi = 'f';

		if(isset($_GET["FAInfoKunjunganRDV"])){
			$model->attributes	= $_GET["FAInfoKunjunganRDV"];
			$model->tglAwal		= $_GET['FAInfoKunjunganRDV']['tglAwal'];
			$model->tglAkhir	= $_GET['FAInfoKunjunganRDV']['tglAkhir'];
		}

		$this->render('rawat_darurat',array(
			"model"=>$model
		));
	}

	protected function totalTagihan($data, $row, $dataColumn){
		$criteria = new CDbCriteria();
		$criteria->select = "SUM(hargasatuan_oa * qty_oa) pendaftaran_id";
		$criteria->compare('pendaftaran_id', $data->pendaftaran_id, false);
		$criteria->addCondition('oasudahbayar_id is null');
		$modRincian = InformasipenjualanaresepV::model()->find($criteria);
		return number_format($modRincian->pendaftaran_id, 0, ".", ",");
	}

	protected function rincianTagihan($data, $row, $dataColumn){
		return CHtml::Link("<i class=\"icon-list-alt\"></i>",
			Yii::app()->createUrl("billingKasir/RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
			array(
				"class"=>"",
				"target"=>"iframeRincianTagihan",
				"onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
				"rel"=>"tooltip",
				"title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
			)
		);
	}

	protected function approveFarmasi($data, $row, $dataColumn){
		$instalasi = "";
		switch($data->instalasi_id){
			case 2 : {$instalasi = 'RJ';break;}
			case 3 : {$instalasi = 'RD';break;}
			case 4 : {$instalasi = 'RI';break;}
		}
		return CHtml::link('Konfirmasi',array('#'), array(
			'class'=>'btn btn-sm btn-warning',
			'onClick'=>'javascript:ubahStatusFarmasi('. $data->pendaftaran_id .', "'. $instalasi .'");return false;'
		));
	}

	public function actionKonfirmasiFarmasi(){

	}

}
