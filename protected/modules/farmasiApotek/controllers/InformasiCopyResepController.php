<?php

class InformasiCopyResepController extends InformasiPenjualanResepController
{
	public function actionIndex()
	{
            $modInfoPenjualan = new FAInformasipenjualanresepV('searchInfoResepPasien');
            $modInfoPenjualan->unsetAttributes();
            $modInfoPenjualan->tglAwal = date("d M Y").' 00:00:00';
            $modInfoPenjualan->tglAkhir = date("d M Y").' 23:59:59';
			
            if(isset($_GET['FAInformasipenjualanresepV']))
			{
                $format = new CustomFormat();
                $modInfoPenjualan->attributes = $_GET['FAInformasipenjualanresepV'];
                $modInfoPenjualan->ruanganasalobat = $_GET['FAInformasipenjualanresepV']['ruanganasalobat'];
                $modInfoPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAwal']);
                $modInfoPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoPenjualan'=>$modInfoPenjualan));
    }

    public function actionCopyResep($idPenjualanResep,$idPasien,$id=null)
    {
        // die(print_r($modelResepDetail));
        $this->layout='//layouts/frameDialog';             
        if (!empty($id)) {
            $model = FACopyResepR::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
        }else{
            $model = new FACopyResepR;
        }
            $modelResepDetail = new FACopyResepDetail;
            // die(print_r($modelResepDetail));
            // $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
            $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
            $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' and pasien_id='.$idPasien.'');
            $modPasien = FAPasienM::model()->findByPk($idPasien);
            $modCopy = CopyresepR::model()->findAll('penjualanresep_id='.$idPenjualanResep);
            foreach($modCopy as $i=>$data){
                $copy = $data->jmlcopy;
            }
            if($modCopy == null){
                $copy = 1;
            }else{
                $copy = $copy + 1;
            }
            //echo $copy;
            //exit;
            if(isset($_POST['FACopyResepR'])){
                // die(print_r($_POST['FACopyResepR']));
                $jmlCopy = $copy;
                $model->attributes = $_POST['FACopyResepR'];
                $model->tglcopy = date('Y-m-d');
                $model->penjualanresep_id = $_POST['FAPenjualanResepT']['penjualanresep_id'];
                $model->keterangancopy = $_POST['FACopyResepR']['keterangancopy'];
                $model->jmlcopy = $jmlCopy;
                $model->create_time = date('Y-m-d');
                $model->update_time = date('Y-m-d');
                $model->create_loginpemakai_id = Yii::app()->user->id;
                $model->update_loginpemakai_id = Yii::app()->user->id;
                $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                //echo "<pre>";
                //echo print_r($model->getAttributes());
                //exit;
                if($modCopy->penjualanresep_id == $idPenjualanResep){
                    $update = CopyresepR::model()->UpdateAll(array(
                                                        'jmlcopy' =>$jmlCopy,
                                                        'tglcopy'=>date('Y-m-d'),
                                                        'keterangancopy' => $_POST['FACopyResepR']['keterangancopy'],
                                                        'create_time'=>date('Y-m-d'),
                                                        'update_time'=>date('Y-m-d'),
                                                        'create_loginpemakai_id'=>Yii::app()->user->id,
                                                        'update_loginpemakai_id'=>Yii::app()->user->id,
                                                        'create_ruangan'=>Yii::app()->user->getState('ruangan_id')
                    ),'penjualanresep_id=:penjualanresep_id',array(':penjualanresep_id'=>$_POST['FAPenjualanResepT']['penjualanresep_id']));

                    if($update){
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $tersimpan='Ya';
                    }else{
                    //$transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan");  
                    }

                }else{
                    // die(print_r($modelPenjualanResep->reseptur_id));
                    // foreach ($_POST['FACopyResepR'] as $i => $v) {
                    //     die(print_r((double)$_POST['FACopyResepR'][$i]['qty_copy']));
                    // }
                    if($model->save()){
                        // $updatemodelResepDetail = FACopyResepDetail::model()->findByPk(762);
                        // $updatemodelResepDetail->qty_copy = $_POST['FACopyResepDetail']['qty_copy'];
                        // $updatemodelResepDetail->save();
                        foreach ($_POST['FACopyResepR'] as $i => $v) {
                            if($_POST['FACopyResepR'][$i]['cek'] !=0){
                                $sql='UPDATE resepturdetail_t SET qty_copy=:qty_copy, det=:det WHERE reseptur_id=:reseptur_id AND obatalkes_id=:obatalkes_id';
                                    $command=Yii::app()->db->createCommand($sql);
                                    $update=$command->execute(array(
                                    'qty_copy'=>(double)$_POST['FACopyResepR'][$i]['qty_copy'],
                                    'det'=>$_POST['FACopyResepR'][$i]['det'],
                                    'reseptur_id'=>(int)$modelPenjualanResep->reseptur_id,
                                    'obatalkes_id'=>(int)$_POST['FACopyResepR'][$i]['obatalkes_id']
                                ));
                            }
                        }
                        
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $tersimpan='Ya';
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan"); 
                    }
                }  
            }
            
            $model->tglcopy = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglcopy, 'yyyy-MM-dd'));

            $tersimpan = 'Tidak';
            // $tess = FACopyResepDetail::model()->findAll('reseptur_id=291');
            // die(print_r($tess));
            $this->render('formCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'model'=>$model,
                                'modCopy'=>$modCopy,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'tersimpan'=>$tersimpan,
                                'modelResepDetail'=>$modelResepDetail,
                        ));
    }
    public function actionPrintCopyResep($idPenjualanResep)
	{
             $this->layout='//layouts/printWindows';             
             
             $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
             $modReseptur = ResepturT::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
             $modCopy = CopyresepR::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
             $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' and pasien_id='.$modelPenjualanResep->pasien_id.'');
             $modPasien = FAPasienM::model()->findByPk($modelPenjualanResep->pasien_id);
 
             $this->render('PrintCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'modReseptur'=>$modReseptur,
                                'modCopy'=>$modCopy,
             ));
    }
    function ConvertRomawi($angka)
    {
        $hsl = "";
        if ($angka < 1 || $angka > 5000) { 
            // Statement di atas buat nentuin angka ngga boleh dibawah 1 atau di atas 5000
            $hsl = ""; //jika tidak ada anggka
        } else {
            while ($angka >= 1000) {
                // While itu termasuk kedalam statement perulangan
                // Jadi misal variable angka lebih dari sama dengan 1000
                // Kondisi ini akan di jalankan
                $hsl .= "M"; 
                // jadi pas di jalanin , kondisi ini akan menambahkan M ke dalam
                // Varible hsl
                $angka -= 1000;
                // Lalu setelah itu varible angka di kurangi 1000 ,
                // Kenapa di kurangi
                // Karena statment ini mengambil 1000 untuk di konversi menjadi M
            }
        }
        if ($angka >= 500) {
            // statement di atas akan bernilai true / benar
            // Jika var angka lebih dari sama dengan 500
            if ($angka > 500) {
                if ($angka >= 900) {
                    $hsl .= "CM";
                    $angka -= 900;
                } else {
                    $hsl .= "D";
                    $angka-=500;
                }
            }
        }
        while ($angka>=100) {
            if ($angka>=400) {
                $hsl .= "CD";
                $angka -= 400;
            } else {
                $angka -= 100;
            }
        }
        if ($angka>=50) {
            if ($angka>=90) {
                $hsl .= "XC";
                $angka -= 90;
            } else {
                $hsl .= "L";
                $angka-=50;
            }
        }
        while ($angka >= 10) {
            if ($angka >= 40) {
                $hsl .= "XL";
                $angka -= 40;
            } else {
                $hsl .= "X";
                $angka -= 10;
            }
        }
        if ($angka >= 5) {
            if ($angka == 9) {
                $hsl .= "IX";
                $angka-=9;
            } else {
                $hsl .= "V";
                $angka -= 5;
            }
        }
        while ($angka >= 1) {
            if ($angka == 4) {
                $hsl .= "IV"; 
                $angka -= 4;
            } else {
                $hsl .= "I";
                $angka -= 1;
            }
        }
        return ($hsl);
    }
    
}