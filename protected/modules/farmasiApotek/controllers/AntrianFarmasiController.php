<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class AntrianFarmasiController extends Controller{   
    
    public function actionAmbilAntrianLt2(){
        $model = new AntrianpasienfarmasiT;
        $modKunjungan = new InfokunjunganrjAntrianfarmasiV;
        $modLokasi = LokasiantrianM::model()->findByPk(3);
        
        $this->render('ambilAntrian',array('model'=>$model,'modKunjungan'=>$modKunjungan,'modLokasi'=>$modLokasi));
    }
    
    public function actionAmbilAntrianLt5(){
        $model = new AntrianpasienfarmasiT;
        $modKunjungan = new InfokunjunganrjAntrianfarmasiV;
        $modLokasi = LokasiantrianM::model()->findByPk(4);
        
        $this->render('ambilAntrian',array('model'=>$model,'modKunjungan'=>$modKunjungan,'modLokasi'=>$modLokasi));
    }
    
    public function actionInformasiAntrianFarmasiLt2(){
        $modInfoNonRacikan = new AntrianfarmasiNonracikanV;
        $modInfoRacikan = new AntrianfarmasiRacikanV;
        $modInformasi = new AntrianfarmasiInformasiV;
        $modLokasi = LokasiantrianM::model()->findByPk(3);
        
        $this->render('informasiAntrianFarmasi',array('modInformasi'=>$modInformasi,'modInfoRacikan'=>$modInfoRacikan,'modInfoNonRacikan'=>$modInfoNonRacikan,'modLokasi'=>$modLokasi));
    }
    
    public function actionInformasiAntrianFarmasiLt5(){
        $modInfoNonRacikan = new AntrianfarmasiNonracikanV;
        $modInfoRacikan = new AntrianfarmasiRacikanV;
        $modInformasi = new AntrianfarmasiInformasiV;
        $modLokasi = LokasiantrianM::model()->findByPk(4);
        
        $this->render('informasiAntrianFarmasi',array('modInformasi'=>$modInformasi,'modInfoRacikan'=>$modInfoRacikan,'modInfoNonRacikan'=>$modInfoNonRacikan,'modLokasi'=>$modLokasi));
    }
    
    public function actionPanggilAntrianFarmasiLt2(){
        $modLokasi = LokasiantrianM::model()->findByPk(3);
        $model = New AntrianfarmasiPemanggilanpasienV('search');
        $model->tglAwal = date("Y-m-d");
        $model->tglAkhir = date("Y-m-d");
         $model->lokasiantrian_id = $modLokasi->lokasiantrian_id;
        if(isset($_GET['AntrianfarmasiPemanggilanpasienV'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['AntrianfarmasiPemanggilanpasienV'];
            $model->tglAwal = $_GET['AntrianfarmasiPemanggilanpasienV']['tglAwal'];
            $model->tglAkhir = $_GET['AntrianfarmasiPemanggilanpasienV']['tglAkhir'];
            $model->jenisantrian_id = $_GET['AntrianfarmasiPemanggilanpasienV']['jenisantrian_id'];
        }

        $this->render('panggilAntrianFarmasi',array('model'=>$model,'modLokasi'=>$modLokasi));
    }
    
    public function actionPanggilAntrianFarmasiLt5(){
        $modLokasi = LokasiantrianM::model()->findByPk(4);
        $model = New AntrianfarmasiPemanggilanpasienV('search');
        $model->tglAwal = date("Y-m-d");
        $model->tglAkhir = date("Y-m-d");
        $model->lokasiantrian_id = $modLokasi->lokasiantrian_id;
        if(isset($_GET['AntrianfarmasiPemanggilanpasienV'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['AntrianfarmasiPemanggilanpasienV'];
            $model->tglAwal = $_GET['AntrianfarmasiPemanggilanpasienV']['tglAwal'];
            $model->tglAkhir = $_GET['AntrianfarmasiPemanggilanpasienV']['tglAkhir'];
            $model->jenisantrian_id = $_GET['AntrianfarmasiPemanggilanpasienV']['jenisantrian_id'];
        } 
        
        $this->render('panggilAntrianFarmasi',array('model'=>$model,'modLokasi'=>$modLokasi));
    }
    
    public function actionSimpanAntrianFarmasi(){
        if (Yii::app()->request->isAjaxRequest) {
            $data = array();
            $data['delaytombol'] = 1;
            $lokasiantrian_id = $_POST['lokasiantrian_id'];
            $jenisantrian_id = $_POST['jenisantrian_id'];
            $pendaftaran_id = $_POST['pendaftaran_id'];
            
            $data['pesan'] = "Data gagal disimpan! ";
            
            $sqlLOket = "SELECT CAST(MAX(SUBSTRING(nourutantrian, 2, 3)) AS integer) nourutantrian FROM antrianpasienfarmasi_t 
                        WHERE lokasiantrian_id = ".$lokasiantrian_id."
                        AND jenisantrian_id = ".$jenisantrian_id."
                        AND date(tglantrianpasien)='".date('Y-m-d')."'";
            $noAntrian   = Yii::app()->db->createCommand($sqlLOket)->queryRow();
            $noAntrianBaru = (str_pad($noAntrian['nourutantrian']+1, 3, 0,STR_PAD_LEFT));
            
            $modJenAntrian = JenisantrianM::model()->findByPk($jenisantrian_id);
            $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
            
            $model = new AntrianpasienfarmasiT;
            $model->lokasiantrian_id = $lokasiantrian_id;
            $model->jenisantrian_id = $jenisantrian_id;
            $model->pendaftaran_id = $pendaftaran_id;
            $model->pasien_id = $modPendaftaran->pasien_id;
            $model->nourutantrian = $modJenAntrian->jenisantrian_singkatan.$noAntrianBaru;
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->tglantrianpasien = date('Y-m-d H:i:s');
            $model->create_time = date('Y-m-d H:i:s');
            if ($model->validate()) {
                $model->save();
                $data['model'] = $model;
                $data['pesan'] = "Data berhasil disimpan!";
            }else{
                $data['pesan'] = "Data gagal disimpan! " . CHtml::errorSummary($model);
            }

        }
        echo CJSON::encode($data);
        Yii::app()->end();
    }
    
    public function actionPrint($antrianpasienfarmasi_id) {
        
        $model = AntrianpasienfarmasiT::model()->findByPk($antrianpasienfarmasi_id);
        $modLok = LokasiantrianM::model()->findByPk($model->lokasiantrian_id);
            
        $this->layout='//layouts/printAntri';
        $this->render('printNoAntrian',array('noantrian'=>$model->nourutantrian,'lokasi'=>$modLok->lokasiantrian_nama));
    }
    
    public function actionBatalAntrian(){
        $antrianpasienfarmasi_id = $_POST['antrianpasienfarmasi_id'];
        $update = AntrianpasienfarmasiT::model()->updateByPk($antrianpasienfarmasi_id, array('is_batalantrian' => TRUE));
        if($update){
            $data['pesan'] = "OK";
        }else{
            $data['pesan'] = "GAGAL";
        }
        echo json_encode($data);
        Yii::app()->end();
    }
    
    public function actionPanggilAntrian(){
        $antrianpasienfarmasi_id = $_POST['antrianpasienfarmasi_id'];
        $model = AntrianpasienfarmasiT::model()->findByPk($antrianpasienfarmasi_id);
        $model->tglpemanggilanpasien = date('Y-m-d H:i:s');
        if ($model->update()) {
            $data['pesan'] = "OK"; //berhasil
        }else{
            $data['pesan'] = "GAGAL"; //gagal
        }
        $data['model'] = $model->attributes;

        echo json_encode($data);
        Yii::app()->end();
    }
    
}

