<?php

class InformasiPenjualanUnitController extends InformasiPenjualanResepController
{
	public function actionIndex()
	{
            $modInfoPenjualan = new FAInformasipenjualanresepV('searchInfoJualResep');
            $modInfoPenjualan->unsetAttributes();
            $modInfoPenjualan->tglAwal = date("d M Y").' 00:00:00';
            $modInfoPenjualan->tglAkhir = date("d M Y").' 23:59:59';
            if(isset($_GET['FAInformasipenjualanresepV'])){
                $format = new CustomFormat();
                $modInfoPenjualan->attributes = $_GET['FAInformasipenjualanresepV'];
                $modInfoPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAwal']);
                $modInfoPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoPenjualan'=>$modInfoPenjualan));
        }
        
        public function actionDetailPenjualan($id,$idPasien) {
        $this->layout = '//layouts/frameDialog';
        
        $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
        $modReseptur = FAPenjualanResepT::model()->findByPk($id);
        
        
        $detailreseptur = FAObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $id . ' ');
        $modPasien = FAPasienM::model()->findByPk($idPasien);

        $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
            'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
        }
        /**
         * actionBatalPenjualanResep extends ke InformasiPenjualanResepController
         * karena fungsi yang ini salah algoritma (sudah di test):
         * - stok tidak kembali
         * commented by: 
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
//        
//        public function actionBatalPenjualanResep(){
//            if (Yii::app()->request->isAjaxRequest){
//                $result = array();
//                if (isset($_POST['value'])){
//                    $value = $_POST['value'];
//                    $modPenjualanResep = PenjualanresepT::model()->findByPk($value);
//                    if ((boolean)count($modPenjualanResep)){
//                        $obatAlkes = ObatalkespasienT::model()->findAll('penjualanresep_id =:penjualanresep', array('penjualanresep'=>$modPenjualanResep->penjualanresep_id));
//                        $transaction = Yii::app()->db->beginTransaction();
//                        try {
//                            $success = false;
//                            $modRetur = new ReturresepT();
//                            $modRetur->noreturresep = Generator::noReturResep();
//                            $modRetur->attributes = $modPenjualanResep->attributes;
//                            $modRetur->tglretur = date('Y-m-d H:i:s');
//                            $modRetur->pegretur_id = Yii::app()->user->getState('pegawai_id');
//                            $modRetur->alasanretur = 'dibatalkan karena belum dibayar';
//                            $modRetur->totalretur = $modPenjualanResep->totalhargajual;
//                            if ($modRetur->validate()){
//                                if ($modRetur->save()){
//                                    $updatePenjualan = PenjualanresepT::model()->updateByPk($modRetur->penjualanresep_id, array('returresep_id'=>$modRetur->returresep_id));
//                                    $success = true && $updatePenjualan;
//                                    $jumlahSave = 0;
//                                    $jumlahObat = count($obatAlkes);
//                                    if ($jumlahObat > 0){
//                                        foreach ($obatAlkes as $key => $value) {
//                                            $modReturDetail[$key] = new ReturresepdetT();
//                                            $modReturDetail[$key]->attributes = $value->attributes;
//                                            $modReturDetail[$key]->qty_retur = $value->qty_oa;
//                                            $modReturDetail[$key]->hargasatuan = $value->hargasatuan_oa;
//                                            $modReturDetail[$key]->returresep_id = $modRetur->returresep_id;
//                                            if ($modReturDetail[$key]->validate()){
//                                                if ($modReturDetail[$key]->save()){
//                                                    $updateDetail = ObatalkespasienT::model()->updateByPk($value->obatalkespasien_id, array('returresepdet_id'=>$modReturDetail[$key]->returresepdet_id));
//                                                    if ($updateDetail){
//                                                        $jumlahSave++;
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                            if ($jumlahSave == 0 && $jumlahObat == $jumlahSave)
//                                $jumlahSave = $jumlahObat = 10;
//                            
//                            if ($success && ($jumlahSave > 0) && ($jumlahSave == $jumlahObat)){
//                                $transaction->commit();
//                                $result['info'] = 'Success Disimpan';
//                                $result['status'] = 'success';
//                            }else{
//                                $transaction->rollback();
//                                $result['info'] = 'Gagal Disimpan';
//                                $result['status'] = 'error';
//                            }
//                        } catch (Exception $exc) {
//                            $transaction->rollback();
//                            $result['info'] = $exc;
//                            $result['status']="error";
//                        }
//                    }
//                }
//                echo json_encode($result);
//                Yii::app()->end();
//            }
//        }
    
        public function actionPrintStruk($id,$idPasien){
           $this->layout = '//layouts/frameDialog';

           $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
           $reseptur = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $detailreseptur = FAObatalkesPasienT::model()->findAll($criteria);
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$reseptur->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$reseptur->pasien_id));
           
            $this->render('PrintStrukPenjualan', array('reseptur' => $reseptur,
                'detailreseptur' => $detailreseptur,'daftar'=>$daftar,'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan));
        }
        public function actionStrukPrint($id,$idPasien){
           $this->layout = '//layouts/frameDialog';

           $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
           $reseptur = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $detailreseptur = FAObatalkesPasienT::model()->findAll($criteria);
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$reseptur->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$reseptur->pasien_id));
           $judulLaporan = 'Struk Penjualan';
           $caraPrint=$_REQUEST['caraPrint'];
           if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                 $this->render('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }  
            
        }
        
        public function actionPrintDetailPenjualan(){
            $id = $_POST['id'];
            $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=:penjualanresep', array(':penjualanresep'=>$id));
            $modReseptur = FAPenjualanResepT::model()->findByPk($id);

            $detailreseptur = FAObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $id . ' ');
            $modPasien = FAPasienM::model()->findByPk($idPasien);

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                    'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur),true));
                $mpdf->Output();
            }  
            $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
        }
        
        public function actionCopyResep($idPenjualanResep,$idPasien,$id=null)
	{
             $this->layout='//layouts/frameDialog';             
                if (!empty($id)) {
                    $model = FACopyResepR::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
                }else{
                    $model = new FACopyResepR;
                }
             $tersimpan = 'Tidak';
             
             $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
             $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' and pasien_id='.$idPasien.'');
             $modPasien = FAPasienM::model()->findByPk($idPasien);
             $modCopy = CopyresepR::model()->findAll('penjualanresep_id='.$idPenjualanResep);
             foreach($modCopy as $i=>$data){
                 $copy = $data->jmlcopy;
             }
                $copy = $copy + 1;
//             echo $copy;
//             exit;
             if(isset($_POST['FACopyResepR'])){
                    $jmlCopy = $copy;
                    $model->attributes = $_POST['FACopyResepR'];
                    $model->tglcopy = date('Y-m-d');
                    $model->penjualanresep_id = $_POST['FAPenjualanResepT']['penjualanresep_id'];
                    $model->keterangancopy = $_POST['FACopyResepR']['keterangancopy'];
                    $model->jmlcopy = $jmlCopy;
                    $model->create_time = date('Y-m-d');
                    $model->update_time = date('Y-m-d');
                    $model->create_loginpemakai_id = Yii::app()->user->id;
                    $model->update_loginpemakai_id = Yii::app()->user->id;
                    $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
//                    
//                    echo print_r($model->getAttributes());
//                    exit;
                if($model->validate()){
                    if($modCopy > 0){
                        $update = CopyresepR::model()->UpdateAll(array(
                                                            'jmlcopy' =>$jmlCopy,
                                                            'tglcopy'=>date('Y-m-d'),
                                                            'keterangancopy' => $_POST['FACopyResepR']['keterangancopy'],
                                                            'create_time'=>date('Y-m-d'),
                                                            'update_time'=>date('Y-m-d'),
                                                            'create_loginpemakai_id'=>Yii::app()->user->id,
                                                            'update_loginpemakai_id'=>Yii::app()->user->id,
                                                            'create_ruangan'=>Yii::app()->user->getState('ruangan_id')
                        ),'penjualanresep_id=:penjualanresep_id',array(':penjualanresep_id'=>$_POST['FAPenjualanResepT']['penjualanresep_id']));

                        if($update){
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $tersimpan='Ya';
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan");  
                        }
                        
                    }else{
                        if($model->save()){
                            Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            $tersimpan='Ya';
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan"); 
                        }
                    }
                }
                    
                    
             }
             
             $model->tglcopy = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($model->tglcopy, 'yyyy-MM-dd'));
             
             $this->render('formCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'model'=>$model,
                                'modCopy'=>$modCopy,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'tersimpan'=>$tersimpan,
                          ));
	}
        
        public function actionPrintCopyResep($idPenjualanResep)
	{
             $this->layout='//layouts/printWindows';             
             
             $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
             $modReseptur = ResepturT::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
             $modCopy = CopyresepR::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
             $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' and pasien_id='.$modelPenjualanResep->pasien_id.'');
             $modPasien = FAPasienM::model()->findByPk($modelPenjualanResep->pasien_id);
 
             $this->render('PrintCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'modReseptur'=>$modReseptur,
                                'modCopy'=>$modCopy,
             ));
	}
}