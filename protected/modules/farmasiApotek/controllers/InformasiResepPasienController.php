<?php

class InformasiResepPasienController extends InformasiPenjualanResepController
{
	public function actionIndex()
	{
            $modInfoPenjualan = new FAInformasipenjualanresepV('searchInfoResepPasien');
            $modInfoPenjualan->unsetAttributes();
            $modInfoPenjualan->tglAwal = date("d M Y").' 00:00:00';
            $modInfoPenjualan->tglAkhir = date("d M Y").' 23:59:59';
			
            if(isset($_GET['FAInformasipenjualanresepV']))
			{
                $format = new CustomFormat();
                $modInfoPenjualan->attributes = $_GET['FAInformasipenjualanresepV'];
                $modInfoPenjualan->ruanganasalobat = $_GET['FAInformasipenjualanresepV']['ruanganasalobat'];
                $modInfoPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAwal']);
                $modInfoPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoPenjualan'=>$modInfoPenjualan));
        }
    function ConvertRomawi($angka)
    {
        $hsl = "";
        if ($angka < 1 || $angka > 5000) {
            // Statement di atas buat nentuin angka ngga boleh dibawah 1 atau di atas 5000
            $hsl = ""; //jika tidak ada anggka
        } else {
            while ($angka >= 1000) {
                // While itu termasuk kedalam statement perulangan
                // Jadi misal variable angka lebih dari sama dengan 1000
                // Kondisi ini akan di jalankan
                $hsl .= "M";
                // jadi pas di jalanin , kondisi ini akan menambahkan M ke dalam
                // Varible hsl
                $angka -= 1000;
                // Lalu setelah itu varible angka di kurangi 1000 ,
                // Kenapa di kurangi
                // Karena statment ini mengambil 1000 untuk di konversi menjadi M
            }
        }
        if ($angka >= 500) {
            // statement di atas akan bernilai true / benar
            // Jika var angka lebih dari sama dengan 500
            if ($angka > 500) {
                if ($angka >= 900) {
                    $hsl .= "CM";
                    $angka -= 900;
                } else {
                    $hsl .= "D";
                    $angka-=500;
                }
            }
        }
        while ($angka>=100) {
            if ($angka>=400) {
                $hsl .= "CD";
                $angka -= 400;
            } else {
                $angka -= 100;
            }
        }
        if ($angka>=50) {
            if ($angka>=90) {
                $hsl .= "XC";
                $angka -= 90;
            } else {
                $hsl .= "L";
                $angka-=50;
            }
        }
        while ($angka >= 10) {
            if ($angka >= 40) {
                $hsl .= "XL";
                $angka -= 40;
            } else {
                $hsl .= "X";
                $angka -= 10;
            }
        }
        if ($angka >= 5) {
            if ($angka == 9) {
                $hsl .= "IX";
                $angka-=9;
            } else {
                $hsl .= "V";
                $angka -= 5;
            }
        }
        while ($angka >= 1) {
            if ($angka == 4) {
                $hsl .= "IV";
                $angka -= 4;
            } else {
                $hsl .= "I";
                $angka -= 1;
            }
        }
        return ($hsl);
    }
}