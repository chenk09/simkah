<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanUnitController extends PenjualanDokterController
{
    public $pesan;
    public function actionIndex()
    {
        $modPenjualan = new FAPenjualanResepT();
        $modPenjualan->jenispenjualan = 'PENJUALAN UNIT';
        $modPenjualan->discount = 0;
        $modPenjualan->noresep = Generator::noResep(Yii::app()->user->getState('instalasi_id'));
        $modPenjualan->tglresep = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $modPenjualan->tglpenjualan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
        $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
        $racikanDetail = array();
        foreach ($modRacikanDetail as $i => $mod){ //convert object to array
            $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
            $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
            $racikanDetail[$i]['qtymin'] = $mod->qtymin;
            $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
            $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
        }
        $this->render('index',
            array(
                'modPenjualan'=>$modPenjualan,
                'konfigFarmasi'=>$konfigFarmasi,
                'nonRacikan'=>$nonRacikan,
                'racikanDetail'=>$racikanDetail,
            )
        );
    }
    
    public function actionSimpanDataPembelian()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $transaction = Yii::app()->db->beginTransaction();
            $status = 'ok';
            $this->pesan = 'success';
            
            try{
                $penjualan = $this->insertPenjualan($_POST['FAPenjualanResepT']);
                $modPenjualanSave = $penjualan['model'];
                $insert = $penjualan['status'];
                $insert = $this->insertDetailPenjualan($_POST['penjualanResep'], $penjualan['model'], $_POST['RekeningakuntansiV']);
                if(!$insert)
                {
                    $status = 'not';
                }else{
                    $transaction->commit();
                    $data = array(
                        'tglresep' => Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'noresep' => Generator::noResep(Yii::app()->user->getState('instalasi_id')),
                        'tglpenjualan'=>Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'jenispenjualan' => 'PENJUALAN UNIT',
                        'lamapelayanan'=>0,
                        'discount' => 0,
                        'pasienprofilrs_id'=>"",
                        'pasieninstalasiunit_id'=>"",
                        'idPenjualan' => $modPenjualanSave->penjualanresep_id,
                    );
                    $this->pesan = $data;
                }
                // echo "<pre>"; print_r($modPenjualanSave); exit();
                
                
            }catch(Exception $exc){
                print_r($exc);
                $transaction->rollback();
            }
            
            $data = array(
                'status' => $status,
                'pesan' => $this->pesan
            );
            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }


    protected function insertDetailPenjualan($params, $modPenjualan,$postRekenings = array())
    {
        $data = true; //pointer to first key
        $modObat = array();
        foreach ($params as $i => $obatResep)
        {
            if(isset($obatResep['detailreseptur_id']))
            {
                $modObatAlkes = new ObatalkespasienT('retail');
                $modObatAlkes->attributes = $obatResep;
                $modObatAlkes->biayakemasan = 0;
                $modObatAlkes->jasadokterresep = 0;
                $modObatAlkes->tarifcyto = 0;
                $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
                $modObatAlkes->penjualanresep_id = $modPenjualan->penjualanresep_id;
                $modObatAlkes->pendaftaran_id = $modPenjualan->pendaftaran_id;
                $modObatAlkes->penjamin_id = $modPenjualan->penjamin_id;
                $modObatAlkes->carabayar_id = $modPenjualan->carabayar_id;
                $modObatAlkes->pasien_id = $modPenjualan->pasien_id;
                $modObatAlkes->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
                $modObatAlkes->pegawai_id = $modPenjualan->pegawai_id;
                $modObatAlkes->permintaan_oa = $obatResep['permintaan_reseptur'];
                $modObatAlkes->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
                $modObatAlkes->qty_oa = $obatResep['qty'];
                $modObatAlkes->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
                $modObatAlkes->harganetto_oa = $obatResep['harganetto_reseptur'];
                $modObatAlkes->hargajual_oa = $obatResep['hargajual_reseptur'];
                $modObatAlkes->signa_oa = $obatResep['signa_reseptur'];
                $modObatAlkes->discount = ($obatResep['disc'] != 0) ? $obatResep['disc'] : 0; 
                $modObatAlkes->pegawai_id = Yii::app()->user->getState('pegawai_id');
                $modObatAlkes->oa = 'AP';
                /* Biaya Service Disimpan pada record racikan pertama sesuai dengan yang muncul di form penjualan -> total tarif service*/
                if ($data){
                    $modObatAlkes->biayaservice = $modPenjualan->totaltarifservice;
                    $modObatAlkes->biayaadministrasi = $modPenjualan->biayaadministrasi;
                    $modObatAlkes->biayakonseling = $modPenjualan->biayakonseling;
                    $modObatAlkes->jasadokterresep = $modPenjualan->jasadokterresep;
                    $data = false; //set false so another key is not first key
                }else{
                    $modObatAlkes->biayaservice= 0;
                    $modObatAlkes->biayaadministrasi = 0;
                    $modObatAlkes->biayakonseling = 0;
                    $modObatAlkes->jasadokterresep = 0;
                }
                $modObatAlkes->tglpelayanan = $modPenjualan->tglpenjualan;

                if($obatResep['disc']!=0){ $modObatAlkes->discount = $obatResep['disc']; }
                $modObatAlkes->tipepaket_id = Params::TIPEPAKET_NONPAKET;

                $modObatAlkes->shift_id = Yii::app()->user->getState('shift_id');
                $modObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modObatAlkes->subsidiasuransi = 0;
                $modObatAlkes->subsidipemerintah = 0;
                $modObatAlkes->subsidirs = 0;
                $modObatAlkes->iurbiaya = 0;
                $modObatAlkes->create_time = date('Y-m-d H:i:s');
                $modObatAlkes->update_time = date('Y-m-d H:i:s');
                $modObatAlkes->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modObatAlkes->create_loginpemakai_id = Yii::app()->user->id;

                if($modObatAlkes->validate()){
                    $modObatAlkes->save();
                    StokobatalkesT::kurangiStok($modObatAlkes->qty_oa, $modObatAlkes->obatalkes_id);
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modObatAlkes->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modObatAlkes->save();
                        $saveDetailJurnal = PenjualanResepController::saveJurnalDetailsObat($modJurnalRekening, $postRekenings, $modObatAlkes);
                    }
                    $modObat[$i] = $modObatAlkes;
                }else{
                    $this->pesan = $modObatAlkes->getErrors();
                }
            }
        }
        
        $is_sukses = true;
        if(count($params) != count($modObat))
        {
            $is_sukses = false;
        }
        
        return $is_sukses;        
    }
    
    protected function insertPenjualan($params)
    {
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $model = new FAPenjualanResepT();
        $model->attributes = $params;
        $model->tglpenjualan = date('Y-m-d H:i:s');
        $model->penjamin_id = Params::DEFAULT_PENJAMIN;
        $model->carabayar_id = Params::DEFAULT_CARABAYAR;
        $model->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
        $model->pasien_id = Params::DEFAULT_PASIEN_APOTEK_UNIT;
        $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $model->pembulatanharga = $konfigFarmasi->pembulatanharga;
        $model->subsidiasuransi = 0;
        $model->subsidipemerintah = 0;
        $model->subsidirs = 0;
        $model->iurbiaya = 0;
        $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
        $model->ruanganasal_nama = $ruanganAsal->ruangan_nama;        
        
        $is_sukses = true;
        if(!$model->save())
        {
            $is_sukses = false;
            $this->pesan = $model->getErrors();
        }
        
        return array('status'=>$is_sukses, 'model'=>$model);
    }
    
    public function actionUpdateJualResepUnit($idPenjualan = null){
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if (!empty($idPenjualan)){
                $modPenjualan = FAPenjualanResepT::model()->findByPk($idPenjualan);
                $modPasien = FAPasienM::model()->findByPk($modPenjualan->pasien_id);
                 if ((boolean)count($modPenjualan)){
                    $modPenjualan->isNewRecord = false;
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }
//               
            if(isset($_POST['penjualanResep'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPenjualan = $this->updatePenjualanResep($modPenjualan, $_POST['FAPenjualanResepT']);
                    $obatAlkes = $this->updateObatAlkes($obatAlkes, $modPenjualan, $_POST['penjualanResep']);
                    if($this->successUpdatePenjualan && $this->successUpdateObat){
                        $transaction->commit();
                        $sukses = 2; // 2= Sukses Update
                        Yii::app()->user->setFlash('success','Transaksi Penjualan Unit berhasil diubah !');
                        $this->redirect(array('InformasiPenjualanUnit/Index'));
                    }else{
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->successUpdateObat)
                            $dataGagal = "Data Obat Alkes";
                        Yii::app()->user->setFlash('error',"Transaksi gagal disimpan. Silahkan cek ".$dataGagal);
                        $sukses = 0;
                    }
                }catch (Exception $exc) {
                    $modPenjualan->isNewRecord = false;
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
                
            }
            
            $this->render('index',array(
                                'modPasien'=>$modPasien,
                                'obatAlkes'=>$obatAlkes,
                                'modPenjualan'=>$modPenjualan,
                                'racikan'=>$racikan,
                                'racikanDetail'=>$racikanDetail,
                                'nonRacikan'=>$nonRacikan,
                                'konfigFarmasi'=>$konfigFarmasi,
                                'sukses'=>$sukses,
                                ));
        }
        /**
    * actionPrintFakturPenjualanUnit digunakan untuk print faktur pada Transaksi Penjualan Unit
    * @param type $id
    */
    public function actionPrintFakturPenjualanUnit(){
        $id = $_REQUEST['id'];
      $this->layout = '//layouts/frameDialog';
      $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
      $modInstalasi = InstalasiM::model()->findByAttributes(array('instalasi_id'=>$modPenjualan->pasieninstalasiunit_id));
      $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
      $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
      $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
       $judulLaporan='Laporan Penerimaan Kas';
       $caraPrint=$_REQUEST['caraPrint'];
       if($caraPrint=='PRINT') {
           $this->layout='//layouts/printWindows';
       }
        $this->render('PrintFakturPenjualanUnit',array('modInstalasi'=>$modInstalasi,'modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
    }
}
?>
