<?php

class KasuspenyakitobatMController extends SBaseController
{
    
                public $layout = '//layouts/column1';
                public $defaultAction = 'admin';
    
                public function filters()
                {
                    return array(
                        'accessControl',
                    );
                }
                
                public function accessRules()
                {
                    return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('@'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','print'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','RemoveTemporary'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
                    );
                }
                
        public function actionIndex()
        {
                $this->render('index');
        }
        
      public function actionAdmin()
      {
        $model = new KasuspenyakitobatM('search');
        $model->unsetAttributes();
        if (isset($_GET['KasuspenyakitobatM']))
            $model->attributes = $_GET['KasuspenyakitobatM'];

        $this->render('admin',array('model'=>$model));
      }
                
                public function actionCreate()
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE))
                    // {
                    //     throw new CHttpException(401,Yii::t('mds','You are probihited to access this page. Contact Super Administrator'));
                    // }
                    $model = new KasuspenyakitobatM;
                    if (isset($_POST['KasuspenyakitobatM']))
                    {
//                        echo $_POST['KasuspenyakitobatM'][0]['jeniskasuspenyakit'];
//                        echo print_r($_POST['KasuspenyakitobatM']);
//                        exit();
                        $modDetails = $this->validasiTabular($model, $_POST['KasuspenyakitobatM']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $jumlah = 0;
                            foreach ($_POST['KasuspenyakitobatM'] as $j=>$row)
                            {
                                $model = new KasuspenyakitobatM();
                                $model->attributes = $row;
                                if($model->save()) {
                                    $jumlah++;
                                }
                            }
                            if ($jumlah == count($_POST['KasuspenyakitobatM'])) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data Berhasil disimpan');
                                $this->redirect(array('admin'));
                            } else {
                                $transaction->rollback();
                            }
                        }
                        catch(Exception $ex) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex));
                        }
                    }
                    $this->render('create',array('model'=>$model,'modDetails'=>$modDetails));
                }
                
                protected function validasiTabular($model, $data) {
                    foreach ($data as $i=>$row) {
                        $modDetails[$i] = new KasuspenyakitobatM;
                        $modDetails[$i]->attributes = $row;
                        $modDetails[$i]->validate();
                    }
                    return $modDetails;
                }
                
                public function actionUpdate($id)
                {
                    //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE))
                    // {
                    //     throw new CHttpException(401,Yii::t('mds','You are probihited to access this page. Contact Super Administrator'));
                    // }
                    $model = new KasuspenyakitobatM;
                      if (isset($_POST['KasuspenyakitobatM']))
                    {
                        $modDetails = $this->validasiTabular($model, $_POST['KasuspenyakitobatM']);
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $jumlah = 0;
                            foreach ($_POST['KasuspenyakitobatM'] as $j=>$row)
                            {
                                $model = new KasuspenyakitobatM();
                                $model->attributes = $row;
                                if($model->save()) {
                                    $jumlah++;
                                }
                            }
                            if ($jumlah == count($_POST['KasuspenyakitobatM'])) {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success','<strong>Berhasil</strong> Data Berhasil disimpan');
                                $this->redirect(array('admin'));
                            } else {
                                $transaction->rollback();
                            }
                        }
                        catch(Exception $ex) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error','<strong>Gagal</strong> Data gagal disimpan'.MyExceptionMessage::getMessage($ex));
                        }
                    }
                    $this->render('update',array('model'=>$model,'modDetails'=>$modDetails));
                }
                
                public function actionDelete($id, $obatalkes)
                {
                        $this->loadModel($id,$obatalkes)->delete();
                        if(!isset($_GET['ajax']))
                                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
                }
                
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
	public function loadModel($id, $obatalkes = null)
	{
                                if (empty($obatalkes))
                                {
                                    $model=KasuspenyakitobatM::model()->findByAttributes(array('jeniskasuspenyakit_id'=>$id));
                                } else {
                                    $model=KasuspenyakitobatM::model()->findByAttributes(array('jeniskasuspenyakit_id'=>$id, 'obatalkes_id'=>$obatalkes));
                                }
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
                
                public function actionPrint()
                {
                    $model= new KasuspenyakitobatM;
                    $model->attributes=$_REQUEST['KasuspenyakitobatM'];
                    $judulLaporan='Data Kasus Penyakit Obat';
                    $caraPrint=$_REQUEST['caraPrint'];
                    if($caraPrint=='PRINT') {
                        $this->layout='//layouts/printWindows';
                        $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($caraPrint=='EXCEL') {
                        $this->layout='//layouts/printExcel';
                        $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
                    }
                    else if($_REQUEST['caraPrint']=='PDF') {
                        $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                        $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                        $mpdf = new MyPDF('',$ukuranKertasPDF); 
                        $mpdf->useOddEven = 2;  
                        $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                        $mpdf->WriteHTML($stylesheet,1);  
                        $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                        $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                        $mpdf->Output();
                    }                 
                }
}