<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanKaryawanController extends PenjualanDokterController
{
    public $pesan;
    public function actionIndex()
    {
        $modPegawai = new FAPegawaiM();
        $modPenjualan = new FAPenjualanResepT();
        
        $modPenjualan->jenispenjualan = 'PENJUALAN KARYAWAN';
        $modPenjualan->discount = 0;
        $modPenjualan->noresep = Generator::noResep(Yii::app()->user->getState('instalasi_id'));
        $modPenjualan->tglresep = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $modPenjualan->tglpenjualan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
        $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
        $racikanDetail = array();
        foreach ($modRacikanDetail as $i => $mod){ //convert object to array
            $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
            $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
            $racikanDetail[$i]['qtymin'] = $mod->qtymin;
            $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
            $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
        }
        $this->render('index',
            array(
                'modPegawai'=>$modPegawai,
                'modPenjualan'=>$modPenjualan,
                'konfigFarmasi'=>$konfigFarmasi,
                'nonRacikan'=>$nonRacikan,
                'racikanDetail'=>$racikanDetail
            )
        );
    }
    
    public function actionSimpanDataPembelian()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $transaction = Yii::app()->db->beginTransaction();
            $status = 'ok';
            $this->pesan = 'success';
            
            try{
                $penjualan = $this->insertPenjualan($_POST['FAPenjualanResepT']);
                $modPenjualanSave = $penjualan['model'];
                $insert = $penjualan['status'];
                $insert = $this->insertDetailPenjualan($_POST['penjualanResep'], $penjualan['model'], $_POST['RekeningakuntansiV']);
                
                if(!$insert)
                {
                    $status = 'not';
                }else{
                    $transaction->commit();
                    $data = array(
                        'tglresep' => Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'noresep' => Generator::noResep(Yii::app()->user->getState('instalasi_id')),
                        'tglpenjualan'=>Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'jenispenjualan' => 'PENJUALAN KARYAWAN',
                        'lamapelayanan'=>0,
                        'discount' => 0,
                        'idPenjualan' => $modPenjualanSave->penjualanresep_id, 
                    );
                    $this->pesan = $data;
                }
                
            }catch(Exception $exc){
                print_r($exc);
                $transaction->rollback();
            }
            
            $data = array(
                'status' => $status,
                'pesan' => $this->pesan
            );
            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }
    
    protected function insertPenjualan($params)
    {
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $model = new FAPenjualanResepT();
        $model->attributes = $params;
        $model->tglpenjualan = date('Y-m-d H:i:s');
        $model->penjamin_id = Params::DEFAULT_PENJAMIN;
        $model->carabayar_id = Params::DEFAULT_CARABAYAR;
        $model->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
        $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $model->pembulatanharga = $konfigFarmasi->pembulatanharga;
        $model->subsidiasuransi = 0;
        $model->subsidipemerintah = 0;
        $model->subsidirs = 0;
        $model->iurbiaya = 0;
        $model->pasien_id = Params::DEFAULT_PASIEN_APOTEK_KARYAWAN;
        $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
        $model->ruanganasal_nama = $ruanganAsal->ruangan_nama;
        $model->pasienpegawai_id = $params['pasienpegawai_id'];
        $is_sukses = true;
        if(!$model->save())
        {
            $is_sukses = false;
            $this->pesan = $model->getErrors();
        }
        
        return array('status'=>$is_sukses, 'model'=>$model);
    }
    
    public function actionUpdateJualResepKaryawan($idPenjualan = null){
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if (!empty($idPenjualan)){
                $modPenjualan = FAPenjualanResepT::model()->findByPk($idPenjualan);
                $modPegawai = new FAPegawaiM;
                $modPegawaiData = FAPegawaiM::model()->findByPk($modPenjualan->pasienpegawai_id);
                $modPegawai->nama_pegawai = $modPegawaiData->nama_pegawai;
                $modPegawai->alamat_pegawai = $modPegawaiData->alamat_pegawai;
                $modPegawai->jeniskelamin = $modPegawaiData->jeniskelamin;
                $modPegawai->nomorindukpegawai = $modPegawaiData->nomorindukpegawai;
               if ((boolean)count($modPenjualan)){
                    $modPenjualan->isNewRecord = false;
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }
           if(isset($_POST['penjualanResep'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPenjualan = $this->updatePenjualanResep($modPenjualan, $_POST['FAPenjualanResepT']);
                    $obatAlkes = $this->updateObatAlkes($obatAlkes, $modPenjualan, $_POST['penjualanResep']);
                    if($this->successUpdatePenjualan && $this->successUpdateObat){
                        $transaction->commit();
                        $sukses = 2; // 2= Sukses Update
                        Yii::app()->user->setFlash('success','Transaksi Penjualan Karyawan berhasil diubah !');
                        $this->redirect(array('InformasiPenjualanKaryawan/Index'));
                    }else{
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->successUpdateObat)
                            $dataGagal = "Data Obat Alkes";
                        Yii::app()->user->setFlash('error',"Transaksi gagal disimpan. Silahkan cek ".$dataGagal);
                        $sukses = 0;
                    }
                }catch (Exception $exc) {
                    $modPenjualan->isNewRecord = false;
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
                
           }
           
           $this->render('index',array(
                                'modPegawai'=>$modPegawai,
                                'obatAlkes'=>$obatAlkes,
                                'modPenjualan'=>$modPenjualan,
                                'racikan'=>$racikan,
                                'racikanDetail'=>$racikanDetail,
                                'nonRacikan'=>$nonRacikan,
                                'konfigFarmasi'=>$konfigFarmasi,
                                'sukses'=>$sukses,
                            ));
    }
    
}

?>
