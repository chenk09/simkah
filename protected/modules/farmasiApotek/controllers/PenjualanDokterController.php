<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanDokterController extends PenjualanResepController
{
    public $pesan;
    public $successUpdateObat = false;
    public $successUpdatePenjualan = false;
    public function actionIndex()
    {
        $modDokter = new FADokterV();
        $modPenjualan = new FAPenjualanResepT();
        $modPenjualan->jenispenjualan = 'PENJUALAN DOKTER';
        $modPenjualan->discount = 0;
        $modPenjualan->noresep = Generator::noResep(Yii::app()->user->getState('instalasi_id'));
        $modPenjualan->tglresep = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $modPenjualan->tglpenjualan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
        $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
        $racikanDetail = array();
        $modJurnalRekening = new JurnalrekeningT;
        $modRekenings = array();
        foreach ($modRacikanDetail as $i => $mod){ //convert object to array
            $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
            $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
            $racikanDetail[$i]['qtymin'] = $mod->qtymin;
            $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
            $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
        }
        $this->render('index',
            array(
                'modDokter'=>$modDokter,
                'modPenjualan'=>$modPenjualan,
                'konfigFarmasi'=>$konfigFarmasi,
                'nonRacikan'=>$nonRacikan,
                'racikanDetail'=>$racikanDetail,
                'modRekenings'=>$modRekenings,
            )
        );
    }
    /**
     * actionGetObatReseptur digunakan untuk:
     * 1. menambah data obat ke tabel obat di penjualan dokter, karyawan, unit
     */
    public function actionGetObatReseptur()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $value = $_POST['id_obat'];
            $format = new CustomFormat();
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            
            $criteria = new CDbCriteria();
            $criteria->compare('obatalkes_id', $value);
            $criteria->addCondition('obatalkes_farmasi = TRUE');
            $criteria->addCondition('obatalkes_aktif = true');
            $criteria->order = 'obatalkes_nama';
            $models = ObatalkesM::model()->with('sumberdana')->find($criteria);
            
            $jum_stok = StokobatalkesT::getStokBarang(
                    $value, Yii::app()->user->getState('ruangan_id')
            );
                    
            switch(Yii::app()->user->getState('instalasi_id')){
                case Params::INSTALASI_ID_RI : $persen = Yii::app()->user->getState('ri_persjual');
                                                break;
                case Params::INSTALASI_ID_RJ : $persen = Yii::app()->user->getState('rj_persjual');
                                                break;
                case Params::INSTALASI_ID_RD : $persen = Yii::app()->user->getState('rd_persjual');
                                                break;
                default : $persen = 0; break;
            }
            $data = $models->attributes;
            
            $data['sumberdana_nama'] = $models->sumberdana->sumberdana_nama;
            $data['stokObat'] = $jum_stok;
            $data['diskonJual'] = $models->diskonJual;
            //PERHITUNGAN MENGGUNAKAN JAVASCRIPT DI FORM:
//            $data['hargajual'] = floor(($persen + 100 ) / 100 * $models->hargajual);
//            $data['kadaluarsa'] = ((strtotime($format->formatDateMediumForDB($models->tglkadaluarsa)) - strtotime(date('Y-m-d'))) > 0) ? 0 : 1 ;
//            $data['hjaResep'] = $models->hjaresep;
//            $data['hjaNonresep'] = $models->hjanonresep;
//            //default harga Satuan
//            $data['hargaSatuan'] = $data['hjaResep'];
//            $data['hargaSatuan_a'] = $data['hjaResep']; 
//            if($models->harganetto > 0)
//            {
//                $data['hargaSatuan'] = $data['hargaSatuan'] * ($konfigFarmasi->totalpersenhargajual == 0 ? 100 : $konfigFarmasi->totalpersenhargajual) / 100;
//                $data['hargaSatuan_e'] = $data['hargaSatuan'] * ($konfigFarmasi->totalpersenhargajual == 0 ? 100 : $konfigFarmasi->totalpersenhargajual) / 100;
//            }
            
//            if(KonfigfarmasiK::statusKonfigFarmasi())
//            {
//                $data['hargaSatuan'] = (($konfigFarmasi->totalpersenhargajual == 0) ? $data['hargajual'] : $data['hargaSatuan']);
//                $data['hargaSatuan_a'] = (($konfigFarmasi->totalpersenhargajual == 0) ? $data['hargajual'] : $data['hargaSatuan']);
//                
//                if($konfigFarmasi->hargajualglobal == true)
//                {
//                    $data['hargaSatuan'] = (($konfigFarmasi->totalpersenhargajual == 0) ? $data['hjaresep'] : $data['hargajual']);
//                    $data['hargaSatuan_a'] = (($konfigFarmasi->totalpersenhargajual == 0) ? $data['hjaresep'] : $data['hargajual']);                    
//                }
//            }else{
//                $data['hargaSatuan'] = $data['hargajual'];
//                $data['hargaSatuan_a'] = $data['hargajual'];
//            }
            //LANGSUNG BYPASS KONFIG FARMASI KARENA MENGGUNAKAN HJA RESEP DAN HJA NON RESEP (SUDAH DI MARGIN)
                
                
                
            /*
            biayaKemasan
            biayaService
            biayaAdministrasi
            discountObat
            jasaDokterResep
            satuanKekuatan
            */
            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }
    
    public function actionSimpanDataPembelian()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            $transaction = Yii::app()->db->beginTransaction();
            $status = 'ok';
            $this->pesan = 'success';
            
            try{
                $penjualan = $this->insertPenjualan($_POST['FAPenjualanResepT']);
                $modPenjualanSave = $penjualan['model'];
                $insert = $penjualan['status'];
                $insert = $this->insertDetailPenjualan($_POST['penjualanResep'], $penjualan['model'], $_POST['RekeningakuntansiV']);
                
                if(!$insert)
                {
                    $status = 'not';
                }else{
                    $transaction->commit();
                    $data = array(
                        'tglresep' => Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'noresep' => Generator::noResep(Yii::app()->user->getState('instalasi_id')),
                        'tglpenjualan'=>Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse(date('Y-m-d hh:mm:ss'), 'yyyy-MM-dd hh:mm:ss','medium',null)),
                        'jenispenjualan' => 'PENJUALAN DOKTER',
                        'lamapelayanan'=>0,
                        'discount' => 0,
                        'idPenjualan' => $modPenjualanSave->penjualanresep_id, 
                    );
                    $this->pesan = $data;
                }
                
            }catch(Exception $exc){
                print_r($exc);
                $transaction->rollback();
            }
            
            $data = array(
                'status' => $status,
                'pesan' => $this->pesan
            );
            echo CJSON::encode($data);
        }
        Yii::app()->end();
    }
    
    protected function insertPenjualan($params)
    {
        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $model = new FAPenjualanResepT();
        $model->attributes = $params;
        $model->tglpenjualan = date('Y-m-d H:i:s');
        $model->penjamin_id = Params::DEFAULT_PENJAMIN;
        $model->carabayar_id = Params::DEFAULT_CARABAYAR;
        $model->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
        $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $model->pembulatanharga = $konfigFarmasi->pembulatanharga;
        $model->subsidiasuransi = 0;
        $model->subsidipemerintah = 0;
        $model->subsidirs = 0;
        $model->iurbiaya = 0;
        $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
        $model->ruanganasal_nama = $ruanganAsal->ruangan_nama;
        $model->pasien_id = Params::DEFAULT_PASIEN_APOTEK_DOKTER;
        $model->pasienpegawai_id = $params['pasienpegawai_id'];
        $is_sukses = true;
        if(!$model->save())
        {
            $is_sukses = false;
            $this->pesan = $model->getErrors();
        }
        
        return array('status'=>$is_sukses, 'model'=>$model);
    }
    
    protected function insertDetailPenjualan($params, $modPenjualan,$postRekenings = array())
    {
        $data = true; //pointer to first key
        $modObat = array();
        foreach ($params as $i => $obatResep)
        {
            if(isset($obatResep['detailreseptur_id']))
            {
                $modObatAlkes = new ObatalkespasienT('retail');
                $modObatAlkes->attributes = $obatResep;
                $modObatAlkes->biayakemasan = 0;
                $modObatAlkes->jasadokterresep = 0;
                $modObatAlkes->tarifcyto = 0;
                $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
                $modObatAlkes->penjualanresep_id = $modPenjualan->penjualanresep_id;
                $modObatAlkes->pendaftaran_id = $modPenjualan->pendaftaran_id;
                $modObatAlkes->penjamin_id = $modPenjualan->penjamin_id;
                $modObatAlkes->carabayar_id = $modPenjualan->carabayar_id;
                $modObatAlkes->pasien_id = $modPenjualan->pasien_id;
                $modObatAlkes->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
                $modObatAlkes->pegawai_id = $modPenjualan->pegawai_id;
                $modObatAlkes->permintaan_oa = $obatResep['permintaan_reseptur'];
                $modObatAlkes->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
                $modObatAlkes->qty_oa = $obatResep['qty'];
                $modObatAlkes->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
                $modObatAlkes->harganetto_oa = $obatResep['harganetto_reseptur'];
                $modObatAlkes->hargajual_oa = $obatResep['hargajual_reseptur'];
				$modObatAlkes->hargasatuanjual_oa = $obatResep['hargasatuanjual_oa'];
                $modObatAlkes->signa_oa = $obatResep['signa_reseptur'];
                $modObatAlkes->discount = ($obatResep['disc'] != 0) ? $obatResep['disc'] : 0; 
                $modObatAlkes->pegawai_id = Yii::app()->user->getState('pegawai_id');
                $modObatAlkes->oa = 'AP';
                /* Biaya Service Disimpan pada record racikan pertama sesuai dengan yang muncul di form penjualan -> total tarif service*/
                if ($data){
                    $modObatAlkes->biayaservice = $modPenjualan->totaltarifservice;
                    $modObatAlkes->biayaadministrasi = $modPenjualan->biayaadministrasi;
                    $modObatAlkes->biayakonseling = $modPenjualan->biayakonseling;
                    $modObatAlkes->jasadokterresep = $modPenjualan->jasadokterresep;
                    $data = false; //set false so another key is not first key
                }else{
                    $modObatAlkes->biayaservice= 0;
                    $modObatAlkes->biayaadministrasi = 0;
                    $modObatAlkes->biayakonseling = 0;
                    $modObatAlkes->jasadokterresep = 0;
                }
                $modObatAlkes->tglpelayanan = $modPenjualan->tglpenjualan;

                if($obatResep['disc']!=0){ $modObatAlkes->discount = $obatResep['disc']; }
                $modObatAlkes->tipepaket_id = Params::TIPEPAKET_NONPAKET;

                $modObatAlkes->shift_id = Yii::app()->user->getState('shift_id');
                $modObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modObatAlkes->subsidiasuransi = 0;
                $modObatAlkes->subsidipemerintah = 0;
                $modObatAlkes->subsidirs = 0;
                $modObatAlkes->iurbiaya = 0;
                $modObatAlkes->create_time = date('Y-m-d H:i:s');
                $modObatAlkes->update_time = date('Y-m-d H:i:s');
                $modObatAlkes->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modObatAlkes->create_loginpemakai_id = Yii::app()->user->id;

                if($modObatAlkes->validate()){
                    $modObatAlkes->save();
                    StokobatalkesT::kurangiStok($modObatAlkes->qty_oa, $modObatAlkes->obatalkes_id);
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modObatAlkes->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                        $modObatAlkes->save();
                        $saveDetailJurnal = PenjualanResepController::saveJurnalDetailsObat($modJurnalRekening, $postRekenings, $modObatAlkes);
                    }
                    $modObat[$i] = $modObatAlkes;
                }else{
                    $this->pesan = $modObatAlkes->getErrors();
                }
            }
        }
        
        $is_sukses = true;
        if(count($params) != count($modObat))
        {
            $is_sukses = false;
        }
        
        return $is_sukses;        
    }
    
    public function actionUpdateJualResepDokter($idPenjualan = null){
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if (!empty($idPenjualan)){
                 $modPenjualan = FAPenjualanResepT::model()->findByPk($idPenjualan);
                 $modDokter = new FADokterV();
                 $modPegawai = FAPegawaiM::model()->findByPk($modPenjualan->pasienpegawai_id);
                 $modDokter->nama_pegawai = $modPegawai->nama_pegawai;
                 $modDokter->nomorindukpegawai = $modPegawai->nomorindukpegawai;
                 $modDokter->jeniskelamin = $modPegawai->jeniskelamin;
                 $modDokter->alamat_pegawai = $modPegawai->alamat_pegawai;
                 $modReseptur = FAResepturT::model()->findByAttributes(array('penjualanresep_id'=>$idPenjualan));
                if ((boolean)count($modPenjualan)){
                    $modPenjualan->isNewRecord = false;
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }

            if(isset($_POST['penjualanResep'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPenjualan = $this->updatePenjualanResep($modPenjualan, $_POST['FAPenjualanResepT']);
                    $obatAlkes = $this->updateObatAlkes($obatAlkes, $modPenjualan, $_POST['penjualanResep']);
                    if($this->successUpdatePenjualan && $this->successUpdateObat){
                        $transaction->commit();
                        $sukses = 2; // 2= Sukses Update
                        Yii::app()->user->setFlash('success','Transaksi Penjualan Dokter berhasil diubah !');
                        $this->redirect(array('InformasiPenjualanDokter/Index'));
                    }else{
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->successUpdateObat)
                            $dataGagal = "Data Obat Alkes";
                        Yii::app()->user->setFlash('error',"Transaksi gagal disimpan. Silahkan cek ".$dataGagal);
                        $sukses = 0;
                    }
                }catch (Exception $exc) {
                    $modPenjualan->isNewRecord = false;
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
                
            }

            $this->render('index',array(
                                'modDokter'=>$modDokter,
                                'obatAlkes'=>$obatAlkes,
                                'modPenjualan'=>$modPenjualan,
                                'racikan'=>$racikan,
                                'racikanDetail'=>$racikanDetail,
                                'nonRacikan'=>$nonRacikan,
                                'konfigFarmasi'=>$konfigFarmasi,
                                'sukses'=>$sukses,
                            ));
        }
        
        /**
         * updateObatAlkes extend ke PenjualanResepController
         * karena fungsi yang ini salah algoritma (sudah di test):
         * - stok tidak kembali
         * commented by: 
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
//    protected function updateObatAlkes($modObatAlkes, $modPenjualan,$penjualanResep)
//    {
//        //== Update Data Obat Lama + Simpan Data Obat Baru ==
//        foreach ($penjualanResep as $i => $obatResep) {
//            if($obatResep['detailreseptur_id'] == 1){ // Jika pilih dicentang
//                if(empty($obatResep['obatalkespasien_id'])){ //Jika data obat baru
//                    $modObatAlkes[$i] = new FAObatalkesPasienT();
//                    $modObatAlkes[$i]->attributes = $obatResep;
//                    $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
//                    $modObatAlkes[$i]->penjualanresep_id = $modPenjualan->penjualanresep_id;
//                    $modObatAlkes[$i]->pendaftaran_id = $modPenjualan->pendaftaran_id;
//                    $modObatAlkes[$i]->penjamin_id = $modPenjualan->penjamin_id;
//                    $modObatAlkes[$i]->carabayar_id = $modPenjualan->carabayar_id;
//                    $modObatAlkes[$i]->pasien_id = $modPenjualan->pasien_id;
//                    $modObatAlkes[$i]->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
//                    if($modObatAlkes[$i]->pegawai_id){
//                            $modObatAlkes[$i]->pegawai_id =$_POST['FAResepturT']['pegawai_id'];
//                    }else{
//                        $modObatAlkes[$i]->pegawai_id = Yii::app()->user->getState('pegawai_id');;
//                    }
//
//                    $modObatAlkes[$i]->discount = ($obatResep['discount']!=0) ? $obatResep['discount'] : 0; 
//                    $modObatAlkes[$i]->oa = 'AP';
//                    $modObatAlkes[$i]->create_loginpemakai_id = Yii::app()->user->id;
//                    $modObatAlkes[$i]->create_time = date('Y-m-d H:i:s');
//                    $modObatAlkes[$i]->shift_id = Yii::app()->user->getState('shift_id');
//                    $modObatAlkes[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                    $modObatAlkes[$i]->obatalkes_id = $obatResep['obatalkes_id'];
//                    $modObatAlkes[$i]->subsidiasuransi = 0;
//                    $modObatAlkes[$i]->subsidipemerintah = 0;
//                    $modObatAlkes[$i]->subsidirs = 0;
//                    $modObatAlkes[$i]->iurbiaya = 0;
//                    $modObatAlkes[$i]->tarifcyto = 0;
//                    $modObatAlkes[$i]->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
//                    $modObatAlkes[$i]->harganetto_oa = $obatResep['harganetto_reseptur'];
//                    $modObatAlkes[$i]->hargajual_oa = $obatResep['hargajual_reseptur'];
//                }
//                    $modObatAlkes[$i]->obatalkes_id = $obatResep['obatalkes_id'];
////                    echo "<pre>";
////                    echo $obatResep['obatalkes_id'];
////                    echo "</pre>";
////                    
//                    $modObatAlkes[$i]->permintaan_oa = $obatResep['permintaan_reseptur'];
//                    $modObatAlkes[$i]->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
//                    $modObatAlkes[$i]->qty_selisih = $obatResep['qty'] - $modObatAlkes[$i]->qty_oa ;
//                    $modObatAlkes[$i]->qty_oa = $obatResep['qty'];
//                    $modObatAlkes[$i]->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
//                    $modObatAlkes[$i]->harganetto_oa = $obatResep['harganetto_reseptur'];
//                    $modObatAlkes[$i]->hargajual_oa = $obatResep['hargajual_reseptur'];
//                    $modObatAlkes[$i]->signa_oa = $obatResep['signa_reseptur'];
//                    $modObatAlkes[$i]->discount = (empty($obatResep['discount'])) ? 0 : $obatResep['discount']; 
//                    $modObatAlkes[$i]->obatalkes_id = $obatResep['obatalkes_id'];
//                    if ($i == 0){ //hanya simpan untuk detail yang pertama
//                        $modObatAlkes[$i]->biayaservice = $modPenjualan->totaltarifservice;
//                        $modObatAlkes[$i]->biayaadministrasi = $modPenjualan->biayaadministrasi;
//                        $modObatAlkes[$i]->biayakonseling = $modPenjualan->biayakonseling;
//                        $modObatAlkes[$i]->jasadokterresep = $modPenjualan->jasadokterresep;
//                    }else{
//                        $modObatAlkes[$i]->biayaservice = 0;
//                        $modObatAlkes[$i]->biayaadministrasi = 0;
//                        $modObatAlkes[$i]->biayakonseling = 0;
//                        $modObatAlkes[$i]->jasadokterresep = 0;
//                    }
//                    $modObatAlkes[$i]->tglpelayanan = $modPenjualan->tglpenjualan;
//                    if($obatResep['disc']!=0){ $modObatAlkes[$i]->discount = $obatResep['disc']; }
//                    $modObatAlkes[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
//                    $modObatAlkes[$i]->update_loginpemakai_id = Yii::app()->user->id;
//                    $modObatAlkes[$i]->update_time = date('Y-m-d H:i:s');
//                    $stok = StokobatalkesT::model()->getStokBarang($modObatAlkes[$i]->obatalkes_id,$modObatAlkes[$i]->ruangan_id);
//                    $cekStok = Yii::app()->db->createCommand($stok)->queryAll();
////                    echo "<pre>";
////                    echo print_r($cekStok);
////                    echo "</pre>";
//                    if($modObatAlkes[$i]->validate()){
////                           echo "<pre>";
////                           echo print_r($modObatAlkes[$i]->Attributes);
////                           echo "</pre>";
//                        if(!empty($cekStok)){
//                               if($modObatAlkes[$i]->qty_oa <= $stok){ // cek stok vs qty
//                                $modObatAlkes[$i]->save();
//                                //Update StokObatAlkes
//                                if(!empty($obatResep['obatalkespasien_id']))
//                                    $this->kurangiStok($modObatAlkes[$i]->qty_selisih, $modObatAlkes[$i]->obatalkes_id);
//                                else
//                                    $this->kurangiStok($modObatAlkes[$i]->qty_oa, $modObatAlkes[$i]->obatalkes_id);
//                                $this->successUpdateObat = true; 
//                            }else{
//                                Yii::app()->user->setFlash('warning',"Stok Obat Alkes ".$modObatAlkes[$i]->obatalkes->obatalkes_kode." - ".$modObatAlkes[$i]->obatalkes->obatalkes_nama." tidak ada");
//                            }
//                        }else{
//                            echo "<script>
//                                        alert('Stok Obat Alkes ".$modObatAlkes[$i]->obatalkes->obatalkes_kode.
//                                                "-".$modObatAlkes[$i]->obatalkes->obatalkes_nama." Tidak Mencukupi');
//                                </script>";
//                        }
//                    } else {
//                        $this->successUpdateObat = false;
//                        Yii::app()->user->setFlash('error',"Data Obat Alkes Tidak valid $i"."<pre>".print_r($modObatAlkes[$i]->attributes,1)."</pre>");
//                    }
//            }else{ //Jika pilih tidak dicentang data di hapus
////                    $modObatAlkes[$i]->deleteByPk($modObatAlkes[$i]->obatalkespasien_id);
//                //Retur Stok
//            }
//            $modObat[$i] = $modObatAlkes;
//        }
//        exit;
//        //==================================   
//        //== Tambahkan Item Baru Jika ada ==
//
//        //== Hapus Item Lama Jika Tidak Di pilih ==
//
//        return $modObat;
//    }
    
     
        
    
    /**
    * actionPrintFakturPenjualanDokter digunakan untuk print faktur pada Transaksi Penjualan Dokter
    * @param type $id
    */



    public function actionPrintFakturPenjualanDokter(){
      $id = $_REQUEST['id'];
      $this->layout = '//layouts/frameDialog';
      $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
      $modDokter = PegawaikaryawanV::model()->findByAttributes(array('pegawai_id'=>$modPenjualan->pasienpegawai_id));
      $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
      $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
      $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
       $judulLaporan='Laporan Penerimaan Kas';
       $caraPrint=$_REQUEST['caraPrint'];
       if($caraPrint=='PRINT') {
           $this->layout='//layouts/printWindows';
           // $this->render('farmasiApotek.views.penjualanDokter.PrintFakturPenjualanDokter', array('modDokter'=>$modDokter,'modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
       }
        $this->render('farmasiApotek.views.penjualanDokter.PrintFakturPenjualanDokter', array('modDokter'=>$modDokter,'modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        
    }


}

?>
