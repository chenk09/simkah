<?php

class LaporanFarmasiController extends SBaseController
{
    //untuk range tanggal default
    public $tglAwal = "d M Y 00:00:00";
    public $tglAkhir = "d M Y 23:59:59";
    public function actionLaporanJasaServices()
    {
        $model = new FAPenjualanResepT();
        $model->unsetAttributes();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
        if(isset($_GET['FAPenjualanResepT']))
        {
            $model->attributes = $_GET['FAPenjualanResepT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAkhir']);
            $model->no_rekam_medik = $_GET['FAPenjualanResepT']['no_rekam_medik'];
            $model->nama_pasien = trim($_GET['FAPenjualanResepT']['nama_pasien']);
            $model->no_pendaftaran = trim($_GET['FAPenjualanResepT']['no_pendaftaran']);

        }
        $this->render('jasaServices/admin',array('model'=>$model));
    }

    public function actionPrintLaporanJasaServices()
    {
        $model = new FAPenjualanResepT();
        $judulLaporan = 'Laporan Jasa Services';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Services';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FAPenjualanResepT'])) {
           $model->attributes = $_REQUEST['FAPenjualanResepT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAkhir']);
            $model->no_rekam_medik = $_GET['FAPenjualanResepT']['no_rekam_medik'];
            $model->nama_pasien = trim($_GET['FAPenjualanResepT']['nama_pasien']);
            $model->no_pendaftaran = trim($_GET['FAPenjualanResepT']['no_pendaftaran']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'jasaServices/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

     public function actionHutangTitipanApotek()
    {
            $format = new CustomFormat();
            $model=new FAHutangtitipanapotikV('searchLaporan');
            $model->unsetAttributes();
			$model->tglAwal = date($this->tglAwal);
			$model->tglAkhir = date($this->tglAkhir);
            if(isset($_GET['FAHutangtitipanapotikV'])){
				$model->attributes=$_GET['FAHutangtitipanapotikV'];
				$model->tglAwal=$format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAwal']);
				$model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAkhir']);
            }
            $this->render('hutangTitipanApotek/index',array(
                    'model'=>$model,
            ));
    }

	public function actionPrintHutangTitipanApotik()
    {

		$format = new CustomFormat();
        $model = new FAHutangtitipanapotikV('searchLaporan');
        $judulLaporan = 'Laporan Hutang Titipan Apotik';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Hutang Titipan Apotik';
        $data['type'] = $_GET['type'];
		if(isset($_GET['FAHutangtitipanapotikV'])){
			$model->attributes=$_GET['FAHutangtitipanapotikV'];
			$model->tglAwal=$format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAwal']);
			$model->tglAkhir=$format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAkhir']);
		}

        $caraPrint = $_GET['caraPrint'];
        $target = 'hutangTitipanApotek/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);

    }

    public function actionFrameGrafikLaporanJasaServices() {
        $this->layout = '//layouts/frameDialog';
        $model = new FAPenjualanResepT();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);

        $data['title'] = 'Grafik Laporan Jasa Services';
        $data['type'] = $_GET['type'];
        $nilai = 1;
        if(isset($_GET['FAPenjualanResepT']))
        {
            $model->attributes = $_GET['FAPenjualanResepT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAPenjualanResepT']['tglAkhir']);
            $model->no_rekam_medik = $_GET['FAPenjualanResepT']['no_rekam_medik'];
            $model->nama_pasien = trim($_GET['FAPenjualanResepT']['nama_pasien']);
            $model->no_pendaftaran = trim($_GET['FAPenjualanResepT']['no_pendaftaran']);

        }
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data, 'nilai'=>$nilai
        ));
    }

    public function actionLaporanPemakaianKategoriObat()
    {
        $model = new FALaporanpenjualanobatV();
        $model->unsetAttributes();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $kategori = CHtml::listData(LookupM::model()->findAllByAttributes(array('lookup_type'=>'obatalkes_kategori')),'lookup_value','lookup_name');
        $model->obatalkes_kategori = $kategori;
        if(isset($_GET['FALaporanpenjualanobatV']))
        {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }
        $this->render('pemakaianKategoriObat/admin',array('model'=>$model));
    }

    public function actionPrintLaporanPemakaianKategoriObat()
    {
        $model = new FALaporanpenjualanobatV();
        $judulLaporan = 'Laporan Kategori Obat Alkes';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kategori Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpenjualanobatV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'pemakaianKategoriObat/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPemakaianKategoriObat() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanpenjualanobatV();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kategori Obat dan Alkes';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanpenjualanobatV'])) {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanPenjualanObat()
    {
        $model = new FALaporanpenjualanobatV();
        $model->unsetAttributes();
        $model->tglAwal = date($this->tglAwal);
        $model->carabayar_id = 1;
        $model->tglAkhir = date($this->tglAkhir);
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'),'penjamin_id','penjamin_id');
        $ruangan = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_nama','ruangan_nama');
        $model->penjamin_id = $penjamin;
        $model->ruanganasal_nama = $ruangan;
        if(isset($_GET['FALaporanpenjualanobatV']))
        {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
            $model->jenispenjualan = $_GET['FALaporanpenjualanobatV']['jenispenjualan'];
        }
        $this->render('penjualanObat/admin',array('model'=>$model));
    }

    public function actionPrintLaporanPenjualanObat() {
        $model = new FALaporanpenjualanobatV('search');
        $judulLaporan = 'Laporan Penjualan Pasien Obat dan Alkes';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Jenis Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpenjualanobatV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'penjualanObat/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikLaporanPenjualanObat() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanpenjualanobatV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $nilai = 2;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Pasien Obat dan Alkes';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanpenjualanobatV'])) {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'nilai' =>$nilai,
        ));
    }

    public function actionLaporanPenjualanJenisoa()
    {
        $model = new FALaporanpenjualanjenisoaV();
        $model->unsetAttributes();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $jenis = CHtml::listData(JenisobatalkesM::model()->findAll('jenisobatalkes_aktif = true'), 'jenisobatalkes_id','jenisobatalkes_id');
        $model->jenisobatalkes_id = $jenis;
        if(isset($_GET['FALaporanpenjualanjenisoaV']))
        {
            $model->attributes = $_GET['FALaporanpenjualanjenisoaV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanjenisoaV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanjenisoaV']['tglAkhir']);
        }
        $this->render('penjualanJenisoa/admin',array('model'=>$model));
    }

    public function actionPrintLaporanPenjualanJenisoa() {
        $model = new FALaporanpenjualanjenisoaV('search');
        $judulLaporan = 'Laporan Penjualan Jenis Obat dan Alkes';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Jenis Obat Alkes';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpenjualanjenisoaV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanjenisoaV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanjenisoaV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanjenisoaV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'penjualanJenisoa/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikLaporanPenjualanJenisoa() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanpenjualanjenisoaV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $nilai = 1;
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Jenis Obat dan Alkes';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanpenjualanjenisoaV'])) {
            $model->attributes = $_GET['FALaporanpenjualanjenisoaV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanjenisoaV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanjenisoaV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'nilai'=> $nilai,
        ));
    }

    public function actionLaporanPenjualanLembarResep()
    {
        $model = new FALaporanlembarresepV();
        $model->unsetAttributes();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $penjamin =CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'),'penjamin_id','penjamin_id');
        $ruangan =CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_nama','ruangan_nama');
        $model->penjamin_id = $penjamin;
        $model->ruanganasal_nama = $ruangan;
        if(isset($_GET['FALaporanlembarresepV']))
        {
            $model->attributes = $_GET['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAkhir']);
        }
        $this->render('lembarResep/admin',array('model'=>$model));
    }

    public function actionPrintLaporanPenjualanLembarResep() {
        $model = new FALaporanlembarresepV('search');
        $judulLaporan = 'Laporan Penjualan Lembar Resep';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Lembar Resep';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanlembarresepV'])) {
            $model->attributes = $_REQUEST['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'lembarResep/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikLaporanPenjualanLembarResep() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanlembarresepV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Lembar Resep';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanlembarresepV'])) {
            $model->attributes = $_GET['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    // Added Laporan Stok Obat Farmasi on March, 6 2013 //
         public function actionLaporanStockFarmasi() {
				$model = new FAInfostokobatalkesruanganV;
				$model->tglAwal = date('d M Y');
				$model->tglAkhir = date('d M Y');
				$model->qtystok_in = '0';
				$model->qtystok_out = '0';
				if (isset($_GET['FAInfostokobatalkesruanganV'])) {
					$format = new CustomFormat;
					$model->attributes = $_GET['FAInfostokobatalkesruanganV'];
					$model->tglAwal = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAwal']);
					$model->tglAkhir = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAkhir']);
					$model->qtystok_in = $_GET['FAInfostokobatalkesruanganV']['qtystok_in'];
					$model->qtystok_out  = $_GET['FAInfostokobatalkesruanganV']['qtystok_out'];
				}
				$this->render('stock/stock',array(
					'model'=>$model,
				));
		}

        public function actionPrintStock()
        {
            $model = new FAInfostokobatalkesruanganV;
            $model->tglAwal = date('d M Y');
            $model->tglAkhir = date('d M Y');
            $judulLaporan = 'Stock Barang';

            //Data Grafik
            $data['title'] = 'Grafik Stock Obat Farmasi';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['FAInfostokobatalkesruanganV'])) {
                    $format = new CustomFormat;
                    $model->attributes = $_GET['FAInfostokobatalkesruanganV'];
                    $model->tglAwal = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAkhir']);
            }
            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'stock/printStock';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

        public function actionFrameStockFarmasi() {
             //if(!Yii::app()->user->checkAccess(Params::DEFAULT_LAPORAN_RETAIL)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $this->layout = '//layouts/frameDialog';

            $model = new FAInfostokobatalkesruanganV;
//            $model->tglAwal = date('d M Y 00:00:00');
//            $model->tglAkhir = date('d M Y 23:59:59');

            //Data Grafik
            $data['title'] = 'Grafik Stock Obat Farmasi';
            $data['type'] = $_GET['type'];

            if (isset($_REQUEST['FAInfostokobatalkesruanganV'])) {
                    $format = new CustomFormat;
                    $model->attributes = $_GET['FAInfostokobatalkesruanganV'];
//                    $model->tglAwal = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAwal']);
//                    $model->tglAkhir = $format->formatDateMediumForDB($_GET['FAInfostokobatalkesruanganV']['tglAkhir']);
            }
            $searchdata = $model->searchGrafik();
            $this->render('_grafik', array(
                'model' => $model,
                'data' => $data,
                'searchdata'=>$searchdata,
            ));
        }

     // End Added Laporan Stok Obat Farmasi //

    /* laporan retur obat */
    public function actionLaporanReturObat()
    {
		$model = new FAReturResepV();
		$model->unsetAttributes();
		$model->tglAwal = date($this->tglAwal);
		$model->tglAkhir = date($this->tglAkhir);

		if(isset($_GET['FAReturResepV']))
		{
			$model->attributes = $_GET['FAReturResepV'];

			$format = new CustomFormat();
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAkhir']);

		}

		/*
		$model = new FALaporanreturobatV();
		$model->unsetAttributes();

		//        $model->tglAwal = date('d M Y H:i:s');
		//        $model->tglAkhir = date('d M Y H:i:s');

		$model->tglAwal = date($this->tglAwal);
		$model->tglAkhir = date($this->tglAkhir);

		if(isset($_GET['FALaporanreturobatV']))
		{
			$model->attributes = $_GET['FALaporanreturobatV'];

			$format = new CustomFormat();
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAkhir']);

			$model->jenispenjualan = $_GET['FALaporanreturobatV']['jenispenjualan'];
			$model->nama_pasien = trim($_GET['FALaporanreturobatV']['nama_pasien']);
			$model->no_pendaftaran = trim($_GET['FALaporanreturobatV']['no_pendaftaran']);
		}
		*/
		$this->render('returObat/admin',array('model'=>$model));
    }

    public function actionPrintLaporanReturObat()
    {
		$judulLaporan = 'Laporan Retur Obat';

		$model = new FAReturResepV();
		$model->unsetAttributes();

		$model->tglAwal = date($this->tglAwal);
		$model->tglAkhir = date($this->tglAkhir);

		//Data Grafik
        $data['title'] = 'Grafik Laporan Retur Obat';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;

		if(isset($_GET['FAReturResepV']))
		{
			$model->attributes = $_GET['FAReturResepV'];

			$format = new CustomFormat();
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAkhir']);

		}

		/*
        $model = new FALaporanreturobatV();
        $judulLaporan = 'Laporan Retur Obat';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Retur Obat';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanreturobatV']))
		{
           $model->attributes = $_REQUEST['FALaporanreturobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAkhir']);
            $model->no_rekam_medik = $_GET['FALaporanreturobatV']['no_rekam_medik'];
            $model->nama_pasien = trim($_GET['FALaporanreturobatV']['nama_pasien']);
            $model->no_pendaftaran = trim($_GET['FALaporanreturobatV']['no_pendaftaran']);
        }
		*/

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'returObat/_printReturObat';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikReturObat()
	{
        $this->layout = '//layouts/frameDialog';

		$model = new FAReturResepV();
		$model->unsetAttributes();
		$model->tglAwal = date($this->tglAwal);
		$model->tglAkhir = date($this->tglAkhir);

		if(isset($_GET['FAReturResepV']))
		{
			$model->attributes = $_GET['FAReturResepV'];

			$format = new CustomFormat();
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAReturResepV']['tglAkhir']);

		}
		$nilai = 1;
/*
        $model = new FALaporanreturobatV();
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        $data['title'] = 'Grafik Laporan Retur Obat';
        $data['type'] = $_GET['type'];
        $nilai = 1;
        if(isset($_GET['FALaporanreturobatV']))
        {
            $model->attributes = $_GET['FALaporanreturobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanreturobatV']['tglAkhir']);
            $model->no_rekam_medik = $_GET['FALaporanreturobatV']['no_rekam_medik'];
            $model->nama_pasien = trim($_GET['FALaporanreturobatV']['nama_pasien']);
            $model->no_pendaftaran = trim($_GET['FALaporanreturobatV']['no_pendaftaran']);

        }
*/
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
			'nilai'=>$nilai
        ));
    }
    /* end laporan retur obat */

    /* laporan stock opname */
    public function actionLaporanStockOpname() {
        $model = new FALaporanfarmasikopnameV();
        // $model->unsetAttributes();
        $model->tglAwal = date('d M Y');
        $model->tglAkhir = date('d M Y');
        // $model->jenisobatalkes_id = 1;

        if (isset($_GET['FALaporanfarmasikopnameV'])) {
            // $model->unsetAttributes();
            $format = new CustomFormat();
            $model->attributes = $_GET['FALaporanfarmasikopnameV'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAkhir']);
            $model->jenisobatalkes_id = $_GET['FALaporanfarmasikopnameV']['jenisobatalkes_id'];

            // echo var_dump($model->tglAwal);
            // echo "<br>";
            // echo var_dump($model->tglAkhir);
        }

        $this->render('stockOpname/admin', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanStockOpname() {
        $model = new FALaporanfarmasikopnameV('search');
        $judulLaporan = 'Laporan Stock Opname';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Stock Opname';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['FALaporanfarmasikopnameV'])) {
            $model->attributes = $_REQUEST['FALaporanfarmasikopnameV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'stockOpname/_printStockOpname';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikStockOpname() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanfarmasikopnameV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Stock Opname';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanfarmasikopnameV'])) {
            $model->attributes = $_GET['FALaporanfarmasikopnameV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanfarmasikopnameV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /* end laporan stock opname */
     /* ============================= Keperluan function laporan ======================================== */
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function printFunction2($model, $modKinerja, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $tglawal = $format->formatDateMediumForDB($model->tglAwal);
        $tglakhir = $format->formatDateMediumForDB($model->tglAkhir);
        $periode = $this->parserTanggal($tglawal).' s/d '.$this->parserTanggal($tglakhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'modKinerja' =>$modKinerja, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'modKinerja' =>$modKinerja, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'modKinerja' =>$modKinerja, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

    }
    /**
     * actionLaporanJasaRacikan untuk laporan jasa racik
     */
    public function actionLaporanJasaRacikan()
    {
        $format = new CustomFormat();
//        $model = new FAPenjualanResepT('searchPrintJasaRacikan');
        $model = new FALaporanpenjualanobatV('searchTableJasaRacikan');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        if(isset($_GET['FALaporanpenjualanobatV']))
        {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }
        $this->render('jasaRacikan/admin',array('model'=>$model));
    }
    /**
     * actionLaporanJasaRacikan untuk laporan jasa racikan
     */
    public function actionPrintLaporanJasaRacikan()
    {
//        $model = new FAPenjualanResepT();
        $model = new FALaporanpenjualanobatV('searchTableJasaRacikan');
        $judulLaporan = 'Laporan Jasa Racikan';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Racikan';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpenjualanobatV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'jasaRacikan/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    /**
     * actionLaporanJasaDokter untuk laporan jasa dokter resep
     */
    public function actionLaporanJasaDokter()
    {
        $format = new CustomFormat();
//        $model = new FAPenjualanResepT('searchPrintJasaRacikan');
        $model = new FALaporanpejulanresepdokterV('searchTableJasaDokter');
        $model->unsetAttributes();
        $model->tglAwal = $format->formatDateTimeMediumForDB(date($this->tglAwal));
        $model->tglAkhir = $format->formatDateTimeMediumForDB(date($this->tglAkhir));

        if(isset($_GET['FALaporanpejulanresepdokterV']))
        {
            $model->attributes = $_GET['FALaporanpejulanresepdokterV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpejulanresepdokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpejulanresepdokterV']['tglAkhir']);
        }
        $this->render('jasaDokter/admin',array('model'=>$model));
    }
    /**
     * actionPrintLaporanJasaDokter untuk laporan jasa dokter
     */
    public function actionPrintLaporanJasaDokter()
    {
//        $model = new FAPenjualanResepT();
        $model = new FALaporanpejulanresepdokterV();
        $judulLaporan = 'Laporan Jasa Dokter Resep';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Dokter Resep';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpejulanresepdokterV'])) {
           $model->attributes = $_REQUEST['FALaporanpejulanresepdokterV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpejulanresepdokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpejulanresepdokterV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'jasaDokter/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }



    /**
     * laporan pendapatan farmasi berdasarkan transaksi
     */
    public function actionLaporanPendapatanTransaksi()
    {
        $format = new CustomFormat();
        $model = new FALaporanpendapatanfarmasiV('searchTablePendapatanTransaksi');
        $model->unsetAttributes();
        $model->tglAwal = $format->formatDateTimeMediumForDB(date($this->tglAwal));
        $model->tglAkhir = $format->formatDateTimeMediumForDB(date($this->tglAkhir));

        if(isset($_GET['FALaporanpendapatanfarmasiV'])){
            $model->attributes = $_GET['FALaporanpendapatanfarmasiV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }
        $this->render('pendapatanTransaksi/admin',array('model'=>$model));
    }

    public function actionPrintLaporanPendapatanTransaksi()
    {
        $model = new FALaporanpendapatanfarmasiV();
        $judulLaporan = 'Laporan Pendapatan Farmasi Apotek';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Farmasi Apotek';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpendapatanfarmasiV'])) {
           $model->attributes = $_REQUEST['FALaporanpendapatanfarmasiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'pendapatanTransaksi/print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    /**
     * laporan pendapatan farmasi berdasarkan obat alkes (detail transaksi)
     */
    public function actionLaporanPendapatanObatAlkes()
    {
        $format = new CustomFormat();
        $model = new FALaporanPendAlkesV;
        $model->unsetAttributes();

        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        
        if(isset($_GET['FALaporanPendAlkesV'])){
            $model->attributes = $_GET['FALaporanPendAlkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanPendAlkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanPendAlkesV']['tglAkhir']);
        }
        $periode = $model->tglAwal.' s/d '. $model->tglAkhir;
        
        if(isset($_GET['print']) && $_GET['print'] == true){
          $model->print = $_GET['print'];
          $model->typePrint = $_GET['type_print'];
          
          if ($_GET['type_print'] == 'print' || $_GET['type_print'] == 'GRAFIK') {
                $this->layout = '//layouts/printWindows';
            } else if ($_GET['type_print'] == 'excel') {
                $this->layout = '//layouts/printExcel';
                $caraPrint = 'EXCEL';
            }
            
          $this->render('pendapatanObatAlkes/print', array('caraPrint' => $caraPrint, 'model'=>$model, 'periode' => $periode, 'judulLaporan' => 'Laporan Pendapatan Obat Alkes'));
          Yii::app()->end();
        }else $this->render('pendapatanObatAlkes/admin',array('model'=>$model));

        /*
        $model = new FALaporanpendapatanfarmasiV('searchTablePendapatanObatAlkes');
        $model->unsetAttributes();
        $model->tglAwal = $format->formatDateTimeMediumForDB(date($this->tglAwal));
        $model->tglAkhir = $format->formatDateTimeMediumForDB(date($this->tglAkhir));

        if(isset($_GET['FALaporanpendapatanfarmasiV'])){
            $model->attributes = $_GET['FALaporanpendapatanfarmasiV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }
        $this->render('pendapatanObatAlkes/admin',array('model'=>$model));
        */
    }

    public function actionPrintLaporanPendapatanObatAlkes()
    {
        $model = new FALaporanpendapatanfarmasiV();
        $judulLaporan = 'Laporan Pendapatan Farmasi Apotek<h3> Jenis : ';
        $data['title'] = 'Grafik Laporan Pendapatan Farmasi Apotek <h3> Jenis : ';
        foreach($_REQUEST['FALaporanpendapatanfarmasiV']['jenisobatalkes_id'] AS $jenis){
            $judulLaporan .= JenisobatalkesM::model()->findByPk($jenis)->jenisobatalkes_nama.', ';
            $data['title'] .= JenisobatalkesM::model()->findByPk($jenis)->jenisobatalkes_nama.', ';
        }
        //Data Grafik
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpendapatanfarmasiV'])) {
           $model->attributes = $_REQUEST['FALaporanpendapatanfarmasiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'pendapatanObatAlkes/print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


   /**
     * laporan pendapatan total farmasi
     */
    public function actionLaporanPendapatanTotalFarmasi()
    {
        $format = new CustomFormat();
        $model = new FALaporanPendAlkesV;
        $model->unsetAttributes();

        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        
        if(isset($_GET['FALaporanPendAlkesV'])){
            $model->attributes = $_GET['FALaporanPendAlkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanPendAlkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanPendAlkesV']['tglAkhir']);
        }
        $periode = $model->tglAwal.' s/d '. $model->tglAkhir;
        if(isset($_GET['print']) && $_GET['print'] == true){
          $model->print = $_GET['print'];
          $model->typePrint = $_GET['type_print'];
          $caraPrint = '';
          if ($_GET['type_print'] == 'print' || $_GET['type_print'] == 'GRAFIK') {
                $this->layout = '//layouts/printWindows';
            } else if ($_GET['type_print'] == 'excel') {
                $this->layout = '//layouts/printExcel';
                $caraPrint = 'EXCEL';
            }
            
          $this->render('totalPendapatanFarmasi/print', array('caraPrint' => $caraPrint, 'model'=>$model, 'periode' => $periode, 'judulLaporan' => 'Laporan Pendapatan Total Farmasi'));

        }else $this->render('totalPendapatanFarmasi/admin_dua',array('model'=>$model));
/*
     $this->render('totalPendapatanFarmasi/admin_dua',array('model'=>$model));
        $format = new CustomFormat();
        $model = new FALaporanpendapatanfarmasiV('searchTableTotalPendapatanFarmasi');
        $model->unsetAttributes();
        $model->tglAwal = $format->formatDateTimeMediumForDB(date($this->tglAwal));
        $model->tglAkhir = $format->formatDateTimeMediumForDB(date($this->tglAkhir));

        if(isset($_GET['FALaporanpendapatanfarmasiV'])){
            $model->attributes = $_GET['FALaporanpendapatanfarmasiV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }
        $this->render('totalPendapatanFarmasi/admin',array('model'=>$model));
*/
    }

    public function actionLaporanPendapatanTotalFarmasi_old() { //jangan di hapus
        $model = new FALaporanpendapatanfarmasiV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['FALaporanpendapatanfarmasiV'])) {
            $model->attributes = $_GET['FALaporanpendapatanfarmasiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }
        $models = $model->findAll($model->searchTableTotalPendapatan());
        if (Yii::app()->request->isAjaxRequest) {
            echo $this->renderPartial('farmasiApotek.laporanFarmasi.LaporanPendapatanTotalFarmasi_table',
                        array(
                            'model'=>$model,
                            'models'=>$models,
                             ), true
                    );
        }else{
        $this->render('totalPendapatanFarmasi/admin', array(
            'model' => $model,
            'models' => $models,
            'filter'=>$filter
        ));
        }
    }


    public function actionPrintLaporanPendapatanTotalFarmasi()
    {
        $model = new FALaporanpendapatanfarmasiV();
        $judulLaporan = 'Laporan Total Pendapatan Farmasi';
        $data['title'] = 'Grafik Laporan Pendapatan Farmasi Apotek';
        // foreach($_REQUEST['FALaporanpendapatanfarmasiV']['jenisobatalkes_id'] AS $jenis){
        //     $judulLaporan .= JenisobatalkesM::model()->findByPk($jenis)->jenisobatalkes_nama.', ';
        //     $data['title'] .= JenisobatalkesM::model()->findByPk($jenis)->jenisobatalkes_nama.', ';
        // }
        //Data Grafik
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai']=LoginpemakaiK::model()->findByPk(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['FALaporanpendapatanfarmasiV'])) {
           $model->attributes = $_REQUEST['FALaporanpendapatanfarmasiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpendapatanfarmasiV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        if ($caraPrint == "PRINTRINCIAN"){
            $caraPrint = 'PRINT';
            $data['rincian']= true;
        }
        $target = 'totalPendapatanFarmasi/print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
}
