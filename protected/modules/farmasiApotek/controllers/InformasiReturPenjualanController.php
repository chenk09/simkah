<?php

class InformasiReturPenjualanController extends SBaseController
{
	public function actionIndex()
	{
            $modInfoReturPenjualan = new ReturresepT();
            $modInfoReturPenjualan->unsetAttributes();
            $modInfoReturPenjualan->tglAwal = date("Y-m-d").' 00:00:00';
            $modInfoReturPenjualan->tglAkhir = date("Y-m-d").' 23:59:59';
            if(isset($_GET['ReturresepT'])){
                $format = new CustomFormat();
                $modInfoReturPenjualan->attributes = $_GET['ReturresepT'];
                $modInfoReturPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['ReturresepT']['tglAwal']);
                $modInfoReturPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ReturresepT']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoReturPenjualan'=>$modInfoReturPenjualan));
        }
        
        public function actionDetailReturPenjualan($id) {
        $this->layout = '//layouts/frameDialog';
        
            $modRetur = ReturresepT::model()->findByPk($id);
            $detail = ReturresepdetT::model()->findAll('returresep_id = '.$modRetur->returresep_id);
            $modPenjualanResep = PenjualanresepT::model()->findByPk($modRetur->penjualanresep_id);
            $modPendaftaran = PendaftaranT::model()->findByPk($modPenjualanResep->pendaftaran_id);
            if (count($detail) > 0){
                $details = array();
                foreach ($detail as $key => $value) {
                    $obatalkes = ObatalkespasienT::model()->findByPk($value->obatalkespasien_id);
                    $details[0]['uraian'] = "Obatalkes";
                    $details[0]['harga'] += ($obatalkes->hargajual_oa + $obatalkes->biayaadministrasi + $obatalkes->biayakonseling + $obatalkes->biayaservice + $obatalkes->jasadokterresep);
                    $details[0]['diskon'] += ($obatalkes->hargasatuan_oa*$obatalkes->discount)/100;
                    $details[0]['qty'] = 1;
                }
            }

        $this->render('view', array('model'=>$model,'details'=>$details, 'modRetur'=>$modRetur, 'modPenjualanResep'=>$modPenjualanResep,
                                    'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 
                                    ));
        }
}