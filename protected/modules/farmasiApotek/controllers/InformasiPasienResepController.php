<?php

class InformasiResepPasienController extends InformasiPenjualanResepController
{
	public function actionIndex()
	{
            $modInfoPenjualan = new FAInformasipenjualanresepV('searchInfoJualResep');
            $modInfoPenjualan->unsetAttributes();
            $modInfoPenjualan->tglAwal = date("d M Y").' 00:00:00';
            $modInfoPenjualan->tglAkhir = date("d M Y").' 23:59:59';
            if(isset($_GET['FAInformasipenjualanresepV'])){
                $format = new CustomFormat();
                $modInfoPenjualan->attributes = $_GET['FAInformasipenjualanresepV'];
                $modInfoPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAwal']);
                $modInfoPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoPenjualan'=>$modInfoPenjualan));
        }
}