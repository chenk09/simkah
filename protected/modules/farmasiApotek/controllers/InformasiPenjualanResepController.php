<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class InformasiPenjualanResepController extends SBaseController
{
    public $suksesRetur = false; 
    public $suksesReturStok = true; //karna di looping
    public $suksesUpdateObatAlkesPasien = true; //karna di looping
	public function actionIndex()
	{
            $modInfoPenjualan = new FAInformasipenjualanresepV('searchInfoJualResep');
            $modInfoPenjualan->unsetAttributes();
            $modInfoPenjualan->jenispenjualan =null;
            $modInfoPenjualan->tglAwal = date("d M Y").' 00:00:00';
            $modInfoPenjualan->tglAkhir = date("d M Y").' 23:59:59';
            if(isset($_GET['FAInformasipenjualanresepV'])){
                $format = new CustomFormat();
                $modInfoPenjualan->attributes = $_GET['FAInformasipenjualanresepV'];
                $modInfoPenjualan->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAwal']);
                $modInfoPenjualan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasipenjualanresepV']['tglAkhir']);
                $modInfoPenjualan->jenispenjualan =$_GET['FAInformasipenjualanresepV']['jenispenjualan'];
            }
		
            $this->render('index',array('modInfoPenjualan'=>$modInfoPenjualan));
        }
        
        public function actionDetailPenjualan($id,$idPasien) {
        $this->layout = '//layouts/frameDialog';
        
        $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
        $modReseptur = FAPenjualanResepT::model()->findByPk($id);
        
        
        $detailreseptur = FAObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $id . ' ');
        $modPasien = FAPasienM::model()->findByPk($idPasien);

        $this->render('/informasiPenjualanResep/DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
            'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
        }
        /**
         * actionBatalPenjualanResep
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function actionBatalPenjualanResep(){
            if (Yii::app()->request->isAjaxRequest){
                $result = array();
                if (isset($_POST['value'])){
                    $value = $_POST['value'];
                    $modPenjualanResep = PenjualanresepT::model()->findByPk($value);
                    if ((boolean)count($modPenjualanResep)){
                        $obatAlkes = ObatalkespasienT::model()->findAll('penjualanresep_id =:penjualanresep', array('penjualanresep'=>$modPenjualanResep->penjualanresep_id));
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $success = false;
                            $modRetur = new ReturresepT();
                            $modRetur->noreturresep = KeyGenerator::noReturResep();
                            $modRetur->attributes = $modPenjualanResep->attributes;
                            $modRetur->tglretur = date('Y-m-d H:i:s');
                            $modRetur->pegretur_id = Yii::app()->user->getState('pegawai_id');
                            $modRetur->alasanretur = 'BATAL TRANSAKSI PENJUALAN';
                            $modRetur->totalretur = $modPenjualanResep->totalhargajual;
                            if ($modRetur->validate()){
                                if ($modRetur->save()){
                                    $updatePenjualan = PenjualanresepT::model()->updateByPk($modRetur->penjualanresep_id, array('returresep_id'=>$modRetur->returresep_id));
                                    $success = true && $updatePenjualan;
                                    $jumlahSave = 0;
                                    $jumlahObat = count($obatAlkes);
                                    if ($jumlahObat > 0){
                                        foreach ($obatAlkes as $key => $value) {
                                            $modReturDetail[$key] = new ReturresepdetT();
                                            $modReturDetail[$key]->attributes = $value->attributes;
                                            $modReturDetail[$key]->qty_retur = $value->qty_oa;
                                            $modReturDetail[$key]->hargasatuan = $value->hargasatuan_oa;
                                            $modReturDetail[$key]->returresep_id = $modRetur->returresep_id;
                                            if ($modReturDetail[$key]->validate()){
                                                if ($modReturDetail[$key]->save()){
                                                    $updateDetail = ObatalkespasienT::model()->updateByPk($value->obatalkespasien_id, 
                                                            array('returresepdet_id'=>$modReturDetail[$key]->returresepdet_id,
                                                            'qty_oa'=>0, //dikosongkan karena semua obat di retur/dikembalikan (stok)
                                                            ));
                                                    $this->returStok($modReturDetail[$key]);
                                                    if ($updateDetail){
                                                        $jumlahSave++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if ($jumlahSave == 0 && $jumlahObat == $jumlahSave)
                                $jumlahSave = $jumlahObat = 10;
                            
                            if ($success && ($jumlahSave > 0) && ($jumlahSave == $jumlahObat)){
                                $transaction->commit();
                                $result['info'] = 'Resep berhasil dibatalkan !';
                                $result['status'] = 'success';
                            }else{
                                $transaction->rollback();
                                $result['info'] = 'Gagal Dibatalkan !';
                                $result['status'] = 'error';
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            $result['info'] = $exc;
                            $result['status']="error";
                        }
                    }
                }
                echo json_encode($result);
                Yii::app()->end();
            }
        }
        /**
         * method retur penjualan resep yang sudah dibayar
         * digunakan di 
         * 1. farmasi Apotek -> informasi penjualan resep -> retur penjualan
         * 2. farmasi Apotek -> informasi resep pasien Rs -> retur penjualan
         * @param integer $idPenjualanResep penjualanresep_id
         */
        public function actionReturPenjualan($idPenjualanResep, $id=null)
        {
            $this->layout = 'frameDialog';
			$jumlah_return = 0;
			if(!empty($idPenjualanResep) && is_null($id))
			{
				$criteria = new CDbCriteria;
				$criteria->select = array("COUNT(returresep_id) as jumlah_return");
				$criteria->group = "penjualanresep_id";
				$criteria->compare('penjualanresep_id', $idPenjualanResep);
				$informasipenjualanresepV = FAReturresepT::model()->find($criteria);
				$jumlah_return = 0;
				
				if(!is_null($informasipenjualanresepV->jumlah_return))
				{
					if($informasipenjualanresepV->jumlah_return < 2)
					{
						Yii::app()->user->setFlash('danger',"Sudah dilakukan return obat 1 kali");
					}else{
						Yii::app()->user->setFlash('danger',"Return tidak bisa dilakukan, karena sudah lebih dari 1 kali");
					}
					$jumlah_return = $informasipenjualanresepV->jumlah_return;
				}
				
			}
			
            $modRetur = new FAReturresepT;
            $modReturDetail = array();
            $modRetur->tglretur = date('Y-m-d H:i:s');
            $modRetur->noreturresep = KeyGenerator::noReturResep();
            $modRetur->pegretur_id = Yii::app()->user->getState('pegawai_id');
            $infoJualObat = FAInformasipenjualanapotikV::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
            new ObatalkesM; //ini agar data obat yang berelasi dengan ObatalkespasienT muncul
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
			if(!empty($id))
			{
                $modRetur = FAReturresepT::model()->findByPk($id);
                $modReturDetail = FAReturresepdetT::model()->findAllByAttributes(array('returresep_id'=>$id));
                $modPenjualanResep = PenjualanresepT::model()->findByPk($modRetur->penjualanresep_id);
                $modRetur->penjualanresep_id = $modPenjualanResep->penjualanresep_id;
                $modObatAlkesPasien = ObatalkespasienT::model()->findAllByAttributes(
					array(
						'penjualanresep_id'=>$modPenjualanResep->penjualanresep_id
					),
					"returresepdet_id IS NOT NULL"
				);
            }
			
            if(!empty($idPenjualanResep) && empty($id))
			{
                $modPenjualanResep = PenjualanresepT::model()->findByAttributes(
					array(
						'penjualanresep_id'=>$idPenjualanResep/*,
						'returresep_id'=>null*/
					)
				);
                $modRetur->penjualanresep_id = $modPenjualanResep->penjualanresep_id;
                $modRetur->mengetahui_id = $modPenjualanResep->pegawai_id;
                $modObatAlkesPasien = ObatalkespasienT::model()->findAllByAttributes(
					array(
						'penjualanresep_id'=>$modPenjualanResep->penjualanresep_id
					)/*,
					"returresepdet_id IS NULL"*/
				);
                //default nilai retur berdasarkan penjualan detail
                foreach($modObatAlkesPasien AS $i => $mod){
                    $modReturDetail[$i] = new FAReturresepdetT;
                    $modReturDetail[$i]->attributes = $mod->attributes;
//                    $modReturDetail[$i]->satuankecil_id = empty($mod->satuankecil_id) ? $mod->satuankecil_id : ObatalkesM::model()->findByPk($mod->obatalkes_id)->satuankecil_id;
//                    $modReturDetail[$i]->qty_retur = $mod->qty_oa;
//                    $modReturDetail[$i]->hargasatuan = $mod->hargasatuan_oa;
                }
            }
            // -- dicoment sementara -- //
//            if(!count($modObatAlkesPasien)){
//                echo '<script type="text/javascript">parent.location.href="'.Yii::app()->createUrl("farmasiApotek/InformasiPenjualanResep/Index", array("modulId"=>$_GET['modulId'])).'";</script>';
//                Yii::app()->user->setFlash('info',"Silahkan pilih resep yang akan diretur");
//                exit();
//            }
            
            if(isset($_POST['FAReturresepT']) && empty($id))
			{
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modRetur = $this->saveReturResep($modRetur, $_POST['FAReturresepT'], $_POST['RekeningakuntansiV']);
                    $modReturDetail = $this->saveReturResepDetail($modRetur,$modReturDetail, $_POST['FAReturresepdetT']);
                    $modObatAlkesPasien = $this->updateObatAlkesPasien($modReturDetail, $modPenjualanResep);
//                  TIDAK ADA RETUR PEMBAYARAN KARENA RETUR DILAKUKAN APABILA PASIEN BELUM MEMBAYAR  <<< $returPembayaran = $this->saveReturPembayaran($modRetur, $modReturDetail);
                    if ($this->suksesRetur  && $this->suksesReturStok && $this->suksesUpdateObatAlkesPasien){ //&& $this->suksesReturBayar
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->redirect(array('returPenjualan', 'idPenjualanResep'=>$modPenjualanResep->penjualanresep_id, 'id'=>$modRetur->returresep_id, 'sukses'=>$sukses));
                    }
                    else{
                        $modRetur->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->suksesRetur)
                            Yii::app()->user->setFlash('error',"Data gagal disimpan!");
                        if(!$this->suksesReturStok)
                            Yii::app()->user->setFlash('error',"Stok Obat Alkes gagal diretur!");
                        if(!$this->suksesUpdateObatAlkesPasien)
                            Yii::app()->user->setFlash('error',"Transaksi Resep Gagal diubah!");
                    }
                } catch (Exception $exc) {
                    $modRetur->isNewRecord = true;
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('returPenjualan',array(
				'infoJualObat'=>$infoJualObat, 
				'modPenjualanResep'=>$modPenjualanResep, 
				'modObatAlkesPasien'=>$modObatAlkesPasien,
				'modRetur'=>$modRetur, 
				'modReturDetail'=>$modReturDetail,
				'jumlah_return'=>$jumlah_return,
				'modRekenings'=>$modRekenings
			));
        }
        /**
         * saveReturResep = simpan returresep_t
         * @param type $modRetur
         * @param type $retur
         * @return type
         */
        protected function saveReturResep($modRetur, $retur, $postRekenings = array())
        {
            $modRetur->attributes = $retur;
            $modRetur->tglretur = date('Y-m-d H:i:s');
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if($modRetur->validate()){
                if($modRetur->save()){
                    $this->suksesRetur = true;
                    if(isset($postRekenings)){
                        $modJurnalRekening = TindakanController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modRetur->jurnalrekening = $modJurnalRekening->jurnalrekening_id;
                        $modRetur->save();
                        $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modObatAlkes, 'ap');
                    }
                    PenjualanresepT::model()->updateByPk($modRetur->penjualanresep_id, array('returresep_id'=>$modRetur->returresep_id));
                }
            }
            return $modRetur;
        }
        /**
         * saveReturResepDetail = simpan returresepdet_t
         * @param type $modRetur
         * @param type $modReturDetail
         * @param type $returdetail
         * @return type
         */
        protected function saveReturResepDetail($modRetur,$modReturDetail,$returdetail)
        {
            if (count($returdetail) > 0){
                foreach($returdetail as $i=>$detail){
                    if(($detail['qty_retur'] > 0) && ($detail['isRetur'] == true)) {
                        $modReturDetail[$i] = new FAReturresepdetT;
                        $modReturDetail[$i]->attributes = $_POST['FAReturresepdetT'];
                        $modReturDetail[$i]->returresep_id = $modRetur->returresep_id;
                        $modReturDetail[$i]->obatalkespasien_id = $detail['obatalkespasien_id'];
                        $modReturDetail[$i]->satuankecil_id = $detail['satuankecil_id'];
                        $modReturDetail[$i]->qty_retur = $detail['qty_retur'];
                        $modReturDetail[$i]->hargasatuan = $detail['hargasatuan'];
                        $modReturDetail[$i]->kondisibrg = $detail['kondisibrg'];
                        if ($modReturDetail[$i]->validate()){
                            $modReturDetail[$i]->save();
                            
                            //buat jurnal pembalik
                           $this->jurnalPembalikObatalkes($detail['obatalkespasien_id']);
                            
                            //Update returresepdet_id pada obatalkespasien_t yang ada
                            ObatalkespasienT::model()->updateByPk($modReturDetail[$i]->obatalkespasien_id, array('returresepdet_id'=>$modReturDetail[$i]->returresepdet_id));
                            $this->returStok($modReturDetail[$i]);
                        }
                    }
                }
            }
            return $modReturDetail;
        }
        /**
         * updateObatAlkesPasien untuk transaksi retur resep
         * @param type $modDetailRetur
         */
        protected function updateObatAlkesPasien($modReturDetail, $modPenjualanResep)
        {
            $modObatAlkesPasienUpdate = array();
            $totalHargaJual = 0;
            $totalHargaNetto = 0;
            foreach($modReturDetail AS $i => $mod){
                if($mod->returresepdet_id != ""){
                    $modObatAlkesPasienUpdate[$i] = ObatalkespasienT::model()->findByAttributes(array('returresepdet_id'=>$mod->returresepdet_id));
                    $qtyBaru = $modObatAlkesPasienUpdate[$i]->qty_oa - $mod->qty_retur;
                    $hargaSatuan = $modObatAlkesPasienUpdate[$i]->hargasatuan_oa;
                    $hargaNetto = $modObatAlkesPasienUpdate[$i]->harganetto_oa;
                    $jasadokterAwal = ($modObatAlkesPasienUpdate[$i]->jasadokterresep/$modObatAlkesPasienUpdate[$i]->qty_oa);
                    $totalHargaNetto += ($qtyBaru * $hargaNetto);
                    $totalHargaJual += ($qtyBaru * $hargaSatuan);
                    $modObatAlkesPasienUpdate[$i]->qty_oa = $qtyBaru;
                    $modObatAlkesPasienUpdate[$i]->jasadokterresep = ($qtyBaru * ($jasadokterAwal));
                    if($modObatAlkesPasienUpdate[$i]->validate()){ //update
                        $modObatAlkesPasienUpdate[$i]->save();
                        $this->suksesUpdateObatAlkesPasien = $this->suksesUpdateObatAlkesPasien && true;
                    }
                    else{
                        $this->suksesUpdateObatAlkesPasien = false;
                    }
                }
            }
            
            //Update Total Penjualan
            PenjualanresepT::model()->updateByPk($modPenjualanResep->penjualanresep_id, array('totalharganetto'=>$totalHargaNetto,'totalhargajual'=>$totalHargaJual));
            return $modObatAlkesPasienUpdate;
        }
        
        /**
         * returStok hanya boleh dilakukan apabila ada ubah retur (pengembalian belum bayar) atau batal transaksi
         * @param type $obatAlkesT
         * @return type
         */
        protected function returStok($modReturDetail){
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $modStokObat = new StokobatalkesT;
            $modStokObat->attributes = $modReturDetail->attributes;
            $modStokObat->sumberdana_id = $modReturDetail->obatpasien->sumberdana_id;
            $modStokObat->obatalkes_id = $modReturDetail->obatpasien->obatalkes_id;
            $modStokObat->harganetto_oa = $modReturDetail->obatpasien->harganetto_oa;
            $modStokObat->hargajual_oa = $modReturDetail->obatpasien->hargasatuan_oa;
            $modStokObat->tglstok_in = date('Y-m-d H:i:s');
            $modStokObat->tglstok_out = null;
            $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modStokObat->qtystok_in = 0;
            $modStokObat->qtystok_out = -$modReturDetail->qty_retur;
            $modStokObat->qtystok_current = $modStokObat->qtystok_in - $modStokObat->qtystok_out;
            $modStokObat->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modStokObat->create_loginpemakai_id = Yii::app()->user->id;
            $modStokObat->create_time = date('Y-m-d H:i:s');
            //discount dari obatalkespasien_t
            $modStokObat->discount = $modReturDetail->obatpasien->discount;
            $modStokObat->update_loginpemakai_id = null;
            $modStokObat->terimamutasidetail_id = null;
            $modStokObat->stokopname_id = null;
            $modStokObat->penerimaandetail_id = null;
            $modObat = ObatalkesM::model()->findByPk($modStokObat->obatalkes_id);
            if(!empty($modObat->obatalkes_id)){
                $modStokObat->hjaresep = $modObat->hjaresep;
                $modStokObat->hjanonresep = $modObat->hjanonresep;
                $modStokObat->marginresep = $modObat->marginresep;
                $modStokObat->marginnonresep = $modObat->marginnonresep;
                $modStokObat->hpp = $modObat->hpp;
            }
            if($modStokObat->validate()){
                $modStokObat->save();
                $this->suksesReturStok = $this->suksesReturStok && true;
            }else{
                $this->suksesReturStok = false;
            }
        }
        /**
         * actionPrintStrukRetur untuk print struk retur setelah retur berhasil dilakukan
         * @param type $idPenjualan
         * @param type $id
         */
        public function actionPrintStrukRetur($id){
            $this->layout = '//layouts/frameDialog';
            if($_GET['caraPrint'] =="PRINT")
               $this->layout='//layouts/printWindows';
            $modPegawaiRetur = new FAPegawaiM;
            $infoJualObat = FAInformasipenjualanapotikV::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
            if(!empty($id)){
                $modRetur = FAReturresepT::model()->findByPk($id);
                $modReturDetail = FAReturresepdetT::model()->findAllByAttributes(array('returresep_id'=>$id));
                $modPenjualan = PenjualanresepT::model()->findByPk($modRetur->penjualanresep_id);
                $modRetur->penjualanresep_id = $modPenjualanResep->penjualanresep_id;
                $modObatAlkesPasien = New ObatalkespasienT;
            }else{
                $this->redirect(array('index'));
            }
            $modPegawaiRetur = FAPegawaiM::model()->findByAttributes(array('pegawai_id'=>$modRetur->pegretur_id));
            if(!empty($modRetur->mengetahui_id))
                $modMengetahuiRetur = FAPegawaiM::model()->findByAttributes(array('pegawai_id'=>$modRetur->mengetahui_id));
//            echo "<pre>";
//            print_r($modPegawaiRetur->attributes); exit;
            $this->render('printStrukRetur',array('infoJualObat'=>$infoJualObat, 
                                            'modPenjualan'=>$modPenjualan, 
                                            'modObatAlkesPasien'=>$modObatAlkesPasien,
                                            'modRetur'=>$modRetur, 
                                            'modReturDetail'=>$modReturDetail,
                                            'modPegawaiRetur'=>$modPegawaiRetur,
                                            'modMengetahuiRetur'=>$modMengetahuiRetur,
                ));
        }
        
        public function actionPrintStruk($id,$idPasien){
           $this->layout = '//layouts/frameDialog';
           
           $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
           $reseptur = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $detailreseptur = FAObatalkesPasienT::model()->findAll($criteria);
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$reseptur->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$reseptur->pasien_id));
           
            $this->render('PrintStrukPenjualan', array('reseptur' => $reseptur,
                'detailreseptur' => $detailreseptur,'daftar'=>$daftar,'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan));
        }
        public function actionStrukPrint($id,$idPasien){
           $this->layout = '//layouts/frameDialog';

           $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $id . ' and pasien_id='.$idPasien.'');
           $reseptur = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $detailreseptur = FAObatalkesPasienT::model()->findAll($criteria);
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$reseptur->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$reseptur->pasien_id));
           $judulLaporan = 'Struk Penjualan';
           $caraPrint=$_REQUEST['caraPrint'];
           if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                 $this->render('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('farmasiApotek.views.informasiPenjualanResep.PrintStrukPenjualan',array('reseptur' => $reseptur,
                                                            'detailreseptur' => $detailreseptur,'daftar'=>$daftar,
                                                            'pasien'=>$pasien,'modDetailPenjualan'=>$modDetailPenjualan,
                                                            'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }  
            
        }
        
        public function actionPrintDetailPenjualan(){
            $id = $_POST['id'];
            $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=:penjualanresep', array(':penjualanresep'=>$id));
            $modReseptur = FAPenjualanResepT::model()->findByPk($id);

            $detailreseptur = FAObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $id . ' ');
            $modPasien = FAPasienM::model()->findByPk($idPasien);

            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                                    'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur),true));
                $mpdf->Output();
            }  
            $this->render('DetailPenjualan', array('modDetailPenjualan' => $modDetailPenjualan,
                'modReseptur' => $modReseptur,'detailreseptur'=>$detailreseptur));
        }
        
        public function actionCopyResep($idPenjualanResep,$idPasien,$id=null)
	{
            $this->layout='//layouts/frameDialog';             
            if (!empty($id)) {
                $model = FACopyResepR::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
            }else{
                $model = new FACopyResepR;
            }
             $tersimpan = 'Tidak';
             
             $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
             $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' and pasien_id='.$idPasien.'');
             $modPasien = FAPasienM::model()->findByPk($idPasien);
             $modCopy = CopyresepR::model()->findAll('penjualanresep_id='.$idPenjualanResep);
             foreach($modCopy as $i=>$data){
                 $copy = $data->jmlcopy;
             }
             if($modCopy == null){
                 $copy = 1;
             }else{
                $copy = $copy + 1;
             }
//             echo $copy;
//             exit;
             if(isset($_POST['FACopyResepR'])){
                    $jmlCopy = $copy;
                    $model->attributes = $_POST['FACopyResepR'];
                    $model->tglcopy = date('Y-m-d');
                    $model->penjualanresep_id = $_POST['FAPenjualanResepT']['penjualanresep_id'];
                    $model->keterangancopy = $_POST['FACopyResepR']['keterangancopy'];
                    $model->jmlcopy = $jmlCopy;
                    $model->create_time = date('Y-m-d');
                    $model->update_time = date('Y-m-d');
                    $model->create_loginpemakai_id = Yii::app()->user->id;
                    $model->update_loginpemakai_id = Yii::app()->user->id;
                    $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
//                  echo "<pre>";
//                    echo print_r($model->getAttributes());
//                    exit;
                if($modCopy->penjualanresep_id == $idPenjualanResep){
                    $update = CopyresepR::model()->UpdateAll(array(
                                                        'jmlcopy' =>$jmlCopy,
                                                        'tglcopy'=>date('Y-m-d'),
                                                        'keterangancopy' => $_POST['FACopyResepR']['keterangancopy'],
                                                        'create_time'=>date('Y-m-d'),
                                                        'update_time'=>date('Y-m-d'),
                                                        'create_loginpemakai_id'=>Yii::app()->user->id,
                                                        'update_loginpemakai_id'=>Yii::app()->user->id,
                                                        'create_ruangan'=>Yii::app()->user->getState('ruangan_id')
                    ),'penjualanresep_id=:penjualanresep_id',array(':penjualanresep_id'=>$_POST['FAPenjualanResepT']['penjualanresep_id']));

                    if($update){
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $tersimpan='Ya';
                    }else{
//                            $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan");  
                    }

                }else{
                     if($model->save()){
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $tersimpan='Ya';
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan"); 
                    }
                }
                    
                    
             }
             
             $model->tglcopy = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($model->tglcopy, 'yyyy-MM-dd'));
             
             $this->render('formCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'model'=>$model,
                                'modCopy'=>$modCopy,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'tersimpan'=>$tersimpan,
                          ));
	}
        
    public function actionPrintCopyResep($idPenjualanResep)
    {
        $this->layout='//layouts/printWindows';
        $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
        $modReseptur = ResepturT::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
        $modCopy = CopyresepR::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
        $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' 
        AND pasien_id='.$modelPenjualanResep->pasien_id.'');
        $modPasien = FAPasienM::model()->findByPk($modelPenjualanResep->pasien_id);

        $this->render('printCopyResep',array(
            'modelPenjualanResep'=>$modelPenjualanResep,
            'modPasien'=>$modPasien,
            'modDetailPenjualan'=>$modDetailPenjualan,
            'modReseptur'=>$modReseptur,
            'modCopy'=>$modCopy,
        ));
    }

    public function jurnalPembalikObatalkes($obatalkespasien_id){
            $sukses = true;
            $modObat = ObatalkespasienT::model()->findByPk($obatalkespasien_id);
            if(isset($modObat->jurnalrekening_id)){
                $modJurnalRekening = JurnalrekeningT::model()->findByPk($modObat->jurnalrekening_id);
                if(isset($modJurnalRekening)){
                    $modJurnalDetail = JurnaldetailT::model()->findAllByAttributes(array('jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id));
                    if(isset($modJurnalDetail)){
                        if(count($modJurnalDetail) > 0){
                            foreach($modJurnalDetail AS $i => $jurnal){
                                $jurnalBaru = new JurnaldetailT;
                                $jurnalBaru->attributes = $jurnal->attributes;
                                $jurnalBaru->jurnaldetail_id = null; //dikosongkan agar record baru
                                $jurnalBaru->saldokredit = $jurnal->saldodebit;
                                $jurnalBaru->saldodebit = $jurnal->saldokredit;
                                $jurnalBaru->uraiantransaksi = "RETUR OBAT";
                                $jurnalBaru->catatan = "RETUR OBAT";
                                if($jurnalBaru->save())
                                    $sukses_pembalik &= true;
                                else
                                    $sukses_pembalik = false;
                            }
                        }
                    }
                }
            }
            return $sukses;
        }

    public function actionPrintResep($idPenjualanResep)
    {
        $this->layout='//layouts/printWindows';
        $modelPenjualanResep = FAPenjualanResepT::model()->findByPk($idPenjualanResep);
        $modReseptur = ResepturT::model()->findAll('penjualanresep_id = '.$idPenjualanResep);
        $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('penjualanresep_id=' . $idPenjualanResep . ' 
        AND pasien_id='.$modelPenjualanResep->pasien_id.'');
        $modPasien = FAPasienM::model()->findByPk($modelPenjualanResep->pasien_id);

        $this->render('printResep',array(
            'modelPenjualanResep'=>$modelPenjualanResep,
            'modPasien'=>$modPasien,
            'modDetailPenjualan'=>$modDetailPenjualan,
            'modReseptur'=>$modReseptur,
        ));
    }
}