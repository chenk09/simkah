<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanBebasController extends PenjualanResepController
{
        protected $pathViewPrint = 'farmasiApotek.views.penjualanResep.PrintBebasLuar';
    
	public function actionJualBebas($id = null)
	{
//            $modPasien = $this->defaultPasien(); // Default jika data pasien di check
            $format = new CustomFormat;
            $modPasien = new FAPasienM();
            $modPasien->tanggal_lahir = date('d M Y', strtotime('-30 years')); //default tanggal lahir
            if (isset($_POST['idJenisObat']))
                $idJenisObat = $_POST['idJenisObat'];
            
            $obatAlkes = null;
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            
            
            
            $modPenjualan = new FAPenjualanResepT;
            $modPenjualan->discount = 0;
            $modPenjualan->biayaadministrasi = 0;
            $modPenjualan->biayakonseling = 0;
            $modPenjualan->tglpenjualan = date('d M Y H:i:s');
//            $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB($modPenjualan->tglpenjualan); 
            $modPenjualan->jenispenjualan = 'PENJUALAN BEBAS';
            $instalasi_id = Yii::app()->user->getState('instalasi_id');
            $modPenjualan->noresep = KeyGenerator::noResep($instalasi_id)."/BEBAS";
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            if (!empty($id)){
                $modPenjualanResep = FAPenjualanResepT::model()->findByPk($id);
                if ((boolean)count($modPenjualanResep)){
                    $modPenjualan = $modPenjualanResep;
                    $modPasien = FAPasienM::model()->findByPk($modPenjualan->pasien_id);
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }

            if(!empty($_POST['FAPenjualanResepT']) && isset($_POST['penjualanResep'])) {
                if($_POST['isUpdatePasien'] == true){ // jika input data pasien
                    $modPasien = $this->defaultPasien();
                    $modPasien->attributes = $_POST['FAPasienM'];
                }else{
                    $modPasien = PasienM::model()->findByPk(Params::DEFAULT_PASIEN_APOTEK_UMUM);
                }
                $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
                $modPasien->no_rekam_medik = KeyGenerator::noRekamMedik(Yii::app()->user->getState('mr_apotik'),'TRUE');
                $modPasien->statusrekammedis = Params::statusRM(1);
                $modPasien->kelompokumur_id = KeyGenerator::kelompokUmur($modPasien->tanggal_lahir);
                
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPasien->save();
                    $modPenjualan = $this->savePenjualanResep($modPasien, null, $_POST['FAPenjualanResepT'], $konfigFarmasi);
                    $obatAlkes = $this->saveObatAlkes($modPenjualan, $_POST['penjualanResep'],false,$_POST['RekeningakuntansiV']);
                    if($this->successSavePenjualan && $this->successSaveObat){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $sukses = 1; // 1 = sukses create
                        $this->redirect(array('JualBebas','id'=>$modPenjualan->penjualanresep_id, 'sukses'=>$sukses));
                        //$this->refresh();
                    } else {
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $sukses = 0;
                    }
                } catch (Exception $exc)
                {
                    $modPenjualan->isNewRecord = true;
                    $transaction->rollback();
                    $successSave = false;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));

                }
            }
            
            $this->render('jualBebas',array('modPasien'=>$modPasien,
                                            'modPenjualan'=>$modPenjualan,
                                            'racikan'=>$racikan,
                                            'racikanDetail'=>$racikanDetail,
                                            'nonRacikan'=>$nonRacikan,
                                            'obatAlkes'=>$obatAlkes,
                                            'konfigFarmasi'=>$konfigFarmasi,
                                            'sukses'=>$sukses,
                ));
	}
        /**
         * Fungsi actionUpdateJualBebas khusus untuk mengubah data lama transaksi Penjualan Bebas
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function actionUpdateJualBebas($idPenjualan = null){
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if (isset($idPenjualan)){
                $modPenjualan = FAPenjualanResepT::model()->findByPk($idPenjualan);
                $modPasien = FAPasienM::model()->findByPk($modPenjualan->pasien_id);
                if ((boolean)count($modPenjualan)){
                    $modPenjualan->isNewRecord = false;
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }
            
            if(isset($_POST['penjualanResep'])){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPenjualan = $this->updatePenjualanResep($modPenjualan, $_POST['FAPenjualanResepT']);
                    $obatAlkes = $this->updateObatAlkes($obatAlkes, $modPenjualan, $_POST['penjualanResep']);
                    if($this->successUpdatePenjualan && $this->successUpdateObat){
                        $transaction->commit();
                        $sukses = 2; // 2= Sukses Update
                        $this->redirect(array('UpdateJualBebas', 'idPenjualan'=>$idPenjualan, 'sukses'=>$sukses));
                    }else{
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->successUpdateObat)
                            $dataGagal = "Data Obat Alkes";
                        Yii::app()->user->setFlash('error',"Transaksi gagal disimpan. Silahkan cek ".$dataGagal);
                        $sukses = 0;
                    }
                }catch (Exception $exc) {
                    $modPenjualan->isNewRecord = false;
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
                
            }

            $this->render('jualBebas',array('modPasien'=>$modPasien,
                                            'modPenjualan'=>$modPenjualan,
                                            'racikan'=>$racikan,
                                            'racikanDetail'=>$racikanDetail,
                                            'nonRacikan'=>$nonRacikan,
                                            'obatAlkes'=>$obatAlkes,
                                            'konfigFarmasi'=>$konfigFarmasi,
                                            'sukses'=>$sukses,
                                            'modRekenings'=>$modRekenings,
                ));
        }
        protected function defaultPasien()
        {
//            $pasien = FAPasienM::model()->findByPk(82);
//           if(!empty($pasien->pasien_id)){
//            $pasien = FAPasienM::model()->findByPk(82);
//           }else{
            $pasien = new FAPasienM;
            $pasien->nama_pasien = 'Pasien Bebas';
            $pasien->jenisidentitas = 'KTP';
            $pasien->namadepan = 'Tn.';
            $pasien->tanggal_lahir = date('Y M d');
            $pasien->kelompokumur_id = 3;
            $pasien->jeniskelamin = 'LAKI-LAKI';
            $pasien->alamat_pasien = Yii::app()->user->getState('alamatlokasi_rumahsakit');
            $pasien->agama = 'ISLAM';
            $pasien->warga_negara = 'INDONESIA';
            $pasien->propinsi_id = Yii::app()->user->getState('propinsi_id');
            $pasien->kabupaten_id = Yii::app()->user->getState('kabupaten_id');
            $pasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
//            $pasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
            $pasien->kecamatan_id = 595;
            $pasien->kelurahan_id = 5794;
            $pasien->ispasienluar = true;
//           }
            return $pasien;
        }
        
        protected function savePenjualanResep($modPasien,$modReseptur = null, $penjualanResep, $konfigFarmasi, $id=null)
        {
            if (!isset($id)){
                $modPenjualan = new FAPenjualanResepT;
            }else{
                $modPenjualan = FAPenjualanResepT::model()->findByPk($id);
            }
            $modPenjualan->attributes = $penjualanResep;
            $modPenjualan->penjamin_id = Params::DEFAULT_PENJAMIN;
            $modPenjualan->carabayar_id = Params::DEFAULT_CARABAYAR;
            $modPenjualan->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $modPenjualan->pasien_id = $modPasien->pasien_id;
            $modPenjualan->tglresep = date('d M Y H:i:s');
            $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
            $modPenjualan->ruanganasal_nama = $ruanganAsal->ruangan_nama;
            $modPenjualan->instalasiasal_nama = $ruanganAsal->instalasi->instalasi_nama;
            $modPenjualan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modPenjualan->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modPenjualan->pembulatanharga = $konfigFarmasi->pembulatanharga;
            $modPenjualan->subsidiasuransi = 0;
            $modPenjualan->subsidipemerintah = 0;
            $modPenjualan->subsidirs = 0;
            $modPenjualan->iurbiaya = 0;
            $instalasi_id = Yii::app()->user->getState('instalasi_id');
            $modPenjualan->noresep = KeyGenerator::noResep($instalasi_id)."/BEBAS";
//            $modPenjualan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
//            $modPenjualan->penjamin_id = $modPendaftaran->penjamin_id;
//            $modPenjualan->carabayar_id = $modPendaftaran->carabayar_id;
            $modPenjualan->pegawai_id = Yii::app()->user->getState('pegawai_id');
//            $modPenjualan->tglresep = $modReseptur->tglreseptur;
//            $modPenjualan->reseptur_id = $modReseptur->reseptur_id;
//            $modPenjualan->noresep = $modReseptur->noresep;
            if ($modPenjualan->isNewRecord){
                if($modPenjualan->validate()){
                    $modPenjualan->save();
                    //ResepturT::model()->updateByPk($modReseptur->reseptur_id, array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                    $this->successSavePenjualan = true;
                }
            }else{
                if ($modPenjualan->save()){
                    $this->successSavePenjualan = true;
                }
            }
            
            return $modPenjualan;
        }
        
        protected function saveObatAlkes($modPenjualan,$penjualanResep,$update=false,$postRekenings = array())
        {
            $modObat = null;
            $format = new CustomFormat;
            if (count($penjualanResep) > 0){
//                if ($update){
//                    $obatResepLama = ObatalkespasienT::model()->findAll('penjualanresep_id=:penjualanresep', array(':penjualanresep'=>$modPenjualan->penjualanresep_id));
//                    if (count($obatResepLama) > 0){
//                        if ($this->kembalikanStok($obatResepLama)){
//                            ObatalkespasienT::model()->deleteAll('penjualanresep_id='.$modPenjualan->penjualanresep_id);
//                        } else {
//                            throw new Exception("Pengembalian stok gagal");
//                        }
//                    }
//                }
                $data = true; //pointer to first key
                foreach ($penjualanResep as $i => $obatResep) {
                    if($obatResep['detailreseptur_id']!=0){
                        $modObatAlkes = new ObatalkespasienT('retail');
                        $modObatAlkes->attributes = $obatResep;
                        $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
                        $modObatAlkes->penjualanresep_id = $modPenjualan->penjualanresep_id;
                        $modObatAlkes->pendaftaran_id = $modPenjualan->pendaftaran_id;
                        $modObatAlkes->penjamin_id = $modPenjualan->penjamin_id;
                        $modObatAlkes->carabayar_id = $modPenjualan->carabayar_id;
                        $modObatAlkes->pasien_id = $modPenjualan->pasien_id;
                        $modObatAlkes->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
                        $modObatAlkes->pegawai_id = $modPenjualan->pegawai_id;
                        $modObatAlkes->permintaan_oa = $obatResep['permintaan_reseptur'];
                        $modObatAlkes->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
                        $modObatAlkes->qty_oa = $obatResep['qty'];
                        $modObatAlkes->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
                        $modObatAlkes->harganetto_oa = $obatResep['harganetto_reseptur'];
                        $modObatAlkes->hargajual_oa = $obatResep['hargajual_reseptur'];
						$modObatAlkes->hargasatuanjual_oa = $obatResep['hargasatuanjual_oa'];
                        $modObatAlkes->signa_oa = $obatResep['signa_reseptur'];
//                        $modObatAlkes->discount = (isset($obatResep['discount'])) ? $obatResep['discount'] : 0;
                        $modObatAlkes->discount = ($obatResep['discount']!=0) ? $obatResep['discount'] : 0; 
                        $modObatAlkes->oa = 'AP';
                        $modObatAlkes->create_time = date('Y-m-d H:i:s');
                        $modObatAlkes->update_time = date('Y-m-d H:i:s');
                        $modObatAlkes->create_loginpemakai_id = Yii::app()->user->id;
                        
                        $modObatAlkes->racikan_id = ($obatResep['isRacikan'] == 0) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                        $obat_alkes = FAObatalkesPasienT::model()->findAllByPk($obatResep['obatalkes_id']);
                        
                        /* Biaya Service Disimpan pada record racikan pertama sesuai dengan yang muncul di form penjualan -> total tarif service*/
                                               
                        if ($data){
                            $modObatAlkes->biayaservice = $modPenjualan->totaltarifservice;
                            $modObatAlkes->biayaadministrasi = $modPenjualan->biayaadministrasi;
                            $modObatAlkes->biayakonseling = $modPenjualan->biayakonseling;
                            $modObatAlkes->jasadokterresep = $modPenjualan->jasadokterresep;
                            $data = false; //set false so another key is not first key
                        }else{
                            $modObatAlkes->biayaservice = 0;
                            $modObatAlkes->biayaadministrasi = 0;
                            $modObatAlkes->biayakonseling = 0;
                            $modObatAlkes->jasadokterresep = 0;
                        }
                        
                        $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB($modPenjualan->tglpenjualan); 
                        $modObatAlkes->tglpelayanan = $modPenjualan->tglpenjualan;
                         
                        if($obatResep['disc']!=0)
                        {
                            $modObatAlkes->discount = $obatResep['disc'];
                        }
                        
                        $modObatAlkes->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modObatAlkes->shift_id = Yii::app()->user->getState('shift_id');
                        $modObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modObatAlkes->subsidiasuransi = 0;
                        $modObatAlkes->subsidipemerintah = 0;
                        $modObatAlkes->subsidirs = 0;
                        $modObatAlkes->iurbiaya = 0;
                        
                        if($modObatAlkes->validate())
                        {
                            $modObatAlkes->save();
                            StokobatalkesT::model()->kurangiStok($modObatAlkes->qty_oa, $modObatAlkes->obatalkes_id);
                            if(isset($postRekenings)){
                                $modJurnalRekening = TindakanController::saveJurnalRekening();
                                //update jurnalrekening_id
                                $modObatAlkes->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                $modObatAlkes->save();
//                                $saveDetailJurnal = TindakanController::saveJurnalDetails($modJurnalRekening, $postRekenings, $modObatAlkes, 'ap');
                                $saveDetailJurnal = PenjualanResepController::saveJurnalDetailsObat($modJurnalRekening, $postRekenings, $modObatAlkes);
                            }
                            $this->successSaveObat = true;
                        } else {
                            $this->successSaveObat = false;
                            Yii::app()->user->setFlash('error',"Data Obat Alkes Tidak valid $i"."<pre>".print_r($modObatAlkes->attributes,1)."</pre>");
                        }
                        $modObat[$i] = $modObatAlkes;
                    }
                }
                
                /* perhitungan untuk biaya racik */
                $attributes = array(
                    'penjualanresep_id'=>$modPenjualan->penjualanresep_id,
                    'racikan_id'=>Params::DEFAULT_RACIKAN_ID
                );
                $obat_racik = FAObatalkesPasienT::model()->findAllByAttributes($attributes);
                if(count($obat_racik) > 0)
                {
                    $jum_racik = count($obat_racik);
                    $sql = "SELECT * FROM racikandetail_m WHERE racikan_id = ". Params::DEFAULT_RACIKAN_ID ." AND ". $jum_racik ." BETWEEN qtymin AND qtymaks";
                    $record = YII::app()->db->createCommand($sql)->queryRow();

                    if($record)
                    {
                        $tarifservice = $record['tarifservice'];
                    }else{
                        $sql = "SELECT * FROM racikan_m WHERE racikan_id = ". Params::DEFAULT_RACIKAN_ID ." AND racikan_aktif = true";
                        $record = YII::app()->db->createCommand($sql)->queryRow();
                        $tarifservice = $record['tarifservice'];
                    }
                    
                    $is_simpan = true;
                    foreach($obat_racik as $val)
                    {
                        $attributes = array(
                            'biayaservice'=>0
                        );
                        if($is_simpan)
                        {
                            $attributes = array(
                                'biayaservice'=>$tarifservice
                            );
                            $is_simpan = false;
                        }
                        FAObatalkesPasienT::model()->updateByPk($val->obatalkespasien_id, $attributes);

                    }
                }
                
            }else{
                $this->successSaveObat = false;
            }
            
            return $modObat;
        }
        
        /**
        * actionPrintFakturPenjualanBebas digunakan untuk print faktur pada Transaksi Penjualan Dokter
        * @param type $id
        */
        public function actionPrintFakturPenjualanBebas($id){
          $this->layout = '//layouts/frameDialog';
          $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
          $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
          $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
          $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
           $judulLaporan='Laporan Penerimaan Kas';
           $caraPrint=$_REQUEST['caraPrint'];
           if($caraPrint=='PRINT') {
               $this->layout='//layouts/printWindows';
           }
            $this->render('PrintFakturPenjualanBebas',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
		
        public function actionPrintFaktur($id)
		{
			$this->layout='//layouts/printWindows';
			$modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
			$daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
			$pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
			$obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
			$judulLaporan = 'Laporan Penerimaan Kas';
			$this->render('print_faktur',
				array(
					'modPenjualan'=>$modPenjualan,
					'daftar'=>$daftar,
					'pasien'=>$pasien,
					'obatAlkes'=>$obatAlkes,
					'judulLaporan'=>$judulLaporan,
					'caraPrint'=>$caraPrint
				)
			);
        }		

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
}
