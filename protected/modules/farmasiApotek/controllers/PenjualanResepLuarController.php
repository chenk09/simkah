<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanResepLuarController extends PenjualanResepController
{
        protected $pathViewPrint = 'farmasiApotek.views.penjualanResep.PrintBebasLuar';
        /**
         * Fungsi actionJualResep khusus untuk menyimpan data baru transaksi Penjualan Resep Luar
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
    public function actionJualResep($id = null)
    {

//            $modPasien = $this->defaultPasien();
        $format = new CustomFormat;
        $modPasien = new FAPasienM();
        $modPasien->tanggal_lahir = date('d M Y', strtotime('-30 years')); //default tanggal lahir

        $konfigFarmasi = KonfigfarmasiK::model()->find();
        $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
        $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
        $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
        $racikanDetail = array();
        foreach ($modRacikanDetail as $i => $mod) { //convert object to array
            $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
            $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
            $racikanDetail[$i]['qtymin'] = $mod->qtymin;
            $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
            $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
        }
        $modPenjualan = new FAPenjualanResepT;
        $modPenjualan->discount = 0;
        $modPenjualan->biayaadministrasi = 0;
        $modPenjualan->biayakonseling = 0;
//            $modPenjualan->tglresep = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenjualan->tglresep, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
//            $modPenjualan->tglresep = $format->formatDateTimeMediumForDB($modPenjualan->tglresep); 
        $modPenjualan->tglresep = date('d M Y H:i:s');
//            $modPenjualan->tglpenjualan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modPenjualan->tglpenjualan, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
        $modPenjualan->tglpenjualan = date('d M Y H:i:s');
//            $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB($modPenjualan->tglpenjualan);
        $modPenjualan->jenispenjualan = 'PENJUALAN RESEP LUAR';
        //$modPenjualan->noresep = Generator::noResep($instalasi_id);
        $modJurnalRekening = new JurnalrekeningT;
        $modRekenings = array();

        if (!empty($id))
        { // untuk melihat informasi data yg berhasil tersimpan
            $modPenjualanResep = FAPenjualanResepT::model()->findByPk($id);
            if ((boolean)count($modPenjualanResep))
            {
                $modPenjualan = $modPenjualanResep;
                $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id' => $modPenjualan->penjualanresep_id));
                $modPasien = FAPasienM::model()->findByPk($modPenjualan->pasien_id);
            }
        }

        if (!empty($_POST['FAPenjualanResepT']) && isset($_POST['penjualanResep']) && (empty($id)))
        {
            if ($_POST['isUpdatePasien']) { // jika input data pasien
                $modPasien = $this->defaultPasien();
                $modPasien->attributes = $_POST['FAPasienM'];
            } else {
//                     $modPasien = $this->defaultPasien();
                $modPasien = PasienM::model()->findByPk(Params::DEFAULT_PASIEN_APOTEK_UMUM);
            }
            $modPasien->tgl_rekam_medik = date('Y-m-d H:i:s');
            $modPasien->no_rekam_medik = Generator::noRekamMedik(Yii::app()->user->getState('mr_apotik'), 'TRUE');
            $modPasien->statusrekammedis = Params::statusRM(1);
            $modPasien->kelompokumur_id = Generator::kelompokUmur($modPasien->tanggal_lahir);

            $transaction = Yii::app()->db->beginTransaction();
            try {
                $modPasien->save();
                $modPenjualan = $this->savePenjualanResepLuar($modPasien, $_POST['FAPenjualanResepT'], $konfigFarmasi);
                $obatAlkes = $this->saveObatAlkes($modPenjualan, $_POST['penjualanResep'], false, $_POST['RekeningakuntansiV']);

                if ($this->successSavePenjualan && $this->successSaveObat) {
                    $transaction->commit();
                    Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                    $sukses = 1; //1 = Sukses Save
                    $this->redirect(array('jualResep', 'id' => $modPenjualan->penjualanresep_id, 'sukses' => $sukses));
                    //$this->refresh();
                } else {
                    $modPenjualan->isNewRecord = true;
                    $transaction->rollback();
                    if (!$this->successSavePenjualan)
                        $dataGagal = "Data Penjualan";
                    if (!$this->successSaveObat)
                        $dataGagal = "Data Obat Alkes";
                    Yii::app()->user->setFlash('error', "Transaksi gagal disimpan. Silahkan cek " . $dataGagal);
                    $sukses = 0;
                }
            } catch (Exception $exc) {
                $modPenjualan->isNewRecord = false;
                $transaction->rollback();
                $sukses = 0;
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
            }
        }

        //Pisahkan no resep depan belakang
        $modPenjualan->noresep_depan = substr($modPenjualan->noresep, 0, 21);
        $modPenjualan->noresep_belakang = substr($modPenjualan->noresep, 21, 100);
        $this->render('jualResep', array(
            'modReseptur' => $modReseptur,
            'modPasien' => $modPasien,
            'obatAlkes' => $obatAlkes,
            'modPenjualan' => $modPenjualan,
            'racikan' => $racikan,
            'racikanDetail' => $racikanDetail,
            'nonRacikan' => $nonRacikan,
            'konfigFarmasi' => $konfigFarmasi,
            'sukses' => $sukses,
            'modRekenings' => $modRekenings,
        ));
    }
        /**
         * defaultPasien untuk menentukan default pasien 
         */
        protected function defaultPasien()
        {
//            $pasien = FAPasienM::model()->findByPk(82);
//           if(!empty($pasien->pasien_id)){
//            $pasien = FAPasienM::model()->findByPk(82);
//           }else{
            $pasien = new FAPasienM;
            $pasien->nama_pasien = 'Pasien Resep Luar';
            $pasien->jenisidentitas = 'KTP';
            $pasien->namadepan = 'Tn.';
            $pasien->tanggal_lahir = date('Y M d');
            $pasien->kelompokumur_id = 3;
            $pasien->jeniskelamin = 'LAKI-LAKI';
            $pasien->alamat_pasien = Yii::app()->user->getState('alamatlokasi_rumahsakit');
            $pasien->agama = 'ISLAM';
            $pasien->warga_negara = 'INDONESIA';
            $pasien->propinsi_id = Yii::app()->user->getState('propinsi_id');
            $pasien->kabupaten_id = Yii::app()->user->getState('kabupaten_id');
            $pasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
//            $pasien->kecamatan_id = Yii::app()->user->getState('kecamatan_id');
            $pasien->kecamatan_id = 595;
            $pasien->kelurahan_id = 5794;
            $pasien->ispasienluar = true;
//           }
            return $pasien;
        }
        
        protected function savePenjualanResepLuar($modPasien, $penjualanResep, $konfigFarmasi)
        {
            $format = new CustomFormat;
            $modPenjualan = new FAPenjualanResepT;
            $modPenjualan->attributes = $penjualanResep;
            $modPenjualan->penjamin_id = Params::DEFAULT_PENJAMIN;
            $modPenjualan->carabayar_id = Params::DEFAULT_CARABAYAR;
            $modPenjualan->kelaspelayanan_id = Params::kelasPelayanan('tanapa_kelas');
            $modPenjualan->pasien_id = $modPasien->pasien_id;
            $modPenjualan->tglpenjualan = date('Y-m-d H:i:s');
            $modPenjualan->tglresep = $format->formatDateTimeMediumForDB($_POST['FAPenjualanResepT']['tglresep']);
            $ruanganAsal = RuanganM::model()->with('instalasi')->findByPk(Yii::app()->user->getState('ruangan_id'));
            $modPenjualan->ruanganasal_nama = $ruanganAsal->ruangan_nama;
            $modPenjualan->instalasiasal_nama = $ruanganAsal->instalasi->instalasi_nama;
            $modPenjualan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modPenjualan->pembulatanharga = $konfigFarmasi->pembulatanharga;
            $modPenjualan->subsidiasuransi = 0;
            $modPenjualan->subsidipemerintah = 0;
            $modPenjualan->subsidirs = 0;
            $modPenjualan->iurbiaya = 0;
            $modPenjualan->noresep_depan = Generator::noResep($instalasi_id);
            $modPenjualan->noresep_belakang = (isset($_POST['FAPenjualanResepT']['noresep_belakang'])) ? $_POST['FAPenjualanResepT']['noresep_belakang'] : null;
            $modPenjualan->noresep = $modPenjualan->noresep_depan.$modPenjualan->noresep_belakang;
            
            if($modPenjualan->pegawai_id){
                $modPenjualan->pegawai_id = $_POST['FAPenjualanResepT']['pegawai_id'];
            }else{
                $modPenjualan->pegawai_id = Yii::app()->user->getState('pegawai_id');;
            }
            
            if ($modPenjualan->isNewRecord){
                if($modPenjualan->validate()){
                    $modPenjualan->save();
                    $this->successSavePenjualan = true;
                }
            }else{
                if ($modPenjualan->save())
                    $this->successSavePenjualan = true;
            }
            return $modPenjualan;
        }
        /**
         * actionPrintKwPenjualanResepLuar print kwitansi penjualan resep luar
         * @param type $id
         */
        public function actionPrintKwPenjualanResepLuar($id = null){
           $this->layout = '//layouts/frameDialog';
           $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
           $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('PrintKwPenjualanResepLuar',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }
}
