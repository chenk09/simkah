<?php

class PencarianPasienController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','indexRJ','indexRI','indexRD'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new FAInfoKunjunganRJV;
                $modRI = new FAInfoKunjunganRIV;
                $modRD = new FAInfoKunjunganRDV;
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
                $modRJ->tglAkhir =date('Y-m-d H:i:s');
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir = date('Y-m-d H:i:s');
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir =date('Y-m-d H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['FAInfoKunjunganRJV'];
                            if(!empty($_POST['FAInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRJV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInfoKunjunganRJV']['tglAwal']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRJV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['FAInfoKunjunganRIV'];
                            if(!empty($_POST['FAInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRIV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInfoKunjunganRIV']['tglAwal']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRIV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['FAInfoKunjunganRDV'];
                            if(!empty($_POST['FAInfoKunjunganRDV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRDV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInfoKunjunganRDV
                                ']['tglAwal']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInfoKunjunganRDV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                                 
                ));
        }

        public function actionIndexRJ()
        {
                $format = new CustomFormat();
                $modRJ = new FAInforawatjalanapotikblmbayarV;
                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
                $modRJ->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInforawatjalanapotikblmbayarV'])){
                    $modRJ->attributes = $_GET['FAInforawatjalanapotikblmbayarV'];
                    $modRJ->no_rekam_medik = $_GET['FAInforawatjalanapotikblmbayarV']['no_rekam_medik'];
                    $modRJ->no_pendaftaran = $_GET['FAInforawatjalanapotikblmbayarV']['no_pendaftaran'];
                    $modRJ->nama_pasien = $_GET['FAInforawatjalanapotikblmbayarV']['nama_pasien'];
                    if(!empty($_GET['FAInforawatjalanapotikblmbayarV']['tglAwal']))
                    {
                        $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInforawatjalanapotikblmbayarV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInforawatjalanapotikblmbayarV']['tglAwal']))
                    {
                        $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInforawatjalanapotikblmbayarV']['tglAkhir']);
                    }
                }
                           
                $this->render('indexRJ',array(
                                 'modRJ'=>$modRJ,
                ));
        }

        public function actionIndexRI()
        {
                $format = new CustomFormat();
                $modRI = new FAInforawatinapapotikblmbayarV;
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInforawatinapapotikblmbayarV'])){
                    $modRI->attributes = $_GET['FAInforawatinapapotikblmbayarV'];
                    $modRI->no_rekam_medik = $_GET['FAInforawatinapapotikblmbayarV']['no_rekam_medik'];
                    $modRI->no_pendaftaran = $_GET['FAInforawatinapapotikblmbayarV']['no_pendaftaran'];
                    $modRI->nama_pasien = $_GET['FAInforawatinapapotikblmbayarV']['nama_pasien'];
                    if(!empty($_GET['FAInforawatinapapotikblmbayarV']['tglAwal']))
                    {
                        $modRI->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInforawatinapapotikblmbayarV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInforawatinapapotikblmbayarV']['tglAwal']))
                    {
                        $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInforawatinapapotikblmbayarV']['tglAkhir']);
                    }
                }
                          
                $this->render('indexRI',array(
                                 'modRI'=>$modRI,
                ));
        }

        public function actionIndexRD()
        {
                $format = new CustomFormat();
                $modRD = new FAInforawatdaruratapotikblbayarV;
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInforawatdaruratapotikblbayarV'])){
                    $modRD->attributes = $_GET['FAInforawatdaruratapotikblbayarV'];
                    $modRD->no_rekam_medik = $_GET['FAInforawatdaruratapotikblbayarV']['no_rekam_medik'];
                    $modRD->no_pendaftaran = $_GET['FAInforawatdaruratapotikblbayarV']['no_pendaftaran'];
                    $modRD->nama_pasien = $_GET['FAInforawatdaruratapotikblbayarV']['nama_pasien'];
                    if(!empty($_GET['FAInforawatdaruratapotikblbayarV']['tglAwal']))
                    {
                        $modRD->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInforawatdaruratapotikblbayarV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInforawatdaruratapotikblbayarV']['tglAwal']))
                    {
                        $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInforawatdaruratapotikblbayarV']['tglAkhir']);
                    }
                }
                            
                $this->render('indexRD',array(
                                 'modRD'=>$modRD,
                ));
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}