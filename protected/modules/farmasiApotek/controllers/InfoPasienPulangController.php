<?php

class InfoPasienPulangController extends SBaseController
{
	
    /**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';

        public function actionIndex()
        {
                $format = new CustomFormat();
                $modRJ = new FAInformasikasirrawatjalanV;
                $modRI = new FAInformasikasirinappulangV;
                $modRD = new FAInformasikasirrdpulangV;
                $cekRJ = true;
                $cekRI = false;
                $cekRD = false;
                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
                $modRJ->tglAkhir =date('Y-m-d H:i:s');
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir = date('Y-m-d H:i:s');
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir =date('Y-m-d H:i:s');
       
                
                if(isset ($_POST['instalasi']))
                {
                    switch ($_POST['instalasi']) {
                        case 'RJ':
                            $cekRJ = true;
                            $cekRI = false;
                            $cekRD = false;
                            $modRJ->attributes = $_POST['FAInformasikasirrawatjalanV'];
                            if(!empty($_POST['FAInformasikasirrawatjalanV']['tglAwal']))
                            {
                                $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirrawatjalanV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInformasikasirrawatjalanV']['tglAwal']))
                            {
                                $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirrawatjalanV']['tglAkhir']);
                            }
                            
                            break;
                            
                        case 'RI':
                            $cekRI = true;
                            $cekRJ = false;
                            $cekRD = false;
                            $modRI->attributes = $_POST['FAInformasikasirinappulangV'];
                            if(!empty($_POST['FAInformasikasirinappulangV']['tglAwal']))
                            {
                                $modRI->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirinappulangV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInformasikasirinappulangV']['tglAwal']))
                            {
                                $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirinappulangV']['tglAkhir']);
                            }
                            break;
                            
                        case 'RD':
                            $cekRD = true;
                            $cekRI = false;
                            $cekRJ = false;
                            $modRD->attributes = $_POST['FAInformasikasirrdpulangV'];
                            if(!empty($_POST['FAInformasikasirrdpulangV']['tglAwal']))
                            {
                                $modRD->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirrdpulangV']['tglAwal']);
                            }
                            if(!empty($_POST['FAInformasikasirrdpulangV']['tglAwal']))
                            {
                                $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInformasikasirrdpulangV']['tglAkhir']);
                            }
                           
                            break;
                    }
                }
                
                $this->render('index',array(
                                 'modRJ'=>$modRJ,
                                 'modRI'=>$modRI,
                                 'modRD'=>$modRD,
                                 'cekRJ'=>$cekRJ,
                                 'cekRI'=>$cekRI,
                                 'cekRD'=>$cekRD,
                                 
                ));
        }

        public function actionIndexRJ()
        {
                $format = new CustomFormat();
                $modRJ = new FAInformasikasirrawatjalanV;
                $modRJ->tglAwal = date("Y-m-d").' 00:00:00';
                $modRJ->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInformasikasirrawatjalanV'])){
                    $modRJ->attributes = $_GET['FAInformasikasirrawatjalanV'];
                    if(!empty($_GET['FAInformasikasirrawatjalanV']['tglAwal']))
                    {
                        $modRJ->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirrawatjalanV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInformasikasirrawatjalanV']['tglAwal']))
                    {
                        $modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirrawatjalanV']['tglAkhir']);
                    }
                }
                           
                $this->render('indexRJ',array(
                                 'modRJ'=>$modRJ,
                ));
        }

        public function actionIndexRI()
        {
                $format = new CustomFormat();
                $modRI = new FAInformasikasirinappulangV;
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInformasikasirinappulangV'])){
                    $modRI->attributes = $_GET['FAInformasikasirinappulangV'];
                    if(!empty($_GET['FAInformasikasirinappulangV']['tglAwal']))
                    {
                        $modRI->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirinappulangV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInformasikasirinappulangV']['tglAwal']))
                    {
                        $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirinappulangV']['tglAkhir']);
                    }
                }
                          
                $this->render('indexRI',array(
                                 'modRI'=>$modRI,
                ));
        }

        public function actionIndexRD()
        {
                $format = new CustomFormat();
                $modRD = new FAInformasikasirrdpulangV;
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir =date('Y-m-d H:i:s');
       
                if(isset($_GET['FAInformasikasirrdpulangV'])){
                    $modRD->attributes = $_GET['FAInformasikasirrdpulangV'];
                    if(!empty($_GET['FAInformasikasirrdpulangV']['tglAwal']))
                    {
                        $modRD->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirrdpulangV']['tglAwal']);
                    }
                    if(!empty($_GET['FAInformasikasirrdpulangV']['tglAwal']))
                    {
                        $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAInformasikasirrdpulangV']['tglAkhir']);
                    }
                }
                            
                $this->render('indexRD',array(
                                 'modRD'=>$modRD,
                ));
        }

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}