<?php
Yii::import('rawatJalan.controllers.TindakanController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PenjualanResepController extends SBaseController
{
    public $successSavePenjualan = false;
    public $successUpdatePenjualan = false;
    public $successSaveObat = false;
    public $successUpdateObat = false;
    public $successSaveReseptur = false;
    public $obatGagalSimpan = array();
    public $pathView = 'farmasiApotek.views.penjualanResep.';

    /**
     * property untuk view 
     * @var String 
     */
    protected $pathViewPrint = 'PrintRincian'; 
        /**
         * Fungsi actionJualResep khusus untuk menyimpan data baru transaksi Penjualan Resep Luar
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function actionJualResep($id=null, $idPendaftaran = null, $tipe = null)
        {   
            if(isset($_GET['frame']))
                $this->layout = 'frameDialog';
            
            $sukses = false;
            $modPendaftaran = new FAPendaftaranT;
            $modPasien = new FAPasienM;
            $modReseptur = new FAResepturT;
            $instalasi_id = Yii::app()->user->getState('instalasi_id');
            $modReseptur->noresep = KeyGenerator::noResep($instalasi_id);
            $modReseptur->noresep_depan = $modReseptur->noresep.'/';
            $modReseptur->tglreseptur = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modReseptur->tglreseptur, 'yyyy-MM-dd hh:mm:ss','medium',null)); 
            $modPenjualan = new FAPenjualanResepT;
            $modPenjualan->tglpenjualan = date ('d M Y H:i:s');
            $modPenjualan->biayaadministrasi = 0;
            $modObatAlkes = null;
            $tanggungan = new TanggunganpenjaminM;
            $modJurnalRekening = new JurnalrekeningT;
            $modRekenings = array();
            
            if (!empty($idPendaftaran)){
                $modPendaftaran = FAPendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = FAPasienM::model()->findByPk($modPendaftaran->pasien_id);
            }
            if (!empty($id)){
                $modPenjualan = FAPenjualanResepT::model()->findByPk($id, 'jenispenjualan = \'PENJUALAN RESEP\'');
                $modPendaftaran = FAPendaftaranT::model()->findByPk($modPenjualan->pendaftaran_id);
                $modObatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                $modPasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
                $modReseptur = FAResepturT::model()->findByAttributes(array('penjualanresep_id'=>$id));
                if(empty($modReseptur->noresep)) //jika tidak ada reseptur
                    $modReseptur = new FAResepturT;
            }
            //menentukan subsidi pasien
            if(!empty($idPendaftaran) || !empty($id)){
                $criteria = new CdbCriteria();
                $criteria->compare('penjamin_id',$modPendaftaran->penjamin_id);
                $criteria->compare('kelaspelayanan_id',$modPendaftaran->kelaspelayanan_id);
                $criteria->compare('carabayar_id',$modPendaftaran->carabayar_id);
                $tanggungan = TanggunganpenjaminM::model()->find($criteria);
            }
            
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if(isset($_POST['FAPendaftaranT']['pendaftaran_id'])) {
                $modPendaftaran = FAPendaftaranT::model()->findByPk($_POST['FAPendaftaranT']['pendaftaran_id']);
                $modPasien = FAPasienM::model()->findByPk($modPendaftaran->pasien_id);
                $modReseptur->attributes = $_POST['FAResepturT'];
                $modReseptur->ruanganreseptur_id = $modPendaftaran->ruangan_id;
                $modReseptur->pegawai_id = $modPendaftaran->pegawai_id;
                
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    
                    $modReseptur = $this->saveReseptur($modPendaftaran, $modPenjualan, $_POST);
                    $modPenjualan = $this->savePenjualanResep($modPendaftaran, $modReseptur, $_POST['FAPenjualanResepT'], $konfigFarmasi);
                    $obatAlkes = $this->saveObatAlkes($modPenjualan, $_POST['penjualanResep'],false,$_POST['RekeningakuntansiV']);
                    if($this->successSavePenjualan && $this->successSaveObat && $this->successSaveReseptur){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $sukses = 1; // 1 = Sukses Create
                        $this->redirect(array('jualResep', 'id'=>$modPenjualan->penjualanresep_id, 'sukses'=>$sukses));
                    } else {
                        $modPenjualan->isNewRecord = true;
                        $transaction->rollback();
                        if(!$this->successSavePenjualan)
                            $dataGagal = "Data Penjualan";
                        if(!$this->successSaveObat)
                            $dataGagal = "Data Obat Alkes";
                        if(!$this->successSaveReseptur)
                            $dataGagal = "Data Reseptur";
                        Yii::app()->user->setFlash('error',"Transaksi gagal disimpan. Silahkan cek ".$dataGagal);
                        $sukses = 0;
                    }
                } catch (Exception $exc) {
                    $modPenjualan->isNewRecord = false;
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            if ($tipe == "informasi"){
                $this->render($this->pathView.'jualResepInformasi',array(
                                            'modReseptur'=>$modReseptur,
                                            'modPendaftaran'=>$modPendaftaran,
                                            'modPasien'=>$modPasien,
                                            'modPenjualan'=>$modPenjualan,
                                            'racikan'=>$racikan,
                                            'racikanDetail'=>$racikanDetail,
                                            'nonRacikan'=>$nonRacikan,
                                            'konfigFarmasi'=>$konfigFarmasi,
                                            'obatAlkes'=>$modObatAlkes,
                                            'tanggungan'=>$tanggungan,
                                            'sukses'=>$sukses,
                                            'modRekenings'=>$modRekenings,
                                            ));
            } else {
              $this->render($this->pathView.'jualResep',array(
                                            'modReseptur'=>$modReseptur,
                                            'modPendaftaran'=>$modPendaftaran,
                                            'modPasien'=>$modPasien,
                                            'modPenjualan'=>$modPenjualan,
                                            'racikan'=>$racikan,
                                            'racikanDetail'=>$racikanDetail,
                                            'nonRacikan'=>$nonRacikan,
                                            'konfigFarmasi'=>$konfigFarmasi,
                                            'obatAlkes'=>$modObatAlkes,
                                            'tanggungan'=>$tanggungan,
                                            'sukses'=>$sukses,
                                            'modRekenings'=>$modRekenings,
                                            ));  
            }
            
        }
        
        /**
         * Fungsi actionUpdateJualResep khusus untuk mengubah data lama transaksi Penjualan Resep &  Penjualan Resep Luar
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function actionUpdateJualResep($idPenjualan)
        {
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
            $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            $modRacikanDetail = RacikandetailM::model()->findAll(); //load semua data untuk perhitungan js & jquery
            $racikanDetail = array();
            foreach ($modRacikanDetail as $i => $mod){ //convert object to array
                $racikanDetail[$i]['racikandetail_id'] = $mod->racikandetail_id;
                $racikanDetail[$i]['racikan_id'] = $mod->racikan_id;
                $racikanDetail[$i]['qtymin'] = $mod->qtymin;
                $racikanDetail[$i]['qtymaks'] = $mod->qtymaks;
                $racikanDetail[$i]['tarifservice'] = $mod->tarifservice;
            }
            if (isset($idPenjualan))
            {
                $modPenjualan = FAPenjualanResepT::model()->findByPk($idPenjualan);
                $modPendaftaran = FAPendaftaranT::model()->findByPk($modPenjualan->pendaftaran_id);
                $modPasien = FAPasienM::model()->findByPk($modPenjualan->pasien_id);
                $modReseptur = FAResepturT::model()->findByAttributes(array('penjualanresep_id'=>$idPenjualan));
                //Pisahkan no resep depan belakang
                $modReseptur->noresep_depan = substr($modReseptur->noresep, 0,27);
                $modReseptur->noresep_belakang = substr($modReseptur->noresep, 27,100);
                if(empty($modReseptur->noresep)) //jika tidak ada reseptur
                    $modReseptur = new FAResepturT;
                if ((boolean)count($modPenjualan)){
                    $modPenjualan->isNewRecord = false;
                    $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                }
            }
            
            if(isset($_POST['penjualanResep']))
            {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modPenjualan = $this->updatePenjualanResep($modPenjualan, $_POST['FAPenjualanResepT']);
                    $obatAlkes = $this->updateObatAlkes($obatAlkes, $modPenjualan, $_POST['penjualanResep']);
                    if($this->successUpdatePenjualan && $this->successUpdateObat){
                        $transaction->commit();
                        $sukses = 2; // 2 = Sukses Update
//                        print_r(count($this->obatGagalSimpan));
//                        exit;
                        if(count($this->obatGagalSimpan) > 0){
                            $daftarobat = "";
                            foreach ($this->obatGagalSimpan AS $i => $obat){
                                $daftarobat .= $obat['kode_obat']."-".$obat['nama_obat']."<br>";
                            }
                            Yii::app()->user->setFlash('warning',"Obat berikut tidak bisa diupdate karena stok tidak mencukupi <br> ".$daftarobat);
                        }else{
                            Yii::app()->user->setFlash('success',"Transaksi berhasil di ubah !");
                        }
                        $this->redirect(array('UpdateJualResep', 'idPenjualan'=>$idPenjualan, 'sukses'=>$sukses));
                    }
                }catch (Exception $exc) {
                    $transaction->rollback();
                    $sukses = 0;
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
                
            }

            //Pisahkan no resep depan belakang
            $modPenjualan->noresep_depan = substr($modPenjualan->noresep, 0,21);
            $modPenjualan->noresep_belakang = substr($modPenjualan->noresep, 21,100);
            $this->render('jualResep',array(
                                            'modReseptur'=>$modReseptur,
                                            'modPendaftaran'=>$modPendaftaran,
                                            'modPasien'=>$modPasien,
                                            'obatAlkes'=>$obatAlkes,
                                            'modPenjualan'=>$modPenjualan,
                                            'modReseptur'=>$modReseptur,
                                            'racikan'=>$racikan,
                                            'racikanDetail'=>$racikanDetail,
                                            'nonRacikan'=>$nonRacikan,
                                            'konfigFarmasi'=>$konfigFarmasi,
                                            'sukses'=>$sukses,
                                            ));
        }

    public function actionReseptur($idReseptur, $id = null, $url = null)
    {
        $this->layout = 'frameDialog';
        $successSave = false;
        $format = new CustomFormat();

        if(!empty($id))
        {
            $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array(
                'reseptur_id' => $idReseptur
            ));
            $modReseptur = FAResepturT::model()->findByPk($idReseptur);
            $modDetailResep = FAResepturDetailT::model()->with('obatalkes')->findAllByAttributes(array(
                'reseptur_id' => $idReseptur
            ), array('order' => 'rke'));
            $obatAlkes = FAObatalkesPasienT::model()->findAll('penjualanresep_id =:idResep ', array(
                'idResep' => $id
            ));
        } else {
            $modReseptur = FAResepturT::model()->findByAttributes(array(
                'reseptur_id' => $idReseptur,
                'penjualanresep_id' => null
            ));
            $dokter = PegawaiM::model()->findByPk($modReseptur->pegawai_id);
            $modReseptur->dokter = $dokter->nama_pegawai;
            if (count($modReseptur) == 1)
            {
                $modDetailResep = FAResepturDetailT::model()->with('obatalkes')->findAllByAttributes(array(
                    'reseptur_id' => $idReseptur
                ), array('order' => 'rke'));
                $konfigFarmasi = KonfigfarmasiK::model()->find();
                $racikan = RacikanM::model()->findByPk(Params::DEFAULT_RACIKAN_ID);
                $nonRacikan = RacikanM::model()->findByPk(Params::DEFAULT_NON_RACIKAN_ID);
            }

            $modPenjualan = new FAPenjualanResepT;
            $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB(date('Y-m-d H:i:s'));
            $modPenjualan->lamapelayanan = round((strtotime($modPenjualan->tglpenjualan) - strtotime($modReseptur->tglreseptur)) / 60);
            $modPenjualan->biayaadministrasi = $konfigFarmasi->administrasi;
            $modPenjualan->biayakonseling = 0;
            $modPenjualan->discount = 0;
        }
        $instalasi_id = Yii::app()->user->getState('instalasi_id');
        $modReseptur->noresep_depan = MyGenerator::noReseptur($instalasi_id);

        $modPendaftaran = PendaftaranT::model()->findByPk($modReseptur->pendaftaran_id);
        $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);

        if (isset($_POST['FAPenjualanResepT']) && isset($_POST['penjualanResep']) && (empty($id)))
        {
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $penjualan = $this->savePenjualanResep($modPendaftaran, $modReseptur, $_POST['FAPenjualanResepT'], $konfigFarmasi);
                if(count($penjualan->getErrors()) > 0)
                {
                    $error = array();
                    foreach($penjualan->getErrors() as $x=>$z)
                    {
                        $error[$x] = implode(", ", $z);
                    }
                    throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
                }
                $obatAlkes = $this->saveObatAlkes($penjualan, $_POST['penjualanResep'], false);

                /*
                $reseptur = $this->saveReseptur($modPendaftaran, $penjualan, $_POST);
                if(count($reseptur->getErrors()) > 0)
                {
                    $error = array();
                    foreach($reseptur->getErrors() as $x=>$z)
                    {
                        $error[$x] = implode(", ", $z);
                    }
                    throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
                }
                */

                $transaction->commit();
                Yii::app()->user->setFlash('success', "Data berhasil disimpan");
                $this->redirect(array('reseptur',
                    'idReseptur' => $modReseptur->reseptur_id,
                    'id' => $penjualan->penjualanresep_id
                ));

            } catch (Exception $exc) {
                $transaction->rollback();
                $successSave = false;
                Yii::app()->user->setFlash('error', "Data gagal disimpan " . MyExceptionMessage::getMessage($exc, true));
            }
        }

        $this->render($this->pathView . '_form_reseptur',
            array(
                'modReseptur' => $modReseptur,
                'modDetailResep' => $modDetailResep,
                'modPendaftaran' => $modPendaftaran,
                'modPasien' => $modPasien,
                'url' => $url,
                'modPenjualan' => $modPenjualan,
                'konfigFarmasi' => $konfigFarmasi,
                'racikan' => $racikan,
                'nonRacikan' => $nonRacikan,
                'successSave' => $successSave,
                'obatAlkes' => $obatAlkes
            )
        );
    }
        
        protected function savePenjualanResep($modPendaftaran,$modReseptur,$penjualanResep, $konfigFarmasi)
        {
            $format = new CustomFormat();
            $modPenjualan = new FAPenjualanResepT;
            $modPenjualan->attributes = $penjualanResep;
            $modPenjualan->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modPenjualan->penjamin_id = $modPendaftaran->penjamin_id;
            $modPenjualan->carabayar_id = $modPendaftaran->carabayar_id;        
//            $modPenjualan->pegawai_id = $modReseptur->pegawai_id;
            $modPenjualan->pegawai_id = $_POST['FAResepturT']['pegawai_id'];
            $modPenjualan->kelaspelayanan_id = $modPendaftaran->kelaspelayanan_id;
            $modPenjualan->pasien_id = $modPendaftaran->pasien_id;
            $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array("pendaftaran_id"=>$modPendaftaran->pendaftaran_id, "pasien_id"=>$modPendaftaran->pasien_id));
            $modPenjualan->pasienadmisi_id = (empty($modPasienAdmisi->pasienadmisi_id)) ? null : $modPasienAdmisi->pasienadmisi_id;
            $modPenjualan->tglpenjualan = date('Y-m-d H:i:s');
            $modPenjualan->tglresep = $format->formatDateTimeMediumForDB($_POST['FAResepturT']['tglreseptur']);
            $modPenjualan->reseptur_id = $modReseptur->reseptur_id;
            $modPenjualan->ruanganasal_nama = RuanganM::model()->findByPk($modReseptur->ruanganreseptur_id)->ruangan_nama;
            $modPenjualan->instalasiasal_nama = RuanganM::model()->with('instalasi')->findByPk($modReseptur->ruanganreseptur_id)->instalasi->instalasi_nama;
            $modPenjualan->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modPenjualan->pembulatanharga = $konfigFarmasi->pembulatanharga;
            $modPenjualan->noresep = $_POST['FAResepturT']['noresep_depan'].$_POST['FAResepturT']['noresep_belakang'];
            $modPenjualan->subsidiasuransi = 0;
            $modPenjualan->subsidipemerintah = 0;
            $modPenjualan->subsidirs = 0;
            $modPenjualan->iurbiaya = 0;
            $modPenjualan->discount = 0; //DI SET NOL KARENA ADA ALERT DISCOUNT TIDAK BOLEH LEBIH BESAR DARI NOL
//            echo "<pre>"; print_r($modPenjualan->attributes); exit;
            if($modPenjualan->validate()){
                $modPenjualan->save();
                // -- to make no rekam medik show in billing kasir --
                PendaftaranT::model()->updateByPk($modPenjualan->pendaftaran_id, array('pembayaranpelayanan_id'=>null));
                ResepturT::model()->updateByPk($modReseptur->reseptur_id, array('penjualanresep_id'=>$modPenjualan->penjualanresep_id));
                //untuk antria farmasi
                AntrianpasienfarmasiT::model()->updateAll(array('is_dilayani'=>true),'pendaftaran_id='.$modPenjualan->pendaftaran_id);
                $this->successSavePenjualan = true;
            } else {
                $this->successSavePenjualan = false;
                Yii::app()->user->setFlash('error',"Data Penjualan Resep Tidak valid"."<pre>".print_r($modPenjualan->attributes,1)."</pre>");
            }
            
            return $modPenjualan;
        }
        /**
         * updatePenjualanResep update total penjualan
         * @param type $modPenjualan
         * @param type $penjualanResep
         * @return type
         */
        protected function updatePenjualanResep($modPenjualan, $penjualanResep){
            $modPenjualan->totalhargajual = $penjualanResep['totalhargajual'];
            $modPenjualan->totaltarifservice = $penjualanResep['totaltarifservice'];
            $modPenjualan->totharganetto = $penjualanResep['totharganetto'];
            $modPenjualan->update_loginpemakai_id = Yii::app()->user->id;
            $modPenjualan->update_time = date('Y-m-d H:i:s');
            if($modPenjualan->save())
                $this->successUpdatePenjualan = true;
            return $modPenjualan;
        }
        /**
         * saveReseptur dan saveDetailReseptur pada transaksi: JualResep
         * karena ada kasus resep di input di apotek tidak di input dari ruangan
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */

        protected function saveReseptur($modPendaftaran, $modPenjualanResep, $post)
        {
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            $format = new CustomFormat();
            $modReseptur = new FAResepturT;
            $modReseptur->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modReseptur->penjualanresep_id = $modPenjualanResep->penjualanresep_id;
            $modReseptur->tglreseptur = $format->formatDateTimeMediumForDB($post['FAResepturT']['tglreseptur']);
            $modReseptur->noresep = $post['FAResepturT']['noresep_depan'] . $post['FAResepturT']['noresep_belakang'];
            $modReseptur->pegawai_id = $post['FAResepturT']['pegawai_id'];
//            KARNA TDK DI INPUT DI FORM JD BERDASARKAN PENDAFTARAN >> $modReseptur->ruangan_id = $post['FAResepturT']['ruangan_id'];
            $modReseptur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modReseptur->ruanganreseptur_id = (empty($post['FAResepturT']['ruangan_id'])) ? $modPendaftaran->ruangan_id : $post['FAResepturT']['ruangan_id']; //ruangan asal
            $modReseptur->pasien_id = $modPendaftaran->pasien_id;
            $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array("pendaftaran_id"=>$modPendaftaran->pendaftaran_id, "pasien_id"=>$modPendaftaran->pasien_id));
            $modReseptur->pasienadmisi_id = (empty($modPasienAdmisi->pasienadmisi_id)) ? null : $modPasienAdmisi->pasienadmisi_id;
            $modReseptur->create_loginpemakai_id = Yii::app()->user->id;
            $modReseptur->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modReseptur->create_time = date ('Y-m-d H:i:s');

            if($modReseptur->validate()){
                $modReseptur->save();
                $this->successSaveReseptur = true;
                $updateStatusPeriksa=PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array('statusperiksa'=>Params::statusPeriksa(2)));
                
                PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,
                    array(
                        'pembayaranpelayanan_id'=>null
                    )
                );
                
                $this->saveDetailReseptur($post, $modReseptur);
            } else {
                $this->successSaveReseptur = false;
            }
            return $modReseptur;
        }

        protected function saveDetailReseptur($post,$reseptur)
        {
            $valid = true;
            $konfigFarmasi = KonfigfarmasiK::model()->find();
            for ($i = 0; $i < count($post['penjualanResep']); $i++) {
                $detail = new FAResepturDetailT;
                $detail->reseptur_id = $reseptur->reseptur_id;
                $detail->obatalkes_id = $post['penjualanResep'][$i]['obatalkes_id'];
                $detail->sumberdana_id = $post['penjualanResep'][$i]['sumberdana_id'];
                $detail->satuankecil_id = $post['penjualanResep'][$i]['satuankecil_id'];
                //membedakan racikan dengan bukan racikan
                $detail->racikan_id = ($post['penjualanResep'][$i]['isRacikan'] == 0) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                $detail->r = 'R/';
                $detail->rke = $post['penjualanResep'][$i]['rke'];
                $detail->qty_reseptur = $post['penjualanResep'][$i]['qty'];
                $detail->signa_reseptur = $post['penjualanResep'][$i]['signa_reseptur'];
                $detail->etiket = $post['penjualanResep'][$i]['etiket'];
                $detail->kekuatan_reseptur = $post['penjualanResep'][$i]['kekuatan_oa'];
                $detail->satuankekuatan = $post['penjualanResep'][$i]['satuankekuatan_oa'];
                
                $detail->hargasatuan_reseptur = $post['penjualanResep'][$i]['hargasatuan_reseptur'];
                $detail->harganetto_reseptur = $post['penjualanResep'][$i]['harganetto_reseptur'];
                $detail->hargajual_reseptur = $post['penjualanResep'][$i]['hargajual_reseptur'];
                
                $detail->permintaan_reseptur = $post['permintaan_reseptur'][$i];
                $detail->jmlkemasan_reseptur = $post['jmlkemasan_reseptur'][$i];
                
                $valid = $detail->validate() && $valid;
                if($valid){
                    $detail->save();
                }
            }
        }
        protected function updateReseptur(){
            
        }
        protected function updateDetailReseptur(){
            
        }

        protected function saveObatAlkes($modPenjualan,$penjualanResep,$update=false, $postRekenings = array())
        {
            $modObat = null;
            $format = new CustomFormat();
            if (count($penjualanResep) > 0)
            {
                /*
                if ($update)
                {
                    $obatResepLama = ObatalkespasienT::model()->findAll('penjualanresep_id=:penjualanresep', array(':penjualanresep'=>$modPenjualan->penjualanresep_id));
                    if (count($obatResepLama) > 0){
                        if ($this->kembalikanStok($obatResepLama)){
                            ObatalkespasienT::model()->deleteAll('penjualanresep_id='.$modPenjualan->penjualanresep_id);
                        } else {
                            throw new Exception("Pengembalian stok gagal");
                        }
                    }
                }
                 * 
                 */
                $data = true; //pointer to first key
                $racikan = '';
                foreach ($penjualanResep as $i => $obatResep)
                {
                    if($obatResep['detailreseptur_id'] != 0)
                    {
//                        $modObatAlkes = new ObatalkespasienT('retail');
                        $modObatAlkes = new FAObatalkesPasienT();
                        $modObatAlkes->attributes = $obatResep;
                        $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
                        $modObatAlkes->penjualanresep_id = $modPenjualan->penjualanresep_id;
                        $modObatAlkes->pendaftaran_id = $modPenjualan->pendaftaran_id;
                        $modObatAlkes->pasienadmisi_id = $modPenjualan->pasienadmisi_id;
                        $modObatAlkes->penjamin_id = $modPenjualan->penjamin_id;
                        $modObatAlkes->carabayar_id = $modPenjualan->carabayar_id;
                        $modObatAlkes->pasien_id = $modPenjualan->pasien_id;
                        $modObatAlkes->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
//                        $modObatAlkes->pegawai_id = $modPenjualan->pegawai_id;
//                        $modObatAlkes->pegawai_id = $_POST['FAResepturT']['pegawai_id'];
                        if($modObatAlkes->pegawai_id)
                        {
                            $modObatAlkes->pegawai_id = $_POST['FAResepturT']['pegawai_id'];
                        }else{
                            $modObatAlkes->pegawai_id = Yii::app()->user->getState('pegawai_id');;
                        }
                        
                        $modObatAlkes->permintaan_oa = $obatResep['permintaan_reseptur'];
                        $modObatAlkes->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
                        $modObatAlkes->qty_oa = $obatResep['qty'];
                        $modObatAlkes->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
						$modObatAlkes->hargasatuanjual_oa = $obatResep['hargasatuanjual_oa'];
                        $modObatAlkes->harganetto_oa = $obatResep['harganetto_reseptur'];
                        $modObatAlkes->hargajual_oa = $obatResep['hargajual_reseptur'];
                        $modObatAlkes->signa_oa = $obatResep['signa_reseptur'];
                        $modObatAlkes->etiket_satuan = $obatResep['etiket_satuan'];
                        $modObatAlkes->etiket_caramakan = $obatResep['etiket_caramakan'];
                        $modObatAlkes->etiket_keterangan = $obatResep['etiket_keterangan'];
//                        $modObatAlkes->discount = (isset($obatResep['discount'])) ? $obatResep['discount'] : 0;
                        $modObatAlkes->discount = ($obatResep['disc']!=0) ? $obatResep['disc'] : 0; 
                        $modObatAlkes->oa = 'AP';
                        $modObatAlkes->racikan_id = ($obatResep['isRacikan'] == 0) ? Params::DEFAULT_NON_RACIKAN_ID : Params::DEFAULT_RACIKAN_ID;
                        
                        $obat_alkes = FAObatalkesPasienT::model()->findAllByPk($obatResep['obatalkes_id']);
                        
                        /* Biaya Service Disimpan pada record racikan pertama sesuai dengan yang muncul di form penjualan -> total tarif service*/
                        /*
                        if (!empty($modObatAlkes->racikan_id))
                        {
                            if($modObatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $modObatAlkes->obatalkes->jenisobatalkes_id == 3)
                            {
                                $tarifService = 0;
                            }else{
                                $tarifService = RacikanM::model()->findByPk($modObatAlkes->racikan_id)->tarifservice;
                            }
//                            $tarifService = RacikanM::model()->findByPk($modObatAlkes->racikan_id)->tarifservice;
                            $modObatAlkes->biayaservice = $tarifService;
                        }
                        */
                        
                        if ($data)
                        {
                            $modObatAlkes->biayaservice = $modPenjualan->totaltarifservice;
                            $modObatAlkes->biayaadministrasi = $modPenjualan->biayaadministrasi;
                            $modObatAlkes->biayakonseling = $modPenjualan->biayakonseling;
                            $modObatAlkes->jasadokterresep = $modPenjualan->jasadokterresep;
                            $data = false; //set false so another key is not first key
                        }else{
                            $modObatAlkes->biayaservice = 0;
                            $modObatAlkes->biayaadministrasi = 0;
                            $modObatAlkes->biayakonseling = 0;
                            $modObatAlkes->jasadokterresep = 0;
                        }
                        
                        $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB($modPenjualan->tglpenjualan); 
                        $modObatAlkes->tglpelayanan = $modPenjualan->tglpenjualan;
                         
                        if($obatResep['disc'] != 0)
                        {
                            $modObatAlkes->discount = $obatResep['disc'];
                        }
                        $modObatAlkes->tipepaket_id = Params::TIPEPAKET_NONPAKET;

                        $modObatAlkes->shift_id = Yii::app()->user->getState('shift_id');
                        $modObatAlkes->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modObatAlkes->subsidiasuransi = 0;
                        $modObatAlkes->subsidipemerintah = 0;
                        $modObatAlkes->subsidirs = 0;
                        $modObatAlkes->iurbiaya = 0;
                        $modObatAlkes->tarifcyto = 0;
                        $stok = StokobatalkesT::model()->getStokBarang($modObatAlkes->obatalkes_id,$modObatAlkes->ruangan_id);
                        
                        if($modObatAlkes->validate())
                        {
                            if($modObatAlkes->qty_oa <= $stok)
                            {
                                //// cek stok vs qty
                                $modObatAlkes->save();
                                StokobatalkesT::kurangiStok($modObatAlkes->qty_oa, $modObatAlkes->obatalkes_id);
                                if(isset($postRekenings)){
                                    $modJurnalRekening = TindakanController::saveJurnalRekening();
                                    //update jurnalrekening_id
                                    $modObatAlkes->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                                    $modObatAlkes->save();
                                    $saveDetailJurnal = PenjualanResepController::saveJurnalDetailsObat($modJurnalRekening, $postRekenings, $modObatAlkes);
                                }
                                $this->successSaveObat = true;
                            }else{
                                Yii::app()->user->setFlash('warning',"Stok Obat Alkes ".$modObatAlkes->obatalkes_id . ' - ' . $modObatAlkes->ruangan_id . ' - ' . $modObatAlkes->obatalkes->obatalkes_kode." - ".$modObatAlkes->obatalkes->obatalkes_nama." tidak mencukupi");
                                $this->successSaveObat = false;
                            }
                        }else{
                            $this->successSaveObat = false;
                            Yii::app()->user->setFlash('error',"Data Obat Alkes Tidak valid $i"."<pre>".print_r($modObatAlkes->attributes,1)."</pre>");
                        }
                        $modObat[$i] = $modObatAlkes;
                    }
                }
                
                /* perhitungan untuk biaya racik 
                 * DIMATIKAN KARENA PERHITUNGANNYA SALAH DAN FATAL DI BILLING
                 * biayaracik sudah disesuaikan dengan penjualanresep_t.totaltarifservice disimpan pada obata record pertama
                 * hitungan formula biaya racik dilakukan di form dengan ajax
                $attributes = array(
                    'penjualanresep_id'=>$modPenjualan->penjualanresep_id,
                    'racikan_id'=>Params::DEFAULT_RACIKAN_ID
                );
                $obat_racik = FAObatalkesPasienT::model()->findAllByAttributes($attributes);
                if(count($obat_racik) > 0)
                {
                    $jum_racik = count($obat_racik);
                    $sql = "SELECT * FROM racikandetail_m WHERE racikan_id = ". Params::DEFAULT_RACIKAN_ID ." AND ". $jum_racik ." BETWEEN qtymin AND qtymaks";
                    $record = YII::app()->db->createCommand($sql)->queryRow();

                    if($record)
                    {
                        $tarifservice = $record['tarifservice'];
                    }else{
                        $sql = "SELECT * FROM racikan_m WHERE racikan_id = ". Params::DEFAULT_RACIKAN_ID ." AND racikan_aktif = true";
                        $record = YII::app()->db->createCommand($sql)->queryRow();
                        $tarifservice = $record['tarifservice'];
                    }
                    
                    $is_simpan = true;
                    foreach($obat_racik as $val)
                    {
                        $attributes = array(
                            'biayaservice'=>0
                        );
                        if($is_simpan)
                        {
                            $attributes = array(
                                'biayaservice'=>$tarifservice
                            );
                            $is_simpan = false;
                        }
                        FAObatalkesPasienT::model()->updateByPk($val->obatalkespasien_id, $attributes);

                    }
                }
                */
            }else{
                $this->successSaveObat = false;
            }
            
            return $modObat;
        }
        protected function updateObatAlkes($modObatAlkes, $modPenjualan,$penjualanResep)
        {
            //== Update Data Obat Lama + Simpan Data Obat Baru ==
            $format = new CustomFormat;
            foreach ($penjualanResep as $i => $obatResep) {
                if(($obatResep['pilihObat'] == 1 || $obatResep['detailreseptur_id'] == 1)){ // Jika pilih dicentang
                    if(empty($obatResep['obatalkespasien_id'])){ //Jika data obat baru
                        
                        $modObatAlkes[$i] = new FAObatalkesPasienT();
                        $modObatAlkes[$i]->attributes = $obatResep;
                        $modPenjualan->penjualanresep_id = (empty($modPenjualan->penjualanresep_id)) ? 1 : $modPenjualan->penjualanresep_id;
                        $modObatAlkes[$i]->penjualanresep_id = $modPenjualan->penjualanresep_id;
                        $modObatAlkes[$i]->pendaftaran_id = $modPenjualan->pendaftaran_id;
                        $modObatAlkes[$i]->penjamin_id = $modPenjualan->penjamin_id;
                        $modObatAlkes[$i]->carabayar_id = $modPenjualan->carabayar_id;
                        $modObatAlkes[$i]->pasien_id = $modPenjualan->pasien_id;
                        $modObatAlkes[$i]->kelaspelayanan_id = $modPenjualan->kelaspelayanan_id;
                        if($modObatAlkes[$i]->pegawai_id){
                                $modObatAlkes[$i]->pegawai_id =$_POST['FAResepturT']['pegawai_id'];
                        }else{
                            $modObatAlkes[$i]->pegawai_id = Yii::app()->user->getState('pegawai_id');;
                        }
                        
                        $modObatAlkes[$i]->discount = ($obatResep['disc']!=0) ? $obatResep['disc'] : 0; 
                        $modObatAlkes[$i]->oa = 'AP';
                        $modObatAlkes[$i]->create_loginpemakai_id = Yii::app()->user->id;
                        $modObatAlkes[$i]->create_time = date('Y-m-d H:i:s');
                        $modObatAlkes[$i]->shift_id = Yii::app()->user->getState('shift_id');
                        $modObatAlkes[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modObatAlkes[$i]->subsidiasuransi = 0;
                        $modObatAlkes[$i]->subsidipemerintah = 0;
                        $modObatAlkes[$i]->subsidirs = 0;
                        $modObatAlkes[$i]->iurbiaya = 0;
                        $modObatAlkes[$i]->tarifcyto = 0;
                        $modObatAlkes[$i]->biayakemasan = 0;
                    }
                        $modObatAlkes[$i]->permintaan_oa = $obatResep['permintaan_reseptur'];
                        $modObatAlkes[$i]->jmlkemasan_oa = $obatResep['jmlkemasan_reseptur'];
                        $modObatAlkes[$i]->qty_selisih = $obatResep['qty'] - $modObatAlkes[$i]->qty_oa ;
                        $modObatAlkes[$i]->qty_oa = $obatResep['qty'];
                        $modObatAlkes[$i]->hargasatuan_oa = $obatResep['hargasatuan_reseptur'];
                        $modObatAlkes[$i]->harganetto_oa = $obatResep['harganetto_reseptur'];
                        $modObatAlkes[$i]->hargajual_oa = $obatResep['hargajual_reseptur'];
                        $modObatAlkes[$i]->signa_oa = $obatResep['signa_reseptur'];
                        $modObatAlkes[$i]->discount = (empty($obatResep['disc'])) ? 0 : $obatResep['disc']; 
                        
                        if ($i == 0){ //hanya simpan untuk detail yang pertama
                            $modObatAlkes[$i]->biayaservice = $modPenjualan->totaltarifservice;
                            $modObatAlkes[$i]->biayaadministrasi = $modPenjualan->biayaadministrasi;
                            $modObatAlkes[$i]->biayakonseling = $modPenjualan->biayakonseling;
                            $modObatAlkes[$i]->jasadokterresep = $modPenjualan->jasadokterresep;
                        }else{
                            $modObatAlkes[$i]->biayaservice = 0;
                            $modObatAlkes[$i]->biayaadministrasi = 0;
                            $modObatAlkes[$i]->biayakonseling = 0;
                            $modObatAlkes[$i]->jasadokterresep = 0;
                        }
                        $modPenjualan->tglpenjualan = $format->formatDateTimeMediumForDB($modPenjualan->tglpenjualan); 
                        $modObatAlkes[$i]->tglpelayanan = $modPenjualan->tglpenjualan;
                        if($obatResep['disc']!=0){ $modObatAlkes[$i]->discount = $obatResep['disc']; }
                        $modObatAlkes[$i]->tipepaket_id = Params::TIPEPAKET_NONPAKET;
                        $modObatAlkes[$i]->update_loginpemakai_id = Yii::app()->user->id;
                        $modObatAlkes[$i]->update_time = date('Y-m-d H:i:s');
                        $stok = StokobatalkesT::model()->getStokBarang($modObatAlkes[$i]->obatalkes_id,Yii::app()->user->getState('ruangan_id'));
                        
                        if($modObatAlkes[$i]->validate())
                        {   
                            if($modObatAlkes[$i]->qty_oa <= $stok){
                                $modObatAlkes[$i]->save();
                                
                                if($modObatAlkes[$i]->qty_selisih > 0){ //jika selisih positif (qty bertambah)
                                    StokobatalkesT::kurangiStok($modObatAlkes[$i]->qty_selisih, $modObatAlkes[$i]->obatalkes_id, Yii::app()->user->getState('ruangan_id'));
                                }else{
                                    $selisih = abs($modObatAlkes[$i]->qty_selisih);
                                    StokobatalkesT::tambahStok($modObatAlkes[$i], $selisih);
                                }
                                $this->successUpdateObat = true;
                            }else{
                                Yii::app()->user->setFlash('warning',"Stok Obat Alkes ".$modObatAlkes[$i]->obatalkes->obatalkes_kode." - ".$modObatAlkes[$i]->obatalkes->obatalkes_nama." tidak mencukupi");
                                $this->obatGagalSimpan[$i]['kode_obat'] = $modObatAlkes[$i]->obatalkes->obatalkes_kode;
                                $this->obatGagalSimpan[$i]['nama_obat'] = $modObatAlkes[$i]->obatalkes->obatalkes_nama;
                                $this->successUpdateObat = true;
                            }
                        }else{
                            $this->successUpdateObat = true;
                            Yii::app()->user->setFlash('error',"Data Obat Alkes Tidak valid $i"."<pre>".print_r($modObatAlkes[$i]->attributes,1)."</pre>");
                        }
                    
                }else{ //Jika pilih tidak dicentang data di hapus
//                    $modObatAlkes[$i]->deleteByPk($modObatAlkes[$i]->obatalkespasien_id);
                    //Retur Stok
                }
                $modObat[$i] = $modObatAlkes;
            }
            //==================================   
            //== Tambahkan Item Baru Jika ada ==
            
            //== Hapus Item Lama Jika Tidak Di pilih ==
            
            
            return $modObat;
        }
        
        /**
         * kembalikanStok hanya boleh dilakukan apabila ada ubah data transaksi penjualan
         * @param type $obatAlkesT
         * @return type
         */
        protected function kembalikanStok($obatAlkesT)
        {
            $success = false;
            $jumlah = 0;
            if (count($obatAlkesT) > 0){
                foreach ($obatAlkesT as $i => $obatResepLama) {
                    $criteria=new CDbCriteria;

                    $criteria->compare('obatalkes_id',$obatResepLama->obatalkes_id);
                    $criteria->addCondition('qtystok_in > 0 OR qtystok_out > 0  OR qtystok_current > 0');
                    $criteria->order='tglkadaluarsa';
                    
                    $datastoklama = StokobatalkesT::model()->find($criteria);
                    $qtyout = $datastoklama->qtystok_out - $obatResepLama->qty_oa;
                    $qtycurrent = $datastoklama->qtystok_current + $obatResepLama->qty_oa;
//                    echo $datastoklama->stokobatalkes_id."<br>";
//                    echo $datastoklama->obatalkes_id."<br>";
//                    echo $qtyout."<br>";
//                    echo $qtycurrent."<br>";
//                    echo $obatResepLama->qty_oa."<br>";
//                    exit();
                    $stok = StokobatalkesT::model()->UpdateAll(
                        array(
                            'qtystok_out' =>$qtyout,
                            'qtystok_current'=>$qtycurrent
                        ),
                        'obatalkes_id=:obatalkes_id and stokobatalkes_id=:stokobatalkes_id',
                        array(
                            ':obatalkes_id'=>$obatResepLama->obatalkes_id,
                            ':stokobatalkes_id'=>$datastoklama->stokobatalkes_id
                        )
                    );
//                    $stok = new StokobatalkesT();
//                    $stok->obatalkes_id = $obatAlkes->obatalkes_id;
//                    $stok->sumberdana_id = $obatAlkes->sumberdana_id;
//                    $stok->ruangan_id = Yii::app()->user->getState('ruangan_id');
//                    $stok->tglstok_in = date('Y-m-d H:i:s');
//                    $stok->tglstok_out = date('Y-m-d H:i:s');
//                    $stok->qtystok_in = $obatAlkes->qty_oa;
//                    $stok->qtystok_out = 0;
//                    $stok->qtystok_current = $obatAlkes->qty_oa;
//                    $stok->harganetto_oa = $obatAlkes->harganetto_oa;
//                    $stok->hargajual_oa = $obatAlkes->hargasatuan_oa;
//                    $stok->discount = $obatAlkes->discount;
//                    $stok->satuankecil_id = $obatAlkes->satuankecil_id;
//                    if ($stok->validate()){
                        if ($stok){
                            $success = true;
//                            $jumlah++;
                        }
//                    }
                }
            }
            
            return (($stok) ? $success : false);
        }
        
        
        public function actionAjaxCariPasien()
        {
                
        }
        /**
         * actionPrintKwPenjualanResep digunakan untuk print kwitansi pada Transaksi Penjualan Resep
         * @param type $id
         */
        public function actionPrintKwPenjualanResep($id = null){
           $this->layout = '//layouts/frameDialog';
           $modPenjualan = FAPenjualanResepT::model()->findByAttributes(array('penjualanresep_id'=>$id));
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
           $obatAlkes = FAObatalkesPasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
            }
            $this->render($this->pathView.'PrintKwPenjualanResep',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
        }
        public function actionPrintKwitansiPenjualan($id = null){
           $this->layout = '//layouts/frameDialog';
           $modPenjualan = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $obatAlkes = FAObatalkesPasienT::model()->findAll($criteria);
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintRincian',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintRincian',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintRincian',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }
        public function actionPrintKwitansiPenjualanBebas($id = null){
           $modPenjualan = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           $obatAlkes = FAObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $modPenjualan->penjualanresep_id . ' ');
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pasien_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$obatAlkes[0]->pasien_id));
           
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintRincianPenjualanBebas',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintRincianPenjualanBebas',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintRincianPenjualanBebas',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }
        public function actionPrintKwitansiPenjualanResepLuar($id = null){
           $this->layout = '//layouts/frameDialog';
           $modPenjualan = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pasien_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$obatAlkes[0]->pasien_id));
           
           $criteria = new CDbCriteria();
           $criteria->select = 't.penjualanresep_id,
                                sum(t.qty_oa) As qty_oa,
                                sum(penjualanresep_t.biayaadministrasi) As biayaadministrasi,
                                sum(penjualanresep_t.biayakonseling) As biayakonseling,
                                sum(penjualanresep_t.totaltarifservice) As biayaservice,
                                sum(penjualanresep_t.jasadokterresep) As jasadokterresep,
                                sum(t.hargasatuan_oa) As hargasatuan_oa,
                                sum((t.qty_oa*t.hargasatuan_oa)*(t.discount/100)) As diskon,
                                sum((t.qty_oa * t.hargasatuan_oa)) As subtotal';
           $criteria->group = 't.penjualanresep_id';
           $criteria->join = 'RIGHT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id RIGHT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = t.obatalkes_id';
//           $criteria->with = array('penjualanresep,obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $obatAlkes = FAObatalkesPasienT::model()->findAll($criteria);
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }
        
        public function actionPrintEtiket($id = null){
           $this->layout = '//layouts/frameDialog';
           $modPenjualan = FAPenjualanResepT::model()->find('penjualanresep_id = ' . $id . '');
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pasien_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$modPenjualan->pasien_id));
           
           $criteria = new CDbCriteria();
           $criteria->with = array('obatalkes');
           $criteria->compare('t.penjualanresep_id',$id);
           $obatAlkes = FAObatalkesPasienT::model()->findAll($criteria);
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintEtiket',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintRincianResepLuar',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintEtiket',array('modPenjualan'=>$modPenjualan, 'obatAlkes'=>$obatAlkes, 'daftar'=>$daftar,'pasien'=>$pasien, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }

        public function actionPrintReseptur($id = null){
           $reseptur = FAResepturT::model()->find('reseptur_id = ' . $id . '');
           $detailreseptur = FAResepturDetailT::model()->findAll('reseptur_id = ' . $reseptur->reseptur_id . ' ');
           $daftar = FAPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$reseptur->pendaftaran_id));
           $pasien = FAPasienM::model()->findByAttributes(array('pasien_id'=>$reseptur->pasien_id));
           $detailpenjualan = FAPenjualanResepT::model()->findAll('reseptur_id='.$reseptur->reseptur_id.'');
           $modDetailPenjualan = FAInformasipenjualanresepV::model()->findAll('reseptur_id=' . $id. ' and pasien_id='.$reseptur->pasien_id.'');
           $modReseptur = FAPenjualanResepT::model()->findByPk($modDetailPenjualan->penjualanresep_id);
           $obatAlkes = FAObatalkesPasienT::model()->findByAttributes(array('penjualanresep_id'=>$detailpenjualan->penjualanresep_id));
            
            $judulLaporan='Laporan Penerimaan Kas';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render($this->pathView.'PrintReseptur',array('obatAlkes'=>$obatAlkes,'modDetailPenjualan'=>$modDetailPenjualan, 'modReseptur'=>$modReseptur,'reseptur'=>$reseptur, 'detailpenjualan'=>$detailpenjualan,'daftar'=>$daftar,'pasien'=>$pasien,'detailreseptur'=>$detailreseptur, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render($this->pathView.'PrintReseptur',array('obatAlkes'=>$obatAlkes, 'modDetailPenjualan'=>$modDetailPenjualan, 'modReseptur'=>$modReseptur,'reseptur'=>$reseptur,  'detailpenjualan'=>$detailpenjualan,'daftar'=>$daftar,'pasien'=>$pasien,'detailreseptur'=>$detailreseptur,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial($this->pathView.'PrintReseptur',array('obatAlkes'=>$obatAlkes,'modDetailPenjualan'=>$modDetailPenjualan, 'modReseptur'=>$modReseptur,'reseptur'=>$reseptur,  'detailpenjualan'=>$detailpenjualan,'daftar'=>$daftar,'pasien'=>$pasien,'detailreseptur'=>$detailreseptur,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            } 
        }
        /**
         * 
         */
        public function actionGetCaraBayar()
        {
            if(Yii::app()->request->isAjaxRequest) {
                $idObatalkes = $_POST['idObatalkes'];
                $returnVal['stok'] = StokobatalkesT::getStokBarang(
                    $idObatalkes, Yii::app()->user->getState('ruangan_id')
                );
                
                echo CJSON::encode($returnVal);
                Yii::app()->end();
            }
        }
        /**
         * actionCopyResep copy resep dari form penjualan resep RS dengan menggunakan request ajax
         */
        public function actionCopyResep()
	{
            if(Yii::app()->request->isAjaxRequest){
//                echo "Yeah"; exit;
                $this->layout='//layouts/frameDialog'; 
                $modelPuenjualanResep = 
                
                $model->tglcopy = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($model->tglcopy, 'yyyy-MM-dd'));
                
                $this->render('formCopyResep',array(
                                'modelPenjualanResep'=>$modelPenjualanResep,
                                'modPasien'=>$modPasien,
                                'model'=>$model,
                                'modCopy'=>$modCopy,
                                'modDetailPenjualan'=>$modDetailPenjualan,
                                'tersimpan'=>$tersimpan,
                          ));
             }
	}
        
        /**
         * @param type $modJurnalRekening
         * @param type $postRekenings
         * @param type $jenisSimpan
         * @return boolean
         */
        public function saveJurnalDetailsObat($modJurnalRekening, $postRekenings, $modTindakanObat, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                //insert berdasarkan daftartindakan_id atau obatalkes_id dgn melihat jnspelayanan
                $idOaTindakanSama = false;
                if(isset($modTindakanObat->daftartindakan_id)){
                    if(trim($rekening['daftartindakan_id'] == $modTindakanObat->daftartindakan_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->daftartindakan->daftartindakan_nama;
                }else if(isset($modTindakanObat->obatalkes_id)){
                    if(trim($rekening['obatalkes_id'] == $modTindakanObat->obatalkes_id))
                        $idOaTindakanSama = true;
                    $nama_tindakan = $modTindakanObat->obatalkes->obatalkes_kode." - ".$modTindakanObat->obatalkes->obatalkes_nama;
                }
                if($idOaTindakanSama){
                    
                    $model[$i] = new JurnaldetailT();
                    $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                    $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                    $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                    $model[$i]->uraiantransaksi = $nama_tindakan;
                    $model[$i]->saldodebit = $rekening['saldodebit'];
                    $model[$i]->saldokredit = $rekening['saldokredit'];
                    $model[$i]->nourut = $i+1;
                    $model[$i]->rekening1_id = $rekening['struktur_id'];
                    $model[$i]->rekening2_id = $rekening['kelompok_id'];
                    $model[$i]->rekening3_id = $rekening['jenis_id'];
                    $model[$i]->rekening4_id = $rekening['obyek_id'];
                    $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                    $model[$i]->catatan = "";
                    if($model[$i]->validate())
                    {
                        $model[$i]->save();
                    }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                        $valid = false;
                        break;
                    }
                }
            }
            return $valid;        
        }
        
        public function actionStatusPeriksa()
        {
            if(Yii::app()->request->isAjaxRequest){
                $status = (isset($_POST['statusperiksa']) ? $_POST['statusperiksa'] : null);
                $pendaftaran_id = (isset($_POST['pendaftaran_id']) ? $_POST['pendaftaran_id'] : null);
                $pasienmasukpenunjang_id = (isset($_POST['pasienmasukpenunjang_id']) ? $_POST['pasienmasukpenunjang_id'] : null);
                $pasienkirimkeunitlain_id = (isset($_POST['pasienkirimkeunitlain_id']) ? $_POST['pasienkirimkeunitlain_id'] : null);
                
                $statuspasien='';
                $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                if(!empty($modPendaftaran->pasienadmisi_id)){
                    $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                    if(!empty($modPasienAdmisi->pasienpulang_id)){
                        $statuspasien = 'SUDAH PULANG';
                    }
                }else{
                   $statuspasien = $modPendaftaran->statusperiksa;
                }
                
                $data['pendaftaran_id'] = $pendaftaran_id;
                $data['pasienmasukpenunjang_id'] = $pasienmasukpenunjang_id;
                $data['pasienadmisi_id'] = $modPendaftaran->pasienadmisi_id;
                $data['pasien_id'] = $modPendaftaran->pasien_id;
                $data['pasienkirimkeunitlain_id'] = $pasienkirimkeunitlain_id;
                $data['statuspasien'] = $statuspasien;
                
                echo json_encode($data);
                Yii::app()->end();
            }
        }
        
        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
}