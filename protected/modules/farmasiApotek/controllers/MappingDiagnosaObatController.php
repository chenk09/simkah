<?php

class MappingDiagnosaObatController extends Controller
{
	public function actionIndex()
	{
	    $model = new DiagnosaM();
        $model->unsetAttributes();
        if (isset($_GET['DiagnosaM']))
        {
            $this->layout = false;
            $model->attributes = $_GET['DiagnosaM'];
        }
		$this->render('index', array(
            'model'=>$model
        ));
	}

    public function actionMappingObat($id)
    {
        $model = DiagnosaM::model()->findByPk($id);

        $obats = new ObatalkesM();
        $obats->unsetAttributes();
        if(isset($_GET['ObatalkesM']))
        {
            $this->layout = false;
            $obats->attributes = $_GET['ObatalkesM'];
        }

        $MapDiag = MapDiagnosaObat::model()->findByAttributes(array(
            'diagnosa_id'=>$id,
            'kelas_pelayanan_id'=>29,
        ));

        $mapobats = new MapDiagnosaObatDet();
        $mapobats->mapping_diag_id = $MapDiag->mapping_diag_obat_id;
        if(isset($_GET['MapDiagnosaObat']))
        {
            $this->layout = false;
            $mapobats->attributes = $_GET['MapDiagnosaObat'];
        }

        if(Yii::app()->request->isPostRequest && isset($_POST['MapDiagnosaObat']))
        {
            $this->layout = false;
            $vRest = array(
                'status'=>'ok'
            );

            $transaction = Yii::app()->db->beginTransaction();
            try{
                $obats = $_POST['obats'];
                if(count($obats) > 0)
                {
                    foreach ($obats as $index => $obat)
                    {
                        $vJum = MapDiagnosaObatDet::model()->countByAttributes(array(
                            'mapping_diag_id' => $MapDiag->mapping_diag_obat_id,
                            'obat_alkes_id' => $obat,
                        ));
                        if($vJum == 0)
                        {
                            $mapobats = new MapDiagnosaObatDet();
                            $mapobats->mapping_diag_id = $MapDiag->mapping_diag_obat_id;
                            $mapobats->obat_alkes_id = $obat;
                            if(!$mapobats->save())
                            {
                                $error = array();
                                foreach($mapobats->getErrors() as $x=>$z)
                                {
                                    $error[$x] = implode(", ", $z);
                                }
                                throw new Exception('<ol><li>'. implode("</li><li>", $error) .'</li></ol>');
                            }
                        }
                    }
                }else{
                    throw new Exception('Master obat belum dipilih, silakan lakukan pemilihan terlebih dahulu !!');
                }
                $transaction->commit();
            }catch (Exception $e)
            {
                $vMsg = $e->getMessage();
                $vRest['status'] = 'not';
                $vRest['msg'] = $vMsg;
                $transaction->rollback();
            }
            echo json_encode($vRest);
            Yii::app()->end();
        }

        $this->render('mapping_obat', array(
            'model'=>$model,
            'obats'=>$obats,
            'mapobats'=>$mapobats,
        ));
    }

    public function actionRemoveMappingObat($id)
    {
        $this->layout = false;
        $vRest = array(
            'status'=>'not'
        );
        if(Yii::app()->request->isPostRequest && isset($_POST['MapDiagnosaObat']))
        {
            $vRest['status'] = 'ok';
            $transaction = Yii::app()->db->beginTransaction();
            try{
                $obats = $_POST['obats'];
                if(count($obats) > 0)
                {
                    foreach ($obats as $index => $obat)
                    {
                        $vJum = MapDiagnosaObatDet::model()->findByPk($obat);
                        if($vJum == 0)
                        {
                            throw new Exception('Data obat tidak terdaftar, silakan dicek kembali !!');
                        }
                        if(!$vJum->delete())
                        {
                            throw new Exception('Proses delete item tidak berhasil, silakan dicek kembali !!');
                        }
                    }
                }else{
                    throw new Exception('Master obat belum dipilih, silakan lakukan pemilihan terlebih dahulu !!');
                }
                $transaction->commit();
            }catch (Exception $e)
            {
                $vMsg = $e->getMessage();
                $vRest['status'] = 'not';
                $vRest['msg'] = $vMsg;
                $transaction->rollback();
            }
        }
        echo json_encode($vRest);
        Yii::app()->end();
    }

}