<?php

class PasienResepController extends SBaseController
{
	public function actionIndex()
	{
            $modReseptur = new FAResepturT('searchPasien');
            $modReseptur->unsetAttributes();
            $modReseptur->tglAwal=date("d M Y").' 00:00:00';
            $modReseptur->tglAkhir=date("d M Y H:i:s");
            if(isset($_GET['FAResepturT'])){
                $format = new CustomFormat();
                $modReseptur->attributes = $_GET['FAResepturT'];
                $modReseptur->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAResepturT']['tglAwal']);
                $modReseptur->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAResepturT']['tglAkhir']);
            }
		
            $this->render('index',array('modReseptur'=>$modReseptur));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}