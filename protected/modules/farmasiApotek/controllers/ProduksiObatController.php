
<?php

class ProduksiObatController extends SBaseController
{
        public $msgProduksiDetail = "";
        public $msgProduksi = "";
        public $msgObat = "";
        public $msgStok = "";
        public $msgKosong = "";
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'create';

	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
      if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new FAProduksiobatT;
		$modDetail=new FAProduksiobatdetT;
		$modObatalkesM=new FAObatalkesM; //agar nama2 elemennya sama dengan yg di master obat gudang untuk javascript
        $dataDetails = array(); //untuk mengembalikan detail jika gagal simpan
        $model->tglproduksiobt = date('d M Y H:i:s');
		if(isset($_POST['FAProduksiobatT']) && isset($_POST['FAProduksiobatdetT']) && isset($_POST['FAObatalkesM']))
		{
            $model->attributes=$_POST['FAProduksiobatT'];
            $model->tglproduksiobt = date('Y-m-d H:i:s');
            $model->create_time = $model->tglproduksiobt;
            $model->update_time = $model->tglproduksiobt;
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->update_loginpemakai_id = Yii::app()->user->id;
            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $model->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modObatalkesM->attributes = $_POST['FAObatalkesM'];
            
            $transaction = Yii::app()->db->beginTransaction();
            try {
			if($model->save()){
                            $suksesDetail = $this->simpanProduksiDetail($model, $modDetail, $_POST['FAProduksiobatdetT']); 
                                if($suksesDetail){
                                    $modObatalkesM->attributes = $_POST['FAObatalkesM'];
                                    if($modObatalkesM->save()){
                                    	$obat = $this->tambahStok($modObatalkesM, $_POST['FAObatalkesM']['stoksekarang']);
                                    	if ($obat->save()){
	                                        $transaction->commit();
	                                        Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
	                                        $this->refresh();
                                    	}else {
                                        	$this->msgStok .= "<br>Stok gagal disimpan !";
                                    	} 
                                    }else{
                                        $this->msgObat .= "<br>Master Obat ".$modObatalkesM->obatalkes_kode."-".$modObatalkesM->obatalkes_nama." gagal disimpan !";
                                    }
                                }
                        }else{
                            $this->msgProduksi .= "Produksi Obat ".$model->noproduksiobt." gagal disimpan !<br>";
                        }
                        //jika gagal simpan
                        $dataDetails = $_POST['FAProduksiobatdetT'];
                        $model->isNewRecord = true;
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ! <br>".$this->msgProduksiDetail.$this->msgStok.$this->msgObat.$this->msgProduksi);
                                
                    } catch (Exception $exc){
                        $model->isNewRecord = true;
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));

                    }
		}

		$this->render('create',array(
			'model'=>$model,
			'modDetail'=>$modDetail,
			'dataDetails'=>$dataDetails,
			'modObatalkesM'=>$modObatalkesM,
		));
	}

   protected function tambahStok($data, $qty){
        $modStokObat = new StokobatalkesT();
        $modStokObat->attributes = $data->attributes;
        $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modStokObat->tglstok_in = date('Y-m-d H:i:s');
        $modStokObat->qtystok_in = $qty;
        $modStokObat->qtystok_out = 0;
        $modStokObat->qtystok_current = $qty;
        $modStokObat->hargajual_oa = $data->hargajual;
        $modStokObat->harganetto_oa = $data->harganetto;
        $modStokObat->discount = $data->discount;
        

        $modStokObat->hjaresep = $data->hjaresep;
        $modStokObat->hjanonresep = $data->hjanonresep;
        $modStokObat->marginresep = $data->marginresep;
        $modStokObat->marginnonresep = $data->marginnonresep;
        $modStokObat->hpp = $data->hpp;

        $modStokObat->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $modStokObat->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modStokObat->tglstok_in = date('Y-m-d H:i:s');
        $modStokObat->tglstok_out = null;
        $modStokObat->create_time = date('Y-m-d H:i:s');
        $modStokObat->update_time = null;
        $modStokObat->create_loginpemakai_id = Yii::app()->user->id;
        $modStokObat->validate();
        return $modStokObat;
    }	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['FAProduksiobatT']))
		{
			$model->attributes=$_POST['FAProduksiobatT'];
			if($model->save()){
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
				$this->redirect(array('view','id'=>$model->produksiobat_id));
                        }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
                        if(!Yii::app()->user->checkAccess(Params::DEFAULT_DELETE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('FAProduksiobatT');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
      if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new FAProduksiobatT('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['FAProduksiobatT']))
			$model->attributes=$_GET['FAProduksiobatT'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=FAProduksiobatT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='faproduksiobat-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $model= new FAProduksiobatT;
            $model->attributes=$_REQUEST['FAProduksiobatT'];
            $judulLaporan='Data FAProduksiobatT';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        /**
         * untuk menyimpan dan validasi detail produksi
         * @param type $model
         * @param type $postDetails
         * @return boolean
         */
        protected function simpanProduksiDetail($model, $modDetail, $postDetails){
        	// echo $model['produksiobat_id'];
        	// exit();
            $sukses = true;
            $modDetails = array();
            foreach($postDetails AS $i => $post){
                $modDetails[$i] = new $modDetail;
                $modDetails[$i]->attributes = $post;
                // echo "</prev>";
                // print_r($post);
                // exit();
                $modDetails[$i]['produksiobat_id'] = $model->produksiobat_id;
                if($modDetails[$i]->validate()){
                    if(StokobatalkesT::kurangiStok($modDetails[$i]->qtyproduksi, $modDetails[$i]->obatalkes_id)){
                        $modDetails[$i]->save();
                        $sukses = $sukses && true;
                    }else{
                        $this->msgProduksiDetail .= "Stok ".$post['obatalkes_nama']." tidak mencukupi ! <br>";
                        $sukses = false;
                    }
                }else{
                    $this->msgProduksiDetail .= "Bahan ".$post['obatalkes_nama']." gagal disimpan ! <br>";
                    $sukses = false;
                }
            }
            return $sukses;
        }
}
