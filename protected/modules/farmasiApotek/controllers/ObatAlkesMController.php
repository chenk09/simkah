
<?php

class ObatAlkesMController extends SBaseController
{
        //$pathView agar bisa di extend dari modul lain
        public $pathView = 'farmasiApotek.views.obatAlkesM.';
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        public $successSaveObatAlkes=false;
        public $succesSaveModObatAlkesDetail=false;

	/**
	 * @return array action filters
	 */
//	DIKOMEN KARENA FILTER USER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view', 'informasi'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print','UbahHarga','cekOtoritas'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=GFObatAlkesM::model()->findByPk($id);
                $obatdetail=ObatalkesdetailM::model()->findByAttributes(array('obatalkes_id'=>$id));
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='gfobat-alkes-m-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionPrint()
        {
            $model= new ObatalkesM;
            $model->attributes=$_REQUEST['GFObatalkesfarmasiV'];
            $judulLaporan='Data Obat Alkes';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
        
        public function actionInformasi()
        {
            $model=new FAInfostokobatalkesruanganV('searchInformasi');
            $model->unsetAttributes();
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
            if(isset($_GET['FAInfostokobatalkesruanganV']))
            {
                $model->attributes = $_GET['FAInfostokobatalkesruanganV'];
                $model->isGroupObat = $_GET['FAInfostokobatalkesruanganV']['isGroupObat'];
                if(!empty($_GET['FAInfostokobatalkesruanganV']['filterTanggal']))
                    $model->filterTanggal = true;
                else 
                    $model->filterTanggal = false;
                $format = new CustomFormat();
                $model->tglAwal  = $format->formatDateTimeMediumForDB($_REQUEST['FAInfostokobatalkesruanganV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FAInfostokobatalkesruanganV']['tglAkhir']);
            }
            $this->render($this->pathView.'informasi',array('model'=>$model));
        }
        /**
         * actionInformasiStok = menampilkan informasi stok barang ruangan login
         */
        public function actionInformasiStok()
        {
            $model=new FAInfostokobatalkesruanganV('searchInformasiStok');
            $format = new CustomFormat();
            $model->unsetAttributes();
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
			
            if(isset($_GET['FAInfostokobatalkesruanganV'])){
                $model->attributes = $_GET['FAInfostokobatalkesruanganV'];
            }else{
				$model->jenisobatalkes_id = 22;
				$model->obatalkes_golongan = "KERAS";
				$model->sumberdana_id = 1;
			}
			
            $this->render($this->pathView.'informasiStok',array('model'=>$model));
        }
}
