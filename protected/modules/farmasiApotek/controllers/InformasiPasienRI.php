<?php

class InformasiPasienRIController extends SBaseController
{
	public function actionPasienRI()
	{
                $format = new CustomFormat();
                $modRI = new BKInformasikasirinappulangV;
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir = date('Y-m-d H:i:s');
                $modRI->tglAwalAdmisi = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhirAdmisi = date('Y-m-d H:i:s');
                
                if(isset($_GET['BKInformasikasirinappulangV'])){
                    $modRI->attributes = $_GET['BKInformasikasirinappulangV'];
                    if(!empty($_GET['BKInformasikasirinappulangV']['tglAwal'])){
                        $modRI->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAwal']);
                    }
                    if(!empty($_GET['BKInformasikasirinappulangV']['tglAkhir'])){
                        $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAkhir']);
                    }
                    $modRI->ceklis = $_GET['BKInformasikasirinappulangV']['ceklis'];
//                    if($modRI->ceklis==1){
                        $modRI->tglAwalAdmisi = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAwalAdmisi']);
                        $modRI->tglAkhirAdmisi = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAkhirAdmisi']);
//                    }
                }
                if (Yii::app()->request->isAjaxRequest) {
                    echo $this->renderPartial('_tablePasienRI', array('modRI'=>$modRI),true);
                }else{
                    $this->render('pasienRI',array('modRI'=>$modRI));
                }
                
	}
}