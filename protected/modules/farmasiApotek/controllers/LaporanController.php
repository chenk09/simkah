<?php
/**
* modules/farmasiApotek/controllers/LaporanController.php
* Updated by    : Hardi
* Date          : 23-04-2014
* Issue         : EHJ-1081
* Deskripsi     : Fungsi yang ditambahkan untuk laporan seluruh barang per produsen
*                 actionSeluruhBarangProdusen(), actionPrintSeluruhBarangProdusen()
**/
?>

<?php

class LaporanController extends LaporanFarmasiController {

    //untuk range tanggal default
    public $tglAwal = "d M Y 00:00:00";
    public $tglAkhir = "d M Y 23:59:59";
    
    public function actionLaporanPenjualanObat()
	{
        $model = new FALaporanpenjualanobatV();
	$model->tglAwal = date($this->tglAwal);
        $model->carabayar_id = 1;
        $model->tglAkhir = date($this->tglAkhir);
        $penjamin = CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'),'penjamin_id','penjamin_id');
        $ruangan = CHtml::listData(RuanganM::model()->findAll('ruangan_aktif = true'),'ruangan_nama','ruangan_nama');
        $model->penjamin_id = $penjamin;
        $model->ruanganasal_nama = $ruangan;
        if(isset($_GET['FALaporanpenjualanobatV'])){
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
        }
        
        $this->render('penjualanObat/index', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanPenjualanObat() {
        $model = new FALaporanpenjualanobatV('search');
        $judulLaporan = 'Laporan Penjualan Obat';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Penjualan Obat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['FALaporanpenjualanobatV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penjualanObat/_printPenjualanObat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikPenjualanObat() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanpenjualanobatV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Penjualan Obat';
        $data['type'] = $_GET['type'];
        if (isset($_GET['FALaporanpenjualanobatV'])) {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    public function actionLaporanLembarResep() {
        $model = new FALaporanlembarresepV('search');
        $format = new CustomFormat;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        if (isset($_GET['FALaporanlembarresepV'])) {
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAkhir']);
        }
        $this->render('laporanlembarresepV/index',array(
            'model'=>$model,
            'tglAwal'=>$model->tglAwal,
            'tglAkhir'=>$model->tglAkhir,
        ));
    }
    
    public function actionPrintLaporanLembarResep() {
        $model = new FALaporanlembarresepV('search');
        $judulLaporan = 'Laporan Lembar Resep';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Penjualan Obat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['FALaporanlembarresepV'])) {
            $model->attributes = $_REQUEST['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'laporanlembarresepV/Print';
        
//        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'tglAwal'=>$model->tglAwal, 'tglAkhir'=>$model->tglAkhir));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'tglAwal'=>$model->tglAwal, 'tglAkhir'=>$model->tglAkhir));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 0, 5, 15, 15);
            $mpdf->tMargin=5;
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'tglAwal'=>$model->tglAwal, 'tglAkhir'=>$model->tglAkhir), true));
            $mpdf->Output();
        }
    }
    
    public function actionframeGrafikLaporanLembarResep() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanlembarresepV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Lembar Resep';
        $data['type'] = $_GET['type'];
        $searchdata = $model->searchGrafikLembarResep();
        if (isset($_GET['FALaporanlembarresepV'])) {
            $model->attributes = $_GET['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'searchdata'=>$searchdata
        ));
    }
    
    public function actionLaporanLembarResepLuar() {
        $model = new FALaporanlembarresepluarV('search');
        $format = new CustomFormat;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        if (isset($_GET['FALaporanlembarresepluarV'])) {
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepluarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepluarV']['tglAkhir']);
        }
        $this->render('laporanlembarresepluarV/index',array(
            'model'=>$model,
        ));
    }
    
    public function actionframeGrafikLaporanLembarResepLuar() {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporanlembarresepluarV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Lembar Resep Luar';
        $data['type'] = $_GET['type'];
        $searchdata = $model->searchGrafik();
        if (isset($_GET['FALaporanlembarresepV'])) {
            $model->attributes = $_GET['FALaporanlembarresepV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepluarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanlembarresepluarV']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
            'searchdata'=>$searchdata
        ));
    }
    
    public function actionPrintLaporanLembarResepLuar() {
        $model = new FALaporanlembarresepluarV('search');
        $judulLaporan = 'Laporan Lembar Resep Luar';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Penjualan Obat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['FALaporanlembarresepluarV'])) {
            $model->attributes = $_REQUEST['FALaporanlembarresepluarV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepluarV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanlembarresepluarV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'laporanlembarresepluarV/Print';
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionLaporanKinerjaPenjualanObatAlkes() {
        $model = new FALaporankinerjapenjualanobatalkesV();
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        if (isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            // $model->jenispenjualan = $_GET['FALaporanpenjualanobatV']['jenispenjualan'];
            $model->pegawai_id = $_GET['FALaporankinerjapenjualanobatalkesV']['pegawai_id'];
            $model->jenisobatalkes_id = $_GET['FALaporankinerjapenjualanobatalkesV']['jenisobatalkes_id'];
            $model->obatalkes_id = $_GET['FALaporankinerjapenjualanobatalkesV']['obatalkes_id'];
        }


        $this->render('kinerjapenjualanOA/index', array(
            'model' => $model,
        ));
    }

    public function actionSeluruhBarangDokter()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $criteria=new CDbCriteria;

        if (isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->pegawai_id = $_GET['FALaporankinerjapenjualanobatalkesV']['pegawai_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('pegawai_id',$model->pegawai_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->addCondition('returresep_id IS NULL');
        $criteria->order = 'pegawai_id, jnskelompok, tglpenjualan';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $this->render('kinerjapenjualanOA/LapSeluruhBarangDokter/admin', array(
            'model' => $model, 'modKinerja'=> $modKinerja,
        ));   
    }

    public function actionPrintSeluruhBarangDokter()
    {
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $criteria=new CDbCriteria;
        $judulLaporan = 'Laporan Seluruh Barang per Dokter';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Barang per Dokter';
        $data['type'] = $_REQUEST['type'];
        if(isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->pegawai_id = $_GET['FALaporankinerjapenjualanobatalkesV']['pegawai_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('pegawai_id',$model->pegawai_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->addCondition('returresep_id IS NULL');
        $criteria->order = 'pegawai_id, jnskelompok, tglpenjualan';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);
        // echo count($modKinerja);

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kinerjapenjualanOA/LapSeluruhBarangDokter/Print';
        $this->printFunction2($model, $modKinerja, $data, $caraPrint, $judulLaporan, $target);

    }

    public function actionSeluruhBarangProdusen()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $criteria=new CDbCriteria;
        $format = new CustomFormat();
        $tglAwal    = $format->formatDateTimeMediumForDB($model->tglAwal);
        $tglAkhir   = $format->formatDateTimeMediumForDB($model->tglAkhir);

        if (isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->produsen_id = $_GET['FALaporankinerjapenjualanobatalkesV']['produsen_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);

            $tglAwal    = $model->tglAwal;
            $tglAkhir   = $model->tglAkhir;
        }
        $criteria->addBetweenCondition('tglpenjualan', $tglAwal, $tglAkhir);
        $criteria->addCondition('returresep_id IS NULL');
        $criteria->order = 'produsen,tglpenjualan,jnskelompok';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $this->render('kinerjapenjualanOA/LapSeluruhBarangProdusen/admin', array(
            'model' => $model, 'modKinerja'=> $modKinerja,
        ));   
    }

    public function actionPrintSeluruhBarangProdusen()
    {
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $criteria=new CDbCriteria;
        $judulLaporan = 'Laporan Seluruh Barang per Produsen';

        //Data Grafik       
        $data['title'] = 'Grafik Laporan Barang per Produsen';
        $data['type'] = $_REQUEST['type'];
        if(isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->produsen_id = $_GET['FALaporankinerjapenjualanobatalkesV']['produsen_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->addCondition('returresep_id IS NULL');
        $criteria->order = 'produsen,tglpenjualan,jnskelompok';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kinerjapenjualanOA/LapSeluruhBarangProdusen/Print';
        $this->printFunction2($model, $modKinerja, $data, $caraPrint, $judulLaporan, $target);

    }

    public function actionDetailPendapatanDokter()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $criteria=new CDbCriteria;

        if (isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->pegawai_id = $_GET['FALaporankinerjapenjualanobatalkesV']['pegawai_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('pegawai_id',$model->pegawai_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'jnskelompok, penjualanresep_id, obatalkes_id, returresep_id desc';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $this->render('kinerjapenjualanOA/LapDetailPendapatanDokter/admin', array(
            'model' => $model, 'modKinerja'=> $modKinerja,
        ));   
    }

    public function actionPrintDetailPendapatanDokter()
    {
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $criteria=new CDbCriteria;
        $judulLaporan = 'Laporan Detail Pendapatan Dokter';
        //Data Grafik       
        $data['title'] = 'Grafik Laporan Detail Pendapatan Dokter';
        $data['type'] = $_REQUEST['type'];
        if(isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->pegawai_id = $_GET['FALaporankinerjapenjualanobatalkesV']['pegawai_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            // $jenisObat = JenisobatalkesM::model()->findByPk($model->jenisobatalkes_id);
            $jenisObat = LookupM::model()->findbyAttributes(array('lookup_type'=>'jnskelompok','lookup_value'=>$model->jnskelompok));

            $model->jenisobatalkes_nama = $jenisObat->lookup_name;
            $pegawai = PegawaiM::model()->findByPk($model->pegawai_id);
            $model->dokter = $pegawai->nama_pegawai;

            $criteria->compare('pegawai_id',$model->pegawai_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'jnskelompok, penjualanresep_id, obatalkes_id, returresep_id desc';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kinerjapenjualanOA/LapDetailPendapatanDokter/Print';
        $this->printFunction2($model, $modKinerja, $data, $caraPrint, $judulLaporan, $target);  
    }

    public function actionDetailPendapatanProdusen()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $model->tglAwal = date($this->tglAwal);
        $model->tglAkhir = date($this->tglAkhir);
        $criteria=new CDbCriteria;

        if (isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->produsen_id = $_GET['FALaporankinerjapenjualanobatalkesV']['produsen_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'jnskelompok, penjualanresep_id, obatalkes_id, returresep_id desc';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $this->render('kinerjapenjualanOA/LapDetailPendapatanProdusen/admin', array(
            'model' => $model, 'modKinerja'=> $modKinerja,
        ));   
    }

    public function actionPrintDetailPendapatanProdusen()
    {
        $model = new FALaporankinerjapenjualanobatalkesV('search');
        $criteria=new CDbCriteria;
        $judulLaporan = 'Laporan Detail Pendapatan Produsen';
        //Data Grafik       
        $data['title'] = 'Grafik Laporan Detail Pendapatan Produsen';
        $data['type'] = $_REQUEST['type'];
        if(isset($_GET['FALaporankinerjapenjualanobatalkesV'])) {
            $model->attributes = $_GET['FALaporankinerjapenjualanobatalkesV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporankinerjapenjualanobatalkesV']['tglAkhir']);
            $model->produsen_id = $_GET['FALaporankinerjapenjualanobatalkesV']['produsen_id'];
            $model->jnskelompok = $_GET['FALaporankinerjapenjualanobatalkesV']['jnskelompok'];

            $jenisObat = LookupM::model()->findbyAttributes(array('lookup_type'=>'jnskelompok','lookup_value'=>$model->jnskelompok));

            $model->jenisobatalkes_nama = $jenisObat->lookup_name;
            
            if(!empty($model->produsen_id)){
                $produsen = PbfM::model()->findByPk($model->produsen_id);
                $model->produsen = $produsen->pbf_nama;
            }

            $criteria->compare('produsen_id',$model->produsen_id);
            $criteria->compare('jnskelompok',$model->jnskelompok);
        }
        $criteria->addBetweenCondition('tglpenjualan', $model->tglAwal, $model->tglAkhir);
        $criteria->order = 'jnskelompok, penjualanresep_id, obatalkes_id, returresep_id desc';
        $modKinerja = FALaporankinerjapenjualanobatalkesV::model()->findAll($criteria);

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kinerjapenjualanOA/LapDetailPendapatanProdusen/Print';
        $this->printFunction2($model, $modKinerja, $data, $caraPrint, $judulLaporan, $target);  
    }

//    YANG INI ERROR JADI MENGGUNAKAN CONTROLLER LaporanFarmasiController 
//    protected function printFunction($model, $modDetail, $data, $caraPrint, $judulLaporan, $target){
//        $format = new CustomFormat();
//        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
////        echo $caraPrint;
//        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
//            $this->layout = '//layouts/printWindows';
//            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint , 'modRincian'=>$modRincian));
//        } else if ($caraPrint == 'EXCEL') {
//            $this->layout = '//layouts/printExcel';
//            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian));
//        } else if ($_REQUEST['caraPrint'] == 'PDF') {
//            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
//            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
//            $mpdf = new MyPDF('', $ukuranKertasPDF);
//            $mpdf->useOddEven = 2;
//            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
//            $mpdf->WriteHTML($stylesheet, 1);
//            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
//            $mpdf->WriteHTML($this->renderPartial($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian), true));
//            $mpdf->Output();
//        }
//    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }

public function actionLaporanPenjualanPerKategori(){
	$model = new FALaporanpenjualanobatV();
	$model->unsetattributes();

        $model->tglAwal = date($this->tglAkhir);
        $model->tglAkhir = date($this->tglAkhir);
	//$model->obatalkes_kategori = "KERAS";

        if (isset($_GET['FALaporanpenjualanobatV'])) {
            $model->attributes = $_GET['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }

        $this->render('penjualanObatPerKategori/index', array(
            'model' => $model,
        ));
}
    public function actionPrintLaporanPenjualanObatPerKategori() {
        $model = new FALaporanpenjualanobatV('search');
        $judulLaporan = 'Laporan Penjualan Obat';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penjualan Obat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['FALaporanpenjualanobatV'])) {
            $model->attributes = $_REQUEST['FALaporanpenjualanobatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['FALaporanpenjualanobatV']['tglAkhir']);
        }
        
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penjualanObatPerKategori/_printPenjualanObat';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
}
