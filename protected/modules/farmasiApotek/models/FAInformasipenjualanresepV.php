<?php

class FAInformasipenjualanresepV extends InformasipenjualanaresepV
{
        public $tglAwal;
        public $tglAkhir;
        public $umur;
        public $returresep_id;
        public $jenispenjualan;
        public $jenisPenjualan;
        public $ruanganasalobat;
        public $jumlah_return;
        public $copy_resep_qty;
        public $copy_resep_det;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
        public function searchInfoJualResep()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->select = array('t.pendaftaran_id, penjualanresep_t.jenispenjualan','penjualanresep_t.returresep_id',' t.jenispenjualan','t.penjualanresep_id','t.pasien_id','t.pegawai_id', 't.nama_pegawai','t.alamat_pasien','t.no_rekam_medik','t.namadepan','t.nama_pasien','t.nama_bin','t.tanggal_lahir',
                                          't.noresep','t.totharganetto','t.totalhargajual','t.instalasiasal_nama','t.ruanganasal_nama','t.reseptur_id',
                                          't.tglpenjualan','(pendaftaran_t.umur) as umur','t.jeniskelamin','t.tglresep, t.create_ruangan');
                $criteria->group = 't.pendaftaran_id, penjualanresep_t.jenispenjualan, penjualanresep_t.returresep_id, t.jenispenjualan, t.no_rekam_medik, t.namadepan, t.nama_pasien, t.nama_bin, t.tanggal_lahir, t.noresep, t.totharganetto,
                                    t.totalhargajual, t.instalasiasal_nama, t.ruanganasal_nama, t.penjualanresep_id, t.reseptur_id, t.pegawai_id, t.nama_pegawai, t.tglpenjualan, t.pasien_id,pendaftaran_t.umur, t.jeniskelamin, t.alamat_pasien, t.tglresep, t.create_ruangan';
                $criteria->order = 't.reseptur_id DESC';
                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id';
                $criteria->addBetweenCondition('t.tglpenjualan',  $this->tglAwal, $this->tglAkhir);
		if(empty($this->jenispenjualan))
//                   $criteria->addInCondition ('LOWER(t.jenispenjualan)', array('penjualan resep', 'penjualan resep luar', 'penjualan bebas'));
                //filter berdasarkan beda jenis penjualan
                   $criteria->addInCondition ('LOWER(t.jenispenjualan)', array('penjualan resep luar', 'penjualan bebas'));
                else
                    $criteria->compare('LOWER(t.jenispenjualan)',strtolower($this->jenispenjualan),true);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
		$criteria->compare('t.totharganetto',$this->totharganetto);
		$criteria->compare('t.totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
		$criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('t.reseptur_id',$this->reseptur_id);
		$criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('t.ruangan_id',  Yii::app()->user->getState('ruangan_id'));
//                $criteria->addCondition('penjualanresep_t.returresep_id IS NULL'); // DI NONAKTIFKAN KARENA ADA KASUS RETUR SEBAGIAN OBAT 
//                $criteria->addCondition('penjualanresep_t.returresep_id is null AND t.jenispenjualan = \'PENJUALAN RESEP\'');
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        /**
         * searchInfoResepPasien digunakan untuk menampilkan semua resep pasien RS
         */
        public function searchInfoResepPasien()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->select = array(
			't.pendaftaran_id',
			'penjualanresep_t.jenispenjualan',
			'penjualanresep_t.returresep_id',
			't.jenispenjualan','t.penjualanresep_id','t.pasien_id','t.pegawai_id', 't.nama_pegawai','t.alamat_pasien','t.no_rekam_medik','t.namadepan','t.nama_pasien','t.nama_bin','t.tanggal_lahir',
			't.noresep','t.totharganetto','t.totalhargajual','t.instalasiasal_nama','t.ruanganasal_nama','t.reseptur_id',
			't.tglpenjualan','(pendaftaran_t.umur) as umur','t.jeniskelamin','t.tglresep, t.create_ruangan'
		);
		$criteria->group = '
			t.pendaftaran_id, penjualanresep_t.jenispenjualan, penjualanresep_t.returresep_id, t.jenispenjualan, t.no_rekam_medik, t.namadepan, t.nama_pasien, t.nama_bin, t.tanggal_lahir, t.noresep, t.totharganetto,
			t.totalhargajual, t.instalasiasal_nama, t.ruanganasal_nama, t.penjualanresep_id, t.reseptur_id, t.pegawai_id, t.nama_pegawai, t.tglpenjualan, t.pasien_id,pendaftaran_t.umur, t.jeniskelamin, t.alamat_pasien, t.tglresep, t.create_ruangan
		';
		$criteria->order = 't.reseptur_id DESC';
		$criteria->join = '
			INNER JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id
			LEFT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id
		';
		$criteria->addBetweenCondition('t.tglpenjualan',  $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(t.jenispenjualan)',strtolower('PENJUALAN RESEP'),true);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
		$criteria->compare('t.totharganetto',$this->totharganetto);
		$criteria->compare('t.totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
		$criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('t.reseptur_id',$this->reseptur_id);
		$criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('t.ruangan_id',$this->ruanganasalobat);
//                $criteria->addCondition('penjualanresep_t.returresep_id IS NULL'); // DI NONAKTIFKAN KARENA ADA KASUS RETUR SEBAGIAN OBAT 
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
         public function searchInfoJualDokter()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $jenisPenjualan = "PENJUALAN DOKTER";
                $criteria->select = array('t.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id, t.pendaftaran_id, penjualanresep_t.jenispenjualan','penjualanresep_t.returresep_id',' t.jenispenjualan','t.penjualanresep_id','t.pasien_id','t.pegawai_id', 't.nama_pegawai','t.alamat_pasien','t.no_rekam_medik','t.namadepan','t.nama_pasien','t.nama_bin','t.tanggal_lahir',
                                          't.noresep','t.totharganetto','t.totalhargajual','t.instalasiasal_nama','t.ruanganasal_nama','t.reseptur_id',
                                          't.tglpenjualan','(pendaftaran_t.umur) as umur','t.jeniskelamin','t.tglresep');
                $criteria->group = 't.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id, t.pendaftaran_id, penjualanresep_t.jenispenjualan, penjualanresep_t.returresep_id, t.jenispenjualan, t.no_rekam_medik, t.namadepan, t.nama_pasien, t.nama_bin, t.tanggal_lahir, t.noresep, t.totharganetto,
                                    t.totalhargajual, t.instalasiasal_nama, t.ruanganasal_nama, t.penjualanresep_id, t.reseptur_id, t.pegawai_id, t.nama_pegawai, t.tglpenjualan, t.pasien_id,pendaftaran_t.umur, t.jeniskelamin, t.alamat_pasien, t.tglresep';
                $criteria->order = 't.reseptur_id DESC';
                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id';
                $criteria->addBetweenCondition('t.tglpenjualan',  $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(t.jenispenjualan)',strtolower($jenisPenjualan),true);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
		$criteria->compare('t.totharganetto',$this->totharganetto);
		$criteria->compare('t.totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
		$criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('t.reseptur_id',$this->reseptur_id);
		$criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);   
//                $criteria->addCondition('penjualanresep_t.returresep_id is null'); // DI NONAKTIFKAN KARENA ADA KASUS RETUR SEBAGIAN OBAT 
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public function searchInfoJualKaryawan()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $jenisPenjualan = "PENJUALAN KARYAWAN";
                $criteria->select = array('t.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id, t.pendaftaran_id, penjualanresep_t.jenispenjualan','penjualanresep_t.returresep_id',' t.jenispenjualan','t.penjualanresep_id','t.pasien_id','t.pegawai_id', 't.nama_pegawai','t.alamat_pasien','t.no_rekam_medik','t.namadepan','t.nama_pasien','t.nama_bin','t.tanggal_lahir',
                                          't.noresep','t.totharganetto','t.totalhargajual','t.instalasiasal_nama','t.ruanganasal_nama','t.reseptur_id',
                                          't.tglpenjualan','(pendaftaran_t.umur) as umur','t.jeniskelamin','t.tglresep');
                $criteria->group = 't.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id,t.pendaftaran_id, penjualanresep_t.jenispenjualan, penjualanresep_t.returresep_id, t.jenispenjualan, t.no_rekam_medik, t.namadepan, t.nama_pasien, t.nama_bin, t.tanggal_lahir, t.noresep, t.totharganetto,
                                    t.totalhargajual, t.instalasiasal_nama, t.ruanganasal_nama, t.penjualanresep_id, t.reseptur_id,t.pegawai_id, t.nama_pegawai, t.tglpenjualan, t.pasien_id,pendaftaran_t.umur, t.jeniskelamin, t.alamat_pasien, t.tglresep';
                $criteria->order = 't.reseptur_id DESC';
                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id';
                $criteria->addBetweenCondition('t.tglpenjualan',  $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(t.jenispenjualan)',strtolower($jenisPenjualan),true);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
		$criteria->compare('t.totharganetto',$this->totharganetto);
		$criteria->compare('t.totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
		$criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('t.reseptur_id',$this->reseptur_id);
		$criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);   
//                $criteria->addCondition('penjualanresep_t.returresep_id is null'); // DI NONAKTIFKAN KARENA ADA KASUS RETUR SEBAGIAN OBAT 
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public function searchInfoJualUnit()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $jenisPenjualan = "PENJUALAN UNIT";
                $criteria->select = array('t.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id,  penjualanresep_t.pasieninstalasiunit_id,t.pendaftaran_id, penjualanresep_t.jenispenjualan','penjualanresep_t.returresep_id',' t.jenispenjualan','t.penjualanresep_id','t.pasien_id','t.pegawai_id', 't.nama_pegawai','t.alamat_pasien','t.no_rekam_medik','t.namadepan','t.nama_pasien','t.nama_bin','t.tanggal_lahir',
                                          't.noresep','t.totharganetto','t.totalhargajual','t.instalasiasal_nama','t.ruanganasal_nama','t.reseptur_id',
                                          't.tglpenjualan','(pendaftaran_t.umur) as umur','t.jeniskelamin','t.tglresep');
                $criteria->group = 't.pasienpegawai_id,  t.pasienprofilrs_id, t.pasieninstalasiunit_id, penjualanresep_t.pasieninstalasiunit_id,t.pendaftaran_id, penjualanresep_t.jenispenjualan, penjualanresep_t.returresep_id, t.jenispenjualan, t.no_rekam_medik, t.namadepan, t.nama_pasien, t.nama_bin, t.tanggal_lahir, t.noresep, t.totharganetto,
                                    t.totalhargajual, t.instalasiasal_nama, t.ruanganasal_nama, t.penjualanresep_id, t.reseptur_id, t.pegawai_id, t.nama_pegawai, t.tglpenjualan, t.pasien_id,pendaftaran_t.umur, t.jeniskelamin, t.alamat_pasien, t.tglresep';
                $criteria->order = 't.reseptur_id DESC';
                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN penjualanresep_t ON penjualanresep_t.penjualanresep_id = t.penjualanresep_id';
                $criteria->addBetweenCondition('t.tglpenjualan',  $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(t.jenispenjualan)',strtolower($jenisPenjualan),true);
		$criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(t.tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
		$criteria->compare('t.totharganetto',$this->totharganetto);
		$criteria->compare('t.totalhargajual',$this->totalhargajual);
		$criteria->compare('LOWER(t.instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
		$criteria->compare('LOWER(t.ruanganasal_nama)',strtolower($this->ruanganasal_nama),true);
		$criteria->compare('t.reseptur_id',$this->reseptur_id);
		$criteria->compare('t.pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);   
//                $criteria->addCondition('penjualanresep_t.returresep_id is null'); // DI NONAKTIFKAN KARENA ADA KASUS RETUR SEBAGIAN OBAT 
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        public function getNamaRuangan(){
            return RuanganM::model()->findByPk($this->create_ruangan)->ruangan_nama;
        }
        public function getNamaDokter($id = null){
            if(empty($id))
                $modDokter = DokterpegawaiV::model()->findByAttributes(array('pegawai_id' =>$this->pegawai_id));
            else
                $modDokter = DokterpegawaiV::model()->findByAttributes(array('pegawai_id' =>$id));
            return $modDokter->gelardepan." ".$modDokter->nama_pegawai.', '.$modDokter->gelarbelakang_nama;
        }
        public function getNamaInstalasi($id){
            $modInstalasi = InstalasiM::model()->findByAttributes(array('instalasi_id' =>$id));
            return $modInstalasi->instalasi_nama;
        }
        public function getNamaPegawai($id = null){
            if(empty($id))
                $modPegawai = PegawaikaryawanV::model()->findByAttributes(array('pegawai_id' =>$this->pegawai_id));
            else
                $modPegawai = PegawaikaryawanV::model()->findByAttributes(array('pegawai_id' =>$id));
            return $modPegawai->nomorindukpegawai." - ".$modPegawai->gelardepan." ".$modPegawai->nama_pegawai.', '.$modPegawai->gelarbelakang_nama;
        }
        
	public function getSudahBayar()
	{
		$result = ObatalkespasienT::model()->count('penjualanresep_id =:penjualanresep and (oasudahbayar_id is not null)', array(':penjualanresep'=>$this->penjualanresep_id));
		return (($result > 0) ? true : false);
	}
	
	public function getNomorResepSudahBayar()
	{
		$result = ObatalkespasienT::model()->count('penjualanresep_id =:penjualanresep and (oasudahbayar_id is not null or returresepdet_id is not null)', array(':penjualanresep'=>$this->penjualanresep_id));
		return (($result > 0) ? true : false);
	}
        
	public function getNoRekamMedisNoPendaftaran()
	{
		return $this->no_rekam_medik.' '.$this->no_pendaftaran;
	}
        
	public function getNamapasien()
	{
		return $this->namadepan.' '.$this->nama_pasien;
	}
        
	public function getInstalasiRuanganAsal()
	{
		return $this->instalasiasal_nama.' '.$this->ruanganasal_nama;
	}
	
	protected function afterFind()
	{
		foreach($this->metadata->tableSchema->columns as $columnName => $column){
			if(!strlen($this->$columnName)) continue;
			if($column->dbType == 'date'){
				$this->$columnName = Yii::app()->dateFormatter->formatDateTime(
					CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null
				);
			}elseif($column->dbType == 'timestamp without time zone'){
				$this->$columnName = Yii::app()->dateFormatter->formatDateTime(
					CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null)
				);
			}elseif ($column->dbType == 'double precision'){
				//                    $format = new CNumberFormatter('id');
				//                    $this->$columnName = $format->format('#,##0', $this->$columnName);
			}
		}
		return true;
	}

	public function getJumlahReturnPenjualan($id = null)
	{
		$criteria = new CDbCriteria;
		$criteria->select = array("COUNT(returresep_id) as jumlah_return");
		$criteria->group = "penjualanresep_id";
		$criteria->compare('penjualanresep_id', $id);
		$informasipenjualanresepV = FAReturresepT::model()->find($criteria);
		return $informasipenjualanresepV->jumlah_return;
	}
	
}