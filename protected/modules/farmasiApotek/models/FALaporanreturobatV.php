<?php
class FALaporanreturobatV extends LaporanreturobatV
{
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
        
        /**
     * data provider untuk table
     */
    public function searchTable(){
        $criteria = $this->functionCriteria(true);
        
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria, 
                      ));
    }
    /**
     * data provider untuk print
     */
    public function searchPrint(){
        $criteria = $this->functionCriteria(true);
        
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    /**
     * data provider untuk grafik
     */
    public function searchGrafik(){
        
        $criteria = $this->functionCriteria();
        $criteria2 = new MyCriteria();
        $criteria2->select = 'count(noreturresep) as jumlah';
        if (!empty($this->noreturresep))
		{
            $criteria2->select .= ', penjamin_nama as data'; 
            $criteria2->group = 'penjamin_nama';
        }else{
            $criteria2->select .= ', carabayar_nama as data'; 
            $criteria2->group = 'carabayar_nama';
        }
        $criteria2->mergeWith($criteria);
        return  new CActiveDataProvider($this, array(
			'criteria'=>$criteria2,
        ));
        
    }
    
    /**
     * method untuk criteria
     * @return CDbCriteria 
     */
    public function functionCriteria($params = null)
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

     $criteria=new CDbCriteria;

            if (isset($params)){
                $criteria->select = 'tglretur,jenispenjualan,noreturresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur,carabayar_nama, penjamin_nama';
                $criteria->group = 'tglretur,jenispenjualan,noreturresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur,carabayar_nama, penjamin_nama';
            }else{
                $criteria->select = 'obatalkes_nama, tglretur,jenispenjualan,noreturresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, carabayar_nama, penjamin_nama';
                $criteria->group = 'obatalkes_nama,tglretur,jenispenjualan,noreturresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur,carabayar_nama, penjamin_nama';
          }
          $criteria->addBetweenCondition('tglretur',$this->tglAwal,$this->tglAkhir);
       //   $criteria->addBetweenCondition('date(tglretur)',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
               $criteria->compare('no_rekam_medik',$this->no_rekam_medik);
                $criteria->compare('no_pendaftaran',$this->no_pendaftaran);
            $criteria->compare('carabayar_id',$this->carabayar_id);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);            
//            (!is_array($this->penjamin_id)) ? $this->penjamin_id = 0 : '' ;
        //    (empty($this->penjamin_id)) ? $this->penjamin_id = 0 : $this->penjamin_id;
            $criteria->compare('penjamin_id',$this->penjamin_id);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
         return $criteria;
    }
}