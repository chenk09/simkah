<?php
ini_set('memory_limit', '-1');
class FAHutangtitipanapotikV extends HutangtitipanapotikV
{
        public $tglAwal, $tglAkhir,$jenisobatalkes_id,$Bruto,$Kso;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function criteriaSearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$criteria=new CDbCriteria;
		// $criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal, $this->tglAkhir);
		$criteria->addBetweenCondition('tglpasienpulang',$this->tglAwal, $this->tglAkhir);
		$criteria->group ='tgl_pendaftaran,no_pendaftaran,pendaftaran_id,no_rekam_medik,pasien_id,namadepan,nama_pasien,instalasiasal_nama,ruanganasal_nama,kso';
		return $criteria;
	}

	public function searchLaporan(){
		$criteria = $this->criteriaSearch();
		$criteria->select=$criteria->group;
		$criteria->order = 'tgl_pendaftaran, ruanganasal_nama,  nama_pasien ';
//		$criteria->addCondition("jmlbayar_oa > 0"); //JIKA dinonaktifkan = Tetap di tampilkan bernilai 0 untuk koreksi data
		if(isset($_GET['caraPrint'])){
			$criteria->limit = -1;
			return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
					'pagination'=>false,
			));
		}else{
			return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
			));
		}
	}

	public function getSumJenisObat($kolom = "", $sum = false)
    {
		$hasil = 0;
		$format = new CustomFormat;
		if(isset($_GET['FAHutangtitipanapotikV']['tglAwal']) && isset($_GET['FAHutangtitipanapotikV']['tglAkhir'])){
			$this->tglAwal = $format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAwal']);
			$this->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FAHutangtitipanapotikV']['tglAkhir']);
		}else{
                        $this->tglAwal = $format->formatDateTimeMediumForDB(date("d M Y 00:00:00"));
                        $this->tglAkhir = $format->formatDateTimeMediumForDB(date("d M Y 23:59:59"));
		}

		$criteria = $this->criteriaSearch();
		if($sum == false){
			$criteria->addCondition("pendaftaran_id = ".$this->pendaftaran_id); //acuan setiap row di table
		}
		$criteria->select = "sum(jmlbayar_oa) as jmlbayar_oa, sum(netto) as netto, max(kso) as kso";

		if($kolom == 'Alkes'){
			$kolom = 'jmlbayar_oa';
			$criteria->addCondition('jenisobatalkes_id = 1' );
		}else if($kolom == 'Gas'){
			$kolom = 'jmlbayar_oa';
			$criteria->addCondition('jenisobatalkes_id = 11' );
		}else if($kolom == 'Obat'){
			$kolom = 'jmlbayar_oa';
			$criteria->addNotInCondition('jenisobatalkes_id', array('11','1'));
		}else if($kolom == 'Bruto'){
			$kolom = 'jmlbayar_oa';
		}else if($kolom == 'Netto'){
			$kolom = 'netto';
		}else if($kolom == 'Kso'){
			$kolom = 'kso';
		}
		if($sum){
			$hasils = $this->model()->findAll($criteria);
			foreach($hasils AS $row){
				if($kolom == 'kso'){ //khusus total kso saja
					$hasil += round($row->jmlbayar_oa * $row->$kolom);
				}else{
					$hasil += round($row->$kolom);
				}
			}
		}else{
			$hasil = $this->model()->find($criteria)->$kolom;
		}

		return $hasil;
    }
}
