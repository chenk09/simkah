<?php

class FAReturResepV extends ReturResepV
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function searchReturFarmasi()
	{
		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('tglretur', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('noreturresep',$this->noreturresep,true);
		$criteria->compare('jenispenjualan',$this->jenispenjualan,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('qty_retur',$this->qty_retur);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('totalretur',$this->totalretur);
		$criteria->compare('alasanretur',$this->alasanretur,true);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));		
	}
	
	public function searchReturFarmasiPrint()
	{
		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('tglretur', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('noreturresep',$this->noreturresep,true);
		$criteria->compare('jenispenjualan',$this->jenispenjualan,true);
		$criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
		$criteria->compare('qty_retur',$this->qty_retur);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('totalretur',$this->totalretur);
		$criteria->compare('alasanretur',$this->alasanretur,true);		
		$criteria->addBetweenCondition('tglretur', $this->tglAwal, $this->tglAkhir);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>false,
		));
	}

}