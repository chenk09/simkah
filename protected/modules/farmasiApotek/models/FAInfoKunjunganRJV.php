<?php

/**
 * This is the model class for table "infokunjungan_rj".
 *
 * The followings are the available columns in table 'infokunjungan_rj':
 * @property integer $pendaftaran_id
 * @property string $tgl_pendaftaran
 * @property string $no_pendaftaran
 * @property string $statusperiksa
 * @property string $statusmasuk
 * @property string $no_rekam_medik
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $alamat_pasien
 * @property string $kelompokumur_nama
 * @property string $ruangan_nama
 * @property string $penjamin_nama
 * @property string $nama_pegawai
 * @property string $jeniskasuspenyakit_nama
 * @property integer $rujukan_id
 */
class FAInfoKunjunganRJV extends InfokunjunganrjV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfokunjunganRj the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRJ()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		//$criteria->compare('LOWER(kelompokumur_nama)',strtolower($this->kelompokumur_nama),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
		//$criteria->compare('rujukan_id',$this->rujukan_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        //=== Start functions untuk dialogPasien ===
        
        public function getTanggungan(){
            $val = array();
            if(!empty($this->penjamin_id) && !empty($this->kelaspelayanan_id) && !empty($this->carabayar_id)){
                $criteria = new CDbCriteria();
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('carabayar_id',$this->carabayar_id);
                $tanggungan = TanggunganpenjaminM::model()->find($criteria);
                $val['makstanggpel'] = $tanggungan->makstanggpel;
                $val['subsidirumahsakitoa'] = $tanggungan->subsidirumahsakitoa;
                $val['subsidipemerintahoa'] = $tanggungan->subsidipemerintahoa;
                $val['subsidiasuransioa'] = $tanggungan->subsidiasuransioa;
                $val['iurbiayaoa'] = $tanggungan->iurbiayaoa;
            }
            return $val;
        }
        //Harus dipecah karena di CGridview tidak bisa menampilkan array dan class model hanya bisa string
        public function getMakstanggpel(){
            $value = $this->Tanggungan['makstanggpel'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidirumahsakitoa(){
            $value = $this->Tanggungan['subsidirumahsakitoa'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidipemerintahoa(){
            $value = $this->Tanggungan['subsidipemerintahoa'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidiasuransioa(){
            $value = $this->Tanggungan['subsidiasuransioa'];
            return empty($value) ? 0 : $value;
        }
        public function getIurbiayaoa(){
            $value = $this->Tanggungan['iurbiayaoa'];
            return empty($value) ? 0 : $value;
        }
        
        //=== End functions ===
}