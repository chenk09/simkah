<?php

class FAPenjualanResepT extends PenjualanresepT
{
    public $isNewRecord = true; //jika record baru = true
    public $noresep_depan;
    public $noresep_belakang = null;
    public $namaPegawai = null;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchTabelServices(){
            
            $criteria = new CDbCriteria();
            $criteria->with = array('pasien','pendaftaran');
            $criteria->addBetweenCondition('tglresep',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pasien.no_rekam_medik', $this->no_rekam_medik);
            $criteria->compare('pendaftaran.no_pendaftaran', $this->no_pendaftaran);
            $criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->addSearchCondition('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien));            
            $criteria->addCondition('totaltarifservice>0');
            $criteria->compare('t.ruangan_id', $this->ruangan_id);
            
            return  new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
            ));
        }
        
        public function searchPrintServices(){
            
            $criteria = new CDbCriteria();
            $criteria->with = array('pasien','pendaftaran');
            $criteria->addBetweenCondition('tglresep',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pasien.no_rekam_medik', $this->no_rekam_medik);
            $criteria->compare('pendaftaran.no_pendaftaran', $this->no_pendaftaran);
            $criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->addSearchCondition('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien));            
            $criteria->addCondition('totaltarifservice>0');
            
            return  new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
            ));
        }
        /**
         * searchPrintServices digunakan pada laporan Jasa Racikan & print
         * @return \CActiveDataProvider
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function searchPrintJasaRacikan(){
            $format = new CustomFormat;
            $criteria = new CDbCriteria();
            $this->tglAwal = $format->formatDateTimeMediumForDB($this->tglAwal);
            $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);
            $criteria->addBetweenCondition('tglresep',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pendaftaran.no_pendaftaran', $this->no_pendaftaran);
            $criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
//          BELUM BISA KARENA NoFaktur ADALAH FUNGSI>>  $criteria->compare('LOWER(NoFaktur)',strtolower($this->NoFaktur),true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->addSearchCondition('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien));            
            $criteria->addCondition('totaltarifservice>0');
            
            return  new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
            ));
        }
        /**
         * searchPrintJasaDokter digunakan pada laporan Jasa Dokter Resep & print
         * @return \CActiveDataProvider
         * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
         */
        public function searchPrintJasaDokter(){
            $format = new CustomFormat;
            $criteria = new CDbCriteria();
            $criteria->with = 'pegawai';
            $this->tglAwal = $format->formatDateTimeMediumForDB($this->tglAwal);
            $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);
            $criteria->addBetweenCondition('tglresep',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pendaftaran.no_pendaftaran', $this->no_pendaftaran);
            $criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->addSearchCondition('LOWER(pegawai.nama_pegawai)',  strtolower($this->namaPegawai));
            $criteria->addCondition('jasadokterresep>0');
            
            return  new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
            ));
        }
        
        public function searchGrafik(){
            
            $criteria = new CDbCriteria();
//            $criteria->with = array('pasien','pendaftaran');
            $criteria->join = 'JOIN pasien_m as pasien ON pasien.pasien_id=t.pasien_id 
                               JOIN pendaftaran_t as pendaftaran on pendaftaran.pendaftaran_id=t.pendaftaran_id 
                               JOIN ruangan_m as ruangan on ruangan.ruangan_id=t.ruangan_id';
            $criteria->addBetweenCondition('t.tglresep',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pasien.no_rekam_medik', $this->no_rekam_medik);
            $criteria->compare('pendaftaran.no_pendaftaran', $this->no_pendaftaran);
            $criteria->compare('t.penjualanresep_id',$this->penjualanresep_id);
            $criteria->compare('LOWER(t.noresep)',strtolower($this->noresep),true);
            $criteria->compare('t.pegawai_id',$this->pegawai_id);
            $criteria->addSearchCondition('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien));            
            $criteria->addCondition('totaltarifservice>0');
            $criteria->select = 'count(t.totaltarifservice) as jumlah, ruangan.ruangan_nama as data';
            $criteria->group = 'ruangan.ruangan_nama';
            
            return  new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                'pagination'=>false,
            ));
        }

        public function getProfilRs()
        {
            return ProfilrumahsakitM::model()->findAll();
        }
        
        //Menampilkan nama lengkap dokter + gelar
        public function getDokter(){
            $modDokter = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$this->pegawai_id));
            if(!empty($modDokter->nama_pegawai))
                return $modDokter->gelardepan." ".$modDokter->nama_pegawai.", ".$modDokter->gelarbelakang->gelarbelakang_nama;
            else
                return null;
        }
        
}