<?php

class FAInformasiinapsiappulangV extends InformasiinapsiappulangV {

    public $tglAwal;
    public $tglAkhir;

    public static function model($className=__CLASS__) {
        return parent::model($className);
    }

    public function searchKunjunganPasien()
    {
        $format = new CustomFormat();
        $this->tglAwal  = $format->formatDateTimeMediumForDB($this->tglAwal);
        $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);

        $criteria = new CDbCriteria;
        $criteria->compare('LOWER(t.no_pendaftaran)', strtolower($this->no_pendaftaran), true);
        $criteria->compare('LOWER(t.no_rekam_medik)', strtolower($this->no_rekam_medik), true);
        $criteria->compare('LOWER(t.nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('LOWER(t.ruangan_nama)', strtolower($this->ruangan_nama), true);
        $criteria->compare('LOWER(t.nama_pegawai)', strtolower($this->nama_pegawai), true);
        $criteria->compare('date(t.tgl_pendaftaran)',$this->tgl_pendaftaran);
        $criteria->compare('LOWER(t.jeniskasuspenyakit_nama)', strtolower($this->jeniskasuspenyakit_nama), true);
        $criteria->compare('LOWER(t.statusperiksa)', strtolower($this->statusperiksa), true);
        $criteria->compare('statusfarmasi',$this->statusfarmasi);
		$criteria->compare('kirim_farmasi',$this->kirim_farmasi);
        
        $criteria->addBetweenCondition('DATE(t.tgl_pendaftaran)', $this->tglAwal, $this->tglAkhir);
        $criteria->order = 't.tgl_pendaftaran DESC';

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }


}

?>
