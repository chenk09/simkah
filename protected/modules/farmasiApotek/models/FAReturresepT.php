<?php

class FAReturresepT extends ReturresepT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	public $jumlah_return;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function cariReturnObat()
	{
		$criteria = new CDbCriteria;
		//$criteria->join = "INNER JOIN returresepdet_t ON returresepdet_t.returresep_id = t.returresep_id";
		$criteria->order = "tglretur desc";
		$criteria->addCondition('tglretur BETWEEN \''. $this->tglAwal .'\' AND \''. $this->tglAkhir .'\'');
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	

}