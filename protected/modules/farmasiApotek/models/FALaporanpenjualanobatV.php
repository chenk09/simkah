<?php
ini_set('memory_limit', '-1');
class FALaporanpenjualanobatV extends LaporanpenjualanobatV {
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTabelKategoriDua() {
        $criteria = new CDbCriteria();

	$format = new CustomFormat();
	$this->tglAwal = $format->formatDateTimeMediumForDB(date("d M Y 00:00:00"));
	$this->tglAkhir = $format->formatDateTimeMediumForDB(date("d M Y 23:59:59"));

        if(isset($_GET['FALaporanpenjualanobatV']['tglAwal'])){
            $this->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
        }

        if(isset($_GET['FALaporanpenjualanobatV']['tglAkhir'])){
            $this->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
	}
	$criteria->addBetweenCondition('DATE(tglpenjualan)', $this->tglAwal,$this->tglAkhir);

        if(isset($_GET['FALaporanpenjualanobatV']['obatalkes_golongan'])){
		$criteria->compare('obatalkes_golongan',$_GET['FALaporanpenjualanobatV']['obatalkes_golongan']);
        }

        $criteria->order = 'obatalkes_kategori DESC';
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        ));
    }

    public function printTabelKategoriDua() {
        $criteria = new CDbCriteria();

        $format = new CustomFormat();
        $this->tglAwal = $format->formatDateTimeMediumForDB(date("d M Y 00:00:00"));
        $this->tglAkhir = $format->formatDateTimeMediumForDB(date("d M Y 23:59:59"));

        if(isset($_GET['FALaporanpenjualanobatV']['tglAwal'])){
            $this->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
        }

        if(isset($_GET['FALaporanpenjualanobatV']['tglAkhir'])){
            $this->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }
        $criteria->addBetweenCondition('DATE(tglpenjualan)', $this->tglAwal,$this->tglAkhir);

        if(isset($_GET['FALaporanpenjualanobatV']['obatalkes_golongan'])){
                $criteria->compare('obatalkes_golongan',$_GET['FALaporanpenjualanobatV']['obatalkes_golongan']);
        }

        $criteria->order = 'obatalkes_kategori DESC';
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
		'pagination'=>false,
        ));
    }


    public function searchTabelKategori() {
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('tglpenjualan', $this->tglAwal, $this->tglAkhir);
       // $criteria->compare('obatalkes_kategori',$this->obatalkes_kategori);
        $criteria->order = 'obatalkes_kategori DESC';
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
        ));
    }
    
    public function searchPrintKategori(){
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('obatalkes_kategori',$this->obatalkes_kategori);
        $criteria->order = 'obatalkes_kategori DESC';
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
        ));
    }
    
    public function searchGrafikKategori(){
        $criteria = new CDbCriteria();
        $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('obatalkes_kategori',$this->obatalkes_kategori);
        $criteria->select = 'count(noresep) as jumlah, obatalkes_kategori as data';
        $criteria->group = 'obatalkes_kategori';
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
        ));
    }
    
    /**
     * data provider untuk table
     */
    public function searchTable(){
        $criteria = $this->functionCriteria(true);
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    /**
     * data provider untuk print
     */
    public function searchPrint(){
        $criteria = $this->functionCriteria(true);
        
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    /**
     * data provider untuk grafik
     */
    public function searchGrafik(){
        
        $criteria = $this->functionCriteria();

        $criteria2 = new MyCriteria();
        $criteria2->select = 'count(noresep) as jumlah';
        if (!empty($this->carabayar_nama)){
            $criteria2->select .= ', penjamin_nama as data'; 
            $criteria2->group = 'penjamin_nama';
        }
        else if (!empty($this->instalasiasal_nama)){
            $criteria2->select .= ', ruanganasal_nama as data'; 
            $criteria2->group = 'ruanganasal_nama';
        }
        else{
            $criteria2->select .= ', carabayar_nama as data'; 
            $criteria2->group = 'carabayar_nama';
        }
        
        
        $criteria2->mergeWith($criteria);
        
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria2,
        ));
        
    }
    
    /**
     * method untuk criteria
     * @return CDbCriteria 
     */
    public function functionCriteria($params = null)
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            if (isset($params)){
                $criteria->select = 'tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, sum(oasudahbayar_id) as oasudahbayar_id';
                $criteria->group = 'tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai';
            }else{
//				$criteria->select = array('tglpenjualan', 'tglresep','jenispenjualan','noresep','no_rekam_medik','no_pendaftaran', 'nama_pasien', 'nama_bin', 'jeniskelamin', 'umur', 'totalhargajual', 'totaltarifservice','biayaadministrasi', 'subsidiasuransi', 'subsidipemerintah', 'subsidirs', 'iurbiaya','carabayar_nama', 'penjamin_nama', 'instalasiasal_nama', 'ruanganasal_nama');
                $criteria->select = 'obatalkes_nama, tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
                $criteria->group = 'obatalkes_nama, tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
            }
            $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
            
			if(!empty($this->carabayar_id))
			{
                            echo "a";
				$criteria->compare('carabayar_id',$this->carabayar_id);
			}
//			$this->penjamin_id = (!empty($this->penjamin_id) ? $this->penjamin_id : 0);
//			if(is_array($this->penjamin_id))
//			{
//				$criteria->addInCondition('penjamin_id', $this->penjamin_id);
//			}
//			
			if(is_array($this->ruanganasal_nama))
			{
				$criteria->addInCondition('ruanganasal_nama', $this->ruanganasal_nama);
			}
//			
			if(is_array($this->oasudahbayar_id) && count($this->oasudahbayar_id) == 1)
			{
				if($this->oasudahbayar_id[0] == 1)
				{
					$criteria->addCondition('oasudahbayar_id IS NOT NULL');
				}else{
					$criteria->addCondition('oasudahbayar_id IS NULL');
				}
			}
			
			/*
			$criteria->addCondition('ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);            
//            (!is_array($this->penjamin_id)) ? $this->penjamin_id = 0 : '' ;

            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->addCondition('ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
            $criteria->compare('LOWER(instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
            (!is_array($this->ruanganasal_nama)) ? $this->ruanganasal_nama = 0 : '' ;
            $this->ruanganasal_nama = (is_array($this->ruanganasal_nama) ? array_map('strtolower', $this->ruanganasal_nama) : strtolower($this->ruanganasal_nama));;
            $criteria->compare('LOWER(ruanganasal_nama)',  $this->ruanganasal_nama);

			*/
            return $criteria;
    }
    
    public function searchData()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->compare('noresep',$this->noresep);
			if(!empty($this->pegawai_id)){
				$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
			}
			if(!empty($this->obatalkes_id)){
				$criteria->addCondition('obatalkes_id = '.$this->obatalkes_id);
			}
            $criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
            $criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
            $criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
            $criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
            $criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
			if(!empty($this->satuankecil_id)){
				$criteria->addCondition('satuankecil_id = '.$this->satuankecil_id);
			}
            $criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
			if(!empty($this->jenisobatalkes_id)){
				$criteria->addCondition('jenisobatalkes_id = '.$this->jenisobatalkes_id);
			}
            $criteria->compare('LOWER(jenisobatalkes_nama)',strtolower($this->jenisobatalkes_nama),true);
			if(!empty($this->sumberdana_id)){
				$criteria->addCondition('sumberdana_id = '.$this->sumberdana_id);
			}
            $criteria->compare('LOWER(sumberdana_nama)',strtolower($this->sumberdana_nama),true);
            $criteria->compare('qty_oa',$this->qty_oa);
            $criteria->compare('hargasatuan_oa',$this->hargasatuan_oa);
            $criteria->compare('hargajual_oa',$this->hargajual_oa);
			if(!empty($this->oasudahbayar_id)){
				$criteria->addCondition('oasudahbayar_id = '.$this->oasudahbayar_id);
			}
			if(!empty($this->racikan_id)){
				$criteria->addCondition('racikan_id = '.$this->racikan_id);
			}
            $criteria->compare('LOWER(r)',strtolower($this->r),true);
            $criteria->compare('rke',$this->rke);
            $criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }

    public function getDokter(){
            $modDokter = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$this->pegawai_id));
            if(!empty($modDokter->nama_pegawai))
                return $modDokter->gelardepan." ".$modDokter->nama_pegawai.", ".$modDokter->gelarbelakang->gelarbelakang_nama;
            else
                return null;
        }

    
    public function getNamaDokter($id = null){
            if($id)
                $modDokter = DokterpegawaiV::model()->findByAttributes(array('pegawai_id' =>$this->pegawai_id));
            else
                $modDokter = DokterpegawaiV::model()->findByAttributes(array('pegawai_id' =>$id));
            return $modDokter->gelardepan." ".$modDokter->nama_pegawai.', '.$modDokter->gelarbelakang_nama;
        }
    
    /**
     * Method untuk mendapatkan nama Model
     * @return String 
     */
    public function getNamaModel(){
        return __CLASS__;
    }
    
    public function getSubTotal(){
        return $this->qty_oa*$this->hargasatuan_oa;
    }
    
    /**
     * data provider untuk table penjualan obat
     */
    public function criteriaCari()
    {
        $criteria = new CDbCriteria;
        $format = new CustomFormat();
        $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);

        if(!empty($this->carabayar_id))
        {
            $criteria->compare('carabayar_id',$this->carabayar_id);
        }
        $this->penjamin_id = (!empty($this->penjamin_id) ? $this->penjamin_id : 0);
        if(is_array($this->penjamin_id))
        {
                $criteria->addInCondition('penjamin_id', $this->penjamin_id);
        }		
        if(is_array($this->ruanganasal_nama))
        {
            $criteria->addInCondition('ruanganasal_nama', $this->ruanganasal_nama);
        }		
        if(is_array($this->oasudahbayar_id) && count($this->oasudahbayar_id) == 1)
        {
            if($this->oasudahbayar_id[0] == 1){
                $criteria->addCondition('oasudahbayar_id IS NOT NULL');
            }else{
                $criteria->addCondition('oasudahbayar_id IS NULL');
            }
        }

        return $criteria;
    }
    
	public function searchPenjualanObat($limit = true){
        $criteria = $this->criteriaCari();
        $pagination = array('pageSize'=>10);
        if(!$limit){
                $criteria->limit = -1;
                $pagination = false;
            }
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>$pagination,
            ));
    }

    public function getOaJual($kolom = "", $sum = false)
    {
        $criteria = $this->criteriaCari();
        $criteria->select = "sum(hargajual_oa) AS hargajual_oa, sum(hargasatuanjual_oa *qty_oa) AS hargasatuanjual_oa";
        if($sum){
                $hasils = $this->model()->findAll($criteria);
                foreach($hasils AS $i => $value){
                    $hasil += $value->$kolom;
                }
            }else{
                $criteria->addCondition("jenispenjualan = '".$this->jenispenjualan."'");
                $hasil = $this->model()->find($criteria)->$kolom;
            }
            return $hasil;
    }  


    /**
     * data provider untuk print penjualan obat
     */
    public function searchPrintPenjualanObat(){
        $criteria = new CDbCriteria;
        
        $format = new CustomFormat();
        if (isset($_GET['FALaporanpenjualanobatV']['tglAwal'])){
            $this->tglAwal = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAwal']);
        }
        if (isset($_GET['FALaporanpenjualanobatV']['tglAkhir'])){
            $this->tglAkhir = $format->formatDateTimeMediumForDB($_GET['FALaporanpenjualanobatV']['tglAkhir']);
        }
        if (isset($_GET['FALaporanpenjualanobatV']['jenispenjualan'])){
            $this->jenispenjualan = $_GET['FALaporanpenjualanobatV']['jenispenjualan'];
        }
        $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('LOWER(jenispenjualan)',  strtolower($this->jenispenjualan), true);
        // $pagination = array('pageSize'=>20);
        // if(!$limit){
        //         $criteria->limit = -1;
        //         $pagination = false;
        //     }
        
        
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    
    public function getPenjaminItems($carabayar_id=null)
    {
        if(!empty($carabayar_id))
                return PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id'=>$carabayar_id,'penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
        else
                return array();
                //return PenjaminpasienM::model()->findAllByAttributes(array('penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
    }
    
    /**
     * method untuk criteria
     * @return CDbCriteria 
     */
    public function searchCriteria()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.
            $criteria=new CDbCriteria;
            $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('LOWER(noresep)',strtolower($this->noresep),true);
            $criteria->compare('LOWER(jenispenjualan)',strtolower($this->jenispenjualan),true);
			if(!empty($this->carabayar_id)){
				$criteria->addCondition('carabayar_id = '.$this->carabayar_id);
			}
			if(!empty($this->pegawai_id)){
				$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
			}
              $criteria->compare('nama_pegawai',$this->nama_pegawai);
            $criteria->addSearchCondition('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien));
            return $criteria;
    }
    public function searchTableJasaRacikan($limit = true){
        $format = new CustomFormat;
        $criteria = $this->searchCriteria();
        $criteria->select = 'tglpenjualan,tglresep,jenispenjualan,noresep,totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya';
        $criteria->group = $criteria->select;
        $pagination = array('pageSize'=>10);
        if(!$limit){
            $criteria->limit = -1;
            $pagination = false;
        }
        $criteria->addCondition('totaltarifservice > 0');
        $criteria->order = "tglresep";
        return  new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>$pagination,
        ));
    }
    
    
    /*---*/
}
