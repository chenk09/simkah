<?php

class FAResepturT extends ResepturT
{
    public $noresep_depan;
    public $noresep_belakang;
    
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	 
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	//Menampilkan nama lengkap dokter + gelar
	public function getDokter(){
		$modDokter = PegawaiM::model()->findByAttributes(array('pegawai_id'=>$this->pegawai_id));
		
		if(!empty($modDokter->nama_pegawai))
			return $modDokter->gelardepan." ".$modDokter->nama_pegawai.", ".$modDokter->gelarbelakang->gelarbelakang_nama;
		else return null;
	}
}