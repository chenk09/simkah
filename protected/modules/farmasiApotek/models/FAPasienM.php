<?php
Yii::import('farmasiApotek.models.FAInfoKunjunganRDV');
Yii::import('farmasiApotek.models.FAInfoKunjunganRJV');
Yii::import('farmasiApotek.models.FAInfopasienmasukkamarV');
class FAPasienM extends PasienM
{
        public $tgl_pendaftaran_cari, $instalasi_nama, $idInstalasi, $ruangan_nama, $carabayar_nama; //untuk pencarian & filtering
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AnamnesaT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        /**
         * searchPasienApotek menampilkan pasien apotek saja
         * @return \CActiveDataProvider
         */
        public function searchPasienApotek()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('pekerjaan_id',$this->pekerjaan_id);
		$criteria->compare('pendidikan_id',$this->pendidikan_id);
		$criteria->compare('t.propinsi_id',$this->propinsi_id);
		$criteria->compare('t.kabupaten_id',$this->kabupaten_id);
		$criteria->compare('t.kecamatan_id',$this->kecamatan_id);
		$criteria->compare('t.kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(propinsi.propinsi_nama)',strtolower($this->propinsiNama),true);
		$criteria->compare('LOWER(kabupaten.kabupaten_nama)',strtolower($this->kabupatenNama),true);
		$criteria->compare('LOWER(kecamatan.kecamatan_nama)',strtolower($this->kecamatanNama),true);
		$criteria->compare('LOWER(kelurahan.kelurahan_nama)',strtolower($this->kelurahanNama),true);
		$criteria->compare('suku_id',$this->suku_id);
		$criteria->compare('profilrs_id',$this->profilrs_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(tgl_rekam_medik)',strtolower($this->tgl_rekam_medik),true);
		$criteria->compare('LOWER(jenisidentitas)',strtolower($this->jenisidentitas),true);
		$criteria->compare('LOWER(no_identitas_pasien)',strtolower($this->no_identitas_pasien),true);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('kelompokumur_id',$this->kelompokumur_id);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('rt',$this->rt);
		$criteria->compare('rw',$this->rw);
		$criteria->compare('LOWER(statusperkawinan)',strtolower($this->statusperkawinan),true);
		$criteria->compare('LOWER(agama)',strtolower($this->agama),true);
		$criteria->compare('LOWER(golongandarah)',strtolower($this->golongandarah),true);
		$criteria->compare('LOWER(rhesus)',strtolower($this->rhesus),true);
		$criteria->compare('anakke',$this->anakke);
		$criteria->compare('jumlah_bersaudara',$this->jumlah_bersaudara);
		$criteria->compare('LOWER(no_telepon_pasien)',strtolower($this->no_telepon_pasien),true);
		$criteria->compare('LOWER(no_mobile_pasien)',strtolower($this->no_mobile_pasien),true);
		$criteria->compare('LOWER(warga_negara)',strtolower($this->warga_negara),true);
		$criteria->compare('LOWER(alamatemail)',strtolower($this->alamatemail),true);
		$criteria->compare('LOWER(statusrekammedis)',strtolower(Params::statusRM(1)),true);
		$criteria->compare('LOWER(tgl_meninggal)',strtolower($this->tgl_meninggal),true);
		$criteria->compare('t.ispasienluar',1);
                //Jika di filter berdasarkan No. Lab dan Rad
                $criteria->addCondition('create_ruangan IN ('.Params::RUANGAN_ID_APOTEK_RJ.')');
                $criteria->with = array('propinsi','kabupaten','kecamatan','kelurahan');
                $criteria->order = 'pasien_id DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        /**
         * searchPasienRumahsakitV menampilkan pasien rumah sakit dari view database
         * @return \CActiveDataProvider
         */
        public function searchPasienRumahsakitV(){
            $format = new CustomFormat;
            $model = null;
            $criteria = new CDbCriteria();
            $criteria->compare('LOWER(no_pendaftaran)', strtolower($this->no_pendaftaran), true);
            if(!empty($this->tgl_pendaftaran_cari)){
                $this->tgl_pendaftaran_cari = $format->formatDateMediumForDB($this->tgl_pendaftaran_cari);
                $criteria->addBetweenCondition('tgl_pendaftaran', $this->tgl_pendaftaran_cari." 00:00:00", $this->tgl_pendaftaran_cari." 23:59:59");
            }
            $criteria->compare('LOWER(no_rekam_medik)', strtolower($this->no_rekam_medik), true);
            $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
            $criteria->compare('instalasi_id', $this->idInstalasi);
//            $criteria->compare('LOWER(instalasi_nama)', strtolower($this->namaInstalasi), true);
            $criteria->compare('LOWER(ruangan_nama)', strtolower($this->ruangan_nama), true);
            $criteria->compare('LOWER(carabayar_nama)', strtolower($this->carabayar_nama), true);
            $criteria->limit = 50;
            $criteria->order = 'tgl_pendaftaran DESC';
            //kembalikan format
            $this->tgl_pendaftaran_cari = empty($this->tgl_pendaftaran_cari) ? null : date('d M Y',strtotime($this->tgl_pendaftaran_cari));
            if($this->idInstalasi == Params::INSTALASI_ID_RD){
//                $model = new FAInfoKunjunganRDV;
                /** diupdate tgl 8 FEB 2014 **/
                $model = new FAInforawatdaruratapotikblbayarV;
            }else if($this->idInstalasi == Params::INSTALASI_ID_RJ){
//                $model = new FAInfoKunjunganRJV;
                /** diupdate tgl 8 FEB 2014 **/
                $model = new FAInforawatjalanapotikblmbayarV;
            }else if($this->idInstalasi == Params::INSTALASI_ID_RI){
//                $model = new FAInfopasienmasukkamarV;
                /** diupdate tgl 8 FEB 2014 **/
                $model = new FAInforawatinapapotikblmbayarV;
            }else{
//                $model = new FAInfopasienmasukkamarV; //default
                /** diupdate tgl 8 FEB 2014 **/
                $model = new FAInforawatinapapotikblmbayarV; //default
            }
            return new CActiveDataProvider($model, array(
                        'criteria'=>$criteria,
                ));
        }
}