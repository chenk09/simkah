<?php
class FAInforawatinapapotikblmbayarV extends InforawatinapapotikblmbayarV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InforawatinapapotikblmbayarV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRI()
	{

		$criteria=new CDbCriteria;
                
                $criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(jeniskelamin)',strtolower($this->jeniskelamin),true);
		$criteria->compare('LOWER(tempat_lahir)',strtolower($this->tempat_lahir),true);
		$criteria->compare('LOWER(tanggal_lahir)',strtolower($this->tanggal_lahir),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                $criteria->addCondition('tgl_pendaftaran BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('caramasuk_id',$this->caramasuk_id);
		$criteria->compare('LOWER(caramasuk_nama)',strtolower($this->caramasuk_nama),true);
		$criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
		$criteria->compare('LOWER(kelaspelayanan_nama)',strtolower($this->kelaspelayanan_nama),true);
		$criteria->compare('kamarruangan_id',$this->kamarruangan_id);
		$criteria->compare('LOWER(tgladmisi)',strtolower($this->tgladmisi),true);
		$criteria->compare('LOWER(kamarruangan_nokamar)',strtolower($this->kamarruangan_nokamar),true);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        //=== Start functions untuk dialogPasien ===
        
        public function getTanggungan(){
            $val = array();
            if(!empty($this->penjamin_id) && !empty($this->kelaspelayanan_id) && !empty($this->carabayar_id)){
                $criteria = new CDbCriteria();
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
                $criteria->compare('carabayar_id',$this->carabayar_id);
                $tanggungan = TanggunganpenjaminM::model()->find($criteria);
                $val['makstanggpel'] = $tanggungan->makstanggpel;
                $val['subsidirumahsakitoa'] = $tanggungan->subsidirumahsakitoa;
                $val['subsidipemerintahoa'] = $tanggungan->subsidipemerintahoa;
                $val['subsidiasuransioa'] = $tanggungan->subsidiasuransioa;
                $val['iurbiayaoa'] = $tanggungan->iurbiayaoa;
            }
            return $val;
        }
        //Harus dipecah karena di CGridview tidak bisa menampilkan array dan class model hanya bisa string
        public function getMakstanggpel(){
            $value = $this->Tanggungan['makstanggpel'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidirumahsakitoa(){
            $value = $this->Tanggungan['subsidirumahsakitoa'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidipemerintahoa(){
            $value = $this->Tanggungan['subsidipemerintahoa'];
            return empty($value) ? 0 : $value;
        }
        public function getSubsidiasuransioa(){
            $value = $this->Tanggungan['subsidiasuransioa'];
            return empty($value) ? 0 : $value;
        }
        public function getIurbiayaoa(){
            $value = $this->Tanggungan['iurbiayaoa'];
            return empty($value) ? 0 : $value;
        }
        
        //=== End functions ===
}
?>