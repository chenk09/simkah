<?php

class FALaporanPendAlkesV extends LaporanPendAlkesV {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function searchTablePendapatanObatAlkes() {
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('tglpenjualan', $this->tglAwal, $this->tglAkhir);
        $criteria->addInCondition('jenisobatalkes_id', $this->jenisobatalkes_id);

        // $criteria->compare('tglpenjualan',$this->tglpenjualan,true);
        // $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
        /*
          $criteria->compare('penjualanresep_id',$this->penjualanresep_id);
          $criteria->compare('obatalkes_id',$this->obatalkes_id);
          $criteria->compare('obatalkes_kode',$this->obatalkes_kode,true);
          $criteria->compare('obatalkes_nama',$this->obatalkes_nama,true);
          $criteria->compare('obatalkes_golongan',$this->obatalkes_golongan,true);
          $criteria->compare('obatalkes_kategori',$this->obatalkes_kategori,true);
          $criteria->compare('obatalkes_kadarobat',$this->obatalkes_kadarobat,true);
          $criteria->compare('jenisobatalkes_nama',$this->jenisobatalkes_nama,true);
          $criteria->compare('ruangan_id',$this->ruangan_id);
          $criteria->compare('ruangan_nama',$this->ruangan_nama,true);
          $criteria->compare('noresep',$this->noresep,true);
          $criteria->compare('jenispenjualan',$this->jenispenjualan,true);
          $criteria->compare('oasudahbayar_id',$this->oasudahbayar_id);
          $criteria->compare('returresep_id',$this->returresep_id);
          $criteria->compare('qty_oa',$this->qty_oa);
          $criteria->compare('harga_satuan',$this->harga_satuan);
          $criteria->compare('ppn_persen',$this->ppn_persen);
          $criteria->compare('hpp',$this->hpp);
          $criteria->compare('bruto_jual',$this->bruto_jual);
          $criteria->compare('netto_jual',$this->netto_jual);
          $criteria->compare('discount_jual',$this->discount_jual);
          $criteria->compare('qty_retur',$this->qty_retur);
          $criteria->compare('qty_total',$this->qty_total);
          $criteria->compare('total_bruto',$this->total_bruto);
          $criteria->compare('total_netto',$this->total_netto);
          $criteria->compare('total_hpp',$this->total_hpp);
         */
        if ($this->print) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false
            ));
        } else {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
            ));
        }
    }

    public function searchGroupByJenisObat() {
        $sql = Yii::app()->db->createCommand();
        $sql->select("jenisobatalkes_m.jenisobatalkes_id,
      jenisobatalkes_m.jenisobatalkes_nama,
	  sum(obatalkespasien_t.hargasatuanjual_oa) AS hargasatuan_jual,
      sum(obatalkespasien_t.qty_oa) AS qty_jual,
      sum(obatalkespasien_t.hargajual_oa) AS bruto_jual,
      sum(obatalkespasien_t.harganetto_oa) AS netto_jual,
      sum((obatalkespasien_t.qty_oa * obatalkes_m.hpp)) AS hpp_jual,
      sum(obatalkespasien_t.discount) AS discount_jual,
      CASE WHEN (sum(returresepdet_t.qty_retur) = (0)::double precision) THEN (0)::double precision ELSE sum(returresepdet_t.qty_retur) END AS qty_retur,
      CASE WHEN (sum(returresepdet_t.qty_retur) = (0)::double precision) THEN (0)::double precision ELSE sum((returresepdet_t.qty_retur * obatalkespasien_t.hargasatuan_oa)) END AS total_ret_bruto,
      CASE WHEN (sum(returresepdet_t.qty_retur) = (0)::double precision) THEN (0)::double precision ELSE sum((returresepdet_t.qty_retur * obatalkespasien_t.harganetto_oa)) END AS total_ret_netto,
      CASE WHEN (sum(returresepdet_t.qty_retur) = (0)::double precision) THEN (0)::double precision ELSE sum((returresepdet_t.qty_retur * obatalkes_m.hpp)) END AS total_ret_hpp,
      sum((obatalkespasien_t.qty_oa * obatalkespasien_t.hargasatuan_oa)) AS total_bruto,
      sum((obatalkespasien_t.qty_oa * obatalkespasien_t.harganetto_oa)) AS total_netto,
      sum((obatalkespasien_t.qty_oa * obatalkes_m.hpp)) AS total_hpp, sum((obatalkespasien_t.qty_oa * obatalkespasien_t.hargasatuanjual_oa)) AS total_hja");
        $sql->from("penjualanresep_t");
        $sql->join("obatalkespasien_t", "penjualanresep_t.penjualanresep_id = obatalkespasien_t.penjualanresep_id");
        $sql->join("obatalkes_m", "obatalkespasien_t.obatalkes_id = obatalkes_m.obatalkes_id");
        $sql->join("jenisobatalkes_m", "obatalkes_m.jenisobatalkes_id = jenisobatalkes_m.jenisobatalkes_id");
        $sql->join("ruangan_m", "penjualanresep_t.ruangan_id = ruangan_m.ruangan_id");
        $sql->leftJoin("returresep_t", "returresep_t.penjualanresep_id = penjualanresep_t.penjualanresep_id");
        $sql->leftJoin("returresepdet_t", "returresepdet_t.returresepdet_id = obatalkespasien_t.returresepdet_id");
        $sql->group("jenisobatalkes_m.jenisobatalkes_id, jenisobatalkes_m.jenisobatalkes_nama");
        $sql->where("penjualanresep_t.tglpenjualan BETWEEN :tanggal_awal AND :tanggal_akhir");
        $sql->params = array(
            ':tanggal_awal' => $this->tglAwal,
            ':tanggal_akhir' => $this->tglAkhir
        );
        $record = $sql->queryAll();
        $result = array();
        $keyField = array();
        if (!empty($record)) {
            foreach ($record as $row) {
                $result[] = $row;
            }
            foreach ($result[0] as $x => $z) {
                $keyField[] = $x;
            }
        }
        $dataProvider = new CArrayDataProvider($result);
        return $dataProvider;
    }

    /**
     * Method untuk mendapatkan nama Model
     * @return String 
     */
    public function getNamaModel() {
        return __CLASS__;
    }

    public function getPenjaminItems($carabayar_id = null) {
        if (!empty($carabayar_id))
            return PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id' => $carabayar_id, 'penjamin_aktif' => true), array('order' => 'penjamin_nama'));
        else
            return array();
        //return PenjaminpasienM::model()->findAllByAttributes(array('penjamin_aktif'=>true),array('order'=>'penjamin_nama'));
    }

    /**
     * data provider untuk table
     */
    public function searchTable() {
        $criteria = $this->functionCriteria(true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * method untuk criteria
     * @return CDbCriteria 
     */
    public function functionCriteria($params = null) {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;
//        if (isset($params)) {
//            $criteria->select = 'tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
//            $criteria->group = 'tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
//        } else {
////				$criteria->select = array('tglpenjualan', 'tglresep','jenispenjualan','noresep','no_rekam_medik','no_pendaftaran', 'nama_pasien', 'nama_bin', 'jeniskelamin', 'umur', 'totalhargajual', 'totaltarifservice','biayaadministrasi', 'subsidiasuransi', 'subsidipemerintah', 'subsidirs', 'iurbiaya','carabayar_nama', 'penjamin_nama', 'instalasiasal_nama', 'ruanganasal_nama');
//            $criteria->select = 'obatalkes_nama, tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
//            $criteria->group = 'obatalkes_nama, tglpenjualan,tglresep,jenispenjualan,noresep,no_rekam_medik, no_pendaftaran, nama_pasien, nama_bin, jeniskelamin, umur, totalhargajual, totaltarifservice,biayaadministrasi, subsidiasuransi, subsidipemerintah, subsidirs, iurbiaya,carabayar_nama, penjamin_nama, instalasiasal_nama, ruanganasal_nama,nama_pegawai, oasudahbayar_id';
//        }
//        $criteria->addBetweenCondition('tglpenjualan', $this->tglAwal, $this->tglAkhir);
//        $criteria->compare('LOWER(jenispenjualan)', strtolower($this->jenispenjualan), true);
//        if (is_array($this->carabayar_id)) {
//            $criteria->addInCondition('carabayar_id', $this->carabayar_id);
//        }
//        $this->penjamin_id = (!empty($this->penjamin_id) ? $this->penjamin_id : 0);
//        if (is_array($this->penjamin_id)) {
//            $criteria->addInCondition('penjamin_id', $this->penjamin_id);
//        }
//
//        if (is_array($this->ruanganasal_nama)) {
//            $criteria->addInCondition('ruanganasal_nama', $this->ruanganasal_nama);
//        }
//
//        if (is_array($this->oasudahbayar_id) && count($this->oasudahbayar_id) == 1) {
//            if ($this->oasudahbayar_id[0] == 1) {
//                $criteria->addCondition('oasudahbayar_id IS NOT NULL');
//            } else {
//                $criteria->addCondition('oasudahbayar_id IS NULL');
//            }
//        }

        /*
          $criteria->addCondition('ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
          $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
          //            (!is_array($this->penjamin_id)) ? $this->penjamin_id = 0 : '' ;

          $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
          $criteria->addCondition('ruangan_id = '.Yii::app()->user->getState('ruangan_id'));
          $criteria->compare('LOWER(instalasiasal_nama)',strtolower($this->instalasiasal_nama),true);
          (!is_array($this->ruanganasal_nama)) ? $this->ruanganasal_nama = 0 : '' ;
          $this->ruanganasal_nama = (is_array($this->ruanganasal_nama) ? array_map('strtolower', $this->ruanganasal_nama) : strtolower($this->ruanganasal_nama));;
          $criteria->compare('LOWER(ruanganasal_nama)',  $this->ruanganasal_nama);

         */
        return $criteria;
    }

}
