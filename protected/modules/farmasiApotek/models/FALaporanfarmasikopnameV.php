<?php
class FALaporanfarmasikopnameV extends LaporanfarmasikopnameV
{
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchTable()
    {
        // var_dump($this->jenisobatalkes_id);
            $criteria=new CDbCriteria;
/*
		$criteria->compare('stokopname_id',$this->stokopname_id);
		$criteria->compare('LOWER(nostokopname)',strtolower($this->nostokopname),true);
		$criteria->compare('isstokawal',$this->isstokawal);
		$criteria->compare('LOWER(jenisstokopname)',strtolower($this->jenisstokopname),true);
		$criteria->compare('LOWER(keterangan_opname)',strtolower($this->keterangan_opname),true);
		$criteria->compare('totalharga',$this->totalharga);
		$criteria->compare('totalnetto',$this->totalnetto);
		$criteria->compare('LOWER(create_time)',strtolower($this->create_time),true);
		$criteria->compare('LOWER(update_time)',strtolower($this->update_time),true);
		$criteria->compare('LOWER(create_loginpemakai_id)',strtolower($this->create_loginpemakai_id),true);
		$criteria->compare('LOWER(update_loginpemakai_id)',strtolower($this->update_loginpemakai_id),true);
		$criteria->compare('LOWER(create_ruangan)',strtolower($this->create_ruangan),true);
		$criteria->compare('satuankecil_id',$this->satuankecil_id);
		$criteria->compare('LOWER(satuankecil_nama)',strtolower($this->satuankecil_nama),true);
		$criteria->compare('sumberdana_id',$this->sumberdana_id);
		$criteria->compare('LOWER(sumberdana_nama)',strtolower($this->sumberdana_nama),true);
		$criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
		$criteria->compare('LOWER(jenisobatalkes_kode)',strtolower($this->jenisobatalkes_kode),true);
		$criteria->compare('LOWER(jenisobatalkes_nama)',strtolower($this->jenisobatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_barcode)',strtolower($this->obatalkes_barcode),true);
		$criteria->compare('LOWER(obatalkes_kode)',strtolower($this->obatalkes_kode),true);
		$criteria->compare('LOWER(obatalkes_nama)',strtolower($this->obatalkes_nama),true);
		$criteria->compare('LOWER(obatalkes_namalain)',strtolower($this->obatalkes_namalain),true);
		$criteria->compare('LOWER(obatalkes_golongan)',strtolower($this->obatalkes_golongan),true);
		$criteria->compare('LOWER(obatalkes_kategori)',strtolower($this->obatalkes_kategori),true);
		$criteria->compare('LOWER(obatalkes_kadarobat)',strtolower($this->obatalkes_kadarobat),true);
		$criteria->compare('kemasanbesar',$this->kemasanbesar);
		$criteria->compare('kekuatan',$this->kekuatan);
		$criteria->compare('LOWER(satuankekuatan)',strtolower($this->satuankekuatan),true);
		$criteria->compare('volume_fisik',$this->volume_fisik);
		$criteria->compare('hargasatuan',$this->hargasatuan);
		$criteria->compare('jumlahharga',$this->jumlahharga);
		$criteria->compare('harganetto',$this->harganetto);
		$criteria->compare('jumlahnetto',$this->jumlahnetto);
		$criteria->compare('LOWER(tglkadaluarsa)',strtolower($this->tglkadaluarsa),true);
//		$criteria->compare('date(tglstokopname)',date($this->tglAwal),true);
		$criteria->compare('LOWER(kondisibarang)',strtolower($this->kondisibarang),true);
 * 
 */
                $criteria->addBetweenCondition('date(tglstokopname)',$this->tglAwal,$this->tglAkhir);
                $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
//                $criteria->compare('ruangan_id',20);
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
    }
    public function searchTableGF()
    {
            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id,true);
            $criteria->compare('ruangan_id',86);
          
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
        
        
    public function searchPrint()
    {
        $criteria=new CDbCriteria;

        $criteria->addBetweenCondition('date(tglstokopname)',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
        // $criteria->compare('ruangan_id',20);
        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }
    public function searchPrintGF()
    {
        $criteria=new CDbCriteria;

        $criteria->addBetweenCondition('tglstokopname',$this->tglAwal,$this->tglAkhir);
        $criteria->compare('jenisobatalkes_id',$this->jenisobatalkes_id);
        $criteria->compare('ruangan_id',86);
        // Klo limit lebih kecil dari nol itu berarti ga ada limit 
        $criteria->limit=-1; 

        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
        ));
    }

}