<?php

class InformasiReturObatAlkesController extends SBaseController
{
       public function actionIndex()
	{
            $modInfoReturPenjualanObat = new ReturbayarpelayananT();
            $modInfoReturPenjualanObat->unsetAttributes();
            $modInfoReturPenjualanObat->tglAwal = date("Y-m-d").' 00:00:00';
            $modInfoReturPenjualanObat->tglAkhir = date("Y-m-d").' 23:59:59';
            if(isset($_GET['ReturbayarpelayananT'])){
                $format = new CustomFormat();
                $modInfoReturPenjualanObat->attributes = $_GET['ReturbayarpelayananT'];
                $modInfoReturPenjualanObat->tglAwal = $format->formatDateTimeMediumForDB($_GET['ReturbayarpelayananT']['tglAwal']);
                $modInfoReturPenjualanObat->tglAkhir = $format->formatDateTimeMediumForDB($_GET['ReturbayarpelayananT']['tglAkhir']);
            }
		
            $this->render('index',array('modInfoReturPenjualanObat'=>$modInfoReturPenjualanObat));
        }
        
        public function actionDetailRetur($id) {
        $this->layout = '//layouts/frameDialog';
        
            $model = ReturbayarpelayananT::model()->findByPk($id);
            
          
        $this->render('view', array('model'=>$model,'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 
                                    ));
        }
        
        public function actionPembayaran($idReturBayar) {
        $this->layout = '//layouts/frameDialog';
        
            $model = ReturbayarpelayananT::model()->findByPk($idReturBayar);
            $format = new CustomFormat();
            $modTandaBukti = new TandabuktikeluarT;
            $modTandaBukti->biayaadministrasi = $model->biayaadministrasi;
            $modTandaBukti->jmlkaskeluar = $model->totaloaretur - $model->biayaadministrasi;
            $modTandaBukti->untukpembayaran = "Retur Pembayaran Penjualan Obat Alkes";
            
            if(isset($_POST['TandabuktikeluarT'])){
                $modTandaBukti->attributes = $_POST['TandabuktikeluarT'];
                $modTandaBukti->returbayarpelayanan_id = $model->returbayarpelayanan_id;
                $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTandaBukti->tglkaskeluar = $format->formatDateTimeMediumForDB($_POST['TandabuktikeluarT']['tglkaskeluar']);
                $modTandaBukti->tahun = date('Y');
                $modTandaBukti->nokaskeluar = Generator::noBuktiKeluar();
                if($modTandaBukti->validate()){
//                    echo "<pre>";
//                    echo print_r($modTandaBukti->getAttributes());
//                    echo "</pre>";
//                    exit();
                    if($modTandaBukti->save()){
                        ReturbayarpelayananT::model()->updateByPk($idReturBayar,array('tandabuktikeluar_id'=>$modTandaBukti->tandabuktikeluar_id));
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    }else{
                        Yii::app()->user->setFlash('error',"Data gagal disimpan");
                    }
                }
                
            }
        $this->render('returPembayaran', array('model'=>$model, 'modTandaBukti'=>$modTandaBukti,
                        'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 
                                    ));
        }
}