<?php

class PembatalanUangMukaNewController extends SBaseController
{
    protected $successSave = true;
    
	public function actionIndex()
	{
            if(!empty($_GET['frame'])){
                $this->layout = 'frameDialog';
            }
            
            $modBatal = new BKPembatalanUangmukaT;
            $modPendaftaran = new BKPendaftaranT;
            $modPasien = new BKPasienM;
            $modBuktiKeluar = new BKTandabuktikeluarT;
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->untukpembayaran = 'Pembatalan Uang Muka';
            $modBuktiKeluar->nokaskeluar = KeyGenerator::noKasKeluar();
            $modBuktiKeluar->biayaadministrasi = 0;
            $pemakaianUangMuka = new BKPemakaianuangmukaT;
            
            $is_dialog = false;
            // pengecekan jika request dari iframe
            if(!empty($_GET['idBayarUangMuka']) && !isset($_POST['BKPembatalanUangmukaT']))
            {
                $modUangMuka = BKBayaruangmukaT::model()->findByPk($_GET['idBayarUangMuka']);
                $modPendaftaran = BKPendaftaranT::model()->findByPk($modUangMuka->pendaftaran_id);
                $modPasien = BKPasienM::model()->findByPk($modUangMuka->pasien_id);
                
                $modBuktiKeluar->jmlkaskeluar = $modUangMuka->jumlahuangmuka;
                $modBuktiKeluar->namapenerima = $modPasien->nama_pasien;
                $modBuktiKeluar->alamatpenerima = $modPasien->alamat_pasien;
                $modBatal->tandabuktibayar_id = $modUangMuka->tandabuktibayar_id;
                $cekPemakaianUangMuka = BKPemakaianuangmukaT::model()->findByPk($modUangMuka->pemakaianuangmuka_id);
                if($cekPemakaianUangMuka)
                {
                    $pemakaianUangMuka = $cekPemakaianUangMuka;
                    $modBuktiKeluar->jmlkaskeluar = $pemakaianUangMuka->sisauangmuka;
                    $modBuktiKeluar->untukpembayaran = 'Pengembalian Uang Muka';
                    $modBatal->keterangan_batal = 'Kelebihan uang muka dipembayaran kasir';
                }
                $is_dialog = true;
            }
            
            if(isset($_POST['BKPembatalanUangmukaT']) && !empty($_POST['BKPendaftaranT']['pendaftaran_id']))
            {

                $modPendaftaran = BKPendaftaranT::model()->findByPk($_POST['BKPendaftaranT']['pendaftaran_id']);
                $modPasien = BKPasienM::model()->findByPk($_POST['BKPendaftaranT']['pasien_id']);
                
                $transaction = Yii::app()->db->beginTransaction();
                try{
                    $cekUangMuka = BKBayaruangmukaT::model()->findByPk(
                        $_POST['BKPembatalanUangmukaT']['bayaruangmuka_id']
                    );

                    $totalUangMuka = $cekUangMuka->jumlahuangmuka - $_POST['BKTandabuktikeluarT']['jmlkaskeluar'];
                    if($totalUangMuka > 0){
                        $sisa = true;
                    }else{
                        $sisa = false;
                    }

                    $is_pengembalian = true;
                    if($cekUangMuka->pemakaianuangmuka_id == '' || is_null($cekUangMuka->pemakaianuangmuka_id))
                    {
                        $is_pengembalian = false;
                    }
                    
                    if($is_pengembalian == true && $sisa != true)
                    {
                        $this->successSave = false;
                        $modBuktiKeluar = new BKTandabuktikeluarT;
                        $modBuktiKeluar->attributes = $_POST['BKTandabuktikeluarT'];
                        $modBuktiKeluar->tglkaskeluar = $_POST['BKPembatalanUangmukaT']['tglpembatalan'];
                        $modBuktiKeluar->keterangan_pengeluaran = $_POST['BKPembatalanUangmukaT']['keterangan_batal'];
                        $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modBuktiKeluar->tahun = date('Y');
                        $modBuktiKeluar->jmlkaskeluar = $_POST['BKTandabuktikeluarT']['jmlkaskeluar'];
                        if($modBuktiKeluar->save())
                        {
                            $cekUangMuka->jumlahuangmuka = $cekUangMuka->jumlahuangmuka - $_POST['BKTandabuktikeluarT']['jmlkaskeluar'];
                            $cekUangMuka->keteranganuangmuka = $_POST['BKTandabuktikeluarT']['keterangan_batal'];
                            if($cekUangMuka->save())
                            {
                                $pemakaianUangMuka = BKPemakaianuangmukaT::model()->findByPk(
                                    $cekUangMuka->pemakaianuangmuka_id
                                );
                                $pemakaianUangMuka->tandabuktikeluar_id = $modBuktiKeluar->tandabuktikeluar_id;
                                if($pemakaianUangMuka->save())
                                {
                                    $this->successSave = true;
                                }
                            }
                        }
                    }else{
                        $modBatal = $this->savePembatalanUangMuka($_POST['BKPembatalanUangmukaT']);
                        $modBuktiKeluar = $this->saveTandaBuktiKeluar($modBatal, $_POST['BKTandabuktikeluarT'], $_POST['JenispengeluaranrekeningV']);
                        $this->updateBayarUangMuka($modBatal,$modBuktiKeluar,$sisa,$totalUangMuka);
                        $this->updateTandaBuktiBayar($modBatal);
                    }
                    
                    $successSave = $this->successSave;
                    if($successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                }catch (Exception $exc){
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render('index',
                array(
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modBuktiKeluar'=>$modBuktiKeluar,
                    'modBatal'=>$modBatal,
                    'successSave'=>$successSave,
                    'is_dialog'=>$is_dialog
                )
            );
	}
        
        protected function savePembatalanUangMuka($postPembatalan)
        {
            $modBatal = new BKPembatalanUangmukaT;
            $modBatal->attributes = $postPembatalan;
            $modBatal->ruangan_id = Yii::app()->user->ruangan_id;
            $modBatal->create_ruangan = Yii::app()->user->ruangan_id;
            $modBatal->create_time = date('Y-m-d');
            
            if($modBatal->validate()){
                $modBatal->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBatal;
        }
        
        protected function saveTandaBuktiKeluar($modPembatalan,$postBuktiKeluar,$postRekenings = array())
        {
            $modBuktiKeluar = new BKTandabuktikeluarT;
            $modBuktiKeluar->tglkaskeluar = $modPembatalan->tglpembatalan;
            $modBuktiKeluar->keterangan_pengeluaran = $modPembatalan->keterangan_batal;
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->jmlkaskeluar = str_replace(',','',$_POST['BKTandabuktikeluarT']['jmlkaskeluar']);
            if($modBuktiKeluar->validate()){
                
                if(isset($postRekenings)){//simpan jurnal rekening
                    $modJurnalRekening = $this->saveJurnalRekening();
                    //update jurnalrekening_id
                    $modBuktiKeluar->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                      ADA DI BAWAH >> $modBuktiKeluar->save();
                    $saveDetailJurnal = $this->saveJurnalDetailsNew($modJurnalRekening, $postRekenings, null, '-');
                }
                
                $modBuktiKeluar->save();
                $this->updatePembatalan($modPembatalan->pembatalanuangmuka_id, $modBuktiKeluar);
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBuktiKeluar;
        }
        
        public function saveJurnalRekening($ruangan_id = null)
        {
            $modJurnalRekening = new JurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
            $modJurnalRekening->nobuktijurnal = MyGenerator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = MyGenerator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = "";
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = date('Y-m-d H:i:s');
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modJurnalRekening->ruangan_id = empty($ruangan_id) ? Yii::app()->user->getState('ruangan_id') : $ruangan_id;
            
            
            if($modJurnalRekening->validate()){
//                    echo "<pre>";
//                    print_r($modJurnalRekening->attributes);
//                    exit;
                $modJurnalRekening->save();
            } else {
                $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
            }
            return $modJurnalRekening;
        }
        
        public function saveJurnalDetailsNew($modJurnalRekening, $postRekenings, $jenisSimpan = null, $sebagaipembayaran_bkm){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                $model[$i] = new JurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                $model[$i]->uraiantransaksi = 'Pengembalian Deposit';
                $model[$i]->saldodebit = $rekening['saldodebit'];
                $model[$i]->saldokredit = $rekening['saldokredit'];
                $model[$i]->nourut = $i+1;
                $model[$i]->rekening1_id = $rekening['struktur_id'];
                $model[$i]->rekening2_id = $rekening['kelompok_id'];
                $model[$i]->rekening3_id = $rekening['jenis_id'];
                $model[$i]->rekening4_id = $rekening['obyek_id'];
                $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                $model[$i]->catatan = "";
                if($model[$i]->validate()){
                    $model[$i]->save();
                }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                    $valid = false;
                    break;
                }
            }
            return $valid;        
        }
        
        protected function updateTandaBuktiBayar($modPembatalan)
        {
            TandabuktibayarT::model()->updateByPk(
                $modPembatalan->tandabuktibayar_id,
                array(
                    'pembatalanuangmuka_id'=>$modPembatalan->pembatalanuangmuka_id
                )
            );
        }
        
        protected function updateBayarUangMuka($modPembatalan,$modBuktiKeluar=null,$sisa=null,$totalUangMuka=null)
        {
            if($sisa == true){
                $idPembatalan = null;
            }else{
                $idPembatalan = $modPembatalan->pembatalanuangmuka_id;
            }
            BKBayaruangmukaT::model()->updateByPk(
                $modPembatalan->bayaruangmuka_id,
                array(
                    'pembatalanuangmuka_id'=>$idPembatalan,
                    'jumlahuangmuka'=>$totalUangMuka
                )
            );
        }

        protected function updatePembatalan($idPembatalan,$modBuktiKeluar)
        {
            BKPembatalanUangmukaT::model()->updateByPk(
                $idPembatalan,
                array(
                    'tandabuktikeluar_id'=>$modBuktiKeluar->tandabuktikeluar_id
                )
            );
        }
        
        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}