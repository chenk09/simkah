<?php

class LaporanController extends SBaseController {

    public function actionLaporanPenerimaanKasir() {
        $model = new BKLaporanpenerimaankasirV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $model->ruangan_id = CHtml::listData(RuangankasirV::model()->findAll(), 'ruangan_id', 'ruangan_id');
        $filter = null;
        if (isset($_GET['BKLaporanpenerimaankasirV'])) {
            $model->attributes = $_GET['BKLaporanpenerimaankasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpenerimaankasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpenerimaankasirV']['tglAkhir']);
        }

        $this->render('penerimaanKasir/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanPenerimaanKasir() {
        $model = new BKLaporanpenerimaankasirV('search');
        $judulLaporan = 'Laporan Penerimaan Kasir';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Kasir';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanpenerimaankasirV'])) {
            $model->attributes = $_REQUEST['BKLaporanpenerimaankasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpenerimaankasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpenerimaankasirV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'penerimaanKasir/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanPenerimaanKasir() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanpenerimaankasirV('search');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Penerimaan Kasir';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanpenerimaankasirV'])) {
            $model->attributes = $_GET['BKLaporanpenerimaankasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpenerimaankasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpenerimaankasirV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
//Laporanretur->mulai

    public function actionLaporanRetur() {
        $model = new BKLaporanreturpelayananV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        // $model->ruangan_id = CHtml::listData(RuangankasirV::model()->findAll(), 'ruangan_id', 'ruangan_id');
        $filter = null;
        if (isset($_GET['BKLaporanreturpelayananV'])) {
            $model->attributes = $_GET['BKLaporanreturpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAkhir']);
        }

        $this->render('retur/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanRetur() {
        $model = new BKLaporanreturpelayananV('search');
        $judulLaporan = 'Laporan Retur Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Retur Pelayanan';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanreturpelayananV'])) {
            $model->attributes = $_REQUEST['BKLaporanreturpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanreturpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanreturpelayananV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'retur/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikLaporanRetur() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanreturpelayananV('search');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembebasan Tarif';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanreturpelayananV'])) {
            $model->attributes = $_GET['BKLaporanreturpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
 //endlaporanretur

//Batas========
    public function actionLaporanPembebasanTarif() {
        $model = new BKLaporanpembebasantarifV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $model->ruangan_id = CHtml::listData(RuangankasirV::model()->findAll(), 'ruangan_id', 'ruangan_id');
        $filter = null;
        if (isset($_GET['BKLaporanpembebasantarifV'])) {
            $model->attributes = $_GET['BKLaporanpembebasantarifV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembebasantarifV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembebasantarifV']['tglAkhir']);
        }

        $this->render('pembebasanTarif/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanPembebasanTarif() {
        $model = new BKLaporanpembebasantarifV('search');
        $judulLaporan = 'Laporan Pembebasan Tarif';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembebasan Tarif';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanpembebasantarifV'])) {
            $model->attributes = $_REQUEST['BKLaporanpembebasantarifV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpembebasantarifV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpembebasantarifV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pembebasanTarif/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }


    public function actionFrameGrafikLaporanPembebasanTarif() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanpembebasantarifV('search');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembebasan Tarif';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanpembebasantarifV'])) {
            $model->attributes = $_GET['BKLaporanpembebasantarifV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembebasantarifV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembebasantarifV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanPembayaranPelayanan() {
        $model = new BKLaporanpembayaranpelayananV('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $model->ruangan_id = CHtml::listData(RuangankasirV::model()->findAll(), 'ruangan_id', 'ruangan_id');
        $model->penjamin_id =  CHtml::listData(PenjaminpasienM::model()->findAll('penjamin_aktif = true'), 'penjamin_id', 'penjamin_id');
        $filter = null;
        if (isset($_GET['BKLaporanpembayaranpelayananV'])) {
            $model->attributes = $_GET['BKLaporanpembayaranpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembayaranpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembayaranpelayananV']['tglAkhir']);
            $model->ruangan_id = $_GET['BKLaporanpembayaranpelayananV']['ruangan_id'];
        }

        $this->render('pembayaranPelayanan/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintlaporanPembayaranPelayanan() {
        $model = new BKLaporanpembayaranpelayananV('search');
        $judulLaporan = 'Laporan Pembayaran Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembayaran Pelayanan';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanpembayaranpelayananV'])) {
            $model->attributes = $_REQUEST['BKLaporanpembayaranpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpembayaranpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanpembayaranpelayananV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'pembayaranPelayanan/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafiklaporanPembayaranPelayanan() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanpembayaranpelayananV('search');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembayaran Pelayanan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanpembayaranpelayananV'])) {
            $model->attributes = $_GET['BKLaporanpembayaranpelayananV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembayaranpelayananV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpembayaranpelayananV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

    }

    public function actionLaporanKunjunganPasien()
    {
        $model = new BKLaporankunjunganPasien('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['BKLaporankunjunganPasien'])) {
            $model->attributes = $_GET['BKLaporankunjunganPasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporankunjunganPasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporankunjunganPasien']['tglAkhir']);
        }

        $this->render('kunjunganPasien/adminKunjunganPasien', array(
            'model' => $model,
        ));
    }

    public function actionPrintLaporanKunjunganPasien()
    {
        $model = new BKLaporankunjunganPasien('search');
        $judulLaporan = 'Laporan Pembayaran Pelayanan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pembayaran Pelayanan';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporankunjunganPasien'])) {
            $model->attributes = $_REQUEST['BKLaporankunjunganPasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporankunjunganPasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporankunjunganPasien']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kunjunganPasien/_printKunjunganPasien';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikKunjunganPasien()
    {
        $this->layout = '//layouts/frameDialog';

        $model = new BKLaporankunjunganPasien('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kunjungan Rumah Sakit';
        $data['type'] = $_GET['type'];

        if (isset($_GET['BKLaporankunjunganPasien'])) {
            $model->attributes = $_GET['BKLaporankunjunganPasien'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporankunjunganPasien']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporankunjunganPasien']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionLaporanPasienSudahBayar()
    {
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['BKPembayaranpelayananT'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];

            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        $this->render('pasienSudahBayar/index',array('model'=>$model));
    }

    /**
     * actionLaporanPasienSudahBayarP3 untuk di laporan billingKasir/laporan/laporanPasienSudahBayar
     * tabulasi P3
     * @author Miranitha Fasha | EHJ-1749 | 01-05-2014
     */
    public function actionLaporanPasienSudahBayarP3()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['BKPembayaranpelayananT'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];

            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        $this->render('pasienSudahBayar/p3/index',array('model'=>$model));
    }

    public function actionPrintLaporanPasienSudahBayarP3()
    {
        $format = new CustomFormat();
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $data = array();
        $data['judulLaporan'] = 'Laporan Pasien Sudah Bayar - P3';
        if(isset($_GET['BKPembayaranpelayananT'])){
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('pasienSudahBayar/p3/Print',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('pasienSudahBayar/p3/Print',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('pasienSudahBayar/p3/Print',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

    /**
     * actionLaporanPasienSudahBayarUmum untuk di laporan billingKasir/laporan/laporanPasienSudahBayar
     * tabulasi Umum
     * @author Miranitha Fasha | EHJ-1749 | 01-05-2014
     */
    public function actionLaporanPasienSudahBayarUmum()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['BKPembayaranpelayananT'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];

            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        $this->render('pasienSudahBayar/umum/index',array('model'=>$model));
    }

    public function actionPrintLaporanPasienSudahBayarUmum()
    {
        $format = new CustomFormat();
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $data = array();
        $data['judulLaporan'] = 'Laporan Pasien Sudah Bayar - Umum';
        if(isset($_GET['BKPembayaranpelayananT'])){
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('pasienSudahBayar/umum/Print',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('pasienSudahBayar/umum/Print',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('pasienSudahBayar/umum/Print',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

    /**
     * actionLaporanPasienSudahBayarAll untuk di laporan billingKasir/laporan/laporanPasienSudahBayar
     * tabulasi All
     * @author Miranitha Fasha | EHJ-1749 | 01-05-2014
     */
    public function actionLaporanPasienSudahBayarAll()
    {
        $this->layout = '//layouts/frameDialog';
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['BKPembayaranpelayananT'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];

            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        $this->render('pasienSudahBayar/all/index',array('model'=>$model));
    }

    public function actionPrintLaporanPasienSudahBayarAll()
    {
        $format = new CustomFormat();
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');
        $data = array();
        $data['judulLaporan'] = 'Laporan Pasien Sudah Bayar - Semua';
        if(isset($_GET['BKPembayaranpelayananT'])){
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            $model->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKPembayaranpelayananT']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPembayaranpelayananT']['nama_pasien'];
            $model->nama_bin = $_GET['BKPembayaranpelayananT']['nama_bin'];
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('pasienSudahBayar/all/Print',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('pasienSudahBayar/all/Print',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('pasienSudahBayar/all/Print',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

	public function actionLaporanPasienSudahPulang()
    {
        $model = new BKRekapitulasipasienpulangV('searchPasienSudahPulang');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if(isset($_GET['BKRekapitulasipasienpulangV']))
		{
            $format = new CustomFormat();
            $model->attributes = $_GET['BKRekapitulasipasienpulangV'];
            $model->no_pendaftaran = $_GET['BKRekapitulasipasienpulangV']['no_pendaftaran'];
            $model->no_rekam_medik = $_GET['BKRekapitulasipasienpulangV']['no_rekam_medik'];
            $model->nama_pasien = $_GET['BKPasiensudaBKRekapitulasipasienpulangVhpulang']['nama_pasien'];
            $model->nama_bin = $_GET['BKRekapitulasipasienpulangV']['nama_bin'];

            if(!empty($_GET['BKRekapitulasipasienpulangV']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKRekapitulasipasienpulangV']['tglAwal']);
            }
            if(!empty($_GET['BKRekapitulasipasienpulangV']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKRekapitulasipasienpulangV']['tglAkhir']);
            }
        }

        $this->render('pasienSudahPulang/adminPasienSudahPulang',array('model'=>$model));
    }

    public function actionLaporanReturold()
    {
        $model = new BKLaporanreturpelayananV('searchPrint');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if(isset($_GET['BKLaporanreturpelayananV'])){
            $format = new CustomFormat();
            $model->attributes = $_GET['BKLaporanreturpelayananV'];

            if(!empty($_GET['BKLaporanreturpelayananV']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAwal']);
            }
            if(!empty($_GET['BKLaporanreturpelayananV']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAkhir']);
            }
        }
        $this->render('retur/adminretur',array('model'=>$model));
    }

    public function actionPrint()
    {
        $format = new CustomFormat();
        $model = new BKPembayaranpelayananT('searchPasienSudahBayar');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $data = array();

        if(isset($_GET['BKPembayaranpelayananT'])){
            if($_GET['filter_tab'] == 'all')
            {
                $data['judulLaporan'] = "Laporan Semua";
                $data['filter'] = "all";
            }
            else if($_GET['filter_tab'] == 'p3')
            {
                $data['judulLaporan'] = "Laporan P3";
                $data['filter'] = "p3";
            }
            else if($_GET['filter_tab'] == 'umum')
            {
                $data['judulLaporan'] = "Laporan Umum";
                $data['filter'] = "umum";
            }
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKPembayaranpelayananT'];
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAwal']);
            }
            if(!empty($_GET['BKPembayaranpelayananT']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPembayaranpelayananT']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('pasienSudahBayar/print',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('pasienSudahBayar/print',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('pasienSudahBayar/print',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

    public function actionPrintPasienPulang()
    {
        $format = new CustomFormat();
        $model = new BKRekapitulasipasienpulangV('searchPrintPasienSudahPulang');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $data = array();

        if(isset($_GET['BKRekapitulasipasienpulangV'])){
            if($_GET['filter_tab'] == 'all')
            {
                $data['judulLaporan'] = "Laporan Pasien Pulang";
                $data['filter'] = "all";
            }
            else if($_GET['filter_tab'] == 'p3')
            {
                $data['judulLaporan'] = "Laporan Pasien Pulang P3";
                $data['filter'] = "p3";
            }
            else if($_GET['filter_tab'] == 'umum')
            {
                $data['judulLaporan'] = "Laporan Pasien Pulang Umum";
                $data['filter'] = "umum";
            }
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKRekapitulasipasienpulangV'];
            if(!empty($_GET['BKRekapitulasipasienpulangV']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKRekapitulasipasienpulangV']['tglAwal']);
            }
            if(!empty($_GET['BKRekapitulasipasienpulangV']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKRekapitulasipasienpulangV']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('pasienSudahPulang/print',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('pasienSudahPulang/print',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
			$mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            //$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            //$mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',10,10,10,10,10,10);
            $mpdf->WriteHTML($this->renderPartial('pasienSudahPulang/print',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

    public function actionPrintRetur()
    {
        $format = new CustomFormat();
        $model = new BKLaporanreturpelayananV('searchPrint');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        $data = array();

        if(isset($_GET['BKLaporanreturpelayananV'])){
            if($_GET['filter_tab'] == 'all')
            {
                $data['judulLaporan'] = "Laporan Semua";
                $data['filter'] = "all";
            }
            $data['caraPrint'] = $_REQUEST['caraPrint'];
            $model->attributes = $_GET['BKLaporanreturpelayananV'];
            if(!empty($_GET['BKLaporanreturpelayananV']['tglAwal']))
            {
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAwal']);
            }
            if(!empty($_GET['BKLaporanreturpelayananV']['tglAwal']))
            {
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanreturpelayananV']['tglAkhir']);
            }
        }
        if($_REQUEST['caraPrint'] == 'PRINT'){
            $this->layout='//layouts/printWindows';
            $this->render('retur/printretur',array('model'=>$model, 'data'=>$data));
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('retur/printretur',array('model'=>$model, 'data'=>$data));
        }else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('retur/printretur',array('model'=>$model, 'data'=>$data),true));
            $mpdf->Output();
        }
    }

    public function actionLaporanKeseluruhan()
    {
        $model = new BKLaporanKeseluruhan('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['BKLaporanKeseluruhan'])) {
            $model->attributes = $_GET['BKLaporanKeseluruhan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanKeseluruhan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanKeseluruhan']['tglAkhir']);
        }

        $this->render('keseluruhan/adminKeseluruhan', array(
            'model' => $model,
        ));
    }

    public function actionprintLaporanKeseluruhan()
    {
        $model = new BKLaporanKeseluruhan('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        $data = array();
        $data['judulLaporan'] = 'Laporan Keseluruhan';

        if (isset($_REQUEST['BKLaporanKeseluruhan'])) {
            $model->attributes = $_GET['BKLaporanKeseluruhan'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanKeseluruhan']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanKeseluruhan']['tglAkhir']);
        }

        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
        if($_REQUEST['caraPrint'] == 'PDF'){
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML(
                $this->renderPartial('keseluruhan/printLaporan',
                    array(
                        'model'=>$model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data
                    ),true)
            );
            $mpdf->Output();
        }else{
            $this->layout='//layouts/printWindows';
            $this->render('keseluruhan/printLaporan', array(
                'model' => $model,
                'caraPrint'=>$_REQUEST['caraPrint'],
                'data'=>$data
            ));
        }
    }

    public function actionRincianTagihan($id)
    {
        $this->layout = '//layouts/frameDialog';
        $data['judulLaporan'] = 'Rincian Tagihan Pasien';
        $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
        $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        $this->render('keseluruhan/rincian', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
    }

    public function actionPrintRincianTagihan($id)
    {
        $data['judulLaporan'] = 'Rincian Tagihan Pasien';
        $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
        $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
            array('pendaftaran_id'=>$id)
        );
        $uang_cicilan = 0;
        foreach($uangmuka as $val)
        {
            $uang_cicilan += $val->jumlahuangmuka;
        }
        $data['uang_cicilan'] = $uang_cicilan;

        if($_REQUEST['caraPrint'] == 'PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('keseluruhan/rincian', array(
                    'modPendaftaran'=>$modPendaftaran,
                    'modRincian'=>$modRincian,
                    'data'=>$data,
                    'caraPrint'=>$_REQUEST['caraPrint']
                )
            );
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('keseluruhan/rincian',
                array(
                    'modPendaftaran'=>$modPendaftaran,
                    'modRincian'=>$modRincian,
                    'data'=>$data,
                    'caraPrint'=>$_REQUEST['caraPrint']
                )
            );
        }
        else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->WriteHTML($style, 1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML(
                $this->renderPartial('keseluruhan/rincian',
                    array(
                        'modPendaftaran'=>$modPendaftaran,
                        'modRincian'=>$modRincian,
                        'data'=>$data,
                        'caraPrint'=>$_REQUEST['caraPrint']
                    )
                ),true
            );
            $mpdf->Output();
        }
    }

    public function actionPrintDetailLaporanKeseluruhan()
    {
        $modRincian = BKRinciantagihanpasienV::model()->findAll(
            array('order'=>'pendaftaran_id,ruangan_id')
        );
        $data['judulLaporan'] = 'Rincian Tagihan Pasien';
        $data['periode'] = 'Periode : xxx';

        $row = array();
        foreach($modRincian as $i=>$val)
        {
            $pendaftaran_id = $val['pendaftaran_id'];
            $row[$pendaftaran_id]['nama'] = $val['nama_pasien'];
            $row[$pendaftaran_id]['no_rm'] = $val['no_rekam_medik'];
            $row[$pendaftaran_id]['no_pendaftaran'] = $val['no_pendaftaran'];
            $row[$pendaftaran_id]['ruangan'][$i]['nama'] = $val['ruangan_nama'];
            $row[$pendaftaran_id]['ruangan'][$i]['id'] = $val['ruangan_id'];
            $row[$pendaftaran_id]['ruangan'][$i]['tindakan'] = $val['daftartindakan_nama'];
            $row[$pendaftaran_id]['ruangan'][$i]['qty'] = $val['qty_tindakan'];
            $row[$pendaftaran_id]['ruangan'][$i]['tarif_tindakan'] = $val['tarif_tindakan'];
            $row[$pendaftaran_id]['ruangan'][$i]['total_tarif'] = ($val['qty_tindakan'] * $val['tarif_tindakan']);
        }

        if($_REQUEST['caraPrint'] == 'PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('keseluruhan/printDetailKeseluruhan', array(
                    'row'=>$row,
                    'data'=>$data,
                    'caraPrint'=>$_REQUEST['caraPrint']
                )
            );
        }
        else if($_REQUEST['caraPrint'] == 'EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('keseluruhan/printDetailKeseluruhan',
                array(
                    'row'=>$row,
                    'data'=>$data,
                    'caraPrint'=>$_REQUEST['caraPrint']
                )
            );
        }
        else if($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML(
                $this->renderPartial('keseluruhan/printDetailKeseluruhan',
                    array(
                        'row'=>$row,
                        'data'=>$data,
                        'caraPrint'=>$_REQUEST['caraPrint']
                    )
                ),true
            );
            $mpdf->Output();
        }
    }

    public function actionLaporanCaraBayar()
    {
        $model = new BKLaporanCaraBayar('search');
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['BKLaporanCaraBayar'])) {
            $model->attributes = $_GET['BKLaporanCaraBayar'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanCaraBayar']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanCaraBayar']['tglAkhir']);
        }

        if(!isset($_REQUEST['caraPrint']))
        {
            $this->render('caraBayar/adminCaraBayar', array(
                'model' => $model,
            ));
        }
        else
        {
            $data = array();
            $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
            if($_REQUEST['caraPrint'] == 'PDF'){
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF);
                $mpdf->useOddEven = 2;
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                if($_GET['BKLaporanCaraBayar']['pilihan_tab'] == 'rekap')
                {
                    $data['judulLaporan'] = 'Rekap P3';
                    $mpdf->WriteHTML(
                        $this->renderPartial('caraBayar/_tableRekapCaraBayar',
                            array(
                                'model'=>$model,
                                'caraPrint'=>$_REQUEST['caraPrint'],
                                'data'=>$data
                            ),true)
                    );
                }else{
                    $data['judulLaporan'] = 'Daftar Pasien';
                    $mpdf->WriteHTML(
                        $this->renderPartial('caraBayar/_printTableCaraBayar',
                            array(
                                'model'=>$model,
                                'caraPrint'=>$_REQUEST['caraPrint'],
                                'data'=>$data
                            ),true)
                    );
                }

                $mpdf->Output();
            }else{
                $this->layout='//layouts/printWindows';

                if($_GET['BKLaporanCaraBayar']['pilihan_tab'] == 'rekap')
                {
                    $data['judulLaporan'] = 'Rekap P3';
                    $this->render('caraBayar/_tableRekapCaraBayar', array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data
                    ));
                }else{
                    $data['judulLaporan'] = 'Daftar Pasien';
                    $this->render('caraBayar/_printTableCaraBayar', array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data
                    ));
                }

            }
        }

    }


    public function actionLaporanFarmasi()
    {
        $model = new BKObatalkesPasienT();
        $model->tglAwal = date('d M Y').' 00:00:00';
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['BKObatalkesPasienT'])) {
            $model->attributes = $_GET['BKObatalkesPasienT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKObatalkesPasienT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKObatalkesPasienT']['tglAkhir']);
            $model->no_pendaftaran = $_GET['BKObatalkesPasienT']['no_pendaftaran'];
        }

        $data = array();
        if(!isset($_REQUEST['caraPrint']))
        {
            $this->render('farmasi/adminFarmasi', array(
                'model' => $model
            ));
        }else{
            $this->layout='//layouts/printWindows';
            $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
            $criteria = new CDbCriteria;
            $criteria->with = array('pasien','pendaftaran', 'penjualanresep');
			 $criteria->addBetweenCondition('tglpelayanan',$model->tglAwal,$model->tglAkhir);
            $criteria->compare('pendaftaran.no_pendaftaran',$model->no_pendaftaran);
            $criteria->compare('pasien.no_rekam_medik',$model->no_rekam_medik);
            $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($model->nama_pasien),true);
            $criteria->compare('pendaftaran.penjamin_id',$model->penjamin_id);
            $criteria->addCondition('t.penjualanresep_id IS NOT NULL');
			if(!empty($model->penjamin_id)){
				if(is_array($model->penjamin_id)){
					$criteria->addInCondition('pendaftaran.penjamin_id',$model->penjamin_id);
				}else{
					$criteria->addCondition("pendaftaran.penjamin_id = ".$model->penjamin_id);
				}
			}
            $criteria->order = 'no_rekam_medik, no_pendaftaran, pendaftaran.penjamin_id';
            $record = BKObatalkesPasienT::model()->findAll($criteria);
            $row = array();
            $temp_no_pendaftaran = '';
            $idx = 0;
            foreach($record as $i=>$val){
                $no_pendaftaran = $val->pendaftaran->no_pendaftaran;
                $no_rekam_medik = $val->pasien->no_rekam_medik;
                $row[$no_rekam_medik]['no_rekam_medik'] = $val->pasien->no_rekam_medik;
                $row[$no_rekam_medik]['nama_pasien'] = $val->pasien->nama_pasien;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['no_pendaftaran'] = $val->pendaftaran->no_pendaftaran;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['tgl_pendaftaran'] = $val->pendaftaran->tgl_pendaftaran;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['no_transaksi'] = $val->obatalkespasien_id;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['tgl_transaksi'] = $val->tglpelayanan;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['item'] = $val->obatalkes_nama;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['apotik'] = MyFunction::formatnumber($val->hargasatuan_oa);
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['qty'] = $val->qty_oa;
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['pasien'] = MyFunction::formatnumber($val->hargasatuan_oa);
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['sub_total'] = MyFunction::formatnumber($val->hargajual_oa);
                $row[$no_rekam_medik]['data_pendaftaran'][$no_pendaftaran]['value'][$idx]['penanggung'] = MyFunction::formatnumber($val->subsidiasuransi);
                $idx++;
                $temp_no_pendaftaran = $no_pendaftaran;
            }

            if($_REQUEST['caraPrint'] == 'PDF'){
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF);
                $mpdf->useOddEven = 2;
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);

                if($_GET['BKObatalkesPasienT']['filter_tab'] == 'trans')
                {
                    $data['judulLaporan'] = 'Laporan Transaksi';
                    $mpdf->WriteHTML(
                        $this->render('farmasi/_printLaporanTrans',
                            array(
                                'model' => $model,
                                'caraPrint'=>$_REQUEST['caraPrint'],
                                'data'=>$data,
                                'record'=>$row
                            ),true
                        )
                    );
                }else{
                    $data['judulLaporan'] = 'Rekap Transaksi';
                    $mpdf->WriteHTML(
                        $this->render('farmasi/_printRekapTrans',
                            array(
                                'model' => $model,
                                'caraPrint'=>$_REQUEST['caraPrint'],
                                'data'=>$data
                            ),true
                        )
                    );
                }
                $mpdf->Output();
            }else{
                //per_reg
                if($_GET['BKObatalkesPasienT']['filter_tab'] == 'trans')
                {
                    $data['judulLaporan'] = 'Laporan Transaksi';
                    $this->render('farmasi/_printLaporanTrans', array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data,
                        'record'=>$row
                    ));
                }else if($_GET['BKObatalkesPasienT']['filter_tab'] == 'rekap'){
                    $data['judulLaporan'] = 'Rekap Transaksi';
                    $this->render('farmasi/_printRekapTrans', array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data
                    ));
                }else if($_GET['BKObatalkesPasienT']['filter_tab'] == 'per_reg'){

                    $data['judulLaporan'] = 'Rekap Transaksi';
                    $data['nama_pasien'] = 'Rekap Transaksi';
                    $data['noreg'] = 'Rekap Transaksi';
                    $data['no_rm'] = 'Rekap Transaksi';
                    $data['alamat'] = 'Rekap Transaksi';
                    $data['perusahaan'] = 'Rekap Transaksi';

                    $criteria = new CDbCriteria;
                    $criteria->with = array('pasien','pendaftaran', 'penjualanresep', 'obatalkes');
                    $criteria->compare('pendaftaran.no_pendaftaran',$model->no_pendaftaran);
                    $criteria->compare('pasien.no_rekam_medik',$model->no_rekam_medik);
                    $criteria->compare('LOWER(pasien.nama_pasien)',strtolower($model->nama_pasien),true);
					if(!empty($model->penjamin_id)){
						if(is_array($model->penjamin_id)){
							$criteria->addInCondition('pendaftaran.penjamin_id',$model->penjamin_id);
						}else{
							$criteria->addCondition("pendaftaran.penjamin_id = ".$model->penjamin_id);
						}
					}
                    $criteria->addCondition('t.penjualanresep_id IS NOT NULL');
                    $record = BKObatalkesPasienT::model()->findAll($criteria);
                    $row = array();
                    $idx = 0;
                    foreach($record as $i=>$val){
                        $obat = $val->oa;
                        $row[$obat]['kelompok'] = ($val->oa == 'OA' ? 'ALKES' : 'OBAT');
                        $row[$obat]['no_rekam_medik'] = $val->pasien->no_rekam_medik;
                        $row[$obat]['nama_pasien'] = $val->pasien->nama_pasien;
                        $row[$obat]['data_transaksi'][$idx]['no_transaksi'] = $val->obatalkespasien_id;
                        $row[$obat]['data_transaksi'][$idx]['nama_item'] = $val->obatalkes->obatalkes_nama;
                        $row[$obat]['data_transaksi'][$idx]['qty'] = $val->qty_oa;
                        $row[$obat]['data_transaksi'][$idx]['harga'] = MyFunction::formatnumber($val->hargasatuan_oa);
                        $idx++;
                    }
                    $data['judulLaporan'] = 'Detail Transaksi';
                    $this->render('farmasi/_printDetailFarmasi', array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data,
                        'row'=>$row
                    ));
                }
            }
        }
    }

    public function actionDetailTransaksiFarmasi($id)
    {
        $criteria = new CDbCriteria;
        $id = (isset($_REQUEST['id_pendaftaran']) ? $_REQUEST['id_pendaftaran'] : $id);
        $criteria->with = array('pasien','pendaftaran', 'penjualanresep', 'obatalkes');
        $criteria->compare('pendaftaran.pendaftaran_id',$id);
        $criteria->addCondition('t.penjualanresep_id IS NOT NULL');
        $criteria->order = 'no_rekam_medik, no_pendaftaran, pendaftaran.penjamin_id';
        $record = BKObatalkesPasienT::model()->findAll($criteria);
        $row = array();
        $data = array();
        $idx = 0;
        foreach($record as $i=>$val){
            $obat = $val->oa;
            $row[$obat]['kelompok'] = ($val->oa == 'OA' ? 'ALKES' : 'OBAT');
            $row[$obat]['no_rekam_medik'] = $val->pasien->no_rekam_medik;
            $row[$obat]['nama_pasien'] = $val->pasien->nama_pasien;
            $row[$obat]['data_transaksi'][$idx]['no_transaksi'] = $val->obatalkespasien_id;
            $row[$obat]['data_transaksi'][$idx]['tgl_transaksi'] = $val->tglpelayanan;
            $row[$obat]['data_transaksi'][$idx]['no_resep'] = $val->penjualanresep->noresep;
            $row[$obat]['data_transaksi'][$idx]['nama_item'] = $val->obatalkes->obatalkes_nama;
            $row[$obat]['data_transaksi'][$idx]['qty'] = $val->qty_oa;
            $row[$obat]['data_transaksi'][$idx]['harga'] = MyFunction::formatnumber($val->hargasatuan_oa);
            $row[$obat]['data_transaksi'][$idx]['total'] = MyFunction::formatnumber(($val->qty_oa*$val->hargasatuan_oa));
            $row[$obat]['data_transaksi'][$idx]['total_ori'] = ($val->qty_oa*$val->hargasatuan_oa);
            $idx++;
        }

        $id = (isset($id) ? $id : 0);
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanFarmasi();
        $model->pendaftaran_id = $id;

        $attributes = array('pendaftaran_id'=>$id);
        $record = BKLaporanFarmasi::model()->findByAttributes($attributes);
        $data['judulLaporan'] = 'Rincian Biaya Transaksi';
        $data['nama_pasien'] = $record->nama_pasien;
        $data['pendaftaran_id'] = $id;
        $data['no_pendaftaran'] = $record->no_pendaftaran;
        $data['no_rm'] = $record->no_rekam_medik;
        $data['alamat'] = $record->alamat_pasien;
        $data['perusahaan'] = '-';
        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));

        if(!isset($_REQUEST['caraPrint']))
        {
            $this->render('farmasi/_tableDetailTransaksi', array(
                'model' => $model,
                'row'=>$row,
                'data'=>$data,
            ));
        }else{
            $this->layout='//layouts/printWindows';
            if($_REQUEST['caraPrint'] == 'PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF);
                $mpdf->useOddEven = 2;
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML(
                    $this->renderPartial('farmasi/_printDetailTransaksi',
                        array(
                            'model' => $model,
                            'row'=>$row,
                            'data'=>$data,
                            'caraPrint'=>$_REQUEST['caraPrint']
                        ), true
                    )
                );
                $mpdf->Output();
            }

            $this->render('farmasi/_printDetailTransaksi', array(
                'model' => $model,
                'row'=>$row,
                'data'=>$data,
                'caraPrint'=>$_REQUEST['caraPrint']
            ));
        }
    }

    public function actionLaporanLaboratorium()
    {
        $model = new BKLaporanpendapatanpenunjangV('searchTable');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['BKLaporanpendapatanpenunjangV'])){
            $model->attributes = $_GET['BKLaporanpendapatanpenunjangV'];
            $model->asal = $_GET['BKLaporanpendapatanpenunjangV']['asal'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpendapatanpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpendapatanpenunjangV']['tglAkhir']);
        }

        $this->render('laboratoirum/adminLaboratorium', array(
            'model' => $model
        ));
    }

    public function actionDetailTransaksiLab($id)
    {
        $this->layout='//layouts/frameDialog';
        $model = new BKLaporanpendapatanpenunjangV('searchTable');
        $model->tglAwal = date('d M Y H:i:s', strtotime('2012-01-01'));
        $model->tglAkhir = date('d M Y H:i:s');
        $model->pendaftaran_id = $id;

        $data = array();
        $data['pendaftaran_id'] = $id;
        $this->render('laboratoirum/_detailTrans', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    public function actionPrintLaporanLab()
    {
        $this->layout='//layouts/printWindows';
        $model = new BKLaporanpendapatanpenunjangV('searchTable');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        if (isset($_GET['BKLaporanpendapatanpenunjangV'])){
            $model->attributes = $_GET['BKLaporanpendapatanpenunjangV'];
            $model->asal = $_GET['BKLaporanpendapatanpenunjangV']['asal'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanpendapatanpenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanpendapatanpenunjangV']['tglAkhir']);
        }

        $data = array();
        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));

        $row = array();
        $record = $model->printLapTransaksi();
        if($_GET['BKLaporanLaboratorium']['filter_tab'] == 'trans')
        {
            foreach($record as $i=>$val){
                $no_rekam_medik = $val->no_rekam_medik;
                $no_pendaftaran = $val->no_pendaftaran;
                $row[$no_rekam_medik]['no_rm'] = $val->no_rekam_medik;
                $row[$no_rekam_medik]['nama'] = $val->nama_pasien;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['no_reg'] = $val->no_pendaftaran;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['tgl_reg'] = $val->tgl_pendaftaran;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['no_trans'] = $val->tindakanpelayanan_id;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['tgl_trans'] = $val->tgl_tindakan;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['nama_item'] = $val->daftartindakan_nama;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['qty'] = $val->qty_tindakan;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['tarif'] = $val->tarif_satuan;
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['sub_total'] = ($val->qty_tindakan * $val->tarif_satuan);
                $row[$no_rekam_medik]['data_transaksi'][$no_pendaftaran]['value'][$i]['tanggungan_p3'] = 0;
            }
        }

        if($_REQUEST['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);

            if($_GET['BKLaporanLaboratorium']['filter_tab'] == 'trans')
            {
                $data['judulLaporan'] = 'Laporan Transaksi';
                $mpdf->WriteHTML(
                    $this->renderPartial('laboratoirum/_printLaporanTrans',
                        array(
                            'model' => $model,
                            'caraPrint'=>$_REQUEST['caraPrint'],
                            'data'=>$data,
                            'row'=>$row
                        ),true
                    )
                );
            }else{
                $data['judulLaporan'] = 'Rekap Transaksi';
                $mpdf->WriteHTML(
                    $this->renderPartial('laboratoirum/_printRekapTrans',
                        array(
                            'model' => $model,
                            'caraPrint'=>$_REQUEST['caraPrint'],
                            'data'=>$data
                        ),true
                    )
                );
            }
            $mpdf->Output();
        }else{
            if($_GET['BKLaporanLaboratorium']['filter_tab'] == 'trans')
            {
                $data['judulLaporan'] = 'Laporan Transaksi Laboratorium';
                $this->render('laboratoirum/_printLaporanTrans',
                    array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data,
                        'row'=>$row
                    )
                );
            }else{
                $data['judulLaporan'] = 'Rekap Transaksi';
                $this->render('laboratoirum/_printRekapTrans',
                    array(
                        'model' => $model,
                        'caraPrint'=>$_REQUEST['caraPrint'],
                        'data'=>$data
                    )
                );
            }
        }
    }

    public function actionPrintDetailTransLab()
    {
        $this->layout='//layouts/printWindows';
        $model = new BKLaporanLaboratorium;
        $model->pendaftaran_id = $_REQUEST['id_pendaftaran'];

        $data = array();
        $data['judulLaporan'] = 'Detail Transaksi';
        $data['periode'] = 'Periode : ' . date("d-m-Y", strtotime($model->tglAwal)) . ' s/d ' . date("d-m-Y", strtotime($model->tglAkhir));
        $data['pendaftaran_id'] = $_REQUEST['id_pendaftaran'];

        $record = $model->searchDetailTable()->getData();
        $row = array();
        foreach($record as $x=>$val)
        {
            $no_rekam_medik = $val->no_rekam_medik;
            $row[$no_rekam_medik]['no_rekam_medik'] = $val->no_rekam_medik;
            $row[$no_rekam_medik]['nama'] = $val->nama_pasien;
            $row[$no_rekam_medik]['noreg'] = $val->no_pendaftaran;
            $row[$no_rekam_medik]['tgl_reg'] = $val->tgl_pendaftaran;
            $row[$no_rekam_medik]['transaksi'][$x]['no_transaksi'] = $val->tindakanpelayanan_id;
            $row[$no_rekam_medik]['transaksi'][$x]['tgl_transaksi'] = $val->tgl_tindakan;
            $row[$no_rekam_medik]['transaksi'][$x]['kode'] = $val->daftartindakan_kode;
            $row[$no_rekam_medik]['transaksi'][$x]['items'] = $val->daftartindakan_nama;
            $row[$no_rekam_medik]['transaksi'][$x]['tarif_pasien'] = $val->tarif_satuan;
            $row[$no_rekam_medik]['transaksi'][$x]['penanggung'] = 0;
            $row[$no_rekam_medik]['transaksi'][$x]['tarif_lab'] = $val->tarif_satuan;
            $row[$no_rekam_medik]['transaksi'][$x]['adm'] = 0;
            $row[$no_rekam_medik]['transaksi'][$x]['total'] = $val->tarif_satuan * $val->qty_tindakan;
        }

        if($_REQUEST['caraPrint'] == 'PDF')
        {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML(
                $this->renderPartial('laboratoirum/_printDetailTrans', array(
                    'model' => $model,
                    'data'=>$data,
                    'caraPrint'=>$_REQUEST['caraPrint'],
                    'row'=>$row,
                ), true)
            );
            $mpdf->Output();
        }else{
            $this->render('laboratoirum/_printDetailTrans', array(
                'model' => $model,
                'data'=>$data,
                'caraPrint'=>$_REQUEST['caraPrint'],
                'row'=>$row,
            ));
        }
    }

    public function actionLaporanClosingKasir(){

        $model = new BKLaporanclosingkasirV('searchInformasi');
        $format = new CustomFormat();
        $model->unsetAttributes();
        $model->tglAwal = date("d M Y");
        $model->tglAkhir = date("d M Y");
        if(isset($_GET['BKLaporanclosingkasirV'])){
            $model->attributes = $_GET['BKLaporanclosingkasirV'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_GET['BKLaporanclosingkasirV']['create_ruangan'];
        }
        $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd'),'medium',null);
        $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd'),'medium',null);

        $this->render('closingKasir/index',array('model'=>$model));
    }

    public function actionPrintLaporanClosingKasir() {
        $model = new BKLaporanclosingkasirV('searchPrint');
        $judulLaporan = 'Laporan Closing Kasir';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Closing Kasir';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanclosingkasirV'])) {
            $model->attributes = $_REQUEST['BKLaporanclosingkasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_REQUEST['BKLaporanclosingkasirV']['create_ruangan'];
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'closingKasir/print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanClosingKasir() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanclosingkasirV('searchGrafik');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Closing Kasir';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanclosingkasirV'])) {
            $model->attributes = $_GET['BKLaporanclosingkasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_REQUEST['BKLaporanclosingkasirV']['create_ruangan'];
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /**
     * actionLaporanJasaDokter
     * @author ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
     */
    public function actionLaporanJasaDokter()
    {
            if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
            $format = new CustomFormat();
            $model=new BKLaporanpembayaranjasadokterV('searchLaporan');
            $model->unsetAttributes();
            $model->tglAwal = date('d M Y');
            $model->tglAkhir = date('d M Y');
            if(isset($_GET['BKLaporanpembayaranjasadokterV'])){
                    $model->attributes=$_GET['BKLaporanpembayaranjasadokterV'];
                    $model->tglAwal=$format->formatDateMediumForDB($_GET['BKLaporanpembayaranjasadokterV']['tglAwal']);
                    $model->tglAkhir=$format->formatDateMediumForDB($_GET['BKLaporanpembayaranjasadokterV']['tglAkhir']);
            }

            $this->render('jasaDokter/index',array(
                    'model'=>$model,
            ));
    }
    public function actionPrintLaporanJasaDokter(){
        $format = new CustomFormat();
        $model = new BKLaporanpembayaranjasadokterV('searchLaporan');
        $judulLaporan = 'Laporan Jasa Dokter';
        //Data Grafik
        $data['title'] = 'Grafik Laporan Jasa Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporanpembayaranjasadokterV'])) {
            $model->attributes = $_REQUEST['BKLaporanpembayaranjasadokterV'];
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['BKLaporanpembayaranjasadokterV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['BKLaporanpembayaranjasadokterV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'jasaDokter/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    /* end by ichan */

    /**
     * actionLaporanRekapJasaDokter
     * by Miranitha Fasha
     */
    public function actionLaporanRekapJasaDokter() {

        $this->pageTitle = Yii::app()->name." - Laporan Rekap Jasa Dokter";
	$filter = "";
        $model = new BKLaporantindakankomponenV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['BKLaporantindakankomponenV'])) {
            $model->attributes = $_GET['BKLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAkhir']);
        }

        $this->render('rekapJasaDokter/index', array(
            'model' => $model, 'filter' => $filter
        ));

    }

    public function actionPrintLaporanRekapJasaDokter() {
        $model = new BKLaporantindakankomponenV();
        $jasa = ucwords($_GET['filter_tab']);
        $judulLaporan = 'Laporan '.$jasa.' Jasa Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Jasa Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporantindakankomponenV'])) {
            $model->attributes = $_REQUEST['BKLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporantindakankomponenV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];

		$target = 'rekapJasaDokter/printRekap';
        $caraPrint = $_REQUEST['caraPrint'];
		if(isset($_GET['filter_tab'])){
			if($_GET['filter_tab'] == 'rekap'){
				$target = 'rekapJasaDokter/printRekap';
			}else{
				$target = 'rekapJasaDokter/printDetail';
			}
		}
        $target = $target;

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    public function actionPrintLaporanDetailJasaDokter() {
        $model = new BKLaporantindakankomponenV();
        $jasa = ucwords($_GET['filter_tab']);
        $judulLaporan = 'Laporan '.$jasa.' Jasa Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Detail Jasa Dokter';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporantindakankomponenV'])) {
            $model->attributes = $_REQUEST['BKLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporantindakankomponenV']['tglAkhir']);
        }

		$target = 'rekapJasaDokter/printDetail';
        $caraPrint = $_REQUEST['caraPrint'];
		if(isset($_GET['filter_tab'])){
			if($_GET['filter_tab'] == 'rekap'){
				$target = 'rekapJasaDokter/printRekap';
			}else{
				$target = 'rekapJasaDokter/printDetail';
			}
		}
        $target = $target;

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanRekapJasaDokter() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporantindakankomponenV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Jasa Dokter';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporantindakankomponenV'])) {
            $model->attributes = $_GET['BKLaporantindakankomponenV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /** end actionLaporanRekapJasaDokter **/


    /*
     * BillingKasir->LaporanRekapTransaksi
     * by Miranitha Fasha
     */

        public function actionLaporanRekapTransaksi() {
        $this->pageTitle = Yii::app()->name." - Laporan Rekap Transaksi";
        $model = new BKLaporanrekaptransaksiV();
        $model->unsetAttributes();
        $modDaftarTindakan = DaftartindakanM::model()->findAll();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['BKLaporanrekaptransaksiV'])) {
            $model->attributes = $_GET['BKLaporanrekaptransaksiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekaptransaksiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekaptransaksiV']['tglAkhir']);
            $model->instalasi = $_GET['BKLaporanrekaptransaksiV']['instalasi'];
        }

        $this->render('rekapTransaksiBedah/index', array(
            'model' => $model,
            'filter' => $filter
        ));

    }

    public function actionPrintLaporanRekapTransaksi() {
        $model = new BKLaporanrekaptransaksiV('search');
        $judulLaporan = 'Laporan Rekap Transaksi Pasien';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Transaksi Pasien';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporanrekaptransaksiV'])) {
            $model->attributes = $_REQUEST['BKLaporanrekaptransaksiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekaptransaksiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekaptransaksiV']['tglAkhir']);
            $model->instalasi = $_GET['BKLaporanrekaptransaksiV']['instalasi'];
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapTransaksiBedah/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanRekapTransaksi() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanrekaptransaksiV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Transaksi Pasien';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanrekaptransaksiV'])) {
            $model->attributes = $_GET['BKLaporanrekaptransaksiV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekaptransaksiV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekaptransaksiV']['tglAkhir']);
            $model->instalasi = $_GET['BKLaporanrekaptransaksiV']['instalasi'];
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    /** end LaporanRekapTransaksi **/

    /*
     * BillingKasir->LaporanRekapPendapatan
     * by Miranitha Fasha - 30 Desember 2013
     */

     public function actionLaporanRekapPendapatan() {
        $model = new BKLaporanrekappendapatanV('search');
		$modelDetail = new LaporanrekappendapatandetailV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if (isset($_GET['BKLaporanrekappendapatanV'])) {
            $model->attributes = $_GET['BKLaporanrekappendapatanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAkhir']);
			$modelDetail->bulan = $model->bulan;
			$modelDetail->tglAwal = $model->tglAwal;
			$modelDetail->tglAkhir = $model->tglAkhir;
        }

        $this->render('rekapPendapatan/index', array(
            'model' => $model, 'filter' => $filter, 'modelDetail' => $modelDetail
        ));
    }
	
	public function actionLaporanRekapPendapatanDetail() {
        $model = new BKLaporanrekappendapatanV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if (isset($_GET['BKLaporanrekappendapatanV'])) {
            $model->attributes = $_GET['BKLaporanrekappendapatanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAkhir']);
        }

        $this->render('rekapPendapatan/index', array(
            'model' => $model, 'filter' => $filter
        ));
    }

    public function actionPrintLaporanRekapPendapatan() {
        $model = new BKLaporanrekappendapatanV('search');
        $judulLaporan = 'Laporan Rekap Pendapatan';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Pendapatan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporanrekappendapatanV'])) {
            $model->attributes = $_REQUEST['BKLaporanrekappendapatanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapPendapatan/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionPrintLaporanRekapPendapatanDetail() {
        $model = new BKLaporanrekappendapatanV('search');
        $judulLaporan = 'Laporan Rekap Pendapatan Detail';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Pendapatan Detail';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporanrekappendapatanV'])) {
            $model->attributes = $_REQUEST['BKLaporanrekappendapatanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapPendapatan/_printDetail';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanRekapPendapatan() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanrekappendapatanV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Rekap Pendapatan';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanrekappendapatanV'])) {
            $model->attributes = $_GET['BKLaporanrekappendapatanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }

    /*
     * end BillingKasir->LaporanRekapPendapatan
     */

    /*
     * BillingKasir->LaporanRekapPiutang
     * by Miranitha Fasha - 30 Desember 2013
     */
        public function actionLaporanRekapPiutang() {
            $this->pageTitle = Yii::app()->name." - Laporan Rekap Piutang";
            $model = new BKLaporanrekappendapatanV('searchPiutang');
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');
            if (isset($_GET['BKLaporanrekappendapatanV'])) {
                $model->attributes = $_GET['BKLaporanrekappendapatanV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAkhir']);
            }

            $this->render('piutangPenjamin/index', array(
                'model' => $model,
            ));
        }

        public function actionPrintLaporanRekapPiutang() {
            $model = new BKLaporanrekappendapatanV;
            $model->is_print = true;
            if($_GET['filter_tab'] == "penjamin"){
                $penjamin = strtoupper($_GET['filter_tab'])." P3";
            }else{
                $penjamin = strtoupper($_GET['filter_tab']);
            }
            $judulLaporan = 'Laporan Rekap Piutang - '.$penjamin.'';

            //Data Grafik
            $data['title'] = 'Grafik Laporan Rekap Piutang Penjamin ';
            $data['type'] = $_REQUEST['type'];
            if (isset($_REQUEST['BKLaporanrekappendapatanV'])) {
                $model->attributes = $_REQUEST['BKLaporanrekappendapatanV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanrekappendapatanV']['tglAkhir']);
            }

            $caraPrint = $_REQUEST['caraPrint'];
            $target = 'piutangPenjamin/_print';

            $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
        }

        public function actionFrameGrafikLaporanRekapPiutang() {
            $this->layout = '//layouts/frameDialog';
            $model = new BKLaporanrekappendapatanV;
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d H:i:s');

            //Data Grafik
            $data['title'] = 'Grafik Laporan Rekap Piutang Penjamin';
            $data['type'] = $_GET['type'];
            if (isset($_GET['BKLaporanrekappendapatanV'])) {
                $model->attributes = $_GET['BKLaporanrekappendapatanV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanrekappendapatanV']['tglAkhir']);
            }

            $this->render('_grafik', array(
                'model' => $model,
                'data' => $data,
            ));
        }
    /*
     * end BillingKasir->LaporanRekapPiutang
     */

    /*
     * BillingKasir/Laporan/LaporanKinerja - Kinerja
     * by Miranitha Fasha - 31 Desember 2013
     */
    public function actionLaporanKinerja() {
        $model = new BKLaporankinerjapenunjangV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');
        if (isset($_GET['BKLaporankinerjapenunjangV'])) {
            $model->attributes = $_GET['BKLaporankinerjapenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporankinerjapenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporankinerjapenunjangV']['tglAkhir']);
            $tglAwal=$model->tglAwal;
            $tglAkhir = $model->tglAkhir;
        }

        $this->render('kinerja/index', array(
            'model' => $model, 'filter' => $filter,'tglAwal'=>$tglAwal,'tglAkhir'=>$tglAkhir,'kelaspelayanan_id'=>$model->kelaspelayanan_id
        ));
    }

    public function actionPrintLaporanKinerja() {
        $model = new BKLaporankinerjapenunjangV;
        $kelas_id = $_REQUEST['BKLaporankinerjapenunjangV']['kelaspelayanan_id'];
        $ruangan_id = $_REQUEST['BKLaporankinerjapenunjangV']['ruanganpenunj_id'];
        $kelas = KelaspelayananM::model()->findByPk($kelas_id);
        $ruangan = RuanganM::model()->findByPk($ruangan_id);
        $kelasNama = $kelas->kelaspelayanan_nama;
        if(empty($kelas_id) && empty($ruangan_id)){
            if($_REQUEST['filter_tab'] =="kelas"){
                $status = 'Per Kelas - All';
            }else if($_REQUEST['filter_tab'] =="bangsal"){
                $status = 'Per Ruangan - All';
            }else{
                $status = '';
            }
        }else if(empty($kelas_id) && !empty($ruangan_id)){
            $status = "Per Ruangan - ".$ruangan->ruangan_nama;
        }else if(!empty($kelas_id) && empty($ruangan_id)){
            $status = "Per Kelas - ".$kelas->kelaspelayanan_nama;
        }
        $judulLaporan = 'Laporan Kinerja '. $status;

        if($_REQUEST['caraPrint']== 'GRAFIK'){
            $model = new BKLaporankinerjapenunjangV;

            if (isset($_REQUEST['BKLaporankinerjapenunjangV'])) {
                $model->attributes = $_REQUEST['BKLaporankinerjapenunjangV'];
                $format = new CustomFormat();
                $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporankinerjapenunjangV']['tglAwal']);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporankinerjapenunjangV']['tglAkhir']);
            }
        }

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kinerja Per Kelas';
        $data['type'] = $_REQUEST['type'];

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'kinerja/_print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanKinerja() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporankinerjapenunjangV('search');
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Kinerja Per Kelas';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporankinerjapenunjangV'])) {
            $model->attributes = $_GET['BKLaporankinerjapenunjangV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporankinerjapenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporankinerjapenunjangV']['tglAkhir']);
        }

        $this->render('kinerja/_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
     /*
      * end BillingKasir/Laporan/LaporanKinerja - Kinerja
      */
    /*
     * Laporan Pembelian -> Pemindahan dari Modul Gudang Farmasi : Menu - Faktur Pembelian
     * @Miranitha Fasha - 05 Maret 2014
     */
    public function actionLaporanPembelian()
    {
        $model = new BKFakturpembelianT('searchLaporan');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        if (isset($_GET['BKFakturpembelianT'])) {
            $format = new CustomFormat;
            $model->attributes = $_GET['BKFakturpembelianT'];
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKFakturpembelianT']['tglAkhir']);
        }
        $this->render('fakturPembelianT/index',array(
            'model'=>$model,
            'tglAwal'=>$model->tglAwal,
            'tglAkhir'=>$model->tglAkhir,
        ));
    }

    public function actionPrintLaporanPembelian() {
        $model = new BKFakturpembelianT();
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $judulLaporan = "Laporan Faktur Pembelian";
        if($_GET['filter_tab'] == "rekap"){
            $judulLaporan = 'Total Faktur Pembelian';
            $data['title'] = 'Grafik Total Faktur Pembelian';
        }else if($_REQUEST['filter_tab'] == "detail"){
             $judulLaporan = 'Detail Faktur';
             $data['title'] = 'Grafik Detail Faktur';
        }

        //Data Grafik
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKFakturpembelianT'])) {
            $model->attributes = $_REQUEST['BKFakturpembelianT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKFakturpembelianT']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'fakturPembelianT/Print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionPrintDetailFakturPembelian($idFaktur = null) {
        $idFaktur = $idFaktur;
        $model = new BKFakturpembelianT();
        $modFaktur = BKFakturpembelianT::model()->findByPk($idFaktur);
        $modFakturDetail = BKFakturDetailT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFaktur));
        $modTerima = BKPenerimaanBarangT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFaktur));
        $modDetail = BKPenerimaanDetailT::model()->findAllByAttributes(array('penerimaanbarang_id'=>$modTerima->penerimaanbarang_id));

        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $judulLaporan = 'Detail Faktur';

        //Data Grafik
        $data['title'] = 'Grafik Detail Faktur';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKFakturpembelianT'])) {
            $model->attributes = $_REQUEST['BKFakturpembelianT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKFakturpembelianT']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'fakturPembelianT/detailPrint';

        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

        $this->layout = '//layouts/printWindows';
        $this->render($target, array('model' => $model, 'idFaktur'=>$idFaktur,'tglAwal'=>$model->tglAwal, 'modFaktur'=>$modFaktur,'tglAkhir'=>$model->tglAkhir,'modDetail'=>$modDetail,'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
    }

    public function actionFrameGrafikLaporanPembelian() {
        $this->layout = '//layouts/frameDialog';

        $model = new BKFakturpembelianT('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        //Data Grafik
        $data['title'] = 'Grafik Laporan Faktur Pembelian';
        $data['type'] = $_GET['type'];

        if (isset($_GET['BKFakturpembelianT'])) {
            $format = new CustomFormat();
            $model->attributes = $_GET['BKFakturpembelianT'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKFakturpembelianT']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKFakturpembelianT']['tglAkhir']);
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data'=>$data,
        ));
    }
    /*
     * End Laporan Pembelian
     */


	public function actionLaporanJasaRumahSakit()
	{
		$model = new BKBayarjasasaranarsV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $this->render('laporanJasaRumahSakit/index',array(
            'model'=>$model
        ));
	}

	public function actionLaporanJasaRumahSakitPrint()
	{
		$model = new BKBayarjasasaranarsV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $this->render('laporanJasaRumahSakit/index',array(
            'model'=>$model
        ));
	}

	public function actionLaporanParamedisRumahSakit()
	{
		$model = new BKBayarjasaparamedisV;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $this->render('laporanJasaRumahSakit/index',array(
            'model'=>$model
        ));
	}

	public function actionLaporanRemunRumahSakit()
	{
		$model = new BKBayarjasaremunparamedis2V;
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y 23:59:59');

        $this->render('laporanJasaRumahSakit/index',array(
            'model'=>$model
        ));
	}
/**
     * actionLaporanRekapNew
     */
    public function actionLaporanRekapBaru() {
        $this->pageTitle = Yii::app()->name." - Laporan Rekap Jasa Dokter";
	$filter = "";
        $model = new LaporanpendapatantindakanV();
        $model->tglAwal = date('Y-m-d 00:00:00');
        $model->tglAkhir = date('Y-m-d H:i:s');

        if (isset($_GET['LaporanpendapatantindakanV'])) {
            $model->attributes = $_GET['LaporanpendapatantindakanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['LaporanpendapatantindakanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['LaporanpendapatantindakanV']['tglAkhir']);
        }

        $this->render('rekapNew/index', array(
            'model' => $model, 'filter' => $filter
        ));

    }
    
    
    public function actionPrintLaporanRekapBaru() {
        $model = new LaporanpendapatantindakanV();
        $jasa = ucwords($_GET['filter_tab']);
        $judulLaporan = 'Laporan Rekap Jasa Dokter';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Pendapatan Tindakan';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['LaporanpendapatantindakanV'])) {
            $model->attributes = $_REQUEST['LaporanpendapatantindakanV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpendapatantindakanV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LaporanpendapatantindakanV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];

		$target = 'rekapNew/printRekap';
        $caraPrint = $_REQUEST['caraPrint'];
		if(isset($_GET['filter_tab'])){
			if($_GET['filter_tab'] == 'rekap'){
				$target = 'rekapNew/printRekap';
			}else{
				$target = 'rekapNew/printDetail';
			}
		}
        $target = $target;

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
}
