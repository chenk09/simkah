<?php

class DaftarPenerimaanUmumController extends SBaseController
{
	public function actionIndex()
	{
            $modPenerimaan = new BKPenerimaanUmumT;
            $format = new CustomFormat();
            $modPenerimaan->tglAwal=date('Y-m-d 00:00:00');
            $modPenerimaan->tglAkhir=date('Y-m-d H:i:s');
		
            if(isset($_GET['BKPenerimaanUmumT'])){
                $modPenerimaan->attributes=$_GET['BKPenerimaanUmumT'];
                $modPenerimaan->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPenerimaanUmumT']['tglAwal']);
                $modPenerimaan->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPenerimaanUmumT']['tglAkhir']);
            }
            
            $this->render('index', array('modPenerimaan'=>$modPenerimaan));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}