<?php

class LaporanKasHarianController extends SBaseController {
    
    public function actionLaporanKasHarian() {
        $model = new BKClosingkasirT('search');
        $this->pageTitle = Yii::app()->name.' - '.'Laporan Laboratorium';
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['BKClosingkasirT'])) {
            $model->attributes = $_GET['BKClosingkasirT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKClosingkasirT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKClosingkasirT']['tglAkhir']);
        }

        $this->render('rekapKas/index', array(
                        'model' => $model,
        ));
    }

    public function actionPrintLaporanKasHarian() {
//        print_r(12);
        $format = new CustomFormat();
        $criteria=new CDbCriteria;

        $criteria->select = 't.closingkasir_id, rincianclosing_t.closingkasir_id,
                            sum(t.terimauangmuka) as terimauangmuka,
                            sum(rincianclosing_t.jumlahuang) as jumlahuang,
                            sum(t.piutang) as piutang, 
                            sum(t.terimauangpelayanan) as uangpelayanan,
                            sum(rincianclosing_t.jumlahuang + t.piutang) as total,
                            t.keterangan_closing, 
                            sum(t.totalsetoran) as totalsetoran, 
                            sum(t.totalpengeluaran) as totalpengeluaran,t.create_ruangan';
        $criteria->group = 't.closingkasir_id, t.create_ruangan, rincianclosing_t.closingkasir_id, t.keterangan_closing';
        $criteria->addBetweenCondition('t.tglclosingkasir',$format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAwal']),$format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAkhir']));
        
        $criteria->compare('t.create_ruangan',Yii::app()->user->getState('ruangan_id'));
        $criteria->join ='LEFT JOIN rincianclosing_t ON rincianclosing_t.closingkasir_id = t.closingkasir_id LEFT JOIN tandabuktibayar_t 
                          ON tandabuktibayar_t.closingkasir_id = t.closingkasir_id';
        
        $criteria3 = new CDbCriteria;

        $criteria3->select = 't.closingkasir_id, rincianclosing_t.closingkasir_id,pendaftaran_t.pendaftaran_id, pasien_m.pasien_id, pendaftaran_t.no_pendaftaran, pasien_m.nama_pasien,
                            sum(t.terimauangmuka) as terimauangmuka,
                            sum(rincianclosing_t.jumlahuang) as jumlahuang,
                            sum(t.piutang) as piutang, 
                            sum(t.terimauangpelayanan) as uangpelayanan,
                            sum(rincianclosing_t.jumlahuang + t.piutang) as total,
                            t.keterangan_closing, 
                            sum(t.totalsetoran) as totalsetoran, 
                            sum(t.totalpengeluaran) as totalpengeluaran, t.create_ruangan';
        $criteria3->group = 't.closingkasir_id, t.create_ruangan, rincianclosing_t.closingkasir_id, t.keterangan_closing,pendaftaran_t.pendaftaran_id, pendaftaran_t.no_pendaftaran, pasien_m.pasien_id, pasien_m.nama_pasien';
        $criteria3->compare('t.create_ruangan',Yii::app()->user->getState('ruangan_id'));
        $criteria3->addBetweenCondition('t.tglclosingkasir',$format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAwal']),$format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAkhir']));
        $criteria3->join ='LEFT JOIN rincianclosing_t ON rincianclosing_t.closingkasir_id = t.closingkasir_id LEFT JOIN tandabuktibayar_t 
                          ON tandabuktibayar_t.closingkasir_id = t.closingkasir_id LEFT JOIN pembayaranpelayanan_t ON 
                          pembayaranpelayanan_t.tandabuktibayar_id = tandabuktibayar_t.tandabuktibayar_id LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = pembayaranpelayanan_t.pendaftaran_id
                          LEFT JOIN pasien_m ON pasien_m.pasien_id = pendaftaran_t.pasien_id';
        
        $model      = BKClosingkasirT::model()->findAll($criteria);
        $modDetail  = BKClosingkasirT::model()->findAll($criteria3);
        
//        echo print_r($modDetail);
        $criteria2=new CDbCriteria;

//        $criteria2->select = 't.closingkasir_id, rincianclosing_t.closingkasir_id, t.keterangan_closing, rincianclosing_t.nourutrincian,rincianclosing_t.nilaiuang,rincianclosing_t.banyakuang,rincianclosing_t.jumlahuang';
        $criteria2->compare('closingkasir_id',$model->closingkasir_id);
//        $criteria2->addBetweenCondition('t.tglclosingkasir',$format->formatDateTimeMediumForDB($_REQUEST['LKClosingkasirT']['tglAwal']),$format->formatDateTimeMediumForDB($_REQUEST['LKClosingkasirT']['tglAkhir']));
//        $criteria2->join ='LEFT JOIN rincianclosing_t ON rincianclosing_t.closingkasir_id = t.closingkasir_id LEFT JOIN tandabuktibayar_t 
//                          ON tandabuktibayar_t.closingkasir_id = t.closingkasir_id';
        $model      = BKClosingkasirT::model()->findAll($criteria);
        $modDetail  = BKClosingkasirT::model()->findAll($criteria3);
        $modRincian = new RincianclosingT;
        
        $data = array();
        //Data Grafik
        $data['title'] = 'Grafik Laporan Kas Harian';
        $data['type'] = $_REQUEST['type'];
//        if (isset($_REQUEST['LKClosingkasirT'])) {
//            $model->attributes = $_REQUEST['LKClosingkasirT'];
//            $format = new CustomFormat();
//            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['LKClosingkasirT']['tglAwal']);
//            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['LKClosingkasirT']['tglAkhir']);
//        }
//        echo " test";
        
            if(isset($_REQUEST['BKClosingkasirT'])){
                if($_REQUEST['filter_tab'] == 'rekap')
                {
                    $judulLaporan = 'REKAPITULASI LAPORAN PENDAPATAN HARIAN';
                    $data['filter'] = "rekap";
                    
                }
                else if($_GET['filter_tab'] == 'detail')
                {
                    $judulLaporan = 'LAPORAN DETAIL PEMBAYARAN';
                    $data['filter'] = "detail";
                    
                }
                $model->attributes = $_REQUEST['BKClosingkasirT'];
                $format = new CustomFormat();
                if(!empty($_REQUEST['BKClosingkasirT']['tglAwal']))
                {
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAwal']);
                }
                if(!empty($_REQUEST['BKClosingkasirT']['tglAwal']))
                {
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKClosingkasirT']['tglAkhir']);
                }
            }
        
//        $data['caraPrint'] = $_REQUEST['caraPrint'];
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'rekapKas/_print';
        
        $this->printFunction($model,$modDetail, $data, $caraPrint, $judulLaporan, $target,$modRincian,$rincianUang);
    }

    public function actionFrameGrafikKasHarian() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKClosingkasirT('search');
        $model->tglAwal = date('d M Y 00:00:00');
        $model->tglAkhir = date('d M Y H:i:s');
        
        //Data Grafik
        $data['title'] = 'Grafik Laporan Sensus Harian';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['BKClosingkasirT'])) {
            $model->attributes = $_GET['BKClosingkasirT'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKClosingkasirT']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKClosingkasirT']['tglAkhir']);
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $modDetail, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
//        echo $caraPrint;
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint , 'modRincian'=>$modRincian));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('modDetail'=>$modDetail,'rincianUang'=>$rincianUang,'model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint, 'modRincian'=>$modRincian), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
    
 public function getTabularFormTabs($form, $model)
{
    $tabs = array();
    $count = 0;
    foreach (array('en'=>'English', 'fi'=>'Finnish', 'sv'=>'Swedish') as $locale => $language)
    {
        $tabs[] = array(
            'active'=>$count++ === 0,
            'label'=>$language,
            'content'=>$this->renderPartial('rekapTransaksiBedah/_table', array('form'=>$form, 'model'=>$model, 'locale'=>$locale, 'language'=>$language), true),
        );
    }
    return $tabs;
}
}