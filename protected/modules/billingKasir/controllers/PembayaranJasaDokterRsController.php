
<?php
ini_set('memory_limit','-1');
class PembayaranJasaDokterRsController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'create';
        public $suksesSimpanDetail = false;

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id = null){
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                $format = new CustomFormat;
                $model=new BKPembayaranjasaT;
                $modTandabukti=new BKTandabuktikeluarT;
                $model->tglbayarjasa = date('d M Y H:i:s');
                $model->nobayarjasa = MyGenerator::noBayarJasa();
                $modDetails= new BKPembjasadetailT;
                $dataDetails=array();
                if(!empty($id)){
                    $model = BKPembayaranjasaT::model()->findByPk($id);
                    $model->pegawaiNama = $model->pegawai->NamaLengkap;
                    $model->tglAwalPendaftaran = date('d M Y', strtotime($model->periodejasa));
                    $model->tglAkhirPendaftaran = date('d M Y', strtotime($model->sampaidgn));
                    $model->tglAwalPenunjang = date('d M Y', strtotime($model->periodejasa));
                    $model->tglAkhirPenunjang = date('d M Y', strtotime($model->sampaidgn));
                    $modDetailsLoad=BKPembjasadetailT::model()->findAllByAttributes(array('pembayaranjasa_id'=>$model->pembayaranjasa_id));
                    foreach($modDetailsLoad AS $i => $data){
                        $attributes = $data->attributeNames();
                        if($i < 20){
                            foreach($attributes as $j=>$attribute) {
                                $dataDetails[$i]["$attribute"] = $data->$attribute;
                                $dataDetails[$i]["penjaminId"] = $data->pendaftaran->penjamin_id;
                            }
                        }else{
                            $dataDetails[$i]["no_masukpenunjang"] = "-- Tidak ditampilkan --";
                            break;
                        }
                    }
                }
//                    SEMENTARA DI BATASI KARENA AKAN BERAT JIKA DATA RATUSAN
//                    DIPERBAIKI MENGGUNAKAN CGRIDVIEW AGAR ADA PAGINATION
                
		if(isset($_POST['BKPembayaranjasaT']) && isset($_POST['BKPembjasadetailT']))
		{
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
						$model->attributes=$_POST['BKPembayaranjasaT'];
						$model->pegawaiNama=$_POST['BKPembayaranjasaT']['pegawaiNama'];
						$model->rujukandariNama=$_POST['BKPembayaranjasaT']['rujukandariNama'];
						
                        $model->tglbayarjasa = date('Y-m-d H:i:s');
                        $model->nobayarjasa = MyGenerator::noBayarJasa();
                        $model->tglAwalPendaftaran = !empty($_POST['BKPembayaranjasaT']['tglAwalPenunjang']) ? $_POST['BKPembayaranjasaT']['tglAwalPenunjang'] : $_POST['BKPembayaranjasaT']['tglAwalPendaftaran'];
                        $model->tglAkhirPendaftaran = !empty($_POST['BKPembayaranjasaT']['tglAkhirPenunjang']) ? $_POST['BKPembayaranjasaT']['tglAkhirPenunjang'] : $_POST['BKPembayaranjasaT']['tglAkhirPendaftaran'];
                        $model->periodejasa = $format->formatDateMediumForDB($model->tglAwalPendaftaran);
                        $model->sampaidgn = $format->formatDateMediumForDB($model->tglAkhirPendaftaran);
                        $model->create_time = date('Y-m-d H:i:s');
                        $model->create_loginpemakai_id = Yii::app()->user->id;
                        $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
                        //Tanda Bukti Keluar
                        $modTandabukti->attributes = NULL;
                        $modTandabukti->tglkaskeluar = date('d M Y H:i:s');
                        $modTandabukti->nokaskeluar = MyGenerator::noKasKeluar();
                        $modTandabukti->namapenerima = (empty($model->rujukandari_id)) ? $model->pegawai->NamaLengkap : $model->rujukandari->namaperujuk;
                        $modTandabukti->shift_id = Yii::app()->user->getState('shift_id');
                        $modTandabukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $modTandabukti->tahun = date('Y');
                        $modTandabukti->jmlkaskeluar = $model->totalbayarjasa;
                        $modTandabukti->biayaadministrasi = 0;
                        $modTandabukti->carabayarkeluar = "TUNAI";
                        $modTandabukti->untukpembayaran = "Jasa Dokter";
                        $modTandabukti->alamatpenerima = (empty($model->rujukandari_id)) ? $model->pegawai->alamat_pegawai : $model->rujukandari->alamatlengkap;
                        if(empty($modTandabukti->alamatpenerima)){
                            $modTandabukti->alamatpenerima = (empty($model->rujukandari_id)) ? "Dokter RS Jasa Kartini" : "Rujukan Luar";
                        }
                        if($model->validate() && $modTandabukti->save()){
                            $model->tandabuktikeluar_id = $modTandabukti->tandabuktikeluar_id;
                            $model->save();
                            $dataDetails = $this->simpanDetail($model, $modDetails, $_POST['BKPembjasadetailT']);
                            $updateKomponen = $this->updateTindakankomponen($model, $dataDetails);
                            if($this->suksesSimpanDetail == true){ // && $updateKomponen == true BELUM TERUJI
                                $transaction->commit();
                                Yii::app()->user->setFlash('success', '<strong>Berhasil!</strong> Data berhasil disimpan.');
                                $this->redirect(array('create','id'=>$model->pembayaranjasa_id));
                            }else{
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan. Silahkan cek kembali tabel detail.');
                            }
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', '<strong>Gagal!</strong> Data gagal disimpan!');
                        }
                    }catch (Exception $exc) {
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        $transaction->rollback();
                    }
		}
		$this->render('create',array(
			'model'=>$model,
			'modDetails'=>$modDetails,
			'dataDetails'=>$dataDetails,
		));
	}
        
        protected function simpanDetail($model, $modDetails, $posts){

            if(count($posts) > 0){
                $saveDetails = array();
                $this->suksesSimpanDetail = true;
                
                foreach($posts AS $i => $post){
                    if($post['pilihDetail'] == true){
                        $saveDetails[$i] = new $modDetails;
                        $saveDetails[$i]->attributes = $post;
                        $saveDetails[$i]->pembayaranjasa_id = $model->pembayaranjasa_id;
                        $saveDetails[$i]->penjaminId = $post['penjaminId'];
                        if($saveDetails[$i]->save()){
                            $this->suksesSimpanDetail = $this->suksesSimpanDetail && true;
                        }else{
                            $this->suksesSimpanDetail = false;
                        }
                    }
                }
            }
            return $saveDetails;
        }
        protected function updateTindakankomponen($model, $dataDetails){
            $sukses = true;
            if(count($dataDetails) > 0){
                foreach($dataDetails AS $i => $data){
                    $criteria = new CDbCriteria();
                    $criteria->with = array('tindakanpelayanan');
                    $criteria->addCondition('tindakanpelayanan.pasien_id = '.$data['pasien_id']);
                    $criteria->addCondition('tindakanpelayanan.pendaftaran_id = '.$data['pendaftaran_id']);
                    if($_POST['tab'] == Params::PEMBAYARAN_JASA_MEDIS){
                        $criteria->addCondition('tindakanpelayanan.dokterpemeriksa1_id = '.$model->pegawai_id);
                        $criteria->addCondition('t.komponentarif_id = '.Params::KOMPONENTARIF_ID_MEDIS);
                    }else if($_POST['tab'] == Params::PEMBAYARAN_JASA_RUJUKAN){
                        $criteria->addCondition('tindakanpelayanan.pasienmasukpenunjang_id = '.$data['pasienmasukpenunjang_id']);
                        $criteria->addCondition('tindakanpelayanan.dokterpemeriksa2_id = '.$model->pegawai_id);
                        $criteria->addCondition('t.komponentarif_id = '.Params::KOMPONENTARIF_ID_PERUJUK);
                    }else if($_POST['tab'] == Params::PEMBAYARAN_JASA_ANASTESI){
                        $criteria->addCondition('tindakanpelayanan.dokteranastesi_id = '.$model->pegawai_id);
                        $criteria->addCondition('t.komponentarif_id = '.Params::KOMPONENTARIF_ID_ANASTESI);
                    }
                    $modKomponens = TindakankomponenT::model()->findAll($criteria);
                    if(count($modKomponens) > 0){
                        foreach($modKomponens AS $i => $komponen){
                            $komponen->pembayaranjasa_id = $model->pembayaranjasa_id;
                            if($komponen->save())
                                $sukses = $sukses && true;
                            else
                                $sukses = false;
                        }
                    }
                }
            }
            return $sukses;
        }
        
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('create'));
	}

	/**
	 * Manages all models.
	 */
	public function actionInformasi()
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$format = new CustomFormat();
                $model=new BKPembayaranjasaT('searchInformasi');
		$model->unsetAttributes();  // clear any default values
                $model->tglAwal=date('d M Y');
                $model->tglAkhir=date('d M Y');
		if(isset($_GET['BKPembayaranjasaT'])){
			$model->attributes=$_GET['BKPembayaranjasaT'];
			$model->noKasKeluar=$_GET['BKPembayaranjasaT']['noKasKeluar'];
			$model->namaPerujuk=$_GET['BKPembayaranjasaT']['namaPerujuk'];
			$model->namaDokter=$_GET['BKPembayaranjasaT']['pegawai_id'];
			$model->tglAwal=$format->formatDateMediumForDB($_GET['BKPembayaranjasaT']['tglAwal']);
			$model->tglAkhir=$format->formatDateMediumForDB($_GET['BKPembayaranjasaT']['tglAkhir']);
                }
                
		$this->render('informasi',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BKPembayaranjasaT::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='bkpembayaranjasa-t-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
//                SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
//                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        public function actionLihatDetail($id){
            $this->layout='//layouts/frameDialog';
            $model=BKPembayaranjasaT::model()->findByPk($id);
            $modDetail = new BKPembjasadetailT;
            $modDetail->unsetAttributes();
            $modDetail->pembayaranjasa_id = $model->pembayaranjasa_id;
            $this->render('Print',array('model'=>$model, 'modDetail'=>$modDetail,'judulLaporan'=>$judulLaporan,'frame'=>true));
        }
        public function actionPrint($id, $caraPrint = null)
        {
            $model=BKPembayaranjasaT::model()->findByPk($id);
            $modDetail = new BKPembjasadetailT;
            $modDetail->unsetAttributes();
            $modDetail->pembayaranjasa_id = $model->pembayaranjasa_id;
            $judulLaporan='Bukti Pembayaran Jasa Dokter';
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('Print',array('model'=>$model, 'modDetail'=>$modDetail,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('Print',array('model'=>$model, 'modDetail'=>$modDetail,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('Print',array('model'=>$model, 'modDetail'=>$modDetail,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
