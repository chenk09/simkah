<?php

class DaftarPasienController extends SBaseController
{
    public $defaultAction = 'pasienRJ';
    
	public function actionIndex()
	{
		$this->render('index');
	}

    //buat pull tanggal 2 may 2014

	public function actionPasienRD()
	{
                $format = new CustomFormat();
                $modRD = new BKInformasikasirrdpulangV;
                $modRD->tglAwal = date("Y-m-d").' 00:00:00';
                $modRD->tglAkhir = date('Y-m-d H:i:s');
                
                if(isset($_GET['BKInformasikasirrdpulangV'])){
                    $modRD->attributes = $_GET['BKInformasikasirrdpulangV'];
                    if(!empty($_GET['BKInformasikasirrdpulangV']['tglAwal']))
                    {
                        $modRD->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrdpulangV']['tglAwal']);
                    }
                    if(!empty($_GET['BKInformasikasirrdpulangV']['tglAwal']))
                    {
                        $modRD->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrdpulangV']['tglAkhir']);
                    }
                }
                
		$this->render('pasienRD',array('modRD'=>$modRD));
	}

	public function actionPasienRI()
	{
                $format = new CustomFormat();
                $modRI = new BKInformasikasirinappulangV;
                $modRI->tglAwal = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhir = date('Y-m-d H:i:s');
                $modRI->tglAwalAdmisi = date("Y-m-d").' 00:00:00';
                $modRI->tglAkhirAdmisi = date('Y-m-d H:i:s');
                
                if(isset($_GET['BKInformasikasirinappulangV'])){
                    $modRI->attributes = $_GET['BKInformasikasirinappulangV'];
                    if(!empty($_GET['BKInformasikasirinappulangV']['tglAwal'])){
                        $modRI->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAwal']);
                    }
                    if(!empty($_GET['BKInformasikasirinappulangV']['tglAkhir'])){
                        $modRI->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAkhir']);
                    }
                    $modRI->ceklis = $_GET['BKInformasikasirinappulangV']['ceklis'];
//                    if($modRI->ceklis==1){
                        $modRI->tglAwalAdmisi = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAwalAdmisi']);
                        $modRI->tglAkhirAdmisi = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAkhirAdmisi']);
//                    }
                }
                if (Yii::app()->request->isAjaxRequest) {
                    echo $this->renderPartial('_tablePasienRI', array('modRI'=>$modRI),true);
                }else{
                    $this->render('pasienRI',array('modRI'=>$modRI));
                }
                
	}

	public function actionPasienRawatJalan()
	{
		$format = new CustomFormat();
		$modRJ = new BKInformasikasirrawatjalansiappulangV;
		$modRJ->tglAwal = date("Y-m-d").' 00:00:00';
		$modRJ->tglAkhir = date('Y-m-d H:i:s');
		
		if(isset($_GET['BKInformasikasirrawatjalansiappulangV'])){
			$modRJ->attributes = $_GET['BKInformasikasirrawatjalansiappulangV'];
			if(!empty($_GET['BKInformasikasirrawatjalansiappulangV']['tglAwal']))
			{
				$modRJ->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalansiappulangV']['tglAwal']);
			}
			if(!empty($_GET['BKInformasikasirrawatjalansiappulangV']['tglAwal']))
			{
				$modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalansiappulangV']['tglAkhir']);
			}
		}
                
		$this->render('pasienRajalSiapPulang',array('modRJ'=>$modRJ));
	}
	
	public function actionPasienRJ()
	{
		$format = new CustomFormat();
		$modRJ = new BKInformasikasirrawatjalanV;
		$modRJ->tglAwal = date("Y-m-d").' 00:00:00';
		$modRJ->tglAkhir = date('Y-m-d H:i:s');
		
		if(isset($_GET['BKInformasikasirrawatjalanV'])){
			$modRJ->attributes = $_GET['BKInformasikasirrawatjalanV'];
			if(!empty($_GET['BKInformasikasirrawatjalanV']['tglAwal']))
			{
				$modRJ->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalanV']['tglAwal']);
			}
			if(!empty($_GET['BKInformasikasirrawatjalanV']['tglAwal']))
			{
				$modRJ->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalanV']['tglAkhir']);
			}
		}
                
		$this->render('pasienRJ',array('modRJ'=>$modRJ));
	}

	public function actionPasienKarcis()
	{
            $format = new CustomFormat();
            $model = new BKTindakanPelayananT('searchPasienKarcis');
            
            if(isset($_GET['BKTindakanPelayananT'])){
                    $model->attributes = $_GET['BKTindakanPelayananT'];
                    $model->no_pendaftaran = $_GET['BKTindakanPelayananT']['no_pendaftaran'];
                    $model->no_rekam_medik = $_GET['BKTindakanPelayananT']['no_rekam_medik'];
                    $model->nama_pasien = $_GET['BKTindakanPelayananT']['nama_pasien'];
                    $model->nama_bin = $_GET['BKTindakanPelayananT']['nama_bin'];
                    if(!empty($_GET['BKTindakanPelayananT']['tglAwal']))
                    {
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKTindakanPelayananT']['tglAwal']);
                    }
                    if(!empty($_GET['BKTindakanPelayananT']['tglAwal']))
                    {
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKTindakanPelayananT']['tglAkhir']);
                    }
                }
            
            $this->render('pasienKarcis',array('model'=>$model));
	}

	public function actionPasienDeposit()
	{
            $format = new CustomFormat();
            $model = new BKBayaruangmukaT('searchPasienDeposit');
            $model->tglAwal = date('Y-m-d 00:00:00');
            $model->tglAkhir = date('Y-m-d 23:59:00');
            if(isset($_GET['BKBayaruangmukaT'])){
                    $model->attributes = $_GET['BKBayaruangmukaT'];
                    $model->no_pendaftaran = $_GET['BKBayaruangmukaT']['no_pendaftaran'];
                    $model->no_rekam_medik = $_GET['BKBayaruangmukaT']['no_rekam_medik'];
                    $model->nama_pasien = $_GET['BKBayaruangmukaT']['nama_pasien'];
                    $model->nama_bin = $_GET['BKBayaruangmukaT']['nama_bin'];
                    if(!empty($_GET['BKBayaruangmukaT']['tglAwal']))
                    {
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKBayaruangmukaT']['tglAwal']);
                    }
                    if(!empty($_GET['BKBayaruangmukaT']['tglAwal']))
                    {
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKBayaruangmukaT']['tglAkhir']);
                    }
                }
            
            $this->render('pasienDeposit',array('model'=>$model));
	}

        /**
         * url to see all pasien that already pay
         * used in :
         * 1. Billing Kasir -> informasi pasien sudah bayar
         */
	public function actionPasienSudahBayar()
	{
                    $format = new CustomFormat();
                    $model = new BKInformasipasiensudahbayarV('searchInformasi');
                    
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d 23:59:59');

                    if(isset($_GET['BKInformasipasiensudahbayarV'])){
                            $model->attributes = $_GET['BKInformasipasiensudahbayarV'];
                            if(!empty($_GET['BKInformasipasiensudahbayarV']['tglAwal']))
                            {
                                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasipasiensudahbayarV']['tglAwal']);
                            }
                            if(!empty($_GET['BKInformasipasiensudahbayarV']['tglAwal']))
                            {
                                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasipasiensudahbayarV']['tglAkhir']);
                            }
                        }

                    $this->render('pasienSudahBayar',array('model'=>$model));
	}

	public function actionPasienBerhutang()
	{
                    $format = new CustomFormat();
                    $model = new BKRinciantagihanpasienberhutangV('searchRincianPasienBerhutang');
                    $model->tglAwal = date('Y-m-d 00:00:00');
                    $model->tglAkhir = date('Y-m-d H:i:s');
                    if(isset($_GET['BKRinciantagihanpasienberhutangV'])){
                            $model->attributes = $_GET['BKRinciantagihanpasienberhutangV'];
                            $model->no_pendaftaran = $_GET['BKRinciantagihanpasienberhutangV']['no_pendaftaran'];
                            $model->no_rekam_medik = $_GET['BKRinciantagihanpasienberhutangV']['no_rekam_medik'];
                            $model->nama_pasien = $_GET['BKRinciantagihanpasienberhutangV']['nama_pasien'];
                            $model->nama_bin = $_GET['BKRinciantagihanpasienberhutangV']['nama_bin'];
                            $model->penjamin_id = $_GET['BKRinciantagihanpasienberhutangV']['penjamin_id'];
                            if(!empty($_GET['BKRinciantagihanpasienberhutangV']['tglAwal']))
                            {
                                $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKRinciantagihanpasienberhutangV']['tglAwal']);
                            }
                            if(!empty($_GET['BKRinciantagihanpasienberhutangV']['tglAwal']))
                            {
                                $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKRinciantagihanpasienberhutangV']['tglAkhir']);
                            }
                        }

                    $this->render('pasienBerhutang',array('model'=>$model));
	}

        public function actioninformasiPenanggung($id)
        {
            $this->layout = '//layouts/frameDialog';
            $model = BKInformasikasirrawatjalanV::model()->findByAttributes(array('no_pendaftaran'=>$id));
            $modPenanggungJawab = PenanggungjawabM::model()->findByPk($model->penanggungjawab_id);
            $this->render('informasiPenanggung',
                array(
                    'model'=>$modPenanggungJawab
                )
            );
        }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        
        public function actionDetailKasMasuk($idPembayaran)
        {
            $this->layout = '//layouts/frameDialog';
            $format = new CustomFormat;
            $criteria = new CDbCriteria();
//            $criteria->addCondition('pendaftaran_id = '.$idPendaftaran);
            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaran);
            $criteria->addCondition('tindakansudahbayar_id IS NOT NULL'); //sudah lunas
            $criteria->order = 'ruangan_id';
            $detail = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
            $func = new MyFunction;
            
            $no_bkm = '';
            $tgl_bkm = '';
            $pembayar = '';
            $total_bayar = '';
            $total_bayar_huruf = '';
            
            $rec = array();
            $totalbiayaadminfarmasi=0;
            $jml_field = count($detail)-1;
            foreach($detail as $key=>$val)
            {
                $data[] = null;
                $data['tglpembayaran'] = substr($format->formatDateINAShort($val->getTandaBukti("tglbuktibayar")),0,12);
                $data['keterangan'] = $val->daftartindakan_nama;
                $data['jumlah'] = $val->SubTotal;
                
                $total_bayar += $data['jumlah'];
                $no_bkm = $val->getTandaBukti("nobuktibayar");
                $tgl_bkm = $val->getTandaBukti("tglbuktibayar");
                $pembayar = $val->getTandaBukti("darinama_bkm");
                $rec[] = $data;

                $totalbiayaadminfarmasi += ($val->biayaadministrasi + $val->biayaservice + $val->biayakonseling) ;
                if($jml_field == $key){
                    if($totalbiayaadminfarmasi!=0){
                        $data['tglpembayaran'] = substr($format->formatDateINAShort($val->getTandaBukti("tglbuktibayar")),0,12);
                        $data['keterangan'] = 'YANFAR';
                        $data['jumlah'] = $totalbiayaadminfarmasi;
                        $rec[] = $data;
                    }
                }
            }
            
            $data = array(
                'header'=>array(
                    'no_bkm'=>$no_bkm,
                    'tgl_bkm'=>$tgl_bkm,
                    'total_bayar'=>$total_bayar+$totalbiayaadminfarmasi,
                    'total_bayar_huruf'=>$func->terbilang($total_bayar+$totalbiayaadminfarmasi),
                    'pembayar'=>$pembayar,
                ),
                'detail'=>$rec,
                'footer'=>123,
            );
            $action_url = Yii::app()->createAbsoluteUrl(
                Yii::app()->controller->module->id .'/'. Yii::app()->controller->id .'/print' . Yii::app()->controller->action->id,
                array(
                    'idPembayaran'=>$idPembayaran
                )
            );
            
            $this->render('detailKasMasuk',
                array(
                    'data'=>$data,
                    'actionUrl'=>$action_url,
                    'totalbiayaadminfarmasi'=>$totalbiayaadminfarmasi,   
                )
            );
        }
        
        public function actionPrintDetailKasMasuk($idPembayaran, $caraPrint)
        {
            
            $format = new CustomFormat;
            $criteria = new CDbCriteria();
//            $criteria->addCondition('pendaftaran_id = '.$idPendaftaran);
            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaran);
            $criteria->addCondition('tindakansudahbayar_id IS NOT NULL'); //sudah lunas
            $criteria->order = 'ruangan_id';
            $detail = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
            $func = new MyFunction;
            
            $no_bkm = '';
            $tgl_bkm = '';
            $pembayar = '';
            $total_bayar = '';
            $total_bayar_huruf = '';
            
            $rec = array();
            foreach($detail as $key=>$val)
            {
                $data[] = null;
                $data['tglpembayaran'] = substr($format->formatDateINAShort($val->getTandaBukti("tglbuktibayar")),0,12);
                $data['keterangan'] = $val->daftartindakan_nama;
                $data['jumlah'] = $val->SubTotal;
                
                $total_bayar += $data['jumlah'];
                $no_bkm = $val->getTandaBukti("nobuktibayar");
                $tgl_bkm = $val->getTandaBukti("tglbuktibayar");
                $pembayar = $val->getTandaBukti("darinama_bkm");
                
                $rec[] = $data;
            }
            
            $data = array(
                'header'=>array(
                    'no_bkm'=>$no_bkm,
                    'tgl_bkm'=>$tgl_bkm,
                    'total_bayar'=>$func->formatNumber($total_bayar),
                    'total_bayar_huruf'=>$func->terbilang($total_bayar),
                    'pembayar'=>$pembayar,
                ),
                'detail'=>$rec,
                'footer'=>123,
            );
            if($caraPrint == 'PRINT')
            {
                $this->layout='//layouts/printWindows';
                $this->render('detailKasMasuk',
                    array(
                        'data'=>$data,
                        'caraPrint'=>$caraPrint
                    )
                );
            }else{
                $this->layout = '//layouts/frameDialog';
                $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet, 1);  
                $mpdf->AddPage($posisi,'','','','',5,5,25,5);
                $mpdf->WriteHTML(
                    $this->render('detailKasMasuk',
                        array(
                            'data'=>$data,
                            'caraPrint'=>$caraPrint
                        ),true
                    )
                );
                $mpdf->Output();                
            }
        }        
}