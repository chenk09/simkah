<?php
class PembayaranPasienController extends SBaseController{
	public $layout='//layouts/column1';
	public $defaultAction = 'index';
	
	public function actionIndex(){
		$format = new CustomFormat();
		$model = new BKInformasikasirrawatjalansiappulangV;
		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));
		
		$model->statusfarmasi = 't';
		
		if(isset($_GET["BKInformasikasirrawatjalansiappulangV"])){
			$model->attributes = $_GET["BKInformasikasirrawatjalansiappulangV"];
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalansiappulangV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrawatjalansiappulangV']['tglAkhir']);
		}
		$this->render('index',array(
			"model"=>$model
		));
	}
	
	public function actionRawatInap(){
		$format = new CustomFormat();
		$model = new BKInformasikasirinappulangV;
		
		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));
		
		$model->statusfarmasi = 't';
		
		if(isset($_GET["BKInformasikasirinappulangV"])){
			$model->attributes = $_GET["BKInformasikasirinappulangV"];
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirinappulangV']['tglAkhir']);
		}
		
		$this->render('rawat_inap',array(
			"model"=>$model
		));
	}
	
	public function actionRawatDarurat(){
		$format = new CustomFormat();
		$model = new BKInformasikasirrdpulangV;
		
		$model->tglAwal      = date('Y-m-d H:i:s', strtotime('-7 days', strtotime(date("Y-m-d"))));
		$model->tglAwal		 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss','medium',null));

		$model->tglAkhir     = date('Y-m-d H:i:s');
		$model->tglAkhir	 = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss','medium',null));
		
		$model->statusfarmasi = 't';
		
		if(isset($_GET["BKInformasikasirrdpulangV"])){
			$model->attributes = $_GET["BKInformasikasirrdpulangV"];
			$model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrdpulangV']['tglAwal']);
			$model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKInformasikasirrdpulangV']['tglAkhir']);
		}		
		
		$this->render('rawat_darurat',array(
			"model"=>$model
		));
	}
	
	protected function rincianTagihan($data, $row, $dataColumn){
		return CHtml::Link("<i class=\"icon-list-alt\"></i>",
			Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
			array(
				"class"=>"", 
				"target"=>"iframeRincianTagihan",
				"onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
				"rel"=>"tooltip",
				"title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
			)
		);
	}
	
	protected function rincianTindakan($data, $row, $dataColumn){
		return CHtml::Link("<i class=\"icon-list-alt\"></i>",
			Yii::app()->controller->createUrl("RinciantagihanpasienV/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
			array(
				"class"=>"", 
				"target"=>"iframeRincianTagihan",
				"onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
				"rel"=>"tooltip",
				"title"=>"Klik untuk melihat Rincian Tagihan",
			)
		);
	}
	
	protected function bayarTagihan($data, $row, $dataColumn){
		$instalasi = "";
		switch($data->instalasi_id){
			case 2 : {$instalasi = 'RJ';break;}
			case 3 : {$instalasi = 'RD';break;}
			case 4 : {$instalasi = 'RI';break;}
		}		
		return CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->controller->createUrl("pembayaran/index",array("idPendaftaran"=>$data->pendaftaran_id,"frame"=>true,"status"=>"RJ")),
			array(
				"class"=>"", 
				"target"=>"iframePembayaran",
				"rel"=>"tooltip",
				"onclick"=>"cekStatusFarmasi('". $data->pendaftaran_id ."', '". $instalasi ."');",
				"title"=>"Klik untuk membayar ke kasir",
			)
		);
	}
}