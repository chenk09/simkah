<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class ActionAjaxController extends SBaseController
{
        /**
         * ajax page to load pembayaran
         * digunakan di :
         * 1. Billing Kasir -> pembayaran -> index
         */
        public function actionLoadPembayaran()
        {
            if(Yii::app()->request->isAjaxRequest) {
                    $idPendaftaran = $_POST['idPendaftaran'];
                    $tindakanAktif = ((isset($_POST['tindakan'])) ? (boolean)$_POST['tindakan'] : false);
                    $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
                    $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);

                    $criteria = new CDbCriteria;
                    $criteria->with = array('daftartindakan','tipepaket');
                    $criteria->join .= "LEFT JOIN pasienmasukpenunjang_t ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = t.pasienmasukpenunjang_id ";
                    $criteria->join .= "LEFT JOIN pasienbatalperiksa_r ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = pasienbatalperiksa_r.pasienmasukpenunjang_id";
                    $criteria->compare('t.pendaftaran_id', $idPendaftaran);
                    $criteria->addCondition('t.tindakansudahbayar_id IS NULL');
                    if(Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_KASIR_LAB){
                        $criteria->addCondition('pasienbatalperiksa_r.pasienbatalperiksa_id IS NULL'); //jangan tampilkan tindakan jika ada pembatalan pemeriksaan
                    }
                    $modTindakan = BKTindakanPelayananT::model()->findAll($criteria);

                    $criteriaoa = new CDbCriteria;
                    $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
//                    $criteriaoa->addCondition('returresepdet_id is null'); //DICOMMENT UNTUK MENAMPILKAN OBAT YG DIRETUR SEBAGIAN QTY
                    $criteriaoa->addCondition('oasudahbayar_id IS NULL');
                    if (!$tindakanAktif){
                        if (isset($_POST['penjualanResep'])){
                            $idPenjualan = $_POST['penjualanResep'];
                            $criteriaoa->compare('penjualanresep_id',$idPenjualan);
                        } else
                            $criteriaoa->addCondition('penjualanresep_id is not null');
                    }
                    $criteriaoa->order = 'penjualanresep_id';
                    $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);

                    $modTandaBukti = new TandabuktibayarT;
                    $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
                    $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
                    
                    $totTarifTind = 0 ;
                    $totHargaSatuanObat = 0;
                    if ($tindakanAktif){
                        foreach($modTindakan as $j=>$tindakan){
                            $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                        }
                    }
                    
                    foreach($modObatalkes as $i=>$obatAlkes) { 
                        $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                    }
                    $totalPembagi = $totTarifTind + $totHargaSatuanObat;
                    
                    $totSubAsuransi=0;$totSubPemerintah=0;$totSubRs=0;$totCyto=0;$totTarif=0;$totQty=0;$totDiscount_tindakan=0;$subsidi=0;$subsidiOs=0;$totPembebasanTarif=0;$totIur=0;$iurBiaya=0;$totIurOa=0;$totIurOa=0;$totalbayartindakan=0;$totalbayarOa=0;$totTarifTind=0;$totHargaSatuanObat=0;
                    if ($tindakanAktif){
                        foreach($modTindakan as $i=>$tindakan) {
                            $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                            $tarifBebas = 0;
                            foreach ($pembebasanTarif as $i => $pembebasan) {
                              $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
                            }
                            $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
                            $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
                            $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
                            $totDiscount_tindakan += $discountTindakan ;

                            $subsidi = $this->cekSubsidi($tindakan);
                            $tarifSatuan = $tindakan->tarif_satuan;
                            $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan; 
                            $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan; 
                            $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto;
                            if(!empty($subsidi['max'])){
                                  $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']); 
                                  $subsidiPemerintah = 0; 
                                  $subsidiRumahSakit = 0; 

                                  $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                                  $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                                  $totSubRs = $totSubRs + $subsidiRumahSakit; 
                                  $iurBiaya = round(($tarifSatuan + $tarifCyto));
                                  $totIur = $totIur + $iurBiaya; 
                                  $subTotal = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi - $discountTindakan; 
                                  $subTotal = ($subTotal > 0) ? $subTotal : 0; 
                                  $totalbayartindakan = $totalbayartindakan + $subTotal; 
                            } else {
                                  $subsidiAsuransi = $subsidi['asuransi'];  
                                  $subsidiPemerintah = $subsidi['pemerintah']; 
                                  $subsidiRumahSakit = $subsidi['rumahsakit']; 

                                  $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                                  $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                                  $totSubRs = $totSubRs + $subsidiRumahSakit; 
                                  $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
                                  $totIur = $totIur + $iurBiaya; 
                                  $subTotal = $iurBiaya * $qtyTindakan;
                                  $subTotal -= $discountTindakan;
                                  $totalbayartindakan = $totalbayartindakan + $subTotal; 
                            }

                            $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                        }
                    }
                    
                    $totQtyOa = 0;
                    $totHargaSatuanObat = 0;
                    $totCytoOa = 0;
                    $totalbayarOa = 0;
                    $totCytoOa = 0;
                    $totHargaSatuan = 0;
                    $totSubAsuransiOa = 0;
                    $totSubPemerintahOa = 0;
                    $totSubRsOa = 0;
                    $totSubAsuransiOa = 0;
                    $totDiscount_oa = 0;
                    foreach($modObatalkes as $i=>$obatAlkes) { 
                        $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
                        $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
                        $totDiscount_oa += $discount_oa;
                        $subsidiOa = $this->cekSubsidiOa($obatAlkes);
                        $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; 
                        $totHargaSatuan = $totHargaSatuan + $obatAlkes->hargasatuan_oa; 
                        $oaHargasatuan = $obatAlkes->hargasatuan_oa; 
                        if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
                            $biayaServiceResep = 0;
                        }else{
                            $biayaServiceResep = $obatAlkes->biayaservice;
                        }
                        $oaCyto = $obatAlkes->biayaadministrasi + $biayaServiceResep + $obatAlkes->biayakonseling; 
                        $totCytoOa = $totCytoOa + $obatAlkes->biayaadministrasi + $obatAlkes->biayaservice + $obatAlkes->biayakonseling; 
                        if(!empty($subsidiOa['max'])){
                              $oaSubsidiasuransi = round($oaHargasatuan/$totalPembagi * $subsidiOa['max']); 
                              $oaSubsidipemerintah = 0; 
                              $oaSubsidirs = 0; 

                              $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                              $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                              $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
                              $oaIurbiaya = round(($oaHargasatuan + $oaCyto)); 
                              $obatAlkes->iurbiaya = $oaIurbiaya; 
                              $totIurOa = $totIurOa + $oaIurbiaya; 
                              $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) - $oaSubsidiasuransi - $discount_oa; 
                              $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                              $totalbayarOa = $totalbayarOa + $subTotalOa; 
                        } else {
                              $oaSubsidiasuransi = $subsidiOa['asuransi']; 
                              $oaSubsidipemerintah = $subsidiOa['pemerintah']; 
                              $oaSubsidirs = $subsidiOa['rumahsakit']; 

                              $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                              $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                              $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
//                              $oaIurbiaya = round(($oaHargasatuan + $oaCyto) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                              $oaIurbiaya = round(($oaHargasatuan) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                              $obatAlkes->iurbiaya = $oaIurbiaya; 
                              $totIurOa = $totIurOa + $oaIurbiaya; 
//                              $subTotalOa = $oaIurbiaya * $obatAlkes->qty_oa; // tanpa cyto
                              $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) + $oaCyto - $discount_oa; 
                              $totalbayarOa = $totalbayarOa + $subTotalOa; 
                        }
                        
                        $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                    }
                    
                    $pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;

                    $totTagihan = round($totalbayartindakan + $totalbayarOa);
                    $uang_muka = BayaruangmukaT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran), 'pembatalanuangmuka_id IS NULL');
                    if(count($uang_muka) > 0){
                        $totDeposit = 0;
                        foreach($uang_muka AS $data){
                            $totDeposit += $data->jumlahuangmuka;
                        }
                    }
                    $modTandaBukti->jmlpembayaran = $totTagihan;
                    $modTandaBukti->biayaadministrasi = 0;
                    $modTandaBukti->biayamaterai = 0;
                    $modTandaBukti->uangkembalian = 0;
                    $modTandaBukti->uangditerima = $totTagihan;
                    $pembulatan = ($pembulatanHarga > 0 ) ? $modTandaBukti->jmlpembayaran % $pembulatanHarga : 0;
                    if($pembulatan>0){
                        $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
                        $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan - $totPembebasanTarif - $totDeposit;
                        $harusDibayar = $modTandaBukti->jmlpembayaran;
                    } else {
                        $modTandaBukti->jmlpembulatan = 0;
                    }
                    $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
                    if ($tindakanAktif){
                        $returnVal['formBayarTindakan'] = $this->renderPartial('_loadPembayaranTindakan',
                                                                     array('modPendaftaran'=>$modPendaftaran,
                                                                           'modPasien'=>$modPasien,
                                                                           'modTindakan'=>$modTindakan,
                                                                           'modObatalkes'=>$modObatalkes,
                                                                           'modTandaBukti'=>$modTandaBukti,
                                                                           'tottagihan'=>$totTagihan,
                                                                           //'totalbayartind'=>$totalbayartindakan,
                                                                           'totpembebasan'=>$totPembebasanTarif,
                                                                           'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
                                                                           'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
                                                                           'totalPembagi'=>$totalPembagi),true);
                    }
                    $returnVal['formBayarOa'] = $this->renderPartial('_loadPembayaranOa',
                                                                 array('modPendaftaran'=>$modPendaftaran,
                                                                       'tindakanAktif'=>$tindakanAktif,
                                                                       'modPasien'=>$modPasien,
                                                                       'modTindakan'=>$modTindakan,
                                                                       'modObatalkes'=>$modObatalkes,
                                                                       'modTandaBukti'=>$modTandaBukti,
                                                                       'tottagihan'=>$totTagihan,
                                                                       //'totalbayarOa'=>$totalbayarOa,
                                                                       'totpembebasan'=>$totPembebasanTarif,
                                                                       'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
                                                                       'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
                                                                       'totalPembagi'=>$totalPembagi),true);
                    
                    $returnVal['tottagihan'] = (!empty($totTagihan)) ? $totTagihan: 0;
                    $returnVal['totpembebasan'] = (!empty($totPembebasanTarif)) ? $totPembebasanTarif: 0;
                    $returnVal['jmlpembulatan'] = (!empty($modTandaBukti->jmlpembulatan)) ? $modTandaBukti->jmlpembulatan : 0;
                    $returnVal['jmlpembayaran'] = (!empty($modTandaBukti->jmlpembayaran)) ? $modTandaBukti->jmlpembayaran : 0;
                    $returnVal['uangditerima'] = (!empty($modTandaBukti->uangditerima)) ? $modTandaBukti->uangditerima : 0;
                    $returnVal['uangkembalian'] = (!empty($modTandaBukti->uangkembalian)) ? $modTandaBukti->uangkembalian : 0;
                    $returnVal['biayamaterai'] = (!empty($modTandaBukti->biayamaterai)) ? $modTandaBukti->biayamaterai : 0;
                    $returnVal['biayaadministrasi'] = (!empty($modTandaBukti->biayaadministrasi)) ? $modTandaBukti->biayaadministrasi : 0;
                    $returnVal['namapasien'] = (!empty($modPasien->nama_pasien)) ? $modPasien->nama_pasien : 0;
                    $returnVal['alamatpasien'] = (!empty($modPasien->alamat_pasien)) ? $modPasien->alamat_pasien : 0;
                    $returnVal['subsidi'] = $subsidi;
                    $returnVal['subsidiOa'] = $subsidiOs;
                    $returnVal['deposit'] = $totDeposit;

                    echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
        }
        
        public function actionLoadPembayaranRetur()
        {
            if(Yii::app()->request->isAjaxRequest) {
                    $idPembayaran = $_POST['idPembayaran'];
                    $modPembayaran = PembayaranpelayananT::model()->findByPk($idPembayaran);
                    $modTandaBukti = TandabuktibayarT::model()->findByPk($modPembayaran->tandabuktibayar_id);
                    
                    $returnVal['tottagihan'] = (!empty($totTagihan)) ? $totTagihan: 0;
                    $returnVal['totpembebasan'] = (!empty($totPembebasanTarif)) ? $totPembebasanTarif: 0;
                    $returnVal['jmlpembulatan'] = (!empty($modTandaBukti->jmlpembulatan)) ? $modTandaBukti->jmlpembulatan : 0;
                    $returnVal['jmlpembayaran'] = (!empty($modTandaBukti->jmlpembayaran)) ? $modTandaBukti->jmlpembayaran : 0;
                    $returnVal['uangditerima'] = (!empty($modTandaBukti->uangditerima)) ? $modTandaBukti->uangditerima : 0;
                    $returnVal['uangkembalian'] = (!empty($modTandaBukti->uangkembalian)) ? $modTandaBukti->uangkembalian : 0;
                    $returnVal['biayamaterai'] = (!empty($modTandaBukti->biayamaterai)) ? $modTandaBukti->biayamaterai : 0;
                    $returnVal['biayaadministrasi'] = (!empty($modTandaBukti->biayaadministrasi)) ? $modTandaBukti->biayaadministrasi : 0;
                    $returnVal['namapasien'] = (!empty($modPasien->nama_pasien)) ? $modPasien->nama_pasien : '';
                    $returnVal['alamatpasien'] = (!empty($modPasien->alamat_pasien)) ? $modPasien->alamat_pasien : '';
                    

                    echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
        }
        
        public function proporsiSubsidi($subtotal,$total,$tagihan,$jmlbayar,$maxsubsidi)
        {
            
        }

        protected function cekSubsidi($modTindakan)
        {
            $subsidi = array();
            switch ($modTindakan->tipepaket_id) {
                case Params::TIPEPAKET_NONPAKET:     
                        $sql = "SELECT * FROM tanggunganpenjamin_m
                                WHERE carabayar_id = ".$modTindakan->carabayar_id."
                                  AND penjamin_id = ".$modTindakan->penjamin_id."
                                  AND kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                  AND tipenonpaket_id = ".$modTindakan->tipepaket_id."
                                  AND tanggunganpenjamin_aktif = TRUE ";
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransitind']!='')?($data['subsidiasuransitind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintahtind']!='')?($data['subsidipemerintahtind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakittind']!='')?($data['subsidirumahsakittind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['max'] = $data['makstanggpel'];
                    break;
                case Params::TIPEPAKET_LUARPAKET:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
                case null:
                        $subsidi['asuransi'] = 0;
                        $subsidi['pemerintah'] = 0;
                        $subsidi['rumahsakit'] = 0;
                        $subsidi['iurbiaya'] = 0;
                    break;
                default:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
            }

            return $subsidi;
        }

        protected function cekSubsidiOa($modObatAlkesPasien)
        {
            $subsidi = array();   
            if($modObatAlkesPasien->carabayar_id != ''){
            $sql = "SELECT * FROM tanggunganpenjamin_m
                        WHERE carabayar_id = ".$modObatAlkesPasien->carabayar_id."
                          AND penjamin_id = ".$modObatAlkesPasien->penjamin_id."
                          AND kelaspelayanan_id = ".$modObatAlkesPasien->kelaspelayanan_id."
                          AND tipenonpaket_id = ".$modObatAlkesPasien->tipepaket_id."
                          AND tanggunganpenjamin_aktif = TRUE ";
                $data = Yii::app()->db->createCommand($sql)->queryRow();

                $subsidi['asuransi'] = ($data['subsidiasuransioa']!='')?($data['subsidiasuransioa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['pemerintah'] = ($data['subsidipemerintahoa']!='')?($data['subsidipemerintahoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['rumahsakit'] = ($data['subsidirumahsakitoa']!='')?($data['subsidirumahsakitoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['max'] = $data['makstanggpel'];
            } else {
                $subsidi['asuransi'] = 0;
                $subsidi['pemerintah'] = 0;
                $subsidi['rumahsakit'] = 0;
                $subsidi['iurbiaya'] = 0;
            }

            return $subsidi;
        }
    
        public function actionCekHakRetur()
        {
            if(!Yii::app()->user->checkAccess('Retur')){
                //throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));
                $data['cekAkses'] = false;
            } else {
                //echo 'punya hak akses';
                $data['cekAkses'] = true;
                $data['userid'] = Yii::app()->user->id;
                $data['username'] = Yii::app()->user->name;
            }
            
            echo CJSON::encode($data);
            Yii::app()->end();
        }
        
        /**
         * ajax page to load pembayaran
         * digunakan di :
         * 1. Billing Kasir -> pembayaran -> index
         */
        public function actionLoadPembayaranUangMuka()
        {
            if(Yii::app()->request->isAjaxRequest) {
                    $idPendaftaran = $_POST['idPendaftaran'];
                    $tindakanAktif = ((isset($_POST['tindakan'])) ? (boolean)$_POST['tindakan'] : false);
                    $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
                    $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);

                    $criteria = new CDbCriteria;
                    $criteria->compare('pendaftaran_id', $idPendaftaran);
                    $criteria->addCondition('tindakansudahbayar_id IS NULL');
                    $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);

                    $criteriaoa = new CDbCriteria;
                    $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
                    $criteriaoa->addCondition('returresepdet_id is null');
                    $criteriaoa->addCondition('oasudahbayar_id IS NULL');
                    $criteriaoa->addCondition('penjualanresep_id is not null');
                    $criteriaoa->order = 'penjualanresep_id';
                    $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);

                    $modTandaBukti = new TandabuktibayarT;
                    $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
                    $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
                    
                    $totTarifTind = 0 ;
                    $totHargaSatuanObat = 0;
                    foreach($modTindakan as $j=>$tindakan){
                        $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                    }
                    
                    foreach($modObatalkes as $i=>$obatAlkes) { 
                        $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                    }
                    $totalPembagi = $totTarifTind + $totHargaSatuanObat;
                    
                    $totSubAsuransi=0;$totSubPemerintah=0;$totSubRs=0;$totCyto=0;$totTarif=0;$totQty=0;$totDiscount_tindakan=0;$subsidi=0;$subsidiOs=0;$totPembebasanTarif=0;$totIur=0;$iurBiaya=0;$totIurOa=0;$totIurOa=0;$totalbayartindakan=0;$totalbayarOa=0;$totTarifTind=0;$totHargaSatuanObat=0;
//                    if ($tindakanAktif){
                        foreach($modTindakan as $i=>$tindakan) {
                            $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                            $tarifBebas = 0;
                            foreach ($pembebasanTarif as $i => $pembebasan) {
                              $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
                            }
                            $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
                            $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
                            $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
                            $totDiscount_tindakan += $discountTindakan ;

                            $subsidi = $this->cekSubsidi($tindakan);
                            $tarifSatuan = $tindakan->tarif_satuan;
                            $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan; 
                            $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan; 
                            $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto;
                            if(!empty($subsidi['max'])){
                                  $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']); 
                                  $subsidiPemerintah = 0; 
                                  $subsidiRumahSakit = 0; 

                                  $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                                  $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                                  $totSubRs = $totSubRs + $subsidiRumahSakit; 
                                  $iurBiaya = round(($tarifSatuan + $tarifCyto));
                                  $totIur = $totIur + $iurBiaya; 
                                  $subTotal = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi - $discountTindakan; 
                                  $subTotal = ($subTotal > 0) ? $subTotal : 0; 
                                  $totalbayartindakan = $totalbayartindakan + $subTotal; 
                            } else {
                                  $subsidiAsuransi = $subsidi['asuransi'];  
                                  $subsidiPemerintah = $subsidi['pemerintah']; 
                                  $subsidiRumahSakit = $subsidi['rumahsakit']; 

                                  $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                                  $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                                  $totSubRs = $totSubRs + $subsidiRumahSakit; 
                                  $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
                                  $totIur = $totIur + $iurBiaya; 
                                  $subTotal = $iurBiaya * $qtyTindakan;
                                  $subTotal -= $discountTindakan;
                                  $totalbayartindakan = $totalbayartindakan + $subTotal; 
                            }

                            $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                        }
//                    }
                    
                    $totQtyOa = 0;
                    $totHargaSatuanObat = 0;
                    $totCytoOa = 0;
                    $totalbayarOa = 0;
                    $totCytoOa = 0;
                    $totHargaSatuan = 0;
                    $totSubAsuransiOa = 0;
                    $totSubPemerintahOa = 0;
                    $totSubRsOa = 0;
                    $totSubAsuransiOa = 0;
                    $totDiscount_oa = 0;
                    foreach($modObatalkes as $i=>$obatAlkes) { 
                        $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
                        $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
                        $totDiscount_oa += $discount_oa;
                        $subsidiOa = $this->cekSubsidiOa($obatAlkes);
                        $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; 
                        $totHargaSatuan = $totHargaSatuan + $obatAlkes->hargasatuan_oa; 
                        $oaHargasatuan = $obatAlkes->hargasatuan_oa; 
                        if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
                            $biayaServiceResep = 0;
                        }else{
                            $biayaServiceResep = $obatAlkes->biayaservice;
                        }
                        $oaCyto = $obatAlkes->biayaadministrasi + $biayaServiceResep + $obatAlkes->biayakonseling; 
                        $totCytoOa = $totCytoOa + $obatAlkes->biayaadministrasi + $obatAlkes->biayaservice + $obatAlkes->biayakonseling; 
                        if(!empty($subsidiOa['max'])){
                              $oaSubsidiasuransi = round($oaHargasatuan/$totalPembagi * $subsidiOa['max']); 
                              $oaSubsidipemerintah = 0; 
                              $oaSubsidirs = 0; 

                              $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                              $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                              $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
                              $oaIurbiaya = round(($oaHargasatuan + $oaCyto)); 
                              $obatAlkes->iurbiaya = $oaIurbiaya; 
                              $totIurOa = $totIurOa + $oaIurbiaya; 
                              $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) - $oaSubsidiasuransi - $discount_oa; 
                              $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                              $totalbayarOa = $totalbayarOa + $subTotalOa; 
                        } else {
                              $oaSubsidiasuransi = $subsidiOa['asuransi']; 
                              $oaSubsidipemerintah = $subsidiOa['pemerintah']; 
                              $oaSubsidirs = $subsidiOa['rumahsakit']; 

                              $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                              $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                              $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
//                              $oaIurbiaya = round(($oaHargasatuan + $oaCyto) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                              $oaIurbiaya = round(($oaHargasatuan) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                              $obatAlkes->iurbiaya = $oaIurbiaya; 
                              $totIurOa = $totIurOa + $oaIurbiaya; 
//                              $subTotalOa = $oaIurbiaya * $obatAlkes->qty_oa; // tanpa cyto
                              $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) + $oaCyto - $discount_oa; 
                              $totalbayarOa = $totalbayarOa + $subTotalOa; 
                        }
                        
                        $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                    }
                    
                    $pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;
//                    echo $totalbayartindakan.' '.$totalbayarOa;
//                    exit();
                    $totTagihan = round($totalbayartindakan + $totalbayarOa);
                    $uang_muka = BayaruangmukaT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran), 'pembatalanuangmuka_id IS NULL');
                    if(count($uang_muka) > 0){
                        $totDeposit = 0;
                        foreach($uang_muka AS $data){
                            $totDeposit += $data->jumlahuangmuka;
                        }
                    }
                    $modTandaBukti->jmlpembayaran = $totTagihan;
                    $modTandaBukti->biayaadministrasi = 0;
                    $modTandaBukti->biayamaterai = 0;
                    $modTandaBukti->uangkembalian = 0;
                    $modTandaBukti->uangditerima = $totTagihan;
                    $pembulatan = ($pembulatanHarga > 0 ) ? $modTandaBukti->jmlpembayaran % $pembulatanHarga : 0;
                    if($pembulatan>0){
                        $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
                        $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan - $totPembebasanTarif - $totDeposit;
                        $harusDibayar = $modTandaBukti->jmlpembayaran;
                    } else {
                        $modTandaBukti->jmlpembulatan = 0;
                    }
                    $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
//                    if ($tindakanAktif){
                        $returnVal['formBayarTindakan'] = $this->renderPartial('_loadPembayaranUangMuka',
                                                                     array('modPendaftaran'=>$modPendaftaran,
                                                                           'modPasien'=>$modPasien,
                                                                           'modTindakan'=>$modTindakan,
                                                                           'modObatalkes'=>$modObatalkes,
                                                                           'modTandaBukti'=>$modTandaBukti,
                                                                           'tottagihan'=>$totTagihan,
                                                                           //'totalbayartind'=>$totalbayartindakan,
                                                                           'totpembebasan'=>$totPembebasanTarif,
                                                                           'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
                                                                           'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
                                                                           'totalPembagi'=>$totalPembagi),true);
//                    }
                    $returnVal['formBayarOa'] = $this->renderPartial('_loadPembayaranOa',
                                                                 array('modPendaftaran'=>$modPendaftaran,
                                                                       'tindakanAktif'=>$tindakanAktif,
                                                                       'modPasien'=>$modPasien,
                                                                       'modTindakan'=>$modTindakan,
                                                                       'modObatalkes'=>$modObatalkes,
                                                                       'modTandaBukti'=>$modTandaBukti,
                                                                       'tottagihan'=>$totTagihan,
                                                                       //'totalbayarOa'=>$totalbayarOa,
                                                                       'totpembebasan'=>$totPembebasanTarif,
                                                                       'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
                                                                       'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
                                                                       'totalPembagi'=>$totalPembagi),true);
                    
                    $returnVal['tottagihan'] = (!empty($totTagihan)) ? $totTagihan: 0;
                    $returnVal['totpembebasan'] = (!empty($totPembebasanTarif)) ? $totPembebasanTarif: 0;
                    $returnVal['jmlpembulatan'] = (!empty($modTandaBukti->jmlpembulatan)) ? $modTandaBukti->jmlpembulatan : 0;
                    $returnVal['jmlpembayaran'] = (!empty($modTandaBukti->jmlpembayaran)) ? $modTandaBukti->jmlpembayaran : 0;
                    $returnVal['uangditerima'] = (!empty($modTandaBukti->uangditerima)) ? $modTandaBukti->uangditerima : 0;
                    $returnVal['uangkembalian'] = (!empty($modTandaBukti->uangkembalian)) ? $modTandaBukti->uangkembalian : 0;
                    $returnVal['biayamaterai'] = (!empty($modTandaBukti->biayamaterai)) ? $modTandaBukti->biayamaterai : 0;
                    $returnVal['biayaadministrasi'] = (!empty($modTandaBukti->biayaadministrasi)) ? $modTandaBukti->biayaadministrasi : 0;
                    $returnVal['namapasien'] = (!empty($modPasien->nama_pasien)) ? $modPasien->nama_pasien : 0;
                    $returnVal['alamatpasien'] = (!empty($modPasien->alamat_pasien)) ? $modPasien->alamat_pasien : 0;
                    $returnVal['subsidi'] = $subsidi;
                    $returnVal['subsidiOa'] = $subsidiOs;
                    $returnVal['deposit'] = $totDeposit;
                    

                    echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
        }
        /**
         * untuk :
         * - Transaksi Pembayaran Jasa
         */
        public function actionAddDetailPembayaranJasa()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $pegawaiId=$_POST['pegawai_id'];
                $rujukandariId = $_POST['rujukandari_id'];
                $komponentarifIds=$_POST['komponentarifId'];
                $data =  array();
                $tr =  "";
                $jasaPerujuk[] = 0;
                $jasaDokter[] = 0;
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
                
                if(!empty($rujukandariId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tglmasukpenunjang', $tglAwal, $tglAkhir);
                    $criteria->addCondition('rujukandari_id = '.$rujukandariId);
                    $criteria->group = "pasienmasukpenunjang_id, tglmasukpenunjang, rujukandari_id, pendaftaran_id, no_pendaftaran, no_rekam_medik, no_masukpenunjang, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, penjamin_nama";
                    $criteria->select = $criteria->group;
                    $criteria->order = 'tglmasukpenunjang';
                    $dataDetails = BKPasienrujukanluardokterV::model()->findAll($criteria);
                    $criteria1 = $criteria;
                    $criteria1->group .= ', daftartindakan_id, daftartindakan_kode, daftartindakan_nama, tarif_tindakan';
                    $criteria1->select = $criteria1->group;
                    $dataTindakans = BKPasienrujukanluardokterV::model()->findAll($criteria1);
                    $criteria2 = $criteria;
                    $criteria2->select .= ", komponentarif_id, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria2->group .= ", komponentarif_id";
                    $criteria2->addInCondition('komponentarif_id', $komponentarifIds); 
                    $dataKomponen = BKPasienrujukanluardokterV::model()->findAll($criteria2);
                    foreach($dataDetails AS $i => $dataDetail){
                        //hitung tarif_tindakankomp per pasienmasukpenunjang_id
                        foreach($dataKomponen AS $j => $dataKom){
                            if($dataDetail->pasienmasukpenunjang_id == $dataKom->pasienmasukpenunjang_id)
                                $dataDetail->tarif_tindakankomp += $dataKom->tarif_tindakankomp;
                        }
                        //hitung tarif_tindakan per pasienmasukpenunjang_id
                        foreach($dataTindakans AS $k=>$tindakan){
                            if($dataDetail->pasienmasukpenunjang_id == $tindakan->pasienmasukpenunjang_id)
                                $dataDetail->tarif_tindakan += $tindakan->tarif_tindakan;
                        }
                    }
                }else if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tgl_pendaftaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->group = "pendaftaran_id, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, penjamin_nama";
                    $criteria->select = "MAX(tgl_pendaftaran) AS tgl_pendaftaran, ".$criteria->group; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
//                    $criteria->order = 'tgl_pendaftaran';
                    $dataDetails = BKPasienpelayanandokterrsV::model()->findAll($criteria);
                    $criteria1 = $criteria;
                    $criteria1->group .= ', daftartindakan_id, daftartindakan_kode, daftartindakan_nama, tarif_tindakan';
                    $criteria1->select = $criteria1->group;
                    $dataTindakans = BKPasienpelayanandokterrsV::model()->findAll($criteria1);
                    $criteria2 = $criteria;
                    $criteria2->select .= ", komponentarif_id, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria2->group .= ", komponentarif_id";
                    $criteria2->addInCondition('komponentarif_id', $komponentarifIds);
                    $dataKomponen = BKPasienpelayanandokterrsV::model()->findAll($criteria2);
                    foreach($dataDetails AS $i => $dataDetail){
                        //hitung tarif_tindakankomp per pendaftaran_id
                        foreach($dataKomponen AS $j => $dataKom){
                            if($dataDetail->pendaftaran_id == $dataKom->pendaftaran_id)
                                $dataDetail->tarif_tindakankomp += $dataKom->tarif_tindakankomp;
                        }
                        //hitung tarif_tindakan per pendaftaran_id
                        foreach($dataTindakans AS $k=>$tindakan){
                            if($dataDetail->pendaftaran_id == $tindakan->pendaftaran_id)
                                $dataDetail->tarif_tindakan += $tindakan->tarif_tindakan;
                        }
                    }
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        if(!empty($rujukandariId)) {
                            $tr .= CHtml::activeHiddenField($modDetails,'['.$i.']pasienmasukpenunjang_id',array('value'=>$detail->pasienmasukpenunjang_id));
                        }
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        if(!empty($rujukandariId)){
                            $tr .= "<td>".$detail->no_masukpenunjang."</td>";
                        }else{
                            $tr .= "<td><center>-</center></td>";
                        }
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
               $data['tr']=$tr;
               echo json_encode($data);
             Yii::app()->end();
            }
        }
        

  public function actionBatalBayar()
  {

    if(Yii::app()->request->isAjaxRequest) {

      $idTandabuktibayar = $_POST['idTandabuktibayar'];
      $idPembayaranpalayanan = $_POST['idPembayaranpelayanan']; 

      $modTandaBuktiBayar = TandabuktibayarT::model()->findByPk($idTandabuktibayar);
      $modBayarAngsuranPelayananT = BayarangsuranpelayananT::model()->findByAttributes(array('tandabuktibayar_id'=>$idTandabuktibayar));

      $barisBayar = COUNT($modBayarAngsuranPelayananT);
      $closing = $modTandaBuktiBayar->closingkasir_id;

      $modPasienAdmisi = PasienadmisiT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));
      $idPasienAdmisi = $modPasienAdmisi->pasienadmisi_id;

      $modPendaftaran = PendaftaranT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));

      $modTindakanSudahBayar = TindakansudahbayarT::model()->findAllByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));
      // if(count($modTindakanSudahBayar)>0){
      //   $modTindakanpelayanan = TindakanpelayananT::model()->findByAttributes(array('tindakansudahbayar_id'=>$modTindakanSudahBayar->tindakansudahbayar_id));
      //   $idTindakanpelayanan = $modTindakanpelayanan->tindakanpelayanan_id;
      // }

      $modOA = OasudahbayarT::model()->findAllByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));
      // if(count($modOA)>0){
      //   $modObatalkespasien = ObatalkespasienT::model()->findByAttributes(array('oasudahbayar_id'=>$modOA->oasudahbayar_id));
      //   $idObatalkespasien = $modObatalkespasien->obatalkespasien_id;
      // }
      // print_r(count($modObatalkespasien));
      // exit();
      $null = null;

      if(empty($closing) && $barisBayar==0){

        TandabuktibayarT::model()->deleteByPk($idTandabuktibayar);

        if(count($modOA)>0){
          foreach ($modOA as $i => $modOASudahBayar) {
            $modObatalkespasien = ObatalkespasienT::model()->findByAttributes(array('oasudahbayar_id'=>$modOASudahBayar->oasudahbayar_id));
            $idObatalkespasien = $modObatalkespasien->obatalkespasien_id;
            ObatalkespasienT::model()->updateByPk($idObatalkespasien,array(
                'oasudahbayar_id'=>$null,
                'subsidiasuransi'=>0,
                'subsidipemerintah'=>0,
                'subsidirs'=>0,
                'iurbiaya'=>$modObatalkespasien->hargasatuan_oa * $modObatalkespasien->qty_oa,
                'update_loginpemakai_id'=>Yii::app()->user->id,
                'update_time'=>date('Y-m-d H:i:s'),
                ));
          }            
        }

        OasudahbayarT::model()->deleteAllByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));


        if(count($modTindakanSudahBayar)>0){
          foreach ($modTindakanSudahBayar as $j => $modTindakans) {
            $modTindakanpelayanan = TindakanpelayananT::model()->findByAttributes(array('tindakansudahbayar_id'=>$modTindakans->tindakansudahbayar_id));
            $idTindakanpelayanan = $modTindakanpelayanan->tindakanpelayanan_id;
            
            TindakanpelayananT::model()->updateByPk($idTindakanpelayanan,array(
                'tindakansudahbayar_id'=>$null,
                'subsidiasuransi_tindakan'=>0,
                'subsidipemerintah_tindakan'=>0,
                'subsisidirumahsakit_tindakan'=>0,
                'iurbiaya_tindakan'=>$modTindakanpelayanan->tarif_satuan * $modTindakanpelayanan->qty_tindakan,
                'update_loginpemakai_id'=>Yii::app()->user->id,
                'update_time'=>date('Y-m-d H:i:s'),
                ));
          }  
        }
        
        PiutangasuransiT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id));

        TindakansudahbayarT::model()->deleteAllByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));

        PembayaranpelayananT::model()->deleteAllByAttributes(array('tandabuktibayar_id'=>$idTandabuktibayar));

        PemakaianuangmukaT::model()->deleteAllByAttributes(array('pembayaranpelayanan_id'=>$idPembayaranpalayanan));

        
        PasienadmisiT::model()->updateByPk($idPasienAdmisi,array('pembayaranpelayanan_id'=>$null));
        if($modPendaftaran->instalasi_id == Params::INSTALASI_ID_RJ){ //khusus untuk RJ saja Status periksa = sedang periksa
            PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array('pembayaranpelayanan_id'=>$null, 'statusperiksa'=>Params::statusPeriksa(2)));
        }else{
            PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id,array('pembayaranpelayanan_id'=>$null));
        }
        
        $returnVal['hasil'] = 'BERHASIL';

      }else{
        $returnVal['hasil'] = 'GAGAL';
      }
      echo CJSON::encode($returnVal);
    }
    Yii::app()->end();
  }
  
  public function actionSetStatusFarmasi(){
      if(Yii::app()->request->isAjaxRequest) {
            $idPendaftaran = $_POST['idPendaftaran'];
            $ruangan = $_POST['ruangan'];
            
            if($ruangan == "RJ"){
                $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                if($modPendaftaran->statusfarmasi == true){
                    $data['statusFarmasi'] = true;
                }else{
                    $data['statusFarmasi'] = false;
                }
            }else if($ruangan == "RI"){
                 $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                 $modAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                 if($modAdmisi->statusfarmasi == true){
                     $data['statusFarmasi'] = true;
                 }else{
                     $data['statusFarmasi'] = false;
                 }
            }else if($ruangan == "RD"){
                $modPendaftaran = PendaftaranT::model()->findByPk($idPendaftaran);
                if($modPendaftaran->statusfarmasi == true){
                    $data['statusFarmasi'] = true;
                }else{
                    $data['statusFarmasi'] = false;
                }
            }
            echo CJSON::encode($data);
        }  
        Yii::app()->end();
  }
  
        /**
         * untuk :
         * - Transaksi Pembayaran Jasa Dokter Rs (Jasa Medis)
         */
		
		public function actionAddDetailPembayaranJasaMedisRs()
		{
			$format = new CustomFormat();
			$pegawaiId=$_POST['pegawai_id'];
			$tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
			$tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
			$data = array();
			
			if(Yii::app()->request->isAjaxRequest)
			{
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
//                    $criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->addBetweenCondition('t.tglpembayaran', $tglAwal, $tglAkhir);
//                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('dokterpemeriksa1_id = '.$pegawaiId.' OR dokterpemeriksa2_id = '.$pegawaiId.' OR dokterpendamping_id = '.$pegawaiId.' OR dokteranastesi_id = '.$pegawaiId.' OR dokterdelegasi_id = '.$pegawaiId);
                    $criteria->addCondition('totaltarif_komponensatuan > 0');
//                    $criteria->group = "t.pendaftaran_id, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id";
//                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_kompsatuan*qty_tindakan) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
//                    $criteria->order = 't.pendaftaran_id';
//					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasamedisrumahsakitV::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
				echo json_encode($data);
				Yii::app()->end();
			}
		}
		
		public function actionAddDetailPembayaranJasaMedisRsDua()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $pegawaiId=$_POST['pegawai_id'];
                $data =  array();
                $dataDetails =  array();
				$t = array();
				$daftar_tindakan = array();
                $tr =  "";
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
                
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tgl_tindakan', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "pendaftaran_id, tgl_tindakan, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_kompsatuan*qty_tindakan) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_tindakan';
//					$criteria->limit = 500; 
                    $dataDetails = BKBayarjasamedisrumahsakitV::model()->findAll($criteria);
                }
				
                if(count($dataDetails)>0){
					foreach ($dataDetails as $i => $detail){
						$modDetails = new BKPembjasadetailT;
						$modDetails->attributes = $detail->attributes;
						$modDetails->jumahtarif = $detail->tarif_tindakan;
						$modDetails->jumlahjasa = $detail->tarif_tindakankomp;
						$modDetails->jumlahbayar = $modDetails->jumlahjasa;
						$modDetails->sisajasa = 0;
						$modDetails->pilihDetail = true;
						
						$criteria = new CdbCriteria();
						$criteria->addBetweenCondition('tgl_tindakan', $tglAwal, $tglAkhir);
						$criteria->addCondition('pegawai_id = '.$pegawaiId);
						$criteria->addCondition('tarif_tindakankomp > 0');
						$criteria->addCondition('pendaftaran_id = '.$detail->pendaftaran_id);
						$criteria->group = "pendaftaran_id, tgl_tindakan, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
						$criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_kompsatuan*qty_tindakan) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
						$criteria->order = 'tgl_tindakan';

						$dataDetails = BKBayarjasamedisrumahsakitV::model()->findAll($criteria);
						foreach($dataDetails as $i=>$tindakan)
						{
								$moddet = new BKPembjasadetailT;
								$moddet->attributes = $detail->attributes;
								$moddet->jumahtarif = $detail->tarif_tindakan;
								$moddet->jumlahjasa = $detail->tarif_tindakankomp;
								$moddet->jumlahbayar = $moddet->jumlahjasa;
								$moddet->sisajasa = 0;
								$moddet->pilihDetail = true;

								$daftar_tindakan[$tindakan->daftartindakan_nama] = array(
									'penjamin'=>$tindakan->penjamin_nama,
									'jumahtarif'=>$moddet->jumahtarif,
									'jumlahjasa'=>$moddet->jumlahjasa,
									'jumlahbayar'=>$moddet->jumlahbayar,
									'sisajasa'=>$moddet->sisajasa
								);
							$jml_tindakan = count($tindakan->daftartindakan_nama);
						}	
						
						$t[$detail->pendaftaran_id] = array(
							'pasien_id'=>$detail->pasien_id,
							'penjamin_id'=>$detail->penjamin_id,
							'penunjang_id'=>$modDetails->pasienmasukpenunjang_id,
							'no_rekam_medik'=>$detail->no_rekam_medik,
							'no_pendaftaran'=>$detail->no_pendaftaran,
							'nama_pasien'=>$modDetails->pasien->nama_pasien,
							'alamat_pasien'=>$detail->alamat_pasien,
							'jml_tindakan'=>$jml_tindakan,
							'daftartindakan'=>$daftar_tindakan
						);
					}
					$i = 0;
						if(count($t)>0)
						{
							foreach($t as $pendaftaran_id => $datadetail)
							{
								if(count($datadetail['daftartindakan'])>1) $rowspan = "rowspan='".(count($datadetail['daftartindakan'])+1)."'";
								$tr .= "<tr>";
								$tr .= "<td ".$rowspan.">".($i+1)."</td>";
								$tr .= "<td ".$rowspan.">".$datadetail['no_pendaftaran']."<br>/".$datadetail['no_rekam_medik']."</td>";
								$tr .= "<td ".$rowspan."><center>".$datadetail['penunjang_id']."</center></td>";
								$tr .= "<td ".$rowspan.">".$datadetail['nama_pasien']."</td>";
								$tr .= "<td ".$rowspan.">".$datadetail['alamat_pasien']." ".count($datadetail['daftartindakan'])."</td>";
								
								if(count($datadetail['daftartindakan'])>1) $tr .= "</tr>";
								$databayar=0;
								foreach ($datadetail['daftartindakan'] as $key => $value) {
									if(count($datadetail['daftartindakan']) != 1){
										$tr .= "<tr>";
									}
									$tr .= "<td>".$key.
										CHtml::HiddenField('BKPembjasadetailT['.$databayar.'][pendaftaran_id]',$pendaftaran_id).
										CHtml::HiddenField('BKPembjasadetailT['.$databayar.'][pembayaranjasa_id]',null).
										CHtml::HiddenField('BKPembjasadetailT['.$databayar.'][pasien_id]',$datadetail['pasien_id']);
										CHtml::HiddenField('BKPembjasadetailT['.$databayar.'][penjaminId]',$value['penjamin'])."</td>";	
									$tr .= "<td>".$value['penjamin']."</td>";
									$tr .= "<td>".CHtml::textField('BKPembjasadetailT['.$databayar.'][jumahtarif]',$value['jumahtarif'],array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
									$tr .= "<td>".CHtml::textField('BKPembjasadetailT['.$databayar.'][jumlahjasa]',$value['jumlahjasa'],array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
									$tr .= "<td>".CHtml::textField('BKPembjasadetailT['.$databayar.'][jumlahbayar]',$value['jumlahbayar'],array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
									$tr .= "<td>".CHtml::textField('BKPembjasadetailT['.$databayar.'][sisajasa]',$value['sisajasa'],array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
									$tr .= "<td>".CHtml::CheckBox('BKPembjasadetailT['.$databayar.'][pilihDetail]', array('onclick'=>'checkIni(this);'))."</td>";
									$tr .= "</tr>";
									$databayar++;
								}														
								$i++;
							}
						}							
//					foreach ($dataDetails as $i => $detail){
//						$modDetails = new BKPembjasadetailT;
//						$modDetails->attributes = $detail->attributes;
//						$modDetails->jumahtarif = $detail->tarif_tindakan;
//						$modDetails->jumlahjasa = $detail->tarif_tindakankomp;
//						$modDetails->jumlahbayar = $modDetails->jumlahjasa;
//						$modDetails->sisajasa = 0;
//						$modDetails->pilihDetail = true;
//						$tr .= "<tr>";
//						$tr .= "<td>".($i+1).
//								CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
//								CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
//								CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
//								CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
//								//tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
//						$tr .= "</td>";    
//						$tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
//						$tr .= "<td><center>-</center></td>";
//						$tr .= "<td>".$modDetails->pasien->nama_pasien."</td>";
//						$tr .= "<td>".$detail->alamat_pasien."</td>";
//						$tr .= "<td>".$detail->daftartindakan_nama."</td>";
//						$tr .= "<td>".$detail->penjamin_nama."</td>";
//						$tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
//						$tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
//						$tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
//						$tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
//						$tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
//						$tr .= "</tr>";
//					}					 
                }
               $data['tr']=$tr;
               echo json_encode($data);
             Yii::app()->end();
            }
        }
        /**
         * untuk :
         * - Transaksi Pembayaran Jasa Dokter Rs (Jasa Rujuk)
         */
        public function actionAddDetailPembayaranJasaRujukRs()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $pegawaiId=$_POST['pegawai_id'];
                $data =  array();
                $dataDetails =  array();
                $tr =  "";
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
                
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('DATE(tglmasukpenunjang)', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "pendaftaran_id, tglmasukpenunjang, no_masukpenunjang, pasienmasukpenunjang_id,pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien,  penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tglmasukpenunjang';
                    $dataDetails = BKBayarjasadokter31rsV::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
					$jml = 1;
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
//                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumahtarif = $detail->getTotalTindakan();
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
						$jmltrf = $modDetails->jumahtarif;
						$jmljasa = $modDetails->jumlahjasa;
						$jmlbayar = $modDetails->jumlahbayar;
						$jmlsisa = $modDetails->sisajasa;
						$jml++;
						$no_rek_med = $detail->no_rekam_medik;
						$no_daftar = $detail->no_pendaftaran;
						$nama_pasien = $detail->nama_pasien;
						$alamat_pasien = $detail->alamat_pasien;
						if(!empty($modDetails->pendaftaran_id)){
							$cara_bayar = $modDetails->pendaftaran->carabayar->carabayar_nama;
							$penjamin = $modDetails->pendaftaran->penjamin->penjamin_nama;
						}else{
							$cara_bayar = '';
							$penjamin = '';
						}
						$data_all[$i] = array(
							'no_rek_med'=>$detail->no_rekam_medik,
							'no_daftar'=>$detail->no_pendaftaran,
							'no_masukpenunjang'=>$detail->no_masukpenunjang,
							'nama_pasien'=>$detail->nama_pasien,
							'alamat_pasien'=>$detail->alamat_pasien,
							'pendaftaran_id'=>$detail->pendaftaran_id,
							'pembayaranjasa_id'=>null,
							'pasien_id'=>$detail->pasien_id,
							'penjamin_id'=>$detail->penjamin_id,
							'pasienmasukpenunjang_id'=>$detail->pasienmasukpenunjang_id,
							'carabayar'=>$cara_bayar,
							'jmltrf'=>$jmltrf,
							'jmljasa'=>$jmljasa,
							'jmlbayar'=>$jmlbayar,
							'jmlsisa'=>$jmlsisa,
							'jml'=>$jml,
							'penjamin'=>$penjamin,
						);
                    }
					$temp_penj = '';
					$urut=0;
					foreach ($data_all as $penunjang => $data){
						$tr .= "<tr>";	
						$tr .= "<td>".($urut+1).
								CHtml::HiddenField('BKPembjasadetailT['.$urut.'][pendaftaran_id]',$data['pendaftaran_id']).
									CHtml::HiddenField('BKPembjasadetailT['.$urut.'][pembayaranjasa_id]',$data['pembayaranjasa_id']).
									CHtml::HiddenField('BKPembjasadetailT['.$urut.'][pasien_id]',$data['pasien_id']).
									CHtml::HiddenField('BKPembjasadetailT['.$urut.'][penjaminId]',$data['penjaminId']).
									//tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$urut.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
									CHtml::HiddenField('BKPembjasadetailT['.$urut.'][pasienmasukpenunjang_id]',$data['pasienmasukpenunjang_id'])."</td>";
						$tr .= "<td>".$data['no_daftar']."/".$data['no_rek_med']."</td>";
						$tr .= "<td>".$data['no_masukpenunjang']."</td>";
						$tr .= "<td>".$data['nama_pasien']."</td>";
						$tr .= "<td>".$data['alamat_pasien']."</td>";
						$tr .= "<td>".$data['carabayar']." / ".(isset($data['penjamin']) ? $data['penjamin'] : '-')."</td>";
						$tr .= "<td>".CHtml::TextField('BKPembjasadetailT['.$urut.'][jumahtarif]',$data['jmltrf'], array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
						$tr .= "<td>".CHtml::TextField('BKPembjasadetailT['.$urut.'][jumlahjasa]',$data['jmljasa'], array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::TextField('BKPembjasadetailT['.$urut.'][jumlahbayar]',$data['jmlbayar'], array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::TextField('BKPembjasadetailT['.$urut.'][sisajasa]',$data['jmlsisa'], array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::CheckBox('BKPembjasadetailT['.$urut.'][pilihDetail]',true, array('onclick'=>'checkIni(this);'))."</td>";
						$tr .= "</tr>";
						$urut++;
					}					 
                    }
               $data['tr']=$tr;
               echo json_encode($data);
             Yii::app()->end();
            }
        }
        /**
         * untuk :
         * - Transaksi Pembayaran Jasa Dokter Rs (Jasa Anastesi)
         */
        public function actionAddDetailPembayaranJasaAnastesiRs()
        {
            if(Yii::app()->request->isAjaxRequest)
			{
                $format = new CustomFormat();
                $pegawaiId = $_POST['pegawai_id'];
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
				
				$data = array();
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
					$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasadokteranastesi12rsV::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
				
                /*
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('pasienpulang_t.tglpasienpulang', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pasienpulang_t ON pasienpulang_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasadokteranastesi12rsV::model()->findAll($criteria);
                }
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        $tr .= "<td><center>-</center></td>";
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->daftartindakan_nama."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
				$data['tr']=$tr;
				*/
				echo json_encode($data);
				Yii::app()->end();
            }
        }
                
        /**
         * untuk :
         * - Transaksi Pembayaran Jasa
         * @author by Miranitha Fasha
         */
        public function actionAddDetailPembayaranJasaDokterLuar()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $rujukandariId = $_POST['rujukandari_id'];
                $data =  array();
                $tr =  "";
                $jasaPerujuk[] = 0;
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
                
                if(!empty($rujukandariId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tglmasukpenunjang', $tglAwal, $tglAkhir);
                    $criteria->addCondition('rujukandari_id = '.$rujukandariId);
                    $criteria->group = "pasienmasukpenunjang_id, tglmasukpenunjang, rujukandari_id, pendaftaran_id, no_pendaftaran, no_rekam_medik, no_masukpenunjang, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, penjamin_nama,komponentarif_id";
                    $criteria->select = $criteria->group.',SUM(tarif_tindakankomp) AS tarif_tindakankomp';
                    $criteria->order = 'tglmasukpenunjang';
                    $dataDetails = BKBayarjasadokterrujukanluarV::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakankomp;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        if(!empty($rujukandariId)) {
                            $tr .= CHtml::activeHiddenField($modDetails,'['.$i.']pasienmasukpenunjang_id',array('value'=>$detail->pasienmasukpenunjang_id));
                        }
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        if(!empty($rujukandariId)){
                            $tr .= "<td>".$detail->no_masukpenunjang."</td>";
                        }else{
                            $tr .= "<td><center>-</center></td>";
                        }
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
               $data['tr']=$tr;
               echo json_encode($data);
             Yii::app()->end();
            }
        }
		
		/**
         * untuk :
         * - Transaksi Pembayaran Jasa Operator
		 * EHJ-3629
         */
        public function actionAddDetailPembayaranJasaOperatorRs()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $pegawaiId = $_POST['pegawai_id'];
				
                $format = new CustomFormat();
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
				
				$data = array();
                if(!empty($pegawaiId)){
                    $criteria = new CDbCriteria();
                    $criteria->compare('pegawai_id', $pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
					$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id, pasienmasukpenunjang_id";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria->order = 'tgl_pendaftaran';					
					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasaoperatorbedahV::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
                /*
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('pasienpulang_t.tglpasienpulang', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pasienpulang_t ON pasienpulang_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasaoperatorbedahV::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        $tr .= "<td><center>-</center></td>";
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->daftartindakan_nama."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
               $data['tr']=$tr;
			   */
				echo json_encode($data);
				Yii::app()->end();
            }
        }
		/**
         * untuk :
         * - Transaksi Pembayaran Jasa Paramedis
		 * EHJ-3629
         */
        public function actionAddDetailPembayaranJasaParamedisRs()
        {
            if(Yii::app()->request->isAjaxRequest){
                $format = new CustomFormat();
                $pegawaiId=$_POST['pegawai_id'];
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
				
				$data = array();
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
					$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasaparamedisV::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
				
                /*
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tgl_pendaftaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
                    $dataDetails = BKBayarjasaparamedisV::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        $tr .= "<td><center>-</center></td>";
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->daftartindakan_nama."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
				$data['tr']=$tr;
				*/
				echo json_encode($data);
				Yii::app()->end();
            }
        }
		/**
         * untuk :
         * - Transaksi Pembayaran Jasa Sarana
		 * EHJ-3629
         */
        
		public function actionAddDetailPembayaranJasaSaranaRs()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $pegawaiId = $_POST['pegawai_id'];
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
				
                $data =  array();
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
					$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasasarana38V::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
				
                /*
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tgl_pendaftaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
                    $dataDetails = BKBayarjasasarana38V::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        $tr .= "<td><center>-</center></td>";
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->daftartindakan_nama."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
				$data['tr']=$tr;
				*/
				echo json_encode($data);
				Yii::app()->end();
            }
        }
		
        public function actionAddDetailPembayaranJasaRemunRs()
        {
            if(Yii::app()->request->isAjaxRequest) { 
                $format = new CustomFormat();
                $pegawaiId = $_POST['pegawai_id'];
                $tglAwal = $format->formatDateMediumForDB($_POST['tgl_awal'])." 00:00:00";
                $tglAkhir = $format->formatDateMediumForDB($_POST['tgl_akhir'])." 23:59:59";
				
                $data =  array();
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
					$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "t.pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, t.pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama, daftartindakan_id, tindakanpelayanan_id";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp";
                    $criteria->order = 'tgl_pendaftaran';
					$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
                    $dataDetails = BKBayarjasaremunparamedis2V::model()->findAll($criteria);
					$data['tr'] = $this->renderPartial('_formDetailPembayaran',array(
						"dataDetails"=>$dataDetails
					), true);
                }
				
                /*
                if(!empty($pegawaiId)){
                    $criteria = new CdbCriteria();
                    $criteria->addBetweenCondition('tgl_pendaftaran', $tglAwal, $tglAkhir);
                    $criteria->addCondition('pegawai_id = '.$pegawaiId);
                    $criteria->addCondition('tarif_tindakankomp > 0');
                    $criteria->group = "pendaftaran_id, tgl_pendaftaran, pegawai_id, no_pendaftaran, no_rekam_medik, pasien_id, nama_pasien, jeniskelamin, alamat_pasien, daftartindakan_nama, penjamin_nama";
                    $criteria->select = $criteria->group.", SUM(tarif_tindakan) AS tarif_tindakan, SUM(tarif_tindakankomp) AS tarif_tindakankomp"; //karna ada gabungan tgl_pendaftaran dan tgl_masukpenunjang
                    $criteria->order = 'tgl_pendaftaran';
                    $dataDetails = BKBayarjasaremunparamedis2V::model()->findAll($criteria);
                }
                
                if(count($dataDetails)>0){
                    foreach ($dataDetails as $i => $detail){
                        $modDetails = new BKPembjasadetailT;
                        $modDetails->attributes = $detail->attributes;
                        $modDetails->jumahtarif = $detail->tarif_tindakan;
                        $modDetails->jumlahjasa = $detail->tarif_tindakankomp;
                        $modDetails->jumlahbayar = $modDetails->jumlahjasa;
                        $modDetails->sisajasa = 0;
                        $modDetails->pilihDetail = true;
                        $tr .= "<tr>";
                        $tr .= "<td>".($i+1).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$detail->pendaftaran_id)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                                CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$detail->pasien_id));
                                CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$detail->penjamin_id));
                                //tidak ada pasienadmisi_id >> CHtml::activeHiddenField($modDetails,'['.$i.']pasienadmisi_id',array('value'=>$detail->pasienadmisi_id));
                        $tr .= "</td>";
                        $tr .= "<td>".$detail->no_rekam_medik."<br>".$detail->no_pendaftaran."</td>";
                        $tr .= "<td><center>-</center></td>";
                        $tr .= "<td>".$detail->nama_pasien."</td>";
                        $tr .= "<td>".$detail->alamat_pasien."</td>";
                        $tr .= "<td>".$detail->daftartindakan_nama."</td>";
                        $tr .= "<td>".$detail->penjamin_nama."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                        $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                        $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('onclick'=>'checkIni(this);'))."</td>";
                        $tr .= "</tr>";
                    }
                }
               $data['tr']=$tr;
			   */
               echo json_encode($data);
             Yii::app()->end();
            }
        }

  public function actionLoadPembayaranVerifikasi()
  {
    if(Yii::app()->request->isAjaxRequest) {
      $idPendaftaran = $_POST['idPendaftaran'];
      $tindakanAktif = ((isset($_POST['tindakan'])) ? (boolean)$_POST['tindakan'] : false);
      $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
      $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);

      $criteria = new CDbCriteria;
      $criteria->with = array('daftartindakan','tipepaket');
      $criteria->join .= "LEFT JOIN pasienmasukpenunjang_t ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = t.pasienmasukpenunjang_id ";
      $criteria->join .= "LEFT JOIN pasienbatalperiksa_r ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = pasienbatalperiksa_r.pasienmasukpenunjang_id";
      $criteria->compare('t.pendaftaran_id', $idPendaftaran);
      $criteria->addCondition('t.tindakansudahbayar_id IS NULL');
      if(Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_KASIR_LAB){
        $criteria->addCondition('pasienbatalperiksa_r.pasienbatalperiksa_id IS NULL'); //jangan tampilkan tindakan jika ada pembatalan pemeriksaan
      }
      $modTindakan = BKTindakanPelayananT::model()->findAll($criteria);

      $criteriaoa = new CDbCriteria;
      $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
      // $criteriaoa->addCondition('returresepdet_id is null'); //DICOMMENT UNTUK MENAMPILKAN OBAT YG DIRETUR SEBAGIAN QTY
      $criteriaoa->addCondition('oasudahbayar_id IS NULL');
      if (!$tindakanAktif){
        if (isset($_POST['penjualanResep'])){
          $idPenjualan = $_POST['penjualanResep'];
          $criteriaoa->compare('penjualanresep_id',$idPenjualan);
        } else
            $criteriaoa->addCondition('penjualanresep_id is not null');
      }
      $criteriaoa->order = 'penjualanresep_id';
      $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);

      $modTandaBukti = new TandabuktibayarT;
      $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
      $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
                    
      $totTarifTind = 0 ;
      $totHargaSatuanObat = 0;
      if ($tindakanAktif){
          foreach($modTindakan as $j=>$tindakan){
              $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
          }
      }
                    
      foreach($modObatalkes as $i=>$obatAlkes) { 
          $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
      }
      $totalPembagi = $totTarifTind + $totHargaSatuanObat;
                    
      $totSubAsuransi=0;$totSubPemerintah=0;$totSubRs=0;$totCyto=0;$totTarif=0;$totQty=0;$totDiscount_tindakan=0;$subsidi=0;$subsidiOs=0;$totPembebasanTarif=0;$totIur=0;$iurBiaya=0;$totIurOa=0;$totIurOa=0;$totalbayartindakan=0;$totalbayarOa=0;$totTarifTind=0;$totHargaSatuanObat=0;
      if ($tindakanAktif){
          foreach($modTindakan as $i=>$tindakan) {
              $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
              $tarifBebas = 0;
              foreach ($pembebasanTarif as $i => $pembebasan) {
                $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
              }
              $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
              $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
              $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
              $totDiscount_tindakan += $discountTindakan ;

              $subsidi = $this->cekSubsidi($tindakan);
              $tarifSatuan = $tindakan->tarif_satuan;
              $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan; 
              $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan; 
              $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto;
              if(!empty($subsidi['max'])){
                    $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']); 
                    $subsidiPemerintah = 0; 
                    $subsidiRumahSakit = 0; 

                    $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                    $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                    $totSubRs = $totSubRs + $subsidiRumahSakit; 
                    $iurBiaya = round(($tarifSatuan + $tarifCyto));
                    $totIur = $totIur + $iurBiaya; 
                    $subTotal = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi - $discountTindakan; 
                    $subTotal = ($subTotal > 0) ? $subTotal : 0; 
                    $totalbayartindakan = $totalbayartindakan + $subTotal; 
              } else {
                    $subsidiAsuransi = $subsidi['asuransi'];  
                    $subsidiPemerintah = $subsidi['pemerintah']; 
                    $subsidiRumahSakit = $subsidi['rumahsakit']; 

                    $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                    $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                    $totSubRs = $totSubRs + $subsidiRumahSakit; 
                    $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
                    $totIur = $totIur + $iurBiaya; 
                    $subTotal = $iurBiaya * $qtyTindakan;
                    $subTotal -= $discountTindakan;
                    $totalbayartindakan = $totalbayartindakan + $subTotal; 
              }

              $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
          }
      }
                    
      $totQtyOa = 0;
      $totHargaSatuanObat = 0;
      $totCytoOa = 0;
      $totalbayarOa = 0;
      $totCytoOa = 0;
      $totHargaSatuan = 0;
      $totSubAsuransiOa = 0;
      $totSubPemerintahOa = 0;
      $totSubRsOa = 0;
      $totSubAsuransiOa = 0;
      $totDiscount_oa = 0;
      foreach($modObatalkes as $i=>$obatAlkes) { 
          $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
          $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
          $totDiscount_oa += $discount_oa;
          $subsidiOa = $this->cekSubsidiOa($obatAlkes);
          $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; 
          $totHargaSatuan = $totHargaSatuan + $obatAlkes->hargasatuan_oa; 
          $oaHargasatuan = $obatAlkes->hargasatuan_oa; 
          if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
              $biayaServiceResep = 0;
          }else{
              $biayaServiceResep = $obatAlkes->biayaservice;
          }
          $oaCyto = $obatAlkes->biayaadministrasi + $biayaServiceResep + $obatAlkes->biayakonseling; 
          $totCytoOa = $totCytoOa + $obatAlkes->biayaadministrasi + $obatAlkes->biayaservice + $obatAlkes->biayakonseling; 
          if(!empty($subsidiOa['max'])){
                $oaSubsidiasuransi = round($oaHargasatuan/$totalPembagi * $subsidiOa['max']); 
                $oaSubsidipemerintah = 0; 
                $oaSubsidirs = 0; 

                $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
                $oaIurbiaya = round(($oaHargasatuan + $oaCyto)); 
                $obatAlkes->iurbiaya = $oaIurbiaya; 
                $totIurOa = $totIurOa + $oaIurbiaya; 
                $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) - $oaSubsidiasuransi - $discount_oa; 
                $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                $totalbayarOa = $totalbayarOa + $subTotalOa; 
          } else {
                $oaSubsidiasuransi = $subsidiOa['asuransi']; 
                $oaSubsidipemerintah = $subsidiOa['pemerintah']; 
                $oaSubsidirs = $subsidiOa['rumahsakit']; 

                $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi; 
                $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah; 
                $totSubRsOa = $totSubRsOa + $oaSubsidirs; 
               // $oaIurbiaya = round(($oaHargasatuan + $oaCyto) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                $oaIurbiaya = round(($oaHargasatuan) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                $obatAlkes->iurbiaya = $oaIurbiaya; 
                $totIurOa = $totIurOa + $oaIurbiaya; 
               // $subTotalOa = $oaIurbiaya * $obatAlkes->qty_oa; // tanpa cyto
                $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) + $oaCyto - $discount_oa; 
                $totalbayarOa = $totalbayarOa + $subTotalOa; 
          }
          
          $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
      }
                    
      $pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;

      $totTagihan = round($totalbayartindakan + $totalbayarOa);
      $uang_muka = BayaruangmukaT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran), 'pembatalanuangmuka_id IS NULL');
      if(count($uang_muka) > 0){
          $totDeposit = 0;
          foreach($uang_muka AS $data){
              $totDeposit += $data->jumlahuangmuka;
          }
      }
      $modTandaBukti->jmlpembayaran = $totTagihan;
      $modTandaBukti->biayaadministrasi = 0;
      $modTandaBukti->biayamaterai = 0;
      $modTandaBukti->uangkembalian = 0;
      $modTandaBukti->uangditerima = $totTagihan;
      $pembulatan = ($pembulatanHarga > 0 ) ? $modTandaBukti->jmlpembayaran % $pembulatanHarga : 0;
      if($pembulatan>0){
          $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
          $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan - $totPembebasanTarif - $totDeposit;
          $harusDibayar = $modTandaBukti->jmlpembayaran;
      } else {
          $modTandaBukti->jmlpembulatan = 0;
      }
      $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
      if ($tindakanAktif){
          $returnVal['formBayarTindakan'] = $this->renderPartial('_loadPembayaranTindakanVerifikasi',
        array('modPendaftaran'=>$modPendaftaran,
                     'modPasien'=>$modPasien,
                     'modTindakan'=>$modTindakan,
                     'modObatalkes'=>$modObatalkes,
                     'modTandaBukti'=>$modTandaBukti,
                     'tottagihan'=>$totTagihan,
                     //'totalbayartind'=>$totalbayartindakan,
                     'totpembebasan'=>$totPembebasanTarif,
                     'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
                     'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
                     'totalPembagi'=>$totalPembagi),true);
      }
      $returnVal['formBayarOa'] = $this->renderPartial('_loadPembayaranOaVerifikasi',
        array('modPendaftaran'=>$modPendaftaran,
             'tindakanAktif'=>$tindakanAktif,
             'modPasien'=>$modPasien,
             'modTindakan'=>$modTindakan,
             'modObatalkes'=>$modObatalkes,
             'modTandaBukti'=>$modTandaBukti,
             'tottagihan'=>$totTagihan,
             //'totalbayarOa'=>$totalbayarOa,
             'totpembebasan'=>$totPembebasanTarif,
             'jmlpembulatan'=>$modTandaBukti->jmlpembulatan,
             'jmlpembayaran'=>$modTandaBukti->jmlpembayaran,
             'totalPembagi'=>$totalPembagi),true);
      
      $returnVal['tottagihan'] = (!empty($totTagihan)) ? $totTagihan: 0;
                    $returnVal['totpembebasan'] = (!empty($totPembebasanTarif)) ? $totPembebasanTarif: 0;
                    $returnVal['jmlpembulatan'] = (!empty($modTandaBukti->jmlpembulatan)) ? $modTandaBukti->jmlpembulatan : 0;
                    $returnVal['jmlpembayaran'] = (!empty($modTandaBukti->jmlpembayaran)) ? $modTandaBukti->jmlpembayaran : 0;
                    $returnVal['uangditerima'] = (!empty($modTandaBukti->uangditerima)) ? $modTandaBukti->uangditerima : 0;
                    $returnVal['uangkembalian'] = (!empty($modTandaBukti->uangkembalian)) ? $modTandaBukti->uangkembalian : 0;
                    $returnVal['biayamaterai'] = (!empty($modTandaBukti->biayamaterai)) ? $modTandaBukti->biayamaterai : 0;
                    $returnVal['biayaadministrasi'] = (!empty($modTandaBukti->biayaadministrasi)) ? $modTandaBukti->biayaadministrasi : 0;
                    $returnVal['namapasien'] = (!empty($modPasien->nama_pasien)) ? $modPasien->nama_pasien : 0;
                    $returnVal['alamatpasien'] = (!empty($modPasien->alamat_pasien)) ? $modPasien->alamat_pasien : 0;
                    $returnVal['subsidi'] = $subsidi;
                    $returnVal['subsidiOa'] = $subsidiOs;
                    $returnVal['deposit'] = $totDeposit;

                    echo CJSON::encode($returnVal);
            }
            Yii::app()->end();
  }
}

?>
