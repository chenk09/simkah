<?php

class InformasiPembebasanTarifController extends SBaseController{
    
    public function actionIndex(){
        $model = new BKInformasipembebasantarifV('searchInformasi');
        $format = new CustomFormat();
        $model->unsetAttributes();
        $model->tglAwal = date("d M Y");
        $model->tglAkhir = date("d M Y");
        if(isset($_GET['BKInformasipembebasantarifV'])){
            $model->attributes = $_GET['BKInformasipembebasantarifV'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKInformasipembebasantarifV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKInformasipembebasantarifV']['tglAkhir']);
        }
        $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd'),'medium',null);
        $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd'),'medium',null);
        
        $this->render('index',array('model'=>$model));
    }
}
?>
