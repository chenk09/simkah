
<?php

class RincianTagihanFarmasiController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
//RULE DI NONAKTIFKAN KARENA FILTER MENGGUNAKAN SRBAC
	/**
	 * @return array action filters
	 */
        
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view', 'rincian'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        /**
         * actionRincian untuk melihat / mencetak Rincian Tagihan Farmasi Pasien RS
         * @param type $id
         * @param type $caraPrint
         */
        public function actionRincian($id, $caraPrint = ""){
            $this->layout = '//layouts/frameDialog';
            $format = new CustomFormat();
            $data['judulHalaman'] = 'Rincian Biaya Farmasi';
            $data['jenis_cetakan'] = 'kwitansi';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
//            $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $criteria = new CDbCriteria();
            $criteria->compare('pendaftaran_id', $id, false);
            $criteria->addCondition('oasudahbayar_id is null');
            $criteria->order = 'jenisobatalkes_nama, tglpenjualan';
            $modRincian = BKInformasipenjualanaresepV::model()->findAll($criteria);
//            $modRincian = BKInformasipenjualanaresepV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'jenisobatalkes_nama, tglpenjualan'));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            
            if($caraPrint == 'PRINT')
            {
                $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('c',$ukuranKertasPDF);
//                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1); 
                $header_title = '
                    <div>&nbsp;</div>
                    <div style="margin-top:58px;font-family:tahoma;font-size: 8pt;">
                        <div style="margin-left:1px;width:100px;float:left">No. RM / Reg</div>
                        <div style="float:left">: '. $modPendaftaran->pasien->no_rekam_medik .' / '. $modPendaftaran->no_pendaftaran .'</div>
                    </div>
                ';
                $mpdf->SetHTMLHeader($header_title);
                    
                $footer = '
                <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;"><tr>
                <td width="50%"></td>
                <td width="50%" align="right">{PAGENO} / {nb}</td>
                </tr></table>
                ';
                $mpdf->SetHTMLFooter($footer);
                /*
                * cara ambil margin
                * tinggi_header * 72 / (72/25.4)
                *  tinggi_header = inchi
                */
                $header = 0.75 * 72 / (72/25.4); 
                $mpdf->AddPage($posisi,'','','','',5,5,$header+4,8,0,0);
                $mpdf->WriteHTML(
                    $this->renderPartial(
                        'billingKasir.views.rincianTagihanFarmasi.rincianPdf', 
                        array(
                            'modPendaftaran'=>$modPendaftaran, 
                            'modRincian'=>$modRincian, 
                            'data'=>$data,
                            'format'=>$format,
                            'caraPrint'=>$caraPrint,
                        ), true
                    )
                );
                $mpdf->Output(); 
                exit();
            }else{
                $this->render(
                    'billingKasir.views.rincianTagihanFarmasi.rincian', 
                    array(
                        'modPendaftaran'=>$modPendaftaran, 
                            'modRincian'=>$modRincian, 
                            'data'=>$data,
                            'format'=>$format,
                            'caraPrint'=>$caraPrint,
                    )
                );                
            }
        }
        /**
         * actionRincian untuk melihat / mencetak Rincian Tagihan Farmasi Sudah Bayar
         * @param type $id
         * @param type $caraPrint
         */
        public function actionRincianSudahBayar($id, $caraPrint = ""){
            $this->layout = '//layouts/frameDialog';
            $format = new CustomFormat();
            $data['judulHalaman'] = 'Rincian Biaya Farmasi (Sudah Bayar)';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
//            $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $criteria = new CDbCriteria();
            $criteria->compare('pendaftaran_id', $id, false);
            $criteria->addCondition('oasudahbayar_id is not null');
            $criteria->order = 'jenisobatalkes_nama, tglpenjualan';
            $modRincian = BKInformasipenjualanaresepV::model()->findAll($criteria);
//            $modRincian = BKInformasipenjualanaresepV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'jenisobatalkes_nama, tglpenjualan'));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            
            if($caraPrint == 'PRINT')
            {
                
                $this->layout = '//layouts/printWindows';
                $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1); 
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                $header = 0.75 * 72 / (72/25.4);
                $header_title = '
                    <div>&nbsp;</div>
                    <div style="margin-top:53px;font-family:tahoma;font-size: 8pt;">
                        <div style="margin-left:1px;width:100px;float:left">No. RM / Reg</div>
                        <div style="float:left">: '. $modPendaftaran->pasien->no_rekam_medik .' / '. $modPendaftaran->no_pendaftaran .'</div>
                    </div>
                ';
                $mpdf->SetHTMLHeader($header_title);
                $footer = '
                <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;"><tr>
                <td width="50%"></td>
                <td width="50%" align="right">{PAGENO} / {nb}</td>
                </tr></table>
                ';
                $mpdf->SetHTMLFooter($footer);                
                $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                $mpdf->WriteHTML(
                    $this->renderPartial(
                        'billingKasir.views.rincianTagihanFarmasi.rincianPdf',
                        array(
                            'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data,
                                'format'=>$format,
                                'caraPrint'=>$caraPrint,
                        ), true
                    )
                );
                $mpdf->Output();                
            }else{
                $this->render(
                    'billingKasir.views.rincianTagihanFarmasi.rincianSudahBayar',
                    array(
                        'modPendaftaran'=>$modPendaftaran, 
                            'modRincian'=>$modRincian, 
                            'data'=>$data,
                            'format'=>$format,
                            'caraPrint'=>$caraPrint,
                    )
                );                
            }
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=  BKInformasipenjualanaresepV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rjrinciantagihanpasien-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
}

