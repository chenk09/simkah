<?php

class DaftarFakturPembelianController extends SBaseController
{
        public $pathView = 'billingKasir.views.daftarFakturPembelian.';
        
	public function actionIndex()
	{
            $modFaktur = new BKInformasifaktursupplierV;
            $format = new CustomFormat();
            $modFaktur->tglAwal=date('Y-m-d');
            $modFaktur->tglAkhir=date('Y-m-d');
            
            if(isset($_GET['BKInformasifaktursupplierV'])){
                $modFaktur->attributes=$_GET['BKInformasifaktursupplierV'];
                $modFaktur->tglAwal = $format->formatDateMediumForDB($_GET['BKInformasifaktursupplierV']['tglAwal']);
                $modFaktur->tglAkhir = $format->formatDateMediumForDB($_GET['BKInformasifaktursupplierV']['tglAkhir']);
                if($_GET['berdasarkanJatuhTempo']>0){
                    $modFaktur->tglAwalJatuhTempo = $format->formatDateMediumForDB($_GET['BKInformasifaktursupplierV']['tglAwalJatuhTempo']);
                    $modFaktur->tglAkhirJatuhTempo = $format->formatDateMediumForDB($_GET['BKInformasifaktursupplierV']['tglAkhirJatuhTempo']);
                } else {
                    $modFaktur->tglAwalJatuhTempo = null;
                    $modFaktur->tglAkhirJatuhTempo = null;
                }
            }
            
            $this->render($this->pathView.'index',array('modFaktur'=>$modFaktur));
	}


        public function actionDetailsFaktur($idFakturPembelian)
        {
            $this->layout='//layouts/frameDialog';
			
            $modFakturPembelian = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
            $modFakturPembelianDetails = BKFakturDetailT::model()->findAll('fakturpembelian_id='.$idFakturPembelian.'');
			
            $this->render($this->pathView.'detailsFaktur',
				array(
					'modFakturPembelian'=>$modFakturPembelian,
					'modFakturPembelianDetails'=>$modFakturPembelianDetails
				)
			);
        }
        
        public function actionDetailFaktur($idFakturPembelian)
        {
            $this->layout='//layouts/frameDialog';
//            $modFakturPembelian = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
//            $modFakturPembelianDetails = BKFakturDetailT::model()->findAll('fakturpembelian_id='.$idFakturPembelian.'');
            $modFakturPembelian = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
            $modFakturPembelianDetails = BKFakturDetailT::model()->findAll('fakturpembelian_id=:idFaktur',array('idFaktur'=>$idFakturPembelian));
            $modFakturPembelian->tglfaktur = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tglfaktur));
            $modFakturPembelian->tgljatuhtempo = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tgljatuhtempo));
            $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$modFakturPembelian->penerimaan->permintaanpembelian_id));
                foreach($modUangMuka as $u=>$uang){
                    $jmlUang += $uang->jumlahuang;
                }
            
            $this->render('gudangFarmasi.views.penerimaanItems.detailsFaktur',array('modFakturPembelian'=>$modFakturPembelian,
                                                'modFakturPembelianDetails'=>$modFakturPembelianDetails,'jmlUang',$jmlUang));
            
        }
        
        public function actionRetur($idFakturPembelian)
        {
            $this->layout='//layouts/frameDialog';
            $modFaktur = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
            $modFakturDetail = BKFakturDetailT::model()->findAll('fakturpembelian_id='.$idFakturPembelian.'');
            $modRetur = new BKReturPembelianT;
            $modRetur->fakturpembelian_id=$modFaktur->fakturpembelian_id;
            $modRetur->noretur=  Generator::noRetur();
            $modRetur->totalretur=0;
            $modRetur->tglretur=date('Y-m-d H:i:s');
            $modRetur->supplier_id=$modFaktur->supplier_id;
            $modRetur->create_loginpemakai_id = Yii::app()->user->id;
            $modRetur->update_loginpemakai_id = Yii::app()->user->id;
            $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modRetur->create_time = date('Y-m-d H:i:s');
            $modRetur->update_time = date('Y-m-d H:i:s');
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modReturDetails = new BKReturDetailT;
            $tersimpan=false;
            
            if(isset($_POST['BKReturPembelianT'])){
//                
                $modRetur->attributes = $_POST['BKReturPembelianT'];
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;

            $transaction = Yii::app()->db->beginTransaction();
            try {     
//                                 
                $jumlahCekList=0;
                $jumlahSave=0;
                $modRetur = new BKReturPembelianT;
              
                $modRetur->attributes=$_POST['BKReturPembelianT'];
                $modRetur->ruangan_id=Yii::app()->user->getState('ruangan_id');
                $modRetur->penerimaanbarang_id = $modFaktur->penerimaanbarang_id;
                $modRetur->create_loginpemakai_id = Yii::app()->user->id;
                $modRetur->update_loginpemakai_id = Yii::app()->user->id;
                $modRetur->create_ruangan = Yii::app()->user->getState('ruangan_id');
                $modRetur->create_time = date('Y-m-d H:i:s');
                $modRetur->update_time = date('Y-m-d H:i:s');
                if($modRetur->save()){
                $jumlahObat=COUNT($_POST['BKReturDetailT']['obatalkes_id']);
                    for($i=0; $i<=$jumlahObat; $i++):
                       if($_POST['checkList'][$i]=='1'){
                            $jumlahCekList++;
                            $modReturDetails = new BKReturDetailT;
                            $modReturDetails->penerimaandetail_id=$_POST['BKReturDetailT']['penerimaandetail_id'][$i];
                            $modReturDetails->obatalkes_id=$_POST['BKReturDetailT']['obatalkes_id'][$i];
                            $modReturDetails->satuanbesar_id=$_POST['BKReturDetailT']['satuanbesar_id'][$i];
                            $modReturDetails->fakturdetail_id=$_POST['BKReturDetailT']['fakturdetail_id'][$i];
                            $modReturDetails->sumberdana_id=$_POST['BKReturDetailT']['sumberdana_id'][$i];
                            $modReturDetails->returpembelian_id=$modRetur->returpembelian_id;
                            $modReturDetails->satuankecil_id=$_POST['BKReturDetailT']['satuankecil_id'][$i];
                            $modReturDetails->jmlretur=$_POST['BKReturDetailT']['jmlretur'][$i];                       
                            $modReturDetails->harganettoretur=$_POST['BKReturDetailT']['harganettoretur'][$i];
                            $modReturDetails->hargappnretur=$_POST['BKReturDetailT']['hargappnretur'][$i];
                            $modReturDetails->hargapphretur=$_POST['BKReturDetailT']['hargapphretur'][$i];
                            $modReturDetails->jmldiscount=$_POST['BKReturDetailT']['jmldiscount'][$i];
                            $modReturDetails->hargasatuanretur=$_POST['BKReturDetailT']['hargasatuanretur'][$i];
                            
                            //ini digunakan untuk mendapatkan jumalah terima dari tabel faktur detail
                            $fd = FakturdetailT::model()->findByPk($modReturDetails->fakturdetail_id);
                            $idfd = $fd->fakturdetail_id;
                            $jum1 = $fd->jmlterima;
                            $jum2 = $modReturDetails->jmlretur;
                            $jumupdate = $jum1-$jum2;
                           
                            
                            if($modReturDetails->save()){
                                $jumlahSave++;
                                BKPenerimaanDetailT::model()->updateByPk($modReturDetails->penerimaandetail_id,
                                                                        array('returdetail_id'=>$modReturDetails->returdetail_id));
                                
                                //ini digunakan untuk mengupdata tabel faktur detail dan penerimaan detail ketika terjadi retur
                                FakturdetailT::model()->updateByPk($idfd, array('jmlterima'=>$jumupdate));
                                PenerimaandetailT::model()->updateByPk($modReturDetails->penerimaandetail_id, array('jmlterima'=>$jumupdate));
                                //========================================================
                                
                                $idStokObatAlkes=BKPenerimaanDetailT::model()->findByPk($modReturDetails->penerimaandetail_id)->stokobatalkes_id;
                                $stokObatAlkesIN=BKStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_in;
                                $stokCurrent=BKStokObatAlkesT::model()->findByPk($idStokObatAlkes)->qtystok_current;
                                $stokINBaru=$stokObatAlkesIN - $modReturDetails->jmlretur;
                                $stokCurrentBaru=$stokCurrent - $modReturDetails->jmlretur;
                                BKStokObatAlkesT::model()->updateByPk($idStokObatAlkes,array('qtystok_in'=>$stokINBaru,
                                                                                             'qtystok_current'=>$stokCurrentBaru));
                            }
                    }
                    endfor;
                    
                 }

                 if(($jumlahCekList==$jumlahSave) and ($jumlahCekList>0)){
                     $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data Berhasil Disimpan ");
                        $tersimpan=true;
                     
                 }else{
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $transaction->rollback();
                 }
             }catch(Exception $exc){
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }   
                    
            }
            
            $this->render($this->pathView.'retur',array('modFaktur'=>$modFaktur,
                            'modFakturDetail'=>$modFakturDetail,
                            'modRetur'=>$modRetur,
                            'modReturDetails'=>$modReturDetails,
                            'tersimpan'=>$tersimpan
                        ));
        }
        
        public function actionPrintFaktur($idFaktur,$caraPrint){
        $judulLaporan = 'Detail Faktur Pembelian';
        $modFakturPembelian = BKFakturPembelianT::model()->findByPk($idFaktur);
        $modFakturPembelianDetails = BKFakturDetailT::model()->findAll('fakturpembelian_id=:idFaktur',array('idFaktur'=>$idFaktur));
        $modFakturPembelian->tglfaktur = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tglfaktur));
        $modFakturPembelian->tgljatuhtempo = date('d-m-Y H:i:s', strtotime($modFakturPembelian->tgljatuhtempo));
        $modUangMuka = UangmukabeliT::model()->findAllByAttributes(array('permintaanpembelian_id'=>$modFakturPembelian->penerimaan->permintaanpembelian_id));
            foreach($modUangMuka as $u=>$uang){
                $jmlUang += $uang->jumlahuang;
            }
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('gudangFarmasi.views.penerimaanItems.PrintFaktur',array('judulLaporan'=>$judulLaporan,
                                        'caraPrint'=>$caraPrint,
                                        'modFakturPembelian'=>$modFakturPembelian,
                                        'modFakturPembelianDetails'=>$modFakturPembelianDetails,'jmlUang'=>$jmlUang));
        }
        else if($caraPrint=='EXCEL') {
            $this->layout='//layouts/printExcel';
            $this->render('gudangFarmasi.views.penerimaanItems.PrintFaktur',array('modDetailRencana'=>$modDetailRencana,
                                        'judulLaporan'=>$judulLaporan,
                                        'modRencanaKebutuhan'=>$modRencanaKebutuhan,'jmlUang'=>$jmlUang));
        }
        else if($_REQUEST['caraPrint']=='PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('',$ukuranKertasPDF); 
            $mpdf->useOddEven = 2;  
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet,1);  
            $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
            $mpdf->WriteHTML($this->renderPartial('gudangFarmasi.views.penerimaanItems.PrintFaktur',array('modDetailRencana'=>$modDetailRencana,
                                                                'judulLaporan'=>$judulLaporan,
                                                                'modRencanaKebutuhan'=>$modRencanaKebutuhan,'jmlUang'=>$jmlUang),true));
            $mpdf->Output();
        }   
    }
}