<?php

class BayarUangMukaBeliController_1 extends SBaseController
{
        protected $successSave=true;
        public $pathView = 'billingKasir.views.bayarUangMukaBeli.';
	public function actionIndex()
	{
            $modSupplier = new BKSupplierM;
            $modUangMuka = new BKUangMukaBeliT;
            $modBuktiKeluar = new BKTandabuktikeluarT;
            $modelBayar = new BKBayarkeSupplierT;
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->untukpembayaran = 'Pembayaran Uang Muka Pembelian';
            $modBuktiKeluar->nokaskeluar = Generator::noKasKeluar();
            $modBuktiKeluar->biayaadministrasi = 0;
		
            if(isset($_POST['BKTandabuktikeluarT'])){
                $idSupplier = $_POST['BKSupplierM']['supplier_id'];
                
                $modSupplier->attributes = $_POST['BKSupplierM'];
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modBuktiKeluar = $this->saveBuktiKeluar($_POST['BKTandabuktikeluarT']);
                    $modelBayar = $this->saveBayarSupplier($_POST['BKBayarkeSupplierT'],$modBuktiKeluar,$idSupplier);
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render($this->pathView.'index',array('modSupplier'=>$modSupplier,
                                        'modUangMuka'=>$modUangMuka,
                                        'modBuktiKeluar'=>$modBuktiKeluar));
	}
        
        protected function saveBuktiKeluar($postBuktiKeluar)
        {
            $modBuktiKeluar = new BKTandabuktikeluarT;
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if($modBuktiKeluar->validate()){
                $modBuktiKeluar->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }            
            return $modBuktiKeluar;
        }

        protected function saveBayarUangMukaBeli($modBuktiKeluar,$idSupplier)
        {
            $modBayarUangMuka = new BKUangMukaBeliT;
            $modBayarUangMuka->supplier_id = $idSupplier;
            $modBayarUangMuka->namabank = $modBuktiKeluar->melalubank;
            $modBayarUangMuka->norekening = $modBuktiKeluar->denganrekening;
            $modBayarUangMuka->rekatasnama = $modBuktiKeluar->atasnamarekening;
            $modBayarUangMuka->jumlahuang = $modBuktiKeluar->jmlkaskeluar;
            if($modBayarUangMuka->validate()){
                $modBayarUangMuka->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBayarUangMuka;
        }
        
        protected function saveBayarSupplier($postBayarSupplier,$modBuktiKeluar,$idSupplier)
        {
            $modelBayar = new BKBayarkeSupplierT;
            $modelBayar->attributes = $postBayarSupplier;
            $modelBayar->uangmukabeli_id = null;
            $modelBayar->fakturpembelian_id = null;
            $modelBayar->tandabuktikeluar_id = $modBuktiKeluar->tandabuktikeluar_id;
            $modelBayar->tglbayarkesupplier = date('Y-m-d h:i:s');
            $modelBayar->totaltagihan = $modBuktiKeluar->jmlkaskeluar;
            $modelBayar->jmldibayarkan = $modBuktiKeluar->jmlkaskeluar;
            if($modelBayar->validate()){
                $modelBayar->save();
                $this->updateBayarUangMuka($modelBayar,$idSupplier);
                $this->updateTandaBuktiKeluar($modelBayar,$idSupplier);
                $this->successSave = true;
            } else {
                $this->successSave = false;
            }
            return $modelBayar;
        }
        
        protected function updateBayarUangMuka($modelBayar,$idSupplier)
        {
            UangmukabeliT::model()->updateAll(array('bayarkesupplier_id'=>$modelBayar->bayarkesupplier_id),'supplier_id = '.$idSupplier);
        }
        protected function updateTandaBuktiKeluar($modelBayar,$idSupplier)
        {
            TandabuktikeluarT::model()->updateByPk($modelBayar->tandabuktikeluar_id,array('bayarkesupplier_id'=>$modelBayar->bayarkesupplier_id));
        }
}

?>