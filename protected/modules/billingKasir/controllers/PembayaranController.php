<?php

class PembayaranController extends SBaseController
{
    
        private $_valid;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        protected $pathView = 'billingKasir.views.pembayaran.';
        
        /**
         * property untuk membedakan action bayar tagihan dengan bayar resep pasien
         * @var string 
         */
        protected $actionBayarTagihanPasien = 'index';
        
        /**
         * method untuk pembayaran rincian tagihan pasien
         * digunakan di :
         * 1. Billing Kasir -> transaksi -> bayar tagihan pasien
         * 2. PembayaranController/bayarResep 
         */
	public function actionIndex($view=null, $id=null)
	{        
            if (Yii::app()->user->isGuest){
                $this->redirect(Yii::app()->createUrl('site/login'));
            }
            $format = new CustomFormat();
            $successSave = false; 
            $tandaBukti = new TandabuktibayarT;
            $cekSudahBayar = null;
            $modBayar = new PembayaranpelayananT();
            $modPenjamin = new BKPiutangasuransiT;
            $modDialogPenjamin = array();
            
            if(isset($_GET['frame']) && !empty($_GET['idPendaftaran']))
            {
                $pendaftaran_id = $_GET['idPendaftaran'];                
                $modDialogPenjamin = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));

                //== cek status farmasi
                $ruangan = $_GET['status'];
                if(isset($ruangan)){
                    if($ruangan == "RJ" || $ruangan == "RD"){
                        $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                        $statusFarmasi = $modPendaftaran->statusfarmasi;
                    }else if($ruangan == "RI"){
                        $modPendaftaran = PendaftaranT::model()->findByPk($pendaftaran_id);
                        $modAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                        $statusFarmasi = $modAdmisi->statusfarmasi;
                    }
                }else{
                    $statusFarmasi = true; //SELAIN RI, RJ, RD BYPASS CEK FARMASI
                }
                if($statusFarmasi == false){
                    $this->redirect(array('/billingKasir/DaftarPasien/Pasien'.$ruangan,'statusfarmasi'=>0));
                }
                //== end cek status farmasi
                $this->layout = 'frameDialog';
                $modPendaftaran = BKPendaftaranT::model()->findByPk($pendaftaran_id);
                $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);
                $modTindakan = $this->loadTindakans($pendaftaran_id);
                $modObatalkes = $this->loadObatalkes($pendaftaran_id);
                
                if(!empty($tandaBukti->tandabuktibayar_id)){
                    $modTandaBukti = $tandaBukti;
                } else {
                    $modTandaBukti = new TandabuktibayarT;
                    $modTandaBukti->darinama_bkm = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
                }
                
                $cekSudahBayar = PembayaranpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$pendaftaran_id));
                
                if (isset($id))
                {
                    $modBayar = PembayaranpelayananT::model()->findByPk($id);
                    if ((boolean)count($modBayar)){
                        $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
                        $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                        if ((boolean)count($modPendaftaran))
                            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                        $modTindakanSudahBayar = TindakansudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        $modTindakan = array();
                        if (count($modTindakanSudahBayar) > 0){
                            foreach ($modTindakanSudahBayar as $key => $value) {
                                $modTindakan[$key] = TindakanpelayananT::model()->findByPk($value->tindakanpelayanan_id);
                            }
                        }
                        $modOaSudahBayar = OasudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        $modObatalkes = array();
                        if (count($modOaSudahBayar) > 0){
                            foreach ($modOaSudahBayar as $key => $value) {
                                $modObatalkes[$key] = ObatalkespasienT::model()->findByPk($value->obatalkespasien_id);
                            }
                        }
                    }
                }
            } else {
                if (isset($id)){
                    $modBayar = PembayaranpelayananT::model()->findByPk($id);
                    if ((boolean)count($modBayar)){
                        $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
                        $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                        if ((boolean)count($modPendaftaran))
                            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                        $modTindakanSudahBayar = TindakansudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        if (count($modTindakanSudahBayar) > 0){
                            $modTindakan = array();
                            foreach ($modTindakanSudahBayar as $key => $value) {
                                $modTindakan[$key] = TindakanpelayananT::model()->findByPk($value->tindakanpelayanan_id);
                            }
                        }
                        $modOaSudahBayar = OasudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        if (count($modOaSudahBayar) > 0){
                            $modObatalkes = array();
                            foreach ($modOaSudahBayar as $key => $value) {
                                $modObatalkes[$key] = ObatalkespasienT::model()->findByPk($value->obatalkespasien_id);
                            }
                        }
                    }
                }else{
                    $modPendaftaran = new BKPendaftaranT;
                    $modPasien = new BKPasienM;
                    $modTindakan[0] = new BKTindakanPelayananT;
                    $modObatalkes[0] = new BKObatalkesPasienT;
                    if(!empty($tandaBukti->tandabuktibayar_id)){
                        $modTandaBukti = $tandaBukti;
                    } else {
                        $modTandaBukti = new TandabuktibayarT;
                    }
                }
            }
            //Set instalasi berdasarkan asal tombol pembayaran di klik ($_GET['status'])
            if(isset($_GET['status'])){
                if($_GET['status'] == 'RI'){
                    $modPasienAdmisi = PasienadmisiT::model()->findByPk($modPendaftaran->pasienadmisi_id);
                    if($modPasienAdmisi){
                        $modPendaftaran->instalasi_nama = "Rawat Inap";
                        $modPendaftaran->instalasi_id = Params::INSTALASI_ID_RI;
                        $modPendaftaran->ruangan_id = $modPasienAdmisi->ruangan_id;
                        $modPendaftaran->carabayar_id = $modPasienAdmisi->carabayar_id;
                        $modPendaftaran->penjamin_id = $modPasienAdmisi->penjamin_id;
                    }
                }
            }
            if(count($cekSudahBayar)>0){
                Yii::app()->user->setFlash('info',"Sudah melakukan pembayaran");
            }
            /*
            if(count($modTindakan) > 0 || count($modObatalkes) > 0)
            {
                PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                    array('pembayaranpelayanan_id'=>null)
                );
            }  
            */
            if(isset($_POST['pembayaran']) || isset($_POST['pembayaranAlkes'])) {  
                //INSTALASI UNTUK KONDISI updatePendaftaran()
                $instalasiBayar == null;
                if(isset($_POST['BKPendaftaranT'])){
                    $instalasiBayar = $_POST['BKPendaftaranT']['instalasi_id']; 
                }
                $transaction = Yii::app()->db->beginTransaction();
                try {
                   
                    $tindakan = ((isset($_POST['pembayaran'])) ? $_POST['pembayaran'] : null);
                    $alkes = ((isset($_POST['pembayaranAlkes'])) ? $_POST['pembayaranAlkes'] : null);
                    
                    $tandaBukti = $this->saveTandabuktiBayar($_POST['TandabuktibayarT'],false,null,$_POST['RekeningpembayarankasirV']);
                    $modBayar = $this->savePembayaranPelayanan($tandaBukti, $tindakan, $alkes, $instalasiBayar);
                    if (isset($modAdmisi)){
                        if (!empty($modAdmisi->kamarruangan_id)){
                            $kamarruangan_status = true;
                            $keterangan_kamar = 'OPEN';
                            $modBookingkamar = BookingkamarT::model()->findByAttributes(array('kamarruangan_id'=>$modAdmisi->kamarruangan_id, 'statuskonfirmasi'=>'SUDAH KONFIRMASI', 'pasienadmisi_id'=>null));
                            if(count($modBookingkamar)>0){
                                $keterangan_kamar = 'BOOKING';
                            }

                            $ukamarruangan = KamarruanganM::model()->updateByPk(
                                $modAdmisi->kamarruangan_id,
                                array(
                                    'kamarruangan_status'=>$kamarruangan_status,
                                    'keterangan_kamar'=>$keterangan_kamar
                                )
                            );
                        }
                    }
                    if ($tandaBukti && $modBayar)
                    {                        
                        if (count($modDialogPenjamin) > 0){
                            BKPiutangasuransiT::model()->deleteAllByAttributes(array('pendaftaran_id'=>$_POST['BKPendaftaranT']['pendaftaran_id']));
                        }
                        $this->savePenjaminPasien($modBayar, $format);                            
                        /** penambahan fungsi ketika ada uang deposit **/
                        if($_POST['deposit'] > 0)
                        {
                            $insert = new BKPemakaianuangmukaT;
                            $insert->pembayaranpelayanan_id = $modBayar->pembayaranpelayanan_id;
                            $insert->tglpemakaian = date('Y-m-d H:i:s');
                            $insert->totaluangmuka = $_POST['deposit'];
                            $insert->sisauangmuka = 0;

                            if($_POST['deposit'] > $_POST['totTagihan'])
                            {
                                $insert->pemakaianuangmuka = $_POST['totTagihan'];
                                $insert->sisauangmuka = $_POST['deposit'] - $_POST['totTagihan'];
                            }else{
                                $insert->pemakaianuangmuka = $_POST['deposit'];
                            }
                            
                            if(!$insert->save())
                            {
                                Yii::app()->user->setFlash('error',"Data gagal disimpan " . $insert->getErros());
                            }else{
                                $attributes = array(
                                    'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                                );                                
                                $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes, 'pembatalanuangmuka_id IS NULL');
                                foreach ($uang_muka as $key => $valUangMuka) {
                                    
                                    $valUangMuka->pemakaianuangmuka_id = $insert->pemakaianuangmuka_id;
                                    $valUangMuka->tgluangmuka=$format->formatDateTimeMediumForDB($valUangMuka->tgluangmuka);
                                    $valUangMuka->create_time=$format->formatDateTimeMediumForDB($valUangMuka->create_time);
                                    $valUangMuka->update_time=$format->formatDateTimeMediumForDB($valUangMuka->update_time);

                                    $valUangMuka->update();
                                }

                                // $uang_muka->pemakaianuangmuka_id = $insert->pemakaianuangmuka_id;
                                // $uang_muka->update();
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            }
                        }else{
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");                            
                        }
                        
                        
                        if (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])){
                            $this->redirect(array(((isset($view)) ? $view : 'index'),'id'=>$modBayar->pembayaranpelayanan_id, 'frame'=>$_GET['frame'], 'idPendaftaran'=>$_GET['idPendaftaran']));
                        }else{
                            $this->redirect(array(((isset($view)) ? $view : 'index'),'id'=>$modBayar->pembayaranpelayanan_id));
                        }
                        $successSave = true;
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }
			
            $this->render($this->pathView.((isset($view)) ? $view : 'index'),array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modTindakan'=>$modTindakan,
                                        'modObatalkes'=>$modObatalkes,
                                        'modBayar'=>$modBayar,
                                        'modTandaBukti'=>$modTandaBukti,
                                        'modPenjamin'=>$modPenjamin,
                                        'modDialogPenjamin'=>$modDialogPenjamin,
                                        'tandaBukti'=>$tandaBukti,
                                        'successSave'=>$successSave,
                                        'cekSudahBayar'=>$cekSudahBayar));
	}
        
        public function actionGetDataRekeningKasir()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest())
            {
                parse_str($_POST['datapasien'],$datapasien);
                parse_str($_POST['datapembayaran'],$datapembayaran);
                $tandabuktibayar = $datapembayaran['TandabuktibayarT'];
                $pasien = array_merge((array)$datapasien['BKPendaftaranT'],(array)$datapasien['BKPasienM']);
                $models_penjamin = array();
                $models_carabayar = array();
                $models_piutang = array();
                
                $totalsubsidiasuransi = ($_POST['totalseluruhsubsidiasuransi'] > 0) ? $_POST['totalseluruhsubsidiasuransi'] : 0;
                $totalsubsidirs = ($_POST['totalseluruhsubsidirs'] > 0) ? $_POST['totalseluruhsubsidirs'] : 0;
                $totalseluruhbiaya = ($_POST['totalseluruhbiaya'] > 0) ? $_POST['totalseluruhbiaya'] : 0;

                if(!empty($pasien['penjamin_id'])){
                    $criteria1 = new CDbCriteria;
                    $criteria1->addCondition("penjamin_id = ".$pasien['penjamin_id']);
                    $criteria1->order='penjamin_id,penjamin_nama,saldonormal';
                    $models_penjamin = RekeningpembayarankasirV::model()->findAll($criteria1);
                }
                if($pasien['penjamin_id'] == Params::DEFAULT_CARABAYAR && $tandabuktibayar['carapembayaran'] == Params::CARAPEMBAYARAN_TUNAI){ //jika carabayar umum
                    $models_carabayar = array();
                }else{
                    $criteria2 = new CDbCriteria;
                    $criteria2->compare("LOWER(penjamin_nama)",strtolower($tandabuktibayar['carapembayaran']),true);
                    $criteria2->order='penjamin_id,penjamin_nama,saldonormal';
                    $models_carabayar = RekeningpembayarankasirV::model()->findAll($criteria2);
                }
                
                if($datapembayaran['pakeKartu'] == 1){
                    $criteria3 = new CDbCriteria;
                    $criteria3->addCondition("jenispenerimaan_id = ".PARAMS::JENISPENERIMAAN_ID_PIUTANG_MERCHANT);
                    $criteria3->order='jenispenerimaan_id,jenispenerimaan_nama,saldonormal';
                    $models_piutang = JenispenerimaanrekeningV::model()->findAll($criteria3);
                }
                if(($totalsubsidiasuransi+$totalsubsidirs) > $totalseluruhbiaya){
                    $criteria4 = new CDbCriteria;
                    $criteria4->addCondition("jenispenerimaan_id =".Params::JENISPENERIMAAN_ID_KLAIM_PIUTANG);
                    $criteria4->order='jenispenerimaan_id,jenispenerimaan_nama,saldonormal';
                    $models_piutang_asuransi = JenispenerimaanrekeningV::model()->findAll($criteria4);
                }
                $models = array_merge((array)$models_penjamin,(array)$models_carabayar,(array)$models_piutang,(array)$models_piutang_asuransi);
                
                foreach($models AS $i => $model){ //untuk membedakan tindakan / obat di form jurnal
                    $saldo = 0;

                    if(empty($model['penjamin_id'])){
                        $saldo = $tandabuktibayar['uangditerima'];
                    }else{
                        if($model['penjamin_id'] == Params::DEFAULT_CARABAYAR){
                            $saldo = $tandabuktibayar['uangditerima'];
                        } else{
                            if (($totalsubsidiasuransi+$totalsubsidirs) > $totalseluruhbiaya){
                                $saldo = $totalseluruhbiaya;
                            } else {
                                $saldo = $totalsubsidiasuransi + $totalsubsidirs;
                            }
                        }
                    }

                    if(!empty($model['jenispenerimaan_id'])){
                        $saldo = $tandabuktibayar['jmlbayardgnkartu'];
                    }
                    if ($model['jenispenerimaan_id'] == Params::JENISPENERIMAAN_ID_KLAIM_PIUTANG){
                        $saldo = ($totalsubsidiasuransi+$totalsubsidirs) - $totalseluruhbiaya;
                    }
                    if($model['saldonormal'] == "D"){
                        $models[$i]['saldodebit'] = $saldo;
                    }else{
                        $models[$i]['saldokredit'] = $saldo;
                    }

                }

                
                if(count($models)>0){
                    echo CJSON::encode(
                        $this->renderPartial('billingKasir.views.pembayaran.rekening._rowRekening', array('modRekenings'=>$models), true)
                    );                
                }else{
                    echo CJSON::encode();
                }
                Yii::app()->end();
            }        
        }
        
        /**
         * set dropdown penjamin
         */
        public function actionSetDropdownPenjamin()
        {
            if(Yii::app()->getRequest()->getIsAjaxRequest()) {
                $model = new PenjaminpasienM;
                $option = CHtml::tag('option',array('value'=>''),CHtml::encode('-- Pilih --'),true);
                if(!empty($_POST['carabayar_id'])){
                    $carabayar_id = $_POST['carabayar_id'];
                    $data = PenjaminpasienM::model()->findAllByAttributes(array('carabayar_id'=>$carabayar_id,'penjamin_aktif'=>true), array('order'=>'penjamin_nama ASC'));
                    $data = CHtml::listData($data,'penjamin_id','penjamin_nama');
                    foreach($data as $value=>$name){
                            $option .= CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
                    }
                } 
                $dataList['listPenjamin'] = $option;
                echo json_encode($dataList);
                Yii::app()->end();
            }
        }
        
        /**
         * method untuk pembayaran rincian tagihan penjualan resep
         * digunakan di :
         * 1. BillingKasir -> Transaksi -> pembayaran Resep Pasien
         */
        public function actionBayarResep($id=null){
            $view = 'bayarResep';
            $this->actionIndex($view,$id);
        }
        
        public function actionProsesBayar()
        {
            $this->layout = 'frameDialog';
            
        }
        
        protected function savePenjaminPasien($modBayar, $format){
            foreach ($_POST['BKPiutangasuransiT'] as $i=>$postAsuransi){
                $modPenjamin[$i] = new BKPiutangasuransiT;
                $modPenjamin[$i]->kelaspelayanan_id = $_POST['BKPendaftaranT']['kelaspelayanan_id'];
                $modPenjamin[$i]->pasien_id = $_POST['BKPendaftaranT']['pasien_id'];
                if (isset($_POST['BKPendaftaranT']['pasienadmisi_id'])){
                    $modPenjamin[$i]->pasienadmisi_id = $_POST['BKPendaftaranT']['pasienadmisi_id'];
                }
                $modPenjamin[$i]->pendaftaran_id = $_POST['BKPendaftaranT']['pendaftaran_id'];
                $modPenjamin[$i]->pembayaranpelayanan_id = $modBayar->pembayaranpelayanan_id;
                $modPenjamin[$i]->penjamin_id = $_POST['BKPiutangasuransiT'][$i]['penjamin_id'];
                $modPenjamin[$i]->carabayar_id = $_POST['BKPiutangasuransiT'][$i]['carabayar_id'];
                $modPenjamin[$i]->tglpiutangasuransi = $format->formatDateMediumForDB($postAsuransi['tglpiutangasuransi']);
				if((count($_POST['BKPiutangasuransiT'])) > 1){
					$modPenjamin[$i]->jmlpiutangasuransi = $postAsuransi['jmlpiutangasuransi'];
				}else{
					$modPenjamin[$i]->jmlpiutangasuransi = $_POST['totalseluruhsubsidiasuransi'];
				}
                $modPenjamin[$i]->create_time = $format->formatDateTimeMediumForDB(date('d M Y H:i:s'));
                $modPenjamin[$i]->create_loginpemakai_id = Yii::app()->user->id;
                $modPenjamin[$i]->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if ($modPenjamin[$i]->validate()){
                    $modPenjamin[$i]->save();
                } else {
                    throw new Exception("Data penjamin tidak valid <pre>".print_r($modPenjamin[$i]->errors,1)."</pre>");
                }
            }
        }
        /**
         * method untuk menyimpan pembayaran pelayanan 
         * digunakan di 
         * 1. PembayaranController/index
         * @param object $tandaBukti BKTandabuktibayarT
         * @param array $postPembayaran post request untuk tindakan pelayanan $_POST['pembayaran']
         * @param array $postPembayaranOa post request untuk obata alkes $_POST['pembayaranAlkes']
         * @throws Exception
         */
        protected function savePembayaranPelayanan($tandaBukti,$postPembayaran,$postPembayaranOa,$instalasiBayar)
        {
                $totalbayartindakan = ((isset($_POST['totalbayartindakan'])) ? $_POST['totalbayartindakan'] : 0);
                $totalbayar_oa = ((isset($_POST['totalbayar_oa'])) ? $_POST['totalbayar_oa'] : 0);
                $totalbiayatindakan = ((isset($_POST['totalbiayatindakan'])) ? $_POST['totalbiayatindakan'] : 0);
                $totalbiaya_oa = ((isset($_POST['totalbiaya_oa'])) ? $_POST['totalbiaya_oa'] : 0);
                $totalsubsidiasuransi = ((isset($_POST['totalsubsidiasuransi'])) ? $_POST['totalsubsidiasuransi'] : 0);
                $totalsubsidiasuransi_oa = ((isset($_POST['totalsubsidiasuransi_oa'])) ? $_POST['totalsubsidiasuransi_oa'] : 0);
                $totalsubsidipemerintah = ((isset($_POST['totalsubsidipemerintah'])) ? $_POST['totalsubsidipemerintah'] : 0);
                $totalsubsidipemerintah_oa = ((isset($_POST['totalsubsidipemerintah_oa'])) ? $_POST['totalsubsidipemerintah_oa'] : 0);
                $totalsubsidirs = ((isset($_POST['totalsubsidirs'])) ? $_POST['totalsubsidirs'] : 0);
                $totalsubsidirs_oa = ((isset($_POST['totalsubsidirs_oa'])) ? $_POST['totalsubsidirs_oa'] : 0);
                $totaliurbiaya = ((isset($_POST['totaliurbiaya'])) ? $_POST['totaliurbiaya'] : 0);
                $totaliurbiaya_oa = ((isset($_POST['totaliurbiaya_oa'])) ? $_POST['totaliurbiaya_oa'] : 0);
                $totalpembebasan = ((isset($_POST['totPembebasan'])) ? $_POST['totPembebasan'] : 0);
                $pendaftaran_id = $_POST['BKPendaftaranT']['pendaftaran_id'];
                $idPasien = $_POST['BKPendaftaranT']['pasien_id'];
                $discount = (isset($_POST['discount'])) ? $_POST['discount'] : 0;
                $format = new CustomFormat();
                $pembayaran = new PembayaranpelayananT;
                $pembayaran->carabayar_id = (!empty($postPembayaran[0]['carabayar_id'])) ? $postPembayaran[0]['carabayar_id'] : 1;
                $pembayaran->penjamin_id = (!empty($postPembayaran[0]['penjamin_id'])) ? $postPembayaran[0]['penjamin_id'] : 117;
                $pembayaran->pasien_id = $idPasien;
                $pembayaran->pendaftaran_id = $pendaftaran_id;
                $pembayaran->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $pembayaran->tglpembayaran = $format->formatDateTimeMediumForDB($_POST['TandabuktibayarT']['tglbuktibayar']);
                $pembayaran->ruanganpelakhir_id = MyGenerator::getRuanganPelayananAkhir($pendaftaran_id);
                $pembayaran->totalbiayaoa = $totalbiaya_oa;
                $pembayaran->totalbiayatindakan = $totalbiayatindakan;
                
//              SALAH DATA >>  $pembayaran->totalbiayapelayanan = $totalbayar_oa + $totalbayartindakan;
                $pembayaran->totalbiayapelayanan = $totalbiayatindakan + $totalbiaya_oa;
                $pembayaran->totalsubsidiasuransi = $_POST['totalseluruhsubsidiasuransi'];
//                $pembayaran->totalsubsidiasuransi = $totalsubsidiasuransi  + $totalsubsidiasuransi_oa;
                $pembayaran->totalsubsidipemerintah = $_POST['totalseluruhsubsidipemerintah'];
//                $pembayaran->totalsubsidipemerintah = $totalsubsidipemerintah + $totalsubsidipemerintah_oa;
                $pembayaran->totalsubsidirs = $_POST['totalseluruhsubsidirs'];
//                $pembayaran->totalsubsidirs = $totalsubsidirs + $totalsubsidirs_oa;
                $pembayaran->totaliurbiaya = $_POST['totalseluruhiurbiaya'];
//                $pembayaran->totaliurbiaya = $totaliurbiaya + $totaliurbiaya_oa;
                
                $pembayaran->totalbayartindakan = $tandaBukti->jmlpembayaran;
                $pembayaran->totaldiscount = $discount;
//                $pembayaran->totaldiscount = (!empty($_POST['totaldiscount_tindakan'])) ? $_POST['totaldiscount_tindakan'] : 0;
                $pembayaran->totalpembebasan = $totalpembebasan;
                $pembayaran->totalsisatagihan = 0;
                if($tandaBukti->carapembayaran == 'HUTANG'){
                    $pembayaran->totalsisatagihan = $tandaBukti->jmlpembayaran;
                }else{
//                if($tandaBukti->carapembayaran == 'CICILAN'){
//                    $pembayaran->totalsisatagihan = ($totalbayar_oa+$totalbayartindakan) - $tandaBukti->uangditerima + $tandaBukti->biayaadministrasi+$tandaBukti->biayamaterai-$discount;
                    $pembayaran->totalsisatagihan = $pembayaran->totaliurbiaya - $pembayaran->totalbayartindakan;
                }
                $pembayaran->statusbayar = $this->cekStatusBayar($pembayaran->totalsisatagihan);
                $pembayaran->nopembayaran = MyGenerator::noPembayaran();
                $pembayaran->tandabuktibayar_id = $tandaBukti->tandabuktibayar_id;

                //$pembayaran->pembayaranpelayanan_id = $pembayaran->getOldPrimaryKey() + 1;
                if($pembayaran->validate()){
                    //echo "savePembayaranPelayanan valid <br/>";
                    //echo "<pre>".print_r($pembayaran->attributes,1)."</pre>";
                    if ($pembayaran->save()) {
                        TandabuktibayarT::model()->updateByPk($tandaBukti->tandabuktibayar_id, array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id));
                        if(!empty($postPembayaran))
                            $this->saveTindakanSudahBayar($pembayaran, $postPembayaran,$tandaBukti);
                        if(!empty($postPembayaranOa))
                            $this->saveOaSudahBayar($pembayaran, $postPembayaranOa,$tandaBukti);
                        if($tandaBukti->carapembayaran == 'CICILAN' || $tandaBukti->carapembayaran == 'HUTANG')
                            $this->saveBayarAngsuran($pembayaran,$tandaBukti);
//                        if($tandaBukti->carapembayaran == 'HUTANG')
//                            $this->saveBayarAngsuran($pembayaran,$tandaBukti);
                        if ($this->cekTindakandanObatDibayar($pendaftaran_id))
                            $this->updatePendaftaran($pendaftaran_id, $pembayaran, $instalasiBayar);
                    }else{
                        throw new Exception("pembayaran tidak valid <pre>".print_r($pembayaran->errors,1)."</pre>");
                    }
                } else {

                    throw new Exception("pembayaran tidak valid <pre>".print_r($pembayaran->errors,1)."</pre>");
                }
                
                return $pembayaran;
        }
        
        /**
         * method untuk cek apakah ada tindakan atau obatalkes yang belum dibayar
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param integer $pendaftaran_id pendaftaran_id
         * @return boolean
         * modified by @author ichan | 01-05-2014 | samakan criteria pencarian tindakan dgn $modTindakan dan $modObatalkes di actionIndex
         */
        protected function cekTindakandanObatDibayar($pendaftaran_id){
            $modTindakan = $this->loadTindakans($pendaftaran_id);
            $modObatalkes = $this->loadObatalkes($pendaftaran_id);
                
            /* old
            $criteria = new CDbCriteria;
            $criteria->compare('pendaftaran_id', $pendaftaran_id);
            $criteria->addCondition('tindakansudahbayar_id IS NULL');
            $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);

            $criteriaoa = new CDbCriteria;
            $criteriaoa->compare('pendaftaran_id', $pendaftaran_id);
            $criteriaoa->addCondition('oasudahbayar_id IS NULL');
            $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);
            */
            $result = false;
            if (empty($modTindakan) && empty($modObatalkes)){ //jika sudah tidak ada yang harus dibayar
                $result = true;
            }
            return $result;
        }
        
        protected function cekStatusBayar($sisaTagihan)
        {
            if($sisaTagihan>0){
                return 'BELUM LUNAS';
            } else {
                return 'LUNAS';
            }
        }

        /**
         * method untuk update tindakanpelayanan_t
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modTindSudahBayar TindakansudahbayarT 
         */
        protected function updateTindakanPelayananT($modTindSudahBayar)
        {
            $sukses = false;
            $persenDiscount = $_POST['disc'];
            $tindakanpelayanan = TindakanpelayananT::model()->findByPk($modTindSudahBayar->tindakanpelayanan_id);
            $discount = 0;
            if ((boolean)count($tindakanpelayanan))
                $discount = floor($persenDiscount / 100 * $tindakanpelayanan->tarif_tindakan);
                        
            $suksesUpdatePelayanan = TindakanpelayananT::model()->updateByPk($modTindSudahBayar->tindakanpelayanan_id, array(
                                                                                                    'subsidiasuransi_tindakan'=>$modTindSudahBayar->jmlsubsidi_asuransi,
                                                                                                    'subsidipemerintah_tindakan'=>$modTindSudahBayar->jmlsubsidi_pemerintah,
                                                                                                    'subsisidirumahsakit_tindakan'=>$modTindSudahBayar->jmlsubsidi_rs,
                                                                                                    'iurbiaya_tindakan'=>$modTindSudahBayar->jmliurbiaya,
                                                                                                    'tindakansudahbayar_id'=>$modTindSudahBayar->tindakansudahbayar_id,
                                                                                                    'discount_tindakan'=>$discount
                                                                                                     ));
            
            $suksesUpdatePelayanan = $this->updateTindakanKomponenT($modTindSudahBayar);
            if($suksesUpdatePelayanan && $suksesUpdateKomponen){
                $sukses = true;
            }
            return $sukses; 
        }
        /**
         * updateTindakanKomponenT untuk update tindakan komponen
         * @param type $tindPelayanan
         * @return boolean
         */
        protected function updateTindakanKomponenT($modTindSudahBayar){
            $sukses = true;
            $modKomponen = TindakankomponenT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$modTindSudahBayar->tindakanpelayanan_id));
            $komponenTotal = TindakankomponenT::model()->findByAttributes(array('tindakanpelayanan_id'=>$modTindSudahBayar->tindakanpelayanan_id, 'komponentarif_id'=>Params::KOMPONENTARIF_ID_TOTAL));
            if(count($modKomponen) > 0){
                foreach($modKomponen AS $i => $komponen){
                    if($komponen->komponentarif_id == Params::KOMPONENTARIF_ID_TOTAL){
                        $komponen->subsidiasuransikomp = $modTindSudahBayar->jmlsubsidi_asuransi;
                        $komponen->subsidipemerintahkomp = $modTindSudahBayar->jmlsubsidi_pemerintah;
                        $komponen->subsidirumahsakitkomp = $modTindSudahBayar->jmlsubsidi_rs;
                        $komponen->iurbiayakomp = $modTindSudahBayar->jmliurbiaya;
                    }else{
                        $komponen->subsidiasuransikomp = ($modTindSudahBayar->jmlsubsidi_asuransi > 0) ? $modTindSudahBayar->jmlsubsidi_asuransi * $komponen->tarif_tindakankomp / $komponenTotal->tarif_tindakankomp : 0;
                        $komponen->subsidipemerintahkomp = ($modTindSudahBayar->jmlsubsidi_pemerintah > 0) ? $modTindSudahBayar->jmlsubsidi_pemerintah * $komponen->tarif_tindakankomp / $komponenTotal->tarif_tindakankomp : 0;
                        $komponen->subsidirumahsakitkomp = ($modTindSudahBayar->jmlsubsidi_rs > 0) ? $modTindSudahBayar->jmlsubsidi_rs * $komponen->tarif_tindakankomp / $komponenTotal->tarif_tindakankomp : 0;
                        $komponen->iurbiayakomp = ($modTindSudahBayar->jmliurbiaya > 0) ? $modTindSudahBayar->jmliurbiaya * $komponen->tarif_tindakankomp / $komponenTotal->tarif_tindakankomp : 0;
                    }
                    if($komponen->save()){
                        $sukses = $sukses && true;
                    }else{
                        $sukses = false;
                    }
                }
            }
            return $sukses;
        }

        /**
         * method untuk update obatalkespasien_t
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param type $modOa ObatalkespasienT
         */
        protected function updateObatAlkesPasienT($modOa)
        {
            $persenDiscount = $_POST['disc'];
            $obatOa = ObatalkespasienT::model()->findByPk($modOa->obatalkespasien_id);
            $discount = floor($persenDiscount / 100 * $obatOa->hargajual_oa);
                        
            ObatalkespasienT::model()->updateByPk($modOa->obatalkespasien_id, array(
                                                        'subsidiasuransi'=>$modOa->jmlsubsidi_asuransi,
                                                        'subsidipemerintah'=>$modOa->jmlsubsidi_pemerintah,
                                                        'subsidirs'=>$modOa->jmlsubsidi_rs,
                                                        'iurbiaya'=>$modOa->jmliurbiaya,
                                                        'oasudahbayar_id'=>$modOa->oasudahbayar_id,
                                                        'discount' => $discount,
                                                        ));
        }
        
        /**
         * Update Pendaftaran untuk pasien yang sudah bayar lunas 
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param integer $pendaftaran_id pendaftaran_id
         * @param object $pembayaran PembayaranpelayananT
         */
        protected function updatePendaftaran($pendaftaran_id,$pembayaran,$instalasiBayar)
        {
            $statusPeriksa = "SUDAH PULANG";
            if($instalasiBayar == Params::INSTALASI_ID_RI){ //jika pembayaran untuk instalasi RI
                $criteria = new CDbCriteria;
                $criteria->compare('pendaftaran_id',$pendaftaran_id);
                $criteria->addCondition('pembayaranpelayanan_id IS NULL');
                $modAdmisi = PasienadmisiT::model()->findAll($criteria);
                foreach($modAdmisi as $i => $admisi) {
                    PasienadmisiT::model()->updateByPk($admisi->pasienadmisi_id, array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id));
                }
//                $statusPeriksa = "SUDAH BAYAR RAWAT INAP";
            }else if($instalasiBayar == Params::INSTALASI_ID_RJ){ //jika pembayaran untuk instalasi RJ
//                $statusPeriksa = "SUDAH BAYAR RAWAT JALAN";
            }else if($instalasiBayar == Params::INSTALASI_ID_RD){ //jika pembayaran untuk instalasi RD
//                $statusPeriksa = "SUDAH BAYAR RAWAT DARURAT";
            }
            PendaftaranT::model()->updateByPk($pendaftaran_id,
                array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id,'statusperiksa'=>$statusPeriksa)
            );
        }

        /**
         * method untuk save tindakanpelayanan yang sudah dibayar TindakansudahbayarT
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modPembayaranPelayanan PembayaranpelayananT
         * @param array $postPembayaran post request untuk TindakanpelayananT $_POST['pembayaran']
         * @param array $modTandaBukti TandabuktibayarT
         */
        protected function saveTindakanSudahBayar($modPembayaranPelayanan,$postPembayaran,$modTandaBukti)
        {
            if (count($postPembayaran) > 0){
//                echo "Jumlah Tindkan".count($postPembayaran);exit;
                foreach($postPembayaran as $i=>$item){
//                    echo "<pre>";
//                    echo print_r($item['tindakanpelayanan_id']);
                    if ($item['tindakanpelayanan_id'] != 0){
                        $model[$i] = new TindakansudahbayarT;
                        $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');;
                        $model[$i]->tindakanpelayanan_id = $item['tindakanpelayanan_id'];
                        $model[$i]->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
                        $model[$i]->daftartindakan_id = $item['daftartindakan_id'];
                        $model[$i]->qty_tindakan = $item['qty_tindakan'];
                        $model[$i]->jmlbiaya_tindakan = $item['tarif_satuan'];
                        $model[$i]->jmlsubsidi_asuransi = $item['subsidiasuransi_tindakan'];
                        $model[$i]->jmlsubsidi_pemerintah = $item['subsidipemerintah_tindakan'];
                        $model[$i]->jmlsubsidi_rs = $item['subsisidirumahsakit_tindakan'];
                        $model[$i]->jmliurbiaya = $item['iurbiaya_tindakan'];
                        $model[$i]->jmlpembebasan = $item['pembebasan_tarif'];
                        //$model[$i]->jmlbayar_tindakan = $item['sub_total'];
                        $model[$i]->jmlbayar_tindakan = $item['sub_total'] / ($_POST['totalbayartindakan'] + $_POST['totalbayar_oa']) * $modTandaBukti->jmlpembayaran;
                        $sisaBayar = $item['sub_total'] - $model[$i]->jmlbayar_tindakan;
                        $model[$i]->jmlsisabayar_tindakan = ($sisaBayar > 0) ? $sisaBayar : 0;

                        if($model[$i]->validate()){
                            $model[$i]->save();
                            $this->updateTindakanPelayananT($model[$i]);
                        } else {
                            echo "saveTindakanSudahBayar tidak valid";
                            echo "<pre>".print_r($model[$i]->errors,1)."</pre>";
                            echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                        }
                    }
                }
                //exit;
            }
        }

        /**
         * method untuk save obatalkespasien yang sudah dibayar OasudahbayarT
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modPembayaranPelayanan PembayaranpelayananT
         * @param array $postPembayaranOa post request untuk ObatalkespasienT $_POST['pembayaranAlkes']
         * @param array $modTandaBukti TandabuktibayarT
         */
        protected function saveOaSudahBayar($modPembayaranPelayanan,$postPembayaranOa,$modTandaBukti)
        {
            if (count($postPembayaranOa) > 0){
//                echo "JUmlah Obat".count($postPembayaranOa);exit;
                foreach($postPembayaranOa as $i=>$item){
//                    echo "<pre>";
//                    echo print_r($item['obatalkespasien_id']);
                    if ($item['obatalkespasien_id'] != 0){
                        $totalbayartindakan = ((isset($_POST['totalbayartindakan'])) ? $_POST['totalbayartindakan'] : 0);
                        $model[$i] = new OasudahbayarT;
                        $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $model[$i]->obatalkespasien_id = $item['obatalkespasien_id'];
                        $model[$i]->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
                        $model[$i]->obatalkes_id = $item['obatalkes_id'];
                        $model[$i]->qty_oa = $item['qty_oa'];
                        $model[$i]->jmlsubsidi_asuransi = (isset($item['subsidiasuransi'])) ? $item['subsidiasuransi'] : 0;
                        $model[$i]->jmlsubsidi_pemerintah = (isset($item['subsidipemerintah'])) ? $item['subsidipemerintah'] : 0;
                        $model[$i]->jmlsubsidi_rs = isset($item['subsidirs']) ? $item['subsidirs'] : 0;
                        $model[$i]->jmliurbiaya = $item['iurbiaya'];
                        //$model[$i]->jmlbayar_oa = $item['sub_total'];
                        $model[$i]->jmlbayar_oa = $item['sub_total'] / ($totalbayartindakan + $_POST['totalbayar_oa']) * $modTandaBukti->jmlpembayaran;
                        $sisaBayar = $item['sub_total'] - $model[$i]->jmlbayar_oa;
                        $model[$i]->jmlsisabayar_oa = ($sisaBayar > 0) ? $sisaBayar : 0;
                        $model[$i]->hargasatuan = $item['hargasatuan'];

                        if($model[$i]->validate()){
                            $model[$i]->save();
                            $this->updateObatAlkesPasienT($model[$i]);
                        } else {
                            echo "saveOaSudahBayar tidak valid";
                            echo "<pre>".print_r($model[$i]->errors,1)."</pre>";
                            echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                        }
                    }
                }
//                exit;
            }
        }
        
        /**
         * method untuk menyimpan/update tandabukti bayar ke table TandabuktibayarT
         * digunakan di :
         * 1. PembayaranController/index
         * @param array $postTandaBuktiBayar post request untuk TandabuktibayarT
         * @return \TandabuktibayarT
         */
        protected function saveTandabuktiBayar($postTandaBuktiBayar, $update=false,$tandaBukti=null, $postRekenings = array())
        {
            $format = new CustomFormat();
            $modTandaBukti = null;
            if ($update && isset($tandaBukti)){
                $modTandaBukti = $tandaBukti;
                $modTandaBukti->jmlpembulatan += $postTandaBuktiBayar['jmlpembulatan'];
                $modTandaBukti->biayaadministrasi += $postTandaBuktiBayar['biayaadministrasi'];
                $modTandaBukti->jmlpembayaran += $postTandaBuktiBayar['jmlpembayaran'];
                $modTandaBukti->biayamaterai += $postTandaBuktiBayar['biayamaterai'];
                $modTandaBukti->uangditerima += $postTandaBuktiBayar['uangditerima'];
                $modTandaBukti->uangkembalian += $postTandaBuktiBayar['uangkembalian'];
                $modTandaBukti->tglbuktibayar = $format->formatDateTimeMediumForDB($postTandaBuktiBayar['tglbuktibayar']);
                $modTandaBukti->jmlbayardgnkartu = $postTandaBuktiBayar['jmlbayardgnkartu'];
                $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                
                if (!$modTandaBukti->save()){
                    echo "saveTandabuktiBayar tidak valid";
                    echo "<pre>".print_r($modTandaBukti->errors,1)."</pre>";
                    echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                }
            }else{
                $modTandaBukti = new TandabuktibayarT;
                $modTandaBukti->attributes = $postTandaBuktiBayar;
                if($modTandaBukti->carapembayaran == 'HUTANG'){
                    $modTandaBukti->uangditerima = 0;
                    $modTandaBukti->jmlpembayaran = 0;
                }else if($modTandaBukti->carapembayaran == 'CICILAN'){
                    $modTandaBukti->jmlpembayaran = $modTandaBukti->uangditerima;
                }
                $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTandaBukti->nourutkasir = MyGenerator::noUrutKasir($modTandaBukti->ruangan_id);
                $modTandaBukti->nobuktibayar = MyGenerator::noBuktiBayar();
                
//              SEQUENCE HARUS OTOMATIS DI DATABASE >>  $modTandaBukti->tandabuktibayar_id = $modTandaBukti->getOldPrimaryKey() + 1;
                if($modTandaBukti->validate()){
                    //echo "saveTandabuktiBayar valid <br/>";
                    //echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                    if(isset($postRekenings)){//simpan jurnal rekening
                        $modJurnalRekening = PembayaranController::saveJurnalRekening();
                        //update jurnalrekening_id
                        $modTandaBukti->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                      ADA DI BAWAH >> $modTandaBukti->save();
                        $saveDetailJurnal = PembayaranController::saveJurnalDetails($modJurnalRekening, $postRekenings, null);
                    }
                    $modTandaBukti->save();
                } else {
                    echo "saveTandabuktiBayar tidak valid";
                    echo "<pre>".print_r($modTandaBukti->errors,1)."</pre>";
                    echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                }
            }
            
            return $modTandaBukti;
        }
        
        protected function saveBayarAngsuran($modPembayaranPelayanan,$modTandaBukti)
        {
            $modelAngsuran = new BayarangsuranpelayananT;
            $modelAngsuran->tandabuktibayar_id = $modPembayaranPelayanan->tandabuktibayar_id;
            $modelAngsuran->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
            $modelAngsuran->tglbayarangsuran = $modPembayaranPelayanan->tglpembayaran;
            $modelAngsuran->bayarke = 1;
            //$modelAngsuran->jmlbayarangsuran = $modPembayaranPelayanan->totalbayartindakan;
            $modelAngsuran->jmlbayarangsuran = $modTandaBukti->uangditerima;
            $modelAngsuran->sisaangsuran = $modPembayaranPelayanan->totalsisatagihan;
            
            //$modelAngsuran->bayarangsuranpelayanan_id = $modelAngsuran->getOldPrimaryKey() + 1;
            if($modelAngsuran->validate()){
                //echo "angsuran valid <br/>";
                //echo "<pre>".print_r($modelAngsuran->attributes,1)."</pre>";
                $modelAngsuran->save();
            } else {
                echo "angsuran tidak valid";
                echo "<pre>".print_r($modelAngsuran->errors,1)."</pre>";
                echo "<pre>".print_r($modelAngsuran->attributes,1)."</pre>";
            }
        }

        protected function cekSubsidi($modTindakan)
        {
            $subsidi = array();
            switch ($modTindakan->tipepaket_id) {
                case Params::TIPEPAKET_NONPAKET:     
                        $sql = "SELECT * FROM tanggunganpenjamin_m
                                WHERE carabayar_id = ".$modTindakan->carabayar_id."
                                  AND penjamin_id = ".$modTindakan->penjamin_id."
                                  AND kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                  AND tipenonpaket_id = ".$modTindakan->tipepaket_id."
                                  AND tanggunganpenjamin_aktif = TRUE ";
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransitind']!='')?($data['subsidiasuransitind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintahtind']!='')?($data['subsidipemerintahtind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakittind']!='')?($data['subsidirumahsakittind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['max'] = $data['makstanggpel'];
                    break;
                case Params::TIPEPAKET_LUARPAKET:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
                case null:
                        $subsidi['asuransi'] = 0;
                        $subsidi['pemerintah'] = 0;
                        $subsidi['rumahsakit'] = 0;
                        $subsidi['iurbiaya'] = 0;
                    break;
                default:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
            }

            return $subsidi;
        }

        protected function cekSubsidiOa($modObatAlkesPasien)
        {
            $subsidi = array();   
            if($modObatAlkesPasien->carabayar_id != ''){
            $sql = "SELECT * FROM tanggunganpenjamin_m
                        WHERE carabayar_id = ".$modObatAlkesPasien->carabayar_id."
                          AND penjamin_id = ".$modObatAlkesPasien->penjamin_id."
                          AND kelaspelayanan_id = ".$modObatAlkesPasien->kelaspelayanan_id."
                          AND tipenonpaket_id = ".$modObatAlkesPasien->tipepaket_id."
                          AND tanggunganpenjamin_aktif = TRUE ";
                $data = Yii::app()->db->createCommand($sql)->queryRow();

                $subsidi['asuransi'] = ($data['subsidiasuransioa']!='')?($data['subsidiasuransioa']/100 * $modObatAlkesPasien->hargajual_oa):0;
                $subsidi['pemerintah'] = ($data['subsidipemerintahoa']!='')?($data['subsidipemerintahoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['rumahsakit'] = ($data['subsidirumahsakitoa']!='')?($data['subsidirumahsakitoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['max'] = $data['makstanggpel'];
            } else {
                $subsidi['asuransi'] = 0;
                $subsidi['pemerintah'] = 0;
                $subsidi['rumahsakit'] = 0;
                $subsidi['iurbiaya'] = 0;
            }

            return $subsidi;
        }

        /**
         * simpan jurnalrekening_t
         * @return \JurnalrekeningT
         * @params $ruangan_id = jika ruangan bukan diambil dari getState("ruangan_id")
         * digunakan di:
         * - billingKasir/PembayaranController
         * - billingKasir/PembayaranSupplierController
         * - billingKasir/PembayaranUangMukaController
         * - farmasiApotek/PembayaranLangsungController
         * - gudangFarmasi/FakturPembelianController
         */
        public function saveJurnalRekening($ruangan_id = null)
        {
            $modJurnalRekening = new JurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = date('Y-m-d H:i:s');
            $modJurnalRekening->nobuktijurnal = MyGenerator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = MyGenerator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = date('Y-m-d H:i:s');
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = "";
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENERIMAAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = date('Y-m-d H:i:s');
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modJurnalRekening->ruangan_id = empty($ruangan_id) ? Yii::app()->user->getState('ruangan_id') : $ruangan_id;
            
            
            if($modJurnalRekening->validate()){
//                    echo "<pre>";
//                    print_r($modJurnalRekening->attributes);
//                    exit;
                $modJurnalRekening->save();
            } else {
                $modJurnalRekening['errorMsg'] = $modJurnalRekening->getErrors();
            }
            return $modJurnalRekening;
        }
        /**
         * insert detail dari post rekening digunakan di:
         * - rawatJalan/TindakanController
         * @param type $modJurnalRekening
         * @param type $postRekenings
         * @param type $jenisPelayanan = 'tm' / 'oa' / 'ap'
         * @param type $jenisSimpan
         * @return boolean
         */
        public function saveJurnalDetails($modJurnalRekening, $postRekenings, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                $model[$i] = new JurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                $model[$i]->uraiantransaksi = $rekening['uraiantransaksi'];
                $model[$i]->saldodebit = $rekening['saldodebit'];
                $model[$i]->saldokredit = $rekening['saldokredit'];
                $model[$i]->nourut = $i+1;
                $model[$i]->rekening1_id = $rekening['struktur_id'];
                $model[$i]->rekening2_id = $rekening['kelompok_id'];
                $model[$i]->rekening3_id = $rekening['jenis_id'];
                $model[$i]->rekening4_id = $rekening['obyek_id'];
                $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                $model[$i]->uraiantransaksi = "Pembayaran Obat Bebas";
                $model[$i]->catatan = "KASIR";
                if($model[$i]->validate()){
                    $model[$i]->save();
                }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                   echo CHtml::errorSummary($model[$i]);
                   exit();
                    $valid = false;
                    break;
                }
            }
            return $valid;        
        }
        
        /**
         * insert detail dari post rekening digunakan di:
         * - rawatJalan/TindakanController
         * @param type $modJurnalRekening
         * @param type $postRekenings
         * @param type $jenisPelayanan = 'tm' / 'oa' / 'ap'
         * @param type $jenisSimpan
         * @return boolean
         */
        public function saveJurnalDetailsNew($modJurnalRekening, $postRekenings, $jenisSimpan = null, $sebagaipembayaran_bkm){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                $model[$i] = new JurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                $model[$i]->uraiantransaksi = $rekening['uraiantransaksi'];
                $model[$i]->uraiantransaksi = $sebagaipembayaran_bkm;
                $model[$i]->saldodebit = $rekening['saldodebit'];
                $model[$i]->saldokredit = $rekening['saldokredit'];
                $model[$i]->nourut = $i+1;
                $model[$i]->rekening1_id = $rekening['struktur_id'];
                $model[$i]->rekening2_id = $rekening['kelompok_id'];
                $model[$i]->rekening3_id = $rekening['jenis_id'];
                $model[$i]->rekening4_id = $rekening['obyek_id'];
                $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                $model[$i]->catatan = "KASIR";
                if($model[$i]->validate()){
                    $model[$i]->save();
                }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                    $valid = false;
                    break;
                }
            }
            return $valid;        
        }
        
        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        /**
         * untuk load tindakanpelayanan_t yang harus dibayarkan
         * @author ichan | 01-05-2014
         * @params $pendaftaran_id
         * @return BKTindakanPelayananT
         */
        protected function loadTindakans($pendaftaran_id){
            $criteria = new CDbCriteria;
            $criteria->with = array('daftartindakan','tipepaket');
            $criteria->join .= "LEFT JOIN pasienmasukpenunjang_t ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = t.pasienmasukpenunjang_id ";
            $criteria->join .= "LEFT JOIN pasienbatalperiksa_r ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = pasienbatalperiksa_r.pasienmasukpenunjang_id";
            $criteria->compare('t.pendaftaran_id', $pendaftaran_id);
            $criteria->addCondition('t.tindakansudahbayar_id IS NULL');
            if(Yii::app()->user->getState('ruangan_id') != Params::RUANGAN_ID_KASIR_LAB){
                $criteria->addCondition('pasienbatalperiksa_r.pasienbatalperiksa_id IS NULL'); //jangan tampilkan tindakan jika ada pembatalan pemeriksaan
            }
            $modTindakan = BKTindakanPelayananT::model()->findAll($criteria);
            return $modTindakan;
        }
        /**
         * untuk load obatalkespasien_t yang harus dibayarkan
         * @author ichan | 01-05-2014
         * @params $pendaftaran_id
         * @return BKTindakanPelayananT
         */
        protected function loadObatalkes($pendaftaran_id){
            $criteriaoa = new CDbCriteria;
            $criteriaoa->with = array('daftartindakan');
            $criteriaoa->compare('t.pendaftaran_id', $pendaftaran_id);
//                $criteriaoa->addCondition('returresepdet_id is null'); UNTUK KASUS RETUR SEBAGIAN QTY HARUSNYA TETAP MUNCUL DI TAGIHAN PASIEN/KASIR
            $criteriaoa->addCondition('t.qty_oa > 0');
            $criteriaoa->addCondition('t.oasudahbayar_id IS NULL');
            $modObatalkes = BKObatalkesPasienT::model()->findAll($criteriaoa);
            return $modObatalkes;
        }
        
}