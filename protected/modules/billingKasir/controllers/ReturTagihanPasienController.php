<?php

class ReturTagihanPasienController extends SBaseController
{
    protected $successSave = true;
    protected $pesan = "succes";
    protected $is_action = "insert";
    
    public function actionIndex()
	{
                    if(!empty($_GET['frame'])){
                        $this->layout = 'frameDialog';
                    }
                    $modPendaftaran = new BKPendaftaranT;
                    $modPasien = new BKPasienM;
                    $modBuktiKeluar = new BKTandabuktikeluarT;
                    $modBuktiKeluar->tahun = date('Y');
                    $modBuktiKeluar->untukpembayaran = 'Retur Tagihan Pasien';
                    $modBuktiKeluar->nokaskeluar = Generator::noKasKeluar();
                    
                    $modRetur = new BKReturbayarpelayananT;
                    $modRetur->noreturbayar = Generator::noReturBayarPelayanan();
                    $modRetur->biayaadministrasi = 0;
                    $modRetur->is_posting = 'retur';

                    $modJurnalRekening = new JurnalrekeningT;
                    $modJurnalDetail = new JurnaldetailT;
                    $modJurnalPosting = new JurnalpostingT;

                    // pengecekan jika request dari iframe
                    
                    $url_batal = Yii::app()->createAbsoluteUrl(
                        Yii::app()->controller->module->id.'/' .Yii::app()->controller->id
                    );
                    
                    if(!empty($_GET['idPembayaran']) && !isset($_POST['BKReturbayarpelayananT'])){
                        $modPembayaran = BKPembayaranpelayananT::model()->findByPk($_GET['idPembayaran']);
                        $modPendaftaran = BKPendaftaranT::model()->findByPk($modPembayaran->pendaftaran_id);
                        $modPasien = BKPasienM::model()->findByPk($modPembayaran->pasien_id);

                        $modRetur->tandabuktibayar_id = $modPembayaran->tandabuktibayar_id;
                        $modRetur->totaloaretur = $modPembayaran->totalbiayaoa;
                        $modRetur->totaltindakanretur = $modPembayaran->totalbiayatindakan;
                        $modRetur->totalbiayaretur = $modPembayaran->totalbiayapelayanan;

                        $modBuktiKeluar->namapenerima = $modPasien->nama_pasien;
                        $modBuktiKeluar->alamatpenerima = $modPasien->alamat_pasien;
                        
                        $url_batal = Yii::app()->createAbsoluteUrl(
                            Yii::app()->controller->module->id.'/' .Yii::app()->controller->id,
                            array(
                                'idPembayaran'=>$_GET['idPembayaran'],
                                'frame'=>1
                            )
                        );
                    }
                    $successSave = false;
//                    echo nl2br(print_r($_POST,1));exit();
                    if(isset($_POST['BKReturbayarpelayananT']) && !empty($_POST['BKPendaftaranT']['pendaftaran_id'])){
                        $modPendaftaran = BKPendaftaranT::model()->findByPk($_POST['BKPendaftaranT']['pendaftaran_id']);
                        $modPasien = BKPasienM::model()->findByPk($_POST['BKPendaftaranT']['pasien_id']);

                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $modRetur = $this->saveReturBayarPelayanan($_POST['BKReturbayarpelayananT']);
                            $modBuktiKeluar = $this->saveTandaBuktiKeluar($modRetur, $_POST['BKTandabuktikeluarT']);
                            $modJurnalRekening = $this->saveJurnalRekening($modRetur, $_POST['BKReturbayarpelayananT']);

                            if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                            {
                                $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                            }else{
                                $modJurnalPosting = null;
                            }

                            $noUrut = 0;
                            foreach($_POST['RekeningakuntansiV'] AS $i => $post){
                                $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, $modJurnalPosting);
                                $noUrut ++;
                            }

                            // exit();
                            $successSave = $this->successSave;
                            if($successSave){
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                $transaction->commit();
                            } else {
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                                $transaction->rollback();
                            }
                        } catch (Exception $exc) {
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                            $transaction->rollback();
                        }
                    }
                    //$modPendaftaran = new BKPendaftaranT;

                    $this->render('index',
                        array(
                            'modPendaftaran'=>$modPendaftaran,
                            'modPasien'=>$modPasien,
                            'modBuktiKeluar'=>$modBuktiKeluar,
                            'modRetur'=>$modRetur,
                            'successSave'=>$successSave,
                            'url_batal'=>$url_batal
                        )
                    );
	}
        
        protected function saveReturBayarPelayanan($postRetur)
        {
            $modRetur = new BKReturbayarpelayananT;
            $modRetur->attributes = $postRetur;
            $modRetur->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if($modRetur->validate())
            {
                $modRetur->save();
                TandabuktibayarT::model()->updateByPk(
                    $modRetur->tandabuktibayar_id,
                    array(
                        'returbayarpelayanan_id'=>$modRetur->returbayarpelayanan_id
                    )
                );
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modRetur;
        }
        
        protected function saveTandaBuktiKeluar($modRetur,$postBuktiKeluar)
        {
            $modBuktiKeluar = new BKTandabuktikeluarT;
            $modBuktiKeluar->tglkaskeluar = $modRetur->tglreturpelayanan;
            $modBuktiKeluar->jmlkaskeluar = $modRetur->totalbiayaretur;
            $modBuktiKeluar->biayaadministrasi = $modRetur->biayaadministrasi;
            $modBuktiKeluar->keterangan_pengeluaran = $modRetur->keteranganretur;
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->returbayarpelayanan_id = $modRetur->returbayarpelayanan_id;
            $modBuktiKeluar->tahun = date('Y');
            
            if($modBuktiKeluar->validate())
            {
                $modBuktiKeluar->save();
                $this->successSave = $this->successSave && true;
                BKReturbayarpelayananT::model()->updateByPk(
                    $modRetur->returbayarpelayanan_id,
                    array(
                        'tandabuktikeluar_id'=>$modBuktiKeluar->tandabuktikeluar_id
                    )
                );
            } else {
                $this->successSave = false;
            }
            
            return $modBuktiKeluar;
        }

    protected function saveJurnalRekening($modRetur, $postPenUmum)
        {
            $modJurnalRekening = new JurnalrekeningT;
            $modJurnalRekening->tglbuktijurnal = $modRetur->tglreturpelayanan;
            $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
            $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
            $modJurnalRekening->noreferensi = 0;
            $modJurnalRekening->tglreferensi = $modRetur->tglreturpelayanan;
            $modJurnalRekening->nobku = "";
            $modJurnalRekening->urianjurnal = "Retur ".$postPenUmum['noreturbayar'];
            /*
            $attributes = array(
                'jenisjurnal_aktif' => true
            );
            $jenisjurnal_id = JenisjurnalM::model()->findByAttributes($attributes);
            $modJurnalRekening->jenisjurnal_id = $jenisjurnal_id->jenisjurnal_id;
             * 
             */
            
            $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
            $periodeID = Yii::app()->session['periodeID'];
            $modJurnalRekening->rekperiod_id = $periodeID[0];
            $modJurnalRekening->create_time = $modRetur->tglreturpelayanan;
            $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
            $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
            
            if($modJurnalRekening->validate()){
                $modJurnalRekening->save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $this->pesan = $modJurnalRekening->getErrors();
            }

            return $modJurnalRekening;
        }

        public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
            $modJurnalDetail = new JurnaldetailT();
            $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
            $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
            $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
            $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
            $modJurnalDetail->saldodebit = $post['saldodebit'];
            $modJurnalDetail->saldokredit = $post['saldokredit'];
            $modJurnalDetail->nourut = $noUrut;
            $modJurnalDetail->rekening1_id = $post['struktur_id'];
            $modJurnalDetail->rekening2_id = $post['kelompok_id'];
            $modJurnalDetail->rekening3_id = $post['jenis_id'];
            $modJurnalDetail->rekening4_id = $post['obyek_id'];
            $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
            $modJurnalDetail->catatan = "";

            if($modJurnalDetail->validate()){
                $modJurnalDetail->save();
            }
            return $modJurnalDetail;        
        }

        protected function saveJurnalPosting($arrJurnalPosting)
        {
            $modJurnalPosting = new JurnalpostingT;
            $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
            $modJurnalPosting->keterangan = "Posting automatis";
            $modJurnalPosting->create_time = date('Y-m-d H:i:s');
            $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
            $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
            if($modJurnalPosting->validate()){
                $modJurnalPosting->save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
                $this->pesan = $modJurnalPosting->getErrors();
            }
            return $modJurnalPosting;
        } 

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
