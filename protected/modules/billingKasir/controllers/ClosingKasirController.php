<?php

class ClosingKasirController extends SBaseController
{
    protected $successSave = true;
    protected $pesan = "succes";
    public function actionIndex($id=null)
    {
        $format = new CustomFormat();
        $informasi = array();
        
        $model = new BKClosingkasirT();
        $model->tglclosingkasir = date('Y-m-d H:m:s');
        
        $mSetorBank = new BKSetorbankT();
        $mBuktBayar = new BKTandabuktibayarT();
        $mBuktBayar->ruangan_id = Yii::app()->user->getState('ruangan_id');
        $mBuktBayar->tglAwal = date('d M Y 00:00:00');
        $mBuktBayar->tglAkhir = date('d M Y 23:59:59');
        $mBuktBayar->shift_id = Yii::app()->user->getState('shift_id');
        $mBuktBayar->create_loginpemakai_id = Yii::app()->user->getState('pegawai_id');

        if (!empty($id)) {
            $model = BKClosingkasirT::model()->findByPk($id);
            $model->isNewRecord = FALSE;
        }
        
        if(isset($_POST['BKTandabuktibayarT']))
        {
            $mBuktBayar->attributes = $_POST['BKTandabuktibayarT'];
            $mBuktBayar->tglAwal = $format->formatDateTimeMediumForDB($_POST['BKTandabuktibayarT']['tglAwal']);
            $mBuktBayar->tglAkhir = $format->formatDateTimeMediumForDB($_POST['BKTandabuktibayarT']['tglAkhir']);
            
            $model->closingdari = $mBuktBayar->tglAwal;
            $model->sampaidengan = $mBuktBayar->tglAkhir;
            
            $model->pegawai_id = Yii::app()->user->getState('pegawai_id');
            $model->create_loginpemakai_id = Yii::app()->user->getState('pegawai_id');
            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $model->shift_id = $mBuktBayar->shift_id;
            
            $mSetorBank->ygmenyetor_id = $mBuktBayar->create_loginpemakai_id;
            $mSetorBank->create_loginpemakai_id = Yii::app()->user->id;
            
        }
        $criteria = new CDbCriteria;
        $criteria->compare('ruangan_id', $mBuktBayar->ruangan_id);
        $criteria->compare('create_loginpemakai_id', $mBuktBayar->create_loginpemakai_id);
        $criteria->addCondition('closingkasir_id IS NULL');
        $criteria->addBetweenCondition('DATE(tglpenerimaan)', $mBuktBayar->tglAwal, $mBuktBayar->tglAkhir);
        $rPenerimaanUmum = PenerimaanumumT::model()->findAll($criteria);
        $total_penerimaan_umum = 0;
        foreach($rPenerimaanUmum as $val)
        {
            $total_penerimaan_umum += $val['totalharga'];
        }
        $informasi['total_penerimaan_umum'] = $total_penerimaan_umum;
        
        $criteria_dua = new CDbCriteria;
        $criteria_dua->compare('create_ruangan', $mBuktBayar->ruangan_id);
        $criteria_dua->compare('create_loginpemakai_id', $mBuktBayar->create_loginpemakai_id);
        $criteria_dua->addCondition('closingkasir_id IS NULL');
        $criteria_dua->addBetweenCondition('DATE(tglpengeluaran)', $mBuktBayar->tglAwal, $mBuktBayar->tglAkhir);
        $rPengeluaranUmum = PengeluaranumumT::model()->findAll($criteria_dua);
        $total_pengeluaran_umum = 0;
        
        foreach($rPengeluaranUmum as $val)
        {
            $total_pengeluaran_umum += $val['totalharga'];
        }
        $informasi['total_pengeluaran_umum'] = $total_pengeluaran_umum;
        
        $attributes = array('lookup_type'=>'nilaiuang', 'lookup_aktif'=>true);
        $rPecahanUang = LookupM::model()->findAllByAttributes($attributes, array('order'=>'lookup_urutan'));
        
        
        if(isset($_POST['BKClosingkasirT']))
        {   
            $transaction = Yii::app()->db->beginTransaction();
            $model->attributes = $_POST['BKClosingkasirT'];
            $model->closingdari = empty($model->closingdari) ? NULL : $format->formatDateTimeMediumForDB($model->closingdari);
            $model->sampaidengan = empty($model->sampaidengan) ? NULL : $format->formatDateTimeMediumForDB($model->sampaidengan);
            $model->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $model->create_loginpemakai_id = Yii::app()->user->id;
            $model->create_time = date('Y-m-d H:i:s');
            $model->update_time = date('Y-m-d H:i:s');
            $model->pegawai_id = Yii::app()->user->getState('pegawai_id');
            $model->shift_id = Yii::app()->user->getState('shift_id');
            
            try{
                if($model->validate())
                {
                    if($model->save())
                    {
                        $x = 0;
                        foreach($_POST['jum_recehan'] as $key=>$val)
                        {
                            $rincianCloding = new RincianclosingT;
                            $rincianCloding->closingkasir_id = $model->closingkasir_id;
                            $rincianCloding->nourutrincian = $x+1;
                            $rincianCloding->nilaiuang = $key;
                            $rincianCloding->banyakuang = (int) $val;
                            $rincianCloding->jumlahuang = $key*$val;
                            $rincianCloding->save();
                            $x++;
                        }
                        
                        $penerimaan = true;
                        if(isset($_POST['isPenerimaanUmum']))
                        {
                            $penerimaan = $this->savePenerimaan($model, $rPenerimaanUmum);
                        }

                        $pengeluaran = true;
                        if(isset($_POST['isPengeluaranUmum']))
                        {
                            $pengeluaran = $this->savePengeluaran($model, $rPengeluaranUmum);
                        }
                        
                        //=========== Save Jurnal Rekening =================
                        $modJurnalRekening = $this->saveJurnalRekening($model, $_POST['BKClosingkasirT']);
                        // if($_POST['BKReturbayarpelayananT']['is_posting']=='posting')
                        // {
                        //     $modJurnalPosting = $this->saveJurnalPosting($modJurnalRekening);
                        // }else{
                        //     $modJurnalPosting = null;
                        // }
                        $noUrut = 0;
                        foreach($_POST['JenispenerimaanrekeningV'] AS $i => $post){
                            $modJurnalDetail = $this->saveJurnalDetail($modJurnalRekening, $post, $noUrut, null);
                            $noUrut ++;
                        }
                        //==================================================

                        if(isset($_POST['setorBank']))
                        {
                            $mSetorBank->attributes = $_POST['BKSetorbankT'];
                            $mSetorBank->create_time = date('Y-m-d h:m:s');
                            if($mSetorBank->validate())
                            {
                                if($mSetorBank->save())
                                {
                                    $buktiBayar =  $this->saveTandaBuktiBayar($model, $_POST['BKClosingkasirT']['nobuktibayar']);
                                    
                                    if($penerimaan && $pengeluaran && $buktiBayar)
                                    {
                                        $transaction->commit();
                                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                                    }else{
                                        Yii::app()->user->setFlash('error',"Data gagal disimpan");
                                    }
                                }else{
                                    Yii::app()->user->setFlash('error',"Data gagal setor bank disimpan");
                                }
                            }
                        }else{
                            $buktiBayar =  $this->saveTandaBuktiBayar($model, $_POST['BKClosingkasirT']['nobuktibayar']);
                            
                            if($penerimaan && $pengeluaran && $buktiBayar)
                            {
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            }else{
                                Yii::app()->user->setFlash('error',"Data gagal disimpan");
                            }                            
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Data berhasil disimpan.');
                    $model->isNewRecord = FALSE;
                    $this->redirect(array('index','id'=>$model->closingkasir_id));
                }else{
                    Yii::app()->user->setFlash('error',"Data tutup kasir gagal disimpan");
                }
            } catch (Exception $exc) {
                Yii::app()->user->setFlash('error',"Data tidak bisa disimpan ".MyExceptionMessage::getMessage($exc,true));
                $transaction->rollback();
            }
            
        }
        //Mengembalikan format tanggal
        $mBuktBayar->tglAwal = date('d M Y H:i:s', strtotime($mBuktBayar->tglAwal));
        $mBuktBayar->tglAkhir = date('d M Y H:i:s', strtotime($mBuktBayar->tglAkhir));
        
        $this->render('index',
            array(
                'model'=>$model,
                'mBuktBayar'=>$mBuktBayar,
                'rPenerimaanUmum'=>$rPenerimaanUmum,
                'rPengeluaranUmum'=>$rPengeluaranUmum,
                'rPecahanUang'=>$rPecahanUang,
                'informasi'=>$informasi,
                'mSetorBank'=>$mSetorBank
            )
        );
    }
    
    protected function savePenerimaan($params, $penerimaan)
    {
        $record = false;
        foreach($penerimaan as $val)
        {
            $record = PenerimaanumumT::model()->updateByPk($val['penerimaanumum_id'], array('closingkasir_id'=>$params['closingkasir_id']));
            TandabuktibayarT::model()->updateByPk($val['tandabuktibayar_id'], array('closingkasir_id'=>$params['closingkasir_id']));
        }
        return $record;
    }
    
    protected function savePengeluaran($params, $pengeluaran)
    {
        $record = false;
        foreach($pengeluaran as $val)
        {
            $record = PengeluaranumumT::model()->updateByPk($val['pengeluaranumum_id'], array('closingkasir_id'=>$params['closingkasir_id']));
            TandabuktibayarT::model()->updateByPk($val['tandabuktibayar_id'], array('closingkasir_id'=>$params['closingkasir_id']));
        }
        return $record;
    }

    protected function saveTandaBuktiBayar($params, $tanda)
    {
        $record = false;
        $tanda = array_filter($tanda, 'strlen');
        foreach($tanda as $val)
        {
            $attributes = array(
                'nobuktibayar' => trim($val)
            );
            $result = TandabuktibayarT::model()->findByAttributes($attributes);
            $record = TandabuktibayarT::model()->updateByPk($result['tandabuktibayar_id'], array('closingkasir_id'=>$params['closingkasir_id']));
        }
        return $record;
    }
    
    public function actionInformasi(){
        
        $model = new BKInformasiclosingkasirV('searchInformasi');
        $format = new CustomFormat();
        $model->unsetAttributes();
        $model->tgl_awal = date("Y-m-d");
        $model->tgl_akhir = date("Y-m-d");
        if(isset($_GET['BKInformasiclosingkasirV'])){
            $model->attributes = $_GET['BKInformasiclosingkasirV'];
            $model->tgl_awal = $format->formatDateMediumForDB($_GET['BKInformasiclosingkasirV']['tgl_awal']);
            $model->tgl_akhir = $format->formatDateMediumForDB($_GET['BKInformasiclosingkasirV']['tgl_akhir']);
        }

        $this->render('informasi',array('model'=>$model, 'format'=>$format));
    }
    public function actionRincianSetoran($idSetor){
        $this->layout = '//layouts/frameDialog';
        $modSetor = BKSetorbankT::model()->findByPk($idSetor);
        if(!$modSetor){
            Yii::app()->user->setFlash('warning', 'Tidak ada transaksi setor ke Bank !');
            $modSetor = new BKSetorbankT;
        }
        $this->render('rincianSetoran',array(
            'modSetor'=>$modSetor,
        ));
        
    }
    public function actionRincian($idClosing){
        $this->layout = '//layouts/frameDialog';
        $criteria=new CDbCriteria;
        $criteria->addCondition('closingkasir_id = '.$idClosing);
        $criteria->select = "closingkasir_id, nilaiuang, banyakuang, jumlahuang";
        $criteria->group = "closingkasir_id, nilaiuang, banyakuang, jumlahuang";
        $models = BKInformasiclosingkasirV::model()->findAll($criteria);
        $this->render('rincian',array(
            'models'=>$models,
        ));
        
    }
    public function actionBatalclosing($idClosing){
        $this->layout = '//layouts/frameDialog';
        $model = BKClosingkasirT::model()->findByPk($idClosing);
        $modRincian = BKRincianclosingT::model()->findAllByAttributes(array('closingkasir_id'=>$model->closingkasir_id));
        $status;
        $transaction = Yii::app()->db->beginTransaction();
        try{
            if(empty($model->setorkebank_id)){
                $modTandabukti = BKTandabuktibayarT::model()->findAllByAttributes(array('closingkasir_id'=>$idClosing));
                if(count($modTandabukti) > 0){
                    foreach($modTandabukti AS $buktibayar){
                        $buktibayar->closingkasir_id = "";
                        $buktibayar->save();
                    }
                }
                foreach($modRincian AS $rincian){
                    $rincian->delete();
                }
                $model->delete();
                $transaction->commit();
                Yii::app()->user->setFlash('success', 'Closing Kasir berhasil dibatalkan !');
                $status = 1;
            }else{
                Yii::app()->user->setFlash('error', 'Closing Kasir gagal dibatalkan karena sudah melakukan setoran ke bank !');
                $status = 0;
                $this->redirect(array('RincianSetoran', 'idSetor'=>$model->setorbank_id));
            }
        }catch (Exception $exc) {
            Yii::app()->user->setFlash('error',"Closing Kasir gagal dibatalkan ".MyExceptionMessage::getMessage($exc,true));
            $transaction->rollback();
        }
        $this->render('batalclosing',array(
            'model'=>$model,
            'modRincian'=>$modRincian,
            'status'=>$status,
        ));
        
    }

    protected function saveJurnalRekening($model, $postPenUmum)
    {
        $modJurnalRekening = new JurnalrekeningT;
        $modJurnalRekening->tglbuktijurnal = $model->tglclosingkasir;
        $modJurnalRekening->nobuktijurnal = Generator::noBuktiJurnalRek();
        $modJurnalRekening->kodejurnal = Generator::kodeJurnalRek();
        $modJurnalRekening->noreferensi = 0;
        $modJurnalRekening->tglreferensi = $model->tglclosingkasir;
        $modJurnalRekening->nobku = "";
        $modJurnalRekening->urianjurnal = "Closing Kasir ".$model->tglclosingkasir;
        
        $modJurnalRekening->jenisjurnal_id = Params::JURNAL_PENGELUARAN_KAS;
        $periodeID = Yii::app()->session['periodeID'];
        $modJurnalRekening->rekperiod_id = $periodeID[0];
        $modJurnalRekening->create_time = $model->tglclosingkasir;
        $modJurnalRekening->create_loginpemakai_id = Yii::app()->user->id;
        $modJurnalRekening->create_ruangan = Yii::app()->user->getState('ruangan_id');
        $modJurnalRekening->ruangan_id = Yii::app()->user->getState('ruangan_id');
        
        if($modJurnalRekening->validate()){
            $modJurnalRekening->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalRekening->getErrors();
        }

        return $modJurnalRekening;
    }

    public function saveJurnalDetail($modJurnalRekening, $post, $noUrut=0, $modJurnalPosting){
        $modJurnalDetail = new JurnaldetailT();
        $modJurnalDetail->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
        $modJurnalDetail->rekperiod_id = $modJurnalRekening->rekperiod_id;
        $modJurnalDetail->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
        $modJurnalDetail->uraiantransaksi = $modJurnalRekening->urianjurnal;
        $modJurnalDetail->saldodebit = $post['saldodebit'];
        $modJurnalDetail->saldokredit = $post['saldokredit'];
        $modJurnalDetail->nourut = $noUrut;
        $modJurnalDetail->rekening1_id = $post['struktur_id'];
        $modJurnalDetail->rekening2_id = $post['kelompok_id'];
        $modJurnalDetail->rekening3_id = $post['jenis_id'];
        $modJurnalDetail->rekening4_id = $post['obyek_id'];
        $modJurnalDetail->rekening5_id = $post['rincianobyek_id'];
        $modJurnalDetail->catatan = "";

        if($modJurnalDetail->validate()){
            $modJurnalDetail->save();
        }
        return $modJurnalDetail;        
    }

    protected function saveJurnalPosting($arrJurnalPosting)
    {
        $modJurnalPosting = new JurnalpostingT;
        $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
        $modJurnalPosting->keterangan = "Posting automatis";
        $modJurnalPosting->create_time = date('Y-m-d H:i:s');
        $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
        $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
        if($modJurnalPosting->validate()){
            $modJurnalPosting->save();
            $this->successSave = true;
        } else {
            $this->successSave = false;
            $this->pesan = $modJurnalPosting->getErrors();
        }
        return $modJurnalPosting;
    } 

    public function actionPrint($caraPrint)
    {
        $closingkasir_id = $_GET['closingkasir'];
        $model = BKClosingkasirT::model()->findByPk($closingkasir_id);

        $judulLaporan = 'BUKTI SETORAN CLOSING KASIR';
        if($caraPrint=='PRINT') {
            $this->layout='//layouts/printWindows';
            $this->render('print',array('judulLaporan'=>$judulLaporan,
                'caraPrint'=>$caraPrint,'model'=>$model));
        }
    }
}

