<?php

class InformasipenjualanresepController extends SBaseController
{
        protected $pathViewPrint = 'farmasiApotek.views.penjualanResep.PrintBebasLuar';
        
        public function accessRules()
	{
            return array(
                    array('allow',  // allow all users to perform 'index' and 'view' actions
                            'actions'=>array('view','printKwitansi', 'informasijualresep'),
                            'users'=>array('@'),
                    ),
                    array('deny',  // deny all users
                            'users'=>array('*'),
                    ),
            );
	}
        
        public function actionInformasijualresep(){
            $model = new BKPenjualanresepT();
            $model->tglAwal = date('d M Y 00:00:00');
            $model->tglAkhir = date('d M Y H:i:s');
            
            if (isset($_GET['BKPenjualanresepT'])){
                $format = new CustomFormat();
                $model->attributes = $_GET['BKPenjualanresepT'];
                $model->tglAwal = $format->formatDateTimeMediumForDB($model->tglAwal);
                $model->tglAkhir = $format->formatDateTimeMediumForDB($model->tglAkhir);
            }
            
            $this->render('informasijualresep',array('model'=>$model));
        }
        
        public function actionDetailResep($id){
            $this->layout = '//layouts/frameDialog';
            $modPenjualan = BKPenjualanresepT::model()->find('penjualanresep_id = ' . $id . '');
           $obatAlkes = BKObatalkesPasienT::model()->findAll('penjualanresep_id = ' . $modPenjualan->penjualanresep_id . ' ');
           $daftar = BKPendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$obatAlkes[0]->pendaftaran_id));
           $pasien = BKPasienM::model()->findByAttributes(array('pasien_id'=>$obatAlkes[0]->pasien_id));
           
            $judulLaporan='Laporan Penerimaan Kas';
                $this->render('PrintBebasLuar',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'obatAlkes'=>$obatAlkes, 'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));

        }
        /**
        * actionFakturPembayaranApotek digunakan untuk print faktur kasir apotek bebas / resep luar / karyawan / dokter / unit
        * @author Ichan | Ihsan F Rahman <ichan90@yahoo.co.id>
        * @param type $idPenjualanResep
        * @param type $idTandaBukti
        */
        public function actionFakturPembayaranApotek($id, $idTandaBukti = null, $caraPrint=null){
            $this->layout = '//layouts/frameDialog';
            $modPenjualan = PenjualanresepT::model()->findByPk($id);
            $daftar = PendaftaranT::model()->findByAttributes(array('pendaftaran_id'=>$modPenjualan->pendaftaran_id));
            $obatAlkes = ObatalkespasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$id));
            $pasien = PasienM::model()->findByPk($modPenjualan->pasien_id);
            $modPegawaiDokter = new PegawaikaryawanV();
            $modInstalasi = new InstalasiM();
            if(!empty($modPenjualan->pasienpegawai_id))
                $modPegawaiDokter = PegawaikaryawanV::model()->findByAttributes(array('pegawai_id'=>$modPenjualan->pasienpegawai_id));
            if(!empty($modPenjualan->pasieninstalasiunit_id))
                $modInstalasi = InstalasiM::model()->findByAttributes(array('instalasi_id'=>$modPenjualan->pasieninstalasiunit_id));
            $criteria = new CDbCriteria;
            $criteria->compare('t.tandabuktibayar_id', $idTandaBukti);
            $tandabukti = TandabuktibayarT::model()->with('pembayaran')->find($criteria);
            $judulLaporan='Sale Invoice';
    //        $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                 $this->layout='//layouts/printWindows';
            }
            $this->render('fakturPembayaranApotek',array('modPenjualan'=>$modPenjualan, 'daftar'=>$daftar,'pasien'=>$pasien,'modPegawaiDokter'=>$modPegawaiDokter,'modInstalasi'=>$modInstalasi,'obatAlkes'=>$obatAlkes, 'tandabukti'=>$tandabukti,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
         }
        /**
         * actionBuktiKasMasukFarmasi cetak BKM (Bukti Kas Masuk) Farmasi
         * @param type $idPenjualanResep
         * @param type $idTandaBukti
         */
        public function actionBuktiKasMasukFarmasi($idPenjualanResep,$idTandaBukti, $caraPrint = null) {
            if (!empty($idTandaBukti) && !empty($idPenjualanResep)) {
                $this->layout='//layouts/frameDialog';
                $format = new CustomFormat();
                if($caraPrint == "PRINT"){
                    $this->layout='//layouts/printWindows';
                }
                $criteria = new CDbCriteria;
                $criteria->compare('t.tandabuktibayar_id', $idTandaBukti);
                $modPenjualan = PenjualanresepT::model()->findByPk($idPenjualanResep);
                $model = TandabuktibayarT::model()->with('pembayaran')->find($criteria);
                $modObatalkes = ObatalkespasienT::model()->findAllByAttributes(array('penjualanresep_id'=>$idPenjualanResep));
                $rincianTagihan = BKInformasipenjualanaresepV::model()->findAllByAttributes
                        (array('pasien_id'=>$model->pembayaran->pasien_id,
                        'penjualanresep_id'=>$idPenjualanResep));
                $modPegawai = PegawaikaryawanV::model()->findByAttributes(array('pegawai_id'=>$modPenjualan->pasienpegawai_id));
                $modInstalasi = InstalasiM::model()->findByAttributes(array('instalasi_id'=>$modPenjualan->pasieninstalasiunit_id));
                $judulLaporan = 'Tanda Bukti Pembayaran Apotek';
                $this->render('buktiKasMasukFarmasi', array(
                    'model' => $model,
                    'judulLaporan'=> $judulLaporan,
                    'rincianTagihan'=>$rincianTagihan,
                    'modObatalkes'=>$modObatalkes,
                    'modPenjualan'=>$modPenjualan,
                    'modPegawai'=>$modPegawai,
                    'modInstalasi'=>$modInstalasi,
                    'format'=>$format,
                ));
            }
        }
        public function terbilang($x, $style=4, $strcomma=",") {
            if ($x < 0) {
                $result = "minus " . trim($this->ctword($x));
            } else {
                $arrnum = explode("$strcomma", $x);
                $arrcount = count($arrnum);
                if ($arrcount == 1) {
                    $result = trim($this->ctword($x));
                } else if ($arrcount > 1) {
                    $result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
                }
            }
            switch ($style) {
                case 1: //1=uppercase  dan
                    $result = strtoupper($result);
                    break;
                case 2: //2= lowercase
                    $result = strtolower($result);
                    break;
                case 3: //3= uppercase on first letter for each word
                    $result = ucwords($result);
                    break;
                default: //4= uppercase on first letter
                    $result = ucfirst($result);
                    break;
            }
            return $result;
        }
        public function ctword($x) {
            $x = abs($x);
            $number = array("", "satu", "dua", "tiga", "empat", "lima",
                "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
            $temp = "";

            if ($x < 12) {
                $temp = " " . $number[$x];
            } else if ($x < 20) {
                $temp = $this->ctword($x - 10) . " belas";
            } else if ($x < 100) {
                $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
            } else if ($x < 200) {
                $temp = " seratus" . $this->ctword($x - 100);
            } else if ($x < 1000) {
                $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
            } else if ($x < 2000) {
                $temp = " seribu" . $this->ctword($x - 1000);
            } else if ($x < 1000000) {
                $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
            } else if ($x < 1000000000) {
                $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
            } else if ($x < 1000000000000) {
                $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
            } else if ($x < 1000000000000000) {
                $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
            }
            return $temp;
        }
}