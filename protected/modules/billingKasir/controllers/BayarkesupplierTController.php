<?php

class BayarkesupplierTController extends SBaseController
{
	public function actionIndex()
	{
                                $model = new BayarkesupplierT('search');
                                $modFaktur = new BKFakturPembelianT;
                                $format = new CustomFormat();
                                
                                if (isset($_GET['BayarkesupplierT'])) {
                                    if (isset($_GET['BKFakturPembelian']))
                                        $modFaktur->attributes=$_GET['BKFakturPembelianT'];
                                    $model->tglAwal = $format->formatDateMediumForDB($_GET['BayarkesupplierT']['tglAwal']);
                                    $model->tglAkhir = $format->formatDateMediumForDB($_GET['BayarkesupplierT']['tglAkhir']);
                                    if ($_GET['berdasarkanpembayaran']>0){
                                        $model->tglAwalbayarkesupplier = $format->formatDateMediumForDB($_GET['BayarkesupplierT']['tglAwalbayarkesupplier']);
                                        $model->tglAkhirbayarkesupplier = $format->formatDateMediumForDB($_GET['BayarkesupplierT']['tglAkhirbayarkesupplier']);
                                    } else {
                                        $model->tglAwalbayarkesupplier = null;
                                        $model->tglAkhirbayarkesupplier = null;
                                    }
                                }
		$this->render('index',array(
                                    'model'=>$model,
                                    'modFaktur'=>$modFaktur,
                                ));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}