<?php
Yii::import('billingKasir.controllers.PembayaranController'); //UNTUK MENGGUNAKAN FUNGSI saveJurnalRekening() & saveJurnalDetails()
class PembayaranUangMukaController extends SBaseController
{
        protected $actionBayarTagihanPasien = 'index';

	public function actionIndex($idBayaruangmuka = null)
	{
            $modBayaruangmuka = new BKBayaruangmukaT;
            $modPendaftaran = new BKPendaftaranT;
            $modPasien = new BKPasienM;
            $modTandaBukti = new BKTandabuktibayarUangMukaT;
            if(!empty($idBayaruangmuka)){
                $modBayaruangmuka = BKBayaruangmukaT::model()->findByPk($idBayaruangmuka);
                $modTandaBukti = BKTandabuktibayarUangMukaT::model()->findByPk($modBayaruangmuka->tandabuktibayar_id);
                $modPendaftaran = BKPendaftaranT::model()->findByPk($modBayaruangmuka->pendaftaran_id);
                $modPasien = BKPasienM::model()->findByPk($modBayaruangmuka->pasien_id);
            }
            $totTagihan = 0;
            $totPembebasanTarif = 0;

            if(!empty($_GET['frame']))
            {
                $this->layout = 'frameDialog';

                $bayaruangmuka = BKBayaruangmukaT::model()->findByPk($_GET['idBayarUangMuka']);

                $tandaBukti = BKTandabuktibayarUangMukaT::model()->findByPk($modBayaruangmuka->tandabuktibayar_id);
                $pendaftaran = BKPendaftaranT::model()->findByPk($modBayaruangmuka->pendaftaran_id);
                $pasien = BKPasienM::model()->findByPk($modBayaruangmuka->pasien_id);

                $result = $this->infoPembayaranUangMuka($modBayaruangmuka->pendaftaran_id);
                $totTagihan = $result['jmlpembayaran'];

                $modPendaftaran->attributes = $pendaftaran->attributes;
                $modPendaftaran->pendaftaran_id = $bayaruangmuka->pendaftaran_id;
                $modPasien->attributes = $pasien->attributes;
                $modTandaBukti->attributes = $tandaBukti->attributes;

            }

            if(isset($_POST['BKTandabuktibayarUangMukaT']))
            {
                $totTagihan = $_POST['totTagihan'];
                $totPembebasanTarif = $_POST['totPembebasan'];
                $modPendaftaran = BKPendaftaranT::model()->findByPk($_POST['BKPendaftaranT']['pendaftaran_id']);
                $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);

                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if(!empty($_GET['frame']))
                    {
                        $modBayaruangmuka = $this->updateBayarUangMuka($modBayaruangmuka, $_POST);
                    }else{
                        $modTandaBukti = $this->saveTandaBuktiBayar(
                            $_POST['BKTandabuktibayarUangMukaT'],$modPendaftaran,$modPasien,$_POST['JenispenerimaanrekeningV']
                        );
                    }
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    if(empty($modBayaruangmuka->bayaruangmuka_id))
                        $modBayaruangmuka = BKBayaruangmukaT::model()->findByAttributes(array('tandabuktibayar_id'=>$modTandaBukti->tandabuktibayar_id));
                    $this->redirect(array('index','idBayaruangmuka'=>$modBayaruangmuka->bayaruangmuka_id));
                }catch(Exception $exc){
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }

            }

            $this->render('index',
                array(
                    'modBayaruangmuka'=>$modBayaruangmuka,
                    'modPendaftaran'=>$modPendaftaran,
                    'modPasien'=>$modPasien,
                    'modTandaBukti'=>$modTandaBukti,
                    'totTagihan'=>$totTagihan,
                    'totPembebasanTarif'=>$totPembebasanTarif,
                    'successSave'=>$successSave
                )
            );
	}

        protected function updateBayarUangMuka($model_bayar_muka,$post)
        {
            $update = BKBayaruangmukaT::model()->updateByPk(
                $model_bayar_muka->bayaruangmuka_id,
                array(
                    'jumlahuangmuka'=>$post['BKTandabuktibayarUangMukaT']['jmlpembayaran']
                )
            );

            $update_bukti_bayar = TandabuktibayarT::model()->updateByPk(
                $model_bayar_muka->tandabuktibayar_id,
                array(
                    'jmlpembayaran'=>$post['BKTandabuktibayarUangMukaT']['jmlpembayaran'],
                    'uangditerima'=>$post['BKTandabuktibayarUangMukaT']['uangditerima'],
                )
            );
            return $update;
        }


        protected function saveTandaBuktiBayar($postTandaBukti,$modPendaftaran,$modPasien,$postRekenings = array())
        {
            $modTandaBukti = new BKTandabuktibayarUangMukaT;
            $modTandaBukti->attributes = $postTandaBukti;
            $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modTandaBukti->nourutkasir = MyGenerator::noUrutKasir($modTandaBukti->ruangan_id);
            $modTandaBukti->nobuktibayar = MyGenerator::noBuktiBayar();

            if($modTandaBukti->validate())
            {
                if(isset($postRekenings)){//simpan jurnal rekening
                    $modJurnalRekening = PembayaranController::saveJurnalRekening();
                    //update jurnalrekening_id
                    $modTandaBukti->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                      ADA DI BAWAH >> $modTandaBukti->save();
                    $sebagaipembayaran_bkm = isset($modTandaBukti->sebagaipembayaran_bkm) ? $modTandaBukti->sebagaipembayaran_bkm : '-';
                    $saveDetailJurnal = PembayaranController::saveJurnalDetailsNew($modJurnalRekening, $postRekenings, null, $sebagaipembayaran_bkm);
                }
                $modTandaBukti->save();
                $this->saveBayarUangMuka($modTandaBukti, $modPendaftaran, $modPasien);

            }else{
                throw new Exception('Data Tanda Bukti Bayar tidak valid');
            }

            return $modTandaBukti;
        }

        protected function saveBayarUangMuka($modTandaBukti,$modPendaftaran,$modPasien)
        {
            $modUangMuka = new BayaruangmukaT;
            $modUangMuka->tandabuktibayar_id = $modTandaBukti->tandabuktibayar_id;
            $modUangMuka->tgluangmuka = $modTandaBukti->tglbuktibayar;
            $modUangMuka->jumlahuangmuka = $modTandaBukti->jmlpembayaran;
            $modUangMuka->pendaftaran_id = $modPendaftaran->pendaftaran_id;
            $modUangMuka->pasien_id = $modPendaftaran->pasien_id;
            $modUangMuka->pasienadmisi_id = $modPendaftaran->pasienadmisi_id;
            $modUangMuka->ruangan_id = Yii::app()->user->getState('ruangan_id');

            if($modUangMuka->validate())
            {
                $modUangMuka->save();
                $this->updateTandaBukti($modTandaBukti, $modUangMuka);
            }else{
                throw new Exception('Data Uang Muka tidak valid');
            }

        }

        protected function updateTandaBukti($modTandaBukti,$modUangMuka)
        {
            TandabuktibayarT::model()->updateByPk($modTandaBukti->tandabuktibayar_id, array('bayaruangmuka_id'=>$modUangMuka->bayaruangmuka_id));
        }

        protected function infoPembayaranUangMuka($idPendaftaran)
        {
            $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
            $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);

            $criteria = new CDbCriteria;
            $criteria->compare('pendaftaran_id', $idPendaftaran);
            $criteria->addCondition('tindakansudahbayar_id IS NULL');
            $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);

            $criteriaoa = new CDbCriteria;
            $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
            $criteriaoa->addCondition('returresepdet_id is null');
            $criteriaoa->addCondition('oasudahbayar_id IS NULL');
            $criteriaoa->addCondition('penjualanresep_id is not null');
            $criteriaoa->order = 'penjualanresep_id';
            $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);

            $modTandaBukti = new TandabuktibayarT;
            $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
            $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;

            $totTarifTind = 0 ;
            $totHargaSatuanObat = 0;
            foreach($modTindakan as $j=>$tindakan){
                $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
            }

            foreach($modObatalkes as $i=>$obatAlkes) {
                $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
            }
            $totalPembagi = $totTarifTind + $totHargaSatuanObat;

            $totSubAsuransi=0;$totSubPemerintah=0;$totSubRs=0;$totCyto=0;$totTarif=0;$totQty=0;$totDiscount_tindakan=0;$subsidi=0;$subsidiOs=0;$totPembebasanTarif=0;$totIur=0;$iurBiaya=0;$totIurOa=0;$totIurOa=0;$totalbayartindakan=0;$totalbayarOa=0;$totTarifTind=0;$totHargaSatuanObat=0;
            foreach($modTindakan as $i=>$tindakan)
            {
                $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                $tarifBebas = 0;
                foreach ($pembebasanTarif as $i => $pembebasan) {
                  $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
                }
                $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
                $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
                $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
                $totDiscount_tindakan += $discountTindakan ;

                $subsidi = $this->cekSubsidi($tindakan);
                $tarifSatuan = $tindakan->tarif_satuan;
                $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan;
                $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan;
                $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto;
                if(!empty($subsidi['max'])){
                      $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']);
                      $subsidiPemerintah = 0;
                      $subsidiRumahSakit = 0;

                      $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                      $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah;
                      $totSubRs = $totSubRs + $subsidiRumahSakit;
                      $iurBiaya = round(($tarifSatuan + $tarifCyto));
                      $totIur = $totIur + $iurBiaya;
                      $subTotal = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi - $discountTindakan;
                      $subTotal = ($subTotal > 0) ? $subTotal : 0;
                      $totalbayartindakan = $totalbayartindakan + $subTotal;
                } else {
                      $subsidiAsuransi = $subsidi['asuransi'];
                      $subsidiPemerintah = $subsidi['pemerintah'];
                      $subsidiRumahSakit = $subsidi['rumahsakit'];

                      $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi;
                      $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah;
                      $totSubRs = $totSubRs + $subsidiRumahSakit;
                      $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit));
                      $totIur = $totIur + $iurBiaya;
                      $subTotal = $iurBiaya * $qtyTindakan;
                      $subTotal -= $discountTindakan;
                      $totalbayartindakan = $totalbayartindakan + $subTotal;
                }

                $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
            }

            $totQtyOa = 0;
            $totHargaSatuanObat = 0;
            $totCytoOa = 0;
            $totalbayarOa = 0;
            $totCytoOa = 0;
            $totHargaSatuan = 0;
            $totSubAsuransiOa = 0;
            $totSubPemerintahOa = 0;
            $totSubRsOa = 0;
            $totSubAsuransiOa = 0;
            $totDiscount_oa = 0;
            foreach($modObatalkes as $i=>$obatAlkes) {
                $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
                $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
                $totDiscount_oa += $discount_oa;
                $subsidiOa = $this->cekSubsidiOa($obatAlkes);
                $totQtyOa = $totQtyOa + $obatAlkes->qty_oa;
                $totHargaSatuan = $totHargaSatuan + $obatAlkes->hargasatuan_oa;
                $oaHargasatuan = $obatAlkes->hargasatuan_oa;
                if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
                    $biayaServiceResep = 0;
                }else{
                    $biayaServiceResep = $obatAlkes->biayaservice;
                }
                $oaCyto = $obatAlkes->biayaadministrasi + $biayaServiceResep + $obatAlkes->biayakonseling;
                $totCytoOa = $totCytoOa + $obatAlkes->biayaadministrasi + $obatAlkes->biayaservice + $obatAlkes->biayakonseling;
                if(!empty($subsidiOa['max'])){
                      $oaSubsidiasuransi = round($oaHargasatuan/$totalPembagi * $subsidiOa['max']);
                      $oaSubsidipemerintah = 0;
                      $oaSubsidirs = 0;

                      $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi;
                      $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah;
                      $totSubRsOa = $totSubRsOa + $oaSubsidirs;
                      $oaIurbiaya = round(($oaHargasatuan + $oaCyto));
                      $obatAlkes->iurbiaya = $oaIurbiaya;
                      $totIurOa = $totIurOa + $oaIurbiaya;
                      $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) - $oaSubsidiasuransi - $discount_oa;
                      $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                      $totalbayarOa = $totalbayarOa + $subTotalOa;
                } else {
                      $oaSubsidiasuransi = $subsidiOa['asuransi'];
                      $oaSubsidipemerintah = $subsidiOa['pemerintah'];
                      $oaSubsidirs = $subsidiOa['rumahsakit'];

                      $totSubAsuransiOa = $totSubAsuransiOa + $oaSubsidiasuransi;
                      $totSubPemerintahOa = $totSubPemerintahOa + $oaSubsidipemerintah;
                      $totSubRsOa = $totSubRsOa + $oaSubsidirs;
                      $oaIurbiaya = round(($oaHargasatuan) - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs)); // tanpa tarif cyto
                      $obatAlkes->iurbiaya = $oaIurbiaya;
                      $totIurOa = $totIurOa + $oaIurbiaya;
                      $subTotalOa = ($oaIurbiaya * $obatAlkes->qty_oa) + $oaCyto - $discount_oa;
                      $totalbayarOa = $totalbayarOa + $subTotalOa;
                }

                $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
            }

            $pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;
            $totTagihan = round($totalbayartindakan + $totalbayarOa);

            $totDeposit = 0;
            $modTandaBukti->jmlpembayaran = $totTagihan;
            $modTandaBukti->biayaadministrasi = 0;
            $modTandaBukti->biayamaterai = 0;
            $modTandaBukti->uangkembalian = 0;
            $modTandaBukti->uangditerima = $totTagihan;
            $pembulatan = ($pembulatanHarga > 0 ) ? $modTandaBukti->jmlpembayaran % $pembulatanHarga : 0;
            if($pembulatan>0){
                $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
                $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan - $totPembebasanTarif - $totDeposit;
                $harusDibayar = $modTandaBukti->jmlpembayaran;
            } else {
                $modTandaBukti->jmlpembulatan = 0;
            }
            $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;

            $returnVal['tottagihan'] = (!empty($totTagihan)) ? $totTagihan: 0;
            $returnVal['totpembebasan'] = (!empty($totPembebasanTarif)) ? $totPembebasanTarif: 0;
            $returnVal['jmlpembulatan'] = (!empty($modTandaBukti->jmlpembulatan)) ? $modTandaBukti->jmlpembulatan : 0;
            $returnVal['jmlpembayaran'] = (!empty($modTandaBukti->jmlpembayaran)) ? $modTandaBukti->jmlpembayaran : 0;
            $returnVal['uangditerima'] = (!empty($modTandaBukti->uangditerima)) ? $modTandaBukti->uangditerima : 0;
            $returnVal['uangkembalian'] = (!empty($modTandaBukti->uangkembalian)) ? $modTandaBukti->uangkembalian : 0;
            $returnVal['biayamaterai'] = (!empty($modTandaBukti->biayamaterai)) ? $modTandaBukti->biayamaterai : 0;
            $returnVal['biayaadministrasi'] = (!empty($modTandaBukti->biayaadministrasi)) ? $modTandaBukti->biayaadministrasi : 0;
            $returnVal['namapasien'] = (!empty($modPasien->nama_pasien)) ? $modPasien->nama_pasien : 0;
            $returnVal['alamatpasien'] = (!empty($modPasien->alamat_pasien)) ? $modPasien->alamat_pasien : 0;
            $returnVal['subsidi'] = $subsidi;
            $returnVal['subsidiOa'] = $subsidiOs;
            return $returnVal;
        }

        protected function cekSubsidi($modTindakan)
        {
            $subsidi = array();
            switch ($modTindakan->tipepaket_id) {
                case Params::TIPEPAKET_NONPAKET:
                        $sql = "SELECT * FROM tanggunganpenjamin_m
                                WHERE carabayar_id = ".$modTindakan->carabayar_id."
                                  AND penjamin_id = ".$modTindakan->penjamin_id."
                                  AND kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                  AND tipenonpaket_id = ".$modTindakan->tipepaket_id."
                                  AND tanggunganpenjamin_aktif = TRUE ";
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransitind']!='')?($data['subsidiasuransitind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintahtind']!='')?($data['subsidipemerintahtind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakittind']!='')?($data['subsidirumahsakittind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['max'] = $data['makstanggpel'];
                    break;
                case Params::TIPEPAKET_LUARPAKET:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
                case null:
                        $subsidi['asuransi'] = 0;
                        $subsidi['pemerintah'] = 0;
                        $subsidi['rumahsakit'] = 0;
                        $subsidi['iurbiaya'] = 0;
                    break;
                default:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
            }

            return $subsidi;
        }

        protected function cekSubsidiOa($modObatAlkesPasien)
        {
            $subsidi = array();
            if($modObatAlkesPasien->carabayar_id != ''){
            $sql = "SELECT * FROM tanggunganpenjamin_m
                        WHERE carabayar_id = ".$modObatAlkesPasien->carabayar_id."
                          AND penjamin_id = ".$modObatAlkesPasien->penjamin_id."
                          AND kelaspelayanan_id = ".$modObatAlkesPasien->kelaspelayanan_id."
                          AND tipenonpaket_id = ".$modObatAlkesPasien->tipepaket_id."
                          AND tanggunganpenjamin_aktif = TRUE ";
                $data = Yii::app()->db->createCommand($sql)->queryRow();

                $subsidi['asuransi'] = ($data['subsidiasuransioa']!='')?($data['subsidiasuransioa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['pemerintah'] = ($data['subsidipemerintahoa']!='')?($data['subsidipemerintahoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['rumahsakit'] = ($data['subsidirumahsakitoa']!='')?($data['subsidirumahsakitoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['max'] = $data['makstanggpel'];
            } else {
                $subsidi['asuransi'] = 0;
                $subsidi['pemerintah'] = 0;
                $subsidi['rumahsakit'] = 0;
                $subsidi['iurbiaya'] = 0;
            }

            return $subsidi;
        }
        /**
         * actionDetailKasMasuk untuk menampilkan & Cetak BKM
         */
        public function actionDetailKasMasuk($idBayaruangmuka, $caraPrint = null)
        {
            $modBayaruangmuka = BayaruangmukaT::model()->findByPk($idBayaruangmuka);
            $modTandaBukti = BKTandabuktibayarUangMukaT::model()->findByPk($modBayaruangmuka->tandabuktibayar_id);
            $modPendaftaran = BKPendaftaranT::model()->findByPk($modBayaruangmuka->pendaftaran_id);
            $modPasien = BKPasienM::model()->findByPk($modBayaruangmuka->pasien_id);
            $func = new MyFunction;

            $no_bkm = '';
            $tgl_bkm = '';
            $pembayar = '';
            $total_bayar = '';
            $total_bayar_huruf = '';

            $data = null;
            $data->tglpembayaran = date('d-m-Y', strtotime($modBayaruangmuka->tgluangmuka));
            $data->keterangan = $modTandaBukti->sebagaipembayaran_bkm."<br>"."No. Pendaftaran: ".$modPendaftaran->no_pendaftaran." - Tgl. Pendaftaran: ".($modPendaftaran->tgl_pendaftaran)." - No.RM: ".$modPasien->no_rekam_medik." - Nama Pasien:".$modPasien->nama_pasien;
            if(!empty($modBayaruangmuka->keteranganuangmuka)){
                $data->keterangan .= "<br>".$modBayaruangmuka->keteranganuangmuka;
            }
            $data->jumlah = $func->formatNumber($modBayaruangmuka->jumlahuangmuka);

            $total_bayar = $modBayaruangmuka->jumlahuangmuka;
            $no_bkm = $modTandaBukti->nobuktibayar;
            $tgl_bkm = date('d-m-Y', strtotime($modTandaBukti->tglbuktibayar));
            $pembayar = $modTandaBukti->darinama_bkm;

            $data = array(
                'header'=>array(
                    'no_bkm'=>$no_bkm,
                    'tgl_bkm'=>$tgl_bkm,
                    'total_bayar'=>$func->formatNumber($total_bayar),
                    'total_bayar_huruf'=>$func->terbilang($total_bayar),
                    'pembayar'=>$pembayar,
                ),
                'detail'=>$data,
                'footer'=>"",
            );
            if($caraPrint == 'PRINT')
            {
                $this->layout='//layouts/printWindows';
                $this->render('detailKasMasuk',
                    array(
                        'data'=>$data,
                        'caraPrint'=>$caraPrint
                    )
                );
            }else if($caraPrint == 'PDF'){
                $this->layout = '//layouts/frameDialog';
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF);
                $mpdf->useOddEven = 2;
//                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML(
                    $this->render('detailKasMasuk',
                        array(
                            'data'=>$data,
                            'caraPrint'=>$caraPrint
                        ),true
                    )
                );
                $mpdf->Output();
            }else{
                $this->layout='//layouts/frameDialog';
                $this->render('detailKasMasuk',
                    array(
                        'data'=>$data,
                        'caraPrint'=>$caraPrint
                    )
                );
            }
        }

        public function actionGenerateInfoPasien()
        {
          if(Yii::app()->request->isPostRequest){
            $data = $_POST;
            $template = $this->renderPartial('info_pasien',
              array(
                'data'=>$data
              ), true
            );
            echo CJSON::encode(array(
              'template'=>$template,
              'data'=>$data
            ));
          }else echo "DATA TIDAK DITEMUKAN";
          Yii::app()->end();
        }

        public function actionPencarianPasien()
        {
          $this->layout = false;
          Yii::app()->clientScript->scriptMap['jquery.js'] = false;
          $id_grid = Yii::app()->request->getParam("id_grid");
          $jenis_instalasi = Yii::app()->request->getParam("jenis_instalasi");

          if($jenis_instalasi == "RANAP"){
            $model = new PasienrawatinapV;
            $model->unsetAttributes();
            $model->instalasi_id = 4;
            if (isset($_GET['PasienrawatinapV'])){
              $model->attributes = $_GET['PasienrawatinapV'];
            }
          }elseif($jenis_instalasi == "RAJAL"){
            $model = new PasienrawatjalankasirV;
            $model->unsetAttributes();
            $model->instalasi_id = 2;
            if (isset($_GET['PasienrawatjalankasirV'])){
              $model->attributes = $_GET['PasienrawatjalankasirV'];
            }
          }elseif($jenis_instalasi == "DAR"){
            $model = new PasienrawatdaruratkasirV;
            $model->unsetAttributes();
            $model->instalasi_id = 3;
            if (isset($_GET['PasienrawatdaruratkasirV'])){
              $model->attributes = $_GET['PasienrawatdaruratkasirV'];
            }
          }elseif($jenis_instalasi == "LAB"){
            $model = new PasienrawatpenunjanglabV;
            $model->unsetAttributes();
            $model->instalasi_id = 5;
            if (isset($_GET['PasienrawatpenunjanglabV'])){
              $model->attributes = $_GET['PasienrawatpenunjanglabV'];
            }
          }

          $this->render('daftar_pasien',array(
            'model'=>$model,
            'id_grid'=>$id_grid
          ));
        }

        protected function generatedButton($data, $row, $dataColumn)
      	{
          $rec = json_encode($data->attributes);
          $hasil = CHtml::link("<i class='icon-check'></i>", "javascript:void(0)", array(
            "onClick"=>"
              var record = ". $rec .";
              $(\"#modal_form_dokumen\").find(\".exit\").trigger(\"click\");
              $.each(record, function(key, val){
                  $('#pembayaran-form').find('[name*=\"['+ key +']\"]').val(val);
              });
              $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(record.nama_pasien);
              $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(record.alamat_pasien);
              loadPembayaran(record.pendaftaran_id);
            ",
          ));
      		return $hasil;
      	}

}
