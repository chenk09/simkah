<?php

class PasienRawatInapSedangDirawatController extends SBaseController
{
        /**
	 * @return array action filters
	 */
        public $successSave; 
        public $successUpdateMasukKamar= false; 
        public $successPasienPulang= false; 
        public $successUpdatePendaftaran= false; 
        public $successUpdatePasienAdmisi= false; 
        public $successRujukanKeluar= true; 
        public $successPaseinM= true; 
        public $successSaveTindakanKomponen = true;
        public $successSaveTindakan;
        
       
        
	public function actionIndex()
	{
           
                $this->pageTitle = Yii::app()->name." - Pasien Rawat Inap Sedang Dirawat";
                $format = new CustomFormat();
                $model = new BKPasienrisedangdirawatV;
                $model->tglAwal = date("d M Y").' 00:00:00';
                $model->tglAkhir = date('d M Y h:i:s');
                
                if(isset ($_REQUEST['BKPasienrisedangdirawatV'])){
                    $model->attributes=$_REQUEST['BKPasienrisedangdirawatV'];
                    $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKPasienrisedangdirawatV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKPasienrisedangdirawatV']['tglAkhir']);
                    $model->ceklis = $_REQUEST['BKPasienrisedangdirawatV']['ceklis'];
               }
		$this->render('index',array('model'=>$model));
	}
        
        public function actionPrint() {
        $model = new BKPasienrisedangdirawatV('searchRI');
        $judulLaporan = 'Laporan Pasien Rawat Inap Sedang Dirawat';
         $format = new CustomFormat();
        //Data Grafik
        $data['title'] = 'Grafik Pasien Rawat Inap Sedang Dirawat';
        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKPasienrisedangdirawatV'])) {
            $model->attributes = $_REQUEST['BKPasienrisedangdirawatV'];
            
           $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKPasienrisedangdirawatV']['tglAwal']);
                    $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKPasienrisedangdirawatV']['tglAkhir']);
                      $model->ceklis = $_REQUEST['BKPasienrisedangdirawatV']['ceklis'];
        }
               
        $caraPrint = $_REQUEST['caraPrint'];
        $target = '_print';
        
        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }
    
    public function actionFrame() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKPasienrisedangdirawatV('searchRI');
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');

        //Data Grafik
        $data['title'] = 'Grafik Pasien Rawat Inap Sedang Dirawat';
        $data['type'] = $_GET['type'];
        
        if (isset($_GET['BKPasienrisedangdirawatV'])) {
            $model->attributes = $_GET['BKPasienrisedangdirawatV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPasienrisedangdirawatV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPasienrisedangdirawatV']['tglAkhir']);
               $model->ceklis = $_REQUEST['BKPasienrisedangdirawatV']['ceklis'];
        }
        
        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
      
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);
        
        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
   protected function parserTanggal($tgl){
    $tgl = explode(' ', $tgl);
    $result = array();
    foreach ($tgl as $row){
        if (!empty($row)){
            $result[] = $row;
        }
    }
    return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];
        
    }
        
	
}
