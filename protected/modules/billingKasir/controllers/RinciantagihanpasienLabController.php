
<?php

class RinciantagihanpasienLabController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
        
//	DIKOMEN KARENA FILTER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view', 'rincian'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}


	public function actionIndex()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new BKRinciantagihanpasienpenunjangV('search');
                $model->tglAwal = date('Y-m-d H:i:s');
                $model->tglAkhir = date('Y-m-d H:i:s');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BKRinciantagihanpasienpenunjangV']))
			$model->attributes=$_GET['BKRinciantagihanpasienpenunjangV'];

		$this->render('billingKasir.views.rinciantagihanpasienLab.index',array(
			'model'=>$model,
		));
	}
        
        public function actionRincian($pembayaranpelayanan_id=null, $id){
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya Sementara';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $criteria = new CDbCriteria();
            $criteria->order = 'ruangan_id';
            $criteria->addCondition('pendaftaran_id = '.$id);
            if(strlen($pembayaranpelayanan_id) == 0){
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria);
            }else{
                $criteria->addCondition('tindakansudahbayar_id > 0'); //sudah lunas
                $modRincian = BKRinciantagihapasiensudahlunasV::model()->findAll($criteria);
            }
            //$criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
            //DICOMMENT KARENA TOTAL TIDAK SAMA KETIKA ADA PEMERIKSAAN 3 BILIRUBIN 
//            $modRincian = BKRinciantagihapasienlaboratoriumV::model()->findAllByAttributes(
//                array('pendaftaran_id' => $id),
//                array(
//                    'order'=> 'daftartindakan_id, ruangan_id'
//                )
//            );
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//            $modRincian->pendaftaran_id = $id;
            $this->render('billingKasir.views.rinciantagihanpasienLab.rincian', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
        }
        
        public function actionRincianKasirLabPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya Sementara';
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria = new CDbCriteria();
                $criteria->order = 'ruangan_id';
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id),
                    'pembatalanuangmuka_id IS NULL'
                );

                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'kwitansi';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }
        /**
         * actionRincianKasirSudahBayarPrint = cetak rincian yang sudah bayar / lunas
         * @param type $id
         * @param type $caraPrint
         */
        public function actionRincianKasirLabSudahBayarPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NOT NULL'); //sudah lunas
                $criteria->order = 'ruangan_id';
                $modRincian = BKRinciantagihapasiensudahlunasV::model()->findAll($criteria);                 
                //$modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id),
                    'pembatalanuangmuka_id IS NULL'
                );
                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'kwitansi';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BKRinciantagihanpasienV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rjrinciantagihanpasien-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $data['judulLaporan']='Rincian Biaya Sementara';
            $caraPrint=$_REQUEST['caraPrint'];
            
            $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                array('pendaftaran_id'=>$id)
            );
            $uang_cicilan = 0;
            foreach($uangmuka as $val)
            {
                $uang_cicilan += $val->jumlahuangmuka;
            }
            $data['uang_cicilan'] = $uang_cicilan;
            
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('billingKasir.views.rinciantagihanpasienLab.rincian', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('billingKasir.views.rinciantagihanpasienLab.rincian',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('billingKasir.views.rinciantagihanpasienLab.rincian',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
}
