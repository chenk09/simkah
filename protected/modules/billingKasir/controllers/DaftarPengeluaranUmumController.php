<?php

class DaftarPengeluaranUmumController extends SBaseController
{
	public function actionIndex()
	{
            $modPengeluaran = new BKPengeluaranumumT();
            $format = new CustomFormat();
            $modPengeluaran->tglAwal=date('Y-m-d 00:00:00');
            $modPengeluaran->tglAkhir=date('Y-m-d H:i:s');
		
            if(isset($_GET['BKPengeluaranumumT'])){
                $modPengeluaran->attributes=$_GET['BKPengeluaranumumT'];
                $modPengeluaran->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKPengeluaranumumT']['tglAwal']);
                $modPengeluaran->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKPengeluaranumumT']['tglAkhir']);
            }
            
            $this->render('index', array('modPengeluaran'=>$modPengeluaran));
	}
        
	public function actionReturPengeluaranUmum()
	{
//            $this->render('index', array('modPengeluaran'=>$modPengeluaran));
	}        

}