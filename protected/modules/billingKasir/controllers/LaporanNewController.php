<?php
class LaporanNewController extends SBaseController
{
    public function actionLaporanClosingKasir(){

        $model = new BKLaporanclosingkasirV('searchInformasi');
        $format = new CustomFormat();
        $model->unsetAttributes();
        $model->tglAwal = date("d M Y");
        $model->tglAkhir = date("d M Y");
        if(isset($_GET['BKLaporanclosingkasirV'])){
            $model->attributes = $_GET['BKLaporanclosingkasirV'];
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_GET['BKLaporanclosingkasirV']['create_ruangan'];
        }
//        $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd'),'medium',null);
//        $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd'),'medium',null);

        $this->render('closingKasir/index',array('model'=>$model));
    }

    public function actionPrintLaporanClosingKasir() {
        $model = new BKLaporanclosingkasirV('searchPrint');
        $judulLaporan = 'Laporan Closing Kasir';

        //Data Grafik
        $data['title'] = 'Grafik Laporan Closing Kasir';
        $data['type'] = $_REQUEST['type'];
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
        if (isset($_REQUEST['BKLaporanclosingkasirV'])) {
            $model->attributes = $_REQUEST['BKLaporanclosingkasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_REQUEST['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_REQUEST['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_REQUEST['BKLaporanclosingkasirV']['create_ruangan'];
        }
        $caraPrint = $_REQUEST['caraPrint'];
        $target = 'closingKasir/print';

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    public function actionFrameGrafikLaporanClosingKasir() {
        $this->layout = '//layouts/frameDialog';
        $model = new BKLaporanclosingkasirV('searchGrafik');
        //Data Grafik
        $data['title'] = 'Grafik Laporan Closing Kasir';
        $data['type'] = $_GET['type'];
        if (isset($_GET['BKLaporanclosingkasirV'])) {
            $model->attributes = $_GET['BKLaporanclosingkasirV'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAwal']);
            $model->tglAkhir = $format->formatDateMediumForDB($_GET['BKLaporanclosingkasirV']['tglAkhir']);
            $model->ruanganKasir = $_REQUEST['BKLaporanclosingkasirV']['create_ruangan'];
        }

        $this->render('_grafik', array(
            'model' => $model,
            'data' => $data,
        ));
    }
    
    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target){
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal).' s/d '.$this->parserTanggal($model->tglAkhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode'=>$periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }
    
    protected function parserTanggal($tgl){
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row){
            if (!empty($row)){
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'),'medium',null).' '.$result[1];

    }
    
}
