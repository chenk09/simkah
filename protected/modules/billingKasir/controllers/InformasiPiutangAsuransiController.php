<?php
class InformasiPiutangAsuransiController extends SBaseController {
    
    public function actionIndex(){
        $model = new BKPiutangasuransiT();
        $model->tgl_awal = date('Y-m-d 00:00:00');
        $model->tgl_akhir = date('Y-m-d H:i:s');
        
        $this->render('index', array('model'=>$model));
    }
    
    /**
    * actionRincian = menampilkan rincian piutang
    * @param type $id
    */
    public function actionRincianPiutang($id, $idpembayaran, $idpiutang){
        $this->layout = '//layouts/frameDialog';
        $data['judulLaporan'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
        $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
        $modPendaftaran->pembayaranpelayanan_id = $idpembayaran;
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.pendaftaran_id = '.$id);
        $criteria->addCondition('t.pembayaranpelayanan_id = '.$idpembayaran);
        $criteria->addCondition('t.tindakansudahbayar_id IS NOT NULL'); //sudah lunas
        $criteria->join = '
            LEFT JOIN obatalkespasien_t ON obatalkespasien_t.obatalkespasien_id = t.tindakanpelayanan_id
            LEFT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = obatalkespasien_t.obatalkes_id
            ';
        $criteria->order = 't.ruangan_id ASC, obatalkes_m.jenisobatalkes_id DESC, t.tgl_tindakan ASC';
        $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);

        $criteria_pp = new CDbCriteria();
        $criteria_pp->compare('pendaftaran_id',$id);
        $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
        $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
        
        $modPenjamin = BKPiutangasuransiT::model()->findByAttributes(array('piutangasuransi_id'=>$idpiutang));
        $criteria_pj =  new CDbCriteria();
        $criteria_pj->compare('pendaftaran_id',$id);
        $criteria_pj->select='SUM(jmlpiutangasuransi) as jmlpiutangasuransi';
        $jmlpiutang = BKPiutangasuransiT::model()->find($criteria_pj)->jmlpiutangasuransi;

        $attributes = array(
                            'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                        );

                        $criteria2 = new CDbCriteria();
                        $criteria2->addCondition('pemakaianuangmuka_id IS NOT NULL AND pembatalanuangmuka_id IS NULL'); 
                        $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                        $uang_cicilan = 0;

                        $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);

                        if(count($uang_muka) > 0)
                        {
                            foreach($uang_muka as $val){
                                $uang_cicilan += $val->jumlahuangmuka;
                            }
                        }
            // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
            //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
            // );

            // $uang_cicilan = 0;
            // foreach($uangmuka as $val)
            // {
            //     $uang_cicilan += $val->jumlahuangmuka;
            // }

        $data['uang_cicilan'] = $uang_cicilan;
        $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
 //          BEDA FORMAT >>>  $this->render('billingKasir.views.rinciantagihanpasienV.rincianNew', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
        $this->render('rincianPiutang', array('modPenjamin'=>$modPenjamin, 'jmlpiutang'=>$jmlpiutang, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian,'modPasienpulang'=>$modPasienpulang, 'data'=>$data));
    }
    
    /**
    * actionRincianKasirSudahBayarPrint = cetak rincian yang sudah bayar / lunas
    * @param type $id
    * @param type $caraPrint
    */
    public function actionRincianKasirSudahBayarPrint($id, $caraPrint, $idpembayaran, $idpiutang, $rinci = null) {
        if (!empty($id))
        {
            $format = new CustomFormat();
            $this->layout = '//layouts/frameDialog';
            $data['judulPrint'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.pendaftaran_id = '.$id);
            $criteria->addCondition('t.pembayaranpelayanan_id = '.$idpembayaran);
            $criteria->addCondition('t.tindakansudahbayar_id IS NOT NULL'); //sudah lunas
 //                $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
            //join untuk gruping obat berdasarkan jenis obat
            $criteria->join = '
                LEFT JOIN obatalkespasien_t ON obatalkespasien_t.obatalkespasien_id = t.tindakanpelayanan_id
                LEFT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = obatalkespasien_t.obatalkes_id
                ';
            $criteria->order = 't.ruangan_id ASC, obatalkes_m.jenisobatalkes_id DESC, t.tgl_tindakan ASC';
            $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
            $modPendaftaran = PendaftaranT::model()->findByPk($id);
            $criteria_pp = new CDbCriteria();
            $criteria_pp->compare('pendaftaran_id',$id);
            $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
            $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
            
            $modPenjamin = BKPiutangasuransiT::model()->findByAttributes(array('piutangasuransi_id'=>$idpiutang));
            $criteria_pj =  new CDbCriteria();
            $criteria_pj->compare('pendaftaran_id',$id);
            $criteria_pj->select='SUM(jmlpiutangasuransi) as jmlpiutangasuransi';
            $jmlpiutang = BKPiutangasuransiT::model()->find($criteria_pj)->jmlpiutangasuransi;

            $attributes = array(
                            'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                        );

                        $criteria2 = new CDbCriteria();
                        $criteria2->addCondition('pemakaianuangmuka_id IS NOT NULL AND pembatalanuangmuka_id IS NULL'); 
                        $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                        $uang_cicilan = 0;

                        $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);

                        if(count($uang_muka) > 0)
                        {
                            foreach($uang_muka as $val){
                                $uang_cicilan += $val->jumlahuangmuka;
                            }
                        }
            // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
            //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
            // );

            // $uang_cicilan = 0;
            // foreach($uangmuka as $val)
            // {
            //     $uang_cicilan += $val->jumlahuangmuka;
            // }

            $data['uang_cicilan'] = $uang_cicilan;
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $data['jenis_cetakan'] = 'rincian_tagihan';

            if ($caraPrint == 'EXCEL') 
            {
                $this->layout='//layouts/printExcel';
                $this->render('rincianPiutang',array('modPendaftaran'=>$modPendaftaran, 'jmlpiutang'=>$jmlpiutang, 'modPenjamin'=>$modPenjamin, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));  
            } 
            if($caraPrint == 'PDF')
            {
                $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF(
                        '',
                        $ukuranKertasPDF, //format A4 Or
                    11, //Font SIZE
                    '', //default font family
                    3, //15 margin_left
                    3, //15 margin right
                    25, //16 margin top
                    10, // margin bottom
                    0, // 9 margin header
                    0, // 9 margin footer
                    'P' // L - landscape, P - portrait
                    );  
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                //$header = 0.75 * 72 / (72/25.4);
                //$mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                ///////
                if($rinci == true){
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianSementaraPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modPasienpulang'=>$modPasienpulang, 
                                'modRincian'=>$modRincian, 
                                'modPenjamin'=>$modPenjamin,
                                'jmlpiutang'=>$jmlpiutang,
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                }else{
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianPiutangPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modPasienpulang'=>$modPasienpulang, 
                                'modRincian'=>$modRincian,
                                'modPenjamin'=>$modPenjamin,
                                'jmlpiutang'=>$jmlpiutang,
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                }
                $mpdf->Output();  
            }

        }
    }
    
    public function actionRincianKasirBaruPrint($id, $idpiutang, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya ';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';

                $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria_pp = new CDbCriteria();
                $criteria_pp->compare('pendaftaran_id',$id);
                $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
                $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
                
                $modPenjamin = BKPiutangasuransiT::model()->findByAttributes(array('piutangasuransi_id'=>$idpiutang));
                $criteria_pj =  new CDbCriteria();
                $criteria_pj->compare('pendaftaran_id',$id);
                $criteria_pj->select='SUM(jmlpiutangasuransi) as jmlpiutangasuransi';
                $jmlpiutang = BKPiutangasuransiT::model()->find($criteria_pj)->jmlpiutangasuransi;

                $attributes = array(
                                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                            );
                
                $criteria2 = new CDbCriteria();
                $criteria2->addCondition('pemakaianuangmuka_id IS NULL AND pembatalanuangmuka_id IS NULL'); 
                $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                $uang_cicilan = 0;

                            $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);
                            
                            if(count($uang_muka) > 0)
                            {
                                foreach($uang_muka as $val){
                                    $uang_cicilan += $val->jumlahuangmuka;
                                }
                            }
                // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                // );

                // $uang_cicilan = 0;
                // foreach($uangmuka as $val)
                // {
                //     $uang_cicilan += $val->jumlahuangmuka;
                // }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'rincian_tagihan';

                if ($caraPrint == 'EXCEL') 
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('billingKasir.views.rinciantagihanpasienV.rincianSementara',array('modPendaftaran'=>$modPendaftaran, 'jmlpiutang'=>$jmlpiutang, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));  
                } else
                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('c',$ukuranKertasPDF); 
//                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
//                    $header_title = 
//                    '
//                       <div>&nbsp;</div>
//                        <div style="margin-top:58px;font-family:tahoma;font-size: 8pt;">
//                            <div style="margin-left:1px;width:100px;float:left">No. RM / Reg</div>
//                            <div style="float:left">: '. $modPendaftaran->pasien->no_rekam_medik .' / '. $modPendaftaran->no_pendaftaran .'</div>
//                        </div>
//                    '
//                    ;
                    //$mpdf->SetHTMLHeader($header_title);
                    
                    $footer = '
                    <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;"><tr>
                    <td width="50%"></td>
                    <td width="50%" align="right">{PAGENO} / {nb}</td>
                    </tr></table>
                    ';
                    $mpdf->SetHTMLFooter($footer);
                    
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);                    
                    $mpdf->AddPage($posisi,'','','','',5,5,$header+4,8,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianSementaraPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modPasienpulang'=>$modPasienpulang, 
                                'modRincian'=>$modRincian, 
                                'modPenjamin'=>$modPenjamin,
                                'jmlpiutang'=>$jmlpiutang,
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    
                    $mpdf->Output();
                    exit;
                }

            }
        }
        
}
?>
