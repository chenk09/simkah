<?php
class KwitansiController extends SBaseController
{
//	DIKOMEN KARENA FILTER USER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('view','printKwitansi','viewRincian', 'UpdateDN'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}
        
        /**
         * method untuk melihat kwitansi pembayaran
         * digunakan di:
         * 1. billing Kasir -> informasi -> pasien sudah bayar
         * @param int $idPembayaranPelayanan pembayaranpelayanan_id
         */
	public function actionView($idPembayaranPelayanan)
	{
            if(!empty($_GET['frame']))
                $this->layout = '//layouts/frameDialog';
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modBayar = PembayaranpelayananT::model()->findByPk($idPembayaranPelayanan);
            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
            
            $criteria = new CdbCriteria();
            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaranPelayanan);
            $tindakanSudahBayar = TindakansudahbayarT::model()->findAll($criteria);
            if(!empty($modBayar->pendaftaran_id)){
                $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                $modPendaftaran->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modBayar->pendaftaran->tgl_pendaftaran);
            }else{
                $modPendaftaran = new PendaftaranT;
            }
            $rincianpembayaran = array();
            $tindakan = array();
                
            if (count($tindakanSudahBayar) > 0){
                $totalTindakan=0;
                foreach ($tindakanSudahBayar as $key => $value) {
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['kelompoktindakan'] = $value->daftartindakan->kelompoktindakan->kelompoktindakan_nama;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['harga'] += $value->jmlbiaya_tindakan;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['discount'] += $value->tindakanpelayanan->discount_tindakan;
                    $totalTindakan += ($value->jmlbiaya_tindakan - $value->tindakanpelayanan->discount_tindakan);
                }
                $rincianpembayaran['tindakan'] = $tindakan;
                $rincianpembayaran['tindakan']['totalTindakan'] = $totalTindakan;
            }
            $oaSudahBayar = OasudahbayarT::model()->findAll($criteria);
            $oa = array();
            if (count($oaSudahBayar) > 0 ){
                $totalOa=0;
                foreach ($oaSudahBayar as $key => $value)
                {
                        $oa[0]['kelompoktindakan'] = $value->obatalkes->jenisobatalkes->jenisobatalkes_nama;
//                        $oa[0]['harga'] += ($value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $oa[0]['harga'] += ($value->hargasatuan * $value->qty_oa);
                        $discount = ($value->obatalkespasien->discount > 0 ) ? $value->obatalkespasien->discount/100 : 0 ;
                        $oa[0]['discount'] += ($discount*$value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $oa[0]['biayaadministrasi'] += $value->obatalkespasien->biayaadministrasi;
                        $oa[0]['biayaservice'] += $value->obatalkespasien->biayaservice;
                        $oa[0]['biayakonseling'] += $value->obatalkespasien->biayakonseling;
                        $totalOa += (($value->hargasatuan * $value->qty_oa) - $oa[0]['discount'] + $oa[0]['biayaadministrasi'] + $oa[0]['biayaservice'] + $oa[0]['biayakonseling']);
                }
                $rincianpembayaran['oa'] = $oa;
                $rincianpembayaran['oa']['totalOa'] = $totalOa;
            }
            
//            UNTUK P3 JML BAYAR = 0
//            if($modTandaBukti->jmlpembayaran == 0 && $modBayar->carabayar_id != 2)
//            { //jika jmlpembayaran nol
//                
//                $modTandaBukti->jmlpembayaran = $rincianpembayaran['tindakan']['totalTindakan'] + $rincianpembayaran['oa']['totalOa'];
//            }
		
            //Jika ada perubahan nama pembayar (darinama_bkm)
            if(isset($_POST['TandabuktibayarT'])){
                if(!empty($_POST['TandabuktibayarT']['darinama_bkm'])){
                    $transaction = Yii::app()->db->beginTransaction();
                    try {
                        $updateSukses = TandabuktibayarT::model()->updateByPk($modBayar->tandabuktibayar_id, array('darinama_bkm'=>$_POST['TandabuktibayarT']['darinama_bkm']));
                        if($updateSukses){
                            $transaction->commit();
                            Yii::app()->user->setFlash('success', 'Data berhasil disimpan !');
                            $this->refresh();
                        }else{
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error', 'Data gagal disimpan !');
                        }
                    }catch (Exception $exc) {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    }
                }
            }
            
            $this->render('viewKwitansi',array('pembayarans'=>$pembayarans,
                                       'judulKwitansi'=>$judulKwitansi,
                                       'modPendaftaran'=>$modPendaftaran,
                                       'rincianpembayaran'=>$rincianpembayaran,
                                       'modTandaBukti'=>$modTandaBukti,
                                       'modBayar'=>$modBayar,
                                       'model'=>$model,
//                                       'pembayaranpelayanan'=>$pembayaranpelayanan
                                        ));
	}


    public function actionViewDeposit($idBayarUangMuka)
    {
            if(!empty($_GET['frame']))
                $this->layout = '//layouts/frameDialog';
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modBayar = BayaruangmukaT::model()->findByPk($idBayarUangMuka);
            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
            
            $this->render('viewKwitansiDeposit',array(
                                       'modBayar'=>$modBayar,
                                       'modTandaBukti'=>$modTandaBukti,));
    }

     public function actionPrintKwitansiDeposit($idBayarUangMuka)
    {
            if(!empty($_GET['frame']))
                $this->layout = '//layouts/frameDialog';
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modBayar = BayaruangmukaT::model()->findByPk($idBayarUangMuka);
            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
            
            if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $ukuranKertasPDF = 'KW';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->SetWatermarkText('COPY');
                $mpdf->showWatermarkText = true;
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1); 
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                $header = 0.78 * 72 / (72/25.4);
                $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                $mpdf->WriteHTML(
                    $this->renderPartial(
                        'billingKasir.views.kwitansi.viewKwitansiDepositPdf',
                        array(
                            'modBayar'=>$modBayar,
                            'modTandaBukti'=>$modTandaBukti,
                        ),true
                    )
                );
                $mpdf->Output();
            }    

    }
        
        /**
         * method untuk print kwitansi
         * digunakan di :
         * 1. Billing Kasir -> Informasi Pasien Sudah Bayar -> Kwitansi Pasien
         * @param int $idPendaftaran pendaftaran_id
         * @param int $idPasienadmisi pasienadmisi_id
         * @param int $idPembayaranPelayanan pembayaranpelayanan_id
         */
        public function actionPrintKwitansi($idPembayaranPelayanan)
        {
            $judulKwitansi = '----- KWITANSI -----';
            $format = new CustomFormat();
            $modBayar = PembayaranpelayananT::model()->findByPk($idPembayaranPelayanan);
            $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
            $criteria = new CdbCriteria();
            $criteria->addCondition('pembayaranpelayanan_id = '.$idPembayaranPelayanan);
            $tindakanSudahBayar = TindakansudahbayarT::model()->findAll($criteria);
            //$modTandaBukti->tglbuktibayar = $format->formatDateTimeMediumForDB();
            if(!empty($modBayar->pendaftaran_id)){
                $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                $modPendaftaran->tgl_pendaftaran = $format->formatDateTimeMediumForDB($modBayar->pendaftaran->tgl_pendaftaran);
            }else{
                $modPendaftaran = new PendaftaranT;
            }
            $rincianpembayaran = array();
            $tindakan = array();
                
            if (count($tindakanSudahBayar) > 0){
                $totalTindakan=0;
                foreach ($tindakanSudahBayar as $key => $value) {
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['kelompoktindakan'] = $value->daftartindakan->kelompoktindakan->kelompoktindakan_nama;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['harga'] += $value->jmlbiaya_tindakan;
                    $tindakan[$value->daftartindakan->kelompoktindakan_id]['discount'] += $value->tindakanpelayanan->discount_tindakan;
                    $totalTindakan += ($value->jmlbiaya_tindakan - $value->tindakanpelayanan->discount_tindakan);
                }
                $rincianpembayaran['tindakan'] = $tindakan;
                $rincianpembayaran['tindakan']['totalTindakan'] = $totalTindakan;
            }
            $oaSudahBayar = OasudahbayarT::model()->findAll($criteria);
            $oa = array();
            if (count($oaSudahBayar) > 0 ){
                $totalOa=0;
                foreach ($oaSudahBayar as $key => $value) {
                        $oa[0]['kelompoktindakan'] = $value->obatalkes->jenisobatalkes->jenisobatalkes_nama;
                        $oa[0]['harga'] += ($value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $discount = ($value->obatalkespasien->discount > 0 ) ? $value->obatalkespasien->discount/100 : 0 ;
                        $oa[0]['discount'] += ($discount*$value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa);
                        $oa[0]['biayaadministrasi'] += $value->obatalkespasien->biayaadministrasi;
                        $oa[0]['biayaservice'] += $value->obatalkespasien->biayaservice;
                        $oa[0]['biayakonseling'] += $value->obatalkespasien->biayakonseling;
                        $totalOa += (($value->obatalkespasien->hargasatuan_oa * $value->obatalkespasien->qty_oa) - $oa[0]['discount'] + $oa[0]['biayaadministrasi'] + $oa[0]['biayaservice'] + $oa[0]['biayakonseling']);
                }
                $rincianpembayaran['oa'] = $oa;
                $rincianpembayaran['oa']['totalOa'] = $totalOa;
            }
//            UNTUK P3 JML BAYAR = 0
//            if($modTandaBukti->jmlpembayaran == 0 && $modBayar->carabayar_id != 2)
//            { //jika jmlpembayaran nol
//                $modTandaBukti->jmlpembayaran = $rincianpembayaran['tindakan']['totalTindakan'] + $rincianpembayaran['oa']['totalOa'];
//            }

            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('billingKasir.views.kwitansi.viewKwitansi', array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint, 'rincianpembayaran'=>$rincianpembayaran,
                                       'modTandaBukti'=>$modTandaBukti,
                                       'modBayar'=>$modBayar));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('billingKasir.views.kwitansi.viewKwitansi',array('model'=>$model,'pembayarans'=>$pembayarans, 'modPendaftaran'=>$modPendaftaran, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint,'rincianpembayaran'=>$rincianpembayaran,
                                       'modTandaBukti'=>$modTandaBukti,
                                       'modBayar'=>$modBayar));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
//                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $ukuranKertasPDF = 'KW';                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1); 
                /*
                 * cara ambil margin
                 * tinggi_header * 72 / (72/25.4)
                 *  tinggi_header = inchi
                 */
                $header = 0.78 * 72 / (72/25.4);
                $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                $mpdf->WriteHTML(
                    $this->renderPartial(
                        'billingKasir.views.kwitansi.viewKwitansiPdf',
                        array(
                            'model'=>$model,
                            'pembayarans'=>$pembayarans,
                            'modPendaftaran'=>$modPendaftaran,
                            'judulKwitansi'=>$judulKwitansi,
                            'caraPrint'=>$caraPrint,
                            'rincianpembayaran'=>$rincianpembayaran,
                                       'modTandaBukti'=>$modTandaBukti,
                                       'modBayar'=>$modBayar
                        ),true
                    )
                );
                $mpdf->Output();
            }                       
        }
        
        public function terbilang($x, $style=4, $strcomma=",") {
        if ($x < 0) {
            $result = "minus " . trim($this->ctword($x));
        } else {
            $arrnum = explode("$strcomma", $x);
            $arrcount = count($arrnum);
            if ($arrcount == 1) {
                $result = trim($this->ctword($x));
            } else if ($arrcount > 1) {
                $result = trim($this->ctword($arrnum[0])) . " koma " . trim($this->ctword($arrnum[1]));
            }
        }
        switch ($style) {
            case 1: //1=uppercase  dan
                $result = strtoupper($result);
                break;
            case 2: //2= lowercase
                $result = strtolower($result);
                break;
            case 3: //3= uppercase on first letter for each word
                $result = ucwords($result);
                break;
            default: //4= uppercase on first letter
                $result = ucfirst($result);
                break;
        }
        return $result;
    }

    public function ctword($x) {
        $x = abs($x);
        $number = array("", "satu", "dua", "tiga", "empat", "lima",
            "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";

        if ($x < 12) {
            $temp = " " . $number[$x];
        } else if ($x < 20) {
            $temp = $this->ctword($x - 10) . " belas";
        } else if ($x < 100) {
            $temp = $this->ctword($x / 10) . " puluh" . $this->ctword($x % 10);
        } else if ($x < 200) {
            $temp = " seratus" . $this->ctword($x - 100);
        } else if ($x < 1000) {
            $temp = $this->ctword($x / 100) . " ratus" . $this->ctword($x % 100);
        } else if ($x < 2000) {
            $temp = " seribu" . $this->ctword($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->ctword($x / 1000) . " ribu" . $this->ctword($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->ctword($x / 1000000) . " juta" . $this->ctword($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->ctword($x / 1000000000) . " milyar" . $this->ctword(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->ctword($x / 1000000000000) . " trilyun" . $this->ctword(fmod($x, 1000000000000));
        }
        return $temp;
    }

     public function actionViewRincian($id){
            $this->layout ='//layouts/printWindows';
            
            $modReturPenjualan = ReturbayarpelayananT::model()->findByPk($id);
            $modTandaBuktiKeluar = TandabuktikeluarT::model()->findByPk($modReturPenjualan->tandabuktikeluar_id);
            $returresep = ReturresepT::model()->findByAttributes(array('returresep_id'=>$modReturPenjualan->returresep_id));
            
            $judulKwitansi = 'Tanda Bukti Pembayaran Retur Penjualan Obat';
            
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('kwitansiReturPenjualanObat', array('returresep'=>$returresep, 'modReturPenjualan'=>$modReturPenjualan,'modTandaBuktiKeluar'=>$modTandaBuktiKeluar, 'judulKwitansi'=>$judulKwitansi, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
        
    }

    public function actionUpdateDN($tandabuktibayar_id,$darinama_bkm){
            $transaction = Yii::app()->db->beginTransaction();
            try {
                TandabuktibayarT::model()->updateByPk($tandabuktibayar_id, array('darinama_bkm'=>$darinama_bkm));
                $transaction->commit();
            } catch (Exception $exc) {
                $transaction->rollback();
            }
    }
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}