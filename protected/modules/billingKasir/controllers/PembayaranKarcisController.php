<?php

class PembayaranKarcisController extends PembayaranController
{
	public function actionIndex($view=null, $id=null)
	{
            $successSave = false; $tandaBukti = new TandabuktibayarT;
            
            if(isset($_POST['pembayaran'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $tandaBukti = $this->saveTandabuktiBayar($_POST['TandabuktibayarT'],false,null);
                    $this->savePembayaranPelayanan($tandaBukti,$_POST['pembayaran'],$_POST['pembayaranAlkes']);
                    
                    $transaction->commit();
                    Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    $successSave = true;
                    //echo "<pre>".print_r($_POST,1)."</pre>";
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }
            
            if(isset($_GET['frame']) && !empty($_GET['idPendaftaran'])){
                $this->layout = 'frameDialog';
                $idPendaftaran = $_GET['idPendaftaran'];
                $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);
                
                $criteria = new CDbCriteria;
                $criteria->compare('pendaftaran_id', $idPendaftaran);
                //$criteria->addCondition('tindakansudahbayar_id IS NULL');
                $criteria->addCondition('t.karcis_id IS NOT NULL');
                $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);
                
                $modTandaBukti = new TandabuktibayarT;
                $modTandaBukti->darinama_bkm = $modPasien->nama_pasien;
                $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
            } else {
                $modPendaftaran = new BKPendaftaranT;
                $modPasien = new BKPasienM;
                $modTindakan[0] = new BKTindakanPelayananT;
                $modObatalkes[0] = new BKObatalkesPasienT;
                $modTandaBukti = new TandabuktibayarT;
            }
	           
            $this->render('index',array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modTindakan'=>$modTindakan,
                                        'modTandaBukti'=>$modTandaBukti,
                                        'tandaBukti'=>$tandaBukti,
                                        'successSave'=>$successSave));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}