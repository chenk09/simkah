<?php
Yii::import('billingKasir.controllers.PembayaranController');//UNTUK MENGGUNAKAN FUNCTION saveJurnalRekening()
class PembayaranSupplierController extends SBaseController
{
        protected $successSave;

        /**
         * pembayaran ke supplier
         * di gunakan :
         * 1. billingKasir -> informasi Faktur Pembelian -> bayar ke supplier 
         */
        public function actionIndex()
	{
            if(isset($_GET['frame']) && !empty($_GET['idFakturPembelian'])){
                $this->layout = 'frameDialog';
                $modelBayar = new BKBayarkeSupplierT;
                $modBuktiKeluar = new BKTandabuktikeluarT;
                $idFakturPembelian = $_GET['idFakturPembelian'];
                $modFakturBeli = BKFakturPembelianT::model()->findByPk($idFakturPembelian);
                $uangMuka = 0;
                $modUangMuka = new BKUangMukaBeliT();
                $sudahBayar = 0;
                $modRekenings = array();
                
                if ((boolean)count($modFakturBeli)){
                    $modDetailBeli = BKFakturDetailT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFakturPembelian));
                    if (isset($modFakturBeli->penerimaanbarang_id)){
                        $modUangMuka = BKUangMukaBeliT::model()->findByAttributes(array('permintaanpembelian_id'=>$modFakturBeli->penerimaan->permintaanpembelian_id));
                        if ((boolean)count($modUangMuka)){
                            $modelBayar->uangmukabeli_id = $modUangMuka->uangmukabeli_id;
                            $modBuktiKeluar->uangmukabeli_id = $modUangMuka->uangmukabeli_id;
                            $uangMuka = ((isset($modUangMuka->jumlahuang)) ? $modUangMuka->jumlahuang : 0);
                        }else{
                            $modUangMuka = new BKUangMukaBeliT();
                        }
                    }
                    
                    $modBayar = BKBayarkeSupplierT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFakturPembelian));
                    if (count($modBayar) > 0){
                        foreach ($modBayar as $key => $value) {
                            $sudahBayar += $value->jmldibayarkan;
                        }
                    }
                }
                $modelBayar->fakturpembelian_id = $idFakturPembelian;
                $modelBayar->totaltagihan = $modFakturBeli->totalhargabruto - $sudahBayar;
                $modelBayar->jmldibayarkan = $modelBayar->totaltagihan - $uangMuka;
                $modBuktiKeluar->tahun = date('Y');
                $modBuktiKeluar->nokaskeluar = Generator::noKasKeluar();
                $modBuktiKeluar->namapenerima = $modFakturBeli->supplier->supplier_nama;
                $modBuktiKeluar->alamatpenerima = $modFakturBeli->supplier->supplier_alamat;
                $modBuktiKeluar->untukpembayaran = 'Pembayaran Supplier';
                $modBuktiKeluar->biayaadministrasi = $modFakturBeli->biayamaterai;
                $modBuktiKeluar->jmlkaskeluar = $modelBayar->jmldibayarkan+$modBuktiKeluar->biayaadministrasi;
            } else {
                $modFakturBeli = new BKFakturPembelianT;
                $modDetailBeli = new BKFakturDetailT;
                $modUangMuka = new BKUangMukaBeliT;
                $modelBayar = new BKBayarkeSupplierT;
                $modBuktiKeluar = new BKTandabuktikeluarT;
                $modBuktiKeluar->tahun = date('Y');
            }
            
            if(isset($_POST['BKBayarkeSupplierT']) && (!isset($modFakturBeli->bayarkesupplier_id))){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modelBayar = $this->saveBayarSupplier($_POST['BKBayarkeSupplierT'],$modelBayar);
                    $modBuktiKeluar = $this->saveBuktiKeluar($idFakturPembelian,$_POST['BKTandabuktikeluarT'],$modelBayar,$modBuktiKeluar,$_POST['RekeningpembayaransupplierV']);
                    $this->updateBayarSupplier($modelBayar, $modBuktiKeluar);
                    $sisa = ($modelBayar->totaltagihan-$modelBayar->jmldibayarkan-$uangMuka);
                    if ($sisa < 1){
                        $update = FakturpembelianT::model()->updateByPk($idFakturPembelian, array('bayarkesupplier_id'=>$modelBayar->bayarkesupplier_id));
                    }
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
                    } else {
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $transaction->rollback();
                    }
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }
            
            $this->render('index',array('modFakturBeli'=>$modFakturBeli,
                                        'modDetailBeli'=>$modDetailBeli,
                                        'modelBayar'=>$modelBayar,
                                        'modBuktiKeluar'=>$modBuktiKeluar,
                                        'modUangMuka'=>$modUangMuka,
                                        'modRekenings'=>$modRekenings,
                                        ));
	}
        
        /**
         * method untuk save pembayaran ke supplier 
         * digunakan di
         * 1. PembayaranSupplier/index
         * @param array $postBayarSupplier post request $_POST['BKBayarkeSupplierT']
         * @param obj $modBayar BKBayarkeSupplierT
         * @return object BKBayarkeSupplierT
         */
        protected function saveBayarSupplier($postBayarSupplier,$modBayar)
        {
            $modBayar->attributes = $postBayarSupplier;
            $format = new CustomFormat();
            $modBayar->tglbayarkesupplier = $format->formatDateTimeMediumForDB($postBayarSupplier['tglbayarkesupplier']);
            if($modBayar->validate()){
                $modBayar->save();
                $this->successSave = true;
            } else {
                $this->successSave = false;
            }
            return $modBayar;
        }
        
        /**
         * method untuk save tanda bukti keluar ke supplier 
         * digunakan di
         * 1. PembayaranSupplier/index
         * @param array $postBuktiKeluar post request $_POST['BKTandaBuktiKeluarT']
         * @param object $modBayarSupplier BKBayarSupplierT
         * @param object $modBuktiKeluar BKTandaBuktiKeluarT
         * @return object BKTandaBuktiKeluarT
         */
        protected function saveBuktiKeluar($idFakturPembelian, $postBuktiKeluar,$modBayarSupplier, $modBuktiKeluar,$postRekenings = array())
        {
            $format = new CustomFormat();
            $modBuktiKeluar->attributes = $postBuktiKeluar;
            $modBuktiKeluar->bayarkesupplier_id = $modBayarSupplier->bayarkesupplier_id;
            $modBuktiKeluar->tglkaskeluar = $format->formatDateTimeMediumForDB($modBuktiKeluar->tglkaskeluar);
            $modBuktiKeluar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->tahun = date('Y');
            $modBuktiKeluar->shift_id = Yii::app()->user->getState('shift_id');
            $modBuktiKeluar->create_ruangan = Yii::app()->user->getState('ruangan_id');
            $modBuktiKeluar->create_time = date('Y-m-d H:i:s');
            $modBuktiKeluar->create_loginpemakai_id = Yii::app()->user->id;

            if($modBuktiKeluar->validate()) {
                if(isset($postRekenings)){//simpan jurnal rekening
                    $modJurnalRekening = PembayaranController::saveJurnalRekening();
                    //update jurnalrekening_id
                    $modBuktiKeluar->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
//                      ADA DI BAWAH >> $modTandaBukti->save();
                    $saveDetailJurnal = PembayaranSupplierController::saveJurnalDetails($modJurnalRekening, $postRekenings, null);
                    $updateFakturPembelian = $this->updateFakturPembelian($modJurnalRekening,$idFakturPembelian, $modBayarSupplier);
                }
                $modBuktiKeluar->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }

            return $modBuktiKeluar;
        }
        
        protected function updateBayarSupplier($modBayarSupplier,$modBuktiKeluar)
        {
            BKBayarkeSupplierT::model()->updateByPk($modBayarSupplier->bayarkesupplier_id, array('tandabuktikeluar_id'=>$modBuktiKeluar->tandabuktikeluar_id));
        }
        protected function updateFakturPembelian($modJurnalRekening,$idFakturPembelian,$modBayarSupplier=null)
        {
            $update = FakturpembelianT::model()->updateByPk($idFakturPembelian, array('jurnalrekening_id'=>$modJurnalRekening->jurnalrekening_id, 'bayarkesupplier_id'=>$modBayarSupplier->bayarkesupplier_id));
            if($update){
                $valid = true;
            }else{
                $valid = false;
            }
            $this->successSave = $valid;
        }

        /**
         * insert detail dari post rekening digunakan di:
         * - rawatJalan/TindakanController
         * @param type $modJurnalRekening
         * @param type $postRekenings
         * @param type $jenisPelayanan = 'tm' / 'oa' / 'ap'
         * @param type $jenisSimpan
         * @return boolean
         */
        public function saveJurnalDetails($modJurnalRekening, $postRekenings, $jenisSimpan = null){
            $valid = true;
            $modJurnalPosting = null;
            if($jenisSimpan == 'posting')
            {
                $modJurnalPosting = new JurnalpostingT;
                $modJurnalPosting->tgljurnalpost = date('Y-m-d H:i:s');
                $modJurnalPosting->keterangan = "Posting automatis";
                $modJurnalPosting->create_time = date('Y-m-d H:i:s');
                $modJurnalPosting->create_loginpemekai_id = Yii::app()->user->id;
                $modJurnalPosting->create_ruangan = Yii::app()->user->getState('ruangan_id');
                if($modJurnalPosting->validate()){
                    $modJurnalPosting->save();
                }
            }
            
            foreach($postRekenings AS $i => $rekening){
                $model[$i] = new JurnaldetailT();
                $model[$i]->jurnalposting_id = ($modJurnalPosting == null ? null : $modJurnalPosting->jurnalposting_id);
                $model[$i]->rekperiod_id = $modJurnalRekening->rekperiod_id;
                $model[$i]->jurnalrekening_id = $modJurnalRekening->jurnalrekening_id;
                $model[$i]->uraiantransaksi = $rekening['nama_rekening'];
                $model[$i]->saldodebit = $rekening['saldodebit'];
                $model[$i]->saldokredit = $rekening['saldokredit'];
                $model[$i]->nourut = $i+1;
                $model[$i]->rekening1_id = $rekening['struktur_id'];
                $model[$i]->rekening2_id = $rekening['kelompok_id'];
                $model[$i]->rekening3_id = $rekening['jenis_id'];
                $model[$i]->rekening4_id = $rekening['obyek_id'];
                $model[$i]->rekening5_id = $rekening['rincianobyek_id'];
                $model[$i]->catatan = "PEMBAYARAN SUPPLIER";
                if($model[$i]->validate()){
                    $model[$i]->save();
                }else{
//                      KARENA TIDAK DI SEMUA CONTROLLER DI DEKLARASIKAN >>  $this->pesan = $model[$i]->getErrors();
                    $valid = false;
                    break;
                }
            }
            return $valid;        
        }


    public function actionIndexGudangUmum()
    {
            if(isset($_GET['frame']) && !empty($_GET['idFakturPembelian'])){
                $this->layout = 'frameDialog';
                $modelBayar = new BKBayarkeSupplierT;
                $modBuktiKeluar = new BKTandabuktikeluarT;
                $idTerimaPersediaan = $_GET['idFakturPembelian'];
                $fakturId = $_GET['fakturId'];
                $modFakturBeli = TerimapersediaanT::model()->findByPk($idTerimaPersediaan);
                $uangMuka = 0;
                $modUangMuka = new BKUangMukaBeliT();
                $sudahBayar = 0;
                $modRekenings = array();
                
                // if ((boolean)count($modFakturBeli)){
                $modDetailBeli = TerimapersdetailT::model()->findAllByAttributes(array('terimapersediaan_id'=>$idTerimaPersediaan));
                $totalharga = 0;
                $discount = 0;
                $ppn = 0;
                $harga = 0;
                if(count($modDetailBeli)>0){
                    foreach ($modDetailBeli as $key => $value) {
                        $discount = ($value->persediaanbarang->totalharga  * ($value->persediaanbarang->discount/100));
                        $ppn = $value->persediaanbarang->pajakppn;
                        $harga = $value->persediaanbarang->totalharga;
                        $totalharga += ($harga - $discount) + $ppn;
                    }
                }
                //     // if (isset($modFakturBeli->penerimaanbarang_id)){
                //     //     $modUangMuka = BKUangMukaBeliT::model()->findByAttributes(array('permintaanpembelian_id'=>$modFakturBeli->penerimaan->permintaanpembelian_id));
                //     //     if ((boolean)count($modUangMuka)){
                //     //         $modelBayar->uangmukabeli_id = $modUangMuka->uangmukabeli_id;
                //     //         $modBuktiKeluar->uangmukabeli_id = $modUangMuka->uangmukabeli_id;
                //     //         $uangMuka = ((isset($modUangMuka->jumlahuang)) ? $modUangMuka->jumlahuang : 0);
                //     //     }else{
                //     //         $modUangMuka = new BKUangMukaBeliT();
                //     //     }
                //     // }
                    
                //     $modBayar = BKBayarkeSupplierT::model()->findAllByAttributes(array('fakturpembelian_id'=>$idFakturPembelian));
                //     if (count($modBayar) > 0){
                //         foreach ($modBayar as $key => $value) {
                //             $sudahBayar += $value->jmldibayarkan;
                //         }
                //     }
                // }
                $modelBayar->fakturpembelian_id = null;
                $modelBayar->totaltagihan = $totalharga;
                $modelBayar->jmldibayarkan = $modelBayar->totaltagihan;
                $modBuktiKeluar->tahun = date('Y');
                $modBuktiKeluar->nokaskeluar = Generator::noKasKeluar();
                $modBuktiKeluar->namapenerima = $modFakturBeli->pembelianbarang->supplier->supplier_nama;
                $modBuktiKeluar->alamatpenerima = $modFakturBeli->pembelianbarang->supplier->supplier_alamat;
                $modBuktiKeluar->untukpembayaran = 'Pembayaran Supplier';
                $modBuktiKeluar->biayaadministrasi = $modFakturBeli->biayaadministrasi;
                $modBuktiKeluar->jmlkaskeluar = $modelBayar->jmldibayarkan+$modBuktiKeluar->biayaadministrasi;
            } else {
                $modFakturBeli = new BKFakturPembelianT;
                $modDetailBeli = new BKFakturDetailT;
                $modUangMuka = new BKUangMukaBeliT;
                $modelBayar = new BKBayarkeSupplierT;
                $modBuktiKeluar = new BKTandabuktikeluarT;
                $modBuktiKeluar->tahun = date('Y');
            }
            
            if(isset($_POST['BKBayarkeSupplierT']) && (!isset($modFakturBeli->bayarkesupplier_id))){
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modelBayar = $this->saveBayarSupplier($_POST['BKBayarkeSupplierT'],$modelBayar);
                    $modBuktiKeluar = $this->saveBuktiKeluar($fakturId,$_POST['BKTandabuktikeluarT'],$modelBayar,$modBuktiKeluar,$_POST['RekeningpembayaransupplierV']);
                    $this->updateBayarSupplier($modelBayar, $modBuktiKeluar);
                    $sisa = ($modelBayar->totaltagihan-$modelBayar->jmldibayarkan-$uangMuka);
                    if ($sisa < 1){
                        $update = TerimapersediaanT::model()->updateByPk($idTerimaPersediaan, array('bayarkesupplier_id'=>$modelBayar->bayarkesupplier_id));
                    }
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        $this->refresh();
                    } else {
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                        $transaction->rollback();
                    }
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }
            
            $this->render('bayarGU',array('modFakturBeli'=>$modFakturBeli,
                                        'modDetailBeli'=>$modDetailBeli,
                                        'modelBayar'=>$modelBayar,
                                        'modBuktiKeluar'=>$modBuktiKeluar,
                                        'modUangMuka'=>$modUangMuka,
                                        'modRekenings'=>$modRekenings,
                                        ));
    }

}