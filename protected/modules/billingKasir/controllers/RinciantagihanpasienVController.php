
<?php

class RinciantagihanpasienVController extends SBaseController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';

	/**
	 * @return array action filters
	 */
        
//      DIKOMEN KARENA FILTER USER MENGGUNAKAN SRBAC
//	public function filters()
//	{
//		return array(
//			'accessControl', // perform access control for CRUD operations
//		);
//	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
//	public function accessRules()
//	{
//		return array(
//			array('allow',  // allow all users to perform 'index' and 'view' actions
//				'actions'=>array('index','view', 'rincian'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow authenticated user to perform 'create' and 'update' actions
//				'actions'=>array('create','update','print', 'rincianKasirBaruPrint'),
//				'users'=>array('@'),
//			),
//			array('allow', // allow admin user to perform 'admin' and 'delete' actions
//				'actions'=>array('admin','delete','RemoveTemporary'),
//				'users'=>array('@'),
//			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
//		);
//	}

//buat pull tanggal 2 may 2014
	public function actionIndex()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model=new BKRinciantagihanpasienV('search');
                $model->tglAwal = date('Y-m-d H:i:s');
                $model->tglAkhir = date('Y-m-d H:i:s');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BKRinciantagihanpasienV']))
			$model->attributes=$_GET['BKRinciantagihanpasienV'];

		$this->render('billingKasir.views.rinciantagihanpasienV.index',array(
			'model'=>$model,
		));
	}
        /**
         * actionRincian = menampilkan rincian yang belum lunas
         * @param type $id
         */
        public function actionRincian($id){
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $criteria = new CDbCriteria();
            $criteria->addCondition('pendaftaran_id = '.$id);
            $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
            $criteria->order = 'ruangan_id ASC, kategoritindakan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
            $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria);

            $criteria_pp = new CDbCriteria();
            $criteria_pp->compare('pendaftaran_id',$id);
            $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
            $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
            
            $modPenjamins = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));

            $attributes = array(
                            'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                        );

            $criteria2 = new CDbCriteria();
            $criteria2->addCondition('pemakaianuangmuka_id IS NULL AND pembatalanuangmuka_id IS NULL'); 
            $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

            $uang_cicilan = 0;

                        $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);

                        if(count($uang_muka) > 0)
                        {
                            foreach($uang_muka as $val){
                                $uang_cicilan += $val->jumlahuangmuka;
                            }
                        }
            // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
            //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
            // );

            // $uang_cicilan = 0;
            // foreach($uangmuka as $val)
            // {
            //     $uang_cicilan += $val->jumlahuangmuka;
            // }

            $data['uang_cicilan'] = $uang_cicilan;
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//            $this->render('billingKasir.views.rinciantagihanpasienV.rincianNew', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
            $this->render('billingKasir.views.rinciantagihanpasienV.rincianSementara', array('modPenjamins'=>$modPenjamins, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian,'modPasienpulang'=>$modPasienpulang, 'data'=>$data));
        }
		
        public function actionRincianPrint($id, $caraPrint){
	    $format = new CustomFormat();
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $criteria = new CDbCriteria();
            $criteria->addCondition('pendaftaran_id = '.$id);
            $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
            $criteria->order = 'ruangan_id ASC, kategoritindakan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
            $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria);

            $criteria_pp = new CDbCriteria();
            $criteria_pp->compare('pendaftaran_id',$id);
            $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
            $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
            
            $modPenjamins = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));

            $attributes = array(
                            'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                        );

            $criteria2 = new CDbCriteria();
            $criteria2->addCondition('pemakaianuangmuka_id IS NULL AND pembatalanuangmuka_id IS NULL'); 
            $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

            $uang_cicilan = 0;

			$uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);

			if(count($uang_muka) > 0)
			{
				foreach($uang_muka as $val){
					$uang_cicilan += $val->jumlahuangmuka;
				}
			}

            $data['uang_cicilan'] = $uang_cicilan;
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
			
//            $this->render('billingKasir.views.rinciantagihanpasienV.rincianSementara', array('modPenjamins'=>$modPenjamins, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian,'modPasienpulang'=>$modPasienpulang, 'data'=>$data));
			if ($caraPrint == 'EXCEL') 
			{
				$this->layout='//layouts/printExcel';
				$this->render('billingKasir.views.rinciantagihanpasienV.rincianSudahBayar',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));  
			}
			if($caraPrint == 'PDF')
			{ 
				$ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
				$posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
				$mpdf = new MyPDF(
						'',
						$ukuranKertasPDF, //format A4 Or
					11, //Font SIZE
					'', //default font family
					3, //15 margin_left
					3, //15 margin right
					25, //16 margin top
					10, // margin bottom
					0, // 9 margin header
					0, // 9 margin footer
					'P' // L - landscape, P - portrait
					);  
				$mpdf->useOddEven = 2;  
				$stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
				$mpdf->WriteHTML($stylesheet,1);
				/*
				 * cara ambil margin
				 * tinggi_header * 72 / (72/25.4)
				 *  tinggi_header = inchi
				 */
				//$header = 0.75 * 72 / (72/25.4);
				//$mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
				///////
				
				$mpdf->WriteHTML(
//					$this->renderPartial('rincianSudahBayarPdf',
					$this->renderPartial('rincianSementaraPdf', // EHJ-3429
						array(
							'modPendaftaran'=>$modPendaftaran, 
							'modPasienpulang'=>$modPasienpulang, 
							'modRincian'=>$modRincian,
							'modPenjamins'=>$modPenjamins,
							'data'=>$data, 
							'format'=>$format,
						), true
					)
				);
				
				$mpdf->Output();  
			}
			
        }
        /**
         * actionRincian = menampilkan rincian yang sudah lunas /bayar
         * @param type $id
         */
        public function actionRincianSudahBayar($id, $idpembayaran){
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modPendaftaran->pembayaranpelayanan_id = $idpembayaran;
            $criteria = new CDbCriteria();
            $criteria->addCondition('t.pendaftaran_id = '.$id);
            $criteria->addCondition('t.pembayaranpelayanan_id = '.$idpembayaran);
            $criteria->addCondition('t.tindakansudahbayar_id IS NOT NULL'); //sudah lunas
//            NGETES GROUPPING
//            $criteria->join = '
//                LEFT JOIN obatalkespasien_t ON obatalkespasien_t.obatalkespasien_id = t.tindakanpelayanan_id
//                LEFT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = obatalkespasien_t.obatalkes_id
//                LEFT JOIN tindakanpelayanan_t ON tindakanpelayanan_t.tindakanpelayanan_id = t.tindakanpelayanan_id
//                LEFT JOIN pasienmasukpenunjang_t ON pasienmasukpenunjang_t.pasienmasukpenunjang_id = tindakanpelayanan_t.pasienmasukpenunjang_id
//                ';
//            $criteria->order = 't.ruangan_id ASC, obatalkes_m.jenisobatalkes_id DESC, pasienmasukpenunjang_t.no_masukpenunjang ASC, t.tgl_tindakan ASC';
            //join untuk gruping obat berdasarkan jenis obat
            $criteria->join = '
                LEFT JOIN obatalkespasien_t ON obatalkespasien_t.obatalkespasien_id = t.tindakanpelayanan_id
                LEFT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = obatalkespasien_t.obatalkes_id
                ';
            $criteria->order = 't.ruangan_id ASC, obatalkes_m.jenisobatalkes_id DESC, t.tgl_tindakan ASC';
            $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
            
            $criteria_pp = new CDbCriteria();
            $criteria_pp->compare('pendaftaran_id',$id);
            $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
            $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
            $modPenjamins = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));

            $attributes = array(
                                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                            );
                            
                            $criteria2 = new CDbCriteria();
                            $criteria2->addCondition('pemakaianuangmuka_id IS NOT NULL AND pembatalanuangmuka_id IS NULL'); 
                            $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                            $uang_cicilan = 0;

                            $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);
                            
                            if(count($uang_muka) > 0)
                            {
                                foreach($uang_muka as $val){
                                    $uang_cicilan += $val->jumlahuangmuka;
                                }
                            }
                // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                // );

                // $uang_cicilan = 0;
                // foreach($uangmuka as $val)
                // {
                //     $uang_cicilan += $val->jumlahuangmuka;
                // }

            $data['uang_cicilan'] = $uang_cicilan;
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//          BEDA FORMAT >>>  $this->render('billingKasir.views.rinciantagihanpasienV.rincianNew', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
            $this->render('billingKasir.views.rinciantagihanpasienV.rincianSudahBayar', array('modPenjamins'=>$modPenjamins, 'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian,'modPasienpulang'=>$modPasienpulang, 'data'=>$data));
        }

        public function actionRincianHutang($id, $idpembayaran){
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Biaya (Hutang)';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modPembayaran = BKPembayaranpelayananT::model()->findByAttributes(array('pendaftaran_id'=>$modPendaftaran->pendaftaran_id));
            $modPenjamin = PenjaminpasienM::model()->findByAttributes(array('penjamin_id'=>$modPembayaran->penjamin_id));
            $modCarabayar = CarabayarM::model()->findByAttributes(array('carabayar_id'=>$modPembayaran->carabayar_id));
            $modBayarangsuranpelayanan = BayarangsuranpelayananT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$modPembayaran->pembayaranpelayanan_id));
            $modSuratketjaminan = SuratketjaminanT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$modPembayaran->pembayaranpelayanan_id));
            $modTandabuktibayar = TandabuktibayarT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$modPembayaran->pembayaranpelayanan_id));
            $criteria = new CDbCriteria();
            $criteria->addCondition('pendaftaran_id = '.$id);
            $modRincian = BKRinciantagihanpasienberhutangV::model()->findAll($criteria);
            // $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);//<<-untuk sementara
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $this->render('billingKasir.views.rinciantagihanpasienV.rincianHutangNew', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 
                'modSuratketjaminan'=>$modSuratketjaminan, 'modTandabuktibayar'=>$modTandabuktibayar));
        }        

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=BKRinciantagihanpasienV::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='rjrinciantagihanpasien-v-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        /**
         *Mengubah status aktif
         * @param type $id 
         */
        public function actionRemoveTemporary($id)
	{
                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_UPDATE)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
                //SAKabupatenM::model()->updateByPk($id, array('kabupaten_aktif'=>false));
                //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionRincianKasirBaruPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya ';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
                $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';

//                $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
				
				$criteria2 = new CDbCriteria();
				$criteria2->addCondition('pendaftaran_id = '.$id);
				$modRincian = BKRinciantagihanpasienberhutangV::model()->findAll($criteria2);
				
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria_pp = new CDbCriteria();
                $criteria_pp->compare('pendaftaran_id',$id);
                $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
                $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
                $modPenjamins = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));

                $attributes = array(
                                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                            );
                
                $criteria2 = new CDbCriteria();
                $criteria2->addCondition('pemakaianuangmuka_id IS NULL AND pembatalanuangmuka_id IS NULL'); 
                $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                $uang_cicilan = 0;

                            $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);
                            
                            if(count($uang_muka) > 0)
                            {
                                foreach($uang_muka as $val){
                                    $uang_cicilan += $val->jumlahuangmuka;
                                }
                            }
                // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                // );

                // $uang_cicilan = 0;
                // foreach($uangmuka as $val)
                // {
                //     $uang_cicilan += $val->jumlahuangmuka;
                // }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'rincian_tagihan';
				$modTandabuktibayar = TandabuktibayarT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$modPembayaran->pembayaranpelayanan_id));
				if ($caraPrint == 'EXCEL') 
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('billingKasir.views.rinciantagihanpasienV.printRincianPasienBerhutang',array('modPendaftaran'=>$modPendaftaran, 'modPenjamins'=>$modPenjamins, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));  
                } else
                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('c',$ukuranKertasPDF); 
//                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
//                    $header_title = 
//                    '
//                       <div>&nbsp;</div>
//                        <div style="margin-top:58px;font-family:tahoma;font-size: 8pt;">
//                            <div style="margin-left:1px;width:100px;float:left">No. RM / Reg</div>
//                            <div style="float:left">: '. $modPendaftaran->pasien->no_rekam_medik .' / '. $modPendaftaran->no_pendaftaran .'</div>
//                        </div>
//                    '
//                    ;
                    //$mpdf->SetHTMLHeader($header_title);
                    
                    $footer = '
                    <table width="100%" style="vertical-align: top; font-family:tahoma;font-size: 8pt;"><tr>
                    <td width="50%"></td>
                    <td width="50%" align="right">{PAGENO} / {nb}</td>
                    </tr></table>
                    ';
                    $mpdf->SetHTMLFooter($footer);
                    
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);                    
                    $mpdf->AddPage($posisi,'','','','',5,5,$header+4,8,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('printRincianPasienBerhutang',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modPasienpulang'=>$modPasienpulang, 
                                'modRincian'=>$modRincian, 
                                'modPenjamins'=>$modPenjamins, 
                                'data'=>$data, 
                                'format'=>$format,
								'modTandabuktibayar'=>$modTandabuktibayar,
                            ), true
                        )
                    );
                    
                    $mpdf->Output();
                    exit;
                }

            }
        }
        /**
         * actionRincianKasirSudahBayarPrint = cetak rincian yang sudah bayar / lunas
         * @param type $id
         * @param type $caraPrint
         */
        public function actionRincianKasirSudahBayarPrint($id, $caraPrint, $idpembayaran, $rinci = null) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
                $criteria = new CDbCriteria();
                $criteria->addCondition('t.pendaftaran_id = '.$id);
                $criteria->addCondition('t.pembayaranpelayanan_id = '.$idpembayaran);
                $criteria->addCondition('t.tindakansudahbayar_id IS NOT NULL'); //sudah lunas
//                $criteria->order = 'ruangan_id ASC, tgl_tindakan ASC, daftartindakan_kode DESC';
                //join untuk gruping obat berdasarkan jenis obat
                $criteria->join = '
                    LEFT JOIN obatalkespasien_t ON obatalkespasien_t.obatalkespasien_id = t.tindakanpelayanan_id
                    LEFT JOIN obatalkes_m ON obatalkes_m.obatalkes_id = obatalkespasien_t.obatalkes_id
                    ';
                $criteria->order = 't.ruangan_id ASC, obatalkes_m.jenisobatalkes_id DESC, t.tgl_tindakan ASC';
                $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria_pp = new CDbCriteria();
                $criteria_pp->compare('pendaftaran_id',$id);
                $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
                $modPasienpulang = PasienpulangT::model()->find($criteria_pp);
                $modPenjamins = BKPiutangasuransiT::model()->findAllByAttributes(array('pendaftaran_id'=>$id));


                $attributes = array(
                                'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
                            );
                            
                            $criteria2 = new CDbCriteria();
                            $criteria2->addCondition('pemakaianuangmuka_id IS NOT NULL AND pembatalanuangmuka_id IS NULL'); 
                            $criteria2->select='SUM(jumlahuangmuka) as jumlahuangmuka';

                            $uang_cicilan = 0;

                            $uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes,$criteria2);
                            
                            if(count($uang_muka) > 0)
                            {
                                foreach($uang_muka as $val){
                                    $uang_cicilan += $val->jumlahuangmuka;
                                }
                            }
                // $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                //     array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                // );

                // $uang_cicilan = 0;
                // foreach($uangmuka as $val)
                // {
                //     $uang_cicilan += $val->jumlahuangmuka;
                // }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'rincian_tagihan';

                if ($caraPrint == 'EXCEL') 
                {
                    $this->layout='//layouts/printExcel';
                    $this->render('billingKasir.views.rinciantagihanpasienV.rincianSudahBayar',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));  
                } 
                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF(
                            '',
                            $ukuranKertasPDF, //format A4 Or
                        11, //Font SIZE
                        '', //default font family
                        3, //15 margin_left
                        3, //15 margin right
                        25, //16 margin top
                        10, // margin bottom
                        0, // 9 margin header
                        0, // 9 margin footer
                        'P' // L - landscape, P - portrait
                        );  
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    //$header = 0.75 * 72 / (72/25.4);
                    //$mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    ///////
                    if($rinci == true){
                        $mpdf->WriteHTML(
                            $this->renderPartial('rincianSementaraPdf',
                                array(
                                    'modPendaftaran'=>$modPendaftaran, 
                                    'modPasienpulang'=>$modPasienpulang, 
                                    'modRincian'=>$modRincian, 
                                    'modPenjamins'=>$modPenjamins, 
                                    'data'=>$data, 
                                    'format'=>$format,
                                ), true
                            )
                        );
                    }else{
                        $mpdf->WriteHTML(
                            $this->renderPartial('rincianSudahBayarPdf',
                                array(
                                    'modPendaftaran'=>$modPendaftaran, 
                                    'modPasienpulang'=>$modPasienpulang, 
                                    'modRincian'=>$modRincian,
                                    'modPenjamins'=>$modPenjamins,
                                    'data'=>$data, 
                                    'format'=>$format,
                                ), true
                            )
                        );
                    }
                    $mpdf->Output();  
                }

            }
        }

        public function actionRincianKasirBerhutangPrint($id, $caraPrint) {
            if (!empty($id))
            {
                $format = new CustomFormat();
                $this->layout = '//layouts/frameDialog';
                $data['judulPrint'] = 'Rincian Biaya (Sudah Bayar / Lunas)';
                $criteria = new CDbCriteria();
                $criteria->addCondition('pendaftaran_id = '.$id);
                $criteria->addCondition('tindakansudahbayar_id IS NOT NULL'); //sudah lunas
                $criteria->order = 'ruangan_id';
                // $modRincian = BKRinciantagihanpasienV::model()->findAll($criteria); 
                $modRincian = BKRinciantagihanpasiensudahbayarV::model()->findAll($criteria);
                $modPendaftaran = PendaftaranT::model()->findByPk($id);
                $criteria_pp = new CDbCriteria();
                $criteria_pp->compare('pendaftaran_id',$id);
                $criteria_pp->compare('pasienadmisi_id',$modPendaftaran->pasienadmisi_id);
                $modPasienpulang = PasienpulangT::model()->find($criteria_pp);

                $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                    array('pendaftaran_id'=>$model->pembayaran->pendaftaran_id)
                );

                $uang_cicilan = 0;
                foreach($uangmuka as $val)
                {
                    $uang_cicilan += $val->jumlahuangmuka;
                }

                $data['uang_cicilan'] = $uang_cicilan;
                $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
                $data['jenis_cetakan'] = 'rincian_tagihan';

                if($caraPrint == 'PDF')
                {
                    $ukuranKertasPDF = 'RBK';                  //Ukuran Kertas Pdf
                    $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                    $mpdf = new MyPDF('',$ukuranKertasPDF); 
                    $mpdf->useOddEven = 2;  
                    $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                    $mpdf->WriteHTML($stylesheet,1);
                    /*
                     * cara ambil margin
                     * tinggi_header * 72 / (72/25.4)
                     *  tinggi_header = inchi
                     */
                    $header = 0.75 * 72 / (72/25.4);
                    $mpdf->AddPage($posisi,'','','','',3,8,$header,5,0,0);
                    $mpdf->WriteHTML(
                        $this->renderPartial('rincianSementaraPdf',
                            array(
                                'modPendaftaran'=>$modPendaftaran, 
                                'modRincian'=>$modRincian, 
                                'data'=>$data, 
                                'format'=>$format,
                            ), true
                        )
                    );
                    $mpdf->Output();  
                }

            }
        }        
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modRincian = BKRinciantagihanpasienV::model()->findAllByAttributes(
                array(
                    'pendaftaran_id' => $id
                ),
                array('order'=>'ruangan_id')
            );
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $caraPrint=$_REQUEST['caraPrint'];
            
            $uangmuka = BayaruangmukaT::model()->findAllByAttributes(
                array('pendaftaran_id'=>$id)
            );
            $uang_cicilan = 0;
            foreach($uangmuka as $val)
            {
                $uang_cicilan += $val->jumlahuangmuka;
            }
            $data['uang_cicilan'] = $uang_cicilan;
            $data['jenis_cetakan'] = 'rincian_tagihan';
            
            //$tindakan_pelayanan = BKTindakanPelayananT::model()->findByPk();
            
            $judulPrint = "RINCIAN BIAYA";
            if($caraPrint=='PRINT')
            {
                $this->layout='//layouts/printWindows';
                $this->render('application.views.print.kwitansiPembayaranRincianBaru',
                    array(
                        'modPendaftaran'=>$modPendaftaran, 
                        'modRincian'=>$modRincian, 
                        'data'=>$data, 
                        'judulPrint'=>$judulPrint
                    )
                );
//                $this->render('billingKasir.views.rinciantagihanpasienV.rincianNew', array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL')
            {
                $this->layout='//layouts/printExcel';
                $this->render('billingKasir.views.rinciantagihanpasienV.rincianSementara',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
            }else if($_REQUEST['caraPrint']=='PDF')
            {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('billingKasir.views.rinciantagihanpasienV.rincianNew',array('modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }
        }
}
