<?php

class PembayaranLabController extends SBaseController
{
    
        private $_valid;
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
        public $defaultAction = 'index';
        protected $pathView = 'billingKasir.views.pembayaran.';
        
        /**
         * property untuk membedakan action bayar tagihan dengan bayar resep pasien
         * @var string 
         */
        protected $actionBayarTagihanPasien = 'index';
        
        /**
         * method untuk pembayaran rincian tagihan pasien
         * digunakan di :
         * 1. Billing Kasir -> transaksi -> bayar tagihan pasien
         * 2. PembayaranController/bayarResep 
         */
	public function actionIndex($view=null, $id=null)
	{        
            if (Yii::app()->user->isGuest){
                $this->redirect(Yii::app()->createUrl('site/login'));
            }
            $successSave = false; 
            $tandaBukti = new TandabuktibayarT;
            $cekSudahBayar = null;
            $modBayar = new PembayaranpelayananT();
            if(isset($_GET['frame']) && !empty($_GET['idPendaftaran'])){
                $this->layout = 'frameDialog';
                $idPendaftaran = $_GET['idPendaftaran'];
                $modPendaftaran = BKPendaftaranT::model()->findByPk($idPendaftaran);
                $modPasien = BKPasienM::model()->findByPk($modPendaftaran->pasien_id);
                
                $criteria = new CDbCriteria;
                $criteria->compare('pendaftaran_id', $idPendaftaran);
                $criteria->addCondition('tindakansudahbayar_id IS NULL');
                $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);
                
                $criteriaoa = new CDbCriteria;
                $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
                $criteriaoa->addCondition('returresepdet_id is null');
                $criteriaoa->addCondition('oasudahbayar_id IS NULL');
                $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);
                
                if($tandaBukti->tandabuktibayar_id != null){
                    $modTandaBukti = $tandaBukti;
                } else {
                    $modTandaBukti = new TandabuktibayarT;
                    $modTandaBukti->darinama_bkm = $modPasien->no_rekam_medik . '-' . $modPendaftaran->no_pendaftaran . '-' . $modPasien->nama_pasien;
                    $modTandaBukti->alamat_bkm = $modPasien->alamat_pasien;
                }
                
                $cekSudahBayar = PembayaranpelayananT::model()->findAllByAttributes(array('pendaftaran_id'=>$idPendaftaran));
                
                if (isset($id)){
                    $modBayar = PembayaranpelayananT::model()->findByPk($id);
                    if ((boolean)count($modBayar)){
                        $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
                        $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                        if ((boolean)count($modPendaftaran))
                            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                        $modTindakanSudahBayar = TindakansudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        $modTindakan = array();
                        if (count($modTindakanSudahBayar) > 0){
                            foreach ($modTindakanSudahBayar as $key => $value) {
                                $modTindakan[$key] = TindakanpelayananT::model()->findByPk($value->tindakanpelayanan_id);
                            }
                        }
                        $modOaSudahBayar = OasudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        $modObatalkes = array();
                        if (count($modOaSudahBayar) > 0){
                            foreach ($modOaSudahBayar as $key => $value) {
                                $modObatalkes[$key] = ObatalkespasienT::model()->findByPk($value->obatalkespasien_id);
                            }
                        }
                    }
                }
            } else {
                if (isset($id)){
                    $modBayar = PembayaranpelayananT::model()->findByPk($id);
                    if ((boolean)count($modBayar)){
                        $modTandaBukti = TandabuktibayarT::model()->findByPk($modBayar->tandabuktibayar_id);
                        $modPendaftaran = PendaftaranT::model()->findByPk($modBayar->pendaftaran_id);
                        if ((boolean)count($modPendaftaran))
                            $modPasien = PasienM::model()->findByPk($modPendaftaran->pasien_id);
                        $modTindakanSudahBayar = TindakansudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        if (count($modTindakanSudahBayar) > 0){
                            $modTindakan = array();
                            foreach ($modTindakanSudahBayar as $key => $value) {
                                $modTindakan[$key] = TindakanpelayananT::model()->findByPk($value->tindakanpelayanan_id);
                            }
                        }
                        $modOaSudahBayar = OasudahbayarT::model()->findAll('pembayaranpelayanan_id =:pembayaranpelayanan', array(':pembayaranpelayanan'=>$modBayar->pembayaranpelayanan_id));
                        if (count($modOaSudahBayar) > 0){
                            $modObatalkes = array();
                            foreach ($modOaSudahBayar as $key => $value) {
                                $modObatalkes[$key] = ObatalkespasienT::model()->findByPk($value->obatalkespasien_id);
                            }
                        }
                    }
                }else{
                    $modPendaftaran = new BKPendaftaranT;
                    $modPasien = new BKPasienM;
                    $modTindakan[0] = new BKTindakanPelayananT;
                    $modObatalkes[0] = new BKObatalkesPasienT;
                    if($tandaBukti->tandabuktibayar_id != null){
                        $modTandaBukti = $tandaBukti;
                    } else {
                        $modTandaBukti = new TandabuktibayarT;
                    }
                }
            }
	           
            if(count($cekSudahBayar)>0){
                Yii::app()->user->setFlash('info',"Sudah melakukan pembayaran");
            }
            /*
            if(count($modTindakan) > 0 || count($modObatalkes) > 0)
            {
                PendaftaranT::model()->updateByPk($modPendaftaran->pendaftaran_id, 
                    array('pembayaranpelayanan_id'=>null)
                );
            }  
            */
            if(isset($_POST['pembayaran']) || isset($_POST['pembayaranAlkes'])) {
                $transaction = Yii::app()->db->beginTransaction();
                try {
                   
                    $tindakan = ((isset($_POST['pembayaran'])) ? $_POST['pembayaran'] : null);
                    $alkes = ((isset($_POST['pembayaranAlkes'])) ? $_POST['pembayaranAlkes'] : null);
                    
                    $tandaBukti = $this->saveTandabuktiBayar($_POST['TandabuktibayarT']);
                    $modBayar = $this->savePembayaranPelayanan($tandaBukti,$tindakan,$alkes);
                    
                    if ($tandaBukti && $modBayar){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                        if (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])){
                            $this->redirect(array(((isset($view)) ? $view : 'index'),'id'=>$modBayar->pembayaranpelayanan_id, 'frame'=>$_GET['frame'], 'idPendaftaran'=>$_GET['idPendaftaran']));
                        }else{
                            $this->redirect(array(((isset($view)) ? $view : 'index'),'id'=>$modBayar->pembayaranpelayanan_id));
                        }
                        $successSave = true;
                    }else{
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                    $transaction->rollback();
                }
            }

            $this->render($this->pathView.((isset($view)) ? $view : 'index'),array('modPendaftaran'=>$modPendaftaran,
                                        'modPasien'=>$modPasien,
                                        'modTindakan'=>$modTindakan,
                                        'modObatalkes'=>$modObatalkes,
                                        'modBayar'=>$modBayar,
                                        'modTandaBukti'=>$modTandaBukti,
                                        'tandaBukti'=>$tandaBukti,
                                        'successSave'=>$successSave,
                                        'cekSudahBayar'=>$cekSudahBayar));
	}
        
        /**
         * method untuk pembayaran rincian tagihan penjualan resep
         * digunakan di :
         * 1. BillingKasir -> Transaksi -> pembayaran Resep Pasien
         */
        public function actionBayarResep($id=null){
            $view = 'bayarResep';
            $this->actionIndex($view,$id);
        }
        
        public function actionProsesBayar()
        {
            $this->layout = 'frameDialog';
            
        }
        
        /**
         * method untuk menyimpan pembayaran pelayanan 
         * digunakan di 
         * 1. PembayaranController/index
         * @param object $tandaBukti BKTandabuktibayarT
         * @param array $postPembayaran post request untuk tindakan pelayanan $_POST['pembayaran']
         * @param array $postPembayaranOa post request untuk obata alkes $_POST['pembayaranAlkes']
         * @throws Exception
         */
        protected function savePembayaranPelayanan($tandaBukti,$postPembayaran,$postPembayaranOa)
        {
                $totalbayartindakan = ((isset($_POST['totalbayartindakan'])) ? $_POST['totalbayartindakan'] : 0);
                $totalbayar_oa = ((isset($_POST['totalbayar_oa'])) ? $_POST['totalbayar_oa'] : 0);
                $totalbiayatindakan = ((isset($_POST['totalbiayatindakan'])) ? $_POST['totalbiayatindakan'] : 0);
                $totalsubsidiasuransi = ((isset($_POST['totalsubsidiasuransi'])) ? $_POST['totalsubsidiasuransi'] : 0);
                $totalsubsidipemerintah = ((isset($_POST['totalsubsidipemerintah'])) ? $_POST['totalsubsidipemerintah'] : 0);
                $totalsubsidirs = ((isset($_POST['totalsubsidirs'])) ? $_POST['totalsubsidirs'] : 0);
                $totaliurbiaya = ((isset($_POST['totaliurbiaya'])) ? $_POST['totaliurbiaya'] : 0);
                $totalpembebasan = ((isset($_POST['totalpembebasan'])) ? $_POST['totalpembebasan'] : 0);
                $idPendaftaran = $_POST['FAPendaftaranT']['pendaftaran_id'];
                $idPasien = $_POST['FAPendaftaranT']['pasien_id'];
                $pembayaran = new PembayaranpelayananT;
                $pembayaran->carabayar_id = (!empty($postPembayaran[0]['carabayar_id'])) ? $postPembayaran[0]['carabayar_id'] : 1;
                $pembayaran->penjamin_id = (!empty($postPembayaran[0]['penjamin_id'])) ? $postPembayaran[0]['penjamin_id'] : 117;
                $pembayaran->pasien_id = $idPasien;
                $pembayaran->pendaftaran_id = $idPendaftaran;
                $pembayaran->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $pembayaran->tglpembayaran = $_POST['TandabuktibayarT']['tglbuktibayar'];
                $pembayaran->ruanganpelakhir_id = Generator::getRuanganPelayananAkhir($idPendaftaran);
                $pembayaran->totalbiayaoa = $totalbayar_oa;
                $pembayaran->totalbiayatindakan = $totalbayartindakan;
                
                $pembayaran->totalbiayapelayanan = $totalbayar_oa + $totalbayartindakan;
                $pembayaran->totalsubsidiasuransi = $totalsubsidiasuransi  + $_POST['totalsubsidiasuransi_oa'];
                $pembayaran->totalsubsidipemerintah = $totalsubsidipemerintah + $_POST['totalsubsidipemerintah_oa'];
                $pembayaran->totalsubsidirs = $totalsubsidirs + $_POST['totalsubsidirs_oa'];
                $pembayaran->totaliurbiaya = $totaliurbiaya + $_POST['totaliurbiaya_oa'];
                
                $pembayaran->totalbayartindakan = $tandaBukti->jmlpembayaran;
                $discount = (!empty($_POST['discount'])) ? $_POST['discount'] : 0;
                $pembayaran->totaldiscount = $discount;
                //$pembayaran->totaldiscount = $_POST['totaldiscount_tindakan'];
                $pembayaran->totalpembebasan = $totalpembebasan;
                $pembayaran->totalsisatagihan = 0;
                if($tandaBukti->carapembayaran == 'HUTANG'){
                    $pembayaran->totalsisatagihan = $tandaBukti->jmlpembayaran;
                }
                if($tandaBukti->carapembayaran == 'CICILAN'){
                    $pembayaran->totalsisatagihan = ($totalbayar_oa+$totalbayartindakan) - $tandaBukti->uangditerima + $tandaBukti->biayaadministrasi+$tandaBukti->biayamaterai-$discount;
                }
                $pembayaran->statusbayar = $this->cekStatusBayar($pembayaran->totalsisatagihan);
                $pembayaran->nopembayaran = Generator::noPembayaran();
                $pembayaran->tandabuktibayar_id = $tandaBukti->tandabuktibayar_id;
                
                //$pembayaran->pembayaranpelayanan_id = $pembayaran->getOldPrimaryKey() + 1;
                if($pembayaran->validate()){
                    //echo "savePembayaranPelayanan valid <br/>";
                    //echo "<pre>".print_r($pembayaran->attributes,1)."</pre>";
                    if ($pembayaran->save()) {
                        TandabuktibayarT::model()->updateByPk($tandaBukti->tandabuktibayar_id, array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id));
                        if(!empty($postPembayaran))
                            $this->saveTindakanSudahBayar($pembayaran, $postPembayaran,$tandaBukti);
                        if(!empty($postPembayaranOa))
                            $this->saveOaSudahBayar($pembayaran, $postPembayaranOa,$tandaBukti);
                        if($tandaBukti->carapembayaran == 'CICILAN')
                            $this->saveBayarAngsuran($pembayaran,$tandaBukti);
                        if($tandaBukti->carapembayaran == 'HUTANG')
                            $this->saveBayarAngsuran($pembayaran,$tandaBukti);
                        if ($this->cekTindakandanObatDibayar($idPendaftaran))
                            $this->updatePendaftaran($idPendaftaran, $pembayaran);
                    }
                    
                } else {
//                    echo "savePembayaranPelayanan tidak valid";
//                    echo "<pre>".print_r($pembayaran->errors,1)."</pre>";
//                    echo "<pre>".print_r($pembayaran->attributes,1)."</pre>";
                    throw new Exception("pembayaran tidak valid <pre>".print_r($pembayaran->errors,1)."</pre>");
                }
                
                return $pembayaran;
        }
        
        /**
         * method untuk cek apakah ada tindakan atau obatalkes yang belum dibayar
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param integer $idPendaftaran pendaftaran_id
         * @return boolean
         */
        protected function cekTindakandanObatDibayar($idPendaftaran){
            $criteria = new CDbCriteria;
            $criteria->compare('pendaftaran_id', $idPendaftaran);
            $criteria->addCondition('tindakansudahbayar_id IS NULL');
            $modTindakan = BKTindakanPelayananT::model()->with('daftartindakan','tipepaket')->findAll($criteria);

            $criteriaoa = new CDbCriteria;
            $criteriaoa->compare('pendaftaran_id', $idPendaftaran);
            $criteriaoa->addCondition('oasudahbayar_id IS NULL');
            $modObatalkes = BKObatalkesPasienT::model()->with('daftartindakan')->findAll($criteriaoa);
            $result = false;
            if (empty($modTindakan) && empty($modObatalkes)){
                $result = true;
            }
            return $result;
        }
        
        protected function cekStatusBayar($sisaTagihan)
        {
            if($sisaTagihan>0){
                return 'BELUM LUNAS';
            } else {
                return 'LUNAS';
            }
        }

        /**
         * method untuk update tindakanpelayanan_t
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modTindSudahBayar TindakansudahbayarT 
         */
        protected function updateTindakanPelayananT($modTindSudahBayar)
        {
            $persenDiscount = $_POST['disc'];
            $tindakanpelayanan = TindakanpelayananT::model()->findByPk($modTindSudahBayar->tindakanpelayanan_id);
            $discount = 0;
            if ((boolean)count($tindakanpelayanan))
                $discount = floor($persenDiscount / 100 * $tindakanpelayanan->tarif_tindakan);
                        
            TindakanpelayananT::model()->updateByPk($modTindSudahBayar->tindakanpelayanan_id, array(
//                                                                                                    'subsidiasuransi_tindakan'=>$modTindSudahBayar->jmlsubsidi_asuransi,
//                                                                                                    'subsidipemerintah_tindakan'=>$modTindSudahBayar->jmlsubsidi_pemerintah,
                                                                                                    'subsisidirumahsakit_tindakan'=>$modTindSudahBayar->jmlsubsidi_rs,
//                                                                                                    'iurbiaya_tindakan'=>$modTindSudahBayar->jmliurbiaya,
                                                                                                    'tindakansudahbayar_id'=>$modTindSudahBayar->tindakansudahbayar_id,
//                                                                                                    'discount_tindakan'=>$discount
                                                                                                     ));
             
        }

        /**
         * method untuk update obatalkespasien_t
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param type $modOa ObatalkespasienT
         */
        protected function updateObatAlkesPasienT($modOa)
        {
            $persenDiscount = $_POST['disc'];
            $obatOa = ObatalkespasienT::model()->findByPk($modOa->obatalkespasien_id);
            $discount = floor($persenDiscount / 100 * $obatOa->hargajual_oa);
                        
            ObatalkespasienT::model()->updateByPk($modOa->obatalkespasien_id, array(
//                                                                                    'subsidiasuransi'=>$modOa->jmlsubsidi_asuransi,
//                                                                                    'subsidipemerintah'=>$modOa->jmlsubsidi_pemerintah,
//                                                                                    'subsidirs'=>$modOa->jmlsubsidi_rs,
//                                                                                    'iurbiaya'=>$modOa->jmliurbiaya,
                                                                                    'oasudahbayar_id'=>$modOa->oasudahbayar_id,
//                                                                                    'discount' => $discount
                                                                                    ));
        }
        
        /**
         * Update Pendaftaran untuk pasien yang sudah bayar lunas 
         * digunakan di :
         * 1. PembayaranController/savePembayaranPelayanan
         * @param integer $idPendaftaran pendaftaran_id
         * @param object $pembayaran PembayaranpelayananT
         */
        protected function updatePendaftaran($idPendaftaran,$pembayaran)
        {
            PendaftaranT::model()->updateByPk($idPendaftaran,
                array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id)
            );
            $criteria = new CDbCriteria;
            $criteria->compare('pendaftaran_id',$idPendaftaran);
            $criteria->addCondition('pembayaranpelayanan_id IS NULL');
            $modAdmisi = PasienadmisiT::model()->findAll($criteria);
            foreach($modAdmisi as $i => $admisi) {
                PasienadmisiT::model()->updateByPk($admisi->pasienadmisi_id, array('pembayaranpelayanan_id'=>$pembayaran->pembayaranpelayanan_id));
            }
        }

        /**
         * method untuk save tindakanpelayanan yang sudah dibayar TindakansudahbayarT
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modPembayaranPelayanan PembayaranpelayananT
         * @param array $postPembayaran post request untuk TindakanpelayananT $_POST['pembayaran']
         * @param array $modTandaBukti TandabuktibayarT
         */
        protected function saveTindakanSudahBayar($modPembayaranPelayanan,$postPembayaran,$modTandaBukti)
        {
            if (count($postPembayaran) > 0){
                foreach($postPembayaran as $i=>$item){
                    if ($item['tindakanpelayanan_id'] != 0){
                        $model[$i] = new TindakansudahbayarT;
                        $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');;
                        $model[$i]->tindakanpelayanan_id = $item['tindakanpelayanan_id'];
                        $model[$i]->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
                        $model[$i]->daftartindakan_id = $item['daftartindakan_id'];
                        $model[$i]->qty_tindakan = $item['qty_tindakan'];
                        $model[$i]->jmlbiaya_tindakan = $item['tarif_satuan'];
                        $model[$i]->jmlsubsidi_asuransi = $item['subsidiasuransi_tindakan'];
                        $model[$i]->jmlsubsidi_pemerintah = $item['subsidipemerintah_tindakan'];
                        $model[$i]->jmlsubsidi_rs = $item['subsisidirumahsakit_tindakan'];
                        $model[$i]->jmliurbiaya = $item['iurbiaya_tindakan'];
                        $model[$i]->jmlpembebasan = $item['pembebasan_tarif'];
                        //$model[$i]->jmlbayar_tindakan = $item['sub_total'];
                        $model[$i]->jmlbayar_tindakan = $item['sub_total'] / ($_POST['totalbayartindakan'] + $_POST['totalbayar_oa']) * $modTandaBukti->jmlpembayaran;
                        $sisaBayar = $item['sub_total'] - $model[$i]->jmlbayar_tindakan;
                        $model[$i]->jmlsisabayar_tindakan = ($sisaBayar > 0) ? $sisaBayar : 0;

                        //$model[$i]->tindakansudahbayar_id = $model[$i]->getOldPrimaryKey() + 1;
                        if($model[$i]->validate()){
                            //echo "saveTindakanSudahBayar valid <br/>";
                            //echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                            $model[$i]->save();
                            $this->updateTindakanPelayananT($model[$i]);
                        } else {
                            echo "saveTindakanSudahBayar tidak valid";
                            echo "<pre>".print_r($model[$i]->errors,1)."</pre>";
                            echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                        }
                    }
                }
            }
        }

        /**
         * method untuk save obatalkespasien yang sudah dibayar OasudahbayarT
         * digunakan di:
         * 1. PembayaranController/savePembayaranPelayanan
         * @param object $modPembayaranPelayanan PembayaranpelayananT
         * @param array $postPembayaranOa post request untuk ObatalkespasienT $_POST['pembayaranAlkes']
         * @param array $modTandaBukti TandabuktibayarT
         */
        protected function saveOaSudahBayar($modPembayaranPelayanan,$postPembayaranOa,$modTandaBukti)
        {
            if (count($postPembayaranOa) > 0){
                foreach($postPembayaranOa as $i=>$item){
                    if ($item['obatalkespasien_id'] != 0){
                        $totalbayartindakan = ((isset($_POST['totalbayartindakan'])) ? $_POST['totalbayartindakan'] : 0);
                        $model[$i] = new OasudahbayarT;
                        $model[$i]->ruangan_id = Yii::app()->user->getState('ruangan_id');
                        $model[$i]->obatalkespasien_id = $item['obatalkespasien_id'];
                        $model[$i]->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
                        $model[$i]->obatalkes_id = $item['obatalkes_id'];
                        $model[$i]->qty_oa = $item['qty_oa'];
                        $model[$i]->jmlsubsidi_asuransi = $item['subsidiasuransi'];
                        $model[$i]->jmlsubsidi_pemerintah = $item['subsidipemerintah'];
                        $model[$i]->jmlsubsidi_rs = $item['subsidirs'];
                        $model[$i]->jmliurbiaya = $item['iurbiaya'];
                        //$model[$i]->jmlbayar_oa = $item['sub_total'];
                        $model[$i]->jmlbayar_oa = $item['sub_total'] / ($totalbayartindakan + $_POST['totalbayar_oa']) * $modTandaBukti->jmlpembayaran;
                        $sisaBayar = $item['sub_total'] - $model[$i]->jmlbayar_oa;
                        $model[$i]->jmlsisabayar_oa = ($sisaBayar > 0) ? $sisaBayar : 0;
                        $model[$i]->hargasatuan = $item['hargasatuan'];

                        //$model[$i]->oasudahbayar_id = $model[$i]->getOldPrimaryKey() + 1;
                        if($model[$i]->validate()){
                            //echo "saveOaSudahBayar valid <br/>";
                            //echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                            $model[$i]->save();
                            $this->updateObatAlkesPasienT($model[$i]);
                        } else {
                            echo "saveOaSudahBayar tidak valid";
                            echo "<pre>".print_r($model[$i]->errors,1)."</pre>";
                            echo "<pre>".print_r($model[$i]->attributes,1)."</pre>";
                        }
                    }
                }
            }
        }
        
        /**
         * method untuk menyimpan/update tandabukti bayar ke table TandabuktibayarT
         * digunakan di :
         * 1. PembayaranController/index
         * @param array $postTandaBuktiBayar post request untuk TandabuktibayarT
         * @return \TandabuktibayarT
         */
        protected function saveTandabuktiBayar($postTandaBuktiBayar, $update=false,$tandaBukti=null)
        {
            $modTandaBukti = null;
            if ($update && isset($tandaBukti)){
                $modTandaBukti = $tandaBukti;
                $modTandaBukti->jmlpembulatan += $postTandaBuktiBayar['jmlpembulatan'];
                $modTandaBukti->biayaadministrasi += $postTandaBuktiBayar['biayaadministrasi'];
                $modTandaBukti->jmlpembayaran += $postTandaBuktiBayar['jmlpembayaran'];
                $modTandaBukti->biayamaterai += $postTandaBuktiBayar['biayamaterai'];
                $modTandaBukti->uangditerima += $postTandaBuktiBayar['uangditerima'];
                $modTandaBukti->uangkembalian += $postTandaBuktiBayar['uangkembalian'];
                $modTandaBukti->tglbuktibayar = $postTandaBuktiBayar['tglbuktibayar'];
                $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                
                if (!$modTandaBukti->save()){
                    echo "saveTandabuktiBayar tidak valid";
                    echo "<pre>".print_r($modTandaBukti->errors,1)."</pre>";
                    echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                }
            }else{
                $modTandaBukti = new TandabuktibayarT;
                $modTandaBukti->attributes = $postTandaBuktiBayar;
                if($modTandaBukti->carapembayaran == 'HUTANG'){
                    $modTandaBukti->uangditerima = 0;
                }
                $modTandaBukti->ruangan_id = Yii::app()->user->getState('ruangan_id');
                $modTandaBukti->nourutkasir = Generator::noUrutKasir($modTandaBukti->ruangan_id);
                $modTandaBukti->nobuktibayar = Generator::noBuktiBayar();

                //$modTandaBukti->tandabuktibayar_id = $modTandaBukti->getOldPrimaryKey() + 1;
                if($modTandaBukti->validate()){
                    //echo "saveTandabuktiBayar valid <br/>";
                    //echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                    $modTandaBukti->save();
                } else {
                    echo "saveTandabuktiBayar tidak valid";
                    echo "<pre>".print_r($modTandaBukti->errors,1)."</pre>";
                    echo "<pre>".print_r($modTandaBukti->attributes,1)."</pre>";
                }
            }
            
            return $modTandaBukti;
        }
        
        protected function saveBayarAngsuran($modPembayaranPelayanan,$modTandaBukti)
        {
            $modelAngsuran = new BayarangsuranpelayananT;
            $modelAngsuran->tandabuktibayar_id = $modPembayaranPelayanan->tandabuktibayar_id;
            $modelAngsuran->pembayaranpelayanan_id = $modPembayaranPelayanan->pembayaranpelayanan_id;
            $modelAngsuran->tglbayarangsuran = $modPembayaranPelayanan->tglpembayaran;
            $modelAngsuran->bayarke = 1;
            //$modelAngsuran->jmlbayarangsuran = $modPembayaranPelayanan->totalbayartindakan;
            $modelAngsuran->jmlbayarangsuran = $modTandaBukti->uangditerima;
            $modelAngsuran->sisaangsuran = $modPembayaranPelayanan->totalsisatagihan;
            
            //$modelAngsuran->bayarangsuranpelayanan_id = $modelAngsuran->getOldPrimaryKey() + 1;
            if($modelAngsuran->validate()){
                //echo "angsuran valid <br/>";
                //echo "<pre>".print_r($modelAngsuran->attributes,1)."</pre>";
                $modelAngsuran->save();
            } else {
                echo "angsuran tidak valid";
                echo "<pre>".print_r($modelAngsuran->errors,1)."</pre>";
                echo "<pre>".print_r($modelAngsuran->attributes,1)."</pre>";
            }
        }

        protected function cekSubsidi($modTindakan)
        {
            $subsidi = array();
            switch ($modTindakan->tipepaket_id) {
                case Params::TIPEPAKET_NONPAKET:     
                        $sql = "SELECT * FROM tanggunganpenjamin_m
                                WHERE carabayar_id = ".$modTindakan->carabayar_id."
                                  AND penjamin_id = ".$modTindakan->penjamin_id."
                                  AND kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                  AND tipenonpaket_id = ".$modTindakan->tipepaket_id."
                                  AND tanggunganpenjamin_aktif = TRUE ";
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransitind']!='')?($data['subsidiasuransitind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintahtind']!='')?($data['subsidipemerintahtind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakittind']!='')?($data['subsidirumahsakittind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modTindakan->tarif_tindakan):0;
                        $subsidi['max'] = $data['makstanggpel'];
                    break;
                case Params::TIPEPAKET_LUARPAKET:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
                case null:
                        $subsidi['asuransi'] = 0;
                        $subsidi['pemerintah'] = 0;
                        $subsidi['rumahsakit'] = 0;
                        $subsidi['iurbiaya'] = 0;
                    break;
                default:
                        $sql = "SELECT subsidiasuransi,subsidipemerintah,subsidirumahsakit,iurbiaya FROM paketpelayanan_m
                                JOIN daftartindakan_m ON daftartindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id
                                JOIN kategoritindakan_m ON kategoritindakan_m.kategoritindakan_id = daftartindakan_m.kategoritindakan_id
                                JOIN tariftindakan_m ON tariftindakan_m.daftartindakan_id = paketpelayanan_m.daftartindakan_id 
                                    AND komponentarif_id = ".Params::KOMPONENTARIF_ID_TOTAL."
                                WHERE tariftindakan_m.kelaspelayanan_id = ".$modTindakan->kelaspelayanan_id."
                                    AND ruangan_id = ".$modTindakan->create_ruangan."
                                    AND daftartindakan_m.daftartindakan_id = ".$modTindakan->daftartindakan_id."
                                    AND paketpelayanan_m.tipepaket_id = ".$modTindakan->tipepaket_id;
                        $data = Yii::app()->db->createCommand($sql)->queryRow();

                        $subsidi['asuransi'] = ($data['subsidiasuransi']!='')?$data['subsidiasuransi']:0;
                        $subsidi['pemerintah'] = ($data['subsidipemerintah']!='')?$data['subsidipemerintah']:0;
                        $subsidi['rumahsakit'] = ($data['subsidirumahsakit']!='')?$data['subsidirumahsakit']:0;
                        $subsidi['iurbiaya'] = ($data['iurbiaya']!='')?$data['iurbiaya']:0;
                    break;
            }

            return $subsidi;
        }

        protected function cekSubsidiOa($modObatAlkesPasien)
        {
            $subsidi = array();   
            if($modObatAlkesPasien->carabayar_id != ''){
            $sql = "SELECT * FROM tanggunganpenjamin_m
                        WHERE carabayar_id = ".$modObatAlkesPasien->carabayar_id."
                          AND penjamin_id = ".$modObatAlkesPasien->penjamin_id."
                          AND kelaspelayanan_id = ".$modObatAlkesPasien->kelaspelayanan_id."
                          AND tipenonpaket_id = ".$modObatAlkesPasien->tipepaket_id."
                          AND tanggunganpenjamin_aktif = TRUE ";
                $data = Yii::app()->db->createCommand($sql)->queryRow();

                $subsidi['asuransi'] = ($data['subsidiasuransioa']!='')?($data['subsidiasuransioa']/100 * $modObatAlkesPasien->hargajual_oa):0;
                $subsidi['pemerintah'] = ($data['subsidipemerintahoa']!='')?($data['subsidipemerintahoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['rumahsakit'] = ($data['subsidirumahsakitoa']!='')?($data['subsidirumahsakitoa']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['iurbiaya'] = ($data['iurbiayatind']!='')?($data['iurbiayatind']/100 * $modObatAlkesPasien->hargasatuan_oa):0;
                $subsidi['max'] = $data['makstanggpel'];
            } else {
                $subsidi['asuransi'] = 0;
                $subsidi['pemerintah'] = 0;
                $subsidi['rumahsakit'] = 0;
                $subsidi['iurbiaya'] = 0;
            }

            return $subsidi;
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}