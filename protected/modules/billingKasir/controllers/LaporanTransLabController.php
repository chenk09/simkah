<?php

class LaporanTransLabController extends SBaseController {

    //public $defaultAction = 'rekapTransaksiLab';
    public function actionIndex($view = 'trans') {
        $model = new BKLaporanPendPenunjangV();
        $model->tglAwal = date('d M Y H:i:s');
        $model->tglAkhir = date('d M Y H:i:s');
        if (isset($_GET['BKLaporanPendPenunjangV'])) {
            $model->attributes = $_GET['BKLaporanPendPenunjangV'];
            $model->asal = $_GET['BKLaporanPendPenunjangV']['asal'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAkhir']);
        }

        if ($view == 'trans') {
            $this->render('rekap_transaksi', array(
                'model' => $model,
                'view' => $view
            ));
        }

        if ($view == 'pasien') {
            $this->render('rekap_pasien', array(
                'model' => $model,
                'view' => $view
            ));
        }

        if ($view == 'reg') {
            $this->render('rekap_registrasi', array(
                'model' => $model,
                'view' => $view
            ));
        }
    }

    public function actionPrintKeseluruhan() {
        $model = new BKLaporanPendPenunjangV();
        $view = in_array($_REQUEST['view'], array('trans', 'pasien', 'reg')) ? $_REQUEST['view'] : 'trans';
        
        if ($view == 'trans') {
            $judulLaporan = 'Laporan Transaksi Laboratorium';
            $data['title'] = 'Grafik Laporan  Transaksi Laboratorium';
        } else
        if ($view == 'pasien') {
            $judulLaporan = 'Laporan Rekap Pasien Transaksi Laboratorium';
            $data['title'] = 'Grafik Laporan Rekap Pasien Transaksi Laboratorium';
        } else {
            $judulLaporan = 'Laporan Per-Registrasi Transaksi Laboratorium';
            $data['title'] = 'Grafik Laporan Per-Registrasi Transaksi Laboratorium';
        }

        $data['type'] = $_REQUEST['type'];
        if (isset($_REQUEST['BKLaporanPendPenunjangV'])) {
            $model->attributes = $_REQUEST['BKLaporanPendPenunjangV'];
            $model->asal = $_GET['BKLaporanPendPenunjangV']['asal'];
            $format = new CustomFormat();
            $model->tglAwal = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanPendPenunjangV']['tglAwal']);
            $model->tglAkhir = $format->formatDateTimeMediumForDB($_REQUEST['BKLaporanPendPenunjangV']['tglAkhir']);
        }

        $caraPrint = $_REQUEST['caraPrint'];

        if ($view == 'trans') {
            $target = '_printTransaksi';
        }

        if ($view == 'pasien') {
            $target = '_printPasien';
        }

        if ($view == 'reg') {
            $target = '_printReg';
        }

        $this->printFunction($model, $data, $caraPrint, $judulLaporan, $target);
    }

    protected function printFunction($model, $data, $caraPrint, $judulLaporan, $target) {
        $format = new CustomFormat();
        $periode = $this->parserTanggal($model->tglAwal) . ' s/d ' . $this->parserTanggal($model->tglAkhir);

        if ($caraPrint == 'PRINT' || $caraPrint == 'GRAFIK') {
            $this->layout = '//layouts/printWindows';
            $this->render($target, array('model' => $model, 'periode' => $periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($caraPrint == 'EXCEL') {
            $this->layout = '//layouts/printExcel';
            $this->render($target, array('model' => $model, 'periode' => $periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint));
        } else if ($_REQUEST['caraPrint'] == 'PDF') {
            $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
            $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
            $mpdf = new MyPDF('', $ukuranKertasPDF);
            $mpdf->useOddEven = 2;
            $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->AddPage($posisi, '', '', '', '', 15, 15, 15, 15, 15, 15);
            $mpdf->WriteHTML($this->renderPartial($target, array('model' => $model, 'periode' => $periode, 'data' => $data, 'judulLaporan' => $judulLaporan, 'caraPrint' => $caraPrint), true));
            $mpdf->Output();
        }
    }

    protected function parserTanggal($tgl) {
        $tgl = explode(' ', $tgl);
        $result = array();
        foreach ($tgl as $row) {
            if (!empty($row)) {
                $result[] = $row;
            }
        }
        return Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($result[0], 'yyyy-MM-dd'), 'medium', null) . ' ' . $result[1];
    }

    public function rekapTransaksiLab() {
        /*
          $model = new BKLaporanPendPenunjangV();
          $model->tglAwal = date('d M Y H:i:s');
          $model->tglAkhir = date('d M Y H:i:s');
          if (isset($_GET['BKLaporanPendPenunjangV'])){
          $model->attributes = $_GET['BKLaporanPendPenunjangV'];
          $model->asal = $_GET['BKLaporanPendPenunjangV']['asal'];
          $format = new CustomFormat();
          $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAwal']);
          $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAkhir']);
          }

          $this->render('rekap_transaksi', array(
          'model' => $model
          ));
         */
    }

    /*
      public function actionRekapPasienLab()
      {
      $model = new BKLaporanPendPenunjangV();
      $model->tglAwal = date('d M Y H:i:s');
      $model->tglAkhir = date('d M Y H:i:s');

      if(isset($_GET['BKLaporanPendPenunjangV'])){
      $model->attributes = $_GET['BKLaporanPendPenunjangV'];
      $model->asal = $_GET['BKLaporanPendPenunjangV']['asal'];
      $format = new CustomFormat();
      $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAwal']);
      $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAkhir']);
      }

      $this->render('rekap_pasien', array(
      'model' => $model
      ));
      }

      public function actionRekapRegLab()
      {
      $model = new BKLaporanPendPenunjangV();
      $model->tglAwal = date('d M Y H:i:s');
      $model->tglAkhir = date('d M Y H:i:s');

      if (isset($_GET['BKLaporanPendPenunjangV'])){
      $model->attributes = $_GET['BKLaporanPendPenunjangV'];
      $model->asal = $_GET['BKLaporanPendPenunjangV']['asal'];
      $format = new CustomFormat();
      $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAwal']);
      $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporanPendPenunjangV']['tglAkhir']);
      }

      $this->render('rekap_registrasi', array(
      'model' => $model
      ));
      }
     */
}
