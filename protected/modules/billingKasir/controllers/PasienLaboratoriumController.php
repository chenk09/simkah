<?php

class PasienLaboratoriumController extends SBaseController
{
	public $layout='//layouts/column1';
        public $defaultAction = 'admin';
        
	public function actionIndex()
	{
//                //if(!Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)){throw new CHttpException(401,Yii::t('mds','You are prohibited to access this page. Contact Super Administrator'));}
		$model = new BKRinciantagihapasienpenunjangV('searchRincianTagihan');
                $format = new CustomFormat();
                $model->tglAwal = date("d M Y").' 00:00:00';
                $model->tglAkhir = date('d M Y h:i:s');
		$model->unsetAttributes();  // clear any default values
                
		if(isset($_GET['BKRinciantagihapasienpenunjangV'])){
			$model->attributes=$_GET['BKRinciantagihapasienpenunjangV'];
			$model->statusBayar=$_GET['BKRinciantagihapasienpenunjangV']['statusBayar'];
                        $format = new CustomFormat();
                        $model->tglAwal = $format->formatDateTimeMediumForDB($_GET['BKRinciantagihapasienpenunjangV']['tglAwal']);
                        $model->tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKRinciantagihapasienpenunjangV']['tglAkhir']);

                }
+
		$this->render('billingKasir.views.pasienLaboratorium.index',array(
			'model'=>$model,
		));
	}
        
        public function actionRincian($id){
            $model = new BKRinciantagihanpasienpenunjangV('searchRincianTagihan');
            $this->layout = '//layouts/frameDialog';
            $data['judulLaporan'] = 'Rincian Tagihan Pasien';
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            if(empty($modPenunjang)){
                $criteria = new CDbCriteria;
                $criteria->with = array('pasienadmisi');
                $criteria->compare('pasienadmisi.pendaftaran_id', $id);
                $modPenunjang = PasienmasukpenunjangT::model()->find($criteria);
            }
            $modRincian = BKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
//            $modRincian->pendaftaran_id = $id;
            $this->render('billingKasir.views.pasienLaboratorium.rincian', array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data));
        }
        
        public function actionPrint()
        {
            $id = $_REQUEST['id'];
            $modPendaftaran = BKPendaftaranT::model()->findByPk($id);
            $modRincian = BKRinciantagihanpasienpenunjangV::model()->findAllByAttributes(array('pendaftaran_id' => $id), array('order'=>'ruangan_id'));
            $modPenunjang = PasienmasukpenunjangT::model()->findByAttributes(array('pendaftaran_id'=>$id));
            $data['nama_pegawai'] = LoginpemakaiK::model()->findByPK(Yii::app()->user->id)->pegawai->nama_pegawai;
            $data['judulLaporan']='Data Rincian Tagihan Pasien';
            $caraPrint=$_REQUEST['caraPrint'];
            if($caraPrint=='PRINT') {
                $this->layout='//layouts/printWindows';
                $this->render('billingKasir.views.rinciantagihanpasienpenunjangV.rincian', array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
                //$this->render('rincian',array('model'=>$model,'judulLaporan'=>$judulLaporan,'caraPrint'=>$caraPrint));
            }
            else if($caraPrint=='EXCEL') {
                $this->layout='//layouts/printExcel';
                $this->render('billingKasir.views.rinciantagihanpasienpenunjangV.rincian',array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint));
            }
            else if($_REQUEST['caraPrint']=='PDF') {
                $ukuranKertasPDF = Yii::app()->user->getState('ukuran_kertas');                  //Ukuran Kertas Pdf
                $posisi = Yii::app()->user->getState('posisi_kertas');                           //Posisi L->Landscape,P->Portait
                $mpdf = new MyPDF('',$ukuranKertasPDF); 
                $mpdf->useOddEven = 2;  
                $stylesheet = file_get_contents(Yii::getPathOfAlias('webroot.css') . '/bootstrap.css');
                $mpdf->WriteHTML($stylesheet,1);  
                $style = '<style>.control-label{float:left; text-align: right; width:140px;font-size:12px; color:black;padding-right:10px;  }</style>';
                $mpdf->WriteHTML($style, 1);  
                $mpdf->AddPage($posisi,'','','','',15,15,15,15,15,15);
                $mpdf->WriteHTML($this->renderPartial('billingKasir.views.rinciantagihanpasienpenunjangV.rincian',array('modPenunjang'=>$modPenunjang,'modPendaftaran'=>$modPendaftaran, 'modRincian'=>$modRincian, 'data'=>$data, 'caraPrint'=>$caraPrint),true));
                $mpdf->Output();
            }                       
        }
    
    
}
