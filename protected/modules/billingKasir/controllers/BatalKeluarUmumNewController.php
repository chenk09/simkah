<?php

class BatalKeluarUmumNewController extends SBaseController
{
        protected $successSave=true;
        public $pathView = 'billingKasir.views.batalKeluarUmumNew.';
        
        public function actionIndex()
	{
            $modBatalBayar = new BKBatalKeluarUmumT;
            $modPengeluaran = new BKPengeluaranumumT;
		
            if(isset($_POST['BKBatalKeluarUmumT'])){
                $modPengeluaran->attributes = $_POST['BKPengeluaranumumT'];
                
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $modBatalBayar = $this->saveBatalKeluarUmum($_POST['BKBatalKeluarUmumT']);
                    $this->updateBuktiKeluar($modBatalBayar);
                    $this->updatePengeluaranUmum($modBatalBayar);
                    
                    if($this->successSave){
                        $transaction->commit();
                        Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                    } else {
                        $transaction->rollback();
                        Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                    }
                } catch (Exception $exc) {
                    $transaction->rollback();
                    Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                }
            }
            
            $this->render($this->pathView.'index',array('modBatalBayar'=>$modBatalBayar,
                                        'modPengeluaran'=>$modPengeluaran));
	}
        
        protected function saveBatalKeluarUmum($postBatalKeluarUmum)
        {
            $format = new CustomFormat();
            $modBatal = new BKBatalKeluarUmumT;
            $modBatal->attributes = $postBatalKeluarUmum;
            $modBatal->ruangan_id = Yii::app()->user->getState('ruangan_id');
            $modBatal->tglbatalkeluar = $format->formatDateMediumForDB($postBatalKeluarUmum['tglbatalkeluar']);
            if($modBatal->validate()){
                $modBatal->save();
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBatal;
        }
        
        protected function updateBuktiKeluar($modBatal)
        {
            TandabuktikeluarT::model()->updateByPk($modBatal->tandabuktikeluar_id, array('batalkeluarumum_id'=>$modBatal->batalkeluarumum_id));
        }
        
        protected function updatePengeluaranUmum($modBatal)
        {
            PengeluaranumumT::model()->updateByPk($modBatal->pengeluaranumum_id, array('batalkeluarumum_id'=>$modBatal->batalkeluarumum_id));
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}