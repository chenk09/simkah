<?php

class BatalBayarSupplierController extends SBaseController
{
        protected $successSave=true;
        public $pathView = 'billingKasir.views.batalBayarSupplier.';
        
	public function actionIndex()
	{
                    if(!empty($_GET['frame'])){
                        $this->layout = 'frameDialog';
                    }
                    $modBuktiKeluar = new BKTandabuktikeluarT;
                    $modBayarSupplier = new BKBayarkeSupplierT;
                    $modBatalBayar = new BKBatalBayarSupplierT;

                    if(isset($_POST['BKBatalBayarSupplierT'])){
                        $modBuktiKeluar->attributes = $_POST['BKTandabuktikeluarT'];
                        $transaction = Yii::app()->db->beginTransaction();
                        try {
                            $modBatalBayar = $this->saveBatalBayarSupplier($_POST['BKBatalBayarSupplierT']);

                            if($this->successSave){
                                $transaction->commit();
                                Yii::app()->user->setFlash('success',"Data berhasil disimpan");
                            } else {
                                $transaction->rollback();
                                Yii::app()->user->setFlash('error',"Data gagal disimpan ");
                            }
                        } catch (Exception $exc) {
                            $transaction->rollback();
                            Yii::app()->user->setFlash('error',"Data gagal disimpan ".MyExceptionMessage::getMessage($exc,true));
                        }
                    }

                    $this->render($this->pathView.'index',array('modBuktiKeluar'=>$modBuktiKeluar,
                                                'modBayarSupplier'=>$modBayarSupplier,
                                                'modBatalBayar'=>$modBatalBayar));
	}
        
        protected function saveBatalBayarSupplier($postBatalBayar)
        {
            $modBatalBayar = new BKBatalBayarSupplierT;
            $modBatalBayar->attributes = $postBatalBayar;
            $modBatalBayar->ruangan_id = Yii::app()->user->getState('ruangan_id');
            if($modBatalBayar->validate()){
                $this->successSave = $this->successSave && true;
            } else {
                $this->successSave = false;
            }
            
            return $modBatalBayar;
        }

        // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}