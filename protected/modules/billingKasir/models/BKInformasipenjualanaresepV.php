<?php

/**
 * This is the model class for table "informasipenjualanaresep_v".
 *
 * The followings are the available columns in table 'informasipenjualanaresep_v':
 * @property integer $pasien_id
 * @property string $no_rekam_medik
 * @property string $namadepan
 * @property string $nama_pasien
 * @property string $nama_bin
 * @property string $jeniskelamin
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $alamat_pasien
 * @property integer $rt
 * @property integer $rw
 * @property integer $penjualanresep_id
 * @property string $jenispenjualan
 * @property string $tglresep
 * @property string $noresep
 * @property double $totharganetto
 * @property double $totalhargajual
 * @property double $totaltarifservice
 * @property double $biayaadministrasi
 * @property double $biayakonseling
 * @property double $pembulatanharga
 * @property double $jasadokterresep
 * @property string $instalasiasal_nama
 * @property string $ruanganasal_nama
 * @property integer $ruangan_id
 * @property integer $pegawai_id
 * @property string $nama_pegawai
 * @property string $gelardepan
 * @property integer $carabayar_id
 * @property string $carabayar_nama
 * @property integer $penjamin_id
 * @property string $penjamin_nama
 * @property string $tglpenjualan
 * @property double $discount
 * @property double $subsidiasuransi
 * @property double $subsidipemerintah
 * @property double $subsidirs
 * @property double $iurbiaya
 * @property integer $lamapelayanan
 * @property integer $pasienadmisi_id
 * @property integer $reseptur_id
 * @property integer $pendaftaran_id
 * @property integer $obatalkes_id
 * @property integer $jenisobatalkes_id
 * @property string $jenisobatalkes_nama
 * @property string $obatalkes_kode
 * @property string $obatalkes_nama
 * @property string $obatalkes_golongan
 * @property string $obatalkes_kategori
 * @property string $obatalkes_kadarobat
 * @property integer $kekuatan
 * @property integer $racikan_id
 * @property integer $shift_id
 * @property string $tglpelayanan
 * @property string $r
 * @property integer $rke
 * @property double $qty_oa
 * @property double $hargasatuan_oa
 * @property string $signa_oa
 * @property double $harganetto_oa
 * @property double $hargajual_oa
 * @property string $etiket
 * @property double $biayaservice
 * @property double $biayakemasan
 * @property string $oa
 * @property string $create_time
 * @property string $update_time
 * @property string $create_loginpemakai_id
 * @property string $create_ruangan
 * @property string $update_loginpemakai_id
 * @property integer $sumberdana_id
 * @property string $sumberdana_nama
 * @property integer $satuankecil_id
 * @property string $satuankecil_nama
 * @property integer $tipepaket_id
 * @property integer $oasudahbayar_id
 */
class BKInformasipenjualanaresepV extends InformasipenjualanaresepV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasipenjualanaresepV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
}