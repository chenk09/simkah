<?php

class BKLaporanclosingkasirV extends LaporanclosingkasirV
{
        public $tglAwal, $tglAkhir, $ruanganKasir = array();
        public $jumlah, $data, $tick; //untuk grafik
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LaporanclosingkasirV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
         * Digunakan di:
         * - Biling Kasir - Laporan - Laporan Closing Kasir
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
        protected function functionCriteria(){
            $criteria=new CDbCriteria;
            if(!empty($this->tglAwal) && !empty($this->tglAkhir)){
                $criteria->addBetweenCondition('tglclosingkasir',$this->tglAwal.' 00:00:00',$this->tglAkhir.' 23:59:59');
            }
            $criteria->compare('closingkasir_id',$this->closingkasir_id);
            $criteria->compare('shift_id',$this->shift_id);
            $criteria->compare('shift_nama',$this->shift_nama,true);
            $criteria->compare('pegawai_id',$this->pegawai_id);
            $criteria->compare('nama_pegawai',$this->nama_pegawai,true);
            $criteria->compare('setorbank_id',$this->setorbank_id);
            $criteria->compare('nostruksetor',$this->nostruksetor,true);
            $criteria->compare('tgldisetor',$this->tgldisetor,true);
            $criteria->compare('namabank',$this->namabank,true);
            $criteria->compare('norekening',$this->norekening,true);
            $criteria->compare('jumlahsetoran',$this->jumlahsetoran);
            $criteria->compare('closingdari',$this->closingdari,true);
            $criteria->compare('sampaidengan',$this->sampaidengan,true);
            $criteria->compare('closingsaldoawal',$this->closingsaldoawal);
            $criteria->compare('terimauangmuka',$this->terimauangmuka);
            $criteria->compare('terimauangpelayanan',$this->terimauangpelayanan);
            $criteria->compare('totalpengeluaran',$this->totalpengeluaran);
            $criteria->compare('totalsetoran',$this->totalsetoran);
            $criteria->compare('keterangan_closing',$this->keterangan_closing,true);
            $criteria->compare('jmluanglogam',$this->jmluanglogam);
            $criteria->compare('jmluangkertas',$this->jmluangkertas);
            $criteria->compare('jmltransaksi',$this->jmltransaksi);
            $criteria->compare('piutang',$this->piutang);
            $criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
            $criteria->compare('create_time',$this->create_time,true);
            $criteria->compare('update_time',$this->update_time,true);
            $criteria->compare('create_loginpemakai_id',$this->create_loginpemakai_id,true);
            $criteria->compare('update_loginpemakai_id',$this->update_loginpemakai_id,true);
            if($this->ruanganKasir)
                $criteria->addInCondition('create_ruangan', $this->ruanganKasir);
            else
                $criteria->addCondition('create_ruangan IS NULL');
            return $criteria;
        }
        
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                $criteria = new CDbCriteria;
                $criteria = $this->functionCriteria();
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function searchTable()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                )
            );
        }
        public function searchTableNew()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                    'pagination'=>FALSE,
                )
            );
        }
        public function searchPrintNew()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                    'pagination'=>FALSE,
                )
            );
        }
        public function searchPrint()
        {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();
            return new CActiveDataProvider($this,
                array(
                    'criteria' => $criteria,
                )
            );
        }
        public function searchGrafik() {
            $criteria = new CDbCriteria;
            $criteria = $this->functionCriteria();

            $criteria->select = 'count(closingkasir_id) as jumlah, shift_nama as data';
            $criteria->select .= ', shift_nama as tick';
            $criteria->group = 'shift_nama';

            return new CActiveDataProvider($this, array(
                        'criteria' => $criteria,
                    ));
        }
        public function getShiftItems(){
            $modShift = ShiftM::model()->findAllByAttributes(array('shift_aktif'=>true), array('order'=>'shift_jamawal'));
            return $modShift;
        }
}