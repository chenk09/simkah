<?php

class BKBayarjasasaranarsV extends BayarjasasaranarsV
{
	public $tglAwal,$tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function cariData()
	{
		$criteria = new CDbCriteria;
		$criteria->addBetweenCondition('tglpembayaran', $this->tglAwal, $this->tglAkhir);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	protected function afterFind()
	{
		foreach($this->metadata->tableSchema->columns as $columnName => $column)
		{
			if(!is_object($this->$columnName))
			{
				if (strtolower($column->dbType) == 'date')
				{
					$this->$columnName = date('m/d/Y', strtotime($this->$columnName));
				}
				elseif (strtolower($column->dbType) == 'datetime')
				{
					$this->$columnName = date('Y-m-d H:i:s', CDateTimeParser::parse($this->$columnName, "dd/mm/yyyy  H:i:s"));
				}
				elseif (strtolower($column->dbType) == 'double precision')
				{
					$this->$columnName = number_format($this->$columnName, 0, ".", ",");
				}
			}
		}
		return parent::afterFind();
	}
	
}