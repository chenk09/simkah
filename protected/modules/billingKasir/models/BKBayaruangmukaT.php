<?php

class BKBayaruangmukaT extends BayaruangmukaT
{
    public $tglAwal;
    public $tglAkhir;
    public $no_rekam_medik;
    public $no_pendaftaran;
    public $nama_pasien;
    public $nama_bin;
    public $sisauangmuka;
    
    public $TglPendaftaran, $NoPendaftaran, $Umur, $KasusPenyakit, $InstalasiId, $InstalasiNama, $RuanganNama
           ,$NamaPasien,$Jeniskelamin,$NamaBin, $PendaftaranId, $PasienId, $PasienAdmisiId, $NoRM;
    public $tgl_pendaftaran_cari,$instalasi_nama,$ruangan_nama,$carabayar_nama,$idInstalasi,
           $tgl_pendaftaran;
    public $jeniskasuspenyakit_nama,$instalasi_id,$carabayar_id,$penjamin_id,$penjamin_nama,$kelaspelayanan_id;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BayaruangmukaT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'pasien'=>array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bayaruangmuka_id' => 'Bayaruangmuka',
			'pendaftaran_id' => 'Pendaftaran',
			'pasien_id' => 'Pasien',
			'pasienadmisi_id' => 'Pasienadmisi',
			'tandabuktibayar_id' => 'Tandabuktibayar',
			'pemakaianuangmuka_id' => 'Pemakainuangmuka',
			'ruangan_id' => 'Ruangan',
			'tgluangmuka' => 'Tgl Deposit',
			'jumlahuangmuka' => 'Jumlah Uang Muka',
			'keteranganuangmuka' => 'Keterangan',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPasienDeposit()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->select = '*, pemakaianuangmuka_t.sisauangmuka AS sisauangmuka';
                $criteria->with = array('pendaftaran','pasien');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.instalasi_id',$this->idInstalasi);
                $criteria->addBetweenCondition('DATE(tgluangmuka)', $this->tglAwal, $this->tglAkhir);
                $criteria->addCondition('pembatalanuangmuka_id IS NULL AND pemakaianuangmuka_t.tandabuktikeluar_id IS NULL');
                $criteria->join = 'LEFT JOIN pemakaianuangmuka_t ON pemakaianuangmuka_t.pemakaianuangmuka_id = t.pemakaianuangmuka_id';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchPasienDepositGrid()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->select = '*, pemakaianuangmuka_t.sisauangmuka AS sisauangmuka';
                $criteria->with = array('pendaftaran','pasien');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.instalasi_id',$this->idInstalasi);
                $criteria->addCondition('pembatalanuangmuka_id IS NULL AND pemakaianuangmuka_t.tandabuktikeluar_id IS NULL');
                $criteria->join = 'LEFT JOIN pemakaianuangmuka_t ON pemakaianuangmuka_t.pemakaianuangmuka_id = t.pemakaianuangmuka_id';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getTglPendaftaran(){
            return $this->pendaftaran->tgl_pendaftaran;
        }
        
        public function getNoPendaftaran(){
            return $this->pendaftaran->no_pendaftaran;
        }
        
        public function getUmur(){
            return $this->pendaftaran->umur;
        }
        
        public function getKasusPenyakit(){
            return $this->pendaftaran->kasuspenyakit->jeniskasuspenyakit_nama;
        }
        
        public function getInstalasiId(){
            return $this->pendaftaran->instalasi_id;
        }
        
        public function getInstalasiNama(){
            return $this->pendaftaran->instalasi->instalasi_nama;
        }
        
        public function getRuanganNama(){
            return $this->pendaftaran->ruangan->ruangan_nama;
        }
        
        public function getNamaPasien(){
            return $this->pasien->nama_pasien;
        }
        
        public function getJeniskelamin(){
            return $this->pasien->jeniskelamin;
        }
        
        public function getNamaBin(){
            return $this->pasien->nama_bin;
        }
        
        public function getPendaftaranId(){
            return $this->pendaftaran->pendaftaran_id;
        }
        
        public function getPasienId(){
            return $this->pasien->pasien_id;
        }
        
        public function getPasienAdmisiId(){
            return $this->pendaftaran->pasienadmisi_id;
        }
        
        public function getNoRM(){
            return $this->pasien->no_rekam_medik;
        }

    public function getPenjaminItems()
    {
        return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE ORDER BY penjamin_nama');
    }
}