<?php
class BKInformasifaktursupplierV extends InformasifaktursupplierV
{
        public $totharganetto, $jmldiscount, $totalpajakpph, $totalpajakppn, $sub_total;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
         public function getNamaModel() {
            return __CLASS__;
        }
        
        public function searchInformasiFaktur()
	{
		$criteria=new CDbCriteria;

                $criteria->addBetweenCondition('tglfaktur', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
                if (isset($_GET['berdasarkanJatuhTempo']))
                    if($_GET['berdasarkanJatuhTempo']>0)
                        $criteria->addBetweenCondition('tgljatuhtempo', $this->tglAwalJatuhTempo, $this->tglAkhirJatuhTempo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchInformasiFakturPembelian()
	{
		$criteria=new CDbCriteria;

		$criteria->group = 'fakturpembelian_id, supplier_id, supplier_nama, tgljatuhtempo, keteranganfaktur, nofaktur,tglfaktur';
		$criteria->select = $criteria->group.',
			sum(((harganettofaktur*jmlkemasan)*jmlterima) - jmldiscount) as harganettofaktur,
			sum(jmldiscount) as jmldiscount,
			sum(hargapphfaktur) as totalpajakpph,
			sum(jmlterima * hargappnfaktur) as totalpajakppn,
			sum(((harganettofaktur*jmlkemasan)*jmlterima)) as subtot,
			sum((((harganettofaktur*jmlkemasan)*jmlterima) - jmldiscount) + hargappnfaktur) as sub_total
		';
		$criteria->addBetweenCondition('tglfaktur', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
		if (isset($_GET['berdasarkanJatuhTempo']))
			if($_GET['berdasarkanJatuhTempo']>0)
				$criteria->addBetweenCondition('tgljatuhtempo', $this->tglAwalJatuhTempo, $this->tglAkhirJatuhTempo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                }
            }
            return true;
        }
}

?>