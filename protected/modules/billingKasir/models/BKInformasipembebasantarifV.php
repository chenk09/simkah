<?php

class BKInformasipembebasantarifV extends InformasipembebasantarifV
{
        public $tglAwal, $tglAkhir;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasipembebasantarifV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * searchInformasi digunakan di:
         * - Biling Kasir/Informasi/Pembebasan Tarif
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $format=new CustomFormat;
		$criteria->compare('pembebasantarif_id',$this->pembebasantarif_id);
		$criteria->addBetweenCondition('tglpembebasan',$format->formatDateMediumForDB($this->tglAwal).' 00:00:00',$format->formatDateMediumForDB($this->tglAkhir).' 23:59:59');
		$criteria->compare('jmlpembebasan',$this->jmlpembebasan);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai),true);
		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('daftartindakan_id',$this->daftartindakan_id);
		$criteria->compare('LOWER(daftartindakan_nama)',strtolower($this->daftartindakan_nama),true);
		$criteria->compare('komponentarif_id',$this->komponentarif_id);
		$criteria->compare('LOWER(komponentarif_nama)',strtolower($this->komponentarif_nama),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}