<?php

class BKInformasiclosingkasirV extends InformasiclosingkasirV
{
        public $tgl_awal, $tgl_akhir;
        public $is_transferbatch;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InformasiclosingkasirV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
         * digunakan di:
         * - Bilingkasir - Informasi - Closing Kasir
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('closingkasir_id',$this->closingkasir_id);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('LOWER(shift_nama',$this->shift_nama,true);
		$criteria->compare('pegawai_id',$this->pegawai_id);
		$criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai),true);
		$criteria->compare('setorbank_id',$this->setorbank_id);
                $criteria->addBetweenCondition('tglclosingkasir',$this->tgl_awal.' 00:00:00',$this->tgl_akhir.' 23:59:59');
		$criteria->compare('closingdari',$this->closingdari,true);
		$criteria->compare('sampaidengan',$this->sampaidengan,true);
		$criteria->compare('closingsaldoawal',$this->closingsaldoawal);
		$criteria->compare('terimauangmuka',$this->terimauangmuka);
		$criteria->compare('terimauangpelayanan',$this->terimauangpelayanan);
		$criteria->compare('totalsetoran',$this->totalsetoran);
		$criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
		$criteria->compare('create_ruangan',Yii::app()->user->getState('ruangan_id'));
		$criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
                $criteria->group = "closingkasir_id, tglclosingkasir, closingdari, sampaidengan, pegawai_id, nama_pegawai, shift_id, shift_nama, closingsaldoawal, terimauangmuka, terimauangpelayanan, totalsetoran, nilaiclosingtrans, setorbank_id, create_ruangan";
                $criteria->select = $criteria->group;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
                
        public function getBuktibayar($attr){
            $modTandabukti = TandabuktibayarT::model()->findByPk($this->tandabuktibayar_id);
            if($attr = "tglbuktibayar"){
                return $modTandabukti->tglbuktibayar;
            }else{
                return null;
            }
        }
        
        public function getShiftItems(){
            $modShift = ShiftM::model()->findAllByAttributes(array('shift_aktif'=>true), array('order'=>'shift_jamawal'));
            return $modShift;
        }
        /**
         * menampilkan closing kasir grup berdasarkan closingkasir_t
         */
        public function searchBatchClosingKasir(){
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->addBetweenCondition('tglclosingkasir',$this->tgl_awal.' 00:00:00',$this->tgl_akhir.' 23:59:59');
//            $criteria->compare('closingkasir_id',$this->closingkasir_id);
//            $criteria->compare('shift_id',$this->shift_id);
//            $criteria->compare('LOWER(shift_nama',$this->shift_nama,true);
//            $criteria->compare('pegawai_id',$this->pegawai_id);
//            $criteria->compare('LOWER(nama_pegawai)', strtolower($this->nama_pegawai),true);
//            $criteria->compare('setorbank_id',$this->setorbank_id);
//            $criteria->compare('closingdari',$this->closingdari,true);
//            $criteria->compare('sampaidengan',$this->sampaidengan,true);
//            $criteria->compare('closingsaldoawal',$this->closingsaldoawal);
//            $criteria->compare('terimauangmuka',$this->terimauangmuka);
//            $criteria->compare('terimauangpelayanan',$this->terimauangpelayanan);
//            $criteria->compare('totalsetoran',$this->totalsetoran);
//            $criteria->compare('nilaiclosingtrans',$this->nilaiclosingtrans);
            $criteria->compare('create_ruangan',Yii::app()->user->getState('ruangan_id'));
            $criteria->addCondition('batch_sent IS NULL');
//            $criteria->compare('profilrs_id',Yii::app()->user->getState('profilrs_id'));
//            $criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
            $criteria->group = "closingkasir_id, tglclosingkasir, closingdari, sampaidengan, pegawai_id, nama_pegawai, shift_id, shift_nama, closingsaldoawal, terimauangmuka, terimauangpelayanan, totalsetoran, nilaiclosingtrans, setorbank_id, create_ruangan,totalpengeluaran,keterangan_closing,jmluanglogam,jmluangkertas,jmltransaksi,piutang,create_time,update_time,create_loginpemakai_id,update_loginpemakai_id,profilrs_id";
            $criteria->select = $criteria->group;
            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
        }
}