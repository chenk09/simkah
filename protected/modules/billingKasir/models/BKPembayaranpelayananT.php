<?php

class BKPembayaranpelayananT extends PembayaranpelayananT
{
    public $tglAwal;
    public $tglAkhir;
    public $no_rekam_medik;
    public $no_pendaftaran;
    public $nama_pasien;
    public $nama_bin;
    public $tgl_pendaftaran_cari,$idInstalasi;
    
     public $TglPendaftaran, $NoPendaftaran, $Umur, $KasusPenyakit, $InstalasiId, $InstalasiNama, $RuanganNama
           ,$NamaPasien,$Jeniskelamin,$NamaBin, $PendaftaranId, $PasienId, $PasienAdmisiId, $NoRM, $alamatPasien,
            $tandabuktibayarId,$totalOaR,$totalTindakanR,$totalBiayaR;
     public $instalasi_nama;
    // public $pembayaranpelayanan_id;
    
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PembayaranpelayananT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'pendaftaran'=>array(self::BELONGS_TO, 'PendaftaranT', 'pendaftaran_id'),
                    'pasien'=>array(self::BELONGS_TO, 'PasienM', 'pasien_id'),
                    'ruangan'=>array(self::BELONGS_TO, 'RuanganM', 'ruangan_id'),
                    'tandabuktibayar'=>array(self::BELONGS_TO, 'TandabuktibayarT', 'tandabuktibayar_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pembayaranpelayanan_id' => 'Pembayaranpelayanan',
			'pembebasantarif_id' => 'Pembebasantarif',
			'suratketjaminan_id' => 'Suratketjaminan',
			'pasien_id' => 'Pasien',
			'carabayar_id' => 'Carabayar',
			'penjamin_id' => 'Penjamin',
			'ruangan_id' => 'Ruangan',
			'tandabuktibayar_id' => 'Tandabuktibayar',
			'pendaftaran_id' => 'Pendaftaran',
			'pasienadmisi_id' => 'Pasienadmisi',
			'nopembayaran' => 'Nopembayaran',
			'tglpembayaran' => 'Tglpembayaran',
			'noresep' => 'Noresep',
			'nosjp' => 'Nosjp',
			'totalbiayaoa' => 'Totalbiayaoa',
			'totalbiayatindakan' => 'Totalbiayatindakan',
			'totalbiayapelayanan' => 'Totalbiayapelayanan',
			'totalsubsidiasuransi' => 'Totalsubsidiasuransi',
			'totalsubsidipemerintah' => 'Totalsubsidipemerintah',
			'totalsubsidirs' => 'Totalsubsidirs',
			'totaliurbiaya' => 'Totaliurbiaya',
			'totalbayartindakan' => 'Totalbayartindakan',
			'totaldiscount' => 'Totaldiscount',
			'totalpembebasan' => 'Totalpembebasan',
			'totalsisatagihan' => 'Totalsisatagihan',
			'ruanganpelakhir_id' => 'Ruanganpelakhir',
			'statusbayar' => 'Statusbayar',
			'create_time' => 'Create Time',
			'update_time' => 'Update Time',
			'create_loginpemakai_id' => 'Create Loginpemakai',
			'update_loginpemakai_id' => 'Update Loginpemakai',
			'create_ruangan' => 'Create Ruangan',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPasienSudahBayar()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                // $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
                $criteria->compare('pendaftaran.pembayaranpelayanan_id', $this->pembayaranpelayanan_id,true);
//                 $criteria->addCondition('pendaftaran.pembayaranpelayanan_id IS NOT NULL');
                $criteria->addCondition("totalsisatagihan = 0");
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';
				
				return new CActiveDataProvider($this, array(
					'criteria'=>$criteria,
				));
	}
        
        public function searchPasienBerdasarkanPenjamin()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->addCondition('pendaftaran.carabayar_id != 1');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
				$criteria->addCondition("totalsisatagihan = 0");
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));            
        }
        
        public function searchPasienBerdasarkanUmum()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
//                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->addCondition('pendaftaran.carabayar_id = 1');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
				$criteria->addCondition("totalsisatagihan = 0");
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));            
        }
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPasienBerhutang()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->addBetweenCondition('tandabuktibayar.tglbuktibayar', $this->tglAwal, $this->tglAkhir);
                $criteria->compare('t.penjamin_id',$this->penjamin_id);
                $criteria->addCondition("tandabuktibayar.carapembayaran NOT IN ('TUNAI')");
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getNamaModel()
        {
            return __CLASS__;
        }
        
        // perhitungan detailPembayaran() SALAH, DICEK DI INFORMASI PASIEN SUDAH BAYAR TOTALNYA BERBEDA DENGAN KWITANSI DAN RINCIAN TAGIHAN
        public function detailPembayaran()
        {
            $query = "
                SELECT * FROM pembayaranpelayanan_t
                JOIN tandabuktibayar_t ON
                    pembayaranpelayanan_t.tandabuktibayar_id = tandabuktibayar_t.tandabuktibayar_id
                JOIN tindakansudahbayar_t ON
                    tindakansudahbayar_t.pembayaranpelayanan_id = tindakansudahbayar_t.pembayaranpelayanan_id AND 
                    pembayaranpelayanan_t.pembayaranpelayanan_id = tindakansudahbayar_t.pembayaranpelayanan_id
                JOIN tindakanpelayanan_t ON 
                    tindakanpelayanan_t.tindakansudahbayar_id = tindakansudahbayar_t.tindakansudahbayar_id
                JOIN daftartindakan_m ON
                    tindakanpelayanan_t.daftartindakan_id = daftartindakan_m.daftartindakan_id
                WHERE pembayaranpelayanan_t.pembayaranpelayanan_id = '". $this->pembayaranpelayanan_id ."'
            ";
            $detail = Yii::app()->db->createCommand($query)->queryAll();
            return $detail;
        }

        public function getTotalRetur()
        {
        	$total="Sudah Retur";
        	//Data Tidak Ditemukan
        	// $modTandaBukti= TandabuktibayarT::model()->findByAttributes(array('pembayaranpelayanan_id'=>$this->pembayaranpelayanan_id));
        	// $modRetur= ReturbayarpelayananT::model()->findByAttributes(array('tandabuktibayar_id'=>$this->tandabuktibayar_id));
        	// $total = $modRetur->totalbiayaretur+$modRetur->biayaadministrasi;
        	return $total;

        }
        
        public function searchPasienRetur(){
            $criteria=new CDbCriteria;

            $criteria->with = array('pendaftaran','pasien','ruangan');
            $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
            $criteria->compare('pendaftaran.instalasi_id',$this->idInstalasi);
            
            return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
        }
        
        public function getTglPendaftaran(){
            return $this->pendaftaran->tgl_pendaftaran;
        }
        
        public function getNoPendaftaran(){
            return $this->pendaftaran->no_pendaftaran;
        }
        
        public function getUmur(){
            return $this->pendaftaran->umur;
        }
        
        public function getKasusPenyakit(){
            return $this->pendaftaran->kasuspenyakit->jeniskasuspenyakit_nama;
        }
        
        public function getInstalasiId(){
            return $this->pendaftaran->instalasi_id;
        }
        
        public function getInstalasiNama(){
            return $this->pendaftaran->instalasi->instalasi_nama;
        }
        
        public function getRuanganNama(){
            return $this->pendaftaran->ruangan->ruangan_nama;
        }
        
        public function getNamaPasien(){
            return $this->pasien->nama_pasien;
        }
        
        public function getJeniskelamin(){
            return $this->pasien->jeniskelamin;
        }
        
        public function getNamaBin(){
            return $this->pasien->nama_bin;
        }
        
        public function getPendaftaranId(){
            return $this->pendaftaran->pendaftaran_id;
        }
        
        public function getPasienId(){
            return $this->pasien->pasien_id;
        }
        
        public function getPasienAdmisiId(){
            return $this->pendaftaran->pasienadmisi_id;
        }
        
        public function getNoRM(){
            return $this->pasien->no_rekam_medik;
        }
        
        public function getalamatPasien(){
            return $this->pasien->alamat_pasien;
        }
        
        public function getTandabuktibayarId(){
            return $this->tandabuktibayar_id;
        }
        
        public function getTotalOaR(){
            return $this->totalbiayaoa;
        }
        
        public function getTotalTindakanR(){
            return $this->totalbiayatindakan;
        }
        
        public function getTotalBiayaR(){
            return $this->totalbiayapelayanan;
        }
        
        
        /**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function searchPrintPasienSudahBayar()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                // $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
                $criteria->compare('pendaftaran.pembayaranpelayanan_id', $this->pembayaranpelayanan_id,true);
                // $criteria->addCondition('pendaftaran.pembayaranpelayanan_id IS NOT NULL');
//                $criteria->with = 'tandabuktibayar';
                $criteria->addCondition("totalsisatagihan = 0");
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));
	}
        
        public function searchPrintPasienBerdasarkanPenjamin()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
                $criteria->addCondition('pendaftaran.carabayar_id != 1');
//                $criteria->addCondition('pendaftaran.penjamin_id != 117');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';
                
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));            
        }
        
        public function searchPrintPasienBerdasarkanUmum()
        {
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('pendaftaran','pasien','tandabuktibayar');
                $criteria->addCondition('tandabuktibayar.returbayarpelayanan_id IS NULL');
//                $criteria->addCondition('pendaftaran.penjamin_id = 117');
                $criteria->addCondition('pendaftaran.carabayar_id = 1');
                $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
                $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
                $criteria->compare('pendaftaran.penjamin_id', $this->penjamin_id,true);
                $criteria->addBetweenCondition('DATE(tandabuktibayar.tglbuktibayar)', $this->tglAwal, $this->tglAkhir);
                $criteria->order = 'tandabuktibayar.tglbuktibayar DESC';                

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                        'pagination'=>false,
		));            
        }

    public function getPenjaminItems()
    {
        return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE ORDER BY penjamin_nama');
    }
}
