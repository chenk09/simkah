<?php
class BKInformasikasirrdpulangV extends InformasikasirrdpulangV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return InfokunjunganrdV the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function searchRDBaru()
	{
		$format = new CustomFormat();
        $this->tglAwal  = $format->formatDateTimeMediumForDB($this->tglAwal);
        $this->tglAkhir = $format->formatDateTimeMediumForDB($this->tglAkhir);
		
		$criteria = new CDbCriteria;
		$criteria->addBetweenCondition('t.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('statusfarmasi',$this->statusfarmasi);
		$criteria->compare('kirim_farmasi',$this->kirim_farmasi);
		$criteria->addCondition("pasienadmisi_id IS NULL");
		return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
		));
	}

	public function searchRD()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->addBetweenCondition('t.tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
//                $criteria->addCondition('t.pembayaranpelayanan_id IS NULL');
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(statusperiksa)',strtolower($this->statusperiksa),true);
		$criteria->compare('LOWER(statusmasuk)',strtolower($this->statusmasuk),true);
		$criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('LOWER(alamat_pasien)',strtolower($this->alamat_pasien),true);
		$criteria->compare('propinsi_id',$this->propinsi_id);
		$criteria->compare('LOWER(propinsi_nama)',strtolower($this->propinsi_nama),true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('LOWER(kabupaten_nama)',strtolower($this->kabupaten_nama),true);
		$criteria->compare('kecamatan_id',$this->kecamatan_id);
		$criteria->compare('LOWER(kecamatan_nama)',strtolower($this->kecamatan_nama),true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('LOWER(kelurahan_nama)',strtolower($this->kelurahan_nama),true);
		$criteria->compare('instalasi_id',$this->instalasi_id);
		$criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('LOWER(nama_pegawai)',strtolower($this->nama_pegawai),true);
		$criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
   		$criteria->order = 'tgl_pendaftaran DESC';
		//$criteria->compare('rujukan_id',$this->rujukan_id);
		//$criteria->compare('LOWER(statusperiksa)',strtolower(Params::statusPeriksa(3)));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        public function getStatusFarmasi($id){
            $criteria = new CDbCriteria();
            $criteria->compare('pendaftaran_id', $id, false);
            $criteria->addCondition('oasudahbayar_id is null and qty_oa > 0');
            $criteria->order = 'jenisobatalkes_nama, tglpenjualan';
            $modRincian = BKInformasipenjualanaresepV::model()->findAll($criteria);
            
            $jmlObat = count($modRincian);
            
            return $jmlObat;
        }

    protected function afterFind(){
        foreach($this->metadata->tableSchema->columns as $columnName => $column){

            if (!strlen($this->$columnName)) continue;

            if ($column->dbType == 'date'){                         
            	$this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
            } elseif ($column->dbType == 'timestamp without time zone'){
                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
            } elseif ($column->dbType == 'double precision') {
               // $format = new CNumberFormatter('id');
               // $this->$columnName = $format->format('#,##0', $this->$columnName);
            }
        }
        return true;
    }

    public function getPenjaminItems()
    {
        return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE ORDER BY penjamin_nama');
    }
	
}