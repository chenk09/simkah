<?php

class BKTindakanPelayananT extends TindakanpelayananT
{
    public $tglAwal;
    public $tglAkhir;
    public $no_rekam_medik;
    public $no_pendaftaran;
    public $nama_pasien;
    public $nama_bin;
    
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return TindakanpelayananT the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function getJumlahTarif()
    {
        return $this->tarif_tindakan * $this->qty_tindakan + $this->tarifcyto_tindakan;
    }
                
    protected function afterFind(){
        foreach($this->metadata->tableSchema->columns as $columnName => $column){

            if (!strlen($this->$columnName)) continue;

            if ($column->dbType == 'date'){                         
            $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
            } elseif ($column->dbType == 'timestamp without time zone'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
            } elseif ($column->dbType == 'double precision') {
//                $format = new CNumberFormatter('id');
//                $this->$columnName = $format->format('#,##0', $this->$columnName);
            }
        }
        return true;
    }

    public function getTipePaketItems($carabayar_id='')
    {
        return TipepaketM::model()->findAllByAttributes(array('tipepaket_aktif'=>true));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchPasienKarcis()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->with = array('pendaftaran','pasien','daftartindakan');
            $criteria->compare('LOWER(pendaftaran.no_pendaftaran)', strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(pasien.no_rekam_medik)', strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(pasien.nama_pasien)', strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(pasien.nama_bin)', strtolower($this->nama_bin),true);
            $criteria->addBetweenCondition('pendaftaran.tgl_pendaftaran', $this->tglAwal, $this->tglAkhir);
            $criteria->addCondition('t.karcis_id IS NOT NULL');
            $criteria->order = 'pendaftaran.tgl_pendaftaran DESC';
//            $criteria->addCondition('t.tindakansudahbayar_id IS NULL');

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    public function getPenjaminItems()
    {
        return PenjaminpasienM::model()->findAll('penjamin_aktif=TRUE ORDER BY penjamin_nama');
    }
}
?>
