<?php

class BKLaporankinerjapenunjangV extends LaporankinerjapenunjangV {

    public $jeniskelamin,$ruanganpenunj_nama, $kelaspelayanan_nama,$tgl_masukpenunjang;
    public $total,$tarif_satuan, $qty_tindakan;
    public $jumlah, $data, $tick;
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    public function searchKinerja()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->select = "t.kelaspelayanan_id,t.kelaspelayanan_nama, t.no_rekam_medik, t.nama_pasien, t.jeniskelamin, t.daftartindakan_nama,date(tglmasukpenunjang) as tgl_masukpenunjang,
                                t.ruanganpenunj_nama,sum(t.qty_tindakan) as qty_tindakan,t.tarif_satuan, sum(t.tarif_satuan * t.qty_tindakan) as total";
            $criteria->group = "t.kelaspelayanan_id,t.kelaspelayanan_nama,t.no_rekam_medik, t.nama_pasien, t.jeniskelamin, t.daftartindakan_nama, date(tglmasukpenunjang),t.ruanganpenunj_nama,
                                t.tarif_satuan";
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->addBetweenCondition('t.tglmasukpenunjang',$this->tglAwal,$this->tglAkhir,true);
            if(isset($this->ruanganpenunj_id)){
                $criteria->compare('ruanganpenunj_id',$this->ruanganpenunj_id);
            }
            if(isset($this->kelaspelayanan_id)){
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
            }

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    public function searchPrintKinerja()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->select = "t.kelaspelayanan_id,t.kelaspelayanan_nama,t.no_rekam_medik, t.nama_pasien, t.jeniskelamin, t.daftartindakan_nama,date(tglmasukpenunjang) as tgl_masukpenunjang,
                                t.ruanganpenunj_nama,sum(t.qty_tindakan) as qty_tindakan,t.tarif_satuan, sum(t.tarif_satuan * t.qty_tindakan) as total";
            $criteria->group = "t.kelaspelayanan_id,t.kelaspelayanan_nama,t.no_rekam_medik, t.nama_pasien, t.jeniskelamin, t.daftartindakan_nama, date(tglmasukpenunjang),t.ruanganpenunj_nama,
                                t.tarif_satuan";
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->addBetweenCondition('t.tglmasukpenunjang',$this->tglAwal,$this->tglAkhir,true); 
             if(isset($this->ruanganpenunj_id)){
                $criteria->compare('ruanganpenunj_id',$this->ruanganpenunj_id);
            }
            if(isset($this->kelaspelayanan_id)){
                $criteria->compare('kelaspelayanan_id',$this->kelaspelayanan_id);
            }
            $criteria->limit = -1;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    public function searchGrafikKinerja()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;

            $criteria->select = "count(t.pasien_id) as jumlah, t.daftartindakan_nama as data, t.no_rekam_medik, t.nama_pasien, t.jeniskelamin,
                                date(t.tglmasukpenunjang) as tgl_masukpenunjang, t.ruanganpenunj_nama,
                                sum(t.qty_tindakan) as qty_tindakan,t.tarif_satuan, sum(t.tarif_satuan * t.qty_tindakan) as total";
            $criteria->group = "t.no_rekam_medik, t.nama_pasien, t.jeniskelamin, date(t.tglmasukpenunjang), t.daftartindakan_nama, t.ruanganpenunj_nama,
                                t.tarif_satuan";
            $criteria->compare('pasien_id',$this->pasien_id);
            $criteria->addBetweenCondition('t.tglmasukpenunjang',$this->tglAwal,$this->tglAkhir,true);
            if(isset($this->ruanganpenunj_id)){
                $criteria->compare('ruanganpenunj_id',$this->ruanganpenunj_id);
            }
            $criteria->limit = -1;

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){
                $format = new CustomFormat();
                if (!strlen($this->tgl_masukpenunjang)) continue;

                if ($column->dbType == 'date'){                         
                   $this->tgl_masukpenunjang = $format->formatDateINA($this->tgl_masukpenunjang);
                }elseif ($column->dbType == 'timestamp without time zone'){
                        $this->tgl_masukpenunjang = $format->formatDateINA($this->tgl_masukpenunjang);
                }
            }
            return true;
        }
}

?>
