<?php

class BKRinciantagihanpasienpenunjangV extends RinciantagihanpasienpenunjangV
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TindakankomponenT the static model class
	 */
        public $totaltagihan;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchRincianTagihan()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

                $criteria->select = array(
                                'tgl_pendaftaran',
                                'no_pendaftaran',
                                'no_rekam_medik',
                                'nama_pasien',
                                'nama_bin',
                                'nama_pegawai',
                                'pendaftaran_id',
                                'jeniskelamin',
                                'carabayar_nama',
                                'penjamin_nama',
                                'jeniskasuspenyakit_id',
                                'jeniskasuspenyakit_nama',
                                'sum(tarif_tindakan) as totaltagihan',
                                'pembayaranpelayanan_id',
                            );
                
                $criteria->group = 'nama_pegawai, pendaftaran_id,tgl_pendaftaran, no_pendaftaran, no_rekam_medik, nama_pasien, nama_bin, jeniskelamin, 
                            carabayar_nama, penjamin_nama, jeniskasuspenyakit_id, jeniskasuspenyakit_nama, pembayaranpelayanan_id';
                
		$criteria->addBetweenCondition('date(tgl_pendaftaran)', $this->tglAwal, $this->tglAkhir);
		$criteria->compare('LOWER(namadepan)',strtolower($this->namadepan),true);
		$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		$criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
		$criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);
		
		$criteria->compare('tindakanpelayanan_id',$this->tindakanpelayanan_id);
		$criteria->compare('penjamin_id',$this->penjamin_id);
		$criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
		$criteria->compare('carabayar_id',$this->carabayar_id);
		$criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
		$criteria->compare('tarif_tindakan',$this->tarif_tindakan);
		$criteria->compare('jeniskasuspenyakit_id',$this->jeniskasuspenyakit_id);
                $criteria->compare('ruanganpendaftaran_id', 18);
		$criteria->compare('LOWER(jeniskasuspenyakit_nama)',strtolower($this->jeniskasuspenyakit_nama),true);
                if ($this->statusBayar == 'LUNAS'){
                    $criteria->addCondition('pembayaranpelayanan_id is not null');
                }else if ($this->statusBayar == 'BELUM LUNAS'){
                    $criteria->addCondition('pembayaranpelayanan_id is null');
                }
		
//                $criteria->join = 'LEFT JOIN pendaftaran_t ON pendaftaran_t.pendaftaran_id = t.pendaftaran_id LEFT JOIN pasien_m ON pasien_m.pasien_id = t.pasien_id';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function getNamaNamaBIN()
        {
            return $this->nama_pasien.' bin '.$this->nama_bin;
        }
        
        public function getCaraBayarPenjamin()
        {
                return $this->carabayar_nama.' / '.$this->penjamin_nama;
        }
        
        public function getAlamatRTRW()
        {
            return $this->alamat_pasien.'<br>'.$this->rt.' / '.$this->rw;
        }
        
        public function getNoRMNoPend(){
            return $this->no_rekam_medik.'<br/>'.$this->no_pendaftaran;
        }
        
        public function getTglMasukNoPenunjang(){
            return $this->tglmasukpenunjang.'<br/>'.PHP_EOL.$this->no_masukpenunjang;
        }
        
        public function getJenisKelaminUmur(){
            return $this->jeniskelamin.'<br/>'.$this->umur;
        }
        public function getInstalasiRuangan(){
            return $this->instalasiasal_nama.'<br/>'.$this->ruanganasal_nama;
        }
        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                        }elseif ($column->dbType == 'timestamp without time zone'){
                                $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                        CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
                        }
            }
            return true;
        }
}

?>
