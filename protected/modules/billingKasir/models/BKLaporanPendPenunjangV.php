<?php

class BKLaporanPendPenunjangV extends LaporanPendPenunjangV {

    public $tglAwal, $tglAkhir, $bulan, $asal;
    public $filter_tab;

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getNamaModel() {
        return __CLASS__;
    }

    private function functionCriteria() {
        $criteria = new CDbCriteria;
        $criteria->addBetweenCondition('DATE(tglpembayaran)', $this->tglAwal, $this->tglAkhir);
        $criteria->compare('no_pendaftaran', strtolower($this->no_pendaftaran), true);
        $criteria->compare('no_rekam_medik', $this->no_rekam_medik);
        $criteria->compare('LOWER(nama_pasien)', strtolower($this->nama_pasien), true);
        $criteria->compare('penjamin_id', $this->penjamin_id);

        if (isset($this->asal)) {
            if ($this->asal == 'rs')
                $criteria->addCondition('rujukan_id IS NULL');
            else if ($this->asal == 'rujukan') {
                $criteria->addCondition('rujukan_id IS NOT NULL');
            }
        }
        $criteria->addInCondition('ruangan_id', array(
            Params::RUANGAN_ID_LAB,
            Params::RUANGAN_ID_RAD,
        ));
        return $criteria;
    }

    public function searchTable($print = false) {
        $criteria = new CDbCriteria;
        $criteria = $this->functionCriteria();
        $criteria->order = 'tglpembayaran';
        if ($print) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
            ));
        }
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                )
        );
    }

    public function searchTableGroup($print = false) {
        $criteria = new CDbCriteria;
        $criteria = $this->functionCriteria();

        $criteria->select = 'no_rekam_medik, no_pendaftaran, nama_pasien, penjamin_nama, pendaftaran_id, rujukan_id, sum(tarif_tindakan) AS tarif_tindakan, sum(subsidiasuransi_tindakan) AS subsidiasuransi_tindakan, sum(subsidipemerintah_tindakan) AS subsidipemerintah_tindakan, sum(subsisidirumahsakit_tindakan) AS subsisidirumahsakit_tindakan, sum(iurbiaya_tindakan) AS iurbiaya_tindakan';
        $criteria->group = 'no_pendaftaran, no_rekam_medik, nama_pasien, penjamin_nama, pendaftaran_id, rujukan_id';
        $criteria->order = 'no_rekam_medik, nama_pasien, no_pendaftaran';

        if ($print) {
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
                'pagination' => false,
            ));
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
                )
        );
    }

    public function getTotalSubsidi() {
        return ($this->subsidiasuransi_tindakan + $this->subsidipemerintah_tindakan + $this->subsisidirumahsakit_tindakan);
    }

}
