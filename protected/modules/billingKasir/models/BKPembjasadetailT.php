<?php

class BKPembjasadetailT extends PembjasadetailT
{
	public $pilihDetail; //checkbox transaksi pembayaran jasa
	public $penjaminId; 
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PembjasadetailT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('pembjasadetail_id',$this->pembjasadetail_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pembayaranjasa_id',$this->pembayaranjasa_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('jumahtarif',$this->jumahtarif);
		$criteria->compare('jumlahjasa',$this->jumlahjasa);
		$criteria->compare('jumlahbayar',$this->jumlahbayar);
		$criteria->compare('sisajasa',$this->sisajasa);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        public function searchPrint()
        {
                // Warning: Please modify the following code to remove attributes that
                // should not be searched.

                $criteria=new CDbCriteria;
		$criteria->compare('pembjasadetail_id',$this->pembjasadetail_id);
		$criteria->compare('pendaftaran_id',$this->pendaftaran_id);
		$criteria->compare('pembayaranjasa_id',$this->pembayaranjasa_id);
		$criteria->compare('pasienmasukpenunjang_id',$this->pasienmasukpenunjang_id);
		$criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
		$criteria->compare('pasien_id',$this->pasien_id);
		$criteria->compare('jumahtarif',$this->jumahtarif);
		$criteria->compare('jumlahjasa',$this->jumlahjasa);
		$criteria->compare('jumlahbayar',$this->jumlahbayar);
		$criteria->compare('sisajasa',$this->sisajasa);
                // Klo limit lebih kecil dari nol itu berarti ga ada limit 
                $criteria->limit=-1; 

                return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                ));
        }
}