<?php
class BKLaporantindakankomponenV extends LaporantindakankomponenV
{
	public $komponentarif_id;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// -- REKAP JASA DOKTER -- //        
	public function searchJasaDokter($pagination = true)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$criteria=new CDbCriteria;
		$criteria->addBetweenCondition('DATE(tgl_pendaftaran)',$this->tglAwal,$this->tglAkhir);
		if(!empty($this->pegawai_id)){
			$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
		}
//EHJ-3605   $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
		$criteria->group = 'pegawai_id,nama_pegawai,gelardepan,gelarbelakang_nama';
		$criteria->select = $criteria->group;
		if($pagination == false){
			$pagination = false;
			$criteria->limit = -1;
		}else{
			$pagination = array('pageSize'=>10);
		}
		//$criteria->limit = 1;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>$pagination,
		));
	}
	
	public function searchDetailJasaDokter($pagination = true)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		$criteria->group = 'pendaftaran_id, pasien_id, nama_pasien, namaperusahaan, no_pendaftaran, no_rekam_medik,tgl_pendaftaran,ruangan_nama,ruangan_id,pegawai_id,gelardepan,nama_pegawai,gelarbelakang_nama,instalasi_nama,instalasi_id';
		$criteria->select = $criteria->group;
		$criteria->addBetweenCondition('tgl_pendaftaran',$this->tglAwal,$this->tglAkhir);
		if(!empty($this->pegawai_id)){
			$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
		}
		if(!empty($this->instalasi_id)){
			$criteria->addCondition('instalasi_id = '.$this->instalasi_id);
		}
		if(!empty($this->ruangan_id)){
			$criteria->addCondition('ruangan_id = '.$this->ruangan_id);
		}
		
		if($pagination == false){
			$pagination = false;
			$criteria->limit = -1;
		}else{
			$pagination = array('pageSize'=>10);
		}
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>$pagination,
		));
	}
	
	// -- END REKAP JASA DOKTER -- //
	public function getDokterItems()
	{
		return DokterV::model()->findAll();
	}
        
	public function getSumKomponen($groups = array(), $nama_kolom = null){
		if(isset($_GET['BKLaporantindakankomponenV']['komponentarif_id'])){
			$komponentarif_id = $_GET['BKLaporantindakankomponenV']['komponentarif_id'];
		}
		$format = new CustomFormat();
		$criteria=new CDbCriteria();
		$criteria->group = 'komponentarif_id,daftartindakan_tindakan,daftartindakan_konsul,daftartindakan_visite,daftartindakan_karcis';
		foreach($groups AS $i => $group){
			if($group == 'pegawai'){
				$criteria->group .= ', pegawai_id';
				if(!empty($this->pegawai_id)){
					$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
				}
			}else if($group == 'pendaftaran'){
				$criteria->group .= ', pendaftaran_id, tgl_pendaftaran';
				if(!empty($this->pendaftaran_id)){
					$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
				}
			}else if($group == 'ruangan'){
				$criteria->group .= ', ruangan_id, instalasi_id';
				if(!empty($this->ruangan_id)){
					$criteria->addCondition('ruangan_id = '.$this->ruangan_id);
				}
				if(!empty($this->instalasi_id)){
					$criteria->addCondition('instalasi_id = '.$this->instalasi_id);
				}
			}
		}

		if(is_array($komponentarif_id)){
			$criteria->addInCondition('komponentarif_id',$komponentarif_id);
		}else{
			$criteria->addCondition('komponentarif_id IS NULL'); //data tdk dimunculkan
		}
		if($nama_kolom == 'visite'){
			$criteria->addCondition('daftartindakan_visite = TRUE');
		}else if($nama_kolom == 'konsul'){
			$criteria->addCondition('daftartindakan_konsul = TRUE');
		}else if($nama_kolom == 'jasaoperator'){
			$criteria->addCondition('komponentarif_id = '.PARAMS::KOMPONENTARIF_ID_OPERATOR);
		}else if($nama_kolom == 'sewaalat'){
			$criteria->addCondition('komponentarif_id = '.PARAMS::KOMPONENTARIF_ID_SEWAALAT);
		}else if($nama_kolom == 'alatbahan'){
			$criteria->addCondition('komponentarif_id = '.PARAMS::KOMPONENTARIF_ID_ALATBAHAN);
		}else if($nama_kolom == 'lainnya'){
			$criteria->addCondition('daftartindakan_visite = FALSE');
			$criteria->addCondition('daftartindakan_konsul = FALSE');
			$criteria->addCondition('daftartindakan_tindakan = FALSE');
			$criteria->addCondition('komponentarif_id <> '.PARAMS::KOMPONENTARIF_ID_OPERATOR);
			$criteria->addCondition('komponentarif_id <> '.PARAMS::KOMPONENTARIF_ID_SEWAALAT);
			$criteria->addCondition('komponentarif_id <> '.PARAMS::KOMPONENTARIF_ID_ALATBAHAN);
		}
		if(isset($_GET['BKLaporantindakankomponenV'])){
			$tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAwal']);
			$tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAkhir']);
			$criteria->addBetweenCondition('tgl_pendaftaran',$tglAwal,$tglAkhir);
		}
		$criteria->select = $criteria->group.', sum(tarif_tindakankomp) AS tarif_tindakankomp';
		$modTarif = LaporantindakankomponenV::model()->findAll($criteria);
		$totTarif = 0;
		foreach($modTarif as $key=>$tarif){
			$totTarif += $tarif->tarif_tindakankomp;
		}
		return $totTarif;
	}
        
	public function getSumTindakan($groups = array(),$kolom = 'tarif_tindakan'){
		if(isset($_GET['BKLaporantindakankomponenV']['komponentarif_id'])){
			$komponentarif_id = $_GET['BKLaporantindakankomponenV']['komponentarif_id'];
		}
		$format = new CustomFormat();
		$criteria = new CDbCriteria;

		$criteria->group = 'tindakanpelayanan_id, daftartindakan_id, tarif_tindakan, qty_tindakan, tarif_satuan';
		$criteria->select = $criteria->group;
				
		foreach($groups AS $i => $group){
			if($group == 'pegawai'){
				$criteria->group .= ', pegawai_id';
				if(!empty($this->pegawai_id)){
					$criteria->addCondition('pegawai_id = '.$this->pegawai_id);
				}
			}else if($group == 'pendaftaran'){
				$criteria->group .= ', pendaftaran_id, tgl_pendaftaran';
				if(!empty($this->pendaftaran_id)){
					$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
				}
			}else if($group == 'ruangan'){
				$criteria->group .= ', ruangan_id, instalasi_id';
				if(!empty($this->ruangan_id)){
					$criteria->addCondition('ruangan_id = '.$this->ruangan_id);
				}
				if(!empty($this->instalasi_id)){
					$criteria->addCondition('instalasi_id = '.$this->instalasi_id);
				}
			}
		}
		$criteria->addCondition('daftartindakan_tindakan = TRUE');
		if(isset($_GET['BKLaporantindakankomponenV'])){
			$tglAwal = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAwal']);
			$tglAkhir = $format->formatDateTimeMediumForDB($_GET['BKLaporantindakankomponenV']['tglAkhir']);
			//$criteria->addBetweenCondition('tgl_pendaftaran',$tglAwal,$tglAkhir);
		}else{
		        $tglAwal = date('Y-m-d 00:00:00');
		        $tglAkhir = date('Y-m-d H:i:s');
		}
		$criteria->addBetweenCondition('DATE(tgl_tindakan)',$tglAwal,$tglAkhir);
		$modTarif = LaporantindakankomponenV::model()->findAll($criteria);
		$totTarif = 0;

		foreach($modTarif as $key=>$tarif){
			$totTarif += $tarif->$kolom;
		}

		return $totTarif;

	}
}
