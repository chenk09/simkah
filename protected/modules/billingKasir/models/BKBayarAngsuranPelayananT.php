<?php

class BKBayarAngsuranPelayananT extends BayarangsuranpelayananT
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    protected function beforeValidate ()
    {
        $format = new CustomFormat();
        foreach($this->metadata->tableSchema->columns as $columnName => $column)
        {
            if($column->dbType == 'date')
            {
                $this->$columnName = $format->formatDateMediumForDB($this->$columnName);
            }elseif ($column->dbType == 'timestamp without time zone'){
                $this->$columnName = $format->formatDateTimeMediumForDB($this->$columnName);
            }
        }
        return parent::beforeValidate();
    }

}