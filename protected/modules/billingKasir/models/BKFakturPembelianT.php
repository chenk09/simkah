<?php

class BKFakturPembelianT extends FakturpembelianT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return FakturpembelianT the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function searchInformasi()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('fakturpembelian_id',$this->fakturpembelian_id);
		$criteria->compare('supplier_id',$this->supplier_id);
		$criteria->compare('syaratbayar_id',$this->syaratbayar_id);
		$criteria->compare('ruangan_id',$this->ruangan_id);
		$criteria->compare('LOWER(nofaktur)',strtolower($this->nofaktur),true);
                $criteria->addBetweenCondition('tglfaktur', $this->tglAwal, $this->tglAkhir);
                if (isset($_GET['berdasarkanJatuhTempo']))
                    if($_GET['berdasarkanJatuhTempo']>0)
                        $criteria->addBetweenCondition('tgljatuhtempo', $this->tglAwalJatuhTempo, $this->tglAkhirJatuhTempo);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

        protected function afterFind(){
            foreach($this->metadata->tableSchema->columns as $columnName => $column){

                if (!strlen($this->$columnName)) continue;

                if ($column->dbType == 'date'){                         
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
                }elseif ($column->dbType == 'timestamp without time zone'){
                        $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                                CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss'));
                }
            }
            return true;
        }
}