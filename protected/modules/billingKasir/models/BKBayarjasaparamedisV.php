<?php
class BKBayarjasaparamedisV extends BayarjasaparamedisV
{
	public $tglAwal,$tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function cariData()
	{
		$criteria = new CDbCriteria;
		$criteria->addBetweenCondition('pembayaranpelayanan_t.tglpembayaran', $tglAwal, $tglAkhir);
		$criteria->join = 'INNER JOIN pembayaranpelayanan_t ON pembayaranpelayanan_t.pendaftaran_id = t.pendaftaran_id';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}	
	
	

}