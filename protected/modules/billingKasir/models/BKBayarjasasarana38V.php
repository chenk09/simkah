<?php

class BKBayarjasasarana38V extends Bayarjasasarana38V
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Bayarjasasarana38V the static model class
	 */
	public $tglAwal,$tglAkhir, $filter_tab;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}