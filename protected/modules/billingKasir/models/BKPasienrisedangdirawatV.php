<?php

class BKPasienrisedangdirawatV extends InformasitagihanrisdngdirwtV
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PasienrawatinapV the static model class
     */
    public $ceklis = false;
    public $carakeluar;
    public $tick;
    
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    public function searchRI()
	{
		$criteria=new CDbCriteria;

              //  $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('carabayar_id',$this->carabayar_id); 
                $criteria->compare('caramasuk_id',$this->caramasuk_id);
                $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                if($this->ceklis == 1)
                {
                    $criteria->addCondition('tgladmisi BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
                }
                
               
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function searchRIprint()
	{
		$criteria=new CDbCriteria;

              //  $criteria->compare('ruangan_id',Yii::app()->user->getState('ruangan_id'));
                $criteria->compare('penjamin_id',$this->penjamin_id);
                $criteria->compare('carabayar_id',$this->carabayar_id); 
                $criteria->compare('caramasuk_id',$this->caramasuk_id);
                $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
                $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
                $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
                 $criteria->limit=-1; 
                if($this->ceklis == 1)
                {
                    $criteria->addCondition('tgladmisi BETWEEN \''.$this->tglAwal.'\' AND \''.$this->tglAkhir.'\'');
                }
                
               
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                                                         'pagination'=>false,
		));
	}
        
        
     
        
//        public function getTotaltagihan(){
//            $criteria = new CDbCriteria();
//            $criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
//            $criteria->addCondition('tindakansudahbayar_id IS NULL'); //belum lunas
//            $criteria->select = 'SUM((tarif_satuan * qty_tindakan) + tarifcyto_tindakan + biayaservice + biayaadministrasi + biayakonseling) AS tarif_tindakan';
//            $jumlah = RinciantagihanpasienV::model()->find($criteria)->tarif_tindakan;
//            if (empty($jumlah)){
//                $jumlah = 0;
//            }
//            return $jumlah;
//        }
        
        public function searchRincianTagihan(){
            $criteria = new CDbCriteria();
            
            $criteria->with = array('pendaftaran');
            $criteria->addBetweenCondition('date(t.tgl_pendaftaran)', $this->tglAwal, $this->tglAkhir);
            $criteria->compare('LOWER(t.no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(t.namadepan)',strtolower($this->namadepan),true);
            $criteria->compare('LOWER(t.nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(t.nama_bin)',strtolower($this->nama_bin),true);
//            $criteria->compare('t.pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('LOWER(t.no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('t.ruangan_id', Yii::app()->user->getState('ruangan_id'));
//            $criteria->compare('LOWER(tgl_pendaftaran)',strtolower($this->tgl_pendaftaran),true);

            if ($this->statusBayar == 'LUNAS'){
                $criteria->addCondition('pendaftaran.pembayaranpelayanan_id is not null');
            }else if ($this->statusBayar == 'BELUM LUNAS'){
                $criteria->addCondition('pendaftaran.pembayaranpelayanan_id is null');
            }
            
            return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                ));
        }
        
        /**
         * Untuk menampilkan informasi pindahan dari
         */
        public function getPindahanDari(){
            $retVal = array();
            $modPindahkamar = RIPindahkamarT::model()->findByAttributes(array('masukkamar_id'=>$this->masukkamar_id));
            $modMasukkamarlama = RIMasukKamarT::model()->findByAttributes(array('pindahkamar_id'=>$modPindahkamar->pindahkamar_id));
            if(!$modPindahkamar)
                $modMasukkamarlama = new RIMasukKamarT;
            return $modMasukkamarlama;
        }
        /**
         * Untuk mengecek tindakan dan obat dari pasienadmisi
         */
        public function getTindakanDanObat(){
            $retVal = array();
            $retVal['ada'] = 0;
            $retVal['jmltindakan'] = 0;
            $retVal['jmlobat'] = 0;
            $retVal['msg'] = "";
            $modTindakan = TindakanpelayananT::model()->findAllByAttributes(array('pasienadmisi_id'=>$this->pasienadmisi_id, 'ruangan_id'=>  Yii::app()->user->getState('ruangan_id')));
            $retVal['jmltindakan'] = count($modTindakan);
            if($retVal['jmltindakan'] > 0)
                $retVal['msg'] .= $retVal['jmltindakan']." tindakan ";
            $modObatalkes = ObatalkespasienT::model()->findAllByAttributes(array('pasienadmisi_id'=>$this->pasienadmisi_id));
            $retVal['jmlobat'] = count($modObatalkes);
            if($retVal['jmlobat'] > 0)
                $retVal['msg'] .= $retVal['jmlobat']." obat ";
            $retVal['ada'] = $retVal['jmltindakan'] + $retVal['jmlobat'];
            return $retVal;
        }
        
        
        public function getTotaltagihan()
{
$criteria=$this->searchRI();
$criteria->select='SUM(totaltagihan)';
return $this->commandBuilder->createFindCommand($this->getTableSchema(),$criteria)->queryScalar();
}
        
}
?>
