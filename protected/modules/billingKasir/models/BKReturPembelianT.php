<?php

class BKReturPembelianT extends ReturpembelianT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ReturpembelianT the static model class
	 */
	public $is_posting;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}