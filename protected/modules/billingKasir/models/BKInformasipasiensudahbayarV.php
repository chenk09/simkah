<?php

class BKInformasipasiensudahbayarV extends InformasipasiensudahbayarV
{
    public $tglAwal;
    public $tglAkhir;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return InformasipasiensudahbayarV the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function searchInformasi()
    {
            // Warning: Please modify the following code to remove attributes that
            // should not be searched.

            $criteria=new CDbCriteria;
            $criteria->addBetweenCondition('tglbuktibayar',$this->tglAwal,$this->tglAkhir);
            $criteria->compare('pembayaranpelayanan_id',$this->pembayaranpelayanan_id);
            $criteria->compare('pendaftaran_id',$this->pendaftaran_id);
            $criteria->compare('pasienadmisi_id',$this->pasienadmisi_id);
            $criteria->compare('LOWER(nobuktibayar)',strtolower($this->nobuktibayar),true);
            $criteria->compare('LOWER(instalasi)',strtolower($this->instalasi),true);
            $criteria->compare('LOWER(no_pendaftaran)',strtolower($this->no_pendaftaran),true);
            $criteria->compare('LOWER(no_rekam_medik)',strtolower($this->no_rekam_medik),true);
            $criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
            $criteria->compare('LOWER(nama_bin)',strtolower($this->nama_bin),true);
            $criteria->compare('LOWER(carabayar_nama)',strtolower($this->carabayar_nama),true);
            $criteria->compare('LOWER(penjamin_nama)',strtolower($this->penjamin_nama),true);
            $criteria->compare('totalbiayapelayanan',$this->totalbiayapelayanan);
            $criteria->compare('totalsubsidiasuransi',$this->totalsubsidiasuransi);
            $criteria->compare('totalsubsidipemerintah',$this->totalsubsidipemerintah);
            $criteria->compare('totalsubsidirs',$this->totalsubsidirs);
            $criteria->compare('totaliurbiaya',$this->totaliurbiaya);
            $criteria->compare('totaldiscount',$this->totaldiscount);
            $criteria->compare('totalpembebasan',$this->totalpembebasan);
            $criteria->compare('totalbayartindakan',$this->totalbayartindakan);
            $criteria->compare('tandabuktibayar_id',$this->tandabuktibayar_id);
            $criteria->compare('returbayarpelayanan_id',$this->returbayarpelayanan_id);
            $criteria->compare('closingkasir_id',$this->closingkasir_id);
            $criteria->compare('LOWER(ruangan_nama)',strtolower($this->ruangan_nama),true);
            $criteria->compare('ruangan_id',$this->ruangan_id);
            $criteria->compare('ruangankasir_id',Yii::app()->user->getState('ruangan_id'));
            //$criteria->compare('LOWER(ruangankasir_nama)',strtolower($this->ruangankasir_nama),true);

            return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    
    public function getTotalRetur(){
        $modRetur = ReturbayarpelayananT::model()->findByAttributes(array('returbayarpelayanan_id'=>$this->returbayarpelayanan_id));
        if($modRetur){
            return $modRetur->totalbiayaretur;
        }else
            return 0;
    }

    protected function afterFind(){
        foreach($this->metadata->tableSchema->columns as $columnName => $column){

            if (!strlen($this->$columnName)) continue;

            if ($column->dbType == 'date'){                         
            $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd'),'medium',null);
            } elseif ($column->dbType == 'timestamp without time zone'){
                    $this->$columnName = Yii::app()->dateFormatter->formatDateTime(
                            CDateTimeParser::parse($this->$columnName, 'yyyy-MM-dd hh:mm:ss','medium',null));
            } elseif ($column->dbType == 'double precision') {
               // $format = new CNumberFormatter('id');
               // $this->$columnName = $format->format('#,##0', $this->$columnName);
            }
        }
        return true;
    }
}