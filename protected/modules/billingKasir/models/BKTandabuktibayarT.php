<?php

class BKTandabuktibayarT extends TandabuktibayarT
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TandabuktibayarT the static model class
	 */
        public $tglAwal;
        public $tglAkhir;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function searchTable()
	{
                    $criteria=new CDbCriteria;
                    $criteria->compare('shift_id',$this->shift_id);
                    $criteria->compare('ruangan_id',$this->ruangan_id);
                    $criteria->compare('create_loginpemakai_id', $this->create_loginpemakai_id);
                    $criteria->addBetweenCondition('DATE(tglbuktibayar)',$this->tglAwal, $this->tglAkhir);
                    $criteria->addCondition('closingkasir_id IS NULL AND pembatalanuangmuka_id IS NULL');
                    return new CActiveDataProvider($this, array(
                        'criteria'=>$criteria,
                        'pagination'=>false,
                    ));
	}        
}