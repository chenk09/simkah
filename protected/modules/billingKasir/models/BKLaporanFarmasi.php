<?php

class BKLaporanFarmasi extends LaporanpenjualanobatV {
    public $filter_tab;
    
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
    
    /**
     * data provider untuk table
     */
    public function searchTable(){
        $criteria = $this->functionCriteria(true);
		if(isset($this->no_pendaftaran) && !empty($this->no_pendaftaran))
			$criteria->compare('no_pendaftaran',$this->no_pendaftaran);
        if(isset($this->no_rekam_medik) && !empty($this->no_rekam_medik))
			$criteria->compare('no_rekam_medik',$this->no_rekam_medik);
        if(isset($this->nama_pasien) && !empty($this->nama_pasien))
			$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		 if(isset($this->penjamin_id) && !empty($this->penjamin_id))
			$criteria->addCondition('penjamin_id = '.$this->penjamin_id);
        $criteria->order = 'no_rekam_medik, no_pendaftaran, penjamin_id';
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }

    public function searchPrintTable(){
        $criteria = $this->functionCriteria(true);
		if(isset($this->no_pendaftaran) && !empty($this->no_pendaftaran))
			$criteria->compare('no_pendaftaran',$this->no_pendaftaran);
		if(isset($this->no_rekam_medik) && !empty($this->no_pendaftaran))
			$criteria->compare('no_rekam_medik',$this->no_pendaftaran);
		if(isset($this->nama_pasien) && !empty($this->no_pendaftaran))
			$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
		if(isset($this->penjamin_id) && !empty($this->penjamin_id))
			$criteria->addCondition('penjamin_id = '.$this->penjamin_id);
        $criteria->order = 'no_rekam_medik, no_pendaftaran, penjamin_id';
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
                    'pagination'=>false,
            ));
    }
    
    public function searchTableDetail(){
        $criteria=new CDbCriteria;
		if(!empty($this->pendaftaran_id)){
			$criteria->addCondition('pendaftaran_id = '.$this->pendaftaran_id);
		}
        return new CActiveDataProvider($this, array(
                'criteria'=>$criteria,
                'pagination'=>false,
            )
        );
    }    
    
    public function searchGroupTable(){
        $criteria = $this->functionCriteria(true);
        $criteria->select = 'no_pendaftaran, no_rekam_medik, nama_pasien, instalasiasal_nama, ruanganasal_nama, penjamin_nama, tglpenjualan, penjamin_id, pendaftaran_id';
        $criteria->group = 'no_pendaftaran, no_rekam_medik, nama_pasien, instalasiasal_nama, ruanganasal_nama, penjamin_nama, tglpenjualan, penjamin_id, pendaftaran_id';
        $criteria->order = 'no_rekam_medik, no_pendaftaran, penjamin_id';
		if(isset($this->no_pendaftaran) && !empty($this->no_pendaftaran))
			$criteria->compare('no_pendaftaran',$this->no_pendaftaran);
		if(isset($this->no_rekam_medik) && !empty($this->no_rekam_medik))
			$criteria->compare('no_rekam_medik',$this->no_rekam_medik);
		if(isset($this->penjamin_id) && !empty($this->penjamin_id))
			$criteria->addCondition('penjamin_id = '.$this->penjamin_id);
		if(isset($this->nama_pasien) && !empty($this->nama_pasien))
			$criteria->compare('LOWER(nama_pasien)',strtolower($this->nama_pasien),true);
        return new CActiveDataProvider($this, array(
                    'criteria'=>$criteria,
            ));
    }
    /**
     * method untuk criteria
     * @return CDbCriteria 
     */
    public function functionCriteria()
    {
            $criteria=new CDbCriteria;
            $criteria->addBetweenCondition('tglpenjualan',$this->tglAwal,$this->tglAkhir);
            return $criteria;
    }
    
    /**
     * Method untuk mendapatkan nama Model
     * @return String 
     */
    public function getNamaModel(){
        return __CLASS__;
    }
    
}

