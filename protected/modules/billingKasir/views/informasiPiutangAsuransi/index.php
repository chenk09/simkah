<legend class="rim2">Informasi Piutang Asuransi</legend>
<?php
    Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
        $('.search-form').toggle();
        return false;
    });
    $('#searchLaporan').submit(function(){
        $('#Grafik').attr('src','').css('height','0px');
        $('#laporanpiutangasuransi-grid').addClass('srbacLoading');
        $.fn.yiiGridView.update('laporanpiutangasuransi-grid', {
                data: $(this).serialize()
        });
        return false;
    });
    ");
?>
<div class="search-form">
    <?php 
        $this->renderPartial('_search',array(
            'model'=>$model,
        )); 
    ?>
</div>
<fieldset>
    <?php $this->renderPartial('_table', array('model'=>$model)); ?>
</fieldset>