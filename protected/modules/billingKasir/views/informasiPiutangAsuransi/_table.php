<?php 
$table = 'ext.bootstrap.widgets.BootExcelGridView';
$data = $model->search();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
  $sort = false;
  $data = $model->searchPrint();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<div id="div_penjamin">
    <div>
        <?php if(!$caraPrint){ ?>
        <legend class="rim"> Tabel Piutang Asuransi </legend>
        <?php } ?>
        <?php 
            $format = new CustomFormat();
            $this->widget($table,array(
                'id'=>'laporanpiutangasuransi-grid',
                'dataProvider'=>$data,
                'enableSorting'=>$sort,
                'template'=>$template,
				'mergeColumns'=>array('norekmed','no_pendaftaran','nama_pasien','unit','tgl_pendaftaran'),
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'columns'=>array(
                        array(
                            'header' => 'No',
                            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'text-align:center;'),
                        ), 
                        array(
                            'header'=>'Initial',
                            'type'=>'raw',
                            'value'=>'$data->penjamin->penjamin_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Tanggal Pembayaran',
                            'type'=>'raw',
							'name'=>'tglpiutangasuransi',
                            'value'=>'$data->tglpiutangasuransi',
//                            'value'=>'Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($data->tglpiutangasuransi, \'yyyy-MM-dd\'),\'medium\',null)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'No. Rekam Medik',
                            'type'=>'raw',
							'name'=>'norekmed',
                            'value'=>'$data->pasien->no_rekam_medik',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'No. Pendaftaran',
                            'type'=>'raw',
							'name'=>'no_pendaftaran',
                            'value'=>'$data->pendaftaran->no_pendaftaran',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                           'header'=>'<center>Nama Pasien</center>',
                           'type'=>'raw',
							'name'=>'nama_pasien',
                           'value'=>'$data->pasien->nama_pasien',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                        array(
                           'header'=>'<center>Unit Pelayanan</center>',
                           'type'=>'raw',
							'name'=>'unit',
                           'value'=>'$data->pelayanan->unitpelayanan->ruangan_nama',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                        array(
                           'header'=>'<center>Tanggal Masuk</center>',
                           'type'=>'raw',
							'name'=>'tgl_pendaftaran',
                           'value'=>'$data->pendaftaran->tgl_pendaftaran',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                        array(
                           'header'=>'<center>Tanggal Keluar</center>',
                           'type'=>'raw',
                           'value'=>'(isset($data->admisi)?$data->admisi->tglpulang:"")',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                        array(
                            'header'=>'<center>Total Piutang</center>',
                            'type'=>'raw',
                            'value'=>'MyFunction::formatNumber($data->jmlpiutangasuransi)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'vertical-align:middle;text-align:right;'),
                        ),
                        array(
                            'header'=>'Rincian Piutang',
                            'type'=>'raw',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("InformasiPiutangAsuransi/rincianPiutang",array("id"=>$data->pendaftaran_id, "idpembayaran"=>$data->pembayaranpelayanan_id,"idpiutang"=>$data->piutangasuransi_id, "frame"=>true)),
                                        array("class"=>"", 
                                              "target"=>"iframeRincianPiutang",
                                              "onclick"=>"$(\"#dialogRincianPiutang\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk melihat Rincian Piutang",
                                        ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                        ),
                        
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            )); 
        ?>
    </div>
</div>
<br/>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianPiutang',
    'options'=>array(
        'title'=>'Rincian Piutang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianPiutang" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>