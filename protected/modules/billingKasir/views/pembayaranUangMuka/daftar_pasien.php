<?php
$column = array(
  array(
    'type'=>'raw',
    'htmlOptions' => array(
      'style' => 'text-align:center;width:20px',
    ),
    'value'=>array($this,'generatedButton')
  ),
  'no_rekam_medik',
  'nama_pasien',
  'alamat_pasien',
  'no_pendaftaran'
);

if($model->instalasi_id == 4) {
  $column[] =  array(
    'name'=>'kelaspelayanan_nama',
    'type'=>'raw',
    'value'=>'$data->kelaspelayanan_nama . " - " . $data->ruangan_nama . " - " . $data->kamarruangan_nokamar'
  );
}elseif($model->instalasi_id == 5){
  $column[] = "no_masukpenunjang";
  $column[] =  array(
    'name'=>'kelaspelayanan_nama',
    'type'=>'raw',
    'value'=>'$data->kelaspelayanan_nama . " - " . $data->ruangan_nama'
  );
}else{
  $column[] =  array(
    'name'=>'kelaspelayanan_nama',
    'type'=>'raw',
    'value'=>'$data->kelaspelayanan_nama . " - " . $data->ruangan_nama'
  );
}

?>
<div class="row">
  <div class="span12">
    <?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
      'id'=>$id_grid,
      'dataProvider'=>$model->search(),
      'filter'=>$model,
      'template'=>"{pager}{summary}\n{items}",
      'itemsCssClass'=>'table table-striped table-bordered table-condensed',
      'columns'=>$column,
      'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));?>
  </div>
</div>
