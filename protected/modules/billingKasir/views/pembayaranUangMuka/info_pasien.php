<table>
  <tr>
    <td width="40%">
      <table class="table table-bordered">
        <tr>
          <td width="150">No. Pendaftaran</td>
          <td><?=$data['no_rekam_medik']?> / <?=$data['no_pendaftaran']?></td>
        </tr>
        <tr>
          <td>Nama Pasien</td>
          <td><?=$data['nama_pasien']?></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td><?=$data['alamat_pasien']?> / <?=$data['kabupaten_nama']?> / <?=$data['propinsi_nama']?></td>
        </tr>
      </table>
    </td>
    <td width="40%">
      <table class="table table-bordered">
        <tr>
          <td width="150">Instalasi</td>
          <td><?=$data['namainstalasi']?></td>
        </tr>
        <tr>
          <td>Ruangan</td>
          <td><?=$data['ruangan_nama']?></td>
        </tr>
        <tr>
          <td>Kunjungan</td>
          <td><?=$data['kunjungan']?></td>
        </tr>
        <tr>
          <td>Cara Bayar</td>
          <td><?=$data['carabayar_nama']?></td>
        </tr>
      </table>
    </td>
    <td width="20%">
      <table class="table table-bordered">
        <tr>
          <td>Nama Pasien</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
