<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-modal.js', CClientScript::POS_BEGIN); ?>
<?php Yii::app()->clientScript->registerScript('modal_begin', '
$("#modal_form_dokumen").on("shown.bs.modal", function(event){
  $("form").trigger("reset");
  var url = $(event.relatedTarget).data("url") + Math.floor((Math.random() * 100) + 1) ;
  setTimeout(function(){
    $(".modal-body").load(url, function(response, status, xhr) {
            if(status == "error" ){
                $(".modal-body").html(response);
            }
        });
  }, 100);
});
function hideModal(){
  $("#modal_form_dokumen").modal("hide");
}
function refreshDialogPendaftaran(obj){
  var x = $(obj).val();
  $("form").trigger("reset");
  $("#BKPendaftaranT_instalasi_id").val(x);
}
', CClientScript::POS_END); ?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<div id="modal_form_dokumen"  class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Daftar Pasien</h4>
            </div>
            <div class="modal-body"><center><H4>loading....</H4></center></div>
            <div class="modal-footer">
                <button type="button" class="exit btn btn-default btn-sm pull-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<fieldset>
    <legend class="rim">Data Pasien</legend>
    <div class="pull-right">
      <?php echo CHtml::link('Pasien Rawat Inap','javascript:void(0)',array(
        'class'=>'btn btn-warning',
        "data-url"=>Yii::app()->createUrl('/billingKasir/pembayaranUangMuka/pencarianPasien', array(
          "jenis_instalasi"=>"RANAP",
          "id_grid"=>md5("Pasien_Rawat_Inap" . date("YmdHis"))
        )),
        "data-backdrop"=>"static",
        'data-toggle'=>'modal',
        'data-target'=>'#modal_form_dokumen'
      ));?>
      <?php echo CHtml::link('Pasien Rawat Jalan','javascript:void(0)',array(
        'class'=>'btn btn-warning',
        "data-url"=>Yii::app()->createUrl('/billingKasir/pembayaranUangMuka/pencarianPasien', array(
          "jenis_instalasi"=>"RAJAL",
          "id_grid"=>md5("Pasien_Rawat_Jalan" . date("YmdHis"))
        )),
        "data-backdrop"=>"static",
        'data-toggle'=>'modal',
        'data-target'=>'#modal_form_dokumen'
      ));?>
      <?php echo CHtml::link('Pasien Rawat Darurat','javascript:void(0)',array(
        'class'=>'btn btn-warning',
        "data-url"=>Yii::app()->createUrl('/billingKasir/pembayaranUangMuka/pencarianPasien', array(
          "jenis_instalasi"=>"DAR",
          "id_grid"=>md5("Pasien_Rawat_Darurat" . date("YmdHis"))
        )),
        "data-backdrop"=>"static",
        'data-toggle'=>'modal',
        'data-target'=>'#modal_form_dokumen'
      ));?>
      <?php echo CHtml::link('Pasien Laboratorium','javascript:void(0)',array(
        'class'=>'btn btn-warning',
        "data-url"=>Yii::app()->createUrl('/billingKasir/pembayaranUangMuka/pencarianPasien', array(
          "jenis_instalasi"=>"LAB",
          "id_grid"=>md5("Pasien_Lab" . date("YmdHis"))
        )),
        "data-backdrop"=>"static",
        'data-toggle'=>'modal',
        'data-target'=>'#modal_form_dokumen'
      ));?>

    </div>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            <td><div class=" control-label"><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'no_rek')); ?></div></td>
            <td>
                <?php
                if(!empty($modPendaftaran->instalasi_id)){
                    echo CHtml::textField('BKPendaftaranT[instalasi_id]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));
                }else{
                    echo CHtml::dropDownList('BKPendaftaranT[instalasi_id]', NULL, CHtml::listData($modPendaftaran->InstalasiUangMukaItems, 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'refreshDialogPendaftaran(this);', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                }
                ?>
            </td>
            <td rowspan="5">
                <?php
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>

            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
               <label class="control-label no_rek" >No Rekam Medik</label>
            </td>
            <td>
              <div class="control-group"  id="pasien">
                  <?php
                      if(!empty($modPasien->no_rekam_medik)){
                        echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true));
                      }else{
                          $this->widget('MyJuiAutoComplete', array(
                            'name'=>'BKPasienM[no_rekam_medik]',
                            'value'=>$modPasien->no_rekam_medik,
                            'source'=>'js: function(request, response) {
                                 $.ajax({
                                     url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienInstalasi').'",
                                     dataType: "json",
                                     data: {
                                         term: request.term,
                                         instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                     },
                                     success: function (data) {
                                         response(data);
                                     }
                                 })
                              }',
                             'options'=>array(
                                   'showAnim'=>'fold',
                                   'minLength' => 1,
                                   'focus'=> 'js:function( event, ui ) {
                                        $(this).val(ui.item.value);
                                        return false;
                                    }',
                                   'select'=>'js:function( event, ui ) {
                                        isiDataPasien(ui.item);
                                        loadPembayaran(ui.item.pendaftaran_id);
                                        return false;
                                    }',
                            ),
                            'htmlOptions'=>array(
                              'onfocus'=>'return cekInstalasi();',
                              'placeholder'=>'Ketik No. Rekam Medik',
                              'onkeypress'=>"return $(this).focusNextInputField(event)"
                            ),
                        ));
                    }
                  ?>
              </div>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>

            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td>
              <?php $this->widget('MyJuiAutoComplete', array(
                   'name'=>'BKPasienM[nama_pasien]',
                   'value'=>$modPasien->nama_pasien,
                   'source'=>'js: function(request, response) {
                        $.ajax({
                            url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/pencarianPasien').'",
                            dataType: "json",
                            data: {
                                daftarpasien:true,
                                instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                term: request.term,
                            },
                            success: function (data) {
                              response(data);
                            }
                        })
                    }',
                    'options'=>array(
                      'showAnim'=>'fold',
                      'minLength' => 2,
                      'focus'=> 'js:function( event, ui ) {
                           $(this).val(ui.item.value);
                           return false;
                       }',
                      'select'=>'js:function( event, ui ) {
                           isiDataPasien(ui.item);
                           loadPembayaran(ui.item.pendaftaran_id);
                           return false;
                       }',
                   ),
                   'htmlOptions'=>array(
                     'onfocus'=>'return cekInstalasi();',
                     'placeholder'=>'Ketik Nama Pasien',
                     'onkeypress'=>"return $(this).focusNextInputField(event)"
                   ),
             )); ?>
           </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[jeniskasuspenyakit_nama]',$modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama, array('readonly'=>true)); ?></td>

            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[instalasi_nama]',$modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasienadmisi_id]',$modPendaftaran->pasienadmisi_id, array('readonly'=>true)); ?>
            </td>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[ruangan_nama]', $modPendaftaran->ruangan->ruangan_nama, array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset>

<script type="text/javascript">
function isiDataPasien(data)
{
    $.each(data, function(key, val){
        $('#pembayaran-form').find('[name*=\"['+ key +']\"]').val(val);
    });
    $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(data.nama_pasien);
    $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(data.alamat_pasien);
/*
    $('#BKPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
    $('#BKPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
    $('#BKPendaftaranT_umur').val(data.umur);
    $('#BKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#BKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#BKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#BKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#BKPendaftaranT_pasien_id').val(data.pasien_id);
    $('#BKPendaftaranT_pasienadmisi_id').val(data.pasienadmisi_id);
    if (typeof data.norekammedik !=  'undefined'){
        $('#BKPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#BKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#BKPasienM_nama_pasien').val(data.namapasien);
    $('#BKPasienM_nama_bin').val(data.namabin);

    //$('#BKTandabuktibayarUangMukaT_jmlpembayaran').focus();
    //$('#BKTandabuktibayarUangMukaT_jmlpembayaran').select();
*/
}

function loadPembayaran(idPendaftaran)
{
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaranUangMuka');?>', {idPendaftaran:idPendaftaran}, function(data){
        $('#TandabuktibayarT_jmlpembayaran').val(data.jmlpembayaran);
        $('#totTagihan').val(formatUang(data.tottagihan));
        $('#totPembebasan').val(formatUang(data.totpembebasan));

        var norekammedik = $('#BKPasienM_no_rekam_medik').val();
        var no_pendaftaran = $('#BKPendaftaranT_no_pendaftaran').val();
        var nama_pembayar = norekammedik + '-' + no_pendaftaran + '-' + data.namapasien;
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').val(formatUang(0));
        $('#BKTandabuktibayarUangMukaT_uangditerima').val(formatUang(0));
        $('#BKTandabuktibayarUangMukaT_jmlpembulatan').val(formatUang(data.jmlpembulatan));
        $('#BKTandabuktibayarUangMukaT_uangkembalian').val(formatUang(data.uangkembalian));
        $('#BKTandabuktibayarUangMukaT_biayamaterai').val(formatUang(data.biayamaterai));
        $('#BKTandabuktibayarUangMukaT_biayaadministrasi').val(formatUang(data.biayaadministrasi));
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(data.namapasien);
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(nama_pembayar);
        $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(data.alamatpasien);
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').focus();
        getDataRekeningUangMuka($("#isian_pembayaran").serialize(),"TUNAI"); //load data jurnal rekening
    }, 'json');
}

function cekInstalasi(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    if(instalasiId.length > 0){
        return true;
    }else{
        alert("Silahkan pilih instalasi ! ");
        $("#BKPendaftaranT_instalasi_id").focus();
        return false;
    }
}

</script>
