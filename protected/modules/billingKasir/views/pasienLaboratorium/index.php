<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<legend class="rim2">Rincian Tagihan Pasien Laboratorium Luar</legend>
<?php
    Yii::app()->clientScript->registerScript('search', "
    $('.currency').each(
        function()
        {
            var result = formatUang($(this).text());
            $(this).text(result);
        }
    );        
    $('.search-button').click(function(){
            $('.search-form').toggle();
            return false;
    });
    $('#search').submit(function(){
            $('#rinciantagihanpasienpenunjang-v-grid').addClass('srbacLoading');
            $.fn.yiiGridView.update('rinciantagihanpasienpenunjang-v-grid', {
                    data: $(this).serialize()
            });
            return false;
    });
    ");

$this->widget('bootstrap.widgets.BootAlert'); 
?>

<?php 
    $module  = $this->module->name; 
    $controller = $this->id;
?>
<?php $this->widget('bootstrap.widgets.BootAlert');	?>
<?php $this->widget('ext.bootstrap.widgets.BootGroupGridView',array(
	'id'=>'rinciantagihanpasienpenunjang-v-grid',
	'dataProvider'=>$model->searchRincianTagihan(),
//	'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeColumns' => array('rincian', 'tagihan', 'cetak'),
	'columns'=>array(
                'tgl_pendaftaran',
                array(
                    'header'=>'No Rekam Medik<br/>No Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik.\'<br/>\'.$data->no_pendaftaran',
                ),
            
                array(
                    'header'=>'Nama Pasien',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien.\'<br/>\'.$data->nama_bin',
                ),
            
                array(
                    'header'=>'Cara Bayar<br/>Penjamin',
                    'type'=>'raw',
                    'value'=>'$data->carabayar_nama."<br/>".$data->penjamin_nama',
                ),
                //'nama_pegawai',
                //'ruangan_nama',
                array(
                    'header'=>'Total Tagihan',
                    'type'=>'raw',
                    'htmlOptions'=>array(
                        'class'=>'currency'
                    ),
                    'value'=>'(empty($data->totaltagihan)) ? "0" : $data->totaltagihan',
                ),            
                array(
                'header'=>'Rincian <br/> Tagihan',
                'name'=>'rincian',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("rinciantagihanpasienLab/rincian",array("pembayaranpelayanan_id"=>$data->pembayaranpelayanan_id, "id"=>$data->pendaftaran_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogPembayaran\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat rincian tagihan pasien",
                            ))',
                'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                array(
                    'header'=>'Bayar Tagihan Pasien',
                    'name'=>'tagihan',
                    'type'=>'raw',
                    'value'=>'(empty($data->pembayaranpelayanan_id) ? 
                            CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->createUrl("billingKasir/pembayaran/index",array("idPendaftaran"=>$data->pendaftaran_id,"frame"=>true)),
                                array("class"=>"", 
                                      "target"=>"iframePembayaran",
                                      "onclick"=>"$(\"#dialogPembayaran\").dialog(\"open\");",
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk membayar tagihan pasien",
                                )) : "<div id=\"$data->pendaftaran_id\">-</div>"
                    )',
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                array(
                    'header'=>'Status Pembayaran',
                    'name'=>'cetak',
                    'type'=>'raw',
                    'value'=>'(empty($data->pembayaranpelayanan_id) ? "<div id=\"$data->pendaftaran_id\">Belum Lunas</div>" : "Sudah Lunas"."<br/>".CHtml::Link("<i class=\"icon-print\"></i>",Yii::app()->controller->createUrl("kwitansiLab/view",array("idPendaftaran"=>$data->pendaftaran_id,"idPembayaranPelayanan"=>$data->pembayaranpelayanan_id,"frame"=>true)),
                                array("class"=>"", 
                                      "target"=>"iframeKwitansi",
                                      "onclick"=>"$(\"#dialogKwitansi\").dialog(\"open\");",
                                      "rel"=>"tooltip",
                                      "title"=>"Klik untuk cetak Kwitansi",
                                )))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),		
	),
        'afterAjaxUpdate'=>'function(id, data){
            jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            $(".currency").each(
                function()
                {
                    var result = formatUang($(this).text());
                    $(this).text(result);
                }
            );
        }',
)); ?>

<div class="search-form">
    <?php 
        $this->renderPartial('billingKasir.views.pasienLaboratorium._search',array(
                    'model'=>$model,
        ));
    ?>
</div>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogKwitansi',
        'options' => array(
            'title' => 'Kwitansi Pasien',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
        ),
    ));
?>
<iframe name='iframeKwitansi' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
        'id' => 'dialogPembayaran',
        'options' => array(
            'title' => 'Pembayaran Tagihan Pasien Laboratorium',
            'autoOpen' => false,
            'modal' => true,
            'width' => 900,
            'height' => 550,
            'resizable' => false,
            'close'=>'js:function(){$.fn.yiiGridView.update(\'rinciantagihanpasienpenunjang-v-grid\', {data: $(\'#search\').serialize()});}'
        ),
    ));
?>
<iframe name='iframePembayaran' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>