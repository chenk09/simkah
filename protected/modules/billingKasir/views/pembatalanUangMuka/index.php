<?php
    $this->breadcrumbs = array(
        'Pembatalan Uang Muka',
    );
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php

    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'id'=>'pembayaran-form',
            'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'focus'=>'#BKTandabuktibayarUangMukaT_jmlpembayaran',
            'htmlOptions'=>array(
                'onKeyPress'=>'return disableKeyPress(event);',
            ),
        )
    );
    
    $this->widget('application.extensions.moneymask.MMask',
        array(
            'element'=>'.currency',
            'currency'=>'PHP',
            'config'=>array(
                'symbol'=>'Rp ',
                'defaultZero'=>true,
                'allowZero'=>true,
                'precision'=>0,
                'thousands'=>',',
                'decimal'=>'.',
            )
        )
    );
    
?>

<!--<legend class="rim2">Transaksi Pembatalan Uang Muka</legend>-->

<?php
    $this->renderPartial(
        '_ringkasDataPasien',
        array(
            'modPendaftaran'=>$modPendaftaran,
            'modPasien'=>$modPasien
        )
    );
?>

<?php //echo $form->errorSummary(array($modBatal,$modBuktiKeluar)); ?>

<fieldset>
    <legend class="rim">Data Pembatalan</legend>
    <table>
        <tr>
            <td width="50%">
                <?php 
                    echo CHtml::activeHiddenField($modBatal,'bayaruangmuka_id',
                        array(
                            'readonly'=>true,
                            'class'=>'span3',
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'value'=>$_GET['idBayarUangMuka']
                        )
                    ); 
                ?>
                <?php 
                    echo CHtml::activeHiddenField($modBatal,'tandabuktibayar_id',
                        array(
                            'readonly'=>true,
                            'class'=>'span3',
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php //echo $form->textFieldRow($modBatal,'tglpembatalan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php $modBatal->tglpembatalan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBatal->tglpembatalan, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php 
                        echo $form->labelEx(
                            $modBatal,'tglpembatalan', 
                            array(
                                'class'=>'control-label'
                            )
                        );
                    ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',
                                    array(
                                        'model'=>$modBatal,
                                        'attribute'=>'tglpembatalan',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array(
                                            'class'=>'dtPicker2-5', 
                                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                                    )
                                );
                        ?>
                    </div>
                </div>
                <?php
                    echo $form->textFieldRow($modBuktiKeluar,'jmlkaskeluar',
                        array(
                            'class'=>'span3 currency req', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                    echo $form->hiddenField($modBuktiKeluar,'tandabuktikeluar_id',array());
                ?>
                <?php 
                    echo $form->textFieldRow($modBuktiKeluar,'biayaadministrasi',
                        array(
                            'class'=>'span3 currency', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textAreaRow($modBatal,'keterangan_batal',
                        array(
                            'class'=>'span3 req', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
            </td>
            <td width="50%">
                <?php // echo $form->dropDownListRow($modBuktiKeluar,'tahun', Params::tahun(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
                <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php
                    echo $form->textFieldRow($modBuktiKeluar,'nokaskeluar',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>50,
                            'readonly'=>true
                        )
                    );
                ?>
                <?php
                    echo $form->dropDownListRow($modBuktiKeluar,'carabayarkeluar', 
                        CaraBayarKeluar::items(),
                        array(
                            'onchange'=>'formCarabayar(this.value)',
                            'class'=>'span3',
                            'onkeypress'=>"return $(this).focusNextInputField(event);",
                            'maxlength'=>50
                        )
                    );
                ?>
                <div id="divCaraBayarTransfer" class="hide">
                    <?php
                        echo $form->textFieldRow($modBuktiKeluar,'melalubank',
                            array(
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        );
                    ?>
                    <?php 
                        echo $form->textFieldRow($modBuktiKeluar,'denganrekening',
                            array(
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        ); 
                    ?>
                    <?php 
                        echo $form->textFieldRow($modBuktiKeluar,'atasnamarekening',
                            array(
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        );
                    ?>
                </div>
                <?php
                    echo $form->textFieldRow($modBuktiKeluar,'namapenerima',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>100
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textAreaRow($modBuktiKeluar,'alamatpenerima',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textFieldRow($modBuktiKeluar,'untukpembayaran',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>100
                        )
                    ); 
                ?>
            </td>
        </tr>
    </table>
</fieldset>

    <div class="form-actions">
            <?php 
            if($modBuktiKeluar->isNewRecord)
            {
                echo CHtml::htmlButton(
                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                    array(
                        'class'=>'btn btn-primary',
                        'type'=>'submit',
                        'onKeypress'=>'return formSubmit(this,event)',
                        'onClick'=>'onClickSubmit();return false;',
                    )
                );
                
                echo "&nbsp;&nbsp;";
                echo CHtml::link(
                    Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                    '#',
                    array(
                        'class'=>'btn btn-info',
                        'onclick'=>"return false",
                        'disabled'=>true
                    )
                );
                
                echo "&nbsp;&nbsp;";
                echo CHtml::link(
                    Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                    $this->createUrl('pembatalanUangMuka/'), 
                    array(
                        'disabled'=>(isset($is_dialog) ? true : false),
                        'class'=>'btn btn-danger',
                        'onclick'=>(isset($is_dialog) ? "return false" : "return true"),
                    )
                );
            } else {
                echo CHtml::htmlButton(
                    Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                    array(
                        'class'=>'btn btn-primary',
                        'type'=>'submit',
                        'onKeypress'=>'return false',
                        'disabled'=>true
                    )
                );
                
                echo "&nbsp;&nbsp;";
                echo CHtml::link(
                    Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                    '#',
                    array(
                        'class'=>'btn btn-info',
                        'onclick'=>"printKasir($modBuktiKeluar->tandabuktikeluar_id);return false",
                        'disabled'=>false
                    )
                ); 
                
                echo "&nbsp;&nbsp;";
                echo CHtml::link(
                    Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    $this->createUrl('pembatalanUangMuka/'),
                    array(
                        'disabled'=>(isset($is_dialog) ? true : false),
                        'class'=>'btn btn-danger',
                        'onclick'=>(isset($is_dialog) ? "return false" : "return true"),
                    )
                );
            }
        ?>
			<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>  
<?php $this->endWidget(); ?>


<script type="text/javascript">

$('.currency').each(
    function(){
        this.value = formatUang(this.value)
    }
);

function simpanProses()
{
    $("#pembayaran-form").submit();
}

function onClickSubmit()
{
    if(cekValidasi())
    {
        confirmPembayaran();
    }
}

function formSubmit(obj,evt)
{
     evt = (evt) ? evt : event;
     var form_id = $(obj).closest('form').attr('id');
     var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
     
     if(charCode == 13)
     {
         if(cekValidasi())
         {
             confirmPembayaran();
         }
     }
     return false;
}

function cekValidasi()
{
    if($('#BKPasienM_no_rekam_medik').val() == "")
    {
        alert('Data pasien masih kosong');
        return false;
    }else{
        var index = 0;
        $("#pembayaran-form").find(".req").each(
            function()
            {
                if($(this).val() == 0 || $(this).val() < 0 || $(this).val() == '')
                {
                    index++;
                }
            }
        );
        if(index > 0)
        {
            alert('Isi yang bertada bintang');
            $("#pembayaran-form").find("input[name$='[biayaadministrasi]']").focus();
            return false;
        }else{
            $('.currency').each(function(){this.value = unformatNumber(this.value)});
            $('.number').each(function(){this.value = unformatNumber(this.value)});
            return true;
        }
    }
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    
    var no_pendaftaran = $('#pembayaran-form').find('input[name$="[no_pendaftaran]"]').val();
    var no_rekam_medik = $('#pembayaran-form').find('input[name$="[no_rekam_medik]"]').val();
    var instalasi_nama = $('#pembayaran-form').find('input[name$="[instalasi_nama]"]').val();
    var nama_pasien = $('#pembayaran-form').find('input[name$="[nama_pasien]"]').val();
    var ruangan_nama = $('#pembayaran-form').find('input[name$="[ruangan_nama]"]').val();
    
    var data_pasien = {
        'no_pendaftaran':no_pendaftaran,
        'no_rekam_medik':no_rekam_medik,
        'instalasi_nama':instalasi_nama,
        'nama_pasien':nama_pasien,
        'ruangan_nama':ruangan_nama,
    }
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]).css('font-weight','bold');
                }
            }
        }
    );
        
         
    var data_pembayaran = {
        'namapenerima':$('#pembayaran-form').find('input[name$="[namapenerima]"]').val(),
        'nokaskeluar':$('#pembayaran-form').find('input[name$="[nokaskeluar]"]').val(),
        'untukpembayaran':$('#pembayaran-form').find('input[name$="[untukpembayaran]"]').val(),
        'keterangan_batal':$('#pembayaran-form').find('textarea[name$="[keterangan_batal]"]').val(),
        'jmlkaskeluar':$('#pembayaran-form').find('input[name$="[jmlkaskeluar]"]').val(),
        'biayaadministrasi':$('#pembayaran-form').find('input[name$="[biayaadministrasi]"]').val()
    }
    $('#content_confirm').find('#info_pembayaran td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]).css('font-weight','bold');
                }
            }
        }
    );
}

/*
pembayaran-form
carapembayaran
*/


function formCarabayar(carabayar)
{
    //alert(carabayar);
    if(carabayar == 'TRANSFER'){
        $('#divCaraBayarTransfer').slideDown();
    } else {
        $('#divCaraBayarTransfer').slideUp();
    }
}

function printKasir(idTandaBukti)
{
    if(idTandaBukti != '')
    {
        window.open('<?php echo Yii::app()->createUrl('print/pembatalanUangMuka',array('caraPrint'=>'PDF','idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=640,height=480,scrollbars=1');
    }     
}

</script>


<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Pembatalan',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>400,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Pasien</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">No. Pendaftaran</td>
                    <td width="250" tag="no_pendaftaran"></td>
                    <td width="150">Nama</td>
                    <td tag="nama_pasien"></td>
                </tr>
                <tr>
                    <td>Instalasi</td>
                    <td tag="instalasi_nama"></td>
                    <td>No. Rekam Medis</td>
                    <td tag="no_rekam_medik"></td>
                </tr>
                <tr>
                    <td>Ruangan</td>
                    <td tag="ruangan_nama"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Pembatalan</legend>
            <table id="info_pembayaran" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Nama Penerima</td>
                    <td width="250" tag="namapenerima"></td>
                    <td width="150">No. Kas</td>
                    <td tag="nokaskeluar"></td>
                </tr>
                <tr>
                    <td>Untuk Pembayaran</td>
                    <td tag="untukpembayaran"></td>
                    <td>Keterangan</td>
                    <td tag="keterangan_batal"></td>
                </tr>
                <tr>
                    <td>Jumlah Kas</td>
                    <td tag="jmlkaskeluar"></td>
                    <td>Biaya Administrasi</td>
                    <td tag="biayaadministrasi">&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses();return false;'
                        
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");$("#pembayaran-form").find("input[name$=\'[biayaadministrasi]\']").focus();return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
