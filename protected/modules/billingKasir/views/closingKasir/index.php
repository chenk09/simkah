<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>

<fieldset>
    <legend class="rim2">Closing Kasir</legend>
    <div>
        <?php
            $this->renderPartial('_search',
                array(
                    'model'=>$model,
                    'mBuktBayar'=>$mBuktBayar,
                )
            );
        ?>
    </div>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'POST',
            'type' => 'horizontal',
            'id' => 'formClosing',
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data',
                'onKeyPress' => 'return disableKeyPress(event)',
                'onsubmit'=>'return cekInputTindakan();'
            ),
        )
    );
?>
    <?php $this->widget('bootstrap.widgets.BootAlert'); ?>
    <fieldset>
        <legend class="rim">Informasi Pembayaran</legend>
        <div>
            <?php
                $this->renderPartial('_table',
                    array(
                        'mBuktBayar'=>$mBuktBayar,
                        'mBuktiKeluar'=>$mBuktiKeluar,
                        'form'=>$form,
                    )
                );
            ?>        
    </div>
    </fieldset>

    <fieldset>
        <legend class="rim">Data Penutupan</legend>
        <div>
            <?php
                $this->renderPartial('_formClosing',
                    array(
                        'model'=>$model,
                        'informasi'=>$informasi,
                        'rPenerimaanUmum'=>$rPenerimaanUmum,
                        'rPengeluaranUmum'=>$rPengeluaranUmum,
                        'rPecahanUang'=>$rPecahanUang,
                        'mSetorBank'=>$mSetorBank,
                        'form'=>$form,
                    )
                );
            ?>
        </div>
    </fieldset>
<?php
    $this->endWidget();
?>  
</fieldset>
