<?php

    $this->widget('application.extensions.moneymask.MMask', array(
        'element' => '.numbersOnly',
        'config' => array(
            'defaultZero' => true,
            'allowZero' => true,
            'decimal' => '.',
            'thousands' => '',
            'precision' =>0,
        )
    ));
    
    $this->widget('application.extensions.moneymask.MMask',array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    ));

?>

<table width="100%" id="tblClosing">
    <tr>
        <td>
            <?php echo $form->textFieldRow($model,'jmltransaksi',array('readOnly'=>true,'class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->hiddenField($model,'closingdari'); ?>
            <?php echo $form->hiddenField($model,'sampaidengan'); ?>
            <?php echo $form->hiddenField($model,'pegawai_id'); ?>
            <?php echo $form->hiddenField($model,'create_loginpemakai_id'); ?>
            <?php echo $form->hiddenField($model,'create_ruangan'); ?>
            <?php echo $form->hiddenField($model,'shift_id'); ?>
            <?php 
                // echo $model->isNewRecord;
            ?>
            <div class="control-group">
                <div class='control-label'>Tgl Tutup Kasir</div>
                <div class="controls">  
                    <?php
                        $this->widget('MyDateTimePicker',
                            array(
                                'model'=>$model,
                                'attribute'=>'tglclosingkasir',
                                'mode'=>'datetime',
                                'options'=>array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                ),
                                'htmlOptions'=>array('readonly' => true,
                                'onkeypress'=>"return $(this).focusNextInputField(event)"),
                            )
                        );
                    ?>
                </div>
            </div>
            <?php echo $form->textFieldRow($model,'closingsaldoawal',array('class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($model,'terimauangpelayanan',array('readOnly'=>true,'class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($model,'terimauangmuka',array('class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)",'onkeyup'=>'hitungPiutang(this)')); ?>            
<!--            <div class="control-group">
                <div class='control-label'>Jum Penerimaan Umum</div>
                <div class="controls">
                    <?php
//                        echo(CHtml::textField("jum_penerimaan_umum", $informasi['total_penerimaan_umum'], array('readOnly'=>true,'size'=>20, 'class'=>'span3 currency'))); 
//                        echo CHtml::htmlButton('List',
//                            array(
//                                'onclick' => 'listPenerimaanUmum()',
//                                'class' => 'btn btn-primary',
//                                'rel' => "tooltip",
//                                'id' => 'penerimaanUmum',
//                                'title' => "Klik Untuk Melihat Penerimaan Umum"
//                            )
//                        );
                    ?>
                    <?php
//                        echo CHtml::checkBox('isPenerimaanUmum',false,
//                            array(
//                                'rel' => "tooltip",
//                                'id' => 'isPenerimaanUmum',
//                                'title' => "Check Untuk Tidak Menyimpan Penerimaan Umum"
//                            )
//                        );
                    ?>                    
                </div>
            </div>-->
<!--            <div class="control-group">
                <div class='control-label'>
                    Jum Pengeluaran Umum
                </div>
                <div class="controls">
                    <?php
//                        echo(CHtml::textField("jum_pengeluaran_umum", $informasi['total_pengeluaran_umum'], array('readOnly'=>true,'size'=>20, 'class'=>'span3 currency'))); 
//                        echo CHtml::htmlButton('List',
//                            array(
//                                'onclick' => 'listPengeluaranUmum()',
//                                'class' => 'btn btn-primary',
//                                'rel' => "tooltip",
//                                'id' => 'pengeluaranUmum',
//                                'title' => "Klik Untuk Melihat Pengeluaran Umum"
//                            )
//                        );
                    ?>
                    <?php
//                        echo CHtml::checkBox('isPengeluaranUmum',false,
//                            array(
//                                'rel' => "tooltip",
//                                'id' => 'isPengeluaranUmum',
//                                'title' => "Check Untuk Tidak Menyimpan Pengeluaran Umum"
//                            )
//                        );
                    ?>
                </div>
            </div>-->
            <div class="control-group">
                <div class='control-label'>Jum Penerimaan Tunai</div>
                <div class="controls">
                    <?php
                        echo(CHtml::textField("jum_penerimaan_tunai", 0, array('readOnly'=>true, 'size'=>20, 'class'=>'span3 currency'))); 
                    ?>
                </div>
            </div>
            <div class="control-group">
                <div class='control-label'>Total Tutup Kasir </div>
                <div class="controls">
                    <?php
                        echo $form->textField($model,'nilaiclosingtrans',array('readOnly'=>true,'class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)"));
                    ?>
                    <div style="margin-top:5px;font-size:11px;color:red;width:200px;padding:5px;border:1px solid;">Total Tutup Kasir = Total Penerimaan Tunai + Total Administrasi</div>
                </div>
            </div>
            <?php
//                echo $form->textFieldRow($model,'nilaiclosingtrans',array('readOnly'=>true,'class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)"));
            ?>
            <?php echo $form->textFieldRow($model,'totalsetoran',array('class'=>'span3 currency','onKeyup'=>"isinilai();",'onkeypress'=>"return $(this).focusNextInputField(event)"));?>            
            <div class="control-group ">
                <?php echo CHtml::label('Langsung Setor Ke bank','setorBank', array('class'=>'control-label inline')) ?> 
                <div class="controls">
                    <?php echo CHtml::checkBox('setorBank',false,array('onchange'=>"setorBankEnable(this);")) ?>
                    <i class="icon-chevron-down"></i>
                </div>
            </div>
            <div id="setor_bank" style="display:none">
                <?php $this->renderPartial('_formSetorBank', array('form'=>$form, 'modSetor'=>$mSetorBank)); ?>
            </div>
            <?php echo $form->hiddenField($model,'piutang',array('class'=>'span3 currency','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textAreaRow($model,'keterangan_closing',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->hiddenField($model,'jmluanglogam', array('value'=>0)); ?>
            <?php echo $form->hiddenField($model,'jmluangkertas', array('value'=>0)); ?>
            
            
        </td>
        <td>
            <?php
                foreach($rPecahanUang as $value)
                {
            ?>
                <div class="control-group">
                    <div class='control-label'><?php echo $value['lookup_name'];?></div>
                    <div class="controls">
                        <?php
                            echo(CHtml::textField("jum_recehan[". $value['lookup_value'] ."]", 0, array('onkeypress'=>"return $(this).focusNextInputField(event)", 'onKeyup'=>'hitungRecehan()', 'is_receh'=>($value['lookup_value'] < 500 ? 1 : 0), 'recehan_val'=>$value['lookup_value'],'size'=>20, 'class'=>'span3 numbersOnly recehan'))); 
                            echo(CHtml::hiddenField("val_recehan[". $value['lookup_value'] ."]", $value['lookup_value'], array('size'=>20, 'class'=>'span3 numbersOnly recehan'))); 
                        ?>
                    </div>
                </div>
            <?php
                }
            ?>
            <div class="control-group">
                <div class='control-label'>Total Recehan</div>
                <div class="controls">
                    <?php
                        echo(CHtml::textField("total_recehan", 0, array('onkeypress'=>"return $(this).focusNextInputField(event)",'size'=>20, 'class'=>'span3 numbersOnly'))); 
                    ?>
                </div>
            </div>            
        </td>
    </tr>
</table>
<?php
//FORM REKENING
$this->renderPartial('billingKasir.views.closingKasir.rekening._formRekening',
    array(
        'form'=>$form,
        'modRekenings'=>$modRekenings,
    )
);
?>


<div class="form-actions">
<?php
    if($model->isNewRecord){
        echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event);')); 
        echo "&nbsp;&nbsp;";
    }else{
        echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event);', 'disabled'=>true)); 
        echo "&nbsp;&nbsp;";
        echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"print('PRINT','$model->closingkasir_id');return false",'disabled'=>FALSE)); 
    }
    echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), $this->createUrl('ClosingKasir/Index'), array('class'=>'btn btn-danger','onclick'=>'return true;'));

?>
</div>
<?php
    $this->endWidget();
?>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',
    array(
    'id' => 'dialogPenerimaanUmum',
    'options' => array(
            'title' => 'List Penerimaan Umum',
            'autoOpen' => false,
            'modal' => true,
            'width' => 700,
            'height' => 400,
            'resizable' => false,
        ),
    )
);
?>
<table id="tblDialogUmum" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No</th>
            <th>Tgl. Penerimaan</th>
            <th>No. Kas Bayar</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
<?php
if(count($rPenerimaanUmum) > 0)
{
    $no=1;
    foreach($rPenerimaanUmum as $value)
    {
?>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $value['tglpenerimaan'];?></td>
            <td><?php echo $value['nopenerimaan'];?></td>
            <td><?php echo $value['totalharga'];?></td>
        </tr>
<?php
        $no++;
    }
}else{
    ?>
        <tr><td colspan="5">Data Tidak Ditemukan</td></tr>
    <?php
}
?>
    </tbody>
</table>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog',
    array(
    'id' => 'dialogPengeluaranUmum',
    'options' => array(
            'title' => 'List Pengeluaran Umum',
            'autoOpen' => false,
            'modal' => true,
            'width' => 700,
            'height' => 400,
            'resizable' => false,
        ),
    )
);
?>
<table id="tblDialogUmum" class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No</th>
            <th>Tgl. Penerimaan</th>
            <th>No. Kas Bayar</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
<?php
if(count($rPengeluaranUmum) > 0)
{
    $no=1;
    foreach($rPengeluaranUmum as $value)
    {
?>
        <tr>
            <td><?php echo $no;?></td>
            <td><?php echo $value['tglpengeluaran'];?></td>
            <td><?php echo $value['nopengeluaran'];?></td>
            <td><?php echo $value['totalharga'];?></td>
        </tr>
<?php
        $no++;
    }
}else{
    ?>
        <tr><td colspan="5">Data Tidak Ditemukan</td></tr>
    <?php
}
?>
    </tbody>
</table>
<?php
$this->endWidget();
?>

<?php
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
    $closingkasir_id = $model->closingkasir_id;
$js = <<< JSCRIPT
function listPenerimaanUmum()
{
    $("#dialogPenerimaanUmum").dialog("open");
}
function listPengeluaranUmum()
{
    $("#dialogPengeluaranUmum").dialog("open");
}

function print(caraPrint,closingkasir_id)
{
    window.open("${urlPrint}/"+"&caraPrint="+caraPrint+"&closingkasir="+closingkasir_id,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('dialog',$js,CClientScript::POS_HEAD);
?>
<script>
    function setTanggalClosing(){
        var tglAwal = $('#BKTandabuktibayarT_tglAwal').val();
        var tglAkhir = $('#BKTandabuktibayarT_tglAkhir').val();
        $("#BKClosingkasirT_closingdari").val(tglAwal);
        $("#BKClosingkasirT_sampaidengan").val(tglAkhir);
    }
    setTanggalClosing();
    setJurnal(false,5);

    function setJurnal(setorbank,jenispenerimaan_id){       
        var nilai = $('#BKClosingkasirT_totalsetoran').val();
        setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
            getDataRekeningClosingKasir(formatDesimal(nilai),jenispenerimaan_id);
        },500);
    }

    function isinilai(){
        var nilai = $('#BKClosingkasirT_totalsetoran').val();
        $('#JenispenerimaanrekeningV_0_saldodebit').val(nilai);
        $('#JenispenerimaanrekeningV_1_saldokredit').val(nilai);
    }
</script>