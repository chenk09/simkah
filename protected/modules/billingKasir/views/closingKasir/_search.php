<div class="search-form" style="">
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'POST',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'
            ),
        )
    );
?>
    <fieldset>
        <legend class="rim">Filter Pencarian</legend>
    <table widht="100%">
        <tr>
            <td>
                <div class="control-group">
                    <div class = 'control-label'>Tanggal Bukti Bayar</div>
                    <div class="controls">  
                        <?php
                            $this->widget('MyDateTimePicker',
                                array(
                                    'model'=>$mBuktBayar,
                                    'attribute'=>'tglAwal',
                                    'mode'=>'datetime',
                                    
                                    'options'=>array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array('readonly' => true,
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",
                                    'onchange'=>'setTanggalClosing()'),
                                    
                                    )
                            );
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class = 'control-label'>Sampai Dengan</div>
                    <div class="controls">  
                        <?php
                            $this->widget('MyDateTimePicker',
                                array(
                                    'model'=>$mBuktBayar,
                                    'attribute'=>'tglAkhir',
                                    'mode'=>'datetime',
                                    'options'=>array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array('readonly' => true,
                                    'onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )
                            );
                        ?>
                    </div>
                </div>
            </td>
            <td>
                <?php
                    echo $form->dropDownListRow($mBuktBayar, 'ruangan_id',
                        CHtml::listData(
                            RuanganM::model()->findAll('ruangan_id IN (28)'), 'ruangan_id', 'ruangan_nama'
                        ),
                        array(
                            'inline'=>true,
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                ?>
                <?php
                    echo $form->dropDownListRow($mBuktBayar, 'shift_id',
                        CHtml::listData(ShiftM::model()->findAll(), 'shift_id', 'shift_nama'),
                        array(
                            'inline'=>true,
                            'empty'=>'-- Pilih --',
                            'onkeypress'=>"return $(this).focusNextInputField(event)"
                        )
                    );
                ?>
                <?php
                    $query = "
                        SELECT loginpemakai_k.* , pegawai_m.nama_pegawai
                        FROM loginpemakai_k 
                        
                       JOIN pegawai_m ON loginpemakai_k.pegawai_id = pegawai_m.pegawai_id
                        WHERE loginpemakai_k.ruanganaktifitas = '28'
                    ";
                    $pegawai = Yii::app()->db->createCommand($query)->queryAll();
                   echo $form->dropDownListRow($mBuktBayar, 'create_loginpemakai_id',
                       CHtml::listData($pegawai, 'loginpemakai_id', 'nama_pegawai'),
                       array(
                           'inline'=>true,
                           'empty'=>'-- Pilih --',
                           'onkeypress'=>"return $(this).focusNextInputField(event)"
                       )
                   );
                    // echo $form->dropDownListRow($mBuktBayar, 'create_loginpemakai_id',
                    //     CHtml::listData(LoginpemakaiK::model()->findAll(), 'loginpemakai_id', 'nama_pemakai'),
                    //     array(
                    //         'inline'=>true,
                    //         'empty'=>'-- Pilih --',
                    //         'onkeypress'=>"return $(this).focusNextInputField(event)"
                    //     )
                    // );
                ?>
            </td>
        </tr>
    </table>
    </fieldset>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onClick'=>'onReset()')); ?>
    </div>
    
<?php
    $this->endWidget();
?>
</div>
<script>
    function onReset(){
        window.open("<?php echo Yii::app()->createUrl($this->route); ?>","_self");
    }
</script>