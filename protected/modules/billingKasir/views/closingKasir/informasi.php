<?php
$this->breadcrumbs=array(
	'Informasi Closing Kasir',
);

Yii::app()->clientScript->registerScript('search', "
$('#divSearch-form form').submit(function(){
	$.fn.yiiGridView.update('informasiclosingkasir-m-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Closing Kasir</legend>
<div id="divSearch-form">
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'informasiclosingkasir-t-search',
        'type'=>'horizontal',
)); ?>
    <?php $this->widget('ext.bootstrap.widgets.HeaderGroupGridView',array(
	'id'=>'informasiclosingkasir-m-grid',
	'dataProvider'=>$model->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Penerimaan</center>',
                'start'=>5, 
                'end'=>6, 
            ),
        ),
	'columns'=>array(
                    array(
                        'name'=>'tglclosingkasir',
                        'header'=>'Tgl. Closing Kasir <br>/ Tgl. Bukti Bayar',
                        'type'=>'raw',
                        'value'=>'$data->tglclosingkasir." <br>/".$data->getBuktibayar(\'tglbuktibayar\')',
                    ),
                    array(
                        'name'=>'closingdari',
                        'header'=>'Closing Dari <br> Sampai Dengan',
                        'type'=>'raw',
                        'value'=>'$data->closingdari." <br> ".$data->sampaidengan',
                    ),
                    'nama_pegawai',
                    'shift_nama',
                    array(
                        'name'=>'closingsaldoawal',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->closingsaldoawal)',
                    ),
                    array(
                        'name'=>'terimauangmuka',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->terimauangmuka)',
                    ),
                    array(
                        'name'=>'terimauangpelayanan',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->terimauangpelayanan)',
                    ),
                    array(
                        'name'=>'nilaiclosingtrans',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->nilaiclosingtrans)',
                    ),
                    array(
                        'header'=>'Setor Ke Bank',
                        'type'=>'raw',
                        'value'=>'empty($data->setorbank_id) ? "<center>-</center>" 
                            : $data->tgldisetor." ".CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("ClosingKasir/RincianSetoran",array("idSetor"=>$data->setorbank_id)),
                                                        array("class"=>"", 
                                                              "target"=>"iframeRincianSetoran",
                                                              "onclick"=>"$(\"#dialogRincianSetoran\").dialog(\"open\");",
                                                              "rel"=>"tooltip",
                                                              "title"=>"Klik untuk melihat Rincian Setoran",
                                                        ))',
                        'htmlOptions'=>array('style'=>'text-align: center;'),
                    ),
                    array(
                        'header'=>'Rincian Closing',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("ClosingKasir/Rincian",array("idClosing"=>$data->closingkasir_id)),
                                        array("class"=>"", 
                                              "target"=>"iframeRincianClosing",
                                              "onclick"=>"$(\"#dialogRincianClosing\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk melihat Rincian Closing",
                                        ))',          
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    array(
                        'header'=>'Batal Closing',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-remove\"></i>",Yii::app()->controller->createUrl("ClosingKasir/Batalclosing",array("idClosing"=>$data->closingkasir_id)),
                                        array("class"=>"", 
                                              "target"=>"iframeBatalClosing",
                                              "onclick"=>"$(\"#dialogBatalClosing\").dialog(\"open\");",
                                              "rel"=>"tooltip",
                                              "title"=>"Klik untuk membatalkan Closing Kasir",
                                        ))',          
                        'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<table>
    <tr>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label('Tgl Pembebasan',  CHtml::activeId($model, 'tgl_awal'), array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $model->tgl_awal = $format->formatDateINA($model->tgl_awal);
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_awal',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'dd M yy',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div>
            <div class="control-group ">
                <?php echo CHtml::label('Sampai Dengan',CHtml::activeId($model, 'tgl_akhir'), array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $model->tgl_akhir = $format->formatDateINA($model->tgl_akhir);
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tgl_akhir',
                                                'mode'=>'date',
                                                'options'=> array(
                                                    'dateFormat'=>'dd M yy',
                                                ),
                                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                    </div>
            </div> 
            
        </td>
        <td>
            <?php echo $form->textFieldRow($model,'nama_pegawai',array('class'=>'span3')); ?>
            <?php echo $form->dropDownlistRow($model,'shift_id',Chtml::listData($model->ShiftItems, 'shift_id', 'shift_nama'),array('empty'=>'-- Pilih --','class'=>'span3')); ?>
        </td>
    </tr>
</table>
<div class="form-actions">
     <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
	 <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onclick'=>'resetForm();')); ?>
	 			<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
</div>
<?php $this->endWidget(); ?>
    
</div>
</fieldset>
<script>
function resetForm(){
    window.open("<?php echo $this->createUrl("/".$this->route); ?>", "_self");
}
</script>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianSetoran',
    'options'=>array(
        'title'=>'Rincian Setoran Ke Bank',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>480,
        'minHeight'=>360,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianSetoran" width="100%" height="320" >
</iframe>
<?php
$this->endWidget();
?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianClosing',
    'options'=>array(
        'title'=>'Rincian Closing Kasir',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>480,
        'minHeight'=>360,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianClosing" width="100%" height="320" >
</iframe>
<?php
$this->endWidget();
?>
<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogBatalClosing',
    'options'=>array(
        'title'=>'Batal Closing',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>480,
        'minHeight'=>300,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeBatalClosing" width="100%" height="256" >
</iframe>
<?php
$this->endWidget();
?>
    