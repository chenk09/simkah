<?php
	echo $this->renderPartial('application.views.headerReport.headerDefault',array('judulLaporan'=>$judulLaporan, 'colspan'=>10));      
?>

<table width="100%">
	<tr>
		<td align="center" colspan="4">Periode Closing Kasir : <?php echo $model->tglclosingkasir; ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">No. Closing Kasir</td>
		<td>: <?php echo $model->closingkasir_id; ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Tgl. Closing Kasir</td>
		<td>: <?php echo $model->tglclosingkasir; ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Shift</td>
		<td>: <?php echo $model->shift->shift_nama; ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Nama Pegawai Penyetor</td>
		<td>: <?php echo $model->pegawai->nama_pegawai; ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Saldo Awal</td>
		<td>: <?php echo number_format($model->closingsaldoawal); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Terima Uang Muka</td>
		<td>: <?php echo number_format($model->terimauangmuka); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Terima Uang Pelayanan</td>
		<td>: <?php echo number_format($model->terimauangpelayanan); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Total Pengeluaran</td>
		<td>: <?php echo number_format($model->totalpengeluaran); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Piutang</td>
		<td>: <?php echo number_format($model->piutang); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Total Nominal Closing Kasir</td>
		<td>: <?php echo number_format($model->nilaiclosingtrans); ?></td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%">Total Setoran Ke Bank</td>
		<td>: <?php echo number_format($model->totalsetoran); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%"></td>
		<td></td>
		<td><?php echo Params::KOTA_KOPSURAT.", ".date('d-m-Y'); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td width="25%"></td>
		<td width="20%"></td>
		<td></td>
		<td><?php echo strtoupper($model->pegawai->nama_pegawai); ?></td>
	</tr>
</table>