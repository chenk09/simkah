<?php
	$no = 0;
	$no_pendaftaran_temp = null;
	foreach($dataDetails as $key=>$val)
	{
		$model = new BKPembjasadetailT;
		foreach($val as $x=>$z)
		{
			if($model->hasAttribute($x)) 
			{
				$model->$x = $z;
			}
		}
		$model->jumahtarif = $val->tarif_tindakan;
		$model->jumlahjasa = $val->totaltarif_komponensatuan;
		$model->jumlahbayar = $val->totaltarif_komponensatuan;		
		$model->pilihDetail = true;
?>
	<tr>
<?php
	if($val->no_pendaftaran != $no_pendaftaran_temp)
	{
?>
		<td><?=$no+1?></td>
		<td><?=$val->no_pendaftaran?><br>/<?=$val->no_rekam_medik?></td>
		<td><center>-</center></td>
		<td><?=$val->nama_pasien?></td>
		<td><?=$val->alamat_pasien?></td>
<?php
		$no++;
	}else{
?>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
<?php
	}
?>
	<td><?=$val->daftartindakan_nama?></td>
	<td><?=$val->penjamin_nama?></td>
	<td>
		<?=CHtml::activeTextField($model, '['. $key .']jumahtarif',array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))?>
		<?=CHtml::activeHiddenField($model, '['. $key .']pendaftaran_id')?>
		<?=CHtml::activeHiddenField($model, '['. $key .']pasien_id')?>
		<?=CHtml::activeHiddenField($model, '['. $key .']pasienmasukpenunjang_id')?>
		<?=CHtml::activeHiddenField($model, '['. $key .']pasienadmisi_id')?>
		<?=CHtml::activeHiddenField($model, '['. $key .']daftartindakan_id')?>
		<?=CHtml::activeHiddenField($model, '['. $key .']tindakanpelayanan_id')?>
	</td>
	<td><?=CHtml::activeTextField($model, '['. $key .']jumlahjasa',array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
	<td><?=CHtml::activeTextField($model, '['. $key .']jumlahbayar',array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
	<td><?=CHtml::activeTextField($model, '['. $key .']sisajasa',array('readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))?></td>
	<td><?=CHtml::activeCheckBox($model, '['. $key .']pilihDetail',array('onclick'=>'checkIni(this);'))?></td>
	</tr>
<?php
		$no_pendaftaran_temp = $val->no_pendaftaran;
	}
?>