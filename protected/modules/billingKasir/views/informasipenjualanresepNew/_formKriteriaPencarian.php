<fieldset>
    <legend class="rim">Pencarian Penjualan</legend>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Tgl Penjualan','tglPenjualan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array(
                                                    'readonly'=>true, 
                                                    'class'=>'dtPicker3', 
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array(
                                                    'readonly'=>true, 
                                                    'class'=>'dtPicker3', 
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                
            </td>
            <td>
                <?php //echo $form->textFieldRow($model,'noresep',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <div class="control-group ">
                    <label for="BKPenjualanresepT_noresep" class="control-label">No Resep / Struk</label>
                    <div class="controls">
                        <?php echo CHtml::activeTextField($model, 'noresep', array('class'=>'span3')); ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset>