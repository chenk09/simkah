<table>
    <tr>
        <td colspan="3">
            <?php echo $this->renderPartial('application.views.headerReport.headerDefault'); ?>
        </td>
    </tr>
    <tr>
        <td align="center" valig="middle" colspan="3">
            <TABLE FRAME=VOID ALIGN=LEFT CELLSPACING=0 COLS=11 RULES=NONE BORDER=0>
                <TBODY>
                        <TR>
                                <TD COLSPAN=10 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE><B><?php echo strtoupper($judulLaporan); ?></B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=10 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE><B>NOMOR BUKTI <?php echo $model->nobuktibayar;?></B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><B>No. Seri   :  </B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD  COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT HEIGHT=17>Bendahara Penerimaan/Bendahara Penerimaan Pembantu Telah menerima uang sebesar  Rp <?php echo number_format($modPenjualan->totalhargajual + ($modPenjualan->biayaadministrasi + $modPenjualan->biayakonseling + $modPenjualan->totaltarifservice),0,'','.');?></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Dengan Huruf (<?php echo $this->terbilang($model->uangditerima);?>)</TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Dari Nama                   :    <?php echo $model->darinama_bkm;?></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>    Alamat                        :    <?php echo $model->alamat_bkm;?></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Sebagai Pembayaran   :    <?php echo $model->sebagaipembayaran_bkm;?></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>

                        <TR>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>

                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000" COLSPAN=2 ALIGN=CENTER VALIGN=MIDDLE><B>Nama Obat Alkes</B></TD>
                                <TD  STYLE="border: 1px solid #000000" COLSPAN=1 ALIGN=CENTER VALIGN=MIDDLE><B>Qty</B></TD>
                                <TD  STYLE="border: 1px solid #000000" COLSPAN=1 ALIGN=CENTER VALIGN=MIDDLE><B>Harga Satuan</B></TD>
                                <TD  STYLE="border: 1px solid #000000" COLSPAN=1 ALIGN=CENTER VALIGN=MIDDLE><B>Jumlah</B></TD>
                        </TR> 
                         <?php
                                $total=0;
                                foreach($rincianTagihan as $i=>$rincian):

//                                $jumlah=$rincian->iurbiaya;
                                    $jumlah = $rincian->qty_oa * $rincian->hargasatuan_oa;
                                 $total=$total+$jumlah;
                        ?>
                        <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000" COLSPAN=2 ALIGN=CENTER><?php echo $rincian->obatalkes->obatalkes_nama; ?></TD>
                                <TD  STYLE="border: 1px solid #000000" COLSPAN=1 ALIGN=CENTER VALIGN=MIDDLE><?php echo $rincian->qty_oa; ?></TD>
                                <TD  STYLE="border: 1px solid #000000" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><?php echo number_format($rincian->hargasatuan_oa,0,'','.'); ?></TD>
                                <TD  STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><?php echo number_format($jumlah,0,"","."); ?></TD>
                        </TR>
                        <?php 
                             endforeach;
                        ?> 
                        <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=4 ALIGN=RIGHT><B>Biaya Administrasi</B></TD>
                                <TD  STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><B><?php echo number_format($modPenjualan->biayaadministrasi,0,"",".") ?></B></TD>
                        </TR>
                        <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=4 ALIGN=RIGHT><B>Biaya Konselling</B></TD>
                                <TD  STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><B><?php echo number_format($modPenjualan->biayakonseling,0,"",".") ?></B></TD>
                        </TR>
                        <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=4 ALIGN=RIGHT><B>Total Tarif Service</B></TD>
                                <TD  STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><B><?php echo number_format($modPenjualan->totaltarifservice,0,"",".") ?></B></TD>
                        </TR>
                        <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=4 ALIGN=RIGHT><B>Total Pembayaran</B></TD>
                                <TD  STYLE="border: 1px solid #000000; padding-right:4px;" COLSPAN=1 ALIGN=RIGHT VALIGN=MIDDLE><B><?php echo number_format($total + ($modPenjualan->biayaadministrasi + $modPenjualan->biayakonseling + $modPenjualan->totaltarifservice),0,"",".") ?></B></TD>
                        </TR>
        <!--                <TR >
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" COLSPAN=2 ALIGN=CENTER>Total Dari TandabuktipembayaranT</TD>
                                <TD  STYLE="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" COLSPAN=1 ALIGN=CENTER VALIGN=MIDDLE><?php //echo $model->jumlahpembayaran ?></TD>
                        </TR>-->


                        <TR>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                            <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Tanggal Diterima Uang   :   <?php echo $model->tglbuktibayar;?></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=5 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE><B>Mengetahui</B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD COLSPAN=3 ALIGN=CENTER VALIGN=MIDDLE><B>Pembayar / Penyetor</B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=5 HEIGHT=17 ALIGN=CENTER VALIGN=MIDDLE><B>Bendahara Penerimaan / Bendahara Penerimaan Pembantu</B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD STYLE="border-bottom: 1px solid #000000" ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><B>NIP. </B></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD HEIGHT=17 ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Lembar Asli       :  Untuk/Pembayaran/Penyetor/Pihak Ketiga</TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Salinan 1           :  Untuk Bendahara Penerimaan/Bendahara Penerimaan Pembantu</TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                        <TR>
                                <TD COLSPAN=7 VALIGN=MIDDLE HEIGHT=20 ALIGN=LEFT>Salinan 2           :  Arsip</TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                                <TD ALIGN=LEFT><BR></TD>
                        </TR>
                </TBODY>
        </TABLE>
        </td>
    </tr>
</table>