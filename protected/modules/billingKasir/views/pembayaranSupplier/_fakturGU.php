<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend>Data Faktur</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'tglterima',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[tglterima]', $modFakturBeli->tglterima, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'tgljatuhtempo',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[tgljatuhtempo]', $modFakturBeli->tgljatuhtempo, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'nopenerimaan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[nopenerimaan]', $modFakturBeli->nopenerimaan, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'nofaktur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPasienM[nofaktur]', $modFakturBeli->nofaktur, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'supplier_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[supplier_id]', $modFakturBeli->pembelianbarang->supplier->supplier_nama, array('readonly'=>true)); 
                echo CHtml::hiddenField('supplier_id', $modFakturBeli->pembelianbarang->supplier->supplier_id, array('readonly'=>true));
            ?></td>
            
            <td><?php echo CHtml::activeLabel($modFakturBeli, 'keterangan_persediaan',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textArea('FAPasienM[keterangan_persediaan]', $modFakturBeli->keterangan_persediaan, array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset> 
