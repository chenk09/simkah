<?php
$this->breadcrumbs=array(
	'Pembayaran Supplier',
);?>
<h2><?php //echo $this->id . '/' . $this->action->id; ?></h2>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>'.',
        'thousands'=>',',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
    'id'=>'bayarkesupplier-t-form',
    'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)',
                             'onsubmit'=>'return cekInputan();'),
)); ?>
<?php $this->renderPartial('_dataFakturBeli',array('modFakturBeli'=>$modFakturBeli,'modUangMuka'=>$modUangMuka)); ?>

<?php echo $form->errorSummary(array($modelBayar,$modBuktiKeluar,$modUangMuka)); ?>

<fieldset>
    <legend>Pembayaran Obat Alkes</legend>
    <table id="tblBayarOA" class="table table-condensed table-bordered">
        <thead>
            <tr>
                <th>Nama Obat Alkes</th>
                <th>Jml Terima</th>
                <th>Harga Netto</th>
                <th>Harga PPN</th>
                <th>Harga PPH</th>
                <th>% Discount</th>
                <th>Jml Discount</th>
                <th>Total Harga</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($modDetailBeli as $i => $detail) { ?>
            <tr>
                <td>
                    <?php echo $detail->obatalkes->obatalkes_nama;
                          echo CHtml::hiddenField('obatalkes_id',$detail->obatalkes_id,array());
                          echo CHtml::hiddenField('jmlterima',$detail->jmlterima,array());
                          echo CHtml::hiddenField('harganettofaktur',$detail->harganettofaktur,array());
                          echo CHtml::hiddenField('hargasatuan',$detail->hargasatuan,array());
                    ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->jmlterima); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->harganettofaktur); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->hargappnfaktur); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->hargapphfaktur); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->persendiscount); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->jmldiscount); ?>
                </td>
                <td>
                    <?php echo MyFunction::formatNumber($detail->hargasatuan*$detail->jmlterima); ?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</fieldset>
<?php
//FORM REKENING
    $this->renderPartial('billingKasir.views.pembayaranSupplier.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
?>
<table>
    <tr>
        <td width="50%">
            
        <?php //echo $form->textFieldRow($modelBayar,'uangmukabeli_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <?php echo $form->hiddenField($modelBayar,'fakturpembelian_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <?php //echo $form->textFieldRow($modelBayar,'tandabuktikeluar_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <?php //echo $form->textFieldRow($modelBayar,'batalbayarsupplier_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <?php //echo $form->textFieldRow($modelBayar,'tglbayarkesupplier',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <div class="control-group ">
            <?php $modelBayar->tglbayarkesupplier = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modelBayar->tglbayarkesupplier, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
            <?php echo $form->labelEx($modelBayar,'tglbayarkesupplier', array('class'=>'control-label inline')) ?>
            <div class="controls">
                <?php   
                        $this->widget('MyDateTimePicker',array(
                                        'model'=>$modelBayar,
                                        'attribute'=>'tglbayarkesupplier',
                                        'mode'=>'datetime',
                                        'options'=> array(
                                            'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                            'maxDate' => 'd',
                                        ),
                                        'htmlOptions'=>array('readonly'=>true, 'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                        ),
                )); ?>

            </div>
        </div>
        <?php echo $form->textFieldRow($modelBayar,'totaltagihan',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
        <?php //echo $form->textFieldRow($modUangMuka,'jumlahuang',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <div class="control-group ">
                <label class='control-label required'>Uang Muka <span class="required">*</span></label>
                <div class="controls">
                    <?php echo $form->textField($modUangMuka,'jumlahuang',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                </div>
            </div>
        <?php echo $form->textFieldRow($modelBayar,'jmldibayarkan',array('class'=>'inputFormTabel currency span3', 'onblur'=>'hitungKasKeluar();', 'onkeyup'=>'hitungKasKeluar()', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onfocus'=>'$(this).select();')); ?>
        
            <div class="control-group ">
                <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php echo $form->labelEx($modBuktiKeluar,'tglkaskeluar', array('class'=>'control-label inline')) ?>
                <div class="controls">
                    <?php   
                            $this->widget('MyDateTimePicker',array(
                                            'model'=>$modBuktiKeluar,
                                            'attribute'=>'tglkaskeluar',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('readonly'=>true, 'class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                    )); ?>

                </div>
            </div>
            <?php //echo $form->dropDownListRow($modBuktiKeluar,'tahun', Params::tahun(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
            <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
            <?php echo $form->textFieldRow($modBuktiKeluar,'nokaskeluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <?php echo $form->textFieldRow($modBuktiKeluar,'biayaadministrasi',array('onkeyup'=>'hitungKasKeluar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onfocus'=>'$(this).select();')); ?>
            <?php echo $form->textFieldRow($modBuktiKeluar,'jmlkaskeluar',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
        </td>
        <td width="50%">
            <?php echo $form->dropDownListRow($modBuktiKeluar,'carabayarkeluar', CaraBayarKeluar::items(),array('onchange'=>'formCarabayar(this.value)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
            <div id="divCaraBayarTransfer" class="hide">
                <?php echo $form->textFieldRow($modBuktiKeluar,'melalubank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modBuktiKeluar,'denganrekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textFieldRow($modBuktiKeluar,'atasnamarekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </div>

            <?php echo $form->textFieldRow($modBuktiKeluar,'namapenerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            <?php echo $form->textAreaRow($modBuktiKeluar,'alamatpenerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            <?php echo $form->textFieldRow($modBuktiKeluar,'untukpembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
        </td>
    </tr>
</table>    
        <div class="form-actions">
                <?php 
                    $disabled = ((isset($modFakturBeli->bayarkesupplier_id)) ? true : null);
                    echo CHtml::htmlButton(Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'disabled'=>$disabled)); 
                    //echo "&nbsp;&nbsp;";
                    //echo CHtml::link(Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKasir($modTandaBukti->tandabuktibayar_id);return false",'disabled'=>false)); 
                ?>
        </div>

<?php $this->endWidget(); ?>


<script type="text/javascript">
$('.currency').each(function(){this.value = formatUang(this.value)});
setJurnal();
function cekInputan()
{
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    return true;
}

function hitungKasKeluar()
{
    var jmlBayar = parseFloat(unformatNumber($('#BKBayarkeSupplierT_jmldibayarkan').val()));
    var biayaAdmin = parseFloat(unformatNumber($('#BKTandabuktikeluarT_biayaadministrasi').val()));
    var kasKeluar = jmlBayar + biayaAdmin;    
                    
    $('#BKTandabuktikeluarT_jmlkaskeluar').val(formatUang(kasKeluar));
}

function formCarabayar(carabayar)
{
    if(carabayar == 'TRANSFER'){
        $('#divCaraBayarTransfer').slideDown();
    } else {
        $('#divCaraBayarTransfer').slideUp();
    }
}
function setJurnal(){
   var idObat = $("#tblBayarOA tbody").parents().find('#obatalkes_id').val();
   var qty = $("#tblBayarOA tbody").parents().find('#jmlterima').val();
   var hargaBruto = '<?php echo $modFakturBeli->totalhargabruto; ?>';
   var hargaSatuan = $('#tblBayarOA tbody').parents().find('#hargasatuan').val();
   var supplier_id = $('#supplier_id').val();
   
   setTimeout(function(){//karna form rekening butuh waktu ketika ajax request nya
        getDataRekeningBayarKeSupplier(supplier_id, formatDesimal(hargaBruto));
    },500);
}
</script>