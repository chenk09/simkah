<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'GET',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'
            ),
        )
    );
?>
<div>
<fieldset>
    <table>
        <tr>   
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Tgl Pembayaran', 'tgl_pendaftaran',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'name' => 'Filter[tglAwal]',
                                                'model'=>$modPendaftaran,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
            //                                    'onchange'=>'ajaxGetList()',
                                                ),
                                )); 
                            ?>
                    </div>
                </div>
            </td>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Cara Bayar', 'cara bayar',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php 
                                echo CHtml::activeDropDownList($modPendaftaran,'carabayar_id', CHtml::listData($modPendaftaran->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,
                                            array('style'=>'width:120px;','empty'=>'-- Pilih --','onkeypress'=>"return $(this).focusNextInputField(event)",
                                                    'ajax' => array('type'=>'POST',
                                                        'url'=> Yii::app()->createUrl('ActionDynamic/GetPenjaminPasien',array('encode'=>false,'namaModel'=>'BKPendaftaranT')), 
                                                    'update'=>'#BKPendaftaranT_penjamin_id'  //selector to update
                                            ),
                                )); 
                            ?>
                    </div>
                </div>
                
            </td>
        </tr>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Sampai Dengan', 'sampai dengan',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'name' => 'Filter[tglAkhir]',
                                                'model'=>$modPendaftaran,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
            //                                    'onchange'=>'ajaxGetList()',
                                                ),
                                )); 
                            ?>
                    </div>
                </div>
            </td>
            
            <td>
                <div class="control-group">
                    <?php echo CHtml::label('Penjamin', 'penjamin',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php 
                                echo CHtml::activeDropDownList($modPendaftaran,'penjamin_id', CHtml::listData($modPendaftaran->getPenjaminItems($modPendaftaran->carabayar_id), 
                                        'penjamin_id', 'penjamin_nama') ,array('style'=>'width:120px;','empty'=>'-- Pilih --',
                                                    'onkeypress'=>"return $(this).focusNextInputField(event)",)); 
                            ?> 
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="control-group">
                    <?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?>
                    <div class="controls">
                            <?php
                                $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'noPendaftaran',
                                           'value'=>$modPendaftaran->no_pendaftaran,
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       ajaxGetList(ui.item.pasien_id);
                                                       return false;
                                                   }',
                                           ),
                                           'htmlOptions'=>array(
                                               'placeholder'=>'Pilih No Pendaftaran',
                                               'style'=>'width:160px;',
                                               'class'=>'span2',
                                               'onchange'=>'ajaxGetList()',
                                           ),
                                           'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                               )); 
                           ?>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</fieldset> 
<div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onClick'=>'ajaxGetList();')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onClick'=>'onReset()')); ?>
</div>
    
<?php
    $this->endWidget();
?>
<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>600,
        'resizable'=>false,
    ),
));
 $format = new CustomFormat();
$modPasien = new BKPendaftaranT('search');
$modPasien->unsetAttributes();
$modPasien->tgl_pendaftaran = date('Y-m-d');
if(isset($_GET['BKPendaftaranT'])) {
    $modPasien->attributes = $_GET['BKPendaftaranT'];
    $modPasien->tgl_pendaftaran =  isset($_GET['BKPendaftaranT']['tgl_pendaftaran']) ? $format->formatDateMediumForDB($_GET['BKPendaftaranT']['tgl_pendaftaran']) : date('Y-m-d');
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-t-grid',
	'dataProvider'=>$modPasien->searchPendaftaranPasienKlaim(),
	'filter'=>$modPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#noPendaftaran\").val(\"$data->no_pendaftaran\");
                                                  $(\"#BKPasienM_no_rekam_medik\").val('.$modPasien->pasien->no_rekam_medik.');
                                                  $(\"#namaPasien\").val('.$modPasien->pasien->nama_pasien.');
                                                  $(\"#Filter_pasien_id\").val(\"$data->pasien_id\");
                                                  $(\"#dialogPasien\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
//                array(
//                    'header'=>'Tgl Pendaftaran',
//                    'value'=>'$data->tgl_pendaftaran',
//                    'type'=>'raw',
//                ),
                array(
                        'header'=>'Tgl Pendaftaran',
                        'name'=>'tgl_pendaftaran',
                        'value'=>'$data->tgl_pendaftaran',
                        'filter'=>$this->widget('MyDateTimePicker',array(
                                        'model'=>$modPasien,
                                        'attribute'=>'tgl_pendaftaran',
                                        'mode'=>'date',
                                        'options'=> array(
                                                                'dateFormat'=>'yy-mm-dd',
                                                            ),
                                        'htmlOptions'=>array('readonly'=>false, 'class'=>'dtPicker3','id'=>'tgl_pendaftaran','placeholder'=>'23 Jan 1993'),
                                        ),true
                                    ),
                        'htmlOptions'=>array('width'=>'80','style'=>'text-align:center'),
                    ),
                array(
                    'header'=>'No Pendaftaran',
                    'name'=>'no_pendaftaran',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header'=>'No Rekam Medik',
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'name'=>'nama_pasien',
                    'value'=>'$data->pasien->nama_pasien',
                ),
                array(
                    'header'=>'Jenis Kelamin',
                    'name'=>'jeniskelamin',
                    'value'=>'$data->pasien->jeniskelamin',
                ),
                array(
                    'header'=>'Alamat Pasien',
                    'name'=>'alamat_pasien',
                    'value'=>'$data->pasien->alamat_pasien',
                ),
               
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});
            jQuery(\'#tgl_pendaftaran\').datepicker(jQuery.extend({
                        showMonthAfterYear:false}, 
                        jQuery.datepicker.regional[\'id\'], 
                       {\'dateFormat\':\'yy-mm-dd\',\'maxDate\':\'d\',\'timeText\':\'Waktu\',\'hourText\':\'Jam\',\'minuteText\':\'Menit\',
                       \'secondText\':\'Detik\',\'showSecond\':true,\'timeOnlyTitle\':\'Pilih Waktu\',\'timeFormat\':\'hh:mms\',
                       \'changeYear\':true,\'changeMonth\':true,\'showAnim\':\'fold\',\'yearRange\':\'-80y:+20y\'})); 
                jQuery(\'#tgl_pendaftaran_date\').on(\'click\', function(){jQuery(\'#tgl_pendaftaran\').datepicker(\'show\');});
            }',
));

$this->endWidget();
//========= end pencarian pasien dialog ====================================
?>
<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasienRekamMedik',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPasien = new BKPasienM('search');
$modPasien->unsetAttributes();
if(isset($_GET['BKPasienM'])) {
    $modPasien->attributes = $_GET['BKPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pasien-m-grid',
	'dataProvider'=>$modPasien->search(),
	'filter'=>$modPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#BKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                                  $(\"#namaPasien\").val(\"$data->nama_pasien\");
                                                  $(\"#Filter_pasien_id\").val(\"$data->pasien_id\");
                                                  $(\"#dialogPasienRekamMedik\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'No Rekam Medik',
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->no_rekam_medik',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'name'=>'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'header'=>'Alamat Pasien',
                    'name'=>'alamat_pasien',
                    'value'=>'$data->alamat_pasien',
                ),
               
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pencarian pasien dialog ====================================
?>
<?php 
//========= Dialog buat cari data pasien =========================
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasienNama',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>1000,
        'height'=>600,
        'resizable'=>false,
    ),
));

$modPasien = new BKPasienM('search');
$modPasien->unsetAttributes();
if(isset($_GET['BKPasienM'])) {
    $modPasien->attributes = $_GET['BKPasienM'];
}
$this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'pendaftaran-m-grid',
	'dataProvider'=>$modPasien->search(),
	'filter'=>$modPasien,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header'=>'Pilih',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","",array("class"=>"btn-small", 
                                    "href"=>"",
                                    "id" => "selectPasien",
                                    "onClick" => "$(\"#namaPasien\").val(\"$data->nama_pasien\");
                                                  $(\"#BKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                                  $(\"#Filter_pasien_id\").val(\"$data->pasien_id\");
                                                  $(\"#dialogPasienNama\").dialog(\"close\"); 
                                                  return false;
                                        "))',
                ),
                array(
                    'header'=>'No Rekam Medik',
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->no_rekam_medik',
                ),
                array(
                    'header'=>'Nama Pasien',
                    'name'=>'nama_pasien',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'header'=>'Alamat Pasien',
                    'name'=>'alamat_pasien',
                    'value'=>'$data->alamat_pasien',
                ),
               
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
));

$this->endWidget();
//========= end pencarian pasien dialog ====================================
?>