<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableLaporanKeseluruhan', {
            data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Keseluruhan</legend>
<div class="search-form">
    <?php
        $this->renderPartial('billingKasir.views.laporan.keseluruhan/_searchKeseluruhan',
            array(
                'model'=>$model,
            )
        ); 
    ?>
</div>

<fieldset> 
    <?php
        $this->renderPartial('billingKasir.views.laporan.keseluruhan/_tableKeseluruhan',
            array(
                'model'=>$model
            )
        );
    ?>
</fieldset>

<script type="text/javascript">
    function checkAll()
    {
        if($("#checkAllRuangan").is(':checked')){
            $("#ruangan").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#ruangan").find("input[type=\'checkbox\']").attr("checked", false);
        }        
        
    }
</script>
<?php

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanKeseluruhan');
$urlPrintDetail= Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printDetailLaporanKeseluruhan');

$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#searchLaporan').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}

function printDetail(caraPrint)
{
    window.open("${urlPrintDetail}/"+$('#searchLaporan').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>