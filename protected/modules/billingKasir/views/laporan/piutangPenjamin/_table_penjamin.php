<div id="div_penjamin">
    <div>
        <?php if(!$caraPrint){ ?>
        <legend class="rim"> Tabel Rekap Piutang - P3 / Penjamin </legend>
        <?php } ?>
        <?php
            $data = $model->searchPiutangPenjamin();
            $this->widget($table,array(
                'id'=>'laporanrekapiutangpenjamin-grid',
                'dataProvider'=>$data,
                'enableSorting'=>$sort,
                'template'=>$template,
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'columns'=>array(
                        array(
                            'header' => 'No',
                            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'text-align:center;'),
                        ),
                        array(
                            'header'=>'Initial',
                            'type'=>'raw',
                            'value'=>'$data->penjamin_nama',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'Tgl Pembayaran',
                            'type'=>'raw',
                            'value'=>'$data->tglpembayaran',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'No. Rekam Medik',
                            'type'=>'raw',
                            'value'=>'$data->no_rekam_medik',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                            'header'=>'No. Pendaftaran',
                            'type'=>'raw',
                            'value'=>'$data->no_pendaftaran',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        ),
                        array(
                           'header'=>'<center>Nama Pasien</center>',
                           'type'=>'raw',
                           'value'=>'$data->nama_pasien',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>Unit Pelayanan</center>',
                           'type'=>'raw',
                           'value'=>'$data->ruanganakhir_nama',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>Tgl Masuk</center>',
                           'type'=>'raw',
                           'value'=>'$data->tgl_pendaftaran',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                       array(
                           'header'=>'<center>Tgl Keluar</center>',
                           'type'=>'raw',
                           'value'=>'$data->tglpulang',
                           'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                       ),
                        array(
                            'header'=>'<center>Total Tagihan</center>',
                            'type'=>'raw',
                            'value'=>'MyFunction::formatNumber($data->totalbiayapelayanan)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'vertical-align:middle;text-align:right;'),
                        ),
                        array(
                            'header'=>'<center>Tanggungan <br/> P3</center>',
                            'type'=>'raw',
                            'value'=>'MyFunction::formatNumber($data->totalsubsidiasuransi)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'vertical-align:middle;text-align:right;'),

                        ),
                        array(
                            'header'=>'<center>Tanggungan <br/> Pasien</center>',
                            'type'=>'raw',
                            'value'=>'MyFunction::formatNumber($data->totalbiayapelayanan-$data->totalsubsidiasuransi)',
                            'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                            'htmlOptions'=>array('style'=>'vertical-align:middle;text-align:right;'),
                        ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            ));
        ?>
    </div>
</div>
