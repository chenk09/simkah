<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
     <style>

   label.checkbox, label.radio{
            width:150px;
            display:inline-block;
        }

    </style>
    <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
    <table>
            <tr>
                <td>
                    <div class="control-group ">
                    
                    <?php echo CHtml::hiddenField('filter_tab','rekap',array()); ?>
                    <?php echo CHtml::label('Tanggal Pencarian ','Tanggal Pencarian ', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php echo $form->dropDownList($model,'bulan',
                                array(
                                    'hari'=>'Hari Ini',
                                    'bulan'=>'Bulan',
                                    'tahun'=>'Tahun',
                                ),
                                array(
                                    'id'=>'PeriodeName',
                                    'onChange'=>'setPeriode()',
                                    'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                                )
                                );
                        ?>
                        </div>
                    </div>
                </td>
                <td width="250px">
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                            ),
                        ));
                    ?>
                </td>
                <td width="50px">s/d</td>
                <td>
                    <?php
                        $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
                        ),
                    ));
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="control-group ">
                    <?php echo CHtml::label('Nama Dokter','namaDokter', array('class'=>'control-label')) ?>
                        <div class="controls">
                            <?php 
                                echo $form->dropDownList($model,'pegawai_id', CHtml::listData(DokterpegawaiV::model()->findAll(array('order'=>'nama_pegawai')), 'pegawai_id', 'nama_pegawai'), 
                                          array('empty'=>'-- Pilih --','style'=>'width:160px','onkeypress'=>"return nextFocus(this,event)")); 
                            ?>
                        </div>
                    </div>
                </td>
            </tr>
    </table>
    
    <div class="control-group">
        <?php echo CHtml::label('Komponen Tarif', 'komponentarifId', array('class'=>'control-label')); ?> 
        <div class="controls">
            <?php echo CHtml::checkBox('pilihSemua', true, array('onclick'=>'checkAllKomponen();')) ?> <label><b>Pilih Semua</b></label>
            <div id="checkBoxList">
                <?php echo $form->checkBoxList($model,'komponentarif_id', CHtml::listData(KomponentarifM::model()->Items, 'komponentarif_id', 'komponentarif_nama'), array('class'=>'komponenTarif')); ?><br>
            </div>                
        </div>
    </div>



    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')),
                array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan'));
        ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('laporan/LaporanRekapBaru'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
    </div>
    <?php //$this->widget('TipsMasterData', array('type' => 'create')); ?>    
</div>    
<?php
$this->endWidget();
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
?>
<?php 
Yii::app()->clientScript->registerScript('cekAll','
  $("#content4").find("input[type=\'checkbox\']").attr("checked", "checked");
',  CClientScript::POS_READY);
?>

<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
    
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#LaporanpendapatantindakanV_tglAwal').val(data.periodeawal);
            $('#LaporanpendapatantindakanV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
    function checkAllKomponen(){
        if($('#pilihSemua').is(':checked')){
            $('#checkBoxList').each(function(){
                $(this).find('input').attr('checked',true);
            });
        }else{
            $('#checkBoxList').each(function(){
                $(this).find('input').removeAttr('checked');
            });
        }
    }
    checkAllKomponen();
</script>


