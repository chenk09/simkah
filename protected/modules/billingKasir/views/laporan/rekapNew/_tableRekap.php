<?php

$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->searchJasaDokter();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)) {
    $sort = false;
    $data = $model->searchJasaDokter(false);
    $template = "{items}";
    if ($caraPrint == "EXCEL")
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<?php

$this->widget($table, array(
    'id' => 'laporanrekapjasadokter-grid',
    'dataProvider' => $data,
    'enableSorting' => $sort,
    'template' => $template,
    'itemsCssClass' => 'table table-striped table-bordered table-condensed',
    'columns' => array(
        array(
            'header' => 'Tgl Pembayaran',
            'type' => 'raw',
            'name' => 'tglpembayaran',
        ),
        array(
            'header' => 'No. Rekam Medik',
            'type' => 'raw',
            'name' => 'no_rekam_medik',
        ),
        array(
            'header' => 'Nama Pasien',
            'type' => 'raw',
            'name' => 'nama_pasien'
        ),
        array(
            'header' => 'Daftar Tindakan',
            'type' => 'raw',
            'name' => 'daftartindakan_nama',
        ),
        array(
            'header' => 'Komponen Tarif Nama',
            'type' => 'raw',
            'name' => 'komponentarif_nama',
        ),
        array(
            'header' => 'Qty Tindakan',
            'type' => 'raw',
            'name' => 'qty_tindakan',
        ),
        array(
            'header' => 'Tarif Satuan',
            'type' => 'raw',
            'value' => 'MyFunction::formatNumber($data->tarif_satuan)',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header' => 'Tarif Tindakan',
            'type' => 'raw',
            'name' => 'tarif_tindakankomp',
            'value' => 'MyFunction::formatNumber($data->tarif_tindakankomp)',
            'htmlOptions' => array('style' => 'text-align:right;'),
        ),
        array(
            'header' => 'Penjamin',
            'type' => 'raw',
            'name' => 'penjamin_nama',
        ),
        array(
            'header' => 'Nama Pegawai',
            'type' => 'raw',
            'name' => 'nama_pegawai',
            'value' => '(empty($data->nama_pegawai) ? "-" : "$data->nama_pegawai" )',
        ),
        array(
            'header' => 'Kelas Pelayanan',
            'type' => 'raw',
            'name' => 'kelaspelayanan_nama',
        ),
        array(
            'header' => 'Ruangan',
            'type' => 'raw',
            'name' => 'ruangan_nama',
        ),
    ),
    'afterAjaxUpdate' => 'function(id, data){jQuery(\'' . Params::TOOLTIP_SELECTOR . '\').tooltip({"placement":"' . Params::TOOLTIP_PLACEMENT . '"});}',
));
?>
