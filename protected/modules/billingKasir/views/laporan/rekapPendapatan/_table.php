<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->searchTable();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchPrint();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'mergeHeaders'=>array(
            array(
                'name'=>'<center>Total Tagihan</center>',
                'start'=>6, //indeks kolom 3
                'end'=>7, //indeks kolom 4
            ),
        ),
	'columns'=>array(
            array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),  
            array(
                'header'=>'Tgl Pembayaran',
                'type'=>'raw',
                'value'=>'$data->tglpembayaran',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'No RM',
                'type'=>'raw',
                'value'=>'$data->no_rekam_medik',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
            array(
                'header'=>'Nama Pasien',
                'type'=>'raw',
                'value'=>'$data->nama_pasien',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
			array(
                'header'=>'Cara Bayar',
                'type'=>'raw',
                'value'=>'$data->carabayar_nama',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
			array(
                'header'=>'Penjamin',
                'type'=>'raw',
                'value'=>'$data->penjamin_nama',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
            ),
//            array(
//                'header'=>'Total Obat',
//                'type'=>'raw',
//                'value'=>'MyFunction::formatNumber($data->totalbiayaoa)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'Total Tindakan',
//                'type'=>'raw',
//                'value'=>'MyFunction::formatNumber($data->totalbiayatindakan)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
            array(
                'header'=>'<center>Total Obat</center>',
                'type'=>'raw',
                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalObat",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
                'header'=>'<center>Total Tindakan</center>',
                'type'=>'raw',
                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalTindakan",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
//            array(
//                'header'=>'<center>Bayar Tunai</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalKas",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'<center>Bank</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalBank",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//               
//            ),
//            array(
//                'header'=>'<center>Giro</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalGiro",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'<center>Piutang P3</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalP3",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'<center>Piutang Pasien</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalPiutang",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'<center>Jumlah</center>',
//                'type'=>'raw',
//                'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapPendapatan/_totalJumlah",array(pendaftaran_id=>"$data->pendaftaran_id",tglpembayaran=>"$data->tglpembayaran"),true)',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:right;'),
//            ),
//            array(
//                'header'=>'<center>User <br/> Name</center>',
//                'type'=>'raw',
//                'value'=>'($data->nama_pemakai != null) ? "$data->nama_pemakai":"-"',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:center;'),
//            ),
//            array(
//                'header'=>'<center>Nama <br/> Perusahaan <br/> P3 </center>',
//                'type'=>'raw',
//                'value'=>'($data->penjamin_nama == "Umum" ) ? "-":"$data->penjamin_nama" ',
//                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
//                'htmlOptions'=>array('style'=>'text-align:center;'),
//            ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>