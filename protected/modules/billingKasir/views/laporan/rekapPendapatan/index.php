<legend class="rim2">Laporan Rekap Pendapatan</legend>
<?php
$url = Yii::app()->createUrl(Yii::app()->controller->module->id . '/laporan/frameGrafikLaporanRekapPendapatan&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
    $('#tableLaporan').addClass('srbacLoading');
	var tabmenu = $('#tabmenu').val();
        console.log(tabmenu);
	if (tabmenu == 'rekap'){
		$.fn.yiiGridView.update('tableLaporan', {
				data: $(this).serialize()
		});
	}else{
		$.fn.yiiGridView.update('tableLaporanDetail', {
            data: $(this).serialize()
		});
	}
    return false;
});
");
?>
<div class="search-form">
    <?php
    $this->renderPartial('rekapPendapatan/_search', array(
        'model' => $model,
    ));
    ?>
</div>
<div>
    <?php
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $this->widget('bootstrap.widgets.BootMenu', array(
        'type' => 'tabs',
        'stacked' => false,
        'htmlOptions' => array('id' => 'tabmenu'),
        'items' => array(
            array('label' => 'Rekap Pendapatan', 'url' => 'javascript:tab(0);', 'active' => true),
            array('label' => 'Detail Pendapatan', 'url' => 'javascript:tab(1);', 'itemOptions' => array("index" => 1)),
        ),
    ))
    ?>
</div>
<fieldset> 
    <div id="rekapPendapatan">
        <legend class="rim">Tabel Rekap Pendapatan</legend>
        <?php $this->renderPartial('rekapPendapatan/_table', array('model' => $model)); ?>
        <?php
        $urlPrint = Yii::app()->createAbsoluteUrl($module . '/' . $controller . '/printLaporanRekapPendapatan');
        $this->renderPartial('rekapPendapatan/_footer', array('urlPrint' => $urlPrint, 'url' => $url));
        ?>
    </div>
    <div id="rekapPendapatanDetail" style="display:none">
        <legend class="rim">Tabel Detail Pendapatan</legend>
        <?php $this->renderPartial('rekapPendapatan/_tableDetail', array('model' => $modelDetail)); ?>
        <?php
        $urlPrint = Yii::app()->createAbsoluteUrl($module . '/' . $controller . '/printLaporanRekapPendapatanDetail');
        $this->renderPartial('rekapPendapatan/_footerDetail', array('urlPrint' => $urlPrint, 'url' => $url));
        ?>
    </div>

    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>

<?php
$js = <<< JS
    $(document).ready(function() {
        $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_rekap").show();
        $("#tabmenu").val('rekap');
        $("#div_detail").hide();
        $("#detailJasaDokter").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){
            $("#tabmenu").val('rekap');
            $("#rekapPendapatan").show();
            $("#rekapPendapatanDetail").hide();       
        } else if (index==1){
            $("#tabmenu").val('detail');
            $("#rekapPendapatan").hide();
            $("#rekapPendapatanDetail").show();       
        } 
   }
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('laporanrekapjasadokter-grid', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('laporandetailjasadokter-grid', {
                data: $("#searchLaporan").serialize()
            });      
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat', $js, CClientScript::POS_HEAD);
?>
