<?php
    if(empty($pendaftaran_id) || empty($tglpembayaran)){
        echo '-';
    }else{
        $format = new CustomFormat();
        $tgl = $format->formatDateTimeMediumForDB($tglpembayaran);
        $modTotal = LaporanrekappendapatanV::model()->findAllByAttributes(array(
            'pendaftaran_id'=>$pendaftaran_id,
            'tglpembayaran'=>$tgl
        ));
        $totTarif = 0;

        foreach($modTotal as $key=>$totals){
                   $totTarif += $totals->totalbiayaoa ;
               
       }
       if(isset($caraPrint)){
           echo $totTarif;
       }else{
        echo MyFunction::formatNumber($totTarif);
       }
    }
    
?>