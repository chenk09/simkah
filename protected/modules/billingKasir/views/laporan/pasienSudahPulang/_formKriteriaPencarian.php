<style>
    td label.checkbox{
        width: 150px;
        display:inline-block;
    }
    .checkbox.inline + .checkbox.inline{
        margin-left:0px;
    }
</style>
<fieldset>
    <legend class="rim"><?php echo  Yii::t('mds','Search Patient') ?></legend>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo $form->label($model, 'tglAwal', array('class'=>'control-label'));?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tglAwal',
							'mode'=>'datetime',
							'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
								'maxDate' => 'd',
							),
							'htmlOptions'=>array(
								'class'=>'dtPicker3',
								'onkeypress'=>"return $(this).focusNextInputField(event)"
							),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo $form::label($model,'tglAkhir', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php $this->widget('MyDateTimePicker',array(
							'model'=>$model,
							'attribute'=>'tglAkhir',
							'mode'=>'datetime',
							'options'=> array(
								'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
							),
							'htmlOptions'=>array(
								'class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
							),
                        )); ?>
                    </div>
                </div>
				<div class="control-group ">
                    <?php echo $form::label($model,'carabayar_id', array('class'=>'control-label')) ?>
                    <div class="controls">
						<?php echo $form->dropDownList($model, 'carabayar_id', CHtml::listData(CarabayarM::model()->findAll('carabayar_aktif = true'), 'carabayar_id', 'carabayar_nama'), array('empty' => '-- Pilih --', 'onkeypress' => "return $(this).focusNextInputField(event)",
							'ajax' => array(
								'type' => 'POST',
								'url' => Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox', array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')),
								'update' => '#penjamin',
							),
						));?>
						<?php echo CHtml::checkBox('cek_penjamin', true, array('onchange'=>'cek_all_penjamin(this)','value'=>'cek_penjamin'));?> <label>Cek Semua</label>
                    </div>
                </div>
				<div class="control-group ">
                    <?php echo $form::label($model,'penjamin_id', array('class'=>'control-label')) ?>
                    <div class="controls"><div id="penjamin"><label>Data Tidak Ditemukan</label></div></div>
                </div>
                <?php echo CHtml::hiddenField('filter_tab', 'all'); ?>
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            </td>
			<td>
				<?php echo $form->dropDownListRow($model, 'instalasi_id', CHtml::listData(InstalasiM::model()->findAll('instalasi_id IN (2,3,4,5,6) AND instalasi_aktif = true'), 'instalasi_id', 'instalasi_nama'), array(
					'empty' => '-- Pilih --',
					'onkeypress' => "return $(this).focusNextInputField(event)",
					'ajax' => array(
						'type' => 'POST',
						'url' => Yii::app()->createUrl('ActionDynamic/GetRuanganDariInstalasi', array('encode' => false, 'namaModel' =>$model->getNamaModel())),
						'update' => "#" . CHtml::activeId($model,'ruanganakhir_id'),
					),
				));?>
				<?php echo $form->dropDownListRow($model, 'ruanganakhir_id', array(), array(
					'empty' => '-- Pilih --',
					'onkeypress' => "return $(this).focusNextInputField(event)",
				));?>			
            </td>
        </tr>
    </table>
</fieldset>

<script type="text/javascript">
    function checkAll(obj){
        if($(obj).is(':checked')){
            $("#penjamin").find("input[type=\'checkbox\']").not('#checkAllCaraBayar').attr("checked", "checked");
        }else{
            $("#penjamin").find("input[type=\'checkbox\']").not('#checkAllCaraBayar').attr("checked", false);
        }
    }
	
	function cek_all_ruangan(obj){
        if($(obj).is(':checked')){
            $("#ruangan_tbl").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#ruangan_tbl").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }
    
    function cek_all_penjamin(obj){
        if($(obj).is(':checked')){
            $("#penjamin_tbl").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#penjamin_tbl").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }
</script>