<?php
	$judulLaporan = "<div><b>". $data['judulLaporan'] ."</b></div>";
	$judulLaporan .= "<div>Periode : ". date("d-m-Y", strtotime($model->tglAwal)) . "s/d" . date("d-m-Y", strtotime($model->tglAkhir)) ."</div>";
?>
<table class="table_grid">
	<tr>
		<td>
<?php
if (isset($data['caraPrint'])){
    if($data['caraPrint'] == 'EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulLaporan'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');
        $headerDefault = "";
    }
    
    if($data['caraPrint'] == 'PDF'){
        $headerDefault = $this->renderPartial('application.views.headerReport.headerDefault', array(
			'width'=>1024,
			'judulLaporan'=>$judulLaporan
		));
    }
	
    if($data['caraPrint'] == 'PRINT'){
        $headerDefault = $this->renderPartial('application.views.headerReport.headerDefault');
    }
}
?>
		</td>
	</tr>
	<tr>
		<td>
<?php
	$this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
		'id'=>'semua_pencarianpasien_grid',
		'dataProvider'=>$model->searchPrintPasienSudahPulang(),
		'template'=>"{items}",
		'itemsCssClass'=>'table table-striped table-bordered table-condensed',
		'htmlOptions'=>array(
			"style"=>"page-break-after:always"
		),
		'columns'=>array(
			array(
				'header'=>'Tgl Masuk',
				'name'=>'tgl_pendaftaran',
				'type'=>'raw',
				'value'=>'$data->tgl_pendaftaran',
				'footerHtmlOptions'=>array('colspan'=>10,'style'=>'text-align:right;font-weight:bold;'),
				'footer'=>'Jumlah Total',
			),
			array(
				'header'=>'Tgl Keluar',
				'name'=>'tglpulang',
				'type'=>'raw',
				'value'=>'($data->instalasi_id == 4 ? $data->tglpulang : $data->tgl_pendaftaran ) ',
			),
			"instalasi_nama",
			"ruangan_nama",
			"carabayar_nama",
			"penjamin_nama",
			array(
				'header'=>'No Rekam Medik',
				'name'=>'no_rekam_medik',
				'type'=>'raw',
				'value'=>'$data->no_rekam_medik',
			),
			array(
				'header'=>'No Pendaftaran',
				'name'=>'no_pendaftaran',
				'type'=>'raw',
				'value'=>'$data->no_pendaftaran',
			),
			array(
				'header'=>'Nama / Alias',
				'name'=>'pasien_id',
				'type'=>'raw',
				'value'=>'$data->namadepan." ".$data->nama_pasien ." / ".$data->nama_bin',
			),
			array(
				'header'=>'Status Pembayaran',
				'type'=>'raw',
				'value'=>'$data->status',
				'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
			),
			 array(
				'header'=>'Jml Tagihan',
				'name'=>'tarif_tindakan',
				'type'=>'raw',
				'value'=>'"Rp. ".MyFunction::formatNumber($data->tarif_tindakan)',
				'htmlOptions'=>array(
					'style'=>'text-align:right;',
					'width'=>'90'
				),
				'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
				'footer'=>'sum(tarif_tindakan)',
			),
		   array(
				'header'=>'Tanggungan Pasien',
				'name'=>'iurbiaya_tindakan',
				'type'=>'raw',
				'value'=>'"Rp. ".MyFunction::formatNumber($data->iurbiaya_tindakan)',
				'htmlOptions'=>array(
					'style'=>'text-align:right;',
					'width'=>'90'
				),
				'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
				'footer'=>'sum(iurbiaya_tindakan)',
			),
		   array(
				'header'=>'Subsidi',
				'name'=>'subsidiasuransi_tindakan',
				'type'=>'raw',
				'value'=>'"Rp. ".MyFunction::formatNumber($data->subsidiasuransi_tindakan)',
				'htmlOptions'=>array(
					'style'=>'text-align:right;',
					'width'=>'90'
				),
				'footerHtmlOptions'=>array('style'=>'text-align:right;font-weight:bold;'),
				'footer'=>'sum(subsidiasuransi_tindakan)',
			)
		),
		'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
	));
?>
</FONT>
		</td>
	</tr>
</table>