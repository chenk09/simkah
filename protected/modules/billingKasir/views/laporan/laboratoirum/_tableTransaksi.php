<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = false;
    $dataProvider = $model->searchTable();
    $template = "{pager}{summary}\n{items}";
?>
<?php
    $this->widget($table,
        array(
            'id'=>'tableTrans',
            'dataProvider'=>$dataProvider,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns' => array('no_rekam_medik'),
            'columns'=>array(
                array(
                    'header'=>'Tgl. Transaksi',
                    'type'=>'raw',
                    'value'=>'$data->tgl_tindakan',
                    'footerHtmlOptions'=>array('colspan'=>5,'style'=>'text-align:right;font-weight:bold;'),
                    'footer'=>'Total (Rp.)',
                ),
                array(
                    'header'=>'No. RM',
                    'name'=>'no_rekam_medik',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik',
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien',
                ),
//                array(
//                    'header'=>'No. Transaksi',
//                    'type'=>'raw',
//                    'value'=>'$data->tindakanpelayanan_id',
//                ),
                
//                array(
//                    'header'=>'Nama Tindakan',
//                    'type'=>'raw',
//                    'value'=>'$data->daftartindakan_nama',
//                ),
//                array(
//                    'header'=>'Qty',
//                    'type'=>'raw',
//                    'value'=>'MyFunction::formatNumber($data->qty_tindakan)',
//                ),
//                array(
//                    'header'=>'Biaya Pasien',
//                    'type'=>'raw',
//                    'value'=>'MyFunction::formatNumber($data->tarif_satuan)',
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                ),
                array(
                    'header'=>'Asal',
                    'type'=>'raw',
                    'value'=>'empty($data->rujukan_id) ? "RS. Jasa Kartini" : "Rujukan"',
                    
                ),
                array(
                    'header'=>'Biaya Pasien',
                    'name'=>'tarif_tindakan',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->tarif_tindakan)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(tarif_tindakan)'
                ),
                array(
                    'header'=>'Tanggungan P3',
                    'name'=>'TotalSubsidi',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->TotalSubsidi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(TotalSubsidi)'
                ),
//                array(
//                    'header'=>'Iur Biaya',
//                    'type'=>'raw',
//                    'value'=>'MyFunction::formatNumber($data->iurbiaya_tindakan)',
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                ),
             ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )
    );
    