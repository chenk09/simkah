<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = false;
    $dataProvider = $model->searchTableGroup();
    $template = "{pager}{summary}\n{items}";
?>
<?php
    $this->widget($table,
        array(
            'id'=>'tableRekap',
            'dataProvider'=>$dataProvider,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns' => array('no_rekam_medik'),
            'columns'=>array(
                array(
                    'header'=>'No. RM',
                    'name'=>'no_rekam_medik',
                    'type'=>'raw',
                    'value'=>'$data->no_rekam_medik',
                    'footerHtmlOptions'=>array('colspan'=>5,'style'=>'text-align:right;font-weight:bold;'),
                    'footer'=>'Total (Rp.)',
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->nama_pasien',
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->no_pendaftaran',
                ),
                array(
                    'header'=>'Asal',
                    'type'=>'raw',
                    'value'=>'empty($data->rujukan_id) ? "RS. Jasa Kartini" : "Rujukan"',
                ),
                array(
                    'header'=>'Initial P3',
                    'type'=>'raw',
                    'value'=>'$data->penjamin_nama',
                ),
                array(
                    'header'=>'Biaya Pasien',
                    'name'=>'tarif_tindakan',
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->tarif_tindakan)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(tarif_tindakan)'
                ),
                array(
                    'header'=>'Tanggungan P3',
                    'name'=>'TotalSubsidi', 
                    'type'=>'raw',
                    'value'=>'MyFunction::formatNumber($data->TotalSubsidi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'footerHtmlOptions'=>array('style'=>'text-align:right;'),
                    'footer'=>'sum(TotalSubsidi)'
                ),
//                array(
//                    'header'=>'Iur Biaya',
//                    'type'=>'raw',
//                    'value'=>'MyFunction::formatNumber($data->iurbiaya_tindakan)',
//                    'htmlOptions'=>array('style'=>'text-align:right;'),
//                ),
             ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )
    );
    