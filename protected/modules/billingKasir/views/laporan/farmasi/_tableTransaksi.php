<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $dataProvider = $model->searchPrintLaporan();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
        
        echo $this->renderPartial('application.views.headerReport.headerLaporan',
            array(
                'judulLaporan'=>$data['judulLaporan'],
                'periode'=>$data['periode']
            )
        );        
    } else{
        $dataProvider = $model->searchTable();
        $template = "{pager}{summary}\n{items}";
    }
?>
<?php
    $this->widget($table,
        array(
            'id'=>'tableLaporanTrans',
            'dataProvider'=>$dataProvider,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns' => array('no_rekam_medik'),
            'columns'=>array(
                array(
                    'header'=>'No. RM',
                    'name'=>'no_rekam_medik',
                    'type'=>'raw',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->pendaftaran->no_pendaftaran',
                ),
                array(
                    'header'=>'Tgl Transaksi',
                    'type'=>'raw',
                    'value'=>'$data->tglpelayanan',
                ),
                array(
                    'header'=>'Nama Item',
                    'type'=>'raw',
                    'value'=>'$data->obatalkes->obatalkes_nama',
                ),
                array(
                    'header'=>'Apotek',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Qty',
                    'type'=>'raw',
                    'value'=>'$data->qty_oa',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Pasien',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Sub Total',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargajual_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Tanggungan P3',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->subsidiasuransi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
             ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )
    );
?>