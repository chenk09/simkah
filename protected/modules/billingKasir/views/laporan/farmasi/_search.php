<style>
    td label.checkbox{
        width: 150px;
        display:inline-block;

    }

    .checkbox.inline + .checkbox.inline{
        margin-left:0px;
    }
</style>
<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'action' => Yii::app()->createUrl($this->route),
        'method' => 'get',
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
    <style>
        table{
            margin-bottom: 0px;
        }
        .form-actions{
            padding:4px;
            margin-top:5px;
        }
        #ruangan label{
            width: 120px;
            display:inline-block;
        }
        .nav-tabs>li>a{display:block; cursor:pointer;}
    </style>
    
<legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>

<table width="100%">
    <tr>
        <td width="55%">
            <div class="control-group ">
                <div class='control-label'>Tanggal Kunjungan&nbsp;</div>
                <div class='controls'>
                    <?php echo CHtml::hiddenField('type', ''); ?>
                    <?php //echo CHtml::hiddenField('src', ''); ?>                    
                    <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                    ?>                    
                </div>
            </div>
            <div class="control-group">
                <div class='control-label'>Cara Bayar</div>
                <div class='controls'>
                    <?php
                        echo $form->dropDownList($model, 'carabayar_id',
                            CHtml::listData(CarabayarM::model()->findAll('carabayar_aktif = true'), 'carabayar_id', 'carabayar_nama'),
                            array(
                                'empty' => '-- Pilih --',
                                'onkeypress' => "return $(this).focusNextInputField(event)",
                                'ajax' => array(
                                    'type'=>'POST',
                                    'url'=>Yii::app()->createUrl('ActionDynamic/GetPenjaminPasienForCheckBox',
                                        array('encode' => false, 'namaModel' => ''.$model->getNamaModel().'')
                                    ),
                                    'update' => '#penjamin',
                                ),
                            )
                        );
                        echo CHtml::checkBox('cek_penjamin', true, array('onchange'=>'cek_all_penjamin(this)','value'=>'cek_penjamin'));
                        echo '&nbsp;<label>Cek Semua</label>';
                    ?>                    
                </div>
            </div>
            <div class="control-group">
                <div class='control-label'>Penjamin</div>
                <div class='controls'>
                    <div id="penjamin"><label>Data Tidak Ditemukan</label></div>
                </div>
            </div>
        </td>
        <td>
            <div class="control-group ">
                <?php echo CHtml::label(' sampai dengan', ' s/d', array('class' => 'control-label')) ?>
                <div class="controls">  
                    <?php
                    $this->widget('MyDateTimePicker', array(
                        'model' => $model,
                        'attribute' => 'tglAkhir',
                        'mode' => 'datetime',
                        'options' => array(
                            'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                        ),
                        'htmlOptions' => array('readonly' => true,
                            'onkeypress' => "return $(this).focusNextInputField(event)"),
                    ));
                    ?>
                </div>
            </div>
            <?php echo $form->hiddenField($model,'filter_tab'); ?>
            <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
            <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
        </td>
    </tr>
</table>
    <div class="form-actions">
        <div style="float:left;margin-right:6px;">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','onClick'=>'cari()')); ?>
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onClick'=>'onReset()')); ?>
        </div>        
        <div style="clear:both;"></div>
    </div>
</div>
<?php
$this->endWidget();
?>
<script type="text/javascript">
	
	function cari(){
		$('#div_reportTranasksi').addClass('srbacLoading');
		$('#div_rekap').addClass('srbacLoading');
		$('#div_per_registrasi').addClass('srbacLoading');
	}
	
    function cek_all_penjamin(obj){
        if($(obj).is(':checked')){
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }
</script>