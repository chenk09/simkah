<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $dataProvider = $model->searchPrintTable();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL"){
            $table = 'ext.bootstrap.widgets.BootExcelGridView';
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="'. $data['judulLaporan'] .'-'.date("Y/m/d").'.xls"');
            header('Cache-Control: max-age=0');
        }
        
        echo $this->renderPartial('application.views.headerReport.headerLaporan',
            array(
                'judulLaporan'=>$data['judulLaporan'],
                'periode'=>$data['periode']
            )
        );        
    } else{
        $dataProvider = $model->searchTable();
        $template = "{pager}{summary}\n{items}";
    }
?>
<?php
    $this->widget($table,
        array(
            'id'=>'tableLaporanKelompok',
            'dataProvider'=>$dataProvider,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'mergeColumns' => array('penjamin_nama', 'no_rekam_medik'),
            'columns'=>array(
                array(
                    'header'=>'Tgl Transaksi',
                    'type'=>'raw',
                    'value'=>'$data->tglpelayanan',
                ),
                array(
                    'header'=>'Penjamin P3',
                    'type'=>'raw',
                    'name'=>'penjamin_nama',
                    'value'=>'$data->penjamin->penjamin_nama',
                ),
                array(
                    'header'=>'No. RM',
                    'type'=>'raw',
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->pendaftaran->no_pendaftaran',
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->pasien->nama_pasien',
                ),
                array(
                    'header'=>'Sex',
                    'type'=>'raw',
                    'value'=>'$data->pasien->jeniskelamin',
                ),
                array(
                    'header'=>'Usia',
                    'type'=>'raw',
                    'value'=>'$data->pendaftaran->umur',
                ),
                array(
                    'header'=>'Golongan',
                    'type'=>'raw',
                    'value'=>'$data->obatalkes->obatalkes_golongan',
                ),
                array(
                    'header'=>'Kategori',
                    'type'=>'raw',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Kelompok',
                    'type'=>'raw',
                    'value'=>'($data->oa == "OA" ? "Alkes" : "Obat")',
                ),
                array(
                    'header'=>'Status Barang',
                    'type'=>'raw',
                    'value'=>'$data->getAsalBarang()',
                ),
                array(
                    'header'=>'Jenis',
                    'type'=>'raw',
                    'value'=>'$data->getJenisObat()',
                ),
                array(
                    'header'=>'Apotek',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Pasien',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Tanggungan P3',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->subsidiasuransi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
             ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )
    );
	?>