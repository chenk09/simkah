<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $dataProvider = $model->printTableRekap();
    $template = "{items}";
    $sort = false;
    
    if ($caraPrint == "EXCEL"){
        $table = 'ext.bootstrap.widgets.BootExcelGridView';
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'. $data['judulLaporan'] .'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');
    }

    echo $this->renderPartial('application.views.headerReport.headerLaporan',
        array(
            'judulLaporan'=>$data['judulLaporan'],
            'periode'=>$data['periode']
        )
    );
?>
<?php
    $this->widget($table,
        array(
            'id'=>'tableLaporanKelompok',
            'dataProvider'=>$dataProvider,
            'template'=>$template,
            'enableSorting'=>$sort,
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'extraRowColumns' => array('kelompok_obat'),
            'columns'=>array(
                array(
                    'header'=>'Kelompok',
                    'type'=>'raw',
                    'name'=>'kelompok_obat',
                    'value'=>'($data->oa == "OA" ? "Alkes" : "Obat")',
                ),
                array(
                    'header'=>'No. RM',
                    'type'=>'raw',
                    'name'=>'no_rekam_medik',
                    'value'=>'$data->pasien->no_rekam_medik',
                ),
                array(
                    'header'=>'No. Pendaftaran',
                    'type'=>'raw',
                    'value'=>'$data->pendaftaran->no_pendaftaran',
                ),
                array(
                    'header'=>'Nama',
                    'type'=>'raw',
                    'value'=>'$data->pasien->nama_pasien',
                ),
                array(
                    'header'=>'Golongan',
                    'type'=>'raw',
                    'value'=>'((is_null($data->obatalkes->obatalkes_golongan) || $data->obatalkes->obatalkes_golongan == "") ? "-" : $data->obatalkes->obatalkes_golongan)',
                ),
                array(
                    'header'=>'Kategori',
                    'type'=>'raw',
                    'value'=>'$data->obatalkes->obatalkes_kategori',
                ),
                array(
                    'header'=>'Jenis',
                    'type'=>'raw',
                    'value'=>'$data->getJenisObat()',
                ),
                array(
                    'header'=>'Apotek',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Pasien',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->hargasatuan_oa)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
                array(
                    'header'=>'Tanggungan P3',
                    'type'=>'raw',
                    'value'=>'"Rp. ".MyFunction::formatnumber($data->subsidiasuransi)',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                ),
             ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
        )
    );
    ?>