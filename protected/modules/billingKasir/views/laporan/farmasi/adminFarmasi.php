<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableLaporanTrans', {
        data: $(this).serialize(),
		success: function (data) {
			$('#div_reportTranasksi').removeClass('srbacLoading');
		}
    });
    $.fn.yiiGridView.update('tableLaporanReg', {
       data: $(this).serialize(),
		success: function (data) {
			$('#div_rekap').removeClass('srbacLoading');
		}
    });
    $.fn.yiiGridView.update('tableLaporanKelompok', {
        data: $(this).serialize(),
		success: function (data) {
			$('#div_per_registrasi').removeClass('srbacLoading');
		}
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Farmasi</legend>
<div class="search-form">
    <?php
        $this->renderPartial('billingKasir.views.laporan.farmasi/_search',
            array(
                'model'=>$model,
            )
        ); 
    ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'tabmenu'),
            'items'=>array(
                array('label'=>'Transaksi Farmasi','url'=>'javascript:tab(0);', 'itemOptions'=>array("index"=>0),'active'=>true),
                array('label'=>'Rekap / Kelompok','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
                array('label'=>'Farmasi Per-Registrasi','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2)),
            ),
        ))
    ?>
    <div id="div_reportTranasksi">
        <legend class="rim">Transaksi Farmasi</legend>
        <fieldset>
            <?php
                $this->renderPartial('billingKasir.views.laporan.farmasi/_tableTransaksi',
                    array(
                        'model'=>$model
                    )
                );
            ?>
        </fieldset>
    </div>
    <div id="div_rekap">
        <legend class="rim">Rekap / Kelompok</legend>
        <fieldset>
            <?php
                $this->renderPartial('billingKasir.views.laporan.farmasi/_tableKelompok',
                    array(
                        'model'=>$model
                    )
                );
            ?>
        </fieldset>
    </div>
    <div id="div_per_registrasi">
        <legend class="rim">Farmasi Per-Registrasi</legend>
        <fieldset>
            <?php
                $this->renderPartial('billingKasir.views.laporan.farmasi/_tablePerRegistrasi',
                    array(
                        'model'=>$model
                    )
                );
            ?>
        </fieldset>
    </div>
</div>
<div class='form-actions'>
	<div style="float:left;margin-right:6px;">
            <?php
                $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
                $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
                $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanKeseluruhan');
                
                $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'=>'primary',
                    'buttons'=>array(
                        array(
                            'label'=>'Print',
                            'icon'=>'icon-print icon-white',
                            'url'=>$urlPrint,
                            'htmlOptions'=>
                                array(
                                    'onclick'=>'print(\'PRINT\');return false;'
                                )
                       ),
                        array(
                            'label'=>'',
                            'items'=>array(
                                array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'PDF\');return false;')),
                                array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'EXCEL\');return false;')),
                            )
                        ),
                    ),
                ));              
             ?>
        </div>
</div>
<?php

$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/LaporanFarmasi');

$js = <<< JSCRIPT
$(document).ready(function() {
    $("#tabmenu").children("li").children("a").click(function() {
        $("#tabmenu").children("li").attr('class','');
        $(this).parents("li").attr('class','active');
        $(".icon-pencil").remove();
        $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
    });
    $("#BKObatalkesPasienT_nama_pasien").attr("disabled",true);
    $("#BKObatalkesPasienT_carabayar_id").attr("disabled",true);
    $("#div_reportTranasksi").show();
    $("#div_rekap").hide();
    $("#div_per_registrasi").hide();
    $("#BKObatalkesPasienT_filter_tab").val("trans");
});

function tab(index){
    $(this).hide();
    $(".btn-group").hide();

    if (index==0){
        $(".btn-group").show();
        $("#BKObatalkesPasienT_filter_tab").val("trans");
        $("#BKObatalkesPasienT_nama_pasien").attr("disabled",true);
        $("#BKObatalkesPasienT_carabayar_id").attr("disabled",true);
        $("#div_reportTranasksi").show();
        $("#div_rekap").hide();
        $("#div_per_registrasi").hide();
    }else if(index==1){
        $(".btn-group").show();
        $("#BKObatalkesPasienT_filter_tab").val("rekap");
        $("#BKObatalkesPasienT_nama_pasien").attr("disabled",false);
        $("#BKObatalkesPasienT_carabayar_id").attr("disabled",false);
        $("#div_reportTranasksi").hide();
        $("#div_rekap").show();
        $("#div_per_registrasi").hide();
    }else if(index==2){
        $(".btn-group").show();
        $("#BKObatalkesPasienT_filter_tab").val("per_reg");
        $("#BKObatalkesPasienT_nama_pasien").attr("disabled",false);
        $("#BKObatalkesPasienT_carabayar_id").attr("disabled",true);
        $("#div_reportTranasksi").hide();
        $("#div_rekap").hide();
        $("#div_per_registrasi").show();
    }
}
function print(caraPrint)
{
    window.open("${urlPrint}/"+$('#searchLaporan').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}


function onReset(){
    setTimeout(
        function(){
            $.fn.yiiGridView.update('tableLaporanTrans', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('tableLaporanReg', {
                data: $("#searchLaporan").serialize()
            });
            $.fn.yiiGridView.update('tableLaporanKelompok', {
                data: $("#searchLaporan").serialize()
            });        
        }, 1000
    );
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
?>