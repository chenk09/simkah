<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->searchTable();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchPrint();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'htmlOptions'=>array(
            'style'=>'font-size',
            
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                            array(
                                'header' => 'No',
                                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                            ),
                            array(
                                'header'=>'Tgl Retur',
                                'name'=>'tglreturpelayanan',
                                'type'=>'raw',
                                'value'=>'$data->tglreturpelayanan',
                            ),
                            'noreturbayar',
                            'nama_pasien',
                            array(
                                'header'=>'No. RM',
                                'name'=>'no_rekam_medik',
                                'type'=>'raw',
                                'value'=>'$data->no_rekam_medik',
                            ),
                            'no_pendaftaran',
                            'ruanganakhir_nama',
                            'totalbiayaretur',
                            'keteranganretur',
                            'user_nm_otorisasi',                                                                

	               ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>