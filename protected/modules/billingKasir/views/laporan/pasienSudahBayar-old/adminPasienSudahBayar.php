<?php
$this->breadcrumbs=array(
	'Pasien Sudah Bayar' => array('/billingKasir/Laporan/pasienSudahBayar'),
	'PasienKarcis',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('semua_pencarianpasien_grid', {
            data: $(this).serialize()
	});
	$.fn.yiiGridView.update('penjamin_pencarianpasien_grid', {
            data: $(this).serialize()
	});
	$.fn.yiiGridView.update('umum_pencarianpasien_grid', {
            data: $(this).serialize()
	});
	return false;
});
");
?>
<?php echo $this->renderPartial('pasienSudahBayar/_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?> 
<div class="form-actions">
    <div style="float:left;margin-right:6px;">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onClick'=>'onReset()')); ?>
    </div>
    <div style="float:left;">
    <?php
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');
        $this->widget('bootstrap.widgets.BootButtonGroup', array(
            'type'=>'primary',
            'buttons'=>array(
                array('label'=>'Print', 'icon'=>'icon-print icon-white', 'url'=>$urlPrint, 'htmlOptions'=>array('onclick'=>'print(\'PRINT\');return false;')),
                array('label'=>'',
                    'items'=>array(
                        array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'PDF\');return false;')),
                        array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'EXCEL\');return false;')),
                    )
                ),
            ),
        ));
    
$js = <<< JSCRIPT
function print(caraPrint)
{
    window.open("${urlPrint}/&"+$('#caripasien-form').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);
    ?>
    </div>
    <div style="clear:both;"></div>
</div>
    <div class="tab">
        <?php
            $this->widget('bootstrap.widgets.BootMenu',array(
                'type'=>'tabs',
                'stacked'=>false,
                'htmlOptions'=>array('id'=>'tabmenu'),
                'items'=>array(
                    array('label'=>'All','url'=>'javascript:tab(0);','active'=>true),
                    array('label'=>'P3','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
                    array('label'=>'Umum','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2)),
                ),
            ))
        ?>
        <div id="div_semua">
            <legend class="rim">Informasi Pasien Sudah Bayar - Semua</legend>
            <div>
                <?php
                    $this->widget('ext.bootstrap.widgets.BootGridView', array(
                        'id'=>'semua_pencarianpasien_grid',
                        'dataProvider'=>$model->searchPasienSudahBayar(),
                        'template'=>"{pager}{summary}\n{items}",
                        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                        'columns'=>array(
                            array(
                                'header'=>'Tgl Bukti Bayar',
                                'name'=>'tglbuktibayar',
                                'type'=>'raw',
                                'value'=>'$data->tandabuktibayar->tglbuktibayar."<br>".$data->tandabuktibayar->nobuktibayar',
                            ),
                            array(
                                'name'=>'instalasi',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                            ),
                            array(
                                'header'=>'No Pendaftaran / No Rekam Medik',
                                'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->no_rekam_medik',
                            ),
                            array(
                                'name'=>'nama_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->namaNamaBin',
                            ),
                            array(
                                'name'=>'alamat_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->alamat_pasien',
                            ),
                            array(
                                'header'=>'Cara Bayar | Penjamin',
                                'name'=>'carabayar_nama',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
                            ),
                            array(
                                'name'=>'total_tagihan',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
                            ),
                            array(
                                'header'=>'Subsidi Asuransi',
                                'name'=>'subsidi_asuransi',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidiasuransi',
                            ),
                            array(
                                'header'=>'Subsidi Pemerintah',
                                'name'=>'subsidi_pemerintah',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidipemerintah',
                            ),
                            array(
                                'header'=>'Subsidi RS / Klinik',
                                'name'=>'subsidi_rs',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidirs',
                            ),
                            array(
                                'header'=>'Biaya',
                                'name'=>'iur_biaya',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
                            ),
                            array(
                                'header'=>'Disc',
                                'name'=>'discount',
                                'type'=>'raw',
                                'value'=>'$data->totaldiscount',
                            ),
                            array(
                                'header'=>'Pembebasan',
                                'name'=>'pembebasan',
                                'type'=>'raw',
                                'value'=>'$data->totalpembebasan',
                            ),
                            array(
                                'header'=>'Jumlah Pembayaran',
                                'name'=>'jumlah_pembayaran',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
                            ),
                        ),
                        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                    ));
                ?>
            </div>
        </div>
        
        <div id="div_penjamin">
            <legend class="rim">Informasi Pasien Sudah Bayar - P3</legend>
            <div style="max-width:1200px;overflow-x:scroll">
                <?php
                    $this->widget('ext.bootstrap.widgets.BootGridView', array(
                        'id'=>'penjamin_pencarianpasien_grid',
                        'dataProvider'=>$model->searchPasienBerdasarkanPenjamin(),
                        'template'=>"{pager}{summary}\n{items}",
                        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                        'columns'=>array(
                            array(
                                'header'=>'Tgl Bukti Bayar',
                                'name'=>'tglbuktibayar',
                                'type'=>'raw',
                                'value'=>'$data->tandabuktibayar->tglbuktibayar."<br>".$data->tandabuktibayar->nobuktibayar',
                            ),
                            array(
                                'name'=>'instalasi',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                            ),
                            array(
                                'header'=>'No Pendaftaran / No Rekam Medik',
                                'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->no_rekam_medik',
                            ),
                            array(
                                'name'=>'nama_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->namaNamaBin',
                            ),
                            array(
                                'name'=>'alamat_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->alamat_pasien',
                            ),
                            array(
                                'header'=>'Cara Bayar | Penjamin',
                                'name'=>'carabayar_nama',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
                            ),
                            array(
                                'name'=>'total_tagihan',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
                            ),
                            array(
                                'header'=>'Subsidi Asuransi',
                                'name'=>'subsidi_asuransi',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidiasuransi',
                            ),
                            array(
                                'header'=>'Subsidi Pemerintah',
                                'name'=>'subsidi_pemerintah',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidipemerintah',
                            ),
                            array(
                                'header'=>'Subsidi RS / Klinik',
                                'name'=>'subsidi_rs',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidirs',
                            ),
                            array(
                                'header'=>'Biaya',
                                'name'=>'iur_biaya',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
                            ),
                            array(
                                'header'=>'Disc',
                                'name'=>'discount',
                                'type'=>'raw',
                                'value'=>'$data->totaldiscount',
                            ),
                            array(
                                'header'=>'Pembebasan',
                                'name'=>'pembebasan',
                                'type'=>'raw',
                                'value'=>'$data->totalpembebasan',
                            ),
                            array(
                                'header'=>'Jumlah Pembayaran',
                                'name'=>'jumlah_pembayaran',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
                            ),
                        ),
                        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                    ));
                ?>
            </div>            
        </div>
        
        <div id="div_umum">
            <legend class="rim">Informasi Pasien Sudah Bayar - Umum</legend>
            <div style="max-width:1200px;overflow-x:scroll">
                <?php
                    $this->widget('ext.bootstrap.widgets.BootGridView', array(
                        'id'=>'umum_pencarianpasien_grid',
                        'dataProvider'=>$model->searchPasienBerdasarkanUmum(),
                        'template'=>"{pager}{summary}\n{items}",
                        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                        'columns'=>array(
                            array(
                                'header'=>'Tgl Bukti Bayar',
                                'name'=>'tglbuktibayar',
                                'type'=>'raw',
                                'value'=>'$data->tandabuktibayar->tglbuktibayar."<br>".$data->tandabuktibayar->nobuktibayar',
                            ),
                            array(
                                'name'=>'instalasi',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                            ),
                            array(
                                'header'=>'No Pendaftaran / No Rekam Medik',
                                'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->no_rekam_medik',
                            ),
                            array(
                                'name'=>'nama_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->namaNamaBin',
                            ),
                            array(
                                'name'=>'alamat_pasien',
                                'type'=>'raw',
                                'value'=>'$data->pasien->alamat_pasien',
                            ),
                            array(
                                'header'=>'Cara Bayar | Penjamin',
                                'name'=>'carabayar_nama',
                                'type'=>'raw',
                                'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
                            ),
                            array(
                                'name'=>'total_tagihan',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
                            ),
                            array(
                                'header'=>'Subsidi Asuransi',
                                'name'=>'subsidi_asuransi',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidiasuransi',
                            ),
                            array(
                                'header'=>'Subsidi Pemerintah',
                                'name'=>'subsidi_pemerintah',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidipemerintah',
                            ),
                            array(
                                'header'=>'Subsidi RS / Klinik',
                                'name'=>'subsidi_rs',
                                'type'=>'raw',
                                'value'=>'$data->totalsubsidirs',
                            ),
                            array(
                                'header'=>'Biaya',
                                'name'=>'iur_biaya',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
                            ),
                            array(
                                'header'=>'Disc',
                                'name'=>'discount',
                                'type'=>'raw',
                                'value'=>'$data->totaldiscount',
                            ),
                            array(
                                'header'=>'Pembebasan',
                                'name'=>'pembebasan',
                                'type'=>'raw',
                                'value'=>'$data->totalpembebasan',
                            ),
                            array(
                                'header'=>'Jumlah Pembayaran',
                                'name'=>'jumlah_pembayaran',
                                'type'=>'raw',
                                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
                            ),
                        ),
                        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
                    ));
                ?>
            </div>            
        </div>        
    </div>
<?php $this->endWidget(); ?>
<?php
$js= <<< JS
    $(document).ready(function() {
        $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_semua").show();
        $("#div_penjamin").hide();
        $("#div_umum").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){
            $("#filter_tab").val('all');
            $("#div_semua").show();
            $("#div_penjamin").hide();
            $("#div_umum").hide();        
        } else if (index==1){
            $("#filter_tab").val('p3');
            $("#div_semua").hide();
            $("#div_penjamin").show();
            $("#div_umum").hide();
        } else if (index==2){
            $("#filter_tab").val('umum');
            $("#div_semua").hide();
            $("#div_penjamin").hide();
            $("#div_umum").show();        
        }
   }
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('semua_pencarianpasien_grid', {
                data: $("#caripasien-form").serialize()
            });
            $.fn.yiiGridView.update('penjamin_pencarianpasien_grid', {
                data: $("#caripasien-form").serialize()
            });
            $.fn.yiiGridView.update('umum_pencarianpasien_grid', {
                data: $("#caripasien-form").serialize()
            });        
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>