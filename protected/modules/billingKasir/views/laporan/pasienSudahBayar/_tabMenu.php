<?php
/**
* modules/billingKasir/Laporan/LaporanPasienSudahBayar&modulId=19
* Updated by    : Miranitha Fasha
* Date          : 1-05-2014
* Issue         : EHJ-1749
* Deskripsi     : view Index yang berisi tab laporan
**/
?>
<?php
$this->widget('bootstrap.widgets.BootMenu', array(
    'type'=>'tabs', 
    'stacked'=>false, 
    'items'=>array(    	
        array('label'=>'All', 'url'=>'javascript:void(0);', 'itemOptions'=>array('id'=>'tab-default','onclick'=>'setTab(this);', 'tab'=>'billingKasir/laporan/LaporanPasienSudahBayarAll')),
        array('label'=>'P3', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'billingKasir/laporan/LaporanPasienSudahBayarP3')),
        array('label'=>'Umum', 'url'=>'javascript:void(0);', 'itemOptions'=>array('onclick'=>'setTab(this);', 'tab'=>'billingKasir/laporan/LaporanPasienSudahBayarUmum')),
 
    ),
));
?>