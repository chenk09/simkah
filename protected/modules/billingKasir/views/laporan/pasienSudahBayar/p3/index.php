<?php
$url = Yii::app()->createUrl('billingKasir/laporan/FrameGrafikLaporanPasienSudahBayarP3&id=1');
Yii::app()->clientScript->registerScript('search', "
$('#searchLaporan').submit(function(){
    $.fn.yiiGridView.update('pasiensudahbayarp3-grid', {
            data: $(this).serialize()
    });
    return false;
});
");
?>

<?php $this->renderPartial('pasienSudahBayar/p3/_search',array('model'=>$model)); ?>
<fieldset>
    <?php $this->renderPartial('pasienSudahBayar/p3/_table',array('model'=>$model)); ?>
    <iframe src="" id="Grafik" width="100%" height='0' onload="javascript:resizeIframe(this);"></iframe>
</fieldset>
<?php        
$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/PrintLaporanPasienSudahBayarP3');
$this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url));
?>
