<?php
/**
* modules/billingKasir/Laporan/LaporanPasienSudahBayar&modulId=19
* Updated by    : Miranitha Fasha
* Date          : 01-05-2014
* Issue         : EHJ-1749
* Deskripsi     : Table / P3
**/
?>
<?php 
    $table = 'ext.bootstrap.widgets.BootGroupGridView';
    $sort = true;
    if (isset($caraPrint)){
        $dataProvider = $model->searchPasienBerdasarkanPenjamin();
        $template = "{items}";
        $sort = false;
        if ($caraPrint == "EXCEL")
            $table = 'ext.bootstrap.widgets.BootExcelGridView';

        echo $this->renderPartial('application.views.headerReport.headerLaporan',
            array(
                'judulLaporan'=>$data['judulLaporan'],
                'periode'=>$data['periode']
            )
        );        
    } else{
        $dataProvider = $model->searchPasienBerdasarkanPenjamin();
        $template = "{pager}{summary}\n{items}";
    }
?>
<div>
      <?php if(!isset($caraPrint)){
        
        }else{
    ?>
    <legend class="rim">Laporan Pasien Sudah Bayar - P3</legend>
        <?php } ?>
    <!--<div style="max-width:1200px;overflow-x:scroll">-->
        <?php
            $this->widget($table, array(
                'id'=>'pasiensudahbayarp3-grid',
                'dataProvider'=>$dataProvider,
                'template'=>$template,
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                'columns'=>array(
                    array(
                        'header'=>'Tgl Bukti Bayar',
                        'name'=>'tglbuktibayar',
                        'type'=>'raw',
                        'value'=>'$data->tandabuktibayar->tglbuktibayar."<br>".$data->tandabuktibayar->nobuktibayar',
                    ),
                    array(
                        'name'=>'instalasi',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                    ),
                    array(
                        'header'=>'No Pendaftaran / No Rekam Medik',
                        'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->pasien->namaNamaBin',
                    ),
                    array(
                        'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->pasien->alamat_pasien',
                    ),
                    array(
                        'header'=>'Cara Bayar | Penjamin',
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
                    ),
                    array(
                        'name'=>'total_tagihan',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
                    ),
                    array(
                        'header'=>'Subsidi Asuransi',
                        'name'=>'subsidi_asuransi',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidiasuransi',
                    ),
                    array(
                        'header'=>'Subsidi Pemerintah',
                        'name'=>'subsidi_pemerintah',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidipemerintah',
                    ),
                    array(
                        'header'=>'Subsidi RS / Klinik',
                        'name'=>'subsidi_rs',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidirs',
                    ),
                    array(
                        'header'=>'Biaya',
                        'name'=>'iur_biaya',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
                    ),
                    array(
                        'header'=>'Disc',
                        'name'=>'discount',
                        'type'=>'raw',
                        'value'=>'$data->totaldiscount',
                    ),
                    array(
                        'header'=>'Pembebasan',
                        'name'=>'pembebasan',
                        'type'=>'raw',
                        'value'=>'$data->totalpembebasan',
                    ),
                    array(
                        'header'=>'Jumlah Pembayaran',
                        'name'=>'jumlah_pembayaran',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
                    ),
                ),
                'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            ));
        ?>
    <!--</div>--> 
</div>