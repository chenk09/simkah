<?php
if (isset($data['caraPrint'])){
    if($data['caraPrint'] == 'EXCEL')
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$data['judulLaporan'].'-'.date("Y/m/d").'.xls"');
        header('Cache-Control: max-age=0');
        $headerDefault = "";
    }
    
    if($data['caraPrint'] == 'PDF'){
        $headerDefault = $this->renderPartial('application.views.headerReport.headerDefault', array('width'=>1024));
    }
    
    if($data['caraPrint'] == 'PRINT'){
        $headerDefault = $this->renderPartial('application.views.headerReport.headerDefault');
    }
}
?>
<table>
    <tr>
        <td><?php echo $headerDefault; ?></td>
    </tr>
    <tr>
        <td align="center" style="padding: 20px;">
            <div><b><?php echo $data['judulLaporan']; ?></b></div>
            <div>Periode : <?php echo date("d-m-Y", strtotime($model->tglAwal)); ?> s/d <?php echo date("d-m-Y", strtotime($model->tglAkhir)); ?></div>
        </td>
    </tr>
    <tr>
        <td>
            <?php
                $dataProvider = null;
                $dataProvider = $model->searchPrintPasienBerdasarkanPenjamin(false);
                $this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
                'id'=>'pasiensudahbayarp3-grid',
                'dataProvider'=>$dataProvider,
                'template'=>"{items}",
                'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                'columns'=>array(
                   array(
                        'header'=>'Tgl Bukti Bayar',
                        'name'=>'tglbuktibayar',
                        'type'=>'raw',
                        'value'=>'$data->tandabuktibayar->tglbuktibayar."<br>".$data->tandabuktibayar->nobuktibayar',
                    ),
                    array(
                        'name'=>'instalasi',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                    ),
                    array(
                        'header'=>'No Pendaftaran / No Rekam Medik',
                        'value'=>'$data->pendaftaran->no_pendaftaran." / ".$data->pasien->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->pasien->namaNamaBin',
                    ),
                    array(
                        'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->pasien->alamat_pasien',
                    ),
                    array(
                        'header'=>'Cara Bayar | Penjamin',
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
                    ),
                    array(
                        'name'=>'total_tagihan',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
                    ),
                    array(
                        'header'=>'Subsidi Asuransi',
                        'name'=>'subsidi_asuransi',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidiasuransi',
                    ),
                    array(
                        'header'=>'Subsidi Pemerintah',
                        'name'=>'subsidi_pemerintah',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidipemerintah',
                    ),
                    array(
                        'header'=>'Subsidi RS / Klinik',
                        'name'=>'subsidi_rs',
                        'type'=>'raw',
                        'value'=>'$data->totalsubsidirs',
                    ),
                    array(
                        'header'=>'Biaya',
                        'name'=>'iur_biaya',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
                    ),
                    array(
                        'header'=>'Disc',
                        'name'=>'discount',
                        'type'=>'raw',
                        'value'=>'$data->totaldiscount',
                    ),
                    array(
                        'header'=>'Pembebasan',
                        'name'=>'pembebasan',
                        'type'=>'raw',
                        'value'=>'$data->totalpembebasan',
                    ),
                    array(
                        'header'=>'Jumlah Pembayaran',
                        'name'=>'jumlah_pembayaran',
                        'type'=>'raw',
                        'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
                    ),
                ),
                'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            ));
            ?>            
        </td>
    </tr>
</table>