<?php
/**
* modules/billingKasir/Laporan/LaporanPasienSudahBayar&modulId=19
* Updated by    : Miranitha
* Date          : 1-05-2014
* Issue         : EHJ-1749
* Deskripsi     : view Index yang berisi tab laporan
**/
?>

<?php
    $this->breadcrumbs=array(
        'falaporan Ms'=>array('index'),
        'Manage',
    );
?>
<legend class="rim2">Laporan Pasien Sudah Bayar</legend>
<br><br>
<?php $this->renderPartial('pasienSudahBayar/_tabMenu',array()); ?>
<?php $this->renderPartial('pasienSudahBayar/_jsFunctions',array()); ?>
<div>
    <iframe id="frame" src="" width='100%' frameborder="0" style="overflow-y:scroll" ></iframe>
</div>