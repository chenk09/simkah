<?php
$view = Yii::app()->controller->action->id;
Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('tableTrans',{
        data: $(this).serialize()
    });
    return false;
});
");
?>
<legend class="rim2">Laporan Jasa Rumah Sakit</legend>
<div class="search-form">
    <?php $this->renderPartial('laporanJasaRumahSakit/_search',
		array(
			'model'=>$model,
		)
	); ?>
</div>
<div class="tab">
    <?php
        $this->widget('bootstrap.widgets.BootMenu',array(
            'type'=>'tabs',
            'stacked'=>false,
            'htmlOptions'=>array('id'=>'tabmenu'),
            'items'=>array(
                array(
					'label'=>'Jasa Sarana',
					'url'=>array('laporanJasaRumahSakit'),
					'active'=>($view == 'laporanJasaRumahSakit' ? true : false )
				),
                array(
					'label'=>'Jasa Paramedis',
					'url'=>array('laporanParamedisRumahSakit'),
					'active'=>($view == 'laporanParamedisRumahSakit' ? true : false )
				),
                array(
					'label'=>'Jasa Remun',
					'url'=>array('laporanRemunRumahSakit'),
					'active'=>($view == 'laporanRemunRumahSakit' ? true : false )
				)
            ),
        ))
    ?>
</div>

<?php
	if(file_exists(__DIR__ . "\/" . $view . ".php"))
	{
		$this->renderPartial('laporanJasaRumahSakit/' . $view,
			array(
				'model'=>$model,
			)
		);
	}
?>