<style>
    table{
        margin-bottom: 0px;
    }
    .form-actions{
        padding:4px;
        margin-top:5px;
    }
    #ruangan label{
        width: 120px;
        display:inline-block;
    }
    .nav-tabs>li>a{display:block; cursor:pointer;}
    td label.checkbox{
        width: 150px;
        display:inline-block;

    }

    .checkbox.inline + .checkbox.inline{
        margin-left:0px;
    }    
</style>

<div class="search-form" style="">
    <?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
        'type' => 'horizontal',
        'id' => 'searchLaporan',
        'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
            ));
    ?>
<legend class="rim"><i class="icon-search"></i> Pencarian berdasarkan : </legend>
<table width="100%">
    <tr>
        <td>
            <div class="control-group ">
                <div class='control-label'>Tanggal Awal</div>
                <div class='controls'>
                    <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAwal',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                    ?>                    
                </div>
            </div>          
        </td>
        <td>
            <div class="control-group ">
                <div class='control-label'>Tanggal Akhir</div>
                <div class='controls'>
                    <?php
                        $this->widget('MyDateTimePicker', array(
                            'model' => $model,
                            'attribute' => 'tglAkhir',
                            'mode' => 'datetime',
                            'options' => array(
                                'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                            ),
                            'htmlOptions' => array('readonly' => true,
                                'onkeypress' => "return $(this).focusNextInputField(event)"),
                        ));
                    ?>                    
                </div>
            </div>
        </td>
    </tr>
</table>
<div class="form-actions">
    <div style="float:left;margin-right:6px;">
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
    <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
    </div>
    <div style="float:left;margin-right:6px;">
        <?php
            $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
            $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai    
            $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanKeseluruhan');

            $this->widget('bootstrap.widgets.BootButtonGroup', array(
                'type'=>'primary',
                'buttons'=>array(
                    array(
                        'label'=>'Print',
                        'icon'=>'icon-print icon-white',
                        'url'=>$urlPrint,
                        'htmlOptions'=>
                            array(
                                'onclick'=>'print(\'PRINT\');return false;'
                            )
                   ),
                    array(
                        'label'=>'',
                        'items'=>array(
                            array('label'=>'PDF', 'icon'=>'icon-book', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'PDF\');return false;')),
                            array('label'=>'Excel','icon'=>'icon-pdf', 'url'=>$urlPrint, 'itemOptions'=>array('onclick'=>'print(\'EXCEL\');return false;')),
                        )
                    ),
                ),
            ));              
         ?>
    </div>
    <div style="clear:both;"></div>
</div>
<?php
    $this->endWidget();
?>
</div>
<script type="text/javascript">
    function cek_all_penjamin(obj){
        if($(obj).is(':checked')){
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", "checked");
        }else{
            $("#penjamin").find("input[type=\'checkbox\']").attr("checked", false);
        }
    }
</script>