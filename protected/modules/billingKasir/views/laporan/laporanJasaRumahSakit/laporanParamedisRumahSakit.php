<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridViewNonRp';
    $sort = false;
    $dataProvider = $model->cariData();
    $template = "{pager}{summary}\n{items}";
?>
<?php $this->widget($table,
array(
	'id'=>'tableTrans',
	'dataProvider'=>$dataProvider,
	'template'=>$template,
	'enableSorting'=>$sort,
	'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'mergeColumns' => array('no_rekam_medik'),
	'columns'=>array(
		"no_rekam_medik",
		"nama_pasien",
		"no_pendaftaran",
		"penjamin_nama",
		"daftartindakan_nama",
		"tarif_tindakankomp"
	),
	'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)
); ?>  