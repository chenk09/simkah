<?php 
    $table = 'ext.bootstrap.widgets.BootGridView';
    $data = $model->searchTable();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    $style = '';
    if (isset($caraPrint)){
      $style = '';
      $sort = false;
      $data = $model->searchPrint();  
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>

    <div id="div_global">
        <legend class="rim">Tabel Rekap Transaksi</legend>
        <div style="<?php echo $style; ?>">
            <?php 
            $this->widget($table,array(
                'id'=>'laporanrekaptransaksi-grid',
                'dataProvider'=>$data,
                'enableSorting'=>$sort,
                'template'=>$template,
                    'itemsCssClass'=>'table table-striped table-bordered table-condensed',
                    'columns'=>array(
            //            'instalasi_nama',
                        array(
                            'header' => 'No',
                            'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1'
                        ),
                        array(
                           'header'=>'No Rekam Medik',
                           'type'=>'raw',
                           'value'=>'$data->no_rekam_medik',
                        ),
                        array(
                           'header'=>'Tgl Pendaftaran',
                           'type'=>'raw',
                           'value'=>'$data->tgl_pendaftaran',
                        ),
                        array(
                           'header'=>'No Pendaftaran',
                           'type'=>'raw',
                           'value'=>'$data->no_pendaftaran',
                        ),
                        array(
                           'header'=>'Nama Pasien',
                           'type'=>'raw',
                           'value'=>'$data->nama_pasien',
                        ),
                        array(
                           'header'=>'Tindakan',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifTindakan",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Visit',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifVisit",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Konsul',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifKonsul",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'ADM <br/> UMUM',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifAdm",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Farmasi',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifFarmasi",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'LAB',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifLab",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'RAD',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifRad",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Bedah',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifBedah",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Ruangan',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifRuangan",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Obsgyn',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifObsgyn",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Gizi',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifGizi",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Laundry',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifLaundry",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',  
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Pulasara',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifPulasara",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Ambulance',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifAmbulance",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Lainnya',
                           'type'=>'raw',
                           'value'=>'$this->grid->owner->renderPartial("billingKasir.views.laporan/rekapTransaksiBedah/_tarifLainnya",array(pendaftaran_id=>"$data->pendaftaran_id",tgl_pendaftaran=>"$data->tgl_pendaftaran"),true)',
                            'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                        array(
                           'header'=>'Total',
                           'type'=>'raw',
                           'value'=>'(empty($data->total) ? "0" : MyFunction::formatNumber($data->total))',
                           'htmlOptions'=>array('style'=>'text-align:right;'),
                        ),
                    ),
                    'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
            )); 
            ?>
        </div>
    </div>

<br/>