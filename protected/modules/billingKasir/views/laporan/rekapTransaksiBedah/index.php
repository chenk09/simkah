<legend class="rim2">Laporan Rekap Transaksi</legend>
<?php

$url = Yii::app()->createUrl(Yii::app()->controller->module->id.'/laporan/frameGrafikLaporanRekapTransaksi&id=1');
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('#searchLaporan').submit(function(){
    $('#Grafik').attr('src','').css('height','0px');
     $('#laporanrekaptransaksi-grid').addClass('srbacLoading');
    $.fn.yiiGridView.update('laporanrekaptransaksi-grid', {
            data: $(this).serialize()
    });
//    $.fn.yiiGridView.update('ugd_laporanrekaptransaksi-grid', {
//            data: $(this).serialize()
//    });
//    $.fn.yiiGridView.update('rj_laporanrekaptransaksi-grid', {
//            data: $(this).serialize()
//    });
//    $.fn.yiiGridView.update('ri_laporanrekaptransaksi-grid', {
//            data: $(this).serialize()
//    });
    return false;
});
");
?>

<div class="search-form">
<?php $this->renderPartial('rekapTransaksiBedah/_search',array(
    'model'=>$model, 'filter'=>$filter
)); ?>
</div><!-- search-form --> 
<div>
    <?php
//        $this->widget('bootstrap.widgets.BootMenu',array(
//            'type'=>'tabs',
//            'stacked'=>false,
//            'htmlOptions'=>array('id'=>'tabmenu'),
//            'items'=>array(
//                array('label'=>'Global','url'=>'javascript:tab(0);','active'=>true),
//                array('label'=>'Per UGD','url'=>'javascript:tab(1);', 'itemOptions'=>array("index"=>1)),
//                array('label'=>'Per Rawat Jalan','url'=>'javascript:tab(2);', 'itemOptions'=>array("index"=>2)),
//                array('label'=>'Per Rawat Inap','url'=>'javascript:tab(3);', 'itemOptions'=>array("index"=>3)),
//            ),
//        ))
    ?>
</div>

<fieldset> 
    <?php $this->renderPartial('rekapTransaksiBedah/_table', array('model'=>$model)); ?>
    <?php //$this->renderPartial('_tab'); ?>
    <iframe src="" id="Grafik" width="100%" height='0'  onload="javascript:resizeIframe(this);">
    </iframe>        
</fieldset>
<?php 
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
    $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/printLaporanRekapTransaksi');
    $this->renderPartial('_footer', array('urlPrint'=>$urlPrint, 'url'=>$url)); 
?>

<?php
$js= <<< JS
    $(document).ready(function() {
        $("#tabmenu").children("li").children("a").click(function() {
            $("#tabmenu").children("li").attr('class','');
            $(this).parents("li").attr('class','active');
            $(".icon-pencil").remove();
            $(this).append("<li class='icon-pencil icon-white' style='float:left'></li>");
        });
        
        $("#div_global").show();
        $("#div_ugd").hide();
        $("#div_rj").hide();
        $("#div_ri").hide();
    });

    function tab(index){
        $(this).hide();
        if (index==0){
            $("#filter_tab").val('global');
            $("#div_global").show();
            $("#div_ugd").hide();
            $("#div_rj").hide();        
            $("#div_ri").hide();        
        } else if (index==1){
            $("#filter_tab").val('ugd');
            $("#div_global").hide();
            $("#div_ugd").show();
            $("#div_rj").hide();
            $("#div_ri").hide();
        } else if (index==2){
            $("#filter_tab").val('rj');
            $("#div_global").hide();
            $("#div_ugd").hide();
            $("#div_rj").show();  
            $("#div_ri").hide();  
        } else if (index==3){
            $("#filter_tab").val('ri');
            $("#div_global").hide();
            $("#div_ugd").hide();
            $("#div_rj").hide();        
            $("#div_ri").show();        
        }
   }
function onReset()
{
    setTimeout(
        function(){
            $.fn.yiiGridView.update('laporanrekaptransaksi-grid', {
                data: $("#caripasien-form").serialize()
            });
            $.fn.yiiGridView.update('ugd_laporanrekaptransaksi-grid', {
                data: $("#caripasien-form").serialize()
            });
            $.fn.yiiGridView.update('rj_laporanrekaptransaksi-grid', {
                data: $("#caripasien-form").serialize()
            });        
            $.fn.yiiGridView.update('ri_laporanrekaptransaksi-grid', {
                data: $("#caripasien-form").serialize()
            });        
        }, 2000
    );
    return false;
}   
JS;
Yii::app()->clientScript->registerScript('pencatatanriwayat',$js,CClientScript::POS_HEAD);
?>