<?php
    if(empty($pendaftaran_id)){
        echo '-';
    }else{
        $modTarif = LaporanrekaptransaksiV::model()->findAllByAttributes(array(
            'pendaftaran_id'=>$pendaftaran_id,
            'tgl_pendaftaran'=>$tgl_pendaftaran
        ));
        $modTarifKonsul  = $modTarif->daftartindakan_karcis;
        $totTarif = 0;

        foreach($modTarif as $key=>$tarif){
            if($tarif->ruangan_id != PARAMS::RUANGAN_ID_OBSGYN && $tarif->ruangan_id != Params::RUANGAN_ID_GIZI && $tarif->ruangan_id == Params::RUANGAN_ID_LAB || $tarif->ruangan_id == Params::RUANGAN_ID_IBS || $tarif->ruangan_id == Params::RUANGAN_ID_RAD ){
                $kondisi = $tarif->daftartindakan_konsul != true && $tarif->daftartindakan_visite != true
                   && $tarif->daftartindakan_tindakan != true && $tarif->daftartindakan_akomodasi != true
                   && $tarif->kategoritindakan_id != PARAMS::KATEGORITINDAKAN_ID_ADM
                   && $tarif->kategoritindakan_id != PARAMS::KATEGORITINDAKAN_ID_AMBULANCE
                   && $tarif->kelompoktindakan_id != Params::KELOMPOKTINDAKAN_ID_JENAZAH
                   && $tarif->kelompoktindakan_id != Params::KELOMPOKTINDAKAN_ID_LAUNDRY
                   && $tarif->instalasi_id != Params::INSTALASI_ID_FARMASI 
                   && $tarif->ruangan_id != 221
                   && $tarif->ruangan_id != Params::RUANGAN_ID_LAB && $tarif->ruangan_id != Params::RUANGAN_ID_IBS 
                   && $tarif->ruangan_id != Params::RUANGAN_ID_GIZI && $tarif->ruangan_id != Params::RUANGAN_ID_RAD 
                   && $tarif->ruangan_id != PARAMS::RUANGAN_ID_OBSGYN && $tarif->daftartindakan_karcis != true;
            }else{
                if($tarif->kategoritindakan_id != PARAMS::KATEGORITINDAKAN_ID_ADM){
                    $karcis = $tarif->daftartindakan_karcis == true;
                }else{
                    $karcis = $tarif->daftartindakan_karcis != true;
                }
                $kondisi = $tarif->daftartindakan_konsul != true && $tarif->daftartindakan_visite != true
                   && $tarif->daftartindakan_tindakan != true && $tarif->daftartindakan_akomodasi != true
                   && $tarif->kategoritindakan_id != PARAMS::KATEGORITINDAKAN_ID_ADM
                   && $tarif->kategoritindakan_id != PARAMS::KATEGORITINDAKAN_ID_AMBULANCE
                   && $tarif->kelompoktindakan_id != Params::KELOMPOKTINDAKAN_ID_JENAZAH
                   && $tarif->kelompoktindakan_id != Params::KELOMPOKTINDAKAN_ID_LAUNDRY
                   && $tarif->instalasi_id != Params::INSTALASI_ID_FARMASI 
                   && $tarif->ruangan_id != 221
                   && $tarif->ruangan_id != Params::RUANGAN_ID_LAB && $tarif->ruangan_id != Params::RUANGAN_ID_IBS 
                   && $tarif->ruangan_id != Params::RUANGAN_ID_GIZI && $tarif->ruangan_id != Params::RUANGAN_ID_RAD 
                   && $tarif->ruangan_id != PARAMS::RUANGAN_ID_OBSGYN || $karcis;        
           }
           if($kondisi){
               $totTarif += $tarif->tarif_tindakan;
           }else{
           }
       }
       if(isset($caraPrint)){
           echo $totTarif;
       }else{
        echo MyFunction::formatNumber($totTarif);
       }
    }
    
?>