<?php 
$table = 'ext.bootstrap.widgets.HeaderGroupGridView';
$data = $model->searchTable();
$template = "{pager}{summary}\n{items}";
$sort = true;
if (isset($caraPrint)){
    $sort = false;
  $data = $model->searchPrint();  
  $template = "{items}";
  if ($caraPrint == "EXCEL")
      $table = 'ext.bootstrap.widgets.BootExcelGridView';
}
?>
<?php $this->widget($table,array(
    'id'=>'tableLaporan',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'htmlOptions'=>array(
            'style'=>'font-size',
            
        ),
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                array(
                    'header' => 'No',
                    'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'header'=>'No Bukti <br> Bayar',
                    'name'=>'nobuktibayar',
                    'type'=>'raw',
                    'value'=>'$data->nobuktibayar',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),

                ),
//                'nobuktibayar',
                array(
                    'header'=>'Tanggal <br> Bukti Bayar',
                    'type'=>'raw',
                    'value'=>'$data->tglbuktibayar',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
//                'tglbuktibayar',
                array(
                    'header'=>'Cara <br> Pembayaran',
                    'type'=>'raw',
                    'value'=>'$data->carapembayaran',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
//                'carapembayaran',
                'darinama_bkm',
                'alamat_bkm',
                'sebagaipembayaran_bkm',
                array(
                    'name'=>'jmlpembulatan',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'MyFunction::formatNumber($data->jmlpembulatan)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'name'=>'biayaadministrasi',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->biayaadministrasi)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'name'=>'biayamaterai',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->biayamaterai)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'name'=>'jmlpembayaran',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->jmlpembayaran)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'name'=>'uangditerima',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->uangditerima)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                array(
                    'name'=>'uangkembalian',
                    'type'=>'raw',
                    'htmlOptions'=>array('style'=>'text-align:right;'),
                    'value'=>'"Rp. ".MyFunction::formatNumber($data->uangkembalian)',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
//                'jmlpembulatan',
//                'biayaadministrasi',
//                'biayamaterai',
//                'uangditerima',
//                'uangkembalian',
                array(
                    'header'=>'Ruangan Kasir',
                    'value'=>'$data->ruangan_nama',
                    'htmlOptions'=>array('style'=>'font-size:10px;'),
                ),
                'shift_nama',
//                array(
//                        'name'=>'tandabuktibayar_id',
//                        'value'=>'$data->tandabuktibayar_id',
//                        'filter'=>false,
//                ),
//                'ruangan_id',
//                
//                'shift_id',
                
//                'nourutkasir',
                /*
                'nobuktibayar',
                'tglbuktibayar',
                
                'dengankartu',
                'bankkartu',
                'nokartu',
                'nostrukkartu',
                
                
                
                'jmlpembayaran',
                
                
                'keterangan_pembayaran',
                'create_time',
                'update_time',*/
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>