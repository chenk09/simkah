<div class="search-form" style="">
    <?php
        $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm', array(
            'action' => Yii::app()->createUrl($this->route),
            'method' => 'get',
            'type' => 'horizontal',
            'id' => 'searchLaporan',
            'htmlOptions' => array('enctype' => 'multipart/form-data', 'onKeyPress' => 'return disableKeyPress(event)'),
                )); ?>
<style>
    label.checkbox, label.radio{
            width:150px;
            display:inline-block;
        }
</style>
    <legend class="rim"><i class="icon-search"></i> Pencarian Berdasarkan : </legend>
    <table>
        <tr>
            <td>
                Tanggal Masuk Penunjang <?php echo $form->dropDownList($model,'bulan',
                        array(
                            'hari'=>'Hari Ini',
                            'bulan'=>'Bulan',
                            'tahun'=>'Tahun',
                        ),
                        array(
                            'empty'=>'--Pilih--',
                            'id'=>'PeriodeName',
                            'onChange'=>'setPeriode()',
                            'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:120px;',
                        )
                        );
                    ?>
            </td>
            <td width="250px">
                <?php echo CHtml::hiddenField('filter_tab', 'kelas'); ?>
                <?php
                $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAwal',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
            <td width="50px">s/d</td>
            <td>
                <?php
                    $this->widget('MyDateTimePicker', array(
                    'model' => $model,
                    'attribute' => 'tglAkhir',
                    'mode' => 'datetime',
                    'options' => array(
                        'dateFormat' => Params::DATE_FORMAT_MEDIUM,
                    ),
                    'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3','onkeypress' => "return $(this).focusNextInputField(event)"
                    ),
                ));
                ?>
            </td>
        </tr>
    </table>
    <div id="search_kelas">
        <table>
            <tr>
                <td>
                    <?php
                        echo $form->dropDownListRow($model,'kelaspelayanan_id',CHtml::listData(KelaspelayananM::model()->findAll(),'kelaspelayanan_id','kelaspelayanan_nama'),array('empty'=>'--Pilih--','style'=>'width:140px'));
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <div id="search_bangsal">
        <table>
            <tr>
                <td>
                    <?php
                        echo $form->dropDownListRow($model,'ruanganpenunj_id',CHtml::listData(RuanganM::model()->findAll(),'ruangan_id','ruangan_nama'),array('empty'=>'--Pilih--','style'=>'width:140px'));
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-ok icon-white"></i>')), 
                array('class' => 'btn btn-primary', 'type' => 'submit', 'id' => 'btn_simpan')); ?>
	<?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                $this->createUrl('laporan/LaporanKinerjaPerBangsal'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
    </div> 
</div>    
<?php
    $this->endWidget();
    $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
    $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai ?>
<?php Yii::app()->clientScript->registerScript('cekAll','
  $("#content4").find("input[type=\'checkbox\']").attr("checked", "checked");
',  CClientScript::POS_READY);
?>
<?php
$urlPeriode = Yii::app()->createUrl('actionAjax/GantiPeriode');
$js = <<< JSCRIPT

function setPeriode(){
    namaPeriode = $('#PeriodeName').val();
    
        $.post('${urlPeriode}',{namaPeriode:namaPeriode},function(data){
            $('#BKLaporankinerjapenunjangV_tglAwal').val(data.periodeawal);
            $('#BKLaporankinerjapenunjangV_tglAkhir').val(data.periodeakhir);
        },'json');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('setPeriode',$js,CClientScript::POS_HEAD);
?>
<script>
    function checkPilihan(event){
            var namaPeriode = $('#PeriodeName').val();

            if(namaPeriode == ''){
                alert('Pilih Kategori Pencarian');
                event.preventDefault();
                $('#dtPicker3').datepicker("hide");
                return true;
                ;
            }
        }
</script>


