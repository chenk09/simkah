<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchJasaDokter();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchJasaDokter(false); 
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>
<?php 
$this->widget($table,array(
    'id'=>'laporanrekapjasadokter-grid',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
    'mergeHeaders'=>array(
            array(
                'name'=>'<center>Dokter</center>',
                'start'=>1, //indeks kolom 3
                'end'=>3, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Bedah</center>',
                'start'=>7, //indeks kolom 3
                'end'=>9, //indeks kolom 4
            ),
    ),
        'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'htmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
              'header'=>'Gelar Depan',
              'type'=>'raw',
              'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
            ),
            array(
              'header'=>'Nama Dokter',
              'type'=>'raw',
              'name'=>'nama_pegawai',
              'value'=>'(empty($data->nama_pegawai) ? "-" : "$data->nama_pegawai" )',
            ),
            array(
              'header'=>'Gelar Belakang',
              'type'=>'raw',
              'value'=>'(empty($data->gelarbelakang_nama) ? "-" : "$data->gelarbelakang_nama" )',
            ),
            array(
              'header'=>'Visite',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"visite"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Konsul',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"konsul"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Tindakan',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumTindakan(array("pegawai")))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Jasa Operator',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"jasaoperator"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Sewa Alat',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"sewaalat"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Alat Bahan',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"alatbahan"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Jasa Lain-lain',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai"),"lainnya"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); 
?>
