<?php 
    $table = 'ext.bootstrap.widgets.HeaderGroupGridView';
    $data = $model->searchDetailJasaDokter();
    $template = "{pager}{summary}\n{items}";
    $sort = true;
    if (isset($caraPrint)){
      $sort = false;
      $data = $model->searchDetailJasaDokter(false); 
      $template = "{items}";
      if ($caraPrint == "EXCEL")
          $table = 'ext.bootstrap.widgets.BootExcelGridView';
    }
?>
<?php 
$this->widget($table,array(
    'id'=>'laporandetailjasadokter-grid',
    'dataProvider'=>$data,
    'enableSorting'=>$sort,
    'template'=>$template,
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
         'mergeHeaders'=>array(
            array(
                'name'=>'<center>Dokter</center>',
                'start'=>7, //indeks kolom 3
                'end'=>9, //indeks kolom 4
            ),
            array(
                'name'=>'<center>Bedah</center>',
                'start'=>12, //indeks kolom 3
                'end'=>15, //indeks kolom 4
            ),
        ),
        'columns'=>array(
            array(
                'header' => 'No',
                'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                'htmlOptions'=>array('style'=>'text-align:center;'),
            ),
            array(
              'header'=>'Tgl Transaksi',
              'type'=>'raw',
              'value'=>'$data->tgl_pendaftaran',
            ),
            array(
              'header'=>'No Rekam Medik',
              'type'=>'raw',
              'value'=>'$data->no_rekam_medik',
            ),
            array(
              'header'=>'Nama Lengkap',
              'type'=>'raw',
              'value'=>'$data->nama_pasien',
            ),
            array(
              'header'=>'Unit Pelayanan',
              'type'=>'raw',
              'value'=>'$data->instalasi_nama',
            ),
            array(
              'header'=>'Nama Ruangan',
              'type'=>'raw',
              'value'=>'$data->ruangan_nama',
            ),
            array(
              'header'=>'Jumlah',
              'type'=>'raw',
              //'value'=>'$data->qty_tindakan',
				'value'=>'MyFunction::formatNumber($data->getSumTindakan(array("pegawai","pendaftaran","ruangan"),"qty_tindakan"))',
            ),
            array(
              'header'=>'Gelar Depan',
              'type'=>'raw',
              'value'=>'(empty($data->gelardepan) ? "-" : "$data->gelardepan" )',
            ),
            array(
              'header'=>'Nama Dokter',
              'type'=>'raw',
              'value'=>'(empty($data->nama_pegawai) ? "-" : "$data->nama_pegawai" )',
            ),
            array(
              'header'=>'Gelar Belakang',
              'type'=>'raw',
              'value'=>'(empty($data->gelarbelakang_nama) ? "-" : "$data->gelarbelakang_nama" )',
            ),
            array(
              'header'=>'Visite',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"visite"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Konsul',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"konsul"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Tindakan',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumTindakan(array("pegawai","pendaftaran","ruangan"),"tarif_satuan"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Jasa Operator',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"jasaoperator"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Sewa Alat',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"sewaalat"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Alat Bahan',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"alatbahan"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
            array(
              'header'=>'Jasa Lain-lain',
              'type'=>'raw',
              'value'=>'MyFunction::formatNumber($data->getSumKomponen(array("pegawai","pendaftaran","ruangan"),"lainnya"))',
              'htmlOptions'=>array('style'=>'text-align:right;'),
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    )); 
?>