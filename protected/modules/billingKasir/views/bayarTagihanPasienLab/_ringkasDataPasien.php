<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim2">Transaksi Bayar <?php echo ((strtolower($this->action->id) == $this->actionBayarTagihanPasien) ? 'Tagihan Pasien' : 'Resep Pasien');?> </legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('LKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
            <label class="no_rek control-label">No Rekam Medik</label>
            </td>
            <td>
                <?php //echo CHtml::textField('FAPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                   if (Yii::app()->controller->module->id =='laboratorium') {
                     $pasien = 'daftarPasienRuangan';
                 }else{
                     $pasien = 'daftarPasien';
                 }
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'LKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/'.$pasien.'').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                loadPembayaran(ui.item.pendaftaran_id);
                                                return false;
                                            }',
                                    ),
                                )); 
                ?>
            </td>
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('LKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td><?php //echo CHtml::textField('FAPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); 
                    $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'LKPasienM[nama_pasien]',
                                           'value'=>$modPasien->nama_pasien,
                                           'source'=>'js: function(request, response) {
                                                          $.ajax({
                                                              url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienberdasarkanNama').'",
                                                              dataType: "json",
                                                              data: {
                                                                  '.strtolower($pasien).':true,
                                                                  term: request.term,
                                                              },
                                                              success: function (data) {
                                                                      response(data);
                                                              }
                                                          })
                                                       }',
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       isiDataPasien(ui.item);
                                                       loadPembayaran(ui.item.pendaftaran_id);
                                                       return false;
                                                   }',
                                           ),
                                       )); 
                   ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            <?php if ((strtolower($this->action->id) == $this->actionBayarTagihanPasien)) { ?>
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('FAPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
            <?php } else { 
                echo '<td>'. CHtml::label('No Resep','',array('class'=>'control-label no_rek')).'</td><td>';
                $this->widget('MyJuiAutoComplete', array(
                               'name'=>'no_resep',
                                'id'=>'no_resep',
//                               'value'=>$modPasien->nama_pasien,
                               'source'=>'js: function(request, response) {
                                              $.ajax({
                                                  url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/getNoResepObatPasien').'",
                                                  dataType: "json",
                                                  data: {
                                                      pendaftaran_id : $("#FAPendaftaranT_pendaftaran_id").val(),
                                                      term: request.term,
                                                  },
                                                  success: function (data) {
                                                          response(data);
                                                  }
                                              })
                                           }',
                                'options'=>array(
                                      'showAnim'=>'fold',
                                      'minLength' => 2,
                                      'focus'=> 'js:function( event, ui ) {
                                           $(this).val(ui.item.value);
                                           return false;
                                       }',
                                      'select'=>'js:function( event, ui ) {
                                           loadPembayaran($("#FAPendaftaranT_pendaftaran_id").val(), ui.item.penjualanresep_id);
                                           return false;
                                       }',
                               ),
//                               'htmlOptions'=>array(
//                                   'onblur'=>'loadPembayaran($("#FAPendaftaranT_pendaftaran_id").val())',
//                                   'onkeypress' =>"if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){loadPembayaran($('#FAPendaftaranT_pendaftaran_id').val());}}",
//                               ),
                           )); 
                echo '<td>';
            }
            
            ?>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('LKPendaftaranT[jeniskasuspenyakit_nama]',  ((isset($modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama)) ? $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama : null), array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('LKPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('LKPendaftaranT[instalasi_nama]',  ((isset($modPendaftaran->instalasi->instalasi_nama)) ? $modPendaftaran->instalasi->instalasi_nama : null), array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('LKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('LKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('LKPendaftaranT[ruangan_nama]', ((isset($modPendaftaran->ruangan->ruangan_nama)) ? $modPendaftaran->ruangan->ruangan_nama : null), array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset> 

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#LKPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
    $('#LKPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
    $('#LKPendaftaranT_umur').val(data.umur);
    $('#LKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#LKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#LKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#LKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#LKPendaftaranT_pasien_id').val(data.pasien_id);
    if (typeof data.norekammedik !=  'undefined'){
        $('#FAPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#LKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#LKPasienM_nama_pasien').val(data.namapasien);
    $('#LKPasienM_nama_bin').val(data.namabin);
}

function loadPembayaran(idPendaftaran,penjualanresep)
{
    
    <?php $tindakanParam = ((strtolower($this->action->id) == $this->actionBayarTagihanPasien) ? ',tindakan : 1' : ',tindakan : 0');?>
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaran');?>', {idPendaftaran:idPendaftaran,penjualanResep : penjualanresep<?php echo $tindakanParam;?>}, function(data){
        $('#tblBayarTind tbody').html(data.formBayarTindakan);
        $('#tblBayarOA tbody').html(data.formBayarOa);
        $('#TandabuktibayarT_jmlpembayaran').val(formatUang(data.jmlpembayaran));
        $('#totTagihan').val(formatUang(data.tottagihan));
        
        $('#TandabuktibayarT_jmlpembulatan').val(formatUang(data.jmlpembulatan));
        $('#TandabuktibayarT_uangditerima').val(formatUang(data.uangditerima));
        $('#TandabuktibayarT_uangkembalian').val(formatUang(data.uangkembalian));
        $('#TandabuktibayarT_biayamaterai').val(formatUang(data.biayamaterai));
        $('#TandabuktibayarT_biayaadministrasi').val(formatUang(data.biayaadministrasi));
        
        var norekammedik = $('#LKPasienM_no_rekam_medik').val();
        var no_pendaftaran = $('#LKPendaftaranT_no_pendaftaran').val();
        var nama_pembayar = norekammedik + '-' + no_pendaftaran + '-' + data.namapasien;
        
        if(penjualanresep != undefined)
        {
            var no_resep = $('#no_resep').val();
            nama_pembayar = norekammedik + '-' + no_resep + '-' + data.namapasien;
        }
//        $('#TandabuktibayarT_darinama_bkm').val(data.namapasien);
        $('#TandabuktibayarT_darinama_bkm').val(nama_pembayar);
        $('#TandabuktibayarT_alamat_bkm').val(data.alamatpasien);
        
        var discount = 0;
        discount = unformatNumber($('#totaldiscount_tindakan').val()) + unformatNumber($('#totaldiscount_oa').val());
        $('#disc').val(0);
        $('#discount').val(0);
        $('#tblBayarTind').find('input.currency:text').each(function(){
            $(this).maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        });
        $('#tblBayarOA').find('input.currency:text').each(function(){
            $(this).maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
        });
        $('.currency').each(function(){this.value = formatUang(this.value)});
        hitungTotalSemuaTind();
        hitungTotalSemuaOa();
        //$('#tblBayarOA').find('input.currency:text').each(function(){this.value = formatNumber(this.value)});
    }, 'json');
}
</script>