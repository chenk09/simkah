<?php
$this->breadcrumbs=array(
	'Daftar Pasien'=>array('/billingKasir/daftarPasien'),
	'PasienKarcis',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Pasien Deposit </legend>
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchPasienDeposit(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'name'=>'tgluangmuka',
                'type'=>'raw',
                'value'=>'$data->tgluangmuka',
            ),
            array(
                'name'=>'instalasi',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
            ),
            array(
                'header'=>'No. Pendaftaran<br>No. RM',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->no_pendaftaran . " <br> " . $data->pasien->no_rekam_medik',
            ),
            array(
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->pasien->nama_pasien',
            ),
            array(
                'header'=>'Alias',
                'name'=>'nama_bin',
                'type'=>'raw',
                'value'=>'$data->pasien->nama_bin',
            ),
            array(
                'header'=>'Cara Bayar',
                'name'=>'carabayar_nama',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->carabayar->carabayar_nama',
            ),
            array(
                'header'=>'Penjamin',
                'name'=>'penjamin_nama',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->penjamin->penjamin_nama',
            ),
            array(
                'name'=>'jumlahuangmuka',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->jumlahuangmuka)',
                'htmlOptions'=>array('style'=>'text-align: left; width:80px')
            ),
            array(
                'name'=>'keteranganuangmuka',
                'type'=>'raw',
                'value'=>'$data->keteranganuangmuka',
            ),
//            array(
//                'header'=>'Kasus Penyakit',
//                'name'=>'jeniskasuspenyakit_nama',
//                'type'=>'raw',
//                'value'=>'$data->pendaftaran->kasuspenyakit->jeniskasuspenyakit_nama',
//            ),
//            array(
//                'name'=>'umur',
//                'type'=>'raw',
//                'value'=>'$data->pendaftaran->umur',
//            ),
//            array(
//                'header'=>'Alamat',
//                'name'=>'alamat_pasien',
//                'type'=>'raw',
//                'value'=>'$data->pasien->alamat_pasien',
//            ),
            array(
                'header'=>'Sisa Uang Muka',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->sisauangmuka)',
                'htmlOptions'=>array('style'=>'text-align: left; width:80px')
            ),
            array(
                'header'=>'Kwitansi Deposit Pasien',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->controller->createUrl("kwitansi/viewDeposit",array("idBayarUangMuka"=>$data->bayaruangmuka_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeKwitansi",
                                  "onclick"=>"$(\"#dialogKwitansi\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk cetak Kwitansi",
                            ))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Pembatalan',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("pembatalanUangMukaNew/index",array("idBayarUangMuka"=>$data->bayaruangmuka_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogBatalUangMuka\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk membatalkan uang muka",
                            ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),            
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
</fieldset>
<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?> 

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKwitansi',
    'options'=>array(
        'title'=>'Kwitansi Deposit Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeKwitansi" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogBatalUangMuka',
    'options'=>array(
        'title'=>'Pembatalan Uang Muka',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePembayaran" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>

<?php 
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dialogEditUangMuka',
            'options'=>array(
                'title'=>'Edit Uang Muka',
                'autoOpen'=>false,
                'modal'=>true,
                'minWidth'=>980,
                'minHeight'=>610,
                'resizable'=>true,
            ),
        )
    );
?>
<iframe src="" name="iframeUangMuka" width="100%" height="550" ></iframe>
<?php
    $this->endWidget();
?>