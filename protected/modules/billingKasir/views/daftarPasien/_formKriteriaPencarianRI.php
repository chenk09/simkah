<fieldset>
    <legend class="rim"><?php echo  Yii::t('mds','Search Patient') ?></legend>
    <table class="table-condensed">
        <tr>
            <td>
                <div class="control-group ">
                    <?php $model->tglAwal = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwal, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                    <?php echo CHtml::label('Tgl Pasien Pulang','tglpulang', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAwal',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>
                <div class="control-group ">
                    <?php $model->tglAkhir = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhir, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                    <?php echo CHtml::label('Sampai Dengan','sampaiDengan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$model,
                                                'attribute'=>'tglAkhir',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                                    'minDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>
                        
                    </div>
                </div>

                <div class="control-group ">
                    <label for="namaPasien" class="control-label">
                        <?php echo CHtml::activecheckBox($model, 'ceklis', array('uncheckValue'=>0,'rel'=>'tooltip', 'onClick'=>'cekTanggal()','data-original-title'=>'Cek untuk pencarian berdasarkan tanggal')); ?>
                        Tgl Admisi 
                    </label>
                    <div class="controls">
                        <?php $model->tglAwalAdmisi = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAwalAdmisi, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                        <?php $format = new CustomFormat;
                            $this->widget('MyDateTimePicker',array(
                                'model'=>$model,
                                'attribute'=>'tglAwalAdmisi',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'disabled'=>true),
                        )); 
                        ?>
                    </div> 
                    <?php echo CHtml::label(' Sampai Dengan',' s/d', array('class'=>'control-label')) ?>

                    <div class="controls"> 
                        <?php $model->tglAkhirAdmisi = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($model->tglAkhirAdmisi, 'yyyy-MM-dd hh:mm:ss'),'medium','medium'); ?>
                        <?php   
                            $this->widget('MyDateTimePicker',array(
                                'model'=>$model,
                                'attribute'=>'tglAkhirAdmisi',
                                'mode'=>'datetime',
                                'options'=> array(
                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    'maxDate' => 'd',
                                ),
                                'htmlOptions'=>array('readonly'=>true,'class'=>'dtPicker3', 'disabled'=>true),
                        )); ?>
                    </div>
                </div>
                
            </td>
            <td>
                <?php echo $form->textFieldRow($model,'no_rekam_medik',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <?php echo $form->textFieldRow($model,'no_pendaftaran',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>

                <?php echo $form->textFieldRow($model,'nama_pasien',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                <div class="control-group">
                    <?php echo CHtml::label('Alias', 'nama_bin', array('class'=>'control-label')); ?>
                    <div class="controls">
                    <?php echo $form->textField($model,'nama_bin',array('class'=>'span3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                    </div>
                </div>

            </td>
        </tr>
    </table>
</fieldset>

<script type="text/javascript">
    
    document.getElementById('BKInformasikasirinappulangV_tglAwalAdmisi_date').setAttribute("style","display:none;");
    document.getElementById('BKInformasikasirinappulangV_tglAkhirAdmisi_date').setAttribute("style","display:none;");
    function cekTanggal(){
        
        var checklist = $('#BKInformasikasirinappulangV_ceklis');
        var pilih = checklist.attr('checked');
        // var tgl_masuk = $(document)
        if(pilih){
            document.getElementById('BKInformasikasirinappulangV_tglAwalAdmisi').disabled = false;
            document.getElementById('BKInformasikasirinappulangV_tglAkhirAdmisi').disabled = false;
            document.getElementById('BKInformasikasirinappulangV_tglAwalAdmisi_date').setAttribute("style","display:block;");
            document.getElementById('BKInformasikasirinappulangV_tglAkhirAdmisi_date').setAttribute("style","display:block;");
        }else{
            document.getElementById('BKInformasikasirinappulangV_tglAwalAdmisi').disabled = true;
            document.getElementById('BKInformasikasirinappulangV_tglAkhirAdmisi').disabled = true;
            document.getElementById('BKInformasikasirinappulangV_tglAwalAdmisi_date').setAttribute("style","display:none;");
            document.getElementById('BKInformasikasirinappulangV_tglAkhirAdmisi_date').setAttribute("style","display:none;");
        }
    }

</script>