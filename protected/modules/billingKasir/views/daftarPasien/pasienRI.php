<?php
$this->breadcrumbs=array(
	'Daftar Pasien'=>array('/billingKasir/daftarPasien'),
	'PasienRI',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
if(isset($_GET['statusfarmasi'])){
    if($_GET['statusfarmasi'] == 0){
        Yii::app()->user->setFlash("error","Alat & Obat Kesehatan belum di verifikasi di Farmasi. Silahkan Hubungi Ke Bagian Farmasi");
    }
}
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php 


//Yii::app()->clientScript->registerScript('cariPasien', "
//$('#caripasien-form').submit(function(){
//	$.fn.yiiGridView.update('pencarianpasien-grid', {
//		data: $(this).serialize()
//	});
//	return false;
//});
//");
?>
<legend class="rim2">Informasi Pasien Rawat Inap</legend>
<?php echo $this->renderPartial('_tablePasienRI', array('modRI'=>$modRI),true);  ?> 
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
)); ?>
<?php echo $this->renderPartial('_formKriteriaPencarianRI', array('model'=>$modRI,'form'=>$form),true);  ?> 

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit','ajax' => array(
                 'type' => 'GET', 
                 'url' => array("/".$this->route), 
                 'update' => '#pencarianpasien-grid',
                 'beforeSend' => 'function(){
                                      $("#pencarianpasien-grid").addClass("srbacLoading");
                                  }',
                 'complete' => 'function(){
                                      $("#pencarianpasien-grid").removeClass("srbacLoading");
                                  }',
             ))); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPembayaranKasir',
    'options'=>array(
        'title'=>'Pembayaran Kasir',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1024,
        'minHeight'=>610,
        'resizable'=>true,
        'close'=>"js:function(){ $.fn.yiiGridView.update('pencarianpasien-grid', {
                        data: $('#caripasien-form').serialize()
                    }); }",
    ),
));
?>
<iframe src="" name="iframePembayaran" id="iframePembayaran" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>


<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>