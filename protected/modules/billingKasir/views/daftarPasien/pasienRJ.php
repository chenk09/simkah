<?php
$this->breadcrumbs=array(
	'Daftar Pasien'=>array('/billingKasir/daftarPasien'),
	'PasienRJ',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 
if(isset($_GET['statusfarmasi'])){
    if($_GET['statusfarmasi'] == 0){
        Yii::app()->user->setFlash("error","Alat & Obat Kesehatan belum di verifikasi di Farmasi. Silahkan Hubungi Ke Bagian Farmasi");
    }
}
?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'method'=>'GET',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Pasien Rawat Jalan</legend>
<?php
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$modRJ->searchRJ(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
        'mergeHeaders'=>array(
            array(
                'name'=>'<center>Penjamin</center>',
                'start'=>5,
                'end'=>6,
            ),
        ),
	'columns'=>array(
                    array(
                        'header'=>'Tgl Pendaftaran',
                        'name'=>'tgl_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->tgl_pendaftaran',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    /*
                    array(
                        'header'=>'Nama Instalasi',
                        'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'value'=>'$data->instalasi_nama',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                     * 
                     */
                    array(
                        'name'=>'no_pendaftaran',
                        'type'=>'raw',
                        'value'=>'$data->no_pendaftaran',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'header'=>'Nama',
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'header'=>'Alias',
                        'name'=>'nama_bin',
                        'type'=>'raw',
                        'value'=>'$data->nama_bin',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
                    ),
                    array(
                        'header'=>'Cara Bayar',
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
                    ),
                    array(
                        'header'=>'Penjamin',
                        'name'=>'penjamin_nama',
                        'type'=>'raw',
                        'value'=>'$data->penjamin_nama',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;')
                    ),
                    array(
                        'header'=>'Penanggung',
                        'name'=>'nama_pj',
                        'type'=>'raw',
                        'value'=>'isset($data->nama_pj) ? CHtml::Link($data->nama_pj,Yii::app()->controller->createUrl("DaftarPasien/informasiPenanggung",array("id"=>$data->no_pendaftaran,"frame"=>true)),array("class"=>"", "target"=>"iframeInformasiPenanggung", "onclick"=>"$(\"#dialogInformasiPenanggung\").dialog(\"open\");","rel"=>"tooltip", "title"=>"Klik untuk melihat Informasi Penanggung Jawab",)) : "-"',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'header'=>'Jenis Kasus Penyakit',
                        'name'=>'jeniskasuspenyakit_nama',
                        'type'=>'raw',
                        'value'=>'$data->jeniskasuspenyakit_nama',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'name'=>'umur',
                        'type'=>'raw',
                        'value'=>'$data->umur',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'header'=>'Alamat',
                        'name'=>'alamat_pasien',
                        'type'=>'raw',
                        'value'=>'$data->alamat_pasien',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                    ),
                    array(
                        'header'=>'Rincian Tagihan',
                        'type'=>'raw',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RinciantagihanpasienV/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianTagihan",
                                          "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Tagihan",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    array(
                        'header'=>'Rincian Tagihan Farmasi',
                        'type'=>'raw',
//                        'value'=>'($data->getStatusFarmasi($data->pendaftaran_id)>0) ? CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
//                                array("class"=>"", 
//                                      "target"=>"iframeRincianTagihan",
//                                      "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
//                                      "rel"=>"tooltip",
//                                      "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
//                                      "htmlOptions"=>array("style"=>"text-align: center; width:40px")
//                                )) : "Belum ada <br/>Transaksi <br/>Apotek"',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincian",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                                    array("class"=>"", 
                                          "target"=>"iframeRincianTagihan",
                                          "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Tagihan Farmasi",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
                    array(
                        'header'=>'Pembayaran Kasir',
                        'type'=>'raw',
                        'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                        'value'=>'CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->controller->createUrl("pembayaran/index",array("idPendaftaran"=>$data->pendaftaran_id,"frame"=>true,"status"=>"RJ")),
                                    array("class"=>"", 
                                          "target"=>"iframePembayaran",
                                          "onclick"=>"cekStatusFarmasi($data->pendaftaran_id);",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk membayar ke kasir",
                                    ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                    ),
            ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
</fieldset>
<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$modRJ,'form'=>$form),true);  ?> 

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset')); ?>
						<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogPembayaranKasir',
    'options'=>array(
        'title'=>'Pembayaran Kasir',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>1024,
        'minHeight'=>610,
        'resizable'=>true,
        'close'=>"js:function(){ $.fn.yiiGridView.update('pencarianpasien-grid', {
                        data: $('#caripasien-form').serialize()
                    }); }",
    ),
));
?>
<iframe src="" name="iframePembayaran" id="iframePembayaran" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogInformasiPenanggung',
    'options'=>array(
        'title'=>'Informasi Penanggung',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>350,
        'minHeight'=>200,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeInformasiPenanggung" width="100%" height="200" ></iframe>
<?php
$this->endWidget();
?>

<script>
    function cekStatusFarmasi(idPendaftaran){
        var idPendaftaran = idPendaftaran;
        var ruangan = "RJ";
         $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/setStatusFarmasi');?>', {idPendaftaran:idPendaftaran, ruangan:ruangan}, function(data){
            if(data.statusFarmasi == true){
                $("#dialogPembayaranKasir").dialog("open");
            }else{
                alert("Alat & Obat Kesehatan belum di verifikasi di Farmasi\nSilahkan Hubungi Ke Bagian Farmasi");
                return true;
            }
        }, 'json');
    }
</script>