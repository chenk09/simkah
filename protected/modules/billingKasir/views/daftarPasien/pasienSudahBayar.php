<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php 

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

?>
<fieldset>
<legend class="rim2">Informasi Pasien Sudah Bayar </legend>
<div style="max-width: 1280px; overflow-x: scroll;">
<?php
    $this->widget('ext.bootstrap.widgets.BootGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchInformasi(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            'nobuktibayar',
            'tglbuktibayar',
            array(
                'header'=>'No. Pendaftaran <br>/ No. Rekam Medik',
                'name'=>'no_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->no_pendaftaran." <br>/ ".$data->no_rekam_medik',
            ),
            'ruangan_nama',
            array(
                'header'=>'Nama Pasien <br>/ Alias',
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->nama_pasien.((empty($data->nama_bin)) ? "":"<br>/ ".$data->nama_bin)',
            ),
            array(
                'header'=>'Cara Bayar <br>/ Penjamin',
                'name'=>'carabayar_nama',
                'type'=>'raw',
                'value'=>'$data->carabayar_nama."<br>/ ".$data->penjamin_nama',
            ),
            array(
                'name'=>'totalbiayapelayanan',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
            ),
            array(
                'name'=>'totalsubsidiasuransi',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalsubsidiasuransi)',
            ),
            array(
                'name'=>'totalsubsidipemerintah',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalsubsidipemerintah)',
            ),
            array(
                'name'=>'totalsubsidirs',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalsubsidirs)',
            ),
            array(
                'name'=>'totaliurbiaya',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
            ),
            array(
                'name'=>'totaldiscount',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaldiscount)',
            ),
            array(
                'name'=>'totalpembebasan',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalpembebasan)',
            ),
            array(
                'name'=>'totalbayartindakan',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbayartindakan)',
            ),
            array(
                'header'=>'Rincian Sudah Bayar',
                'type'=>'raw',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RinciantagihanpasienV/rincianSudahBayar",array("id"=>$data->pendaftaran_id, "idpembayaran"=>$data->pembayaranpelayanan_id, "frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeRincianTagihan",
                                  "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat Rincian Tagihan",
                            ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ), 
            array(
                'header'=>'Rincian Farmasi',
                'type'=>'raw',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RincianTagihanFarmasi/rincianSudahBayar",array("id"=>$data->pendaftaran_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeRincianTagihan",
                                  "onclick"=>"$(\"#dialogRincianTagihan\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat Rincian Farmasi Sudah Dibayar",
                            ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Retur Tagihan Pasien',
                'type'=>'raw',
                'value'=>'($data->returbayarpelayanan_id != null)? "Sudah Retur: Rp. ".MyFunction::formatNumber($data->TotalRetur):
                            CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("returTagihanPasien/index",array("idPembayaran"=>$data->pembayaranpelayanan_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk meretur tagihan pasien",
                            ))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'Kwitansi Pasien',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-silver\"></i>",Yii::app()->controller->createUrl("kwitansi/view",array("idPembayaranPelayanan"=>$data->pembayaranpelayanan_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeKwitansi",
                                  "onclick"=>"$(\"#dialogKwitansi\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk cetak Kwitansi",
                            ))',           'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
            array(
                'header'=>'BKM',
                'type'=>'raw',
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("detailKasMasuk",array("idPembayaran"=>$data->pembayaranpelayanan_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeDetPembayaran",
                                  "onclick"=>"$(\"#dialogDetPembayaran\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk detail BKM",
                            ))',
                            'htmlOptions'=>array(
                                'style'=>'text-align: center;'
                            )
            ),
            array(
                'header'=>'Batal Pembayaran',
                'type'=>'raw',
                'value'=>'($data->returbayarpelayanan_id != null)? "-": CHtml::Link("<i class=\"icon-remove\"></i>",Yii::app()->controller->createUrl("detailKasMasuk",array("idTandabuktibayar"=>$data->tandabuktibayar_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeDetPembayaran",
                                  "onclick"=>"cekHakBatalBayar(this,$data->tandabuktibayar_id,$data->pembayaranpelayanan_id);return false;",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk Membatalkan Pembayaran",
                            ))',
                            'htmlOptions'=>array(
                                'style'=>'text-align: center;'
                            )
            ),
            array(
                'name'=>'closingkasir_id',
                'type'=>'raw',
                'value'=>'empty($data->closingkasir_id) ? "BELUM" : "SUDAH"',
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
</div>
</fieldset>

<?php echo $this->renderPartial('_formCariPasienSudahBayar', array('model'=>$model,'form'=>$form),true);  ?> 

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),array('class'=>'btn btn-danger', 'type'=>'reset', 'onclick'=>'window.open(window.location, "_self");')); ?>
			<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRetur',
    'options'=>array(
        'title'=>'Retur Tagihan Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframePembayaran" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogKwitansi',
    'options'=>array(
        'title'=>'Kwitansi Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeKwitansi" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetPembayaran',
    'options'=>array(
        'title'=>'Detail Pembayaran',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>980,
        'minHeight'=>610,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetPembayaran" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianTagihan',
    'options'=>array(
        'title'=>'Rincian Tagihan',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>750,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
            <?php echo CHtml::hiddenField('tandabuktibayar_id', '', array()); ?> 
            <?php echo CHtml::hiddenField('pembayaranpelayanan_id', '', array()); ?> 
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>
    
    <div class="form-actions">
        <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                            array('class'=>'btn btn-primary', 'type'=>'submit', 'onclick'=>'cekLogin();return false;')); ?>
        <?php echo CHtml::link(Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), '#', array('class'=>'btn btn-danger','onclick'=>"$('#loginDialog').dialog('close');return false",'disabled'=>false)); ?>
    </div> 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>

<script type="text/javascript">

function cekLogin()
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLoginBatalBayar',array('task'=>'BatalBayar'));?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
            alert(data.error);
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
            // alert(data.status);

            // $('#<?php echo CHtml::activeId($modRetur, 'pegretur_id'); ?>').val(data.userid);

            var idTandabuktibayar = $('#tandabuktibayar_id').val();
            var idPembayaranpelayanan = $('#pembayaranpelayanan_id').val();
            jQuery.ajax({
                'url':'<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/BatalBayar')?>',
                'data':{idTandabuktibayar:idTandabuktibayar, idPembayaranpelayanan:idPembayaranpelayanan},
                'type':'post',
                'dataType':'json',
                'success':function(data){
                    // alert(data);
                    if(data.hasil=='GAGAL'){
                        alert('Pembatalan pembayaran gagal, data sudah di Closing.');
                    }else{
                        alert('Pembatalan pembayaran sudah dilakukan');
                        // reloadTable();
                        $.fn.yiiGridView.update('pencarianpasien-grid', {}); 
                    }
                },
                'cache':false
            });

            $('#loginDialog').dialog('close');
            return true;
        }else{
            alert(data.status);
        }
    }, 'json');
}

function cekHakBatalBayar(obj, tandabuktibayar_id, pembayaranpelayanan_id)
{
    var answer = confirm('Yakin Akan Membatalkan Pembayaran ini ?');
    if (answer){
        iframe = $(obj).attr("target");
        urlIframe = $(obj).attr("href");
        $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekHakBatalBayar');?>', {idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>', idTandaBuktiBayar:tandabuktibayar_id}, function(data){
            if(data.cekAkses){
                
                $('#loginDialog').dialog('open');  
                $('#tandabuktibayar_id').val(tandabuktibayar_id);
                $('#pembayaranpelayanan_id').val(pembayaranpelayanan_id);
            }
        }, 'json');
    }
}
</script>
