<?php
$this->breadcrumbs=array(
	'Daftar Pasien'=>array('/billingKasir/daftarPasien'),
	'PasienKarcis',
);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'caripasien-form',
	'enableAjaxValidation'=>false,
                'type'=>'horizontal',
                'focus'=>'#',
                'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)'),
));

Yii::app()->clientScript->registerScript('cariPasien', "
$('#caripasien-form').submit(function(){
	$.fn.yiiGridView.update('pencarianpasien-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<fieldset>
<legend class="rim2">Informasi Pasien Berhutang</legend>
<div>
<?php
    $this->widget('ext.bootstrap.widgets.HeaderGroupGridView', array(
	'id'=>'pencarianpasien-grid',
	'dataProvider'=>$model->searchRincianPasienBerhutang(),
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
            array(
                'header'=>'Tgl Bukti Bayar',
                'name'=>'tglpembayaran',
                'type'=>'raw',
                'value'=>'$data->tglpembayaran."<br>".$data->nopembayaran',
            ),
            array(
                'name'=>'instalasi',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
            ),
            array(
                'name'=>'no_pendaftaran',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->no_pendaftaran',
            ),
            array(
                'name'=>'no_rekam_medik',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->pasien->no_rekam_medik',
            ),
            array(
                'name'=>'nama_pasien',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->pasien->namaNamaBin',
            ),
            array(
                'header'=>'Cara Bayar | Penjamin',
                'name'=>'carabayar_nama',
                'type'=>'raw',
                'value'=>'$data->pendaftaran->carabayar->carabayar_nama."<br>".$data->pendaftaran->penjamin->penjamin_nama',
            ),
            array(
                'name'=>'total_tagihan',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totalbiayapelayanan)',
            ),
            array(
                'header'=>'Subsidi Asuransi',
                'name'=>'subsidi_asuransi',
                'type'=>'raw',
                'value'=>'$data->totalsubsidiasuransi',
            ),
            array(
                'header'=>'Subsidi Pemerintah',
                'name'=>'subsidi_pemerintah',
                'type'=>'raw',
                'value'=>'$data->totalsubsidipemerintah',
            ),
            array(
                'header'=>'Subsidi Rumah Sakit',
                'name'=>'subsidi_rs',
                'type'=>'raw',
                'value'=>'$data->totalsubsidirs',
            ),
            array(
                'header'=>'Sisa Biaya',
                'name'=>'iur_biaya',
                'type'=>'raw',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->totaliurbiaya)',
            ),
            array(
                'header'=>'Discount',
                'name'=>'discount',
                'type'=>'raw',
                'value'=>'$data->totaldiscount',
            ),
            array(
                'header'=>'Pembebasan',
                'name'=>'pembebasan',
                'type'=>'raw',
                'value'=>'$data->totalpembebasan',
            ),
            array(
                'header'=>'Jumlah Pembayaran',
                'name'=>'jmlbayarangsuran',
                'value'=>'"Rp. ".MyFunction::formatNumber($data->jmlbayarangsuran)',
                'headerHtmlOptions'=>array('style'=>'text-align:center;'),
                'htmlOptions'=>array('style'=>'text-align:right;'),
                'footerHtmlOptions'=>array(
                    'style'=>'text-align:right',
                    'class'=>'currency',
                    'colspan'=>14,
                ),                            
                'footer'=>'sum(jmlbayarangsuran)',
            ),
            array(
                'header'=>'Rincian Hutang',
                'type'=>'raw',
                'headerHtmlOptions'=>array('style'=>'vertical-align:middle;text-align:center;'),
                'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("RinciantagihanpasienV/rincianHutang",array("id"=>$data->pendaftaran_id, "idpembayaran"=>$data->tandabuktibayar->pembayaran->pembayaranpelayanan_id, "frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframeRincianTagihan",
                                  "onclick"=>"$(\"#dialogRincianHutang\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk melihat Rincian Tagihan",
                            ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),            
            array(
                'header'=>'Bayar Angsuran',
                'type'=>'raw',
                'value'=>'($data->jmlbayarangsuran >= round($data->totaliurbiaya))? "Lunas":
                CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("bayarAngsuran/index",array("idPembayaran"=>$data->tandabuktibayar->pembayaran->pembayaranpelayanan_id,"frame"=>true)),
                            array("class"=>"", 
                                  "target"=>"iframePembayaran",
                                  "onclick"=>"$(\"#dialogRetur\").dialog(\"open\");",
                                  "rel"=>"tooltip",
                                  "title"=>"Klik untuk membayar angsuran",
                            ))',          'htmlOptions'=>array('style'=>'text-align: center; width:40px')
            ),
        ),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));
?>
</div>
</fieldset>
<?php echo $this->renderPartial('_formKriteriaPencarian', array('model'=>$model,'form'=>$form),true);  ?> 

    <div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::link(Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                        $this->createUrl('DaftarPasien/PasienBerhutang'), array('class'=>'btn btn-danger','onKeypress'=>'return formSubmit(this,event)')); ?>
						<?php  
$content = $this->renderPartial('../tips/informasi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRetur',
    'options'=>array(
        'title'=>'Pembayaran Angsuran',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>850,
        'minHeight'=>500,
        'width' => 900,
        'height' => 500,
        'resizable'=>true,
        'close'=> 'js:function(){$.fn.yiiGridView.update(\'pencarianpasien-grid\', {data: $("#caripasien-form").serialize()});}'
    ),
));
?>
<iframe src="" name="iframePembayaran" width="100%" height="500" ></iframe>
<?php
$this->endWidget();
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogRincianHutang',
    'options'=>array(
        'title'=>'Rincian Tagihan Pasien Berhutang',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>700,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeRincianTagihan" width="100%" height="550" ></iframe>
<?php
$this->endWidget();
?>