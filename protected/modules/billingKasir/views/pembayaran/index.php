<style>
    .cols_hide{
        display:none;
    }
</style>
<?php
$this->breadcrumbs=array(
	'Pembayaran', 
);?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));
?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.number',
    'currency'=>'PHP',
    'config'=>array(
//        'symbol'=>'Rp ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>2,
    )
));
?>

<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        //'action'=>$this->createUrl('/billingKasir/pembayaran/prosesBayar'),
        'focus'=>'#TandabuktibayarT_uangditerima',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)',
        ),    
));?>
<?php
    if(!empty($_GET['id'])){
?>
     <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert">×</a>
        Data berhasil disimpan
    </div>
<?php } ?>
<?php $this->renderPartial('billingKasir.views.pembayaran._ringkasDataPasien',array('form'=>$form, 'modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien, 'modPenjamin'=>$modPenjamin, 'modDialogPenjamin'=>$modDialogPenjamin));?>
<fieldset>
    <legend class="rim">Pembayaran Tindakan</legend>
    <table id="tblBayarTind" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <?php $checkSemua = (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? true : false; ?>
                <th width="10">Pilih<br><?php 
                                echo CHtml::checkBox(
                                    'checkTindakan',
//                                    $checkSemua,
                                    true,
                                    array(
                                        'onkeypress'=>"return $(this).focusNextInputField(event)",
                                        'class'=>'checkbox-column',
                                        'onclick'=>'checkAllTindakan()',
                                        'id'=>'checkTindakan',
                                    )
                                ) ?></th>
                <th width="70">Tanggal</th>
                <th>Nama Tindakan</th>
                <th width="50">Qty</th>
                <th width="50">Tarif</th>
                <th width="50">Tarif Cyto</th>
                <th width="50">Discount</th>
                <th width="50">Subsidi Asuransi</th>
                <th class="cols_hide" width="50">Subsidi Pemerintah</th>
                <th width="50">Subsidi Rumah Sakit</th>
                <!--
                <th width="50">Rumah Sakit</th>
                <th width="50">Dokter</th>
                <th width="50">Perawat</th>
                -->
                <th width="50">Iur Biaya</th>
                <th width="50">Sub Total</th>
            </tr>
        </thead>
        <tbody>
        <?php
          if(!empty($_GET['idPendaftaran'])){

            $totTarifTind = 0 ;
            if (count($modTindakan) > 0){
                foreach($modTindakan as $j=>$tindakan){
                    $totTarifTind = $totTarifTind + $tindakan->tarif_tindakan;
                }
            }
            $totHargaSatuanObat = 0;
            if (count($modObatalkes) > 0){
                foreach($modObatalkes as $i=>$obatAlkes) { 
                    $totHargaSatuanObat = $totHargaSatuanObat + $obatAlkes->hargasatuan_oa;
                }
            }
            $totalPembagi = $totTarifTind + $totHargaSatuanObat;
         
              $totQty = 0; $totTarif = 0; $totCyto = 0; $totSubAsuransi = 0; $totSubPemerintah = 0; $totSubRs = 0; $totIur = 0; 
              $totPembebasanTarif = 0; $totDiscount_tindakan = 0; $totDiscount_oa = 0; $totBayarTind = 0; 
              if (count($modTindakan) > 0){
                    foreach($modTindakan as $i=>$tindakan)
                    { 
                        //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
                        $tindakan->tarif_tindakan = round($tindakan->tarif_tindakan, 2, PHP_ROUND_HALF_UP);
                        
                        $tarif_tindakan += $tindakan->tarif_tindakan - ($tindakan->tarif_medis + $tindakan->tarif_paramedis);
                        $tarif_medis += $tindakan->tarif_medis;
                        $tarif_paramedis += $tindakan->tarif_paramedis;

                        $pembebasanTarif = PembebasantarifT::model()->findAllByAttributes(array('tindakanpelayanan_id'=>$tindakan->tindakanpelayanan_id));
                        $tarifBebas = 0; 
                        foreach ($pembebasanTarif as $i => $pembebasan) {
                            $tarifBebas = $tarifBebas + $pembebasan->jmlpembebasan;
                        }
                        $totPembebasanTarif = $totPembebasanTarif + $tarifBebas;
                        $disc = ($tindakan->discount_tindakan > 0) ? $tindakan->discount_tindakan/100 : 0;
                        $discountTindakan = ($disc*$tindakan->tarif_satuan*$tindakan->qty_tindakan);
                        $totDiscount_tindakan += $discountTindakan ;

                        $subsidi = $this->cekSubsidi($tindakan);
                        $tarifSatuan = $tindakan->tarif_satuan;
                        $qtyTindakan = $tindakan->qty_tindakan; $totQty = $totQty + $qtyTindakan; 
                        $tarifTindakan = $tindakan->tarif_tindakan; $totTarif = $totTarif + $tarifTindakan; //tarif tindakan = tarif_satuan * qty_tindakan
                        $tarifCyto = $tindakan->tarifcyto_tindakan; $totCyto = $totCyto + $tarifCyto; 
                        if(!empty($subsidi['max'])){
//                              JANGAN ADA PEMBULATAN >> $subsidiAsuransi = round($tarifTindakan/$totalPembagi * $subsidi['max']); 
                            $subsidiAsuransi = $tarifTindakan/$totalPembagi * $subsidi['max']; 
                            $subsidiPemerintah = 0; 
                            $subsidiRumahSakit = 0; 

                            $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi; 
                            $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                            $totSubRs = $totSubRs + $subsidiRumahSakit; 
//                            JANGAN ADA PEMBULATAN >>   $iurBiaya = round(($tarifSatuan + $tarifCyto)); 
                            $iurBiaya = ($tarifSatuan + $tarifCyto); 
//                            JANGAN ADA PEMBULATAN >>   $subTotalTind = round($iurBiaya * $qtyTindakan) - $subsidiAsuransi; 
                            $subTotalTind = ($iurBiaya * $qtyTindakan) - $subsidiAsuransi; 
                            $subTotalTind -= $discountTindakan;
                            $subTotalTind = ($subTotalTind > 0) ? $subTotalTind : 0;
                            $iurBiaya = $subTotalTind;
                            $totBayarTind = $totBayarTind + $subTotalTind;
                            $totIur = $totBayarTind;
                        } else {
                            $subsidiAsuransi = $subsidi['asuransi']; 
                            $subsidiPemerintah = $subsidi['pemerintah']; 
                            $subsidiRumahSakit = $subsidi['rumahsakit'];

                            $totSubAsuransi = $totSubAsuransi + $subsidiAsuransi; 
                            $totSubPemerintah = $totSubPemerintah + $subsidiPemerintah; 
                            $totSubRs = $totSubRs + $subsidiRumahSakit; 
//                            JANGAN ADA PEMBULATAN >>   $iurBiaya = round(($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
                            $iurBiaya = (($tarifSatuan + $tarifCyto) - ($subsidiAsuransi + $subsidiPemerintah + $subsidiRumahSakit)); 
//                            JANGAN ADA PEMBULATAN >>   $subTotalTind = round($iurBiaya * $qtyTindakan);
                            $subTotalTind = ($iurBiaya * $qtyTindakan);
                            $subTotalTind -= $discountTindakan;
                            $iurBiaya = $subTotalTind;
                            $totBayarTind = $totBayarTind + $subTotalTind;
                            $totIur = $totBayarTind;

                        }


          
                      ?>
            <tr>
                <td>
                    <?php echo CHtml::checkBox("pembayaran[$i][tindakanpelayanan_id]", true, array('onchange'=>'hitungTotalSemuaTind();','value'=>$tindakan->tindakanpelayanan_id,'uncheckValue'=>'0', 'class'=>'pilihan')) ?>
                </td>
                <td>
                    <?php echo $tindakan->tgl_tindakan ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][tgl_tindakan]",$tindakan->tgl_tindakan, array('readonly'=>true,'class'=>'inputFormTabel span2')); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][carabayar_id]",$tindakan->carabayar_id, array('readonly'=>true)); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][penjamin_id]",$tindakan->penjamin_id, array('readonly'=>true)); ?> 
                    <?php echo CHtml::hiddenField("pembayaran[$i][pembebasan_tarif]",$tarifBebas, array('readonly'=>true)); ?>
                </td>
                <td>
                    <?php echo ((isset($tindakan->tipepaket->tipepaket_nama)) ? $tindakan->tipepaket->tipepaket_nama : null).' - '.  ((isset($tindakan->daftartindakan->daftartindakan_nama)) ? $tindakan->daftartindakan->daftartindakan_nama : null); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][daftartindakan_id]", $tindakan->daftartindakan_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][qty_tindakan]", $tindakan->qty_tindakan, array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][tarif_satuan]", $tindakan->tarif_satuan, array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::hiddenField("pembayaran[$i][tarif_tindakan]", $tindakan->tarif_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][tarifcyto_tindakan]", $tindakan->tarifcyto_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][discount_tindakan]",$discountTindakan, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][subsidiasuransi_tindakan]", $subsidiAsuransi, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td class="cols_hide">
                    <?php echo CHtml::textField("pembayaran[$i][subsidipemerintah_tindakan]", $subsidiPemerintah, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][subsisidirumahsakit_tindakan]", $subsidiRumahSakit, array('readonly'=>false,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <!--
                <td>
                    <?php echo CHtml::textField("detail_tarif_rs[$i]", $tindakan->tarif_tindakan - ($tindakan->tarif_medis + $tindakan->tarif_paramedis), array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>                    
                </td>
                <td>
                    <?php echo CHtml::textField("detail_tarif_dokter[$i]", $tindakan->tarif_medis, array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>                    
                </td>
                <td>
                    <?php echo CHtml::textField("detail_tarif_paramedis[$i]", $tindakan->tarif_paramedis, array('readonly'=>true,'onblur'=>'hitungTotalSemuaTind();','class'=>'inputFormTabel currency lebar3')); ?>                    
                </td>
                -->
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][iurbiaya_tindakan]", $iurBiaya, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaran[$i][sub_total]", $subTotalTind, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php //echo $tindakan->daftartindakan_id; ?>
                </td>
            </tr>
        <?php }}               }?>
            <tr class="trfooter">
                <td>
                    <?php echo CHtml::checkBox('inputTotalTind',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'checkbox-column','onclick'=>'cekInputTotalTindakan(this)')) ?>
                </td>
                <td colspan="2">Input Total <?php //echo $subsidi['max']; ?></td>
                <td>
                    <?php echo CHtml::textField("totalqtytindakan", $totQty, array('readonly'=>true,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbiayatindakan", $totTarif, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalcyto", $totCyto, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaldiscount_tindakan", $totDiscount_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidiasuransi", $totSubAsuransi, array('readonly'=>true,'onblur'=>'proporsiSubAsuransiTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td class="cols_hide">
                    <?php echo CHtml::textField("totalsubsidipemerintah", $totSubPemerintah, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidirs", $totSubRs, array('readonly'=>true,'onblur'=>'proporsiSubRsTind();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <!--
                <td><?php echo CHtml::textField("tarif_tindakan", $tarif_tindakan, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?></td>
                <td><?php echo CHtml::textField("tarif_medis", $tarif_medis, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?></td>
                <td><?php echo CHtml::textField("tarif_paramedis", $tarif_paramedis, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?></td>                
                -->
                <td>
                    <?php echo CHtml::textField("totaliurbiaya", $totIur, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbayartindakan", $totBayarTind, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::hiddenField("totalpembebasan", $totPembebasanTarif, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?> 
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<fieldset>
    <legend class="rim">Pembayaran Obat Alkes</legend>
    <table id="tblBayarOA" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <?php $checkSemua = (isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? true : false; ?>
                <th width="10">Pilih<br><?php 
                                echo CHtml::checkBox('checkAllObat',$checkSemua, array('onkeypress'=>"return $(this).focusNextInputField(event)",
                                'class'=>'checkbox-column','onclick'=>'checkAll()','checked'=>'checked')) ?></th>
                <th width="70">Tanggal</th>
                <th>Nama Obat Alkes</th>
                <th width="50">Qty</th>
                <th width="50">Harga Satuan</th>
                <th width="50">Discount</th>
                <th width="50">Adm/Serv/Kons</th>
                <th width="50">Subsidi Asuransi</th>
                <th class="cols_hide" width="50">Subsidi Pemerintah</th>
                <th width="50">Subsidi Rumah Sakit</th>
                <th width="50">Iur Biaya</th>
                <th width="50">Sub Total</th>
            </tr>
        </thead>
        <tbody>
        <?php 
            $totBayarOa = 0;
            $totDiscount_oa = 0;
            $subsidiOa = 0;
            $totQtyOa = 0;
            $totHargaJualOa = 0;
            $totCytoOa = 0;
            $totSubAsuransiOa = 0;
            $totSubPemerintahOa = 0;
            $totSubRsOa = 0;
            $totIurOa = 0;
            $subTotalOa = 0;
            
            if (isset($modObatalkes) && (count($modObatalkes) > 0)){
                foreach($modObatalkes as $i=>$obatAlkes) { 
                    //PEMBULATAN KEATAS HARGA SATUAN DESIMAL 2 ANGKA
                    $obatAlkes->hargasatuan_oa = round($obatAlkes->hargasatuan_oa, 2, PHP_ROUND_HALF_UP);
                    
                    $disc = ($obatAlkes->discount > 0) ? $obatAlkes->discount/100 : 0;
                    $discount_oa = ($disc*$obatAlkes->qty_oa*$obatAlkes->hargasatuan_oa);
                    $totDiscount_oa += $discount_oa;
                    $subsidiOa = $this->cekSubsidiOa($obatAlkes);
                    $totQtyOa = $totQtyOa + $obatAlkes->qty_oa; 
                    $totHargaJualOa = $totHargaJualOa + ($obatAlkes->hargasatuan_oa * $obatAlkes->qty_oa); 
                    $oaHargajual = $obatAlkes->hargasatuan_oa * $obatAlkes->qty_oa; 
                    $biayaServiceResep = $obatAlkes->biayaservice;
//                    DI KOMEN AGAR SAMA DENGAN RINCIAN
//                    if (isset($obatAlkes->obatalkes)){
//                        if($obatAlkes->obatalkes->obatalkes_kategori == "GENERIK" OR $obatAlkes->obatalkes->jenisobatalkes_id == 3){
//                            $biayaServiceResep = 0;
//                        }
//                    }
                    $oaCyto = ($obatAlkes->biayaadministrasi + $obatAlkes->biayakonseling + $biayaServiceResep); 
                    $totCytoOa += $oaCyto; 
                    if(!empty($subsidiOa['max'])){
                        $oaSubsidiasuransi = ($oaHargajual/$totalPembagi * $subsidiOa['max']); 
                        $oaSubsidipemerintah = 0; 
                        $oaSubsidirs = 0; 
                    } else {
                        $oaSubsidiasuransi = ($subsidiOa['asuransi'] > 0) ? $subsidiOa['asuransi'] : 0; 
                        $oaSubsidipemerintah = ($subsidiOa['pemerintah'] > 0) ? $subsidiOa['pemerintah'] : 0; 
                        $oaSubsidirs = ($subsidiOa['rumahsakit'] > 0) ? $subsidiOa['rumahsakit'] : 0; 
                    }
                    $totSubAsuransiOa += $oaSubsidiasuransi; 
                    $totSubPemerintahOa += $oaSubsidipemerintah; 
                    $totSubRsOa += $oaSubsidirs; 
                    $subTotalOa = ($oaHargajual + $oaCyto) - $discount_oa - ($oaSubsidiasuransi + $oaSubsidipemerintah + $oaSubsidirs); 
                    $subTotalOa = ($subTotalOa > 0) ? $subTotalOa : 0;
                    $obatAlkes->iurbiaya = $oaIurbiaya;
                    $totBayarOa += $subTotalOa;
                    $oaIurbiaya = $subTotalOa;
                    $totIurOa += $oaIurbiaya; 
        ?>
            <tr>
                <td><?php echo CHtml::checkBox("pembayaranAlkes[$i][obatalkespasien_id]", true, array('onchange'=>'hitungTotalSemuaOa();','value'=>$obatAlkes->obatalkespasien_id,'uncheckValue'=>'0', 'class'=>'pilihan')) ?></td>
                <td><?php echo $obatAlkes->tglpelayanan ?></td>
                <td>
                    <?php //echo $obatAlkes->daftartindakan->daftartindakan_nama ?>
                    <?php echo ((isset($obatAlkes->obatalkes->obatalkes_nama)) ? $obatAlkes->obatalkes->obatalkes_nama : null); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][obatalkes_id]" ,$obatAlkes->obatalkes_id, array('readonly'=>true,'class'=>'inputFormTabel lebar2')); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][carabayar_id]",$obatAlkes->carabayar_id, array('readonly'=>true)); ?>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][penjamin_id]",$obatAlkes->penjamin_id, array('readonly'=>true)); ?>
                    <?php //echo CHtml::hiddenField("pembayaranAlkes[$i][discount]",$obatAlkes->discount, array('readonly'=>true)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][qty_oa]", $obatAlkes->qty_oa, array('onblur'=>'hitungTotalSemuaOa();','readonly'=>true,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::hiddenField("pembayaranAlkes[$i][hargajual_oa]", $obatAlkes->hargajual_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][hargasatuan]",$obatAlkes->hargasatuan_oa, array('onblur'=>'hitungTotalSemuaOa();','readonly'=>TRUE,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][discount]",$discount_oa, array('onblur'=>'hitungTotalSemuaOa();','readonly'=>false,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][tarifcyto]", $oaCyto, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidiasuransi]", $oaSubsidiasuransi, array('readonly'=>false,'onblur'=>'hitungTotalSemuaOa();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td class="cols_hide">
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidipemerintah]", $oaSubsidipemerintah, array('readonly'=>false,'onblur'=>'hitungTotalSemuaOa();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][subsidirs]", $oaSubsidirs, array('readonly'=>false,'onblur'=>'hitungTotalSemuaOa();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][iurbiaya]", $oaIurbiaya, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php //echo $obatAlkes->daftartindakan_id ?>
                    <?php echo CHtml::textField("pembayaranAlkes[$i][sub_total]", $subTotalOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        <?php } 
        
        }?>
            <tr class="trfooter">
                <td>
                    <?php echo CHtml::checkBox('inputTotalOa',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'checkbox-column','onclick'=>'cekInputTotalOa(this)')) ?>
                </td>
                <td colspan="2">Input Total <?php //echo $subsidiOa['max']; ?></td>
                <td>
                    <?php echo CHtml::textField("totalqty_oa", $totQtyOa, array('readonly'=>true,'class'=>'inputFormTabel number lebar2')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbiaya_oa", $totHargaJualOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaldiscount_oa", $totDiscount_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalcyto_oa", $totCytoOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidiasuransi_oa", $totSubAsuransiOa, array('readonly'=>true,'onblur'=>'proporsiSubAsuransiOa();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td class="cols_hide">
                    <?php echo CHtml::textField("totalsubsidipemerintah_oa", $totSubPemerintahOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalsubsidirs_oa", $totSubRsOa, array('readonly'=>true,'onblur'=>'proporsiSubRsOa();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totaliurbiaya_oa", $totIurOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)",)); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalbayar_oa", $totBayarOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        </tbody>
    </table>
    <table id="tblTotalSeluruh" class="table table-bordered table-condensed">
        <thead>
            <tr>
                <th>Pilih </th>
                <th width="80%">Total Pelayanan dan Obat Alkes </th>
                <th width="200">Total Biaya + Tarif Cyto /Adm/Serv/Kons</th>
                <th width="50">Discount</th>
                <th width="50">Subsidi Asuransi</th>
                <th class="cols_hide" width="50">Subsidi Pemerintah</th>
                <th width="50">Subsidi Rumah Sakit</th>
                <th width="50">Iur Biaya</th>
                <th width="50">Total</th>
            </tr>
        </thead>
        <tbody>
            <tr class="trfooter">
                <td>
                    <?php echo CHtml::checkBox('inputTotalSemua',false, array('onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'checkbox-column','onclick'=>'cekInputTotal(this)')) ?>
                </td>
                <td>Input Total Semua</td>
                <td>
                    <?php echo CHtml::textField("totalseluruhbiaya", $totTarif + $totHargaJualOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                    <!-- EHJ-3459 -->
                    <?php echo CHtml::hiddenField("totalseluruhcyto", $totCyto + $totCytoOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalseluruhdiscount", $totDiscount_tindakan + $totDiscount_oa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalseluruhsubsidiasuransi", $totSubAsuransi + $totSubAsuransiOa, array('readonly'=>true,'onblur'=>'proporsiSubAsuransiSemua();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td class="cols_hide">
                    <?php echo CHtml::textField("totalseluruhsubsidipemerintah", $totSubPemerintah + $totSubPemerintahOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalseluruhsubsidirs", $totSubRs + $totSubRsOa, array('readonly'=>true,'onblur'=>'proporsiSubRsSemua();','class'=>'inputFormTabel currency lebar3','onkeypress'=>"return $(this).focusNextInputField(event)")); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalseluruhiurbiaya", $totIur + $totIurOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
                <td>
                    <?php echo CHtml::textField("totalseluruhbayar", $totBayarTind + $totBayarOa, array('readonly'=>true,'class'=>'inputFormTabel currency lebar3')); ?>
                </td>
            </tr>
        </tbody>
    </table>
</fieldset>

<?php
$pembulatanHarga = KonfigfarmasiK::model()->find()->pembulatanharga;
//JANGAN ADA PEMBULATAN >> $totTagihan = round($totBayarTind + $totBayarOa);
$totTagihan = ($totBayarTind + $totBayarOa);
$totDeposit = 0;
$is_pengambalian = 0;

$attributes = array(
    'pendaftaran_id'=>$modPendaftaran->pendaftaran_id
);
$uang_muka = BayaruangmukaT::model()->findAllByAttributes($attributes, 'pembatalanuangmuka_id IS NULL');
if(count($uang_muka) > 0)
{
    foreach($uang_muka as $data){
        $totDeposit += $data->jumlahuangmuka;
    }
}


if ($modTandaBukti->isNewRecord)
{
    $modTandaBukti->jmlpembayaran = $totTagihan;
    $modTandaBukti->biayaadministrasi = 0;
    $modTandaBukti->biayamaterai = 0;
    $modTandaBukti->uangkembalian = 0;
    $modTandaBukti->uangditerima = $totTagihan;
    $pembulatan = ($pembulatanHarga > 0) ? $totTagihan % $pembulatanHarga : 0 ;
    
    if($pembulatan > 0)
    {
        $modTandaBukti->jmlpembulatan = $pembulatanHarga - $pembulatan;
    } else {
        $modTandaBukti->jmlpembulatan = 0;
    }
    
    $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan -$totDiscount_oa - $totPembebasanTarif;
    if($totDeposit > 0)
    {
        $modTandaBukti->jmlpembayaran = $modTandaBukti->jmlpembayaran + $modTandaBukti->jmlpembulatan - $totDiscount_tindakan -$totDiscount_oa - $totPembebasanTarif - $totDeposit;
        if($modTandaBukti->jmlpembayaran < 0)
        {
            $modTandaBukti->jmlpembayaran = 0;
            $is_pengambalian = 1;
        }
    }
    $harusDibayar = $modTandaBukti->jmlpembayaran;
    $modTandaBukti->uangditerima = $modTandaBukti->jmlpembayaran;
}
?>

<fieldset id="formDataPembayaran">
    <legend class="rim">Data Pembayaran</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Tagihan','totBiaya', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php
                            echo CHtml::textField(
                                'totBiaya',$totTagihan,
                                array(
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Iur Biaya','harusDibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php
                            echo CHtml::textField(
                                'totTagihan',$totTagihan,
                                array(
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Discount','disc', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php 
                            echo CHtml::textField(
                                'disc',0,
                                array(
                                    'readonly'=>false,
                                    'onBlur'=>'hitungDiscount(this);',
//                                    'class'=>'inputFormTabel number lebar2 currency', 
                                     'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            );
                        ?> %
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Discount Rupiah','discRupiah', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php
                                echo CHtml::textField(
                                    'discRupiah',0,
                                    array(
                                        'readonly'=>false,
                                        'onBlur'=>'hitungDiscount(this);',
//                                        'class'=>'inputFormTabel number lebar3 currency', 
                                         'class'=>'inputFormTabel currency span3', 
                                        'onkeypress'=>"return $(this).focusNextInputField(event);"
                                    )
                                );
                            ?> [ dalam rupiah ] <br>
                            <?php
                            echo CHtml::hiddenField(
                                'discount',$modBayar->totaldiscount,
                                array(
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            );
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Deposit','deposit', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php 
                            echo CHtml::textField(
                                'deposit',$totDeposit,
                                array(
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Pembebasan','totPembebasan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php 
                            echo CHtml::textField(
                                'totPembebasan',$totPembebasanTarif,
                                array(
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            );
                        ?>
                    </div>
                </div>
                <?php
                    echo $form->textFieldRow(
                        $modTandaBukti,
                        'jmlpembulatan',
                        array(
                            'readonly'=>true,
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    );
                ?>
                <?php 
                    echo $form->textFieldRow(
                        $modTandaBukti,'biayaadministrasi',
                        array(
                            'onkeyup'=>'hitungJmlBayar();',
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textFieldRow(
                        $modTandaBukti,'biayamaterai',
                        array(
                            'onkeyup'=>'hitungJmlBayar();',
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <div class="control-group">
                    <?php echo CHtml::label('Jml Harus Bayar','jmlpembayaran', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php 
                            echo $form->textField(
                                $modTandaBukti,'jmlpembayaran',
                                array(
//                                    'onkeyup'=>'hitungKembalian();',
                                    'readonly'=>true,
                                    'class'=>'inputFormTabel currency span3', 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            ); 
                        ?>
                    </div>
                </div>
                <?php 
                    echo $form->textFieldRow(
                        $modTandaBukti,'uangditerima',
                        array(
//                            'onkeyup'=>'hitungKembalian(obj);',
                            'class'=>'inputFormTabel currency span3', 
                            'onblur'=>'cekKembalian();', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textFieldRow(
                        $modTandaBukti,'uangkembalian',
                        array(
                            'readonly'=>true,
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    );
                ?>
                <?php 
                    echo CHtml::hiddenField(
                        'is_pengambalian', $is_pengambalian,array()
                    );
                ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php $modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modTandaBukti,'tglbuktibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                            $this->widget('MyDateTimePicker',
                                array(
                                    'model'=>$modTandaBukti,
                                    'attribute'=>'tglbuktibayar',
                                    'mode'=>'datetime',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                        'maxDate' => 'd',
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'dtPicker3', 
                                        'onkeypress'=>"return $(this).focusNextInputField(event)"
                                    ),
                                )
                            );
                        ?>
                    </div>
                </div>
                <?php //echo $form->textFieldRow($modTandaBukti,'tglbuktibayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                <?php
                    echo $form->dropDownListRow(
                        $modTandaBukti,
                        'carapembayaran',
                        CaraPembayaran::items(),
                        array(
                            'onchange'=>'ubahCaraPembayaran(this)',
                            'class'=>'span3',
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>50,
                        )
                    );
                ?>
                <div class="control-group ">
                    <div class="controls">
                        <div id="info_pembayaran" style="font-size:11px;padding:5px;border-radius:5px;border:1px solid #3093C7;width:190px;">
                            TUNAI : Pasien melakukan pembayaran dengan lunas
                        </div>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Menggunakan Kartu','pakeKartu', array('class'=>'control-label inline')) ?> 
                    <div class="controls">
                        <?php 
                            echo CHtml::checkBox(
                                'pakeKartu',false,
                                array(
                                    'onchange'=>"enableInputKartu();", 
                                    'onkeypress'=>"return $(this).focusNextInputField(event);"
                                )
                            ); 
                        ?> 
                        <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div id="divDenganKartu" class="hide">
                    <?php 
                        echo $form->dropDownListRow(
                            $modTandaBukti,'dengankartu',
                            DenganKartu::items(),
                            array(
                                'onchange'=>'enableInputKartu()',
                                'empty'=>'-- Pilih --',
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>50
                            )
                        ); 
                    ?>
                    <?php 
                        echo $form->textFieldRow(
                            $modTandaBukti,'bankkartu',
                            array(
                                'readonly'=>true,
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        ); 
                    ?>
                    <?php 
                        echo $form->textFieldRow(
                            $modTandaBukti,'nokartu',
                            array(
                                'readonly'=>true,
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        ); 
                    ?>
                    <?php 
                        echo $form->textFieldRow(
                            $modTandaBukti,'jmlbayardgnkartu',
                            array(
                                'readonly'=>true,
                                'class'=>'inputFormTabel currency span3', 
                                'onblur'=>'jumlahBayarKartu()',
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        ); 
                    ?>
                    <?php 
                        echo $form->textFieldRow(
                            $modTandaBukti,'nostrukkartu',
                            array(
                                'readonly'=>true,
                                'class'=>'span3', 
                                'onkeypress'=>"return $(this).focusNextInputField(event);", 
                                'maxlength'=>100
                            )
                        ); 
                    ?>
                </div>

                <?php 
                    echo $form->textFieldRow(
                        $modTandaBukti,'darinama_bkm',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>100
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textAreaRow(
                        $modTandaBukti,'alamat_bkm',
                        array(
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    ); 
                ?>
                <?php
                    $tgl1=explode(' ', $modPendaftaran->tgl_pendaftaran);
                    $tglPend=$tgl1[0].' '.$tgl1[1].' '.$tgl1[2];


                    $tgl2=explode(' ', $modPendaftaran->tglselesaiperiksa);
                    $tglSelesai=$tgl2[0].' '.$tgl2[1].' '.$tgl2[2];
                ?>
                <?php 
                    if($modPendaftaran->instalasi_id==Params::INSTALASI_ID_RI)
                    {
                        echo $form->textFieldRow($modTandaBukti,'sebagaipembayaran_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100, 'value'=>'Biaya Pelayanan Rumah Sakit dari Tanggal '. $tglPend .' sampai dengan '. $tglSelesai));
                    } else {
                        echo $form->textFieldRow($modTandaBukti,'sebagaipembayaran_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100, 'value'=>'Biaya Pelayanan Rumah Sakit Tanggal '. $tglPend));
                    }

                ?>
            </td>
        </tr>
    </table>
    <?php
    //FORM REKENING
    $this->renderPartial('billingKasir.views.pembayaran.rekening._formRekening',
        array(
            'form'=>$form,
            'modRekenings'=>$modRekenings,
        )
    );
    ?>
</fieldset>

    <?php //echo $form->textFieldRow($modTandaBukti,'closingkasir_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'shift_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'bayaruangmuka_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php // echo $form->textFieldRow($modTandaBukti,'pembayaranpelayanan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nourutkasir',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nobuktibayar',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'keterangan_pembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
    <div class="form-actions">
             <?php 
                if($modBayar->isNewRecord){
                    echo CHtml::htmlButton(
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array(
                            'class'=>'btn btn-primary', 
                            'type'=>'submit', 
                            'onClick'=>'submitForm();return false;'
                        )
                    );
                }else{
                    echo CHtml::htmlButton(
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array(
                            'class'=>'btn btn-primary', 
                            'type'=>'submit',
                            'disabled'=>true
                        )
                    ); 
                }
                echo "&nbsp;&nbsp;";
                $reffUrl = ((isset($_GET['frame']) && !empty($_GET['idPendaftaran'])) ? array('modulId'=>Yii::app()->session['modulId'], 'frame'=>$_GET['frame'], 'idPendaftaran'=>$_GET['idPendaftaran']) : array('modulId'=>Yii::app()->session['modulId']));
                if(!$modBayar->isNewRecord)
                {
//                    CHtml::link(Yii::t('mds', '{icon} Print Kwitansi Perusahaan', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKwitansi($modTandaBukti->tandabuktibayar_id, 'perusahaaan');return false",'disabled'=>false)).
//                    CHtml::link(Yii::t('mds', '{icon} Print Kwitansi', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', array('class'=>'btn btn-info','onclick'=>"printKwitansi($modTandaBukti->tandabuktibayar_id, 'umum');return false",'disabled'=>false)).
                    if( Yii::app()->user->getState('ruangan_id') == Params::RUANGAN_ID_KASIR_LAB ){ //Jika login di Ruangan Kasir Lab
                        echo CHtml::link(
                            Yii::t('mds', '{icon} Print Rincian Lab', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 
                            '#', 
                            array(
                                'class'=>'btn btn-info',
                                'onclick'=>"printRincianKasirLab(\"PDF\");return false",
                                'disabled'=>false
                            )
                        );
                    }else{
//----------------------------------------------------- ISSUE EHJ-157
//                        echo CHtml::link(
//                            Yii::t('mds', '{icon} Print Rincian', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 
//                            '#', 
//                            array(
//                                'class'=>'btn btn-info',
//                                'onclick'=>"printRincianKasir($modTandaBukti->tandabuktibayar_id, $modBayar->pembayaranpelayanan_id);return false",
//                                'disabled'=>false
//                            )
//                        );
//------------------------------------------------------
                    }
                }else{
                    echo CHtml::link(
                        Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 
                        '#', 
                        array(
                            'class'=>'btn btn-info',
                            'onclick'=>"return false",
                            'disabled'=>true
                        )
                    ); 
                }
            ?>
            <?php
                echo CHtml::link(
                    Yii::t('mds', '{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                    $this->createUrl('index',$reffUrl), 
                    array(
                        'class'=>'btn btn-danger'
                    )
                ); 
            ?>
            <?php  
                $content = $this->renderPartial('billingKasir.views.tips.transaksi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
    </div>

<?php $this->endWidget(); ?>

<?php 
        $controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
        $module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
        $urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/kwitansi/view');
        $urlPrintRincianLab=  Yii::app()->createAbsoluteUrl('billingKasir/rinciantagihanpasienLab/rincianKasirLabSudahBayarPrint');
        $idPendaftaran = (!empty($_GET['idPendaftaran'])) ? $_GET['idPendaftaran'] : null;
        //http://localhost/ehospitaljk/index.php?r=billingKasir/RinciantagihanpasienV/rincian&id=389&frame=1

$js = <<< JSCRIPT
function printKasir(caraPrint)
{
    window.open("${urlPrint}/"+$('#sanapza-m-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=1024px');
}
function printRincianKasirLab(caraPrint)
{
    window.open("${urlPrintRincianLab}/&id=${idPendaftaran}&caraPrint="+caraPrint,"",'location=_new, width=1024px');
}
JSCRIPT;
Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>

<div id="testForm"></div>

<script type="text/javascript">
$('.currency').each(function(){this.value = formatDesimal(this.value)});

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#pembayaran-form').find('.currency').each(function(){
        $(this).val(unformatNumber($(this).val()));
    });
    $('#pembayaran-form').submit();
}

function submitForm()
{
    if(cekInputTindakan())
    {
        confirmPembayaran();
    }
}

function cekInputTindakan()
{
    jumlahPilihan = $(".pilihan:checked").length;
    if (jumlahPilihan < 1){
        alert('Tidak ada tindakan / Obat Alkes');
        return false;
    }
    
    if($('#TandabuktibayarT_sebagaipembayaran_bkm').val() == ''){
        alert('"Sebagai Pembayaran" tidak boleh kosong!');
        $('#TandabuktibayarT_sebagaipembayaran_bkm').addClass('error');
        $('#TandabuktibayarT_sebagaipembayaran_bkm').focus();
        return false;
    }
    
    if($('#totalbayartindakan').val() <=0 && $('#totalbayar_oa').val()<=0
        && $('totaldiscount_tindakan').val() <= 0 
        && $('totalsubsidiasuransi').val() <= 0 
        && $('totalsubsidipemerintah').val() <= 0
        && $('totalsubsidirs').val() <= 0 ){
        alert('Tidak ada tindakan / Obat Alkes');
        return false;
    } else {
        $('.currency').each(function(){this.value = unformatNumber(this.value)});
        $('.number').each(function(){this.value = unformatNumber(this.value)});
//        var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
//        var totTagihan = unformatNumber($('#totalseluruhbayar').val());
        var deposit = unformatNumber($('#deposit').val());
        var totalseluruhbiaya = unformatNumber($('#totalseluruhbiaya').val());
		
//        if(deposit > totTagihan)
        if(deposit > totalseluruhbiaya) //EHJ-3375
        {
            alert('Deposit lebih besar dari pada pembayaran \nSilahkan melakukan pengembalian deposit . . , !!');
			return false;
        }
        return true;
    }
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    var no_pendaftaran = $('#pembayaran-form').find('input[name$="[no_pendaftaran]"]').val();
    var no_rekam_medik = $('#pembayaran-form').find('input[name$="[no_rekam_medik]"]').val();
    var instalasi_nama = $('#pembayaran-form').find('input[name$="[instalasi_nama]"]').val();
    var nama_pasien = $('#pembayaran-form').find('input[name$="[nama_pasien]"]').val();
    var ruangan_nama = $('#pembayaran-form').find('input[name$="[ruangan_nama]"]').val();
    
    var jum_pembayaran = $('#pembayaran-form').find('input[name$="[jmlpembayaran]"]').val();
    var dari_nama = $('#pembayaran-form').find('input[name$="[darinama_bkm]"]').val();
    var uang_diterima = $('#pembayaran-form').find('input[name$="[uangditerima]"]').val();
    var sbg_pembayaran = $('#pembayaran-form').find('input[name$="[sebagaipembayaran_bkm]"]').val();
    
    var data_pembayaran = {
        'jum_pembayaran':jum_pembayaran,
        'dari_nama':dari_nama,
        'uang_diterima':uang_diterima,
        'sbg_pembayaran':sbg_pembayaran
    }
    $('#content_confirm').find('#info_pembayaran_temp td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]);
                }
            }
        }
    );    
    
    
    var data_pasien = {
        'no_pendaftaran':no_pendaftaran,
        'no_rekam_medik':no_rekam_medik,
        'instalasi_nama':instalasi_nama,
        'nama_pasien':nama_pasien,
        'ruangan_nama':ruangan_nama,
    }
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]);
                }
            }
        }
    );
    var tr_info = "";
    var total_hrg_tindakan = 0;
    $('#tblBayarTind').find('tbody tr').each(
        function()
        {
            if($(this).attr('class') != 'trfooter')
            {
                var is_check = $(this).find('td input[name$="[tindakanpelayanan_id]"]').is(':checked');
                if(is_check)
                {
                    var nama_tindakan = $(this).find('td input[name$="[daftartindakan_id]"]').parent().text();
                    var qty_tindakan = $(this).find('td input[name$="[qty_tindakan]"]').val();
                    var tarif_satuan = $(this).find('td input[name$="[tarif_satuan]"]').val();

                    tr_info += '<tr>';
                    tr_info += '<td>'+ nama_tindakan +'</td><td>'+ qty_tindakan +'</td><td class="curency">'+ tarif_satuan +'</td><td class="curency">'+ tarif_satuan*qty_tindakan +'</td>'
                    tr_info += '</tr>';
                    total_hrg_tindakan += parseFloat(tarif_satuan*qty_tindakan);
                }
            }

        }
    );
    
    var tr_info_alkes = "";
    var total_hrg_alkes = 0;
    $('#tblBayarOA').find('tbody tr').each(
        function()
        {
            if($(this).attr('class') != 'trfooter')
            {
                var is_check = $(this).find('td input[name$="[obatalkespasien_id]"]').is(':checked');
                if(is_check)
                {
                    var nama_tindakan = $(this).find('td input[name$="[obatalkes_id]"]').parent().text();
                    var qty_tindakan = $(this).find('td input[name$="[qty_oa]"]').val();
                    var tarif_satuan = $(this).find('td input[name$="[hargasatuan]"]').val();

                    tr_info_alkes += '<tr>';
                    tr_info_alkes += '<td>'+ nama_tindakan +'</td><td>'+ qty_tindakan +'</td><td class="curency">'+ tarif_satuan +'</td><td class="curency">'+ tarif_satuan*qty_tindakan +'</td>';
                    tr_info_alkes += '</tr>';
                    total_hrg_alkes += parseFloat(tarif_satuan*qty_tindakan);
                }
            }

        }
    );
    
    $('#info_tindakan_temp').find('tbody').empty();
    tr_info += '<tr class="trfooter">';
    tr_info += '<td colspan="3">Total</td><td class="curency">'+ total_hrg_tindakan +'</td>';
    tr_info += '</tr>';
    $('#info_tindakan_temp').find('tbody').append(tr_info);
    
    $('#info_alkes_temp').find('tbody').empty();
    tr_info_alkes += '<tr class="trfooter">';
    tr_info_alkes += '<td colspan="3">Total</td><td class="curency">'+ total_hrg_alkes +'</td>';
    tr_info_alkes += '</tr>';    
    $('#info_alkes_temp').find('tbody').append(tr_info_alkes);
    
    $('#content_confirm').find('.curency').each(
        function()
        {
//            var result = formatNumber(parseInt($(this).text()));
//          JANGAN DIBULATKAN >>  var result = formatMoney($(this).text());
            var result = accounting.formatMoney($(this).text(),'',2,',','.');
            $(this).text(result);            
        }
    );
        

}
    
function hitungTotalTind(obj)
{
    var discount = unformatNumber($(obj).parents('tr').find('input[name$="[discount_tindakan]"]').val());
    var qty = unformatNumber(obj.value);
    var tarifSatuan = unformatNumber($(obj).parents('tr').find('input[name$="[tarif_satuan]"]').val());
    var tarif = qty * tarifSatuan;
    var tarifTind = unformatNumber($(obj).parents('tr').find('input[name$="[tarif_tindakan]"]').val());
    var tarifCyto = unformatNumber($(obj).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
    var subAsu = unformatNumber($(obj).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val());
    var subPem = unformatNumber($(obj).parents('tr').find('input[name$="[subsidipemerintah_tindakan]"]').val());
    var subRs = unformatNumber($(obj).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val());
    var discTindakan = unformatNumber($(obj).parents('tr').find('input[name$="[discount_tindakan]"]').val());
    var iurBiaya = tarif + tarifCyto - subAsu - subPem - subRs - discount;
    $(obj).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').val(formatDesimal(iurBiaya));
    $(obj).parents('tr').find('input[name$="[sub_total]"]').val(formatDesimal((iurBiaya)));
}

function hitungTotalSemuaTind()
{  
    var totIurBiaya = 0;
    var totSubRs = 0;
    var totSubPem = 0;
    var totSubAsu = 0;
    var totCyto = 0;
    var totTarifTind = 0;
    var totQty = 0;
    var totDiscount = 0;
    var totSubTotal = 0;
    
    $('#tblBayarTind').find('input[name$="[qty_tindakan]"]').each(function(){
        hitungTotalTind(this);
        if($(this).parents('tr').find('input[name$="[tindakanpelayanan_id]"]').is(':checked')){
            totIurBiaya += unformatNumber($(this).parents('tr').find('input[name$="[iurbiaya_tindakan]"]').val());
            totSubRs += unformatNumber($(this).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val());
            totSubPem += unformatNumber($(this).parents('tr').find('input[name$="[subsidipemerintah_tindakan]"]').val());
            totSubAsu += unformatNumber($(this).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val());
            totCyto += unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
            var qtyTind = unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
            totTarifTind += tarifCyto + (unformatNumber($(this).parents('tr').find('input[name$="[tarif_satuan]"]').val()) * qtyTind);
            totQty += unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
            totDiscount += unformatNumber($(this).parents('tr').find('input[name$="[discount_tindakan]"]').val());
            totSubTotal += unformatNumber($(this).parents('tr').find('input[name$="[sub_total]"]').val());
        }
    });
    $('#totaliurbiaya').val(formatDesimal(totIurBiaya));
    $('#totalsubsidirs').val(formatDesimal(totSubRs));
    $('#totalsubsidipemerintah').val(formatDesimal(totSubPem));
    $('#totalsubsidiasuransi').val(formatDesimal(totSubAsu));
    $('#totalcyto').val(formatDesimal(totCyto));
    $('#totalbiayatindakan').val(formatDesimal(totTarifTind));
    $('#totalqtytindakan').val(formatDesimal(totQty));
    $('#totaldiscount_tindakan').val(formatDesimal(totDiscount));
    $('#totalbayartindakan').val(formatDesimal(totSubTotal));
        
//    hitungJmlBayar();
//    hitungTotalSemua();
    hitungTotalAsuransi();
}

function hitungDiscount(obj)
{
    var discRupiah = 0;
    var discPersen = 0, discount=0;
    var rec = unformatNumber($(obj).val());
    var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
    $('#discount').val(0);
    if(totTagihan > 0)
    {
        if(rec > 0 )
        { 
            if($(obj).attr('name') == 'disc')
            {
                discRupiah = (rec / 100) * totTagihan;
                $("#discRupiah").val(formatDesimal(discRupiah));
            }else{
                discPersen = (rec/totTagihan) * 100;
                $("#disc").val(formatDesimal(discPersen));
            }
        }else{
            $("#discRupiah").val(0);
            $("#disc").val(formatDesimal(0));
        }
        $('#discount').val($("#discRupiah").val());
        hitungJmlBayar();        
//        endResult();        
    }

}

function hitungKembalian(obj)
{
    var jmlBayar = unformatNumber($('#TandabuktibayarT_jmlpembayaran').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    var deposit = unformatNumber($('#deposit').val());
    var uangKembalian;
    
    if ($("#<?php echo CHtml::activeId($modTandaBukti,'carapembayaran'); ?>").val() == 'TUNAI' || $("#<?php echo CHtml::activeId($modTandaBukti,'carapembayaran'); ?>").val() == 'PIUTANG')
    {
        

        
        
    }else{
        if($("#<?php echo CHtml::activeId($modTandaBukti,'carapembayaran'); ?>").val() != 'HUTANG')
        {
            if($(obj).attr("id") == '<?php echo CHtml::activeId($modTandaBukti,'uangditerima'); ?>' )
            {
                if(uangDiterima > jmlBayar)
                {
                    uangDiterima = jmlBayar;
                }
            }else{
                uangDiterima = jmlBayar;
            }
        }else{
            uangDiterima = 0;
        }
        uangKembalian = 0;        
    }
    
    
   
    /*
    if ($("#<?php // echo CHtml::activeId($modTandaBukti,'carapembayaran'); ?>").val() == 'CICILAN')
    {
        if ($(obj).attr("id") == '<?php // echo CHtml::activeId($modTandaBukti,'uangditerima'); ?>' )
        {
            if (uangDiterima > jmlBayar){
                uangDiterima = jmlBayar;
//                alert("Uang diterima tidak boleh kurang dari jumlah pembayaran");
            }
        }
        else{
            uangDiterima = jmlBayar;
        }
        uangKembalian = 0;
    }else{
        uangKembalian = uangDiterima - jmlBayar;
    }
    */
   
    $('#TandabuktibayarT_uangditerima').val(formatDesimal(uangDiterima));
    $('#TandabuktibayarT_uangkembalian').val(formatDesimal(uangKembalian));
    jumlahBayarKartu();
}

function cekKembalian()
{
    var jmlBayar = unformatNumber($('#TandabuktibayarT_jmlpembayaran').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    var deposit = unformatNumber($('#deposit').val());
    var totBiaya = unformatNumber($('#totBiaya').val());
    var jmlPembulatan = unformatNumber($('#TandabuktibayarT_jmlpembulatan').val());
    var biayaAdministrasi = unformatNumber($('#TandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#TandabuktibayarT_biayamaterai').val());
    var discount = unformatNumber($('#discRupiah').val());
    var totPembebasan = unformatNumber($('#totPembebasan').val());
    var uangKembalian = 0;
    var x,y =0;
    x =deposit+discount+totPembebasan+uangDiterima;
    y =totBiaya + jmlPembulatan + biayaAdministrasi + biayaMaterai;
    
    if($('#TandabuktibayarT_carapembayaran').val() === 'TUNAI')
    {
        if($('#pakeKartu').is(':checked')){
            
        }else
        if((uangDiterima+deposit) < jmlBayar)
        {
            if(confirm('Uang diterima tidak boleh lebih kecil dari Jumlah Harus Bayar')){
                uangDiterima = jmlBayar;
            }else{
                $('#TandabuktibayarT_uangditerima').addClass('error');
                $('#TandabuktibayarT_uangditerima').focus();
                $('#TandabuktibayarT_uangditerima').select();
            }
        }
        
        if(x > y)
        {
            uangKembalian=x-y;
        }else{
            uangKembalian=0;
        }
         
    }else if($('#TandabuktibayarT_carapembayaran').val() === 'PIUTANG'){
        if($('#pakeKartu').is(':checked')){
            
        }else
        if(uangDiterima < jmlBayar)
        {
            alert("Untuk Piutang, uang diterima tidak boleh lebih kecil dari Jumlah Harus Bayar");
            $('#TandabuktibayarT_uangditerima').focus();
            uangDiterima = jmlBayar;
        }
        
        if(x > y)
        {
            uangKembalian=x-y;
        }else{
            uangKembalian=0;
        }
        
    }else{
        uangKembalian = 0;
    }
    
//    uangKembalian = uangDiterima - jmlBayar;
    $('#TandabuktibayarT_uangditerima').val(formatDesimal(uangDiterima));
    $('#TandabuktibayarT_uangkembalian').val(formatDesimal(uangKembalian));
//    getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); 
}

function enableInputKartu()
{
    if($('#pakeKartu').is(':checked')){
        $('#divDenganKartu').show();
        $('#pakeKartu').val(1);
        getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); //load data jurnal rekening
    }else{ 
        getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); //load data jurnal rekening
        $('#divDenganKartu').hide();
        $('#pakeKartu').val(0);
    }
    if($('#TandabuktibayarT_dengankartu').val() != ''){
        //alert('isi');
        $('#TandabuktibayarT_bankkartu').removeAttr('readonly');
        $('#TandabuktibayarT_nokartu').removeAttr('readonly');
        $('#TandabuktibayarT_nostrukkartu').removeAttr('readonly');
        $('#TandabuktibayarT_jmlbayardgnkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#TandabuktibayarT_bankkartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nokartu').attr('readonly','readonly');
        $('#TandabuktibayarT_nostrukkartu').attr('readonly','readonly');
        $('#TandabuktibayarT_jmlbayardgnkartu').attr('readonly','readonly');
        
        $('#TandabuktibayarT_bankkartu').val('');
        $('#TandabuktibayarT_nokartu').val('');
        $('#TandabuktibayarT_nostrukkartu').val('');
        $('#TandabuktibayarT_jmlbayardgnkartu').val(0);
    }
    jumlahBayarKartu();
}

function jumlahBayarKartu(){
    var jmlBayar = unformatNumber($('#TandabuktibayarT_jmlbayardgnkartu').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    var jmlPembayaran = unformatNumber($('#TandabuktibayarT_jmlpembayaran').val());
    var totalUangDiterima = 0;
    if(jmlPembayaran > jmlBayar){
        totalUangDiterima = jmlPembayaran - jmlBayar;
    }else{
        totalUangDiterima = 0;
    }
    $('#TandabuktibayarT_uangditerima').val(formatNumber(totalUangDiterima));
    var jenispenerimaan_id = '<?php echo PARAMS::JENISPENERIMAAN_ID_PIUTANG_MERCHANT; ?>';
    updateRekeningKartu(jenispenerimaan_id,jmlBayar,totalUangDiterima); //update saldo debik & kredit piutang merchant / kartu debit kredit
}

function hitungJmlBayar()
{
//    var totBiaya = unformatNumber($('#totalbayartindakan').val()) + unformatNumber($('#totalbayar_oa').val());
//    var totTagihan = unformatNumber($('#totalbayar_oa').val()) + unformatNumber($('#totalbayartindakan').val());
	var totBiaya = unformatNumber($('#totalseluruhbayar').val());
    var totTagihan = unformatNumber($('#totalseluruhiurbiaya').val());
    var biayaAdministrasi = unformatNumber($('#TandabuktibayarT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#TandabuktibayarT_biayamaterai').val());
    var deposit = unformatNumber($('#deposit').val());
    var totPembebasan = unformatNumber($('#totPembebasan').val());
    var totDiscountTind = unformatNumber($('#totaldiscount_tindakan').val());
    var totDiscountOa = unformatNumber($('#totaldiscount_oa').val());
    var uangDiterima = unformatNumber($('#TandabuktibayarT_uangditerima').val());
    var totDiscount = totDiscountTind+totDiscountOa;
    var totBayar = totTagihan;
	var uangKembalian=0;
    var discount = unformatNumber($('#discRupiah').val());
    var jmlPembulatan = unformatNumber($('#TandabuktibayarT_jmlpembulatan').val());
    var pembulatan = <?php if ($pembulatanHarga > 0){?>(totTagihan - discount) % <?php echo $pembulatanHarga; }else echo 0;?>;
    jmlPembulatan = <?php echo $pembulatanHarga; ?> - pembulatan;
    if (jQuery.isNumeric(jmlPembulatan)){  
        if (jmlPembulatan >= <?php echo $pembulatanHarga; ?>){
            jmlPembulatan = 0;
        }
        $('#TandabuktibayarT_jmlpembulatan').val(formatDesimal(jmlPembulatan));
    }
    $('#totBiaya').val(formatDesimal(totBiaya));
    $('#totTagihan').val(formatDesimal(totTagihan));
	
//   x =deposit+discount+totPembebasan+uangDiterima;
   x =deposit+discount+totPembebasan; //EHJ-3401
   y =totTagihan + jmlPembulatan + biayaAdministrasi + biayaMaterai;
    if(x > y)
    {
        totBayar=0;
        uangKembalian=x-y;
    }else{
        totBayar=y-x;
        uangKembalian=0;
    }
    subTotBayar= totTagihan + jmlPembulatan + biayaAdministrasi + biayaMaterai
    $('#TandabuktibayarT_uangditerima').val(formatDesimal(totBayar));
	$('#TandabuktibayarT_jmlpembayaran').val(formatDesimal(totBayar));
	$('#totTagihan').val(formatDesimal(totBayar));
    $('#TandabuktibayarT_uangkembalian').val(formatDesimal(uangKembalian));
    
    if($('#TandabuktibayarT_carapembayaran').val() != 'TUNAI' && $('#TandabuktibayarT_carapembayaran').val() != 'PIUTANG')
    {
        $('#TandabuktibayarT_uangditerima').val(0);
    }

}

function ubahCaraPembayaran(obj)
{
    $('#info_pembayaran').text('TUNAI : Pasien melakukan pembayaran dengan lunas');
    hitungJmlBayar();
    $('#pembayaran-form').find('input[name$="[uangditerima]"]').removeAttr('readonly');
    if(obj.value != 'TUNAI')
    {
        if(obj.value == 'HUTANG')
        {
            $('#pembayaran-form').find('input[name$="[uangditerima]"]').attr('readonly', true);
            $('#info_pembayaran').text('HUTANG : Pasien melakukan pembayaran dengan cara berhutang (dengan surat jaminan)');
            $('#pembayaran-form').find('input[name$="[uangditerima]"]').val(0);
        }else if(obj.value == 'CICILAN')
        {
            $('#info_pembayaran').text('CICILAN : Pasien melakukan pembayaran dengan cara mencicil (beberapa kali bayar)');
            $('#pembayaran-form').find('input[name$="[uangditerima]"]').val(0);
        }else{
            $('#info_pembayaran').text('PIUTANG : Pasien melakukan pembayaran dengan tanggungan Penjamin /Pemerintah /Perusahaan');
            hitungJmlBayar();
        }
    }
    getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); //load data jurnal rekening
}
function printKwitansi(idTandaBukti, jenis) //unruk print kwitansi sesuai format RSJK
{
    if(idTandaBukti!='')
    { 
        window.open('<?php echo Yii::app()->createUrl('print/printKwitansiKasir',array('idTandaBukti'=>'')); ?>'+idTandaBukti + '&jenis_print=' + jenis,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    }
}

function printKwitansiOld(idTandaBukti, jenis)
{
    if(idTandaBukti!='')
    { 
        window.open('<?php echo Yii::app()->createUrl('print/printKwitansiKasir',array('idTandaBukti'=>'')); ?>'+idTandaBukti + '&jenis_print=' + jenis,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    }
}

function printKasir(idTandaBukti)
{
    if(idTandaBukti!=''){ 
             window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    }     
}

function printRincianKasir(idTandaBukti, idPembayaran)
{
    if(idTandaBukti!=''){ 
            window.open('<?php echo Yii::app()->createUrl('print/rincianKasirBaruPrint',array('caraPrint'=>'PDF')); ?>&idTandaBukti='+idTandaBukti+'&idPembayaran='+idPembayaran,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    }     
}

function printRincianTagihan()
{
    alert('oii');
}

function hitungTagihan(obj)
{
    var obQty = $(obj).parents('tr').find('input[name$="[qty_oa]"]');
    var obTarif = $(obj).parents('tr').find('input[name$="[hargajual_oa]"]');
    var obCyto = $(obj).parents('tr').find('input[name$="[tarifcyto]"]');
    var obSubas = $(obj).parents('tr').find('input[name$="[subsidiasuransi]"]');
    var obSubpem = $(obj).parents('tr').find('input[name$="[subsidipemerintah]"]');
    var obSubrs = $(obj).parents('tr').find('input[name$="[subsidirs]"]');
    var obIurbiaya = $(obj).parents('tr').find('input[name$="[iurbiaya]"]');
    var obSubtotal = $(obj).parents('tr').find('input[name$="[sub_total]"]');
    
    var qty = unformatNumber($(obQty).val());
    var tarif = unformatNumber($(obTarif).val());
}

function hitungTotalOa(obj)
{
    var qty = unformatNumber(obj.value);
    var hargasatuan = unformatNumber($(obj).parents('tr').find('input[name$="[hargasatuan]"]').val());
    var harga = qty * hargasatuan;
    var tarifCyto = unformatNumber($(obj).parents('tr').find('input[name$="[tarifcyto]"]').val());
    var subAsu = unformatNumber($(obj).parents('tr').find('input[name$="[subsidiasuransi]"]').val());
    var subPem = unformatNumber($(obj).parents('tr').find('input[name$="[subsidipemerintah]"]').val());
    var subRs = unformatNumber($(obj).parents('tr').find('input[name$="[subsidirs]"]').val());
    var discount = unformatNumber($(obj).parents('tr').find('input[name$="[discount]"]').val());
    var iurBiaya = harga + tarifCyto - subAsu - subPem - subRs - discount;
    $(obj).parents('tr').find('input[name$="[iurbiaya]"]').val(formatDesimal(iurBiaya));
    $(obj).parents('tr').find('input[name$="[sub_total]"]').val(formatDesimal((iurBiaya)));
    $(obj).parents('tr').find('input[name$="[totalcyto_oa]"]').val(formatDesimal(tarifCyto));
}

function hitungTotalSemuaOa()
{
    var totSubTotal = 0;
    var totIurBiaya = 0;
    var totSubRs = 0;
    var totSubPem = 0;
    var totSubAsu = 0;
    var totCyto = 0;
    var totTarifOa = 0;
    var totQty = 0;
    var totDiscount = 0;

    $('#tblBayarOA').find('input[name$="[qty_oa]"]').each(function(){
        hitungTotalOa(this);
        if($(this).parents('tr').find('input[name$="[obatalkespasien_id]"]').is(':checked')){
            totSubTotal += unformatNumber($(this).parents('tr').find('input[name$="[sub_total]"]').val());
            totIurBiaya += unformatNumber($(this).parents('tr').find('input[name$="[iurbiaya]"]').val());
            totSubRs += unformatNumber($(this).parents('tr').find('input[name$="[subsidirs]"]').val());
            totSubPem += unformatNumber($(this).parents('tr').find('input[name$="[subsidipemerintah]"]').val());
            totSubAsu += unformatNumber($(this).parents('tr').find('input[name$="[subsidiasuransi]"]').val());
            totCyto += unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
            var qtyOa = unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
            totTarifOa += tarifCyto +(unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val()) * qtyOa);
            totQty += unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
            totDiscount += unformatNumber($(this).parents('tr').find('input[name$="[discount]"]').val());
        }
        
    });
    $('#totalbayar_oa').val(formatDesimal(totSubTotal));
    $('#totaliurbiaya_oa').val(formatDesimal(totIurBiaya));
    $('#totalsubsidirs_oa').val(formatDesimal(totSubRs));
    $('#totalsubsidipemerintah_oa').val(formatDesimal(totSubPem));
    $('#totalsubsidiasuransi_oa').val(formatDesimal(totSubAsu));
    $('#totalcyto_oa').val(formatDesimal(totCyto));
    $('#totalbiaya_oa').val(formatDesimal(totTarifOa));
    $('#totalqty_oa').val(formatDesimal(totQty));
    $('#totaldiscount_oa').val(formatDesimal(totDiscount));
    
//    hitungJmlBayar();
//    hitungTotalSemua();
    hitungTotalAsuransi();
}

function checkAll() {
    if ($("#checkAllObat").is(":checked")) {
        $('#tblBayarOA input[name*="obatalkespasien_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#tblBayarOA input[name*="obatalkespasien_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    hitungTotalSemuaOa();
}

function checkAllTindakan() {
    if ($("#checkTindakan").is(":checked")) {
        $('#tblBayarTind input[name*="tindakanpelayanan_id"]').each(function(){
           $(this).attr('checked',true);
        })
//        alert('Checked');
    } else {
       $('#tblBayarTind input[name*="tindakanpelayanan_id"]').each(function(){
           $(this).removeAttr('checked');
        })
    }
    hitungTotalSemuaTind();
}

function cekParentValue(value,obj){
    if ($(obj).is(":checked")){
        $('#tblBayarOA input[name*="obatalkespasien_id"][parent-value="'+value+'"]').each(function(){
           $(this).attr('checked',true);
        });
    }else{
        $('#tblBayarOA input[name*="obatalkespasien_id"][parent-value="'+value+'"]').each(function(){
           $(this).removeAttr('checked');
        });
    }
    hitungTotalSemuaOa();
}
</script>

<?php $this->beginWidget('zii.widgets.jui.CJuiDialog', array(// the dialog
    'id' => 'dialogRincian',
    'options' => array(
        'title' => 'Rincian Tagihan Pasien',
        'autoOpen' => false,
        'modal' => true,
        'width' => 900,
        'height' => 550,
        'resizable' => false,
    ),
));
?>
<iframe name='frameRincian' width="100%" height="100%"></iframe>
<?php $this->endWidget(); ?>

<?php
//if($successSave){
//Yii::app()->clientScript->registerScript('tutupDialog',"
//window.parent.setTimeout(\"$('#dialogBayarKarcis').dialog('close')\",1500);
//window.parent.$.fn.yiiGridView.update('pencarianpasien-grid', {
//		data: $('#caripasien-form').serialize()
//});
//",  CClientScript::POS_READY);
//}
?>


<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Pembayaran',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>550,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Pasien</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">No. Pendaftaran</td>
                    <td width="250" tag="no_pendaftaran"></td>
                    <td width="150">Nama</td>
                    <td tag="nama_pasien"></td>
                </tr>
                <tr>
                    <td>Instalasi</td>
                    <td tag="instalasi_nama"></td>
                    <td>No. Rekam Medis</td>
                    <td tag="no_rekam_medik"></td>
                </tr>
                <tr>
                    <td>Ruangan</td>
                    <td tag="ruangan_nama"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Tindakan</legend>
            <table id="info_tindakan_temp" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="300">Nama Tindakan</th>
                        <th width="50">Qty</th>
                        <th width="150">Tarif</th>
                        <th>Total</th>
                    </tr>                    
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Obat ALkes</legend>
            <table id="info_alkes_temp" class="table table-bordered table-condensed">
                <thead>
                    <tr>
                        <th width="300">Nama Obat</th>
                        <th width="50">Qty</th>
                        <th width="150">Tarif</th>
                        <th>Total</th>
                    </tr>                    
                </thead>
                <tbody></tbody>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Pembayaran</legend>
            <table id="info_pembayaran_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Jumlah Pembayaran</td>
                    <td width="250" tag="jum_pembayaran" class="curency"></td>
                    <td width="150">Dari Nama</td>
                    <td tag="dari_nama"></td>
                </tr>
                <tr>
                    <td>Uang Diterima</td>
                    <td class="curency" tag="uang_diterima"></td>
                    <td >Sebagai Pembayaran</td>
                    <td tag="sbg_pembayaran"></td>
                </tr>
            </table>
        </fieldset>        
    </div>
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses(this);return false;',
                        'disabled'=>false,                        
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
<script>
function resetSubsidi(){
    if(!$('#inputTotalTind').is(':checked')){
        $("#totalsubsidiasuransi").attr('readonly',true);
        $("#totalsubsidirs").attr('readonly',true);
        $("#totalsubsidiasuransi").val(0.00);
        $("#totalsubsidirs").val(0.00);
    }
    if(!$('#inputTotalOa').is(':checked')){
        $("#totalsubsidiasuransi_oa").attr('readonly',true);
        $("#totalsubsidirs_oa").attr('readonly',true);
        $("#totalsubsidiasuransi_oa").val(0.00);
        $("#totalsubsidirs_oa").val(0.00);
    }
    if(!$('#inputTotalSemua').is(':checked')){
        $("#totalseluruhsubsidiasuransi").attr('readonly',true);
        $("#totalseluruhsubsidirs").attr('readonly',true);
        $("#totalseluruhsubsidiasuransi").val(0.00);
        $("#totalseluruhsubsidirs").val(0.00);
    }
    //reset subsidi tindakan
    $('#tblBayarTind').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val("0");
        $(this).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val("0");
    });
    hitungTotalSemuaTind();
    //reset subsidi obat
    $('#tblBayarOA').find(':checkbox:checked').each(function(){
        $(this).parents('tr').find('input[name$="[subsidiasuransi]"]').val("0");
        $(this).parents('tr').find('input[name$="[subsidirs]"]').val("0");
    });
    hitungTotalSemuaOa();
}
function cekInputTotalTindakan(obj){
    resetSubsidi();
    var jumlahAsuransi = $('#tblInputPenjamin tbody tr').length;
    if($(obj).is(":checked")){
        $("#inputTotalSemua").removeAttr('checked');
//        $(obj).parents('tr').find("#totalsubsidiasuransi").val(0.00);
        $(obj).parents('tr').find("#totalsubsidirs").val(0.00);
        $(obj).parents('tr').find("#totalsubsidiasuransi").attr('readonly', true);
        if (jumlahAsuransi < 2){
            $(obj).parents('tr').find("#totalsubsidiasuransi").removeAttr('readonly');
        }
        $(obj).parents('tr').find("#totalsubsidirs").removeAttr('readonly');
    }else{
        $(obj).parents('tr').find("#totalsubsidiasuransi").attr('readonly',true);
        $(obj).parents('tr').find("#totalsubsidirs").attr('readonly',true);
    }
    $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").val(0.00);
    $(obj).parents('tr').find("#totalseluruhsubsidirs").val(0.00);
    $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").attr('readonly',true);
    $(obj).parents('tr').find("#totalseluruhsubsidirs").attr('readonly',true);
//    hitungTotalSemua();
    hitungTotalAsuransi();
}
function cekInputTotalOa(obj){
    resetSubsidi();
    var jumlahAsuransi = $('#tblInputPenjamin tbody tr').length;
    if($(obj).is(":checked")){
        $("#inputTotalSemua").removeAttr('checked');
        $(obj).parents('tr').find("#totalsubsidiasuransi_oa").val(0.00);
        $(obj).parents('tr').find("#totalsubsidirs_oa").val(0.00);
        $(obj).parents('tr').find("#totalsubsidiasuransi_oa").attr('readonly', true);
        if (jumlahAsuransi < 2){
            $(obj).parents('tr').find("#totalsubsidiasuransi_oa").removeAttr('readonly');
        }
        $(obj).parents('tr').find("#totalsubsidirs_oa").removeAttr('readonly');
    }else{
        $(obj).parents('tr').find("#totalsubsidiasuransi_oa").attr('readonly',true);
        $(obj).parents('tr').find("#totalsubsidirs_oa").attr('readonly',true);
    }
    $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").val(0.00);
    $(obj).parents('tr').find("#totalseluruhsubsidirs").val(0.00);
    $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").attr('readonly',true);
    $(obj).parents('tr').find("#totalseluruhsubsidirs").attr('readonly',true);
//    hitungTotalSemua();
    hitungTotalAsuransi();
}
function cekInputTotal(obj){
    resetSubsidi();
    $("#inputTotalTind").removeAttr('checked');
    $("#inputTotalOa").removeAttr('checked');
//    $("#inputTotalSemua").removeAttr('checked');
    if($(obj).is(":checked")){
        $(obj).attr('checked',true);
        $(obj).parents('tr').find("#totalsubsidiasuransi").val(0.00);
        $(obj).parents('tr').find("#totalsubsidirs").val(0.00);
        $(obj).parents('tr').find("#totalsubsidiasuransi_oa").val(0.00);
        $(obj).parents('tr').find("#totalsubsidirs_oa").val(0.00);
        $(obj).parents('tr').find("#totalsubsidiasuransi").removeAttr('readonly');
        $(obj).parents('tr').find("#totalsubsidirs").removeAttr('readonly');
        $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").removeAttr('readonly');
        $(obj).parents('tr').find("#totalseluruhsubsidirs").removeAttr('readonly');
    }else{
//        $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").val(0.00);
        $(obj).parents('tr').find("#totalseluruhsubsidirs").val(0.00);
        $(obj).parents('tr').find("#totalseluruhsubsidiasuransi").attr('readonly',true);
        $(obj).parents('tr').find("#totalseluruhsubsidirs").attr('readonly',true);
    }
//    hitungTotalSemua();
    hitungTotalAsuransi();
}
function hitungAsuransi(){
    hitungTotalAsuransi();
    getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran"));
    jumlahBayarKartu();
    proporsiSubAsuransiSemua();
}

function hitungTotalAsuransi(){
    var totalPiutang = 0;
    $('#tblInputPenjamin tr').each(function(){
       totalPiutang += unformatNumber($(this).find('input[name$="[jmlpiutangasuransi]"]').val());       
    });
    $('#TandabuktibayarT_uangditerima').val(0);
    hitungTotalSemua(totalPiutang);
    hitungJmlBayar();
}

function hitungTotalSemua(totalPiutang=0){   
    var totalCyto = unformatNumber($('#totalcyto').val()) + unformatNumber($('#totalcyto_oa').val());
    var totalDiskon = unformatNumber($('#totaldiscount_tindakan').val()) + unformatNumber($('#totaldiscount_oa').val());

    var totalSubsidiAsuransi = totalPiutang + unformatNumber($('#totalsubsidiasuransi').val()) + unformatNumber($('#totalsubsidiasuransi_oa').val());
    if(totalPiutang>0){
        var totalSubsidiAsuransi = totalPiutang;
    }
    var totalSubsidiRs = unformatNumber($('#totalsubsidirs').val()) + unformatNumber($('#totalsubsidirs_oa').val());
    var totalBiaya = unformatNumber($('#totalbiayatindakan').val()) + unformatNumber($('#totalbiaya_oa').val());
    var totalIurBiaya = unformatNumber($('#totaliurbiaya').val()) + unformatNumber($('#totaliurbiaya_oa').val());
//    var totalIurBiaya = (totalBiaya+totalCyto) - (totalDiskon+totalSubsidiAsuransi+totalSubsidiRs);
    var total = unformatNumber($('#totalbayartindakan').val()) + unformatNumber($('#totalbayar_oa').val());
//    var total = (totalBiaya+totalCyto) - (totalDiskon+totalSubsidiAsuransi+totalSubsidiRs);
//    if($("#inputTotalSemua").is(":checked")){
//        //jangan di total jika input dari total
//    }else{

	//EHJ-3382 ----
	// totalIurBiaya	= totalIurBiaya-totalSubsidiAsuransi;
	// total			= total- totalSubsidiAsuransi;
	// -----------
	
    if (totalIurBiaya < 0){
        totalIurBiaya = 0;
    }
    if (total < 0){
        total = 0;
    }
    $("#totalseluruhcyto").val(formatDesimal(totalCyto));
    $("#totalseluruhdiscount").val(formatDesimal(totalDiskon));
    $("#totalseluruhsubsidiasuransi").val(formatDesimal(totalSubsidiAsuransi));
    $("#totalseluruhsubsidirs").val(formatDesimal(totalSubsidiRs));
//    }
    $("#totalseluruhiurbiaya").val(formatDesimal(totalIurBiaya));
    $("#totalseluruhbiaya").val(formatDesimal(totalBiaya));
    $("#totalseluruhbayar").val(formatDesimal(total));
}
//== perhitungan proporsi
function proporsiSubAsuransiSemua(){
    if($("#inputTotalSemua").is(":checked")){
	var totalSubsidiRs = unformatNumber($("#totalseluruhsubsidirs").val());
    var totalSubsidi = unformatNumber($("#totalseluruhsubsidiasuransi").val());
    var totalBiaya = unformatNumber($("#totalseluruhbiaya").val());
    var totalPiutang = 0;
	var totalCyto = unformatNumber($("#totalseluruhcyto").val());
    var totalDiscount = unformatNumber($("#totalseluruhdiscount").val());
	var totalIurBiaya = (totalBiaya) - (totalDiscount+totalSubsidiRs+totalSubsidi);
	var total = (totalBiaya) - (totalDiscount+totalSubsidiRs+totalSubsidi);
	
        $('#tblInputPenjamin tr').each(function(){
           totalPiutang += unformatNumber($(this).find('input[name$="[jmlpiutangasuransi]"]').val());       
        });
        // if (totalPiutang == 0){
            //proporsi subsidi tindakan
            $('#tblBayarTind').find(':checkbox:checked').each(function(){
                var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
                var tarif = unformatNumber($(this).parents('tr').find('input[name$="[tarif_satuan]"]').val());
                var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
                var proporsi = ((tarif * qty + tarifCyto) / totalBiaya) * totalSubsidi;
                $(this).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val(formatDesimal(proporsi));
            });
            hitungTotalSemuaTind();
            //proporsi subsidi obat
            $('#tblBayarOA').find(':checkbox:checked').each(function(){
                var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
                var harga = unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val());
                var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
                var proporsi = ((harga * qty + tarifCyto) / totalBiaya) * totalSubsidi;
                $(this).parents('tr').find('input[name$="[subsidiasuransi]"]').val(formatDesimal(proporsi));
            });
            hitungTotalSemuaOa();
        // }
	
        $("#totalseluruhsubsidiasuransi").val(formatDesimal(totalSubsidi));
        $("#totalseluruhsubsidirs").val(formatDesimal(totalSubsidiRs));
	$("#totalseluruhiurbiaya").val(formatDesimal(totalIurBiaya));
	$("#totalseluruhbayar").val(formatDesimal(total));
	
        // getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran"));
    }
    jumlahBayarKartu();
}

function proporsiSubRsSemua(){
    if($("#inputTotalSemua").is(":checked")){
        var totalSubsidiAsuransi = unformatNumber($("#totalseluruhsubsidiasuransi").val());
        var totalSubsidi = unformatNumber($("#totalseluruhsubsidirs").val());
        var totalBiaya = unformatNumber($("#totalseluruhbiaya").val());
        var totalCyto = unformatNumber($("#totalseluruhcyto").val());
        var totalDiscount = unformatNumber($("#totalseluruhdiscount").val());
	    var totalIurBiaya = (totalBiaya) - (totalDiscount+totalSubsidiAsuransi+totalSubsidi);
        var total = (totalBiaya) - (totalDiscount+totalSubsidiAsuransi+totalSubsidi);
		
	$('#tblBayarTind').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
            var tarif = unformatNumber($(this).parents('tr').find('input[name$="[tarif_satuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
            var proporsi = ((tarif * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaTind();
	$('#tblBayarOA').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
            var harga = unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
            var proporsi = ((harga * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsidirs]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaOa();
	
	$("#totalseluruhsubsidiasuransi").val(formatDesimal(totalSubsidiAsuransi));
	$("#totalseluruhsubsidirs").val(formatDesimal(totalSubsidi));
	$("#totalseluruhiurbiaya").val(formatDesimal(totalIurBiaya));
	$("#totalseluruhbayar").val(formatDesimal(total));
	
        getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran"));
	
    }
}
function proporsiSubAsuransiTind(){
    if($("#inputTotalTind").is(":checked")){
        var totalSubsidi = unformatNumber($("#totalsubsidiasuransi").val());
        var totalBiaya = unformatNumber($("#totalbiayatindakan").val());
        //proporsi subsidi tindakan
        $('#tblBayarTind').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
            var tarif = unformatNumber($(this).parents('tr').find('input[name$="[tarif_satuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
            var proporsi = ((tarif * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsidiasuransi_tindakan]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaTind();
    }
}
function proporsiSubRsTind(){
    if($("#inputTotalTind").is(":checked")){
        var totalSubsidi = unformatNumber($("#totalsubsidirs").val());
        var totalBiaya = unformatNumber($("#totalbiayatindakan").val());
        //proporsi subsidi tindakan
        $('#tblBayarTind').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_tindakan]"]').val());
            var tarif = unformatNumber($(this).parents('tr').find('input[name$="[tarif_satuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto_tindakan]"]').val());
            var proporsi = ((tarif * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsisidirumahsakit_tindakan]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaTind();
    }
}
function proporsiSubAsuransiOa(){
    if($("#inputTotalOa").is(":checked")){
        var totalSubsidi = unformatNumber($("#totalsubsidiasuransi_oa").val());
        var totalBiaya = unformatNumber($("#totalbiaya_oa").val());
        //proporsi subsidi obat
        $('#tblBayarOA').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
            var harga = unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
            var proporsi = ((harga * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsidiasuransi]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaOa();
    }
}
function proporsiSubRsOa(){
    if($("#inputTotalOa").is(":checked")){
        var totalSubsidi = unformatNumber($("#totalsubsidirs_oa").val());
        var totalBiaya = unformatNumber($("#totalbiaya_oa").val());
        //proporsi subsidi obat
        $('#tblBayarOA').find(':checkbox:checked').each(function(){
            var qty = unformatNumber($(this).parents('tr').find('input[name$="[qty_oa]"]').val());
            var harga = unformatNumber($(this).parents('tr').find('input[name$="[hargasatuan]"]').val());
            var tarifCyto = unformatNumber($(this).parents('tr').find('input[name$="[tarifcyto]"]').val());
            var proporsi = ((harga * qty + tarifCyto) / totalBiaya) * totalSubsidi;
            $(this).parents('tr').find('input[name$="[subsidirs]"]').val(formatDesimal(proporsi));
        });
        hitungTotalSemuaOa();
    }
}
//PERHITUNGAN OTOMATIS SETELAH HALAMAN DIMUAT SEMUA
hitungTotalSemuaOa();
hitungTotalSemuaTind();
hitungTotalSemua();
$('#hideshow').live('click', function(event) {        
         $('#divPenjamin').toggle('show');
});

</script>
<?php
if(isset($_GET['idPendaftaran'])){ 
    Yii::app()->clientScript->registerScript('loadRekeningDefault', '
        getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); //load data jurnal rekening
    ', CClientScript::POS_READY);
}
?>
