<style type="text/css">
    .btn-inverse{
        padding: 6px;
    }
</style>    
<?php $this->widget('bootstrap.widgets.BootAlert'); ?> 

<fieldset id="formDataPasien">
    <legend class="rim2">Transaksi Bayar <?php echo ((strtolower($this->action->id) == $this->actionBayarTagihanPasien) ? 'Tagihan Pasien' : 'Resep Pasien');?> </legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td><div class=" control-label"><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'no_rek')); ?></div></td>
            <td>
                <?php
                if(!empty($modPendaftaran->instalasi_id)){
                    echo CHtml::activeHiddenField($modPendaftaran,'instalasi_id',array('readonly'=>true));
                    echo CHtml::textField('BKPendaftaranT[instalasi_nama]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));
                }else{
                    echo CHtml::dropDownList('BKPendaftaranT[instalasi_id]', NULL, CHtml::listData($modPendaftaran->InstalasiUangMukaItems, 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'refreshDialogPendaftaran();', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                }
                ?>
            </td> 
            
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
            <label class="no_rek control-label">No Rekam Medik</label>
            </td>
            <td>
                <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                   if (Yii::app()->controller->module->id =='billingKasir') {
                     $pasien = 'daftarPasien';
                 }else{
                     $pasien = 'daftarPasienRuangan';
                 }
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'BKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/'.$pasien.'').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                           instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                loadPembayaran(ui.item.pendaftaran_id);
                                                return false;
                                            }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                                    'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span2', 
                                                        'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td><?php //echo CHtml::textField('BKPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); 
                    $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'BKPasienM[nama_pasien]',
                                           'value'=>$modPasien->nama_pasien,
                                           'source'=>'js: function(request, response) {
                                                          $.ajax({
                                                              url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienberdasarkanNama').'",
                                                              dataType: "json",
                                                              data: {
                                                                  '.strtolower($pasien).':true,
                                                                  term: request.term,
                                                                  instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                                              },
                                                              success: function (data) {
                                                                      response(data);
                                                              }
                                                          })
                                                       }',
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       isiDataPasien(ui.item);
                                                       loadPembayaran(ui.item.pendaftaran_id);
                                                       return false;
                                                   }',
                                           ),
                                       )); 
                   ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[jeniskasuspenyakit_nama]',  ((isset($modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama)) ? $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama : null), array('readonly'=>true)); ?></td>
            
            <?php if ((strtolower($this->action->id) == $this->actionBayarTagihanPasien)) { ?>
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
            <?php } else { 
                echo '<td>'. CHtml::label('No Resep','',array('class'=>'control-label no_rek')).'</td><td>';
                $this->widget('MyJuiAutoComplete', array(
                               'name'=>'no_resep',
                                'id'=>'no_resep',
//                               'value'=>$modPasien->nama_pasien,
                               'source'=>'js: function(request, response) {
                                              $.ajax({
                                                  url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/getNoResepObatPasien').'",
                                                  dataType: "json",
                                                  data: {
                                                      pendaftaran_id : $("#BKPendaftaranT_pendaftaran_id").val(),
                                                      term: request.term,
                                                  },
                                                  success: function (data) {
                                                          response(data);
                                                  }
                                              })
                                           }',
                                'options'=>array(
                                      'showAnim'=>'fold',
                                      'minLength' => 2,
                                      'focus'=> 'js:function( event, ui ) {
                                           $(this).val(ui.item.value);
                                           return false;
                                       }',
                                      'select'=>'js:function( event, ui ) {
                                           loadPembayaran($("#BKPendaftaranT_pendaftaran_id").val(), ui.item.penjualanresep_id);
                                           return false;
                                       }',
                               ),
//                               'htmlOptions'=>array(
//                                   'onblur'=>'loadPembayaran($("#BKPendaftaranT_pendaftaran_id").val())',
//                                   'onkeypress' =>"if (jQuery.trim($(this).val()) != ''){if (event.keyCode == 13){loadPembayaran($('#BKPendaftaranT_pendaftaran_id').val());}}",
//                               ),
                           )); 
                echo '<td>';
            }
            
            ?>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[instalasi_nama]',  ((isset($modPendaftaran->instalasi->instalasi_nama)) ? $modPendaftaran->instalasi->instalasi_nama : null), array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[ruangan_nama]', ((isset($modPendaftaran->ruangan->ruangan_nama)) ? $modPendaftaran->ruangan->ruangan_nama : null), array('readonly'=>true)); ?></td>
            
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'carabayar_id',array('class'=>'control-label')) ?>
            </td>
            <td><?php // echo "cara bayar id : ".$modPendaftaran->carabayar_id; ?>
                <?php echo CHtml::textField('BKPendaftaranT[carabayar_nama]', (isset($modPendaftaran->carabayar->carabayar_nama) ? $modPendaftaran->carabayar->carabayar_nama : ""), array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[carabayar_id]', $modPendaftaran->carabayar_id, array('readonly'=>true));   ?>
                <?php 
                if($modPendaftaran->carabayar_id == Params::CARABAYAR_ASURANSI || $modPendaftaran->carabayar_id == Params::CARABAYAR_BPJS){
                     echo CHtml::Link('<i class="icon-plus icon-white"></i>','',
                                array('id'=>'hideshow',                                    
                                      'class'=>'btn btn-primary',
                                      'rel'=>'tooltip',
                                      'title'=>'Klik Untuk Menambahkan Cara bayar & Penjamin',
                                    )); 
                }
                else{
                    echo CHtml::Link('<i class="icon-plus icon-white"></i>','',
                                array('id'=>'idVisible',                                    
                                      'class'=>'btn btn-inverse',
                                      'rel'=>'tooltip',
                                      'title'=>'Cara bayar hanya untuk Asuransi dan BPJS',
                                    )); 
                }
                   
                ?>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            
            <td>
                <?php echo CHtml::activeLabel($modPendaftaran, 'penjamin_id',array('class'=>'control-label')) ?>
            </td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[penjamin_nama]', (isset($modPendaftaran->penjamin->penjamin_nama) ? $modPendaftaran->penjamin->penjamin_nama : ""), array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[penjamin_id]', $modPendaftaran->penjamin_id, array('readonly'=>true));   ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[kelaspelayanan_id]',((isset($modPendaftaran->kelaspelayanan_id)) ? $modPendaftaran->kelaspelayanan_id : ''), array('readonly'=>true)); ?></td>
            </td>
        </tr>
    </table>
    <div id="divPenjamin" class="hide">
        <legend class="rim">Penambahan Penjamin</legend>
        <table id="tblInputPenjamin" class="table table-bordered table-condensed"  width="100%" style="overflow-x: scroll;">
            <thead>
                <tr>
                    <!--<th width="10">No.</th>-->
                    <th>Cara Bayar</th>
                    <th>Penjamin</th>
                    <th>Jumlah Subsidi Asuransi</th>
                    <th>Tanggal Jatuh Tempo</th>
                    <th>Tambah Asuransi</th>
                </tr>
            </thead>
            <tbody>
            <?php
                if (count($modDialogPenjamin) == 0){
                    $this->renderPartial('billingKasir.views.pembayaran.penjamin._rowPenjaminPertama',array('form'=>$form, 'modPenjamin'=>$modPenjamin, 'modDialogPenjamin'=>$modDialogPenjamin, 'modPendaftaran'=>$modPendaftaran));
                } else {
                    $this->renderPartial('billingKasir.views.pembayaran.penjamin._rowPenjaminPiutang',array('form'=>$form, 'modPenjamin'=>$modPenjamin, 'modDialogPenjamin'=>$modDialogPenjamin, 'modPendaftaran'=>$modPendaftaran));
                }
            ?>
            </tbody>
        </table>
    </div>
</fieldset> 
<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $modDialogPasien = new BKPasienM('searchTagihanPasien');
    $modDialogPasien->unsetAttributes();
    if(isset($_GET['BKPasienM'])) {
        $modDialogPasien->attributes = $_GET['BKPasienM'];
        $modDialogPasien->idInstalasi = $_GET['BKPasienM']['idInstalasi'];
        $modDialogPasien->no_pendaftaran = $_GET['BKPasienM']['no_pendaftaran'];
        $modDialogPasien->tgl_pendaftaran_cari = $_GET['BKPasienM']['tgl_pendaftaran_cari'];
        $modDialogPasien->instalasi_nama = $_GET['BKPasienM']['instalasi_nama'];
        $modDialogPasien->carabayar_nama = $_GET['BKPasienM']['carabayar_nama'];
        $modDialogPasien->ruangan_nama = $_GET['BKPasienM']['ruangan_nama'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pendaftaran-t-grid',
            'dataProvider'=>$modDialogPasien->searchTagihanPasien(),
            'filter'=>$modDialogPasien,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPasien\").dialog(\"close\");
                                            $(\"#BKPendaftaranT_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                            $(\"#BKPendaftaranT_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                            $(\"#BKPendaftaranT_umur\").val(\"$data->umur\");
                                            $(\"#BKPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");
                                            $(\"#BKPendaftaranT_instalasi_id\").val(\"$data->instalasi_id\");
                                            $(\"#BKPendaftaranT_instalasi_nama\").val(\"$data->instalasi_nama\");
                                            $(\"#BKPendaftaranT_ruangan_nama\").val(\"$data->ruangan_nama\");
                                            $(\"#BKPendaftaranT_pendaftaran_id\").val(\"$data->pendaftaran_id\");
                                            $(\"#BKPendaftaranT_carabayar_id\").val(\"$data->carabayar_id\");
											$(\"#BKPiutangasuransiT_0_carabayar_id\").val(\"$data->carabayar_id\");
											$(\"#BKPiutangasuransiT_0_penjamin_id\").val(\"$data->penjamin_id\");	
                                            $(\"#BKPendaftaranT_penjamin_id\").val(\"$data->penjamin_id\");
                                            $(\"#BKPendaftaranT_pasien_id\").val(\"$data->pasien_id\");
                                            $(\"#BKPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");
                                            $(\"#BKPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                            $(\"#BKPendaftaranT_kelaspelayanan_id\").val(\"$data->kelaspelayanan_id\");
                                            $(\"#FATandabuktibayarUangMukaT_darinama_bkm\").val(\"$data->nama_pasien\");

                                            $(\"#BKPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                            $(\"#BKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                            $(\"#BKPasienM_nama_pasien\").val(\"$data->nama_pasien\");
                                            $(\"#BKPasienM_nama_bin\").val(\"$data->nama_bin\");
                                            $(\"#BKPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                            $(\"#BKPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");
                                            loadPembayaran($data->pendaftaran_id);

                                        "))',
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    'jeniskelamin',
                    'no_pendaftaran',
                    array(
                        'name'=>'tgl_pendaftaran',
                        'filter'=> 
                        CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran_cari', array('placeholder'=>'contoh: 15 Jan 2013')),
                    ),
                    array(
                        'name'=>'instalasi_nama',
                        'type'=>'raw',
                    ),
                    array(
                        'name'=>'ruangan_nama',
                        'type'=>'raw',
                    ),
                    array(
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                    ),


            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>
<script type="text/javascript">
function isiDataPasien(data)
{
    $('#BKPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
    $('#BKPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
    $('#BKPendaftaranT_umur').val(data.umur);
    $('#BKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#BKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#BKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#BKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#BKPendaftaranT_pasien_id').val(data.pasien_id);
    $('#BKPendaftaranT_carabayar_id').val(data.carabayar_id);
    $('#BKPendaftaranT_penjamin_id').val(data.penjamin_id);
    $('#BKPendaftaranT_kelaspelayanan_id').val(data.kelaspelayanan_id);		
    if (typeof data.norekammedik !=  'undefined'){
        $('#BKPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#BKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#BKPasienM_nama_pasien').val(data.namapasien);
    $('#BKPasienM_nama_bin').val(data.namabin);
    $('#BKPendaftaranT_carabayar_nama').val(data.carabayar_nama);
    $('#BKPendaftaranT_penjamin_nama').val(data.penjamin_nama);
}

function loadPembayaran(idPendaftaran,penjualanresep)
{
    
    <?php $tindakanParam = ((strtolower($this->action->id) == $this->actionBayarTagihanPasien) ? ',tindakan : 1' : ',tindakan : 0');?>
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaran');?>', {idPendaftaran:idPendaftaran,penjualanResep : penjualanresep<?php echo $tindakanParam;?>}, function(data){
        $("#checkTindakan").attr('checked', true);
        $("#checkAllObat").attr('checked', true);
        setTimeout('checkAllTindakan()', 500);
        setTimeout('checkAll()', 500);
        $('#tblBayarTind tbody').html(data.formBayarTindakan);
        $('#tblBayarOA tbody').html(data.formBayarOa);
        $('#totTagihan').val(formatDesimal(data.tottagihan));
        
        $('#TandabuktibayarT_jmlpembayaran').val(formatDesimal(data.jmlpembayaran));
        $('#TandabuktibayarT_jmlpembulatan').val(formatDesimal(data.jmlpembulatan));
        $('#TandabuktibayarT_uangditerima').val(formatDesimal(data.uangditerima));
        $('#TandabuktibayarT_uangkembalian').val(formatDesimal(data.uangkembalian));
        $('#TandabuktibayarT_biayamaterai').val(formatDesimal(data.biayamaterai));
        $('#TandabuktibayarT_biayaadministrasi').val(formatDesimal(data.biayaadministrasi));
        
        var norekammedik = $('#BKPasienM_no_rekam_medik').val();
        var no_pendaftaran = $('#BKPendaftaranT_no_pendaftaran').val();
        var nama_pembayar = norekammedik + '-' + no_pendaftaran + '-' + data.namapasien;
        
        if(penjualanresep != undefined)
        {
            var no_resep = $('#no_resep').val();
            nama_pembayar = norekammedik + '-' + no_resep + '-' + data.namapasien;
        }
//        $('#TandabuktibayarT_darinama_bkm').val(data.namapasien);
        $('#TandabuktibayarT_darinama_bkm').val(nama_pembayar);
        $('#TandabuktibayarT_alamat_bkm').val(data.alamatpasien);
        
        var discount = 0;
        discount = unformatNumber($('#totaldiscount_tindakan').val()) + unformatNumber($('#totaldiscount_oa').val());
        $('#disc').val(0);
        $('#discount').val(0);
        $('#tblBayarTind').find('input.currency:text').each(function(){
//          JANGAN DIBULATKAN >>  $(this).maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
            $(this).maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
        });
        $('#tblBayarOA').find('input.currency:text').each(function(){
//          JANGAN DIBULATKAN >>    $(this).maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
            $(this).maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
        });
        $('#tblBayarOA').find('input.number:text').each(function(){
//          JANGAN DIBULATKAN >>    $(this).maskMoney({"symbol":"Rp. ","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":0});
            $(this).maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
        });
        $('.currency').each(function(){this.value = formatDesimal(this.value)});
        //Load Deposit
        $('#deposit').val(formatDesimal(data.deposit));
        $('#TandabuktibayarT_uangditerima').focus();
        hitungTotalSemuaTind();
        hitungTotalSemuaOa();
        //$('#tblBayarOA').find('input.currency:text').each(function(){this.value = formatDesimal(this.value)});
        getDataRekeningKasir($("#formDataPasien"),$("#formDataPembayaran")); //load data jurnal rekening
    }, 'json');
    
}

function refreshDialogPendaftaran(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    var instalasiNama = $("#BKPendaftaranT_instalasi_id option:selected").text();
    $.fn.yiiGridView.update('pendaftaran-t-grid', {
        data: {
            "BKPasienM[idInstalasi]":instalasiId,
            "BKPasienM[instalasi_nama]":instalasiNama,
        }
    });
}
    
function cekInstalasi(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    if(instalasiId.length > 0){
        return true;
    }else{
        alert("Silahkan pilih instalasi ! ");
        $("#BKPendaftaranT_instalasi_id").focus();
        return false;
    }
}

/**
 * set dropdown penjamin 
 * @param {type} carabayar_id
 * @returns {undefined}
 */
function setDropDownPenjamin(obj)
{
    var i = obj.id.replace("BKPiutangasuransiT_","").replace("_carabayar_id","");
    var carabayar_id = obj.value;
    $.ajax({
       type:'POST',
       url:'<?php echo $this->createUrl('SetDropdownPenjamin'); ?>',
       data: {carabayar_id : carabayar_id},
       dataType: "json",
       success:function(data){
           $("#BKPiutangasuransiT_"+i+"_penjamin_id").html(data.listPenjamin);
       },
        error: function (jqXHR, textStatus, errorThrown) { console.log(errorThrown);}
    });
}

// the subviews rendered with placeholders
var trTindakan = new String(<?php echo CJSON::encode($this->renderPartial('billingKasir.views.pembayaran.penjamin._rowPenjamin',array('form'=>$form, 'modPenjamin'=>$modPenjamin, 'modDialogPenjamin'=>$modDialogPenjamin, 'modPendaftaran'=>$modPendaftaran, 'removeButton'=>true),true));?>);
var trTindakanFirst = new String(<?php echo CJSON::encode($this->renderPartial('billingKasir.views.pembayaran.penjamin._rowPenjamin',array('form'=>$form, 'modPenjamin'=>$modPenjamin, 'modDialogPenjamin'=>$modDialogPenjamin, 'modPendaftaran'=>$modPendaftaran,'removeButton'=>false),true));?>);

function addRowTindakan(obj)
{
    $(obj).parents('table').children('tbody').append(trTindakan.replace());
    $('#tblInputPenjamin').find('input[name*="[ii]"][class*="currency"]').each(function(){
            $(this).maskMoney({"symbol":"","defaultZero":true,"allowZero":true,"decimal":".","thousands":",","precision":2});
    });
    var jumlahAsuransi = $('#tblInputPenjamin tbody tr').length;
    if($("#inputTotalTind").is(":checked")){
        $("#totalsubsidiasuransi_oa").attr('readonly', true);
        $("#totalsubsidiasuransi").attr('readonly', true);
        if (jumlahAsuransi < 2){
            $("#totalsubsidiasuransi_oa").removeAttr('readonly');
            $("#totalsubsidiasuransi").removeAttr('readonly');
        }
    }else{
        $("#totalsubsidiasuransi_oa").attr('readonly', true);
        $("#totalsubsidiasuransi").attr('readonly', true);
    }
    $('.currency').each(function(){this.value = formatDesimal(this.value)});
    <?php 
        $attributes = $modPenjamin->attributeNames(); 
        foreach($attributes as $i=>$attribute){
            echo "renameInput('BKPiutangasuransiT','$attribute');";
        }
    ?>
    $('#tblInputPenjamin tbody').each(function(){
        jQuery('input[name$="[tglpiutangasuransi]"]').datepicker(
            jQuery.extend(
                {
                    showMonthAfterYear:false
                }, 
                jQuery.datepicker.regional['id'],
                {
                    'dateFormat':'dd M yy',
//                    'maxDate':'d',
//                    'timeText':'Waktu',
//                    'hourText':'Jam',
//                    'minuteText':'Menit',
//                    'secondText':'Detik',
                    'showSecond':false,
//                    'timeOnlyTitle':'Pilih Waktu',
                    'timeFormat':'hh:mm:ss',
                    'changeYear':true,
                    'changeMonth':true,
                    'showAnim':'fold',
                    'yearRange':'-80y:+20y'
                }
            )
        );
    });
    
    jQuery('<?php echo Params::TOOLTIP_SELECTOR; ?>').tooltip({"placement":"<?php echo Params::TOOLTIP_PLACEMENT;?>"});
}

function batalTindakan(obj)
{
    if(confirm('Apakah anda yakin akan menghapus penjamin?')){
        $(obj).parents('tr').next('tr').detach();
        $(obj).parents('tr').detach();
        var jumlahAsuransi = $('#tblInputPenjamin tbody tr').length;
        if($("#inputTotalTind").is(":checked")){
            $("#totalsubsidiasuransi_oa").attr('readonly', true);
            $("#totalsubsidiasuransi").attr('readonly', true);
            if (jumlahAsuransi < 2){
                $("#totalsubsidiasuransi_oa").removeAttr('readonly');
                $("#totalsubsidiasuransi").removeAttr('readonly');
            }
        }else{
            $("#totalsubsidiasuransi_oa").attr('readonly', true);
            $("#totalsubsidiasuransi").attr('readonly', true);
        }
        <?php 
            foreach($attributes as $i=>$attribute){
                echo "renameInput('BKPiutangasuransiT','$attribute');";
            }
        ?>
    }
}

function renameInput(modelName,attributeName)
{
    var trLength = $('#tblInputPenjamin tr').length;
    var i = -1;
    $('#tblInputPenjamin tr').each(function(){
        if($(this).has('select[name$="[carabayar_id]"]').length){
            i++;
        }
        $(this).find('input[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('input[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
        $(this).find('select[name$="['+attributeName+']"]').attr('name',modelName+'['+i+']['+attributeName+']');
        $(this).find('select[name$="['+attributeName+']"]').attr('id',modelName+'_'+i+'_'+attributeName+'');
    });
}

</script>
