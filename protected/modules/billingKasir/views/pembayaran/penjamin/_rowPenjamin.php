<?php
   echo "<tr>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '[ii]carabayar_id', CHtml::listData($modPendaftaran->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$modPendaftaran->carabayar_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",
                'onchange'=>'setDropDownPenjamin(this)',
                'class'=>'span2',
            ));
        echo "</td>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '[ii]penjamin_id', CHtml::listData($modPendaftaran->getPenjaminItems($modPendaftaran->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$modPendaftaran->penjamin_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2'));
        echo "</td>";        
        echo "<td>";
            echo $form->textField($modPenjamin,'[ii]jmlpiutangasuransi',array('class'=>'span2 currency', 'onblur'=>'hitungAsuransi();', 'onkeypress'=>"return $(this).focusNextInputField(event);"));
        echo "</td>";
        echo "<td>";
            $modPenjamin->tglpiutangasuransi = date('d M Y');
            $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPenjamin,
                                         'attribute'=>'[ii]tglpiutangasuransi',
                                         'mode'=>'date',
                                         'options'=> array(
                                             'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                             'maxDate' => 'd',
                                         ),
                                         'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                         ),
                         ));
        echo "</td>";
        echo'<td>';
            if(!isset($removeButton)){
                $removeButton = false;
            }
            if($removeButton){
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin')); 
                echo "<br/><br/>";
                echo CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'batalTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan penjamin'));
            } else {
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin'));
            }
        echo'</td>';
   echo "</tr>";
?>
