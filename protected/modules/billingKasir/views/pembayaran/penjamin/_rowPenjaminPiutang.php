<?php
$format = new CustomFormat;
foreach ($modDialogPenjamin as $i=>$penjamin){
   echo "<tr>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '['.$i.']carabayar_id', CHtml::listData($modPendaftaran->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$penjamin->carabayar_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",
                'onchange'=>'setDropDownPenjamin(this.value)',
                'class'=>'span2','disabled'=>'disabled'
            ));
            echo CHtml::hiddenField('BKPiutangasuransiT['.$i.'][carabayar_id]', $penjamin->carabayar_id, array('readonly'=>true));
        echo "</td>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '['.$i.']penjamin_id', CHtml::listData($modPendaftaran->getPenjaminItems($penjamin->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$penjamin->penjamin_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','disabled'=>'disabled'));
            echo CHtml::hiddenField('BKPiutangasuransiT['.$i.'][penjamin_id]', $penjamin->penjamin_id, array('readonly'=>true));
        echo "</td>";
        echo "<td>";
            echo $form->textField($modPenjamin,'['.$i.']jmlpiutangasuransi', array('value'=>$penjamin->jmlpiutangasuransi, 'class'=>'span2 currency', 'readonly'=>'true', 'onkeypress'=>"return $(this).focusNextInputField(event);"));
        echo "</td>";
        echo "<td>";
            $modPenjamin->tglpiutangasuransi = $format->formatDateINAtimeNew($penjamin->tglpiutangasuransi);
            $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPenjamin,
                                         'attribute'=>'['.$i.']tglpiutangasuransi',
                                         'mode'=>'date',
                                         'options'=> array(
                                             'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                             'maxDate' => 'd',
                                         ),
                                         'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                         ),
                         ));
        echo "</td>";
        echo'<td>';
            if(!isset($removeButton)){
                $removeButton = false;
            }
            if($removeButton){
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin')); 
                echo "<br/><br/>";
                echo CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'batalTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan penjamin'));
            } else {
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin'));
            }
        echo'</td>';
   echo "</tr>";
}
?>
