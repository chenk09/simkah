<?php
   echo "<tr>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '[0]carabayar_id', CHtml::listData($modPendaftaran->getCaraBayarItems(), 'carabayar_id', 'carabayar_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$modPendaftaran->carabayar_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",
                'onchange'=>'setDropDownPenjamin(this)',
                'class'=>'span2','disabled'=>'disabled'
            ));
            echo CHtml::hiddenField('BKPiutangasuransiT[0][carabayar_id]', $modPendaftaran->carabayar_id, array('readonly'=>true));
        echo "</td>";
        echo "<td>";
            echo $form->dropDownList($modPenjamin, '[0]penjamin_id', CHtml::listData($modPendaftaran->getPenjaminItems($modPendaftaran->carabayar_id), 'penjamin_id', 'penjamin_nama') ,array('empty'=>'-- Pilih --', 'options'=>array("$modPendaftaran->penjamin_id"=>array("selected"=>"selected")), 'onkeypress'=>"return $(this).focusNextInputField(event)",'class'=>'span2','disabled'=>'disabled'));
            echo CHtml::hiddenField('BKPiutangasuransiT[0][penjamin_id]', $modPendaftaran->penjamin_id, array('readonly'=>true));
        echo "</td>";        
        echo "<td>";
            echo $form->textField($modPenjamin,'[0]jmlpiutangasuransi',array('class'=>'span2 currency', 'onblur'=>'hitungAsuransi();', 'onkeypress'=>"return $(this).focusNextInputField(event);"));
        echo "</td>";
        echo "<td>";
            $modPenjamin->tglpiutangasuransi = date('d M Y');
            $this->widget('MyDateTimePicker',array(
                                         'model'=>$modPenjamin,
                                         'attribute'=>'[0]tglpiutangasuransi',
                                         'mode'=>'date',
                                         'options'=> array(
                                             'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
//                                             'maxDate' => 'd',
                                         ),
                                         'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                         ),
                         ));
        echo "</td>";
        echo'<td>';
            if(!isset($removeButton)){
                $removeButton = false;
            }
            if($removeButton){
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin')); 
                echo "<br/><br/>";
                echo CHtml::link("<i class='icon-minus'></i>", '#', array('onclick'=>'batalTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk membatalkan penjamin'));
            } else {
                echo CHtml::link("<i class='icon-plus'></i>", '#', array('onclick'=>'addRowTindakan(this);return false;','rel'=>'tooltip','title'=>'Klik untuk menambah penjamin'));
            }
        echo'</td>';
   echo "</tr>";
?>
