<?php $this->widget('bootstrap.widgets.BootAlert'); ?> 

<fieldset>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            <td><div class=" control-label"><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'no_rek')); ?></div></td>
            <td>
                <?php
                if(!empty($modPendaftaran->instalasi_id)){
                    echo CHtml::textField('BKPendaftaranT[instalasi_id]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));
                }else{
                    echo CHtml::dropDownList('BKPendaftaranT[instalasi_id]', NULL, CHtml::listData($modPendaftaran->InstalasiUangMukaItems, 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'refreshDialogPendaftaran();', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                }
                ?>
            </td>            
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
               <label class="control-label no_rek" >No Rekam Medik</label>
            </td>
            <td>
                <div class="control-group"  id="PasienLab">
                <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                    if (!empty($modPasien->no_rekam_medik)) { 
                    echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true));
                    }else{
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'BKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienInstalasi').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                           instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 1,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                loadPembayaran(ui.item.pendaftaran_id);
                                                return false;
                                            }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPasienLab','idTombol'=>'tombolPasienDialogLab'),
                                    'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span2', 
                                                        'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                    }
                ?>
            </div>
            <div class="control-group"  id="pasien">
                <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                    if (!empty($modPasien->no_rekam_medik)) { 
                    echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true));
                    }else{
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'BKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienInstalasi').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                           instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 1,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                loadPembayaran(ui.item.pendaftaran_id);
                                                return false;
                                            }',
                                    ),
                                    'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialogLab'),
                                    'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span2', 
                                                        'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                    }
                ?>
            </div>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td><?php //echo CHtml::textField('BKPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); 
                    $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'BKPasienM[nama_pasien]',
                                           'value'=>$modPasien->nama_pasien,
                                           'source'=>'js: function(request, response) {
                                                          $.ajax({
                                                              url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienberdasarkanNama').'",
                                                              dataType: "json",
                                                              data: {
                                                                  daftarpasien:true,
                                                                  instalasiId: $("#BKPendaftaranT_instalasi_id").val(),
                                                                  term: request.term,
                                                              },
                                                              success: function (data) {
                                                                      response(data);
                                                              }
                                                          })
                                                       }',
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       isiDataPasien(ui.item);
                                                       loadPembayaran(ui.item.pendaftaran_id);
                                                       return false;
                                                   }',
                                           ),
                                       )); 
            ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[jeniskasuspenyakit_nama]',$modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>   
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[instalasi_nama]',$modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasienadmisi_id]',$modPendaftaran->pasienadmisi_id, array('readonly'=>true)); ?>
            </td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[ruangan_nama]', $modPendaftaran->ruangan->ruangan_nama, array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset> 
<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $modDialogPasien = new BKPasienM('searchPasienRumahsakitV');
    $modDialogPasien->unsetAttributes();
    if(isset($_GET['BKPasienM'])) {
        $modDialogPasien->attributes = $_GET['BKPasienM'];
        $modDialogPasien->idInstalasi = $_GET['BKPasienM']['idInstalasi'];
        $modDialogPasien->no_pendaftaran = $_GET['BKPasienM']['no_pendaftaran'];
        $modDialogPasien->tgl_pendaftaran_cari = $_GET['BKPasienM']['tgl_pendaftaran_cari'];
        $modDialogPasien->instalasi_nama = $_GET['BKPasienM']['instalasi_nama'];
        $modDialogPasien->carabayar_nama = $_GET['BKPasienM']['carabayar_nama'];
        $modDialogPasien->ruangan_nama = $_GET['BKPasienM']['ruangan_nama'];
        $modDialogPasien->nama_pasien = $_GET['BKPasienM']['nama_pasien'];

    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pendaftaran-t-grid',
            'dataProvider'=>$modDialogPasien->searchPasienRumahsakitV(),
            'filter'=>$modDialogPasien,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPasien\").dialog(\"close\");
                                            $(\"#BKPendaftaranT_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                            $(\"#BKPendaftaranT_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                            $(\"#BKPendaftaranT_umur\").val(\"$data->umur\");
                                            $(\"#BKPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");
                                            $(\"#BKPendaftaranT_instalasi_id\").val(\"$data->instalasi_id\");
                                            $(\"#BKPendaftaranT_instalasi_nama\").val(\"$data->instalasi_nama\");
                                            $(\"#BKPendaftaranT_ruangan_nama\").val(\"$data->ruangan_nama\");
                                            $(\"#BKPendaftaranT_pendaftaran_id\").val(\"$data->pendaftaran_id\");
                                            $(\"#BKPendaftaranT_carabayar_id\").val(\"$data->carabayar_id\");
                                            $(\"#BKPendaftaranT_penjamin_id\").val(\"$data->penjamin_id\");
                                            $(\"#BKPendaftaranT_kelaspelayanan_id\").val(\"$data->kelaspelayanan_id\");
                                            $(\"#BKPendaftaranT_pasien_id\").val(\"$data->pasien_id\");
                                            $(\"#BKTandabuktibayarUangMukaT_darinama_bkm\").val(\"$data->nama_pasien\");

                                            $(\"#BKPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                            $(\"#BKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                            $(\"#BKPasienM_nama_pasien\").val(\"$data->nama_pasien\");
                                            $(\"#BKPasienM_nama_bin\").val(\"$data->nama_bin\");
                                            $(\"#BKPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                            $(\"#BKPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");
                                            loadPembayaran($data->pendaftaran_id);

                                        "))',
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    'jeniskelamin',
                    'no_pendaftaran',
                    array(
                        'name'=>'tgl_pendaftaran',
                        'filter'=> 
                        CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran_cari', array('placeholder'=>'contoh: 15 Jan 2013')),
                    ),
                    array(
                        'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'filter'=> 
                        CHtml::activeHiddenField($modDialogPasien, 'idInstalasi', array())."".CHtml::activeTextField($modDialogPasien, 'instalasi_nama', array()),

                    ), 
                    array(
                        'name'=>'ruangan_nama',
                        'type'=>'raw',
                    ),
                    array(
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                    ),


            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>
<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasienLab',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $modDialogPasienLab = new BKPasienM('searchPasienRumahsakitV');
    $modDialogPasienLab->unsetAttributes();
    if(isset($_GET['BKPasienM'])) {
        $modDialogPasienLab->attributes = $_GET['BKPasienM'];
        $modDialogPasienLab->idInstalasi = $_GET['BKPasienM']['idInstalasi'];
        $modDialogPasienLab->no_pendaftaran = $_GET['BKPasienM']['no_pendaftaran'];
        $modDialogPasienLab->tgl_pendaftaran_cari = $_GET['BKPasienM']['tgl_pendaftaran_cari'];
        $modDialogPasienLab->instalasi_nama = $_GET['BKPasienM']['instalasi_nama'];
        $modDialogPasienLab->carabayar_nama = $_GET['BKPasienM']['carabayar_nama'];
        $modDialogPasienLab->ruangan_nama = $_GET['BKPasienM']['ruangan_nama'];
        $modDialogPasienLab->nama_pasien = $_GET['BKPasienM']['nama_pasien'];

    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pendaftaran-t-lab-grid',
            'dataProvider'=>$modDialogPasienLab->searchPasienRumahsakitV(),
            'filter'=>$modDialogPasienLab,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPasienLab\").dialog(\"close\");
                                            $(\"#BKPendaftaranT_tgl_pendaftaran\").val(\"$data->tgl_pendaftaran\");
                                            $(\"#BKPendaftaranT_no_pendaftaran\").val(\"$data->no_pendaftaran\");
                                            $(\"#BKPendaftaranT_umur\").val(\"$data->umur\");
                                            $(\"#BKPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->jeniskasuspenyakit_nama\");
                                            $(\"#BKPendaftaranT_instalasi_id\").val(\"$data->instalasi_id\");
                                            $(\"#BKPendaftaranT_instalasi_nama\").val(\"$data->instalasi_nama\");
                                            $(\"#BKPendaftaranT_ruangan_nama\").val(\"$data->ruangan_nama\");
                                            $(\"#BKPendaftaranT_pendaftaran_id\").val(\"$data->pendaftaran_id\");
                                            $(\"#BKPendaftaranT_carabayar_id\").val(\"$data->carabayar_id\");
                                            $(\"#BKPendaftaranT_penjamin_id\").val(\"$data->penjamin_id\");
                                            $(\"#BKPendaftaranT_kelaspelayanan_id\").val(\"$data->kelaspelayanan_id\");
                                            $(\"#BKPendaftaranT_pasien_id\").val(\"$data->pasien_id\");
                                            $(\"#BKTandabuktibayarUangMukaT_darinama_bkm\").val(\"$data->nama_pasien\");

                                            $(\"#BKPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                            $(\"#BKPasienM_no_rekam_medik\").val(\"$data->no_rekam_medik\");
                                            $(\"#BKPasienM_nama_pasien\").val(\"$data->nama_pasien\");
                                            $(\"#BKPasienM_nama_bin\").val(\"$data->nama_bin\");
                                            $(\"#BKPendaftaranT_carabayar_nama\").val(\"$data->carabayar_nama\");
                                            $(\"#BKPendaftaranT_penjamin_nama\").val(\"$data->penjamin_nama\");
                                            loadPembayaran($data->pendaftaran_id);

                                        "))',
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->nama_pasien',
                    ),
                    'jeniskelamin',
                    'no_pendaftaran',
                    array(
                        'name'=>'tgl_pendaftaran',
                        'filter'=> 
                        CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran_cari', array('placeholder'=>'contoh: 15 Jan 2013')),
                    ),
                    array(
                        'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'filter'=> 
                        CHtml::activeHiddenField($modDialogPasien, 'idInstalasi', array())."".CHtml::activeTextField($modDialogPasien, 'instalasi_nama', array()),

                    ), 
                    // array(
                    //     'name'=>'ruangan_nama',
                    //     'type'=>'raw',
                    // ),
                    array(
                        'name'=>'carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->carabayar_nama',
                    ),


            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>

<script type="text/javascript">
function isiDataPasien(data)
{
    $('#BKPendaftaranT_tgl_pendaftaran').val(data.tgl_pendaftaran);
    $('#BKPendaftaranT_no_pendaftaran').val(data.no_pendaftaran);
    $('#BKPendaftaranT_umur').val(data.umur);
    $('#BKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#BKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#BKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#BKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#BKPendaftaranT_pasien_id').val(data.pasien_id);
    $('#BKPendaftaranT_pasienadmisi_id').val(data.pasienadmisi_id);
    if (typeof data.norekammedik !=  'undefined'){
        $('#BKPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#BKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#BKPasienM_nama_pasien').val(data.namapasien);
    $('#BKPasienM_nama_bin').val(data.namabin);
    
    //$('#BKTandabuktibayarUangMukaT_jmlpembayaran').focus();
    //$('#BKTandabuktibayarUangMukaT_jmlpembayaran').select();    
}

function loadPembayaran(idPendaftaran)
{
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaranUangMuka');?>', {idPendaftaran:idPendaftaran}, function(data){
        $('#TandabuktibayarT_jmlpembayaran').val(data.jmlpembayaran);
        $('#totTagihan').val(formatUang(data.tottagihan));
        $('#totPembebasan').val(formatUang(data.totpembebasan));
        
        var norekammedik = $('#BKPasienM_no_rekam_medik').val();
        var no_pendaftaran = $('#BKPendaftaranT_no_pendaftaran').val();
        var nama_pembayar = norekammedik + '-' + no_pendaftaran + '-' + data.namapasien;
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').val(formatUang(0));
        $('#BKTandabuktibayarUangMukaT_uangditerima').val(formatUang(0));
        $('#BKTandabuktibayarUangMukaT_jmlpembulatan').val(formatUang(data.jmlpembulatan));
        $('#BKTandabuktibayarUangMukaT_uangkembalian').val(formatUang(data.uangkembalian));
        $('#BKTandabuktibayarUangMukaT_biayamaterai').val(formatUang(data.biayamaterai));
        $('#BKTandabuktibayarUangMukaT_biayaadministrasi').val(formatUang(data.biayaadministrasi));
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(data.namapasien);
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(nama_pembayar);
        $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(data.alamatpasien);
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').focus();
        getDataRekeningUangMuka($("#isian_pembayaran").serialize()); //load data jurnal rekening
    }, 'json');
}

function refreshDialogPendaftaran(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    var instalasiNama = $("#BKPendaftaranT_instalasi_id option:selected").text();
     if(instalasiId == <?php echo Params::INSTALASI_ID_LAB; ?>){
      $.fn.yiiGridView.update('pendaftaran-t-lab-grid', {
          data: {
              "BKPasienM[idInstalasi]":instalasiId,
              "BKPasienM[instalasi_nama]":instalasiNama,
          }
      });
       $("#pasien").hide();
       $("#PasienLab").show();
        // $("#pasien").find('input').each(function(){
        //     $(this).val("");
        // });
        
    }else if(instalasiId != <?php echo Params::INSTALASI_ID_LAB; ?>){
        $.fn.yiiGridView.update('pendaftaran-t-grid', {
            data: {
                "BKPasienM[idInstalasi]":instalasiId,
                "BKPasienM[instalasi_nama]":instalasiNama,
            }
        });
        $("#pasien").show();
        $("#PasienLab").hide();
        
        // $("#pasienLab").find('input').each(function(){
        //     $(this).val("");
        // });
        
    }   
}
    
function cekInstalasi(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    if(instalasiId.length > 0){
        return true;
    }else{
        alert("Silahkan pilih instalasi ! ");
        $("#BKPendaftaranT_instalasi_id").focus();
        return false;
    }
}

$('#PasienLab').hide();
</script>