<?php
$this->breadcrumbs=array( 
	'Pembayaran',
);?>

<?php
$this->widget('application.extensions.moneymask.MMask',
    array(
        'element'=>'.currency',
        'currency'=>'PHP',
        'config'=>array(
            'symbol'=>'Rp ',
            'defaultZero'=>true,
            'allowZero'=>true,
            'precision'=>0,
        )
    )
);
?>

<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php
    $form = $this->beginWidget('ext.bootstrap.widgets.BootActiveForm',
        array(
            'id'=>'pembayaran-form',
            'enableAjaxValidation'=>false,
            'type'=>'horizontal',
            'focus'=>'#BKTandabuktibayarUangMukaT_jmlpembayaran',
//            'focus'=>'#BKPendaftaranT_instalasi_id',
            'htmlOptions'=>array(
                'onKeyPress'=>'return disableKeyPress(event)',
            ),
        )
    );
?>
<legend class="rim2">Transaksi Bayar Uang Muka</legend>

<?php
    $this->renderPartial(
        '_ringkasDataPasien',
        array(
            'modPendaftaran'=>$modPendaftaran,
            'modPasien'=>$modPasien
        )
    );
?>

<?php
    $modTandaBukti->sebagaipembayaran_bkm = 'Uang Muka (Deposit)';
?>

<fieldset id="isian_pembayaran">
    <legend class="rim">Data Pembayaran</legend>
    <table>
        <tr>
            <td>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Tagihan','harusDibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('totTagihan',$totTagihan,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <div class="control-group ">
                    <?php echo CHtml::label('Total Pembebasan','totPembebasan', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::textField('totPembebasan',$totPembebasanTarif,array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?>
                    </div>
                </div>
                <?php echo CHtml::activeHiddenField($modTandaBukti,'jmlpembulatan',array('readonly'=>true,'class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modTandaBukti,'biayaadministrasi',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modTandaBukti,'biayamaterai',array('onkeyup'=>'hitungJmlBayar();','class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php
                    echo $form->textFieldRow($modTandaBukti,'jmlpembayaran',
                        array(
                            'onkeyup'=>'hitungKembalian();',
                            'readonly'=>false,
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);"
                        )
                    );
                ?>
                <?php 
                    echo $form->textFieldRow($modTandaBukti,'uangditerima',
                        array(
                            'onkeyup'=>'hitungKembalian();',
                            'class'=>'inputFormTabel currency span3 req', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);",
                        )
                    ); 
                ?>
                <?php 
                    echo $form->textFieldRow($modTandaBukti,'uangkembalian',
                        array(
                            'class'=>'inputFormTabel currency span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);",
                        )
                    ); 
                ?>
            </td>
            <td>
                <div class="control-group ">
                    <?php $modTandaBukti->tglbuktibayar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modTandaBukti->tglbuktibayar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modTandaBukti,'tglbuktibayar', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                                'model'=>$modTandaBukti,
                                                'attribute'=>'tglbuktibayar',
                                                'mode'=>'datetime',
                                                'options'=> array(
                                                    'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                    'maxDate' => 'd',
                                                ),
                                                'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                                ),
                        )); ?>

                    </div>
                </div>
                <?php //echo $form->textFieldRow($modTandaBukti,'tglbuktibayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>

                <?php 
                    echo $form->dropDownListRow($modTandaBukti,'carapembayaran',  CaraPembayaran::items(),
                        array(
                            'onchange'=>'ubahCaraPembayaran(this)',
                            'disabled'=>false,
                            'class'=>'span3', 
                            'onkeypress'=>"return $(this).focusNextInputField(event);", 
                            'maxlength'=>50
                        )
                    ); 
                ?>
                <div class="control-group ">
                    <?php echo CHtml::label('Menggunakan Kartu','pakeKartu', array('class'=>'control-label inline')) ?>
                    <div class="controls">
                        <?php echo CHtml::checkBox('pakeKartu',false,array('onchange'=>"enableInputKartu();", 'onkeypress'=>"return $(this).focusNextInputField(event);")) ?> <i class="icon-chevron-down"></i>
                    </div>
                </div>
                <div id="divDenganKartu" class="hide">
                    <?php 
                        echo $form->dropDownListRow($modTandaBukti,'dengankartu',  DenganKartu::items(),array('onchange'=>'enableInputKartu()','empty'=>'-- Pilih --','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); 
                    ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'bankkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nokartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modTandaBukti,'nostrukkartu',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>

                <?php echo $form->textFieldRow($modTandaBukti,'darinama_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textAreaRow($modTandaBukti,'alamat_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modTandaBukti,'sebagaipembayaran_bkm',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
        </tr>
    </table>
    
</fieldset>
<?php
//FORM REKENING
$this->renderPartial('billingKasir.views.pembayaranUangMuka.rekening._formRekening',
    array(
        'form'=>$form,
        'modRekenings'=>$modRekenings,
    )
);
?>

    <?php //echo $form->textFieldRow($modTandaBukti,'closingkasir_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'shift_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'bayaruangmuka_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'pembayaranpelayanan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nourutkasir',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'nobuktibayar',array('class'=>'inputFormTabel currency span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
    <?php //echo $form->textFieldRow($modTandaBukti,'keterangan_pembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            
    <div class="form-actions">
            <?php
                if (empty($modBayaruangmuka->bayaruangmuka_id))
                {
                    echo CHtml::htmlButton(
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array(
                            'class'=>'btn btn-primary', 
                            'type'=>'submit', 
                            'onKeypress'=>'return formSubmit(this,event)',
                            'onClick'=>'onClickSubmit();return false;',
                            
                        )
                    ); 
                    echo "&nbsp;&nbsp;";
                }else{
                    echo CHtml::htmlButton(
                        Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array(
                            'class'=>'btn btn-primary', 
                            'type'=>'submit',
                            'disabled'=>true
                        )
                    ); 
                    echo "&nbsp;&nbsp;";
                    echo CHtml::link(
                        Yii::t('mds', '{icon} Print Kwitansi', array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                        '#', 
                        array(
                            'class'=>'btn btn-info',
                            'onclick'=>"printKasir($modTandaBukti->tandabuktibayar_id);return false",
                            'disabled'=>false
                        )
                    ); 
                    echo CHtml::link(
                        Yii::t('mds', '{icon} Print BKM', array('{icon}'=>'<i class="icon-print icon-white"></i>')),
                        '#', 
                        array(
                            'class'=>'btn btn-info',
                            'onclick'=>"printBkm($modBayaruangmuka->bayaruangmuka_id, 'PRINT');return false",
                            'disabled'=>false
                        )
                    ); 
                }
            ?>
            <?php 
                echo CHtml::link(
                    Yii::t('mds','{icon} Reset', array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    Yii::app()->createAbsoluteUrl(
                        Yii::app()->controller->module->id.'/'.Yii::app()->controller->id . '/index',
                        array(
                            'modulId'=>Yii::app()->session['modulId']
                        )
                    ),
                    array(
                        'class'=>'btn btn-danger', 
                        'type'=>'reset'
                    )
                ); 
            ?>
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<div id="testForm"></div>

<script type="text/javascript">
$('.currency').each(function(){this.value = formatUang(this.value)});

function simpanProses()
{
    $("#pembayaran-form").submit();
}

function onClickSubmit()
{
    if(cekInput())
    {
        confirmPembayaran();
    }
}

function formSubmit(obj,evt)
{
     evt = (evt) ? evt : event;
     var form_id = $(obj).closest('form').attr('id');
     var charCode = (evt.charCode) ? evt.charCode : ((evt.which) ? evt.which : evt.keyCode);
     
     if(charCode == 13)
     {
         if(cekInput())
         {
             confirmPembayaran();
         }
     }
     return false;
}

function confirmPembayaran()
{
    $('#dlgConfirmasi').dialog('open');
    
    var no_pendaftaran = $('#pembayaran-form').find('input[name$="[no_pendaftaran]"]').val();
    var no_rekam_medik = $('#pembayaran-form').find('input[name$="[no_rekam_medik]"]').val();
    var instalasi_nama = $('#pembayaran-form').find('input[name$="[instalasi_nama]"]').val();
    var nama_pasien = $('#pembayaran-form').find('input[name$="[nama_pasien]"]').val();
    var ruangan_nama = $('#pembayaran-form').find('input[name$="[ruangan_nama]"]').val();
    
    var data_pasien = {
        'no_pendaftaran':no_pendaftaran,
        'no_rekam_medik':no_rekam_medik,
        'instalasi_nama':instalasi_nama,
        'nama_pasien':nama_pasien,
        'ruangan_nama':ruangan_nama,
    }
    
    var data_pembayaran = {
        'darinama_bkm':$('#pembayaran-form').find('input[name$="[darinama_bkm]"]').val(),
        'sebagaipembayaran_bkm':$('#pembayaran-form').find('input[name$="[sebagaipembayaran_bkm]"]').val(),
        'totTagihan':formatUang($('#pembayaran-form').find('input[id="totTagihan"]').val()),
        'jmlpembayaran':formatUang($('#pembayaran-form').find('input[name$="[jmlpembayaran]"]').val()),
        'uangditerima':formatUang($('#pembayaran-form').find('input[name$="[uangditerima]"]').val())
    }    
    
    $('#content_confirm').find('#info_pasien_temp td').each(
        function()
        {
            for(x in data_pasien)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pasien[x]).css('font-weight','bold');
                }
            }
        }
    );
    
    $('#content_confirm').find('#info_pembayaran_temp td').each(
        function()
        {
            for(x in data_pembayaran)
            {
                if($(this).attr('tag') == x)
                {
                    $(this).text(data_pembayaran[x]).css('font-weight','bold');
                }
            }
        }
    );
}

function hitungKembalian()
{
    var jmlBayar = unformatNumber($('#BKTandabuktibayarUangMukaT_jmlpembayaran').val());
    $('#BKTandabuktibayarUangMukaT_uangditerima').val(formatUang(jmlBayar));
    var uangDiterima = unformatNumber($('#BKTandabuktibayarUangMukaT_uangditerima').val());

    var index = 0;
    $("#tblInputRekening > tbody").find('tr').each(
        function()
        {
            var saldonormal = $(this).find('input[name$="[saldonormal]"]').val();
            if(saldonormal=='D')
                $(this).find('input[name$="[saldodebit]"]').val(formatUang(jmlBayar));
            else
                $(this).find('input[name$="[saldokredit]"]').val(formatUang(jmlBayar));
            
            index++;
        }
    );
//    alert(uangDiterima);
    var uangKembalian = uangDiterima - jmlBayar;
//    var totaluangkembalian = formatNumber(uangKembalian);
    
    $('#BKTandabuktibayarUangMukaT_uangkembalian').val(formatUang(uangKembalian));
}

function enableInputKartu()
{
    if($('#pakeKartu').is(':checked'))
        $('#divDenganKartu').show();
    else 
        $('#divDenganKartu').hide();
    if($('#BKTandabuktibayarUangMukaT_dengankartu').val() != ''){
        //alert('isi');
        $('#BKTandabuktibayarUangMukaT_bankkartu').removeAttr('readonly');
        $('#BKTandabuktibayarUangMukaT_nokartu').removeAttr('readonly');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').removeAttr('readonly');
    } else {
        //alert('kosong');
        $('#BKTandabuktibayarUangMukaT_bankkartu').attr('readonly','readonly');
        $('#BKTandabuktibayarUangMukaT_nokartu').attr('readonly','readonly');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').attr('readonly','readonly');
        
        $('#BKTandabuktibayarUangMukaT_bankkartu').val('');
        $('#BKTandabuktibayarUangMukaT_nokartu').val('');
        $('#BKTandabuktibayarUangMukaT_nostrukkartu').val('');
    }
}

function hitungJmlBayar()
{
    var biayaAdministrasi = unformatNumber($('#BKTandabuktibayarUangMukaT_biayaadministrasi').val());
    var biayaMaterai = unformatNumber($('#BKTandabuktibayarUangMukaT_biayamaterai').val());
    var deposit = unformatNumber($('#deposit').val());
    var totPembebasan = unformatNumber($('#totPembebasan').val());
    var totDiscountTind = unformatNumber($('#totaldiscount_tindakan').val());
    var totBayar = 0;
    var totTagihan = unformatNumber($('#totTagihan').val());
    var jmlPembulatan = unformatNumber($('#BKTandabuktibayarUangMukaT_jmlpembulatan').val());
    
    totBayar = totTagihan + jmlPembulatan + biayaAdministrasi + biayaMaterai - totDiscountTind - totPembebasan - deposit;
    
    $('#BKTandabuktibayarUangMukaT_jmlpembayaran').val(formatUang(totBayar));
    hitungKembalian();
}

function ubahCaraPembayaran(obj)
{
    if(obj.value == 'CICILAN'){
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').removeAttr('readonly');
    } else {
        $('#BKTandabuktibayarUangMukaT_jmlpembayaran').attr('readonly', true);
        hitungJmlBayar();
    }
    
    if(obj.value == 'TUNAI'){
        hitungJmlBayar();
    } 
}

function printKasir(idTandaBukti)
{
    if(idTandaBukti!=''){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('caraPrint'=>'PDF','idTandaBukti'=>'')); ?>'+idTandaBukti,'printwin','left=100,top=100,width=640,height=400,scrollbars=1');
    }     
}
function printBkm(idBayaruangmuka, caraPrint)
{
    if(idBayaruangmuka!=''){ //JIka di Konfig Systen diset TRUE untuk Print kunjungan
             window.open('<?php echo Yii::app()->createUrl('billingKasir/pembayaranUangMuka/detailKasMasuk'); ?>&idBayaruangmuka='+idBayaruangmuka+'&caraPrint='+caraPrint,'printwin','left=100,top=100,width=640,height=400,scrollbars=1');
    }     
}

function cekInput()
{


    if($('#BKTandabuktibayarUangMukaT_sebagaipembayaran_bkm').val() == ''){
        alert('"Sebagai Pembayaran" tidak boleh kosong!');
        $('#BKTandabuktibayarUangMukaT_sebagaipembayaran_bkm').addClass('error');
        return false;
    } 

    if($('#BKPendaftaranT_pendaftaran_id').val() == ''){
        alert('Belum input Pasien');
        $('#BKPasienM_no_rekam_medik').focus();
        return false;
    }
    
    if($('#pembayaran-form').find('input[name$="[jmlpembayaran]"]').val() == 0 || $('#pembayaran-form').find('input[name$="[jmlpembayaran]"]').val().length == 0)
    {
        alert('Jumlah Pembayaran harus di isi');
        $('#pembayaran-form').find('input[name$="[jmlpembayaran]"]').focus()
        return false;
    }
    
     if($('#BKTandabuktibayarUangMukaT_alamat_bkm').val() == ''){
        alert('"Alamat" tidak boleh kosong!');
        $('#BKTandabuktibayarUangMukaT_alamat_bkm').addClass('error');
        return false;
    } 
    
    var index = 0;
    $("#isian_pembayaran").find(".req").each(
        function()
        {
            if($(this).val() == 0 || $(this).val() < 0)
            {
                index++;
            }
        }
    );
        
    if(index > 0)
    {
        alert('Isi inputan yang bertanda bintang !!');
        return false;
    }
    
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    return true;
}
</script>

<?php
//if($successSave){
//Yii::app()->clientScript->registerScript('tutupDialog',"
//window.parent.setTimeout(\"$('#dialogBayarKarcis').dialog('close')\",1500);
//window.parent.$.fn.yiiGridView.update('pencarianpasien-grid', {
//		data: $('#caripasien-form').serialize()
//});
//",  CClientScript::POS_READY);
//}
?>



<?php
    $this->beginWidget('zii.widgets.jui.CJuiDialog',
        array(
            'id'=>'dlgConfirmasi',
            'options'=>array(
                'title'=>'Konfirmasi Pembayaran',
                'autoOpen'=>false,
                'modal'=>true,
                'width'=>900,
                'height'=>400,
                'resizable'=>false,
            ),
        )
    );
?>
<div id="detail_confirmasi">
    <div id="content_confirm">
        <fieldset>
            <legend class="rim">Info Pasien</legend>
            <table id="info_pasien_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">No. Pendaftaran</td>
                    <td width="280" tag="no_pendaftaran"></td>
                    <td width="150">Nama</td>
                    <td tag="nama_pasien"></td>
                </tr>
                <tr>
                    <td>Instalasi</td>
                    <td tag="instalasi_nama"></td>
                    <td>No. Rekam Medis</td>
                    <td tag="no_rekam_medik"></td>
                </tr>
                <tr>
                    <td>Ruangan</td>
                    <td tag="ruangan_nama"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
        <fieldset>
            <legend class="rim">Detail Pembayaran</legend>
            <table id="info_pembayaran_temp" class="table table-bordered table-condensed">
                <tr>
                    <td width="150">Dari Nama</td>
                    <td width="280" tag="darinama_bkm"></td>
                    <td width="150">Pembayaran</td>
                    <td tag="sebagaipembayaran_bkm"></td>
                </tr>
                <tr>
                    <td>Total Tagihan</td>
                    <td tag="totTagihan"></td>
                    <td>Uang Muka</td>
                    <td tag="jmlpembayaran"></td>
                </tr>
                <tr>
                    <td>Uang Diterima</td>
                    <td tag="uangditerima"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div class="form-actions">
            <?php
                echo CHtml::link(
                    'Teruskan',
                    '#',
                    array(
                        'class'=>'btn btn-primary',
                        'onClick'=>'simpanProses();return false;'
                        
                    )
                );
            ?>
            <?php
                echo CHtml::link(
                    'Kembali', 
                    '#',
                    array(
                        'class'=>'btn btn-danger',
                        'onClick'=>'$("#dlgConfirmasi").dialog("close");$("#BKTandabuktibayarUangMukaT_jmlpembayaran").focus();return false;'
                    )
                );
            ?>
    </div>
</div>
<?php
    $this->endWidget();
?>
