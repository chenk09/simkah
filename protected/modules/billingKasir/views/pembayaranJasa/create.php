<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php
$this->breadcrumbs=array(
	'Pembayaran Jasa',
);
?>
<legend class="rim2">Transaksi Pembayaran Jasa Dokter</legend>
<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'bkpembayaranjasa-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatNumbers();'),
        'focus'=>'#',
)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $this->renderPartial('_formCari', array('form'=>$form, 'model'=>$model)); ?>
<?php echo isset($_GET['id']) ? '' : '<div id="tabelDetail" class="grid-view">'; ?>
<?php echo $this->renderPartial('_formDetail', array('form'=>$form, 'model'=>$model, 'modDetails'=>$modDetails, 'dataDetails'=>$dataDetails)); ?>
<?php echo isset($_GET['id']) ? '' : '</div>'; ?>
<?php echo $this->renderPartial('_form', array('form'=>$form, 'model'=>$model)); ?>

<div class="form-actions">
        <?php
        if(isset($_GET['id'])){
            $disabledSimpan = 'disabled';
            $disabledPrint = '';
        }else{
            $disabledSimpan = '';
            $disabledPrint = 'disabled';
        }
        ?>
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'disabled'=>$disabledSimpan,'onclick'=>'simpanProses(this);')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/pembayaranJasa/create'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
        <?php 
            echo CHtml::link(
                Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 
                "#", 
                array(
                    'class'=>'btn btn-info',
                    'onclick'=>"print('PRINT'); return false",
                    'disabled'=>$disabledPrint,
                )
            ); 
        ?>
        <?php  
            $content = $this->renderPartial('billingKasir.views.tips.transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
</div>
<?php $this->endWidget(); ?>
<script>
function pilihDokter(){
    var pilih = $("#BKPembayaranjasaT_pilihDokter");
    if(pilih.val() == "rujukan"){
        $("#formRujukan").show();
        $("#formTglPenunjang").show();
        $("#formDokter").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPendaftaran").find('input').each(function(){
            $(this).val("");
        });
        $("#formDokter").hide();
        $("#formTglPendaftaran").hide();
    }else{
        $("#formDokter").show();
        $("#formTglPendaftaran").show();
        $("#formRujukan").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPenunjang").find('input').each(function(){
            $(this).val("");
        });
        $("#formRujukan").hide();
        $("#formTglPenunjang").hide();
    }
    bersihTabelDetail();
    bersihFormPembayaran();
}
pilihDokter(); //default
function addDetail(){
    var rujukandari_id = $('#BKPembayaranjasaT_rujukandari_id').val();
    var pegawai_id = $('#BKPembayaranjasaT_pegawai_id').val();
    var tglAwalPenunjang = $('#BKPembayaranjasaT_tglAwalPenunjang').val();
    var tglAkhirPenunjang = $('#BKPembayaranjasaT_tglAkhirPenunjang').val();
    var tglAwalPendaftaran = $('#BKPembayaranjasaT_tglAwalPendaftaran').val();
    var tglAkhirPendaftaran = $('#BKPembayaranjasaT_tglAkhirPendaftaran').val();
    var komponentarifIds = {};
    var i = 0;
    $('#checkBoxList').find('input').each(function(){
        if($(this).is(':checked')){
            komponentarifIds[i] = $(this).val();
            i ++;
        }
    });
    if(tglAwalPenunjang.length > 0 && tglAkhirPenunjang.length > 0 && i > 0){
        var tglAwal = tglAwalPenunjang;
        var tglAkhir = tglAkhirPenunjang;
    }else if(tglAwalPendaftaran.length > 0 && tglAkhirPendaftaran.length > 0){
        var tglAwal = tglAwalPendaftaran;
        var tglAkhir = tglAkhirPendaftaran;
    }else{
        alert ("Silahkan isi form dengan benar ! Rujukan / Dokter, Tanggal dan Komponen Tarif wajib diisi !");
        return false;
    }
    bersihTabelDetail();
    bersihFormPembayaran();
    $('#tabelDetail').addClass('srbacLoading');
    $.post("<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasa'); ?>", {
        rujukandari_id:rujukandari_id, 
        pegawai_id: pegawai_id, 
        tgl_awal:tglAwal, 
        tgl_akhir:tglAkhir, 
        komponentarifId:komponentarifIds},
        function(data){
            if (data.tr == ""){
                alert('Data tidak ditemukan !');
                $('#tabelDetail').removeClass('srbacLoading');
                return false;
            }else{
                $('#tabelDetail tbody').append(data.tr);
                $("#tabelDetail tbody tr .currency").each(function(){
                    $(this).maskMoney({"defaultZero":true,"allowZero":true,"decimal":"","thousands":",","precision":0,"symbol":null});
                });    
                formatNumbers();
                hitungSemua();
                $('#tabelDetail').removeClass('srbacLoading');
            }
        }, "json");
    return false;
}
function formatNumbers(){
    $('.currency').each(function(){this.value = formatNumber(this.value)});
}
formatNumbers();
function unformatNumbers(){
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
}
function bersihTabelDetail(){
    $('#tabelDetail tbody').html("");
}
function bersihFormPembayaran(){
    $('#formPembayaran .currency').each(function(){
        $(this).val(0);
    });
}
function hitungSemua(){
    unformatNumbers();
    var totTarif = 0;
    var totJasa = 0;
    var totBayar = 0;
    var totSisa = 0;
    $('#tabelDetail tbody tr').each(function(){
        if($(this).find('input[name$="[pilihDetail]"]').is(":checked")){ //hitung yang dicheck aja
            var jmltarif = parseFloat($(this).find('input[name$="[jumahtarif]"]').val());
            var jmljasa = parseFloat($(this).find('input[name$="[jumlahjasa]"]').val());
            var jmlbayar = parseFloat($(this).find('input[name$="[jumlahbayar]"]').val());
            var jmlsisa = parseFloat(jmljasa - jmlbayar);
            $(this).find('input[name$="[sisajasa]"]').val(jmlsisa);
            totTarif += jmltarif;
            totJasa += jmljasa;
            totBayar += jmlbayar;
            totSisa += jmlsisa;
        }
    });
    $("#BKPembayaranjasaT_totaltarif").val(totTarif);
    $("#BKPembayaranjasaT_totaljasa").val(totJasa);
    $("#BKPembayaranjasaT_totalbayarjasa").val(totBayar);
    $("#BKPembayaranjasaT_totalsisajasa").val(totSisa);
    formatNumbers();
}
function checkAll(obj){
    if($(obj).is(':checked')){
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
        });
    }else{
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        });
    }
    hitungSemua();
}
function checkIni(obj){
    if($(obj).is(':checked')){
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
    }else{
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        $('#pilihSemua').removeAttr('checked');
    }
    hitungSemua();
}

function print(caraPrint) 
{
    <?php if (!empty($model->pembayaranjasa_id)){?>
        window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/Print', array('id'=>$model->pembayaranjasa_id)); ?>'+'&caraPrint=' + caraPrint,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    <?php } ?>
}

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#bkpembayaranjasa-t-form').submit();
}  
</script>