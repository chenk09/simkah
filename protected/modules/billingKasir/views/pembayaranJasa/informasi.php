<legend class="rim2">Informasi Pembayaran Jasa Dokter</legend>
<?php
$this->breadcrumbs=array(
	'Informasi Pembayaran Jasa Dokter',
);

//$arrMenu = array();
//                (Yii::app()->user->checkAccess(Params::DEFAULT_ADMIN)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Manage').' BKPembayaranjasaT ', 'header'=>true, 'itemOptions'=>array('class'=>'heading-master'))) :  '' ;
//                array_push($arrMenu,array('label'=>Yii::t('mds','List').' BKPembayaranjasaT', 'icon'=>'list', 'url'=>array('index'))) ;
//                (Yii::app()->user->checkAccess(Params::DEFAULT_CREATE)) ?array_push($arrMenu,array('label'=>Yii::t('mds','Create').' BKPembayaranjasaT', 'icon'=>'file', 'url'=>array('create'))) :  '' ;
//                
//$this->menu=$arrMenu;

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('bkpembayaranjasa-t-grid', {
		data: $(this).serialize()
	});
	return false;
});
");

$this->widget('bootstrap.widgets.BootAlert'); ?>

<?php //echo CHtml::link(Yii::t('mds','{icon} Advanced Search',array('{icon}'=>'<i class="icon-search"></i>')),'#',array('class'=>'search-button btn')); ?>
<!-- search-form -->

<?php $this->widget('ext.bootstrap.widgets.BootGridView',array(
	'id'=>'bkpembayaranjasa-t-grid',
	'dataProvider'=>$model->searchInformasi(),
	//'filter'=>$model,
        'template'=>"{pager}{summary}\n{items}",
        'itemsCssClass'=>'table table-striped table-bordered table-condensed',
	'columns'=>array(
                'nobayarjasa',
		'tglbayarjasa',
                array(
                    'header'=>'Dokter RS / Perujuk',
                    'type'=>'raw',
                    'value'=>'empty($data->rujukandari_id) ? $data->pegawai->NamaLengkap : $data->rujukandari->namaperujuk',
                ),
                'periodejasa',
		'sampaidgn',
                array(
                    'name'=>'tandabuktikeluar_id',
                    'type'=>'raw',
                    'value'=>'empty($data->tandabuktikeluar_id) ? "<center>-</center>" : $data->tandabuktikeluar->nokaskeluar',
                ),
                array(
                    'name'=>'totaltarif',
                    'type'=>'raw',
                    'value'=>'"<div style=\"text-align:right\">Rp. ".MyFunction::formatNumber($data->totaltarif)."</div>"',
                ),
                array(
                    'name'=>'totaljasa',
                    'type'=>'raw',
                    'value'=>'"<div style=\"text-align:right\">Rp. ".MyFunction::formatNumber($data->totaljasa)."</div>"',
                ),
                array(
                    'name'=>'totalbayarjasa',
                    'type'=>'raw',
                    'value'=>'"<div style=\"text-align:right\">Rp. ".MyFunction::formatNumber($data->totalbayarjasa)."</div>"',
                ),
                array(
                    'name'=>'totalsisajasa',
                    'type'=>'raw',
                    'value'=>'"<div style=\"text-align:right\">Rp. ".MyFunction::formatNumber($data->totalsisajasa)."</div>"',
                ),
                array(
                    'header'=>'Detail',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("pembayaranJasa/lihatDetail",array("id"=>$data->pembayaranjasa_id)),
                                    array("class"=>"", 
                                          "target"=>"iframeDetail",
                                          "onclick"=>"$(\"#dialogDetail\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Transaksi",
                                    ))',          
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
                array(
                    'header'=>'Rekap',
                    'type'=>'raw',
                    'value'=>'CHtml::Link("<i class=\"icon-list-alt\"></i>",Yii::app()->controller->createUrl("pembayaranJasa/lihatDetailRumahsakit",array("id"=>$data->pembayaranjasa_id)),
                                    array("class"=>"", 
                                          "target"=>"iframeDetail",
                                          "onclick"=>"$(\"#dialogDetail\").dialog(\"open\");",
                                          "rel"=>"tooltip",
                                          "title"=>"Klik untuk melihat Rincian Transaksi",
                                    ))',          
                    'htmlOptions'=>array('style'=>'text-align: center; width:40px')
                ),
	),
        'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
)); ?>
<legend class="rim">Pencarian</legend>
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
<?php 
 
//echo CHtml::htmlButton(Yii::t('mds','{icon} PDF',array('{icon}'=>'<i class="icon-book icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PDF\')'))."&nbsp&nbsp"; 
//echo CHtml::htmlButton(Yii::t('mds','{icon} Excel',array('{icon}'=>'<i class="icon-pdf icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'EXCEL\')'))."&nbsp&nbsp"; 
//echo CHtml::htmlButton(Yii::t('mds','{icon} Print',array('{icon}'=>'<i class="icon-print icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'button','onclick'=>'print(\'PRINT\')'))."&nbsp&nbsp"; 
//$this->widget('TipsMasterData',array('type'=>'admin'));
//$controller = Yii::app()->controller->id; //mengambil Controller yang sedang dipakai
//$module = Yii::app()->controller->module->id; //mengambil Module yang sedang dipakai
//$urlPrint=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/print');

//$js = <<< JSCRIPT
//function print(caraPrint)
//{
//window.open("${urlPrint}/"+$('#bkpembayaranjasa-t-search').serialize()+"&caraPrint="+caraPrint,"",'location=_new, width=900px');
//}
//JSCRIPT;
//Yii::app()->clientScript->registerScript('print',$js,CClientScript::POS_HEAD);                        
?>

<?php 
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'dialogDetail',
    'options'=>array(
        'title'=>'Rincian Pembayaran Jasa Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'minWidth'=>840,
        'minHeight'=>400,
        'resizable'=>true,
    ),
));
?>
<iframe src="" name="iframeDetail" width="100%" height="550" >
</iframe>
<?php
$this->endWidget();
?>
