<?php if(isset($_GET['id'])){$disabled = 'disabled';} else {$readonly = false;} ?>
<legend class="rim">Daftar Jasa</legend>
<table class="table table-bordered table-condensed">
    <thead>
        <tr>
            <th>No.</th>
            <th>No Pendaftaran <br/>/ No. RM</th>
            <th>No. Penunjang</th>
            <th>Nama Pasien</th>
            <th>Alamat Pasien</th>
            <th>Cara Bayar<br/> / Penjamin</th>
            <th>Jumlah Tarif</th>
            <th>Jumlah Jasa</th>
            <th>Jumlah Bayar</th>
            <th>Sisa Jasa</th>
            <th>Pilih <?php echo CHtml::checkBox('pilihSemua', true, array('disabled'=>$disabled, 'onclick'=>'checkAll(this);'));?></th>
        </tr>
    </thead>
    <tbody>
        <?php //target untuk actionAjax/addDetailPembayaranJasa ?>
        <?php
       
        if(count($dataDetails)>0){
            foreach ($dataDetails as $i => $detail){
                $modDetails = new $modDetails;
                $modDetails->attributes = $detail;
                $modDetails->pilihDetail = true;
                $modDetails->penjaminId = $detail['penjaminId'];
                $tr .= "<tr>";
                $tr .= "<td>".($i+1).
                        CHtml::activeHiddenField($modDetails,'['.$i.']pendaftaran_id',array('value'=>$modDetails->pendaftaran_id)).
                        CHtml::activeHiddenField($modDetails,'['.$i.']pembayaranjasa_id',array('value'=>null)).
                        CHtml::activeHiddenField($modDetails,'['.$i.']pasien_id',array('value'=>$modDetails->pasien_id));
                        CHtml::activeHiddenField($modDetails,'['.$i.']penjaminId',array('value'=>$modDetails->pendaftaran->penjamin_id));
                if(!empty($rujukandariId)) {
                    $tr .= CHtml::activeHiddenField($modDetails,'['.$i.']pasienmasukpenunjang_id',array('value'=>$modDetails->pasienmasukpenunjang_id));
                }
                $tr .= "</td>";
                $tr .= "<td>".$modDetails->pasien->no_rekam_medik."<br>".$modDetails->pendaftaran->no_pendaftaran."</td>";
                if(!empty($modDetails->rujukandari_id)){
                    $tr .= "<td>".empty($modDetails->no_masukpenunjang) ? "-" : $modDetails->no_masukpenunjang."</td>";
                }else{
                    $tr .= "<td><center>-</center></td>";
                }
                $tr .= "<td>".$modDetails->pasien->nama_pasien."</td>";
                $tr .= "<td>".$modDetails->pasien->alamat_pasien."</td>";
                $tr .= "<td>".PenjaminpasienM::model()->findByPk($modDetails->penjaminId)->penjamin_nama."</td>";
                $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumahtarif', array('disabled'=>$disabled,'readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahjasa', array('disabled'=>$disabled,'readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']jumlahbayar', array('disabled'=>$disabled,'readonly'=>false, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'onkeyup'=>'hitungSemua();'))."</td>";
                $tr .= "<td>".CHtml::activeTextField($modDetails,'['.$i.']sisajasa', array('disabled'=>$disabled,'readonly'=>true, 'class'=>'inputFormTabel currency', 'onkeypress'=>"return $(this).focusNextInputField(event);"))."</td>";
                $tr .= "<td>".CHtml::activeCheckBox($modDetails,'['.$i.']pilihDetail', array('disabled'=>$disabled))."</td>";
                $tr .= "</tr>";
            }
            
            echo $tr;
        }
        ?>
    </tbody>
</table>
