<script>
function pilihDokter(){
    var pilih = $("#BKPembayaranjasaT_pilihDokter");
    if(pilih.val() == "rujukan"){
        $("#formRujukan").show();
        $("#formTglPenunjang").show();
        $("#formDokter").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPendaftaran").find('input').each(function(){
            $(this).val("");
        });
        $("#formDokter").hide();
        $("#formTglPendaftaran").hide();
    }else{
        $("#formDokter").show();
        $("#formTglPendaftaran").show();
        $("#formRujukan").find('input').each(function(){
            $(this).val("");
        });
        $("#formTglPenunjang").find('input').each(function(){
            $(this).val("");
        });
        $("#formRujukan").hide();
        $("#formTglPenunjang").hide();
    }
    bersihTabelDetail();
    bersihFormPembayaran();
}
//pilihDokter(); //default
function addDetail(){
    var rujukandari_id = $('#BKPembayaranjasaT_rujukandari_id').val();
    var pegawai_id = $('#BKPembayaranjasaT_pegawai_id').val();
    var tglAwalPenunjang = $('#BKPembayaranjasaT_tglAwalPenunjang').val();
    var tglAkhirPenunjang = $('#BKPembayaranjasaT_tglAkhirPenunjang').val();
//    var komponentarifIds = {};
//    var i = 0;
//    $('#checkBoxList').find('input').each(function(){
//        if($(this).is(':checked')){
//            komponentarifIds[i] = $(this).val();
//            i ++;
//        }
//    });
//    if(tglAwalPenunjang.length > 0 && tglAkhirPenunjang.length > 0 && i > 0){
    if(rujukandari_id > 0 && tglAwalPenunjang.length > 0 && tglAkhirPenunjang.length > 0){
        var tglAwal = tglAwalPenunjang;
        var tglAkhir = tglAkhirPenunjang;
    }else{
        alert ("Silahkan isi form dengan benar ! Tanggal & Rujukan Dari wajib diisi !");
//        alert ("Silahkan isi form dengan benar ! Rujukan / Dokter, Tanggal dan Komponen Tarif wajib diisi !");
        return false;
    }
    bersihTabelDetail();
    bersihFormPembayaran();
    $('#tabelDetail').addClass('srbacLoading');
    $.post("<?php echo Yii::app()->createUrl('billingKasir/actionAjax/addDetailPembayaranJasaDokterLuar'); ?>", {
        rujukandari_id:rujukandari_id, 
//        pegawai_id: pegawai_id, 
        tgl_awal:tglAwal, 
        tgl_akhir:tglAkhir},
//        komponentarifId:komponentarifIds},
        function(data){
            if (data.tr == ""){
                alert('Data tidak ditemukan !');
                $('#tabelDetail').removeClass('srbacLoading');
                return false;
            }else{
                $('#tabelDetail tbody').append(data.tr);
                $("#tabelDetail tbody tr .currency").each(function(){
                    $(this).maskMoney({"defaultZero":true,"allowZero":true,"decimal":"","thousands":",","precision":0,"symbol":null});
                });    
                formatNumbers();
                hitungSemua();
                $('#tabelDetail').removeClass('srbacLoading');
            }
        }, "json");
    return false;
}
function formatNumbers(){
    $('.currency').each(function(){this.value = formatNumber(this.value)});
}
formatNumbers();
function unformatNumbers(){
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
}
function bersihTabelDetail(){
    $('#tabelDetail tbody').html("");
}
function bersihFormPembayaran(){
    $('#formPembayaran .currency').each(function(){
        $(this).val(0);
    });
}
function hitungSemua(){
    unformatNumbers();
    var totTarif = 0;
    var totJasa = 0;
    var totBayar = 0;
    var totSisa = 0;
    $('#tabelDetail tbody tr').each(function(){
        if($(this).find('input[name$="[pilihDetail]"]').is(":checked")){ //hitung yang dicheck aja
            var jmltarif = parseFloat($(this).find('input[name$="[jumahtarif]"]').val());
            var jmljasa = parseFloat($(this).find('input[name$="[jumlahjasa]"]').val());
            var jmlbayar = parseFloat($(this).find('input[name$="[jumlahbayar]"]').val());
            var jmlsisa = parseFloat(jmljasa - jmlbayar);
            $(this).find('input[name$="[sisajasa]"]').val(jmlsisa);
            totTarif += jmltarif;
            totJasa += jmljasa;
            totBayar += jmlbayar;
            totSisa += jmlsisa;
        }
    });
    $("#BKPembayaranjasaT_totaltarif").val(totTarif);
    $("#BKPembayaranjasaT_totaljasa").val(totJasa);
    $("#BKPembayaranjasaT_totalbayarjasa").val(totBayar);
    $("#BKPembayaranjasaT_totalsisajasa").val(totSisa);
    formatNumbers();
}
function checkAll(obj){
    if($(obj).is(':checked')){
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
        });
    }else{
        $('#tabelDetail tbody tr').each(function(){
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
            $(this).find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        });
    }
    hitungSemua();
}
function checkIni(obj){
    if($(obj).is(':checked')){
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).attr('checked',true)});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val(1)});
    }else{
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).removeAttr('checked')});
        $(obj).parent().find('input[name$="[pilihDetail]"]').each(function(){$(this).val("")});
        $('#pilihSemua').removeAttr('checked');
    }
    hitungSemua();
}

function print(caraPrint) 
{
    <?php if (!empty($model->pembayaranjasa_id)){?>
        window.open('<?php echo Yii::app()->createUrl($this->module->id.'/'.$this->id.'/Print', array('id'=>$model->pembayaranjasa_id)); ?>'+'&caraPrint=' + caraPrint,'printwin','left=100,top=100,width=980,height=400,scrollbars=1');
    <?php } ?>
}

function checkAllKomponen(){
    if($('#pilihSemua').is(':checked')){
        $('#checkBoxList').each(function(){
            $(this).find('input').attr('checked',true);
        });
    }else{
        $('#checkBoxList').each(function(){
            $(this).find('input').removeAttr('checked');
        });
    }
}
checkAllKomponen();

function simpanProses(obj)
{
    $(obj).attr('disabled',true);
    $(obj).removeAttr('onclick');
    $('#bkpembayaranjasa-t-form').submit();
}  
</script>
