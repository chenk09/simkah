<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'config'=>array(
        'defaultZero'=>true,
        'allowZero'=>true,
        'precision'=>0,
    )
));
?>
<?php
$this->breadcrumbs=array(
	'Pembayaran Jasa',
);
?>
<legend class="rim2">Bayar Jasa Dokter Rujukan Dari Luar Rumah Sakit</legend>
<p class="help-block"><?php echo Yii::t('mds','Fields with <span class="required">*</span> are required.') ?></p>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'bkpembayaranjasa-t-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'htmlOptions'=>array('onKeyPress'=>'return disableKeyPress(event)', 'onsubmit'=>'unformatNumbers();'),
        'focus'=>'#',
)); ?>
<?php echo $form->errorSummary($model); ?>
<?php echo $this->renderPartial('_formCari', array('form'=>$form, 'model'=>$model)); ?>
<?php echo isset($_GET['id']) ? '' : '<div id="tabelDetail" class="grid-view">'; ?>
<?php echo $this->renderPartial('_formDetail', array('form'=>$form, 'model'=>$model, 'modDetails'=>$modDetails, 'dataDetails'=>$dataDetails)); ?>
<?php echo isset($_GET['id']) ? '' : '</div>'; ?>
<?php echo $this->renderPartial('_form', array('form'=>$form, 'model'=>$model)); ?>

<div class="form-actions">
        <?php
        if(isset($_GET['id'])){
            $disabledSimpan = 'disabled';
            $disabledPrint = '';
        }else{
            $disabledSimpan = '';
            $disabledPrint = 'disabled';
        }
        ?>
        <?php echo CHtml::htmlButton($model->isNewRecord ? Yii::t('mds','{icon} Create',array('{icon}'=>'<i class="icon-ok icon-white"></i>')) : 
                                             Yii::t('mds','{icon} Save',array('{icon}'=>'<i class="icon-ok icon-white"></i>')),
                        array('class'=>'btn btn-primary', 'type'=>'submit', 'onKeypress'=>'return formSubmit(this,event)', 'disabled'=>$disabledSimpan,'onclick'=>'simpanProses(this)')); ?>
        <?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/pembayaranJasaDokterLuar/create'), 
                array('class'=>'btn btn-danger',
                      'onclick'=>'if(!confirm("'.Yii::t('mds','Do You want to cancel?').'")) return false;')); ?>
        <?php 
            echo CHtml::link(
                Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), 
                "#", 
                array(
                    'class'=>'btn btn-info',
                    'onclick'=>"print('PRINT'); return false",
                    'disabled'=>$disabledPrint,
                )
            ); 
        ?>
        <?php  
            $content = $this->renderPartial('billingKasir.views.tips.transaksi',array(),true);
            $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
        ?>
</div>
<?php $this->endWidget(); ?>
<?php $this->renderPartial('_jsFunctions',array('model'=>$model));?>