<style>
    #checkBoxList{
        width:1024px;
    }
    #checkBoxList label.checkbox{
        width: 150px;
        display:inline-block;
    }

</style>
<legend class="rim">Pencarian</legend>
<fieldset id="formCari">
<table>
    <tr>
        <td width='15%'><?php //echo $form->dropDownList($model,'pilihDokter', array('rujukan'=>'Jasa Dokter Luar', 'rs'=>'Jasa Dokter RS'), array('onchange'=>'pilihDokter();', 'class'=>'span3')); ?>
            <div class="control-group" id="formRujukan">
                <?php echo $form->labelEx($model,'rujukandari_id', array('class'=>'control-label')); ?>
                <div class="controls">
                    <?php echo $form->hiddenField($model,'rujukandari_id',array('readonly'=>true, 'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                    <?php $this->widget('MyJuiAutoComplete', array(
                               'model'=>$model,
                               'attribute'=>'rujukandariNama',
                               'source'=>'js: function(request, response) {
                                              $.ajax({
                                                  url: "'.Yii::app()->createUrl('ActionAutoComplete/RujukanDari').'",
                                                  dataType: "json",
                                                  data: {
                                                      term: request.term,
                                                  },
                                                  success: function (data) {
                                                          response(data);
                                                  }
                                              })
                                           }',
                                'options'=>array(
                                      'showAnim'=>'fold',
                                      'minLength' => 2,
                                      'focus'=> 'js:function( event, ui ) {
                                           $(this).val("");
                                           return false;
                                       }',
                                      'select'=>'js:function( event, ui ) {
                                           $(this).val(ui.item.value);
                                           $("#BKPembayaranjasaT_rujukandari_id").val(ui.item.rujukandari_id);
                                           return false;
                                       }',
                               ),
                               'tombolDialog'=>array('idDialog'=>'dialogPerujuk','idTombol'=>'tombolPerujukDialog'),
                               'htmlOptions'=>array('placeholder'=>'Ketik Nama Perujuk', 'onkeypress'=>"return $(this).focusNextInputField(event);"),
                           )); 
                    ?>
                </div>
            </div>
        </td>
    </tr>
</table>
<table width='100%'>
<tr id="formTglPenunjang">
    <td width="20%">
        <?php echo CHtml::label('Tanggal Pasien Masuk Penunjang','tglAwalPenunjang'); ?>
    </td>
    <td>
        <?php
            $this->widget('MyDateTimePicker', array(
                'model' => $model,
                'attribute' => 'tglAwalPenunjang',
                'mode' => 'date',
                'options' => array(
                    'dateFormat' => Params::DATE_TIME_FORMAT,
                    'maxDate' => 'd',
                ),
                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                ),
            ));
        ?>
    </td>
    <td width="20%">
        <?php echo CHtml::label('Sampai dengan ','tglAkhirPenunjang'); ?>
    </td>
    <td>
        <?php
            $this->widget('MyDateTimePicker', array(
                'model' => $model,
                'attribute' => 'tglAkhirPenunjang',
                'mode' => 'date',
                
                'options' => array(
                    'dateFormat' => Params::DATE_TIME_FORMAT,
                    'maxDate' => 'd',
                ),
                'htmlOptions' => array('readonly' => true, 'class' => 'dtPicker3', 'onkeypress' => "return $(this).focusNextInputField(event)"
                ),
            ));
        ?>
    </td>
</tr>
</table>
<div class="form-actions">
    <?php 
    if(!isset($_GET['id'])){
        echo CHtml::htmlButton(Yii::t('mds', '{icon} Search', array('{icon}' => '<i class="icon-search icon-white"></i>')), array('class' => 'btn btn-primary', 'type' => 'button', 'onclick'=>'addDetail();'));
    }
    ?>
</div>
</fieldset>
<?php 
//========= Dialog buat cari data dokter =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogDokter',
    'options'=>array(
        'title'=>'Pencarian Data Dokter',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $pegawai = new DokterpegawaiV('searchByDokter');
    if (isset($_GET['DokterpegawaiV'])){
        $pegawai->attributes = $_GET['DokterpegawaiV'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'dokter-t-grid',
            'dataProvider'=>$pegawai->searchByDokter(),
            'filter'=>$pegawai,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogDokter\").dialog(\"close\");
                                            $(\"#BKPembayaranjasaT_pegawai_id\").val(\"$data->pegawai_id\");
                                            $(\"#BKPembayaranjasaT_pegawaiNama\").val(\"$data->gelardepan"." "."$data->nama_pegawai".", "."$data->gelarbelakang_nama\");

                                        "))',
                    ),
                    'gelardepan',
                    array(
                        'name'=>'nama_pegawai',
                        'header'=>'Nama Dokter',
                    ),
                    'gelarbelakang_nama',
                    'jeniskelamin',
                    'agama',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>

<?php 
//========= Dialog buat cari data dokter =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPerujuk',
    'options'=>array(
        'title'=>'Pencarian Data Perujuk',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $perujuk = new RujukandariM('search');
    if (isset($_GET['RujukandariM'])){
        $perujuk->attributes = $_GET['RujukandariM'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'perujuk-t-grid',
            'dataProvider'=>$perujuk->search(),
            'filter'=>$perujuk,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPerujuk\").dialog(\"close\");
                                            $(\"#BKPembayaranjasaT_rujukandari_id\").val(\"$data->rujukandari_id\");
                                            $(\"#BKPembayaranjasaT_rujukandariNama\").val(\"$data->namaperujuk\");

                                        "))',
                    ),
                    'namaperujuk',
                    'spesialis',
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>
