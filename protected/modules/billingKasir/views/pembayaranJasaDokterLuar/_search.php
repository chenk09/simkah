<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'id'=>'bkpembayaranjasa-t-search',
        'type'=>'horizontal',
)); ?>
<table width="100%"><tr>
    <td width="50%">
        <div class="control-group">
            <label class="control-label">Tgl. Pembayaran</label>
            <div class="controls">
                <?php   
                    $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tglAwal',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
                                    ),
                    )); 
                ?>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Sampai dengan</label>
            <div class="controls">
                <?php   
                    $this->widget('MyDateTimePicker',array(
                                    'model'=>$model,
                                    'attribute'=>'tglAkhir',
                                    'mode'=>'date',
                                    'options'=> array(
                                        'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                    ),
                                    'htmlOptions'=>array('class'=>'dtPicker3', 'onkeypress'=>"return $(this).focusNextInputField(event)",'style'=>'width:140px;',
                                    ),
                    )); 
                ?>
            </div>
        </div>
	<?php echo $form->textFieldRow($model,'nobayarjasa',array('class'=>'span3','maxlength'=>10)); ?>


    </td>
    <td>
        <?php echo $form->textFieldRow($model,'namaPerujuk',array('class'=>'span3')); ?>
        
        <?php echo $form->textFieldRow($model,'namaDokter',array('class'=>'span3')); ?>
            
        <?php echo $form->textFieldRow($model,'noKasKeluar',array('class'=>'span3')); ?>
    </td>
</tr></table>
	<div class="form-actions">
            <?php echo CHtml::htmlButton(Yii::t('mds','{icon} Search',array('{icon}'=>'<i class="icon-search icon-white"></i>')),array('class'=>'btn btn-primary', 'type'=>'submit')); ?>
            <?php echo CHtml::link(Yii::t('mds','{icon} Ulang',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')), 
                Yii::app()->createUrl($this->module->id.'/pembayaranJasaDokterLuar/informasi'), 
                array('class'=>'btn btn-danger')); ?>
            <?php  
                $content = $this->renderPartial('../tips/informasi',array(),true);
                $this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
            ?>
	</div>

<?php $this->endWidget(); ?>
