<?php $this->widget('bootstrap.widgets.BootAlert'); ?>

<fieldset>
    <legend class="rim">Data Pasien</legend>
    <table class="table table-condensed">
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'tgl_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[tgl_pendaftaran]', $modPendaftaran->tgl_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td><div class=" control-label"><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'no_rek')); ?></div></td>
             <td>
                <?php
                if(!empty($modPendaftaran->instalasi_id)){
                    echo CHtml::activeHiddenField($modPendaftaran,'instalasi_id',array('readonly'=>true));
                    echo CHtml::textField('BKPendaftaranT[instalasi_nama]', $modPendaftaran->instalasi->instalasi_nama, array('readonly'=>true));
                }else{
                    echo CHtml::dropDownList('BKPendaftaranT[instalasi_id]', NULL, CHtml::listData($modPendaftaran->InstalasiUangMukaItems, 'instalasi_id', 'instalasi_nama'), array('empty'=>'-- Pilih --', 'onchange'=>'refreshDialogPendaftaran();', 'onkeypress'=>"return $(this).focusNextInputField(event)"));
                }
                ?>
            </td> 
            <td rowspan="5">
                <?php 
                    if(!empty($modPasien->photopasien)){
                        echo CHtml::image(Params::urlPhotoPasienDirectory().$modPasien->photopasien, 'photo pasien', array('width'=>120));
                    } else {
                        echo CHtml::image(Params::urlPhotoPasienDirectory().'no_photo.jpeg', 'photo pasien', array('width'=>120));
                    }
                ?> 
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'no_pendaftaran',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[no_pendaftaran]', $modPendaftaran->no_pendaftaran, array('readonly'=>true)); ?></td>
            
            <td>
                <?php //echo CHtml::activeLabel($modPasien, 'no_rekam_medik',array('class'=>'control-label')); ?>
                 <label class="control-label no_rek">No Rekam Medik</label>
            </td>
            <td>
                <?php //echo CHtml::textField('BKPasienM[no_rekam_medik]', $modPasien->no_rekam_medik, array('readonly'=>true)); ?>
                <?php 
                    $this->widget('MyJuiAutoComplete', array(
                                    'name'=>'BKPasienM[no_rekam_medik]',
                                    'value'=>$modPasien->no_rekam_medik,
                                    'source'=>'js: function(request, response) {
                                                   $.ajax({
                                                       url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienRetur').'",
                                                       dataType: "json",
                                                       data: {
                                                           term: request.term,
                                                       },
                                                       success: function (data) {
                                                               response(data);
                                                       }
                                                   })
                                                }',
                                     'options'=>array(
                                           'showAnim'=>'fold',
                                           'minLength' => 2,
                                           'focus'=> 'js:function( event, ui ) {
                                                $(this).val(ui.item.value);
                                                return false;
                                            }',
                                           'select'=>'js:function( event, ui ) {
                                                isiDataPasien(ui.item);
                                                isiTandaBuktiKeluar(ui.item);
//                                                $("#BKPendaftaranT_pendaftaran_id").val();
//                                                $("#BKPendaftaranT_pasienadmisi_id").val();
                                                //loadPembayaran(ui.item.pembayaranpelayanan_id);
                                                cekHakRetur();
                                                return false;
                                            }',
                                    ),
                                     'tombolDialog'=>array('idDialog'=>'dialogPasien','idTombol'=>'tombolPasienDialog'),
                                     'htmlOptions'=>array('onfocus'=>'return cekInstalasi();','class'=>'span2', 
                                                                    'placeholder'=>'Ketik No. Rekam Medik','onkeypress'=>"return $(this).focusNextInputField(event)"),
                                )); 
                ?>
            </td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'umur',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[umur]', $modPendaftaran->umur, array('readonly'=>true)); ?></td>
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_pasien',array('class'=>'control-label no_rek')); ?></td>
            <td><?php //echo CHtml::textField('BKPasienM[nama_pasien]', $modPasien->nama_pasien, array('readonly'=>true)); 
                    $this->widget('MyJuiAutoComplete', array(
                                           'name'=>'BKPasienM[nama_pasien]',
                                           'value'=>$modPasien->nama_pasien,
                                           'source'=>'js: function(request, response) {
                                                          $.ajax({
                                                              url: "'.Yii::app()->createUrl('billingKasir/ActionAutoComplete/daftarPasienberdasarkanNama').'",
                                                              dataType: "json",
                                                              data: {
                                                                  retur:true,
                                                                  term: request.term,
                                                              },
                                                              success: function (data) {
                                                                      response(data);
                                                              }
                                                          })
                                                       }',
                                            'options'=>array(
                                                  'showAnim'=>'fold',
                                                  'minLength' => 2,
                                                  'focus'=> 'js:function( event, ui ) {
                                                       $(this).val(ui.item.value);
                                                       return false;
                                                   }',
                                                  'select'=>'js:function( event, ui ) {
                                                       isiDataPasien(ui.item);
                                                       loadPembayaran(ui.item.pendaftaran_id);
                                                       return false;
                                                   }',
                                           ),
                                       )); 
            ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'jeniskasuspenyakit_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[jeniskasuspenyakit_nama]',  ((isset($modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama)) ? $modPendaftaran->kasuspenyakit->jeniskasuspenyakit_nama : null), array('readonly'=>true)); ?></td>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'jeniskelamin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[jeniskelamin]', $modPasien->jeniskelamin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'instalasi_id',array('class'=>'control-label')); ?></td>
            <td>
                <?php echo CHtml::textField('BKPendaftaranT[instalasi_nama]',  ((isset($modPendaftaran->instalasi->instalasi_nama)) ? $modPendaftaran->instalasi->instalasi_nama : null), array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pendaftaran_id]',$modPendaftaran->pendaftaran_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasien_id]',$modPendaftaran->pasien_id, array('readonly'=>true)); ?>
                <?php echo CHtml::hiddenField('BKPendaftaranT[pasienadmisi_id]',$modPendaftaran->pasienadmisi_id, array('readonly'=>true)); ?>
            </td>
            
            
            <td><?php echo CHtml::activeLabel($modPasien, 'nama_bin',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPasienM[nama_bin]', $modPasien->nama_bin, array('readonly'=>true)); ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            
            <td><?php echo CHtml::activeLabel($modPendaftaran, 'ruangan_id',array('class'=>'control-label')); ?></td>
            <td><?php echo CHtml::textField('BKPendaftaranT[ruangan_nama]', ((isset($modPendaftaran->ruangan->ruangan_nama)) ? $modPendaftaran->ruangan->ruangan_nama : null), array('readonly'=>true)); ?></td>
        </tr>
    </table>
</fieldset> 
<?php 
//========= Dialog buat cari data pendaftaran =========================

$this->beginWidget('zii.widgets.jui.CJuiDialog', array( // the dialog
    'id'=>'dialogPasien',
    'options'=>array(
        'title'=>'Pencarian Data Pasien',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>900,
        'height'=>540,
        'resizable'=>false,
    ),
));
    $modDialogPasien = new BKPembayaranpelayananT('searchPasienRetur');
    $modDialogPasien->unsetAttributes();
    if(isset($_GET['BKPembayaranpelayananT'])) {
        $modDialogPasien->attributes = $_GET['BKPembayaranpelayananT'];
        $modDialogPasien->idInstalasi = $_GET['BKPembayaranpelayananT']['idInstalasi'];
        $modDialogPasien->no_pendaftaran = $_GET['BKPembayaranpelayananT']['no_pendaftaran'];
        $modDialogPasien->tgl_pendaftaran_cari = $_GET['BKPembayaranpelayananT']['tgl_pendaftaran_cari'];
        $modDialogPasien->instalasi_nama = $_GET['BKPembayaranpelayananT']['instalasi_nama'];
    }

    $this->widget('ext.bootstrap.widgets.BootGridView',array(
            'id'=>'pendaftaran-t-grid',
            'dataProvider'=>$modDialogPasien->searchPasienRetur(),
            'filter'=>$modDialogPasien,
            'template'=>"{pager}{summary}\n{items}",
            'itemsCssClass'=>'table table-striped table-bordered table-condensed',
            'columns'=>array(
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::Link("<i class=\"icon-check\"></i>","javascript:void(0);",array("class"=>"btn-small", 
                                        "id" => "selectPendaftaran",
                                        "onClick" => "
                                            $(\"#dialogPasien\").dialog(\"close\");
                                            $(\"#BKPendaftaranT_tgl_pendaftaran\").val(\"$data->tglPendaftaran\");
                                            $(\"#BKPendaftaranT_no_pendaftaran\").val(\"$data->noPendaftaran\");
                                            $(\"#BKPendaftaranT_umur\").val(\"$data->umur\");
                                            $(\"#BKPendaftaranT_jeniskasuspenyakit_nama\").val(\"$data->kasusPenyakit\");
                                            $(\"#BKPendaftaranT_instalasi_id\").val(\"$data->instalasiId\");
                                            $(\"#BKPendaftaranT_instalasi_nama\").val(\"$data->instalasiNama\");
                                            $(\"#BKPendaftaranT_ruangan_nama\").val(\"$data->ruanganNama\");
                                            $(\"#BKPendaftaranT_pendaftaran_id\").val(\"$data->pendaftaranId\");
                                            $(\"#BKPendaftaranT_pasien_id\").val(\"$data->pasienId\");
                                            $(\"#BKPendaftaranT_pasienadmisi_id\").val(\"$data->pasienAdmisiId\");
                                            $(\"#BKPasienM_jeniskelamin\").val(\"$data->jeniskelamin\");
                                            $(\"#BKPasienM_no_rekam_medik\").val(\"$data->noRM\");
                                            $(\"#BKPasienM_nama_pasien\").val(\"$data->namaPasien\");
                                            $(\"#BKPasienM_nama_bin\").val(\"$data->namaBIn\");
                                            
                                            $(\'#BKTandabuktikeluarT_namapenerima\').val(\"$data->namaPasien\");
                                            $(\'#BKTandabuktikeluarT_alamatpenerima\').val(\"$data->alamatPasien\");
                                            $(\'#BKReturbayarpelayananT_tandabuktibayar_id\').val(\"$data->tandabuktibayarId\");
                                            $(\'#BKReturbayarpelayananT_totaloaretur\').val(formatNumber(\"$data->totalbiayaoa\"));
                                            $(\'#BKReturbayarpelayananT_totaltindakanretur\').val(formatNumber(\"$data->totalbiayatindakan\"));
                                            $(\'#BKReturbayarpelayananT_totalbiayaretur\').val(formatNumber(\"$data->totalbiayapelayanan\"));

                                            $(\'#BKReturbayarpelayananT_noreturbayar\').focus();
                                            cekHakRetur();

                                        "))',
                    ),
                    array(
                        'name'=>'no_rekam_medik',
                        'type'=>'raw',
                        'value'=>'$data->pasien->no_rekam_medik',
                    ),
                    array(
                        'name'=>'nama_pasien',
                        'type'=>'raw',
                        'value'=>'$data->pasien->nama_pasien',
                    ),
                    'pasien.jeniskelamin',
                    'pendaftaran.no_pendaftaran',
                    array(
                        'name'=>'pendaftaran.tgl_pendaftaran',
                        'filter'=> 
                        CHtml::activeTextField($modDialogPasien, 'tgl_pendaftaran_cari', array('placeholder'=>'contoh: 15 Jan 2013')),
                    ),
                   array(
                        'name'=>'instalasi_nama',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->instalasi->instalasi_nama',
                    ),
                    array(
                        'name'=>'pendaftaran.ruangan.ruangan_nama',
                        'type'=>'raw',
                    ),
                    array(
                        'header'=>'Cara Bayar',
                        'name'=>'pendaftraran.carabayar.carabayar_nama',
                        'type'=>'raw',
                        'value'=>'$data->pendaftaran->carabayar->carabayar_nama',
                    ),
            ),
            'afterAjaxUpdate'=>'function(id, data){jQuery(\''.Params::TOOLTIP_SELECTOR.'\').tooltip({"placement":"'.Params::TOOLTIP_PLACEMENT.'"});}',
    ));

$this->endWidget();
////======= end pendaftaran dialog =============
?>
<script type="text/javascript">
function isiDataPasien(data)
{
    $('#BKPendaftaranT_tgl_pendaftaran').val(data.tglpendaftaran);
    $('#BKPendaftaranT_no_pendaftaran').val(data.nopendaftaran);
    $('#BKPendaftaranT_umur').val(data.umur);
    $('#BKPendaftaranT_jeniskasuspenyakit_nama').val(data.jeniskasuspenyakit);
    $('#BKPendaftaranT_instalasi_nama').val(data.namainstalasi);
    $('#BKPendaftaranT_ruangan_nama').val(data.namaruangan);
    $('#BKPendaftaranT_pendaftaran_id').val(data.pendaftaran_id);
    $('#BKPendaftaranT_pasien_id').val(data.pasien_id);
    $('#BKPendaftaranT_pasienadmisi_id').val(data.pasienadmisi_id);
    if (typeof data.norekammedik !=  'undefined'){
        $('#BKPasienM_no_rekam_medik').val(data.norekammedik);
    }
    $('#BKPasienM_jeniskelamin').val(data.jeniskelamin);
    $('#BKPasienM_nama_pasien').val(data.namapasien);
    $('#BKPasienM_nama_bin').val(data.namabin);
    
    $('#BKReturbayarpelayananT_tandabuktibayar_id').val(data.tandabuktibayar_id);
    $('#BKReturbayarpelayananT_totaloaretur').val(formatNumber(data.totalbiayaoa));
    $('#BKReturbayarpelayananT_totaltindakanretur').val(formatNumber(data.totalbiayatindakan));
    $('#BKReturbayarpelayananT_totalbiayaretur').val(formatNumber(data.totalbiayapelayanan));
    
    $('#BKReturbayarpelayananT_noreturbayar').focus();
}

function isiTandaBuktiKeluar(data)
{
    $('#BKTandabuktikeluarT_namapenerima').val(data.namapasien);
    $('#BKTandabuktikeluarT_alamatpenerima').val(data.alamatpasien);
}

function loadPembayaran(idPembayaran)
{
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/loadPembayaranRetur');?>', {idPembayaran:idPembayaran}, function(data){
        $('#tblBayarTind tbody').html(data.formBayarTindakan);
        $('#tblBayarOA tbody').html(data.formBayarOa);
        $('#TandabuktibayarT_jmlpembayaran').val(formatNumber(data.jmlpembayaran));
        $('#totTagihan').val(formatNumber(data.tottagihan));
        
        $('#BKTandabuktibayarUangMukaT_jmlpembulatan').val(formatNumber(data.jmlpembulatan));
        $('#BKTandabuktibayarUangMukaT_uangditerima').val(formatNumber(data.uangditerima));
        $('#BKTandabuktibayarUangMukaT_uangkembalian').val(formatNumber(data.uangkembalian));
        $('#BKTandabuktibayarUangMukaT_biayamaterai').val(formatNumber(data.biayamaterai));
        $('#BKTandabuktibayarUangMukaT_biayaadministrasi').val(formatNumber(data.biayaadministrasi));
        $('#BKTandabuktibayarUangMukaT_darinama_bkm').val(data.namapasien);
        $('#BKTandabuktibayarUangMukaT_alamat_bkm').val(data.alamatpasien);
    }, 'json');
}

function refreshDialogPendaftaran(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    var instalasiNama = $("#BKPendaftaranT_instalasi_id option:selected").text();
    $.fn.yiiGridView.update('pendaftaran-t-grid', {
        data: {
            "BKPembayaranpelayananT[idInstalasi]":instalasiId,
            "BKPembayaranpelayananT[instalasi_nama]":instalasiNama,
        }
    });
}
    
function cekInstalasi(){
    var instalasiId = $("#BKPendaftaranT_instalasi_id").val();
    if(instalasiId.length > 0){
        return true;
    }else{
        alert("Silahkan pilih instalasi ! ");
        $("#BKPendaftaranT_instalasi_id").focus();
        return false;
    }
}
</script>
