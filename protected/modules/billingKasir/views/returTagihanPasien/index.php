<?php
$this->breadcrumbs=array(
	'Pembayaran',
);?>

<?php
$this->widget('application.extensions.moneymask.MMask',array(
    'element'=>'.currency',
    'currency'=>'PHP',
    'config'=>array(
        'symbol'=>'Rp. ',
//        'showSymbol'=>true,
//        'symbolStay'=>true,
        'defaultZero'=>true,
        'allowZero'=>true,
        'decimal'=>',',
        'thousands'=>'.',
        'precision'=>0,
    )
));
?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/accounting.js'); ?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/form.js'); ?>
<?php $form=$this->beginWidget('ext.bootstrap.widgets.BootActiveForm',array(
	'id'=>'pembayaran-form',
	'enableAjaxValidation'=>false,
        'type'=>'horizontal',
        'focus'=>'#BKTandabuktibayarUangMukaT_jmlpembayaran',
        'htmlOptions'=>array(
            'onKeyPress'=>'return disableKeyPress(event)',
            'onsubmit'=>'return cekOtorisasi();'
        ),
));?>

<legend class="rim2">Transaksi Retur Tagihan Pasien</legend>

<?php $this->renderPartial('_ringkasDataPasien',array('modPendaftaran'=>$modPendaftaran,'modPasien'=>$modPasien));?>
<?php $this->widget('bootstrap.widgets.BootAlert'); ?>
<?php //echo $form->errorSummary(array($modRetur,$modBuktiKeluar)); ?>
<fieldset>
 <legend class="rim">Data Retur</legend>
    <table>
        <tr>
            <td width="50%">
                <?php echo CHtml::activeHiddenField($modRetur,'tandabuktikeluar_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modRetur,'tandabuktibayar_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modRetur,'returbayarpelayanan_id',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modRetur,'is_posting',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($modRetur,'ruangan_id',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php //echo $form->textFieldRow($modRetur,'tglreturpelayanan',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <div class="control-group ">
                    <?php $modRetur->tglreturpelayanan = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modRetur->tglreturpelayanan, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                    <?php echo $form->labelEx($modRetur,'tglreturpelayanan', array('class'=>'control-label')) ?>
                    <div class="controls">
                        <?php   
                                $this->widget('MyDateTimePicker',array(
                                            'model'=>$modRetur,
                                            'attribute'=>'tglreturpelayanan',
                                            'mode'=>'datetime',
                                            'options'=> array(
                                                'dateFormat'=>Params::DATE_FORMAT_MEDIUM,
                                                'maxDate' => 'd',
                                            ),
                                            'htmlOptions'=>array('class'=>'dtPicker2-5', 'onkeypress'=>"return $(this).focusNextInputField(event)"
                                            ),
                        )); ?>

                    </div>
                </div>
                <?php echo $form->textFieldRow($modRetur,'noreturbayar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo $form->textFieldRow($modRetur,'totaloaretur',array('class'=>'span3 currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modRetur,'totaltindakanretur',array('class'=>'span3 currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modRetur,'totalbiayaretur',array('class'=>'span3 currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modRetur,'biayaadministrasi',array('class'=>'span3 currency', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textAreaRow($modRetur,'keteranganretur',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modRetur,'user_nm_otorisasi',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo CHtml::activeHiddenField($modRetur,'user_id_otorisasi',array('readonly'=>true,'class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
            </td>
            <td width="50%">
                <?php //echo $form->dropDownListRow($modBuktiKeluar,'tahun', Params::tahun(),array('class'=>'span2', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>4)); ?>
                <?php $modBuktiKeluar->tglkaskeluar = Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($modBuktiKeluar->tglkaskeluar, 'yyyy-MM-dd hh:mm:ss','medium',null)); ?>
                <?php echo $form->textFieldRow($modBuktiKeluar,'nokaskeluar',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <?php echo $form->dropDownListRow($modBuktiKeluar,'carabayarkeluar', CaraBayarKeluar::items(),array('onchange'=>'formCarabayar(this.value)','class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>50)); ?>
                <div id="divCaraBayarTransfer" class="hide">
                    <?php echo $form->textFieldRow($modBuktiKeluar,'melalubank',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modBuktiKeluar,'denganrekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                    <?php echo $form->textFieldRow($modBuktiKeluar,'atasnamarekening',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                </div>
                <?php echo $form->textFieldRow($modBuktiKeluar,'namapenerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
                <?php echo $form->textAreaRow($modBuktiKeluar,'alamatpenerima',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);")); ?>
                <?php echo $form->textFieldRow($modBuktiKeluar,'untukpembayaran',array('class'=>'span3', 'onkeypress'=>"return $(this).focusNextInputField(event);", 'maxlength'=>100)); ?>
            </td>
        </tr>
    </table>

    <legend class="rim">Jurnal Rekening</legend>
    <table>
        <tr>
            <td width="70%">
                <div>
                    <?php
                        $this->renderPartial('billingKasir.views.returTagihanPasien.rekening._rowListRekening',
                            array(
                                'form'=>$form,
                            )
                        );
                    ?>
                </div>                
            </td>
        </tr>
    </table>
                 
</fieldset>
            
    <div class="form-actions">
        <div style="float:left;margin-right:6px;">
            <?php
                $controller = Yii::app()->controller->id;
                $module = Yii::app()->controller->module->id; 
                $urlSave=  Yii::app()->createAbsoluteUrl($module.'/'.$controller.'/index');
            
                if($modRetur->isNewRecord)
                {
                    $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'=>'primary',
                    'buttons'=>array(
                        array(
                            'label'=>'Simpan',
                            'icon'=>'icon-ok icon-white',
                            'url'=>"#",
                            'htmlOptions'=>
                                array(
                                    'onclick'=>'simpanPengeluaran(\'retur\');return false;',
                                )
                       ),
                        array(
                            'label'=>'',
                            'items'=>array(
                                array(
                                    'label'=>'Posting',
                                    'icon'=>'icon-ok',
                                    'url'=>"#",
                                    'itemOptions' => array(
                                        'onclick'=>'simpanPengeluaran(\'posting\');return false;'
                                    )
                                ),
                            )
                        ),
                        ),
                    ));
                    echo"</div>";
                    echo CHtml::link(
                        Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                        array(
                            'class'=>'btn btn-info',
                            'onclick'=>"return false",
                            'disabled'=>true
                        )
                    );
                }else{
                    $this->widget('bootstrap.widgets.BootButtonGroup', array(
                    'type'=>'primary',
                    'buttons'=>array(
                        array(
                            'label'=>'Simpan',
                            'icon'=>'icon-ok icon-white',
                            'url'=>"#",
                            'htmlOptions'=>
                                array(
                                    'onclick'=>'simpanPengeluaran(\'retur\');return false;', 'disabled'=>true
                                )
                       ),
                        array(
                            'label'=>'',
                            'items'=>array(
                                array(
                                    'label'=>'Posting',
                                    'icon'=>'icon-ok',
                                    'url'=>"#",
                                    'itemOptions' => array(
                                        'onclick'=>'simpan(\'\');return false;', 'disabled'=>true
                                    )
                                ),
                            )
                        ),
                        ),
                    ));
                    echo"</div>";
                    echo "&nbsp;&nbsp;";
                    echo CHtml::link(
                        Yii::t('mds', '{icon} Print', array('{icon}'=>'<i class="icon-print icon-white"></i>')), '#', 
                        array(
                            'class'=>'btn btn-info',
                            'onclick'=>"printKasir($modRetur->returbayarpelayanan_id);return false",
                            'disabled'=>false
                        )
                    );
                }
            ?>
            <?php
                echo CHtml::link(
                    Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    $url_batal,
                    array(
                        'class'=>'btn btn-danger',
                        'disabled'=>false
                    )
                );
                /*
                echo CHtml::htmlButton(
                    Yii::t('mds','{icon} Reset',array('{icon}'=>'<i class="icon-refresh icon-white"></i>')),
                    array(
                        'class'=>'btn btn-danger', 
                        'type'=>'reset'
                    )
                );
                 * 
                 */ 
            ?>
<?php  
$content = $this->renderPartial('../tips/transaksi',array(),true);
$this->widget('TipsMasterData',array('type'=>'transaksi','content'=>$content)); 
?>
    </div>

<?php $this->endWidget(); ?>

<script type="text/javascript">

getDataRekeningReturPelayanan();

$('.currency').each(function(){this.value = formatNumber(this.value)});
function printKasir(idTandaBukti)
{
    if(idTandaBukti.length > 0)
    {
        window.open('<?php echo Yii::app()->createUrl('print/returnTagihan',array('idTandaBukti'=>"")); ?>'+idTandaBukti,'printwin','left=100,top=100,width=800,height=400,scrollbars=1');
    }else{
        idTandaBukti = $('#BKReturbayarpelayananT_returbayarpelayanan_id').val();
        window.open('<?php echo Yii::app()->createUrl('print/returnTagihan',array('idTandaBukti'=>"")); ?>'+idTandaBukti,'printwin','left=100,top=100,width=800,height=400,scrollbars=1');
    }   
}

function simpanPengeluaran(params)
{
    // alert(params);
    $('#BKReturbayarpelayananT_is_posting').val(params);

    jenis_simpan = params;
    var kosong = "" ;
    var dataKosong = $("#input-pengeluaran").find(".reqForm[value="+ kosong +"]");
    if(dataKosong.length > 0){
        alert('Bagian dengan tanda * harus diisi ');
    }else{
        // var detail = 0;
        // $('#tblInputUraian tbody tr').each(
        //     function(){
        //         detail++;
        //     }
        // );
        
        // if(detail > 0){
            $('.currency').each(
                function(){
                    this.value = unformatNumber(this.value)
                }
            );
            $('#pembayaran-form').submit();
            /*MENGGUNAKAN METHOD POST PHP
            $.post('<?php // echo Yii::app()->createUrl(Yii::app()->controller->module->id . '/' . Yii::app()->controller->id . '/SimpanPengeluaran');?>', {jenis_simpan:jenis_simpan, data:$('#akpengeluaran-umum-t-form').serialize()},
                function(data){
                    if(data.status == 'ok')
                    {
                        if(data.action == 'insert')
                        {
                            alert("Simpan data berhasil");
                            $("#tblInputUraian").find('tr[class$="child"]').detach();
                            $("#reseter").click();
                            $("#input-pengeluaran").find("input[name$='[nopengeluaran]']").val(data.pesan.nopengeluaran);
                            $("#input-pengeluaran").find("input[name$='[nokaskeluar]']").val(data.pesan.nokaskeluar);
                            $("#tblInputRekening > tbody").find('tr').detach();
                        }else{
                            alert("Update data berhasil");
                        }
                    }
            }, "json");
                */
        // }else{
        //     alert('Detail uraian masih kosong');
        // }
         
    }
    
       
}

function simpan(){
    alert("Data tidak bisa di posting ulang!");
    return false;
}

/*
function printKasir(idTandaBukti)
{
    if(idTandaBukti>0)
    {
        window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('idTandaBukti'=>"")); ?>'+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    } else {
        idTandaBukti = $('#BKReturbayarpelayananT_tandabuktibayar_id').val();
        window.open('<?php echo Yii::app()->createUrl('print/bayarKasir',array('idTandaBukti'=>"")); ?>'+idTandaBukti,'printwin','left=100,top=100,width=400,height=400,scrollbars=1');
    }   
}
*/
function cekHakRetur()
{
    var cek = false;
    $.post('<?php echo Yii::app()->createUrl('billingKasir/ActionAjax/CekHakRetur');?>', {idUser:'<?php echo Yii::app()->user->id; ?>',useName:'<?php echo Yii::app()->user->name; ?>'}, function(data){
        if($('#BKReturbayarpelayananT_user_nm_otorisasi').val() != '' || $('#BKReturbayarpelayananT_user_id_otorisasi').val() != '')
            return true;
        if(data.cekAkses){
            //alert('punya hak');
            $('#BKReturbayarpelayananT_user_nm_otorisasi').val(data.username);
            $('#BKReturbayarpelayananT_user_id_otorisasi').val(data.userid);
            return true;
        } else {
            $('#loginDialog').dialog('open');
        }
    }, 'json');
    
    return cek;
}

function cekLogin()
{
    $.post('<?php echo Yii::app()->createUrl('ActionAjax/CekLogin',array('task'=>'Retur'));?>', $('#formLogin').serialize(), function(data){
        if(data.error != '')
            alert(data.error);
        $('#'+data.cssError).addClass('error');
        if(data.status=='success'){
            //alert(data.status);
            $('#BKReturbayarpelayananT_user_nm_otorisasi').val(data.username);
            $('#BKReturbayarpelayananT_user_id_otorisasi').val(data.userid);
            $('#loginDialog').dialog('close');
            $("#pembayaran-form").submit();
        }else{
            alert(data.status);
        }
    }, 'json');
}

function cekOtorisasi()
{
    if($('#BKReturbayarpelayananT_user_nm_otorisasi').val() == '' || $('#BKReturbayarpelayananT_user_id_otorisasi').val() == ''){
        $('#loginDialog').dialog('open');
        return false;
    } 
    
    $('.currency').each(function(){this.value = unformatNumber(this.value)});
    return true;
}

function formCarabayar(carabayar)
{
    //alert(carabayar);
    if(carabayar == 'TRANSFER'){
        $('#divCaraBayarTransfer').slideDown();
    } else {
        $('#divCaraBayarTransfer').slideUp();
        $('#divCaraBayarTransfer input').each(function(){$(this).val('')});
    }
}
</script>


<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array( 
    'id'=>'loginDialog',
    'options'=>array(
        'title'=>'Login',
        'autoOpen'=>false,
        'modal'=>true,
        'width'=>400,
        'height'=>190,
        'resizable'=>false,
    ),
));?>
<?php echo CHtml::beginForm('', 'POST', array('class'=>'form-horizontal','id'=>'formLogin')); ?>
    <div class="control-group ">
        <?php echo CHtml::label('Login Pemakai','username', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::textField('username', '', array()); ?>
        </div>
    </div>

    <div class="control-group ">
        <?php echo CHtml::label('Password','password', array('class'=>'control-label')) ?>
        <div class="controls">
            <?php echo CHtml::passwordField('password', '', array()); ?>
        </div>
    </div>
    
    <div class="form-actions">
        <?php
            echo CHtml::htmlButton(
                Yii::t('mds','{icon} Login',array('{icon}'=>'<i class="icon-lock icon-white"></i>')),
                array(
                    'class'=>'btn btn-primary',
                    'type'=>'submit',
                    'onclick'=>'cekLogin();return false;'
                )
            );
        ?>
        <?php
            echo CHtml::link(
                Yii::t('mds', '{icon} Cancel', array('{icon}'=>'<i class="icon-ban-circle icon-white"></i>')), '#', 
                array(
                    'class'=>'btn btn-danger',
                    'onclick'=>"$('#loginDialog').dialog('close');return false",
                    'disabled'=>false
                )
            );
        ?>
    </div> 
<?php echo CHtml::endForm(); ?>
<?php $this->endWidget();?>