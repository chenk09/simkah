<table>
    <tr>
        <td>
             <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('nofaktur')); ?>:</b>
            <?php echo CHtml::encode($modFakturPembelian->nofaktur); ?>
            <br />
             <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('tglfaktur')); ?>:</b>
            <?php echo CHtml::encode($modFakturPembelian->tglfaktur); ?>
            <br />
        </td>
<!--         <td>
            <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('totharganetto')); ?>:</b>
            <?php echo "Rp. ".CHtml::encode(MyFunction::formatNumber($modFakturPembelian->totharganetto)); ?>
            <br />
             <b><?php echo CHtml::encode($modFakturPembelian->getAttributeLabel('jmldiscount')); ?>:</b>
            <?php echo CHtml::encode($modFakturPembelian->jmldiscount); ?>
            <br />
        </td> -->
    </tr>   
</table>
<hr/>
<table id="tableObatAlkes" class="table table-bordered table-condensed">
    <thead>
    <tr>
        <th>No.Urut</th>
        <th>Sumber Dana</th>
        <th>Kategori/&nbsp;&nbsp;&nbsp;&nbsp;<br/>Nama Obat</th>
        <th>Jumlah Diterima</th>
        <th>Harga Netto</th>
        <th>Harga PPN</th>
        <th>Harga PPH</th>
        <th>Jumlah Diskon</th>
        <th>Sub Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
        $no=1;
        $totalJumlahDiterima = 0;
        $totalNetto = 0;
        $totalPPN = 0;
        $totalPPH = 0;
        $totalJumlahDiskon = 0;
        $totalHargaSatuan = 0;
        if (isset($modFakturPembelianDetails)){
            if (count($modFakturPembelianDetails) > 0){
                foreach($modFakturPembelianDetails AS $tampilData):
                    echo "<tr>
                            <td>".$no."</td>
                            <td>".$tampilData->sumberdana['sumberdana_nama']."</td>  
                            <td>".$tampilData->obatalkes['obatalkes_kategori']."<br>".$tampilData->obatalkes['obatalkes_nama']."</td>   
                            <td>".
								$tampilData['jmlterima'] . " " . $tampilData->satuanbesar['satuanbesar_nama'] ." / ".
								$tampilData['jmlkemasan'] . " " . $tampilData->satuankecil['satuankecil_nama']
							."</td>
                            <td>".MyFunction::formatNumber($tampilData['harganettofaktur'])."</td>     
                            <td>".MyFunction::formatNumber($tampilData['hargappnfaktur'])."</td>     
                            <td>".MyFunction::formatNumber($tampilData['hargapphfaktur'])."</td> 
                            <td>".MyFunction::formatNumber($tampilData['jmldiscount'])."</td>        
                            <td>".MyFunction::formatNumber(((($tampilData['harganettofaktur'] * $tampilData['jmlkemasan'])*$tampilData['jmlterima']) + $tampilData['hargappnfaktur'] + $tampilData['hargapphfaktur']) - $tampilData['jmldiscount'])."</td>
                         </tr>";  
                        $no++;
                        $totalJumlahDiterima+=$tampilData['jmlterima'];
                        $totalNetto+=$tampilData['harganettofaktur'];
                        $totalPPN+=$tampilData['hargappnfaktur'];
                        $totalPPH+=$tampilData['hargapphfaktur'];
                        $totalJumlahDiskon+=$tampilData['jmldiscount'];
                        $totalHargaSatuan+=((($tampilData['harganettofaktur'] * $tampilData['jmlkemasan'])*$tampilData['jmlterima']) + $tampilData['hargappnfaktur'] + $tampilData['hargapphfaktur']) - $tampilData['jmldiscount'];
                endforeach;
            }
        }
    echo "</tbody><tfoot><tr>
            <td colspan='4'> Total</td>
            <td>".MyFunction::formatNumber($totalNetto)."</td>
            <td>".MyFunction::formatNumber($totalPPN)."</td>
            <td>".MyFunction::formatNumber($totalPPH)."</td>
            <td>".MyFunction::formatNumber($totalJumlahDiskon)."</td>
            <td>".MyFunction::formatNumber($totalHargaSatuan)."</td>
         </tr></tfoot>";    
    ?>
</table>