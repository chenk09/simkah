<?php
$data = array();
if(count($model) > 0){
    foreach($model as $key=>$value)
    {
        // $key = 99;
        echo('<tr>');
                echo "<td>".
                        CHtml::hiddenField("row", 0,array('readonly'=>true, 'class'=>'span1')).
                        CHtml::textField("RekeningakuntansiV[$key][kdstruktur]", $value->rekening1->kdrekening1,array('readonly'=>true, 'class'=>'span1', 'style'=>'width:20px')).
                        CHtml::hiddenField("RekeningakuntansiV[$key][struktur_id]", $value->rekening1_id,array('readonly'=>true, 'class'=>'span1')).
                    "</td>";
                echo "<td>".
                        CHtml::textField("RekeningakuntansiV[$key][kdkelompok]", $value->rekening2->kdrekening2,array('readonly'=>true, 'class'=>'span1', 'style'=>'width:20px')).
                        CHtml::hiddenField("RekeningakuntansiV[$key][kelompok_id]", $value->rekening2_id,array('readonly'=>true, 'class'=>'span1')).
                    "</td>";
                echo "<td>".
                        CHtml::textField("RekeningakuntansiV[$key][kdjenis]", $value->rekening3->kdrekening3,array('readonly'=>true, 'class'=>'span1', 'style'=>'width:20px')).
                        CHtml::hiddenField("RekeningakuntansiV[$key][jenis_id]", $value->rekening3_id,array('readonly'=>true, 'class'=>'span1')).
                    "</td>";
                echo "<td>".
                        CHtml::textField("RekeningakuntansiV[$key][kdobyek]", $value->rekening4->kdrekening4,array('readonly'=>true, 'class'=>'span1', 'style'=>'width:20px')).
                        CHtml::hiddenField("RekeningakuntansiV[$key][obyek_id]", $value->rekening4_id,array('readonly'=>true, 'class'=>'span1')).
                    "</td>";
                echo "<td>".
                        CHtml::textField("RekeningakuntansiV[$key][kdrincianobyek]", $value->rekeningkredit->kdrekening5,array('readonly'=>true, 'class'=>'span1')).
                        CHtml::hiddenField("RekeningakuntansiV[$key][rincianobyek_id]", $value->rekening5_id,array('readonly'=>true, 'class'=>'span1')).
                    "</td>";
                echo('<td>');
                if(isset($value->rekening3->nmrekening3)){
                    $nama = $value->rekening3->nmrekening3;
                    if(isset($value->rekening4_id))
                    {
                        $nama = $value->rekening4->nmrekening4;
                        if(isset($value->rekening5_id))
                        {
                            $nama = $value->rekeningkredit->nmrekening5;
                        }
                    }
                }else{
                    $nama = "";
                }
                echo CHtml::hiddenField("RekeningakuntansiV[$key][nama_rekening]", $nama,array('readonly'=>true));
                $this->widget('MyJuiAutoComplete',
                    array(
                        'value'=>$nama,
                        'name' => "RekeningakuntansiV[$key][rekDebitKredit]",
                        'id' => "RekeningakuntansiV_".$key."_rekDebitKredit",
//                        'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>'Kredit')),
                        'sourceUrl' => Yii::app()->createUrl('ActionAutoComplete/rekeningAkuntansi', array('id_jenis_rek'=>null)),
                        'options' => array(
                            'showAnim' => 'fold',
                            'minLength' => 2,
                            'focus' => 'js:function( event, ui ){
                                return false;
                            }',
                            'select' => 'js:function( event, ui ){
                                $(this).val(ui.item.value);
                                var data = {
                                    //DATA DI TAMBAHKAN MELAUI FUNGSI .autocomplete di renameRowRekening()
                                };
                                editDataRekeningFromGrid(data, row);                            
                                return false;
                            }'
                        ),
                        'htmlOptions' => array(
                            'onkeypress' => "return $(this).focusNextInputField(event)",
                            'placeholder'=>'Ketikan Nama Rekening Debit',
                            'class'=>'span2',
                            'style'=>'width:150px;',
                        ),
                        'tombolDialog' => array(
                            'idDialog' => 'dialogRekDebitKredit',
                            'jsFunction'=>"setDialogRekening(this);",
                        ),
                    )
                );
            echo('</td>');
            
            echo '<td>';
                echo CHtml::textField("RekeningakuntansiV[$key][saldodebit]", 
                    0,
                    array(
                        'class'=>'inputFormTabel currency',
                        //'disabled'=>($status == 'debit' ? "" : "disabled"),
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    )
                );
            echo '</td>';
            echo '<td>';
                echo CHtml::textField("RekeningakuntansiV[$key][saldokredit]",
                    0,
                    array(
                        'class'=>'inputFormTabel currency',
                        //'disabled'=>($status == 'kredit' ? "" : "disabled"),
                        'onkeypress' => "return $(this).focusNextInputField(event)",
                    )
                );
            echo '</td>';
            echo('<td>');
               echo CHtml::link(
                       '<i class="icon-plus"></i>',
                       "#",
                       array(
                           'onclick'=>'addDataRekening(this); return false;',
                           'rel'=>"tooltip",
                           'data-original-title'=>"Menambah Rekening"
                       )
                );
               echo CHtml::link(
                       '<i class="icon-minus"></i>',
                       "#",
                       array(
                           'onclick'=>'removeDataRekening(this); return false;',
                           'rel'=>"tooltip",
                           'data-original-title'=>"Menghapus Rekening"
                       )
                );
            echo('</td>');
        echo('</tr>');
    }
}else{
    
}
?>
<script type="text/javascript">

ubahNilaiRekening();
function ubahNilaiRekening()
{
    var nilai = $('#BKReturPembelianT_totalretur').val();
    $('#RekeningakuntansiV_0_saldodebit').val(nilai);
    $('#RekeningakuntansiV_1_saldokredit').val(nilai);
}
    
</script>